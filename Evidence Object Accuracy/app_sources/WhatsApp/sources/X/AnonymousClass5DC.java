package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DC  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DC implements Iterator {
    public int A00 = 0;
    public final int A01;
    public final /* synthetic */ AbstractC111915Bh A02;

    public AnonymousClass5DC(AbstractC111915Bh r2) {
        this.A02 = r2;
        this.A01 = r2.A02();
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return C12990iw.A1Y(this.A00, this.A01);
    }

    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        try {
            AbstractC111915Bh r2 = this.A02;
            int i = this.A00;
            this.A00 = i + 1;
            return Byte.valueOf(r2.A01(i));
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException(e.getMessage());
        }
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw C12970iu.A0z();
    }
}
