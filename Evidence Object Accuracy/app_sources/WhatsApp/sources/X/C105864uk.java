package X;

/* renamed from: X.4uk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105864uk implements AbstractC12520i3 {
    public final int A00;
    public final AbstractC12520i3 A01;

    public C105864uk(AbstractC12520i3 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC12520i3
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof C105864uk)) {
                return false;
            }
            C105864uk r4 = (C105864uk) obj;
            if (this.A00 != r4.A00 || !this.A01.equals(r4.A01)) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AbstractC12520i3
    public int hashCode() {
        return (this.A01.hashCode() * 1013) + this.A00;
    }

    public String toString() {
        AnonymousClass0PG r2 = new AnonymousClass0PG(C12980iv.A0r(this));
        r2.A00(this.A01, "imageCacheKey");
        r2.A00(String.valueOf(this.A00), "frameIndex");
        return r2.toString();
    }
}
