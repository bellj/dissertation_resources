package X;

import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.2eC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53392eC extends AnonymousClass0PW {
    public static final Set A03;
    public static final Set A04 = Collections.unmodifiableSet(new HashSet(Collections.singletonList(5)));
    public final AbstractC15710nm A00;
    public final AnonymousClass5U7 A01;
    public final String A02;

    static {
        Integer[] numArr = new Integer[5];
        C12960it.A1O(numArr, 4);
        C12980iv.A1T(numArr, 11);
        C12990iw.A1V(numArr, 12);
        numArr[3] = 14;
        C12960it.A1P(numArr, 15, 4);
        A03 = Collections.unmodifiableSet(new HashSet(Arrays.asList(numArr)));
    }

    public C53392eC(AbstractC15710nm r1, AnonymousClass5U7 r2, String str) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = str;
    }

    @Override // X.AnonymousClass0PW
    public void A00() {
        Log.i("DeviceAuthenticationPlugin/AuthenticationCallback/failed");
    }

    @Override // X.AnonymousClass0PW
    public void A01(int i, CharSequence charSequence) {
        AnonymousClass5U7 r1;
        int i2;
        Log.i(C12960it.A0W(i, "DeviceAuthenticationPlugin/AuthenticationCallback/errorCode: "));
        Set set = A03;
        Integer valueOf = Integer.valueOf(i);
        if (set.contains(valueOf)) {
            this.A00.AaV(C12960it.A0d(this.A02, C12960it.A0k("DeviceAuthenticationPlugin/FatalError/")), String.valueOf(i), false);
            r1 = this.A01;
            i2 = 2;
        } else if (A04.contains(valueOf)) {
            this.A00.AaV(C12960it.A0d(this.A02, C12960it.A0k("DeviceAuthenticationPlugin/TemporaryError/")), String.valueOf(i), false);
            r1 = this.A01;
            i2 = 3;
        } else {
            this.A01.AMd(0);
            return;
        }
        r1.AMd(i2);
    }

    @Override // X.AnonymousClass0PW
    public void A02(C04700Ms r3) {
        Log.i("DeviceAuthenticationPlugin/AuthenticationCallback/succeeded");
        this.A01.AMd(-1);
    }
}
