package X;

import android.os.Build;
import android.view.View;
import android.view.WindowInsets;

/* renamed from: X.08o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C018408o {
    public static final C018408o A01;
    public final C06250St A00;

    static {
        C018408o r0;
        if (Build.VERSION.SDK_INT >= 30) {
            r0 = C02670Dm.A00;
        } else {
            r0 = C06250St.A01;
        }
        A01 = r0;
    }

    public C018408o() {
        this.A00 = new C06250St(this);
    }

    public C018408o(WindowInsets windowInsets) {
        C06250St r0;
        int i = Build.VERSION.SDK_INT;
        if (i >= 30) {
            r0 = new C02670Dm(this, windowInsets);
        } else if (i >= 29) {
            r0 = new C02680Dn(this, windowInsets);
        } else if (i >= 28) {
            r0 = new C02690Do(this, windowInsets);
        } else if (i >= 21) {
            r0 = new C02700Dp(this, windowInsets);
        } else if (i >= 20) {
            r0 = new C02710Dq(this, windowInsets);
        } else {
            r0 = new C06250St(this);
        }
        this.A00 = r0;
    }

    public static AnonymousClass0U7 A00(AnonymousClass0U7 r5, int i, int i2, int i3, int i4) {
        int max = Math.max(0, r5.A01 - i);
        int max2 = Math.max(0, r5.A03 - i2);
        int max3 = Math.max(0, r5.A02 - i3);
        int max4 = Math.max(0, r5.A00 - i4);
        if (max == i && max2 == i2 && max3 == i3 && max4 == i4) {
            return r5;
        }
        return AnonymousClass0U7.A00(max, max2, max3, max4);
    }

    public static C018408o A01(View view, WindowInsets windowInsets) {
        C018408o r2 = new C018408o(windowInsets);
        if (view != null && AnonymousClass028.A0q(view)) {
            C018408o A0G = AnonymousClass028.A0G(view);
            C06250St r1 = r2.A00;
            r1.A0D(A0G);
            r1.A0B(view.getRootView());
        }
        return r2;
    }

    public static C018408o A02(WindowInsets windowInsets) {
        return new C018408o(windowInsets);
    }

    @Deprecated
    public int A03() {
        return this.A00.A03().A00;
    }

    @Deprecated
    public int A04() {
        return this.A00.A03().A01;
    }

    @Deprecated
    public int A05() {
        return this.A00.A03().A02;
    }

    @Deprecated
    public int A06() {
        return this.A00.A03().A03;
    }

    public WindowInsets A07() {
        C06250St r1 = this.A00;
        if (r1 instanceof C02710Dq) {
            return ((C02710Dq) r1).A03;
        }
        return null;
    }

    @Deprecated
    public C018408o A08(int i, int i2, int i3, int i4) {
        AnonymousClass0RW r0 = new AnonymousClass0RW(this);
        AnonymousClass0U7 A00 = AnonymousClass0U7.A00(i, i2, i3, i4);
        AnonymousClass0PY r02 = r0.A00;
        r02.A02(A00);
        return r02.A00();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C018408o)) {
            return false;
        }
        return C015407h.A01(this.A00, ((C018408o) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
