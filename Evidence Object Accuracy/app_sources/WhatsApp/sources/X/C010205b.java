package X;

/* renamed from: X.05b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C010205b extends AbstractC010305c {
    public final /* synthetic */ AnonymousClass01F A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C010205b(AnonymousClass01F r2) {
        super(false);
        this.A00 = r2;
    }

    @Override // X.AbstractC010305c
    public void A00() {
        AnonymousClass01F r1 = this.A00;
        r1.A0k(true);
        if (r1.A0R.A01) {
            r1.A0n();
        } else {
            r1.A01.A00();
        }
    }
}
