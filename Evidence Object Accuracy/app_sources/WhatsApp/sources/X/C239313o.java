package X;

import java.util.Set;

/* renamed from: X.13o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239313o {
    public Set A00 = null;
    public final C233511i A01;
    public final C17230qT A02;

    public C239313o(C233511i r2, C17230qT r3) {
        this.A02 = r3;
        this.A01 = r2;
    }

    public static boolean A00(String str) {
        String A00 = C27811Jh.A00(str);
        return "clearChat".equals(A00) || "deleteChat".equals(A00) || "deleteMessageForMe".equals(A00);
    }
}
