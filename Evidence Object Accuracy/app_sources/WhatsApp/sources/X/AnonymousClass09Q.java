package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.09Q  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09Q extends AnimatorListenerAdapter implements AbstractC018608t, AbstractC12400hr {
    public boolean A00 = false;
    public boolean A01;
    public final int A02;
    public final View A03;
    public final ViewGroup A04;
    public final boolean A05;

    @Override // X.AbstractC018608t
    public void AXt(AnonymousClass072 r1) {
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
    }

    public AnonymousClass09Q(View view, int i) {
        this.A03 = view;
        this.A02 = i;
        this.A04 = (ViewGroup) view.getParent();
        this.A05 = true;
        A00(true);
    }

    public final void A00(boolean z) {
        ViewGroup viewGroup;
        if (this.A05 && this.A01 != z && (viewGroup = this.A04) != null) {
            this.A01 = z;
            AnonymousClass0LH.A00(viewGroup, z);
        }
    }

    @Override // X.AbstractC018608t
    public void AXq(AnonymousClass072 r3) {
        if (!this.A00) {
            AnonymousClass0U3.A01(this.A03, this.A02);
            ViewGroup viewGroup = this.A04;
            if (viewGroup != null) {
                viewGroup.invalidate();
            }
        }
        A00(false);
        r3.A09(this);
    }

    @Override // X.AbstractC018608t
    public void AXr(AnonymousClass072 r2) {
        A00(false);
    }

    @Override // X.AbstractC018608t
    public void AXs(AnonymousClass072 r2) {
        A00(true);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A00 = true;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        if (!this.A00) {
            AnonymousClass0U3.A01(this.A03, this.A02);
            ViewGroup viewGroup = this.A04;
            if (viewGroup != null) {
                viewGroup.invalidate();
            }
        }
        A00(false);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener, X.AbstractC12400hr
    public void onAnimationPause(Animator animator) {
        if (!this.A00) {
            AnonymousClass0U3.A01(this.A03, this.A02);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorPauseListener, X.AbstractC12400hr
    public void onAnimationResume(Animator animator) {
        if (!this.A00) {
            AnonymousClass0U3.A01(this.A03, 0);
        }
    }
}
