package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.contact.picker.ContactPicker;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.1Kz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC28171Kz extends AnonymousClass1L0 {
    public AnonymousClass2FR A00;
    public AnonymousClass2F9 A01;
    public C20670w8 A02;
    public C15550nR A03;
    public C17050qB A04;
    public C20650w6 A05;
    public C20820wN A06;
    public C20850wQ A07;
    public C26041Bu A08;
    public C18470sV A09;
    public C20710wC A0A;
    public C19890uq A0B;
    public C20740wF A0C;
    public C18350sJ A0D;
    public AbstractC15850o0 A0E;
    public final boolean A0F;
    public final boolean A0G;

    public AbstractActivityC28171Kz() {
        this(false, true);
    }

    public AbstractActivityC28171Kz(boolean z, boolean z2) {
        this.A0G = z;
        this.A0F = z2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009f, code lost:
        if (((X.ActivityC13810kN) r3).A09.A00.getBoolean("support_ban_appeal_screen_before_verification", false) != false) goto L_0x00a1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2e() {
        /*
        // Method dump skipped, instructions count: 922
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC28171Kz.A2e():void");
    }

    public void A2f(C29851Uy r2) {
        ContactPickerFragment contactPickerFragment;
        if (this instanceof VoipActivityV2) {
            contactPickerFragment = ((VoipActivityV2) this).A18;
        } else if (this instanceof ContactPicker) {
            contactPickerFragment = ((ContactPicker) this).A05;
        } else {
            return;
        }
        if (contactPickerFragment != null) {
            contactPickerFragment.A0l.notifyDataSetChanged();
            ContactPickerFragment.A2d = false;
        }
    }

    public void A2g(boolean z) {
        this.A01.A03(z, true);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 200) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            AnonymousClass2F9 r0 = this.A01;
            if (((AbstractC44141yJ) r0).A03.A03(r0.A06)) {
                this.A01.A02();
            }
        } else {
            A2g(false);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AnonymousClass2FR r1 = this.A00;
        boolean z = this.A0G;
        boolean z2 = this.A0F;
        C48292Fk r21 = new C48292Fk(this);
        AnonymousClass01J r11 = r1.A00.A03;
        C18850tA r0 = (C18850tA) r11.AKx.get();
        C14950mJ r02 = (C14950mJ) r11.AKf.get();
        C27041Fu r15 = (C27041Fu) r11.A1I.get();
        C19890uq r14 = (C19890uq) r11.ABw.get();
        C19490uC r13 = (C19490uC) r11.A1A.get();
        C27021Fs r12 = (C27021Fs) r11.A3R.get();
        C15830ny r10 = (C15830ny) r11.AKH.get();
        AbstractC15850o0 r9 = (AbstractC15850o0) r11.ANA.get();
        C15860o1 r8 = (C15860o1) r11.A3H.get();
        C17050qB r7 = (C17050qB) r11.ABL.get();
        C15880o3 r6 = (C15880o3) r11.ACF.get();
        C20740wF r4 = (C20740wF) r11.AIA.get();
        C18350sJ r3 = (C18350sJ) r11.AHX.get();
        C26041Bu r2 = (C26041Bu) r11.ACL.get();
        AnonymousClass2F9 r03 = new AnonymousClass2F9(this, (C14900mE) r11.A8X.get(), (C15570nT) r11.AAr.get(), r21, r15, r0, r7, r02, r13, r6, (C20850wQ) r11.ACI.get(), r2, r12, r14, r4, (C25651Af) r11.AFq.get(), r3, r8, r9, r10, (AbstractC14440lR) r11.ANe.get(), z, z2);
        this.A01 = r03;
        ((AbstractC44141yJ) r03).A00.A05(this, new AnonymousClass02B() { // from class: X.2Fn
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AbstractActivityC28171Kz r22 = AbstractActivityC28171Kz.this;
                if (((Number) obj).intValue() == 0) {
                    Log.e("VerifyMessageStoreActivity/messagestoreverified/missing-params bounce to regphone");
                    r22.A0D.A0A(1);
                    r22.startActivity(C14960mK.A05(r22));
                    r22.finish();
                }
            }
        });
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        Dialog dialog;
        AnonymousClass2F9 r4 = this.A01;
        if (i == 100) {
            Log.i("verifymsgstore/dialog/setup");
            Activity activity = r4.A01;
            ProgressDialogC48342Fq r1 = new ProgressDialogC48342Fq(activity);
            AnonymousClass2F9.A0B = r1;
            r1.setTitle(activity.getString(R.string.msg_store_migrate_title));
            AnonymousClass2F9.A0B.setMessage(activity.getString(R.string.msg_store_migrate_message));
            AnonymousClass2F9.A0B.setIndeterminate(false);
            AnonymousClass2F9.A0B.setCancelable(false);
            AnonymousClass2F9.A0B.setProgressStyle(1);
            dialog = AnonymousClass2F9.A0B;
        } else if (i == 101) {
            Log.i("verifymsgstore/dialog/msgstoreerror");
            C004802e r2 = new C004802e(r4.A01);
            r2.A07(R.string.alert);
            r2.A06(R.string.msg_store_error_found);
            r2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() { // from class: X.2G3
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    AnonymousClass2F9.this.A01.finish();
                }
            });
            dialog = r2.create();
        } else if (i == 200) {
            Log.i("verifymsgstore/dialog/cannot-connect");
            dialog = r4.A01(200, R.string.msg_store_unable_to_start_restore_no_connectivity);
        } else if (i != 201) {
            switch (i) {
                case 103:
                    Log.i("verifymsgstore/dialog/restore");
                    C004802e r22 = new C004802e(r4.A01);
                    r22.A07(R.string.msg_store_backup_found);
                    r22.A06(R.string.msg_store_creation_backup_message);
                    r22.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() { // from class: X.2G5
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            AnonymousClass2F9 r23 = AnonymousClass2F9.this;
                            C36021jC.A00(r23.A01, 103);
                            r23.A00 = true;
                            r23.A03(true, false);
                        }
                    });
                    r22.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() { // from class: X.2G7
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            Activity activity2 = AnonymousClass2F9.this.A01;
                            C36021jC.A00(activity2, 103);
                            C36021jC.A01(activity2, 106);
                        }
                    });
                    r22.A0B(false);
                    dialog = r22.create();
                    break;
                case 104:
                    Log.i("verifymsgstore/dialog/groupsync");
                    Activity activity2 = r4.A01;
                    ProgressDialogC48342Fq r23 = new ProgressDialogC48342Fq(activity2);
                    r23.setTitle(R.string.register_xmpp_title);
                    r23.setMessage(activity2.getString(R.string.register_wait_message));
                    r23.setIndeterminate(true);
                    r23.setCancelable(false);
                    return r23;
                case 105:
                    Log.i("verifymsgstore/dialog/restoreduetoerror");
                    StringBuilder sb = new StringBuilder();
                    Activity activity3 = r4.A01;
                    sb.append(activity3.getString(R.string.msg_store_lost_due_to_previous_error));
                    sb.append(" ");
                    sb.append(activity3.getString(R.string.msg_store_creation_backup_message_restore_due_to_error));
                    String obj = sb.toString();
                    C004802e r24 = new C004802e(activity3);
                    r24.A07(R.string.msg_store_backup_found_title);
                    r24.A0A(obj);
                    r24.setPositiveButton(R.string.msg_store_restore_db, new DialogInterface.OnClickListener() { // from class: X.2G0
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            AnonymousClass2F9 r25 = AnonymousClass2F9.this;
                            C36021jC.A00(r25.A01, 105);
                            Log.i("verifymsgstore/dialog/restoreduetoerror/restore");
                            r25.A00 = true;
                            r25.A03(true, false);
                        }
                    });
                    r24.setNegativeButton(R.string.msg_store_do_not_restore, new DialogInterface.OnClickListener() { // from class: X.2G1
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            Activity activity4 = AnonymousClass2F9.this.A01;
                            C36021jC.A00(activity4, 105);
                            Log.i("verifymsgstore/dialog/restoreduetoerror/skiprestore");
                            C36021jC.A01(activity4, 106);
                        }
                    });
                    r24.A0B(false);
                    dialog = r24.create();
                    break;
                case 106:
                    C004802e r25 = new C004802e(r4.A01);
                    r25.A07(R.string.msg_store_confirm);
                    r25.A06(R.string.dont_restore_message);
                    r25.setPositiveButton(R.string.msg_store_do_not_restore, new DialogInterface.OnClickListener() { // from class: X.2Fx
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            AnonymousClass2F9 r26 = AnonymousClass2F9.this;
                            C36021jC.A00(r26.A01, 106);
                            Log.i("verifymsgstore/dialog/checknorestore/skiprestore");
                            r26.A00 = false;
                            r26.A03(false, false);
                        }
                    });
                    r25.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.2Fy
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i2) {
                            AnonymousClass2F9 r26 = AnonymousClass2F9.this;
                            C36021jC.A00(r26.A01, 106);
                            Log.i("verifymsgstore/dialog/checknorestore/restore");
                            r26.A00 = true;
                            r26.A03(true, false);
                        }
                    });
                    r25.A0B(false);
                    dialog = r25.create();
                    break;
                case 107:
                    Log.i("verifymsgstore/dialog/restorefrombackupduetoerrorcardnotfoundaskretry");
                    StringBuilder sb2 = new StringBuilder();
                    Activity activity4 = r4.A01;
                    sb2.append(activity4.getString(R.string.msg_store_lost_due_to_previous_error));
                    sb2.append(" ");
                    boolean A00 = C14950mJ.A00();
                    int i2 = R.string.msg_store_media_card_not_found_ask_retry_shared_storage;
                    if (A00) {
                        i2 = R.string.msg_store_media_card_not_found_ask_retry;
                    }
                    sb2.append(activity4.getString(i2));
                    String obj2 = sb2.toString();
                    C004802e r26 = new C004802e(activity4);
                    r26.A07(R.string.alert);
                    r26.A0A(obj2);
                    r26.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() { // from class: X.2Fu
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i3) {
                            AnonymousClass2F9 r27 = AnonymousClass2F9.this;
                            C36021jC.A00(r27.A01, 107);
                            if (((AbstractC44141yJ) r27).A03.A03(r27.A06)) {
                                r27.A02();
                            }
                        }
                    });
                    r26.setNegativeButton(R.string.skip, new DialogInterface.OnClickListener() { // from class: X.2Fw
                        @Override // android.content.DialogInterface.OnClickListener
                        public final void onClick(DialogInterface dialogInterface, int i3) {
                            AnonymousClass2F9 r27 = AnonymousClass2F9.this;
                            C36021jC.A00(r27.A01, 107);
                            r27.A00 = false;
                            r27.A03(false, false);
                        }
                    });
                    r26.A0B(false);
                    dialog = r26.create();
                    break;
                case C43951xu.A03 /* 108 */:
                    Log.i("verifymsgstore/dialog/msgstorenotrestored");
                    C004802e r12 = new C004802e(r4.A01);
                    r12.A07(R.string.alert);
                    r12.A06(R.string.msg_store_error_not_restored);
                    r12.setPositiveButton(R.string.ok, null);
                    dialog = r12.create();
                    break;
                default:
                    return super.onCreateDialog(i);
            }
        } else {
            Log.i("verifymsgstore/dialog/keyserviceunavailable");
            dialog = r4.A01(201, R.string.msg_store_unable_to_start_restore_process);
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(i);
    }
}
