package X;

/* renamed from: X.4xg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107624xg implements AnonymousClass5XM {
    public final int A00;

    @Override // X.AnonymousClass5XM
    public int AFt(int i) {
        int i2 = i - 1;
        if (i2 >= 0) {
            return i2;
        }
        return -1;
    }

    public C107624xg(int i) {
        this.A00 = i;
    }

    @Override // X.AnonymousClass5XM
    public AnonymousClass5XM A7I() {
        return new C107624xg(0);
    }

    @Override // X.AnonymousClass5XM
    public AnonymousClass5XM A7J(int i, int i2) {
        return new C107624xg(this.A00 + i2);
    }

    @Override // X.AnonymousClass5XM
    public AnonymousClass5XM A7K(int i, int i2) {
        return new C107624xg((this.A00 - i2) + 0);
    }

    @Override // X.AnonymousClass5XM
    public int AD2() {
        return this.A00 > 0 ? 0 : -1;
    }

    @Override // X.AnonymousClass5XM
    public int ADm() {
        int i = this.A00;
        int i2 = i - 1;
        if (i <= 0) {
            return -1;
        }
        return i2;
    }

    @Override // X.AnonymousClass5XM
    public int AEe(int i) {
        int i2 = i + 1;
        if (i2 >= this.A00) {
            return -1;
        }
        return i2;
    }

    @Override // X.AnonymousClass5XM
    public int getLength() {
        return this.A00;
    }
}
