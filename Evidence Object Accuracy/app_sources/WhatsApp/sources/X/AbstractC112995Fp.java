package X;

/* renamed from: X.5Fp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC112995Fp implements AnonymousClass5XE, AnonymousClass20H {
    public final AnonymousClass5XE A00;

    public AbstractC112995Fp(AnonymousClass5XE r1) {
        this.A00 = r1;
    }

    public void A01(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        int i4 = i + i2;
        if (i4 > bArr.length) {
            throw new AnonymousClass5O2("input buffer too small");
        } else if (i3 + i2 > bArr2.length) {
            throw new C114975Nu("output buffer too short");
        } else {
            while (i < i4) {
                i3++;
                i++;
                bArr2[i3] = A00(bArr[i]);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        if (r0 == r2) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
        r6.A01 = 0;
        r1 = r6.A06;
        r0 = r1.length - r2;
        java.lang.System.arraycopy(r1, r2, r1, 0, r0);
        java.lang.System.arraycopy(r3, 0, r1, r0, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0063, code lost:
        if (r1 == r2) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte A00(byte r9) {
        /*
        // Method dump skipped, instructions count: 517
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC112995Fp.A00(byte):byte");
    }
}
