package X;

import android.graphics.Point;
import android.graphics.Rect;
import java.util.concurrent.Callable;

/* renamed from: X.6KT  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KT implements Callable {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KT(Rect rect, AnonymousClass661 r2) {
        this.A01 = r2;
        this.A00 = rect;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        Point point;
        AnonymousClass661 r1 = this.A01;
        if (r1.isConnected()) {
            r1.A09();
            C129305xV r4 = r1.A0K;
            Rect rect = this.A00;
            AnonymousClass664 r3 = new AnonymousClass664(this);
            r4.A06.A06("Focus requests must be on the Optic thread. ");
            if (r4.A09) {
                C129885yS r8 = r4.A05;
                if (C117295Zj.A1S(AbstractC130695zp.A0P, r8.A01(r4.A00))) {
                    C119105ct A00 = r8.A00(r4.A00);
                    ((AbstractC125485rK) A00).A00.A01(AbstractC130685zo.A0b, AnonymousClass61X.A04(rect));
                    A00.A02();
                }
                if (r4.A09) {
                    AbstractC130695zp A01 = r8.A01(r4.A00);
                    C125465rI r2 = AbstractC130695zp.A0O;
                    if (C117295Zj.A1S(r2, A01) || C117295Zj.A1S(AbstractC130695zp.A04, A01)) {
                        if (r4.A08) {
                            r4.A01.cancelAutoFocus();
                        }
                        r4.A07 = false;
                        r4.A08 = true;
                        r4.A0A = false;
                        C119105ct A002 = r8.A00(r4.A00);
                        if (C117295Zj.A1S(r2, r8.A01(r4.A00))) {
                            ((AbstractC125485rK) A002).A00.A01(AbstractC130685zo.A0B, AnonymousClass61X.A04(rect));
                            point = new Point(rect.centerX(), rect.centerY());
                        } else {
                            point = new Point(0, 0);
                        }
                        AbstractC125485rK.A02(AbstractC130685zo.A0C, A002, 1);
                        A002.A02();
                        r4.A00(point, EnumC124565pk.FOCUSING, r4.A02);
                        r4.A01.autoFocus(new AnonymousClass638(point, r3, r4));
                    }
                }
            }
        }
        return null;
    }
}
