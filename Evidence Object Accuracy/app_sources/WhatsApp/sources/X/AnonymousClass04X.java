package X;

import android.view.View;

/* renamed from: X.04X  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass04X {
    public void A00(int i) {
    }

    public int A01(View view) {
        return 0;
    }

    public int A02(View view) {
        return 0;
    }

    public abstract int A03(View view, int i, int i2);

    public abstract int A04(View view, int i, int i2);

    public abstract void A05(View view, float f, float f2);

    public void A06(View view, int i) {
    }

    public abstract void A07(View view, int i, int i2, int i3, int i4);

    public abstract boolean A08(View view, int i);
}
