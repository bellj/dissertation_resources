package X;

import android.database.AbstractCursor;
import com.whatsapp.jid.Jid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0na  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15630na extends AbstractCursor {
    public final int A00;
    public final int A01;
    public final int A02;
    public final C15610nY A03;
    public final C15620nZ A04;
    public final C15430nF A05;
    public final List A06;
    public final String[] A07;

    @Override // android.database.AbstractCursor, android.database.Cursor
    public boolean isNull(int i) {
        return false;
    }

    public C15630na(C15610nY r5, C15620nZ r6, C15430nF r7, List list, String[] strArr) {
        int length;
        strArr = strArr == null ? new String[]{"_id", "display_name", "is_group"} : strArr;
        this.A07 = strArr;
        this.A06 = new ArrayList(list);
        this.A03 = r5;
        this.A05 = r7;
        this.A04 = r6;
        int i = 0;
        while (true) {
            length = strArr.length;
            if (i < length) {
                if (strArr[i].equals("_id")) {
                    break;
                }
                i++;
            } else {
                i = -1;
                break;
            }
        }
        this.A01 = i;
        int i2 = 0;
        while (true) {
            if (i2 < length) {
                if (strArr[i2].equals("display_name")) {
                    break;
                }
                i2++;
            } else {
                i2 = -1;
                break;
            }
        }
        this.A00 = i2;
        int i3 = 0;
        while (true) {
            if (i3 < length) {
                if (strArr[i3].equals("is_group")) {
                    break;
                }
                i3++;
            } else {
                i3 = -1;
                break;
            }
        }
        this.A02 = i3;
    }

    public final C15370n3 A00(int i) {
        if (i >= 0) {
            List list = this.A06;
            if (i < list.size()) {
                return (C15370n3) list.get(i);
            }
        }
        StringBuilder sb = new StringBuilder("Position: ");
        sb.append(i);
        sb.append(", size = ");
        sb.append(this.A06.size());
        throw new IllegalStateException(sb.toString());
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String[] getColumnNames() {
        return this.A07;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getCount() {
        return this.A06.size();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public double getDouble(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public float getFloat(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getInt(int i) {
        if (i != -1) {
            C15370n3 A00 = A00(getPosition());
            if (i == this.A02) {
                return A00.A0K() ? 1 : 0;
            }
            StringBuilder sb = new StringBuilder("Column #");
            sb.append(i);
            sb.append(" is not an int.");
            throw new IllegalStateException(sb.toString());
        }
        throw new IllegalStateException("Invalid column index");
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public long getLong(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public short getShort(int i) {
        throw new UnsupportedOperationException();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String getString(int i) {
        if (i != -1) {
            C15370n3 A00 = A00(getPosition());
            if (i == this.A01) {
                C15620nZ r1 = this.A04;
                C15430nF r2 = this.A05;
                Jid A0B = A00.A0B(AbstractC14640lm.class);
                if (A0B == null) {
                    return null;
                }
                return r1.A01.A03(r2, A0B.getRawString());
            } else if (i == this.A00) {
                return this.A03.A0D(A00, false, false);
            } else {
                if (i == this.A02) {
                    return Integer.toString(getInt(i));
                }
                StringBuilder sb = new StringBuilder("Column #");
                sb.append(i);
                sb.append(" is not a string.");
                throw new IllegalStateException(sb.toString());
            }
        } else {
            throw new IllegalStateException("Invalid column index");
        }
    }
}
