package X;

import com.whatsapp.contact.picker.invite.InviteNonWhatsAppContactPickerActivity;

/* renamed from: X.4rI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103774rI implements AnonymousClass07L {
    public final /* synthetic */ InviteNonWhatsAppContactPickerActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C103774rI(InviteNonWhatsAppContactPickerActivity inviteNonWhatsAppContactPickerActivity) {
        this.A00 = inviteNonWhatsAppContactPickerActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        this.A00.A0K.A04(str);
        return false;
    }
}
