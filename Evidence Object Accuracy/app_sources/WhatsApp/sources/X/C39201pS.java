package X;

import java.io.File;

/* renamed from: X.1pS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39201pS extends AbstractC39111pJ {
    public final AnonymousClass1KA A00;
    public final String A01;
    public final boolean A02;
    public final boolean A03;

    public C39201pS(C39741qT r9, AbstractC14470lU r10, C39781qX r11, C39771qW r12, AnonymousClass1KA r13, AbstractC39761qV r14, File file, String str, boolean z, boolean z2) {
        super(r9, r10, r11, r12, r14, file);
        this.A01 = str;
        this.A00 = r13;
        this.A03 = z;
        this.A02 = z2;
    }
}
