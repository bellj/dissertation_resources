package X;

/* renamed from: X.2AZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AZ extends C47312Ad {
    public final C44641zJ A00;

    public AnonymousClass2AZ(C32821cn r7, C33241dg r8, Long l, String str, String str2, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        super(r7, bArr, bArr2, bArr3);
        C44621zH r3 = (C44621zH) C44631zI.A0e.A0T();
        if (str != null) {
            r3.A03();
            C44631zI r1 = (C44631zI) r3.A00;
            r1.A01 |= 1;
            r1.A04 = str;
        }
        if (str2 != null) {
            String substring = str2.substring(str2.length() - 2);
            r3.A03();
            C44631zI r12 = (C44631zI) r3.A00;
            r12.A01 |= 4;
            r12.A06 = substring;
        }
        if (l != null) {
            long longValue = l.longValue();
            r3.A03();
            C44631zI r4 = (C44631zI) r3.A00;
            r4.A02 |= 32;
            r4.A03 = longValue;
        }
        if (r8 != null) {
            r8.A03(r3);
        }
        C82503vj r42 = (C82503vj) C44641zJ.A05.A0T();
        EnumC87184An r2 = EnumC87184An.A02;
        r42.A03();
        C44641zJ r13 = (C44641zJ) r42.A00;
        r13.A00 |= 1;
        r13.A01 = r2.value;
        AnonymousClass1G4 A0T = C57482n8.A06.A0T();
        byte[] bArr4 = r7.A03;
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr4, 0, bArr4.length);
        A0T.A03();
        C57482n8 r14 = (C57482n8) A0T.A00;
        r14.A00 |= 1;
        r14.A01 = A01;
        String str3 = r7.A00;
        A0T.A03();
        C57482n8 r15 = (C57482n8) A0T.A00;
        r15.A00 |= 2;
        r15.A05 = str3;
        byte[] bArr5 = r7.A04;
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr5, 0, bArr5.length);
        A0T.A03();
        C57482n8 r16 = (C57482n8) A0T.A00;
        r16.A00 |= 4;
        r16.A04 = A012;
        byte[] bArr6 = r7.A02;
        AbstractC27881Jp A013 = AbstractC27881Jp.A01(bArr6, 0, bArr6.length);
        A0T.A03();
        C57482n8 r17 = (C57482n8) A0T.A00;
        r17.A00 |= 8;
        r17.A03 = A013;
        byte[] bArr7 = r7.A01;
        AbstractC27881Jp A014 = AbstractC27881Jp.A01(bArr7, 0, bArr7.length);
        A0T.A03();
        C57482n8 r18 = (C57482n8) A0T.A00;
        r18.A00 |= 16;
        r18.A02 = A014;
        r42.A03();
        C44641zJ r19 = (C44641zJ) r42.A00;
        r19.A04 = (C57482n8) A0T.A02();
        r19.A00 |= 2;
        r42.A05(r3);
        this.A00 = (C44641zJ) r42.A02();
    }
}
