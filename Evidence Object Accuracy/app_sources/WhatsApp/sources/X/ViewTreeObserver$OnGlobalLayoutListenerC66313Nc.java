package X;

import android.view.ViewTreeObserver;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.whatsapp.conversation.waveforms.VoiceVisualizer;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3Nc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC66313Nc implements ViewTreeObserver.OnGlobalLayoutListener {
    public boolean A00 = true;
    public final /* synthetic */ AbstractC28651Ol A01;
    public final /* synthetic */ C14680lr A02;
    public final /* synthetic */ File A03;

    public ViewTreeObserver$OnGlobalLayoutListenerC66313Nc(AbstractC28651Ol r2, C14680lr r3, File file) {
        this.A02 = r3;
        this.A03 = file;
        this.A01 = r2;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C14680lr r3 = this.A02;
        VoiceVisualizer voiceVisualizer = r3.A0D;
        double floor = Math.floor((double) (C12990iw.A02(voiceVisualizer) / voiceVisualizer.A0A));
        if (floor != 0.0d || !this.A00) {
            this.A00 = true;
            C12980iv.A1E(r3.A06, this);
            r3.A0F.Ab6(new Runnable(this.A01, this, this.A03, floor) { // from class: X.2ip
                public final /* synthetic */ double A00;
                public final /* synthetic */ AbstractC28651Ol A01;
                public final /* synthetic */ ViewTreeObserver$OnGlobalLayoutListenerC66313Nc A02;
                public final /* synthetic */ File A03;

                {
                    this.A02 = r2;
                    this.A03 = r3;
                    this.A00 = r4;
                    this.A01 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ViewTreeObserver$OnGlobalLayoutListenerC66313Nc r5 = this.A02;
                    File file = this.A03;
                    double d = this.A00;
                    AbstractC28651Ol r4 = this.A01;
                    if (d == 0.0d) {
                        d = 43.0d;
                    }
                    List<Number> A01 = AnonymousClass3GE.A01(file, (int) d);
                    ArrayList A0y = C12980iv.A0y(A01);
                    for (Number number : A01) {
                        A0y.add(Float.valueOf(((float) number.byteValue()) / 100.0f));
                    }
                    r5.A02.A0B.A0H(new RunnableBRunnable0Shape3S0300000_I1(r5, A0y, r4, 33));
                }
            });
            return;
        }
        this.A00 = false;
    }
}
