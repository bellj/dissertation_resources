package X;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.3qF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC79223qF extends AnonymousClass4UO {
    public static final Logger A01 = C72463ee.A0M(AbstractC79223qF.class);
    public static final boolean A02 = C95624e5.A06;
    public C108684zU A00;

    public static int A01(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        int i = 2;
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    public static int A00(int i) {
        return C72453ed.A07(i);
    }

    public static AnonymousClass497 A02(long j, long j2) {
        return new AnonymousClass497(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(j), Long.valueOf(j2), 1));
    }

    public static AnonymousClass497 A03(C79283qL r3, Throwable th) {
        return new AnonymousClass497(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(r3.A00), Integer.valueOf(r3.A01), 1), th);
    }

    public void A04(byte b) {
        if (this instanceof C79293qM) {
            C79293qM r6 = (C79293qM) this;
            long j = r6.A00;
            long j2 = r6.A02;
            if (j < j2) {
                r6.A00 = 1 + j;
                C95624e5.A02.A07(j, b);
                return;
            }
            throw A02(j, j2);
        } else if (!(this instanceof C79273qK)) {
            C79283qL r3 = (C79283qL) this;
            try {
                byte[] bArr = r3.A03;
                int i = r3.A00;
                r3.A00 = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw A03(r3, e);
            }
        } else {
            try {
                ((C79273qK) this).A01.put(b);
            } catch (BufferOverflowException e2) {
                throw new AnonymousClass497(e2);
            }
        }
    }

    public void A05(int i) {
        long j;
        if (this instanceof C79293qM) {
            C79293qM r6 = (C79293qM) this;
            if (r6.A00 <= r6.A03) {
                while (true) {
                    int i2 = i & -128;
                    j = r6.A00;
                    if (i2 == 0) {
                        break;
                    }
                    r6.A00 = j + 1;
                    C95624e5.A02.A07(j, (byte) ((i & 127) | 128));
                    i >>>= 7;
                }
            } else {
                while (true) {
                    j = r6.A00;
                    long j2 = r6.A02;
                    if (j < j2) {
                        if ((i & -128) == 0) {
                            break;
                        }
                        r6.A00 = j + 1;
                        C95624e5.A02.A07(j, (byte) ((i & 127) | 128));
                        i >>>= 7;
                    } else {
                        throw A02(j, j2);
                    }
                }
            }
            r6.A00 = 1 + j;
            C95624e5.A02.A07(j, (byte) i);
        } else if (!(this instanceof C79273qK)) {
            C79283qL r3 = (C79283qL) this;
            if (!A02 || r3.A01 - r3.A00 < 10) {
                while ((i & -128) != 0) {
                    try {
                        byte[] bArr = r3.A03;
                        int i3 = r3.A00;
                        r3.A00 = i3 + 1;
                        bArr[i3] = (byte) ((i & 127) | 128);
                        i >>>= 7;
                    } catch (IndexOutOfBoundsException e) {
                        throw A03(r3, e);
                    }
                }
                byte[] bArr2 = r3.A03;
                int i4 = r3.A00;
                r3.A00 = i4 + 1;
                bArr2[i4] = (byte) i;
                return;
            }
            while (true) {
                int i5 = i & -128;
                byte[] bArr3 = r3.A03;
                int i6 = r3.A00;
                r3.A00 = i6 + 1;
                long j3 = (long) i6;
                if (i5 == 0) {
                    C95624e5.A08(bArr3, (byte) i, j3);
                    return;
                } else {
                    C95624e5.A08(bArr3, (byte) ((i & 127) | 128), j3);
                    i >>>= 7;
                }
            }
        } else {
            C79273qK r2 = (C79273qK) this;
            while ((i & -128) != 0) {
                try {
                    r2.A01.put((byte) ((i & 127) | 128));
                    i >>>= 7;
                } catch (BufferOverflowException e2) {
                    throw new AnonymousClass497(e2);
                }
            }
            r2.A01.put((byte) i);
        }
    }

    public void A06(int i) {
        if (this instanceof C79293qM) {
            C79293qM r5 = (C79293qM) this;
            r5.A05.putInt((int) (r5.A00 - r5.A01), i);
            r5.A00 += 4;
        } else if (!(this instanceof C79273qK)) {
            C79283qL r4 = (C79283qL) this;
            try {
                byte[] bArr = r4.A03;
                int i2 = r4.A00;
                int i3 = i2 + 1;
                r4.A00 = i3;
                bArr[i2] = (byte) i;
                int i4 = i3 + 1;
                r4.A00 = i4;
                bArr[i3] = (byte) (i >> 8);
                int i5 = i4 + 1;
                r4.A00 = i5;
                bArr[i4] = (byte) (i >> 16);
                r4.A00 = i5 + 1;
                bArr[i5] = i >> 24;
            } catch (IndexOutOfBoundsException e) {
                throw A03(r4, e);
            }
        } else {
            try {
                ((C79273qK) this).A01.putInt(i);
            } catch (BufferOverflowException e2) {
                throw new AnonymousClass497(e2);
            }
        }
    }

    public void A07(int i, String str) {
        long j;
        long j2;
        int A012;
        if (this instanceof C79293qM) {
            C79293qM r7 = (C79293qM) this;
            AnonymousClass4UO.A06(r7, i, 2);
            long j3 = r7.A00;
            try {
                int length = str.length();
                int A07 = C72453ed.A07(length * 3);
                int A072 = C72453ed.A07(length);
                if (A072 == A07) {
                    int i2 = ((int) (j3 - r7.A01)) + A072;
                    ByteBuffer byteBuffer = r7.A05;
                    byteBuffer.position(i2);
                    C95164dF.A01(str, byteBuffer);
                    int position = byteBuffer.position() - i2;
                    r7.A05(position);
                    j = r7.A00;
                    j2 = (long) position;
                } else {
                    int A00 = C95164dF.A00(str);
                    r7.A05(A00);
                    long j4 = r7.A00;
                    ByteBuffer byteBuffer2 = r7.A05;
                    byteBuffer2.position((int) (j4 - r7.A01));
                    C95164dF.A01(str, byteBuffer2);
                    j = r7.A00;
                    j2 = (long) A00;
                }
                r7.A00 = j + j2;
            } catch (AnonymousClass4CM e) {
                r7.A00 = j3;
                r7.A05.position((int) (j3 - r7.A01));
                r7.A0C(e, str);
            } catch (IllegalArgumentException e2) {
                throw new AnonymousClass497(e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new AnonymousClass497(e3);
            }
        } else if (!(this instanceof C79273qK)) {
            C79283qL r4 = (C79283qL) this;
            AnonymousClass4UO.A06(r4, i, 2);
            int i3 = r4.A00;
            try {
                int length2 = str.length();
                int A073 = C72453ed.A07(length2 * 3);
                int A074 = C72453ed.A07(length2);
                if (A074 == A073) {
                    int i4 = i3 + A074;
                    r4.A00 = i4;
                    A012 = C95164dF.A00.A01(str, r4.A03, i4, r4.A01 - i4);
                    r4.A00 = i3;
                    r4.A05((A012 - i3) - A074);
                } else {
                    r4.A05(C95164dF.A00(str));
                    byte[] bArr = r4.A03;
                    int i5 = r4.A00;
                    A012 = C95164dF.A00.A01(str, bArr, i5, r4.A01 - i5);
                }
                r4.A00 = A012;
            } catch (AnonymousClass4CM e4) {
                r4.A00 = i3;
                r4.A0C(e4, str);
            } catch (IndexOutOfBoundsException e5) {
                throw new AnonymousClass497(e5);
            }
        } else {
            C79273qK r3 = (C79273qK) this;
            AnonymousClass4UO.A06(r3, i, 2);
            ByteBuffer byteBuffer3 = r3.A01;
            int position2 = byteBuffer3.position();
            try {
                int length3 = str.length();
                int A075 = C72453ed.A07(length3 * 3);
                int A076 = C72453ed.A07(length3);
                if (A076 == A075) {
                    int position3 = byteBuffer3.position() + A076;
                    byteBuffer3.position(position3);
                    try {
                        C95164dF.A01(str, byteBuffer3);
                        int position4 = byteBuffer3.position();
                        byteBuffer3.position(position2);
                        r3.A05(position4 - position3);
                        byteBuffer3.position(position4);
                    } catch (IndexOutOfBoundsException e6) {
                        throw new AnonymousClass497(e6);
                    }
                } else {
                    r3.A05(C95164dF.A00(str));
                    try {
                        C95164dF.A01(str, byteBuffer3);
                    } catch (IndexOutOfBoundsException e7) {
                        throw new AnonymousClass497(e7);
                    }
                }
            } catch (AnonymousClass4CM e8) {
                byteBuffer3.position(position2);
                r3.A0C(e8, str);
            } catch (IllegalArgumentException e9) {
                throw new AnonymousClass497(e9);
            }
        }
    }

    public void A08(long j) {
        long j2;
        if (this instanceof C79293qM) {
            C79293qM r6 = (C79293qM) this;
            if (r6.A00 <= r6.A03) {
                while (true) {
                    int i = ((j & -128) > 0 ? 1 : ((j & -128) == 0 ? 0 : -1));
                    j2 = r6.A00;
                    if (i == 0) {
                        break;
                    }
                    r6.A00 = j2 + 1;
                    C95624e5.A02.A07(j2, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
            } else {
                while (true) {
                    j2 = r6.A00;
                    long j3 = r6.A02;
                    if (j2 < j3) {
                        if ((j & -128) == 0) {
                            break;
                        }
                        r6.A00 = j2 + 1;
                        C95624e5.A02.A07(j2, (byte) ((((int) j) & 127) | 128));
                        j >>>= 7;
                    } else {
                        throw A02(j2, j3);
                    }
                }
            }
            r6.A00 = 1 + j2;
            C95624e5.A02.A07(j2, (byte) ((int) j));
        } else if (!(this instanceof C79273qK)) {
            C79283qL r3 = (C79283qL) this;
            if (!A02 || r3.A01 - r3.A00 < 10) {
                while ((j & -128) != 0) {
                    try {
                        byte[] bArr = r3.A03;
                        int i2 = r3.A00;
                        r3.A00 = i2 + 1;
                        bArr[i2] = (byte) ((((int) j) & 127) | 128);
                        j >>>= 7;
                    } catch (IndexOutOfBoundsException e) {
                        throw A03(r3, e);
                    }
                }
                byte[] bArr2 = r3.A03;
                int i3 = r3.A00;
                r3.A00 = i3 + 1;
                bArr2[i3] = (byte) ((int) j);
                return;
            }
            while (true) {
                int i4 = ((j & -128) > 0 ? 1 : ((j & -128) == 0 ? 0 : -1));
                byte[] bArr3 = r3.A03;
                int i5 = r3.A00;
                r3.A00 = i5 + 1;
                long j4 = (long) i5;
                int i6 = (int) j;
                if (i4 == 0) {
                    C95624e5.A08(bArr3, (byte) i6, j4);
                    return;
                } else {
                    C95624e5.A08(bArr3, (byte) ((i6 & 127) | 128), j4);
                    j >>>= 7;
                }
            }
        } else {
            C79273qK r5 = (C79273qK) this;
            while ((-128 & j) != 0) {
                try {
                    r5.A01.put((byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                } catch (BufferOverflowException e2) {
                    throw new AnonymousClass497(e2);
                }
            }
            r5.A01.put((byte) ((int) j));
        }
    }

    public void A09(long j) {
        if (this instanceof C79293qM) {
            C79293qM r5 = (C79293qM) this;
            r5.A05.putLong((int) (r5.A00 - r5.A01), j);
            r5.A00 += 8;
        } else if (!(this instanceof C79273qK)) {
            C79283qL r3 = (C79283qL) this;
            try {
                byte[] bArr = r3.A03;
                int i = r3.A00;
                int i2 = i + 1;
                r3.A00 = i2;
                bArr[i] = (byte) ((int) j);
                int i3 = i2 + 1;
                r3.A00 = i3;
                C72453ed.A15(j, bArr, 8, i2);
                int i4 = i3 + 1;
                r3.A00 = i4;
                C72453ed.A15(j, bArr, 16, i3);
                int i5 = i4 + 1;
                r3.A00 = i5;
                C72453ed.A15(j, bArr, 24, i4);
                int i6 = i5 + 1;
                r3.A00 = i6;
                C72453ed.A15(j, bArr, 32, i5);
                int i7 = i6 + 1;
                r3.A00 = i7;
                C72453ed.A15(j, bArr, 40, i6);
                int i8 = i7 + 1;
                r3.A00 = i8;
                C72453ed.A15(j, bArr, 48, i7);
                r3.A00 = i8 + 1;
                C72453ed.A15(j, bArr, 56, i8);
            } catch (IndexOutOfBoundsException e) {
                throw A03(r3, e);
            }
        } else {
            try {
                ((C79273qK) this).A01.putLong(j);
            } catch (BufferOverflowException e2) {
                throw new AnonymousClass497(e2);
            }
        }
    }

    public void A0A(AbstractC111915Bh r2, int i) {
        AnonymousClass4UO.A06(this, i, 2);
        A05(r2.A02());
        r2.A03(this);
    }

    public void A0B(AbstractC117125Yn r2, AnonymousClass5XT r3, int i) {
        AnonymousClass4UO.A06(this, i, 2);
        A05(AbstractC108624zO.A05(r3, r2));
        r3.Agw(this.A00, r2);
    }

    public final void A0C(AnonymousClass4CM r7, String str) {
        A01.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) r7);
        byte[] bytes = str.getBytes(C93094Zb.A03);
        try {
            int length = bytes.length;
            A05(length);
            A0D(bytes, 0, length);
        } catch (AnonymousClass497 e) {
            throw e;
        } catch (IndexOutOfBoundsException e2) {
            throw new AnonymousClass497(e2);
        }
    }

    public void A0D(byte[] bArr, int i, int i2) {
        if (this instanceof C79293qM) {
            C79293qM r2 = (C79293qM) this;
            if (bArr != null) {
                if (i >= 0 && i2 >= 0 && bArr.length - i2 >= i) {
                    long j = (long) i2;
                    long j2 = r2.A00;
                    if (r2.A02 - j >= j2) {
                        C95624e5.A02.A0E(bArr, (long) i, j2, j);
                        r2.A00 += j;
                        return;
                    }
                }
                Object[] objArr = new Object[3];
                objArr[0] = Long.valueOf(r2.A00);
                objArr[1] = Long.valueOf(r2.A02);
                C12960it.A1P(objArr, i2, 2);
                throw new AnonymousClass497(String.format("Pos: %d, limit: %d, len: %d", objArr));
            }
            throw C12980iv.A0n("value");
        } else if (!(this instanceof C79273qK)) {
            C79283qL r4 = (C79283qL) this;
            try {
                System.arraycopy(bArr, i, r4.A03, r4.A00, i2);
                r4.A00 += i2;
            } catch (IndexOutOfBoundsException e) {
                Object[] objArr2 = new Object[3];
                C12960it.A1P(objArr2, r4.A00, 0);
                C12960it.A1P(objArr2, r4.A01, 1);
                C12960it.A1P(objArr2, i2, 2);
                throw new AnonymousClass497(String.format("Pos: %d, limit: %d, len: %d", objArr2), e);
            }
        } else {
            try {
                ((C79273qK) this).A01.put(bArr, i, i2);
            } catch (IndexOutOfBoundsException e2) {
                throw new AnonymousClass497(e2);
            } catch (BufferOverflowException e3) {
                throw new AnonymousClass497(e3);
            }
        }
    }
}
