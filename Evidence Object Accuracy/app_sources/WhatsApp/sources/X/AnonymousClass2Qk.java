package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import java.util.ArrayList;

/* renamed from: X.2Qk  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Qk extends C50632Qh {
    public InsetDrawable A00;

    @Override // X.C50632Qh
    public void A06() {
    }

    @Override // X.C50632Qh
    public boolean A0H() {
        return false;
    }

    public AnonymousClass2Qk(AnonymousClass2QW r1, AbstractC50652Qj r2) {
        super(r1, r2);
    }

    @Override // X.C50632Qh
    public float A01() {
        return this.A0N.getElevation();
    }

    @Override // X.C50632Qh
    public GradientDrawable A03() {
        return new C73083fe();
    }

    @Override // X.C50632Qh
    public C50702Qp A04() {
        return new C80693sn();
    }

    @Override // X.C50632Qh
    public void A07() {
        A09();
    }

    @Override // X.C50632Qh
    public void A0A(float f, float f2, float f3) {
        int i = Build.VERSION.SDK_INT;
        if (i == 21) {
            this.A0N.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(C50632Qh.A0V, A0I(f, f3));
            stateListAnimator.addState(C50632Qh.A0U, A0I(f, f2));
            stateListAnimator.addState(C50632Qh.A0S, A0I(f, f2));
            stateListAnimator.addState(C50632Qh.A0T, A0I(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            AnonymousClass2QW r6 = this.A0N;
            arrayList.add(ObjectAnimator.ofFloat(r6, "elevation", f).setDuration(0L));
            if (i >= 22 && i <= 24) {
                arrayList.add(ObjectAnimator.ofFloat(r6, View.TRANSLATION_Z, r6.getTranslationZ()).setDuration(100L));
            }
            arrayList.add(ObjectAnimator.ofFloat(r6, View.TRANSLATION_Z, 0.0f).setDuration(100L));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(C50632Qh.A0P);
            stateListAnimator.addState(C50632Qh.A0R, animatorSet);
            stateListAnimator.addState(C50632Qh.A0Q, A0I(0.0f, 0.0f));
            r6.setStateListAnimator(stateListAnimator);
        }
        if (((C50642Qi) this.A0O).A00.A0B) {
            A09();
        }
    }

    @Override // X.C50632Qh
    public void A0B(ColorStateList colorStateList) {
        Drawable drawable = this.A09;
        if (drawable instanceof RippleDrawable) {
            ((RippleDrawable) drawable).setColor(AnonymousClass2RB.A02(colorStateList));
        } else {
            super.A0B(colorStateList);
        }
    }

    @Override // X.C50632Qh
    public void A0C(ColorStateList colorStateList, ColorStateList colorStateList2, PorterDuff.Mode mode, int i) {
        Drawable drawable;
        C73083fe r1 = new C73083fe();
        r1.setShape(1);
        r1.setColor(-1);
        Drawable A03 = C015607k.A03(r1);
        this.A0A = A03;
        C015607k.A04(colorStateList, A03);
        if (mode != null) {
            C015607k.A07(mode, this.A0A);
        }
        if (i > 0) {
            C50702Qp A05 = A05(colorStateList, i);
            this.A0G = A05;
            drawable = new LayerDrawable(new Drawable[]{A05, this.A0A});
        } else {
            this.A0G = null;
            drawable = this.A0A;
        }
        RippleDrawable rippleDrawable = new RippleDrawable(AnonymousClass2RB.A02(colorStateList2), drawable, null);
        this.A09 = rippleDrawable;
        this.A08 = rippleDrawable;
        AnonymousClass2Qk.super.setBackgroundDrawable(rippleDrawable);
    }

    @Override // X.C50632Qh
    public void A0E(Rect rect) {
        AnonymousClass2QV r2 = ((C50642Qi) this.A0O).A00;
        if (r2.A0B) {
            r2.getSizeDimension();
            float elevation = this.A0N.getElevation() + this.A03;
            int ceil = (int) Math.ceil((double) elevation);
            int ceil2 = (int) Math.ceil((double) (elevation * 1.5f));
            rect.set(ceil, ceil2, ceil, ceil2);
            return;
        }
        rect.set(0, 0, 0, 0);
    }

    @Override // X.C50632Qh
    public void A0F(Rect rect) {
        InsetDrawable insetDrawable;
        AnonymousClass2QV r1 = ((C50642Qi) this.A0O).A00;
        if (r1.A0B) {
            InsetDrawable insetDrawable2 = new InsetDrawable(this.A09, rect.left, rect.top, rect.right, rect.bottom);
            this.A00 = insetDrawable2;
            insetDrawable = insetDrawable2;
        } else {
            insetDrawable = this.A09;
        }
        AnonymousClass2Qk.super.setBackgroundDrawable(insetDrawable);
    }

    @Override // X.C50632Qh
    public void A0G(int[] iArr) {
        if (Build.VERSION.SDK_INT == 21) {
            AnonymousClass2QW r2 = this.A0N;
            float f = 0.0f;
            if (r2.isEnabled()) {
                r2.setElevation(super.A00);
                if (r2.isPressed()) {
                    f = this.A03;
                } else if (r2.isFocused() || r2.isHovered()) {
                    f = this.A01;
                }
            } else {
                r2.setElevation(0.0f);
            }
            r2.setTranslationZ(f);
        }
    }

    public final Animator A0I(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        AnonymousClass2QW r7 = this.A0N;
        animatorSet.play(ObjectAnimator.ofFloat(r7, "elevation", f).setDuration(0L)).with(ObjectAnimator.ofFloat(r7, View.TRANSLATION_Z, f2).setDuration(100L));
        animatorSet.setInterpolator(C50632Qh.A0P);
        return animatorSet;
    }
}
