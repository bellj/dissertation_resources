package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.4jY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98974jY implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        float f = 0.0f;
        LatLng latLng = null;
        float f2 = 0.0f;
        float f3 = 0.0f;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                latLng = (LatLng) C95664e9.A07(parcel, LatLng.CREATOR, readInt);
            } else if (c == 3) {
                f = C95664e9.A00(parcel, readInt);
            } else if (c == 4) {
                C95664e9.A0F(parcel, readInt, 4);
                f2 = parcel.readFloat();
            } else if (c != 5) {
                C95664e9.A0D(parcel, readInt);
            } else {
                f3 = C95664e9.A00(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new CameraPosition(latLng, f, f2, f3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new CameraPosition[i];
    }
}
