package X;

import java.util.List;

/* renamed from: X.1fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34121fc extends AbstractC28131Kt {
    public final AbstractC14640lm A00;
    public final C34081fY A01;
    public final List A02;

    public C34121fc(AbstractC14640lm r1, C34081fY r2, C34021fS r3, String str, List list) {
        super(r3, str);
        this.A00 = r1;
        this.A02 = list;
        this.A01 = r2;
    }
}
