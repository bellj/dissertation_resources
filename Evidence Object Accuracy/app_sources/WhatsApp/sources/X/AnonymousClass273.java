package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.273  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass273 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass273 A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AnonymousClass229 A01;
    public String A02 = "";

    static {
        AnonymousClass273 r0 = new AnonymousClass273();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        C467226y r1;
        switch (r8.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass273 r10 = (AnonymousClass273) obj2;
                this.A01 = (AnonymousClass229) r9.Aft(this.A01, r10.A01);
                int i = this.A00;
                boolean z = false;
                if ((i & 2) == 2) {
                    z = true;
                }
                String str = this.A02;
                int i2 = r10.A00;
                boolean z2 = false;
                if ((i2 & 2) == 2) {
                    z2 = true;
                }
                this.A02 = r9.Afy(str, r10.A02, z, z2);
                if (r9 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A032 = r92.A03();
                            if (A032 == 0) {
                                break;
                            } else if (A032 == 10) {
                                if ((this.A00 & 1) == 1) {
                                    r1 = (C467226y) this.A01.A0T();
                                } else {
                                    r1 = null;
                                }
                                AnonymousClass229 r0 = (AnonymousClass229) r92.A09(r102, AnonymousClass229.A0A.A0U());
                                this.A01 = r0;
                                if (r1 != null) {
                                    r1.A04(r0);
                                    this.A01 = (AnonymousClass229) r1.A01();
                                }
                                this.A00 |= 1;
                            } else if (A032 == 18) {
                                String A0A = r92.A0A();
                                this.A00 |= 2;
                                this.A02 = A0A;
                            } else if (!A0a(r92, A032)) {
                                break;
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass273();
            case 5:
                return new AnonymousClass274();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (AnonymousClass273.class) {
                        if (A04 == null) {
                            A04 = new AnonymousClass255(A03);
                        }
                    }
                }
                return A04;
            default:
                throw new UnsupportedOperationException();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass229 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass229 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
