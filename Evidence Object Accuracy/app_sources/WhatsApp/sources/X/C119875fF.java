package X;

import android.app.Activity;
import android.widget.ImageButton;
import com.whatsapp.WaEditText;
import com.whatsapp.emoji.search.EmojiSearchContainer;

/* renamed from: X.5fF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119875fF extends C15270mq {
    public final /* synthetic */ EmojiSearchContainer A00;
    public final /* synthetic */ C129745yE A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C119875fF(Activity activity, ImageButton imageButton, AbstractC15710nm r18, AbstractC49822Mw r19, WaEditText waEditText, AnonymousClass01d r21, C14820m6 r22, AnonymousClass018 r23, AnonymousClass19M r24, C231510o r25, EmojiSearchContainer emojiSearchContainer, AnonymousClass193 r27, C129745yE r28, C16630pM r29, C252718t r30) {
        super(activity, imageButton, r18, r19, waEditText, r21, r22, r23, r24, r25, r27, r29, r30);
        this.A01 = r28;
        this.A00 = emojiSearchContainer;
    }

    @Override // X.AbstractC15280mr, android.widget.PopupWindow
    public void dismiss() {
        super.dismiss();
        EmojiSearchContainer emojiSearchContainer = this.A00;
        if (emojiSearchContainer.getVisibility() == 0) {
            emojiSearchContainer.setVisibility(8);
        }
    }
}
