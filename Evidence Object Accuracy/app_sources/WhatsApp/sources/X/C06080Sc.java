package X;

/* renamed from: X.0Sc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06080Sc {
    public Object A00 = AnonymousClass4HY.A03;
    public final AbstractC11150fp A01;

    public C06080Sc(AbstractC11150fp r2) {
        this.A01 = r2;
    }

    public static final C114215Kq A00(AnonymousClass5WO r4) {
        int i;
        if (!(r4 instanceof C114205Kp)) {
            i = 1;
        } else {
            C114205Kp r2 = (C114205Kp) r4;
            while (true) {
                Object obj = r2._reusableCancellableContinuation;
                if (obj == null) {
                    r2._reusableCancellableContinuation = AnonymousClass4HG.A00;
                    break;
                } else if (obj instanceof C114215Kq) {
                    if (AnonymousClass0KN.A00(r2, obj, AnonymousClass4HG.A00, C114205Kp.A04)) {
                        C114215Kq r3 = (C114215Kq) obj;
                        if (r3 != null) {
                            Object obj2 = r3._state;
                            if (!(obj2 instanceof C92844Xq) || ((C92844Xq) obj2).A00 == null) {
                                r3._decision = 0;
                                r3._state = AnonymousClass5F4.A00;
                                return r3;
                            }
                            r3.A04();
                        }
                    }
                } else if (obj != AnonymousClass4HG.A00 && !(obj instanceof Throwable)) {
                    throw new IllegalStateException(C16700pc.A08("Inconsistent state ", obj));
                }
            }
            i = 2;
        }
        return new C114215Kq(r4, i);
    }

    public Object A01() {
        Object obj = this.A00;
        AnonymousClass4VA r0 = AnonymousClass4HY.A03;
        if (obj != r0) {
            this.A00 = r0;
            return obj;
        }
        throw new IllegalStateException("'hasNext' should be called prior to 'next' invocation");
    }

    public Object A02(AnonymousClass5WO r3) {
        Object obj = this.A00;
        AnonymousClass4VA r1 = AnonymousClass4HY.A03;
        if (obj == r1) {
            Object A04 = this.A01.A04();
            this.A00 = A04;
            if (A04 == r1) {
                return A03(r3);
            }
        }
        return true;
    }

    public final Object A03(AnonymousClass5WO r5) {
        C114215Kq A00 = A00(C93024Yr.A00(r5));
        AnonymousClass5L7 r2 = new AnonymousClass5L7(A00, this);
        while (true) {
            AbstractC11150fp r1 = this.A01;
            if (!(r1.A07(r2))) {
                Object A04 = r1.A04();
                this.A00 = A04;
                if (A04 != AnonymousClass4HY.A03) {
                    A00.A08(true);
                    break;
                }
            } else {
                r1.A05(A00, r2);
                break;
            }
        }
        return A00.A03();
    }
}
