package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.5vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128125vb {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C18650sn A03;
    public final C18610sj A04;
    public final String A05 = "CREATE_AUTH_TICKET_BASED_FACTOR";
    public final List A06;
    public final List A07;

    public C128125vb(Context context, C14900mE r3, C18640sm r4, C18650sn r5, C18610sj r6, List list, List list2) {
        this.A00 = context;
        this.A01 = r3;
        this.A02 = r4;
        this.A04 = r6;
        this.A03 = r5;
        this.A06 = list;
        this.A07 = list2;
    }
}
