package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.1cu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32891cu {
    public static byte[] A00(byte[] bArr, byte[] bArr2, int i) {
        return A01(bArr, new byte[32], bArr2, i);
    }

    public static byte[] A01(byte[] bArr, byte[] bArr2, byte[] bArr3, int i) {
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            instance.init(new SecretKeySpec(bArr2, DefaultCrypto.HMAC_SHA256));
            byte[] doFinal = instance.doFinal(bArr);
            try {
                int ceil = (int) Math.ceil(((double) i) / 32.0d);
                byte[] bArr4 = new byte[0];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                for (int i2 = 1; i2 < ceil + 1; i2++) {
                    Mac instance2 = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
                    instance2.init(new SecretKeySpec(doFinal, DefaultCrypto.HMAC_SHA256));
                    instance2.update(bArr4);
                    if (bArr3 != null) {
                        instance2.update(bArr3);
                    }
                    instance2.update((byte) i2);
                    bArr4 = instance2.doFinal();
                    int min = Math.min(i, bArr4.length);
                    byteArrayOutputStream.write(bArr4, 0, min);
                    i -= min;
                }
                return byteArrayOutputStream.toByteArray();
            } catch (InvalidKeyException | NoSuchAlgorithmException e) {
                throw new AssertionError(e);
            }
        } catch (InvalidKeyException | NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }
}
