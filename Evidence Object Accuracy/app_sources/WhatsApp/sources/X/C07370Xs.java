package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0Xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07370Xs implements AbstractC11730gl {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ViewGroup A01;
    public final /* synthetic */ AnonymousClass0EA A02;
    public final /* synthetic */ AnonymousClass0EJ A03;

    public C07370Xs(View view, ViewGroup viewGroup, AnonymousClass0EA r3, AnonymousClass0EJ r4) {
        this.A03 = r4;
        this.A00 = view;
        this.A01 = viewGroup;
        this.A02 = r3;
    }

    @Override // X.AbstractC11730gl
    public void ANe() {
        View view = this.A00;
        view.clearAnimation();
        this.A01.endViewTransition(view);
        this.A02.A00();
    }
}
