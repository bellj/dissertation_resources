package X;

/* renamed from: X.3ro  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80163ro extends AbstractC80173rp implements AbstractC115675Sm {
    public static final C80163ro zzl;
    public static volatile AbstractC115275Qw zzm;
    public int zzc;
    public String zzd = "";
    public String zze = "";
    public AnonymousClass5Z6 zzf;
    public int zzg;
    public String zzh;
    public long zzi;
    public long zzj;
    public AnonymousClass5Z6 zzk;

    static {
        C80163ro r1 = new C80163ro();
        zzl = r1;
        AnonymousClass50T.A08(C80163ro.class, r1);
    }

    public C80163ro() {
        C80243rw r0 = C80243rw.A02;
        this.zzf = r0;
        this.zzh = "";
        this.zzk = r0;
    }
}
