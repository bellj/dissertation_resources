package X;

import java.util.List;

/* renamed from: X.257  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass257 extends RuntimeException {
    public static final long serialVersionUID = -7466929953374883507L;
    public final List missingFields = null;

    public AnonymousClass257() {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
