package X;

import android.graphics.Bitmap;
import com.whatsapp.mediacomposer.MediaComposerActivity;

/* renamed from: X.3hf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74253hf extends C006202y {
    public final /* synthetic */ MediaComposerActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74253hf(MediaComposerActivity mediaComposerActivity, int i) {
        super(i);
        this.A00 = mediaComposerActivity;
    }

    @Override // X.C006202y
    public int A02(Object obj, Object obj2) {
        return ((Bitmap) obj2).getByteCount() >> 10;
    }
}
