package X;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.WaEditText;

/* renamed from: X.3Oj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66643Oj implements AnonymousClass02Q {
    public final /* synthetic */ Conversation A00;

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r3) {
        return false;
    }

    @Override // X.AnonymousClass02Q
    public boolean AU7(Menu menu, AbstractC009504t r3) {
        return false;
    }

    public C66643Oj(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r7) {
        Conversation conversation = this.A00;
        View inflate = LayoutInflater.from(conversation.A1U().A02()).inflate(R.layout.conversation_search_view, (ViewGroup) null, false);
        r7.A09(inflate);
        WaEditText waEditText = (WaEditText) inflate.findViewById(R.id.search_src_text);
        conversation.A1A = waEditText;
        if (waEditText == null) {
            return false;
        }
        waEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: X.4mk
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z) {
                C66643Oj r0 = C66643Oj.this;
                if (z) {
                    r0.A00.A3N();
                }
            }
        });
        conversation.A1A.addTextChangedListener(conversation.A4f);
        conversation.A1A.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: X.4pO
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                C66643Oj r2 = C66643Oj.this;
                if (i != 3 && (keyEvent == null || keyEvent.getKeyCode() != 66 || keyEvent.getAction() != 0)) {
                    return false;
                }
                Conversation.A0D(r2.A00, true);
                return true;
            }
        });
        View A0D = AnonymousClass028.A0D(inflate, R.id.search_up);
        conversation.A0L = A0D;
        C12960it.A10(A0D, this, 3);
        View A0D2 = AnonymousClass028.A0D(inflate, R.id.search_down);
        conversation.A0J = A0D2;
        C12960it.A10(A0D2, this, 4);
        conversation.A0M = AnonymousClass028.A0D(inflate, R.id.search_up_progress_bar);
        conversation.A0K = AnonymousClass028.A0D(inflate, R.id.search_down_progress_bar);
        conversation.A1A.setText(conversation.A20.A09);
        conversation.A1A.selectAll();
        conversation.A1A.requestFocus();
        conversation.A1A.setSelected(true);
        return true;
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        r4.A09(null);
        Conversation conversation = this.A00;
        conversation.A0g = null;
        C15360n1 r0 = conversation.A20;
        r0.A08 = null;
        r0.A0A = null;
        conversation.A2V = null;
        if (conversation.A1y.A05()) {
            conversation.A2w.A03();
        } else {
            conversation.A09.setVisibility(0);
            conversation.A2w.requestFocus();
        }
        conversation.A1g.getConversationCursorAdapter().A01++;
        conversation.A1g.A02();
    }
}
