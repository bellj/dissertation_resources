package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.5gv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120895gv extends AbstractC451020e {
    public final C64513Fv A00;
    public final String A01;

    public C120895gv(Context context, C14900mE r2, C18650sn r3, C64513Fv r4, String str) {
        super(context, r2, r3);
        this.A01 = str;
        this.A00 = r4;
    }

    public static AnonymousClass2SO A01(AnonymousClass1V8 r5) {
        return new AnonymousClass2SO(new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, r5.A0I("alias_value", null), "upiAlias"), r5.A0H("alias_type"), r5.A0H("alias_id"), r5.A0H("alias_status").toLowerCase(Locale.US));
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r4) {
        StringBuilder A0k = C12960it.A0k("PAY: onRequestError action: ");
        String str = this.A01;
        A0k.append(str);
        Log.i(C12960it.A0Z(r4, " error: ", A0k));
        C64513Fv r1 = this.A00;
        if (r1 != null) {
            r1.A06(str, r4.A00);
        }
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r6) {
        StringBuilder A0k = C12960it.A0k("PAY: onResponseError action: ");
        String str = this.A01;
        A0k.append(str);
        Log.i(C12960it.A0Z(r6, " error: ", A0k));
        C64513Fv r3 = this.A00;
        if (r3 != null) {
            r3.A06(str, r6.A00);
            int i = r6.A00;
            if (i == 403 || i == 405 || i == 406 || i == 426 || i == 460 || i == 410 || i == 409 || i == 2826008) {
                synchronized (r3) {
                    r3.A01 = i;
                    CopyOnWriteArrayList copyOnWriteArrayList = r3.A07;
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("payability-");
                    copyOnWriteArrayList.add(C12960it.A0f(A0h, i));
                }
            } else if (i == 440) {
                synchronized (r3) {
                    r3.A02 = i;
                    CopyOnWriteArrayList copyOnWriteArrayList2 = r3.A07;
                    StringBuilder A0h2 = C12960it.A0h();
                    A0h2.append("tos-");
                    copyOnWriteArrayList2.add(C12960it.A0f(A0h2, i));
                }
            }
        }
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r3) {
        StringBuilder A0k = C12960it.A0k("PAY: onResponseSuccess for op: action: ");
        String str = this.A01;
        Log.i(C12960it.A0d(str, A0k));
        C64513Fv r0 = this.A00;
        if (r0 != null) {
            r0.A05(str);
        }
    }
}
