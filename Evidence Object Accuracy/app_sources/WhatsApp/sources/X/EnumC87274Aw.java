package X;

import com.whatsapp.R;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87274Aw extends Enum {
    public static final /* synthetic */ EnumC87274Aw[] A00;
    public static final EnumC87274Aw A01;
    public static final EnumC87274Aw A02;
    public final int drawableRes;

    public static EnumC87274Aw valueOf(String str) {
        return (EnumC87274Aw) Enum.valueOf(EnumC87274Aw.class, str);
    }

    public static EnumC87274Aw[] values() {
        return (EnumC87274Aw[]) A00.clone();
    }

    static {
        EnumC87274Aw r4 = new EnumC87274Aw(0, "VIDEO", R.drawable.ic_action_videocall);
        A01 = r4;
        EnumC87274Aw r1 = new EnumC87274Aw(1, "VOICE", R.drawable.ic_action_call);
        A02 = r1;
        EnumC87274Aw[] r0 = new EnumC87274Aw[2];
        C72453ed.A1J(r4, r1, r0);
        A00 = r0;
    }

    public EnumC87274Aw(int i, String str, int i2) {
        this.drawableRes = i2;
    }
}
