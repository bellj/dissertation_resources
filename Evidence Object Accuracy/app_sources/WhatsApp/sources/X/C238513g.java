package X;

import android.database.Cursor;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.13g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238513g {
    public final C14830m7 A00;
    public final C16510p9 A01;
    public final C18460sU A02;
    public final C16490p7 A03;

    public C238513g(C14830m7 r1, C16510p9 r2, C18460sU r3, C16490p7 r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
    }

    public List A00(AnonymousClass1IS r18) {
        ArrayList arrayList = new ArrayList();
        C16510p9 r1 = this.A01;
        AbstractC14640lm r0 = r18.A00;
        AnonymousClass009.A05(r0);
        String[] strArr = {String.valueOf(r1.A02(r0)), String.valueOf(r18.A02 ? 1 : 0), r18.A01};
        C16310on A01 = this.A03.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT receipt_device_jid_row_id, receipt_recipient_jid_row_id, status, timestamp FROM receipt_orphaned WHERE chat_row_id = ? AND from_me = ? AND key_id = ? ORDER BY _id ASC", strArr);
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("receipt_device_jid_row_id");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("receipt_recipient_jid_row_id");
            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("status");
            int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("timestamp");
            while (A09.moveToNext()) {
                C18460sU r11 = this.A02;
                DeviceJid deviceJid = (DeviceJid) r11.A07(DeviceJid.class, A09.getLong(columnIndexOrThrow));
                if (deviceJid != null) {
                    arrayList.add(new C43061wL(deviceJid, (UserJid) r11.A07(UserJid.class, A09.getLong(columnIndexOrThrow2)), A09.getInt(columnIndexOrThrow3), A09.getLong(columnIndexOrThrow4)));
                }
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(AnonymousClass1IS r6) {
        C16510p9 r1 = this.A01;
        AbstractC14640lm r0 = r6.A00;
        AnonymousClass009.A05(r0);
        String[] strArr = {String.valueOf(r1.A02(r0)), String.valueOf(r6.A02 ? 1 : 0), r6.A01};
        C16310on A02 = this.A03.A02();
        try {
            A02.A03.A01("receipt_orphaned", "chat_row_id = ? AND from_me = ? AND key_id = ?", strArr);
            StringBuilder sb = new StringBuilder();
            sb.append("orphanedreceiptstore/deleteOrphanedReceipts/key:");
            sb.append(r6);
            Log.i(sb.toString());
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
