package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1rf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40481rf {
    public final C15570nT A00;
    public final Map A01 = new ConcurrentHashMap();
    public final Map A02 = new ConcurrentHashMap();
    public final TreeSet A03 = new TreeSet();

    public C40481rf(C15570nT r3, List list) {
        this.A00 = r3;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            A04((AnonymousClass1Iv) it.next());
        }
    }

    public static final String A00(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        AnonymousClass009.A05(str);
        if (!C38491oB.A02(str)) {
            return "□";
        }
        return C37471mS.A00(AnonymousClass3JU.A07(new C37471mS(new C37471mS(C38491oB.A04(new C37471mS(str).A00)).toString()).A00));
    }

    public synchronized int A01() {
        int i;
        i = 0;
        Iterator A03 = A03();
        while (A03.hasNext()) {
            C40491rg r1 = (C40491rg) A03.next();
            if (!TextUtils.isEmpty(r1.A02)) {
                i += r1.A04.size();
            }
        }
        return i;
    }

    public synchronized Collection A02() {
        return this.A02.values();
    }

    public synchronized Iterator A03() {
        return this.A03.descendingIterator();
    }

    public synchronized void A04(AnonymousClass1Iv r14) {
        C40491rg r0;
        TreeSet treeSet;
        if (!(r14 instanceof AnonymousClass1X9)) {
            AnonymousClass009.A07("Wrong message add on passed into MessageReactionsImpl");
        } else {
            C15570nT r4 = this.A00;
            r4.A08();
            UserJid userJid = r4.A05;
            if (userJid == null) {
                AnonymousClass009.A07("myUserJid is null. User logged out?");
            } else {
                AnonymousClass1X9 r142 = (AnonymousClass1X9) r14;
                this.A02.put(Long.valueOf(r142.A11), r142);
                if (!r142.A0z.A02) {
                    userJid = r142.A0C();
                    AnonymousClass009.A05(userJid);
                }
                AnonymousClass2VH r3 = new AnonymousClass2VH(r4, userJid, r142.A01, r142.A00, r142.A0I, ((AnonymousClass1Iv) r142).A00);
                String A00 = A00(r3.A05);
                if (!TextUtils.isEmpty(A00)) {
                    Map map = this.A01;
                    if (!map.containsKey(A00)) {
                        r0 = new C40491rg(r4, r3, A00);
                        map.put(A00, r0);
                        treeSet = this.A03;
                    } else {
                        Object obj = map.get(A00);
                        AnonymousClass009.A05(obj);
                        r0 = (C40491rg) obj;
                        treeSet = this.A03;
                        treeSet.remove(r0);
                        r0.A00(r3);
                    }
                    treeSet.add(r0);
                }
            }
        }
    }
}
