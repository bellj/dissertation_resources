package X;

import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.StickyHeadersRecyclerView;

/* renamed from: X.3S0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S0 implements AbstractC12560i7 {
    public final /* synthetic */ StickyHeadersRecyclerView A00;

    @Override // X.AbstractC12560i7
    public void AV2(boolean z) {
    }

    public AnonymousClass3S0(StickyHeadersRecyclerView stickyHeadersRecyclerView) {
        this.A00 = stickyHeadersRecyclerView;
    }

    @Override // X.AbstractC12560i7
    public boolean ARR(MotionEvent motionEvent, RecyclerView recyclerView) {
        StickyHeadersRecyclerView stickyHeadersRecyclerView = this.A00;
        AnonymousClass03U r0 = stickyHeadersRecyclerView.A08;
        if (r0 == null || r0.A0H == null) {
            return false;
        }
        float y = motionEvent.getY();
        int height = stickyHeadersRecyclerView.A08.A0H.getHeight();
        int i = stickyHeadersRecyclerView.A04;
        if (i >= 0) {
            i = 0;
        }
        if (y > ((float) (height + i))) {
            return false;
        }
        stickyHeadersRecyclerView.A07.A00.AXZ(motionEvent);
        return true;
    }

    @Override // X.AbstractC12560i7
    public void AXa(MotionEvent motionEvent, RecyclerView recyclerView) {
        this.A00.A07.A00.AXZ(motionEvent);
    }
}
