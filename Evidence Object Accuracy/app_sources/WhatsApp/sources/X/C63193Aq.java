package X;

import android.animation.ValueAnimator;
import android.view.View;

/* renamed from: X.3Aq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63193Aq {
    public static ValueAnimator A00(View view, Runnable runnable, int i) {
        int height = view.getHeight();
        ValueAnimator ofInt = ValueAnimator.ofInt(C12970iu.A1a(height, i));
        C12980iv.A11(ofInt, view, 8);
        ofInt.setInterpolator(new C015907n());
        ofInt.setDuration((long) (C12980iv.A05(height, i) >> 3));
        C12990iw.A0w(ofInt, runnable, 13);
        return ofInt;
    }
}
