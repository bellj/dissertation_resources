package X;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.69D  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69D implements AnonymousClass18M {
    public final C16590pI A00;
    public final AnonymousClass018 A01;
    public final C14850m9 A02;
    public final C18660so A03;
    public final AnonymousClass61M A04;
    public final C22710zW A05;

    @Override // X.AnonymousClass18M
    public String ABv(int i) {
        return null;
    }

    @Override // X.AnonymousClass18M
    public /* synthetic */ String ACl(int i, String str) {
        return str;
    }

    @Override // X.AnonymousClass18M
    public int ACm(C64513Fv r2, int i) {
        return 0;
    }

    @Override // X.AnonymousClass18M
    public void AI2(String str) {
    }

    @Override // X.AnonymousClass18M
    public boolean AJI(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJZ(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJa(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJe(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AJk(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public boolean AKB(int i) {
        return false;
    }

    @Override // X.AnonymousClass18M
    public int ALP() {
        return 0;
    }

    @Override // X.AnonymousClass18M
    public int ALQ() {
        return 0;
    }

    public AnonymousClass69D(C16590pI r1, AnonymousClass018 r2, C14850m9 r3, C18660so r4, AnonymousClass61M r5, C22710zW r6) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A05 = r6;
        this.A04 = r5;
        this.A03 = r4;
    }

    public AnonymousClass04S A00(Context context, DialogInterface.OnDismissListener onDismissListener, DialogInterface.OnDismissListener onDismissListener2, DialogInterface.OnDismissListener onDismissListener3, String str, int i) {
        if (i == 2896002) {
            C004802e A0S = C12980iv.A0S(context);
            A0S.A06(R.string.brazil_p2p_disabled_upsell_merchant_error);
            C12970iu.A1I(A0S);
            A0S.setNegativeButton(R.string.learn_more, new AnonymousClass62O(context, this));
            return A0S.create();
        } else if (i == 10780) {
            return AnonymousClass61M.A02(context, onDismissListener2, context.getString(R.string.error_payment_provider_down));
        } else {
            switch (i) {
                case 2826028:
                case 2826029:
                    return AnonymousClass61M.A02(context, onDismissListener3, C12960it.A0X(context, str, C12970iu.A1b(), 0, R.string.br_payments_receiver_generic_error));
                default:
                    return this.A04.A04(context, onDismissListener, onDismissListener2, onDismissListener3, str, null, i);
            }
        }
    }

    public AnonymousClass04S A01(Context context, C14850m9 r6, C129925yW r7, int i, int i2) {
        String str;
        AnonymousClass018 r1;
        int i3;
        AnonymousClass04S A01 = AnonymousClass61M.A01(context, null, null, null, i);
        if (A01 != null) {
            return A01;
        }
        if (r6.A07(698)) {
            str = r7.A02(String.valueOf(i));
        } else {
            str = "";
        }
        if (TextUtils.isEmpty(str)) {
            if (i != -233) {
                int i4 = R.string.dyi_request_report_incomplete_setup_failure_dialog_message;
                if (i != 477) {
                    if (i == 10229) {
                        r1 = this.A01;
                        i3 = R.string.brazil_card_token_expired_error;
                    } else if (i != 10234) {
                        i4 = R.string.error_payment_provider_down;
                        if (i != 10780) {
                            if (i != 2896002) {
                                str = this.A01.A09(i2);
                            } else {
                                C004802e A0S = C12980iv.A0S(context);
                                A0S.A06(R.string.brazil_p2p_disabled_upsell_merchant_error);
                                A0S.setPositiveButton(R.string.ok, null);
                                A0S.setNegativeButton(R.string.learn_more, new AnonymousClass62O(context, this));
                                return A0S.create();
                            }
                        }
                    } else {
                        r1 = this.A01;
                        i3 = R.string.payment_verify_card_error;
                    }
                }
                str = context.getString(i4);
            } else {
                r1 = this.A01;
                i3 = R.string.payment_card_cannot_verified_error;
            }
            str = r1.A09(i3);
        }
        return AnonymousClass61M.A02(context, new DialogInterface.OnDismissListener() { // from class: X.62r
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
            }
        }, str);
    }

    @Override // X.AnonymousClass18M
    public String ABs(int i) {
        if (!C12960it.A1V(i, 2826013) || !this.A05.A03.A07(1587)) {
            return null;
        }
        return this.A00.A00.getString(R.string.transaction_action_text_incoming_payment_limit_reached_consumer_br);
    }

    @Override // X.AnonymousClass18M
    public int ABt(AnonymousClass1AA r3, C30141Wg r4, int i) {
        if (!C12960it.A1V(i, 2826013) || !this.A05.A03.A07(1587)) {
            return -1;
        }
        return 20;
    }

    @Override // X.AnonymousClass18M
    public String ABu(int i) {
        if (!C12960it.A1V(i, 2826013) || !this.A05.A03.A07(1587)) {
            return null;
        }
        this.A02.A02(1590);
        return this.A00.A00.getString(R.string.transaction_tertiary_text_incoming_payment_limit_reached_br);
    }

    @Override // X.AnonymousClass18M
    public boolean AJY(int i) {
        return C12960it.A1V(i, 2001);
    }

    @Override // X.AnonymousClass18M
    public boolean AJb(int i) {
        return C12960it.A1V(i, 10244);
    }

    @Override // X.AnonymousClass18M
    public boolean AJc(int i) {
        return C12960it.A1V(i, 10242);
    }

    @Override // X.AnonymousClass18M
    public boolean AJd(int i) {
        return C12960it.A1V(i, 10241);
    }

    @Override // X.AnonymousClass18M
    public boolean AJf(int i) {
        return C12960it.A1V(i, 10240);
    }

    @Override // X.AnonymousClass18M
    public boolean AJw(int i) {
        return C12960it.A1V(i, 2826013);
    }

    @Override // X.AnonymousClass18M
    public boolean AdY(int i) {
        return true;
    }
}
