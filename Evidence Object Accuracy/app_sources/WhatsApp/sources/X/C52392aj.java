package X;

import android.content.Context;
import android.graphics.Matrix;
import android.view.MotionEvent;
import android.view.TextureView;

/* renamed from: X.2aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52392aj extends TextureView implements AnonymousClass004 {
    public int A00;
    public AnonymousClass2P7 A01;
    public boolean A02;
    public boolean A03;

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        return false;
    }

    public C52392aj(Context context) {
        super(context);
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A01;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A01 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.A03) {
            setTransform(null);
        } else {
            int i5 = this.A00;
            if (i5 == 90 || i5 == 270) {
                int measuredWidth = getMeasuredWidth();
                int measuredHeight = getMeasuredHeight();
                Matrix A01 = C13000ix.A01();
                float f = (float) i5;
                float f2 = (float) measuredWidth;
                float f3 = f2 / 2.0f;
                float f4 = (float) measuredHeight;
                float f5 = f4 / 2.0f;
                A01.postRotate(f, f3, f5);
                A01.postScale(f2 / f4, f4 / f2, f3, f5);
                setTransform(A01);
            }
        }
        this.A03 = false;
        super.onLayout(z, i, i2, i3, i4);
    }

    public void setRotationAngle(int i) {
        if (this.A00 != i) {
            this.A00 = i;
            if (!(i == 90 || i == 270)) {
                this.A00 = 0;
                this.A03 = true;
            }
            requestLayout();
        }
    }
}
