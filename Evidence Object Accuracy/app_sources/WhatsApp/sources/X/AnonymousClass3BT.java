package X;

/* renamed from: X.3BT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BT {
    public final C14900mE A00;
    public final AnonymousClass4O9 A01;
    public final AnonymousClass4OA A02;
    public final AnonymousClass3ED A03;
    public final AbstractC14440lR A04;

    public AnonymousClass3BT(C14900mE r1, AnonymousClass4O9 r2, AnonymousClass4OA r3, AnonymousClass3ED r4, AbstractC14440lR r5) {
        C16700pc.A0G(r1, r5);
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }
}
