package X;

import java.security.MessageDigest;

/* renamed from: X.5HY  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HY extends MessageDigest {
    public int A00;
    public AnonymousClass5XI A01;

    public AnonymousClass5HY(AnonymousClass5XI r2) {
        super(r2.AAf());
        this.A01 = r2;
        this.A00 = r2.ACZ();
    }

    @Override // java.security.MessageDigestSpi
    public byte[] engineDigest() {
        byte[] bArr = new byte[this.A00];
        this.A01.A97(bArr, 0);
        return bArr;
    }

    @Override // java.security.MessageDigestSpi
    public int engineGetDigestLength() {
        return this.A00;
    }

    @Override // java.security.MessageDigestSpi
    public void engineReset() {
        this.A01.reset();
    }

    @Override // java.security.MessageDigestSpi
    public void engineUpdate(byte b) {
        this.A01.AfG(b);
    }

    @Override // java.security.MessageDigestSpi
    public void engineUpdate(byte[] bArr, int i, int i2) {
        this.A01.update(bArr, i, i2);
    }
}
