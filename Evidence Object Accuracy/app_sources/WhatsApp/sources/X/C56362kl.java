package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.util.Base64;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.auth.api.signin.internal.SignInHubActivity;
import com.google.android.gms.common.api.Scope;
import java.util.Arrays;
import java.util.Set;

/* renamed from: X.2kl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56362kl extends AbstractC77963o9 {
    public final GoogleSignInOptions A00;

    @Override // X.AbstractC95064d1
    public final String A05() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }

    @Override // X.AbstractC95064d1
    public final String A06() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 12451000;
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final boolean AZd() {
        return true;
    }

    public C56362kl(Context context, Looper looper, GoogleSignInOptions googleSignInOptions, AbstractC14980mM r16, AbstractC15000mO r17, AnonymousClass3BW r18) {
        super(context, looper, r16, r17, r18, 91);
        AnonymousClass3H6 r4;
        if (googleSignInOptions != null) {
            r4 = new AnonymousClass3H6(googleSignInOptions);
        } else {
            r4 = new AnonymousClass3H6();
        }
        byte[] bArr = new byte[16];
        C88334Fg.A00.nextBytes(bArr);
        r4.A03 = Base64.encodeToString(bArr, 11);
        Set set = r18.A06;
        if (!set.isEmpty()) {
            for (Object obj : set) {
                Set set2 = r4.A05;
                set2.add(obj);
                set2.addAll(Arrays.asList(new Scope[0]));
            }
        }
        this.A00 = r4.A00();
    }

    @Override // X.AbstractC95064d1
    public final /* bridge */ /* synthetic */ IInterface A04(IBinder iBinder) {
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        return !(queryLocalInterface instanceof C78853pe) ? new C78853pe(iBinder) : queryLocalInterface;
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final Intent AGl() {
        Context context = this.A0F;
        GoogleSignInOptions googleSignInOptions = this.A00;
        AnonymousClass3GP.A00.A00("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(googleSignInOptions, context.getPackageName());
        Intent A0E = C12990iw.A0E("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        A0E.setPackage(context.getPackageName());
        A0E.setClass(context, SignInHubActivity.class);
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("config", signInConfiguration);
        A0E.putExtra("config", A0D);
        return A0E;
    }
}
