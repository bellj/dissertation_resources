package X;

/* renamed from: X.4Ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87074Ac {
    INVALID_COUNTRY_CODE,
    NOT_A_NUMBER,
    TOO_SHORT_AFTER_IDD,
    TOO_SHORT_NSN,
    TOO_LONG
}
