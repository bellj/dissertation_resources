package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.08j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C017908j extends C009604u {
    public int A00 = 0;

    public C017908j() {
        super(-2, -2);
        super.A00 = 8388627;
    }

    public C017908j(C009604u r2) {
        super(r2);
    }

    public C017908j(C017908j r2) {
        super((C009604u) r2);
        this.A00 = r2.A00;
    }

    public C017908j(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public C017908j(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public C017908j(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
        ((ViewGroup.MarginLayoutParams) this).leftMargin = marginLayoutParams.leftMargin;
        ((ViewGroup.MarginLayoutParams) this).topMargin = marginLayoutParams.topMargin;
        ((ViewGroup.MarginLayoutParams) this).rightMargin = marginLayoutParams.rightMargin;
        ((ViewGroup.MarginLayoutParams) this).bottomMargin = marginLayoutParams.bottomMargin;
    }
}
