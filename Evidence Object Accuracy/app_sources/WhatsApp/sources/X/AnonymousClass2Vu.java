package X;

/* renamed from: X.2Vu  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2Vu {
    public int A00;
    public Object A01;

    public AnonymousClass2Vu(Object obj, int i) {
        this.A00 = i;
        this.A01 = obj;
    }

    public final int A00() {
        return this.A00;
    }
}
