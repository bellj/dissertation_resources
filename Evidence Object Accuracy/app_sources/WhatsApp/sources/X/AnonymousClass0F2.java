package X;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

/* renamed from: X.0F2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0F2 extends AnonymousClass0QE {
    public final /* synthetic */ RecyclerView A00;

    public AnonymousClass0F2(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    @Override // X.AnonymousClass0QE
    public void A00() {
        RecyclerView recyclerView = this.A00;
        recyclerView.A0q(null);
        recyclerView.A0y.A0C = true;
        recyclerView.A0r(true);
        if (recyclerView.A0J.A04.size() <= 0) {
            recyclerView.requestLayout();
        }
    }

    @Override // X.AnonymousClass0QE
    public void A02(int i, int i2) {
        RecyclerView recyclerView = this.A00;
        recyclerView.A0q(null);
        AnonymousClass0Z5 r3 = recyclerView.A0J;
        if (i2 >= 1) {
            ArrayList arrayList = r3.A04;
            arrayList.add(r3.ALk(null, 1, i, i2));
            r3.A00 |= 1;
            if (arrayList.size() == 1) {
                A06();
            }
        }
    }

    @Override // X.AnonymousClass0QE
    public void A03(int i, int i2) {
        RecyclerView recyclerView = this.A00;
        recyclerView.A0q(null);
        AnonymousClass0Z5 r4 = recyclerView.A0J;
        if (i2 >= 1) {
            ArrayList arrayList = r4.A04;
            arrayList.add(r4.ALk(null, 2, i, i2));
            r4.A00 |= 2;
            if (arrayList.size() == 1) {
                A06();
            }
        }
    }

    @Override // X.AnonymousClass0QE
    public void A04(int i, int i2, int i3) {
        RecyclerView recyclerView = this.A00;
        recyclerView.A0q(null);
        AnonymousClass0Z5 r4 = recyclerView.A0J;
        if (i != i2) {
            ArrayList arrayList = r4.A04;
            arrayList.add(r4.ALk(null, 8, i, i2));
            r4.A00 |= 8;
            if (arrayList.size() == 1) {
                A06();
            }
        }
    }

    @Override // X.AnonymousClass0QE
    public void A05(Object obj, int i, int i2) {
        RecyclerView recyclerView = this.A00;
        recyclerView.A0q(null);
        AnonymousClass0Z5 r4 = recyclerView.A0J;
        if (i2 >= 1) {
            ArrayList arrayList = r4.A04;
            arrayList.add(r4.ALk(obj, 4, i, i2));
            r4.A00 |= 4;
            if (arrayList.size() == 1) {
                A06();
            }
        }
    }

    public void A06() {
        RecyclerView recyclerView = this.A00;
        if (!recyclerView.A0h || !recyclerView.A0j) {
            recyclerView.A0c = true;
            recyclerView.requestLayout();
            return;
        }
        recyclerView.postOnAnimation(recyclerView.A12);
    }
}
