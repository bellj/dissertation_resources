package X;

/* renamed from: X.4AG  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4AG {
    /* Fake field, exist only in values array */
    VERBOSE,
    DEBUG,
    INFO,
    /* Fake field, exist only in values array */
    WARN,
    ERROR,
    /* Fake field, exist only in values array */
    ASSERT
}
