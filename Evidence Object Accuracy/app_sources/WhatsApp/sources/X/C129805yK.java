package X;

/* renamed from: X.5yK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129805yK {
    public boolean A00;
    public final C1308560f A01;

    public C129805yK(C1308560f r1) {
        this.A01 = r1;
    }

    public final void A00(String str) {
        A01(C12960it.A0d(str, C12960it.A0k("Can only check if prepared on the Optic thread. ")));
        if (!this.A00) {
            StringBuilder A0k = C12960it.A0k("Not prepared: ");
            A0k.append(str);
            A0k.append(" Current thread: ");
            throw C12960it.A0U(C12960it.A0d(Thread.currentThread().getName(), A0k));
        }
    }

    public final void A01(String str) {
        if (!this.A01.A09()) {
            StringBuilder A0j = C12960it.A0j(str);
            A0j.append(" Current thread: ");
            throw C12990iw.A0m(C12960it.A0d(Thread.currentThread().getName(), A0j));
        }
    }

    public final void A02(String str, boolean z) {
        A01(C12960it.A0d(str, C12960it.A0k("Can only set the prepared state on the Optic thread. ")));
        this.A00 = z;
    }
}
