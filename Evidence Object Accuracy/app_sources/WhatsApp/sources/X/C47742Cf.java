package X;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import android.view.View;

/* renamed from: X.2Cf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C47742Cf {
    public static final boolean A0j;
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05 = 15.0f;
    public float A06;
    public float A07;
    public float A08;
    public float A09;
    public float A0A;
    public float A0B;
    public float A0C;
    public float A0D;
    public float A0E;
    public float A0F = 15.0f;
    public float A0G;
    public float A0H;
    public float A0I;
    public int A0J;
    public int A0K = 16;
    public int A0L;
    public int A0M = 16;
    public TimeInterpolator A0N;
    public TimeInterpolator A0O;
    public ColorStateList A0P;
    public ColorStateList A0Q;
    public Bitmap A0R;
    public Paint A0S;
    public Typeface A0T;
    public Typeface A0U;
    public Typeface A0V;
    public CharSequence A0W;
    public CharSequence A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public int[] A0c;
    public final Rect A0d;
    public final Rect A0e;
    public final RectF A0f;
    public final TextPaint A0g;
    public final TextPaint A0h;
    public final View A0i;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT < 18) {
            z = true;
        }
        A0j = z;
    }

    public C47742Cf(View view) {
        this.A0i = view;
        TextPaint textPaint = new TextPaint(129);
        this.A0g = textPaint;
        this.A0h = new TextPaint(textPaint);
        this.A0d = new Rect();
        this.A0e = new Rect();
        this.A0f = new RectF();
    }

    public static int A00(float f, int i, int i2) {
        float f2 = 1.0f - f;
        return Color.argb((int) ((((float) Color.alpha(i)) * f2) + (((float) Color.alpha(i2)) * f)), (int) ((((float) Color.red(i)) * f2) + (((float) Color.red(i2)) * f)), (int) ((((float) Color.green(i)) * f2) + (((float) Color.green(i2)) * f)), (int) ((((float) Color.blue(i)) * f2) + (((float) Color.blue(i2)) * f)));
    }

    public final Typeface A01(int i) {
        TypedArray obtainStyledAttributes = this.A0i.getContext().obtainStyledAttributes(i, new int[]{16843692});
        try {
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                return Typeface.create(string, 0);
            }
            obtainStyledAttributes.recycle();
            return null;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        if (r1.height() <= 0) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r2 = this;
            android.graphics.Rect r1 = r2.A0d
            int r0 = r1.width()
            if (r0 <= 0) goto L_0x001d
            int r0 = r1.height()
            if (r0 <= 0) goto L_0x001d
            android.graphics.Rect r1 = r2.A0e
            int r0 = r1.width()
            if (r0 <= 0) goto L_0x001d
            int r1 = r1.height()
            r0 = 1
            if (r1 > 0) goto L_0x001e
        L_0x001d:
            r0 = 0
        L_0x001e:
            r2.A0Z = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47742Cf.A02():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00dd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 259
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47742Cf.A03():void");
    }

    public void A04(float f) {
        if (f < 0.0f) {
            f = 0.0f;
        } else if (f > 1.0f) {
            f = 1.0f;
        }
        if (f != this.A0B) {
            this.A0B = f;
            A05(f);
        }
    }

    public final void A05(float f) {
        int defaultColor;
        int defaultColor2;
        int defaultColor3;
        float f2 = f;
        RectF rectF = this.A0f;
        Rect rect = this.A0e;
        float f3 = (float) rect.left;
        Rect rect2 = this.A0d;
        float f4 = (float) rect2.left;
        TimeInterpolator timeInterpolator = this.A0N;
        float f5 = f;
        if (timeInterpolator != null) {
            f5 = timeInterpolator.getInterpolation(f);
        }
        rectF.left = f3 + (f5 * (f4 - f3));
        float f6 = this.A0A;
        float f7 = this.A01;
        TimeInterpolator timeInterpolator2 = this.A0N;
        float f8 = f;
        if (timeInterpolator2 != null) {
            f8 = timeInterpolator2.getInterpolation(f);
        }
        rectF.top = f6 + (f8 * (f7 - f6));
        float f9 = (float) rect.right;
        float f10 = (float) rect2.right;
        TimeInterpolator timeInterpolator3 = this.A0N;
        float f11 = f;
        if (timeInterpolator3 != null) {
            f11 = timeInterpolator3.getInterpolation(f);
        }
        rectF.right = f9 + (f11 * (f10 - f9));
        float f12 = (float) rect.bottom;
        float f13 = (float) rect2.bottom;
        TimeInterpolator timeInterpolator4 = this.A0N;
        if (timeInterpolator4 != null) {
            f2 = timeInterpolator4.getInterpolation(f);
        }
        rectF.bottom = f12 + (f2 * (f13 - f12));
        float f14 = this.A09;
        float f15 = this.A00;
        TimeInterpolator timeInterpolator5 = this.A0N;
        float f16 = f;
        if (timeInterpolator5 != null) {
            f16 = timeInterpolator5.getInterpolation(f);
        }
        this.A06 = f14 + (f16 * (f15 - f14));
        float f17 = this.A0A;
        float f18 = this.A01;
        TimeInterpolator timeInterpolator6 = this.A0N;
        float f19 = f;
        if (timeInterpolator6 != null) {
            f19 = timeInterpolator6.getInterpolation(f);
        }
        this.A07 = f17 + (f19 * (f18 - f17));
        float f20 = this.A0F;
        float f21 = this.A05;
        TimeInterpolator timeInterpolator7 = this.A0O;
        float f22 = f;
        if (timeInterpolator7 != null) {
            f22 = timeInterpolator7.getInterpolation(f);
        }
        A07(f20 + (f22 * (f21 - f20)));
        ColorStateList colorStateList = this.A0P;
        ColorStateList colorStateList2 = this.A0Q;
        TextPaint textPaint = this.A0g;
        int[] iArr = this.A0c;
        if (colorStateList != colorStateList2) {
            if (iArr != null) {
                defaultColor2 = colorStateList2.getColorForState(iArr, 0);
            } else {
                defaultColor2 = colorStateList2.getDefaultColor();
            }
            int[] iArr2 = this.A0c;
            if (iArr2 != null) {
                defaultColor3 = this.A0P.getColorForState(iArr2, 0);
            } else {
                defaultColor3 = this.A0P.getDefaultColor();
            }
            defaultColor = A00(f, defaultColor2, defaultColor3);
        } else if (iArr != null) {
            defaultColor = colorStateList.getColorForState(iArr, 0);
        } else {
            defaultColor = colorStateList.getDefaultColor();
        }
        textPaint.setColor(defaultColor);
        float f23 = this.A0E;
        float f24 = this.A0C;
        float f25 = this.A0D;
        textPaint.setShadowLayer(f23 + ((this.A04 - f23) * f), f24 + ((this.A02 - f24) * f), f25 + ((this.A03 - f25) * f), A00(f, this.A0L, this.A0J));
        this.A0i.postInvalidateOnAnimation();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        if (r9 != false) goto L_0x0047;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(float r11) {
        /*
            r10 = this;
            java.lang.CharSequence r0 = r10.A0W
            if (r0 == 0) goto L_0x008f
            android.graphics.Rect r0 = r10.A0d
            int r0 = r0.width()
            float r8 = (float) r0
            android.graphics.Rect r0 = r10.A0e
            int r0 = r0.width()
            float r7 = (float) r0
            float r6 = r10.A05
            float r0 = r11 - r6
            float r1 = java.lang.Math.abs(r0)
            r0 = 981668463(0x3a83126f, float:0.001)
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            r0 = 0
            if (r1 >= 0) goto L_0x0023
            r0 = 1
        L_0x0023:
            r5 = 1065353216(0x3f800000, float:1.0)
            r4 = 1
            r3 = 0
            if (r0 == 0) goto L_0x0095
            r10.A0G = r5
            android.graphics.Typeface r1 = r10.A0U
            android.graphics.Typeface r0 = r10.A0T
            if (r1 == r0) goto L_0x0093
            r10.A0U = r0
            r9 = 1
        L_0x0034:
            r0 = 0
            int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x004c
            float r0 = r10.A08
            int r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x0047
            boolean r0 = r10.A0Y
            if (r0 != 0) goto L_0x0047
            r0 = r9
            r9 = 0
            if (r0 == 0) goto L_0x0048
        L_0x0047:
            r9 = 1
        L_0x0048:
            r10.A08 = r6
            r10.A0Y = r3
        L_0x004c:
            java.lang.CharSequence r0 = r10.A0X
            if (r0 == 0) goto L_0x0052
            if (r9 == 0) goto L_0x008f
        L_0x0052:
            android.text.TextPaint r2 = r10.A0g
            float r0 = r10.A08
            r2.setTextSize(r0)
            android.graphics.Typeface r0 = r10.A0U
            r2.setTypeface(r0)
            float r0 = r10.A0G
            int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x0065
            r4 = 0
        L_0x0065:
            r2.setLinearText(r4)
            java.lang.CharSequence r1 = r10.A0W
            android.text.TextUtils$TruncateAt r0 = android.text.TextUtils.TruncateAt.END
            java.lang.CharSequence r2 = android.text.TextUtils.ellipsize(r1, r2, r8, r0)
            java.lang.CharSequence r0 = r10.A0X
            boolean r0 = android.text.TextUtils.equals(r2, r0)
            if (r0 != 0) goto L_0x008f
            r10.A0X = r2
            android.view.View r0 = r10.A0i
            int r1 = X.AnonymousClass028.A05(r0)
            r0 = 1
            if (r1 != r0) goto L_0x0090
            X.02T r1 = X.AnonymousClass02U.A02
        L_0x0085:
            int r0 = r2.length()
            boolean r0 = r1.AK0(r2, r3, r0)
            r10.A0a = r0
        L_0x008f:
            return
        L_0x0090:
            X.02T r1 = X.AnonymousClass02U.A01
            goto L_0x0085
        L_0x0093:
            r9 = 0
            goto L_0x0034
        L_0x0095:
            float r2 = r10.A0F
            android.graphics.Typeface r1 = r10.A0U
            android.graphics.Typeface r0 = r10.A0V
            if (r1 == r0) goto L_0x00c4
            r10.A0U = r0
            r9 = 1
        L_0x00a0:
            float r0 = r11 - r2
            float r1 = java.lang.Math.abs(r0)
            r0 = 981668463(0x3a83126f, float:0.001)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00c0
            r10.A0G = r5
        L_0x00af:
            float r6 = r6 / r2
            float r0 = r7 * r6
            int r0 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r0 <= 0) goto L_0x00be
            float r8 = r8 / r6
            float r8 = java.lang.Math.min(r8, r7)
        L_0x00bb:
            r6 = r2
            goto L_0x0034
        L_0x00be:
            r8 = r7
            goto L_0x00bb
        L_0x00c0:
            float r11 = r11 / r2
            r10.A0G = r11
            goto L_0x00af
        L_0x00c4:
            r9 = 0
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47742Cf.A06(float):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r9.A0G == 1.0f) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A07(float r10) {
        /*
            r9 = this;
            r9.A06(r10)
            boolean r0 = X.C47742Cf.A0j
            if (r0 == 0) goto L_0x0010
            float r1 = r9.A0G
            r0 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            r0 = 1
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            r9.A0b = r0
            if (r0 == 0) goto L_0x0081
            android.graphics.Bitmap r0 = r9.A0R
            if (r0 != 0) goto L_0x0081
            android.graphics.Rect r0 = r9.A0e
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0081
            java.lang.CharSequence r0 = r9.A0X
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0081
            r0 = 0
            r9.A05(r0)
            android.text.TextPaint r8 = r9.A0g
            float r0 = r8.ascent()
            r9.A0H = r0
            float r0 = r8.descent()
            r9.A0I = r0
            java.lang.CharSequence r2 = r9.A0X
            r1 = 0
            int r0 = r2.length()
            float r0 = r8.measureText(r2, r1, r0)
            int r2 = java.lang.Math.round(r0)
            float r1 = r9.A0I
            float r0 = r9.A0H
            float r1 = r1 - r0
            int r1 = java.lang.Math.round(r1)
            if (r2 <= 0) goto L_0x0081
            if (r1 <= 0) goto L_0x0081
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r2, r1, r0)
            r9.A0R = r0
            android.graphics.Canvas r2 = new android.graphics.Canvas
            r2.<init>(r0)
            java.lang.CharSequence r3 = r9.A0X
            r4 = 0
            int r5 = r3.length()
            r6 = 0
            float r7 = (float) r1
            float r0 = r8.descent()
            float r7 = r7 - r0
            r2.drawText(r3, r4, r5, r6, r7, r8)
            android.graphics.Paint r0 = r9.A0S
            if (r0 != 0) goto L_0x0081
            r1 = 3
            android.graphics.Paint r0 = new android.graphics.Paint
            r0.<init>(r1)
            r9.A0S = r0
        L_0x0081:
            android.view.View r0 = r9.A0i
            r0.postInvalidateOnAnimation()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47742Cf.A07(float):void");
    }

    public void A08(int i) {
        Context context = this.A0i.getContext();
        C013406h r2 = new C013406h(context, context.obtainStyledAttributes(i, AnonymousClass07O.A0M));
        TypedArray typedArray = r2.A02;
        if (typedArray.hasValue(3)) {
            this.A0P = r2.A01(3);
        }
        if (typedArray.hasValue(0)) {
            this.A05 = (float) typedArray.getDimensionPixelSize(0, (int) this.A05);
        }
        this.A0J = typedArray.getInt(6, 0);
        this.A02 = typedArray.getFloat(7, 0.0f);
        this.A03 = typedArray.getFloat(8, 0.0f);
        this.A04 = typedArray.getFloat(9, 0.0f);
        r2.A04();
        this.A0T = A01(i);
        A03();
    }

    public void A09(int i) {
        Context context = this.A0i.getContext();
        C013406h r2 = new C013406h(context, context.obtainStyledAttributes(i, AnonymousClass07O.A0M));
        TypedArray typedArray = r2.A02;
        if (typedArray.hasValue(3)) {
            this.A0Q = r2.A01(3);
        }
        if (typedArray.hasValue(0)) {
            this.A0F = (float) typedArray.getDimensionPixelSize(0, (int) this.A0F);
        }
        this.A0L = typedArray.getInt(6, 0);
        this.A0C = typedArray.getFloat(7, 0.0f);
        this.A0D = typedArray.getFloat(8, 0.0f);
        this.A0E = typedArray.getFloat(9, 0.0f);
        r2.A04();
        this.A0V = A01(i);
        A03();
    }

    public void A0A(int i, int i2, int i3, int i4) {
        Rect rect = this.A0d;
        if (rect.left != i || rect.top != i2 || rect.right != i3 || rect.bottom != i4) {
            rect.set(i, i2, i3, i4);
            this.A0Y = true;
            A02();
        }
    }

    public void A0B(int i, int i2, int i3, int i4) {
        Rect rect = this.A0e;
        if (rect.left != i || rect.top != i2 || rect.right != i3 || rect.bottom != i4) {
            rect.set(i, i2, i3, i4);
            this.A0Y = true;
            A02();
        }
    }

    public void A0C(Canvas canvas) {
        boolean z;
        int save = canvas.save();
        if (this.A0X != null && this.A0Z) {
            float f = this.A06;
            float f2 = this.A07;
            if (!this.A0b || this.A0R == null) {
                z = false;
                TextPaint textPaint = this.A0g;
                textPaint.ascent();
                textPaint.descent();
            } else {
                z = true;
                f2 += this.A0H * this.A0G;
            }
            float f3 = this.A0G;
            if (f3 != 1.0f) {
                canvas.scale(f3, f3, f, f2);
            }
            if (z) {
                canvas.drawBitmap(this.A0R, f, f2, this.A0S);
            } else {
                CharSequence charSequence = this.A0X;
                canvas.drawText(charSequence, 0, charSequence.length(), f, f2, this.A0g);
            }
        }
        canvas.restoreToCount(save);
    }

    public void A0D(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.A0W)) {
            this.A0W = charSequence;
            this.A0X = null;
            Bitmap bitmap = this.A0R;
            if (bitmap != null) {
                bitmap.recycle();
                this.A0R = null;
            }
            A03();
        }
    }
}
