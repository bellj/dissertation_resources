package X;

import com.whatsapp.Mp4Ops;

/* renamed from: X.47r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C865147r extends AbstractC107844y2 {
    public final AbstractC15710nm A00;
    public final Mp4Ops A01;
    public final C16590pI A02;
    public final String A03;

    public C865147r(AbstractC15710nm r1, Mp4Ops mp4Ops, C16590pI r3, String str) {
        this.A02 = r3;
        this.A01 = mp4Ops;
        this.A00 = r1;
        this.A03 = str;
    }

    @Override // X.AbstractC47452At
    public AnonymousClass2BW A8D() {
        C16590pI r4 = this.A02;
        return new C67783Sw(this.A00, this.A01, r4, this.A03);
    }
}
