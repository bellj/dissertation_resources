package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.whatsapp.biz.catalog.view.CatalogMediaCard;

/* renamed from: X.2cD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC53012cD extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC53012cD(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (!this.A01) {
            this.A01 = true;
            CatalogMediaCard catalogMediaCard = (CatalogMediaCard) this;
            AnonymousClass01J r7 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            catalogMediaCard.A0F = C12960it.A0S(r7);
            catalogMediaCard.A03 = C12970iu.A0R(r7);
            catalogMediaCard.A04 = C12970iu.A0S(r7);
            catalogMediaCard.A0J = C12960it.A0T(r7);
            catalogMediaCard.A02 = C12980iv.A0W(r7);
            catalogMediaCard.A0H = (AnonymousClass17R) r7.AIz.get();
            catalogMediaCard.A07 = (AnonymousClass19S) r7.AIh.get();
            catalogMediaCard.A0E = C12960it.A0R(r7);
            catalogMediaCard.A06 = (C25821Ay) r7.A31.get();
            catalogMediaCard.A09 = C12990iw.A0V(r7);
            catalogMediaCard.A05 = C12980iv.A0Y(r7);
            catalogMediaCard.A08 = C12980iv.A0Z(r7);
            C14900mE A0R = C12970iu.A0R(r7);
            C15570nT A0S = C12970iu.A0S(r7);
            AbstractC14440lR A0T = C12960it.A0T(r7);
            AnonymousClass12P A0W = C12980iv.A0W(r7);
            AnonymousClass19X r8 = (AnonymousClass19X) r7.AIe.get();
            AnonymousClass18U A0T2 = C12990iw.A0T(r7);
            AnonymousClass19S r6 = (AnonymousClass19S) r7.AIh.get();
            C253619c r4 = (C253619c) r7.AId.get();
            AnonymousClass19T r2 = (AnonymousClass19T) r7.A2y.get();
            C25821Ay r1 = (C25821Ay) r7.A31.get();
            AnonymousClass19Q A0Z = C12980iv.A0Z(r7);
            C254719n r15 = (C254719n) r7.A2U.get();
            C19850um r0 = (C19850um) r7.A2v.get();
            catalogMediaCard.A0C = new AnonymousClass3C2(A0W, A0R, A0T2, A0S, r15, C12980iv.A0Y(r7), r1, r6, r0, A0Z, r2, r8, r4, C12970iu.A0b(r7), (AnonymousClass17R) r7.AIz.get(), A0T, (AnonymousClass18X) r7.AIo.get());
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
