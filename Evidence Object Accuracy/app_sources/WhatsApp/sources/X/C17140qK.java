package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.voipcalling.JNIUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.0qK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17140qK {
    public SharedPreferences A00;
    public final C14850m9 A01;
    public final C16630pM A02;

    public C17140qK(C14850m9 r1, C16630pM r2) {
        this.A01 = r1;
        this.A02 = r2;
    }

    public static final String A00(int i, int i2) {
        if (i2 == 1) {
            StringBuilder sb = new StringBuilder();
            sb.append("voip_camera_info_");
            sb.append(i);
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("voip_camera_info_");
        sb2.append(i);
        sb2.append("_api_");
        sb2.append(i2);
        return sb2.toString();
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A02.A01("voip_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public String A02() {
        String A03 = this.A01.A03(151);
        if (TextUtils.isEmpty(A03)) {
            return A01().getString("camera2_required_hardware_support_level", null);
        }
        return A03;
    }

    public List A03() {
        Map<String, ?> all = A01().getAll();
        ArrayList arrayList = new ArrayList();
        for (String str : all.keySet()) {
            if (str.startsWith("joinable_")) {
                arrayList.add(str.substring(9));
            }
        }
        return arrayList;
    }

    public void A04(JNIUtils.H26xSupportResult h26xSupportResult) {
        A01().edit().putBoolean("video_codec_h264_hw_supported", h26xSupportResult.isH264HwSupported).putBoolean("video_codec_h264_sw_supported", h26xSupportResult.isH264SwSupported).putBoolean("video_codec_h265_hw_supported", h26xSupportResult.isH265HwSupported).putBoolean("video_codec_h265_sw_supported", h26xSupportResult.isH265SwSupported).apply();
    }

    public void A05(String str) {
        SharedPreferences.Editor edit = A01().edit();
        StringBuilder sb = new StringBuilder("active_joinable_call");
        sb.append(str);
        edit.putBoolean(sb.toString(), true).apply();
    }
}
