package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape0S3100000_I0;
import com.whatsapp.jid.Jid;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.0r6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17610r6 {
    public final C16170oZ A00;
    public final C18010rl A01;
    public final C15650ng A02;
    public final AbstractC14440lR A03;
    public final Map A04;

    public C17610r6(C16170oZ r2, C18010rl r3, C15650ng r4, AbstractC14440lR r5, Map map) {
        C16700pc.A0E(r2, 1);
        C16700pc.A0E(r5, 2);
        C16700pc.A0E(r4, 3);
        C16700pc.A0E(r3, 5);
        this.A00 = r2;
        this.A03 = r5;
        this.A02 = r4;
        this.A04 = map;
        this.A01 = r3;
    }

    public void A00(Activity activity, String str, String str2, Map map) {
        Intent intent;
        Bundle extras;
        if (activity != null && (intent = activity.getIntent()) != null && (extras = intent.getExtras()) != null) {
            String string = extras.getString("chat_id");
            AbstractC14640lm r4 = (AbstractC14640lm) Jid.getNullable(string);
            String string2 = extras.getString("message_id");
            long j = extras.getLong("message_row_id", 0);
            String string3 = extras.getString("action_name");
            if (r4 != null && str != null && str2 != null && map != null && string2 != null && string3 != null) {
                this.A00.A0H(r4, str, str2, new JSONObject(map).toString(), j);
                this.A03.Ab2(new RunnableBRunnable0Shape0S3100000_I0(this, string2, string, string3, 1));
            }
        }
    }
}
