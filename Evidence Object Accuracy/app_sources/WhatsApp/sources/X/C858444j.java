package X;

import com.whatsapp.group.GroupChatInfo;
import java.util.Set;

/* renamed from: X.44j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C858444j extends AbstractC33331dp {
    public final /* synthetic */ GroupChatInfo A00;

    public C858444j(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        GroupChatInfo groupChatInfo = this.A00;
        if (set.contains(groupChatInfo.A1C)) {
            GroupChatInfo.A03(groupChatInfo);
        }
    }
}
