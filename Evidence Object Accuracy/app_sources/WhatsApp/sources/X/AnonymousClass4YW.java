package X;

/* renamed from: X.4YW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4YW {
    public int A00;
    public int A01 = 1;
    public int A02;
    public int A03;
    public int A04;
    public String A05;
    public C95044cz A06;
    public C95044cz A07 = new C95044cz();
    public C95404de[] A08 = new C95404de[256];
    public C95404de[] A09;
    public final C92894Xv A0A;

    public AnonymousClass4YW(C92894Xv r2) {
        this.A0A = r2;
    }

    public static C95404de A00(AnonymousClass4YW r1, int i) {
        C95404de[] r12 = r1.A08;
        return r12[i % r12.length];
    }

    public static void A01(String str, C95044cz r1, AnonymousClass4YW r2) {
        r1.A04(r2.A02(str));
    }

    public int A02(String str) {
        int hashCode = (1 + str.hashCode()) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, hashCode); A00 != null; A00 = A00.A01) {
            if (A00.A04 == 1 && A00.A02 == hashCode && A00.A08.equals(str)) {
                return A00.A03;
            }
        }
        C95044cz r10 = this.A07;
        r10.A02(1);
        int length = str.length();
        if (length <= 65535) {
            int i = r10.A00;
            if (i + 2 + length > r10.A01.length) {
                r10.A05(length + 2);
            }
            byte[] bArr = r10.A01;
            int i2 = i + 1;
            C72463ee.A0W(bArr, length, i);
            int i3 = i2 + 1;
            bArr[i2] = (byte) length;
            int i4 = 0;
            while (i4 < length) {
                char charAt = str.charAt(i4);
                if (charAt < 1 || charAt > 127) {
                    r10.A00 = i3;
                    r10.A09(i4, str, 65535);
                    break;
                }
                bArr[i3] = (byte) charAt;
                i4++;
                i3++;
            }
            r10.A00 = i3;
            int i5 = this.A01;
            this.A01 = i5 + 1;
            C95404de r0 = new C95404de(str, i5, 1, hashCode);
            A0F(r0);
            return r0.A03;
        }
        throw C12970iu.A0f("UTF8 string too large");
    }

    public int A03(String str) {
        int hashCode = (128 + str.hashCode()) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, hashCode); A00 != null; A00 = A00.A01) {
            if (A00.A04 == 128 && A00.A02 == hashCode && A00.A08.equals(str)) {
                return A00.A03;
            }
        }
        return A06(new C95404de(str, this.A04, 128, hashCode));
    }

    public int A04(String str, int i) {
        int hashCode = (129 + str.hashCode() + i) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, hashCode); A00 != null; A00 = A00.A01) {
            if (A00.A04 == 129 && A00.A02 == hashCode && A00.A05 == ((long) i) && A00.A08.equals(str)) {
                return A00.A03;
            }
        }
        return A06(new C95404de(str, this.A04, hashCode, (long) i));
    }

    public int A05(String str, String str2) {
        int hashCode = (12 + (str.hashCode() * str2.hashCode())) & Integer.MAX_VALUE;
        C95404de A00 = A00(this, hashCode);
        while (true) {
            if (A00 != null) {
                if (A00.A04 == 12 && A00.A02 == hashCode && A00.A06.equals(str) && A00.A08.equals(str2)) {
                    break;
                }
                A00 = A00.A01;
            } else {
                this.A07.A08(12, A02(str), A02(str2));
                int i = this.A01;
                this.A01 = i + 1;
                A00 = new C95404de(str, str2, i, hashCode);
                A0F(A00);
                break;
            }
        }
        return A00.A03;
    }

    public final int A06(C95404de r5) {
        C95404de[] r3 = this.A09;
        C95404de[] r2 = r3;
        if (r3 == null) {
            r3 = new C95404de[16];
            this.A09 = r3;
            r2 = r3;
        }
        int i = this.A04;
        int length = r3.length;
        if (i == length) {
            r2 = new C95404de[length << 1];
            System.arraycopy(r3, 0, r2, 0, length);
            this.A09 = r2;
        }
        int i2 = this.A04;
        this.A04 = i2 + 1;
        r2[i2] = r5;
        A0F(r5);
        return r5.A03;
    }

    public final C95404de A07(int i, int i2) {
        int i3 = (i + i2) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, i3); A00 != null; A00 = A00.A01) {
            if (A00.A04 == i && A00.A02 == i3 && A00.A05 == ((long) i2)) {
                return A00;
            }
        }
        C95044cz r0 = this.A07;
        r0.A02(i);
        r0.A03(i2);
        int i4 = this.A01;
        this.A01 = i4 + 1;
        C95404de r4 = new C95404de(i4, i, i3, (long) i2);
        A0F(r4);
        return r4;
    }

    public final C95404de A08(int i, long j) {
        int i2 = (int) j;
        int i3 = (int) (j >>> 32);
        int i4 = (i + i2 + i3) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, i4); A00 != null; A00 = A00.A01) {
            if (A00.A04 == i && A00.A02 == i4 && A00.A05 == j) {
                return A00;
            }
        }
        int i5 = this.A01;
        C95044cz r4 = this.A07;
        r4.A02(i);
        int i6 = r4.A00;
        if (i6 + 8 > r4.A01.length) {
            r4.A05(8);
        }
        byte[] bArr = r4.A01;
        int A0O = C72453ed.A0O(bArr, i6, i3);
        bArr[A0O] = (byte) i3;
        int A0O2 = C72453ed.A0O(bArr, A0O + 1, i2);
        bArr[A0O2] = (byte) i2;
        r4.A00 = A0O2 + 1;
        this.A01 += 2;
        C95404de r6 = new C95404de(i5, i, i4, j);
        A0F(r6);
        return r6;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v10, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public C95404de A09(Object obj) {
        char c;
        if (!(obj instanceof Integer) && !(obj instanceof Byte)) {
            if (obj instanceof Character) {
                c = ((Character) obj).charValue();
            } else if (!(obj instanceof Short)) {
                if (obj instanceof Boolean) {
                    c = C12970iu.A1Y(obj);
                } else if (obj instanceof Float) {
                    return A07(4, Float.floatToRawIntBits(C72453ed.A02(obj)));
                } else {
                    if (obj instanceof Long) {
                        return A08(5, C12980iv.A0G(obj));
                    }
                    if (obj instanceof Double) {
                        return A08(6, Double.doubleToRawLongBits(C72453ed.A00(obj)));
                    }
                    if (obj instanceof String) {
                        return A0A((String) obj, 8);
                    }
                    if (obj instanceof C95574dz) {
                        C95574dz r11 = (C95574dz) obj;
                        int i = r11.A00;
                        if (i == 12 || i == 10) {
                            return A0A(r11.A03.substring(r11.A01, r11.A02), 7);
                        }
                        String A06 = r11.A06();
                        if (i == 11) {
                            return A0A(A06, 16);
                        }
                        return A0A(A06, 7);
                    } else if (obj instanceof C92854Xr) {
                        C92854Xr r112 = (C92854Xr) obj;
                        return A0D(r112.A03, r112.A02, r112.A01, r112.A00, r112.A04);
                    } else if (obj instanceof C92804Xm) {
                        C92804Xm r113 = (C92804Xm) obj;
                        return A0B(r113.A01, r113.A00, 17, A0E(r113.A02, r113.A03).A03);
                    } else {
                        throw C12970iu.A0f(C12960it.A0b("value ", obj));
                    }
                }
            }
            return A07(3, c == 1 ? 1 : 0);
        }
        c = C12960it.A05(obj);
        return A07(3, c == 1 ? 1 : 0);
    }

    public final C95404de A0A(String str, int i) {
        int hashCode = (i + str.hashCode()) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, hashCode); A00 != null; A00 = A00.A01) {
            if (A00.A04 == i && A00.A02 == hashCode && A00.A08.equals(str)) {
                return A00;
            }
        }
        this.A07.A07(i, A02(str));
        int i2 = this.A01;
        this.A01 = i2 + 1;
        C95404de r0 = new C95404de(str, i2, i, hashCode);
        A0F(r0);
        return r0;
    }

    public final C95404de A0B(String str, String str2, int i, int i2) {
        int hashCode = (i + (str.hashCode() * str2.hashCode() * (i2 + 1))) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, hashCode); A00 != null; A00 = A00.A01) {
            if (A00.A04 == i && A00.A02 == hashCode && A00.A05 == ((long) i2) && A00.A06.equals(str) && A00.A08.equals(str2)) {
                return A00;
            }
        }
        this.A07.A08(i, i2, A05(str, str2));
        int i3 = this.A01;
        this.A01 = i3 + 1;
        C95404de r5 = new C95404de(null, str, str2, i3, i, hashCode, (long) i2);
        A0F(r5);
        return r5;
    }

    public final C95404de A0C(String str, String str2, String str3, int i) {
        int hashCode = (i + (str.hashCode() * str2.hashCode() * str3.hashCode())) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, hashCode); A00 != null; A00 = A00.A01) {
            if (A00.A04 == i && A00.A02 == hashCode && A00.A07.equals(str) && A00.A06.equals(str2) && A00.A08.equals(str3)) {
                return A00;
            }
        }
        this.A07.A08(i, A0A(str, 7).A03, A05(str2, str3));
        int i2 = this.A01;
        this.A01 = i2 + 1;
        C95404de r2 = new C95404de(str, str2, str3, i2, i, hashCode, 0);
        A0F(r2);
        return r2;
    }

    public C95404de A0D(String str, String str2, String str3, int i, boolean z) {
        int i2;
        int hashCode = (15 + (str.hashCode() * str2.hashCode() * str3.hashCode() * i)) & Integer.MAX_VALUE;
        for (C95404de A00 = A00(this, hashCode); A00 != null; A00 = A00.A01) {
            if (A00.A04 == 15 && A00.A02 == hashCode && A00.A05 == ((long) i) && A00.A07.equals(str) && A00.A06.equals(str2) && A00.A08.equals(str3)) {
                return A00;
            }
        }
        C95044cz r7 = this.A07;
        if (i <= 4) {
            i2 = 9;
        } else {
            i2 = 10;
            if (z) {
                i2 = 11;
            }
        }
        int i3 = A0C(str, str2, str3, i2).A03;
        int i4 = r7.A00;
        if (i4 + 4 > r7.A01.length) {
            r7.A05(4);
        }
        byte[] bArr = r7.A01;
        bArr[i4] = (byte) 15;
        C95044cz.A01(r7, bArr, i4 + 1, i, i3);
        int i5 = this.A01;
        this.A01 = i5 + 1;
        C95404de r8 = new C95404de(str, str2, str3, i5, 15, hashCode, (long) i);
        A0F(r8);
        return r8;
    }

    public C95404de A0E(C92854Xr r17, Object... objArr) {
        C95044cz r7 = this.A06;
        if (r7 == null) {
            r7 = new C95044cz();
            this.A06 = r7;
        }
        int length = objArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < length; i++) {
            iArr[i] = A09(objArr[i]).A03;
        }
        int i2 = r7.A00;
        r7.A04(A0D(r17.A03, r17.A02, r17.A01, r17.A00, r17.A04).A03);
        r7.A04(length);
        for (int i3 = 0; i3 < length; i3++) {
            r7.A04(iArr[i3]);
        }
        int i4 = r7.A00 - i2;
        int hashCode = r17.hashCode();
        for (Object obj : objArr) {
            hashCode ^= obj.hashCode();
        }
        int i5 = hashCode & Integer.MAX_VALUE;
        C95044cz r6 = this.A06;
        byte[] bArr = r6.A01;
        for (C95404de A00 = A00(this, i5); A00 != null; A00 = A00.A01) {
            if (A00.A04 == 64 && A00.A02 == i5) {
                int i6 = (int) A00.A05;
                for (int i7 = 0; i7 < i4; i7++) {
                    if (bArr[i2 + i7] == bArr[i6 + i7]) {
                    }
                }
                r6.A00 = i2;
                return A00;
            }
        }
        int i8 = this.A00;
        this.A00 = i8 + 1;
        C95404de r10 = new C95404de(i8, 64, i5, (long) i2);
        A0F(r10);
        return r10;
    }

    public final void A0F(C95404de r10) {
        int i = this.A02;
        C95404de[] r7 = this.A08;
        C95404de[] r6 = r7;
        int length = r7.length;
        if (i > ((length * 3) >> 2)) {
            int i2 = (length << 1) + 1;
            r6 = new C95404de[i2];
            for (int i3 = length - 1; i3 >= 0; i3--) {
                C95404de r3 = r7[i3];
                while (r3 != null) {
                    int i4 = r3.A02 % i2;
                    C95404de r1 = r3.A01;
                    r3.A01 = r6[i4];
                    r6[i4] = r3;
                    r3 = r1;
                }
            }
            this.A08 = r6;
        }
        this.A02 = i + 1;
        int length2 = r10.A02 % r6.length;
        r10.A01 = r6[length2];
        r6[length2] = r10;
    }
}
