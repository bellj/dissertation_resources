package X;

import android.content.Context;

/* renamed from: X.5gW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120645gW extends C120895gv {
    public final /* synthetic */ C120525gK A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120645gW(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120525gK r11) {
        super(context, r8, r9, r10, "upi-change-mpin");
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r2) {
        super.A02(r2);
        AnonymousClass6MS r0 = this.A00.A00;
        if (r0 != null) {
            r0.AVu(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r2) {
        super.A03(r2);
        AnonymousClass6MS r0 = this.A00.A00;
        if (r0 != null) {
            r0.AVu(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r3) {
        super.A04(r3);
        AnonymousClass6MS r1 = this.A00.A00;
        if (r1 != null) {
            r1.AVu(null);
        }
    }
}
