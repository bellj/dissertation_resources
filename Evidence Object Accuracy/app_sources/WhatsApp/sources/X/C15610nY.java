package X;

import android.content.Context;
import android.content.res.Resources;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.chromium.net.UrlRequest;

/* renamed from: X.0nY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15610nY {
    public final C15570nT A00;
    public final C15550nR A01;
    public final AnonymousClass10S A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;
    public final C19990v2 A05;
    public final C15600nX A06;
    public final C14850m9 A07;
    public final ConcurrentHashMap A08 = new ConcurrentHashMap();
    public final ConcurrentHashMap A09 = new ConcurrentHashMap();

    public C15610nY(C15570nT r2, C15550nR r3, AnonymousClass10S r4, C16590pI r5, AnonymousClass018 r6, C19990v2 r7, C15600nX r8, C14850m9 r9) {
        this.A07 = r9;
        this.A03 = r5;
        this.A00 = r2;
        this.A05 = r7;
        this.A01 = r3;
        this.A04 = r6;
        this.A02 = r4;
        this.A06 = r8;
    }

    public static CharSequence A00(Context context, AnonymousClass018 r3, C15370n3 r4) {
        int i;
        Integer num = r4.A0H;
        if (num == null) {
            return null;
        }
        int intValue = num.intValue();
        if (intValue == 0) {
            return r4.A0P;
        }
        switch (num.intValue()) {
            case 1:
                i = R.string.phone_type_home;
                break;
            case 2:
                i = R.string.phone_type_mobile;
                break;
            case 3:
                i = R.string.phone_type_work;
                break;
            case 4:
                i = R.string.phone_type_fax_work;
                break;
            case 5:
                i = R.string.phone_type_fax_home;
                break;
            case 6:
                i = R.string.phone_type_pager;
                break;
            case 7:
                i = R.string.phone_type_other;
                break;
            case 8:
                i = R.string.phone_type_callback;
                break;
            case 9:
                i = R.string.phone_type_car;
                break;
            case 10:
                i = R.string.phone_type_company_main;
                break;
            case 11:
                i = R.string.phone_type_isdn;
                break;
            case 12:
                i = R.string.phone_type_main;
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                i = R.string.phone_type_other_fax;
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                i = R.string.phone_type_radio;
                break;
            case 15:
                i = R.string.phone_type_telex;
                break;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                i = R.string.phone_type_tty_tdd;
                break;
            case 17:
                i = R.string.phone_type_work_mobile;
                break;
            case 18:
                i = R.string.phone_type_work_pager;
                break;
            case 19:
                i = R.string.phone_type_assistant;
                break;
            case C43951xu.A01 /* 20 */:
                i = R.string.phone_type_mms;
                break;
            default:
                return r3.A00.getResources().getString(ContactsContract.CommonDataKinds.Phone.getTypeLabelResource(intValue));
        }
        Integer valueOf = Integer.valueOf(i);
        if (valueOf != null) {
            return context.getString(valueOf.intValue());
        }
        return r3.A00.getResources().getString(ContactsContract.CommonDataKinds.Phone.getTypeLabelResource(intValue));
    }

    public static String A01(C15610nY r1, C15370n3 r2) {
        return r1.A0A(r2, -1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0019, code lost:
        if (android.text.TextUtils.isEmpty(r2.A0K) == false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02(X.C15370n3 r2, boolean r3) {
        /*
            com.whatsapp.jid.Jid r0 = r2.A0D
            boolean r0 = X.C15380n4.A0M(r0)
            if (r0 != 0) goto L_0x001b
            int r1 = r2.A06
            r0 = 3
            if (r1 != r0) goto L_0x0020
            if (r3 != 0) goto L_0x001b
            X.1Pc r0 = r2.A0C
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = r2.A0K
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0030
        L_0x001b:
            java.lang.String r0 = r2.A0D()
            return r0
        L_0x0020:
            r0 = 2
            if (r1 == r0) goto L_0x0033
            r0 = 1
            if (r1 == r0) goto L_0x0033
            boolean r0 = r2.A0K()
            if (r0 == 0) goto L_0x0040
            boolean r0 = r2.A0Y
            if (r0 == 0) goto L_0x0040
        L_0x0030:
            java.lang.String r0 = r2.A0K
            return r0
        L_0x0033:
            X.1Pc r0 = r2.A0C
            if (r0 != 0) goto L_0x0030
            java.lang.String r0 = r2.A0K
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0040
            goto L_0x0030
        L_0x0040:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15610nY.A02(X.0n3, boolean):java.lang.String");
    }

    public static boolean A03(C15370n3 r3) {
        if (C15380n4.A0F(r3.A0D) || !TextUtils.isEmpty(r3.A0K)) {
            return false;
        }
        if (!r3.A0J()) {
            return !TextUtils.isEmpty(r3.A0U);
        }
        if (r3.A0H() || TextUtils.isEmpty(r3.A0D())) {
            return false;
        }
        return true;
    }

    public String A04(C15370n3 r3) {
        return A0B(r3, -1, false);
    }

    public String A05(C15370n3 r4) {
        if (!C15380n4.A0F(r4.A0D) || !TextUtils.isEmpty(r4.A0K)) {
            return A04(r4);
        }
        Jid A0B = r4.A0B(C29901Ve.class);
        AnonymousClass009.A05(A0B);
        return A0E((AbstractC15590nW) A0B, -1, true);
    }

    public String A06(C15370n3 r3) {
        if (this.A00.A0F(r3.A0D)) {
            return this.A03.A00.getString(R.string.you);
        }
        if (r3.A0C != null) {
            return A04(r3);
        }
        if (!TextUtils.isEmpty(r3.A0U)) {
            return A09(r3);
        }
        return null;
    }

    public String A07(C15370n3 r8) {
        Context context;
        int i;
        if (C15380n4.A0N(r8.A0D)) {
            context = this.A03.A00;
            i = R.string.my_status;
        } else if (r8.A0L()) {
            return A02(r8, false);
        } else {
            if (!TextUtils.isEmpty(r8.A0K)) {
                return r8.A0K;
            }
            if (!TextUtils.isEmpty(r8.A0I)) {
                return r8.A0I;
            }
            if (r8.A0K()) {
                String A09 = this.A05.A09((AbstractC14640lm) r8.A0B(AbstractC14640lm.class));
                if (!TextUtils.isEmpty(A09)) {
                    return A09;
                }
                context = this.A03.A00;
                i = R.string.group_subject_unknown;
            } else if (C15380n4.A0F(r8.A0D)) {
                Jid A0B = r8.A0B(C29901Ve.class);
                AnonymousClass009.A05(A0B);
                AnonymousClass1YM A02 = this.A06.A02((AbstractC15590nW) A0B);
                boolean A0H = A02.A0H(this.A00);
                int size = A02.A02.size();
                if (A0H) {
                    size--;
                }
                return this.A03.A00.getResources().getQuantityString(R.plurals.broadcast_n_recipients, size, Integer.valueOf(size));
            } else {
                String A092 = this.A05.A09((AbstractC14640lm) r8.A0B(AbstractC14640lm.class));
                if (!TextUtils.isEmpty(A092)) {
                    return A092;
                }
                if (!TextUtils.isEmpty(A09(r8))) {
                    this.A07.A07(604);
                }
                return this.A04.A0G(C248917h.A01(r8));
            }
        }
        return context.getString(i);
    }

    public String A08(C15370n3 r7) {
        if (C15380n4.A0N(r7.A0D)) {
            return this.A03.A00.getString(R.string.my_status);
        }
        if (r7.A0L()) {
            return A02(r7, false);
        }
        if (!TextUtils.isEmpty(r7.A0K)) {
            return r7.A0K;
        }
        if (r7.A0K()) {
            String A09 = this.A05.A09((AbstractC14640lm) r7.A0B(AbstractC14640lm.class));
            if (TextUtils.isEmpty(A09)) {
                return this.A03.A00.getString(R.string.group_subject_unknown);
            }
            return A09;
        } else if (C15380n4.A0F(r7.A0D)) {
            Jid A0B = r7.A0B(C29901Ve.class);
            AnonymousClass009.A05(A0B);
            AnonymousClass1YM A02 = this.A06.A02((AbstractC15590nW) A0B);
            boolean A0H = A02.A0H(this.A00);
            int size = A02.A02.size();
            if (A0H) {
                size--;
            }
            return this.A03.A00.getResources().getQuantityString(R.plurals.broadcast_n_recipients, size, Integer.valueOf(size));
        } else {
            String A092 = this.A05.A09((AbstractC14640lm) r7.A0B(AbstractC14640lm.class));
            if (!TextUtils.isEmpty(A092)) {
                return A092;
            }
            if (!TextUtils.isEmpty(r7.A0U)) {
                return A09(r7);
            }
            return this.A04.A0G(C248917h.A01(r7));
        }
    }

    public String A09(C15370n3 r6) {
        Context context;
        Object[] objArr;
        String A0D;
        boolean A07 = this.A07.A07(604);
        int i = R.string.conversation_header_pushname;
        if (A07) {
            i = R.string.conversation_pushname_elevated_profile;
        }
        if (!r6.A0J() && !TextUtils.isEmpty(r6.A0U)) {
            context = this.A03.A00;
            objArr = new Object[1];
            A0D = r6.A0U;
        } else if (!r6.A0J() || r6.A0H() || TextUtils.isEmpty(r6.A0D())) {
            return "";
        } else {
            context = this.A03.A00;
            objArr = new Object[1];
            A0D = r6.A0D();
        }
        objArr[0] = A0D;
        return context.getString(i, objArr);
    }

    public String A0A(C15370n3 r7, int i) {
        if (r7.A0C == null || TextUtils.isEmpty(r7.A0M) || r7.A0L()) {
            return A0C(r7, i, false, true, false);
        }
        return r7.A0M;
    }

    public String A0B(C15370n3 r7, int i, boolean z) {
        return A0C(r7, i, z, true, false);
    }

    public String A0C(C15370n3 r4, int i, boolean z, boolean z2, boolean z3) {
        String A0D = A0D(r4, z, z3);
        if (TextUtils.isEmpty(A0D)) {
            A0D = A09(r4);
            if (TextUtils.isEmpty(A0D) || !this.A07.A07(604) || !(i == 1 || i == 4 || i == 5)) {
                if (z2) {
                    return this.A04.A0G(C248917h.A01(r4));
                }
                AbstractC14640lm r0 = (AbstractC14640lm) r4.A0B(AbstractC14640lm.class);
                if (r0 == null) {
                    return null;
                }
                return C248917h.A03(r0);
            }
        }
        return A0D;
    }

    public String A0D(C15370n3 r7, boolean z, boolean z2) {
        String A09;
        if (C15380n4.A0N(r7.A0D)) {
            return this.A03.A00.getString(R.string.my_status);
        }
        if (r7.A0L()) {
            return A02(r7, z);
        }
        if (!TextUtils.isEmpty(r7.A0K)) {
            return r7.A0K;
        }
        if (r7.A0K()) {
            String A092 = this.A05.A09((AbstractC14640lm) r7.A0B(AbstractC14640lm.class));
            if (!TextUtils.isEmpty(A092)) {
                return A092;
            }
            if (z2) {
                return null;
            }
            return this.A03.A00.getString(R.string.group_subject_unknown);
        } else if (C15380n4.A0F(r7.A0D)) {
            Jid A0B = r7.A0B(C29901Ve.class);
            AnonymousClass009.A05(A0B);
            AnonymousClass1YM A02 = this.A06.A02((AbstractC15590nW) A0B);
            boolean A0H = A02.A0H(this.A00);
            int size = A02.A02.size();
            if (A0H) {
                size--;
            }
            return this.A03.A00.getResources().getQuantityString(R.plurals.broadcast_n_recipients, size, Integer.valueOf(size));
        } else {
            AbstractC14640lm r1 = (AbstractC14640lm) r7.A0B(AbstractC14640lm.class);
            if (r1 == null) {
                A09 = null;
            } else {
                A09 = this.A05.A09(r1);
            }
            if (!TextUtils.isEmpty(A09)) {
                return A09;
            }
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0052, code lost:
        if (X.C15380n4.A0G(r12) != false) goto L_0x0054;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A0E(X.AbstractC15590nW r12, int r13, boolean r14) {
        /*
            r11 = this;
            r5 = r11
            if (r14 == 0) goto L_0x0043
            java.util.concurrent.ConcurrentHashMap r2 = r11.A09
        L_0x0005:
            java.lang.Object r0 = r2.get(r12)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x005e
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            X.0nX r0 = r11.A06
            X.1YM r0 = r0.A02(r12)
            X.1JO r0 = r0.A07()
            java.util.Iterator r4 = r0.iterator()
            r3 = 0
        L_0x0021:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0046
            java.lang.Object r1 = r4.next()
            X.1YO r1 = (X.AnonymousClass1YO) r1
            X.0nT r0 = r11.A00
            com.whatsapp.jid.UserJid r1 = r1.A03
            boolean r0 = r0.A0F(r1)
            if (r0 == 0) goto L_0x0039
            r3 = 1
            goto L_0x0021
        L_0x0039:
            X.0nR r0 = r11.A01
            X.0n3 r0 = r0.A0B(r1)
            r6.add(r0)
            goto L_0x0021
        L_0x0043:
            java.util.concurrent.ConcurrentHashMap r2 = r11.A08
            goto L_0x0005
        L_0x0046:
            r7 = -1
            if (r14 == 0) goto L_0x004b
            r7 = 10
        L_0x004b:
            if (r3 == 0) goto L_0x0054
            boolean r0 = X.C15380n4.A0G(r12)
            r9 = 1
            if (r0 == 0) goto L_0x0055
        L_0x0054:
            r9 = 0
        L_0x0055:
            r10 = 0
            r8 = r13
            java.lang.String r0 = r5.A0G(r6, r7, r8, r9, r10)
            r2.put(r12, r0)
        L_0x005e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15610nY.A0E(X.0nW, int, boolean):java.lang.String");
    }

    public String A0F(Iterable iterable, int i) {
        Set hashSet = new HashSet();
        return A0G(hashSet, -1, i, A0N(iterable, hashSet), true);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    public String A0G(Iterable iterable, int i, int i2, boolean z, boolean z2) {
        List A0I = A0I(iterable, i2, z, z2);
        int size = A0I.size();
        if (size > i && i >= 0) {
            String[] strArr = new String[i + 1];
            for (int i3 = 0; i3 < i; i3++) {
                strArr[i3] = A0I.get(i3);
            }
            int i4 = size - i;
            strArr[i] = this.A04.A0I(new Object[]{Integer.valueOf(i4)}, R.plurals.names_others, (long) i4);
            A0I = Arrays.asList(strArr);
        }
        return C32721cd.A00(this.A04, A0I, z2);
    }

    public ArrayList A0H(Context context, C32731ce r9, List list) {
        String str;
        int size;
        int i;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        boolean z = false;
        while (it.hasNext()) {
            C15370n3 A0B = this.A01.A0B((AbstractC14640lm) it.next());
            if (C15380n4.A0N(A0B.A0D)) {
                z = true;
            } else {
                String A04 = A04(A0B);
                if (A04 != null) {
                    arrayList.add(A04);
                }
            }
        }
        if (z) {
            int i2 = r9.A00;
            if (i2 == 0) {
                str = context.getString(R.string.status_media_privacy_contacts);
            } else {
                Resources resources = context.getResources();
                if (i2 == 1) {
                    size = r9.A01.size();
                    i = R.plurals.status_chip_allowlist_description;
                } else if (i2 == 2) {
                    size = r9.A02.size();
                    i = R.plurals.status_chip_denylist_description;
                } else {
                    str = "";
                }
                str = resources.getQuantityString(i, size, Integer.valueOf(size));
            }
            arrayList.add(0, str);
        }
        return arrayList;
    }

    public List A0I(Iterable iterable, int i, boolean z, boolean z2) {
        String A0C;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            C15370n3 r6 = (C15370n3) it.next();
            if (z2 || r6.A0C == null || TextUtils.isEmpty(r6.A0M) || r6.A0L()) {
                A0C = A0C(r6, i, false, true, false);
            } else {
                A0C = r6.A0M;
            }
            if (A0C != null) {
                if (A0L(r6, -1)) {
                    arrayList2.add(A0C);
                } else {
                    arrayList.add(A0C);
                }
            }
        }
        Collator instance = Collator.getInstance(AnonymousClass018.A00(this.A04.A00));
        instance.setDecomposition(1);
        Collections.sort(arrayList, instance);
        Collections.sort(arrayList2);
        arrayList.addAll(arrayList2);
        if (z) {
            arrayList.add(this.A03.A00.getString(R.string.you));
        }
        return arrayList;
    }

    public void A0J(AbstractC15590nW r2) {
        this.A08.remove(r2);
        this.A09.remove(r2);
    }

    public boolean A0K(C15370n3 r4) {
        if (r4.A0C != null) {
            String str = r4.A0K;
            String A0D = r4.A0D();
            if (!TextUtils.isEmpty(A0D) && !TextUtils.isEmpty(str) && AnonymousClass1US.A07(str).equals(AnonymousClass1US.A07(A0D))) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        if (r1 == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (A03(r7) == false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0L(X.C15370n3 r7, int r8) {
        /*
            r6 = this;
            X.0m9 r1 = r6.A07
            r0 = 604(0x25c, float:8.46E-43)
            boolean r5 = r1.A07(r0)
            r4 = 0
            r3 = 1
            if (r5 == 0) goto L_0x0013
            boolean r0 = A03(r7)
            r1 = 1
            if (r0 != 0) goto L_0x0014
        L_0x0013:
            r1 = 0
        L_0x0014:
            if (r8 == r3) goto L_0x001c
            r0 = 4
            if (r8 == r0) goto L_0x001c
            r0 = 5
            if (r8 != r0) goto L_0x001f
        L_0x001c:
            r2 = 1
            if (r1 != 0) goto L_0x0020
        L_0x001f:
            r2 = 0
        L_0x0020:
            if (r5 == 0) goto L_0x0045
            boolean r0 = r7.A0H()
            if (r0 != 0) goto L_0x004d
        L_0x0028:
            r1 = 1
        L_0x0029:
            com.whatsapp.jid.Jid r0 = r7.A0D
            boolean r0 = X.C15380n4.A0F(r0)
            if (r0 != 0) goto L_0x0044
            java.lang.String r0 = r7.A0K
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0044
            if (r2 != 0) goto L_0x0044
            java.lang.String r0 = r7.A0D()
            if (r0 == 0) goto L_0x0043
            if (r1 == 0) goto L_0x0044
        L_0x0043:
            r4 = 1
        L_0x0044:
            return r4
        L_0x0045:
            int r1 = r7.A06
            r0 = 2
            if (r1 == r0) goto L_0x0028
            if (r1 != r3) goto L_0x004d
            goto L_0x0028
        L_0x004d:
            r1 = 0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15610nY.A0L(X.0n3, int):boolean");
    }

    public boolean A0M(C15370n3 r7, List list, boolean z) {
        String A01;
        boolean equals;
        if (list != null && !list.isEmpty()) {
            if (!TextUtils.isEmpty(r7.A0K)) {
                A01 = AbstractC32741cf.A03(r7.A0K);
            } else if (C15380n4.A0F(r7.A0D)) {
                Jid A0B = r7.A0B(C29901Ve.class);
                AnonymousClass009.A05(A0B);
                A01 = A0E((AbstractC15590nW) A0B, -1, false);
            } else {
                A01 = C248917h.A01(r7);
            }
            AnonymousClass018 r1 = this.A04;
            if (!C32751cg.A03(r1, A01, list, z) && ((!r7.A0J() || !r7.A0H() || !C32751cg.A03(r1, r7.A0D(), list, z)) && !C32751cg.A03(r1, r7.A0O, list, z) && !C32751cg.A03(r1, r7.A0J, list, z) && !C32751cg.A03(r1, r7.A0S, list, z))) {
                if (!C15380n4.A0F(r7.A0D) && !r7.A0K()) {
                    Jid A0B2 = r7.A0B(AbstractC14640lm.class);
                    AnonymousClass009.A05(A0B2);
                    AbstractC14640lm r3 = (AbstractC14640lm) A0B2;
                    if (!TextUtils.isEmpty(C248917h.A03(r3))) {
                        Iterator it = list.iterator();
                        while (it.hasNext()) {
                            String str = (String) it.next();
                            String str2 = r3.user;
                            if (z) {
                                equals = str2.contains(str);
                                continue;
                            } else {
                                equals = str2.equals(str);
                                continue;
                            }
                            if (!equals) {
                            }
                        }
                    }
                }
                return false;
            }
        }
        return true;
    }

    public final boolean A0N(Iterable iterable, Set set) {
        Iterator it = iterable.iterator();
        boolean z = false;
        while (it.hasNext()) {
            AbstractC14640lm r1 = (AbstractC14640lm) it.next();
            if (this.A00.A0F(r1)) {
                z = true;
            } else {
                set.add(this.A01.A0B(r1));
            }
        }
        return z;
    }
}
