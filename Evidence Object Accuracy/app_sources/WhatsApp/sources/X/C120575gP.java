package X;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5gP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120575gP extends C120895gv {
    public final /* synthetic */ C120445gC A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120575gP(Context context, C14900mE r8, C120445gC r9, C18650sn r10, C64513Fv r11) {
        super(context, r8, r10, r11, "upi-get-token");
        this.A00 = r9;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        AbstractC136436Mn r1 = this.A00.A04;
        if (r1 != null) {
            r1.AR3(r3, false);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        AbstractC136436Mn r1 = this.A00.A04;
        if (r1 != null) {
            r1.AR3(r3, false);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r13) {
        String str;
        JSONObject A05;
        super.A04(r13);
        AnonymousClass1V8 A0c = C117305Zk.A0c(r13);
        if (A0c != null) {
            String A0I = A0c.A0I("token", null);
            if (!TextUtils.isEmpty(A0I)) {
                Log.i("PAY: IndiaUpiSetupCoordinator/token stored");
                C120445gC r4 = this.A00;
                C1329668y r8 = r4.A03;
                synchronized (r8) {
                    try {
                        C18600si r10 = r8.A03;
                        String A04 = r10.A04();
                        long A00 = r8.A00.A00();
                        if (TextUtils.isEmpty(A04)) {
                            A05 = C117295Zj.A0a();
                        } else {
                            A05 = C13000ix.A05(A04);
                        }
                        A05.put("v", "2");
                        A05.put("token", A0I);
                        A05.put("tokenTs", A00);
                        C117295Zj.A1E(r10, A05);
                    } catch (JSONException e) {
                        Log.w("PAY: IndiaUpiPaymentSharedPrefs storeToken threw: ", e);
                    }
                }
                byte[] decode = Base64.decode(A0I, 0);
                AbstractC136436Mn r7 = r4.A04;
                if (r7 != null) {
                    r7.AR3(null, true);
                }
                C126615tA r11 = r4.A0A;
                String str2 = r4.A09;
                AbstractC14440lR r2 = r4.A08;
                C18590sh r82 = r4.A07;
                synchronized (C129315xW.class) {
                    String A01 = r82.A01();
                    try {
                        StringBuilder A0h = C12960it.A0h();
                        C117325Zm.A08("com.whatsapp", "|", str2, A0h);
                        A0h.append("|");
                        str = C117305Zk.A0n(AnonymousClass61J.A04(AnonymousClass61J.A02(C12960it.A0d(A01, A0h)), decode));
                    } catch (Exception e2) {
                        Log.e("PAY: IndiaUpiSetupCoordinator/registerApp threw: ", e2);
                        str = null;
                    }
                    if (str != null) {
                        boolean z = true;
                        AnonymousClass009.A0F(C12960it.A1T(TextUtils.isEmpty("com.whatsapp") ? 1 : 0));
                        AnonymousClass009.A0F(C12960it.A1T(TextUtils.isEmpty(str2) ? 1 : 0));
                        AnonymousClass009.A0F(C12960it.A1T(TextUtils.isEmpty(A01) ? 1 : 0));
                        if (TextUtils.isEmpty(str)) {
                            z = false;
                        }
                        AnonymousClass009.A0F(z);
                        C124165oi r6 = new C124165oi(r7, r82, str, str2, r11);
                        C129315xW.A0D = r6;
                        C12990iw.A1N(r6, r2);
                    } else {
                        Log.w("PAY: IndiaUpiSetupCoordinator/registerApp got null intent");
                        if (r7 != null) {
                            r7.AUn(false);
                        }
                    }
                }
                return;
            }
        } else {
            Log.e("PAY: IndiaUpiSetupCoordinator/token missing account node");
        }
        AbstractC136436Mn r0 = this.A00.A04;
        if (r0 != null) {
            r0.AR3(null, false);
        }
    }
}
