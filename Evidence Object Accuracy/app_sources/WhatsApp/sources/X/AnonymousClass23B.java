package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;

/* renamed from: X.23B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23B implements AnonymousClass1MF {
    public final int A00;
    public final C14900mE A01;

    @Override // X.AnonymousClass1MF
    public void AYA(C15370n3 r1) {
    }

    public /* synthetic */ AnonymousClass23B(C14900mE r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AnonymousClass1MF
    public void AR8(C15370n3 r4) {
        C14900mE r2 = this.A01;
        if (this.A00 != 1) {
            r2.A03();
        }
        r2.A0H(new RunnableBRunnable0Shape5S0100000_I0_5(this, 37));
    }
}
