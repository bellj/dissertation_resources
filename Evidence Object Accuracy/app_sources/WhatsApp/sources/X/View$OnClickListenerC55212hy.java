package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.gallerypicker.GalleryPickerFragment;

/* renamed from: X.2hy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnClickListenerC55212hy extends AnonymousClass03U implements View.OnClickListener {
    public AnonymousClass4TC A00;
    public final ImageView A01;
    public final ImageView A02;
    public final TextView A03;
    public final TextEmojiLabel A04;
    public final /* synthetic */ GalleryPickerFragment A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public View$OnClickListenerC55212hy(View view, GalleryPickerFragment galleryPickerFragment) {
        super(view);
        this.A05 = galleryPickerFragment;
        this.A04 = C12970iu.A0U(view, R.id.title);
        this.A01 = C12970iu.A0L(view, R.id.icon);
        this.A03 = C12960it.A0J(view, R.id.count);
        this.A02 = C12970iu.A0L(view, R.id.thumbnail);
        view.setOnClickListener(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0043, code lost:
        if (r5 == 6) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r5 >= 4) goto L_0x0015;
     */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r10) {
        /*
            r9 = this;
            X.4TC r6 = r9.A00
            com.whatsapp.gallerypicker.GalleryPickerFragment r0 = r9.A05
            X.00k r4 = r0.A0B()
            int r5 = r6.A02
            java.lang.String r2 = "bucketId"
            r0 = 9
            if (r5 == r0) goto L_0x00d1
            android.net.Uri r1 = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI
            r0 = 4
            if (r5 < r0) goto L_0x0023
        L_0x0015:
            android.net.Uri$Builder r1 = r1.buildUpon()
            java.lang.String r0 = r6.A04
            android.net.Uri$Builder r0 = r1.appendQueryParameter(r2, r0)
            android.net.Uri r1 = r0.build()
        L_0x0023:
            android.content.Intent r8 = r4.getIntent()
            android.content.Intent r3 = X.C12970iu.A0B(r1)
            java.lang.String r1 = r6.A05
            java.lang.String r0 = "window_title"
            r3.putExtra(r0, r1)
            r2 = 1
            if (r5 == 0) goto L_0x0046
            r1 = 4
            if (r5 == r2) goto L_0x00ce
            r0 = 2
            if (r5 == r0) goto L_0x0045
            if (r5 == r1) goto L_0x0046
            r0 = 5
            if (r5 == r0) goto L_0x00ce
            r0 = 6
            r2 = 7
            if (r5 != r0) goto L_0x0046
        L_0x0045:
            r2 = 2
        L_0x0046:
            int r0 = r6.A01
            r2 = r2 & r0
            java.lang.String r0 = "include_media"
            r3.putExtra(r0, r2)
            java.lang.String r1 = "preview"
            r7 = 1
            boolean r0 = r8.getBooleanExtra(r1, r7)
            r3.putExtra(r1, r0)
            java.lang.String r2 = "quoted_message_row_id"
            r0 = 0
            long r5 = r8.getLongExtra(r2, r0)
            r3.putExtra(r2, r5)
            java.lang.String r5 = "quoted_group_jid"
            java.lang.String r2 = r8.getStringExtra(r5)
            r3.putExtra(r5, r2)
            java.lang.String r5 = "jid"
            java.lang.String r2 = r8.getStringExtra(r5)
            r3.putExtra(r5, r2)
            r2 = 2147483647(0x7fffffff, float:NaN)
            java.lang.String r5 = "max_items"
            int r2 = r8.getIntExtra(r5, r2)
            r3.putExtra(r5, r2)
            java.lang.String r5 = "is_in_multi_select_mode_only"
            r6 = 0
            boolean r2 = r8.getBooleanExtra(r5, r6)
            r3.putExtra(r5, r2)
            java.lang.String r5 = "android.intent.extra.TEXT"
            java.lang.String r2 = r8.getStringExtra(r5)
            r3.putExtra(r5, r2)
            java.lang.String r5 = "mentions"
            java.util.ArrayList r2 = r8.getStringArrayListExtra(r5)
            r3.putExtra(r5, r2)
            java.lang.String r2 = "picker_open_time"
            long r0 = r8.getLongExtra(r2, r0)
            r3.putExtra(r2, r0)
            java.lang.String r1 = "origin"
            int r0 = r8.getIntExtra(r1, r7)
            r3.putExtra(r1, r0)
            boolean r0 = com.whatsapp.gallerypicker.GalleryPickerFragment.A0O
            if (r0 == 0) goto L_0x00c8
            java.lang.String r0 = "com.whatsapp.gallery.NewMediaPicker"
            r3.setClassName(r4, r0)
        L_0x00b8:
            X.01T[] r0 = new X.AnonymousClass01T[r6]
            X.08l r0 = X.C018108l.A02(r4, r0)
            android.os.Bundle r1 = r0.A03()
            r0 = 90
            r4.startActivityForResult(r3, r0, r1)
            return
        L_0x00c8:
            java.lang.Class<com.whatsapp.gallerypicker.MediaPicker> r0 = com.whatsapp.gallerypicker.MediaPicker.class
            r3.setClass(r4, r0)
            goto L_0x00b8
        L_0x00ce:
            r2 = 4
            goto L_0x0046
        L_0x00d1:
            android.net.Uri r1 = X.C617031s.A00
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: X.View$OnClickListenerC55212hy.onClick(android.view.View):void");
    }
}
