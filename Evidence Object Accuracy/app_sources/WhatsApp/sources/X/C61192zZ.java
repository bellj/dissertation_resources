package X;

import android.app.Activity;
import android.content.Context;
import com.whatsapp.conversationslist.ViewHolder;

/* renamed from: X.2zZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61192zZ extends AbstractC65103Id implements AnonymousClass5RT {
    public C92434Vw A00;
    public final Activity A01;
    public final Context A02;
    public final AnonymousClass1J1 A03;
    public final AbstractC33091dK A04;
    public final AnonymousClass018 A05;
    public final C15860o1 A06;

    public C61192zZ(Activity activity, Context context, C15570nT r29, C15450nH r30, AnonymousClass11L r31, C14650lo r32, C238013b r33, C15550nR r34, C15610nY r35, AnonymousClass1J1 r36, AbstractC33091dK r37, C63543Bz r38, ViewHolder viewHolder, C14830m7 r40, C16590pI r41, AnonymousClass018 r42, C19990v2 r43, C241714m r44, C14850m9 r45, C20710wC r46, AnonymousClass13H r47, C22710zW r48, C17070qD r49, AnonymousClass14X r50, C15860o1 r51, C92434Vw r52, AnonymousClass3J9 r53) {
        super(activity, context, r29, r30, r31, r32, r33, r34, r35, r37, r38, viewHolder, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r53);
        this.A02 = context;
        this.A01 = activity;
        this.A05 = r42;
        this.A03 = r36;
        this.A04 = r37;
        this.A06 = r51;
        this.A00 = r52;
    }
}
