package X;

import android.content.Context;

/* renamed from: X.5gZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120675gZ extends C120895gv {
    public final /* synthetic */ AnonymousClass5UT A00;
    public final /* synthetic */ C120555gN A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120675gZ(Context context, C14900mE r8, AnonymousClass5UT r9, C18650sn r10, C64513Fv r11, C120555gN r12) {
        super(context, r8, r10, r11, "upi-reject-collect");
        this.A01 = r12;
        this.A00 = r9;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r2) {
        super.A02(r2);
        this.A00.AUq(r2);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r2) {
        super.A03(r2);
        this.A00.AUq(r2);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r3) {
        super.A04(r3);
        this.A00.AUq(null);
    }
}
