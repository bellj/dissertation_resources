package X;

import java.io.EOFException;

/* renamed from: X.4Xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92864Xs {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public final C95304dT A05 = C95304dT.A05(255);
    public final int[] A06 = new int[255];

    public boolean A00(AnonymousClass5Yf r12, long j) {
        C95314dV.A03(C12960it.A1T((r12.AFo() > r12.AFf() ? 1 : (r12.AFo() == r12.AFf() ? 0 : -1))));
        C95304dT r8 = this.A05;
        r8.A0Q(4);
        while (true) {
            if (j != -1 && r12.AFo() + 4 >= j) {
                break;
            }
            try {
                if (!r12.AZ5(r8.A02, 0, 4, true)) {
                    break;
                }
                r8.A0S(0);
                if (r8.A0I() == 1332176723) {
                    r12.Aaj();
                    return true;
                }
                r12.Ae3(1);
            } catch (EOFException unused) {
            }
        }
        do {
            if (j != -1 && r12.AFo() >= j) {
                break;
            }
        } while (r12.Ae1(1) != -1);
        return false;
    }

    public boolean A01(AnonymousClass5Yf r13, boolean z) {
        this.A03 = 0;
        this.A04 = 0;
        this.A02 = 0;
        this.A01 = 0;
        this.A00 = 0;
        C95304dT r4 = this.A05;
        r4.A0Q(27);
        try {
            if (r13.AZ5(r4.A02, 0, 27, z) && r4.A0I() == 1332176723) {
                if (r4.A0C() == 0) {
                    this.A03 = r4.A0C();
                    byte[] bArr = r4.A02;
                    int i = r4.A01;
                    int i2 = i + 1;
                    r4.A01 = i2;
                    int i3 = i2 + 1;
                    r4.A01 = i3;
                    int i4 = i3 + 1;
                    r4.A01 = i4;
                    int i5 = i4 + 1;
                    r4.A01 = i5;
                    int i6 = i5 + 1;
                    r4.A01 = i6;
                    int i7 = i6 + 1;
                    r4.A01 = i7;
                    int i8 = i7 + 1;
                    r4.A01 = i8;
                    long j = (((long) bArr[i]) & 255) | ((((long) bArr[i2]) & 255) << 8) | ((((long) bArr[i3]) & 255) << 16) | ((((long) bArr[i4]) & 255) << 24) | ((((long) bArr[i5]) & 255) << 32) | ((((long) bArr[i6]) & 255) << 40) | ((((long) bArr[i7]) & 255) << 48);
                    r4.A01 = i8 + 1;
                    this.A04 = ((((long) bArr[i8]) & 255) << 56) | j;
                    r4.A0G();
                    r4.A0G();
                    r4.A0G();
                    int A0C = r4.A0C();
                    this.A02 = A0C;
                    this.A01 = A0C + 27;
                    r4.A0Q(A0C);
                    C95304dT.A06(r13, r4, A0C);
                    for (int i9 = 0; i9 < this.A02; i9++) {
                        int[] iArr = this.A06;
                        int A0C2 = r4.A0C();
                        iArr[i9] = A0C2;
                        this.A00 += A0C2;
                    }
                    return true;
                } else if (!z) {
                    throw AnonymousClass496.A00("unsupported bit stream revision");
                }
            }
        } catch (EOFException e) {
            if (!z) {
                throw e;
            }
        }
        return false;
    }
}
