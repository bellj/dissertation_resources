package X;

import android.content.SharedPreferences;
import android.os.CancellationSignal;
import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.jid.UserJid;
import com.whatsapp.migration.export.encryption.ExportEncryptionManager$KeyPrefetchWorker;
import com.whatsapp.util.Log;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.114  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass114 {
    public final C15570nT A00;
    public final C14830m7 A01;
    public final C19510uE A02;
    public final C15750nq A03;
    public final C15760nr A04;
    public final C21710xr A05;
    public final SecureRandom A06;

    public AnonymousClass114(C15570nT r1, C14830m7 r2, C19510uE r3, C15750nq r4, C15760nr r5, C21710xr r6, SecureRandom secureRandom) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A05 = r6;
        this.A06 = secureRandom;
        this.A02 = r3;
        this.A04 = r5;
    }

    public void A00() {
        C15570nT r0 = this.A00;
        r0.A08();
        C27631Ih r1 = r0.A05;
        if (r1 == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("ExportEncryptionManager/maybeScheduleGenerateEncryptionKey(); ");
            sb.append("skip scheduling, no user logged in");
            Log.i(sb.toString());
            return;
        }
        synchronized (this) {
            AnonymousClass2F8 A01 = this.A03.A01();
            if (A01 == null || !A02(r1, A01)) {
                C003901s r12 = new C003901s();
                r12.A01 = EnumC004001t.CONNECTED;
                C004101u r2 = new C004101u(r12);
                C004201x r13 = new C004201x(ExportEncryptionManager$KeyPrefetchWorker.class);
                r13.A00.A09 = r2;
                ((AnonymousClass022) this.A05.get()).A05(AnonymousClass023.KEEP, (AnonymousClass021) r13.A00(), "export-key-prefetch");
                return;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ExportEncryptionManager/maybeScheduleGenerateEncryptionKey(); ");
            sb2.append("skip scheduling, encryption key is already prefetched recently");
            Log.i(sb2.toString());
        }
    }

    public void A01(CancellationSignal cancellationSignal) {
        StringBuilder sb;
        String str;
        byte[] copyOf;
        C15570nT r2 = this.A00;
        r2.A08();
        C27631Ih r5 = r2.A05;
        if (r5 == null) {
            sb = new StringBuilder();
            sb.append("ExportEncryptionManager/maybeGenerateEncryptionKey(); ");
            str = "skipped key prefetching, no user is logged in";
        } else {
            System.currentTimeMillis();
            synchronized (this) {
                C15750nq r1 = this.A03;
                AnonymousClass2F8 A01 = r1.A01();
                if (A01 != null) {
                    if (A02(r5, A01)) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("ExportEncryptionManager/maybeGenerateEncryptionKey(); ");
                        sb2.append("skipped key prefetching, key is already prefetched recently");
                        Log.i(sb2.toString());
                        return;
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("ExportEncryptionManager/maybeGenerateEncryptionKey(); ");
                    sb3.append("reset prefetched key, a different user is now logged in or key is older");
                    Log.i(sb3.toString());
                    r1.A04();
                    this.A02.A04.A00.clear();
                }
                byte[] bArr = new byte[32];
                this.A06.nextBytes(bArr);
                String encodeToString = Base64.encodeToString(bArr, 2);
                C19510uE r11 = this.A02;
                C15570nT r13 = r11.A00;
                r13.A08();
                C27631Ih r14 = r13.A05;
                if (r14 != null) {
                    byte[] decode = Base64.decode(encodeToString, 2);
                    CountDownLatch countDownLatch = new CountDownLatch(1);
                    if (r11.A02.A01(new RunnableBRunnable0Shape4S0100000_I0_4(countDownLatch, 23), decode, new byte[16], 2)) {
                        try {
                            C19510uE.A00(cancellationSignal, countDownLatch);
                            if (countDownLatch.getCount() <= 0) {
                                r13.A08();
                                C27631Ih r0 = r13.A05;
                                if (r0 == null) {
                                    throw new AnonymousClass2GB(301, "User was logged out while waiting for encryption key.");
                                } else if (r0.equals(r14)) {
                                    ArrayList arrayList = new ArrayList();
                                    for (Map.Entry entry : new HashMap(r11.A04.A00).entrySet()) {
                                        C32771ci r7 = (C32771ci) entry.getKey();
                                        C32761ch r9 = (C32761ch) entry.getValue();
                                        if (Arrays.equals(r9.A01, decode)) {
                                            String str2 = r7.A00;
                                            byte[] bArr2 = r7.A01;
                                            if (bArr2 == null) {
                                                copyOf = null;
                                            } else {
                                                copyOf = Arrays.copyOf(bArr2, bArr2.length);
                                            }
                                            byte[] bArr3 = r9.A02;
                                            if (!(str2 == null || copyOf == null || bArr3 == null)) {
                                                arrayList.add(new AnonymousClass2F8(r14, str2, Base64.encodeToString(decode, 2), Base64.encodeToString(copyOf, 2), Base64.encodeToString(bArr3, 2), System.currentTimeMillis()));
                                            }
                                        }
                                    }
                                    Collections.sort(arrayList, new Comparator() { // from class: X.5CL
                                        @Override // java.util.Comparator
                                        public final int compare(Object obj, Object obj2) {
                                            return ((AnonymousClass2F8) obj).A04.compareTo(((AnonymousClass2F8) obj2).A04);
                                        }
                                    });
                                    if (!arrayList.isEmpty()) {
                                        AnonymousClass2F8 r6 = (AnonymousClass2F8) arrayList.get(0);
                                        r2.A08();
                                        C27631Ih r22 = r2.A05;
                                        if (r22 == null) {
                                            sb = new StringBuilder();
                                            sb.append("ExportEncryptionManager/maybeGenerateEncryptionKey(); ");
                                            str = "user logged out while waiting for encryption key";
                                        } else {
                                            synchronized (this) {
                                                AnonymousClass2F8 A012 = r1.A01();
                                                if (A012 == null || !A02(r22, A012)) {
                                                    StringBuilder sb4 = new StringBuilder();
                                                    sb4.append("ExportEncryptionManager/maybeGenerateEncryptionKey(); ");
                                                    sb4.append("prefetched key for current user:  user=");
                                                    UserJid userJid = r6.A01;
                                                    sb4.append(userJid.getRawString());
                                                    sb4.append(" version=");
                                                    String str3 = r6.A05;
                                                    sb4.append(str3);
                                                    sb4.append(", account_hash=");
                                                    String str4 = r6.A02;
                                                    sb4.append(str4);
                                                    sb4.append(", server_salt=");
                                                    String str5 = r6.A04;
                                                    sb4.append(str5);
                                                    sb4.append(", last_fetched_time=");
                                                    long j = r6.A00;
                                                    sb4.append(j);
                                                    Log.i(sb4.toString());
                                                    ((SharedPreferences) r1.A02.get()).edit().putString("/export/enc/prefetched/owner", userJid.getRawString()).putString("/export/enc/prefetched/version", str3).putString("/export/enc/prefetched/account_hash", str4).putString("/export/enc/prefetched/server_salt", str5).putLong("/export/enc/prefetched/last_fetch_time", j).putString("/export/enc/prefetched/seed", r6.A03).apply();
                                                } else {
                                                    StringBuilder sb5 = new StringBuilder();
                                                    sb5.append("ExportEncryptionManager/maybeGenerateEncryptionKey(); ");
                                                    sb5.append("concurrent conflict, encryption key was prefetched recently");
                                                    Log.i(sb5.toString());
                                                }
                                            }
                                            return;
                                        }
                                    } else {
                                        throw new AnonymousClass2GD(101, "Failed to create a key.");
                                    }
                                } else {
                                    throw new AnonymousClass2GB(301, "User changed while waiting for encryption key.");
                                }
                            } else {
                                throw new AnonymousClass2GD(103, "Failed to create a key, timed out.");
                            }
                        } catch (InterruptedException e) {
                            throw new AnonymousClass2GD("Failed to create a key, interrupted.", e);
                        }
                    } else {
                        throw new AnonymousClass2GD(102, "Not connected to server, cannot create keys.");
                    }
                } else {
                    throw new AnonymousClass2GB(301, "Cannot create encryption key when user is not logged in.");
                }
            }
        }
        sb.append(str);
        Log.i(sb.toString());
    }

    public boolean A02(UserJid userJid, AnonymousClass2F8 r10) {
        long abs = Math.abs(System.currentTimeMillis() - r10.A00);
        boolean z = false;
        if (userJid.equals(r10.A01)) {
            z = true;
        }
        boolean z2 = false;
        if (abs < 604800000) {
            z2 = true;
        }
        if (!z || !z2) {
            return false;
        }
        return true;
    }
}
