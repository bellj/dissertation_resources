package X;

import com.whatsapp.R;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/* renamed from: X.3d0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CallableC71483d0 implements Callable {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ C89024Ij A01;
    public final /* synthetic */ AnonymousClass3SS A02;
    public final /* synthetic */ C92554Wj A03;
    public final /* synthetic */ Map A04;

    public CallableC71483d0(C14260l7 r1, C89024Ij r2, AnonymousClass3SS r3, C92554Wj r4, Map map) {
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = map;
        this.A01 = r2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass4TT r7;
        C94244bU r8;
        C14260l7 r6 = this.A00;
        C92554Wj r5 = this.A03;
        Map map = this.A04;
        C89024Ij r0 = this.A01;
        if (r0 != null) {
            r7 = r0.A00;
        } else {
            r7 = null;
        }
        C1093551j r10 = C1093551j.A00;
        C90764Pd A01 = C65093Ic.A01(r6.A02.A01);
        r6.A02(R.id.bk_context_key_performance_logger);
        AnonymousClass4HX.A00.getAndIncrement();
        r10.A6N("Bloks Bind");
        if (map == null) {
            r8 = r5.A00;
        } else {
            C94244bU r4 = r5.A00;
            HashMap hashMap = new HashMap(r4.A03);
            hashMap.putAll(map);
            r8 = new C94244bU(r4.A02, r4.A01, hashMap, r4.A00);
        }
        AnonymousClass4TT A00 = AnonymousClass3G5.A00(AnonymousClass4Yz.A00, r6, r7, r8, r5.A01, r10, A01);
        C88824Hg.A00.incrementAndGet();
        r10.A9T();
        return C12990iw.A0L(A00.A01, new C89024Ij(A00));
    }
}
