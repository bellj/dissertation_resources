package X;

import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Build;

/* renamed from: X.0LC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0LC {
    public static Cursor A00(AnonymousClass0QN r3, AbstractC12390hq r4, boolean z) {
        int i;
        r3.A01();
        r3.A02();
        Cursor AZi = ((AnonymousClass0ZH) r3.A00).A00().A00().AZi(r4);
        MatrixCursor matrixCursor = AZi;
        if (z) {
            boolean z2 = AZi instanceof AbstractWindowedCursor;
            matrixCursor = AZi;
            if (z2) {
                AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor) AZi;
                int count = abstractWindowedCursor.getCount();
                if (abstractWindowedCursor.hasWindow()) {
                    i = abstractWindowedCursor.getWindow().getNumRows();
                } else {
                    i = count;
                }
                matrixCursor = AZi;
                if (Build.VERSION.SDK_INT < 23 || i < count) {
                    try {
                        MatrixCursor matrixCursor2 = new MatrixCursor(abstractWindowedCursor.getColumnNames(), abstractWindowedCursor.getCount());
                        while (abstractWindowedCursor.moveToNext()) {
                            Object[] objArr = new Object[abstractWindowedCursor.getColumnCount()];
                            for (int i2 = 0; i2 < abstractWindowedCursor.getColumnCount(); i2++) {
                                int type = abstractWindowedCursor.getType(i2);
                                if (type == 0) {
                                    objArr[i2] = null;
                                } else if (type == 1) {
                                    objArr[i2] = Long.valueOf(abstractWindowedCursor.getLong(i2));
                                } else if (type == 2) {
                                    objArr[i2] = Double.valueOf(abstractWindowedCursor.getDouble(i2));
                                } else if (type == 3) {
                                    objArr[i2] = abstractWindowedCursor.getString(i2);
                                } else if (type == 4) {
                                    objArr[i2] = abstractWindowedCursor.getBlob(i2);
                                } else {
                                    throw new IllegalStateException();
                                }
                            }
                            matrixCursor2.addRow(objArr);
                        }
                    } finally {
                        abstractWindowedCursor.close();
                    }
                }
            }
        }
        return matrixCursor;
    }
}
