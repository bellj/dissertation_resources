package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.AbstractCollection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1ZO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZO extends AnonymousClass1ZP {
    public static final Parcelable.Creator CREATOR = new C99784kr();
    public int A00;
    public long A01 = -1;
    public AnonymousClass1ZQ A02;
    public C30911Zh A03;
    public C30981Zo A04;
    public UserJid A05;
    public String A06;
    public boolean A07;

    public String A08() {
        return "";
    }

    public void A0A(String str) {
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZO() {
    }

    public AnonymousClass1ZO(Parcel parcel) {
        this.A05 = UserJid.getNullable(parcel.readString());
        this.A07 = parcel.readInt() != 1 ? false : true;
        this.A00 = parcel.readInt();
        this.A06 = parcel.readString();
        this.A01 = parcel.readLong();
        this.A02 = new AnonymousClass1ZQ(parcel);
        this.A04 = (C30981Zo) parcel.readParcelable(C30981Zo.class.getClassLoader());
        this.A03 = new C30911Zh(parcel);
    }

    public static int A00(String str) {
        if (str != null) {
            switch (str.hashCode()) {
                case 116014:
                    if (str.equals("upi")) {
                        return 3;
                    }
                    break;
                case 3387444:
                    if (str.equals("novi")) {
                        return 2;
                    }
                    break;
                case 97229420:
                    if (str.equals("fbpay")) {
                        return 1;
                    }
                    break;
            }
        }
        return 0;
    }

    public static int A01(String str) {
        switch (str.hashCode()) {
            case -1422950650:
                if (str.equals("active")) {
                    return 3;
                }
                break;
            case -309833220:
                if (str.equals("ineligible")) {
                    return 1;
                }
                break;
            case 100743639:
                if (str.equals("eligible")) {
                    return 2;
                }
                break;
        }
        return 0;
    }

    public static String A02(int i) {
        if (i == 1) {
            return "fbpay";
        }
        if (i == 2) {
            return "novi";
        }
        if (i == 3) {
            return "upi";
        }
        StringBuilder sb = new StringBuilder("PAY: getPaymentServiceEnumName/invalid service enum: ");
        sb.append(i);
        throw new AssertionError(sb.toString());
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        try {
            JSONObject jSONObject = new JSONObject();
            AnonymousClass1ZQ A07 = A07();
            HashMap hashMap = new HashMap();
            JSONObject jSONObject2 = new JSONObject();
            for (Map.Entry entry : A07.A01.entrySet()) {
                hashMap.put(A02(((Number) entry.getKey()).intValue()), entry.getValue());
                try {
                    jSONObject2.putOpt(A02(((Number) entry.getKey()).intValue()), entry.getValue());
                } catch (JSONException e) {
                    StringBuilder sb = new StringBuilder("PAY: ConsumerStatusData/getDataHashesDbString/exception: ");
                    sb.append(e);
                    Log.e(sb.toString());
                }
            }
            jSONObject.putOpt("consumer_status", new JSONObject(hashMap));
            C30981Zo r0 = this.A04;
            if (r0 == null) {
                r0 = new C30981Zo();
                this.A04 = r0;
            }
            JSONObject jSONObject3 = new JSONObject();
            try {
                HashMap hashMap2 = r0.A00;
                for (String str : hashMap2.keySet()) {
                    AbstractCollection abstractCollection = (AbstractCollection) hashMap2.get(str);
                    if (abstractCollection != null) {
                        JSONArray jSONArray = new JSONArray();
                        Iterator it = abstractCollection.iterator();
                        while (it.hasNext()) {
                            jSONArray.put(it.next());
                        }
                        jSONObject3.put(str, jSONArray);
                    }
                }
            } catch (JSONException unused) {
                Log.e("PAY: PaymentContactInfoCountryData/IncentiveTransactions/toJson/ failed to build json");
            }
            jSONObject.putOpt("incentive", jSONObject3);
            C30911Zh r9 = this.A03;
            if (r9 == null) {
                r9 = new C30911Zh();
                this.A03 = r9;
            }
            JSONObject jSONObject4 = new JSONObject();
            try {
                HashMap hashMap3 = new HashMap();
                JSONObject jSONObject5 = new JSONObject();
                for (Map.Entry entry2 : r9.A01.entrySet()) {
                    hashMap3.put(A02(((Number) entry2.getKey()).intValue()), entry2.getValue());
                    try {
                        jSONObject5.putOpt(A02(((Number) entry2.getKey()).intValue()), entry2.getValue());
                    } catch (JSONException e2) {
                        StringBuilder sb2 = new StringBuilder("PAY: EligibleOfferData/getOfferIdsForDbJson/exception: ");
                        sb2.append(e2);
                        Log.e(sb2.toString());
                    }
                }
                jSONObject4.putOpt("dhash", new JSONObject(hashMap3));
                HashMap hashMap4 = new HashMap();
                JSONObject jSONObject6 = new JSONObject();
                for (Map.Entry entry3 : r9.A00.entrySet()) {
                    hashMap4.put(A02(((Number) entry3.getKey()).intValue()), entry3.getValue());
                    try {
                        jSONObject6.putOpt(A02(((Number) entry3.getKey()).intValue()), entry3.getValue());
                    } catch (JSONException e3) {
                        StringBuilder sb3 = new StringBuilder("PAY: EligibleOfferData/getOfferIdsForDbJson/exception: ");
                        sb3.append(e3);
                        Log.e(sb3.toString());
                    }
                }
                jSONObject4.putOpt("offers", new JSONObject(hashMap4));
            } catch (JSONException e4) {
                StringBuilder sb4 = new StringBuilder("PAY: EligibleOfferData/getOfferIdsForDbJson/exception: ");
                sb4.append(e4);
                Log.e(sb4.toString());
            }
            jSONObject.putOpt("eligible_offers", jSONObject4);
            return jSONObject.toString();
        } catch (JSONException e5) {
            StringBuilder sb5 = new StringBuilder("PAY: PaymentContactInfoCountryData/toDBString/exception: ");
            sb5.append(e5);
            Log.e(sb5.toString());
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                AnonymousClass1ZQ A07 = A07();
                JSONObject optJSONObject = jSONObject.optJSONObject("consumer_status");
                if (optJSONObject != null) {
                    Iterator<String> keys = optJSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        int A00 = A00(next);
                        if (A00 != 0) {
                            A07.A01.put(Integer.valueOf(A00), optJSONObject.optString(next));
                        }
                    }
                }
                C30981Zo r9 = this.A04;
                if (r9 == null) {
                    r9 = new C30981Zo();
                    this.A04 = r9;
                }
                JSONObject optJSONObject2 = jSONObject.optJSONObject("incentive");
                if (optJSONObject2 != null) {
                    try {
                        Iterator<String> keys2 = optJSONObject2.keys();
                        while (keys2.hasNext()) {
                            String next2 = keys2.next();
                            JSONArray optJSONArray = optJSONObject2.optJSONArray(next2);
                            if (optJSONArray != null) {
                                HashSet hashSet = new HashSet();
                                for (int i = 0; i < optJSONArray.length(); i++) {
                                    hashSet.add(Long.valueOf(optJSONArray.getLong(i)));
                                }
                                r9.A00.put(next2, hashSet);
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("PAY: PaymentContactInfoCountryData/IncentiveTransactions/setIdTransactionMapfromJSON/failed to parse idJSON array", e);
                    }
                }
                C30911Zh r5 = this.A03;
                if (r5 == null) {
                    r5 = new C30911Zh();
                    this.A03 = r5;
                }
                JSONObject optJSONObject3 = jSONObject.optJSONObject("eligible_offers");
                if (optJSONObject3 != null) {
                    try {
                        JSONObject jSONObject2 = optJSONObject3.getJSONObject("dhash");
                        Iterator<String> keys3 = jSONObject2.keys();
                        while (keys3.hasNext()) {
                            String next3 = keys3.next();
                            int A002 = A00(next3);
                            if (A002 != 0) {
                                r5.A01.put(Integer.valueOf(A002), jSONObject2.optString(next3));
                            }
                        }
                        JSONObject jSONObject3 = optJSONObject3.getJSONObject("offers");
                        Iterator<String> keys4 = jSONObject3.keys();
                        while (keys4.hasNext()) {
                            String next4 = keys4.next();
                            int A003 = A00(next4);
                            if (A003 != 0) {
                                r5.A00.put(Integer.valueOf(A003), jSONObject3.optString(next4));
                            }
                        }
                    } catch (JSONException e2) {
                        StringBuilder sb = new StringBuilder("PAY: PaymentContactInfoCountryData/fromDbString/exception: ");
                        sb.append(e2);
                        Log.e(sb.toString());
                    }
                }
            } catch (JSONException e3) {
                StringBuilder sb2 = new StringBuilder("PAY: PaymentContactInfoCountryData/fromDbString/exception: ");
                sb2.append(e3);
                Log.e(sb2.toString());
            }
        }
    }

    public int A05() {
        return this.A00;
    }

    public int A06(int i) {
        return (int) ((A07().A00 >> (i << 2)) & 15);
    }

    public final AnonymousClass1ZQ A07() {
        AnonymousClass1ZQ r0 = this.A02;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass1ZQ r02 = new AnonymousClass1ZQ();
        this.A02 = r02;
        return r02;
    }

    public void A09(int i) {
        this.A00 = i;
    }

    public void A0B(boolean z) {
        this.A07 = z;
    }

    public boolean A0C() {
        return this.A07;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(C15380n4.A03(this.A05));
        parcel.writeInt(this.A07 ? 1 : 0);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A06);
        parcel.writeLong(this.A01);
        AnonymousClass1ZQ A07 = A07();
        parcel.writeLong(A07.A00);
        Map map = A07.A01;
        parcel.writeInt(map.size());
        for (Map.Entry entry : map.entrySet()) {
            parcel.writeInt(((Number) entry.getKey()).intValue());
            parcel.writeString((String) entry.getValue());
        }
        parcel.writeParcelable(this.A04, i);
        C30911Zh r3 = this.A03;
        if (r3 == null) {
            r3 = new C30911Zh();
            this.A03 = r3;
        }
        Map map2 = r3.A01;
        parcel.writeInt(map2.size());
        for (Map.Entry entry2 : map2.entrySet()) {
            parcel.writeInt(((Number) entry2.getKey()).intValue());
            parcel.writeString((String) entry2.getValue());
        }
        Map map3 = r3.A00;
        parcel.writeInt(map3.size());
        for (Map.Entry entry3 : map3.entrySet()) {
            parcel.writeInt(((Number) entry3.getKey()).intValue());
            parcel.writeString((String) entry3.getValue());
        }
    }
}
