package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.whatsapp.CodeInputField;

/* renamed from: X.2ea  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53522ea extends AnonymousClass04v {
    public final /* synthetic */ CodeInputField A00;
    public final /* synthetic */ String A01;

    public C53522ea(CodeInputField codeInputField, String str) {
        this.A00 = codeInputField;
        this.A01 = str;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        String A0s = C12980iv.A0s(this);
        AccessibilityNodeInfo accessibilityNodeInfo = r4.A02;
        accessibilityNodeInfo.setClassName(A0s);
        accessibilityNodeInfo.setContentDescription(this.A01);
    }
}
