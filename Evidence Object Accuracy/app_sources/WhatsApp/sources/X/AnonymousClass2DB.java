package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2DB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DB extends AbstractC28131Kt {
    public final UserJid A00;
    public final String A01;
    public final byte[] A02;

    public AnonymousClass2DB(UserJid userJid, C34021fS r2, String str, String str2, byte[] bArr) {
        super(r2, str);
        this.A00 = userJid;
        this.A01 = str2;
        this.A02 = bArr;
    }
}
