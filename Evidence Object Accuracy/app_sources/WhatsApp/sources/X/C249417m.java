package X;

import android.content.Context;
import android.database.ContentObserver;
import android.provider.ContactsContract;
import com.whatsapp.util.Log;

/* renamed from: X.17m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C249417m {
    public final ContentObserver A00;
    public final C15570nT A01;
    public final AnonymousClass116 A02;
    public volatile boolean A03;

    public C249417m(C15570nT r2, AnonymousClass116 r3, C20730wE r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = new AnonymousClass252(r2, this, r4);
    }

    public void A00(Context context) {
        if (!this.A03) {
            synchronized (this) {
                if (!this.A03 && this.A02.A00()) {
                    this.A01.A08();
                    Log.i("androidcontactscontentobserver/registered");
                    this.A03 = true;
                    context.getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, this.A00);
                }
            }
        }
    }
}
