package X;

/* renamed from: X.0nZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15620nZ {
    public final C15550nR A00;
    public final AnonymousClass13C A01;

    public C15620nZ(C15550nR r1, AnonymousClass13C r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public C15370n3 A00(C15430nF r3, String str) {
        try {
            C15370n3 A0A = this.A00.A0A(AbstractC14640lm.A00(this.A01.A02(r3, str)));
            if (A0A != null) {
                return A0A;
            }
            throw new SecurityException("Invalid contact ID");
        } catch (AnonymousClass1MW e) {
            throw new SecurityException(e);
        }
    }
}
