package X;

/* renamed from: X.0qn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17430qn extends AbstractC17440qo {
    public C17420qm A00;

    public C17430qn(C17420qm r4) {
        super("bk.action.UpdatedAvatar", "bk.action.DeletedAvatar", "bk.action.avatars.AsyncAvatarEditorLauncherClosed");
        this.A00 = r4;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r5, C1093651k r6, C14240l5 r7) {
        boolean booleanValue;
        C14230l4 r72 = (C14230l4) r7;
        String str = r6.A00;
        char c = 65535;
        switch (str.hashCode()) {
            case -327257951:
                if (str.equals("bk.action.UpdatedAvatar")) {
                    c = 0;
                    break;
                }
                break;
            case 963920255:
                if (str.equals("bk.action.DeletedAvatar")) {
                    c = 1;
                    break;
                }
                break;
            case 1731322695:
                if (str.equals("bk.action.avatars.AsyncAvatarEditorLauncherClosed")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                Boolean bool = (Boolean) r5.A00.get(1);
                C17420qm r0 = this.A00;
                AnonymousClass1AI.A00(r72);
                C20980wd r02 = r0.A00;
                if (bool == null) {
                    booleanValue = false;
                } else {
                    booleanValue = bool.booleanValue();
                }
                for (AbstractC17410ql r03 : r02.A01()) {
                    r03.AMl(booleanValue);
                }
                break;
            case 1:
                C17420qm r04 = this.A00;
                AnonymousClass1AI.A00(r72);
                for (AbstractC17410ql r05 : r04.A00.A01()) {
                    r05.AMj();
                }
                break;
            case 2:
                for (AbstractC17410ql r06 : this.A00.A00.A01()) {
                    r06.AMk();
                }
                break;
        }
        return null;
    }
}
