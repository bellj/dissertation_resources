package X;

import com.whatsapp.profile.ProfilePhotoReminder;

/* renamed from: X.52q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1096852q implements AbstractC116455Vm {
    public final /* synthetic */ ProfilePhotoReminder A00;

    public C1096852q(ProfilePhotoReminder profilePhotoReminder) {
        this.A00 = profilePhotoReminder;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A05);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A05, iArr, 25);
    }
}
