package X;

import android.os.Bundle;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.08u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C018708u implements AnonymousClass05A {
    public final /* synthetic */ AnonymousClass07E A00;

    public C018708u(AnonymousClass07E r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass05A
    public Bundle AbH() {
        AnonymousClass07E r4 = this.A00;
        for (Map.Entry entry : new HashMap(r4.A03).entrySet()) {
            r4.A04((String) entry.getKey(), ((AnonymousClass05A) entry.getValue()).AbH());
        }
        Map map = r4.A02;
        Set keySet = map.keySet();
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>(keySet.size());
        ArrayList<? extends Parcelable> arrayList2 = new ArrayList<>(arrayList.size());
        for (Object obj : keySet) {
            arrayList.add(obj);
            arrayList2.add(map.get(obj));
        }
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("keys", arrayList);
        bundle.putParcelableArrayList("values", arrayList2);
        return bundle;
    }
}
