package X;

/* renamed from: X.0cs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09420cs implements Runnable {
    public final /* synthetic */ C05100Og A00;

    public RunnableC09420cs(C05100Og r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        C05100Og r1 = this.A00;
        r1.A00();
        r1.A00 = null;
        r1.A01 = false;
    }
}
