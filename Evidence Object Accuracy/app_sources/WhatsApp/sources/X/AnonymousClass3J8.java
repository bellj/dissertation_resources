package X;

import android.app.Activity;
import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.3J8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3J8 {
    public static boolean A01(int i) {
        return i == 13 || i == 14;
    }

    public static void A00(SharedPreferences sharedPreferences, long j, long j2) {
        StringBuilder A0k = C12960it.A0k("AccountDefenceVerificationHelper/save-original-wait-time-diffs ");
        A0k.append(j);
        A0k.append(", ");
        A0k.append(j2);
        C12960it.A1F(A0k);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong("com.whatsapp.registration.VerifyPhoneNumber.account_defence_original_sms_wait_time", j);
        edit.putLong("com.whatsapp.registration.VerifyPhoneNumber.account_defence_original_voice_wait_time", j2);
        if (!edit.commit()) {
            Log.e("AccountDefenceVerificationHelper/save-original-wait-time-diffs/error");
        }
    }

    public CharSequence A02(Activity activity, String str, int i) {
        int i2;
        String A0X;
        Log.i(C12960it.A0W(i, "AccountDefenceVerificationHelper/getAccountDefenceTopDescription for state:"));
        if (i == 1 || i == 2) {
            i2 = R.string.account_defence_2nd_otp_verify_top_desc_sending_code;
        } else if (i != 3) {
            A0X = activity.getString(R.string.account_defence_sms_code_wait_description);
            return AnonymousClass23M.A08(new RunnableBRunnable0Shape16S0100000_I1_2(activity, 7), A0X, "learn-more");
        } else {
            i2 = R.string.account_defence_2nd_otp_verify_top_desc_code_sent;
        }
        A0X = C12960it.A0X(activity, str, new Object[1], 0, i2);
        return AnonymousClass23M.A08(new RunnableBRunnable0Shape16S0100000_I1_2(activity, 7), A0X, "learn-more");
    }
}
