package X;

import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.InteractiveAnnotation;
import com.whatsapp.SerializableLocation;
import com.whatsapp.SerializablePoint;
import com.whatsapp.util.Log;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1c7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32411c7 {
    public static final Field A00;
    public static final Field A01;

    static {
        try {
            Field declaredField = AbstractC27091Fz.class.getDeclaredField("unknownFields");
            A01 = declaredField;
            declaredField.setAccessible(true);
            Field declaredField2 = AnonymousClass256.class.getDeclaredField("count");
            A00 = declaredField2;
            declaredField2.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("BaseMessageUtil/isValidV2E2eMessage/error unknown-message-count, exception=", e);
            throw new AssertionError(e);
        }
    }

    public static int A0O(Object obj) {
        try {
            return A00.getInt(A01.get(obj));
        } catch (IllegalAccessException e) {
            Log.e("BaseMessageUtil/isValidV2E2eMessage/error unknown-message-count, exception=", e);
            throw new AssertionError(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001e, code lost:
        if (r0.equals(r7) == false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C43261wh A0P(X.C15570nT r17, X.C14850m9 r18, X.AnonymousClass1PG r19, X.AbstractC15340mz r20, byte[] r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 1031
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32411c7.A0P(X.0nT, X.0m9, X.1PG, X.0mz, byte[], boolean):X.1wh");
    }

    public static List A0Q(C16150oX r11) {
        int length;
        InteractiveAnnotation[] interactiveAnnotationArr = r11.A0V;
        if (interactiveAnnotationArr == null || (length = interactiveAnnotationArr.length) <= 0) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList(length);
        int i = 0;
        do {
            InteractiveAnnotation interactiveAnnotation = interactiveAnnotationArr[i];
            AnonymousClass1G4 A0T = C57262mk.A04.A0T();
            int i2 = 0;
            while (true) {
                SerializablePoint[] serializablePointArr = interactiveAnnotation.polygonVertices;
                if (i2 < serializablePointArr.length) {
                    if (serializablePointArr[i2] != null) {
                        AnonymousClass1G4 A0T2 = C57472n7.A05.A0T();
                        double d = interactiveAnnotation.polygonVertices[i2].x;
                        A0T2.A03();
                        C57472n7 r8 = (C57472n7) A0T2.A00;
                        r8.A02 |= 4;
                        r8.A00 = d;
                        double d2 = interactiveAnnotation.polygonVertices[i2].y;
                        A0T2.A03();
                        C57472n7 r82 = (C57472n7) A0T2.A00;
                        r82.A02 |= 8;
                        r82.A01 = d2;
                        AbstractC27091Fz A02 = A0T2.A02();
                        A0T.A03();
                        C57262mk r2 = (C57262mk) A0T.A00;
                        AnonymousClass1K6 r1 = r2.A02;
                        if (!((AnonymousClass1K7) r1).A00) {
                            r1 = AbstractC27091Fz.A0G(r1);
                            r2.A02 = r1;
                        }
                        r1.add(A02);
                    }
                    i2++;
                } else {
                    AnonymousClass1G4 A0T3 = C57272ml.A04.A0T();
                    double d3 = interactiveAnnotation.serializableLocation.latitude;
                    A0T3.A03();
                    C57272ml r10 = (C57272ml) A0T3.A00;
                    r10.A02 |= 1;
                    r10.A00 = d3;
                    double d4 = interactiveAnnotation.serializableLocation.longitude;
                    A0T3.A03();
                    C57272ml r102 = (C57272ml) A0T3.A00;
                    r102.A02 |= 2;
                    r102.A01 = d4;
                    String str = interactiveAnnotation.serializableLocation.name;
                    A0T3.A03();
                    C57272ml r12 = (C57272ml) A0T3.A00;
                    r12.A02 |= 4;
                    r12.A03 = str;
                    AbstractC27091Fz A022 = A0T3.A02();
                    A0T.A03();
                    C57262mk r13 = (C57262mk) A0T.A00;
                    r13.A03 = A022;
                    r13.A00 = 2;
                    arrayList.add(A0T.A02());
                    i++;
                }
            }
        } while (i < length);
        return arrayList;
    }

    public static void A0R(C16150oX r17, List list) {
        C57272ml r0;
        if (list.size() > 0) {
            ArrayList arrayList = new ArrayList();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C57262mk r8 = (C57262mk) it.next();
                SerializablePoint[] serializablePointArr = new SerializablePoint[r8.A02.size()];
                for (int i = 0; i < r8.A02.size(); i++) {
                    C57472n7 r7 = (C57472n7) r8.A02.get(i);
                    int i2 = r7.A02;
                    if ((i2 & 1) == 1 || (i2 & 2) == 2) {
                        Log.i("MessageUtils/buildE2eMessage/info contains deprecated point");
                        break;
                    } else {
                        serializablePointArr[i] = new SerializablePoint(r7.A00, r7.A01);
                    }
                }
                if (r8.A00 == 2) {
                    r0 = (C57272ml) r8.A03;
                } else {
                    r0 = C57272ml.A04;
                }
                arrayList.add(new InteractiveAnnotation(new SerializableLocation(r0.A03, r0.A00, r0.A01), serializablePointArr));
            }
            r17.A0V = (InteractiveAnnotation[]) arrayList.toArray(new InteractiveAnnotation[0]);
        }
    }

    @Deprecated
    public static void A0S(AbstractC15340mz r3, C39971qq r4) {
        byte[] bArr;
        if (r4.A06 || C30051Vw.A17(r3)) {
            if (r3 instanceof AbstractC16420oz) {
                ((AbstractC16420oz) r3).A6k(r4);
            } else {
                StringBuilder sb = new StringBuilder("MessageUtil/buildE2EMessage/error unrecognized media type during send, message.key=");
                sb.append(r3.A0z);
                sb.append("; media_wa_type=");
                byte b = r3.A0y;
                sb.append((int) b);
                sb.append("; media_wa_type=");
                sb.append((int) b);
                Log.w(sb.toString());
            }
            if (r3.A10() && !r4.A07 && (bArr = r3.A1D) != null) {
                AnonymousClass1G3 r42 = r4.A03;
                AnonymousClass2n6 r1 = ((C27081Fy) r42.A00).A0g;
                if (r1 == null) {
                    r1 = AnonymousClass2n6.A05;
                }
                AnonymousClass1G4 A0T = AnonymousClass2n6.A05.A0T();
                A0T.A04(r1);
                AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
                A0T.A03();
                AnonymousClass2n6 r12 = (AnonymousClass2n6) A0T.A00;
                r12.A00 |= 4;
                r12.A02 = A012;
                r42.A03();
                C27081Fy r2 = (C27081Fy) r42.A00;
                r2.A0g = (AnonymousClass2n6) A0T.A02();
                r2.A00 |= 67108864;
                return;
            }
            return;
        }
        throw new IllegalArgumentException("message not completely loaded");
    }

    public static boolean A0T(AnonymousClass1PG r3, AbstractC15340mz r4) {
        if (r3 != null) {
            AbstractC14640lm r1 = r4.A0z.A00;
            if (C15380n4.A0L(r1) || C15380n4.A0J(r1) || (C15380n4.A0G(r1) && C37381mH.A00(r4.A0C, 4) >= 0)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public static boolean A0U(AnonymousClass1PG r2, AbstractC15340mz r3, byte[] bArr) {
        if (r3.A0E() != null || r3.A12(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) || r3.A12(1) || ((!TextUtils.isEmpty(r3.A0e) && !TextUtils.isEmpty(r3.A0d)) || (!TextUtils.isEmpty(r3.A0j)) || r3.A0z() || A0T(r2, r3) || (bArr != null && C15380n4.A0G(r3.A0z.A00)))) {
            return true;
        }
        AbstractC455722e r0 = r3.A0J;
        if (r0 == null || !(r0 instanceof C455922g)) {
            return false;
        }
        return true;
    }
}
