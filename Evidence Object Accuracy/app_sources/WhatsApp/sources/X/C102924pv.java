package X;

import android.content.Context;
import com.whatsapp.community.AddGroupsToCommunityActivity;

/* renamed from: X.4pv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102924pv implements AbstractC009204q {
    public final /* synthetic */ AddGroupsToCommunityActivity A00;

    public C102924pv(AddGroupsToCommunityActivity addGroupsToCommunityActivity) {
        this.A00 = addGroupsToCommunityActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
