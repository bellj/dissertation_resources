package X;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.05Z  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05Z {
    public AnonymousClass06U A00;
    public final ArrayList A01 = new ArrayList();
    public final HashMap A02 = new HashMap();

    public AnonymousClass01E A00(String str) {
        AnonymousClass01E A0A;
        for (C06460Ts r0 : this.A02.values()) {
            if (!(r0 == null || (A0A = r0.A02.A0A(str)) == null)) {
                return A0A;
            }
        }
        return null;
    }

    public List A01() {
        ArrayList arrayList = new ArrayList();
        for (Object obj : this.A02.values()) {
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public List A02() {
        ArrayList arrayList;
        ArrayList arrayList2 = this.A01;
        if (arrayList2.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (arrayList2) {
            arrayList = new ArrayList(arrayList2);
        }
        return arrayList;
    }

    public void A03(AnonymousClass01E r3) {
        ArrayList arrayList = this.A01;
        if (!arrayList.contains(r3)) {
            synchronized (arrayList) {
                arrayList.add(r3);
            }
            r3.A0U = true;
            return;
        }
        StringBuilder sb = new StringBuilder("Fragment already added: ");
        sb.append(r3);
        throw new IllegalStateException(sb.toString());
    }

    public void A04(C06460Ts r4) {
        AnonymousClass01E r2 = r4.A02;
        String str = r2.A0T;
        HashMap hashMap = this.A02;
        if (hashMap.get(str) == null) {
            hashMap.put(r2.A0T, r4);
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb = new StringBuilder("Added fragment to active set ");
                sb.append(r2);
                Log.v("FragmentManager", sb.toString());
            }
        }
    }

    public void A05(C06460Ts r5) {
        AnonymousClass01E r3 = r5.A02;
        if (r3.A0i) {
            this.A00.A04(r3);
        }
        if (this.A02.put(r3.A0T, null) != null && AnonymousClass01F.A01(2)) {
            StringBuilder sb = new StringBuilder("Removed fragment from active set ");
            sb.append(r3);
            Log.v("FragmentManager", sb.toString());
        }
    }
}
