package X;

import java.util.Arrays;

/* renamed from: X.1Wq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30241Wq extends AbstractC30251Wr {
    public String A00;
    public String A01;

    public C30241Wq(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 33, j);
    }

    @Override // X.AbstractC15340mz
    public void A0Z(int i) {
        boolean z = false;
        if (i == 2) {
            z = true;
        }
        AnonymousClass009.A0B("FMessageBlankReply can only be quote message.", z);
        super.A0Z(i);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C30241Wq r5 = (C30241Wq) obj;
            if (!C29941Vi.A00(this.A00, r5.A00) || !C29941Vi.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01});
    }
}
