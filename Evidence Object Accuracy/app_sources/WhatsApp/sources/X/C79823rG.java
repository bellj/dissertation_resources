package X;

import android.os.IBinder;
import com.google.android.gms.maps.internal.IProjectionDelegate;

/* renamed from: X.3rG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79823rG extends C65873Li implements IProjectionDelegate {
    public C79823rG(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IProjectionDelegate");
    }
}
