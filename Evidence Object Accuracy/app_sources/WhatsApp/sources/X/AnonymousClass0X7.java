package X;

import android.view.ViewTreeObserver;
import android.widget.PopupWindow;

/* renamed from: X.0X7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X7 implements PopupWindow.OnDismissListener {
    public final /* synthetic */ ViewTreeObserver.OnGlobalLayoutListener A00;
    public final /* synthetic */ C02400Cd A01;

    public AnonymousClass0X7(ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener, C02400Cd r2) {
        this.A01 = r2;
        this.A00 = onGlobalLayoutListener;
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        ViewTreeObserver viewTreeObserver = this.A01.A04.getViewTreeObserver();
        if (viewTreeObserver != null) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.A00);
        }
    }
}
