package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/* renamed from: X.0lR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC14440lR {
    ThreadPoolExecutor A8a(String str, BlockingQueue blockingQueue, int i, int i2, int i3, long j);

    void AaP(Runnable runnable);

    void Aaz(AbstractC16350or v, Object... objArr);

    void Ab2(Runnable runnable);

    void Ab4(Runnable runnable, String str);

    void Ab5(AbstractC16350or v, Object... objArr);

    void Ab6(Runnable runnable);

    Runnable AbK(Runnable runnable, String str, long j);
}
