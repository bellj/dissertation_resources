package X;

import java.util.Map;

/* renamed from: X.0rE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17690rE extends AbstractC17700rF {
    public final Map A00;

    public C17690rE(Map map) {
        this.A00 = map;
        if (!map.containsKey(0)) {
            throw new IllegalArgumentException("Logging entry-point controller map must contain controller for NULL LoggingEntryPoint type");
        } else if (!map.containsKey(1)) {
            throw new IllegalArgumentException("Logging entry-point controller map must contain controller for CTWA_ADS LoggingEntryPoint type");
        }
    }
}
