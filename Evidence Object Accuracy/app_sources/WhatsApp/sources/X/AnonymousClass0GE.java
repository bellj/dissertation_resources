package X;

import android.os.Parcel;
import android.util.SparseIntArray;

/* renamed from: X.0GE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0GE extends AnonymousClass0QP {
    public int A00;
    public int A01;
    public int A02;
    public final int A03;
    public final int A04;
    public final Parcel A05;
    public final SparseIntArray A06;
    public final String A07;

    public AnonymousClass0GE(Parcel parcel) {
        this(parcel, new AnonymousClass00N(), new AnonymousClass00N(), new AnonymousClass00N(), "", parcel.dataPosition(), parcel.dataSize());
    }

    public AnonymousClass0GE(Parcel parcel, AnonymousClass00N r4, AnonymousClass00N r5, AnonymousClass00N r6, String str, int i, int i2) {
        super(r4, r5, r6);
        this.A06 = new SparseIntArray();
        this.A00 = -1;
        this.A02 = 0;
        this.A01 = -1;
        this.A05 = parcel;
        this.A04 = i;
        this.A03 = i2;
        this.A02 = i;
        this.A07 = str;
    }

    @Override // X.AnonymousClass0QP
    public AnonymousClass0QP A02() {
        Parcel parcel = this.A05;
        int dataPosition = parcel.dataPosition();
        int i = this.A02;
        if (i == this.A04) {
            i = this.A03;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.A07);
        sb.append("  ");
        return new AnonymousClass0GE(parcel, super.A01, super.A02, super.A00, sb.toString(), dataPosition, i);
    }

    @Override // X.AnonymousClass0QP
    public void A05(int i) {
        int i2 = this.A00;
        if (i2 >= 0) {
            int i3 = this.A06.get(i2);
            Parcel parcel = this.A05;
            int dataPosition = parcel.dataPosition();
            parcel.setDataPosition(i3);
            parcel.writeInt(dataPosition - i3);
            parcel.setDataPosition(dataPosition);
        }
        this.A00 = i;
        SparseIntArray sparseIntArray = this.A06;
        Parcel parcel2 = this.A05;
        sparseIntArray.put(i, parcel2.dataPosition());
        parcel2.writeInt(0);
        parcel2.writeInt(i);
    }

    @Override // X.AnonymousClass0QP
    public boolean A09(int i) {
        while (true) {
            int i2 = this.A02;
            int i3 = this.A03;
            int i4 = this.A01;
            if (i2 < i3) {
                if (i4 == i) {
                    return true;
                }
                if (String.valueOf(i4).compareTo(String.valueOf(i)) > 0) {
                    return false;
                }
                Parcel parcel = this.A05;
                parcel.setDataPosition(i2);
                int readInt = parcel.readInt();
                this.A01 = parcel.readInt();
                this.A02 += readInt;
            } else if (i4 != i) {
                return false;
            } else {
                return true;
            }
        }
    }
}
