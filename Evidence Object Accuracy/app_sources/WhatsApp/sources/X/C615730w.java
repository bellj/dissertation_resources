package X;

/* renamed from: X.30w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615730w extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;

    public C615730w() {
        super(2032, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(7, this.A02);
        r3.Abe(2, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(3, this.A00);
        r3.Abe(4, this.A05);
        r3.Abe(1, this.A01);
        r3.Abe(5, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamInlineVideoPlaybackClosed {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inlineVideoCancelBeforePlayStateT", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inlineVideoDurationT", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inlineVideoPlayStartT", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inlineVideoPlayed", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inlineVideoStallT", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inlineVideoType", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inlineVideoWatchT", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
