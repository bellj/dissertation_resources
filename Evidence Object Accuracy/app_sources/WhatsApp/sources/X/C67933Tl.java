package X;

/* renamed from: X.3Tl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67933Tl implements AnonymousClass5T5 {
    public final String A00;

    public C67933Tl(String str) {
        this.A00 = str;
    }

    @Override // X.AnonymousClass5T5
    public boolean Aej(AnonymousClass28D r8) {
        for (AnonymousClass28D r0 : r8.A0K()) {
            if (C87834De.A00(r0.A0H(), this.A00)) {
                return true;
            }
        }
        for (int i : C65093Ic.A02(r8)) {
            for (AnonymousClass28D r1 : r8.A0L(i)) {
                if (!(r1 == null || r1.A0H() == null || !r1.A0H().equals(this.A00))) {
                    return true;
                }
            }
        }
        for (int i2 : C65093Ic.A03(r8)) {
            AnonymousClass28D A0F = r8.A0F(i2);
            if (!(A0F == null || A0F.A0H() == null || !A0F.A0H().equals(this.A00))) {
                return true;
            }
        }
        return false;
    }
}
