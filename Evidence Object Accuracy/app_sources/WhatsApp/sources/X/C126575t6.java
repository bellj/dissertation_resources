package X;

/* renamed from: X.5t6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126575t6 {
    public final AnonymousClass1V8 A00;

    public C126575t6(AnonymousClass3CT r5, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-unblock-vpa");
        if (C117295Zj.A1X(str, false)) {
            C41141sy.A01(A0N, "vpa", str);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r5);
    }
}
