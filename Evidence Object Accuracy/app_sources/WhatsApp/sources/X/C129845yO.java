package X;

import android.hardware.Camera;
import java.util.AbstractCollection;
import java.util.List;

/* renamed from: X.5yO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129845yO {
    public final int A00;
    public final int A01;
    public final int A02;

    public C129845yO(int i, int i2) {
        this.A02 = i;
        this.A01 = i2;
        this.A00 = i * i2;
    }

    public static int A00(AbstractCollection abstractCollection, List list, int i) {
        Camera.Size size = (Camera.Size) list.get(i);
        abstractCollection.add(new C129845yO(size.width, size.height));
        return i + 1;
    }

    public static void A01(C129845yO r2, StringBuilder sb) {
        sb.append(r2.A02);
        sb.append('x');
        sb.append(r2.A01);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C129845yO)) {
            return false;
        }
        C129845yO r4 = (C129845yO) obj;
        if (this.A02 == r4.A02 && this.A01 == r4.A01) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = this.A01;
        int i2 = this.A02;
        return i ^ ((i2 >>> 16) | (i2 << 16));
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A02);
        A0h.append("x");
        return C12960it.A0f(A0h, this.A01);
    }
}
