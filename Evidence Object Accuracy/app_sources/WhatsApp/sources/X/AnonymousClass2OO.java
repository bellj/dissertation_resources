package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.2OO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OO extends Handler {
    public C29631Ua A00;
    public AnonymousClass1L5 A01;
    public boolean A02;
    public boolean A03;
    public final /* synthetic */ C20970wc A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2OO(Looper looper, C20970wc r2) {
        super(looper);
        this.A04 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:62:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r93) {
        /*
        // Method dump skipped, instructions count: 1039
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2OO.handleMessage(android.os.Message):void");
    }
}
