package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.09d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C019409d extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0Gj A00;

    public C019409d(AnonymousClass0Gj r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            this.A00.A06(context, intent);
        }
    }
}
