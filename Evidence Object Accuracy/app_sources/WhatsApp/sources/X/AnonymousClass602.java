package X;

import java.util.List;
import org.json.JSONArray;

/* renamed from: X.602  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass602 {
    public final List A00 = C12960it.A0l();
    public final List A01 = C12960it.A0l();
    public final List A02 = C12960it.A0l();
    public final List A03 = C12960it.A0l();

    public static final JSONArray A00(List list) {
        JSONArray A0L = C117315Zl.A0L();
        for (Object obj : list) {
            A0L.put(obj);
        }
        return A0L;
    }

    public List A01(String str) {
        switch (str.hashCode()) {
            case -1629586251:
                if (str.equals("withdrawal")) {
                    return this.A03;
                }
                break;
            case 934614152:
                if (str.equals("balance_top_up")) {
                    return this.A00;
                }
                break;
            case 1554454174:
                if (str.equals("deposit")) {
                    return this.A01;
                }
                break;
        }
        return this.A02;
    }
}
