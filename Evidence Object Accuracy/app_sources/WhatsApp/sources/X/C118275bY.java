package X;

import com.whatsapp.payments.ui.BrazilMerchantDetailsListActivity;

/* renamed from: X.5bY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118275bY extends AnonymousClass0Yo {
    public final /* synthetic */ BrazilMerchantDetailsListActivity A00;
    public final /* synthetic */ C128345vx A01;

    public C118275bY(BrazilMerchantDetailsListActivity brazilMerchantDetailsListActivity, C128345vx r2) {
        this.A01 = r2;
        this.A00 = brazilMerchantDetailsListActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118035bA.class)) {
            BrazilMerchantDetailsListActivity brazilMerchantDetailsListActivity = this.A00;
            C128345vx r0 = this.A01;
            C14830m7 r1 = r0.A0A;
            C14900mE r12 = r0.A01;
            AbstractC15710nm r13 = r0.A00;
            C15570nT r14 = r0.A02;
            C17220qS r15 = r0.A0J;
            C16590pI r152 = r0.A0B;
            AbstractC14440lR r142 = r0.A0e;
            C241414j r132 = r0.A0H;
            C18590sh r122 = r0.A0c;
            C17070qD r11 = r0.A0V;
            C15650ng r10 = r0.A0D;
            C1309660r r9 = r0.A0K;
            C18600si r8 = r0.A0Q;
            C18610sj r7 = r0.A0S;
            AnonymousClass102 r6 = r0.A0G;
            C20370ve r5 = r0.A0F;
            C18620sk r4 = r0.A0U;
            return new C118035bA(brazilMerchantDetailsListActivity, r13, r12, r14, r0.A08, r1, r152, r10, r5, r6, r132, r15, r9, r0.A0O, r0.A0P, r8, r7, r4, r11, r0.A0X, r122, r142);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
