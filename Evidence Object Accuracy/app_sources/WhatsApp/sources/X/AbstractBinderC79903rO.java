package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3rO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC79903rO extends AbstractBinderC73293fz implements AnonymousClass5YC {
    public static AnonymousClass5YC A00(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.IMarkerDelegate");
        if (queryLocalInterface instanceof AnonymousClass5YC) {
            return (AnonymousClass5YC) queryLocalInterface;
        }
        return new C79783rC(iBinder);
    }
}
