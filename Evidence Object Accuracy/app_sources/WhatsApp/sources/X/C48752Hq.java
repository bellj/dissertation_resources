package X;

import android.os.Looper;
import java.util.Iterator;

/* renamed from: X.2Hq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48752Hq extends AnonymousClass015 {
    public final AbstractC48712Hi A00;

    public C48752Hq(AbstractC48712Hi r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C48802Hv r2 = (C48802Hv) ((C48722Hj) ((AbstractC48712Hi) AnonymousClass027.A00(AbstractC48712Hi.class, this.A00))).A02.get();
        Thread thread = C48812Hw.A00;
        if (thread == null) {
            thread = Looper.getMainLooper().getThread();
            C48812Hw.A00 = thread;
        }
        if (Thread.currentThread() == thread) {
            Iterator it = r2.A00.iterator();
            if (it.hasNext()) {
                it.next();
                throw new NullPointerException("onCleared");
            }
            return;
        }
        throw new IllegalStateException("Must be called on the Main thread.");
    }
}
