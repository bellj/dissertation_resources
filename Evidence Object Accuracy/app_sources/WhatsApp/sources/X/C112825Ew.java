package X;

import java.util.Iterator;

/* renamed from: X.5Ew  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112825Ew implements AnonymousClass1WO {
    public final /* synthetic */ Iterator A00;

    public C112825Ew(Iterator it) {
        this.A00 = it;
    }

    @Override // X.AnonymousClass1WO
    public Iterator iterator() {
        return this.A00;
    }
}
