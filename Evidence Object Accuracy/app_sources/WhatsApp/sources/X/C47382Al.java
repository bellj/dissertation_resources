package X;

import android.database.ContentObserver;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.mediaview.MediaViewBaseFragment;

/* renamed from: X.2Al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47382Al extends ContentObserver {
    public final /* synthetic */ AnonymousClass2Am A00;

    @Override // android.database.ContentObserver
    public boolean deliverSelfNotifications() {
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C47382Al(AnonymousClass2Am r2) {
        super(null);
        this.A00 = r2;
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z) {
        super.onChange(z);
        ((MediaViewBaseFragment) this.A00.A07).A09.post(new RunnableBRunnable0Shape8S0100000_I0_8(this, 12));
    }
}
