package X;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/* renamed from: X.5Mw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114735Mw extends AnonymousClass1TM {
    public static final Boolean A06 = new Boolean(false);
    public static final Boolean A07 = new Boolean(true);
    public static final Hashtable A08;
    public static final Hashtable A09;
    public static final Hashtable A0A;
    public static final Hashtable A0B;
    public static final Hashtable A0C;
    public static final Hashtable A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;
    public static final AnonymousClass1TK A0K;
    public static final AnonymousClass1TK A0L = C72453ed.A12("2.5.4.54");
    public static final AnonymousClass1TK A0M;
    public static final AnonymousClass1TK A0N;
    public static final AnonymousClass1TK A0O;
    public static final AnonymousClass1TK A0P;
    public static final AnonymousClass1TK A0Q;
    public static final AnonymousClass1TK A0R;
    public static final AnonymousClass1TK A0S;
    public static final AnonymousClass1TK A0T;
    public static final AnonymousClass1TK A0U;
    public static final AnonymousClass1TK A0V;
    public static final AnonymousClass1TK A0W;
    public static final AnonymousClass1TK A0X;
    public static final AnonymousClass1TK A0Y;
    public static final AnonymousClass1TK A0Z;
    public static final AnonymousClass1TK A0a;
    public static final AnonymousClass1TK A0b;
    public static final AnonymousClass1TK A0c;
    public static final AnonymousClass1TK A0d;
    public static final AnonymousClass1TK A0e;
    public static final AnonymousClass1TK A0f;
    public static final AnonymousClass1TK A0g;
    public static final AnonymousClass1TK A0h;
    public static final AnonymousClass1TK A0i;
    public static final AnonymousClass1TK A0j;
    public static final AnonymousClass1TK A0k;
    public static final AnonymousClass1TK A0l;
    public static final AnonymousClass1TK A0m;
    public int A00;
    public Vector A01 = new Vector();
    public Vector A02 = new Vector();
    public Vector A03 = new Vector();
    public AbstractC114775Na A04;
    public boolean A05;

    public C114735Mw() {
    }

    public static final String A00(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        if (length != 0) {
            char charAt = str.charAt(0);
            stringBuffer.append(charAt);
            int i = 1;
            while (i < length) {
                char charAt2 = str.charAt(i);
                if (charAt != ' ' || charAt2 != ' ') {
                    stringBuffer.append(charAt2);
                }
                i++;
                charAt = charAt2;
            }
        }
        return stringBuffer.toString();
    }

    @Override // X.AnonymousClass1TM
    public int hashCode() {
        if (!this.A05) {
            this.A05 = true;
            int i = 0;
            while (true) {
                Vector vector = this.A02;
                if (i == vector.size()) {
                    break;
                }
                String A00 = A00(A03((String) this.A03.elementAt(i)));
                int hashCode = this.A00 ^ vector.elementAt(i).hashCode();
                this.A00 = hashCode;
                this.A00 = A00.hashCode() ^ hashCode;
                i++;
            }
        }
        return this.A00;
    }

    static {
        AnonymousClass1TK A12 = C72453ed.A12("2.5.4.6");
        A0F = A12;
        AnonymousClass1TK A122 = C72453ed.A12("2.5.4.10");
        A0W = A122;
        AnonymousClass1TK A123 = C72453ed.A12("2.5.4.11");
        A0X = A123;
        AnonymousClass1TK A124 = C72453ed.A12("2.5.4.12");
        A0h = A124;
        AnonymousClass1TK A125 = C72453ed.A12("2.5.4.3");
        A0G = A125;
        AnonymousClass1TK A126 = C72453ed.A12("2.5.4.5");
        A0d = A126;
        AnonymousClass1TK A127 = C72453ed.A12("2.5.4.9");
        A0f = A127;
        A0c = A126;
        AnonymousClass1TK A128 = C72453ed.A12("2.5.4.7");
        A0T = A128;
        AnonymousClass1TK A129 = C72453ed.A12("2.5.4.8");
        A0e = A129;
        AnonymousClass1TK A1210 = C72453ed.A12("2.5.4.4");
        A0g = A1210;
        AnonymousClass1TK A1211 = C72453ed.A12("2.5.4.42");
        A0R = A1211;
        AnonymousClass1TK A1212 = C72453ed.A12("2.5.4.43");
        A0S = A1212;
        AnonymousClass1TK A1213 = C72453ed.A12("2.5.4.44");
        A0Q = A1213;
        AnonymousClass1TK A1214 = C72453ed.A12("2.5.4.45");
        A0k = A1214;
        AnonymousClass1TK A1215 = C72453ed.A12("2.5.4.15");
        A0E = A1215;
        AnonymousClass1TK A1216 = C72453ed.A12("2.5.4.17");
        A0a = A1216;
        AnonymousClass1TK A1217 = C72453ed.A12("2.5.4.46");
        A0M = A1217;
        AnonymousClass1TK A1218 = C72453ed.A12("2.5.4.65");
        A0b = A1218;
        AnonymousClass1TK A1219 = C72453ed.A12("1.3.6.1.5.5.7.9.1");
        A0J = A1219;
        AnonymousClass1TK A1220 = C72453ed.A12("1.3.6.1.5.5.7.9.2");
        A0Y = A1220;
        AnonymousClass1TK A1221 = C72453ed.A12("1.3.6.1.5.5.7.9.3");
        A0P = A1221;
        AnonymousClass1TK A1222 = C72453ed.A12("1.3.6.1.5.5.7.9.4");
        A0H = A1222;
        AnonymousClass1TK A1223 = C72453ed.A12("1.3.6.1.5.5.7.9.5");
        A0I = A1223;
        AnonymousClass1TK A1224 = C72453ed.A12("1.3.36.8.3.14");
        A0V = A1224;
        AnonymousClass1TK A1225 = C72453ed.A12("2.5.4.16");
        A0Z = A1225;
        AnonymousClass1TK r29 = AbstractC116975Xq.A09;
        A0i = r29;
        AnonymousClass1TK r28 = AbstractC116975Xq.A07;
        A0U = r28;
        AnonymousClass1TK r4 = AnonymousClass1TJ.A1u;
        A0O = r4;
        AnonymousClass1TK r27 = AnonymousClass1TJ.A24;
        A0m = r27;
        AnonymousClass1TK r26 = AnonymousClass1TJ.A23;
        A0l = r26;
        A0N = r4;
        AnonymousClass1TK A1226 = C72453ed.A12("0.9.2342.19200300.100.1.25");
        A0K = A1226;
        AnonymousClass1TK A1227 = C72453ed.A12("0.9.2342.19200300.100.1.1");
        A0j = A1227;
        Hashtable hashtable = new Hashtable();
        A09 = hashtable;
        Hashtable hashtable2 = new Hashtable();
        A0C = hashtable2;
        Hashtable hashtable3 = new Hashtable();
        A0B = hashtable3;
        Hashtable hashtable4 = new Hashtable();
        A08 = hashtable4;
        A0A = hashtable;
        A0D = hashtable4;
        hashtable.put(A12, "C");
        hashtable.put(A122, "O");
        hashtable.put(A124, "T");
        hashtable.put(A123, "OU");
        hashtable.put(A125, "CN");
        hashtable.put(A128, "L");
        hashtable.put(A129, "ST");
        hashtable.put(A126, "SERIALNUMBER");
        hashtable.put(r4, "E");
        hashtable.put(A1226, "DC");
        hashtable.put(A1227, "UID");
        hashtable.put(A127, "STREET");
        hashtable.put(A1210, "SURNAME");
        hashtable.put(A1211, "GIVENNAME");
        hashtable.put(A1212, "INITIALS");
        hashtable.put(A1213, "GENERATION");
        hashtable.put(r26, "unstructuredAddress");
        hashtable.put(r27, "unstructuredName");
        hashtable.put(A1214, "UniqueIdentifier");
        hashtable.put(A1217, "DN");
        hashtable.put(A1218, "Pseudonym");
        hashtable.put(A1225, "PostalAddress");
        hashtable.put(A1224, "NameAtBirth");
        hashtable.put(A1222, "CountryOfCitizenship");
        hashtable.put(A1223, "CountryOfResidence");
        hashtable.put(A1221, "Gender");
        hashtable.put(A1220, "PlaceOfBirth");
        hashtable.put(A1219, "DateOfBirth");
        hashtable.put(A1216, "PostalCode");
        hashtable.put(A1215, "BusinessCategory");
        hashtable.put(r29, "TelephoneNumber");
        hashtable.put(r28, "Name");
        hashtable2.put(A12, "C");
        hashtable2.put(A122, "O");
        hashtable2.put(A123, "OU");
        hashtable2.put(A125, "CN");
        hashtable2.put(A128, "L");
        hashtable2.put(A129, "ST");
        hashtable2.put(A127, "STREET");
        hashtable2.put(A1226, "DC");
        hashtable2.put(A1227, "UID");
        hashtable3.put(A12, "C");
        hashtable3.put(A122, "O");
        hashtable3.put(A123, "OU");
        hashtable3.put(A125, "CN");
        hashtable3.put(A128, "L");
        hashtable3.put(A129, "ST");
        hashtable3.put(A127, "STREET");
        hashtable4.put("c", A12);
        hashtable4.put("o", A122);
        hashtable4.put("t", A124);
        hashtable4.put("ou", A123);
        hashtable4.put("cn", A125);
        hashtable4.put("l", A128);
        hashtable4.put("st", A129);
        hashtable4.put("sn", A126);
        hashtable4.put("serialnumber", A126);
        hashtable4.put("street", A127);
        hashtable4.put("emailaddress", r4);
        hashtable4.put("dc", A1226);
        hashtable4.put("e", r4);
        hashtable4.put("uid", A1227);
        hashtable4.put("surname", A1210);
        hashtable4.put("givenname", A1211);
        hashtable4.put("initials", A1212);
        hashtable4.put("generation", A1213);
        hashtable4.put("unstructuredaddress", r26);
        hashtable4.put("unstructuredname", r27);
        hashtable4.put("uniqueidentifier", A1214);
        hashtable4.put("dn", A1217);
        hashtable4.put("pseudonym", A1218);
        hashtable4.put("postaladdress", A1225);
        hashtable4.put("nameofbirth", A1224);
        hashtable4.put("countryofcitizenship", A1222);
        hashtable4.put("countryofresidence", A1223);
        hashtable4.put("gender", A1221);
        hashtable4.put("placeofbirth", A1220);
        hashtable4.put("dateofbirth", A1219);
        hashtable4.put("postalcode", A1216);
        hashtable4.put("businesscategory", A1215);
        hashtable4.put("telephonenumber", r29);
        hashtable4.put("name", r28);
    }

    public C114735Mw(AbstractC114775Na r12) {
        Vector vector;
        this.A04 = r12;
        Enumeration A0C2 = r12.A0C();
        while (A0C2.hasMoreElements()) {
            AnonymousClass5NV A00 = AnonymousClass5NV.A00(((AnonymousClass1TN) A0C2.nextElement()).Aer());
            int i = 0;
            while (true) {
                AnonymousClass1TN[] r1 = A00.A01;
                if (i < r1.length) {
                    AbstractC114775Na A04 = AbstractC114775Na.A04((Object) C72463ee.A0N(r1, i));
                    if (A04.A0B() == 2) {
                        this.A02.addElement(AnonymousClass1TK.A00(A04.A0D(0)));
                        AnonymousClass1TN A01 = AbstractC114775Na.A01(A04);
                        if (!(A01 instanceof AnonymousClass5VP) || (A01 instanceof AnonymousClass5NR)) {
                            try {
                                Vector vector2 = this.A03;
                                StringBuilder A0h2 = C12960it.A0h();
                                A0h2.append("#");
                                byte[] A0c2 = C72463ee.A0c(A01);
                                byte[] A02 = C95374db.A02(A0c2, 0, A0c2.length);
                                int length = A02.length;
                                char[] cArr = new char[length];
                                for (int i2 = 0; i2 != length; i2++) {
                                    cArr[i2] = (char) (A02[i2] & 255);
                                }
                                vector2.addElement(C12960it.A0d(new String(cArr), A0h2));
                            } catch (IOException unused) {
                                throw C12970iu.A0f("cannot encode value");
                            }
                        } else {
                            String AGy = ((AnonymousClass5VP) A01).AGy();
                            if (AGy.length() <= 0 || AGy.charAt(0) != '#') {
                                vector = this.A03;
                            } else {
                                vector = this.A03;
                                AGy = C12960it.A0d(AGy, C12960it.A0k("\\"));
                            }
                            vector.addElement(AGy);
                        }
                        this.A01.addElement(i != 0 ? A07 : A06);
                        i++;
                    } else {
                        throw C12970iu.A0f("badly sized pair");
                    }
                }
            }
        }
    }

    public static final void A01(String str, StringBuffer stringBuffer, Hashtable hashtable, AnonymousClass1TK r9) {
        String str2 = (String) hashtable.get(r9);
        if (str2 == null) {
            str2 = r9.A01;
        }
        stringBuffer.append(str2);
        stringBuffer.append('=');
        int length = stringBuffer.length();
        stringBuffer.append(str);
        int length2 = stringBuffer.length();
        if (str.length() >= 2 && str.charAt(0) == '\\' && str.charAt(1) == '#') {
            length += 2;
        }
        while (length < length2 && stringBuffer.charAt(length) == ' ') {
            stringBuffer.insert(length, "\\");
            length += 2;
            length2++;
        }
        while (true) {
            length2--;
            if (length2 <= length || stringBuffer.charAt(length2) != ' ') {
                break;
            }
            stringBuffer.insert(length2, '\\');
        }
        while (length <= length2) {
            char charAt = stringBuffer.charAt(length);
            if (!(charAt == '\"' || charAt == '\\' || charAt == '+' || charAt == ',')) {
                switch (charAt) {
                    case ';':
                    case '<':
                    case '=':
                    case '>':
                        break;
                    default:
                        length++;
                        continue;
                }
            }
            stringBuffer.insert(length, "\\");
            length += 2;
            length2++;
        }
    }

    public final String A03(String str) {
        String A00 = AnonymousClass1T7.A00(str.trim());
        int length = A00.length();
        if (length <= 0 || A00.charAt(0) != '#') {
            return A00;
        }
        try {
            AnonymousClass1TL A03 = AnonymousClass1TL.A03(C95374db.A01(A00, length - 1));
            return A03 instanceof AnonymousClass5VP ? AnonymousClass1T7.A00(((AnonymousClass5VP) A03).AGy().trim()) : A00;
        } catch (IOException e) {
            throw C12960it.A0U(C12960it.A0b("unknown encoding in name: ", e));
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        AbstractC114775Na r0 = this.A04;
        if (r0 != null) {
            return r0;
        }
        C94954co r4 = new C94954co(10);
        C94954co r1 = new C94954co(10);
        Vector vector = this.A02;
        if (0 != vector.size()) {
            C94954co.A00().A06((AnonymousClass1TM) vector.elementAt(0));
            this.A03.elementAt(0);
            throw C12980iv.A0n("getConvertedValue");
        }
        AnonymousClass5NZ A01 = C94954co.A01(new C114785Nb(r1), r4);
        this.A04 = A01;
        return A01;
    }

    @Override // X.AnonymousClass1TM
    public boolean equals(Object obj) {
        C114735Mw r13;
        Object obj2 = obj;
        if (obj2 != this) {
            boolean z = obj2 instanceof C114735Mw;
            if (z || (obj2 instanceof AbstractC114775Na)) {
                if (!Aer().A04(((AnonymousClass1TN) obj2).Aer())) {
                    try {
                        if (z) {
                            r13 = (C114735Mw) obj2;
                        } else {
                            if (obj2 instanceof AnonymousClass5N2) {
                                obj2 = ((AnonymousClass5N2) obj2).A01;
                            } else if (obj == null) {
                                r13 = null;
                            }
                            r13 = new C114735Mw(AbstractC114775Na.A04(obj2));
                        }
                        Vector vector = this.A02;
                        int size = vector.size();
                        Vector vector2 = r13.A02;
                        if (size == vector2.size()) {
                            boolean[] zArr = new boolean[size];
                            int i = -1;
                            int i2 = size - 1;
                            int i3 = -1;
                            if (vector.elementAt(0).equals(vector2.elementAt(0))) {
                                i = size;
                                i2 = 0;
                                i3 = 1;
                            }
                            while (i2 != i) {
                                AnonymousClass1TL r4 = (AnonymousClass1TL) vector.elementAt(i2);
                                String str = (String) this.A03.elementAt(i2);
                                for (int i4 = 0; i4 < size; i4++) {
                                    if (!zArr[i4] && r4.A04((AnonymousClass1TL) vector2.elementAt(i4))) {
                                        String A03 = A03(str);
                                        String A032 = A03((String) r13.A03.elementAt(i4));
                                        if (A03.equals(A032) || A00(A03).equals(A00(A032))) {
                                            zArr[i4] = true;
                                            i2 += i3;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (IllegalArgumentException unused) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public String toString() {
        Hashtable hashtable = A09;
        StringBuffer stringBuffer = new StringBuffer();
        Vector vector = new Vector();
        StringBuffer stringBuffer2 = null;
        int i = 0;
        while (true) {
            Vector vector2 = this.A02;
            if (i >= vector2.size()) {
                break;
            }
            if (C12970iu.A1Y(this.A01.elementAt(i))) {
                stringBuffer2.append('+');
                A01((String) this.A03.elementAt(i), stringBuffer2, hashtable, (AnonymousClass1TK) vector2.elementAt(i));
            } else {
                stringBuffer2 = new StringBuffer();
                A01((String) this.A03.elementAt(i), stringBuffer2, hashtable, (AnonymousClass1TK) vector2.elementAt(i));
                vector.addElement(stringBuffer2);
            }
            i++;
        }
        boolean z = true;
        for (int i2 = 0; i2 < vector.size(); i2++) {
            if (z) {
                z = false;
            } else {
                stringBuffer.append(',');
            }
            stringBuffer.append(vector.elementAt(i2).toString());
        }
        return stringBuffer.toString();
    }
}
