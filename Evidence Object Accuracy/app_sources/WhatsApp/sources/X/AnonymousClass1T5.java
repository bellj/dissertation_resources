package X;

import java.security.Permission;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1T5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1T5 {
    public static Permission A06 = new AnonymousClass1T6("additionalEcParameters");
    public static Permission A07 = new AnonymousClass1T6("threadLocalDhDefaultParams");
    public static Permission A08 = new AnonymousClass1T6("DhDefaultParams");
    public static Permission A09 = new AnonymousClass1T6("acceptableEcCurves");
    public static Permission A0A = new AnonymousClass1T6("threadLocalEcImplicitlyCa");
    public static Permission A0B = new AnonymousClass1T6("ecImplicitlyCa");
    public ThreadLocal A00 = new ThreadLocal();
    public ThreadLocal A01 = new ThreadLocal();
    public volatile Object A02;
    public volatile Map A03 = new HashMap();
    public volatile Set A04 = new HashSet();
    public volatile AnonymousClass5C1 A05;
}
