package X;

import android.util.Pair;
import android.widget.SectionIndexer;
import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.List;

/* renamed from: X.2xC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xC extends C36591kA implements SectionIndexer {
    public List A00 = C12960it.A0l();
    public List A01 = C12960it.A0l();

    public AnonymousClass2xC(ContactPickerFragment contactPickerFragment, AnonymousClass01H r3) {
        super(contactPickerFragment, r3);
    }

    @Override // android.widget.SectionIndexer
    public int getPositionForSection(int i) {
        List list = this.A00;
        if (i >= list.size() || i < 0) {
            return -1;
        }
        return C12960it.A05(list.get(i));
    }

    @Override // android.widget.SectionIndexer
    public int getSectionForPosition(int i) {
        List list = this.A02;
        List list2 = this.A01;
        List list3 = this.A00;
        if (i < 0) {
            return 0;
        }
        if (i >= list.size()) {
            return C12980iv.A0C(list2);
        }
        int size = list3.size();
        do {
            size--;
            if (size < 0) {
                return 0;
            }
        } while (C12960it.A05(list3.get(size)) > i);
        return size;
    }

    @Override // android.widget.SectionIndexer
    public Object[] getSections() {
        return this.A01.toArray(C13000ix.A08());
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        Pair A00 = AnonymousClass3AW.A00(this.A03.A16, this.A02);
        this.A01 = (List) A00.first;
        this.A00 = (List) A00.second;
    }
}
