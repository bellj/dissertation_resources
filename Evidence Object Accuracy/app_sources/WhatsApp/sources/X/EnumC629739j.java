package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class EnumC629739j extends Enum {
    public static final EnumC629739j A00 = new EnumC629739j("ERROR", "error", 2);
    public static final EnumC629739j A01 = new EnumC629739j("ERROR_CODE", "code", 3);
    public static final EnumC629739j A02 = new EnumC629739j("ERROR_MESSAGE", "message", 4);
    public static final EnumC629739j A03 = new EnumC629739j("ERROR_PARAMS", "params", 5);
    public static final EnumC629739j A04 = new EnumC629739j("SCREEN_DATA", "screen_data", 1);
    public static final EnumC629739j A05 = new EnumC629739j("SUCCESS_FLAG", "success", 0);
    public final String key;

    public EnumC629739j(String str, String str2, int i) {
        this.key = str2;
    }
}
