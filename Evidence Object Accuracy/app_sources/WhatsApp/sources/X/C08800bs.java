package X;

import android.view.animation.Interpolator;
import com.whatsapp.R;
import java.lang.ref.WeakReference;

/* renamed from: X.0bs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08800bs implements AbstractC17450qp {
    public final AbstractC17450qp A00;

    public C08800bs(AbstractC17450qp r3) {
        this.A00 = new C08790br(new C08780bq(r3));
    }

    public static Interpolator A00(AnonymousClass28D r5) {
        if (r5 == null) {
            return AnonymousClass0L1.A00(0.0f, 0.0f, 1.0f, 1.0f);
        }
        return AnonymousClass0L1.A00(r5.A09(35, 0.0f), r5.A09(38, 0.0f), r5.A09(36, 1.0f), r5.A09(40, 1.0f));
    }

    public static AbstractC12100hN A01(C14230l4 r1, AbstractC14200l1 r2) {
        return new AbstractC12100hN(r1, r2) { // from class: X.0b8
            public final /* synthetic */ C14230l4 A00;
            public final /* synthetic */ AbstractC14200l1 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC12100hN
            public final void API() {
                A00(this.A00, this.A01);
            }

            public static /* synthetic */ void A00(C14230l4 r12, AbstractC14200l1 r22) {
                if (r22 != null) {
                    C14250l6.A00(r12, C14220l3.A01, r22);
                }
            }
        };
    }

    public static AbstractC12110hO A02(C14230l4 r1, AbstractC14200l1 r2) {
        return new AbstractC12110hO(r1, r2) { // from class: X.0bA
            public final /* synthetic */ C14230l4 A00;
            public final /* synthetic */ AbstractC14200l1 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC12110hO
            public final void AW1() {
                A00(this.A00, this.A01);
            }

            public static /* synthetic */ void A00(C14230l4 r12, AbstractC14200l1 r22) {
                if (r22 != null) {
                    C14250l6.A00(r12, C14220l3.A01, r22);
                }
            }
        };
    }

    public static void A03(C14230l4 r12, C14220l3 r13) {
        String str;
        AnonymousClass28D r8 = (AnonymousClass28D) r13.A00(0);
        C14260l7 A02 = C67973Tp.A02(r12, r13, 1);
        if (r8 == null) {
            str = "Cannot show toast with null content.";
        } else {
            AnonymousClass3JI A00 = AnonymousClass3JI.A00(r12, r8);
            AnonymousClass28D r1 = (AnonymousClass28D) r13.A00(1);
            if (r1 == null) {
                str = "Cannot show toast with invalid options.";
            } else {
                int A0B = r1.A0B(35, 5000);
                int A0B2 = r1.A0B(42, 100);
                int A0B3 = r1.A0B(36, 100);
                Interpolator A002 = A00(r1.A0F(43));
                Interpolator A003 = A00(r1.A0F(38));
                AbstractC14200l1 A0G = r1.A0G(41);
                AbstractC14200l1 A0G2 = r1.A0G(40);
                AnonymousClass0PS r14 = new AnonymousClass0PS(A02);
                r14.A01(A00);
                r14.A00 = A0B;
                r14.A02 = A0B2;
                r14.A01 = A0B3;
                r14.A04 = A002;
                r14.A03 = A003;
                r14.A06 = A02(r12, A0G);
                r14.A05 = A01(r12, A0G2);
                r14.A08 = r8.A0H();
                r14.A00().A01();
                return;
            }
        }
        C28691Op.A00("bk.action.toast.ShowToast", str);
    }

    public static void A04(C14230l4 r5, C14220l3 r6) {
        AnonymousClass3JI r3 = (AnonymousClass3JI) r6.A01(0);
        AnonymousClass28D r2 = (AnonymousClass28D) r6.A00(1);
        C14260l7 A02 = C67973Tp.A02(r5, r6, 2);
        if (r2 == null) {
            C28691Op.A00("bk.action.toast.ShowToastV2", "Cannot show toast with invalid options.");
            return;
        }
        AnonymousClass0PS r1 = new AnonymousClass0PS(A02);
        r1.A01(r3);
        r1.A00 = r2.A0B(35, 5000);
        r1.A02 = r2.A0B(42, 100);
        r1.A01 = r2.A0B(36, 100);
        r1.A04 = A00(r2.A0F(43));
        r1.A03 = A00(r2.A0F(38));
        r1.A06 = A02(r5, r2.A0G(41));
        r1.A05 = A01(r5, r2.A0G(40));
        r1.A08 = r3.A05().A0H();
        r1.A09 = A06(r2);
        r1.A00().A01();
    }

    public static void A05(C14220l3 r2) {
        AnonymousClass0BZ r1;
        String str = (String) r2.A00(0);
        if (str != null && (r1 = (AnonymousClass0BZ) C04470Lv.A00.get()) != null && str.equals(r1.getTag(R.id.foa_toast_tag_server_id))) {
            r1.A01(r1.A01);
        }
    }

    public static boolean A06(AnonymousClass28D r1) {
        return "mini".equals(r1.A0I(44));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003a, code lost:
        if (r13.equals("bk.action.array.FindIndex") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0043, code lost:
        if (r13.equals("bk.action.string.MatchesRegex") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0058, code lost:
        if (r13.equals("bk.action.text.GetText") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0061, code lost:
        if (r13.equals("bk.action.GetDatetimeText") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008c, code lost:
        if (r13.equals("commerce.action.GetCreditCardMetadata") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00f4, code lost:
        if (r13.equals("bk.action.array.Map") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002a, code lost:
        if (r13.equals("bk.action.animation.linear.GetCurrentValue") == false) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0033, code lost:
        if (r13.equals("bk.action.array.SortedArray") == false) goto L_0x001d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01fa  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0216  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0266  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x02a2  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x02af  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x02f5  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x02f9  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0331  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x033b  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0342  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0378  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x03aa  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x03d2  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x03d6  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x03e8  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x044e  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0464  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0179  */
    /* renamed from: A07 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A9j(X.C14230l4 r24, X.C14220l3 r25, X.C1093651k r26) {
        /*
        // Method dump skipped, instructions count: 1284
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08800bs.A9j(X.0l4, X.0l3, X.51k):java.lang.Object");
    }

    public final void A08(C14260l7 r7, C14230l4 r8, AbstractC14200l1 r9, String str, long j, boolean z) {
        AnonymousClass3JV.A07(r7, C14190l0.A00(new AnonymousClass010(this, r8, r9, str, new WeakReference(r7)), j, z), str);
    }
}
