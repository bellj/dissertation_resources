package X;

/* renamed from: X.5xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129535xs {
    public final int A00;
    public final int A01;
    public final AnonymousClass66P A02;

    public /* synthetic */ C129535xs(C127225u9 r2) {
        this.A01 = r2.A01;
        this.A00 = r2.A00;
        this.A02 = r2.A02;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C129535xs r5 = (C129535xs) obj;
            if (!(this.A01 == r5.A01 && this.A00 == r5.A00 && this.A02 == r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (this.A01 * 31) + this.A00;
    }
}
