package X;

import android.os.IInterface;

/* renamed from: X.3rL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class BinderC79873rL extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115705Sp A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC79873rL(AbstractC115705Sp r2) {
        super("com.google.android.gms.maps.internal.IOnCameraMoveStartedListener");
        this.A00 = r2;
    }
}
