package X;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.SyncResult;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.whatsapp.util.Log;
import java.util.concurrent.ExecutionException;

/* renamed from: X.1DQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DQ extends AbstractThreadedSyncAdapter {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final AnonymousClass1DP A02;

    public AnonymousClass1DQ(AbstractC15710nm r3, C15570nT r4, AnonymousClass1DP r5, C16590pI r6) {
        super(r6.A00, true);
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r5;
    }

    @Override // android.content.AbstractThreadedSyncAdapter
    public void onPerformSync(Account account, Bundle bundle, String str, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        this.A01.A08();
        C42271uw r1 = new C42271uw(AnonymousClass1JA.A02);
        r1.A03 = true;
        r1.A04 = true;
        r1.A00 = AnonymousClass1JB.A09;
        C42251uu A01 = r1.A01();
        C42701vg r4 = new C42701vg(true);
        A01.A03.add(r4);
        AnonymousClass1DP r3 = this.A02;
        r3.A0R.execute(new RunnableBRunnable0Shape2S0200000_I0_2(r3, 45, A01));
        try {
            r4.get();
        } catch (InterruptedException unused) {
        } catch (ExecutionException e) {
            AnonymousClass009.A0D(e);
            Log.e("ContactsSyncAdapterService/onCreate", e);
            this.A00.AaV("ContactsSyncAdapterService/onCreate", e.getMessage(), true);
        }
    }
}
