package X;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;

/* renamed from: X.5C6  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5C6 implements Comparator {
    @Override // java.util.Comparator
    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        return ((Scope) obj).A01.compareTo(((Scope) obj2).A01);
    }
}
