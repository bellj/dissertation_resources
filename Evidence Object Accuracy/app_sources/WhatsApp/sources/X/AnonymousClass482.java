package X;

import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.482  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass482 extends C236712o {
    public final /* synthetic */ VoipActivityV2 A00;

    public AnonymousClass482(VoipActivityV2 voipActivityV2) {
        this.A00 = voipActivityV2;
    }

    @Override // X.C236712o
    public void A01(AnonymousClass1YT r3) {
        Log.i("voip/VoipActivityV2/onCallEnded");
        VoipActivityV2 voipActivityV2 = this.A00;
        if (!voipActivityV2.A0f.A00) {
            voipActivityV2.finish();
        }
    }

    @Override // X.C236712o
    public void A03(AnonymousClass1YT r3, boolean z) {
        Log.i("voip/VoipActivityV2/onCallMissed");
        VoipActivityV2 voipActivityV2 = this.A00;
        if (!voipActivityV2.A0f.A00) {
            voipActivityV2.finish();
        }
    }
}
