package X;

import java.util.List;

/* renamed from: X.2v5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59652v5 extends C37171lc {
    public final C48122Ek A00;
    public final C89354Jq A01;
    public final List A02;

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C59652v5) {
                C59652v5 r5 = (C59652v5) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C59652v5(C48122Ek r2, C89354Jq r3, List list) {
        super(AnonymousClass39o.A0M);
        C16700pc.A0G(r2, list);
        this.A00 = r2;
        this.A02 = list;
        this.A01 = r3;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return C12990iw.A08(this.A01, C12990iw.A08(this.A02, this.A00.hashCode() * 31) * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("NearbyBusinessWidgetListItem(searchLocation=");
        A0k.append(this.A00);
        A0k.append(", businessProfiles=");
        A0k.append(this.A02);
        A0k.append(", nearbyBusinessClickListener=");
        return C12960it.A0a(this.A01, A0k);
    }
}
