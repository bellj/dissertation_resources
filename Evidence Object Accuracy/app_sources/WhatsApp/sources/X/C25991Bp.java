package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.os.CancellationSignal;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25991Bp {
    public static final int A0R = 3;
    public static final int A0S = 30;
    public static final String A0T = "MessagesImporter/";
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C002701f A02;
    public final C18780t0 A03;
    public final C15810nw A04;
    public final C14830m7 A05;
    public final C14820m6 A06;
    public final C20650w6 A07;
    public final C16510p9 A08;
    public final C19990v2 A09;
    public final C15650ng A0A;
    public final C21410xN A0B;
    public final AnonymousClass1C5 A0C;
    public final C18680sq A0D;
    public final C22830zi A0E;
    public final C20620w3 A0F;
    public final C20140vH A0G;
    public final AnonymousClass134 A0H;
    public final C21400xM A0I;
    public final C20160vJ A0J;
    public final C25981Bo A0K;
    public final C26001Bq A0L;
    public final AnonymousClass1C6 A0M;
    public final C15740np A0N;
    public final C15860o1 A0O;
    public final AbstractC15870o2 A0P;
    public final AnonymousClass01H A0Q;

    public C25991Bp(C14830m7 r2, C16510p9 r3, AbstractC15710nm r4, C19990v2 r5, C20140vH r6, C14330lG r7, AnonymousClass134 r8, C20650w6 r9, C15810nw r10, C25981Bo r11, C15650ng r12, C20160vJ r13, C20620w3 r14, C15860o1 r15, C26001Bq r16, C15740np r17, C21400xM r18, C14820m6 r19, C18680sq r20, C22830zi r21, C21410xN r22, AnonymousClass1C5 r23, C18780t0 r24, AbstractC15870o2 r25, AnonymousClass1C6 r26, AnonymousClass01H r27, C002701f r28) {
        this.A05 = r2;
        this.A08 = r3;
        this.A00 = r4;
        this.A09 = r5;
        this.A0G = r6;
        this.A01 = r7;
        this.A0H = r8;
        this.A07 = r9;
        this.A04 = r10;
        this.A0K = r11;
        this.A0A = r12;
        this.A0J = r13;
        this.A0F = r14;
        this.A0O = r15;
        this.A0L = r16;
        this.A0N = r17;
        this.A0I = r18;
        this.A06 = r19;
        this.A0D = r20;
        this.A0E = r21;
        this.A0B = r22;
        this.A0C = r23;
        this.A03 = r24;
        this.A0P = r25;
        this.A0M = r26;
        this.A0Q = r27;
        this.A02 = r28;
    }

    public static int A00(C462525d r4) {
        int i = 0;
        if (r4 == null) {
            return 0;
        }
        int i2 = r4.A00;
        if ((i2 & 1) == 1 && r4.A03) {
            i = 1;
        }
        if ((i2 & 2) == 2 && r4.A01) {
            i |= 2;
        }
        if ((i2 & 4) == 4 && r4.A04) {
            i |= 4;
        }
        return ((i2 & 8) != 8 || !r4.A02) ? i : i | 8;
    }

    public static int A01(List list) {
        Iterator it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            i = (int) (((long) i) + ((C90454Ny) it.next()).A01);
        }
        return i;
    }

    public static C91784Tc A02(InputStream inputStream) {
        try {
            JSONObject jSONObject = new JSONObject(new String(AnonymousClass3A5.A00(inputStream), DefaultCrypto.UTF_8));
            JSONObject jSONObject2 = jSONObject.getJSONObject("header");
            C91784Tc r7 = new C91784Tc();
            r7.A00 = jSONObject2.getLong("creation_date");
            r7.A05 = jSONObject2.getString("os");
            r7.A06 = jSONObject2.getString("os_version");
            r7.A02 = jSONObject2.getString("app_name");
            r7.A03 = jSONObject2.getString("app_version");
            r7.A04 = jSONObject2.getString("format_version");
            if (jSONObject.has("messages")) {
                JSONObject jSONObject3 = jSONObject.getJSONObject("messages");
                AnonymousClass4QT r6 = new AnonymousClass4QT();
                r6.A00 = jSONObject3.getString("filename");
                r6.A01 = jSONObject3.getString("format");
                if (jSONObject3.has("chunks")) {
                    JSONArray jSONArray = jSONObject3.getJSONArray("chunks");
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject4 = jSONArray.getJSONObject(i);
                        C90454Ny r2 = new C90454Ny();
                        r2.A00 = jSONObject4.getInt("chunk_number");
                        r2.A01 = (long) jSONObject4.getInt("messages_count");
                        arrayList.add(r2);
                    }
                    r6.A02 = arrayList;
                }
                r7.A01 = r6;
            }
            return r7;
        } catch (IOException | JSONException e) {
            throw new IOException("Unable to parse JSON header.", e);
        }
    }

    public static InputStream A03(String str, String str2) {
        File file = new File(str);
        if (file.exists()) {
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file));
            try {
                try {
                    for (ZipEntry nextEntry = zipInputStream.getNextEntry(); nextEntry != null; nextEntry = zipInputStream.getNextEntry()) {
                        if (nextEntry.getName().equalsIgnoreCase(str2)) {
                            return zipInputStream;
                        }
                    }
                    StringBuilder sb = new StringBuilder("Failed to find entry '");
                    sb.append(str2);
                    sb.append("' in '");
                    sb.append(str);
                    sb.append("' archive.");
                    throw new FileNotFoundException(sb.toString());
                } catch (IOException e) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("MessagesImporter/Failed to read entry '");
                    sb2.append(str2);
                    sb2.append("' in '");
                    sb2.append(str);
                    sb2.append("' archive.");
                    Log.e(sb2.toString(), e);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("MessagesImporter/Failed to read entry '");
                    sb3.append(str2);
                    sb3.append("' in '");
                    sb3.append(str);
                    sb3.append("' archive.");
                    throw new IOException(sb3.toString(), e);
                }
            } finally {
                try {
                    zipInputStream.close();
                } catch (IOException unused) {
                }
            }
        } else {
            StringBuilder sb4 = new StringBuilder("Unable to locate input data file '");
            sb4.append(str);
            sb4.append("'.");
            throw new FileNotFoundException(sb4.toString());
        }
    }

    private void A04(CancellationSignal cancellationSignal, File file) {
        if (file.exists()) {
            AnonymousClass1C7 r2 = (AnonymousClass1C7) this.A0Q.get();
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
                try {
                    byte[] bArr = new byte[C25981Bo.A0F];
                    for (ZipEntry nextEntry = zipInputStream.getNextEntry(); nextEntry != null; nextEntry = zipInputStream.getNextEntry()) {
                        cancellationSignal.throwIfCanceled();
                        String name = nextEntry.getName();
                        if (TextUtils.isEmpty(name)) {
                            throw new IllegalArgumentException("Filename is not specified.");
                        } else if (!TextUtils.isEmpty(name)) {
                            File file2 = new File(r2.A00.getFilesDir(), "migration/import/sandbox");
                            File file3 = new File(file2, name);
                            if (file3.getCanonicalPath().startsWith(file2.getCanonicalPath())) {
                                if (file3.exists()) {
                                    file3.delete();
                                }
                                if (file3.getParentFile() != null) {
                                    file3.getParentFile().mkdirs();
                                }
                                FileOutputStream fileOutputStream = new FileOutputStream(file3);
                                C15740np.A01(cancellationSignal, zipInputStream, fileOutputStream, bArr);
                                fileOutputStream.close();
                            } else {
                                StringBuilder sb = new StringBuilder("Invalid file name: ");
                                sb.append(name);
                                sb.append(", sandbox escaping attempt.");
                                throw new IOException(sb.toString());
                            }
                        } else {
                            throw new IllegalArgumentException("Filename is not specified.");
                        }
                    }
                    zipInputStream.close();
                    fileInputStream.close();
                } catch (Throwable th) {
                    try {
                        zipInputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (IOException e) {
                Log.e("MessagesImporter/Failed to unpack files from archive.", e);
                C14350lI.A0C(new File(r2.A00.getFilesDir(), "migration/import/sandbox"));
                throw e;
            }
        } else {
            StringBuilder sb2 = new StringBuilder("Unable to locate input data file '");
            sb2.append(file.getAbsolutePath());
            sb2.append("'.");
            throw new FileNotFoundException(sb2.toString());
        }
    }

    private void A05(AnonymousClass1G6 r6, String str, Throwable th) {
        Locale locale = Locale.US;
        Object[] objArr = new Object[4];
        objArr[0] = str;
        AnonymousClass1G8 r0 = r6.A0M;
        if (r0 == null) {
            r0 = AnonymousClass1G8.A05;
        }
        objArr[1] = r0.A01;
        EnumC40021qv A00 = EnumC40021qv.A00(r6.A06);
        if (A00 == null) {
            A00 = EnumC40021qv.A2B;
        }
        objArr[2] = A00;
        objArr[3] = Integer.valueOf(r6.A0G.size());
        String format = String.format(locale, "%s; key=%s, stub_type=%s, params=%d", objArr);
        StringBuilder sb = new StringBuilder(A0T);
        sb.append(format);
        Log.e(sb.toString(), th);
        if (th != null) {
            AbstractC15710nm r2 = this.A00;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(format);
            sb2.append("; ex=");
            sb2.append(th);
            r2.A02("xpm-msg-importer-parsing-failed", sb2.toString(), th);
            return;
        }
        this.A00.AaV("xpm-msg-importer-parsing-failed", format, false);
    }

    private void A06(Map map, Map map2) {
        int i;
        for (Map.Entry entry : map.entrySet()) {
            Object key = entry.getKey();
            AnonymousClass1PE r7 = (AnonymousClass1PE) entry.getValue();
            AnonymousClass1PB r3 = (AnonymousClass1PB) map2.get(key);
            if (r3 != null) {
                int i2 = r3.A01;
                if ((i2 & C25981Bo.A0F) == 131072 && r3.A0Y) {
                    r7.A0C(-1, 0, 0, 0);
                } else if ((i2 & 16) == 16 && (i = r3.A07) > 0) {
                    long A03 = this.A0G.A03(r7.A05(), i) - 1;
                    r7.A0C(this.A0H.A00(r7.A05(), A03), this.A0H.A01(r7.A05(), A03), i, this.A0C.A00(r7.A05(), A03));
                }
                this.A08.A0H(r7.A04(null), r7);
            }
        }
    }

    public AnonymousClass1PE A07(CancellationSignal cancellationSignal, AbstractC14640lm r21, AbstractC115345Re r22, AnonymousClass1PB r23, Map map, Map map2, byte[] bArr) {
        int i;
        AnonymousClass1PJ r0;
        long j;
        int i2;
        EnumC40871sT r02;
        AnonymousClass1PE A06 = this.A09.A06(r21);
        long j2 = 0;
        int i3 = 0;
        if (A06 == null) {
            A0N(r21, r23.A0P);
            if (r21 instanceof UserJid) {
                int i4 = r23.A01;
                if ((i4 & 262144) == 262144 && (i4 & 524288) == 524288) {
                    this.A03.A00((UserJid) r21, r23.A0H.A04(), r23.A0F);
                }
                if ((r23.A01 & 33554432) == 33554432) {
                    this.A03.A08((UserJid) r21, Long.valueOf(r23.A0E));
                }
            }
            if (r23.A0I.size() > 0) {
                AnonymousClass1G6 r03 = ((C40761sH) r23.A0I.get(0)).A03;
                if (r03 == null) {
                    r03 = AnonymousClass1G6.A0k;
                }
                j = r03.A0A * 1000;
            } else {
                j = 0;
            }
            A06 = this.A09.A06(r21);
            if (A06 == null) {
                return null;
            }
            this.A08.A0H(A06.A04(Long.valueOf(j)), A06);
            A0E(cancellationSignal, r21, r22, r23, bArr);
            if (r21 instanceof AbstractC15590nW) {
                for (C40861sS r8 : r23.A0J) {
                    UserJid nullable = UserJid.getNullable(r8.A03);
                    if (nullable != null) {
                        if ((r8.A01 & 2) == 2) {
                            int i5 = r8.A02;
                            if (i5 != 0) {
                                if (i5 == 1) {
                                    r02 = EnumC40871sT.A01;
                                } else if (i5 == 2) {
                                    r02 = EnumC40871sT.A03;
                                }
                                i2 = r02.value;
                            }
                            r02 = EnumC40871sT.A02;
                            i2 = r02.value;
                        } else {
                            i2 = 0;
                        }
                        this.A0D.A04(new AnonymousClass1YO(nullable, i2, false, true), (AbstractC15590nW) r21);
                    }
                }
            }
        }
        if ((r23.A01 & DefaultCrypto.BUFFER_SIZE) != 8192) {
            A06.A09(0);
        } else if (r23.A0Z) {
            A06.A09(1);
        } else {
            A06.A09(-1);
        }
        synchronized (A06) {
            A06.A00 = 1;
        }
        synchronized (A06) {
            A06.A09 = -1;
        }
        A06.A0B(r23.A09 * 1000);
        if ((r23.A01 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            String str = r23.A0P;
            synchronized (A06) {
                A06.A0e = str;
            }
        }
        int i6 = r23.A01;
        if ((i6 & 16384) == 16384) {
            A06.A0f = r23.A0U;
        }
        if ((i6 & 128) == 128) {
            i = r23.A04;
        } else {
            i = 0;
        }
        if ((i6 & 256) == 256) {
            j2 = r23.A0B;
        }
        if ((i6 & 32768) == 32768) {
            AnonymousClass1PH r04 = r23.A0K;
            if (r04 == null) {
                r04 = AnonymousClass1PH.A02;
            }
            int i7 = r04.A01;
            if (i7 != 0) {
                if (i7 == 1) {
                    r0 = AnonymousClass1PJ.A02;
                } else if (i7 == 2) {
                    r0 = AnonymousClass1PJ.A03;
                }
                i3 = Math.min(2, Math.max(0, r0.value));
            }
            r0 = AnonymousClass1PJ.A01;
            i3 = Math.min(2, Math.max(0, r0.value));
        }
        AnonymousClass1PG r1 = A06.A0Y;
        A06.A0A(Math.max(r1.expiration, i), Math.max(r1.ephemeralSettingTimestamp, j2), i3);
        map.put(r21, A06);
        map2.put(r21, r23);
        return A06;
    }

    public AbstractC15340mz A08(C40761sH r6) {
        AbstractC15340mz r0;
        if (r6 == null) {
            this.A0L.A00("import/msg/failed").incrementAndGet();
            Log.e("MessagesImporter/Conversation message is null.");
            return null;
        }
        AnonymousClass1G6 r3 = r6.A03;
        if (r3 == null) {
            r3 = AnonymousClass1G6.A0k;
        }
        try {
            AnonymousClass1sN A01 = this.A0J.A01(r3);
            if (A01 != null && (r0 = A01.A00) != null) {
                return r0;
            }
            this.A0L.A00("import/msg/failed").incrementAndGet();
            A05(r3, "Parsed WMI message is null.", null);
            return null;
        } catch (Exception e) {
            this.A0L.A00("import/msg/failed").incrementAndGet();
            A05(r3, "Failed to parse message from WMI.", e);
            return null;
        }
    }

    public File A09(String str, byte b, boolean z) {
        String name = new File(str).getName();
        C14330lG r2 = this.A01;
        int i = 2;
        if (z) {
            i = 1;
        }
        return new File(r2.A0B(b, 0, i), name);
    }

    public void A0A() {
        throw new IOException("Unsupported build version.");
    }

    public void A0B(CancellationSignal cancellationSignal) {
        A0H(cancellationSignal, new AnonymousClass583(this.A0K));
    }

    public void A0C(CancellationSignal cancellationSignal, AnonymousClass1PE r7, AbstractC115345Re r8, AnonymousClass1G6 r9, AbstractC15340mz r10, byte[] bArr) {
        A0J(cancellationSignal, r8, r10, bArr);
        if (r10.A0z.A02) {
            A0M(r7, r9, r10);
        }
        if (C30041Vv.A0y(r10, true)) {
            synchronized (r7) {
                r7.A0Z = r10;
            }
            long j = r10.A11;
            synchronized (r7) {
                r7.A0M = j;
            }
            long j2 = r10.A12;
            synchronized (r7) {
                r7.A0N = j2;
            }
            long j3 = r10.A12;
            synchronized (r7) {
                r7.A0U = j3;
            }
            long j4 = r10.A11;
            synchronized (r7) {
                r7.A0T = j4;
            }
        }
        if (r10.A0I > r7.A02()) {
            r7.A0B(r10.A0I);
        }
    }

    public void A0D(CancellationSignal cancellationSignal, AnonymousClass1PE r11, AbstractC115345Re r12, List list, byte[] bArr) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C40761sH r0 = (C40761sH) it.next();
            cancellationSignal.throwIfCanceled();
            AbstractC15340mz A08 = A08(r0);
            if (A08 != null) {
                AnonymousClass1G6 r6 = r0.A03;
                if (r6 == null) {
                    r6 = AnonymousClass1G6.A0k;
                }
                A0C(cancellationSignal, r11, r12, r6, A08, bArr);
            }
        }
    }

    public void A0E(CancellationSignal cancellationSignal, AbstractC14640lm r16, AbstractC115345Re r17, AnonymousClass1PB r18, byte[] bArr) {
        if ((r18.A01 & 8388608) == 8388608 && this.A0P.AHh(r16, true) == null) {
            C462425c r5 = r18.A0L;
            if (r5 == null) {
                r5 = C462425c.A03;
            }
            A0F(cancellationSignal, r16, r17, r5, bArr, false);
            A0F(cancellationSignal, r16, r17, r5, bArr, true);
        }
    }

    public void A0F(CancellationSignal cancellationSignal, AbstractC14640lm r12, AbstractC115345Re r13, C462425c r14, byte[] bArr, boolean z) {
        IOException e;
        StringBuilder sb;
        String str;
        int i;
        C33181da A08;
        String str2 = r14.A02;
        if (!TextUtils.isEmpty(str2)) {
            String A082 = this.A04.A08(new File(str2));
            try {
                File A09 = A09(A082, (byte) 1, true);
                if (!A09.exists()) {
                    try {
                        A0L(cancellationSignal, r13, A09, A082, bArr);
                        this.A0L.A00("import/msg/file/success").incrementAndGet();
                    } catch (IOException e2) {
                        e = e2;
                        this.A0L.A00("import/msg/file/failed").incrementAndGet();
                        sb = new StringBuilder();
                        str = "MessagesImporter/cannot import file for wallpaper, file=";
                        sb.append(str);
                        sb.append(A082);
                        Log.e(sb.toString(), e);
                    }
                }
                if ((r14.A00 & 2) == 2) {
                    i = r14.A01;
                } else {
                    i = 100;
                }
                try {
                    C33191db r2 = new C33191db(Integer.valueOf(i), "USER_PROVIDED", A09.getCanonicalPath());
                    C15860o1 r1 = (C15860o1) this.A0P;
                    if (r12 == null) {
                        A08 = r1.A05();
                    } else {
                        A08 = r1.A08(r12.getRawString());
                    }
                    if (z) {
                        A08.A04 = r2;
                    } else {
                        A08.A05 = r2;
                    }
                    r1.A0M(A08);
                } catch (IOException e3) {
                    e = e3;
                    sb = new StringBuilder();
                    str = "MessagesImporter/cannot get path for imported file, file=";
                    sb.append(str);
                    sb.append(A082);
                    Log.e(sb.toString(), e);
                }
            } catch (IOException e4) {
                e = e4;
                sb = new StringBuilder();
                str = "MessagesImporter/cannot get corrected media file for wallpaper, file=";
            }
        }
    }

    public void A0G(CancellationSignal cancellationSignal, AnonymousClass4QT r27, AbstractC115345Re r28, String str) {
        AnonymousClass1PE A07;
        List list = r27.A02;
        if (list == null || list.size() == 0) {
            Log.e("MessagesImporter/Messages chunks are not specified.");
            throw new IOException("Messages chunks are not specified.");
        }
        byte[] bArr = new byte[C25981Bo.A0F];
        int A01 = A01(r27.A02);
        Map hashMap = new HashMap();
        Map hashMap2 = new HashMap();
        TreeMap treeMap = new TreeMap();
        InputStream A03 = A03(str, r27.A00);
        int i = 0;
        for (int i2 = 0; i2 < r27.A02.size(); i2++) {
            try {
                r27.A02.get(i2);
                try {
                    AnonymousClass1P7 r2 = (AnonymousClass1P7) AbstractC27091Fz.A0D(AnonymousClass1P7.A0D, A03);
                    if (r2 != null) {
                        if (i2 == 0) {
                            A0I(cancellationSignal, r28, r2, bArr);
                        }
                        for (int i3 = 0; i3 < r2.A07.size(); i3++) {
                            AnonymousClass1PB r0 = (AnonymousClass1PB) r2.A07.get(i3);
                            if (A0S(r0)) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("MessagesImporter/Skipping chat, messages count: ");
                                sb.append(r0.A0I.size());
                                Log.i(sb.toString());
                                this.A0L.A00("import/chat/skipped").incrementAndGet();
                            } else {
                                AbstractC14640lm A012 = AbstractC14640lm.A01(r0.A0O);
                                if (!(A012 == null || (A07 = A07(cancellationSignal, A012, r28, r0, hashMap, hashMap2, bArr)) == null)) {
                                    A0D(cancellationSignal, A07, r28, r0.A0I, bArr);
                                    A0O(r0, treeMap);
                                    i += r0.A0I.size();
                                    for (C89624Kr r02 : this.A0M.A01()) {
                                        r02.A00.A01(1, i, A01);
                                    }
                                }
                            }
                        }
                    } else {
                        Log.e("MessagesImporter/Failed to parse serialized messages file.");
                        throw new IOException("Failed to parse serialized messages file.");
                    }
                } catch (IOException e) {
                    Log.e("MessagesImporter/Failed to parse serialized messages file.", e);
                    throw new IOException("Failed to parse serialized messages file.", e);
                }
            } catch (Throwable th) {
                try {
                    A03.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A03.close();
        A06(hashMap, hashMap2);
        A0R(treeMap);
    }

    public void A0H(CancellationSignal cancellationSignal, AbstractC115345Re r6) {
        String absolutePath = ((AnonymousClass583) r6).A00.A06("migration/messages_export.zip").getAbsolutePath();
        try {
            InputStream A03 = A03(absolutePath, "header.json");
            C91784Tc A02 = A02(A03);
            A03.close();
            AnonymousClass4QT r2 = A02.A01;
            if (r2 != null && !TextUtils.isEmpty(r2.A00) && "protobuf".equalsIgnoreCase(r2.A01)) {
                A0G(cancellationSignal, r2, r6, absolutePath);
            }
        } catch (IOException e) {
            throw new AnonymousClass2GB("Unable to locate header metadata file in messages archive.", e, 202);
        }
    }

    public void A0I(CancellationSignal cancellationSignal, AbstractC115345Re r15, AnonymousClass1P7 r16, byte[] bArr) {
        int i;
        if ((r16.A01 & 8) == 8) {
            C57632nN r4 = r16.A0C;
            if (r4 == null) {
                r4 = C57632nN.A0B;
            }
            if ((r4.A00 & 1) == 1) {
                C462425c r10 = r4.A08;
                if (r10 == null) {
                    r10 = C462425c.A03;
                }
                A0F(cancellationSignal, null, r15, r10, bArr, false);
            }
            if ((r4.A00 & 4) == 4) {
                C462425c r102 = r4.A07;
                if (r102 == null) {
                    r102 = C462425c.A03;
                }
                A0F(cancellationSignal, null, r15, r102, bArr, true);
            }
            boolean z = false;
            if ((r4.A00 & 2) == 2) {
                z = true;
            }
            if (z) {
                AnonymousClass4BT A00 = AnonymousClass4BT.A00(r4.A02);
                if (A00 == null) {
                    A00 = AnonymousClass4BT.A01;
                }
                if (A00 != AnonymousClass4BT.A01) {
                    C15860o1 r3 = this.A0O;
                    if (A00 == AnonymousClass4BT.A03) {
                        i = 2;
                    } else {
                        i = 1;
                    }
                    C33181da A08 = r3.A08("individual_chat_defaults");
                    if (i != A08.A00) {
                        A08.A00 = i;
                        r3.A0M(A08);
                    }
                }
            }
            if ((r4.A00 & 8) == 8) {
                C14820m6 r1 = this.A06;
                C462525d r0 = r4.A06;
                if (r0 == null) {
                    r0 = C462525d.A05;
                }
                r1.A00.edit().putInt("autodownload_wifi_mask", A00(r0)).apply();
            }
            if ((r4.A00 & 16) == 16) {
                C14820m6 r12 = this.A06;
                C462525d r02 = r4.A04;
                if (r02 == null) {
                    r02 = C462525d.A05;
                }
                r12.A00.edit().putInt("autodownload_cellular_mask", A00(r02)).apply();
            }
            if ((r4.A00 & 32) == 32) {
                C14820m6 r13 = this.A06;
                C462525d r03 = r4.A05;
                if (r03 == null) {
                    r03 = C462525d.A05;
                }
                r13.A00.edit().putInt("autodownload_roaming_mask", A00(r03)).apply();
            }
            if ((r4.A00 & 64) == 64) {
                C15860o1 r32 = this.A0O;
                boolean z2 = !r4.A0A;
                C33181da A082 = r32.A08("individual_chat_defaults");
                if (z2 != A082.A0D) {
                    A082.A0D = z2;
                    r32.A0M(A082);
                }
            }
            if ((r4.A00 & 128) == 128) {
                C15860o1 r33 = this.A0O;
                boolean z3 = !r4.A09;
                C33181da A083 = r33.A08("group_chat_defaults");
                if (z3 != A083.A0D) {
                    A083.A0D = z3;
                    r33.A0M(A083);
                }
            }
        }
    }

    public void A0J(CancellationSignal cancellationSignal, AbstractC115345Re r6, AbstractC15340mz r7, byte[] bArr) {
        try {
            r7.A0T(16384);
            if (r7 instanceof AbstractC16130oV) {
                A0K(cancellationSignal, r6, (AbstractC16130oV) r7, bArr);
            } else if (r7 instanceof AnonymousClass1XB) {
                A0Q((AnonymousClass1XB) r7);
            }
            AbstractC15340mz A0E = r7.A0E();
            if (A0E != null && (A0E instanceof AbstractC16130oV)) {
                A0K(cancellationSignal, r6, (AbstractC16130oV) A0E, bArr);
            }
            if (r7.A04 > 0) {
                this.A0B.A01(r7, r7.A0I);
            }
            this.A0A.A0T(r7);
            this.A0L.A00("import/msg/success").incrementAndGet();
            A0P(r7);
        } catch (Exception e) {
            this.A0L.A00("import/msg/failed").incrementAndGet();
            Log.e("MessagesImporter/Failed to insert message.", e);
            AbstractC15710nm r2 = this.A00;
            StringBuilder sb = new StringBuilder("Failed to insert message: ");
            sb.append(e.toString());
            r2.A02("xpm-msg-importer-insert-failed", sb.toString(), e);
        }
    }

    public void A0K(CancellationSignal cancellationSignal, AbstractC115345Re r14, AbstractC16130oV r15, byte[] bArr) {
        File file;
        boolean z;
        C16150oX r3 = r15.A02;
        if (r3 != null && (file = r3.A0F) != null) {
            String A08 = this.A04.A08(file);
            File A09 = A09(A08, r15.A0y, r15.A0z.A02);
            if (!A09.exists()) {
                try {
                    A0L(cancellationSignal, r14, A09, A08, bArr);
                    this.A0L.A00("import/msg/file/success").incrementAndGet();
                } catch (IOException e) {
                    this.A0L.A00("import/msg/file/failed").incrementAndGet();
                    StringBuilder sb = new StringBuilder("MessagesImporter/processMediaMessage; cannot import file for message, file=");
                    sb.append(A08);
                    Log.e(sb.toString(), e);
                    z = true;
                }
            }
            z = false;
            if (!A09.exists() || z) {
                r3.A0F = null;
                r3.A0P = false;
                return;
            }
            this.A02.A07(A09, 1, true);
            r3.A0F = A09;
            r3.A0P = true;
        }
    }

    public void A0L(CancellationSignal cancellationSignal, AbstractC115345Re r4, File file, String str, byte[] bArr) {
        this.A0N.A03(cancellationSignal, ((AnonymousClass583) r4).A00.A06(str), file, bArr);
    }

    public void A0M(AnonymousClass1PE r25, AnonymousClass1G6 r26, AbstractC15340mz r27) {
        AbstractC15340mz r12;
        C25991Bp r11;
        HashSet hashSet;
        AbstractC20610w2 r1;
        for (C40771sI r13 : r26.A0J) {
            try {
                r11 = this;
                r12 = r27;
                C20620w3 r0 = r11.A0F;
                long j = r12.A11;
                UserJid nullable = UserJid.getNullable(r13.A07);
                long j2 = r13.A04 * 1000;
                long j3 = r13.A03 * 1000;
                long j4 = r13.A02 * 1000;
                C21390xL r02 = r0.A03;
                if (r02.A00("receipt_user_ready", 0) == 2 || (j > 0 && j < r02.A01("migration_receipt_index", 0))) {
                    C39921ql A00 = r0.A00(j);
                    boolean A002 = j2 > 0 ? A00.A00(nullable, 5, j2) : false;
                    if (j3 > 0) {
                        A002 |= A00.A00(nullable, 13, j3);
                    }
                    if (j4 > 0) {
                        A002 |= A00.A00(nullable, 8, j4);
                    }
                    if (A002) {
                        long A01 = r0.A01.A01(nullable);
                        boolean z = false;
                        if (A01 != -1) {
                            z = true;
                        }
                        AnonymousClass009.A0C("invalid jid", z);
                        ContentValues contentValues = new ContentValues(5);
                        contentValues.put("message_row_id", Long.valueOf(j));
                        contentValues.put("receipt_user_jid_row_id", Long.valueOf(A01));
                        if (j2 > 0) {
                            contentValues.put("receipt_timestamp", Long.valueOf(j2));
                        }
                        if (j3 > 0) {
                            contentValues.put("read_timestamp", Long.valueOf(j3));
                        }
                        if (j4 > 0) {
                            contentValues.put("played_timestamp", Long.valueOf(j4));
                        }
                        C16310on A02 = r0.A02.A02();
                        C16330op r6 = A02.A03;
                        if (r6.A00("receipt_user", contentValues, "message_row_id = ? AND receipt_user_jid_row_id = ?", new String[]{String.valueOf(j), String.valueOf(A01)}) <= 0 && r6.A02(contentValues, "receipt_user") == -1) {
                            Log.e("ReceiptUserStore/insertOrUpdateEntireUserReceiptForMessage/insert failed");
                        }
                        A02.close();
                    }
                }
            } catch (SQLiteConstraintException e) {
                Log.e("MessagesImporter/Failed to insert user receipt.", e);
                r11.A00.A02("xpm-failed-receipt-import", e.toString(), e);
            }
            if (r13.A03 > 0) {
                long j5 = r12.A11;
                synchronized (r25) {
                    r25.A0Q = j5;
                }
                long j6 = r12.A12;
                synchronized (r25) {
                    r25.A0R = j6;
                }
            }
            if (r13.A06.size() > 0) {
                hashSet = new HashSet(r13.A06.size());
                for (String str : r13.A06) {
                    DeviceJid nullable2 = DeviceJid.getNullable(str);
                    if (nullable2 != null) {
                        hashSet.add(nullable2);
                    }
                }
            } else {
                hashSet = new HashSet();
            }
            if (r13.A05.size() > 0) {
                for (String str2 : r13.A05) {
                    DeviceJid nullable3 = DeviceJid.getNullable(str2);
                    if (nullable3 != null) {
                        hashSet.remove(nullable3);
                        try {
                            r11.A0E.A01(nullable3, r12, r12.A0I);
                        } catch (SQLiteConstraintException e2) {
                            Log.e("MessagesImporter/Failed to insert device receipt.", e2);
                            r11.A00.A02("xpm-failed-receipt-import", e2.toString(), e2);
                        }
                    }
                }
            }
            if (!hashSet.isEmpty()) {
                try {
                    C22830zi r14 = r11.A0E;
                    if (r12 instanceof AnonymousClass1Iv) {
                        r1 = r14.A01;
                    } else {
                        r1 = r14.A02;
                    }
                    hashSet.size();
                    r1.A02(r12, hashSet, false);
                } catch (SQLiteConstraintException e3) {
                    Log.e("MessagesImporter/Failed to insert blank device receipt.", e3);
                    r11.A00.A02("xpm-failed-receipt-import", e3.toString(), e3);
                }
            }
        }
    }

    public void A0N(AbstractC14640lm r7, String str) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        this.A07.A04(r7, new RunnableBRunnable0Shape4S0100000_I0_4(countDownLatch, 23), str);
        try {
            countDownLatch.await(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e("MessagesImporter/Chat creation interrupted.", e);
        }
        if (countDownLatch.getCount() > 0) {
            StringBuilder sb = new StringBuilder("Failed to create chat for ");
            sb.append(r7);
            throw new AnonymousClass2GB(1, sb.toString());
        }
    }

    public void A0O(AnonymousClass1PB r8, Map map) {
        int i;
        AbstractC14640lm A01 = AbstractC14640lm.A01(r8.A0O);
        AnonymousClass009.A05(A01);
        if ((r8.A01 & 2097152) == 2097152 && (i = r8.A06) > 0) {
            map.put(Integer.valueOf(i), A01);
        }
        if ((r8.A01 & 4194304) == 4194304) {
            long j = r8.A0D;
            if (j > 0) {
                j *= 1000;
            }
            if (j != 0) {
                this.A0O.A0T(A01, j, false);
            }
        }
        if ((r8.A01 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            AnonymousClass4BT A00 = AnonymousClass4BT.A00(r8.A05);
            if (A00 == null) {
                A00 = AnonymousClass4BT.A01;
            }
            if (A00 != AnonymousClass4BT.A01) {
                C15860o1 r3 = this.A0O;
                int i2 = A00.value;
                C33181da A08 = r3.A08(A01.getRawString());
                if (i2 != A08.A00) {
                    A08.A00 = i2;
                    r3.A0M(A08);
                }
            }
        }
    }

    public void A0P(AbstractC15340mz r5) {
        int i = r5.A07;
        if (i != 0) {
            C40481rf r1 = r5.A0V;
            if ((i & 1) == 1 && r1 != null) {
                for (AnonymousClass1Iv r2 : r1.A02()) {
                    this.A0I.A00(r2, false);
                }
            }
        }
    }

    public void A0Q(AnonymousClass1XB r3) {
        int i = r3.A00;
        if ((i == 11 || i == 9) && TextUtils.isEmpty(r3.A0I())) {
            C19990v2 r1 = this.A09;
            AbstractC14640lm r0 = r3.A0z.A00;
            AnonymousClass009.A05(r0);
            r3.A0l(r1.A09(r0));
        }
    }

    public void A0R(TreeMap treeMap) {
        long A00 = this.A05.A00();
        for (Number number : treeMap.descendingKeySet()) {
            int intValue = number.intValue();
            Object obj = treeMap.get(Integer.valueOf(intValue));
            AnonymousClass009.A05(obj);
            this.A0O.A09((AbstractC14640lm) obj, A00 - ((long) intValue));
        }
    }

    public boolean A0S(AnonymousClass1PB r5) {
        AnonymousClass1K6<C40761sH> r3 = r5.A0I;
        if (r3.size() <= 3) {
            for (C40761sH r0 : r3) {
                AbstractC15340mz A08 = A08(r0);
                if (A08 == null || (A08 instanceof AnonymousClass1XB)) {
                }
            }
            return true;
        }
        return false;
    }
}
