package X;

import com.whatsapp.wabloks.ui.FcsBottomsheetBaseContainer;

/* renamed from: X.6Et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C134476Et implements AnonymousClass5VC {
    public final /* synthetic */ FcsBottomsheetBaseContainer A00;

    public /* synthetic */ C134476Et(FcsBottomsheetBaseContainer fcsBottomsheetBaseContainer) {
        this.A00 = fcsBottomsheetBaseContainer;
    }

    @Override // X.AnonymousClass5VC
    public final void AMo() {
        r0.A02.A02(r0.A03().getString("fds_observer_id")).A01(new AnonymousClass6EA(this.A00.A06, true));
    }
}
