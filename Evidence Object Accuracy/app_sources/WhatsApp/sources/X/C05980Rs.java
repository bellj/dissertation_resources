package X;

import android.content.Context;

/* renamed from: X.0Rs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05980Rs {
    public static final String A04 = C06390Tk.A01("ConstraintsCmdHandler");
    public final int A00;
    public final Context A01;
    public final C07620Zm A02;
    public final AnonymousClass0Zu A03;

    public C05980Rs(Context context, C07620Zm r5, int i) {
        this.A01 = context;
        this.A00 = i;
        this.A02 = r5;
        this.A03 = new AnonymousClass0Zu(context, null, r5.A08);
    }
}
