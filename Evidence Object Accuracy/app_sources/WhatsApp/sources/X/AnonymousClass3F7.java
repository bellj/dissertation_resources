package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3F7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3F7 {
    public final UserJid A00;
    public final String A01;
    public final String A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass3F7) {
                AnonymousClass3F7 r5 = (AnonymousClass3F7) obj;
                if (!C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A01, r5.A01) || !C16700pc.A0O(this.A00, r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public AnonymousClass3F7(UserJid userJid, String str, String str2) {
        C16700pc.A0E(userJid, 3);
        this.A02 = str;
        this.A01 = str2;
        this.A00 = userJid;
    }

    public int hashCode() {
        return C12990iw.A08(this.A00, ((this.A02.hashCode() * 31) + this.A01.hashCode()) * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CatalogCategoryTabItem(tabName=");
        A0k.append(this.A02);
        A0k.append(", categoryId=");
        A0k.append(this.A01);
        A0k.append(", bizJid=");
        return C12960it.A0a(this.A00, A0k);
    }
}
