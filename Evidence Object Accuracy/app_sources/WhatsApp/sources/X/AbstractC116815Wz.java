package X;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.common.api.internal.LifecycleCallback;

/* renamed from: X.5Wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116815Wz {
    void A5e(LifecycleCallback lifecycleCallback, String str);

    LifecycleCallback AB9(Class cls, String str);

    Activity ADs();

    void startActivityForResult(Intent intent, int i);
}
