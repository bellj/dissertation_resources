package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3gt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73843gt extends Animation {
    public final /* synthetic */ AbstractC28551Oa A00;

    public /* synthetic */ C73843gt(AbstractC28551Oa r1) {
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        AbstractC28551Oa r1 = this.A00;
        r1.A00 = 1.0f - f;
        r1.invalidate();
    }
}
