package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/* renamed from: X.61L  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61L {
    public static final char[] A00 = "0123456789abcdef".toCharArray();

    public static C1310460z A00(C130125yq r7) {
        C127385uP A01 = r7.A01();
        if (A01 == null) {
            Log.e("PAY: getEncryptionKeyRequestNode failed - decryptionKeyInfo null");
            return null;
        }
        String encodeToString = Base64.encodeToString(A01.A01.A02.A01, 2);
        AnonymousClass61S[] r5 = new AnonymousClass61S[4];
        int i = A01.A02 & 65535;
        AnonymousClass61S.A05("key_id", new String(new byte[]{(byte) (i >> 8), (byte) i}), r5, 0);
        r5[1] = new AnonymousClass61S("key_id_is_base64", false);
        AnonymousClass61S.A05("key_type", "X25519", r5, 2);
        return C117315Zl.A0B("encryption_key_request", C12960it.A0m(AnonymousClass61S.A00("key", encodeToString), r5, 3));
    }

    public static C1310460z A01(C130125yq r4) {
        KeyPair A02 = r4.A02();
        if (A02 == null) {
            Log.e("PAY: getSigningKeyNode failed - signingKeyPair null");
            return null;
        }
        String encodeToString = Base64.encodeToString(A02.getPublic().getEncoded(), 2);
        AnonymousClass61S[] r3 = new AnonymousClass61S[2];
        AnonymousClass61S.A05("key_type", "ECDSA_SECP256R1", r3, 0);
        return C117315Zl.A0B("signing_key_request", C12960it.A0m(AnonymousClass61S.A00("key", encodeToString), r3, 1));
    }

    public static C1310460z A02(C130125yq r8, boolean z) {
        AnonymousClass61S A002;
        C1308160b r5 = r8.A02;
        int i = r5.A00;
        int i2 = i + 240;
        char[] cArr = C1308160b.A0D;
        short s = (short) (((cArr[(byte) ((i2 >> 4) & 15)] << '\b') | cArr[(byte) (i2 & 15)]) & 65535);
        r5.A00 = (i + 1) % 16;
        C127385uP r2 = new C127385uP(C29361Rw.A00(), new Date().getTime(), s);
        r5.A0C.put(Short.valueOf(s), r2);
        String encodeToString = Base64.encodeToString(r2.A01.A02.A01, 2);
        AnonymousClass61S[] r4 = new AnonymousClass61S[4];
        int i3 = r2.A02 & 65535;
        AnonymousClass61S.A05("key_id", new String(new byte[]{(byte) (i3 >> 8), (byte) i3}), r4, 0);
        if (z) {
            A002 = new AnonymousClass61S("key_id_is_base64", false);
        } else {
            A002 = AnonymousClass61S.A00("key_id_is_base64", "false");
        }
        r4[1] = A002;
        AnonymousClass61S.A05("key_type", "X25519", r4, 2);
        return C117315Zl.A0B("encryption_key_request", C12960it.A0m(AnonymousClass61S.A00("key", encodeToString), r4, 3));
    }

    public static byte[] A03(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            throw C117315Zl.A0J(e);
        }
    }
}
