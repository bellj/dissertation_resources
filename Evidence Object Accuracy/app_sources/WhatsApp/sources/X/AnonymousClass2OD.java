package X;

import java.util.Arrays;

/* renamed from: X.2OD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OD extends AnonymousClass2OC {
    public final int A00;
    public final Object[] A01;

    public AnonymousClass2OD(Object[] objArr, int i) {
        this.A00 = i;
        this.A01 = objArr;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass2OD r5 = (AnonymousClass2OD) obj;
            if (this.A00 != r5.A00 || !Arrays.equals(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ((217 + this.A00) * 31) + Arrays.hashCode(this.A01);
    }
}
