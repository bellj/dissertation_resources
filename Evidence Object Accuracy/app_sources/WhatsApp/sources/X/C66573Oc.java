package X;

import android.content.Context;
import com.whatsapp.support.Remove;

/* renamed from: X.3Oc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66573Oc implements AbstractC009204q {
    public final /* synthetic */ Remove A00;

    public C66573Oc(Remove remove) {
        this.A00 = remove;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        Remove remove = this.A00;
        if (!remove.A01) {
            remove.A01 = true;
            remove.A00 = C12960it.A0R(((AnonymousClass2FL) ((AnonymousClass2FJ) remove.generatedComponent())).A1E);
        }
    }
}
