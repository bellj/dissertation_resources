package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.5nn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123745nn extends AbstractC130195yx {
    public int A00 = 0;
    public C120985h4 A01;

    public C123745nn(Context context, AnonymousClass018 r3, AnonymousClass14X r4) {
        super(context, r3, r4, 3);
    }

    @Override // X.AbstractC130195yx
    public CharSequence A03() {
        AbstractC30791Yv A00 = super.A01.A00();
        Context context = this.A05;
        return A00.AA7(context, C12960it.A0X(context, super.A03(), C12970iu.A1b(), 0, R.string.payments_history_amount_credited));
    }

    @Override // X.AbstractC130195yx
    public void A06(AnonymousClass1IR r4) {
        super.A06(r4);
        AbstractC1316063k r2 = this.A02.A01;
        AnonymousClass009.A05(r2);
        C120985h4 r22 = (C120985h4) r2;
        int i = 1;
        if (TextUtils.isEmpty(r22.A01)) {
            i = 2;
        }
        this.A00 = i;
        this.A01 = r22;
    }
}
