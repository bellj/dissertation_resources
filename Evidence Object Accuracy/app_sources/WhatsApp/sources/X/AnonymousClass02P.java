package X;

import java.util.Iterator;
import java.util.Map;

/* renamed from: X.02P  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass02P extends AnonymousClass016 {
    public AnonymousClass03E A00 = new AnonymousClass03E();

    @Override // X.AnonymousClass017
    public void A02() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            AnonymousClass091 r1 = (AnonymousClass091) ((Map.Entry) it.next()).getValue();
            r1.A01.A08(r1);
        }
    }

    @Override // X.AnonymousClass017
    public void A03() {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            AnonymousClass091 r1 = (AnonymousClass091) ((Map.Entry) it.next()).getValue();
            r1.A01.A09(r1);
        }
    }

    public void A0C(AnonymousClass017 r3) {
        AnonymousClass091 r1 = (AnonymousClass091) this.A00.A01(r3);
        if (r1 != null) {
            r1.A01.A09(r1);
        }
    }

    public void A0D(AnonymousClass017 r3, AnonymousClass02B r4) {
        AnonymousClass091 r1 = new AnonymousClass091(r3, r4);
        AnonymousClass091 r0 = (AnonymousClass091) this.A00.A02(r3, r1);
        if (r0 != null) {
            if (r0.A02 != r4) {
                throw new IllegalArgumentException("This source was already added with the different observer");
            }
        } else if (super.A00 > 0) {
            r1.A01.A08(r1);
        }
    }
}
