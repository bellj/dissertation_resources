package X;

import android.app.Activity;
import java.util.concurrent.Executor;

/* renamed from: X.0Rr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05970Rr {
    public AnonymousClass0PZ A00;
    public final Activity A01;
    public final AnonymousClass024 A02;
    public final Executor A03;

    public C05970Rr(Activity activity, AnonymousClass024 r2, Executor executor) {
        this.A01 = activity;
        this.A03 = executor;
        this.A02 = r2;
    }

    public static final void A00(C05970Rr r1, AnonymousClass0PZ r2) {
        C16700pc.A0E(r1, 0);
        C16700pc.A0E(r2, 1);
        r1.A02.accept(r2);
    }
}
