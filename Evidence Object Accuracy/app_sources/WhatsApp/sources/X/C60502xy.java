package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.TextAndDateLayout;

/* renamed from: X.2xy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60502xy extends AnonymousClass1OY {
    public boolean A00;
    public final View A01 = AnonymousClass028.A0D(this, R.id.action_text);
    public final View A02 = AnonymousClass028.A0D(this, R.id.button_div);
    public final View A03 = AnonymousClass028.A0D(this, R.id.expired_invitation_container);
    public final View A04 = AnonymousClass028.A0D(this, R.id.view_contacts_btn);
    public final ImageView A05 = C12970iu.A0K(this, R.id.avatar);
    public final TextEmojiLabel A06 = C12970iu.A0T(this, R.id.group_invite_caption);
    public final TextEmojiLabel A07 = C12970iu.A0T(this, R.id.instructions);
    public final TextEmojiLabel A08 = C12970iu.A0T(this, R.id.group_name);
    public final TextAndDateLayout A09 = ((TextAndDateLayout) AnonymousClass028.A0D(this, R.id.text_and_date));
    public final AbstractC41521tf A0A = new C1115259s(this);

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return 0;
    }

    public C60502xy(Context context, AbstractC13890kV r3, C28581Od r4) {
        super(context, r3, r4);
        A0Z();
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        if (r3.A07 != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M() {
        /*
            r10 = this;
            X.0mz r3 = r10.A0O
            X.1Od r3 = (X.C28581Od) r3
            com.whatsapp.TextEmojiLabel r1 = r10.A08
            java.lang.String r0 = r3.A05
            r1.setText(r0)
            X.C27531Hw.A06(r1)
            java.lang.String r2 = r3.A04
            X.0m7 r0 = r10.A0k
            long r6 = r0.A00()
            long r4 = r3.A01
            r0 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r0
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0024
            boolean r0 = r3.A07
            r9 = 0
            if (r0 == 0) goto L_0x0025
        L_0x0024:
            r9 = 1
        L_0x0025:
            boolean r0 = android.text.TextUtils.isEmpty(r2)
            r5 = 8
            r4 = 0
            com.whatsapp.TextEmojiLabel r1 = r10.A06
            if (r0 != 0) goto L_0x00b8
            r10.setMessageText(r2, r1, r3)
            android.view.View r1 = r10.A02
            r0 = 0
            if (r9 == 0) goto L_0x003a
            r0 = 8
        L_0x003a:
            r1.setVisibility(r0)
        L_0x003d:
            com.whatsapp.components.TextAndDateLayout r8 = r10.A09
            if (r8 == 0) goto L_0x0060
            boolean r7 = android.text.TextUtils.isEmpty(r2)
            r6 = 7
            r1 = -2
            r0 = -1
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams
            r2.<init>(r0, r1)
            r1 = 2131363995(0x7f0a089b, float:1.8347815E38)
            if (r7 == 0) goto L_0x00ad
            r2.addRule(r6, r1)
            r2.addRule(r5, r1)
            r8.setLayoutParams(r2)
            android.view.ViewGroup r0 = r10.A05
            X.C12970iu.A1F(r0)
        L_0x0060:
            r0 = 35
            com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1 r1 = new com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1
            r1.<init>(r10, r0, r3)
            if (r9 == 0) goto L_0x009d
            r0 = 0
            r10.setOnClickListener(r0)
            r10.setClickable(r4)
            android.view.View r0 = r10.A04
            r0.setVisibility(r5)
            android.view.View r0 = r10.A03
        L_0x0077:
            r0.setVisibility(r4)
            X.0wC r1 = r10.A11
            X.0mz r0 = r10.A0O
            X.1Od r0 = (X.C28581Od) r0
            int r0 = r0.A00
            boolean r2 = r1.A0W(r0)
            com.whatsapp.TextEmojiLabel r1 = r10.A07
            r0 = 2131888748(0x7f120a6c, float:1.941214E38)
            if (r2 == 0) goto L_0x0090
            r0 = 2131890164(0x7f120ff4, float:1.9415012E38)
        L_0x0090:
            r1.setText(r0)
            X.19O r2 = r10.A1O
            android.widget.ImageView r1 = r10.A05
            X.1tf r0 = r10.A0A
            r2.A07(r1, r3, r0)
            return
        L_0x009d:
            r10.setOnClickListener(r1)
            android.view.View r0 = r10.A01
            r0.setOnClickListener(r1)
            android.view.View r0 = r10.A03
            r0.setVisibility(r5)
            android.view.View r0 = r10.A04
            goto L_0x0077
        L_0x00ad:
            r2.addRule(r6, r1)
            r0 = 3
            r2.addRule(r0, r1)
            r8.setLayoutParams(r2)
            goto L_0x0060
        L_0x00b8:
            java.lang.String r0 = ""
            r10.setMessageText(r0, r1, r3)
            android.view.View r0 = r10.A02
            r0.setVisibility(r5)
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60502xy.A1M():void");
    }

    @Override // X.AbstractC28551Oa
    public C28581Od getFMessage() {
        return (C28581Od) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_group_invite_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_group_invite_right;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C28581Od);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
