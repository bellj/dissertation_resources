package X;

import android.text.TextUtils;
import androidx.sharetarget.ShortcutInfoCompatSaverImpl;
import java.util.List;
import org.xmlpull.v1.XmlSerializer;

/* renamed from: X.0eA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10180eA implements Runnable {
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A00;
    public final /* synthetic */ List A01;

    public RunnableC10180eA(ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl, List list) {
        this.A00 = shortcutInfoCompatSaverImpl;
        this.A01 = list;
    }

    public static void A00(String str, String str2, XmlSerializer xmlSerializer) {
        if (!TextUtils.isEmpty(str2)) {
            xmlSerializer.attribute(null, str, str2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:77:0x01b1 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 508
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC10180eA.run():void");
    }
}
