package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1gy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34951gy implements AbstractC33171dZ {
    @Override // X.AbstractC33171dZ
    public boolean A9v(AbstractC14640lm r2) {
        return r2 instanceof UserJid;
    }
}
