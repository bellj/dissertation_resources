package X;

import android.content.Context;
import android.os.Build;
import java.io.File;
import java.util.HashMap;

/* renamed from: X.0TT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TT {
    public static final String A00 = C06390Tk.A01("WrkDbPathHelper");
    public static final String[] A01 = {"-journal", "-shm", "-wal"};

    public static File A00(Context context) {
        return new File(context.getNoBackupFilesDir(), "androidx.work.workdb");
    }

    public static void A01(Context context) {
        String str;
        File A002;
        File databasePath = context.getDatabasePath("androidx.work.workdb");
        if (Build.VERSION.SDK_INT >= 23 && databasePath.exists()) {
            C06390Tk A003 = C06390Tk.A00();
            String str2 = A00;
            A003.A02(str2, "Migrating WorkDatabase to the no-backup directory", new Throwable[0]);
            HashMap hashMap = new HashMap();
            int i = Build.VERSION.SDK_INT;
            if (i >= 23) {
                File databasePath2 = context.getDatabasePath("androidx.work.workdb");
                if (i < 23) {
                    A002 = context.getDatabasePath("androidx.work.workdb");
                } else {
                    A002 = A00(context);
                }
                hashMap.put(databasePath2, A002);
                String[] strArr = A01;
                for (String str3 : strArr) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(databasePath2.getPath());
                    sb.append(str3);
                    File file = new File(sb.toString());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(A002.getPath());
                    sb2.append(str3);
                    hashMap.put(file, new File(sb2.toString()));
                }
            }
            for (File file2 : hashMap.keySet()) {
                File file3 = (File) hashMap.get(file2);
                if (file2.exists() && file3 != null) {
                    if (file3.exists()) {
                        C06390Tk.A00().A05(str2, String.format("Over-writing contents of %s", file3), new Throwable[0]);
                    }
                    Object[] objArr = {file2, file3};
                    if (file2.renameTo(file3)) {
                        str = "Migrated %s to %s";
                    } else {
                        str = "Renaming %s to %s failed";
                    }
                    C06390Tk.A00().A02(str2, String.format(str, objArr), new Throwable[0]);
                }
            }
        }
    }
}
