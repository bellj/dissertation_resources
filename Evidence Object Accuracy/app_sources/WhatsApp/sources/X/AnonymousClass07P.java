package X;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatSpinner;
import java.lang.reflect.Constructor;

/* renamed from: X.07P  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07P {
    public static final String LOG_TAG = "AppCompatViewInflater";
    public static final String[] sClassPrefixList = {"android.widget.", "android.view.", "android.webkit."};
    public static final AnonymousClass00O sConstructorMap = new AnonymousClass00O();
    public static final Class[] sConstructorSignature = {Context.class, AttributeSet.class};
    public static final int[] sOnClickAttrs = {16843375};
    public final Object[] mConstructorArgs = new Object[2];

    public View createView(Context context, String str, AttributeSet attributeSet) {
        return null;
    }

    private void checkOnClickListener(View view, AttributeSet attributeSet) {
        Context context = view.getContext();
        if ((context instanceof ContextWrapper) && view.hasOnClickListeners()) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, sOnClickAttrs);
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                view.setOnClickListener(new AnonymousClass0WM(view, string));
            }
            obtainStyledAttributes.recycle();
        }
    }

    public AnonymousClass07K createAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        return new AnonymousClass07K(context, attributeSet);
    }

    public AnonymousClass0BS createButton(Context context, AttributeSet attributeSet) {
        return new AnonymousClass0BS(context, attributeSet);
    }

    public AppCompatCheckBox createCheckBox(Context context, AttributeSet attributeSet) {
        return new AppCompatCheckBox(context, attributeSet);
    }

    public AnonymousClass0BU createCheckedTextView(Context context, AttributeSet attributeSet) {
        return new AnonymousClass0BU(context, attributeSet);
    }

    public AnonymousClass011 createEditText(Context context, AttributeSet attributeSet) {
        return new AnonymousClass011(context, attributeSet);
    }

    public AnonymousClass08i createImageButton(Context context, AttributeSet attributeSet) {
        return new AnonymousClass08i(context, attributeSet);
    }

    public AnonymousClass03X createImageView(Context context, AttributeSet attributeSet) {
        return new AnonymousClass03X(context, attributeSet);
    }

    public C02370Bu createMultiAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        return new C02370Bu(context, attributeSet);
    }

    public AppCompatRadioButton createRadioButton(Context context, AttributeSet attributeSet) {
        return new AppCompatRadioButton(context, attributeSet);
    }

    public C02380Bx createRatingBar(Context context, AttributeSet attributeSet) {
        return new C02380Bx(context, attributeSet);
    }

    public AppCompatSeekBar createSeekBar(Context context, AttributeSet attributeSet) {
        return new AppCompatSeekBar(context, attributeSet);
    }

    public AppCompatSpinner createSpinner(Context context, AttributeSet attributeSet) {
        return new AppCompatSpinner(context, attributeSet);
    }

    public C004602b createTextView(Context context, AttributeSet attributeSet) {
        return new C004602b(context, attributeSet);
    }

    public AnonymousClass0C1 createToggleButton(Context context, AttributeSet attributeSet) {
        return new AnonymousClass0C1(context, attributeSet);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        if (r0 != null) goto L_0x0026;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View createView(android.view.View r3, java.lang.String r4, android.content.Context r5, android.util.AttributeSet r6, boolean r7, boolean r8, boolean r9, boolean r10) {
        /*
        // Method dump skipped, instructions count: 308
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07P.createView(android.view.View, java.lang.String, android.content.Context, android.util.AttributeSet, boolean, boolean, boolean, boolean):android.view.View");
    }

    private View createViewByPrefix(Context context, String str, String str2) {
        String obj;
        AnonymousClass00O r3 = sConstructorMap;
        Constructor constructor = (Constructor) r3.get(str);
        if (constructor == null) {
            if (str2 != null) {
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str2);
                    sb.append(str);
                    obj = sb.toString();
                } catch (Exception unused) {
                    return null;
                }
            } else {
                obj = str;
            }
            constructor = Class.forName(obj, false, context.getClassLoader()).asSubclass(View.class).getConstructor(sConstructorSignature);
            r3.put(str, constructor);
        }
        constructor.setAccessible(true);
        return (View) constructor.newInstance(this.mConstructorArgs);
    }

    private View createViewFromTag(Context context, String str, AttributeSet attributeSet) {
        View view;
        if (str.equals("view")) {
            str = attributeSet.getAttributeValue(null, "class");
        }
        try {
            Object[] objArr = this.mConstructorArgs;
            objArr[0] = context;
            objArr[1] = attributeSet;
            if (-1 == str.indexOf(46)) {
                int i = 0;
                while (true) {
                    String[] strArr = sClassPrefixList;
                    if (i < strArr.length) {
                        view = createViewByPrefix(context, str, strArr[i]);
                        if (view != null) {
                            break;
                        }
                        i++;
                    } else {
                        return null;
                    }
                }
            } else {
                view = createViewByPrefix(context, str, null);
            }
            return view;
        } catch (Exception unused) {
            return null;
        } finally {
            Object[] objArr2 = this.mConstructorArgs;
            objArr2[0] = null;
            objArr2[1] = null;
        }
    }

    public static Context themifyContext(Context context, AttributeSet attributeSet, boolean z, boolean z2) {
        int i;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass07O.A0O, 0, 0);
        if (z) {
            i = obtainStyledAttributes.getResourceId(0, 0);
        } else {
            i = 0;
        }
        if (z2 && i == 0 && (i = obtainStyledAttributes.getResourceId(4, 0)) != 0) {
            Log.i(LOG_TAG, "app:theme is now deprecated. Please move to using android:theme instead.");
        }
        obtainStyledAttributes.recycle();
        if (i == 0 || ((context instanceof AnonymousClass062) && ((AnonymousClass062) context).A00 == i)) {
            return context;
        }
        return new AnonymousClass062(context, i);
    }

    private void verifyNotNull(View view, String str) {
        if (view == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(getClass().getName());
            sb.append(" asked to inflate view for <");
            sb.append(str);
            sb.append(">, but returned null");
            throw new IllegalStateException(sb.toString());
        }
    }
}
