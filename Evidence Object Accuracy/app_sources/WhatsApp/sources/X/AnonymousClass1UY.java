package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.1UY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UY {
    public static final boolean A00 = C28391Mz.A03();
    public static final boolean A01 = C28391Mz.A05();

    public static PendingIntent A00(Context context, int i, Intent intent, int i2) {
        if (A00) {
            i2 |= 67108864;
        }
        return PendingIntent.getActivity(context, i, intent, i2);
    }

    public static PendingIntent A01(Context context, int i, Intent intent, int i2) {
        if (A00) {
            i2 |= 67108864;
        }
        return PendingIntent.getBroadcast(context, i, intent, i2);
    }

    public static PendingIntent A02(Context context, Intent intent) {
        int i = 0;
        if (A00) {
            i = 67108864;
        }
        return PendingIntent.getForegroundService(context, 0, intent, i);
    }

    public static PendingIntent A03(Context context, Intent intent, int i) {
        if (A00) {
            i |= 67108864;
        }
        return PendingIntent.getService(context, 0, intent, i);
    }
}
