package X;

import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.ECGenParameterSpec;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.crypto.KeyAgreement;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5gy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120925gy extends AbstractC129485xn {
    public String A00;
    public final C130705zq A01;
    public final JniBridge A02;
    public final JSONObject A03;
    public final JSONObject A04;
    public final byte[] A05 = C003501n.A0D(12);

    public C120925gy(C130705zq r2, JniBridge jniBridge, JSONObject jSONObject, JSONObject jSONObject2) {
        AnonymousClass009.A0E(C12960it.A1U(jSONObject2.length()));
        this.A02 = jniBridge;
        this.A01 = r2;
        this.A04 = jSONObject;
        this.A03 = jSONObject2;
    }

    @Override // X.AbstractC129485xn
    public AbstractC129485xn A00(PublicKey... publicKeyArr) {
        AnonymousClass009.A0E(C12980iv.A1X(this.A00));
        super.A00(publicKeyArr);
        try {
            String encodeToString = Base64.encodeToString(C117315Zl.A0a(this.A04.toString()), 11);
            byte[] bArr = this.A05;
            String encodeToString2 = Base64.encodeToString(bArr, 11);
            ECGenParameterSpec eCGenParameterSpec = new ECGenParameterSpec("secp256r1");
            KeyPairGenerator instance = KeyPairGenerator.getInstance("EC");
            instance.initialize(eCGenParameterSpec);
            KeyPair generateKeyPair = instance.generateKeyPair();
            PublicKey publicKey = generateKeyPair.getPublic();
            JSONObject A0a = C117295Zj.A0a();
            JSONObject put = A0a.put("alg", "ECDH-ES").put("enc", "A256GCM");
            List list = super.A00;
            AnonymousClass009.A05(list);
            Collections.sort(list);
            put.put("apu", Base64.encodeToString(C117315Zl.A0a(TextUtils.join(";", list)), 11)).put("apv", Base64.encodeToString(C117315Zl.A0a(C130255z3.A00(null)), 11)).put("epk", C117295Zj.A0a().put("kty", "EC").put("crv", "P-256").put("der", C117305Zk.A0n(publicKey.getEncoded())));
            String encodeToString3 = Base64.encodeToString(C117315Zl.A0a(A0a.toString()), 11);
            byte[] bytes = TextUtils.join(".", new String[]{encodeToString3, encodeToString}).getBytes();
            PrivateKey privateKey = generateKeyPair.getPrivate();
            KeyAgreement instance2 = KeyAgreement.getInstance("ECDH");
            instance2.init(privateKey);
            instance2.doPhase(null, true);
            byte[] generateSecret = instance2.generateSecret();
            List list2 = super.A00;
            AnonymousClass009.A05(list2);
            Collections.sort(list2);
            byte[] bytes2 = TextUtils.join(";", list2).getBytes();
            byte[] bytes3 = C130255z3.A00(null).getBytes();
            boolean z = true;
            byte[] A03 = C16050oM.A03(1);
            byte[] A0a2 = C117315Zl.A0a("A256GCM");
            byte[] A032 = C16050oM.A03(256);
            int length = generateSecret.length;
            int i = length + 4;
            int i2 = i + 4;
            int length2 = A0a2.length;
            int i3 = i2 + length2;
            int i4 = i3 + 4;
            int length3 = bytes2.length;
            int i5 = i4 + length3;
            int i6 = i5 + 4;
            int length4 = bytes3.length;
            int i7 = i6 + length4;
            int i8 = i7 + 4;
            byte[] bArr2 = new byte[i8];
            System.arraycopy(A03, 0, bArr2, 0, 4);
            System.arraycopy(generateSecret, 0, bArr2, 4, length);
            System.arraycopy(C16050oM.A03(length2), 0, bArr2, i, 4);
            System.arraycopy(A0a2, 0, bArr2, i2, length2);
            System.arraycopy(C16050oM.A03(length3), 0, bArr2, i3, 4);
            System.arraycopy(bytes2, 0, bArr2, i4, length3);
            System.arraycopy(C16050oM.A03(length4), 0, bArr2, i5, 4);
            System.arraycopy(bytes3, 0, bArr2, i6, length4);
            System.arraycopy(A032, 0, bArr2, i7, 4);
            if (i8 != i8) {
                z = false;
            }
            AnonymousClass009.A0B("length doesn't match", z);
            byte[] bArr3 = (byte[]) JniBridge.jvidispatchOIOOOO(4, (long) 16, MessageDigest.getInstance("SHA-256").digest(bArr2), bArr, this.A03.toString().getBytes(), bytes);
            if (bArr3 == null) {
                throw new C124785q7(C12960it.A0U("cipher failed"));
            }
            int length5 = bArr3.length;
            int i9 = length5 - 16;
            AnonymousClass01T A05 = C117315Zl.A05(Arrays.copyOfRange(bArr3, 0, i9), Arrays.copyOfRange(bArr3, i9, length5));
            Object obj = A05.A00;
            AnonymousClass009.A05(obj);
            String encodeToString4 = Base64.encodeToString((byte[]) obj, 11);
            Object obj2 = A05.A01;
            AnonymousClass009.A05(obj2);
            this.A00 = TextUtils.join(".", new String[]{encodeToString, encodeToString3, "", encodeToString2, encodeToString4, Base64.encodeToString((byte[]) obj2, 11)});
            return this;
        } catch (UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | JSONException e) {
            Log.w("PAY: EncryptedTrustTokenBuilder/declareSigningKeys", e);
            throw new C124785q7(e);
        }
    }
}
