package X;

import android.content.Context;

/* renamed from: X.5g6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120385g6 extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C14830m7 A02;
    public final C18650sn A03;
    public final C18610sj A04;

    public C120385g6(Context context, C14900mE r3, C14830m7 r4, C1308460e r5, C18650sn r6, C18610sj r7) {
        super(r5.A04, r7);
        this.A00 = context;
        this.A02 = r4;
        this.A01 = r3;
        this.A04 = r7;
        this.A03 = r6;
    }
}
