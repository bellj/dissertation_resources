package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0Eu  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0Eu extends AbstractC06200So {
    public int A00 = 3;

    @Override // X.AbstractC06200So
    public int A01(AnonymousClass03U r5, RecyclerView recyclerView) {
        int i = this.A00;
        return (i << 16) | (0 << 8) | ((0 | i) << 0);
    }
}
