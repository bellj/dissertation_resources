package X;

import android.os.Bundle;
import android.os.Parcelable;
import com.whatsapp.fieldstats.events.WamCall;
import com.whatsapp.fieldstats.extension.WamCallExtendedField;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.chromium.net.UrlRequest;

/* renamed from: X.0yP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22050yP {
    public long A00;
    public WamCall A01;
    public final AbstractC15710nm A02;
    public final C15570nT A03;
    public final C15450nH A04;
    public final C16240og A05;
    public final AnonymousClass181 A06;
    public final AnonymousClass01d A07;
    public final C14830m7 A08;
    public final C16590pI A09;
    public final C14820m6 A0A;
    public final C19990v2 A0B;
    public final C15600nX A0C;
    public final C22180yf A0D;
    public final C21680xo A0E;
    public final AnonymousClass15Y A0F;
    public final C16120oU A0G;
    public final AnonymousClass11M A0H;
    public final C14840m8 A0I;
    public final C16630pM A0J;
    public final AbstractC14440lR A0K;

    public C22050yP(AbstractC15710nm r2, C15570nT r3, C15450nH r4, C16240og r5, AnonymousClass181 r6, AnonymousClass01d r7, C14830m7 r8, C16590pI r9, C14820m6 r10, C19990v2 r11, C15600nX r12, C22180yf r13, C21680xo r14, AnonymousClass15Y r15, C16120oU r16, AnonymousClass11M r17, C14840m8 r18, C16630pM r19, AbstractC14440lR r20) {
        this.A09 = r9;
        this.A08 = r8;
        this.A02 = r2;
        this.A03 = r3;
        this.A0K = r20;
        this.A0B = r11;
        this.A0G = r16;
        this.A06 = r6;
        this.A04 = r4;
        this.A0E = r14;
        this.A0D = r13;
        this.A07 = r7;
        this.A05 = r5;
        this.A0H = r17;
        this.A0I = r18;
        this.A0F = r15;
        this.A0A = r10;
        this.A0C = r12;
        this.A0J = r19;
    }

    public static Bundle A00(Object obj) {
        Bundle bundle = new Bundle();
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            int modifiers = field.getModifiers();
            if (Modifier.isPublic(modifiers) && !Modifier.isStatic(modifiers)) {
                String name = field.getName();
                try {
                    Object obj2 = field.get(obj);
                    if (obj2 != null) {
                        if (obj2 instanceof Double) {
                            bundle.putDouble(name, ((Number) obj2).doubleValue());
                        } else if (obj2 instanceof Integer) {
                            bundle.putInt(name, ((Number) obj2).intValue());
                        } else if (obj2 instanceof Long) {
                            bundle.putLong(name, ((Number) obj2).longValue());
                        } else if (obj2 instanceof Boolean) {
                            bundle.putBoolean(name, ((Boolean) obj2).booleanValue());
                        } else if (obj2 instanceof String) {
                            bundle.putString(name, (String) obj2);
                        } else if (!name.equals("fields") || !(obj2 instanceof ArrayList)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("unexpected member ");
                            sb.append(name);
                            sb.append(" in fieldstats event, only Double, Integer, and String members are supported");
                            AnonymousClass009.A07(sb.toString());
                        } else {
                            ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
                            Iterator it = ((AbstractCollection) obj2).iterator();
                            while (it.hasNext()) {
                                Object next = it.next();
                                if (next instanceof WamCallExtendedField) {
                                    arrayList.add(next);
                                }
                            }
                            bundle.putParcelableArrayList(name, arrayList);
                        }
                    }
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
        return bundle;
    }

    public static Integer A01(AnonymousClass1I0 r3) {
        int i;
        if (r3 != null) {
            int i2 = r3.A00;
            if (r3.A04) {
                switch (i2) {
                    case 1:
                        i = 104;
                        break;
                    case 2:
                        i = 100;
                        break;
                    case 3:
                        i = 102;
                        break;
                    case 4:
                        i = C43951xu.A03;
                        break;
                    case 5:
                    case 6:
                    case 12:
                        i = 103;
                        break;
                    case 7:
                        i = 109;
                        break;
                    case 8:
                        i = 105;
                        break;
                    case 9:
                        i = 106;
                        break;
                    case 10:
                        i = 107;
                        break;
                    case 11:
                        i = 101;
                        break;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        i = 111;
                        break;
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        i = 110;
                        break;
                    case 15:
                        i = 112;
                        break;
                    default:
                        i = 0;
                        break;
                }
            } else if (r3.A06) {
                i = 1;
            }
            return Integer.valueOf(i);
        }
        return null;
    }

    public static void A02(Bundle bundle, Object obj) {
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            int modifiers = field.getModifiers();
            if (Modifier.isPublic(modifiers) && !Modifier.isStatic(modifiers) && !Modifier.isFinal(modifiers)) {
                try {
                    field.set(obj, bundle.get(field.getName()));
                    continue;
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

    public void A03(int i) {
        AnonymousClass1u7 r1 = new AnonymousClass1u7();
        r1.A00 = Integer.valueOf(i);
        this.A0G.A07(r1);
    }

    public void A04(int i, long j) {
        A08(null, i, 0, 0, j);
    }

    public void A05(int i, long j, long j2, long j3, long j4, long j5, boolean z, boolean z2) {
        C41781u9 r2 = new C41781u9();
        r2.A03 = Integer.valueOf(i);
        r2.A02 = Double.valueOf((double) (Math.round(((double) j) / 1000.0d) * 1000));
        r2.A07 = Long.valueOf(Math.round(((double) j2) / 1000.0d) * 1000);
        r2.A00 = Boolean.valueOf(z);
        r2.A04 = 0;
        r2.A05 = Long.valueOf(j3);
        r2.A06 = Long.valueOf(j4);
        r2.A01 = Boolean.valueOf(z2);
        r2.A08 = Long.valueOf(j5);
        this.A0G.A06(r2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0007, code lost:
        if (r5.A06 == false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.AnonymousClass1I0 r5) {
        /*
            r4 = this;
            X.15Y r3 = r4.A0F
            if (r5 == 0) goto L_0x0009
            boolean r1 = r5.A06
            r0 = 1
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r0)
            r1 = 23
            r0 = 0
            r3.A02(r2, r1, r0)
            r0 = 1
            r3.A02(r2, r1, r0)
            java.lang.Integer r2 = A01(r5)
            r1 = 105(0x69, float:1.47E-43)
            r0 = 0
            r3.A02(r2, r1, r0)
            r0 = 1
            r3.A02(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22050yP.A06(X.1I0):void");
    }

    public void A07(WamCall wamCall, boolean z) {
        long longValue;
        this.A01 = wamCall;
        Long l = wamCall.logSampleRatio;
        if (l == null) {
            longValue = 1;
        } else {
            longValue = l.longValue();
        }
        wamCall.logSampleRatio = null;
        C16120oU r1 = this.A0G;
        r1.A08(wamCall, (int) longValue);
        if (z) {
            r1.A01();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0089, code lost:
        if (r0 == 0) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.AbstractC14640lm r6, int r7, int r8, int r9, long r10) {
        /*
            r5 = this;
            X.1uA r2 = new X.1uA
            r2.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)
            r2.A02 = r0
            java.lang.Long r0 = java.lang.Long.valueOf(r10)
            r2.A05 = r0
            r4 = 3
            if (r7 != r4) goto L_0x0021
            if (r6 == 0) goto L_0x0021
            boolean r0 = r6 instanceof com.whatsapp.jid.UserJid
            if (r0 == 0) goto L_0x0071
            r3 = 1
        L_0x001b:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)
        L_0x001f:
            r2.A01 = r1
        L_0x0021:
            if (r8 <= 0) goto L_0x002f
            r0 = 32
            if (r8 > r0) goto L_0x006f
            r0 = 32
        L_0x0029:
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.A04 = r0
        L_0x002f:
            if (r9 <= 0) goto L_0x0047
            r0 = 32
            if (r9 > r0) goto L_0x006d
            r0 = 32
        L_0x0037:
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.A03 = r0
            int r0 = X.C20870wS.A00(r9)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.A00 = r0
        L_0x0047:
            X.0oU r3 = r5.A0G
            r3.A07(r2)
            X.1uB r4 = new X.1uB
            r4.<init>()
            X.0og r0 = r5.A05
            int r2 = r0.A04
            r1 = 2
            r0 = 0
            if (r2 != r1) goto L_0x005a
            r0 = 1
        L_0x005a:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r4.A00 = r0
            r3.A07(r4)
            X.0nH r1 = r5.A04
            X.0oW r0 = X.AbstractC15460nI.A0D
            int r0 = r1.A02(r0)
            monitor-enter(r3)
            goto L_0x008c
        L_0x006d:
            long r0 = (long) r9
            goto L_0x0037
        L_0x006f:
            long r0 = (long) r8
            goto L_0x0029
        L_0x0071:
            boolean r0 = r6 instanceof X.C15580nU
            r1 = 0
            if (r0 == 0) goto L_0x001f
            X.0v2 r0 = r5.A0B
            com.whatsapp.jid.GroupJid r6 = (com.whatsapp.jid.GroupJid) r6
            int r0 = r0.A03(r6)
            r3 = 4
            if (r0 == r4) goto L_0x001b
            r3 = 2
            if (r0 != r3) goto L_0x0089
            java.lang.Integer r1 = java.lang.Integer.valueOf(r4)
            goto L_0x001f
        L_0x0089:
            if (r0 != 0) goto L_0x001f
            goto L_0x001b
        L_0x008c:
            r3.A0L = r0     // Catch: all -> 0x00b3
            monitor-exit(r3)     // Catch: all -> 0x00b3
            boolean r0 = r3.A0O
            if (r0 == 0) goto L_0x00b2
            X.1uC r2 = new X.1uC
            r2.<init>()
            r0 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            r2.A00 = r0
            r0 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.A01 = r0
            r3.A07(r2)
            X.1uD r0 = new X.1uD
            r0.<init>()
            r3.A07(r0)
        L_0x00b2:
            return
        L_0x00b3:
            r0 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x00b3
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22050yP.A08(X.0lm, int, int, int, long):void");
    }

    public void A09(AbstractC15340mz r8, int i) {
        int i2;
        long j;
        long j2;
        int i3;
        C41751u3 r2 = new C41751u3();
        r2.A03 = Integer.valueOf(i);
        AbstractC14640lm r5 = r8.A0z.A00;
        if (C15380n4.A0J(r5)) {
            i2 = 2;
        } else if (C15380n4.A0N(r5)) {
            i2 = 3;
        } else {
            i2 = 1;
            if (C15380n4.A0G(r5)) {
                i2 = 4;
            }
        }
        r2.A05 = Integer.valueOf(i2);
        r2.A09 = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(this.A08.A00() - r8.A0G));
        byte b = r8.A0y;
        int i4 = 1;
        if (b != 11) {
            i4 = 0;
            if (b == 31) {
                i4 = 2;
            }
        }
        r2.A06 = Integer.valueOf(i4);
        r2.A02 = Integer.valueOf(C20870wS.A04(r8));
        r2.A01 = Integer.valueOf(C20870wS.A01(this.A0D, r8));
        if (i == 1 && (r8 instanceof C30371Xd)) {
            switch (((C30371Xd) r8).A00) {
                case 1:
                    i3 = 1;
                    break;
                case 2:
                    i3 = 4;
                    break;
                case 3:
                    i3 = 5;
                    break;
                case 4:
                    i3 = 6;
                    break;
                case 5:
                    i3 = 2;
                    break;
                case 6:
                    i3 = 7;
                    break;
                case 7:
                    i3 = 3;
                    break;
                case 8:
                    i3 = 8;
                    break;
                case 9:
                    i3 = 9;
                    break;
                default:
                    i3 = 0;
                    break;
            }
            r2.A04 = Integer.valueOf(i3);
        }
        Set A04 = this.A0C.A04(r5);
        int size = C15380n4.A09(this.A02, A04).size();
        int size2 = A04.size();
        if (size > 0) {
            if (size <= 32) {
                j2 = 32;
            } else {
                j2 = (long) size;
            }
            r2.A08 = Long.valueOf(j2);
        }
        if (size2 > 0) {
            if (size2 <= 32) {
                j = 32;
            } else {
                j = (long) size2;
            }
            r2.A07 = Long.valueOf(j);
            r2.A00 = Integer.valueOf(C20870wS.A00(size2));
        }
        this.A0G.A06(r2);
    }

    public void A0A(Double d, Long l, int i, int i2) {
        C41771u8 r1 = new C41771u8();
        r1.A01 = Integer.valueOf(i);
        r1.A02 = Integer.valueOf(i2);
        r1.A00 = d;
        r1.A03 = l;
        this.A0G.A07(r1);
    }

    public void A0B(Integer num, Integer num2, Long l, String str) {
        AnonymousClass1u4 r1 = new AnonymousClass1u4();
        r1.A03 = str;
        r1.A01 = num2;
        r1.A00 = num;
        r1.A02 = l;
        this.A0G.A05(r1);
    }

    public void A0C(String str, int i) {
        C41761u5 r1 = new C41761u5();
        r1.A09 = str;
        r1.A05 = Integer.valueOf(i);
        r1.A03 = 1;
        this.A0G.A05(r1);
    }

    public void A0D(String str, int i) {
        C41761u5 r1 = new C41761u5();
        r1.A09 = str;
        r1.A05 = 12;
        r1.A04 = Integer.valueOf(i);
        r1.A03 = 1;
        this.A0G.A05(r1);
    }

    public void A0E(String str, int i) {
        C41761u5 r1 = new C41761u5();
        r1.A09 = str;
        r1.A05 = 4;
        r1.A04 = Integer.valueOf(i);
        r1.A03 = 1;
        this.A0G.A05(r1);
    }

    public void A0F(boolean z, int i) {
        AnonymousClass1u6 r2 = new AnonymousClass1u6();
        int i2 = 1;
        if (!z) {
            i2 = 2;
            if (i > 1) {
                i2 = 3;
            }
        }
        r2.A00 = Integer.valueOf(i2);
        r2.A01 = Long.valueOf((long) i);
        this.A0G.A06(r2);
    }
}
