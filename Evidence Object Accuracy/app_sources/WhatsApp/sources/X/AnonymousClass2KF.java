package X;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.2KF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2KF extends Handler {
    public final WeakReference A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AnonymousClass2KF(Looper looper, WeakReference weakReference) {
        super(looper);
        AnonymousClass009.A05(looper);
        this.A00 = weakReference;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        Activity activity = (Activity) this.A00.get();
        if (activity == null) {
            removeMessages(1);
        } else if (message.what == 1) {
            Log.e("verifymsgstorehelper/timeout");
            removeMessages(1);
            C36021jC.A01(activity, 200);
        }
    }
}
