package X;

import com.whatsapp.Conversation;
import com.whatsapp.util.Log;

/* renamed from: X.19B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19B extends AbstractC16220oe {
    public void A05() {
        Log.i("messageaudio/play");
        for (AnonymousClass2P3 r0 : A01()) {
            Conversation conversation = r0.A00;
            AbstractC14670lq r1 = conversation.A3y;
            if (r1 != null) {
                if (r1.A0P != null) {
                    r1.A03();
                } else {
                    AbstractC28651Ol r02 = r1.A0N;
                    if (r02 != null && r02.A0D()) {
                        conversation.A3y.A04();
                    }
                }
            }
            AbstractC35401hl r12 = conversation.A3t;
            if (r12 != null && r12.ADU()) {
                r12.AYz();
            }
        }
    }
}
