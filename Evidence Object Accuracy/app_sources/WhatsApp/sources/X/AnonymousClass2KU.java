package X;

import android.util.Pair;

/* renamed from: X.2KU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2KU {
    public final C246516i A00 = new C246516i(20);

    public Pair A00(String str) {
        Pair pair;
        C246516i r2 = this.A00;
        synchronized (r2) {
            if (r2.containsKey(str)) {
                AnonymousClass2O6 r1 = (AnonymousClass2O6) r2.get(str);
                boolean z = false;
                if (r1 == null) {
                    z = true;
                }
                pair = Pair.create(Boolean.valueOf(z), r1);
            } else {
                pair = null;
            }
        }
        return pair;
    }

    public void A01(String str) {
        C246516i r1 = this.A00;
        synchronized (r1) {
            r1.put(str, null);
        }
    }
}
