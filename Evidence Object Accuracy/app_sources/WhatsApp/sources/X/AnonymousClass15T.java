package X;

/* renamed from: X.15T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15T {
    public final C14830m7 A00;
    public final AnonymousClass15S A01;
    public final AnonymousClass15Z A02;
    public final AnonymousClass15R A03;
    public final C21710xr A04;

    public AnonymousClass15T(C14830m7 r1, AnonymousClass15S r2, AnonymousClass15Z r3, AnonymousClass15R r4, C21710xr r5) {
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
    }
}
