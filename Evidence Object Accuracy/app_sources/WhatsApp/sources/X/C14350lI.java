package X;

import android.content.ContentResolver;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import androidx.core.content.FileProvider;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: X.0lI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C14350lI {
    public static long A00(C33491eA r11, File file) {
        long j;
        String str;
        if (!file.exists()) {
            return 0;
        }
        if (!file.isDirectory()) {
            return file.length();
        }
        LinkedList linkedList = new LinkedList();
        linkedList.add(file);
        long j2 = 0;
        while (linkedList.peek() != null) {
            Object poll = linkedList.poll();
            AnonymousClass009.A05(poll);
            File[] listFiles = ((File) poll).listFiles();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    if (file2.exists()) {
                        if (file2.isDirectory()) {
                            linkedList.add(file2);
                        } else {
                            if (r11 != null) {
                                AnonymousClass28Q r3 = r11.A00;
                                try {
                                    str = file2.getCanonicalPath();
                                } catch (IOException e) {
                                    StringBuilder sb = new StringBuilder("gdrive-util/should-backup/");
                                    sb.append(file2);
                                    Log.e(sb.toString(), e);
                                    str = null;
                                }
                                if (!Boolean.valueOf(r3.A00(file2, str)).booleanValue()) {
                                    j = 0;
                                    j2 += j;
                                }
                            }
                            j = file2.length();
                            j2 += j;
                        }
                    }
                }
            }
        }
        return j2;
    }

    public static Uri A01(Context context, File file) {
        if (Build.VERSION.SDK_INT < 23) {
            return Uri.fromFile(file);
        }
        return FileProvider.A00(context, file, AnonymousClass01V.A05);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:54:0x0166 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:58:0x0185 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.StringBuilder */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v0, types: [int] */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.util.Pair A02(X.C21780xy r14, java.io.File r15, int r16, int r17) {
        /*
        // Method dump skipped, instructions count: 443
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14350lI.A02(X.0xy, java.io.File, int, int):android.util.Pair");
    }

    public static File A03(Uri uri) {
        if (uri.getPath() == null || (uri.getScheme() != null && !"file".equals(uri.getScheme()))) {
            return null;
        }
        return new File(uri.getPath());
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e4 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00f6 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0113 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00ec A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00a5 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x009a A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File A04(java.io.File r13, java.io.File r14, java.lang.String r15) {
        /*
        // Method dump skipped, instructions count: 303
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14350lI.A04(java.io.File, java.io.File, java.lang.String):java.io.File");
    }

    public static File A05(String str, String str2) {
        File file;
        try {
            file = new File(str, str2);
        } catch (IOException unused) {
        }
        if (file.getCanonicalPath().startsWith(str)) {
            return file;
        }
        return null;
    }

    public static String A06(File file, MessageDigest messageDigest, long j) {
        if (!file.exists() || j > file.length()) {
            return null;
        }
        messageDigest.reset();
        byte[] bArr = new byte[4096];
        long j2 = 0;
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            int i = 0;
            while (true) {
                if (i == -1) {
                    break;
                }
                i = bufferedInputStream.read(bArr, 0, 4096);
                if (i > 0) {
                    long j3 = ((long) i) + j2;
                    if (j3 >= j) {
                        messageDigest.update(bArr, 0, (int) (j - j2));
                        break;
                    }
                    messageDigest.update(bArr, 0, i);
                    j2 = j3;
                }
            }
            String A04 = C003501n.A04(messageDigest.digest());
            bufferedInputStream.close();
            fileInputStream.close();
            return A04;
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public static String A07(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        return (str.lastIndexOf(File.separator) > lastIndexOf || lastIndexOf < 0) ? "" : str.substring(lastIndexOf + 1);
    }

    public static String A08(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        int lastIndexOf2 = str.lastIndexOf(File.separator);
        if (lastIndexOf2 > lastIndexOf) {
            lastIndexOf = -1;
        }
        if (lastIndexOf2 < 0) {
            lastIndexOf2 = 0;
        }
        if (lastIndexOf >= 0) {
            return str.substring(lastIndexOf2, lastIndexOf);
        }
        return str.substring(lastIndexOf2);
    }

    public static void A09(C14950mJ r24, File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            long j = 0;
            for (File file2 : listFiles) {
                j += file2.length();
            }
            if (j > 0) {
                ArrayList arrayList = new ArrayList(Arrays.asList(listFiles));
                Collections.sort(arrayList, new Comparator() { // from class: X.28P
                    @Override // java.util.Comparator
                    public final int compare(Object obj, Object obj2) {
                        return (((File) obj).lastModified() > ((File) obj2).lastModified() ? 1 : (((File) obj).lastModified() == ((File) obj2).lastModified() ? 0 : -1));
                    }
                });
                long A01 = r24.A01();
                long currentTimeMillis = System.currentTimeMillis();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    File file3 = (File) it.next();
                    long lastModified = file3.lastModified();
                    if (currentTimeMillis - lastModified >= 604800000 || j > 20000000 || (j > 2000000 && 50000000 > A01 && A01 > 0)) {
                        long length = file3.length();
                        StringBuilder sb = new StringBuilder("cleanup/");
                        sb.append(file3.getName());
                        sb.append(":");
                        sb.append(currentTimeMillis);
                        sb.append(" - ");
                        sb.append(lastModified);
                        sb.append(" fileLength=");
                        sb.append(length);
                        sb.append(" directoryLengthBeforeCleanup=");
                        sb.append(j);
                        sb.append(" storageAvailableBeforeCleanup=");
                        sb.append(A01);
                        Log.i(sb.toString());
                        if (!file3.delete()) {
                            StringBuilder sb2 = new StringBuilder("cleanup/failed to delete ");
                            sb2.append(file3.getName());
                            Log.i(sb2.toString());
                        } else {
                            j -= length;
                            A01 += length;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public static void A0A(C15690nk r3, File file, File file2) {
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            r3.A05(fileInputStream);
            File parentFile = file2.getParentFile();
            if (parentFile != null) {
                parentFile.mkdirs();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            A0G(fileInputStream, fileOutputStream);
            fileOutputStream.close();
            fileInputStream.close();
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public static void A0B(C15690nk r1, File file, File file2) {
        if (!file.renameTo(file2)) {
            A0A(r1, file, file2);
            A0M(file);
        }
    }

    public static void A0C(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    A0C(file2);
                } else {
                    file2.getPath();
                    A0M(file2);
                }
            }
        }
        file.getPath();
        A0M(file);
    }

    public static void A0D(File file, long j) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (System.currentTimeMillis() - file2.lastModified() > j) {
                    StringBuilder sb = new StringBuilder("cleanup/");
                    sb.append(file2.getName());
                    sb.append(":");
                    sb.append(System.currentTimeMillis());
                    sb.append(" - ");
                    sb.append(file2.lastModified());
                    Log.i(sb.toString());
                    if (!file2.delete()) {
                        StringBuilder sb2 = new StringBuilder("cleanup/failed to delete ");
                        sb2.append(file2.getName());
                        Log.i(sb2.toString());
                    }
                }
            }
        }
    }

    public static void A0E(File file, String str) {
        int length;
        File[] listFiles = file.getParentFile().listFiles();
        if (listFiles != null && (length = listFiles.length) > 0) {
            int i = 0;
            do {
                File file2 = listFiles[i];
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(file2.getName());
                sb.append(" ");
                sb.append(file2.length());
                sb.append(" drw=");
                String str2 = "1";
                String str3 = "0";
                if (file2.isDirectory()) {
                    str3 = str2;
                }
                sb.append(str3);
                String str4 = "0";
                if (file2.canRead()) {
                    str4 = str2;
                }
                sb.append(str4);
                if (!file2.canWrite()) {
                    str2 = "0";
                }
                sb.append(str2);
                Log.i(sb.toString());
                i++;
            } while (i < length);
        }
    }

    public static void A0F(File file, byte[] bArr) {
        if (bArr != null) {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            try {
                bufferedOutputStream.write(bArr);
                bufferedOutputStream.close();
            } catch (Throwable th) {
                try {
                    bufferedOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public static void A0G(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read >= 0) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static void A0H(FileChannel fileChannel, WritableByteChannel writableByteChannel) {
        for (long j = 0; j < fileChannel.size(); j += 131072) {
            fileChannel.transferTo(j, Math.min(131072L, fileChannel.size() - j), writableByteChannel);
        }
    }

    public static boolean A0I(Uri uri, AnonymousClass01d r6, C15890o4 r7, boolean z) {
        String scheme = uri.getScheme();
        if (!"settings".equals(uri.getAuthority()) && ("file".equals(scheme) || "content".equals(scheme))) {
            try {
                try {
                    ContentResolver A0C = r6.A0C();
                    if (A0C == null) {
                        Log.w("file-utils/ringtone-available/false cr=null");
                        return false;
                    }
                    AnonymousClass1P1.A03(A0C.openInputStream(uri));
                    return true;
                } catch (SQLiteException | FileNotFoundException | NullPointerException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("file-utils/ringtone-available/false/file-not-found ");
                    sb.append(uri);
                    Log.i(sb.toString(), e);
                    return false;
                } catch (IllegalStateException e2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("file-utils/ringtone-available/false/illegal-state ");
                    sb2.append(uri);
                    Log.i(sb2.toString(), e2);
                    return false;
                }
            } catch (IllegalArgumentException e3) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("file-utils/ringtone-available/false/illegal-argument ");
                sb3.append(uri);
                Log.i(sb3.toString(), e3);
                return false;
            } catch (SecurityException e4) {
                if (uri.toString().startsWith(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI.toString())) {
                    Log.i("file-utils/ringtone-available/false/access-denied", e4);
                    if (!r7.A07()) {
                        return z;
                    }
                } else {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("file-utils/ringtone-available/true/cannot-check ");
                    sb4.append(uri.toString());
                    Log.i(sb4.toString(), e4);
                    return true;
                }
            }
        }
        return true;
    }

    public static boolean A0J(C17050qB r5, File file, File file2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            C32871cs A00 = r5.A00(file2);
            try {
                byte[] bArr = new byte[C25981Bo.A0F];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read >= 0) {
                        A00.write(bArr, 0, read);
                    } else {
                        A00.close();
                        fileInputStream.close();
                        return true;
                    }
                }
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException e) {
            Log.e("fileutils/copyexternalfile/failed to copy external file", e);
            return false;
        }
    }

    public static boolean A0K(C17050qB r1, File file, File file2) {
        if (file.renameTo(file2)) {
            return true;
        }
        boolean A0J = A0J(r1, file, file2);
        if (!file.delete()) {
            Log.w("fileutils/moveFile/could not delete source file");
        }
        return A0J;
    }

    public static boolean A0L(C21780xy r5, File file, File file2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            C32871cs r3 = new C32871cs(r5.A00, file2);
            byte[] bArr = new byte[C25981Bo.A0F];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read >= 0) {
                    r3.write(bArr, 0, read);
                } else {
                    r3.close();
                    fileInputStream.close();
                    return true;
                }
            }
        } catch (IOException e) {
            Log.e("fileutils/copyinternalfile/failed to copy internal file", e);
            return false;
        }
    }

    public static boolean A0M(File file) {
        if (!file.exists() || file.delete()) {
            return true;
        }
        StringBuilder sb = new StringBuilder("file-utils/delete-file/failed ");
        sb.append(file);
        Log.w(sb.toString());
        return false;
    }

    public static boolean A0N(File file) {
        if (file.isDirectory()) {
            try {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    for (File file2 : listFiles) {
                        if (!A0N(file2)) {
                            return false;
                        }
                    }
                }
            } catch (OutOfMemoryError e) {
                StringBuilder sb = new StringBuilder("trash/delete-recursive/out-of-memory ");
                sb.append(file.getAbsolutePath());
                Log.e(sb.toString(), e);
                return false;
            }
        }
        return file.delete();
    }

    public static boolean A0O(File file) {
        StringBuilder sb;
        String str;
        String absolutePath = file.getAbsolutePath();
        if (file.exists()) {
            if (file.isDirectory()) {
                StringBuilder sb2 = new StringBuilder("FileUtils/prepareEmptyDir/Directory already exists unexpectedly! Cleaning up. ");
                sb2.append(absolutePath);
                Log.e(sb2.toString());
                A0D(file, -1);
                return true;
            }
            sb = new StringBuilder();
            str = "FileUtils/prepareEmptyDir/Directory already exists unexpectedly and is a file! Wrong call.";
        } else if (file.mkdirs()) {
            return true;
        } else {
            sb = new StringBuilder();
            str = "FileUtils/prepareEmptyDir/Could not make directory ";
        }
        sb.append(str);
        sb.append(absolutePath);
        Log.e(sb.toString());
        return false;
    }

    public static boolean A0P(File file, InputStream inputStream) {
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    bufferedOutputStream.write(bArr, 0, read);
                } else {
                    bufferedOutputStream.close();
                    return true;
                }
            }
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder("FileUtils/saveInputStreamToFile/could not save file to:");
            sb.append(file.getAbsolutePath());
            Log.e(sb.toString(), e);
            return false;
        }
    }

    public static boolean A0Q(File file, InputStream inputStream, long j) {
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
            long j2 = 0;
            do {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    bufferedOutputStream.write(bArr, 0, read);
                    j2 += (long) read;
                } else {
                    bufferedOutputStream.close();
                    return true;
                }
            } while (j2 <= j);
            StringBuilder sb = new StringBuilder();
            sb.append("FileUtils/saveInputStreamToFileWithMaxBytes file size of ");
            sb.append(j2);
            sb.append(" but max of ");
            sb.append(j);
            throw new IOException(sb.toString());
        } catch (IOException e) {
            StringBuilder sb2 = new StringBuilder("FileUtils/saveInputStreamToFileWithMaxBytes/could not save file to:");
            sb2.append(file.getAbsolutePath());
            Log.e(sb2.toString(), e);
            return false;
        }
    }

    public static boolean A0R(String str) {
        String str2;
        if (!TextUtils.isEmpty(str)) {
            if (!str.matches("[\\w-.,'\\s]+")) {
                str2 = "FileUtils/fileNameInvalid/file name contains invalid characters, allowed characters are a to z, A to Z, _ , ' - . and space character";
            } else if (!str.contains("..")) {
                return false;
            } else {
                str2 = "FileUtils/fileNameInvalid/file name cannot contain ..";
            }
            Log.e(str2);
        }
        return true;
    }
}
