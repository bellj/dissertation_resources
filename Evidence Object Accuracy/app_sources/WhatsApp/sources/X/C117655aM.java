package X;

import android.content.Context;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5aM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117655aM extends FrameLayout implements AnonymousClass004 {
    public TextView A00;
    public TextView A01;
    public C14830m7 A02;
    public AnonymousClass018 A03;
    public AnonymousClass14X A04;
    public AnonymousClass2P7 A05;
    public boolean A06;

    public C117655aM(Context context) {
        super(context);
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A02 = C12980iv.A0b(A00);
            this.A04 = (AnonymousClass14X) A00.AFM.get();
            this.A03 = C12960it.A0R(A00);
        }
        FrameLayout.inflate(getContext(), R.layout.payout_transaction_row, this);
        this.A01 = C12960it.A0I(this, R.id.date);
        this.A00 = C12960it.A0I(this, R.id.amount);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }
}
