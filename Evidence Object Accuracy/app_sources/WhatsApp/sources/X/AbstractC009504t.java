package X;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

/* renamed from: X.04t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC009504t {
    public Object A00;
    public boolean A01;

    public abstract Menu A00();

    public abstract MenuInflater A01();

    public abstract View A02();

    public abstract CharSequence A03();

    public abstract CharSequence A04();

    public abstract void A05();

    public abstract void A06();

    public abstract void A07(int i);

    public abstract void A08(int i);

    public abstract void A09(View view);

    public abstract void A0A(CharSequence charSequence);

    public abstract void A0B(CharSequence charSequence);

    public abstract void A0C(boolean z);

    public abstract boolean A0D();
}
