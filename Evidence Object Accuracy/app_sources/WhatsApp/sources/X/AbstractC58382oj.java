package X;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* renamed from: X.2oj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC58382oj extends AbstractC75803kU {
    public C004902f A00 = null;
    public AnonymousClass01E A01 = null;
    public final AnonymousClass036 A02 = new AnonymousClass036();
    public final AnonymousClass036 A03 = new AnonymousClass036();
    public final AnonymousClass01F A04;

    public AbstractC58382oj(AnonymousClass01F r2) {
        this.A04 = r2;
    }

    @Override // X.AnonymousClass01A
    public Parcelable A03() {
        Bundle bundle;
        AnonymousClass036 r7 = this.A03;
        int i = 0;
        if (r7.A00() > 0) {
            bundle = C12970iu.A0D();
            long[] jArr = new long[r7.A00()];
            for (int i2 = 0; i2 < r7.A00(); i2++) {
                long A01 = r7.A01(i2);
                jArr[i2] = A01;
                bundle.putParcelable(Long.toString(A01), (C06780Vb) r7.A03(i2));
            }
            bundle.putLongArray("states", jArr);
        } else {
            bundle = null;
        }
        while (true) {
            AnonymousClass036 r1 = this.A02;
            if (i >= r1.A00()) {
                return bundle;
            }
            AnonymousClass01E r3 = (AnonymousClass01E) r1.A03(i);
            if (r3 != null && r3.A0c()) {
                if (bundle == null) {
                    bundle = C12970iu.A0D();
                }
                this.A04.A0P(bundle, r3, C12970iu.A0w(C12960it.A0k("f"), r1.A01(i)));
            }
            i++;
        }
    }

    @Override // X.AnonymousClass01A
    public void A09(Parcelable parcelable, ClassLoader classLoader) {
        if (parcelable != null) {
            Bundle bundle = (Bundle) parcelable;
            bundle.setClassLoader(classLoader);
            long[] longArray = bundle.getLongArray("states");
            AnonymousClass036 r7 = this.A03;
            r7.A05();
            AnonymousClass036 r4 = this.A02;
            r4.A05();
            if (longArray != null) {
                for (long j : longArray) {
                    r7.A09(j, bundle.getParcelable(Long.toString(j)));
                }
            }
            Iterator<T> it = bundle.keySet().iterator();
            while (it.hasNext()) {
                String A0x = C12970iu.A0x(it);
                if (A0x.startsWith("f")) {
                    AnonymousClass01E A08 = this.A04.A08(bundle, A0x);
                    if (A08 != null) {
                        A08.A0b(false);
                        r4.A09(Long.parseLong(A0x.substring(1)), A08);
                    } else {
                        Log.w(C12960it.A0d(A0x, C12960it.A0k("FragmentPagerAdapter/Bad fragment at key ")));
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass01A
    public void A0A(ViewGroup viewGroup) {
        C004902f r0 = this.A00;
        if (r0 != null) {
            r0.A03();
            this.A00 = null;
        }
    }

    @Override // X.AnonymousClass01A
    public void A0B(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            StringBuilder A0k = C12960it.A0k("ViewPager with adapter ");
            A0k.append(this);
            throw C12960it.A0U(C12960it.A0d(" requires a view id", A0k));
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v6, types: [com.whatsapp.mediacomposer.ImageComposerFragment, X.01E] */
    /* JADX WARN: Type inference failed for: r7v7, types: [X.01E, com.whatsapp.mediacomposer.VideoComposerFragment] */
    /* JADX WARN: Type inference failed for: r7v8, types: [com.whatsapp.mediacomposer.GifComposerFragment, X.01E] */
    /* JADX WARNING: Unknown variable types count: 3 */
    @Override // X.AbstractC75803kU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A0G(android.view.ViewGroup r9, int r10) {
        /*
        // Method dump skipped, instructions count: 303
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC58382oj.A0G(android.view.ViewGroup, int):java.lang.Object");
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ void A0H(ViewGroup viewGroup, Object obj, int i) {
        AnonymousClass01E r4 = (AnonymousClass01E) obj;
        AnonymousClass01E r0 = this.A01;
        if (r4 != r0) {
            if (r0 != null) {
                r0.A0b(false);
                this.A01.A0n(false);
            }
            r4.A0b(true);
            r4.A0n(true);
            this.A01 = r4;
        }
    }

    @Override // X.AbstractC75803kU
    public boolean A0J(View view, Object obj) {
        return C12970iu.A1Z(((AnonymousClass01E) obj).A0A, view);
    }

    /* renamed from: A0K */
    public void A0I(ViewGroup viewGroup, AnonymousClass01E r10, int i) {
        long j;
        int A0F = A0F(r10);
        AnonymousClass036 r7 = this.A02;
        if (r7.A01) {
            r7.A06();
        }
        int i2 = 0;
        while (true) {
            if (i2 >= r7.A00) {
                break;
            }
            Object[] objArr = r7.A03;
            if (objArr[i2] != r10) {
                i2++;
            } else if (i2 != -1) {
                j = r7.A01(i2);
                Object obj = objArr[i2];
                Object obj2 = AnonymousClass036.A04;
                if (obj != obj2) {
                    objArr[i2] = obj2;
                    r7.A01 = true;
                }
            }
        }
        j = -1;
        if (!r10.A0c() || A0F == -2) {
            this.A03.A07(j);
        } else {
            this.A03.A09(j, this.A04.A06(r10));
        }
        C004902f r1 = this.A00;
        if (r1 == null) {
            r1 = new C004902f(this.A04);
            this.A00 = r1;
        }
        r1.A05(r10);
    }
}
