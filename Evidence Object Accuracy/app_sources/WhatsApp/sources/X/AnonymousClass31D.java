package X;

/* renamed from: X.31D  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31D extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Long A08;
    public Long A09;

    public AnonymousClass31D() {
        super(478, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A02);
        r3.Abe(6, this.A08);
        r3.Abe(4, this.A03);
        r3.Abe(2, this.A04);
        r3.Abe(8, this.A05);
        r3.Abe(1, this.A00);
        r3.Abe(7, this.A06);
        r3.Abe(9, this.A01);
        r3.Abe(3, this.A09);
        r3.Abe(10, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamE2eMessageRecv {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eCiphertextType", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eCiphertextVersion", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eDestination", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eFailureReason", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eSenderType", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eSuccessful", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageMediaType", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "offline", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "retryCount", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "revokeType", C12960it.A0Y(this.A07));
        return C12960it.A0d("}", A0k);
    }
}
