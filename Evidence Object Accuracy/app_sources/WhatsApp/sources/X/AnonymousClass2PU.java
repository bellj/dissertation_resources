package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2PU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PU extends AnonymousClass2PA {
    public final int A00;
    public final AnonymousClass1JX A01;

    public AnonymousClass2PU(Jid jid, AnonymousClass1JX r2, String str, int i, long j) {
        super(jid, str, j);
        this.A00 = i;
        this.A01 = r2;
    }
}
