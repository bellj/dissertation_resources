package X;

import android.content.Context;
import android.text.SpannableStringBuilder;

/* renamed from: X.1tv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C41681tv implements AbstractC41671tu {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Context A01;
    public final /* synthetic */ AnonymousClass13H A02;
    public final /* synthetic */ boolean A03;

    public /* synthetic */ C41681tv(Context context, AnonymousClass13H r2, int i, boolean z) {
        this.A02 = r2;
        this.A01 = context;
        this.A00 = i;
        this.A03 = z;
    }

    @Override // X.AbstractC41671tu
    public final void AUx(SpannableStringBuilder spannableStringBuilder, C15370n3 r7, int i, int i2) {
        AnonymousClass13H r0 = this.A02;
        Context context = this.A01;
        int i3 = this.A00;
        boolean z = this.A03;
        spannableStringBuilder.setSpan(new C58252oO(context, r0.A00, r7, i3), i, i2, 33);
        if (z) {
            spannableStringBuilder.setSpan(new C52292aZ(context.getApplicationContext()), i + 1, i2, 33);
        }
    }
}
