package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0rF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17700rF {
    public AbstractC455722e A00(UserJid userJid) {
        if (this instanceof C21480xU) {
            return ((C21480xU) this).A07(userJid);
        }
        if (!(this instanceof C21460xS)) {
            Object obj = ((C17690rE) this).A00.get(1);
            AnonymousClass009.A05(obj);
            AbstractC455722e A00 = ((AbstractC17700rF) obj).A00(userJid);
            if (A00 != null) {
                return A00;
            }
        }
        return new C456322k();
    }

    public void A01() {
        if (this instanceof C21480xU) {
            C17680rD r7 = ((C21480xU) this).A01;
            ArrayList arrayList = new ArrayList();
            C16630pM r4 = r7.A02;
            String str = r7.A03;
            Map<String, ?> all = r4.A01(str).getAll();
            for (Map.Entry<String, ?> entry : all.entrySet()) {
                String key = entry.getKey();
                Object obj = all.get(key);
                if (obj != null) {
                    try {
                        arrayList.add(r7.A01.A00(obj.toString()));
                    } catch (C456222j e) {
                        r7.A01(e, "getAllObjects");
                        r4.A01(str).edit().remove(key).apply();
                    }
                } else {
                    StringBuilder sb = new StringBuilder("JidKeyedSharedPreferencesStore/getAllEntryPoints/ null pref value for key=");
                    sb.append(entry);
                    Log.e(sb.toString());
                }
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C455922g r5 = (C455922g) it.next();
                if (System.currentTimeMillis() - r5.A00 > C455922g.A03) {
                    r7.A00(((AbstractC456022h) r5).A00);
                }
            }
        } else if (!(this instanceof C21460xS)) {
            for (Map.Entry entry2 : ((C17690rE) this).A00.entrySet()) {
                ((AbstractC17700rF) entry2.getValue()).A01();
            }
        }
    }

    public void A02(AbstractC455722e r3) {
        if (this instanceof C21480xU) {
            ((C21480xU) this).A01.A00(((AbstractC456022h) r3).A00);
        } else if (!(this instanceof C21460xS)) {
            Object obj = ((C17690rE) this).A00.get(Integer.valueOf(r3.A00));
            AnonymousClass009.A05(obj);
            ((AbstractC17700rF) obj).A02(r3);
        }
    }

    public void A03(AbstractC455722e r3) {
        if (this instanceof C21480xU) {
            ((C21480xU) this).A01.A00(((AbstractC456022h) r3).A00);
        } else if (!(this instanceof C21460xS)) {
            Object obj = ((C17690rE) this).A00.get(Integer.valueOf(r3.A00));
            AnonymousClass009.A05(obj);
            ((AbstractC17700rF) obj).A03(r3);
        }
    }

    public void A04(AbstractC455722e r7) {
        if (this instanceof C21480xU) {
            C455922g r72 = (C455922g) r7;
            C17680rD r3 = ((C21480xU) this).A01;
            try {
                UserJid userJid = ((AbstractC456022h) r72).A00;
                String rawString = userJid.getRawString();
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("brj", userJid.getRawString());
                    jSONObject.put("ap", r72.A01);
                    jSONObject.put("s", r72.A02);
                    jSONObject.put("ct", r72.A00);
                    r3.A02.A01(r3.A03).edit().putString(rawString, jSONObject.toString()).apply();
                } catch (JSONException e) {
                    throw new C456222j("CTWA: AdsEntryPointTransformer/toData/JSONException", e);
                }
            } catch (C456222j e2) {
                r3.A01(e2, "saveObject");
            }
        } else if (!(this instanceof C21460xS)) {
            Object obj = ((C17690rE) this).A00.get(Integer.valueOf(r7.A00));
            AnonymousClass009.A05(obj);
            ((AbstractC17700rF) obj).A04(r7);
        }
    }

    public void A05(AbstractC455722e r6, C455822f r7) {
        if (this instanceof C21480xU) {
            C455922g r62 = (C455922g) r6;
            if (((C21480xU) this).A02.A00()) {
                long currentTimeMillis = System.currentTimeMillis();
                try {
                    C27861Jn r4 = new C27861Jn(r62.A01.getBytes(AnonymousClass01V.A08));
                    r7.A03();
                    C456122i r1 = (C456122i) r7.A00;
                    r1.A00 |= 4;
                    r1.A03 = r4;
                    String str = r62.A02;
                    r7.A03();
                    C456122i r12 = (C456122i) r7.A00;
                    r12.A00 |= 2;
                    r12.A04 = str;
                    int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(currentTimeMillis - r62.A00);
                    r7.A03();
                    C456122i r13 = (C456122i) r7.A00;
                    r13.A00 |= 8;
                    r13.A01 = seconds;
                } catch (Exception e) {
                    Log.e("CtwaAdsEntryPoint/fillE2ECallInfo/failed to fill E2E context info/exception=", e);
                }
            }
        } else if (!(this instanceof C21460xS)) {
            Object obj = ((C17690rE) this).A00.get(Integer.valueOf(r6.A00));
            AnonymousClass009.A05(obj);
            ((AbstractC17700rF) obj).A05(r6, r7);
        }
    }

    public void A06(AbstractC455722e r3, AbstractC15340mz r4) {
        if (!(this instanceof C21480xU)) {
            if (!(this instanceof C21460xS)) {
                Object obj = ((C17690rE) this).A00.get(Integer.valueOf(r3.A00));
                AnonymousClass009.A05(obj);
                ((AbstractC17700rF) obj).A06(r3, r4);
            }
        } else if (((C21480xU) this).A02.A00()) {
            r4.A0J = r3;
        }
    }
}
