package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.4HN  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4HN {
    public static final int[] A00 = {1, 2, 2, 2, 2, 3, 3, 4, 4, 5, 6, 6, 6, 7, 8, 8};
    public static final int[] A01 = {-1, 8000, 16000, 32000, -1, -1, 11025, 22050, 44100, -1, -1, 12000, 24000, 48000, -1, -1};
    public static final int[] A02 = {64, 112, 128, 192, 224, 256, 384, 448, 512, 640, 768, 896, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 1152, 1280, 1536, 1920, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, 2304, 2560, 2688, 2816, 2823, 2944, 3072, 3840, 4096, 6144, 7680};
}
