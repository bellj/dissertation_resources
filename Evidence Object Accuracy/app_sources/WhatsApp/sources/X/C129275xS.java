package X;

import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.5xS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C129275xS {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ ActivityC13790kL A02;
    public final /* synthetic */ FingerprintBottomSheet A03;
    public final /* synthetic */ PinBottomSheetDialogFragment A04;
    public final /* synthetic */ AbstractC118055bC A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ String A07;
    public final /* synthetic */ String A08;

    public /* synthetic */ C129275xS(ActivityC13790kL r1, FingerprintBottomSheet fingerprintBottomSheet, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AbstractC118055bC r4, String str, String str2, String str3, int i, int i2) {
        this.A05 = r4;
        this.A00 = i;
        this.A04 = pinBottomSheetDialogFragment;
        this.A03 = fingerprintBottomSheet;
        this.A01 = i2;
        this.A06 = str;
        this.A07 = str2;
        this.A02 = r1;
        this.A08 = str3;
    }

    public final void A00(C452120p r12, AnonymousClass1V8 r13) {
        AbstractC118055bC r6 = this.A05;
        int i = this.A00;
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A04;
        FingerprintBottomSheet fingerprintBottomSheet = this.A03;
        int i2 = this.A01;
        String str = this.A06;
        String str2 = this.A07;
        ActivityC13790kL r4 = this.A02;
        String str3 = this.A08;
        if (r12 != null) {
            if (i == 1 && pinBottomSheetDialogFragment != null) {
                pinBottomSheetDialogFragment.A1M();
            }
            r6.A03.A0A(r12);
            return;
        }
        if (i == 0 && fingerprintBottomSheet != null) {
            fingerprintBottomSheet.A1C();
        }
        if (!(r6 instanceof C123595nP)) {
            C123585nO r62 = (C123585nO) r6;
            if (i == 0) {
                r4.A2C(i2);
            }
            r62.A04.Ab2(new Runnable(r4, pinBottomSheetDialogFragment, r62, r13, str3, str, i) { // from class: X.6K8
                public final /* synthetic */ int A00;
                public final /* synthetic */ ActivityC13790kL A01;
                public final /* synthetic */ PinBottomSheetDialogFragment A02;
                public final /* synthetic */ C123585nO A03;
                public final /* synthetic */ AnonymousClass1V8 A04;
                public final /* synthetic */ String A05;
                public final /* synthetic */ String A06;

                {
                    this.A03 = r3;
                    this.A00 = r7;
                    this.A01 = r1;
                    this.A05 = r5;
                    this.A02 = r2;
                    this.A04 = r4;
                    this.A06 = r6;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C123585nO r7 = this.A03;
                    int i3 = this.A00;
                    ActivityC13790kL r5 = this.A01;
                    String str4 = this.A05;
                    PinBottomSheetDialogFragment pinBottomSheetDialogFragment2 = this.A02;
                    AnonymousClass1V8 r3 = this.A04;
                    String str5 = this.A06;
                    r7.A02.A0B(new C1329068r(r5, pinBottomSheetDialogFragment2, r7, str4, i3), r3, r7.A00.A0A, str5);
                }
            });
            return;
        }
        C123595nP r63 = (C123595nP) r6;
        Log.i("DyiViewModel/request-report/on-pin-node-ready");
        if (!str2.equals("DYIREPORT")) {
            Log.e("DyiViewModel/request-report/on-pin-node-ready :: no matching actions");
            return;
        }
        if (i == 0) {
            r4.A2C(i2);
        }
        r63.A09(new C129115xC(r4, pinBottomSheetDialogFragment, r63, str3, i), r13, str);
    }
}
