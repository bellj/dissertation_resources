package X;

import android.util.Log;

/* renamed from: X.4LU  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4LU {
    public final C006202y A00;

    public AnonymousClass4LU(String str, int i) {
        C74263hg r1 = new C74263hg(i);
        this.A00 = r1;
        r1.A00 = new AnonymousClass5VB(str) { // from class: X.5B3
            public final /* synthetic */ String A00;

            {
                this.A00 = r1;
            }

            @Override // X.AnonymousClass5VB
            public final void APj(Object obj, Object obj2, Object obj3, boolean z) {
                String str2 = this.A00;
                String str3 = (String) obj;
                if (z) {
                    StringBuilder A0k = C12960it.A0k("Bloks: CacheShards evicted ");
                    A0k.append(str2);
                    A0k.append(" :: ");
                    Log.d("Whatsapp", C12960it.A0d(str3, A0k));
                }
            }
        };
    }
}
