package X;

/* renamed from: X.0PD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PD {
    public EnumC03840Ji A00;
    public String A01;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof AnonymousClass0PD) {
            AnonymousClass0PD r4 = (AnonymousClass0PD) obj;
            if (this.A00 == r4.A00) {
                return this.A01.equals(r4.A01);
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + this.A00.hashCode();
    }
}
