package X;

/* renamed from: X.24L  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass24L extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;

    public AnonymousClass24L() {
        super(1250, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamStatusRevoke {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusLifeT", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionId", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
