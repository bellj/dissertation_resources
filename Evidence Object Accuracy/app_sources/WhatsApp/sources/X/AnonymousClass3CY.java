package X;

import android.net.Uri;
import java.io.File;

/* renamed from: X.3CY  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CY {
    public final /* synthetic */ C68423Vi A00;
    public final /* synthetic */ File A01;

    public AnonymousClass3CY(C68423Vi r1, File file) {
        this.A00 = r1;
        this.A01 = file;
    }

    public final void A00(EnumC868449c r6) {
        C63743Ct r2;
        File file;
        AnonymousClass49P r0;
        switch (r6.ordinal()) {
            case 0:
                C63743Ct r4 = this.A00.A02;
                String obj = Uri.fromFile(this.A01).toString();
                C16700pc.A0B(obj);
                AbstractC14200l1 r22 = r4.A03;
                if (r22 != null) {
                    Object[] A1a = C12980iv.A1a();
                    C12990iw.A1P(r4.A00, obj, A1a);
                    C14250l6.A00(r4.A01, new C14220l3(C16770pj.A0I(A1a)), r22);
                    return;
                }
                return;
            case 1:
                r2 = this.A00.A02;
                file = this.A01;
                C16700pc.A0B(file);
                r0 = AnonymousClass49P.A03;
                break;
            case 2:
                r2 = this.A00.A02;
                file = this.A01;
                C16700pc.A0B(file);
                r0 = AnonymousClass49P.A00;
                break;
            default:
                return;
        }
        r2.A00(r0);
        file.delete();
    }
}
