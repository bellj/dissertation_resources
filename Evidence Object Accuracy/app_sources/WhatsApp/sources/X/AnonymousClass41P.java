package X;

import com.whatsapp.blocklist.BlockList;

/* renamed from: X.41P  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41P extends AnonymousClass2Dn {
    public final /* synthetic */ BlockList A00;

    public AnonymousClass41P(BlockList blockList) {
        this.A00 = blockList;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        BlockList.A02(this.A00);
    }
}
