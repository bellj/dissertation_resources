package X;

/* renamed from: X.1fV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34051fV {
    public final int A00;
    public final boolean A01;
    public final boolean A02;

    public C34051fV(int i, boolean z, boolean z2) {
        this.A00 = i;
        this.A01 = z;
        this.A02 = z2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C34051fV.class != obj.getClass()) {
                return false;
            }
            C34051fV r5 = (C34051fV) obj;
            if (!(this.A00 == r5.A00 && this.A01 == r5.A01 && this.A02 == r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (((this.A00 * 31) + (this.A01 ? 1 : 0)) * 31) + (this.A02 ? 1 : 0);
    }
}
