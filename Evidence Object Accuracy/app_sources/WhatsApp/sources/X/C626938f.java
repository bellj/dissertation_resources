package X;

import android.content.SharedPreferences;
import com.whatsapp.registration.VerifyPhoneNumber;
import java.lang.ref.WeakReference;

/* renamed from: X.38f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C626938f extends AbstractC16350or {
    public final C14820m6 A00;
    public final C25961Bm A01;
    public final C20800wL A02;
    public final C862946p A03;
    public final AnonymousClass4AY A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final WeakReference A0A;
    public final boolean A0B;

    public C626938f(C14820m6 r4, C25961Bm r5, C20800wL r6, AbstractC44441yy r7, C862946p r8, AnonymousClass4AY r9, String str, String str2, String str3, String str4, String str5, boolean z) {
        this.A07 = str;
        this.A09 = str2;
        this.A08 = str3;
        this.A04 = r9;
        this.A03 = r8;
        this.A0A = C12970iu.A10(r7);
        this.A02 = r6;
        this.A01 = r5;
        this.A00 = r4;
        SharedPreferences sharedPreferences = r4.A00;
        int A01 = C12970iu.A01(sharedPreferences, "reg_attempts_verify_code") + 1;
        C12960it.A0u(sharedPreferences, "reg_attempts_verify_code", A01);
        ((AnonymousClass4V6) r8).A00 = A01;
        this.A0B = z;
        this.A06 = str4;
        this.A05 = str5;
    }

    public static void A00(VerifyPhoneNumber verifyPhoneNumber) {
        if (verifyPhoneNumber.A0f.A03()) {
            verifyPhoneNumber.A0f.A01();
        } else {
            verifyPhoneNumber.A0M.setText("");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:174:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x016c  */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void A07(java.lang.Object r9) {
        /*
        // Method dump skipped, instructions count: 980
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C626938f.A07(java.lang.Object):void");
    }
}
