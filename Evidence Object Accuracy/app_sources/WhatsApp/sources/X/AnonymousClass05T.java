package X;

import androidx.lifecycle.CompositeGeneratedAdaptersObserver;
import androidx.lifecycle.ReflectiveGenericLifecycleObserver;
import androidx.lifecycle.SingleGeneratedAdapterObserver;
import java.lang.reflect.Constructor;
import java.util.List;

/* renamed from: X.05T  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05T {
    public AnonymousClass05I A00;
    public AnonymousClass054 A01;

    public AnonymousClass05T(AnonymousClass05I r5, AnonymousClass03H r6) {
        AnonymousClass054 reflectiveGenericLifecycleObserver;
        if (r6 instanceof AnonymousClass054) {
            reflectiveGenericLifecycleObserver = (AnonymousClass054) r6;
        } else {
            Class<?> cls = r6.getClass();
            if (AnonymousClass0TR.A00(cls) == 2) {
                List list = (List) AnonymousClass0TR.A01.get(cls);
                if (list.size() == 1) {
                    AnonymousClass0TR.A01(r6, (Constructor) list.get(0));
                    reflectiveGenericLifecycleObserver = new SingleGeneratedAdapterObserver();
                } else {
                    AbstractC11300g4[] r1 = new AbstractC11300g4[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        r1[i] = AnonymousClass0TR.A01(r6, (Constructor) list.get(i));
                    }
                    reflectiveGenericLifecycleObserver = new CompositeGeneratedAdaptersObserver(r1);
                }
            } else {
                reflectiveGenericLifecycleObserver = new ReflectiveGenericLifecycleObserver(r6);
            }
        }
        this.A01 = reflectiveGenericLifecycleObserver;
        this.A00 = r5;
    }

    public void A00(AnonymousClass074 r4, AbstractC001200n r5) {
        AnonymousClass05I A01 = r4.A01();
        AnonymousClass05I r1 = this.A00;
        if (A01.compareTo(r1) < 0) {
            r1 = A01;
        }
        this.A00 = r1;
        this.A01.AWQ(r4, r5);
        this.A00 = A01;
    }
}
