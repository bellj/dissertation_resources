package X;

import com.whatsapp.util.Log;

/* renamed from: X.0pJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16600pJ {
    public boolean A00;
    public final StringBuilder A01 = new StringBuilder();

    public void A00(String str, int i) {
        boolean z;
        if (i == 2) {
            Log.i(str);
        } else if (i == 3) {
            Log.w(str);
        } else if (i == 4) {
            Log.e(str);
        }
        synchronized (this) {
            z = this.A00;
        }
        if (z) {
            synchronized (this) {
                StringBuilder sb = this.A01;
                sb.append("\n");
                sb.append(str);
            }
        }
    }
}
