package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2EM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EM {
    public AnonymousClass016 A00;
    public AnonymousClass016 A01;
    public AnonymousClass016 A02;
    public AnonymousClass016 A03;
    public AnonymousClass016 A04;
    public AnonymousClass016 A05;
    public AnonymousClass016 A06;
    public AnonymousClass016 A07;
    public AnonymousClass016 A08;
    public AnonymousClass016 A09;
    public final C14900mE A0A;
    public final C14650lo A0B;
    public final C21770xx A0C;
    public final AnonymousClass2EN A0D;
    public final AnonymousClass1FZ A0E;
    public final C19850um A0F;
    public final AnonymousClass19Q A0G;
    public final AnonymousClass2EH A0H;
    public final UserJid A0I;
    public final AnonymousClass2EJ A0J = new AnonymousClass2EL(this);
    public final AnonymousClass2EJ A0K = new AnonymousClass3ZR(this);
    public final AbstractC14440lR A0L;

    public AnonymousClass2EM(C14900mE r2, C14650lo r3, C21770xx r4, AnonymousClass2EN r5, AnonymousClass1FZ r6, C19850um r7, AnonymousClass19Q r8, AnonymousClass2EH r9, UserJid userJid, AbstractC14440lR r11) {
        this.A0A = r2;
        this.A0L = r11;
        this.A0I = userJid;
        this.A0C = r4;
        this.A0E = r6;
        this.A0F = r7;
        this.A0H = r9;
        this.A0D = r5;
        this.A0G = r8;
        this.A0B = r3;
    }
}
