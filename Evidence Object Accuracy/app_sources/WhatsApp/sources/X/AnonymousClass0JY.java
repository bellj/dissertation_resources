package X;

/* renamed from: X.0JY  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0JY {
    INT_TYPE,
    FLOAT_TYPE,
    COLOR_TYPE,
    COLOR_DRAWABLE_TYPE,
    STRING_TYPE,
    BOOLEAN_TYPE,
    DIMENSION_TYPE
}
