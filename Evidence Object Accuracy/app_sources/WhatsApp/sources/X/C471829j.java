package X;

/* renamed from: X.29j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C471829j extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Long A04;

    public C471829j() {
        super(1180, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A00);
        r3.Abe(6, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(2, this.A03);
        r3.Abe(1, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamStatusReply {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isPosterBiz", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isPosterInAddressBook", this.A01);
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusReplyMessageType", obj);
        Integer num2 = this.A03;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusReplyResult", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionId", this.A04);
        sb.append("}");
        return sb.toString();
    }
}
