package X;

/* renamed from: X.1wJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43041wJ extends C42951wA implements AbstractC43011wG {
    public C43031wI A00;
    public final C16590pI A01;

    public C43041wJ(AbstractC15710nm r14, C15570nT r15, C15450nH r16, C15550nR r17, C15610nY r18, AnonymousClass01d r19, C16590pI r20, AnonymousClass018 r21, C15670ni r22, C22630zO r23, C43031wI r24) {
        super(r14, r15, r16, r17, r18, r19, r21, r22, r23, r24.A01);
        this.A01 = r20;
        this.A00 = r24;
    }

    @Override // X.AbstractC43011wG
    public AnonymousClass1IS AEx() {
        return this.A00.A00.A0z;
    }
}
