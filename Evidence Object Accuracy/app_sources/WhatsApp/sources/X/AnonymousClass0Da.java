package X;

import android.text.TextUtils;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.0Da  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Da extends AnonymousClass0Q6 {
    public AnonymousClass0Da() {
        super(CharSequence.class, R.id.tag_state_description, 64, 30);
    }

    @Override // X.AnonymousClass0Q6
    public Object A01(View view) {
        return C05720Qr.A00(view);
    }

    @Override // X.AnonymousClass0Q6
    public void A03(View view, Object obj) {
        C05720Qr.A01(view, (CharSequence) obj);
    }

    @Override // X.AnonymousClass0Q6
    public boolean A04(Object obj, Object obj2) {
        return !TextUtils.equals((CharSequence) obj, (CharSequence) obj2);
    }
}
