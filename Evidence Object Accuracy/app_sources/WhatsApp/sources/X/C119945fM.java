package X;

/* renamed from: X.5fM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119945fM extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;

    public C119945fM() {
        super(3446, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A00);
        r3.Abe(1, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDisappearingModeSettingEvents {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "disappearingModeEntryPoint", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "disappearingModeSettingEventName", C12960it.A0Y(this.A01));
        return C12960it.A0d("}", A0k);
    }
}
