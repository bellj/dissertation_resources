package X;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape0S0500000_I0;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.filter.FilterUtils;
import com.whatsapp.mediacomposer.doodle.DoodleView;
import com.whatsapp.util.Log;

/* renamed from: X.21U */
/* loaded from: classes2.dex */
public class AnonymousClass21U {
    public float A00;
    public int A01;
    public Bitmap A02;
    public Bitmap A03;
    public Bitmap A04;
    public Rect A05;
    public View A06;
    public AnonymousClass2UH A07;
    public BottomSheetBehavior A08;
    public AnonymousClass2Ab A09;
    public AnonymousClass21W A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final int A0I;
    public final int A0J;
    public final Handler A0K = new Handler(Looper.getMainLooper());
    public final View A0L;
    public final View A0M;
    public final CoordinatorLayout A0N;
    public final AnonymousClass0MU A0O;
    public final ActivityC000900k A0P;
    public final RecyclerView A0Q;
    public final AnonymousClass1O4 A0R;
    public final C14820m6 A0S;
    public final AnonymousClass018 A0T;
    public final AnonymousClass1BI A0U;
    public final AnonymousClass21V A0V;
    public final ExecutorC27271Gr A0W;
    public final Runnable A0X;
    public final String A0Y;

    public AnonymousClass21U(Uri uri, View view, ActivityC000900k r10, C18720su r11, C14820m6 r12, AnonymousClass018 r13, AnonymousClass1BI r14, AnonymousClass21V r15, AnonymousClass2Ab r16, AbstractC14440lR r17, int i) {
        this.A0P = r10;
        this.A0T = r13;
        this.A0S = r12;
        this.A0M = view;
        this.A0V = r15;
        this.A01 = i;
        this.A0U = r14;
        this.A09 = r16;
        this.A0R = r11.A02();
        this.A0W = new ExecutorC27271Gr(r17, false);
        this.A0O = new AnonymousClass0MU(view.getContext(), new C73583gT(this));
        this.A0N = (CoordinatorLayout) this.A0M.findViewById(R.id.filter_sheet_container);
        this.A0L = this.A0M.findViewById(R.id.filter_bottom_sheet);
        this.A0Q = (RecyclerView) this.A0M.findViewById(R.id.filter_selector);
        this.A06 = this.A0M.findViewById(R.id.media_content);
        this.A00 = 0.28f;
        StringBuilder sb = new StringBuilder();
        sb.append(uri.toString());
        sb.append("-filter");
        this.A0Y = sb.toString();
        this.A0J = r10.getResources().getDimensionPixelSize(R.dimen.filter_thumb_width);
        this.A0I = r10.getResources().getDimensionPixelSize(R.dimen.filter_thumb_height);
        this.A0X = new RunnableBRunnable0Shape0S0500000_I0(r10, r13, r14, view, this, 2);
    }

    public static /* synthetic */ void A00(AnonymousClass21U r6) {
        RecyclerView recyclerView = r6.A0Q;
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) recyclerView.getLayoutParams();
        if (marginLayoutParams != null) {
            Rect rect = r6.A05;
            marginLayoutParams.leftMargin = rect.left;
            marginLayoutParams.rightMargin = rect.right;
            marginLayoutParams.topMargin = rect.top;
            marginLayoutParams.bottomMargin = rect.bottom;
            recyclerView.setLayoutParams(marginLayoutParams);
            recyclerView.A0h = true;
        }
        View view = r6.A0M;
        int width = view.getWidth();
        int height = view.getHeight();
        int dimensionPixelSize = r6.A0P.getResources().getDimensionPixelSize(R.dimen.filter_selector_height);
        float f = 0.5f;
        if (width < height) {
            f = 0.28f;
        }
        r6.A00 = f;
        r6.A08.A0L((height - dimensionPixelSize) - r6.A05.bottom);
        float f2 = ((float) width) / 2.0f;
        float height2 = ((float) (height - recyclerView.getHeight())) / 2.0f;
        View view2 = r6.A06;
        view2.setPivotX(f2);
        view2.setPivotY(height2);
        DoodleView doodleView = r6.A09.A0H;
        doodleView.setPivotX(f2);
        doodleView.setPivotY(height2);
        if (r6.A08.A0B == 3) {
            float f3 = 1.0f - r6.A00;
            view2.setScaleX(f3);
            view2.setScaleY(f3);
            doodleView.setScaleX(f3);
            doodleView.setScaleY(f3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        if (A08() != false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r3 = this;
            android.view.View r2 = r3.A0L
            if (r2 == 0) goto L_0x001e
            boolean r0 = r3.A07()
            if (r0 != 0) goto L_0x0011
            boolean r1 = r3.A08()
            r0 = 0
            if (r1 == 0) goto L_0x0012
        L_0x0011:
            r0 = 1
        L_0x0012:
            r1 = 0
            if (r0 == 0) goto L_0x001f
            boolean r0 = r3.A08()
            if (r0 != 0) goto L_0x001e
            r2.setVisibility(r1)
        L_0x001e:
            return
        L_0x001f:
            r0 = 1
            r3.A0E = r0
            r3.A0C = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass21U.A01():void");
    }

    public void A02() {
        if (this.A04 != null) {
            AnonymousClass1O4 r5 = this.A0R;
            String str = this.A0Y;
            Bitmap bitmap = (Bitmap) r5.A00(str);
            this.A03 = bitmap;
            if (bitmap == null) {
                int i = this.A01;
                if (i == 0) {
                    this.A03 = this.A04;
                    r5.A00.A07(str);
                    return;
                }
                Bitmap A00 = FilterUtils.A00(this.A04, this.A0U, i, true);
                this.A03 = A00;
                if (A00 == null) {
                    this.A03 = this.A04;
                    this.A01 = 0;
                    Log.w("FilterSelectorController/updateFilteredMediaBitmap/filter failed");
                    return;
                }
                r5.A03(str, A00);
            }
        }
    }

    public final void A03() {
        AnonymousClass21W r5 = this.A0A;
        if (r5 != null) {
            int i = 0;
            while (true) {
                AnonymousClass21U r2 = r5.A0A;
                RecyclerView recyclerView = r2.A0Q;
                if (i < recyclerView.getChildCount()) {
                    View$OnClickListenerC55222hz r0 = (View$OnClickListenerC55222hz) recyclerView.A0E(recyclerView.getChildAt(i));
                    if (r0 != null) {
                        ImageView imageView = r0.A02;
                        imageView.setBackgroundResource(0);
                        imageView.setImageDrawable(null);
                    }
                    i++;
                } else {
                    new C864247g(r5.A01).A02.executeOnExecutor(r2.A0W, new Void[0]);
                    return;
                }
            }
        }
    }

    public final void A04() {
        if (this.A04 != null && !this.A0B) {
            ActivityC000900k r2 = this.A0P;
            if (r2.A06.A02 != AnonymousClass05I.DESTROYED) {
                new AnonymousClass36l(r2, this).A02.executeOnExecutor(this.A0W, new Void[0]);
                this.A0B = true;
            }
        }
    }

    public void A05(Runnable runnable, Runnable runnable2, int i) {
        if (this.A04 != null) {
            AnonymousClass1O4 r2 = this.A0R;
            String str = this.A0Y;
            Bitmap bitmap = (Bitmap) r2.A00(str);
            if (i != this.A01 || i == 0) {
                r2.A00.A07(str);
            }
            if (i != 0) {
                ((AbstractC16350or) new C625137n(bitmap, this.A0P, this, runnable, runnable2, i)).A02.executeOnExecutor(this.A0W, new Void[0]);
                return;
            }
            this.A03 = this.A04;
            if (runnable != null) {
                runnable.run();
            }
            this.A01 = 0;
            this.A0V.A00();
            return;
        }
        Log.e("FilterSelectorController/startUpdateFilteredMediaBitmapTask/mediaBitmap is null");
    }

    public void A06(boolean z) {
        View view = this.A0L;
        if (view == null) {
            return;
        }
        if (A07() || A08()) {
            view.setVisibility(4);
            return;
        }
        this.A0E = false;
        this.A0C = true;
        this.A0D = z;
    }

    public boolean A07() {
        BottomSheetBehavior bottomSheetBehavior;
        return this.A0L == null || (bottomSheetBehavior = this.A08) == null || bottomSheetBehavior.A0B == 4;
    }

    public boolean A08() {
        BottomSheetBehavior bottomSheetBehavior;
        return (this.A0L == null || (bottomSheetBehavior = this.A08) == null || bottomSheetBehavior.A0B != 3) ? false : true;
    }

    public boolean A09() {
        View view = this.A0L;
        if (view == null || A07() || this.A0H) {
            return false;
        }
        this.A07.A01(view, 1);
        this.A08.A0M(4);
        this.A0H = true;
        return true;
    }
}
