package X;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;

/* renamed from: X.0I6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I6 extends AnonymousClass0P4 {
    public float A00;
    public float A01;
    public RectF A02 = new RectF();
    public final /* synthetic */ C06540Ua A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0I6(C06540Ua r2, float f, float f2) {
        super(r2);
        this.A03 = r2;
        this.A00 = f;
        this.A01 = f2;
    }

    @Override // X.AnonymousClass0P4
    public void A00(String str) {
        C06540Ua r4 = this.A03;
        if (r4.A0j()) {
            Rect rect = new Rect();
            r4.A03.A00.getTextBounds(str, 0, str.length(), rect);
            RectF rectF = new RectF(rect);
            rectF.offset(this.A00, this.A01);
            this.A02.union(rectF);
        }
        this.A00 += r4.A03.A00.measureText(str);
    }

    @Override // X.AnonymousClass0P4
    public boolean A01(AbstractC03250Hb r7) {
        if (!(r7 instanceof C03480Hy)) {
            return true;
        }
        C03480Hy r2 = (C03480Hy) r7;
        AnonymousClass0OO A04 = ((AnonymousClass0OO) r7).A01.A04(r2.A02);
        if (A04 == null) {
            C06540Ua.A06("TextPath path reference '%s' not found", r2.A02);
            return false;
        }
        AnonymousClass0HP r4 = (AnonymousClass0HP) A04;
        Path path = new C08500bI(r4.A00, this.A03).A02;
        Matrix matrix = ((AbstractC03280He) r4).A00;
        if (matrix != null) {
            path.transform(matrix);
        }
        RectF rectF = new RectF();
        path.computeBounds(rectF, true);
        this.A02.union(rectF);
        return false;
    }
}
