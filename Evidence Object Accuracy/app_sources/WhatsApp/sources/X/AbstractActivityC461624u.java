package X;

import com.whatsapp.authentication.AppAuthenticationActivity;

/* renamed from: X.24u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC461624u extends ActivityC13810kN {
    public boolean A00 = false;

    public AbstractActivityC461624u() {
        A0R(new C102794pi(this));
    }

    @Override // X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AppAuthenticationActivity appAuthenticationActivity = (AppAuthenticationActivity) this;
            AnonymousClass01J r2 = ((AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent())).A1E;
            ((ActivityC13830kP) appAuthenticationActivity).A05 = (AbstractC14440lR) r2.ANe.get();
            appAuthenticationActivity.A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) appAuthenticationActivity).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) appAuthenticationActivity).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) appAuthenticationActivity).A04 = (C14330lG) r2.A7B.get();
            appAuthenticationActivity.A0B = (AnonymousClass19M) r2.A6R.get();
            appAuthenticationActivity.A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) appAuthenticationActivity).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) appAuthenticationActivity).A08 = (AnonymousClass01d) r2.ALI.get();
            appAuthenticationActivity.A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) appAuthenticationActivity).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) appAuthenticationActivity).A07 = (C18640sm) r2.A3u.get();
            appAuthenticationActivity.A05 = (C21290xB) r2.ANf.get();
            appAuthenticationActivity.A06 = (C22670zS) r2.A0V.get();
        }
    }
}
