package X;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3CG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CG {
    public final Map A00 = C12970iu.A11();

    public final void A00(IBinder iBinder) {
        Map map = this.A00;
        synchronized (map) {
            if (iBinder != null) {
                iBinder.queryLocalInterface("com.google.android.gms.wearable.internal.IWearableService");
            }
            new BinderC80583sV();
            Iterator A0n = C12960it.A0n(map);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                A15.getValue();
                try {
                    new C78553p6();
                    throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                    break;
                } catch (RemoteException unused) {
                    String valueOf = String.valueOf(A15.getKey());
                    String valueOf2 = String.valueOf((Object) null);
                    StringBuilder A0t = C12980iv.A0t(valueOf.length() + 32 + valueOf2.length());
                    A0t.append("onPostInitHandler: Didn't add: ");
                    A0t.append(valueOf);
                    A0t.append("/");
                    Log.w("WearableClient", C12960it.A0d(valueOf2, A0t));
                }
            }
        }
    }
}
