package X;

import android.content.SharedPreferences;

/* renamed from: X.1E3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1E3 {
    public static final String A02;
    public SharedPreferences A00;
    public final C16630pM A01;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass01V.A07);
        sb.append("_daily_events");
        A02 = sb.toString();
    }

    public AnonymousClass1E3(C16630pM r1) {
        this.A01 = r1;
    }
}
