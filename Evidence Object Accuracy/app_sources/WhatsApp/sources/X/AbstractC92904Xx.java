package X;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.net.ssl.SSLException;

/* renamed from: X.4Xx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC92904Xx {
    public final AnonymousClass49F A00;
    public final AnonymousClass49F A01;
    public final InputStream A02;

    public AbstractC92904Xx(AnonymousClass49F r3, InputStream inputStream) {
        if (inputStream == null || r3 == null) {
            throw AnonymousClass1NR.A00("transportIn or recordStream is null", (byte) 80);
        }
        this.A02 = inputStream;
        this.A00 = new AnonymousClass49F();
        this.A01 = r3;
    }

    public C89634Ks A00() {
        try {
            AnonymousClass49F r6 = this.A00;
            if (r6.available() <= 0) {
                return null;
            }
            r6.A00();
            byte[] bArr = new byte[4];
            if (r6.read(bArr) < 4) {
                r6.reset();
                return new AnonymousClass466();
            }
            ByteBuffer wrap = ByteBuffer.wrap(bArr);
            byte b = wrap.get();
            byte[] bArr2 = new byte[3];
            wrap.get(bArr2);
            int A00 = AnonymousClass3JS.A00(bArr2);
            if (r6.available() < A00) {
                r6.reset();
                return new AnonymousClass466();
            }
            r6.reset();
            int i = A00 + 4;
            byte[] bArr3 = new byte[i];
            if (r6.read(bArr3) == i) {
                byte b2 = (byte) b;
                if (b2 == 1) {
                    return new C861745z(bArr3);
                }
                if (b2 != 2) {
                    if (b2 == 4) {
                        return new AnonymousClass465(bArr3);
                    }
                    if (b2 == 8) {
                        return new AnonymousClass463(bArr3);
                    }
                    if (b2 == 11) {
                        return new AnonymousClass468(bArr3);
                    }
                    if (b2 == 13) {
                        return new C861545x(bArr3);
                    }
                    if (b2 == 15) {
                        return new AnonymousClass469(bArr3);
                    }
                    if (b2 == 20) {
                        return new AnonymousClass46A(bArr3);
                    }
                    if (b2 == 24) {
                        return new AnonymousClass467(bArr3);
                    }
                    throw new SSLException(C12960it.A0W(b2, "Invalid handshake message type "));
                } else if (i < 38 || !AnonymousClass3JS.A04(Arrays.copyOfRange(bArr3, 6, 38), C88834Hh.A05)) {
                    return new AnonymousClass46B(bArr3);
                } else {
                    return new AnonymousClass464(bArr3);
                }
            } else {
                throw AnonymousClass1NR.A00(C12960it.A0e("Could not read handshake message of length ", C12960it.A0h(), i), (byte) 80);
            }
        } catch (IOException e) {
            throw C72453ed.A0f(e);
        }
    }

    public C89634Ks A01() {
        if (!(this instanceof AnonymousClass46E)) {
            AnonymousClass46F r5 = (AnonymousClass46F) this;
            try {
                try {
                    C89634Ks A00 = r5.A00();
                    if (A00 != null && !(A00 instanceof AnonymousClass466)) {
                        return A00;
                    }
                    if (r5.A02()) {
                        byte[] bArr = new byte[5];
                        AnonymousClass49F r9 = ((AbstractC92904Xx) r5).A01;
                        int read = r9.read(bArr);
                        if (read == 5) {
                            ByteBuffer wrap = ByteBuffer.wrap(bArr);
                            byte b = wrap.get();
                            wrap.getShort();
                            byte[] bArr2 = new byte[2];
                            wrap.get(bArr2);
                            int A01 = AnonymousClass3JS.A01(bArr2);
                            if (b == 23 || b == 20) {
                                byte[] bArr3 = new byte[A01];
                                int read2 = r9.read(bArr3);
                                if (read2 != A01) {
                                    StringBuilder A0j = C12960it.A0j("read returned fewer than expected bytes ");
                                    A0j.append(read2);
                                    throw AnonymousClass1NR.A00(C12960it.A0e(" != ", A0j, A01), (byte) 80);
                                } else if (b == 20) {
                                    return new AnonymousClass466();
                                } else {
                                    byte[] A8i = r5.A01.A8i(bArr, bArr3, 0, A01, r5.A00);
                                    r5.A00++;
                                    C94044bA r6 = new C94044bA(A8i);
                                    byte b2 = r6.A00;
                                    if (b2 == 20) {
                                        return new AnonymousClass466();
                                    }
                                    switch (b2) {
                                        case 21:
                                            return new C861245u(r6.A02);
                                        case 22:
                                            AnonymousClass49F r4 = ((AbstractC92904Xx) r5).A00;
                                            byte[] bArr4 = r6.A02;
                                            r4.A60(bArr4, 0, bArr4.length);
                                            return r5.A00();
                                        case 23:
                                            if (((AbstractC92904Xx) r5).A00.available() <= 0) {
                                                return new C861345v(r6.A02);
                                            }
                                            throw AnonymousClass1NR.A00("App data and handshake messages cannot interleave", (byte) 10);
                                        default:
                                            throw AnonymousClass1NR.A00(C12960it.A0e("Invalid content type ", C12960it.A0h(), b2), (byte) 10);
                                    }
                                }
                            } else {
                                throw AnonymousClass1NR.A00(C12960it.A0e("Invalid content type ", C12960it.A0h(), b), (byte) 47);
                            }
                        } else {
                            StringBuilder A0j2 = C12960it.A0j("read returned fewer than expected bytes ");
                            A0j2.append(read);
                            throw AnonymousClass1NR.A00(C12960it.A0e(" != ", A0j2, 5), (byte) 80);
                        }
                    } else {
                        byte[] bArr5 = new byte[16645];
                        int read3 = r5.A02.read(bArr5);
                        if (read3 != -1) {
                            ((AbstractC92904Xx) r5).A01.A60(bArr5, 0, read3);
                            return new AnonymousClass466();
                        }
                        throw new AnonymousClass1NR(new SSLException("Transport layer is reached end of file."), (byte) 80, true);
                    }
                } catch (SocketException | SocketTimeoutException e) {
                    throw new AnonymousClass1NR(new SSLException(e), (byte) 80, true);
                }
            } catch (IOException e2) {
                throw C72453ed.A0f(e2);
            }
        } else {
            try {
                try {
                    C89634Ks A002 = A00();
                    if (A002 != null && !(A002 instanceof AnonymousClass466)) {
                        return A002;
                    }
                    if (A02()) {
                        byte[] bArr6 = new byte[5];
                        AnonymousClass49F r11 = this.A01;
                        int read4 = r11.read(bArr6);
                        if (read4 == 5) {
                            ByteBuffer wrap2 = ByteBuffer.wrap(bArr6);
                            byte b3 = wrap2.get();
                            wrap2.getShort();
                            byte[] bArr7 = new byte[2];
                            wrap2.get(bArr7);
                            int A012 = AnonymousClass3JS.A01(bArr7);
                            byte[] bArr8 = new byte[A012];
                            int read5 = r11.read(bArr8);
                            if (read5 != A012) {
                                StringBuilder A0j3 = C12960it.A0j("read returned fewer than expected bytes ");
                                A0j3.append(read5);
                                throw AnonymousClass1NR.A00(C12960it.A0e(" != ", A0j3, A012), (byte) 80);
                            } else if (b3 == 20) {
                                return new AnonymousClass466();
                            } else {
                                switch (b3) {
                                    case 21:
                                        return new C861245u(bArr8);
                                    case 22:
                                        this.A00.A60(bArr8, 0, A012);
                                        return A00();
                                    case 23:
                                        if (this.A00.available() <= 0) {
                                            return new C861345v(bArr8);
                                        }
                                        throw AnonymousClass1NR.A00("App data and handshake messages cannot interleave", (byte) 10);
                                    default:
                                        throw AnonymousClass1NR.A00(C12960it.A0e("Received Message with invalid type ", C12960it.A0h(), b3), (byte) 10);
                                }
                            }
                        } else {
                            StringBuilder A0j4 = C12960it.A0j("read returned fewer than expected bytes ");
                            A0j4.append(read4);
                            throw AnonymousClass1NR.A00(C12960it.A0e(" != ", A0j4, 5), (byte) 80);
                        }
                    } else {
                        byte[] bArr9 = new byte[16645];
                        int read6 = this.A02.read(bArr9);
                        if (read6 != -1) {
                            this.A01.A60(bArr9, 0, read6);
                            return new AnonymousClass466();
                        }
                        throw new AnonymousClass1NR(new SSLException("Transport layer is reached end of file."), (byte) 80, true);
                    }
                } catch (IOException e3) {
                    throw C72453ed.A0f(e3);
                }
            } catch (SocketException | SocketTimeoutException e4) {
                throw new AnonymousClass1NR(new SSLException(e4), (byte) 80, true);
            }
        }
    }

    public boolean A02() {
        try {
            AnonymousClass49F r7 = this.A01;
            if (r7.available() < 5) {
                return false;
            }
            byte[] bArr = new byte[5];
            r7.A00();
            int read = r7.read(bArr);
            if (read == 5) {
                r7.reset();
                ByteBuffer wrap = ByteBuffer.wrap(bArr);
                byte b = wrap.get();
                short s = wrap.getShort();
                byte[] bArr2 = new byte[2];
                wrap.get(bArr2);
                int A01 = AnonymousClass3JS.A01(bArr2);
                if (!AnonymousClass4GK.A00.contains(Byte.valueOf(b)) || s != 771) {
                    throw new AnonymousClass1NR(new SSLException(C12960it.A0d(AnonymousClass3JS.A03(bArr), C12960it.A0j("Invalid record header "))), (byte) 10, true);
                } else if (A01 < 0 || A01 > 16640) {
                    throw new AnonymousClass1NR(new SSLException(C12960it.A0d(AnonymousClass3JS.A03(bArr), C12960it.A0j("Invalid record header "))), (byte) 22, true);
                } else if (r7.available() >= A01 + 5) {
                    return true;
                } else {
                    return false;
                }
            } else {
                StringBuilder A0h = C12960it.A0h();
                A0h.append("read returned fewer than expected bytes ");
                A0h.append(read);
                throw AnonymousClass1NR.A00(C12960it.A0e(" != ", A0h, 5), (byte) 80);
            }
        } catch (IOException e) {
            throw C72453ed.A0f(e);
        }
    }
}
