package X;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.0yN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22030yN {
    public final AnonymousClass10H A00;
    public final MessageDigest A01;

    public C22030yN(AnonymousClass10H r4) {
        this.A00 = r4;
        try {
            this.A01 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            StringBuilder sb = new StringBuilder("ABOfflineAssign assign will fail due to MD5 algorithm not found: ");
            sb.append(e);
            throw new RuntimeException(sb.toString());
        }
    }
}
