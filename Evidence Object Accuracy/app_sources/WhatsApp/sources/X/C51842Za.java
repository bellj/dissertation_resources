package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Insets;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

/* renamed from: X.2Za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51842Za extends Drawable {
    public Bitmap A00;
    public Bitmap A01;
    public Canvas A02;
    public Canvas A03;
    public Paint A04;
    public Path A05 = null;
    public final PorterDuffXfermode A06 = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
    public final Drawable A07;
    public final AbstractC469028d A08;

    public C51842Za(Resources.Theme theme, Resources resources, AbstractC469028d r5, int i) {
        Drawable A04 = AnonymousClass00X.A04(theme, resources, i);
        AnonymousClass009.A05(A04);
        this.A07 = A04;
        this.A08 = r5;
        A00();
    }

    public C51842Za(Resources resources, Bitmap bitmap, AbstractC469028d r5) {
        this.A07 = new BitmapDrawable(resources, bitmap);
        this.A08 = r5;
        A00();
    }

    public C51842Za(Drawable drawable, AbstractC469028d r4) {
        this.A07 = drawable;
        this.A08 = r4;
        A00();
    }

    public final void A00() {
        if (Build.VERSION.SDK_INT < 18) {
            Paint A0F = C12990iw.A0F();
            this.A04 = A0F;
            A0F.setColor(-16777216);
            this.A04.setDither(true);
            this.A04.setFilterBitmap(true);
            this.A04.setStyle(Paint.Style.FILL);
            this.A04.setAntiAlias(true);
        }
    }

    public final void A01(int i, int i2) {
        Bitmap bitmap;
        Bitmap bitmap2;
        Drawable drawable = this.A07;
        if (drawable instanceof BitmapDrawable) {
            this.A01 = ((BitmapDrawable) drawable).getBitmap();
        } else {
            try {
                bitmap2 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError unused) {
                bitmap2 = null;
            }
            this.A01 = bitmap2;
            if (bitmap2 != null) {
                this.A03 = new Canvas(bitmap2);
            }
        }
        try {
            bitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError unused2) {
            bitmap = null;
        }
        this.A00 = bitmap;
        if (bitmap != null) {
            this.A02 = new Canvas(bitmap);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void applyTheme(Resources.Theme theme) {
        this.A07.applyTheme(theme);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean canApplyTheme() {
        return this.A07.canApplyTheme();
    }

    @Override // android.graphics.drawable.Drawable
    public void clearColorFilter() {
        this.A07.clearColorFilter();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Path path = this.A05;
        if (path == null) {
            path = (Path) this.A08.apply(new RectF(copyBounds()));
            this.A05 = path;
        }
        if (Build.VERSION.SDK_INT >= 18 || this.A04 == null || this.A00 == null || this.A01 == null) {
            canvas.drawARGB(0, 0, 0, 0);
            int save = canvas.save();
            canvas.clipPath(this.A05);
            this.A07.draw(canvas);
            canvas.restoreToCount(save);
            return;
        }
        Canvas canvas2 = this.A02;
        AnonymousClass009.A05(canvas2);
        canvas2.drawARGB(0, 0, 0, 0);
        this.A02.drawPath(path, this.A04);
        Paint paint = this.A04;
        AnonymousClass009.A05(paint);
        paint.setXfermode(this.A06);
        Canvas canvas3 = this.A03;
        if (canvas3 != null) {
            this.A07.draw(canvas3);
        }
        this.A02.drawBitmap(this.A01, 0.0f, 0.0f, this.A04);
        this.A04.setXfermode(null);
        canvas.drawBitmap(this.A00, 0.0f, 0.0f, (Paint) null);
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        return this.A07.getAlpha();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.Callback getCallback() {
        return this.A07.getCallback();
    }

    @Override // android.graphics.drawable.Drawable
    public int getChangingConfigurations() {
        return this.A07.getChangingConfigurations();
    }

    @Override // android.graphics.drawable.Drawable
    public ColorFilter getColorFilter() {
        return this.A07.getColorFilter();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        return this.A07.getConstantState();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable getCurrent() {
        return this.A07.getCurrent();
    }

    @Override // android.graphics.drawable.Drawable
    public Rect getDirtyBounds() {
        return this.A07.getDirtyBounds();
    }

    @Override // android.graphics.drawable.Drawable
    public void getHotspotBounds(Rect rect) {
        this.A07.getHotspotBounds(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.A07.getIntrinsicHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.A07.getIntrinsicWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getLayoutDirection() {
        return this.A07.getLayoutDirection();
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumHeight() {
        return this.A07.getMinimumHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getMinimumWidth() {
        return this.A07.getMinimumWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return this.A07.getOpacity();
    }

    @Override // android.graphics.drawable.Drawable
    public Insets getOpticalInsets() {
        return this.A07.getOpticalInsets();
    }

    @Override // android.graphics.drawable.Drawable
    public void getOutline(Outline outline) {
        this.A07.getOutline(outline);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean getPadding(Rect rect) {
        return this.A07.getPadding(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public int[] getState() {
        return this.A07.getState();
    }

    @Override // android.graphics.drawable.Drawable
    public Region getTransparentRegion() {
        return this.A07.getTransparentRegion();
    }

    @Override // android.graphics.drawable.Drawable
    public void invalidateSelf() {
        this.A07.invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isAutoMirrored() {
        return this.A07.isAutoMirrored();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isFilterBitmap() {
        return this.A07.isFilterBitmap();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isProjected() {
        return this.A07.isProjected();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        return this.A07.isStateful();
    }

    @Override // android.graphics.drawable.Drawable
    public void jumpToCurrentState() {
        this.A07.jumpToCurrentState();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        return this.A07.mutate();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLayoutDirectionChanged(int i) {
        return this.A07.onLayoutDirectionChanged(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void scheduleSelf(Runnable runnable, long j) {
        this.A07.scheduleSelf(runnable, j);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.A07.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAutoMirrored(boolean z) {
        this.A07.setAutoMirrored(z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setBounds(int i, int i2, int i3, int i4) {
        this.A07.setBounds(i, i2, i3, i4);
        this.A05 = (Path) this.A08.apply(new RectF((float) i, (float) i2, (float) i3, (float) i4));
        if (Build.VERSION.SDK_INT < 18) {
            A01(i3 - i, i4 - i2);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setBounds(Rect rect) {
        this.A07.setBounds(rect);
        this.A05 = (Path) this.A08.apply(new RectF(rect));
        if (Build.VERSION.SDK_INT < 18) {
            A01(rect.width(), rect.height());
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setChangingConfigurations(int i) {
        this.A07.setChangingConfigurations(i);
    }

    @Override // android.graphics.drawable.Drawable
    @Deprecated
    public void setColorFilter(int i, PorterDuff.Mode mode) {
        this.A07.setColorFilter(i, mode);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.A07.setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Drawable
    @Deprecated
    public void setDither(boolean z) {
        this.A07.setDither(z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setFilterBitmap(boolean z) {
        this.A07.setFilterBitmap(z);
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspot(float f, float f2) {
        this.A07.setHotspot(f, f2);
    }

    @Override // android.graphics.drawable.Drawable
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.A07.setHotspotBounds(i, i2, i3, i4);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setState(int[] iArr) {
        return this.A07.setState(iArr);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTint(int i) {
        this.A07.setTint(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintBlendMode(BlendMode blendMode) {
        this.A07.setTintBlendMode(blendMode);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintList(ColorStateList colorStateList) {
        this.A07.setTintList(colorStateList);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintMode(PorterDuff.Mode mode) {
        this.A07.setTintMode(mode);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        return this.A07.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Drawable
    public void unscheduleSelf(Runnable runnable) {
        this.A07.unscheduleSelf(runnable);
    }
}
