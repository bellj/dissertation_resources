package X;

/* renamed from: X.30j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614430j extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public String A03;
    public String A04;

    public C614430j() {
        super(2570, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A03);
        r3.Abe(3, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamNotificationStanzaReceive {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "notificationStanzaDuration", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "notificationStanzaOfflineCount", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "notificationStanzaStage", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "notificationStanzaSubType", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "notificationStanzaType", this.A04);
        return C12960it.A0d("}", A0k);
    }
}
