package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2xm  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xm extends AnonymousClass1OY {
    @Override // X.AbstractC28551Oa
    public int getBubbleAlpha() {
        return 153;
    }

    public AnonymousClass2xm(Context context, AbstractC13890kV r8, AnonymousClass1XM r9) {
        super(context, r8, r9);
        TextEmojiLabel A0U = C12970iu.A0U(this, R.id.message_text);
        boolean A07 = AbstractC28491Nn.A07(A0U);
        A0U.setLongClickable(A07);
        AnonymousClass1OY.A0Q(A0U, this, C12960it.A0X(getContext(), C252018m.A00(this.A1M, "26000255"), C12970iu.A1b(), A07 ? 1 : 0, R.string.decryption_placeholder_message_text));
        ((AbstractC28551Oa) this).A0M.A09(r9, 2);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_decryption_failure_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_decryption_failure_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_decryption_failure_right;
    }
}
