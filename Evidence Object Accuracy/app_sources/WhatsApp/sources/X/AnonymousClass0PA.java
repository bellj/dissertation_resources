package X;

import android.database.sqlite.SQLiteDatabase;
import androidx.work.impl.WorkDatabase_Impl;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/* renamed from: X.0PA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PA {
    public final int A00 = 12;
    public final /* synthetic */ WorkDatabase_Impl A01;

    public AnonymousClass0PA(WorkDatabase_Impl workDatabase_Impl) {
        this.A01 = workDatabase_Impl;
    }

    public AnonymousClass0N3 A00(AbstractC12920im r37) {
        String obj;
        StringBuilder sb;
        String str;
        HashMap hashMap = new HashMap(2);
        hashMap.put("work_spec_id", new C05470Pr("work_spec_id", "TEXT", null, 1, 1, true));
        hashMap.put("prerequisite_id", new C05470Pr("prerequisite_id", "TEXT", null, 2, 1, true));
        HashSet hashSet = new HashSet(2);
        hashSet.add(new C05420Pm("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
        hashSet.add(new C05420Pm("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("prerequisite_id"), Arrays.asList("id")));
        HashSet hashSet2 = new HashSet(2);
        hashSet2.add(new C05340Pe("index_Dependency_work_spec_id", Arrays.asList("work_spec_id"), false));
        hashSet2.add(new C05340Pe("index_Dependency_prerequisite_id", Arrays.asList("prerequisite_id"), false));
        C06100Se r5 = new C06100Se("Dependency", hashMap, hashSet, hashSet2);
        C06100Se A00 = C06100Se.A00(r37, "Dependency");
        if (!r5.equals(A00)) {
            sb = new StringBuilder();
            str = "Dependency(androidx.work.impl.model.Dependency).\n Expected:\n";
        } else {
            HashMap hashMap2 = new HashMap(25);
            hashMap2.put("id", new C05470Pr("id", "TEXT", null, 1, 1, true));
            hashMap2.put("state", new C05470Pr("state", "INTEGER", null, 0, 1, true));
            hashMap2.put("worker_class_name", new C05470Pr("worker_class_name", "TEXT", null, 0, 1, true));
            hashMap2.put("input_merger_class_name", new C05470Pr("input_merger_class_name", "TEXT", null, 0, 1, false));
            hashMap2.put("input", new C05470Pr("input", "BLOB", null, 0, 1, true));
            hashMap2.put("output", new C05470Pr("output", "BLOB", null, 0, 1, true));
            hashMap2.put("initial_delay", new C05470Pr("initial_delay", "INTEGER", null, 0, 1, true));
            hashMap2.put("interval_duration", new C05470Pr("interval_duration", "INTEGER", null, 0, 1, true));
            hashMap2.put("flex_duration", new C05470Pr("flex_duration", "INTEGER", null, 0, 1, true));
            hashMap2.put("run_attempt_count", new C05470Pr("run_attempt_count", "INTEGER", null, 0, 1, true));
            hashMap2.put("backoff_policy", new C05470Pr("backoff_policy", "INTEGER", null, 0, 1, true));
            hashMap2.put("backoff_delay_duration", new C05470Pr("backoff_delay_duration", "INTEGER", null, 0, 1, true));
            hashMap2.put("period_start_time", new C05470Pr("period_start_time", "INTEGER", null, 0, 1, true));
            hashMap2.put("minimum_retention_duration", new C05470Pr("minimum_retention_duration", "INTEGER", null, 0, 1, true));
            hashMap2.put("schedule_requested_at", new C05470Pr("schedule_requested_at", "INTEGER", null, 0, 1, true));
            hashMap2.put("run_in_foreground", new C05470Pr("run_in_foreground", "INTEGER", null, 0, 1, true));
            hashMap2.put("out_of_quota_policy", new C05470Pr("out_of_quota_policy", "INTEGER", null, 0, 1, true));
            hashMap2.put("required_network_type", new C05470Pr("required_network_type", "INTEGER", null, 0, 1, false));
            hashMap2.put("requires_charging", new C05470Pr("requires_charging", "INTEGER", null, 0, 1, true));
            hashMap2.put("requires_device_idle", new C05470Pr("requires_device_idle", "INTEGER", null, 0, 1, true));
            hashMap2.put("requires_battery_not_low", new C05470Pr("requires_battery_not_low", "INTEGER", null, 0, 1, true));
            hashMap2.put("requires_storage_not_low", new C05470Pr("requires_storage_not_low", "INTEGER", null, 0, 1, true));
            hashMap2.put("trigger_content_update_delay", new C05470Pr("trigger_content_update_delay", "INTEGER", null, 0, 1, true));
            hashMap2.put("trigger_max_content_delay", new C05470Pr("trigger_max_content_delay", "INTEGER", null, 0, 1, true));
            hashMap2.put("content_uri_triggers", new C05470Pr("content_uri_triggers", "BLOB", null, 0, 1, false));
            HashSet hashSet3 = new HashSet(0);
            HashSet hashSet4 = new HashSet(2);
            hashSet4.add(new C05340Pe("index_WorkSpec_schedule_requested_at", Arrays.asList("schedule_requested_at"), false));
            hashSet4.add(new C05340Pe("index_WorkSpec_period_start_time", Arrays.asList("period_start_time"), false));
            r5 = new C06100Se("WorkSpec", hashMap2, hashSet3, hashSet4);
            A00 = C06100Se.A00(r37, "WorkSpec");
            if (!r5.equals(A00)) {
                sb = new StringBuilder();
                str = "WorkSpec(androidx.work.impl.model.WorkSpec).\n Expected:\n";
            } else {
                HashMap hashMap3 = new HashMap(2);
                hashMap3.put("tag", new C05470Pr("tag", "TEXT", null, 1, 1, true));
                hashMap3.put("work_spec_id", new C05470Pr("work_spec_id", "TEXT", null, 2, 1, true));
                HashSet hashSet5 = new HashSet(1);
                hashSet5.add(new C05420Pm("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                HashSet hashSet6 = new HashSet(1);
                hashSet6.add(new C05340Pe("index_WorkTag_work_spec_id", Arrays.asList("work_spec_id"), false));
                r5 = new C06100Se("WorkTag", hashMap3, hashSet5, hashSet6);
                A00 = C06100Se.A00(r37, "WorkTag");
                if (!r5.equals(A00)) {
                    sb = new StringBuilder();
                    str = "WorkTag(androidx.work.impl.model.WorkTag).\n Expected:\n";
                } else {
                    HashMap hashMap4 = new HashMap(2);
                    hashMap4.put("work_spec_id", new C05470Pr("work_spec_id", "TEXT", null, 1, 1, true));
                    hashMap4.put("system_id", new C05470Pr("system_id", "INTEGER", null, 0, 1, true));
                    HashSet hashSet7 = new HashSet(1);
                    hashSet7.add(new C05420Pm("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                    r5 = new C06100Se("SystemIdInfo", hashMap4, hashSet7, new HashSet(0));
                    A00 = C06100Se.A00(r37, "SystemIdInfo");
                    if (!r5.equals(A00)) {
                        sb = new StringBuilder();
                        str = "SystemIdInfo(androidx.work.impl.model.SystemIdInfo).\n Expected:\n";
                    } else {
                        HashMap hashMap5 = new HashMap(2);
                        hashMap5.put("name", new C05470Pr("name", "TEXT", null, 1, 1, true));
                        hashMap5.put("work_spec_id", new C05470Pr("work_spec_id", "TEXT", null, 2, 1, true));
                        HashSet hashSet8 = new HashSet(1);
                        hashSet8.add(new C05420Pm("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                        HashSet hashSet9 = new HashSet(1);
                        hashSet9.add(new C05340Pe("index_WorkName_work_spec_id", Arrays.asList("work_spec_id"), false));
                        r5 = new C06100Se("WorkName", hashMap5, hashSet8, hashSet9);
                        A00 = C06100Se.A00(r37, "WorkName");
                        if (!r5.equals(A00)) {
                            sb = new StringBuilder();
                            str = "WorkName(androidx.work.impl.model.WorkName).\n Expected:\n";
                        } else {
                            HashMap hashMap6 = new HashMap(2);
                            hashMap6.put("work_spec_id", new C05470Pr("work_spec_id", "TEXT", null, 1, 1, true));
                            hashMap6.put("progress", new C05470Pr("progress", "BLOB", null, 0, 1, true));
                            HashSet hashSet10 = new HashSet(1);
                            hashSet10.add(new C05420Pm("WorkSpec", "CASCADE", "CASCADE", Arrays.asList("work_spec_id"), Arrays.asList("id")));
                            r5 = new C06100Se("WorkProgress", hashMap6, hashSet10, new HashSet(0));
                            A00 = C06100Se.A00(r37, "WorkProgress");
                            if (!r5.equals(A00)) {
                                sb = new StringBuilder();
                                str = "WorkProgress(androidx.work.impl.model.WorkProgress).\n Expected:\n";
                            } else {
                                HashMap hashMap7 = new HashMap(2);
                                hashMap7.put("key", new C05470Pr("key", "TEXT", null, 1, 1, true));
                                hashMap7.put("long_value", new C05470Pr("long_value", "INTEGER", null, 0, 1, false));
                                C06100Se r52 = new C06100Se("Preference", hashMap7, new HashSet(0), new HashSet(0));
                                C06100Se A002 = C06100Se.A00(r37, "Preference");
                                if (r52.equals(A002)) {
                                    return new AnonymousClass0N3(null, true);
                                }
                                StringBuilder sb2 = new StringBuilder("Preference(androidx.work.impl.model.Preference).\n Expected:\n");
                                sb2.append(r52);
                                sb2.append("\n Found:\n");
                                sb2.append(A002);
                                obj = sb2.toString();
                                return new AnonymousClass0N3(obj, false);
                            }
                        }
                    }
                }
            }
        }
        sb.append(str);
        sb.append(r5);
        sb.append("\n Found:\n");
        sb.append(A00);
        obj = sb.toString();
        return new AnonymousClass0N3(obj, false);
    }

    public void A01(AbstractC12920im r3) {
        SQLiteDatabase sQLiteDatabase = ((AnonymousClass0ZE) r3).A00;
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `Dependency` (`work_spec_id` TEXT NOT NULL, `prerequisite_id` TEXT NOT NULL, PRIMARY KEY(`work_spec_id`, `prerequisite_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`prerequisite_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS `index_Dependency_work_spec_id` ON `Dependency` (`work_spec_id`)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS `index_Dependency_prerequisite_id` ON `Dependency` (`prerequisite_id`)");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `WorkSpec` (`id` TEXT NOT NULL, `state` INTEGER NOT NULL, `worker_class_name` TEXT NOT NULL, `input_merger_class_name` TEXT, `input` BLOB NOT NULL, `output` BLOB NOT NULL, `initial_delay` INTEGER NOT NULL, `interval_duration` INTEGER NOT NULL, `flex_duration` INTEGER NOT NULL, `run_attempt_count` INTEGER NOT NULL, `backoff_policy` INTEGER NOT NULL, `backoff_delay_duration` INTEGER NOT NULL, `period_start_time` INTEGER NOT NULL, `minimum_retention_duration` INTEGER NOT NULL, `schedule_requested_at` INTEGER NOT NULL, `run_in_foreground` INTEGER NOT NULL, `out_of_quota_policy` INTEGER NOT NULL, `required_network_type` INTEGER, `requires_charging` INTEGER NOT NULL, `requires_device_idle` INTEGER NOT NULL, `requires_battery_not_low` INTEGER NOT NULL, `requires_storage_not_low` INTEGER NOT NULL, `trigger_content_update_delay` INTEGER NOT NULL, `trigger_max_content_delay` INTEGER NOT NULL, `content_uri_triggers` BLOB, PRIMARY KEY(`id`))");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkSpec_schedule_requested_at` ON `WorkSpec` (`schedule_requested_at`)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `WorkSpec` (`period_start_time`)");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `WorkTag` (`tag` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`tag`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkTag_work_spec_id` ON `WorkTag` (`work_spec_id`)");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `WorkName` (`name` TEXT NOT NULL, `work_spec_id` TEXT NOT NULL, PRIMARY KEY(`name`, `work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkName_work_spec_id` ON `WorkName` (`work_spec_id`)");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        sQLiteDatabase.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'c103703e120ae8cc73c9248622f3cd1e')");
    }
}
