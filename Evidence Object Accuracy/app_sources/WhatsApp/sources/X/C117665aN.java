package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import java.util.ArrayList;
import org.npci.commonlibrary.NPCIFragment;

/* renamed from: X.5aN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117665aN extends FrameLayout implements AbstractC136546My {
    public int A00;
    public int A01;
    public Object A02;
    public ArrayList A03;

    public C117665aN(Context context) {
        super(context);
    }

    public void A00(ArrayList arrayList, AbstractC136396Mj r5) {
        this.A03 = arrayList;
        addView(C117325Zm.A01(arrayList, 0));
        C117305Zk.A0s(this.A03, 0).A0B = r5;
        this.A00 = 0;
        this.A01 = getResources().getDisplayMetrics().widthPixels;
        int i = 1;
        while (true) {
            ArrayList arrayList2 = this.A03;
            if (i < arrayList2.size()) {
                C117795ag A0s = C117305Zk.A0s(arrayList2, i);
                A0s.A0B = r5;
                A0s.setX((float) this.A01);
                addView(A0s);
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC136546My
    public boolean AA5() {
        boolean z;
        String inputValue = C117305Zk.A0s(this.A03, this.A00).getInputValue();
        ArrayList arrayList = this.A03;
        int i = this.A00;
        if (C117305Zk.A0s(arrayList, i).A00 != inputValue.length()) {
            C117325Zm.A01(arrayList, i).requestFocus();
            return false;
        } else if (i == arrayList.size() - 1) {
            C117325Zm.A01(arrayList, i).requestFocus();
            int i2 = 0;
            while (true) {
                ArrayList arrayList2 = this.A03;
                if (i2 >= arrayList2.size()) {
                    return true;
                }
                if (!C117305Zk.A0s(arrayList2, i2).getInputValue().equals(inputValue)) {
                    int i3 = 0;
                    while (true) {
                        ArrayList arrayList3 = this.A03;
                        if (i3 >= arrayList3.size()) {
                            break;
                        }
                        C117305Zk.A0s(arrayList3, i3).setText("");
                        i3++;
                    }
                    int i4 = this.A00;
                    if (i4 != 0) {
                        C117325Zm.A01(this.A03, i4).animate().x((float) this.A01);
                        C117325Zm.A01(this.A03, this.A00 - 1).animate().x(0.0f);
                        int i5 = this.A00 - 1;
                        this.A00 = i5;
                        C117325Zm.A01(this.A03, i5).requestFocus();
                    }
                    ((NPCIFragment) C117305Zk.A0s(this.A03, i2).A0B).A1A(this, getContext().getString(R.string.npci_info_pins_dont_match));
                    return false;
                }
                i2++;
            }
        } else {
            int i6 = this.A00;
            ArrayList arrayList4 = this.A03;
            if (i6 >= arrayList4.size() - 1) {
                z = false;
            } else {
                C117325Zm.A01(arrayList4, i6).animate().x((float) (-this.A01));
                C117325Zm.A01(this.A03, this.A00 + 1).animate().x(0.0f);
                int i7 = this.A00 + 1;
                this.A00 = i7;
                C117325Zm.A01(this.A03, i7).requestFocus();
                z = true;
            }
            return !z;
        }
    }

    @Override // X.AbstractC136546My
    public void Aez(Drawable drawable, View.OnClickListener onClickListener, String str, int i, boolean z, boolean z2) {
        int i2 = 0;
        while (true) {
            ArrayList arrayList = this.A03;
            if (i2 < arrayList.size()) {
                C117305Zk.A0s(arrayList, i2).Aez(drawable, onClickListener, str, 0, true, true);
                i2++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC136546My
    public boolean Af0() {
        return C117305Zk.A0s(this.A03, this.A00).Af0();
    }

    @Override // X.AbstractC136546My
    public Object getFormDataTag() {
        Object obj = this.A02;
        if (obj == null) {
            return C117305Zk.A0s(this.A03, 0).A07;
        }
        return obj;
    }

    @Override // X.AbstractC136546My
    public String getInputValue() {
        return C117305Zk.A0s(this.A03, 0).getInputValue();
    }

    public void setFormDataTag(Object obj) {
        this.A02 = obj;
    }

    public void setText(String str) {
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A03;
            if (i < arrayList.size()) {
                C117305Zk.A0s(arrayList, i).setText(str);
                i++;
            } else {
                return;
            }
        }
    }
}
