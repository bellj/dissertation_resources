package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.whatsapp.notification.PopupNotification;

/* renamed from: X.2iE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55332iE extends AnonymousClass01A {
    public final /* synthetic */ PopupNotification A00;

    public C55332iE(PopupNotification popupNotification) {
        this.A00 = popupNotification;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A00.A1M.size();
    }

    @Override // X.AnonymousClass01A
    public Object A05(ViewGroup viewGroup, int i) {
        PopupNotification popupNotification = this.A00;
        RelativeLayout relativeLayout = new RelativeLayout(popupNotification);
        ScrollView scrollView = new ScrollView(popupNotification);
        AbstractC15340mz r0 = (AbstractC15340mz) popupNotification.A1M.get(i);
        View A02 = PopupNotification.A02(popupNotification, r0);
        relativeLayout.setTag(r0.A0z);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        scrollView.addView(A02);
        relativeLayout.addView(scrollView, layoutParams);
        viewGroup.addView(relativeLayout);
        return relativeLayout;
    }

    @Override // X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        viewGroup.removeView((View) obj);
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return C12970iu.A1Z(view, obj);
    }
}
