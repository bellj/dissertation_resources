package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

/* renamed from: X.1MO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MO implements Executor {
    public int A00;
    public final int A01;
    public final AbstractC14440lR A02;
    public final Queue A03 = new ArrayDeque();
    public final boolean A04;

    public AnonymousClass1MO(AbstractC14440lR r2, int i, boolean z) {
        this.A02 = r2;
        this.A01 = i;
        this.A04 = z;
    }

    public final void A00() {
        Runnable runnable = (Runnable) this.A03.poll();
        if (runnable != null) {
            this.A00++;
            boolean z = this.A04;
            AbstractC14440lR r0 = this.A02;
            if (z) {
                r0.Ab6(runnable);
            } else {
                r0.Ab2(runnable);
            }
        }
    }

    @Override // java.util.concurrent.Executor
    public synchronized void execute(Runnable runnable) {
        this.A03.offer(new RunnableBRunnable0Shape8S0200000_I0_8(this, 35, runnable));
        if (this.A00 < this.A01) {
            A00();
        }
    }
}
