package X;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Set;

/* renamed from: X.3pn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78943pn extends AbstractC78753pU {
    public static final HashMap A07;
    public static final Parcelable.Creator CREATOR = new C98554is();
    public int A00;
    public PendingIntent A01;
    public C78373oo A02;
    public String A03;
    public byte[] A04;
    public final int A05;
    public final Set A06;

    public C78943pn() {
        this.A06 = new AnonymousClass01b(3);
        this.A05 = 1;
    }

    public C78943pn(PendingIntent pendingIntent, C78373oo r2, String str, Set set, byte[] bArr, int i, int i2) {
        this.A06 = set;
        this.A05 = i;
        this.A03 = str;
        this.A00 = i2;
        this.A04 = bArr;
        this.A01 = pendingIntent;
        this.A02 = r2;
    }

    static {
        HashMap A11 = C12970iu.A11();
        A07 = A11;
        A11.put("accountType", new C78633pE(null, "accountType", 7, 7, 2, false, false));
        A11.put("status", new C78633pE(null, "status", 0, 0, 3, false, false));
        A11.put("transferBytes", new C78633pE(null, "transferBytes", 8, 8, 4, false, false));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        Set set = this.A06;
        if (AbstractC78753pU.A00(set, 1)) {
            C95654e8.A07(parcel, 1, this.A05);
        }
        if (AbstractC78753pU.A00(set, 2)) {
            C95654e8.A0D(parcel, this.A03, 2, true);
        }
        if (AbstractC78753pU.A00(set, 3)) {
            C95654e8.A07(parcel, 3, this.A00);
        }
        if (AbstractC78753pU.A00(set, 4)) {
            C95654e8.A0G(parcel, this.A04, 4, true);
        }
        if (AbstractC78753pU.A00(set, 5)) {
            C95654e8.A0B(parcel, this.A01, 5, i, true);
        }
        if (AbstractC78753pU.A00(set, 6)) {
            C95654e8.A0B(parcel, this.A02, 6, i, true);
        }
        C95654e8.A06(parcel, A00);
    }
}
