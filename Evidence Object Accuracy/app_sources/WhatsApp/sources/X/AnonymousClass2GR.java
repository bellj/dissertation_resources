package X;

import android.text.Editable;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Pair;
import android.widget.ImageButton;
import com.whatsapp.status.playback.widget.StatusEditText;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.2GR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GR extends C469928m {
    public final /* synthetic */ AnonymousClass2GS A00;

    public /* synthetic */ AnonymousClass2GR(AnonymousClass2GS r1) {
        this.A00 = r1;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        String str;
        for (UnderlineSpan underlineSpan : (UnderlineSpan[]) editable.getSpans(0, editable.length(), UnderlineSpan.class)) {
            editable.removeSpan(underlineSpan);
        }
        AnonymousClass2GS r4 = this.A00;
        StatusEditText statusEditText = r4.A0G;
        C42971wC.A07(statusEditText.getContext(), statusEditText.getPaint(), editable, r4.A0B, r4.A0C, r4.A0F, 1.1f);
        String obj = editable.toString();
        boolean z = true;
        boolean z2 = false;
        if (TextUtils.getTrimmedLength(obj) > 0) {
            z2 = true;
        }
        ImageButton imageButton = r4.A06;
        imageButton.setEnabled(z2);
        C63223At.A00(imageButton, z2, true);
        String A01 = C33771f3.A01(obj);
        int length = obj.length();
        r4.A08.A0B(Integer.valueOf(AnonymousClass24D.A02(obj, 0, length)));
        if (!r4.A04 || !r4.A0I) {
            C14310lE r3 = r4.A0A;
            C37071lG r2 = r4.A01;
            if (r2 == null) {
                r2 = new C37071lG(r4.A09);
                r4.A01 = r2;
            }
            r3.A04(editable, r2);
        }
        int length2 = editable.length();
        int A012 = AnonymousClass2VC.A01(editable, 0, length2) + (AnonymousClass2GS.A00(editable, 0, length2) * 49);
        int i = 700;
        if (A01 == null || A01.equals(r4.A03) || r4.A04) {
            z = false;
        }
        boolean z3 = r4.A0I;
        if (z3 && z) {
            i = 350;
        }
        if (A012 >= i && r4.A00 == 0) {
            int inputType = statusEditText.getInputType();
            r4.A00 = inputType;
            if (inputType != 0) {
                statusEditText.setInputType(inputType | 524288);
                statusEditText.setText(obj);
                statusEditText.setSelection(length);
            }
            if (!z3) {
                return;
            }
        } else if (!z3) {
            int i2 = r4.A00;
            if (i2 != 0) {
                statusEditText.setInputType(i2);
                r4.A00 = 0;
                return;
            }
            return;
        }
        ArrayList arrayList = null;
        if (!TextUtils.isEmpty(obj)) {
            Pattern pattern = C33771f3.A01;
            if (length > 4096) {
                str = obj.substring(0, 4096);
            } else {
                str = obj;
            }
            Matcher matcher = pattern.matcher(str);
            while (matcher.find()) {
                Pair A00 = C33771f3.A00(obj, matcher.start(), matcher.end());
                if (A00 != null) {
                    String substring = obj.substring(((Number) A00.first).intValue(), ((Number) A00.second).intValue());
                    String[] strArr = C33771f3.A03;
                    int length3 = strArr.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 < length3) {
                            String str2 = strArr[i3];
                            if (substring.regionMatches(true, 0, str2, 0, str2.length())) {
                                if (arrayList == null) {
                                    arrayList = new ArrayList();
                                }
                                if (!arrayList.contains(A00)) {
                                    arrayList.add(A00);
                                }
                            } else {
                                i3++;
                            }
                        }
                    }
                }
            }
        }
        if (!(A01 == null || arrayList == null || arrayList.size() <= 0 || statusEditText.getText() == null)) {
            for (int i4 = 0; i4 < arrayList.size(); i4++) {
                statusEditText.getText().setSpan(new UnderlineSpan(), ((Number) ((Pair) arrayList.get(i4)).first).intValue(), ((Number) ((Pair) arrayList.get(i4)).second).intValue(), 33);
            }
        }
    }
}
