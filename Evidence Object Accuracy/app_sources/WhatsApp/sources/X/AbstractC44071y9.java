package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.search.views.itemviews.AudioPlayerMetadataView;
import com.whatsapp.search.views.itemviews.AudioPlayerView;
import com.whatsapp.search.views.itemviews.VoiceNoteProfileAvatarView;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;

/* renamed from: X.1y9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC44071y9 extends AbstractC44081yA {
    public C14900mE A00;
    public C239613r A01;
    public C16170oZ A02;
    public AnonymousClass11P A03;
    public C15890o4 A04;
    public C14850m9 A05;
    public AnonymousClass109 A06;
    public C22370yy A07;
    public C26161Cg A08;
    public C30421Xi A09;
    public AnonymousClass1CY A0A;
    public AnonymousClass19O A0B;
    public AnonymousClass01H A0C;
    public final View.OnClickListener A0D = new ViewOnClickCListenerShape4S0100000_I0_4(this, 0);
    public final AbstractView$OnClickListenerC34281fs A0E = new ViewOnClickCListenerShape15S0100000_I0_2(this, 21);
    public final AbstractView$OnClickListenerC34281fs A0F = new ViewOnClickCListenerShape15S0100000_I0_2(this, 19);
    public final AbstractView$OnClickListenerC34281fs A0G = new ViewOnClickCListenerShape15S0100000_I0_2(this, 20);

    public AbstractC44071y9(Context context) {
        super(context);
    }

    public void A01() {
        AnonymousClass1J1 r1;
        C15370n3 A0B;
        if (!(this instanceof C44061y8)) {
            AnonymousClass34W r12 = (AnonymousClass34W) this;
            C30421Xi r6 = r12.A09;
            AudioPlayerView audioPlayerView = r12.A03;
            AnonymousClass3JG.A00(r12.A0E, r12.A0G, r12.A0F, r12.A0D, r6, new AbstractC116155Ui() { // from class: X.3Zt
                @Override // X.AbstractC116155Ui
                public final void AWP(int i) {
                    String A03;
                    AnonymousClass34W r4 = AnonymousClass34W.this;
                    if (i == 0) {
                        r4.A03.A03.setVisibility(8);
                        r4.A02();
                    } else if (i == 1) {
                        ConversationRowAudioPreview conversationRowAudioPreview = r4.A00;
                        conversationRowAudioPreview.A00();
                        conversationRowAudioPreview.setDuration(C44891zj.A03(r4.A02, ((AbstractC16130oV) r4.A09).A01));
                        C65033Hw.A00(r4.A03.A03, r4.A06, r4.A09);
                    } else if (i == 2 || i == 3) {
                        r4.A03.A03.setVisibility(8);
                        ConversationRowAudioPreview conversationRowAudioPreview2 = r4.A00;
                        conversationRowAudioPreview2.A00();
                        C30421Xi r13 = r4.A09;
                        AnonymousClass018 r2 = r4.A02;
                        int i2 = ((AbstractC16130oV) r13).A00;
                        if (i2 != 0) {
                            A03 = C38131nZ.A04(r2, (long) i2);
                        } else {
                            A03 = C44891zj.A03(r2, ((AbstractC16130oV) r13).A01);
                        }
                        conversationRowAudioPreview2.setDuration(A03);
                    }
                }
            }, audioPlayerView);
            int AFz = ((AnonymousClass19F) r12.A04.get()).AFz(r12.A09.A11);
            if (AFz >= 0) {
                audioPlayerView.setSeekbarProgress(AFz);
                return;
            }
            return;
        }
        C44061y8 r4 = (C44061y8) this;
        C30421Xi r9 = ((AbstractC44071y9) r4).A09;
        AudioPlayerView audioPlayerView2 = r4.A09;
        AnonymousClass3JG.A00(((AbstractC44071y9) r4).A0E, r4.A0G, r4.A0F, ((AbstractC44071y9) r4).A0D, r9, new AbstractC116155Ui() { // from class: X.3Zu
            @Override // X.AbstractC116155Ui
            public final void AWP(int i) {
                String A03;
                C44061y8 r42 = C44061y8.this;
                if (i == 0) {
                    r42.A09.A03.setVisibility(8);
                    r42.A02();
                } else if (i == 1) {
                    r42.A08.setDescription(C44891zj.A03(r42.A05, ((AbstractC16130oV) ((AbstractC44071y9) r42).A09).A01));
                    C65033Hw.A00(r42.A09.A03, ((AbstractC44071y9) r42).A06, ((AbstractC44071y9) r42).A09);
                } else if (i == 2 || i == 3) {
                    r42.A09.A03.setVisibility(8);
                    AudioPlayerMetadataView audioPlayerMetadataView = r42.A08;
                    C30421Xi r13 = ((AbstractC44071y9) r42).A09;
                    AnonymousClass018 r2 = r42.A05;
                    int i2 = ((AbstractC16130oV) r13).A00;
                    if (i2 != 0) {
                        A03 = C38131nZ.A04(r2, (long) i2);
                    } else {
                        A03 = C44891zj.A03(r2, ((AbstractC16130oV) r13).A01);
                    }
                    audioPlayerMetadataView.setDescription(A03);
                }
            }
        }, audioPlayerView2);
        VoiceNoteProfileAvatarView voiceNoteProfileAvatarView = r4.A0A;
        voiceNoteProfileAvatarView.setupMicBackgroundColor(R.color.search_attachment_background);
        C65023Hv.A02(((AbstractC44071y9) r4).A09, audioPlayerView2, voiceNoteProfileAvatarView);
        boolean z = r4.A0D;
        if (z) {
            AnonymousClass1IS r0 = ((AbstractC44071y9) r4).A09.A0z;
            voiceNoteProfileAvatarView.A02(0, false, r0.A02, C15380n4.A0J(r0.A00));
        }
        if (((AbstractC44071y9) r4).A09.A1B()) {
            voiceNoteProfileAvatarView.A0A = true;
            voiceNoteProfileAvatarView.A05.setImageResource(R.drawable.audio_ptt_forwarded_icon);
            voiceNoteProfileAvatarView.A04.setVisibility(4);
        } else {
            voiceNoteProfileAvatarView.setIsForwardedByNonAuthorPttUi(false);
            ImageView imageView = voiceNoteProfileAvatarView.A05;
            C30421Xi r7 = ((AbstractC44071y9) r4).A09;
            AnonymousClass1IS r13 = r7.A0z;
            if (r13.A02) {
                r1 = r4.A0E;
                C15570nT r02 = r4.A01;
                r02.A08();
                A0B = r02.A01;
                AnonymousClass009.A05(A0B);
            } else {
                ImageView imageView2 = voiceNoteProfileAvatarView.A03;
                AbstractC14640lm r2 = r13.A00;
                AnonymousClass009.A05(r2);
                if (C15380n4.A0J(r2)) {
                    AbstractC14640lm A0B2 = r7.A0B();
                    AnonymousClass009.A05(A0B2);
                    imageView2.setVisibility(0);
                    imageView.setVisibility(8);
                    r4.A0E.A06(imageView2, r4.A02.A0B(A0B2));
                } else {
                    imageView2.setVisibility(8);
                    imageView.setVisibility(0);
                    r1 = r4.A0E;
                    A0B = r4.A02.A0B(r2);
                }
            }
            r1.A06(imageView, A0B);
        }
        int AFz2 = ((AnonymousClass19F) r4.A0B.get()).AFz(((AbstractC44071y9) r4).A09.A11);
        if (AFz2 >= 0) {
            audioPlayerView2.setSeekbarProgress(AFz2);
        }
        if (z) {
            C30421Xi r14 = ((AbstractC44071y9) r4).A09;
            if (!r14.A12(32768)) {
                audioPlayerView2.setTag(null);
                audioPlayerView2.A00();
                return;
            }
            audioPlayerView2.setTag(r14.A0z);
            r4.A06.A01(((AbstractC44071y9) r4).A09, new RunnableBRunnable0Shape11S0100000_I0_11(r4, 33));
        }
    }

    public C30421Xi getFMessageAudio() {
        return this.A09;
    }

    public final void setAudioMessage(C30421Xi r1) {
        this.A09 = r1;
        A01();
    }
}
