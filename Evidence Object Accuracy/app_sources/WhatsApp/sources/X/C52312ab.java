package X;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.transition.Fade;
import android.transition.TransitionValues;
import android.view.ViewGroup;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.2ab  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52312ab extends Fade {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ ViewProfilePhoto A02;

    public C52312ab(ViewProfilePhoto viewProfilePhoto, int i, int i2) {
        this.A02 = viewProfilePhoto;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // android.transition.Visibility, android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        int i;
        ObjectAnimator objectAnimator = (ObjectAnimator) super.createAnimator(viewGroup, transitionValues, transitionValues2);
        if (!(objectAnimator == null || (i = this.A01) == 0)) {
            ViewProfilePhoto viewProfilePhoto = this.A02;
            objectAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(objectAnimator, this, i, viewProfilePhoto.getWindow().getStatusBarColor()) { // from class: X.4ek
                public final /* synthetic */ int A00;
                public final /* synthetic */ int A01;
                public final /* synthetic */ ObjectAnimator A02;
                public final /* synthetic */ C52312ab A03;

                {
                    this.A03 = r2;
                    this.A00 = r3;
                    this.A01 = r4;
                    this.A02 = r1;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    C52312ab.A00(this.A02, this.A03, this.A00, this.A01);
                }
            });
            viewProfilePhoto.getWindow().setNavigationBarColor(C016907y.A03(C12960it.A00(objectAnimator), this.A00, viewProfilePhoto.getWindow().getNavigationBarColor()));
        }
        return objectAnimator;
    }
}
