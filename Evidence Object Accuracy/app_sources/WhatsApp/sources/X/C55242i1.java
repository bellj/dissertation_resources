package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.StateSet;
import android.view.View;
import android.widget.CheckBox;
import com.whatsapp.R;

/* renamed from: X.2i1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55242i1 extends AnonymousClass03U implements AnonymousClass02B {
    public final View A00;
    public final CheckBox A01;
    public final int[] A02;
    public final int[] A03;

    public C55242i1(View view, int[] iArr, int[] iArr2) {
        super(view);
        this.A00 = AnonymousClass028.A0D(view, R.id.selectionRingContainer);
        this.A01 = (CheckBox) AnonymousClass028.A0D(view, R.id.selectionRing);
        this.A02 = iArr;
        this.A03 = iArr2;
    }

    public void A08(boolean z, int i) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        CheckBox checkBox = this.A01;
        Resources resources = checkBox.getResources();
        int i2 = R.drawable.group_profile_emoji_editor_color_selection_unchecked;
        if (z) {
            i2 = R.drawable.group_profile_emoji_editor_color_selection_checked;
        }
        Drawable A04 = AnonymousClass00X.A04(C12980iv.A0I(checkBox), resources, i2);
        AnonymousClass009.A05(A04);
        LayerDrawable layerDrawable = (LayerDrawable) A04;
        int[] iArr = this.A02;
        int i3 = iArr[i % iArr.length];
        int[] iArr2 = this.A03;
        int i4 = iArr2[i % iArr2.length];
        Drawable findDrawableByLayerId = layerDrawable.findDrawableByLayerId(R.id.color_selection_ring);
        Drawable findDrawableByLayerId2 = layerDrawable.findDrawableByLayerId(R.id.color_selection_circle);
        Drawable A042 = AnonymousClass2GE.A04(findDrawableByLayerId, i4);
        Drawable A043 = AnonymousClass2GE.A04(findDrawableByLayerId2, i3);
        layerDrawable.setDrawableByLayerId(R.id.color_selection_ring, A042);
        layerDrawable.setDrawableByLayerId(R.id.color_selection_circle, A043);
        stateListDrawable.addState(StateSet.WILD_CARD, layerDrawable);
        stateListDrawable.mutate();
        checkBox.setButtonDrawable(stateListDrawable);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0018, code lost:
        if (r1 != false) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r1 != false) goto L_0x001f;
     */
    @Override // X.AnonymousClass02B
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANq(java.lang.Object r7) {
        /*
            r6 = this;
            java.lang.Number r7 = (java.lang.Number) r7
            int r5 = r6.A00()
            int r0 = r7.intValue()
            r4 = 1
            boolean r3 = X.C12960it.A1V(r5, r0)
            android.widget.CheckBox r2 = r6.A01
            boolean r1 = r2.isChecked()
            if (r3 == 0) goto L_0x001a
            r0 = 1
            if (r1 == 0) goto L_0x002c
        L_0x001a:
            r0 = 0
            if (r3 != 0) goto L_0x002c
            if (r1 == 0) goto L_0x002c
        L_0x001f:
            if (r5 < 0) goto L_0x0028
            if (r0 != 0) goto L_0x0025
            if (r4 == 0) goto L_0x0028
        L_0x0025:
            r6.A08(r3, r5)
        L_0x0028:
            r2.setChecked(r3)
            return
        L_0x002c:
            r4 = 0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C55242i1.ANq(java.lang.Object):void");
    }
}
