package X;

import org.json.JSONObject;

/* renamed from: X.58Q  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58Q implements AnonymousClass5W9 {
    @Override // X.AnonymousClass5W9
    public AnonymousClass5UW A8V(JSONObject jSONObject) {
        return new AnonymousClass58H(C72453ed.A0u(jSONObject), jSONObject.getBoolean("booleanEquals"));
    }

    @Override // X.AnonymousClass5W9
    public String ADP() {
        return "booleanEquals";
    }
}
