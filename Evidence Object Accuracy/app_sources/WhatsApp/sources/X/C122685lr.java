package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122685lr extends AbstractC118835cS {
    public ImageView A00;
    public LinearLayout A01;
    public TextView A02;

    public C122685lr(View view) {
        super(view);
        this.A01 = (LinearLayout) view.findViewById(R.id.payment_support_container);
        this.A00 = C12970iu.A0L(view, R.id.payment_support_icon);
        this.A02 = C12960it.A0J(view, R.id.payment_support_title);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r4, int i) {
        C123165mi r42 = (C123165mi) r4;
        this.A01.setOnClickListener(r42.A00);
        ImageView imageView = this.A00;
        AnonymousClass2GE.A05(imageView.getContext(), imageView, R.color.settings_icon);
        boolean z = r42.A01;
        TextView textView = this.A02;
        int i2 = R.string.settings_help;
        if (z) {
            i2 = R.string.transaction_details_get_help_label;
        }
        textView.setText(i2);
    }
}
