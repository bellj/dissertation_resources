package X;

import java.util.Arrays;

/* renamed from: X.50H  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50H implements AbstractC115645Sj {
    @Override // X.AbstractC115645Sj
    public final byte[] Agu(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
