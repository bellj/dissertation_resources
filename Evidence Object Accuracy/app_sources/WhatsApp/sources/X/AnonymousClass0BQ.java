package X;

import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import java.util.ArrayList;

/* renamed from: X.0BQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BQ extends BaseAdapter {
    public int A00 = -1;
    public AnonymousClass07H A01;
    public boolean A02;
    public final int A03;
    public final LayoutInflater A04;
    public final boolean A05;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    public AnonymousClass0BQ(LayoutInflater layoutInflater, AnonymousClass07H r3, int i, boolean z) {
        this.A05 = z;
        this.A04 = layoutInflater;
        this.A01 = r3;
        this.A03 = i;
        A01();
    }

    /* renamed from: A00 */
    public C07340Xp getItem(int i) {
        ArrayList A04;
        boolean z = this.A05;
        AnonymousClass07H r0 = this.A01;
        if (z) {
            r0.A05();
            A04 = r0.A08;
        } else {
            A04 = r0.A04();
        }
        int i2 = this.A00;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return (C07340Xp) A04.get(i);
    }

    public void A01() {
        AnonymousClass07H r0 = this.A01;
        C07340Xp r4 = r0.A04;
        if (r4 != null) {
            r0.A05();
            ArrayList arrayList = r0.A08;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                if (arrayList.get(i) == r4) {
                    this.A00 = i;
                    return;
                }
            }
        }
        this.A00 = -1;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        ArrayList A04;
        boolean z = this.A05;
        AnonymousClass07H r0 = this.A01;
        if (z) {
            r0.A05();
            A04 = r0.A08;
        } else {
            A04 = r0.A04();
        }
        int i = this.A00;
        int size = A04.size();
        if (i >= 0) {
            return size - 1;
        }
        return size;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
        if (r5 == r1) goto L_0x002e;
     */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r7, android.view.View r8, android.view.ViewGroup r9) {
        /*
            r6 = this;
            r4 = 0
            if (r8 != 0) goto L_0x000b
            android.view.LayoutInflater r1 = r6.A04
            int r0 = r6.A03
            android.view.View r8 = r1.inflate(r0, r9, r4)
        L_0x000b:
            X.0Xp r0 = r6.getItem(r7)
            int r5 = r0.getGroupId()
            int r0 = r7 + -1
            if (r0 < 0) goto L_0x0044
            X.0Xp r0 = r6.getItem(r0)
            int r1 = r0.getGroupId()
        L_0x001f:
            r3 = r8
            androidx.appcompat.view.menu.ListMenuItemView r3 = (androidx.appcompat.view.menu.ListMenuItemView) r3
            X.07H r0 = r6.A01
            boolean r0 = r0.A0G()
            r2 = 1
            if (r0 == 0) goto L_0x002e
            r0 = 1
            if (r5 != r1) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            r3.setGroupDividerEnabled(r0)
            r1 = r8
            X.0hg r1 = (X.AbstractC12290hg) r1
            boolean r0 = r6.A02
            if (r0 == 0) goto L_0x003c
            r3.setForceShowIcon(r2)
        L_0x003c:
            X.0Xp r0 = r6.getItem(r7)
            r1.AIt(r0, r4)
            return r8
        L_0x0044:
            r1 = r5
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0BQ.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    @Override // android.widget.BaseAdapter
    public void notifyDataSetChanged() {
        A01();
        super.notifyDataSetChanged();
    }
}
