package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124485pc extends Enum {
    public static final EnumC124485pc A00 = new EnumC124485pc("KEY_CODE_EMPTY", "Your organization key is empty. Please provide a organization key.", 0, 1001);
    public static final EnumC124485pc A01 = new EnumC124485pc("KEY_CODE_INVALID", "Your organization key is invalid. Please contact your system administrator or UPI support team.", 1, 1002);
    public static final EnumC124485pc A02 = new EnumC124485pc("PARSER_MISCONFIG", "XML Parser configuration error.Keys.xml may not be formatted correctly.", 3, 1004);
    public static final EnumC124485pc A03 = new EnumC124485pc("TRUST_NOT_VALID", "Trust is not valid", 7, 1008);
    public static final EnumC124485pc A04 = new EnumC124485pc("UNKNOWN_ERROR", "Unknown error occurred.", 6, 1007);
    public final int code;
    public final String description;

    static {
        new EnumC124485pc("PUBLICKEY_NOT_FOUND", "Public key file not found please contact your system administrator UPI support team", 2, 1003);
        new EnumC124485pc("XML_PATH_ERROR", "XML File is not found or cannot be read.", 4, 1005);
        new EnumC124485pc("KEYS_NOT_VALID", "Keys are not valid. Please contact your system administrator UPI support team", 5, 1006);
    }

    public EnumC124485pc(String str, String str2, int i, int i2) {
        this.code = i2;
        this.description = str2;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.code);
        A0h.append(": ");
        return C12960it.A0d(this.description, A0h);
    }
}
