package X;

import com.google.common.collect.Maps;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3tk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81273tk extends Maps.EntrySet<K, Collection<V>> {
    public final /* synthetic */ C81143tX this$1;

    public C81273tk() {
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public C81273tk(C81143tX r1) {
        this();
        this.this$1 = r1;
    }

    public void clear() {
        map().clear();
    }

    public boolean contains(Object obj) {
        return C50912Rv.safeContains(this.this$1.submap.entrySet(), obj);
    }

    public boolean isEmpty() {
        return map().isEmpty();
    }

    public Iterator iterator() {
        return new AnonymousClass5DG(this.this$1);
    }

    public Map map() {
        return this.this$1;
    }

    public boolean remove(Object obj) {
        if (!contains(obj)) {
            return false;
        }
        this.this$1.this$0.removeValuesForKey(((Map.Entry) obj).getKey());
        return true;
    }

    public boolean removeAll(Collection collection) {
        try {
            return super.removeAll(collection);
        } catch (UnsupportedOperationException unused) {
            return C28281Ml.removeAllImpl(this, collection.iterator());
        }
    }

    public boolean retainAll(Collection collection) {
        try {
            return super.retainAll(collection);
        } catch (UnsupportedOperationException unused) {
            HashSet newHashSetWithExpectedSize = C28281Ml.newHashSetWithExpectedSize(collection.size());
            for (Object obj : collection) {
                if (contains(obj) && (obj instanceof Map.Entry)) {
                    newHashSetWithExpectedSize.add(((Map.Entry) obj).getKey());
                }
            }
            return map().keySet().retainAll(newHashSetWithExpectedSize);
        }
    }

    public int size() {
        return map().size();
    }
}
