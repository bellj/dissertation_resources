package X;

/* renamed from: X.68k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328368k implements AnonymousClass1FK {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ AbstractActivityC121705jc A01;

    public C1328368k(AnonymousClass3FE r1, AbstractActivityC121705jc r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        AbstractActivityC121705jc.A0l(this.A00, null, r4.A00);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        AbstractActivityC121705jc.A0l(this.A00, null, r4.A00);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r2) {
        C117315Zl.A0T(this.A00);
    }
}
