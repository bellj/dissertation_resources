package X;

/* renamed from: X.39p  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass39p {
    A01(3),
    A02(4),
    A03(6),
    A05(7),
    A04(0);
    
    public final int value;

    AnonymousClass39p(int i) {
        this.value = i;
    }
}
