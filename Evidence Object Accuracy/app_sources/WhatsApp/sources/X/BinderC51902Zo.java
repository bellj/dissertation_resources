package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import com.whatsapp.instrumentation.api.InstrumentationService;

/* renamed from: X.2Zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class BinderC51902Zo extends Binder implements IInterface {
    public final /* synthetic */ InstrumentationService A00;

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this;
    }

    public BinderC51902Zo(InstrumentationService instrumentationService) {
        this.A00 = instrumentationService;
        attachInterface(this, "com.whatsapp.instrumentation.InstrumentationInterface");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x015e  */
    @Override // android.os.Binder
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTransact(int r30, android.os.Parcel r31, android.os.Parcel r32, int r33) {
        /*
        // Method dump skipped, instructions count: 2182
        */
        throw new UnsupportedOperationException("Method not decompiled: X.BinderC51902Zo.onTransact(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
