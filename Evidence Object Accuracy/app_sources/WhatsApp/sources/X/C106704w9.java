package X;

/* renamed from: X.4w9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106704w9 implements AbstractC116785Ww {
    public boolean A00;
    public final C107144wr A01 = new C107144wr(null);
    public final C95304dT A02 = C95304dT.A05(16384);

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r6) {
        this.A01.A8b(r6, new C92824Xo(Integer.MIN_VALUE, 0, 1));
        r6.A9V();
        C106904wT.A00(r6, -9223372036854775807L);
    }

    @Override // X.AbstractC116785Ww
    public int AZn(AnonymousClass5Yf r6, AnonymousClass4IG r7) {
        C95304dT r4 = this.A02;
        int read = r6.read(r4.A02, 0, 16384);
        if (read == -1) {
            return -1;
        }
        r4.A0S(0);
        r4.A0R(read);
        if (!this.A00) {
            this.A01.A04 = 0;
            this.A00 = true;
        }
        this.A01.A7a(r4);
        return 0;
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        this.A00 = false;
        this.A01.AbP();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        r12.Aaj();
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003f, code lost:
        if ((r6 - r2) >= 8192) goto L_0x009a;
     */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean Ae5(X.AnonymousClass5Yf r12) {
        /*
            r11 = this;
            r5 = 10
            X.4dT r4 = X.C95304dT.A05(r5)
            r3 = 0
            r2 = 0
        L_0x0008:
            X.C95304dT.A06(r12, r4, r5)
            r4.A0S(r3)
            int r1 = r4.A0D()
            r0 = 4801587(0x494433, float:6.728456E-39)
            if (r1 == r0) goto L_0x008a
            r12.Aaj()
            r12.A5r(r2)
            r6 = r2
        L_0x001e:
            r5 = 0
        L_0x001f:
            byte[] r1 = r4.A02
            r0 = 7
            r12.AZ4(r1, r3, r0)
            r4.A0S(r3)
            int r8 = r4.A0F()
            r0 = 44096(0xac40, float:6.1792E-41)
            if (r8 == r0) goto L_0x0045
            r0 = 44097(0xac41, float:6.1793E-41)
            if (r8 == r0) goto L_0x0045
            r12.Aaj()
            int r6 = r6 + 1
            int r1 = r6 - r2
            r0 = 8192(0x2000, float:1.14794E-41)
            if (r1 >= r0) goto L_0x009a
            r12.A5r(r6)
            goto L_0x001e
        L_0x0045:
            r0 = 1
            int r5 = r5 + r0
            r10 = 4
            if (r5 < r10) goto L_0x004b
            return r0
        L_0x004b:
            byte[] r9 = r4.A02
            int r0 = r9.length
            r7 = 7
            if (r0 < r7) goto L_0x009a
            r0 = 2
            byte r0 = r9[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r1 = r0 << 8
            r0 = 3
            byte r0 = r9[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r1 = r1 | r0
            r0 = 65535(0xffff, float:9.1834E-41)
            if (r1 != r0) goto L_0x0088
            byte r0 = r9[r10]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r1 = r0 << 16
            r0 = 5
            byte r0 = r9[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r0 << 8
            r1 = r1 | r0
            r0 = 6
            byte r0 = r9[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r1 = r1 | r0
        L_0x0077:
            r0 = 44097(0xac41, float:6.1793E-41)
            if (r8 != r0) goto L_0x007e
            int r7 = r7 + 2
        L_0x007e:
            int r1 = r1 + r7
            r0 = -1
            if (r1 == r0) goto L_0x009a
            int r0 = r1 + -7
            r12.A5r(r0)
            goto L_0x001f
        L_0x0088:
            r7 = 4
            goto L_0x0077
        L_0x008a:
            r0 = 3
            r4.A0T(r0)
            int r1 = r4.A0B()
            int r0 = r1 + 10
            int r2 = r2 + r0
            r12.A5r(r1)
            goto L_0x0008
        L_0x009a:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106704w9.Ae5(X.5Yf):boolean");
    }
}
