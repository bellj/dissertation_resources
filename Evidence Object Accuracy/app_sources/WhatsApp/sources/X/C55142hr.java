package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2hr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55142hr extends AnonymousClass03U {
    public View A00;
    public ThumbnailButton A01;
    public final /* synthetic */ CatalogCarouselDetailImageView A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55142hr(View view, CatalogCarouselDetailImageView catalogCarouselDetailImageView) {
        super(view);
        this.A02 = catalogCarouselDetailImageView;
        this.A00 = AnonymousClass028.A0D(view, R.id.product_detail_image_container);
        this.A01 = (ThumbnailButton) AnonymousClass028.A0D(view, R.id.product_detail_image);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r9 == false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(int r12, int r13, boolean r14) {
        /*
            r11 = this;
            if (r13 != 0) goto L_0x0086
            r2 = 0
        L_0x0004:
            r10 = 1
            boolean r9 = X.C12990iw.A1Y(r12, r13)
            if (r12 == r13) goto L_0x000c
            r10 = 0
        L_0x000c:
            android.view.View r4 = r11.A00
            android.view.ViewGroup$LayoutParams r1 = r4.getLayoutParams()
            r8 = -1
            r1.height = r8
            if (r14 == 0) goto L_0x001a
            r0 = -2
            if (r9 != 0) goto L_0x001b
        L_0x001a:
            r0 = -1
        L_0x001b:
            r1.width = r0
            r4.setLayoutParams(r1)
            com.whatsapp.components.button.ThumbnailButton r5 = r11.A01
            android.view.ViewGroup$LayoutParams r4 = r5.getLayoutParams()
            r6 = 4605380978949069210(0x3fe999999999999a, double:0.8)
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x0050
            com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView r0 = r11.A02
            X.01d r0 = r0.A07
            android.view.WindowManager r0 = r0.A0O()
            android.graphics.Point r1 = new android.graphics.Point
            r1.<init>()
            android.view.Display r0 = r0.getDefaultDisplay()
            r0.getSize(r1)
            int r0 = r1.x
            r4.height = r0
            double r0 = (double) r0
            double r0 = r0 * r6
            int r8 = (int) r0
        L_0x004a:
            r4.width = r8
        L_0x004c:
            r5.setLayoutParams(r4)
            return
        L_0x0050:
            r6 = 4611280694460924559(0x3ffe8f5c28f5c28f, double:1.91)
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0077
            com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView r0 = r11.A02
            X.01d r0 = r0.A07
            android.view.WindowManager r0 = r0.A0O()
            android.graphics.Point r1 = new android.graphics.Point
            r1.<init>()
            android.view.Display r0 = r0.getDefaultDisplay()
            r0.getSize(r1)
            int r0 = r1.x
            r4.width = r0
            double r1 = (double) r0
            double r1 = r1 / r6
            int r0 = (int) r1
            r4.height = r0
            goto L_0x004c
        L_0x0077:
            if (r10 == 0) goto L_0x007c
            r4.height = r8
            goto L_0x004a
        L_0x007c:
            r0 = -2
            if (r9 == 0) goto L_0x0080
            r0 = -1
        L_0x0080:
            r4.height = r0
            if (r9 == 0) goto L_0x004a
            r8 = -2
            goto L_0x004a
        L_0x0086:
            r0 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r2 = (double) r12
            double r2 = r2 * r0
            double r0 = (double) r13
            double r2 = r2 / r0
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C55142hr.A08(int, int, boolean):void");
    }
}
