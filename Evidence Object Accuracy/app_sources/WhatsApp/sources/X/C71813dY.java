package X;

import android.net.SSLSessionCache;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: X.3dY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71813dY extends SSLSocketFactory {
    public final /* synthetic */ SSLSessionCache A00;
    public final /* synthetic */ AnonymousClass1NW A01;
    public final /* synthetic */ SSLContext A02;
    public final /* synthetic */ SSLSocketFactory A03;

    public C71813dY(SSLSessionCache sSLSessionCache, AnonymousClass1NW r2, SSLContext sSLContext, SSLSocketFactory sSLSocketFactory) {
        this.A01 = r2;
        this.A03 = sSLSocketFactory;
        this.A02 = sSLContext;
        this.A00 = sSLSessionCache;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(String str, int i) {
        Socket createSocket = this.A03.createSocket(str, i);
        AnonymousClass1NW r2 = this.A01;
        r2.A01(createSocket);
        AnonymousClass3I7.A00(this.A00, str, this.A02, i);
        r2.A00.incrementAndGet();
        return createSocket;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        Socket createSocket = this.A03.createSocket(str, i, inetAddress, i2);
        AnonymousClass1NW r2 = this.A01;
        r2.A01(createSocket);
        AnonymousClass3I7.A00(this.A00, str, this.A02, i);
        r2.A00.incrementAndGet();
        return createSocket;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(InetAddress inetAddress, int i) {
        Socket createSocket = this.A03.createSocket(inetAddress, i);
        AnonymousClass1NW r3 = this.A01;
        r3.A01(createSocket);
        AnonymousClass3I7.A00(this.A00, inetAddress.getHostName(), this.A02, i);
        r3.A00.incrementAndGet();
        return createSocket;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        Socket createSocket = this.A03.createSocket(inetAddress, i, inetAddress2, i2);
        AnonymousClass1NW r3 = this.A01;
        r3.A01(createSocket);
        AnonymousClass3I7.A00(this.A00, inetAddress2.getHostName(), this.A02, i);
        r3.A00.incrementAndGet();
        return createSocket;
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        Socket createSocket = this.A03.createSocket(socket, str, i, z);
        AnonymousClass1NW r2 = this.A01;
        r2.A01(createSocket);
        AnonymousClass3I7.A00(this.A00, str, this.A02, i);
        r2.A00.incrementAndGet();
        return createSocket;
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public String[] getDefaultCipherSuites() {
        return this.A03.getDefaultCipherSuites();
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public String[] getSupportedCipherSuites() {
        return this.A03.getSupportedCipherSuites();
    }
}
