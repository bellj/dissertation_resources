package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1YB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YB {
    public Map A00 = new HashMap();

    public C28601Of A00() {
        Map map = this.A00;
        AnonymousClass009.A05(map);
        C28601Of r0 = new C28601Of(map);
        this.A00 = null;
        return r0;
    }

    public void A01(Object obj, Object obj2) {
        Map map = this.A00;
        AnonymousClass009.A05(map);
        map.put(obj, obj2);
    }
}
