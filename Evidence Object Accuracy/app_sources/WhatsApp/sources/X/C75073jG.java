package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.stickers.StickerView;

/* renamed from: X.3jG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75073jG extends AbstractC05270Ox {
    public final /* synthetic */ C54472gm A00;

    public C75073jG(C54472gm r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        if (i == 0 && i2 == 0) {
            C54472gm r1 = this.A00;
            StickerView stickerView = r1.A0B;
            if (stickerView != null && stickerView.getVisibility() == 0) {
                r1.A0F();
                return;
            }
            return;
        }
        this.A00.A0E();
    }
}
