package X;

/* renamed from: X.5KE  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KE extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1VC $future;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KE(AnonymousClass1VC r2) {
        super(1);
        this.$future = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        this.$future.A00(new Exception((Throwable) obj));
        return AnonymousClass1WZ.A00;
    }
}
