package X;

import android.content.Context;

/* renamed from: X.5gC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120445gC extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C17220qS A02;
    public final C1329668y A03;
    public final AbstractC136436Mn A04;
    public final C18650sn A05;
    public final AnonymousClass6BE A06;
    public final C18590sh A07;
    public final AbstractC14440lR A08;
    public final String A09;
    public final C126615tA A0A;

    public C120445gC(Context context, C14900mE r2, C17220qS r3, C1329668y r4, AbstractC136436Mn r5, C18650sn r6, C64513Fv r7, C18610sj r8, AnonymousClass6BE r9, C18590sh r10, AbstractC14440lR r11, String str, C126615tA r13) {
        super(r7, r8);
        this.A00 = context;
        this.A01 = r2;
        this.A09 = str;
        this.A0A = r13;
        this.A08 = r11;
        this.A02 = r3;
        this.A07 = r10;
        this.A06 = r9;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
    }
}
