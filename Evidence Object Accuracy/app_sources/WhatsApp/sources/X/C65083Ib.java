package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

/* renamed from: X.3Ib  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65083Ib {
    public static final Uri A05 = new Uri.Builder().scheme("content").authority("com.google.android.gms.chimera").build();
    public final int A00;
    public final ComponentName A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public C65083Ib(ComponentName componentName) {
        this.A02 = null;
        this.A03 = null;
        C13020j0.A01(componentName);
        this.A01 = componentName;
        this.A00 = 4225;
        this.A04 = false;
    }

    public C65083Ib(String str, String str2, int i, boolean z) {
        C13020j0.A05(str);
        this.A02 = str;
        C13020j0.A05(str2);
        this.A03 = str2;
        this.A01 = null;
        this.A00 = i;
        this.A04 = z;
    }

    public final Intent A00(Context context) {
        Bundle bundle;
        String str = this.A02;
        if (str == null) {
            return C12970iu.A0A().setComponent(this.A01);
        }
        Intent intent = null;
        if (this.A04) {
            Bundle A0D = C12970iu.A0D();
            A0D.putString("serviceActionBundleKey", str);
            try {
                bundle = context.getContentResolver().call(A05, "serviceIntentCall", (String) null, A0D);
            } catch (IllegalArgumentException e) {
                Log.w("ConnectionStatusConfig", "Dynamic intent resolution failed: ".concat(e.toString()));
                bundle = null;
            }
            if (bundle != null) {
                intent = (Intent) bundle.getParcelable("serviceResponseIntentKey");
            }
            if (intent == null) {
                Log.w("ConnectionStatusConfig", C12960it.A0c(String.valueOf(str), "Dynamic lookup for intent failed for action: "));
            }
        }
        if (intent == null) {
            return C12990iw.A0E(str).setPackage(this.A03);
        }
        return intent;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C65083Ib) {
                C65083Ib r5 = (C65083Ib) obj;
                if (!C13300jT.A00(this.A02, r5.A02) || !C13300jT.A00(this.A03, r5.A03) || !C13300jT.A00(this.A01, r5.A01) || this.A00 != r5.A00 || this.A04 != r5.A04) {
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        Object[] objArr = new Object[5];
        objArr[0] = this.A02;
        objArr[1] = this.A03;
        objArr[2] = this.A01;
        objArr[3] = Integer.valueOf(this.A00);
        return C12980iv.A0B(Boolean.valueOf(this.A04), objArr, 4);
    }

    public final String toString() {
        String str = this.A02;
        if (str != null) {
            return str;
        }
        ComponentName componentName = this.A01;
        C13020j0.A01(componentName);
        return componentName.flattenToString();
    }
}
