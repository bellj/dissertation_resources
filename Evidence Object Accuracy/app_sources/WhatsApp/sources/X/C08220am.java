package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.0am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08220am implements AbstractC12040hH {
    public final String A00;
    public final List A01;
    public final boolean A02;

    public C08220am(String str, List list, boolean z) {
        this.A00 = str;
        this.A01 = list;
        this.A02 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C08020aS(r2, this, r3);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ShapeGroup{name='");
        sb.append(this.A00);
        sb.append("' Shapes: ");
        sb.append(Arrays.toString(this.A01.toArray()));
        sb.append('}');
        return sb.toString();
    }
}
