package X;

import java.util.Arrays;

/* renamed from: X.4X2  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4X2 {
    public int A00;
    public byte[] A01;

    public AnonymousClass4X2(byte[] bArr, int i) {
        this.A01 = AnonymousClass1TT.A02(bArr);
        this.A00 = i;
    }

    public int hashCode() {
        return this.A00 ^ AnonymousClass1TT.A00(this.A01);
    }

    public boolean equals(Object obj) {
        if (obj instanceof AnonymousClass4X2) {
            AnonymousClass4X2 r4 = (AnonymousClass4X2) obj;
            if (r4.A00 == this.A00) {
                return Arrays.equals(this.A01, r4.A01);
            }
        }
        return false;
    }
}
