package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;

/* renamed from: X.1Ly  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Ly {
    public CharSequence A00 = "";
    public Runnable A01;
    public final C14900mE A02;
    public final AnonymousClass1BQ A03;

    public AnonymousClass1Ly(C14900mE r2, AnonymousClass1BQ r3) {
        this.A02 = r2;
        this.A03 = r3;
    }

    public void A00(CharSequence charSequence, int i) {
        if (this.A00.length() <= 20 || charSequence.length() <= 20) {
            Runnable runnable = this.A01;
            if (runnable != null) {
                this.A02.A0G(runnable);
            }
            this.A00 = charSequence;
            RunnableBRunnable0Shape8S0200000_I0_8 runnableBRunnable0Shape8S0200000_I0_8 = new RunnableBRunnable0Shape8S0200000_I0_8(this, 12, charSequence);
            this.A01 = runnableBRunnable0Shape8S0200000_I0_8;
            this.A02.A0J(runnableBRunnable0Shape8S0200000_I0_8, (long) i);
        }
    }
}
