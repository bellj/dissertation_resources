package X;

/* renamed from: X.5Ku  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114255Ku extends AbstractC114195Ko {
    public static final C114255Ku A01 = new C114255Ku();
    public AnonymousClass5E8 A00 = new AnonymousClass5E8(C88864Hl.A01, C88864Hl.A03, C88864Hl.A02);

    @Override // X.AbstractC10990fX, java.lang.Object
    public String toString() {
        return "Dispatchers.Default";
    }

    @Override // X.AbstractC10990fX
    public void A04(Runnable runnable, AnonymousClass5X4 r4) {
        this.A00.A02(runnable, C88864Hl.A06);
    }
}
