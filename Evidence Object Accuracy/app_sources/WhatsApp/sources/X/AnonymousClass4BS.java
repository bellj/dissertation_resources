package X;

/* renamed from: X.4BS  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BS {
    A03(0),
    A01(1),
    A02(2);
    
    public final int value;

    AnonymousClass4BS(int i) {
        this.value = i;
    }

    public static AnonymousClass4BS A00(int i) {
        if (i == 0) {
            return A03;
        }
        if (i == 1) {
            return A01;
        }
        if (i != 2) {
            return null;
        }
        return A02;
    }
}
