package X;

import android.content.res.Resources;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.util.Locale;

/* renamed from: X.3J4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3J4 {
    public static final String[] A00 = {"ET", "NE", "NG"};
    public static final String[] A01 = {"IN", "MA", "MY", "MX"};

    public static String A00(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        StringBuilder A0h = C12960it.A0h();
        A0h.append(str.substring(0, 1).toUpperCase(Locale.US));
        return C12960it.A0d(str.substring(1), A0h);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00be  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A01(X.C15570nT r4, X.AnonymousClass01d r5, X.AnonymousClass018 r6) {
        /*
            java.util.ArrayList r3 = X.C12960it.A0l()
            java.util.Locale r0 = java.util.Locale.getDefault()
            java.lang.String r2 = X.AbstractC27281Gs.A01(r0)
            java.lang.String r1 = X.AbstractC27291Gt.A05(r0)
            X.4Nh r0 = new X.4Nh
            r0.<init>(r2, r1)
            r3.add(r0)
            android.content.res.Resources r0 = android.content.res.Resources.getSystem()
            android.content.res.Configuration r0 = r0.getConfiguration()
            java.util.Locale r1 = r0.locale
            boolean r0 = A02()
            if (r0 != 0) goto L_0x0038
            java.lang.String r2 = X.AbstractC27281Gs.A01(r1)
            java.lang.String r1 = X.AbstractC27291Gt.A05(r1)
            X.4Nh r0 = new X.4Nh
            r0.<init>(r2, r1)
            r3.add(r0)
        L_0x0038:
            r4.A08()
            com.whatsapp.Me r0 = r4.A00
            if (r0 == 0) goto L_0x0085
            java.lang.String r5 = r0.cc
            java.lang.String r2 = r0.number
            java.util.Locale r1 = r6.A05
            java.util.Locale r0 = r6.A04
            X.3H4 r4 = new X.3H4
            r4.<init>(r5, r2, r1, r0)
        L_0x004c:
            int r0 = r4.A01
            if (r0 <= 0) goto L_0x00ad
            r5 = 0
        L_0x0051:
            int r0 = r4.A01
            if (r5 >= r0) goto L_0x00b2
            java.lang.String[] r0 = r4.A04
            r2 = r0[r5]
            java.util.Iterator r1 = r3.iterator()
        L_0x005d:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0074
            java.lang.Object r0 = r1.next()
            X.4Nh r0 = (X.C90284Nh) r0
            java.lang.String r0 = r0.A00
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x005d
        L_0x0071:
            int r5 = r5 + 1
            goto L_0x0051
        L_0x0074:
            java.lang.String[] r0 = r4.A04
            r2 = r0[r5]
            java.lang.String[] r0 = r4.A05
            r1 = r0[r5]
            X.4Nh r0 = new X.4Nh
            r0.<init>(r2, r1)
            r3.add(r0)
            goto L_0x0071
        L_0x0085:
            android.telephony.TelephonyManager r0 = r5.A0N()
            if (r0 == 0) goto L_0x00ad
            java.lang.String r1 = r0.getSimCountryIso()
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x00ad
            java.lang.String r2 = r1.toUpperCase()
            android.content.res.Resources r0 = android.content.res.Resources.getSystem()
            android.content.res.Configuration r0 = r0.getConfiguration()
            java.util.Locale r1 = r0.locale
            java.util.Locale r0 = java.util.Locale.getDefault()
            X.3H4 r4 = new X.3H4
            r4.<init>(r2, r1, r0)
            goto L_0x004c
        L_0x00ad:
            java.lang.String r0 = "LanguageSelectorUtils/error getting locale data"
            com.whatsapp.util.Log.e(r0)
        L_0x00b2:
            java.util.Set r0 = X.AbstractC41281tH.A05
            java.util.Iterator r5 = r0.iterator()
        L_0x00b8:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x00ec
            java.lang.String r4 = X.C12970iu.A0x(r5)
            java.util.Locale r0 = X.AbstractC27291Gt.A09(r4)
            java.lang.String r2 = X.AbstractC27281Gs.A01(r0)
            java.util.Iterator r1 = r3.iterator()
        L_0x00ce:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00e3
            java.lang.Object r0 = r1.next()
            X.4Nh r0 = (X.C90284Nh) r0
            java.lang.String r0 = r0.A00
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00ce
            goto L_0x00b8
        L_0x00e3:
            X.4Nh r0 = new X.4Nh
            r0.<init>(r2, r4)
            r3.add(r0)
            goto L_0x00b8
        L_0x00ec:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3J4.A01(X.0nT, X.01d, X.018):java.util.List");
    }

    public static boolean A02() {
        return Locale.getDefault().getLanguage().equals(Resources.getSystem().getConfiguration().locale.getLanguage());
    }

    public static boolean A03(AnonymousClass01d r5, C22040yO r6) {
        int i;
        TelephonyManager A0N = r5.A0N();
        if (A0N != null) {
            String simCountryIso = A0N.getSimCountryIso();
            if (!TextUtils.isEmpty(simCountryIso)) {
                String upperCase = simCountryIso.toUpperCase();
                if (C37871n9.A01(upperCase, A00)) {
                    return true;
                }
                if (C37871n9.A01(upperCase, A01)) {
                    i = 1778;
                } else if (upperCase.equals("AZ")) {
                    i = 1779;
                }
                return r6.A01(i);
            }
        }
        return false;
    }
}
