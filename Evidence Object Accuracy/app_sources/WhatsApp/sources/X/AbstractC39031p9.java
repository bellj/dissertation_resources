package X;

import android.graphics.Bitmap;

/* renamed from: X.1p9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC39031p9 {
    void dispose();

    int getHeight();

    int getWidth();

    int getXOffset();

    int getYOffset();

    void renderFrame(int i, int i2, Bitmap bitmap);
}
