package X;

import android.content.SharedPreferences;
import android.util.Pair;
import android.util.SparseArray;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0yO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22040yO {
    public SharedPreferences A00;
    public boolean A01;
    public final C14850m9 A02;
    public final C21680xo A03;
    public final C22030yN A04;
    public final C22020yM A05;
    public final C16630pM A06;

    public C22040yO(C14850m9 r1, C21680xo r2, C22030yN r3, C22020yM r4, C16630pM r5) {
        this.A02 = r1;
        this.A05 = r4;
        this.A03 = r2;
        this.A04 = r3;
        this.A06 = r5;
    }

    public final synchronized void A00() {
        if (!this.A01) {
            C22030yN r8 = this.A04;
            C22020yM r6 = this.A05;
            List<AnonymousClass217> list = r8.A00.A00;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (AnonymousClass217 r9 : list) {
                AnonymousClass21A r0 = r9.A00;
                if (r0 == null || r0.AJL(r6)) {
                    Map map = r6.A00;
                    if (map.containsKey("device_id")) {
                        MessageDigest messageDigest = r8.A01;
                        messageDigest.update(r9.A01.getBytes());
                        int intValue = new BigInteger(1, messageDigest.digest(((String) map.get("device_id")).getBytes())).mod(new BigInteger(Integer.toString(SearchActionVerificationClientService.NOTIFICATION_ID))).intValue();
                        Iterator it = r9.A02.iterator();
                        int i = 0;
                        while (true) {
                            if (it.hasNext()) {
                                AnonymousClass218 r3 = (AnonymousClass218) it.next();
                                for (AnonymousClass21C r1 : r3.A04) {
                                    i += r1.A00;
                                    if (intValue < i) {
                                        Pair create = Pair.create(r3, r1);
                                        if (create != null) {
                                            AnonymousClass218 r4 = (AnonymousClass218) create.first;
                                            AnonymousClass21C r32 = (AnonymousClass21C) create.second;
                                            AnonymousClass21A r02 = r4.A02;
                                            if (r02 == null || r02.AJL(r6)) {
                                                long currentTimeMillis = System.currentTimeMillis() / 1000;
                                                if (currentTimeMillis >= r4.A01 && currentTimeMillis <= r4.A00) {
                                                    AnonymousClass21F r03 = new AnonymousClass21F(r32, r4, r9);
                                                    arrayList.add(r03);
                                                    arrayList2.addAll(r03.A00.A02);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("device_id");
                        sb.append(" has not been set on UserInfo");
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
            }
            AnonymousClass21G r62 = new AnonymousClass21G(arrayList, arrayList2);
            C21680xo r5 = this.A03;
            SparseArray sparseArray = r62.A00;
            synchronized (r5) {
                SharedPreferences.Editor edit = r5.A01.edit();
                edit.remove("ab_props:sys:config_hash");
                edit.remove("ab_props:sys:last_refresh_time");
                edit.remove("ab_props:sys:last_version");
                for (int i2 = 0; i2 < sparseArray.size(); i2++) {
                    r5.A04(edit, (String) sparseArray.valueAt(i2), sparseArray.keyAt(i2));
                }
                edit.apply();
                r5.A04.A09.clear();
            }
            HashSet hashSet = new HashSet();
            for (AnonymousClass21F r04 : r62.A01) {
                hashSet.add(r04.toString());
            }
            SharedPreferences sharedPreferences = this.A00;
            if (sharedPreferences == null) {
                sharedPreferences = this.A06.A01(AnonymousClass01V.A07);
                this.A00 = sharedPreferences;
            }
            SharedPreferences.Editor edit2 = sharedPreferences.edit();
            edit2.putStringSet("ab_offline_props:offline_exposure_strings", hashSet);
            edit2.apply();
            this.A01 = true;
        }
    }

    public synchronized boolean A01(int i) {
        A00();
        return this.A02.A07(i);
    }
}
