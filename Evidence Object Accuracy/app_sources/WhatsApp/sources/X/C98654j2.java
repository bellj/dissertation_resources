package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4j2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98654j2 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78613pC[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        C78623pD r3 = null;
        byte[] bArr = null;
        int[] iArr = null;
        String[] strArr = null;
        int[] iArr2 = null;
        byte[][] bArr2 = null;
        C78643pF[] r7 = null;
        boolean z = true;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    r3 = (C78623pD) C95664e9.A07(parcel, C78623pD.CREATOR, readInt);
                    break;
                case 3:
                    bArr = C95664e9.A0I(parcel, readInt);
                    break;
                case 4:
                    iArr = C95664e9.A0J(parcel, readInt);
                    break;
                case 5:
                    strArr = C95664e9.A0L(parcel, readInt);
                    break;
                case 6:
                    iArr2 = C95664e9.A0J(parcel, readInt);
                    break;
                case 7:
                    bArr2 = C95664e9.A0M(parcel, readInt);
                    break;
                case '\b':
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case '\t':
                    r7 = (C78643pF[]) C95664e9.A0K(parcel, C78643pF.CREATOR, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78613pC(r3, bArr, iArr, iArr2, r7, strArr, bArr2, z);
    }
}
