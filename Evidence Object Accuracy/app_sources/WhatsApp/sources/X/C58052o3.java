package X;

import android.view.ViewGroup;
import android.view.animation.Animation;

/* renamed from: X.2o3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58052o3 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C14680lr A00;

    public C58052o3(C14680lr r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        C14680lr r3 = this.A00;
        ViewGroup viewGroup = r3.A06;
        viewGroup.setVisibility(8);
        r3.A04.setVisibility(0);
        r3.A0C.setVisibility(0);
        r3.A0G.setProgress(0);
        viewGroup.clearAnimation();
    }
}
