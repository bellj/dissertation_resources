package X;

import com.whatsapp.util.Log;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1BH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BH {
    public int A00 = 3;
    public long A01 = 0;
    public long A02 = 0;
    public C39471px A03;
    public final AbstractC15710nm A04;
    public final C18790t3 A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final C17170qN A08;
    public final C14820m6 A09;
    public final AnonymousClass018 A0A;
    public final C18810t5 A0B;
    public final C18800t4 A0C;
    public final AbstractC14440lR A0D;
    public final HashMap A0E = new HashMap();

    public AnonymousClass1BH(AbstractC15710nm r3, C18790t3 r4, C14830m7 r5, C16590pI r6, C17170qN r7, C14820m6 r8, AnonymousClass018 r9, C18810t5 r10, C18800t4 r11, AbstractC14440lR r12) {
        this.A07 = r6;
        this.A06 = r5;
        this.A04 = r3;
        this.A0D = r12;
        this.A05 = r4;
        this.A0A = r9;
        this.A0C = r11;
        this.A0B = r10;
        this.A09 = r8;
        this.A08 = r7;
    }

    public static String A00(int i) {
        if (i == 0) {
            return "LOADING";
        }
        if (i == 1) {
            return "LOADING_FAILED";
        }
        if (i == 2) {
            return "MANIFEST_STALE";
        }
        if (i == 3) {
            return "NO_MANIFEST";
        }
        if (i == 4) {
            return "READ_NEEDED";
        }
        if (i == 5) {
            return "UP_TO_DATE";
        }
        StringBuilder sb = new StringBuilder("Unknown manifest state: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public final synchronized int A01() {
        return this.A00;
    }

    public final synchronized int A02(int i) {
        int i2 = this.A00;
        if (i2 == 0 && i == 0) {
            Log.e("ManifestManager/setState/State change ERROR - loading to loading!");
            i = this.A00;
        } else {
            A00(i2);
            A00(i);
            this.A00 = i;
        }
        return i;
    }

    public final synchronized long A03() {
        long j;
        j = this.A01;
        if (j == 0) {
            j = this.A09.A00.getLong("downloadable_manifest_last_fetched_time_millis", 0);
            this.A01 = j;
        }
        return j;
    }

    public final synchronized void A04(long j) {
        this.A01 = j;
        this.A09.A00.edit().putLong("downloadable_manifest_last_fetched_time_millis", j).apply();
    }

    public final synchronized void A05(C39481py r4, String str) {
        int i = this.A00;
        if (i == 0) {
            HashMap hashMap = this.A0E;
            ArrayList arrayList = (ArrayList) hashMap.get(str);
            if (arrayList == null || arrayList.isEmpty()) {
                hashMap.put(str, new ArrayList(Collections.singletonList(r4)));
            } else {
                arrayList.add(r4);
                hashMap.put(str, arrayList);
            }
        } else if (i == 5) {
            C39471px r0 = this.A03;
            AnonymousClass009.A05(r0);
            r4.A01((C39451pv) r0.A01.get(str));
        } else if (i == 1) {
            r4.A00();
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("ManifestManager/registerCallback/Invalid state encountered when trying to register category : ");
            sb.append(str);
            sb.append(" state : ");
            sb.append(A00(i));
            Log.e(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:99:0x025b, code lost:
        if (r8.equals("categories") == false) goto L_0x025d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x037a  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x037e  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x020f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(X.C39451pv r20, X.AnonymousClass1VN r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 1060
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BH.A06(X.1pv, X.1VN, boolean):void");
    }

    public final void A07(C39471px r7, boolean z) {
        ArrayList arrayList;
        AnonymousClass009.A00();
        if (z) {
            AnonymousClass009.A05(r7);
        }
        synchronized (this) {
            HashMap hashMap = this.A0E;
            arrayList = new ArrayList(hashMap.entrySet());
            hashMap.clear();
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            entry.getKey();
            Iterator it2 = ((AbstractCollection) entry.getValue()).iterator();
            while (it2.hasNext()) {
                C39481py r2 = (C39481py) it2.next();
                if (z) {
                    r2.A01((C39451pv) r7.A01.get(entry.getKey()));
                } else {
                    r2.A00();
                }
            }
        }
    }
}
