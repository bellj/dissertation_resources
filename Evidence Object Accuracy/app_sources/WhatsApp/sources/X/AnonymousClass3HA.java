package X;

import android.graphics.Paint;

/* renamed from: X.3HA  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3HA {
    public static final AnonymousClass3HA A03 = new C61262zk();
    public static final AnonymousClass3HA A04 = new AnonymousClass42R();
    public static final AnonymousClass3HA A05 = new AnonymousClass42S();
    public Paint A00 = C12990iw.A0F();
    public final AnonymousClass00O A01 = new AnonymousClass00O();
    public final Object A02 = C12970iu.A0l();

    public boolean A00(String str) {
        String str2;
        int i = 0;
        while (i < str.length()) {
            int codePointAt = str.codePointAt(i);
            if (codePointAt > 255) {
                if (codePointAt <= 65535) {
                    str2 = Character.toString((char) codePointAt);
                } else {
                    str2 = new String(Character.toChars(codePointAt));
                }
                if (!AnonymousClass0RB.A00(this.A00, str2)) {
                    return false;
                }
            }
            i += Character.charCount(codePointAt);
        }
        return true;
    }

    public final boolean A01(String str) {
        AnonymousClass00O r2;
        Boolean bool;
        Object obj = this.A02;
        synchronized (obj) {
            r2 = this.A01;
            bool = (Boolean) r2.get(str);
        }
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean A00 = A00(str);
        synchronized (obj) {
            r2.put(str, Boolean.valueOf(A00));
        }
        return A00;
    }
}
