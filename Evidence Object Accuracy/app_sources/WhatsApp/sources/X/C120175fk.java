package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.5fk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120175fk extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C129255xQ A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120175fk(Context context, C14900mE r2, AnonymousClass1FK r3, C18650sn r4, C129255xQ r5) {
        super(context, r2, r4);
        this.A01 = r5;
        this.A00 = r3;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        Log.i(C12960it.A0b("PAY: BrazilRemoveMerchantAccount onRequestError: ", r2));
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AV3(r2);
        }
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        Log.e(C12960it.A0b("PAY: BrazilRemoveMerchantAccount onResponseError=", r2));
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AVA(r2);
        }
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r4) {
        Log.i("Pay: BrazilRemoveMerchantAccount successfully removed merchant account");
        this.A01.A08.Ab2(new Runnable(this.A00, this) { // from class: X.6Hn
            public final /* synthetic */ AnonymousClass1FK A00;
            public final /* synthetic */ C120175fk A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C120175fk r0 = this.A01;
                AnonymousClass1FK r42 = this.A00;
                C129255xQ r5 = r0.A01;
                C17070qD r3 = r5.A07;
                r3.A03();
                C241414j r02 = r3.A09;
                ArrayList A0l = C12960it.A0l();
                Iterator it = r02.A09().iterator();
                while (it.hasNext()) {
                    A0l.add(C117305Zk.A0H(it).A0A);
                }
                if (!A0l.isEmpty()) {
                    Iterator it2 = A0l.iterator();
                    while (it2.hasNext()) {
                        it2.next();
                        r3.A03();
                        Log.w("PAY: removeMerchantPaymentMethod for nonSmbApp!");
                    }
                }
                r5.A06.A04();
                if (r42 != null) {
                    r42.AVB(new AnonymousClass46N());
                }
            }
        });
    }
}
