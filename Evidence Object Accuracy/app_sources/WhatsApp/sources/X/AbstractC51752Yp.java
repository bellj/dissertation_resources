package X;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.phoneid.PhoneIdRequestReceiver;
import com.whatsapp.util.Log;

/* renamed from: X.2Yp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC51752Yp extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        C27371Hb A00 = ((PhoneIdRequestReceiver) this).A00.A00();
        if ("com.facebook.GET_PHONE_ID".equals(intent.getAction())) {
            Bundle resultExtras = getResultExtras(true);
            PendingIntent pendingIntent = (PendingIntent) resultExtras.getParcelable("auth");
            if (pendingIntent == null) {
                Log.e(C12960it.A0b("phoneid-request-receiver/on-receive invalid auth intent; data=", resultExtras));
            } else if (C45051zz.A02(pendingIntent, context)) {
                Bundle A0D = C12970iu.A0D();
                A0D.putLong("timestamp", A00.A00);
                setResult(-1, A00.A01, A0D);
            }
        }
    }
}
