package X;

import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.R;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.math.BigDecimal;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* renamed from: X.6Bq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133666Bq implements AbstractC16960q2 {
    public final C16590pI A00;
    public final AnonymousClass018 A01;
    public final C18600si A02;
    public final C17900ra A03;
    public final C17070qD A04;

    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124665pu.class;
    }

    public C133666Bq(C16590pI r1, AnonymousClass018 r2, C18600si r3, C17900ra r4, C17070qD r5) {
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
    }

    @Override // X.AbstractC16960q2
    public /* bridge */ /* synthetic */ Object Aam(Enum r7, Object obj, Map map) {
        boolean z;
        int i;
        C17930rd A01;
        AbstractC28901Pl r8 = (AbstractC28901Pl) obj;
        EnumC124665pu r72 = (EnumC124665pu) r7;
        boolean A0N = C16700pc.A0N(r8, r72);
        boolean z2 = true;
        switch (C117315Zl.A01(r72, C125255qx.A00)) {
            case 1:
                return r8.A0A;
            case 2:
                String str = r8.A07.A03;
                if (!"UNSET".equals(str)) {
                    return str;
                }
                C17930rd A012 = this.A03.A01();
                if (A012 != null) {
                    return A012.A03;
                }
                return null;
            case 3:
                return C1311161i.A02(this.A00.A00, this.A01, r8, this.A04, false);
            case 4:
                return r8.A0B;
            case 5:
                int intValue = Integer.valueOf(r8.A04()).intValue();
                if (intValue == A0N) {
                    return "DEBIT";
                }
                if (intValue == 2) {
                    return "BANK";
                }
                if (intValue == 3) {
                    return "WALLET";
                }
                if (intValue == 4) {
                    return "CREDIT";
                }
                if (intValue == 5) {
                    return "MERCHANT";
                }
                if (intValue == 6) {
                    return "COMBO";
                }
                if (intValue == 7) {
                    return "CARD_UNSET";
                }
                return intValue == 8 ? "PREPAID" : "UNKNOWN";
            case 6:
                return "UNKNOWN";
            case 7:
                return Long.valueOf(r8.A05);
            case 8:
                return Long.valueOf(r8.A06);
            case 9:
                i = r8.A01;
                break;
            case 10:
                i = r8.A00;
                break;
            case 11:
                i = r8.A03;
                break;
            case 12:
                i = r8.A02;
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                if (!(r8 instanceof C30841Za) || (A01 = this.A03.A01()) == null) {
                    return null;
                }
                AbstractC30791Yv r1 = A01.A02;
                C94074bD r3 = new C94074bD();
                r3.A03 = r1;
                BigDecimal bigDecimal = ((C30841Za) r8).A01.A00;
                int i2 = ((AbstractC30781Yu) r1).A01;
                r3.A02 = bigDecimal.scaleByPowerOfTen(i2).longValue();
                r3.A01 = new BigDecimal(A0N ? 1 : 0).scaleByPowerOfTen(i2).intValue();
                return r3.A00();
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                if (r8 instanceof C30841Za) {
                    return Long.valueOf(((C30841Za) r8).A00);
                }
                return null;
            case 15:
                byte[] bArr = r8.A0D;
                if (bArr != null) {
                    return Base64.encodeToString(bArr, 2);
                }
                if (r8 instanceof C30881Ze) {
                    return C37501mV.A07(BitmapFactory.decodeResource(C16590pI.A00(this.A00), R.drawable.av_card));
                }
                if (r8 instanceof C30861Zc) {
                    return C37501mV.A07(BitmapFactory.decodeResource(C16590pI.A00(this.A00), R.drawable.av_bank));
                }
                return null;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                C18600si r0 = this.A02;
                String str2 = r8.A0A;
                String[] split = r0.A05().split(";");
                int length = split.length;
                int i3 = 0;
                while (true) {
                    if (i3 < length) {
                        String str3 = split[i3];
                        if (TextUtils.isEmpty(str3) || !str3.equalsIgnoreCase(str2)) {
                            i3++;
                        } else {
                            z = true;
                        }
                    } else {
                        z = false;
                    }
                }
                return Boolean.valueOf(z);
            case 17:
                return r8.A08;
            default:
                throw new C113285Gx();
        }
        if (i != 2) {
            z2 = false;
        }
        return Boolean.valueOf(z2);
    }
}
