package X;

import android.view.View;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2id  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55492id extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C14260l7 A01;
    public final /* synthetic */ Object A02;

    public RunnableC55492id(View view, C14260l7 r2, Object obj) {
        this.A02 = obj;
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // java.lang.Runnable
    public void run() {
        String valueOf;
        AnonymousClass28D r1;
        View view;
        Object obj = this.A02;
        if (obj == null) {
            valueOf = null;
        } else if (obj instanceof String) {
            valueOf = (String) obj;
        } else {
            valueOf = String.valueOf(obj);
        }
        AnonymousClass4TT r0 = AnonymousClass3JV.A03(this.A01).A02;
        if (r0 == null) {
            r1 = null;
        } else {
            r1 = r0.A01;
        }
        AnonymousClass28D A00 = C87854Dg.A00(r1, new C1093351h(valueOf));
        if (A00 != null) {
            C89054Im r02 = A00.A03;
            if (r02 == null || (view = r02.A00) == null) {
                C28691Op.A00("AccessibilityUtils", C12960it.A0d(valueOf, C12960it.A0k("No View found for component with id: ")));
                return;
            }
            int id = view.getId();
            if (id == -1) {
                id = View.generateViewId();
                view.setId(id);
            }
            AnonymousClass028.A0b(this.A00, id);
            return;
        }
        Object[] A1b = C12970iu.A1b();
        A1b[0] = valueOf;
        throw C12960it.A0U(String.format("Component does not exist in the hierarchy for id: %s. Is the component with this ID not yet rendered? If so, this will not work.", A1b));
    }
}
