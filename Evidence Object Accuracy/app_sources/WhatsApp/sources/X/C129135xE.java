package X;

import java.util.ArrayList;

/* renamed from: X.5xE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129135xE {
    public final C18640sm A00;
    public final C18600si A01;
    public final C18610sj A02;
    public final AnonymousClass1BY A03;
    public final C22120yY A04;
    public final AbstractC14440lR A05;

    public C129135xE(C18640sm r1, C18600si r2, C18610sj r3, AnonymousClass1BY r4, C22120yY r5, AbstractC14440lR r6) {
        this.A05 = r6;
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A00 = r1;
    }

    public void A00(AnonymousClass6MM r12, C128545wH r13, String str) {
        if ("token".equals(r13.A00.A03)) {
            ArrayList A0l = C12960it.A0l();
            C12970iu.A1T("fbpay_pin", str, A0l);
            AbstractC14440lR r0 = this.A05;
            AnonymousClass1BY r7 = this.A03;
            C18600si r3 = this.A01;
            C22120yY r8 = this.A04;
            C12990iw.A1N(new C120305fx(this.A00, r3, this.A02, null, r12, r7, r8, A0l, 0), r0);
            return;
        }
        r12.AX5(str);
    }
}
