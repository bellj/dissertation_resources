package X;

import android.view.View;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.6CE  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CE implements AnonymousClass6MZ {
    public final /* synthetic */ PinBottomSheetDialogFragment A00;
    public final /* synthetic */ C129285xT A01;

    public AnonymousClass6CE(PinBottomSheetDialogFragment pinBottomSheetDialogFragment, C129285xT r2) {
        this.A01 = r2;
        this.A00 = pinBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass6MZ
    public void AOP(String str) {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
        pinBottomSheetDialogFragment.A1N();
        C129285xT r2 = this.A01;
        int A02 = r2.A05.A02();
        AnonymousClass605 r22 = r2.A06;
        if (A02 == 1) {
            AnonymousClass6CD r1 = new AnonymousClass6M0(pinBottomSheetDialogFragment, this) { // from class: X.6CD
                public final /* synthetic */ PinBottomSheetDialogFragment A00;
                public final /* synthetic */ AnonymousClass6CE A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass6M0
                public final void AVD(C452120p r4) {
                    AnonymousClass6CE r0 = this.A01;
                    PinBottomSheetDialogFragment pinBottomSheetDialogFragment2 = this.A00;
                    pinBottomSheetDialogFragment2.A1M();
                    if (r4 == null) {
                        ((AbstractView$OnClickListenerC121485iL) r0.A01.A08).A07.setChecked(false);
                        pinBottomSheetDialogFragment2.A1B();
                        return;
                    }
                    C117295Zj.A1C(r4, pinBottomSheetDialogFragment2, r0);
                }
            };
            r22.A01(new AbstractC136296Lz(r1, r22, str) { // from class: X.6C2
                public final /* synthetic */ AnonymousClass6M0 A00;
                public final /* synthetic */ AnonymousClass605 A01;
                public final /* synthetic */ String A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A00 = r1;
                }

                @Override // X.AbstractC136296Lz
                public final void AVF(C128545wH r6) {
                    AnonymousClass605 r4 = this.A01;
                    r4.A06.A00(new C1330569h(this.A00, r4, r6), r6, this.A02);
                }
            }, r1, "FB");
            return;
        }
        AnonymousClass6CC r12 = new AnonymousClass6M0(pinBottomSheetDialogFragment, this) { // from class: X.6CC
            public final /* synthetic */ PinBottomSheetDialogFragment A00;
            public final /* synthetic */ AnonymousClass6CE A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass6M0
            public final void AVD(C452120p r4) {
                AnonymousClass6CE r0 = this.A01;
                PinBottomSheetDialogFragment pinBottomSheetDialogFragment2 = this.A00;
                pinBottomSheetDialogFragment2.A1M();
                if (r4 == null) {
                    ((AbstractView$OnClickListenerC121485iL) r0.A01.A08).A07.setChecked(true);
                    pinBottomSheetDialogFragment2.A1B();
                    return;
                }
                C117295Zj.A1C(r4, pinBottomSheetDialogFragment2, r0);
            }
        };
        r22.A01(new AnonymousClass6C1(r12, r22, str), r12, "FB");
    }

    @Override // X.AnonymousClass6MZ
    public void AQi(View view) {
        C129285xT r1 = this.A01;
        C125605rW r3 = new C125605rW(r1.A01);
        C12960it.A1E(new C124065oO(r3, r1.A04), r1.A09);
    }
}
