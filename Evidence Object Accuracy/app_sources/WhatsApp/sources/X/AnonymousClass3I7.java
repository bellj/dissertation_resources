package X;

import android.net.SSLSessionCache;
import com.whatsapp.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;

/* renamed from: X.3I7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3I7 {
    public static boolean A00;

    public static void A00(SSLSessionCache sSLSessionCache, String str, SSLContext sSLContext, int i) {
        Field field;
        Field field2;
        if (!A00 && sSLContext != null && sSLSessionCache != null) {
            try {
                Field declaredField = sSLSessionCache.getClass().getDeclaredField("mSessionCache");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(sSLSessionCache);
                Class<?> cls = obj.getClass();
                Class<?> cls2 = Integer.TYPE;
                Method method = cls.getMethod("getSessionData", String.class, cls2);
                Integer valueOf = Integer.valueOf(i);
                byte[] bArr = (byte[]) method.invoke(obj, str, valueOf);
                if (bArr != null) {
                    Method declaredMethod = sSLContext.getClientSessionContext().getClass().getSuperclass().getDeclaredMethod("toSession", byte[].class, String.class, cls2);
                    declaredMethod.setAccessible(true);
                    SSLSession sSLSession = (SSLSession) declaredMethod.invoke(sSLContext.getClientSessionContext(), bArr, str, valueOf);
                    if (sSLSession != null) {
                        A02(sSLSession);
                    }
                }
            } catch (Exception e) {
                Log.i(C12960it.A0d(e.toString(), C12960it.A0k("SSLSessionCacheHack:")));
                A00 = true;
            }
            try {
                sSLSessionCache.getClass().getDeclaredField("mSessionCache").setAccessible(true);
                Method method2 = sSLContext.getClientSessionContext().getClass().getMethod("getSession", String.class, Integer.TYPE);
                SSLSessionContext clientSessionContext = sSLContext.getClientSessionContext();
                Integer valueOf2 = Integer.valueOf(i);
                SSLSession sSLSession2 = (SSLSession) method2.invoke(clientSessionContext, str, valueOf2);
                sSLContext.getClientSessionContext().getSessionTimeout();
                if (sSLSession2 != null) {
                    A02(sSLSession2);
                }
                try {
                    field = sSLContext.getClass().getDeclaredField("contextSpi");
                } catch (NoSuchFieldException unused) {
                    field = sSLContext.getClass().getDeclaredField("spiImpl");
                }
                if (field != null) {
                    field.setAccessible(true);
                    Object obj2 = field.get(sSLContext);
                    if (obj2 != null) {
                        try {
                            field2 = obj2.getClass().getSuperclass().getDeclaredField("sslParameters");
                        } catch (NoSuchFieldException unused2) {
                            field2 = obj2.getClass().getDeclaredField("sslParameters");
                        }
                        field2.setAccessible(true);
                        Object obj3 = field2.get(obj2);
                        try {
                            Method declaredMethod2 = obj3.getClass().getDeclaredMethod("getCachedClientSession", sSLContext.getClientSessionContext().getClass(), String.class, Integer.TYPE);
                            declaredMethod2.setAccessible(true);
                            declaredMethod2.invoke(obj3, sSLContext.getClientSessionContext(), str, valueOf2);
                            return;
                        } catch (NoSuchMethodException unused3) {
                        }
                    } else {
                        return;
                    }
                }
                A00 = true;
            } catch (Exception e2) {
                Log.w(C12960it.A0d(e2.toString(), C12960it.A0k("SSLSessionCacheHack:")));
                A00 = true;
            }
        }
    }

    public static void A01(SSLSessionCache sSLSessionCache, SSLContext sSLContext) {
        String A0d;
        if (sSLContext.getClientSessionContext() == null) {
            A0d = "SSLSessionCacheHack/session context is null";
        } else if (!C12980iv.A0r(sSLContext.getClientSessionContext()).equals("ClientSessionContext")) {
            A0d = C12960it.A0d(C12980iv.A0s(sSLContext.getClientSessionContext()), C12960it.A0k("SSLSessionCacheHack/session context does not match, class="));
        } else {
            try {
                Field declaredField = sSLSessionCache.getClass().getDeclaredField("mSessionCache");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(sSLSessionCache);
                sSLContext.getClientSessionContext().getClass().getMethod("setPersistentCache", Class.forName("com.android.org.conscrypt.SSLClientSessionCache")).invoke(sSLContext.getClientSessionContext(), obj);
                return;
            } catch (ClassNotFoundException e) {
                e.toString();
                return;
            } catch (Exception e2) {
                Log.e(C12960it.A0d(e2.toString(), C12960it.A0j("SSLSessionCacheHack:")));
                return;
            }
        }
        Log.w(A0d);
    }

    public static void A02(SSLSession sSLSession) {
        long j;
        try {
            j = (long) sSLSession.getPeerCertificates().length;
        } catch (SSLPeerUnverifiedException e) {
            Log.e(e);
            j = -1;
        }
        StringBuilder A0k = C12960it.A0k("age=");
        A0k.append(C12980iv.A0D(System.currentTimeMillis() - sSLSession.getCreationTime()));
        A0k.append(", creation=");
        A0k.append(sSLSession.getCreationTime());
        A0k.append(", protocol=");
        A0k.append(sSLSession.getProtocol());
        A0k.append(", valid=");
        A0k.append(sSLSession.isValid());
        A0k.append(", peerCertificateCount=");
        A0k.append(j);
        A0k.toString();
    }
}
