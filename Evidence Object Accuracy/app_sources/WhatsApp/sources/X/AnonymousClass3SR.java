package X;

/* renamed from: X.3SR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SR implements AbstractC72393eW {
    public final AbstractC72393eW A00;
    public final boolean A01;
    public final float[] A02;

    public /* synthetic */ AnonymousClass3SR(AbstractC72393eW r1, float[] fArr, boolean z) {
        this.A00 = r1;
        this.A02 = fArr;
        this.A01 = z;
    }

    @Override // X.AbstractC72393eW
    public AbstractC72393eW ABK(int i) {
        return this.A00.ABK(i);
    }

    @Override // X.AbstractC72393eW
    public int ABP() {
        return this.A00.ABP();
    }

    @Override // X.AbstractC72393eW
    public int ADK() {
        return this.A00.ADK();
    }

    @Override // X.AbstractC72393eW
    public Object ADo() {
        return this.A00.ADo();
    }

    @Override // X.AbstractC72393eW
    public int AEr() {
        return this.A00.AEr();
    }

    @Override // X.AbstractC72393eW
    public int AEs() {
        return this.A00.AEs();
    }

    @Override // X.AbstractC72393eW
    public int AEt() {
        return this.A00.AEt();
    }

    @Override // X.AbstractC72393eW
    public int AEu() {
        return this.A00.AEu();
    }

    @Override // X.AbstractC72393eW
    public AbstractC65073Ia AG8() {
        return this.A00.AG8();
    }

    @Override // X.AbstractC72393eW
    public int AHm() {
        return this.A00.AHm();
    }

    @Override // X.AbstractC72393eW
    public int AHs(int i) {
        return this.A00.AHs(i);
    }

    @Override // X.AbstractC72393eW
    public int AHt(int i) {
        return this.A00.AHt(i);
    }

    @Override // X.AbstractC72393eW
    public int getHeight() {
        int height = this.A00.getHeight();
        float[] fArr = this.A02;
        return height + Math.round(C12980iv.A01(fArr, 0)) + Math.round(C12980iv.A01(fArr, 3));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (java.lang.Float.isNaN(r1) == false) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0023, code lost:
        if (java.lang.Float.isNaN(r1) == false) goto L_0x0025;
     */
    @Override // X.AbstractC72393eW
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getWidth() {
        /*
            r5 = this;
            X.3eW r0 = r5.A00
            int r4 = r0.getWidth()
            float[] r3 = r5.A02
            boolean r2 = r5.A01
            if (r2 != 0) goto L_0x0031
            r0 = 5
            r1 = r3[r0]
            boolean r0 = java.lang.Float.isNaN(r1)
            if (r0 != 0) goto L_0x0031
        L_0x0015:
            int r0 = java.lang.Math.round(r1)
            int r4 = r4 + r0
            if (r2 == 0) goto L_0x002b
            r0 = 5
            r1 = r3[r0]
            boolean r0 = java.lang.Float.isNaN(r1)
            if (r0 != 0) goto L_0x002b
        L_0x0025:
            int r0 = java.lang.Math.round(r1)
            int r4 = r4 + r0
            return r4
        L_0x002b:
            r0 = 2
            float r1 = X.C12980iv.A01(r3, r0)
            goto L_0x0025
        L_0x0031:
            r0 = 1
            float r1 = X.C12980iv.A01(r3, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3SR.getWidth():int");
    }
}
