package X;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.1lg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC37201lg extends WaImageView {
    public boolean A00;

    public AbstractC37201lg(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public static Paint A00(View view) {
        Paint paint = new Paint();
        paint.setColor(AnonymousClass00T.A00(view.getContext(), R.color.secondary_text));
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setDither(true);
        return paint;
    }
}
