package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.orderdetails.PaymentCheckoutOrderDetailsViewV2;

/* renamed from: X.68C  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68C implements AnonymousClass1QA {
    public final /* synthetic */ IndiaUpiCheckOrderDetailsActivity A00;
    public final /* synthetic */ EnumC124545pi A01;
    public final /* synthetic */ C128335vw A02;

    @Override // X.AnonymousClass1QA
    public void AWZ() {
    }

    public AnonymousClass68C(IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity, EnumC124545pi r2, C128335vw r3) {
        this.A00 = indiaUpiCheckOrderDetailsActivity;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass1QA
    public void AWX() {
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A00;
        AbstractC005102i A1U = indiaUpiCheckOrderDetailsActivity.A1U();
        if (A1U != null) {
            int i = this.A02.A00;
            int i2 = R.string.order_summary_bar_text;
            if (i == 1) {
                i2 = R.string.order_details_action_bar_text;
            }
            A1U.A0I(indiaUpiCheckOrderDetailsActivity.getResources().getString(i2));
        }
        C128335vw r7 = this.A02;
        int i3 = 11;
        if (r7.A00 == 1) {
            i3 = 4;
        }
        indiaUpiCheckOrderDetailsActivity.A0E.A00(r7.A07, indiaUpiCheckOrderDetailsActivity.A0F, i3);
        C125945s4 r0 = new C125945s4(((ActivityC13810kN) indiaUpiCheckOrderDetailsActivity).A0C.A03(1767));
        Object obj = r0.A00.get(indiaUpiCheckOrderDetailsActivity.A0F);
        PaymentCheckoutOrderDetailsViewV2 paymentCheckoutOrderDetailsViewV2 = indiaUpiCheckOrderDetailsActivity.A07;
        C15570nT r5 = ((ActivityC13790kL) indiaUpiCheckOrderDetailsActivity).A01;
        int i4 = 1;
        if (obj == null) {
            i4 = 0;
        }
        paymentCheckoutOrderDetailsViewV2.A00(indiaUpiCheckOrderDetailsActivity, r5, this.A01, r7, indiaUpiCheckOrderDetailsActivity.A0F, i4);
    }
}
