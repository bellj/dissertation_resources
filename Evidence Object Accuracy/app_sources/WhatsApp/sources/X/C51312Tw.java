package X;

/* renamed from: X.2Tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51312Tw extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;

    public C51312Tw() {
        super(2880, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(28, this.A01);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamDirectoryBusinessOnboardingSmbClient {");
        String str = null;
        Integer num = this.A00;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryEntryPoint", str);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryOnboardingLoggingVersion", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySessionId", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
