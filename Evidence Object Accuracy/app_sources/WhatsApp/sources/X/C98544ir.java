package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashSet;

/* renamed from: X.4ir  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98544ir implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78933pm[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        HashSet A12 = C12970iu.A12();
        C78943pn r4 = null;
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            int i2 = 1;
            if (c != 1) {
                i2 = 2;
                if (c != 2) {
                    i2 = 3;
                    if (c != 3) {
                        i2 = 4;
                        if (c != 4) {
                            i2 = 5;
                            if (c != 5) {
                                C95664e9.A0D(parcel, readInt);
                            } else {
                                str3 = C95664e9.A08(parcel, readInt);
                            }
                        } else {
                            str2 = C95664e9.A08(parcel, readInt);
                        }
                    } else {
                        str = C95664e9.A08(parcel, readInt);
                    }
                } else {
                    r4 = (C78943pn) C95664e9.A07(parcel, C78943pn.CREATOR, readInt);
                }
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
            C12980iv.A1R(A12, i2);
        }
        if (parcel.dataPosition() == A01) {
            return new C78933pm(r4, str, str2, str3, A12, i);
        }
        throw new C113235Gs(parcel, C12960it.A0e("Overread allowed size end=", C12980iv.A0t(37), A01));
    }
}
