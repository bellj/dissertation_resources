package X;

import android.content.Context;
import com.whatsapp.R;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4BQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4BQ extends Enum {
    public static final /* synthetic */ AnonymousClass4BQ[] A00;
    public static final AnonymousClass4BQ A01;
    public static final AnonymousClass4BQ A02;
    public static final AnonymousClass4BQ A03;
    public static final AnonymousClass4BQ A04;
    public static final AnonymousClass4BQ A05;
    public final int dimension;
    public final int innerStrokeWidth;
    public final int strokeWidth;

    public static AnonymousClass4BQ valueOf(String str) {
        return (AnonymousClass4BQ) Enum.valueOf(AnonymousClass4BQ.class, str);
    }

    public static AnonymousClass4BQ[] values() {
        return (AnonymousClass4BQ[]) A00.clone();
    }

    static {
        AnonymousClass4BQ r2 = new AnonymousClass4BQ("EXTRA_SMALL", 0, R.dimen.wds_profile_status_extra_small, R.dimen.wds_profile_status_stroke_extra_small, R.dimen.wds_profile_status_inner_stroke_small);
        A02 = r2;
        AnonymousClass4BQ r3 = new AnonymousClass4BQ("SMALL", 1, R.dimen.wds_profile_status_small, R.dimen.wds_profile_status_stroke_small, R.dimen.wds_profile_status_inner_stroke_small);
        A05 = r3;
        AnonymousClass4BQ r4 = new AnonymousClass4BQ("MEDIUM", 2, R.dimen.wds_profile_status_medium, R.dimen.wds_profile_status_stroke_medium, R.dimen.wds_profile_status_inner_stroke_small);
        A04 = r4;
        AnonymousClass4BQ r5 = new AnonymousClass4BQ("LARGE", 3, R.dimen.wds_profile_status_large, R.dimen.wds_profile_status_stroke_large, R.dimen.wds_profile_status_inner_stroke_large);
        A03 = r5;
        AnonymousClass4BQ r8 = new AnonymousClass4BQ("EXTRA_LARGE", 4, R.dimen.wds_profile_status_extra_large, R.dimen.wds_profile_status_stroke_extra_large, R.dimen.wds_profile_status_inner_stroke_large);
        A01 = r8;
        AnonymousClass4BQ[] r1 = new AnonymousClass4BQ[5];
        C12970iu.A1U(r2, r3, r1);
        r1[2] = r4;
        r1[3] = r5;
        r1[4] = r8;
        A00 = r1;
    }

    public AnonymousClass4BQ(String str, int i, int i2, int i3, int i4) {
        this.dimension = i2;
        this.strokeWidth = i3;
        this.innerStrokeWidth = i4;
    }

    public final C92744Xg A00(Context context) {
        C16700pc.A0E(context, 0);
        float dimension = context.getResources().getDimension(this.dimension);
        return new C92744Xg(new C92714Xd(dimension, dimension), context.getResources().getDimension(this.strokeWidth), context.getResources().getDimension(this.innerStrokeWidth));
    }
}
