package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.backup.google.quota.BackupQuotaNotificationDismissedReceiver;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1D6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1D6 {
    public final AbstractC15710nm A00;
    public final C15810nw A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C18360sK A04;
    public final C14820m6 A05;
    public final AnonymousClass018 A06;
    public final C15660nh A07;
    public final C14850m9 A08;
    public final C16120oU A09;

    public AnonymousClass1D6(AbstractC15710nm r1, C15810nw r2, C14830m7 r3, C16590pI r4, C18360sK r5, C14820m6 r6, AnonymousClass018 r7, C15660nh r8, C14850m9 r9, C16120oU r10) {
        this.A02 = r3;
        this.A08 = r9;
        this.A00 = r1;
        this.A03 = r4;
        this.A09 = r10;
        this.A01 = r2;
        this.A06 = r7;
        this.A07 = r8;
        this.A05 = r6;
        this.A04 = r5;
    }

    public int A00() {
        SharedPreferences sharedPreferences;
        int i;
        C14850m9 r7 = this.A08;
        if (r7.A07(932)) {
            C14820m6 r8 = this.A05;
            if (r8.A09() != null && ((i = (sharedPreferences = r8.A00).getInt("gdrive_backup_quota_warning_visibility", 0)) == 1 || i == 2)) {
                if (sharedPreferences.getLong("backup_quota_media_cutoff_timestamp", -1) > -1) {
                    return 1;
                }
                String A09 = r8.A09();
                if (A09 == null || r8.A08(A09) <= ((long) r7.A02(1013)) * SearchActionVerificationClientService.MS_TO_NS) {
                    if (A06()) {
                        return 2;
                    }
                } else if (!A08(System.currentTimeMillis())) {
                    return 3;
                }
            }
        }
        return 0;
    }

    public long A01() {
        C14850m9 r5 = this.A08;
        if (!r5.A07(932) || this.A05.A00.getLong("backup_quota_user_notice_period_end_timestamp", -1) != -1) {
            return this.A05.A00.getLong("backup_quota_user_notice_period_end_timestamp", -1);
        }
        boolean A06 = A06();
        long currentTimeMillis = System.currentTimeMillis();
        if (A06) {
            return currentTimeMillis + TimeUnit.DAYS.toMillis((long) r5.A02(1241));
        }
        return currentTimeMillis;
    }

    public long A02(Set set, long j) {
        String str;
        C14850m9 r1 = this.A08;
        long A02 = ((long) r1.A02(1239)) * SearchActionVerificationClientService.MS_TO_NS;
        long A022 = ((long) r1.A02(1240)) * SearchActionVerificationClientService.MS_TO_NS;
        C15660nh r7 = this.A07;
        StringBuilder sb = new StringBuilder("mediamsgstore/getSizeOfSpecifiedTypesOfMediaFilesFromTimestamp until: ");
        sb.append(j);
        sb.append(" for message types ");
        sb.append(set);
        sb.append(" with maxDocumentSize");
        sb.append(A022);
        sb.append(" and maxMediaSize");
        sb.append(A02);
        Log.i(sb.toString());
        int size = set.size();
        StringBuilder sb2 = new StringBuilder("SELECT SUM (file_size)  FROM (");
        sb2.append("SELECT * FROM message_media AS message_media JOIN available_message_view AS message ON message_media.message_row_id = message._id WHERE ");
        sb2.append(" message.message_type IN ");
        sb2.append(AnonymousClass1Ux.A00(size));
        if (j > 0) {
            str = AnonymousClass2K9.A00("message");
        } else {
            str = "";
        }
        sb2.append(str);
        sb2.append(" AND ");
        sb2.append("(  CASE  WHEN  (message.message_type IN (26,9))");
        sb2.append(" THEN message_media.file_size <= ");
        sb2.append(A022);
        sb2.append(" ELSE message_media.");
        sb2.append("file_size");
        sb2.append(" <= ");
        sb2.append(A02);
        sb2.append(" END )");
        sb2.append(" GROUP BY message_media.");
        sb2.append("file_hash");
        sb2.append(")");
        String obj = sb2.toString();
        ArrayList arrayList = new ArrayList();
        for (Object obj2 : set) {
            arrayList.add(String.valueOf(obj2));
        }
        long j2 = 0;
        if (j > 0) {
            String valueOf = String.valueOf(j);
            arrayList.add(valueOf);
            arrayList.add(valueOf);
            arrayList.add(valueOf);
            arrayList.add(valueOf);
        }
        C16310on A01 = r7.A0C.get();
        try {
            Cursor A09 = A01.A03.A09(obj, (String[]) arrayList.toArray(C15660nh.A0H));
            if (A09.moveToNext()) {
                j2 = A09.getLong(0);
            }
            A09.close();
            A01.close();
            return j2;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public String A03() {
        String str = (String) C44891zj.A00(this.A06, ((long) this.A08.A02(1013)) * SearchActionVerificationClientService.MS_TO_NS, false, true).first;
        if (str != null) {
            return str.replace(' ', (char) 160);
        }
        return str;
    }

    public Set A04() {
        HashSet hashSet = new HashSet();
        hashSet.add((byte) 2);
        hashSet.add((byte) 9);
        hashSet.add((byte) 26);
        hashSet.add((byte) 20);
        hashSet.add((byte) 13);
        hashSet.add((byte) 29);
        hashSet.add((byte) 1);
        hashSet.add((byte) 25);
        if (this.A05.A00.getBoolean("gdrive_include_videos_in_backup", false)) {
            hashSet.add((byte) 3);
            hashSet.add((byte) 28);
        }
        return hashSet;
    }

    public void A05() {
        Log.i("gdrive/backup/quota/backup-quota-imposed-next-backup");
        Context context = this.A03.A00;
        PendingIntent A01 = AnonymousClass1UY.A01(context, 39, new Intent(context, BackupQuotaNotificationDismissedReceiver.class), 134217728);
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.backup.google.SettingsGoogleDrive");
        intent.putExtra("backup_quota_notification", true);
        PendingIntent A00 = AnonymousClass1UY.A00(context, 0, intent, 134217728);
        String string = context.getString(R.string.gdrive_backup_quota_reached_notification_title);
        String string2 = context.getString(R.string.gdrive_backup_quota_reached_notification_message, A03());
        C005602s A002 = C22630zO.A00(context);
        A002.A0J = "chat_history_backup@1";
        A002.A09 = A00;
        A002.A07.deleteIntent = A01;
        A002.A0D(true);
        A002.A0E(false);
        A002.A0A(string);
        A002.A09(string2);
        C18360sK.A01(A002, R.drawable.notifybar);
        if (Build.VERSION.SDK_INT >= 21) {
            A002.A06 = 1;
        }
        this.A04.A03(39, A002.A01());
        AnonymousClass2KB r1 = new AnonymousClass2KB();
        r1.A06 = 3;
        this.A09.A07(r1);
    }

    public boolean A06() {
        C14820m6 r1 = this.A05;
        String A09 = r1.A09();
        return A09 != null && r1.A08(A09) > ((long) this.A08.A02(1589)) * SearchActionVerificationClientService.MS_TO_NS;
    }

    public boolean A07() {
        if (!this.A08.A07(932)) {
            return false;
        }
        if (this.A05.A00.getLong("backup_quota_user_notice_period_end_timestamp", -1) > 0) {
            return A08(System.currentTimeMillis());
        }
        return !A06();
    }

    public boolean A08(long j) {
        return C38121nY.A00(j, this.A05.A00.getLong("backup_quota_user_notice_period_end_timestamp", -1)) >= 0;
    }
}
