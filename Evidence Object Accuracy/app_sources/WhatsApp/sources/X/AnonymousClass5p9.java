package X;

import android.app.Activity;
import com.whatsapp.wabloks.ext.WaBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1;

/* renamed from: X.5p9  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5p9 extends AnonymousClass67N {
    public C17120qI A00;

    public AnonymousClass5p9(AnonymousClass01H r27, AnonymousClass01H r28, AnonymousClass01H r29, AnonymousClass01H r30, AnonymousClass01H r31, AnonymousClass01H r32, AnonymousClass01H r33, AnonymousClass01H r34, AnonymousClass01H r35, AnonymousClass01H r36, AnonymousClass01H r37, AnonymousClass01H r38, AnonymousClass01H r39, AnonymousClass01H r40, AnonymousClass01H r41, AnonymousClass01H r42, AnonymousClass01H r43, AnonymousClass01H r44, AnonymousClass01H r45, AnonymousClass01H r46, AnonymousClass01H r47, AnonymousClass01H r48, AnonymousClass01H r49, AnonymousClass01H r50) {
        super(r27, r28, r29, r37, r38, r30, r35, r33, r41, r44, r45, r32, r42, r47, r39, r36, r34, r46, r31, r40, r48, r43, r49, r50);
        this.A00 = (C17120qI) r40.get();
    }

    @Override // X.AnonymousClass1AH
    public void AKO(Activity activity, String str, String str2) {
        AnonymousClass01F A0V = ((ActivityC000900k) activity).A0V();
        WaBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1 waBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1 = new WaBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1(this, str, str2);
        waBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1.A1B();
        waBkGlobalInterpreterExtImpl$WaBkWaBloksActivityGlobalInterpreterExt$1.A1F(A0V, "bloks-dialog");
    }

    @Override // X.AnonymousClass1AH
    public void AcL(Activity activity, AbstractC115815Ta r5) {
        if (activity instanceof AbstractC50222Op) {
            ((AbstractC50222Op) activity).AfS(r5);
        } else {
            this.A00.A01(activity).A01(new AnonymousClass6E9(r5.AAL().A0I(36)));
        }
    }
}
