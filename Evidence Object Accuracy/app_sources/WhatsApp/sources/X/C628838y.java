package X;

import android.os.SystemClock;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* renamed from: X.38y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C628838y extends AbstractC16350or {
    public final int A00;
    public final long A01;
    public final long A02;
    public final AbstractC15710nm A03;
    public final C14330lG A04;
    public final C14900mE A05;
    public final C239613r A06;
    public final C16170oZ A07;
    public final C16590pI A08;
    public final AnonymousClass018 A09;
    public final C32731ce A0A;
    public final AnonymousClass1BI A0B;
    public final AnonymousClass19M A0C;
    public final C14850m9 A0D;
    public final C16120oU A0E;
    public final C453421e A0F;
    public final C14410lO A0G;
    public final C26471Dp A0H;
    public final AnonymousClass1BX A0I;
    public final AbstractC15340mz A0J;
    public final AnonymousClass1AB A0K;
    public final C22190yg A0L;
    public final Collection A0M;
    public final HashSet A0N;
    public final List A0O;
    public final List A0P;
    public final Map A0Q;
    public final boolean A0R;
    public final boolean A0S;
    public final boolean A0T;
    public final boolean A0U;
    public final boolean A0V;
    public final boolean A0W;
    public final boolean A0X;

    public C628838y(AbstractC15710nm r3, C14330lG r4, C14900mE r5, C239613r r6, C16170oZ r7, C16590pI r8, AnonymousClass018 r9, C32731ce r10, AnonymousClass1BI r11, AnonymousClass19M r12, C14850m9 r13, C16120oU r14, C453421e r15, C14410lO r16, C26471Dp r17, AnonymousClass1BX r18, AbstractC453221c r19, AbstractC15340mz r20, AnonymousClass1AB r21, C22190yg r22, Collection collection, HashSet hashSet, List list, List list2, Map map, int i, long j, long j2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        super(r19);
        this.A08 = r8;
        this.A05 = r5;
        this.A03 = r3;
        this.A06 = r6;
        this.A0I = r18;
        this.A04 = r4;
        this.A0E = r14;
        this.A0C = r12;
        this.A07 = r7;
        this.A0H = r17;
        this.A0G = r16;
        this.A0B = r11;
        this.A0L = r22;
        this.A09 = r9;
        this.A0K = r21;
        this.A0P = list;
        this.A0O = list2;
        this.A0M = collection;
        this.A0F = r15;
        this.A0J = r20;
        this.A02 = j;
        this.A0R = z;
        this.A00 = i;
        this.A0S = z2;
        this.A0Q = map;
        this.A0N = hashSet;
        this.A0X = z3;
        this.A0W = z4;
        this.A0A = r10;
        this.A0U = z5;
        this.A0T = z6;
        this.A0V = z7;
        this.A01 = j2;
        this.A0D = r13;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r13v2, resolved type: java.util.List */
    /* JADX DEBUG: Multi-variable search result rejected for r13v8, resolved type: java.util.List */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v5 */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x0568, code lost:
        if (r3 == false) goto L_0x056a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x0591, code lost:
        if (r9 == null) goto L_0x0593;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r39) {
        /*
        // Method dump skipped, instructions count: 1594
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C628838y.A05(java.lang.Object[]):java.lang.Object");
    }

    public final int A08(int i) {
        if (this.A0D.A07(1516)) {
            if (i == 1) {
                return 2;
            }
            if (i == 2) {
                return 4;
            }
            if (i == 3) {
                return 3;
            }
            if (i == 5) {
                return 12;
            }
            if (i == 7) {
                return 1;
            }
            if (i == 30) {
                return 4;
            }
            switch (i) {
                case 10:
                case 11:
                    return 4;
                case 12:
                case 18:
                    return 1;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                case 19:
                case C43951xu.A01:
                case 21:
                    return 2;
                case 15:
                    return 10;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                case 17:
                    return 11;
            }
        } else if (i == 2 || i == 10 || i == 11) {
            return 4;
        } else {
            switch (i) {
                case 15:
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                case 17:
                    return 5;
            }
        }
        return 0;
    }

    public final AnonymousClass31R A09(byte b, boolean z) {
        List list = this.A0P;
        boolean contains = list.contains(AnonymousClass1VX.A00);
        return C63123Aj.A00(b, list.size(), this.A00, this.A02, SystemClock.elapsedRealtime(), this.A01, contains, z, this.A0U, this.A0T, this.A0V);
    }
}
