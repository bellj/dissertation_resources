package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.2Hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48792Hu {
    public int A00;

    public C48792Hu(Context context, C14850m9 r4) {
        this.A00 = AnonymousClass00T.A00(context, r4.A07(926) ? R.color.color_picker_wave2_default_color : R.color.color_picker_default_color);
    }
}
