package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Pe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50342Pe {
    public final UserJid A00;
    public final boolean A01;

    public C50342Pe(UserJid userJid, boolean z) {
        this.A00 = userJid;
        this.A01 = z;
    }
}
