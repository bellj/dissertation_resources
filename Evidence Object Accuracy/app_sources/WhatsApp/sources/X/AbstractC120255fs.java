package X;

import android.content.Context;

/* renamed from: X.5fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC120255fs extends AbstractC451020e {
    public final AnonymousClass6M0 A00;

    public AbstractC120255fs(Context context, C14900mE r2, C18650sn r3, AnonymousClass6M0 r4) {
        super(context, r2, r3);
        this.A00 = r4;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        A05(r2);
        this.A00.AVD(r2);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        A05(r2);
        this.A00.AVD(r2);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r3) {
        C452120p r1;
        AnonymousClass1V8 A0c = C117305Zk.A0c(r3);
        if (A0c != null) {
            r1 = C452120p.A00(A0c);
        } else {
            r1 = null;
        }
        A05(r1);
        this.A00.AVD(r1);
    }

    public void A05(C452120p r5) {
        AnonymousClass60T r1;
        C128545wH r0;
        C130015yf r2;
        long j;
        AnonymousClass605 r22;
        C30931Zj r12;
        String str;
        AnonymousClass61E r13;
        String str2;
        if (this instanceof C121225hS) {
            C121225hS r3 = (C121225hS) this;
            if (r5 == null) {
                r12 = r3.A00.A08;
                str = "changePin success";
                r12.A07(str);
                return;
            }
            r22 = r3.A00;
            r22.A08.A07(C12960it.A0b("changePin error: ", r5));
            int i = r5.A00;
            if (i != 1441) {
                if (i == 1448) {
                    r1 = r22.A07;
                    r0 = r3.A01;
                    C128545wH.A00(r5, r1, r0);
                    return;
                }
                return;
            }
            r2 = r22.A0A;
            j = r5.A02;
        } else if (!(this instanceof C121215hR)) {
            if (this instanceof C121205hQ) {
                C121205hQ r23 = (C121205hQ) this;
                if (r5 == null) {
                    r2 = r23.A00.A0A;
                    r2.A01();
                    j = 0;
                } else if (r5.A00 == 1448) {
                    r1 = r23.A00.A07;
                    r0 = r23.A01;
                } else {
                    return;
                }
            } else if (!(this instanceof C121245hU)) {
                C121235hT r32 = (C121235hT) this;
                r22 = r32.A00;
                C30931Zj r14 = r22.A08;
                if (r5 == null) {
                    r14.A07("setFingerprintFromPin success");
                    r13 = r22.A09;
                    str2 = r32.A02;
                    r13.A06(str2);
                    return;
                }
                r14.A07(C12960it.A0b("setFingerprintFromPin error: ", r5));
                r22.A09.A05();
                int i2 = r5.A00;
                if (i2 != 1441) {
                    if (i2 == 1448) {
                        r1 = r22.A07;
                        r0 = r32.A01;
                    } else {
                        return;
                    }
                }
                r2 = r22.A0A;
                j = r5.A02;
            } else {
                C121245hU r33 = (C121245hU) this;
                if (r5 == null) {
                    AnonymousClass605 r24 = r33.A00;
                    r24.A08.A07("deleteFingerprint success");
                    r24.A09.A05();
                    return;
                }
                r22 = r33.A00;
                r22.A08.A07(C12960it.A0b("deleteFingerprint error: ", r5));
                int i3 = r5.A00;
                if (i3 == 1441) {
                    r22.A09.A06(r33.A02);
                    r2 = r22.A0A;
                    j = r5.A02;
                } else if (i3 == 1440 || i3 == 445) {
                    r13 = r22.A09;
                    str2 = r33.A02;
                    r13.A06(str2);
                    return;
                } else if (i3 == 1448) {
                    r22.A09.A06(r33.A02);
                    r1 = r22.A07;
                    r0 = r33.A01;
                } else {
                    return;
                }
            }
            C128545wH.A00(r5, r1, r0);
            return;
        } else {
            C121215hR r34 = (C121215hR) this;
            if (r5 == null) {
                r12 = r34.A00.A08;
                str = "verifyPinToken success";
                r12.A07(str);
                return;
            }
            r22 = r34.A00;
            r22.A08.A07(C12960it.A0b("verifyPinToken error: ", r5));
            int i4 = r5.A00;
            if (i4 != 1441) {
                if (i4 == 1448) {
                    r1 = r22.A07;
                    r0 = r34.A01;
                    C128545wH.A00(r5, r1, r0);
                    return;
                }
                return;
            }
            r2 = r22.A0A;
            j = r5.A02;
        }
        r2.A02(j);
    }
}
