package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;
import java.util.List;

/* renamed from: X.3Dz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64033Dz {
    public final AnonymousClass1V8 A00;
    public final List A01;

    public C64033Dz(AbstractC15710nm r10, AnonymousClass1V8 r11) {
        AnonymousClass1V8.A01(r11, "persist_data");
        String[] A08 = C13000ix.A08();
        A08[0] = "data";
        this.A01 = AnonymousClass3JT.A0B(r11, new IDxNFunctionShape17S0100000_2_I1(r10, 7), A08, 1, Long.MAX_VALUE);
        this.A00 = r11;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C64033Dz.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((C64033Dz) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
