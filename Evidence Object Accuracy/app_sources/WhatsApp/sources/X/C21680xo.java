package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21680xo {
    public C22040yO A00;
    public final SharedPreferences A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C14850m9 A04;
    public final C21210x3 A05;
    public final AnonymousClass15Y A06;
    public final C16630pM A07;
    public final AbstractC14440lR A08;

    public C21680xo(C14830m7 r2, C16590pI r3, C14850m9 r4, C21210x3 r5, AnonymousClass15Y r6, C16630pM r7, AbstractC14440lR r8) {
        this.A02 = r2;
        this.A04 = r4;
        this.A08 = r8;
        this.A03 = r3;
        this.A05 = r5;
        this.A06 = r6;
        this.A01 = r7.A02("ab-props");
        this.A07 = r7;
    }

    public synchronized long A00() {
        return this.A01.getLong("ab_props:sys:last_refresh_time", 0);
    }

    public synchronized String A01() {
        String str;
        Set<String> stringSet = this.A01.getStringSet("ab_props:sys:last_exposure_keys", null);
        if (stringSet == null) {
            str = "";
        } else {
            str = TextUtils.join(",", stringSet);
        }
        return str;
    }

    public synchronized void A02(int i) {
        SharedPreferences.Editor edit = this.A01.edit();
        edit.putInt("ab_props:sys:fetch_attemp_count", i);
        edit.apply();
    }

    public synchronized void A03(int i) {
        SharedPreferences.Editor edit = this.A01.edit();
        edit.putInt("ab_props:sys:last_error_code", i);
        edit.apply();
    }

    public final boolean A04(SharedPreferences.Editor editor, String str, int i) {
        if (!TextUtils.isEmpty(str)) {
            String num = Integer.toString(i);
            try {
                C14850m9 r4 = this.A04;
                AbstractC17190qP r0 = r4.A01;
                Integer valueOf = Integer.valueOf(i);
                if (r0.containsKey(valueOf)) {
                    boolean z = false;
                    if (Integer.parseInt(str) != 0) {
                        z = true;
                    }
                    editor.putBoolean(num, z);
                    return true;
                } else if (r4.A03.containsKey(valueOf)) {
                    editor.putInt(num, Integer.parseInt(str));
                    return true;
                } else if (r4.A02.containsKey(valueOf)) {
                    editor.putFloat(num, Float.parseFloat(str));
                    return true;
                } else {
                    if (!r4.A05.containsKey(valueOf)) {
                        if (r4.A04.containsKey(valueOf)) {
                            new JSONObject(str);
                        }
                    }
                    editor.putString(num, str);
                    return true;
                }
            } catch (NumberFormatException | JSONException e) {
                StringBuilder sb = new StringBuilder("ABPropsManager/invalid format for config; configCode=");
                sb.append(i);
                sb.append("; value=");
                sb.append(str);
                Log.e(sb.toString(), e);
            }
        }
        return false;
    }
}
