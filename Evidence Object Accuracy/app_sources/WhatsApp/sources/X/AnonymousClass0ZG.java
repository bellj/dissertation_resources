package X;

import android.content.Context;

/* renamed from: X.0ZG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZG implements AbstractC11890h2 {
    public final /* synthetic */ Context A00;

    public AnonymousClass0ZG(Context context) {
        this.A00 = context;
    }

    @Override // X.AbstractC11890h2
    public AbstractC12910il A7s(C04820Ne r4) {
        C05040Oa r2 = new C05040Oa(this.A00);
        r2.A02 = r4.A02;
        r2.A01 = r4.A01;
        r2.A03 = true;
        return new AnonymousClass0ZF().A7s(r2.A00());
    }
}
