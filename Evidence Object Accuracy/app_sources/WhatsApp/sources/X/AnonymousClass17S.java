package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.17S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass17S {
    public Intent A00(Context context) {
        if (!(this instanceof AnonymousClass17R)) {
            Intent intent = new Intent();
            intent.setClassName(context.getPackageName(), "com.whatsapp.profile.ProfileInfoActivity");
            return intent;
        }
        throw new UnsupportedOperationException();
    }
}
