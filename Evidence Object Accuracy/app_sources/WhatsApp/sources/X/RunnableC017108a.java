package X;

import android.view.Menu;
import android.view.Window;

/* renamed from: X.08a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC017108a implements Runnable {
    public final /* synthetic */ AnonymousClass08Z A00;

    public RunnableC017108a(AnonymousClass08Z r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass07H r3;
        AnonymousClass08Z r1 = this.A00;
        Menu A0X = r1.A0X();
        if (A0X instanceof AnonymousClass07H) {
            r3 = (AnonymousClass07H) A0X;
            if (r3 != null) {
                r3.A07();
            }
        } else {
            r3 = null;
        }
        try {
            A0X.clear();
            Window.Callback callback = r1.A00;
            if (!callback.onCreatePanelMenu(0, A0X) || !callback.onPreparePanel(0, null, A0X)) {
                A0X.clear();
            }
        } finally {
            if (r3 != null) {
                r3.A06();
            }
        }
    }
}
