package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.104  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass104 {
    public final AnonymousClass1F0 A00;
    public final C15570nT A01;
    public final C20660w7 A02;

    public AnonymousClass104(AnonymousClass1F0 r1, C15570nT r2, C20660w7 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public static final void A00(byte[] bArr, byte[] bArr2) {
        if (bArr.length != 32) {
            throw new IllegalArgumentException("expected media key of length 32 bytes.");
        } else if (bArr2.length != 12) {
            throw new IllegalArgumentException("expected iv of length 12 bytes.");
        }
    }

    public void A01(AbstractC14640lm r7, Jid jid, UserJid userJid, AbstractC16130oV r10, String str, String str2, int i, boolean z) {
        byte[] bArr;
        if (i == 1 || this.A01.A0E(DeviceJid.of(jid))) {
            byte[] bArr2 = null;
            String str3 = null;
            if (r10 != null) {
                C16150oX r0 = r10.A02;
                AnonymousClass009.A05(r0);
                byte[] bArr3 = r0.A0U;
                AnonymousClass009.A05(bArr3);
                String str4 = r10.A0z.A01;
                if (i == 1) {
                    str3 = str2;
                }
                C43111wQ r1 = new C43111wQ(str4, str3, i);
                bArr2 = C003501n.A0D(12);
                A00(bArr3, bArr2);
                bArr = (byte[]) JniBridge.jvidispatchOOOOO(2, str4, r1.A00, bArr3, bArr2);
            } else {
                bArr = null;
            }
            C20660w7 r12 = this.A02;
            if (r12.A01.A06) {
                C17220qS r3 = r12.A06;
                Message obtain = Message.obtain(null, 0, 34, 0);
                Bundle data = obtain.getData();
                data.putString("id", str);
                data.putParcelable("jid", jid);
                data.putParcelable("remote_jid", r7);
                data.putBoolean("from_me", z);
                data.putParcelable("participant", userJid);
                data.putByteArray("enc_data", bArr);
                data.putByteArray("enc_iv", bArr2);
                data.putInt("error_code", i);
                r3.A08(obtain, false);
            }
        }
    }
}
