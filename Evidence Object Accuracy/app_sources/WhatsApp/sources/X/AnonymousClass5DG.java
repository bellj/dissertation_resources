package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.5DG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5DG implements Iterator {
    public Collection collection;
    public final Iterator delegateIterator;
    public final /* synthetic */ C81143tX this$1;

    public AnonymousClass5DG(C81143tX r2) {
        this.this$1 = r2;
        this.delegateIterator = C12960it.A0n(r2.submap);
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.delegateIterator.hasNext();
    }

    @Override // java.util.Iterator
    public Map.Entry next() {
        Map.Entry A15 = C12970iu.A15(this.delegateIterator);
        this.collection = (Collection) A15.getValue();
        return this.this$1.wrapEntry(A15);
    }

    @Override // java.util.Iterator
    public void remove() {
        C28291Mn.A05("no calls to next() since the last call to remove()", C12960it.A1W(this.collection));
        this.delegateIterator.remove();
        AbstractC80933tC.access$220(this.this$1.this$0, this.collection.size());
        this.collection.clear();
        this.collection = null;
    }
}
