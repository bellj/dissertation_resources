package X;

import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.widget.TextView;

/* renamed from: X.59P  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass59P implements AbstractC116215Uo {
    @Override // X.AbstractC116215Uo
    public Layout A8K(TextPaint textPaint, TextView textView, CharSequence charSequence, int i) {
        return new StaticLayout(charSequence, textPaint, i, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, true);
    }
}
