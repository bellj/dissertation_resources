package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5wa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128735wa {
    public final UserJid A00;
    public final AnonymousClass63Y A01;

    public C128735wa(UserJid userJid, AnonymousClass63Y r2) {
        this.A00 = userJid;
        this.A01 = r2;
    }

    public boolean A00() {
        AnonymousClass63Y r3 = this.A01;
        String str = ((AbstractC30781Yu) r3.A01).A04;
        String str2 = ((AbstractC30781Yu) C30771Yt.A06).A04;
        return !str.equals(str2) && !((AbstractC30781Yu) r3.A02).A04.equals(str2);
    }
}
