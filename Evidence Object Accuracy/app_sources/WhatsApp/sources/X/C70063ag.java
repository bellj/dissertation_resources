package X;

import com.whatsapp.chatinfo.ListChatInfo;
import com.whatsapp.util.Log;

/* renamed from: X.3ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70063ag implements AnonymousClass5WL {
    public final /* synthetic */ ListChatInfo A00;

    public C70063ag(ListChatInfo listChatInfo) {
        this.A00 = listChatInfo;
    }

    @Override // X.AnonymousClass5WL
    public void ASv() {
        C36021jC.A00(this.A00, 2);
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        Log.i("list_chat_info/onclick_leaveGroup");
        ListChatInfo listChatInfo = this.A00;
        C12960it.A1E(new AnonymousClass38O(listChatInfo, ((AbstractActivityC33001d7) listChatInfo).A01, listChatInfo.A2u(), z), ((ActivityC13830kP) listChatInfo).A05);
    }
}
