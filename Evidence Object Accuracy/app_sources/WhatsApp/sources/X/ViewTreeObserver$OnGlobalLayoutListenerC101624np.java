package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101624np implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass21U A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101624np(AnonymousClass21U r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass21U r1 = this.A00;
        C12980iv.A1F(r1.A0N, this);
        AnonymousClass21U.A00(r1);
    }
}
