package X;

import com.whatsapp.util.Log;

/* renamed from: X.2Jr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49192Jr implements AbstractC18940tJ {
    public final /* synthetic */ C14580lf A00;
    public final /* synthetic */ C18850tA A01;

    public C49192Jr(C14580lf r1, C18850tA r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC18940tJ
    public void AXL() {
        Log.i("sync-manager/doPreCompanionLogoutTask onSyncdFailed");
        this.A00.A02(Boolean.FALSE);
    }

    @Override // X.AbstractC18940tJ
    public void AXM() {
        Log.i("sync-manager/doPreCompanionLogoutTask onSyncdSuccess");
        this.A00.A02(Boolean.TRUE);
    }
}
