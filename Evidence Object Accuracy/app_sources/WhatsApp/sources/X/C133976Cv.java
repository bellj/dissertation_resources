package X;

import android.content.DialogInterface;
import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.orderdetails.PaymentOptionsBottomSheet;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.6Cv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133976Cv implements AbstractC136536Mx {
    public final /* synthetic */ IndiaUpiCheckOrderDetailsActivity A00;

    @Override // X.AbstractC136536Mx
    public void AOX(AbstractC14640lm r1, AbstractC16390ow r2) {
    }

    public C133976Cv(IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity) {
        this.A00 = indiaUpiCheckOrderDetailsActivity;
    }

    public final void A00(C30821Yy r9, AbstractC16390ow r10, String str) {
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A00;
        indiaUpiCheckOrderDetailsActivity.A2C(R.string.register_wait_message);
        AbstractC14440lR r7 = ((ActivityC13830kP) indiaUpiCheckOrderDetailsActivity).A05;
        C15650ng r2 = ((AbstractActivityC121685jC) indiaUpiCheckOrderDetailsActivity).A09;
        AnonymousClass1A7 r5 = indiaUpiCheckOrderDetailsActivity.A04;
        AnonymousClass1QC.A09(((ActivityC13810kN) indiaUpiCheckOrderDetailsActivity).A05, r2, ((AbstractActivityC121525iS) indiaUpiCheckOrderDetailsActivity).A07, new AnonymousClass68E(r9, this, r10, str), r5, r10, r7);
    }

    @Override // X.AbstractC136536Mx
    public void ASj(AbstractC14640lm r5, AbstractC16390ow r6, long j) {
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A00;
        indiaUpiCheckOrderDetailsActivity.A0E.A00(r6, indiaUpiCheckOrderDetailsActivity.A0F, 8);
        C117305Zk.A0z(indiaUpiCheckOrderDetailsActivity, r5, j);
    }

    @Override // X.AbstractC136536Mx
    public void ATG(AbstractC14640lm r5, AbstractC16390ow r6, String str) {
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A00;
        indiaUpiCheckOrderDetailsActivity.A0E.A00(r6, indiaUpiCheckOrderDetailsActivity.A0F, 7);
        Intent A00 = indiaUpiCheckOrderDetailsActivity.A0B.A00(indiaUpiCheckOrderDetailsActivity, null, null, str);
        if (A00 == null) {
            Log.e("Pay: IndiaUpiCheckOrderDetailsActivity/onOpenTransactionDetailClicked the transaction details intent is null");
        } else {
            indiaUpiCheckOrderDetailsActivity.startActivity(A00);
        }
    }

    @Override // X.AbstractC136536Mx
    public void ATb(C30821Yy r5, AbstractC16390ow r6, String str, String str2, List list) {
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A00;
        indiaUpiCheckOrderDetailsActivity.A0E.A00(r6, indiaUpiCheckOrderDetailsActivity.A0F, 9);
        PaymentOptionsBottomSheet A00 = PaymentOptionsBottomSheet.A00(str, list);
        A00.A02 = new C127725ux(r5, this, r6, str2);
        C42791vs.A01(A00, indiaUpiCheckOrderDetailsActivity.A0V());
    }

    @Override // X.AbstractC136536Mx
    public void AVr(C30821Yy r8, AbstractC14640lm r9, AbstractC16390ow r10, String str) {
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A00;
        indiaUpiCheckOrderDetailsActivity.A0E.A00(r10, indiaUpiCheckOrderDetailsActivity.A0F, 5);
        if ("WhatsappPay".equals(indiaUpiCheckOrderDetailsActivity.A0F)) {
            A00(r8, r10, str);
            return;
        }
        List list = C117305Zk.A0b(r10).A0A;
        AnonymousClass009.A05(list);
        AnonymousClass009.A0E(!list.isEmpty());
        String str2 = ((AnonymousClass1ZJ) r10.ABf().A01.A0A.get(0)).A01;
        C004802e A0S = C12980iv.A0S(indiaUpiCheckOrderDetailsActivity);
        A0S.A0B(false);
        A0S.setTitle(indiaUpiCheckOrderDetailsActivity.getString(R.string.order_details_leave_whatsapp_title));
        A0S.A0A(C12960it.A0X(indiaUpiCheckOrderDetailsActivity, str2, new Object[1], 0, R.string.order_details_leave_whatsapp_message));
        C72463ee.A0S(A0S);
        A0S.setPositiveButton(R.string.btn_continue, new DialogInterface.OnClickListener(r10, str2) { // from class: X.62h
            public final /* synthetic */ AbstractC16390ow A01;
            public final /* synthetic */ String A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C133976Cv r0 = C133976Cv.this;
                AbstractC16390ow r5 = this.A01;
                String str3 = this.A02;
                IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity2 = r0.A00;
                indiaUpiCheckOrderDetailsActivity2.A0E.A00(r5, indiaUpiCheckOrderDetailsActivity2.A0F, 17);
                indiaUpiCheckOrderDetailsActivity2.startActivity(C117295Zj.A04(str3));
            }
        });
        C12970iu.A1J(A0S);
    }
}
