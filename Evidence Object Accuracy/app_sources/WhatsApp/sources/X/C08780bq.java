package X;

/* renamed from: X.0bq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08780bq implements AbstractC17450qp {
    public final AbstractC17450qp A00;

    public C08780bq(AbstractC17450qp r1) {
        this.A00 = r1;
    }

    public static final double A00(AnonymousClass4YC r1) {
        return r1.A00();
    }

    public static final void A01(AnonymousClass4YC r2, float f) {
        r2.A02((double) f);
    }

    public final AnonymousClass4YC A02(C14230l4 r4, AbstractC14200l1 r5) {
        AnonymousClass4YC A01 = C94254bV.A00().A01();
        C14210l2 r1 = new C14210l2();
        r1.A05(A01, 0);
        A01.A03(new C03500Ia(this, r4, r1.A03(), r5));
        return A01;
    }

    /* renamed from: A03 */
    public Object A9j(C14230l4 r5, C14220l3 r6, C1093651k r7) {
        String str = r7.A00;
        switch (str.hashCode()) {
            case -600206934:
                if (str.equals("bk.action.animation.spring.GetCurrentValue")) {
                    return C64983Hr.A00(A00((AnonymousClass4YC) r6.A00(0)));
                }
                break;
            case -512481856:
                if (str.equals("bk.action.animation.spring.Create")) {
                    return A02(r5, C94724cR.A00(r6.A01(0)));
                }
                break;
            case 1371463060:
                if (str.equals("bk.action.animation.spring.SetEndValue")) {
                    A01((AnonymousClass4YC) r6.A00(0), ((Number) r6.A01(1)).floatValue());
                    return null;
                }
                break;
        }
        return this.A00.A9j(r6, r7, r5);
    }
}
