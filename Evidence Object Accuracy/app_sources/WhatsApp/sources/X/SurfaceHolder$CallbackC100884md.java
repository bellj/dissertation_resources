package X;

import android.view.SurfaceHolder;

/* renamed from: X.4md  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class SurfaceHolder$CallbackC100884md implements SurfaceHolder.Callback {
    public final /* synthetic */ C849340j A00;

    public SurfaceHolder$CallbackC100884md(C849340j r1) {
        this.A00 = r1;
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        C849340j r1 = this.A00;
        r1.hashCode();
        AnonymousClass009.A01();
        ((Number) AnonymousClass5B1.A00(r1, new CallableC112555Dv(r1, i2, i3))).intValue();
        AnonymousClass5X3 r0 = r1.A02;
        if (r0 != null) {
            r0.ATu(r1);
        }
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        C849340j r0 = this.A00;
        r0.hashCode();
        r0.A05();
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        C849340j r0 = this.A00;
        r0.hashCode();
        r0.A04();
    }
}
