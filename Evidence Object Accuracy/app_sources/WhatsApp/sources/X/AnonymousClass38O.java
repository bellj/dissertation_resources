package X;

import android.os.SystemClock;
import java.lang.ref.WeakReference;

/* renamed from: X.38O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38O extends AbstractC16350or {
    public final long A00 = SystemClock.elapsedRealtime();
    public final C16170oZ A01;
    public final AbstractC14640lm A02;
    public final WeakReference A03;
    public final boolean A04;

    public AnonymousClass38O(ActivityC13810kN r3, C16170oZ r4, AbstractC14640lm r5, boolean z) {
        super(r3);
        this.A03 = C12970iu.A10(r3);
        this.A02 = r5;
        this.A01 = r4;
        this.A04 = z;
    }
}
