package X;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import com.facebook.redex.IDxCListenerShape11S0100000_3_I1;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.5bP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118185bP extends AnonymousClass015 implements AnonymousClass1In {
    public int A00;
    public int A01 = R.string.transaction_details;
    public AnonymousClass016 A02 = new AnonymousClass016(C12960it.A0l());
    public AnonymousClass1IR A03;
    public AbstractC35651iS A04;
    public C124305ow A05;
    public C127915vG A06;
    public AnonymousClass1IS A07;
    public C27691It A08 = C13000ix.A03();
    public Boolean A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public boolean A0D = false;
    public boolean A0E = false;
    public final Bundle A0F;
    public final C14900mE A0G;
    public final C15570nT A0H;
    public final C14650lo A0I;
    public final AnonymousClass1AA A0J;
    public final C238013b A0K;
    public final C15550nR A0L;
    public final AnonymousClass01d A0M;
    public final C14830m7 A0N;
    public final C16590pI A0O;
    public final AnonymousClass018 A0P;
    public final C15650ng A0Q;
    public final C20300vX A0R;
    public final C20370ve A0S;
    public final AnonymousClass102 A0T;
    public final C241414j A0U;
    public final C129925yW A0V;
    public final C20380vf A0W;
    public final C21860y6 A0X;
    public final C243515e A0Y;
    public final C22710zW A0Z;
    public final C17070qD A0a;
    public final AnonymousClass1A7 A0b;
    public final AbstractC16870pt A0c;
    public final C30931Zj A0d = C117305Zk.A0V("PaymentTransactionDetailsViewModel", "payment-settings");
    public final AnonymousClass17Z A0e;
    public final AnonymousClass604 A0f;
    public final AnonymousClass14X A0g;
    public final AbstractC14440lR A0h;

    public C118185bP(Bundle bundle, C14900mE r6, C15570nT r7, C14650lo r8, AnonymousClass1AA r9, C238013b r10, C15550nR r11, AnonymousClass01d r12, C14830m7 r13, C16590pI r14, AnonymousClass018 r15, C15650ng r16, C20300vX r17, C20370ve r18, AnonymousClass102 r19, C241414j r20, C129925yW r21, C20380vf r22, C21860y6 r23, C243515e r24, C22710zW r25, C17070qD r26, AnonymousClass1A7 r27, AbstractC16870pt r28, AnonymousClass17Z r29, AnonymousClass604 r30, AnonymousClass14X r31, AbstractC14440lR r32) {
        AbstractC35651iS r0;
        this.A0N = r13;
        this.A0G = r6;
        this.A0H = r7;
        this.A0O = r14;
        this.A0h = r32;
        this.A0U = r20;
        this.A0g = r31;
        this.A0L = r11;
        this.A0M = r12;
        this.A0P = r15;
        this.A0a = r26;
        this.A0K = r10;
        this.A0Q = r16;
        this.A0J = r9;
        this.A0f = r30;
        this.A0X = r23;
        this.A0R = r17;
        this.A0Z = r25;
        this.A0T = r19;
        this.A0I = r8;
        this.A0V = r21;
        this.A0c = r28;
        this.A0e = r29;
        this.A0S = r18;
        this.A0b = r27;
        this.A0Y = r24;
        this.A0W = r22;
        this.A0F = bundle;
        bundle.getString("referral_screen");
        this.A07 = C38211ni.A03(bundle, "");
        this.A0B = bundle.getString("extra_transaction_id");
        String string = bundle.getString("extra_payment_receipt_type");
        this.A0A = string == null ? "native" : string;
        this.A0C = bundle.getString("extra_transaction_ref");
        this.A09 = Boolean.valueOf(bundle.getBoolean("extra_is_pending_request_saved_instance", false));
        if (!(this instanceof C123575nN)) {
            r0 = new AnonymousClass69W(this);
        } else {
            r0 = new AnonymousClass69V((C123575nN) this);
        }
        this.A04 = r0;
        r24.A03(r0);
        this.A00 = bundle.getInt("extra_payment_flow_entry_point", 0);
    }

    public static void A00(C128365vz r2, C118185bP r3, String str) {
        r2.A0m = str;
        C127915vG r0 = r3.A06;
        if (r0 != null) {
            AnonymousClass1IR r02 = r0.A01;
            r2.A0Q = C125015qX.A00(C31001Zq.A05(r02.A03, r02.A02));
        }
    }

    public static void A01(C118185bP r0, Object obj) {
        r0.A08.A0B(obj);
    }

    public Runnable A04(C123315mx r2) {
        if (r2.A05 != null) {
            return new Runnable(r2, this) { // from class: X.6Is
                public final /* synthetic */ C123315mx A00;
                public final /* synthetic */ C118185bP A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C118185bP r3 = this.A01;
                    C123315mx r22 = this.A00;
                    C128315vu A00 = C128315vu.A00(4);
                    A00.A03 = r22.A05;
                    C118185bP.A01(r3, A00);
                }
            };
        }
        if (this.A0a.A02().AF8() == null || A05() == null || !this.A0X.A0B()) {
            return null;
        }
        return new Runnable(r2, this) { // from class: X.6Ir
            public final /* synthetic */ C123315mx A00;
            public final /* synthetic */ C118185bP A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C118185bP r6 = this.A01;
                C123315mx r5 = this.A00;
                String A05 = r6.A05();
                AbstractC450620a AF8 = r6.A0a.A02().AF8();
                if (AF8 != null && r5.A02 != 0) {
                    r5.A01 = 4;
                    r5.A02 = 0;
                    AnonymousClass016 r1 = r6.A02;
                    r1.A0B(r1.A01());
                    AF8.Afg(C117305Zk.A0I(C117305Zk.A0J(), String.class, A05, "paymentHandle"), 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0038: INVOKE  
                          (r3v0 'AF8' X.20a)
                          (wrap: X.1ZR : 0x002f: INVOKE  (r1v3 X.1ZR A[REMOVE]) = 
                          (wrap: X.2SM : 0x0027: INVOKE  (r2v0 X.2SM A[REMOVE]) =  type: STATIC call: X.5Zk.A0J():X.2SM)
                          (wrap: java.lang.Class : 0x002b: CONST_CLASS   java.lang.String.class)
                          (r4v0 'A05' java.lang.String)
                          ("paymentHandle")
                         type: STATIC call: X.5Zk.A0I(X.2SN, java.lang.Class, java.lang.Object, java.lang.String):X.1ZR)
                          (wrap: X.694 : 0x0035: CONSTRUCTOR  (r0v6 X.694 A[REMOVE]) = (r5v0 'r5' X.5mx), (r6v0 'r6' X.5bP), (r4v0 'A05' java.lang.String) call: X.694.<init>(X.5mx, X.5bP, java.lang.String):void type: CONSTRUCTOR)
                         type: INTERFACE call: X.20a.Afg(X.1ZR, X.6Ln):void in method: X.6Ir.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0035: CONSTRUCTOR  (r0v6 X.694 A[REMOVE]) = (r5v0 'r5' X.5mx), (r6v0 'r6' X.5bP), (r4v0 'A05' java.lang.String) call: X.694.<init>(X.5mx, X.5bP, java.lang.String):void type: CONSTRUCTOR in method: X.6Ir.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.694, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                        this = this;
                        X.5bP r6 = r7.A01
                        X.5mx r5 = r7.A00
                        java.lang.String r4 = r6.A05()
                        X.0qD r0 = r6.A0a
                        X.0pp r0 = r0.A02()
                        X.20a r3 = r0.AF8()
                        if (r3 == 0) goto L_0x003b
                        r1 = 0
                        int r0 = r5.A02
                        if (r0 == 0) goto L_0x003b
                        r0 = 4
                        r5.A01 = r0
                        r5.A02 = r1
                        X.016 r1 = r6.A02
                        java.lang.Object r0 = r1.A01()
                        r1.A0B(r0)
                        X.2SM r2 = X.C117305Zk.A0J()
                        java.lang.Class<java.lang.String> r1 = java.lang.String.class
                        java.lang.String r0 = "paymentHandle"
                        X.1ZR r1 = X.C117305Zk.A0I(r2, r1, r4, r0)
                        X.694 r0 = new X.694
                        r0.<init>(r5, r6, r4)
                        r3.Afg(r1, r0)
                    L_0x003b:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.RunnableC135496Ir.run():void");
                }
            };
        }

        public String A05() {
            C127915vG r0 = this.A06;
            if (r0 != null) {
                Boolean A02 = r0.A01.A02();
                AbstractC30891Zf r1 = this.A06.A01.A0A;
                AnonymousClass009.A05(r1);
                if (A02 != null) {
                    return A02.booleanValue() ? r1.A0F() : r1.A0G();
                }
            }
            return null;
        }

        public String A06(AnonymousClass1IR r2) {
            if (r2.A0F() || C31001Zq.A09(r2.A0F)) {
                return r2.A0F;
            }
            return r2.A0K;
        }

        public void A07() {
            AbstractC15340mz r0;
            String str;
            C128315vu A00;
            C30931Zj r4 = this.A0d;
            r4.A06("Parent- HANDLE_SEND_AGAIN child did not handle");
            C127915vG r02 = this.A06;
            if (r02 != null) {
                r0 = r02.A02;
            } else {
                r0 = null;
            }
            if (r0 != null) {
                AnonymousClass1IR r3 = r0.A0L;
                if (r3 != null) {
                    UserJid userJid = r3.A0D;
                    if (userJid != null) {
                        if (!this.A0K.A0I(userJid)) {
                            AbstractC30891Zf r03 = r3.A0A;
                            if (r03 == null || r03.A02 == null) {
                                A00 = C128315vu.A00(12);
                            } else {
                                A00 = C128315vu.A00(21);
                                A00.A0C = r3.A0A.A02.A01;
                                C127915vG r04 = this.A06;
                                AnonymousClass009.A05(r04);
                                C16380ov r1 = r04.A03;
                                AnonymousClass009.A05(r1);
                                A00.A08 = r1.A0z;
                                AnonymousClass009.A05(r1);
                                A00.A01 = this.A00;
                            }
                        } else {
                            A00 = C128315vu.A00(13);
                            A00.A06 = r3.A0D;
                        }
                        A01(this, A00);
                    }
                    str = "Parent- HANDLE_SEND_AGAIN pmtTxnInfo.receiverJid is null";
                } else {
                    str = "Parent- HANDLE_SEND_AGAIN pmtTxnInfo is null";
                }
            } else {
                str = "Parent- HANDLE_SEND_AGAIN FMessage is null";
            }
            r4.A06(str);
            A00 = C128315vu.A00(8);
            A00.A0B = this.A0O.A00.getString(R.string.payments_generic_error);
            A01(this, A00);
        }

        public void A08() {
            AnonymousClass1IR r5;
            C127915vG r1 = this.A06;
            if (r1 != null && !this.A0E && (r5 = r1.A01) != null) {
                this.A0E = true;
                if (this.A0X.A0C() && !TextUtils.isEmpty(r5.A0K) && !r5.A0F()) {
                    C30931Zj r2 = this.A0d;
                    StringBuilder A0k = C12960it.A0k("syncing pending transaction: ");
                    A0k.append(r5.A0K);
                    A0k.append(" status: ");
                    A0k.append(r5.A02);
                    C117295Zj.A1F(r2, A0k);
                    AbstractC16870pt A0U = C117305Zk.A0U(this.A0a);
                    if (A0U != null) {
                        A0U.AeG();
                    }
                    this.A0b.A01(new C1328568m(A0U, this), r5.A0K, r5.A0J());
                }
            }
        }

        public void A09() {
            AnonymousClass016 r2 = this.A02;
            List list = (List) r2.A01();
            list.clear();
            A0O(false);
            C127915vG r0 = this.A06;
            if (r0 != null && r0.A01 != null) {
                A0G(list);
                r2.A0B(list);
            }
        }

        public void A0A() {
            AnonymousClass1IR r0;
            if (!this.A0D) {
                C128315vu A00 = C128315vu.A00(0);
                C127915vG r02 = this.A06;
                if (!(r02 == null || (r0 = r02.A01) == null || !r0.A0D())) {
                    this.A01 = R.string.request_details;
                }
                A00.A02.putInt("action_bar_title_res_id", this.A01);
                A01(this, A00);
                this.A0D = true;
            }
        }

        public void A0B(Context context) {
            if (this instanceof C123565nM) {
                C123565nM r6 = (C123565nM) this;
                Activity A00 = AbstractC35731ia.A00(context);
                AnonymousClass009.A05(A00);
                AbstractC001200n r5 = (AbstractC001200n) A00;
                String str = ((C118185bP) r6).A0B;
                if (str != null) {
                    C130095yn r3 = r6.A0B;
                    AnonymousClass009.A05(str);
                    AnonymousClass016 A0T = C12980iv.A0T();
                    r3.A0A.Ab2(new AnonymousClass6JG(A0T, r3, str));
                    C117295Zj.A0s(r5, A0T, r6, 145);
                    return;
                }
                ((C118185bP) r6).A08.A0B(new C123525nI(2));
            } else if (this instanceof C123555nL) {
                C123555nL r2 = (C123555nL) this;
                r2.A0h.Ab2(new Runnable() { // from class: X.6HB
                    @Override // java.lang.Runnable
                    public final void run() {
                        C123555nL r0 = C123555nL.this;
                        r0.A0b.A00(new C1327768e(r0), null, null, null);
                    }
                });
            }
        }

        public void A0C(AnonymousClass1IR r4) {
            if (this instanceof C123565nM) {
                Bundle bundle = this.A0F;
                if (bundle.getParcelable("extra_transaction_detail_data") != null) {
                    bundle.putParcelable("extra_transaction_detail_data", r4);
                }
            }
            if ((!TextUtils.isEmpty(r4.A0K) && r4.A0K.equals(this.A0B)) || ((!TextUtils.isEmpty(r4.A0L) && r4.A0L.equals(this.A07.A01)) || (!TextUtils.isEmpty(r4.A0M) && r4.A0M.equals(this.A07.A01)))) {
                A0P(false);
            }
        }

        public void A0D(C123325my r3) {
            AnonymousClass1IR r1 = this.A06.A01;
            r3.A03 = AnonymousClass14X.A04(this.A0O.A00, r1);
            r3.A01 = AnonymousClass14X.A01(r1);
        }

        public void A0E(C126075sI r3) {
            if (r3.A00 == 2) {
                A0P(true);
            }
        }

        public void A0F(String str, Integer num) {
            this.A0c.AKj(null, 1, num, "payment_transaction_details", str, null, null, false, true);
        }

        public void A0G(List list) {
            A0L(list);
            A0N(list);
            C123575nN.A03(list);
            C127915vG r0 = this.A06;
            AnonymousClass009.A05(r0);
            AnonymousClass1IR r3 = r0.A01;
            AbstractC30891Zf r2 = r3.A0A;
            if (!(r2 == null || r2.A02 == null)) {
                list.add(new C123085ma(new View.OnClickListener(r2, r3, this) { // from class: X.64R
                    public final /* synthetic */ AbstractC30891Zf A00;
                    public final /* synthetic */ AnonymousClass1IR A01;
                    public final /* synthetic */ C118185bP A02;

                    {
                        this.A02 = r3;
                        this.A00 = r1;
                        this.A01 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        C118185bP r4 = this.A02;
                        AbstractC30891Zf r32 = this.A00;
                        AnonymousClass1IR r22 = this.A01;
                        C128315vu A00 = C128315vu.A00(21);
                        C38171nd r02 = r32.A02;
                        AnonymousClass009.A05(r02);
                        A00.A0C = r02.A01;
                        A00.A05 = r22;
                        C16380ov r03 = r4.A06.A03;
                        if (r03 != null) {
                            A00.A08 = r03.A0z;
                        }
                        A00.A01 = r4.A00;
                        C118185bP.A01(r4, A00);
                    }
                }));
                list.add(new C122975mP());
            }
            A0K(list);
            A0M(list);
        }

        public void A0H(List list) {
            IDxCListenerShape11S0100000_3_I1 iDxCListenerShape11S0100000_3_I1;
            UserJid userJid;
            String str;
            C30971Zn r8;
            C123315mx r7 = new C123315mx();
            AnonymousClass1IR r6 = this.A06.A01;
            Context context = this.A0O.A00;
            r7.A09 = C130345zG.A00(context, r6.A03);
            r7.A08 = this.A0g.A0L(r6);
            long j = r6.A05;
            if (j > 0) {
                Object[] A1b = C12970iu.A1b();
                AnonymousClass018 r2 = this.A0P;
                C14830m7 r9 = this.A0N;
                r7.A0A = C12960it.A0X(context, C38121nY.A05(r2, AnonymousClass1MY.A02(r2, r9.A02(j)), AnonymousClass3JK.A00(r2, r9.A02(r6.A05))), A1b, 0, R.string.time_and_date);
            }
            AbstractC38191ng A0K = C117305Zk.A0K(this.A0a);
            if (A0K != null && C117305Zk.A1U(A0K.A07)) {
                if (r6.A03 == 9) {
                    int i = r6.A01;
                    int i2 = R.drawable.ic_incentive_sender_wa;
                    if (i != 1) {
                        i2 = R.drawable.ic_incentive_sender_fb;
                        if (i != 2) {
                            i2 = R.drawable.avatar_contact;
                        }
                    }
                    r7.A00 = i2;
                    AnonymousClass1IR r1 = this.A03;
                    if (r1 != null) {
                        r7.A07 = C12960it.A0X(context, Html.escapeHtml(A0K.A08.A0L(r1)), new Object[1], 0, A0K.A06());
                        r7.A03 = C117305Zk.A0A(this, 173);
                    }
                } else {
                    AnonymousClass17Z r22 = this.A0e;
                    AbstractC30891Zf r0 = r6.A0A;
                    if (!(r0 == null || (r8 = r0.A00) == null)) {
                        C50942Ry r23 = (C50942Ry) r22.A0F.A02.get(Long.valueOf(Long.valueOf(Long.parseLong(r8.A02)).longValue()));
                        if (r23 != null && TextUtils.isEmpty(r8.A00)) {
                            int i3 = r6.A02;
                            if (!(i3 == 405 || i3 == 417 || i3 == 419 || i3 == 420)) {
                                switch (i3) {
                                }
                                r7.A07 = str;
                                r7.A06 = A0K.A09();
                            }
                            AnonymousClass20C r02 = r23.A07;
                            str = C12960it.A0X(context, AnonymousClass14X.A02(context, A0K.A06, r02.A01, r02.A02).toString(), C12970iu.A1b(), 0, A0K.A05());
                            r7.A07 = str;
                            r7.A06 = A0K.A09();
                        }
                    }
                    str = null;
                    r7.A07 = str;
                    r7.A06 = A0K.A09();
                }
            }
            Boolean A02 = r6.A02();
            if (A02 != null) {
                if (A02.booleanValue()) {
                    userJid = r6.A0D;
                } else {
                    userJid = r6.A0E;
                }
                if (userJid != null) {
                    r7.A05 = this.A0L.A0B(userJid);
                }
            }
            Runnable A04 = A04(r7);
            if (A04 != null) {
                iDxCListenerShape11S0100000_3_I1 = C117305Zk.A0A(A04, 175);
            } else {
                iDxCListenerShape11S0100000_3_I1 = null;
            }
            r7.A04 = iDxCListenerShape11S0100000_3_I1;
            list.add(r7);
        }

        public void A0I(List list) {
            if (this.A0Z.A03() && A0Q(this.A06.A01)) {
                list.add(new C123095mb(C117305Zk.A0A(this, 174)));
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Code restructure failed: missing block: B:149:0x038b, code lost:
            if (android.text.TextUtils.isEmpty(r10) == false) goto L_0x0171;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0079, code lost:
            if (r3.A08 == null) goto L_0x007b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x016f, code lost:
            if (android.text.TextUtils.isEmpty(r10) == false) goto L_0x0171;
         */
        /* JADX WARNING: Removed duplicated region for block: B:129:0x02fb  */
        /* JADX WARNING: Removed duplicated region for block: B:146:0x0372  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00a9  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00ed  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0108  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x0130  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x019a  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void A0J(java.util.List r15) {
            /*
            // Method dump skipped, instructions count: 1670
            */
            throw new UnsupportedOperationException("Method not decompiled: X.C118185bP.A0J(java.util.List):void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
            if (X.AnonymousClass604.A00(r5) == false) goto L_0x0020;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void A0K(java.util.List r7) {
            /*
                r6 = this;
                X.5vG r0 = r6.A06
                X.1IR r5 = r0.A01
                X.1Pl r4 = r0.A00
                X.5mi r3 = new X.5mi
                r3.<init>()
                X.0zW r0 = r6.A0Z
                X.0m9 r1 = r0.A03
                r0 = 1359(0x54f, float:1.904E-42)
                boolean r0 = r1.A07(r0)
                if (r0 == 0) goto L_0x0020
                boolean r0 = X.AnonymousClass604.A00(r5)
                r2 = 1
                r1 = 22
                if (r0 != 0) goto L_0x0023
            L_0x0020:
                r2 = 0
                r1 = 10
            L_0x0023:
                X.64c r0 = new X.64c
                r0.<init>(r4, r5, r6, r1)
                r3.A00 = r0
                r3.A01 = r2
                r7.add(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: X.C118185bP.A0K(java.util.List):void");
        }

        public void A0L(List list) {
            C123575nN.A02(this, list);
            A0H(list);
        }

        public void A0M(List list) {
            AnonymousClass1IR r3 = this.A06.A01;
            if (this.A0Z.A03.A07(1359) && r3.A03 == 100 && r3.A0H()) {
                C123105mc r2 = new C123105mc();
                r2.A00 = new IDxCListenerShape2S0200000_3_I1(this, 49, r3);
                list.add(r2);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:106:0x01f8 A[ORIG_RETURN, RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x0184  */
        /* JADX WARNING: Removed duplicated region for block: B:90:0x0197  */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x01bb  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void A0N(java.util.List r10) {
            /*
            // Method dump skipped, instructions count: 505
            */
            throw new UnsupportedOperationException("Method not decompiled: X.C118185bP.A0N(java.util.List):void");
        }

        public void A0O(boolean z) {
            C128315vu A00 = C128315vu.A00(1);
            A00.A0H = z;
            A01(this, A00);
        }

        public void A0P(boolean z) {
            C124305ow r2;
            if (this.A05 == null) {
                String str = this.A0A;
                if ("native".equals(str)) {
                    r2 = new C124305ow(this, this.A07, this.A0B, z);
                } else if ("non_native".equals(str)) {
                    r2 = new C123545nK(this, this.A07, this.A0B, z);
                } else {
                    C117305Zk.A1N("PaymentTransactionDetailsViewModel", "unsupported payment receipt type");
                    return;
                }
                this.A05 = r2;
                C12960it.A1E(r2, this.A0h);
            }
        }

        public boolean A0Q(AnonymousClass1IR r4) {
            C17070qD r1 = this.A0a;
            return this.A0g.A0b(r4, r1.A02().ABq(), r1.A02().AEE());
        }

        public boolean A0R(String str) {
            ClipboardManager A0B = this.A0M.A0B();
            if (A0B != null) {
                try {
                    A0B.setPrimaryClip(ClipData.newPlainText(str, str));
                    this.A0G.A07(R.string.transaction_payment_method_id_copied, 0);
                    return true;
                } catch (NullPointerException | SecurityException e) {
                    this.A0d.A0A("getTransactionIdRow paymentTransactionID", e);
                }
            }
            this.A0G.A07(R.string.view_contact_unsupport, 0);
            return true;
        }

        @Override // X.AnonymousClass1In
        public void ATd() {
            A0P(false);
        }
    }
