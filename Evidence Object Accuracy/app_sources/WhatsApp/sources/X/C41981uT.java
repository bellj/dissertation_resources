package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1uT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41981uT extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C41981uT A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public AbstractC27881Jp A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public AbstractC27881Jp A04;

    static {
        C41981uT r0 = new C41981uT();
        A05 = r0;
        r0.A0W();
    }

    public C41981uT() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A03 = r0;
        this.A01 = r0;
        this.A02 = r0;
        this.A04 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r6, Object obj, Object obj2) {
        switch (r6.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C41981uT r8 = (C41981uT) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                AbstractC27881Jp r2 = this.A03;
                boolean z2 = true;
                if ((r8.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A03 = r7.Afm(r2, r8.A03, z, z2);
                boolean z3 = false;
                if ((this.A00 & 2) == 2) {
                    z3 = true;
                }
                AbstractC27881Jp r3 = this.A01;
                boolean z4 = false;
                if ((r8.A00 & 2) == 2) {
                    z4 = true;
                }
                this.A01 = r7.Afm(r3, r8.A01, z3, z4);
                boolean z5 = false;
                if ((this.A00 & 4) == 4) {
                    z5 = true;
                }
                AbstractC27881Jp r32 = this.A02;
                boolean z6 = false;
                if ((r8.A00 & 4) == 4) {
                    z6 = true;
                }
                this.A02 = r7.Afm(r32, r8.A02, z5, z6);
                boolean z7 = false;
                if ((this.A00 & 8) == 8) {
                    z7 = true;
                }
                AbstractC27881Jp r33 = this.A04;
                boolean z8 = false;
                if ((r8.A00 & 8) == 8) {
                    z8 = true;
                }
                this.A04 = r7.Afm(r33, r8.A04, z7, z8);
                if (r7 == C463025i.A00) {
                    this.A00 |= r8.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                this.A00 |= 1;
                                this.A03 = r72.A08();
                            } else if (A03 == 18) {
                                this.A00 |= 2;
                                this.A01 = r72.A08();
                            } else if (A03 == 26) {
                                this.A00 |= 4;
                                this.A02 = r72.A08();
                            } else if (A03 == 34) {
                                this.A00 |= 8;
                                this.A04 = r72.A08();
                            } else if (!A0a(r72, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C41981uT();
            case 5:
                return new AnonymousClass2KQ();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C41981uT.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A03, 1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A09(this.A01, 2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A02, 3);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A09(this.A04, 4);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A03, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A01, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A02, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A04, 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
