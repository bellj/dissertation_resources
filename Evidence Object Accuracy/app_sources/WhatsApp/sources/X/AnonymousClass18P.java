package X;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S0500000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S1500000_I0;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.18P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass18P {
    public final C14900mE A00;
    public final C14830m7 A01;
    public final C20830wO A02;
    public final C15650ng A03;
    public final C15600nX A04;
    public final C21380xK A05;
    public final C20710wC A06;
    public final C21860y6 A07;
    public final AnonymousClass18T A08;
    public final AnonymousClass18S A09;
    public final C18610sj A0A;
    public final C17070qD A0B;
    public final C20320vZ A0C;
    public final AbstractC14440lR A0D;

    public AnonymousClass18P(C14900mE r1, C14830m7 r2, C20830wO r3, C15650ng r4, C15600nX r5, C21380xK r6, C20710wC r7, C21860y6 r8, AnonymousClass18T r9, AnonymousClass18S r10, C18610sj r11, C17070qD r12, C20320vZ r13, AbstractC14440lR r14) {
        this.A01 = r2;
        this.A00 = r1;
        this.A0D = r14;
        this.A05 = r6;
        this.A0C = r13;
        this.A0B = r12;
        this.A03 = r4;
        this.A06 = r7;
        this.A07 = r8;
        this.A09 = r10;
        this.A0A = r11;
        this.A08 = r9;
        this.A04 = r5;
        this.A02 = r3;
    }

    public void A00(Context context, AnonymousClass1IR r8) {
        AbstractC16830pp r0;
        int AFH;
        AbstractC38041nQ A01 = this.A0B.A01(r8.A0G);
        if (A01 != null) {
            r0 = A01.AFX(r8.A0I);
        } else {
            r0 = null;
        }
        if (r0 == null || (AFH = r0.AFH()) == 0) {
            ((AbstractC13860kS) context).Adr(new Object[0], 0, R.string.payment_unblock_error);
        } else {
            ((AbstractC13860kS) context).Adr(new Object[]{context.getString(AFH)}, 0, R.string.unblock_payment_id_error_default);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004c, code lost:
        if (r1.A02().ABo().AJE(new X.AnonymousClass1ZR(new X.AnonymousClass2SM(), java.lang.String.class, r6, "upiHandle")) == false) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r8.A00.A0I(com.whatsapp.jid.UserJid.of(r0)) == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.content.Context r16, X.AnonymousClass1IR r17, X.AnonymousClass5W7 r18, boolean r19) {
        /*
            r15 = this;
            X.18S r8 = r15.A09
            r5 = r17
            com.whatsapp.jid.UserJid r0 = r5.A0D
            if (r0 == 0) goto L_0x0015
            X.13b r1 = r8.A00
            com.whatsapp.jid.UserJid r0 = com.whatsapp.jid.UserJid.of(r0)
            boolean r0 = r1.A0I(r0)
            r7 = 1
            if (r0 != 0) goto L_0x0016
        L_0x0015:
            r7 = 0
        L_0x0016:
            X.1Zf r0 = r5.A0A
            if (r0 == 0) goto L_0x0059
            java.lang.String r6 = r0.A0F()
        L_0x001e:
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x004e
            X.0qD r1 = r8.A04
            X.0pp r0 = r1.A02()
            X.18N r0 = r0.ABo()
            if (r0 == 0) goto L_0x004e
            X.0pp r0 = r1.A02()
            X.18N r4 = r0.ABo()
            X.2SM r3 = new X.2SM
            r3.<init>()
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            java.lang.String r1 = "upiHandle"
            X.1ZR r0 = new X.1ZR
            r0.<init>(r3, r2, r6, r1)
            boolean r1 = r4.AJE(r0)
            r0 = 1
            if (r1 != 0) goto L_0x004f
        L_0x004e:
            r0 = 0
        L_0x004f:
            r4 = r18
            if (r7 != 0) goto L_0x005b
            if (r0 != 0) goto L_0x005b
            r4.AWu()
            return
        L_0x0059:
            r6 = 0
            goto L_0x001e
        L_0x005b:
            android.app.Activity r9 = X.AnonymousClass12P.A00(r16)
            com.whatsapp.jid.UserJid r11 = r5.A0D
            X.2SM r3 = new X.2SM
            r3.<init>()
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            X.1Zf r0 = r5.A0A
            java.lang.String r1 = r0.A0F()
            java.lang.String r0 = "paymentHandle"
            X.1ZR r12 = new X.1ZR
            r12.<init>(r3, r2, r1, r0)
            r13 = 0
            X.53y r10 = new X.53y
            r10.<init>()
            r14 = r19
            r8.A00(r9, r10, r11, r12, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass18P.A01(android.content.Context, X.1IR, X.5W7, boolean):void");
    }

    public final void A02(View view, Button button, AnonymousClass1IR r7) {
        AbstractC16830pp AFX;
        View A0D = AnonymousClass028.A0D(view, R.id.request_decline_button);
        View A0D2 = AnonymousClass028.A0D(view, R.id.request_pay_button);
        A0D.setVisibility(8);
        A0D2.setVisibility(8);
        AbstractC38041nQ A01 = this.A0B.A01(r7.A0G);
        if (A01 == null || (AFX = A01.AFX(r7.A0I)) == null) {
            view.setVisibility(8);
            return;
        }
        view.setVisibility(0);
        button.setVisibility(0);
        AnonymousClass17Y ABn = AFX.ABn();
        if (ABn != null) {
            if (!ABn.AdJ(r7.A0A)) {
                return;
            }
        } else if (this.A07.A0C()) {
            return;
        }
        button.setOnClickListener(new ViewOnClickCListenerShape0S0300000_I0(button, AFX, r7, 11));
    }

    public void A03(View view, Button button, AnonymousClass1IR r27, AbstractC38231nk r28, AnonymousClass1In r29, AbstractC15340mz r30, String str, boolean z) {
        AnonymousClass1IR A0L;
        int i;
        View A0D = AnonymousClass028.A0D(view, R.id.request_decline_button);
        View A0D2 = AnonymousClass028.A0D(view, R.id.request_pay_button);
        Context context = view.getContext();
        if (r30 == null) {
            A0L = null;
        } else {
            C17070qD r0 = this.A0B;
            r0.A03();
            A0L = r0.A08.A0L(r30.A0z.A01);
        }
        if (!A06(r27) && (A0L == null || A0L.A0A())) {
            int i2 = r27.A03;
            if (i2 == 20 && r27.A02 == 12) {
                A0D2.setEnabled(true);
                A0D2.setOnClickListener(new ViewOnClickCListenerShape0S1500000_I0(context, r27, r29, this, r30, str, 0));
                A0D.setEnabled(true);
                A0D.setOnClickListener(new ViewOnClickCListenerShape0S1500000_I0(context, r27, r29, this, r30, str, 1));
                view.setVisibility(0);
                if (r28 != null) {
                    r28.A03(context, view, r27);
                    return;
                }
                return;
            } else if (r28 != null && i2 == 40 && ((i = r27.A02) == 20 || i == 417 || i == 418)) {
                r28.A02(context, view, A0D2, A0D, button, r27, r29, str);
                return;
            }
        } else if (!z) {
            A0D2.setEnabled(false);
            A0D.setEnabled(false);
            view.setVisibility(0);
            return;
        }
        view.setVisibility(8);
    }

    public final void A04(View view, Button button, AnonymousClass1IR r13, AnonymousClass1In r14, boolean z) {
        UserJid userJid = r13.A0D;
        View A0D = AnonymousClass028.A0D(view, R.id.request_decline_button);
        View A0D2 = AnonymousClass028.A0D(view, R.id.request_pay_button);
        if (userJid == null || A06(r13) || r13.A0P) {
            view.setVisibility(8);
            return;
        }
        A0D.setVisibility(8);
        A0D2.setVisibility(8);
        view.setVisibility(8);
        if (z && button != null) {
            boolean z2 = false;
            view.setVisibility(0);
            button.setVisibility(0);
            if (r13.A02 == 19) {
                z2 = true;
            }
            A05(button, r13, z2, true);
            if (r13.A02 != 19) {
                button.setOnClickListener(new ViewOnClickCListenerShape0S0500000_I0(this, button, r14, r13, userJid, 0));
            }
        }
    }

    public final void A05(TextView textView, AnonymousClass1IR r12, boolean z, boolean z2) {
        int i;
        int i2;
        if (z) {
            textView.setText(R.string.payments_request_canceling);
            textView.setEnabled(false);
            i = r12.A02;
            i2 = 19;
        } else {
            textView.setEnabled(true);
            int i3 = R.string.cancel;
            if (z2) {
                i3 = R.string.payments_cancel_request;
            }
            textView.setText(i3);
            i = r12.A02;
            i2 = 12;
        }
        if (i != i2) {
            r12.A02 = i2;
            C17070qD r0 = this.A0B;
            r0.A03();
            r0.A08.A0k(r12, new AnonymousClass1IS(r12.A0C, r12.A0L, r12.A0Q), i, 0, -1);
        }
    }

    public final synchronized boolean A06(AnonymousClass1IR r6) {
        boolean z;
        AbstractC14640lm r1 = r6.A0C;
        z = false;
        if (r1 != null) {
            C15370n3 A01 = this.A02.A01(r1);
            if (A01.A0K()) {
                C15600nX r12 = this.A04;
                C15580nU r0 = (C15580nU) A01.A0B(C15580nU.class);
                AnonymousClass009.A05(r0);
                if (r12.A0C(r0)) {
                    C20710wC r13 = this.A06;
                    C15580nU r02 = (C15580nU) A01.A0B(C15580nU.class);
                    AnonymousClass009.A05(r02);
                    if (r13.A0Z(A01, r02)) {
                        z = true;
                    }
                }
            }
        }
        return z;
    }
}
