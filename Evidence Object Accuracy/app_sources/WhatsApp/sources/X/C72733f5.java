package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.CodeInputField;

/* renamed from: X.3f5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72733f5 extends AnimatorListenerAdapter {
    public final /* synthetic */ float A00;
    public final /* synthetic */ CodeInputField A01;

    public C72733f5(CodeInputField codeInputField, float f) {
        this.A01 = codeInputField;
        this.A00 = f;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A01.setX(this.A00);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.setX(this.A00);
    }
}
