package X;

import android.util.Base64;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.1uu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42251uu {
    public int A00 = 0;
    public boolean A01;
    public final AnonymousClass1JA A02;
    public final List A03 = new ArrayList();
    public final List A04;
    public final Set A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;

    public C42251uu(C42271uw r3) {
        this.A02 = r3.A05;
        this.A01 = r3.A02;
        this.A07 = r3.A03;
        this.A08 = r3.A04;
        this.A06 = r3.A01;
        this.A04 = r3.A06;
        this.A05 = r3.A07;
        AnonymousClass1JB r1 = r3.A00;
        this.A0A = r1.A01;
        this.A0G = r1.A07;
        this.A0H = r1.A08;
        this.A0F = r1.A06;
        this.A09 = r1.A00;
        this.A0B = r1.A02;
        this.A0E = r1.A05;
        this.A0C = r1.A03;
        this.A0D = r1.A04;
    }

    public static C42251uu A00(String str) {
        JSONObject jSONObject = new JSONObject(str);
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has("sync_jid_hash")) {
            JSONArray jSONArray = jSONObject.getJSONArray("sync_jid_hash");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(Base64.decode((String) jSONArray.get(i), 0));
            }
        }
        ArrayList arrayList2 = new ArrayList();
        if (jSONObject.has("sync_jid")) {
            JSONArray jSONArray2 = jSONObject.getJSONArray("sync_jid");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                UserJid nullable = UserJid.getNullable((String) jSONArray2.get(i2));
                if (nullable != null) {
                    arrayList2.add(nullable);
                }
            }
        }
        int i3 = jSONObject.getInt("sync_type_code");
        AnonymousClass1JA[] values = AnonymousClass1JA.values();
        for (AnonymousClass1JA r2 : values) {
            if (r2.code == i3) {
                C42271uw r3 = new C42271uw(r2);
                r3.A02 = jSONObject.getBoolean("sync_is_urgent");
                r3.A03 = jSONObject.getBoolean("sync_only_if_changed");
                r3.A04 = jSONObject.getBoolean("sync_only_if_registered");
                r3.A01 = jSONObject.getBoolean("sync_clear_whatsapp_sync_data");
                r3.A00 = new AnonymousClass1JB(jSONObject.optBoolean("sync_sidelist", true), jSONObject.optBoolean("sync_status", true), jSONObject.optBoolean("sync_picture", true), jSONObject.optBoolean("sync_business", true), jSONObject.optBoolean("sync_devices", true), jSONObject.optBoolean("sync_payment", true), jSONObject.optBoolean("sync_disappearing_mode", true), jSONObject.optBoolean("sync_lid", true), jSONObject.optBoolean("sync_contact", true));
                C42271uw.A00(r3, arrayList);
                r3.A07.addAll(arrayList2);
                C42251uu A01 = r3.A01();
                A01.A00 = jSONObject.getInt("sync_retry_count");
                if (jSONObject.getBoolean("sync_should_retry")) {
                    A01.A03.add(new C42701vg(true));
                }
                return A01;
            }
        }
        return null;
    }

    public String A01() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("sync_type_code", this.A02.code);
        jSONObject.put("sync_is_urgent", this.A01);
        jSONObject.put("sync_only_if_changed", this.A07);
        jSONObject.put("sync_only_if_registered", this.A08);
        jSONObject.put("sync_clear_whatsapp_sync_data", this.A06);
        jSONObject.put("sync_should_retry", A02());
        jSONObject.put("sync_retry_count", this.A00);
        jSONObject.put("sync_contact", this.A0A);
        jSONObject.put("sync_sidelist", this.A0G);
        jSONObject.put("sync_status", this.A0H);
        jSONObject.put("sync_picture", this.A0F);
        jSONObject.put("sync_disappearing_mode", this.A0C);
        jSONObject.put("sync_lid", this.A0D);
        List<byte[]> list = this.A04;
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (byte[] bArr : list) {
                arrayList.add(Base64.encodeToString(bArr, 0));
            }
            jSONObject.put("sync_jid_hash", new JSONArray((Collection) arrayList));
        }
        Set<Jid> set = this.A05;
        if (!set.isEmpty()) {
            ArrayList arrayList2 = new ArrayList();
            for (Jid jid : set) {
                arrayList2.add(jid.getRawString());
            }
            jSONObject.put("sync_jid", new JSONArray((Collection) arrayList2));
        }
        return jSONObject.toString();
    }

    public boolean A02() {
        for (C42701vg r0 : this.A03) {
            if (r0.A00) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        StringBuilder sb = new StringBuilder("SyncRequest, mode=");
        AnonymousClass1JA r1 = this.A02;
        sb.append(r1.mode.modeString);
        sb.append(", context=");
        sb.append(r1.context.contextString);
        sb.append(", protocols=");
        String str8 = "";
        if (this.A0A) {
            str = "C";
        } else {
            str = str8;
        }
        sb.append(str);
        if (this.A0G) {
            str2 = "I";
        } else {
            str2 = str8;
        }
        sb.append(str2);
        if (this.A0H) {
            str3 = "S";
        } else {
            str3 = str8;
        }
        sb.append(str3);
        if (this.A09) {
            str4 = "B";
        } else {
            str4 = str8;
        }
        sb.append(str4);
        if (this.A0B) {
            str5 = "D";
        } else {
            str5 = str8;
        }
        sb.append(str5);
        if (this.A0E) {
            str6 = "P";
        } else {
            str6 = str8;
        }
        sb.append(str6);
        if (this.A0C) {
            str7 = "M";
        } else {
            str7 = str8;
        }
        sb.append(str7);
        if (this.A0D) {
            str8 = "L";
        }
        sb.append(str8);
        return sb.toString();
    }
}
