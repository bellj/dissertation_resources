package X;

import java.io.OutputStream;

/* renamed from: X.5N8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N8 extends AnonymousClass1TP {
    public AnonymousClass5N8(OutputStream outputStream) {
        super(outputStream);
    }

    @Override // X.AnonymousClass1TP
    public AnonymousClass1TP A00() {
        return this;
    }

    @Override // X.AnonymousClass1TP
    public AnonymousClass5N8 A01() {
        return this;
    }

    @Override // X.AnonymousClass1TP
    public void A04(AnonymousClass1TL r2, boolean z) {
        r2.A06().A08(this, z);
    }
}
