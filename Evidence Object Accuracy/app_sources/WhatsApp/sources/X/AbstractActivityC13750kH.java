package X;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape1S0600000_I1;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.Conversation;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.conversation.ConversationListView;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.http.GoogleSearchDialogFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.polls.PollVoterViewModel;
import com.whatsapp.reactions.ReactionsTrayViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0kH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC13750kH extends AbstractActivityC13760kI implements AbstractC13890kV {
    public PopupWindow.OnDismissListener A00 = new C102514pG(this);
    public AbstractC009504t A01;
    public C16210od A02;
    public C16170oZ A03;
    public C14650lo A04;
    public C18850tA A05;
    public AnonymousClass116 A06;
    public C15550nR A07;
    public AnonymousClass190 A08;
    public C15610nY A09;
    public AnonymousClass10T A0A;
    public C21270x9 A0B;
    public AnonymousClass19K A0C;
    public AnonymousClass3DI A0D;
    public AnonymousClass19I A0E;
    public AnonymousClass1OX A0F;
    public C53242dY A0G;
    public AnonymousClass19D A0H;
    public AnonymousClass11P A0I;
    public C35451ht A0J;
    public C16590pI A0K;
    public C15890o4 A0L;
    public C15650ng A0M;
    public C15600nX A0N;
    public C253218y A0O;
    public C20000v3 A0P;
    public C20040v7 A0Q;
    public AnonymousClass12H A0R;
    public C242114q A0S;
    public AnonymousClass193 A0T;
    public C16120oU A0U;
    public C20710wC A0V;
    public AnonymousClass11G A0W;
    public AnonymousClass109 A0X;
    public C22370yy A0Y;
    public AnonymousClass13H A0Z;
    public C245115u A0a;
    public AnonymousClass19J A0b;
    public C22710zW A0c;
    public C17070qD A0d;
    public AbstractC15340mz A0e;
    public C239913u A0f;
    public ReactionsTrayViewModel A0g;
    public C240514a A0h;
    public AnonymousClass12F A0i;
    public C253018w A0j;
    public ViewTreeObserver$OnGlobalLayoutListenerC33691ev A0k;
    public AnonymousClass1AB A0l;
    public C252018m A0m;
    public AnonymousClass198 A0n;
    public C253118x A0o;
    public C26501Ds A0p;
    public AnonymousClass19L A0q;
    public HashMap A0r;
    public HashSet A0s = new HashSet();
    public Map A0t;
    public C30721Yo A0u;

    public C15270mq A2g() {
        if (this instanceof MediaAlbumActivity) {
            MediaAlbumActivity mediaAlbumActivity = (MediaAlbumActivity) this;
            C15270mq r2 = mediaAlbumActivity.A0D;
            if (r2 != null) {
                return r2;
            }
            C252718t r22 = ((ActivityC13790kL) mediaAlbumActivity).A0D;
            AbstractC15710nm r8 = ((ActivityC13810kN) mediaAlbumActivity).A03;
            AnonymousClass19M r14 = ((ActivityC13810kN) mediaAlbumActivity).A0B;
            C231510o r15 = mediaAlbumActivity.A0E;
            AnonymousClass01d r11 = ((ActivityC13810kN) mediaAlbumActivity).A08;
            AnonymousClass018 r13 = ((ActivityC13830kP) mediaAlbumActivity).A01;
            AnonymousClass193 r1 = mediaAlbumActivity.A0T;
            C15270mq r5 = new C15270mq(mediaAlbumActivity, null, r8, (KeyboardPopupLayout) ((ActivityC13810kN) mediaAlbumActivity).A00, new WaEditText(mediaAlbumActivity), r11, ((ActivityC13810kN) mediaAlbumActivity).A09, r13, r14, r15, r1, mediaAlbumActivity.A0H, r22);
            mediaAlbumActivity.A0D = r5;
            r5.setOnDismissListener(((AbstractActivityC13750kH) mediaAlbumActivity).A00);
            C15270mq r4 = mediaAlbumActivity.A0D;
            AnonymousClass19M r3 = ((ActivityC13810kN) mediaAlbumActivity).A0B;
            C231510o r23 = mediaAlbumActivity.A0E;
            C15330mx r7 = new C15330mx(mediaAlbumActivity, ((ActivityC13830kP) mediaAlbumActivity).A01, r3, r4, r23, (EmojiSearchContainer) AnonymousClass00T.A05(mediaAlbumActivity, R.id.emoji_search_container), mediaAlbumActivity.A0H);
            mediaAlbumActivity.A0F = r7;
            C15270mq r24 = mediaAlbumActivity.A0D;
            r24.A0E = new RunnableBRunnable0Shape5S0100000_I0_5(mediaAlbumActivity, 9);
            r24.A0C = r7;
            r7.A02.A0G = r24.A09.A01;
            r24.A0C(new C1096552n(mediaAlbumActivity));
            r7.A00 = new AbstractC14020ki() { // from class: X.56f
                @Override // X.AbstractC14020ki
                public final void APd(C37471mS r32) {
                    ReactionsTrayViewModel reactionsTrayViewModel = MediaAlbumActivity.this.A0g;
                    AnonymousClass009.A05(reactionsTrayViewModel);
                    reactionsTrayViewModel.A05(AbstractC36671kL.A06(r32.A00));
                }
            };
            return r24;
        } else if (!(this instanceof Conversation)) {
            return null;
        } else {
            return ((Conversation) this).A2k;
        }
    }

    public Collection A2h() {
        ArrayList arrayList = new ArrayList();
        C35451ht r0 = this.A0J;
        if (r0 != null) {
            arrayList.addAll(r0.A04.values());
        } else {
            AbstractC15340mz r1 = this.A0e;
            if (r1 != null) {
                C15650ng r02 = this.A0M;
                if (r02.A0K.A03(r1.A0z) != null) {
                    arrayList.add(this.A0e);
                    return arrayList;
                }
            }
        }
        return arrayList;
    }

    public void A2i() {
        if (this.A01 != null) {
            C35451ht r1 = this.A0J;
            if (r1 == null || r1.A04.size() == 0) {
                A2j();
                return;
            }
            if (r1.A04.size() > 1 && A2n()) {
                ReactionsTrayViewModel reactionsTrayViewModel = this.A0g;
                AnonymousClass009.A05(reactionsTrayViewModel);
                reactionsTrayViewModel.A04(0);
            }
            this.A01.A06();
        }
    }

    public void A2j() {
        AbstractC009504t r0 = this.A01;
        if (r0 != null) {
            r0.A05();
        }
        ReactionsTrayViewModel reactionsTrayViewModel = this.A0g;
        if (reactionsTrayViewModel != null) {
            reactionsTrayViewModel.A04(0);
        }
    }

    public void A2k() {
        MentionableEntry mentionableEntry;
        if ((this instanceof Conversation) && (mentionableEntry = ((Conversation) this).A2w) != null) {
            mentionableEntry.A03();
        }
    }

    public void A2l() {
        String str;
        if (this instanceof AbstractActivityC35431hr) {
            AbstractActivityC35431hr r1 = (AbstractActivityC35431hr) this;
            if (((AbstractActivityC13750kH) r1).A01 == null) {
                StringBuilder sb = new StringBuilder();
                if (!(r1 instanceof StarredMessagesActivity)) {
                    str = "kept";
                } else {
                    str = "starred";
                }
                sb.append(str);
                sb.append("/selectionrequested");
                Log.i(sb.toString());
                r1.A07.notifyDataSetChanged();
                C14830m7 r0 = ((ActivityC13790kL) r1).A05;
                C14850m9 r02 = ((ActivityC13810kN) r1).A0C;
                C14900mE r03 = ((ActivityC13810kN) r1).A05;
                C252718t r04 = ((ActivityC13790kL) r1).A0D;
                AnonymousClass13H r05 = r1.A0Z;
                C253018w r06 = r1.A0j;
                AbstractC15710nm r07 = ((ActivityC13810kN) r1).A03;
                C15570nT r08 = ((ActivityC13790kL) r1).A01;
                AbstractC14440lR r09 = ((ActivityC13830kP) r1).A05;
                AnonymousClass12U r010 = r1.A0L;
                C16120oU r011 = r1.A0U;
                AnonymousClass19M r012 = ((ActivityC13810kN) r1).A0B;
                C15450nH r013 = ((ActivityC13810kN) r1).A06;
                C18850tA r014 = ((AbstractActivityC13750kH) r1).A05;
                C16170oZ r015 = ((AbstractActivityC13750kH) r1).A03;
                C231510o r016 = r1.A0G;
                C88054Ec r017 = r1.A0K;
                AnonymousClass12P r018 = ((ActivityC13790kL) r1).A00;
                C15550nR r019 = ((AbstractActivityC13750kH) r1).A07;
                C22180yf r020 = r1.A0F;
                AnonymousClass01d r021 = ((ActivityC13810kN) r1).A08;
                C15610nY r022 = ((AbstractActivityC13750kH) r1).A09;
                AnonymousClass018 r023 = ((ActivityC13830kP) r1).A01;
                C20710wC r024 = r1.A0V;
                AnonymousClass12F r025 = r1.A0i;
                C253218y r026 = ((AbstractActivityC13750kH) r1).A0O;
                AnonymousClass12T r15 = r1.A02;
                AnonymousClass193 r14 = ((AbstractActivityC13750kH) r1).A0T;
                C242114q r13 = ((AbstractActivityC13750kH) r1).A0S;
                C23000zz r12 = r1.A0M;
                C22700zV r11 = r1.A05;
                C14820m6 r10 = ((ActivityC13810kN) r1).A09;
                C22370yy r9 = r1.A0Y;
                C22100yW r8 = r1.A0E;
                C255419u r7 = r1.A09;
                AnonymousClass109 r6 = r1.A0X;
                ((AbstractActivityC13750kH) r1).A01 = r1.A1W(new C60942z3(r018, r07, r03, r08, r013, r015, r1, r15, r014, r019, r11, r022, r1, r1.A08, r7, r021, r0, r10, r023, ((AbstractActivityC13750kH) r1).A0N, r026, r13, r8, r020, r012, r016, r14, r02, r011, r024, r6, r9, r05, r1.A0J, r017, r025, r06, r010, r12, r04, r09, r1.A0N));
            }
        } else if (!(this instanceof MediaAlbumActivity)) {
            Conversation conversation = (Conversation) this;
            if (((AbstractActivityC13750kH) conversation).A01 == null) {
                AbstractC009504t r027 = conversation.A0g;
                if (r027 != null) {
                    r027.A05();
                }
                Log.i("conversation/selectionrequested");
                conversation.A1g.setTranscriptMode(0);
                conversation.A1g.A02();
                C14830m7 r028 = ((ActivityC13790kL) conversation).A05;
                C14850m9 r029 = ((ActivityC13810kN) conversation).A0C;
                C14900mE r030 = ((ActivityC13810kN) conversation).A05;
                C252718t r031 = ((ActivityC13790kL) conversation).A0D;
                AnonymousClass13H r032 = ((AbstractActivityC13750kH) conversation).A0Z;
                C253018w r033 = ((AbstractActivityC13750kH) conversation).A0j;
                AbstractC15710nm r034 = ((ActivityC13810kN) conversation).A03;
                C15570nT r035 = ((ActivityC13790kL) conversation).A01;
                AbstractC14440lR r036 = ((ActivityC13830kP) conversation).A05;
                AnonymousClass12U r037 = conversation.A3U;
                C16120oU r038 = ((AbstractActivityC13750kH) conversation).A0U;
                AnonymousClass19M r039 = ((ActivityC13810kN) conversation).A0B;
                C15450nH r040 = ((ActivityC13810kN) conversation).A06;
                C18850tA r041 = ((AbstractActivityC13750kH) conversation).A05;
                C16170oZ r042 = ((AbstractActivityC13750kH) conversation).A03;
                C231510o r043 = conversation.A2g;
                C88054Ec r044 = conversation.A3T;
                AnonymousClass12P r045 = ((ActivityC13790kL) conversation).A00;
                C15550nR r046 = ((AbstractActivityC13750kH) conversation).A07;
                C22180yf r047 = conversation.A2e;
                AnonymousClass01d r048 = ((ActivityC13810kN) conversation).A08;
                C15610nY r049 = ((AbstractActivityC13750kH) conversation).A09;
                AnonymousClass018 r050 = conversation.A26;
                C20710wC r051 = ((AbstractActivityC13750kH) conversation).A0V;
                AnonymousClass12F r052 = ((AbstractActivityC13750kH) conversation).A0i;
                C253218y r053 = ((AbstractActivityC13750kH) conversation).A0O;
                AnonymousClass12T r152 = conversation.A1P;
                AnonymousClass193 r142 = ((AbstractActivityC13750kH) conversation).A0T;
                C242114q r132 = ((AbstractActivityC13750kH) conversation).A0S;
                C23000zz r122 = conversation.A3h;
                C22700zV r112 = conversation.A1Y;
                C14820m6 r102 = ((ActivityC13810kN) conversation).A09;
                C22370yy r92 = ((AbstractActivityC13750kH) conversation).A0Y;
                C22100yW r82 = conversation.A2d;
                C255419u r72 = conversation.A1x;
                AnonymousClass109 r62 = ((AbstractActivityC13750kH) conversation).A0X;
                ((AbstractActivityC13750kH) conversation).A01 = conversation.A1W(new AnonymousClass2z7(r045, conversation, r034, r030, r035, r040, r042, conversation, r152, r041, r046, r112, r049, conversation.A1v, r72, r048, r028, r102, r050, ((AbstractActivityC13750kH) conversation).A0N, r053, r132, r82, r047, r039, r043, r142, r029, r038, r051, r62, r92, r032, conversation.A3J, r044, r052, r033, r037, r122, r031, r036, conversation.A45));
                conversation.A1g.getConversationCursorAdapter().A06 = ((AbstractActivityC13750kH) conversation).A01;
            }
        } else {
            MediaAlbumActivity mediaAlbumActivity = (MediaAlbumActivity) this;
            if (((AbstractActivityC13750kH) mediaAlbumActivity).A01 == null) {
                Log.i("starred/selectionrequested");
                mediaAlbumActivity.A08.notifyDataSetChanged();
                C14830m7 r054 = ((ActivityC13790kL) mediaAlbumActivity).A05;
                C14850m9 r055 = ((ActivityC13810kN) mediaAlbumActivity).A0C;
                C14900mE r056 = ((ActivityC13810kN) mediaAlbumActivity).A05;
                C252718t r057 = ((ActivityC13790kL) mediaAlbumActivity).A0D;
                AnonymousClass13H r058 = mediaAlbumActivity.A0Z;
                C253018w r059 = mediaAlbumActivity.A0j;
                AbstractC15710nm r060 = ((ActivityC13810kN) mediaAlbumActivity).A03;
                C15570nT r061 = ((ActivityC13790kL) mediaAlbumActivity).A01;
                AbstractC14440lR r062 = ((ActivityC13830kP) mediaAlbumActivity).A05;
                AnonymousClass12U r063 = mediaAlbumActivity.A0J;
                C16120oU r064 = mediaAlbumActivity.A0U;
                AnonymousClass19M r065 = ((ActivityC13810kN) mediaAlbumActivity).A0B;
                C15450nH r066 = ((ActivityC13810kN) mediaAlbumActivity).A06;
                C18850tA r067 = ((AbstractActivityC13750kH) mediaAlbumActivity).A05;
                C16170oZ r068 = ((AbstractActivityC13750kH) mediaAlbumActivity).A03;
                C231510o r069 = mediaAlbumActivity.A0E;
                C88054Ec r070 = mediaAlbumActivity.A0I;
                AnonymousClass12P r071 = ((ActivityC13790kL) mediaAlbumActivity).A00;
                C15550nR r072 = ((AbstractActivityC13750kH) mediaAlbumActivity).A07;
                C22180yf r073 = mediaAlbumActivity.A0C;
                AnonymousClass01d r074 = ((ActivityC13810kN) mediaAlbumActivity).A08;
                C15610nY r075 = ((AbstractActivityC13750kH) mediaAlbumActivity).A09;
                AnonymousClass018 r076 = ((ActivityC13830kP) mediaAlbumActivity).A01;
                C20710wC r077 = mediaAlbumActivity.A0V;
                AnonymousClass12F r078 = mediaAlbumActivity.A0i;
                C253218y r079 = ((AbstractActivityC13750kH) mediaAlbumActivity).A0O;
                AnonymousClass12T r153 = mediaAlbumActivity.A02;
                AnonymousClass193 r143 = mediaAlbumActivity.A0T;
                C242114q r133 = ((AbstractActivityC13750kH) mediaAlbumActivity).A0S;
                C23000zz r123 = mediaAlbumActivity.A0K;
                C22700zV r113 = mediaAlbumActivity.A05;
                C14820m6 r103 = ((ActivityC13810kN) mediaAlbumActivity).A09;
                C22370yy r93 = mediaAlbumActivity.A0Y;
                C22100yW r83 = mediaAlbumActivity.A0B;
                C255419u r73 = mediaAlbumActivity.A0A;
                AnonymousClass109 r63 = mediaAlbumActivity.A0X;
                ((AbstractActivityC13750kH) mediaAlbumActivity).A01 = mediaAlbumActivity.A1W(new AnonymousClass2z5(r071, r060, r056, r061, r066, r068, mediaAlbumActivity, r153, r067, r072, r113, r075, mediaAlbumActivity, mediaAlbumActivity.A09, r73, r074, r054, r103, r076, ((AbstractActivityC13750kH) mediaAlbumActivity).A0N, r079, r133, r83, r073, r065, r069, r143, r055, r064, r077, r63, r93, r058, mediaAlbumActivity.A0H, r070, r078, r059, r063, r123, r057, r062, mediaAlbumActivity.A0L));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2m(int r20) {
        /*
        // Method dump skipped, instructions count: 518
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC13750kH.A2m(int):void");
    }

    public boolean A2n() {
        ReactionsTrayViewModel reactionsTrayViewModel = this.A0g;
        return reactionsTrayViewModel != null && ((Number) reactionsTrayViewModel.A09.A01()).intValue() == 2;
    }

    @Override // X.AbstractC13890kV
    public synchronized void A5t(AnonymousClass1IS r5) {
        Map map = this.A0t;
        if (map == null) {
            map = new HashMap();
            this.A0t = map;
        }
        map.put(r5, new AnonymousClass01T(0L, 0));
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void A5u(Drawable drawable, View view) {
        if (this instanceof Conversation) {
            Conversation conversation = (Conversation) this;
            ImageView imageView = new ImageView(conversation);
            imageView.setImageDrawable(drawable);
            imageView.setVisibility(4);
            imageView.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass3NY(view, imageView, conversation));
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(9);
            conversation.A10.addView(imageView, layoutParams);
        }
    }

    @Override // X.AbstractC13890kV
    public void A8w(AnonymousClass1IS r2) {
        Map map = this.A0t;
        if (map != null) {
            map.remove(r2);
        }
    }

    @Override // X.AbstractC13890kV
    public void AAC(AbstractC15340mz r8) {
        long j;
        int i;
        A2j();
        this.A0e = r8;
        C42641vY r3 = new C42641vY(this);
        r3.A06 = true;
        r3.A02 = r8.A0z.A00;
        byte b = r8.A0y;
        r3.A0R = new ArrayList(Collections.singleton(Integer.valueOf(Byte.valueOf(b).intValue())));
        if (r8 instanceof AnonymousClass1X2) {
            j = ((long) ((AbstractC16130oV) r8).A00) * 1000;
        } else {
            j = 0;
        }
        r3.A0L = Long.valueOf(j);
        if (b == 0) {
            String A0I = r8.A0I();
            AnonymousClass009.A05(A0I);
            i = A0I.length();
        } else {
            i = 0;
        }
        r3.A0J = Integer.valueOf(i);
        r3.A0H = Integer.valueOf(C30041Vv.A0v(r8) ? 1 : 0);
        r3.A09 = Boolean.valueOf(r8.A12(1));
        r3.A07 = Boolean.valueOf(r8.A12(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH));
        boolean z = false;
        if (r8.A05 >= 127) {
            z = true;
        }
        r3.A08 = Boolean.valueOf(z);
        startActivityForResult(r3.A00(), 2);
    }

    @Override // X.AbstractC13890kV
    public AnonymousClass3DI AAl() {
        return this.A0D;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ int ABe() {
        if (this instanceof AbstractActivityC35431hr) {
            return !(((AbstractActivityC35431hr) this) instanceof StarredMessagesActivity) ? 4 : 1;
        }
        if (!(this instanceof MediaAlbumActivity)) {
            return 0;
        }
        return 2;
    }

    @Override // X.AbstractC13890kV
    public C64533Fx ABj() {
        if (!(this instanceof AbstractActivityC35431hr)) {
            return this.A0E.A01;
        }
        return this.A0E.A00;
    }

    @Override // X.AbstractC13890kV
    public synchronized int ACM(AnonymousClass1X3 r8) {
        long j;
        int max;
        AnonymousClass01T r0;
        int i = ((AbstractC16130oV) r8).A00;
        if (i <= 1) {
            j = 600;
        } else {
            j = (long) (i * 1000);
        }
        max = Math.max(3, (int) Math.ceil(6000.0d / ((double) j)));
        Map map = this.A0t;
        if (!(map == null || (r0 = (AnonymousClass01T) map.get(r8.A0z)) == null)) {
            max -= ((Integer) r0.A01).intValue();
        }
        return max;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ PollVoterViewModel AFm() {
        if (!(this instanceof Conversation)) {
            return null;
        }
        return ((Conversation) this).A3I;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ ArrayList AGT() {
        if (this instanceof AbstractActivityC35431hr) {
            return ((AbstractActivityC35431hr) this).A0P;
        }
        if (!(this instanceof Conversation)) {
            return null;
        }
        return ((Conversation) this).A20.A0A;
    }

    @Override // X.AbstractC13900kW
    public AnonymousClass1AB AGx() {
        return this.A0l;
    }

    @Override // X.AbstractC13890kV
    public int AH8(AbstractC15340mz r4) {
        Number number;
        HashMap hashMap = this.A0r;
        if (hashMap == null || (number = (Number) hashMap.get(r4.A0z)) == null) {
            return 0;
        }
        return number.intValue();
    }

    @Override // X.AbstractC13890kV
    public boolean AIM() {
        return this.A0J != null;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AJl() {
        return (this instanceof Conversation) && ((Conversation) this).A20.A07 != null;
    }

    @Override // X.AbstractC13890kV
    public boolean AJm(AbstractC15340mz r3) {
        C35451ht r0 = this.A0J;
        if (r0 != null) {
            if (r0.A04.containsKey(r3.A0z)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AJv() {
        AbstractC14670lq r0;
        if (!(this instanceof Conversation) || (r0 = ((Conversation) this).A3y) == null || r0.A1G.A05.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AKC(AbstractC15340mz r4) {
        if (!(this instanceof Conversation)) {
            return false;
        }
        Conversation conversation = (Conversation) this;
        if (((AbstractActivityC13750kH) conversation).A0s.contains(r4.A0z)) {
            return false;
        }
        C15350n0 conversationCursorAdapter = conversation.A1g.getConversationCursorAdapter();
        if (conversationCursorAdapter.A03(r4) > conversationCursorAdapter.A01()) {
            return true;
        }
        return conversationCursorAdapter.A0Q.contains(r4);
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void AUy(AbstractC15340mz r18, boolean z) {
        boolean A0A;
        if (this instanceof Conversation) {
            Conversation conversation = (Conversation) this;
            MentionableEntry mentionableEntry = conversation.A2w;
            AbstractC009504t r1 = conversation.A0g;
            KeyboardPopupLayout keyboardPopupLayout = conversation.A10;
            ConversationListView conversationListView = conversation.A1g;
            ViewGroup viewGroup = conversation.A0R;
            ViewGroup viewGroup2 = conversation.A0P;
            ViewGroup viewGroup3 = conversation.A0Q;
            AnonymousClass19P r10 = conversation.A1r;
            AnonymousClass1AB r15 = ((AbstractActivityC13750kH) conversation).A0l;
            if (r18 != null && !conversation.A1y.A05() && mentionableEntry != null) {
                if (r1 != null) {
                    r1.A05();
                }
                conversation.Ach(r18);
                View view = conversation.A0N;
                if (view == null) {
                    view = new AnonymousClass34M(conversation, new RunnableBRunnable0Shape1S0600000_I1(conversation, viewGroup3, keyboardPopupLayout, conversationListView, viewGroup, viewGroup2, 4));
                    conversation.A0N = view;
                    viewGroup3.addView(view);
                }
                r10.A01(view, ((AbstractActivityC13750kH) conversation).A0F.A01(conversation), conversation.A2s, r18, r15, false);
                ConversationListView conversationListView2 = conversation.A1g;
                if (conversationListView2 == null) {
                    A0A = false;
                } else {
                    A0A = conversationListView2.A0A();
                }
                if (!z) {
                    viewGroup3.setVisibility(0);
                    return;
                }
                keyboardPopupLayout.setClipChildren(false);
                int transcriptMode = conversationListView.getTranscriptMode();
                if (A0A) {
                    conversationListView.setTranscriptMode(2);
                } else {
                    conversationListView.setTranscriptMode(0);
                }
                viewGroup3.setVisibility(0);
                viewGroup3.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC66353Ng(view, viewGroup3, viewGroup, conversationListView, keyboardPopupLayout, transcriptMode, A0A));
            }
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXC(AbstractC009504t r2) {
        super.AXC(r2);
        C41691tw.A02(this, R.color.primary);
    }

    @Override // X.ActivityC13810kN, X.ActivityC000800j, X.AbstractC002200x
    public void AXD(AbstractC009504t r2) {
        super.AXD(r2);
        C41691tw.A02(this, R.color.action_mode_dark);
    }

    @Override // X.AbstractC13890kV
    public void AbN(AbstractC15340mz r2) {
        GoogleSearchDialogFragment.A00(this, ((ActivityC13810kN) this).A06, r2);
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ void Ach(AbstractC15340mz r2) {
        if (this instanceof Conversation) {
            ((Conversation) this).A20.A0B(r2);
        }
    }

    @Override // X.AbstractC13890kV
    public void Acq(List list, boolean z) {
        if (this.A0J != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AbstractC15340mz r2 = (AbstractC15340mz) it.next();
                C35451ht r0 = this.A0J;
                AnonymousClass1IS r1 = r2.A0z;
                HashMap hashMap = r0.A04;
                if (z) {
                    hashMap.put(r1, r2);
                } else {
                    hashMap.remove(r1);
                }
            }
            A2i();
        }
    }

    @Override // X.AbstractC13890kV
    public void Ad0(AbstractC15340mz r4, int i) {
        HashMap hashMap = this.A0r;
        if (hashMap == null) {
            hashMap = new HashMap();
            this.A0r = hashMap;
        }
        hashMap.put(r4.A0z, Integer.valueOf(i));
    }

    @Override // X.AbstractC13890kV
    public synchronized boolean AdM(AnonymousClass1IS r8) {
        boolean z;
        AnonymousClass01T r1;
        Map map = this.A0t;
        z = false;
        if (!(map == null || (r1 = (AnonymousClass01T) map.get(r8)) == null)) {
            long longValue = ((Long) r1.A00).longValue();
            int intValue = ((Integer) r1.A01).intValue();
            if (longValue < 6000 || intValue < 3) {
                z = true;
            }
        }
        return z;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean AdX() {
        return this instanceof Conversation;
    }

    @Override // X.AbstractC13890kV
    public /* synthetic */ boolean Adn() {
        return this instanceof Conversation;
    }

    @Override // X.AbstractC13890kV
    public void AeE(AbstractC15340mz r6) {
        C14900mE r4 = ((ActivityC13810kN) this).A05;
        AnonymousClass12H r3 = this.A0R;
        C35451ht r0 = new C35451ht(r4, new AnonymousClass55N(this), this.A0J, r3);
        this.A0J = r0;
        r0.A04.put(r6.A0z, r6);
        A2l();
        A2i();
    }

    @Override // X.AbstractC13890kV
    public boolean Af1(AbstractC15340mz r5) {
        C35451ht r0 = this.A0J;
        boolean z = false;
        if (r0 != null) {
            AnonymousClass1IS r2 = r5.A0z;
            boolean containsKey = r0.A04.containsKey(r2);
            HashMap hashMap = this.A0J.A04;
            if (containsKey) {
                hashMap.remove(r2);
            } else {
                hashMap.put(r2, r5);
                z = true;
            }
            A2i();
        }
        return z;
    }

    @Override // X.AbstractC13890kV
    public void AfW(AnonymousClass1X3 r8, long j) {
        long j2;
        AnonymousClass01T r3;
        AnonymousClass1IS r4 = r8.A0z;
        int i = ((AbstractC16130oV) r8).A00;
        if (i <= 1) {
            j2 = 600;
        } else {
            j2 = (long) (i * 1000);
        }
        int i2 = (int) (j / j2);
        synchronized (this) {
            Map map = this.A0t;
            if (!(map == null || (r3 = (AnonymousClass01T) map.get(r4)) == null)) {
                long longValue = ((Long) r3.A00).longValue() + j;
                int intValue = ((Integer) r3.A01).intValue();
                if (i2 > 0) {
                    intValue += i2;
                }
                this.A0t.put(r4, new AnonymousClass01T(Long.valueOf(longValue), Integer.valueOf(intValue)));
            }
        }
    }

    @Override // X.AbstractC13890kV
    public void AfZ(AbstractC15340mz r3) {
        this.A0s.add(r3.A0z);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        List<C30741Yq> list;
        super.onActivityResult(i, i2, intent);
        if (i == 41) {
            if (i2 == -1) {
                if (intent != null) {
                    Uri data = intent.getData();
                    AnonymousClass009.A05(data);
                    str = data.getLastPathSegment();
                } else {
                    str = null;
                }
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                C30721Yo r0 = this.A0u;
                if (!(r0 == null || (list = r0.A05) == null)) {
                    for (C30741Yq r1 : list) {
                        arrayList2.add(r1.A02);
                        UserJid userJid = r1.A01;
                        if (userJid != null) {
                            arrayList.add(userJid);
                        } else {
                            arrayList.add(null);
                        }
                    }
                    this.A08.A02(this.A0u.A08(), str, arrayList2, arrayList);
                }
            }
            this.A0n.A00();
        }
    }

    @Override // X.ActivityC13810kN, X.ActivityC13830kP, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        int i;
        super.onConfigurationChanged(configuration);
        this.A0E.A00(this);
        ReactionsTrayViewModel reactionsTrayViewModel = this.A0g;
        if (reactionsTrayViewModel != null && (i = configuration.orientation) != reactionsTrayViewModel.A01) {
            reactionsTrayViewModel.A01 = i;
            reactionsTrayViewModel.A04(0);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A0D = new AnonymousClass3DI(((ActivityC13810kN) this).A05, ((ActivityC13810kN) this).A09, this.A0c, this.A0d);
        C16590pI r15 = this.A0K;
        C26501Ds r3 = this.A0p;
        C15550nR r13 = this.A07;
        AnonymousClass018 r2 = ((ActivityC13830kP) this).A01;
        C39091pH r11 = new C39091pH(this.A04, r13, this.A0A, r15, r2, r3, C39091pH.A00(((ActivityC13830kP) this).A05));
        C14850m9 r7 = ((ActivityC13810kN) this).A0C;
        AbstractC14440lR r10 = ((ActivityC13830kP) this).A05;
        C15450nH r32 = ((ActivityC13810kN) this).A06;
        C21270x9 r4 = this.A0B;
        this.A0F = new AnonymousClass1OX(A0V(), r32, r4, this.A0H, this.A0I, r7, this.A0f, this.A0l, r10, r11);
        this.A0E.A00(this);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        AnonymousClass12P r5;
        C252018m r4;
        C14820m6 r3;
        boolean z;
        C14850m9 r1;
        AnonymousClass12P r52;
        C252018m r42;
        C14820m6 r32;
        boolean z2;
        C14850m9 r12;
        if (i != 13) {
            if (i != 19) {
                switch (i) {
                    case 23:
                        r12 = ((ActivityC13810kN) this).A0C;
                        r52 = ((ActivityC13790kL) this).A00;
                        r42 = this.A0m;
                        r32 = ((ActivityC13810kN) this).A09;
                        z2 = true;
                        break;
                    case 24:
                        r12 = ((ActivityC13810kN) this).A0C;
                        r52 = ((ActivityC13790kL) this).A00;
                        r42 = this.A0m;
                        r32 = ((ActivityC13810kN) this).A09;
                        z2 = false;
                        break;
                    case 25:
                        r1 = ((ActivityC13810kN) this).A0C;
                        r5 = ((ActivityC13790kL) this).A00;
                        r4 = this.A0m;
                        r3 = ((ActivityC13810kN) this).A09;
                        z = true;
                        break;
                    default:
                        return super.onCreateDialog(i);
                }
                return AnonymousClass3AJ.A00(this, r52, new AnonymousClass3UN(this, r32, i, r12.A07(1333)), r42, z2);
            }
            r1 = ((ActivityC13810kN) this).A0C;
            r5 = ((ActivityC13790kL) this).A00;
            r4 = this.A0m;
            r3 = ((ActivityC13810kN) this).A09;
            z = false;
            return AnonymousClass3AJ.A00(this, r5, new AnonymousClass3UM(this, r3, i, r1.A07(1333)), r4, z);
        }
        C35451ht r2 = this.A0J;
        if (r2 == null || r2.A04.isEmpty()) {
            Log.e("conversation/dialog/delete no messages");
            return super.onCreateDialog(i);
        }
        StringBuilder sb = new StringBuilder("conversation/dialog/delete/");
        sb.append(r2.A04.size());
        Log.i(sb.toString());
        C14850m9 r0 = ((ActivityC13810kN) this).A0C;
        C14900mE r02 = ((ActivityC13810kN) this).A05;
        C14830m7 r03 = ((ActivityC13790kL) this).A05;
        AbstractC14440lR r04 = ((ActivityC13830kP) this).A05;
        AnonymousClass19M r05 = ((ActivityC13810kN) this).A0B;
        C16170oZ r15 = this.A03;
        C15550nR r122 = this.A07;
        C15610nY r11 = this.A09;
        AnonymousClass018 r10 = ((ActivityC13830kP) this).A01;
        C20710wC r9 = this.A0V;
        AnonymousClass11G r8 = this.A0W;
        C14820m6 r7 = ((ActivityC13810kN) this).A09;
        C15600nX r6 = this.A0N;
        AnonymousClass19J r53 = this.A0b;
        HashSet hashSet = new HashSet(this.A0J.A04.values());
        AnonymousClass52V r33 = new AbstractC35511i9() { // from class: X.52V
            @Override // X.AbstractC35511i9
            public final void AOx() {
                AbstractActivityC13750kH.this.A2j();
            }
        };
        HashSet hashSet2 = new HashSet();
        C35451ht r06 = this.A0J;
        if (r06 != null) {
            hashSet2.addAll(r06.A04.values());
        }
        return C64993Hs.A00(this, new AnonymousClass52T(this, 13), r33, r02, r15, r122, r11, new AnonymousClass4Q5(this, null, hashSet2), r03, r7, r10, r6, r05, r0, r9, r8, r53, r04, C64993Hs.A01(this, r122, r11, null, hashSet), hashSet, true);
    }

    @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        C35451ht r0 = this.A0J;
        if (r0 != null) {
            r0.A00();
            this.A0J = null;
        }
        AnonymousClass1OX r1 = this.A0F;
        AnonymousClass1J1 r02 = r1.A00;
        if (r02 != null) {
            r02.A00();
        }
        AnonymousClass1AB r03 = r1.A01;
        if (r03 != null) {
            r03.A03();
        }
        C39091pH r04 = r1.A0B;
        if (r04 != null) {
            r04.A06();
        }
        AnonymousClass3DI r2 = this.A0D;
        AnonymousClass398 r12 = r2.A00;
        if (r12 != null) {
            r12.A04 = true;
            r12.interrupt();
            r2.A00 = null;
        }
        this.A0C.A00.clear();
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        synchronized (this) {
            Map map = this.A0t;
            if (map != null) {
                map.clear();
            }
        }
        this.A0q.A00();
    }

    @Override // X.ActivityC13770kJ, android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle != null) {
            List<AnonymousClass1IS> A04 = C38211ni.A04(bundle);
            if (A04 != null) {
                C14900mE r4 = ((ActivityC13810kN) this).A05;
                AnonymousClass12H r3 = this.A0R;
                this.A0J = new C35451ht(r4, new AnonymousClass55N(this), this.A0J, r3);
                for (AnonymousClass1IS r2 : A04) {
                    AbstractC15340mz A03 = this.A0M.A0K.A03(r2);
                    if (A03 != null) {
                        this.A0J.A04.put(r2, A03);
                    }
                }
                A2l();
                A2i();
            }
            AnonymousClass1IS A032 = C38211ni.A03(bundle, "");
            if (A032 != null) {
                this.A0e = this.A0M.A0K.A03(A032);
            }
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        C35451ht r0 = this.A0J;
        if (r0 != null) {
            ArrayList arrayList = new ArrayList();
            for (AbstractC15340mz r02 : r0.A04.values()) {
                arrayList.add(r02.A0z);
            }
            C38211ni.A09(bundle, arrayList);
        }
        AbstractC15340mz r03 = this.A0e;
        if (r03 != null) {
            C38211ni.A08(bundle, r03.A0z, "");
        }
    }
}
