package X;

import com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity;

/* renamed from: X.5oU  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oU extends AbstractC16350or {
    public final /* synthetic */ IndiaUpiDeviceBindStepActivity A00;
    public final /* synthetic */ String A01;

    public AnonymousClass5oU(IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity, String str) {
        this.A00 = indiaUpiDeviceBindStepActivity;
        this.A01 = str;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity = this.A00;
        if (indiaUpiDeviceBindStepActivity.A0C.A07.contains("upi-get-challenge") || ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B.A05().A00 != null) {
            return null;
        }
        indiaUpiDeviceBindStepActivity.A0C.A03("upi-get-challenge");
        C16590pI r15 = ((AbstractActivityC121685jC) indiaUpiDeviceBindStepActivity).A07;
        C14900mE r14 = ((ActivityC13810kN) indiaUpiDeviceBindStepActivity).A05;
        AbstractC15710nm r13 = ((ActivityC13810kN) indiaUpiDeviceBindStepActivity).A03;
        C15570nT r12 = ((ActivityC13790kL) indiaUpiDeviceBindStepActivity).A01;
        AbstractC14440lR r11 = ((ActivityC13830kP) indiaUpiDeviceBindStepActivity).A05;
        C64513Fv r10 = indiaUpiDeviceBindStepActivity.A0C;
        C17220qS r9 = ((AbstractActivityC121685jC) indiaUpiDeviceBindStepActivity).A0H;
        C18590sh r8 = indiaUpiDeviceBindStepActivity.A0M;
        C18600si r7 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0C;
        C130165yu r6 = indiaUpiDeviceBindStepActivity.A0R;
        C18610sj r5 = ((AbstractActivityC121685jC) indiaUpiDeviceBindStepActivity).A0M;
        AnonymousClass162 r4 = indiaUpiDeviceBindStepActivity.A0D;
        AbstractC17860rW r3 = ((AbstractActivityC121685jC) indiaUpiDeviceBindStepActivity).A0L;
        AnonymousClass6BE r2 = ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0D;
        new C129315xW(r13, r14, r12, r15, r9, ((AbstractActivityC121665jA) indiaUpiDeviceBindStepActivity).A0B, IndiaUpiDeviceBindStepActivity.A0X, ((AbstractActivityC121685jC) indiaUpiDeviceBindStepActivity).A0K, r10, r3, r7, r5, r4, r2, r8, r11, r6).A00();
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        this.A00.A3B(this.A01);
    }
}
