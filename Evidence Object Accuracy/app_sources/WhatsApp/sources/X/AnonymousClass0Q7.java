package X;

import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.0Q7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Q7 {
    public int A00 = -1;
    public int A01 = 0;
    public AnonymousClass0QC A02;
    public AnonymousClass0Q7 A03;
    public HashSet A04 = null;
    public final AnonymousClass0JZ A05;
    public final AnonymousClass0QV A06;

    public AnonymousClass0Q7(AnonymousClass0JZ r2, AnonymousClass0QV r3) {
        this.A06 = r3;
        this.A05 = r2;
    }

    public int A00() {
        AnonymousClass0Q7 r0;
        if (this.A06.A0N == 8) {
            return 0;
        }
        int i = this.A00;
        if (i <= -1 || (r0 = this.A03) == null || r0.A06.A0N != 8) {
            return this.A01;
        }
        return i;
    }

    public void A01() {
        HashSet hashSet;
        AnonymousClass0Q7 r0 = this.A03;
        if (!(r0 == null || (hashSet = r0.A04) == null)) {
            hashSet.remove(this);
        }
        this.A03 = null;
        this.A01 = 0;
        this.A00 = -1;
    }

    public void A02() {
        AnonymousClass0QC r0 = this.A02;
        if (r0 == null) {
            this.A02 = new AnonymousClass0QC(AnonymousClass0JQ.UNRESTRICTED);
        } else {
            r0.A00();
        }
    }

    public boolean A03() {
        AnonymousClass0Q7 r0;
        HashSet hashSet = this.A04;
        if (hashSet != null) {
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                AnonymousClass0Q7 r2 = (AnonymousClass0Q7) it.next();
                AnonymousClass0JZ r1 = r2.A05;
                switch (r1.ordinal()) {
                    case 0:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        r0 = null;
                        break;
                    case 1:
                        r0 = r2.A06.A0X;
                        break;
                    case 2:
                        r0 = r2.A06.A0S;
                        break;
                    case 3:
                        r0 = r2.A06.A0W;
                        break;
                    case 4:
                        r0 = r2.A06.A0Y;
                        break;
                    default:
                        throw new AssertionError(r1.name());
                }
                if (r0.A03 != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A06.A0f);
        sb.append(":");
        sb.append(this.A05.toString());
        return sb.toString();
    }
}
