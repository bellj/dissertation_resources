package X;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape1S0100100_I1;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.2BV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2BV implements AnonymousClass2BW, AnonymousClass2BZ {
    public long A00;
    public long A01;
    public long A02;
    public RandomAccessFile A03;
    public boolean A04;
    public final Handler A05 = new Handler(Looper.getMainLooper());
    public final C15610nY A06;
    public final C16590pI A07;
    public final C14950mJ A08;
    public final C20830wO A09;
    public final AnonymousClass1X4 A0A;
    public final AnonymousClass2Ba A0B;
    public final C47522Bb A0C;

    @Override // X.AnonymousClass2BW
    public void A5p(AnonymousClass5QP r1) {
    }

    @Override // X.AnonymousClass2BZ
    public void ANU(AnonymousClass2Ba r1, long j) {
    }

    @Override // X.AnonymousClass2BZ
    public void APR(int i) {
    }

    @Override // X.AnonymousClass2BZ
    public void AV6() {
    }

    public AnonymousClass2BV(C15610nY r3, C16590pI r4, C14950mJ r5, C20830wO r6, AnonymousClass1X4 r7, AnonymousClass2Ba r8, C47522Bb r9) {
        this.A07 = r4;
        this.A08 = r5;
        this.A06 = r3;
        this.A09 = r6;
        this.A0A = r7;
        this.A0B = r8;
        this.A0C = r9;
    }

    @Override // X.AnonymousClass2BW
    public /* synthetic */ Map AGF() {
        return Collections.emptyMap();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return Uri.fromFile(this.A0B.A02());
    }

    @Override // X.AnonymousClass2BZ
    public void APS(AnonymousClass2Ba r4) {
        this.A05.post(new RunnableBRunnable0Shape4S0200000_I0_4(this, 49, r4));
    }

    @Override // X.AnonymousClass2BZ
    public void AQT(AnonymousClass2Ba r6) {
        RandomAccessFile randomAccessFile;
        long filePointer;
        File A02 = this.A0B.A02();
        if (this.A04 && (randomAccessFile = this.A03) != null) {
            try {
                filePointer = randomAccessFile.getFilePointer();
            } catch (IOException e) {
                Log.e(e);
            }
            try {
                this.A03.close();
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(A02, "r");
                this.A03 = randomAccessFile2;
                randomAccessFile2.seek(filePointer);
            } catch (IOException e2) {
                Log.e(e2);
                Log.e("WhatsappChunkAwareDataSource/hotswap failed");
            } finally {
                this.A03 = null;
            }
        }
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r10) {
        this.A00 = 0;
        long j = r10.A03;
        this.A02 = j;
        AnonymousClass2Ba r8 = this.A0B;
        synchronized (r8) {
            r8.A0F.add(this);
        }
        C91024Qd r1 = r8.A0E;
        long j2 = this.A02;
        Handler handler = r1.A02;
        handler.removeCallbacks(r1.A01);
        RunnableBRunnable0Shape1S0100100_I1 runnableBRunnable0Shape1S0100100_I1 = new RunnableBRunnable0Shape1S0100100_I1(r1, j2, 2);
        r1.A01 = runnableBRunnable0Shape1S0100100_I1;
        handler.postDelayed(runnableBRunnable0Shape1S0100100_I1, 200);
        long A01 = r8.A01() - j;
        this.A01 = A01;
        return A01;
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        RandomAccessFile randomAccessFile = this.A03;
        if (randomAccessFile != null) {
            try {
                try {
                    randomAccessFile.close();
                } catch (IOException e) {
                    throw new C868248z(e);
                }
            } finally {
                this.A03 = null;
                if (this.A04) {
                    this.A04 = false;
                }
            }
        }
        AnonymousClass2Ba r1 = this.A0B;
        synchronized (r1) {
            r1.A0F.remove(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x009d, code lost:
        if (r0 == -1) goto L_0x009f;
     */
    @Override // X.AnonymousClass2BY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read(byte[] r12, int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 217
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2BV.read(byte[], int, int):int");
    }
}
