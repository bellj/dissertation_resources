package X;

/* renamed from: X.3FD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FD {
    public AnonymousClass3IQ A00;
    public C65243It A01;
    public boolean A02;
    public final C71183cW A03;

    public AnonymousClass3FD(C71183cW r3) {
        int i = r3.A00;
        if (i < 21 || (i & 3) != 1) {
            throw C82573vq.A00();
        }
        this.A03 = r3;
    }

    public final int A00(int i, int i2, int i3) {
        boolean z = this.A02;
        C71183cW r0 = this.A03;
        boolean A03 = z ? r0.A03(i2, i) : r0.A03(i, i2);
        int i4 = i3 << 1;
        return A03 ? i4 | 1 : i4;
    }

    public AnonymousClass3IQ A01() {
        AnonymousClass3IQ r0 = this.A00;
        if (r0 == null) {
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < 6; i3++) {
                i2 = A00(i3, 8, i2);
            }
            int A00 = A00(8, 7, A00(8, 8, A00(7, 8, i2)));
            int i4 = 5;
            do {
                A00 = A00(8, i4, A00);
                i4--;
            } while (i4 >= 0);
            int i5 = this.A03.A00;
            int i6 = i5 - 7;
            for (int i7 = i5 - 1; i7 >= i6; i7--) {
                i = A00(8, i7, i);
            }
            for (int i8 = i5 - 8; i8 < i5; i8++) {
                i = A00(i8, 8, i);
            }
            r0 = AnonymousClass3IQ.A00(A00, i);
            if (r0 == null) {
                r0 = AnonymousClass3IQ.A00(A00 ^ 21522, i ^ 21522);
            }
            this.A00 = r0;
            if (r0 == null) {
                throw C82573vq.A00();
            }
        }
        return r0;
    }

    public C65243It A02() {
        int i;
        C65243It r0 = this.A01;
        if (r0 != null) {
            return r0;
        }
        int i2 = this.A03.A00;
        int i3 = (i2 - 17) >> 2;
        if (i3 <= 6) {
            return C65243It.A01(i3);
        }
        int i4 = i2 - 11;
        int i5 = 5;
        int i6 = 0;
        int i7 = 5;
        int i8 = 0;
        do {
            i = i2 - 9;
            while (i >= i4) {
                i8 = A00(i, i7, i8);
                i--;
            }
            i7--;
        } while (i7 >= 0);
        C65243It A00 = C65243It.A00(i8);
        if (A00 == null || (A00.A01 << 2) + 17 != i2) {
            do {
                for (int i9 = i; i9 >= i4; i9--) {
                    i6 = A00(i5, i9, i6);
                }
                i5--;
            } while (i5 >= 0);
            A00 = C65243It.A00(i6);
            if (A00 == null || (A00.A01 << 2) + 17 != i2) {
                throw C82573vq.A00();
            }
        }
        this.A01 = A00;
        return A00;
    }
}
