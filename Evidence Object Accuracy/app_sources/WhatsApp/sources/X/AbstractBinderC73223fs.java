package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;

/* renamed from: X.3fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73223fs extends Binder implements IInterface {
    public AbstractBinderC73223fs() {
        attachInterface(this, "com.google.android.gms.clearcut.internal.IClearcutLoggerCallbacks");
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this;
    }

    @Override // android.os.Binder
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        AbstractBinderC79213qE r2 = (AbstractBinderC79213qE) this;
        switch (i) {
            case 1:
                ((BinderC79513qi) r2).A00.A05((Status) C12970iu.A0F(parcel, Status.CREATOR));
                return true;
            case 2:
                C12970iu.A0F(parcel, Status.CREATOR);
                throw C12970iu.A0z();
            case 3:
                C12970iu.A0F(parcel, Status.CREATOR);
                parcel.readLong();
                throw C12970iu.A0z();
            case 4:
                C12970iu.A0F(parcel, Status.CREATOR);
                throw C12970iu.A0z();
            case 5:
                C12970iu.A0F(parcel, Status.CREATOR);
                parcel.readLong();
                throw C12970iu.A0z();
            case 6:
                C12970iu.A0F(parcel, Status.CREATOR);
                parcel.createTypedArray(C78613pC.CREATOR);
                throw C12970iu.A0z();
            case 7:
                C12970iu.A0F(parcel, DataHolder.CREATOR);
                throw C12970iu.A0z();
            case 8:
                C12970iu.A0F(parcel, Status.CREATOR);
                C12970iu.A0F(parcel, C78493p0.CREATOR);
                throw C12970iu.A0z();
            case 9:
                C12970iu.A0F(parcel, Status.CREATOR);
                C12970iu.A0F(parcel, C78493p0.CREATOR);
                throw C12970iu.A0z();
            default:
                return false;
        }
    }
}
