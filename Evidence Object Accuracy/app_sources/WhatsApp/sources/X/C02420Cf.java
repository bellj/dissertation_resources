package X;

/* renamed from: X.0Cf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02420Cf extends AbstractC10380eX {
    public C02420Cf(AnonymousClass05U r1, AnonymousClass05U r2) {
        super(r1, r2);
    }

    @Override // X.AbstractC10380eX
    public AnonymousClass05U A00(AnonymousClass05U r2) {
        return r2.A01;
    }

    @Override // X.AbstractC10380eX
    public AnonymousClass05U A01(AnonymousClass05U r2) {
        return r2.A00;
    }
}
