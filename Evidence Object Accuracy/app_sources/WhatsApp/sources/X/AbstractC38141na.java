package X;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.1na  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC38141na {
    boolean A6y();

    boolean A6z(UserJid userJid);

    Intent AAY(AbstractC15340mz v);

    int ADZ();

    AnonymousClass4S0 ADa();

    C69973aX ADb(C16590pI v, C22590zK v2, AbstractC14440lR v3);

    DialogFragment AFK(String str, ArrayList arrayList, boolean z, boolean z2);

    String AFM(Context context, String str, boolean z);

    int AFW();

    boolean AIF();
}
