package X;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1To  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29561To extends SQLiteOpenHelper implements AbstractC16300om {
    public static final String[] A0B = {"messages", "messages_fts", "messages_links", "quoted_message_order", "quoted_message_product", "messages_quotes", "messages_vcards", "messages_vcards_jids", "pay_transactions", "messages_quotes_payment_invite_legacy"};
    public C16330op A00;
    public Integer A01;
    public boolean A02;
    public final C29621Ty A03;
    public final C29691Ug A04;
    public final C231410n A05;
    public final AnonymousClass1I2 A06;
    public final File A07;
    public final Object A08 = new Object();
    public volatile Boolean A09 = null;
    public volatile Boolean A0A = null;

    public C29561To(Context context, C29621Ty r5, C29691Ug r6, C231410n r7, File file, Set set) {
        super(context, "msgstore.db", (SQLiteDatabase.CursorFactory) null, 1);
        this.A05 = r7;
        this.A03 = r5;
        this.A07 = file;
        this.A04 = r6;
        this.A06 = new AnonymousClass1I2(new C002601e(set, null));
    }

    public static Pair A00(String str, String str2, String str3) {
        String lowerCase = String.format("%s_bd_for_%s_trigger", str, str2).toLowerCase(Locale.getDefault());
        return new Pair(lowerCase, String.format("CREATE TRIGGER %s BEFORE DELETE ON %s BEGIN DELETE FROM %s WHERE %s; END", lowerCase, str, str2, str3));
    }

    public static Pair A01(String str, boolean z) {
        String str2;
        if (!z) {
            str2 = "messages";
        } else {
            str2 = "message";
        }
        return A00(str2, str, "message_row_id=old._id");
    }

    public static String A02(C16330op r3, String str, String str2) {
        Cursor A09 = r3.A09("SELECT value FROM props WHERE key = ?", new String[]{str});
        try {
            if (A09.moveToNext()) {
                String string = A09.getString(0);
                A09.close();
                return string;
            }
            A09.close();
            return str2;
        } catch (Throwable th) {
            if (A09 != null) {
                try {
                    A09.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }

    public static void A03(C16330op r4, String str) {
        ContentValues contentValues = new ContentValues(2);
        contentValues.put("key", "msgtore_db_schema_version");
        contentValues.put("value", str);
        C29721Uk.A00("DatabaseHelper", "setProp", "REPLACE_PROPS");
        r4.A05(contentValues, "props");
    }

    public static void A04(C16330op r2, String str, String str2) {
        if (TextUtils.isEmpty(AnonymousClass1Uj.A00(r2, "table", str))) {
            StringBuilder sb = new StringBuilder("CREATE_");
            sb.append(str);
            sb.toString();
            r2.A0B(str2);
        }
    }

    public static boolean A05(C16330op r3) {
        try {
            if (Integer.parseInt(A02(r3, "migration_completed", String.valueOf(0))) == 1) {
                return true;
            }
            return false;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    public final Boolean A06(C16330op r2) {
        if (this.A09 == null) {
            boolean z = false;
            if (r2 != null) {
                z = true;
            }
            AnonymousClass009.A0E(z);
            this.A09 = Boolean.valueOf(A05(r2));
        }
        return this.A09;
    }

    public final void A07() {
        int i;
        String str;
        boolean equals;
        String str2;
        C16330op r3 = this.A00;
        if (r3 != null) {
            C28181Ma r2 = new C28181Ma("databasehelper/prepareWritableDatabase");
            try {
                i = Integer.parseInt(A02(r3, "chat_ready", String.valueOf(0)));
            } catch (NumberFormatException unused) {
                i = 0;
            }
            if (i == 2) {
                AnonymousClass1Uj.A01(this.A00, "DatabaseHelper", "chat_list");
            }
            C16330op r32 = this.A00;
            if (this.A04.A00) {
                equals = false;
            } else {
                String str3 = "";
                if (!TextUtils.isEmpty(AnonymousClass1Uj.A00(r32, "table", "props"))) {
                    str3 = A02(r32, "msgtore_db_schema_version", str3);
                }
                if (A06(this.A00).booleanValue()) {
                    str = "152111e1de35706b2ad0b608f47ecc77";
                } else {
                    str = "c8d3b444a3012f678e1974073781ef3e";
                }
                equals = str3.equals(str);
            }
            if (!equals) {
                this.A00.A00.beginTransaction();
                try {
                    boolean A0F = A0F(this.A00);
                    boolean A05 = A05(this.A00);
                    StringBuilder sb = new StringBuilder();
                    sb.append("DatabaseHelper/on open existing DB, migration flags: migrationCompleted=");
                    sb.append(A05);
                    sb.append(", writeToOldSchemaEnabled=");
                    sb.append(A0F);
                    Log.i(sb.toString());
                    A0C(this.A00, A0F);
                    r2.A00();
                    A0B(this.A00, A05);
                    r2.A00();
                    A0D(this.A00, A05, A0F);
                    r2.A00();
                    this.A09 = null;
                    this.A0A = null;
                    C16330op r1 = this.A00;
                    if (A05) {
                        str2 = "152111e1de35706b2ad0b608f47ecc77";
                    } else {
                        str2 = "c8d3b444a3012f678e1974073781ef3e";
                    }
                    A03(r1, str2);
                    r2.A00();
                    this.A00.A00.setTransactionSuccessful();
                    r2.A00();
                    r2.A00();
                    this.A00.A00.endTransaction();
                } catch (Throwable th) {
                    this.A00.A00.endTransaction();
                    r2.A01();
                    throw th;
                }
            } else {
                this.A09 = null;
                this.A0A = null;
                A06(this.A00);
                A0F(this.A00);
            }
            r2.A01();
            Iterator it = this.A06.iterator();
            while (it.hasNext()) {
                ((C29701Uh) it.next()).A00.A00.edit().putBoolean("force_db_check", false).apply();
            }
            return;
        }
        throw new IllegalStateException("databasehelper/prepareWritableDatabase/database is not initialized");
    }

    public final void A08(SQLiteException sQLiteException) {
        AnonymousClass12I r1;
        int i;
        Iterator it = this.A06.iterator();
        while (it.hasNext()) {
            C29701Uh r2 = (C29701Uh) it.next();
            if (sQLiteException instanceof SQLiteFullException) {
                r1 = r2.A01;
                i = 0;
            } else if (sQLiteException instanceof SQLiteCantOpenDatabaseException) {
                if (r2.A03.A01()) {
                    r1 = r2.A01;
                    i = 2;
                }
            } else if (sQLiteException.toString().contains("unable to open")) {
                r1 = r2.A01;
                i = 3;
            } else if (sQLiteException.toString().contains("attempt to write a readonly database")) {
                r1 = r2.A01;
                i = 4;
            }
            r1.A00(i);
        }
    }

    public void A09(C16310on r9) {
        String str;
        if (this.A00 != null) {
            AnonymousClass1Lx A00 = r9.A00();
            try {
                String[] strArr = C29741Um.A00;
                for (String str2 : strArr) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("message_fts");
                    sb.append(str2);
                    String obj = sb.toString();
                    C16330op r2 = this.A00;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("DROP TABLE IF EXISTS ");
                    sb2.append(obj);
                    r2.A0B(sb2.toString());
                    if (A06(r9.A03).booleanValue()) {
                        str = "message";
                    } else {
                        str = "messages";
                    }
                    String lowerCase = String.format("%s_bd_for_%s_trigger", str, obj).toLowerCase(Locale.getDefault());
                    C16330op r22 = this.A00;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("DROP TRIGGER IF EXISTS ");
                    sb3.append(lowerCase);
                    r22.A0B(sb3.toString());
                }
                A00.A00();
                A00.close();
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } else {
            throw new IllegalStateException("databasehelper/dropOldFtsTables/database is not initialized");
        }
    }

    public final void A0A(C16330op r5) {
        this.A09 = null;
        this.A0A = null;
        if (TextUtils.isEmpty(AnonymousClass1Uj.A00(r5, "table", "messages"))) {
            Log.i("DatabaseHelper/old tables verification, old tables do not exist.");
            C29731Ul.A00(r5, "migration_completed", "DatabaseHelper", 1);
            C29731Ul.A00(r5, "write_to_old_schema_disabled", "DatabaseHelper", 1);
            return;
        }
        Log.i("DatabaseHelper/old tables verification, old tables are available.");
    }

    public void A0B(C16330op r3, boolean z) {
        SQLiteDatabase sQLiteDatabase = r3.A00;
        sQLiteDatabase.beginTransaction();
        try {
            r3.A0B("DROP VIEW IF EXISTS available_messages_view");
            r3.A0B("DROP VIEW IF EXISTS legacy_available_messages_view");
            r3.A0B("DROP VIEW IF EXISTS message_view");
            r3.A0B("DROP VIEW IF EXISTS available_message_view");
            r3.A0B("DROP VIEW IF EXISTS deleted_messages_view");
            r3.A0B("DROP VIEW IF EXISTS deleted_messages_ids_view");
            if (z) {
                r3.A0B("CREATE VIEW message_view AS SELECT message._id AS _id, message.sort_id AS sort_id, message.chat_row_id AS chat_row_id, from_me, key_id, sender_jid_row_id, NULL AS sender_jid_raw_string, status, broadcast, recipient_count, participant_hash, origination_flags, origin, timestamp, received_timestamp, receipt_server_timestamp, message_type, text_data, starred, lookup_tables, message_add_on_flags, NULL AS data, NULL AS media_url, NULL AS media_mime_type, NULL AS media_size, NULL AS media_name, NULL AS media_caption, NULL AS media_hash, NULL AS media_duration, NULL AS latitude, NULL AS longitude, NULL AS thumb_image, NULL AS raw_data, NULL AS quoted_row_id, NULL AS mentioned_jids, NULL AS multicast_id, NULL AS edit_version, NULL AS media_enc_hash, NULL AS payment_transaction_id, NULL AS preview_type, NULL AS receipt_device_timestamp, NULL AS read_device_timestamp, NULL AS played_device_timestamp, NULL AS future_message_type, 2 AS table_version FROM message");
                r3.A0B("CREATE VIEW available_message_view AS  SELECT message._id AS _id, message.sort_id AS sort_id, message.chat_row_id AS chat_row_id, from_me, key_id, sender_jid_row_id, NULL AS sender_jid_raw_string, status, broadcast, recipient_count, participant_hash, origination_flags, origin, timestamp, received_timestamp, receipt_server_timestamp, message_type, text_data, starred, lookup_tables, message_add_on_flags, NULL AS data, NULL AS media_url, NULL AS media_mime_type, NULL AS media_size, NULL AS media_name, NULL AS media_caption, NULL AS media_hash, NULL AS media_duration, NULL AS latitude, NULL AS longitude, NULL AS thumb_image, NULL AS raw_data, NULL AS quoted_row_id, NULL AS mentioned_jids, NULL AS multicast_id, NULL AS edit_version, NULL AS media_enc_hash, NULL AS payment_transaction_id, NULL AS preview_type, NULL AS receipt_device_timestamp, NULL AS read_device_timestamp, NULL AS played_device_timestamp, NULL AS future_message_type, 2 AS table_version, expire_timestamp, keep_in_chat FROM message LEFT JOIN deleted_chat_job AS job ON job.chat_row_id = message.chat_row_id LEFT JOIN message_ephemeral AS message_ephemeral ON message._id = message_ephemeral.message_row_id WHERE  IFNULL(NOT((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_starred_message_row_id, 1)) OR ( (job.deleted_message_categories IS NOT NULL) AND (job.deleted_message_categories LIKE '%\"' || message.message_type || '\"%') AND ((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)))) OR ((job.singular_message_delete_rows_id IS NOT NULL) AND (job.singular_message_delete_rows_id LIKE '%\"' || message._id || '\"%'))), 0)");
                r3.A0B("CREATE VIEW IF NOT EXISTS deleted_messages_view AS   SELECT message._id AS _id, message.sort_id AS sort_id, message.chat_row_id AS chat_row_id, from_me, key_id, sender_jid_row_id, NULL AS sender_jid_raw_string, status, broadcast, recipient_count, participant_hash, origination_flags, origin, timestamp, received_timestamp, receipt_server_timestamp, message_type, text_data, starred, lookup_tables, message_add_on_flags, NULL AS data, NULL AS media_url, NULL AS media_mime_type, NULL AS media_size, NULL AS media_name, NULL AS media_caption, NULL AS media_hash, NULL AS media_duration, NULL AS latitude, NULL AS longitude, NULL AS thumb_image, NULL AS raw_data, NULL AS quoted_row_id, NULL AS mentioned_jids, NULL AS multicast_id, NULL AS edit_version, NULL AS media_enc_hash, NULL AS payment_transaction_id, NULL AS preview_type, NULL AS receipt_device_timestamp, NULL AS read_device_timestamp, NULL AS played_device_timestamp, NULL AS future_message_type, 2 AS table_version, ((((job.singular_message_delete_rows_id LIKE '%\"' || message._id || '\"%') AND (job.delete_files_singular_delete== 1)) OR ((job.deleted_messages_remove_files == 1) AND ((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_starred_message_row_id, 1)))) OR ((job.deleted_categories_remove_files == 1) AND ( (job.deleted_message_categories IS NOT NULL) AND (job.deleted_message_categories LIKE '%\"' || message.message_type || '\"%') AND ((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1))))))) as remove_files FROM  deleted_chat_job AS job JOIN message AS message  ON job.chat_row_id = message.chat_row_id   WHERE  IFNULL((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_starred_message_row_id, 1)) OR ( (job.deleted_message_categories IS NOT NULL) AND (job.deleted_message_categories LIKE '%\"' || message.message_type || '\"%') AND ((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)))) OR ((job.singular_message_delete_rows_id IS NOT NULL) AND (job.singular_message_delete_rows_id LIKE '%\"' || message._id || '\"%')), 0) ORDER BY message._id");
                r3.A0B("CREATE VIEW IF NOT EXISTS deleted_messages_ids_view AS  SELECT message._id, message.sort_id, message.chat_row_id, message.message_type FROM deleted_chat_job AS job  JOIN message AS message  ON job.chat_row_id = message.chat_row_id WHERE  IFNULL((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_starred_message_row_id, 1)) OR ( (job.deleted_message_categories IS NOT NULL) AND (job.deleted_message_categories LIKE '%\"' || message.message_type || '\"%') AND ((IFNULL(message.starred, 0) = 0 AND message._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(message.starred, 0) = 1 AND message._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)))) OR ((job.singular_message_delete_rows_id IS NOT NULL) AND (job.singular_message_delete_rows_id LIKE '%\"' || message._id || '\"%')), 0)");
            } else {
                r3.A0B("CREATE VIEW legacy_available_messages_view AS  SELECT messages.*, chat._id AS chat_row_id,expire_timestamp, keep_in_chat FROM messages AS messages JOIN jid AS jid ON jid.raw_string = messages.key_remote_jid JOIN chat AS chat ON chat.jid_row_id = jid._id LEFT JOIN deleted_chat_job AS job ON job.chat_row_id = chat._id  LEFT JOIN message_ephemeral AS message_ephemeral\n ON messages._id = message_ephemeral.message_row_id WHERE IFNULL(NOT((IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_starred_message_row_id, 1)) OR ( (job.deleted_message_categories IS NOT NULL) AND   (job.deleted_message_categories LIKE '%\"' || messages.media_wa_type || '\"%') AND   ( (IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)) )) OR ((job.singular_message_delete_rows_id IS NOT NULL) AND (job.singular_message_delete_rows_id LIKE '%\"' || messages._id || '\"%'))), 0)");
                r3.A0B("CREATE VIEW message_view AS SELECT messages._id AS _id, messages._id AS sort_id, chat._id AS chat_row_id, key_from_me AS from_me, key_id, -1 AS sender_jid_row_id, remote_resource AS sender_jid_raw_string, status, needs_push AS broadcast, recipient_count, participant_hash, forwarded AS origination_flags, origin, timestamp, received_timestamp, receipt_server_timestamp, CAST (CASE WHEN (messages.media_wa_type = 0 AND messages.status=6) THEN 7 ELSE messages.media_wa_type END AS INTEGER) AS message_type, '' as text_data, starred, lookup_tables, data, media_url, media_mime_type, media_size, media_name, media_caption, media_hash, media_duration, latitude, longitude, thumb_image, raw_data, quoted_row_id, mentioned_jids, multicast_id, edit_version, media_enc_hash, payment_transaction_id, preview_type, receipt_device_timestamp, read_device_timestamp, played_device_timestamp, future_message_type, message_add_on_flags, 1 AS table_version FROM messages JOIN jid AS chat_jid ON messages.key_remote_jid= chat_jid.raw_string JOIN chat AS chat ON chat.jid_row_id = chat_jid._id");
                r3.A0B("CREATE VIEW available_message_view AS SELECT messages._id AS _id, messages._id AS sort_id, chat._id AS chat_row_id, key_from_me AS from_me, key_id, -1 AS sender_jid_row_id, remote_resource AS sender_jid_raw_string, status, needs_push AS broadcast, recipient_count, participant_hash, forwarded AS origination_flags, origin, timestamp, received_timestamp, receipt_server_timestamp, CAST (CASE WHEN (messages.media_wa_type = 0 AND messages.status=6) THEN 7 ELSE messages.media_wa_type END AS INTEGER) AS message_type, '' as text_data, starred, lookup_tables, data, media_url, media_mime_type, media_size, media_name, media_caption, media_hash, media_duration, latitude, longitude, thumb_image, raw_data, quoted_row_id, mentioned_jids, multicast_id, edit_version, media_enc_hash, payment_transaction_id, preview_type, receipt_device_timestamp, read_device_timestamp, played_device_timestamp, future_message_type, message_add_on_flags, 1 AS table_version, expire_timestamp, keep_in_chat FROM messages JOIN jid AS chat_jid ON messages.key_remote_jid= chat_jid.raw_string JOIN chat AS chat ON chat.jid_row_id = chat_jid._id LEFT JOIN message_ephemeral AS message_ephemeral ON messages._id = message_ephemeral.message_row_id LEFT JOIN deleted_chat_job AS job ON job.chat_row_id = chat._id WHERE IFNULL(NOT((IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_starred_message_row_id, 1)) OR ( (job.deleted_message_categories IS NOT NULL) AND   (job.deleted_message_categories LIKE '%\"' || messages.media_wa_type || '\"%') AND   ( (IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)) )) OR ((job.singular_message_delete_rows_id IS NOT NULL) AND (job.singular_message_delete_rows_id LIKE '%\"' || messages._id || '\"%'))), 0)");
                r3.A0B("CREATE VIEW IF NOT EXISTS deleted_messages_view AS  SELECT messages._id AS _id, messages._id AS sort_id, chat._id AS chat_row_id, key_from_me AS from_me, key_id, -1 AS sender_jid_row_id, remote_resource AS sender_jid_raw_string, status, needs_push AS broadcast, recipient_count, participant_hash, forwarded AS origination_flags, origin, timestamp, received_timestamp, receipt_server_timestamp, CAST (CASE WHEN (messages.media_wa_type = 0 AND messages.status=6) THEN 7 ELSE messages.media_wa_type END AS INTEGER) AS message_type, '' as text_data, starred, lookup_tables, data, media_url, media_mime_type, media_size, media_name, media_caption, media_hash, media_duration, latitude, longitude, thumb_image, raw_data, quoted_row_id, mentioned_jids, multicast_id, edit_version, media_enc_hash, payment_transaction_id, preview_type, receipt_device_timestamp, read_device_timestamp, played_device_timestamp, future_message_type, message_add_on_flags, 1 AS table_version,  (( ((job.singular_message_delete_rows_id LIKE '%\"' || messages._id || '\"%') AND (job.delete_files_singular_delete == 1)) OR ((job.deleted_messages_remove_files == 1) AND ((IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_starred_message_row_id, 1)))) OR   ((job.deleted_categories_remove_files == 1) AND ( (job.deleted_message_categories IS NOT NULL) AND   (job.deleted_message_categories LIKE '%\"' || messages.media_wa_type || '\"%') AND   ( (IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)) ))))) as remove_files  FROM deleted_chat_job AS job JOIN chat AS chat ON job.chat_row_id = chat._id JOIN jid AS chat_jid ON chat.jid_row_id = chat_jid._id LEFT JOIN messages AS messages ON messages.key_remote_jid = chat_jid.raw_string WHERE IFNULL((IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_starred_message_row_id, 1)) OR ( (job.deleted_message_categories IS NOT NULL) AND   (job.deleted_message_categories LIKE '%\"' || messages.media_wa_type || '\"%') AND   ( (IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)) )) OR ((job.singular_message_delete_rows_id IS NOT NULL) AND (job.singular_message_delete_rows_id LIKE '%\"' || messages._id || '\"%')), 0)  ORDER BY messages._id");
                r3.A0B("CREATE VIEW IF NOT EXISTS deleted_messages_ids_view AS  SELECT messages._id AS _id, messages._id AS sort_id, CAST (CASE WHEN (messages.media_wa_type = 0 AND messages.status=6) THEN 7 ELSE messages.media_wa_type END AS INTEGER) AS message_type, job.chat_row_id AS chat_row_id FROM deleted_chat_job AS job JOIN chat AS chat ON job.chat_row_id = chat._id JOIN jid AS chat_jid ON chat.jid_row_id = chat_jid._id LEFT JOIN messages AS messages ON messages.key_remote_jid = chat_jid.raw_string WHERE IFNULL((IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_starred_message_row_id, 1)) OR ( (job.deleted_message_categories IS NOT NULL) AND   (job.deleted_message_categories LIKE '%\"' || messages.media_wa_type || '\"%') AND   ( (IFNULL(messages.starred, 0) = 0 AND messages._id <= IFNULL(job.deleted_categories_message_row_id, 1)) OR (IFNULL(messages.starred, 0) = 1 AND messages._id <= IFNULL(job.deleted_categories_starred_message_row_id, 1)) )) OR ((job.singular_message_delete_rows_id IS NOT NULL) AND (job.singular_message_delete_rows_id LIKE '%\"' || messages._id || '\"%')), 0)");
            }
            r3.A0B("DROP VIEW IF EXISTS chat_view");
            r3.A0B("CREATE VIEW chat_view AS SELECT chat._id AS _id, jid.raw_string AS raw_string_jid, hidden AS hidden, subject AS subject, created_timestamp AS created_timestamp, display_message_row_id AS display_message_row_id, last_message_row_id AS last_message_row_id, last_read_message_row_id AS last_read_message_row_id, last_read_receipt_sent_message_row_id AS last_read_receipt_sent_message_row_id, last_important_message_row_id AS last_important_message_row_id, archived AS archived, sort_timestamp AS sort_timestamp, mod_tag AS mod_tag, gen AS gen, spam_detection AS spam_detection, unseen_earliest_message_received_time AS unseen_earliest_message_received_time, unseen_message_count AS unseen_message_count, unseen_missed_calls_count AS unseen_missed_calls_count, unseen_row_count AS unseen_row_count, unseen_message_reaction_count AS unseen_message_reaction_count, last_message_reaction_row_id AS last_message_reaction_row_id, last_seen_message_reaction_row_id AS last_seen_message_reaction_row_id, plaintext_disabled AS plaintext_disabled, vcard_ui_dismissed AS vcard_ui_dismissed, change_number_notified_message_row_id AS change_number_notified_message_row_id, show_group_description AS show_group_description, ephemeral_expiration AS ephemeral_expiration, last_read_ephemeral_message_row_id AS last_read_ephemeral_message_row_id, ephemeral_setting_timestamp AS ephemeral_setting_timestamp, ephemeral_disappearing_messages_initiator AS ephemeral_disappearing_messages_initiator, unseen_important_message_count AS unseen_important_message_count, group_type AS group_type, growth_lock_level AS growth_lock_level, growth_lock_expiration_ts AS growth_lock_expiration_ts, last_read_message_sort_id AS last_read_message_sort_id, display_message_sort_id AS display_message_sort_id, last_message_sort_id AS last_message_sort_id, last_read_receipt_sent_message_sort_id AS last_read_receipt_sent_message_sort_id, has_new_community_admin_dialog_been_acknowledged AS has_new_community_admin_dialog_been_acknowledged, history_sync_progress AS history_sync_progress FROM chat chat LEFT JOIN jid jid ON chat.jid_row_id = jid._id");
            sQLiteDatabase.setTransactionSuccessful();
        } finally {
            sQLiteDatabase.endTransaction();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:434:0x104c, code lost:
        if (java.lang.Integer.parseInt(A02(r32, "links_ready", java.lang.String.valueOf(0))) == 0) goto L_0x104e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x045a  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0470  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0486  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x049c  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x04b4  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x04c9  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x04df  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x04f2  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0515  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x052b  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0546  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x055a  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0570  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0586  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x059c  */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x05ad  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x05cb  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x05dc  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x05f2  */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x0604  */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x0616  */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x0627  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x0640  */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x0667  */
    /* JADX WARNING: Removed duplicated region for block: B:225:0x0680  */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x0691  */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x06a5  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x06df  */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x06f2  */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x070a  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0723  */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x0739  */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x0752  */
    /* JADX WARNING: Removed duplicated region for block: B:257:0x0789  */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x07a2  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x07b3  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x07c9  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x07da  */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x07ee  */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x07ff  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x0810  */
    /* JADX WARNING: Removed duplicated region for block: B:281:0x0821  */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x0832  */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x0843  */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x0854  */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x0865  */
    /* JADX WARNING: Removed duplicated region for block: B:296:0x0876  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x0887  */
    /* JADX WARNING: Removed duplicated region for block: B:302:0x0898  */
    /* JADX WARNING: Removed duplicated region for block: B:305:0x08a9  */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x08ba  */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x08cb  */
    /* JADX WARNING: Removed duplicated region for block: B:314:0x08dc  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x08ed  */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x08fe  */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x090f  */
    /* JADX WARNING: Removed duplicated region for block: B:326:0x0925  */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x0936  */
    /* JADX WARNING: Removed duplicated region for block: B:332:0x095c  */
    /* JADX WARNING: Removed duplicated region for block: B:335:0x096d  */
    /* JADX WARNING: Removed duplicated region for block: B:338:0x0983  */
    /* JADX WARNING: Removed duplicated region for block: B:341:0x0994  */
    /* JADX WARNING: Removed duplicated region for block: B:344:0x09a5  */
    /* JADX WARNING: Removed duplicated region for block: B:347:0x09b6  */
    /* JADX WARNING: Removed duplicated region for block: B:350:0x09c7  */
    /* JADX WARNING: Removed duplicated region for block: B:353:0x09ec  */
    /* JADX WARNING: Removed duplicated region for block: B:356:0x0a09  */
    /* JADX WARNING: Removed duplicated region for block: B:362:0x0a8a  */
    /* JADX WARNING: Removed duplicated region for block: B:365:0x0a9b  */
    /* JADX WARNING: Removed duplicated region for block: B:368:0x0aac  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x0ac2  */
    /* JADX WARNING: Removed duplicated region for block: B:374:0x0ad3  */
    /* JADX WARNING: Removed duplicated region for block: B:377:0x0ae9  */
    /* JADX WARNING: Removed duplicated region for block: B:380:0x0afa  */
    /* JADX WARNING: Removed duplicated region for block: B:382:0x0b01  */
    /* JADX WARNING: Removed duplicated region for block: B:386:0x0b16  */
    /* JADX WARNING: Removed duplicated region for block: B:387:0x0b29  */
    /* JADX WARNING: Removed duplicated region for block: B:388:0x0b36  */
    /* JADX WARNING: Removed duplicated region for block: B:389:0x0b43  */
    /* JADX WARNING: Removed duplicated region for block: B:390:0x0b50  */
    /* JADX WARNING: Removed duplicated region for block: B:391:0x0b5d  */
    /* JADX WARNING: Removed duplicated region for block: B:392:0x0b6a  */
    /* JADX WARNING: Removed duplicated region for block: B:393:0x0b7c  */
    /* JADX WARNING: Removed duplicated region for block: B:394:0x0b89  */
    /* JADX WARNING: Removed duplicated region for block: B:395:0x0b9a  */
    /* JADX WARNING: Removed duplicated region for block: B:396:0x0ba7  */
    /* JADX WARNING: Removed duplicated region for block: B:397:0x0c86  */
    /* JADX WARNING: Removed duplicated region for block: B:398:0x0ca3  */
    /* JADX WARNING: Removed duplicated region for block: B:399:0x0cbb  */
    /* JADX WARNING: Removed duplicated region for block: B:403:0x0cd3  */
    /* JADX WARNING: Removed duplicated region for block: B:404:0x0d00  */
    /* JADX WARNING: Removed duplicated region for block: B:405:0x0d32  */
    /* JADX WARNING: Removed duplicated region for block: B:407:0x0d9c  */
    /* JADX WARNING: Removed duplicated region for block: B:408:0x0db1  */
    /* JADX WARNING: Removed duplicated region for block: B:409:0x0dc3  */
    /* JADX WARNING: Removed duplicated region for block: B:410:0x0dd1  */
    /* JADX WARNING: Removed duplicated region for block: B:412:0x0dfb  */
    /* JADX WARNING: Removed duplicated region for block: B:413:0x0e08  */
    /* JADX WARNING: Removed duplicated region for block: B:469:0x111d  */
    /* JADX WARNING: Removed duplicated region for block: B:473:0x1133  */
    /* JADX WARNING: Removed duplicated region for block: B:476:0x1149  */
    /* JADX WARNING: Removed duplicated region for block: B:479:0x115f  */
    /* JADX WARNING: Removed duplicated region for block: B:482:0x1170  */
    /* JADX WARNING: Removed duplicated region for block: B:485:0x1186  */
    /* JADX WARNING: Removed duplicated region for block: B:488:0x1199  */
    /* JADX WARNING: Removed duplicated region for block: B:494:0x11bc  */
    /* JADX WARNING: Removed duplicated region for block: B:497:0x11cd  */
    /* JADX WARNING: Removed duplicated region for block: B:500:0x11e3  */
    /* JADX WARNING: Removed duplicated region for block: B:503:0x11f4  */
    /* JADX WARNING: Removed duplicated region for block: B:506:0x1205  */
    /* JADX WARNING: Removed duplicated region for block: B:509:0x1216  */
    /* JADX WARNING: Removed duplicated region for block: B:512:0x1227  */
    /* JADX WARNING: Removed duplicated region for block: B:515:0x123d  */
    /* JADX WARNING: Removed duplicated region for block: B:518:0x124e  */
    /* JADX WARNING: Removed duplicated region for block: B:521:0x125f  */
    /* JADX WARNING: Removed duplicated region for block: B:524:0x1292  */
    /* JADX WARNING: Removed duplicated region for block: B:527:0x12d8  */
    /* JADX WARNING: Removed duplicated region for block: B:530:0x12e9  */
    /* JADX WARNING: Removed duplicated region for block: B:533:0x12f4  */
    /* JADX WARNING: Removed duplicated region for block: B:534:0x1303  */
    /* JADX WARNING: Removed duplicated region for block: B:536:0x131c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0C(X.C16330op r32, boolean r33) {
        /*
        // Method dump skipped, instructions count: 4938
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29561To.A0C(X.0op, boolean):void");
    }

    public void A0D(C16330op r20, boolean z, boolean z2) {
        String str;
        String str2;
        Pair A00;
        Pair A002;
        Pair A003;
        Pair A004;
        String str3;
        String str4;
        String str5;
        String replaceAll;
        C29791Ur r18 = C29791Ur.A00;
        TreeMap treeMap = new TreeMap(r18);
        try {
            Cursor A09 = r20.A09("select name, sql from sqlite_master where type='trigger';", null);
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("name");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("sql");
            while (A09.moveToNext()) {
                treeMap.put(A09.getString(columnIndexOrThrow), A09.getString(columnIndexOrThrow2));
            }
            A09.close();
        } catch (Exception e) {
            Log.e("databasehelper/onCreate/dropTriggers", e);
        }
        ArrayList arrayList = new ArrayList();
        String str6 = "messages";
        if (z2) {
            String str7 = "message";
            String str8 = str6;
            if (z) {
                str8 = str7;
                str7 = str6;
            }
            arrayList.add(A00(str8, str7, "_id=old._id"));
        }
        arrayList.add(A01("messages_hydrated_four_row_template", z));
        if (!z) {
            str = str6;
        } else {
            str = "message";
        }
        arrayList.add(A00(str, "message_ftsv2", "docid=old._id"));
        if (z2) {
            arrayList.add(A01("messages_vcards", z));
            arrayList.add(A01("messages_links", z));
        }
        arrayList.add(A01("message_product", z));
        arrayList.add(A01("message_group_invite", z));
        arrayList.add(A01("message_order", z));
        arrayList.add(A01("message_template", z));
        arrayList.add(A01("message_location", z));
        arrayList.add(A01("message_media", z));
        arrayList.add(A01("receipt_user", z));
        arrayList.add(A01("receipt_device", z));
        arrayList.add(A01("played_self_receipt", z));
        arrayList.add(A01("message_mentions", z));
        arrayList.add(A01("message_vcard", z));
        if (z2) {
            arrayList.add(A00("message_vcard", "messages_vcards_jids", "message_row_id = old.message_row_id"));
        }
        arrayList.add(A01("message_streaming_sidecar", z));
        arrayList.add(A01("mms_thumbnail_metadata", z));
        arrayList.add(A01("audio_data", z));
        arrayList.add(A01("message_ephemeral", z));
        arrayList.add(A01("message_broadcast_ephemeral", z));
        arrayList.add(A01("message_privacy_state", z));
        arrayList.add(A01("missed_call_logs", z));
        arrayList.add(A01("message_link", z));
        arrayList.add(A01("message_forwarded", z));
        arrayList.add(A01("message_thumbnail", z));
        arrayList.add(A01("message_text", z));
        arrayList.add(A01("message_revoked", z));
        arrayList.add(A01("message_rating", z));
        arrayList.add(A01("message_future", z));
        arrayList.add(A01("message_send_count", z));
        arrayList.add(A01("message_system", z));
        arrayList.add(A01("agent_message_attribution", z));
        arrayList.add(A00("message_system", "message_system_block_contact", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_ephemeral_setting_not_applied", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_chat_participant", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_device_change", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_initial_privacy_provider", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_group", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_number_change", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_photo_change", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_value_change", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_payment", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_payment_transaction_reminder", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_payment_status_update", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_business_state", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_payment_invite_setup", "message_row_id=old.message_row_id"));
        arrayList.add(A01("message_external_ad_content", z));
        arrayList.add(A01("message_ui_elements", z));
        arrayList.add(A01("message_ui_elements_reply", z));
        arrayList.add(A01("message_view_once_media", z));
        arrayList.add(A01("labeled_messages", z));
        arrayList.add(A01("message_ephemeral", z));
        arrayList.add(A01("message_ephemeral_setting", z));
        if (!z) {
            str2 = str6;
        } else {
            str2 = "message";
        }
        arrayList.add(A00(str2, "labeled_messages_fts", "docid=old._id"));
        arrayList.add(A00("message_system", "message_system_linked_group_call", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_sibling_group_link_change", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_community_link_changed", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_system", "message_system_group_with_parent", "message_row_id=old.message_row_id"));
        arrayList.add(A01("message_status_psa_campaign", z));
        if (z2) {
            arrayList.add(A00(str6, "receipts", "key_remote_jid=old.key_remote_jid AND key_id=old.key_id"));
        }
        arrayList.add(A00("message_template", "message_template_button", "message_row_id=old.message_row_id"));
        arrayList.add(A00("quick_replies", "quick_reply_usage", "quick_reply_id=old._id"));
        arrayList.add(A00("quick_replies", "quick_reply_keywords", "quick_reply_id=old._id"));
        arrayList.add(A00("quick_replies", "quick_reply_attachments", "quick_reply_id=old._id"));
        arrayList.add(A01("message_quoted", z));
        if (z2) {
            arrayList.add(A00(str6, "messages_quotes", "_id=old.quoted_row_id"));
        }
        arrayList.add(A00("message_quoted", "message_quoted_group_invite", "message_row_id=old.message_row_id"));
        if (z2) {
            arrayList.add(A00("messages_quotes", "message_quoted_group_invite_legacy", "message_row_id=old._id"));
        }
        arrayList.add(A00("message_quoted", "message_quoted_location", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_quoted", "message_quoted_media", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_quoted", "message_quoted_mentions", "message_row_id = old.message_row_id"));
        if (z) {
            A00 = A00("message_quoted", "message_quoted_product", "message_row_id=old.message_row_id");
        } else {
            A00 = A00("messages_quotes", "message_quoted_product", "message_row_id=old._id");
        }
        arrayList.add(A00);
        if (z) {
            A002 = A00("message_quoted", "message_quoted_order", "message_row_id=old.message_row_id");
        } else {
            A002 = A00("messages_quotes", "message_quoted_order", "message_row_id=old._id");
        }
        arrayList.add(A002);
        arrayList.add(A00("message_quoted", "message_quoted_text", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_quoted", "message_quoted_vcard", "message_row_id=old.message_row_id"));
        if (z2) {
            arrayList.add(A00("messages_quotes", "message_quoted_ui_elements_reply_legacy", "message_row_id=old._id"));
        }
        if (z) {
            A003 = A00("message_quoted", "message_quoted_ui_elements", "message_row_id=old.message_row_id");
        } else {
            A003 = A00("messages_quotes", "message_quoted_ui_elements", "message_row_id=old._id");
        }
        arrayList.add(A003);
        if (z) {
            arrayList.add(A00("message_quoted", "message_quoted_ui_elements_reply", "message_row_id=old.message_row_id"));
        }
        if (z) {
            A004 = A00("message_quoted", "message_template_quoted", "message_row_id=old.message_row_id");
        } else {
            A004 = A00("messages_quotes", "message_template_quoted", "message_row_id=old._id");
        }
        arrayList.add(A004);
        if (z2) {
            arrayList.add(A00("messages_quotes", "quoted_message_product", "message_row_id=old._id"));
            arrayList.add(A00("messages_quotes", "quoted_message_order", "message_row_id=old._id"));
        }
        arrayList.add(A00("message_media", "message_media_interactive_annotation", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_vcard", "message_vcard_jid", "vcard_row_id=old._id"));
        arrayList.add(A00("message_media", "message_media_vcard_count", "message_row_id=old.message_row_id"));
        arrayList.add(A01("message_vcard_jid", z));
        arrayList.add(A00("group_participant_user", "group_participant_device", "group_participant_row_id=old._id"));
        arrayList.add(A00("call_log", "call_log_participant_v2", "call_log_row_id=old._id"));
        arrayList.add(A00("call_log", "joinable_call_log", "call_log_row_id=old._id"));
        arrayList.add(A00("missed_call_logs", "missed_call_log_participant", "call_logs_row_id=old._id"));
        arrayList.add(A00("chat", "message_link", "chat_row_id=old._id"));
        arrayList.add(A00("labels", "labeled_jid", "label_id=old._id"));
        arrayList.add(A00("labels", "labeled_messages", "label_id=old._id"));
        arrayList.add(A00("labels", "labeled_jids", "label_id=old._id"));
        arrayList.add(A00("message_quoted", "message_quoted_blank_reply", "message_row_id=old.message_row_id"));
        arrayList.add(A01("message_payment_invite", z));
        arrayList.add(A00("message_quoted", "message_quoted_payment_invite", "message_row_id=old.message_row_id"));
        if (z2) {
            arrayList.add(A00("messages_quotes", "messages_quotes_payment_invite_legacy", "message_row_id=old._id"));
        }
        arrayList.add(A00("message_media_interactive_annotation", "message_media_interactive_annotation_vertex", "message_media_interactive_annotation_row_id=old._id"));
        arrayList.add(A00("payment_background", "payment_background_order", "background_id=old.background_id"));
        if (!z) {
            str3 = str6;
        } else {
            str3 = "message";
        }
        arrayList.add(A00(str3, "message_add_on", "parent_message_row_id=old._id"));
        arrayList.add(A00("chat", "message_add_on_orphan", "parent_chat_row_id=old._id"));
        arrayList.add(A00("message_add_on", "message_add_on_receipt_device", "message_add_on_row_id=old._id"));
        arrayList.add(A00("message_add_on", "message_add_on_reaction", "message_add_on_row_id=old._id"));
        if (!z) {
            str4 = str6;
        } else {
            str4 = "message";
        }
        arrayList.add(A00(str4, "message_poll", "message_row_id=old._id"));
        arrayList.add(A00("message_poll", "message_poll_option", "message_row_id=old.message_row_id"));
        arrayList.add(A00("message_add_on", "message_add_on_poll_vote", "message_add_on_row_id=old._id"));
        arrayList.add(A00("message_add_on", "message_add_on_poll_vote_selected_option", "message_add_on_row_id=old._id"));
        arrayList.add(A00("message_add_on", "message_add_on_keep_in_chat", "message_add_on_row_id=old._id"));
        if (!z) {
            str5 = str6;
        } else {
            str5 = "message";
        }
        arrayList.add(A00(str5, "message_secret", "message_row_id=old._id"));
        arrayList.add(A00("chat", "community_chat", "chat_row_id=old._id"));
        TreeMap treeMap2 = new TreeMap(r18);
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            treeMap2.put(pair.first, pair.second);
        }
        if (!TextUtils.isEmpty(AnonymousClass1Uj.A00(r20, "table", "call_logs"))) {
            Pair A005 = A00("call_logs", "call_log_participant", "call_logs_row_id=old._id");
            treeMap2.put(A005.first, A005.second);
        }
        if (!TextUtils.isEmpty(AnonymousClass1Uj.A00(r20, "table", "messages_fts"))) {
            if (z) {
                str6 = "message";
            }
            Pair A006 = A00(str6, "messages_fts", "docid=old._id");
            treeMap2.put(A006.first, A006.second);
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        for (Map.Entry entry : treeMap.entrySet()) {
            Object key = entry.getKey();
            String str9 = (String) treeMap2.get(key);
            if (str9 != null) {
                String str10 = (String) entry.getValue();
                String replaceAll2 = str9.toLowerCase(Locale.getDefault()).replaceAll("\\s*", "");
                if (str10 == null) {
                    replaceAll = null;
                } else {
                    replaceAll = str10.toLowerCase(Locale.getDefault()).replaceAll("\\s*", "");
                }
                if (replaceAll2.equalsIgnoreCase(replaceAll)) {
                    arrayList3.add(key);
                }
            }
            arrayList2.add(String.format("DROP TRIGGER %s;", key));
        }
        Iterator it2 = arrayList3.iterator();
        while (it2.hasNext()) {
            treeMap2.remove(it2.next());
        }
        for (Map.Entry entry2 : treeMap2.entrySet()) {
            arrayList2.add(entry2.getValue());
        }
        Iterator it3 = arrayList2.iterator();
        while (it3.hasNext()) {
            String str11 = (String) it3.next();
            StringBuilder sb = new StringBuilder("DatabaseHelper/createDatabaseTriggers/");
            sb.append(str11);
            sb.toString();
            r20.A0B(str11);
        }
    }

    public boolean A0E(C16310on r2) {
        if (this.A0A != null) {
            return this.A0A.booleanValue();
        }
        return A0F(r2.A03);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (java.lang.Integer.parseInt(A02(r4, "write_to_old_schema_disabled", java.lang.String.valueOf(0))) == 0) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0F(X.C16330op r4) {
        /*
            r3 = this;
            boolean r0 = A05(r4)
            if (r0 != 0) goto L_0x0011
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
        L_0x0008:
            r3.A0A = r0
            java.lang.Boolean r0 = r3.A0A
            boolean r0 = r0.booleanValue()
            return r0
        L_0x0011:
            java.lang.String r2 = "write_to_old_schema_disabled"
            r1 = 0
            java.lang.String r0 = java.lang.String.valueOf(r1)     // Catch: NumberFormatException -> 0x0023
            java.lang.String r0 = A02(r4, r2, r0)     // Catch: NumberFormatException -> 0x0023
            int r0 = java.lang.Integer.parseInt(r0)     // Catch: NumberFormatException -> 0x0023
            if (r0 != 0) goto L_0x0024
        L_0x0023:
            r1 = 1
        L_0x0024:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r1)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29561To.A0F(X.0op):boolean");
    }

    @Override // X.AbstractC16300om
    public C29621Ty AEm() {
        return this.A03;
    }

    @Override // X.AbstractC16300om
    public synchronized C16330op AG6() {
        return AHr();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0112, code lost:
        if (r7 != false) goto L_0x0114;
     */
    @Override // X.AbstractC16300om
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized X.C16330op AHr() {
        /*
        // Method dump skipped, instructions count: 671
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C29561To.AHr():X.0op");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper, java.lang.AutoCloseable
    public synchronized void close() {
        C16330op r0 = this.A00;
        if (r0 != null && r0.A00.isOpen()) {
            StringBuilder sb = new StringBuilder();
            sb.append("msgstore/close, ");
            sb.append(this.A00.A00);
            Log.i(sb.toString());
            this.A00.A00.close();
        }
        this.A00 = null;
        this.A01 = null;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    @Deprecated
    public synchronized SQLiteDatabase getReadableDatabase() {
        AnonymousClass009.A07("Use getReadableLoggableDatabase instead");
        return AG6().A00;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    @Deprecated
    public synchronized SQLiteDatabase getWritableDatabase() {
        AnonymousClass009.A07("Use getWritableLoggableDatabase instead");
        return AHr().A00;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        synchronized (this) {
            C16330op A01 = AnonymousClass1Tx.A01(sQLiteDatabase, this.A05);
            C29811Ut r0 = new C29811Ut(this, atomicBoolean);
            SQLiteDatabase sQLiteDatabase2 = A01.A00;
            sQLiteDatabase2.beginTransactionWithListener(r0);
            Log.i("msgstore/create");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "chat_list");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "props");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_fts");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_ftsv2");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_quotes");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_vcard");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_dehydrated_hsm");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_hydrated_four_row_template");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_vcards");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_vcards_jids");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_orphaned_edit");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_mentions");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_links");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_product");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "quoted_message_product");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_product");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_order");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "quoted_message_order");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_order");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_group_invite");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_group_invite_legacy");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_group_invite");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_template");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_template_button");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_template_quoted");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_location");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_location");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_media");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_media_interactive_annotation");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_media_interactive_annotation_vertex");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_media");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "frequents");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "frequent");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "receipt_user");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "receipt_device");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "receipt_orphaned");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "receipts");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_mentions");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_vcard");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_media_vcard_count");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_vcard_jid");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "user_device");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "group_participant_user");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "group_participant_device");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "group_participants");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "group_participants_history");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "group_notification_version");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "media_refs");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "media_streaming_sidecar");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_thumbnails");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_streaming_sidecar");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "mms_thumbnail_metadata");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "audio_data");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "status_list");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "status");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "conversion_tuples");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "deleted_chat_job");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "pay_transactions");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "pay_transaction");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "payment_background");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "payment_background_order");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_ephemeral");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_linked_group_call");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_community_link_changed");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_group_with_parent");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_sibling_group_link_change");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "call_log");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "missed_call_logs");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "missed_call_log_participant");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "jid");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "jid_map");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "lid_display_name");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "chat");
            StringBuilder sb = new StringBuilder("DROP VIEW IF EXISTS ");
            sb.append("chat_view");
            String obj = sb.toString();
            StringBuilder sb2 = new StringBuilder("DROP_");
            sb2.append("chat_view");
            C29721Uk.A00("DatabaseHelper", "dropViewIfExistsWithoutStatement", sb2.toString());
            A01.A0B(obj);
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_link");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_forwarded");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_thumbnail");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_text");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_text");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_revoked");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_rating");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_future");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_payment");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_payment_transaction_reminder");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_payment_status_update");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_send_count");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_group");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_value_change");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_number_change");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_device_change");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_initial_privacy_provider");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_photo_change");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_chat_participant");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_block_contact");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_business_state");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "media_hash_thumbnail");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "user_device_info");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "played_self_receipt");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_external_ad_content");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_ui_elements");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_ui_elements");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_ui_elements_reply");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_ui_elements_reply");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_ui_elements_reply_legacy");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_privacy_state");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_view_once_media");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_view_once_media");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_view_once_media_legacy");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_broadcast_ephemeral");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_ephemeral_setting");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_ephemeral_setting_not_applied");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "labeled_jids");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "labeled_messages");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "labels");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "labeled_jid");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "away_messages");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "away_messages_exemptions");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "quick_replies");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "quick_reply_usage");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "quick_reply_keywords");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "keywords");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "quick_reply_attachments");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "agent_devices");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "agent_message_attribution");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "agent_chat_assignment");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_payment_invite");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_quoted_payment_invite");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "messages_quotes_payment_invite_legacy");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_system_payment_invite_setup");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_status_psa_campaign");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_add_on");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_add_on_orphan");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_add_on_receipt_device");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_add_on_reaction");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_poll");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_poll_option");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_add_on_poll_vote");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_add_on_poll_vote_selected_option");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_add_on_keep_in_chat");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "message_secret");
            AnonymousClass1Uj.A01(A01, "DatabaseHelper", "community_chat");
            if (TextUtils.isEmpty(AnonymousClass1Uj.A00(A01, "table", "props"))) {
                A01.A0B("CREATE TABLE props (_id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT UNIQUE, value TEXT)");
            }
            C29731Ul.A00(A01, "fts_ready", "DatabaseHelper", 5);
            C29731Ul.A00(A01, "call_log_ready", "DatabaseHelper", 1);
            C29731Ul.A00(A01, "chat_ready", "DatabaseHelper", 2);
            C29731Ul.A00(A01, "blank_me_jid_ready", "DatabaseHelper", 1);
            C29731Ul.A00(A01, "participant_user_ready", "DatabaseHelper", 2);
            C29731Ul.A00(A01, "broadcast_me_jid_ready", "DatabaseHelper", 2);
            C29731Ul.A00(A01, "receipt_user_ready", "DatabaseHelper", 2);
            C29731Ul.A00(A01, "receipt_device_migration_complete", "DatabaseHelper", 1);
            C29731Ul.A00(A01, "status_list_ready", "DatabaseHelper", 1);
            A01.A0C("DELETE FROM props WHERE key = ?", new String[]{"message_streaming_sidecar_timestamp"});
            C29731Ul.A00(A01, "media_message_ready", "DatabaseHelper", 2);
            C29731Ul.A00(A01, "media_message_fixer_ready", "DatabaseHelper", 3);
            if (this.A04.A01) {
                Log.i("DatabaseHelper/using migrated DB");
                C29731Ul.A00(A01, "main_message_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "missed_calls_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "location_ready", "DatabaseHelper", 2);
                C29731Ul.A00(A01, "mention_message_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "new_vcards_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "participant_user_ready", "DatabaseHelper", 2);
                C29731Ul.A00(A01, "links_ready", "DatabaseHelper", 2);
                C29731Ul.A00(A01, "quoted_message_ready", "DatabaseHelper", 2);
                C29731Ul.A00(A01, "system_message_ready", "DatabaseHelper", 2);
                C29731Ul.A00(A01, "thumbnail_ready", "DatabaseHelper", 2);
                C29731Ul.A00(A01, "text_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "future_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "send_count_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "labeled_jids_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "frequent_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "revoked_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "new_pay_transaction_ready", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "migration_completed", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "write_to_old_schema_disabled", "DatabaseHelper", 1);
                C29731Ul.A00(A01, "drop_deprecated_tables_status", "DatabaseHelper", 1);
            } else {
                Log.i("DatabaseHelper/using NOT migrated DB");
            }
            boolean A0F = A0F(A01);
            boolean A05 = A05(A01);
            StringBuilder sb3 = new StringBuilder();
            sb3.append("DatabaseHelper/On new DB creation, migration flags: migrationCompleted=");
            sb3.append(A05);
            sb3.append(", writeToOldSchemaEnabled=");
            sb3.append(A0F);
            Log.i(sb3.toString());
            A0C(A01, A0F);
            if (A0F) {
                A01.A0B("INSERT INTO messages(_id, key_remote_jid, key_from_me, key_id, status, needs_push, data, timestamp, media_url, media_mime_type, media_wa_type, media_size, media_name, media_hash, media_duration, origin, latitude, longitude, thumb_image, received_timestamp, send_timestamp, receipt_server_timestamp, receipt_device_timestamp, read_device_timestamp, played_device_timestamp, mentioned_jids) VALUES (1, '-1', 0, '-1', -1, 0, NULL, 0, NULL, NULL, -1, -1, NULL, NULL, 0, 0, 0, 0, NULL, -1, -1, -1, -1, -1, -1, NULL)");
            }
            A0B(A01, A05);
            A0D(A01, A05, A0F);
            A0A(A01);
            A03(A01, A05 ? "152111e1de35706b2ad0b608f47ecc77" : "c8d3b444a3012f678e1974073781ef3e");
            sQLiteDatabase2.setTransactionSuccessful();
            Iterator it = this.A06.iterator();
            while (it.hasNext()) {
                SharedPreferences sharedPreferences = ((C29701Uh) it.next()).A00.A00;
                sharedPreferences.edit().putBoolean("md_messaging_enabled", true).apply();
                sharedPreferences.edit().putBoolean("force_db_check", false).apply();
            }
            sQLiteDatabase2.endTransaction();
            this.A00 = A01;
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.getVersion();
        sQLiteDatabase.execSQL("PRAGMA synchronous=NORMAL;");
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA secure_delete=1", null);
            if (rawQuery != null) {
                while (rawQuery.moveToNext()) {
                    rawQuery.getInt(0);
                }
                rawQuery.close();
            }
        } catch (SQLiteDiskIOException e) {
            Log.e("msgstore/enable_secure_delete", e);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("msgstore/upgrade version ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        onCreate(sQLiteDatabase);
    }
}
