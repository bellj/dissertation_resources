package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import java.util.ArrayList;

/* renamed from: X.0IR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IR extends AnonymousClass0IS implements AbstractC11540gS {
    public static Bitmap A04;
    public static AnonymousClass0Q8 A05;
    public static final ArrayList A06 = new ArrayList(5);
    public int A00;
    public boolean A01;
    public final AnonymousClass0I8 A02;
    public final AnonymousClass0IC A03;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0IR(X.AnonymousClass04Q r19, X.AnonymousClass0IC r20) {
        /*
            r18 = this;
            X.0Mn r3 = new X.0Mn
            r3.<init>()
            r4 = r20
            r3.A00 = r4
            r1 = 0
            X.0Q8 r0 = X.AnonymousClass0IR.A05
            if (r0 != 0) goto L_0x0015
            X.0Q8 r0 = new X.0Q8
            r0.<init>()
            X.AnonymousClass0IR.A05 = r0
        L_0x0015:
            r2 = r18
            r5 = r19
            r2.<init>(r5, r0, r3)
            r0 = 1
            r2.A00 = r0
            r2.A03 = r1
            r2.A03 = r4
            X.0I8 r0 = new X.0I8
            r0.<init>(r5)
            r2.A02 = r0
            X.04Q r1 = r2.A09
            r1.A0C(r0)
            X.04L r0 = r1.A0E
            r0.A0P = r2
            X.0ID r0 = new X.0ID
            r0.<init>(r2)
            r2.A08 = r0
            android.graphics.Bitmap r0 = X.AnonymousClass0IR.A04
            if (r0 != 0) goto L_0x00a6
            int r3 = r1.A0O
            android.content.Context r0 = r1.A0P
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r1 = r0.densityDpi
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ALPHA_8
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r3, r3, r0)
            X.AnonymousClass0IR.A04 = r0
            android.graphics.Canvas r4 = new android.graphics.Canvas
            r4.<init>(r0)
            r0 = 320(0x140, float:4.48E-43)
            r2 = 16
            if (r1 < r0) goto L_0x0061
            r2 = 32
        L_0x0061:
            android.graphics.Paint r9 = new android.graphics.Paint
            r9.<init>()
            r0 = -7235677(0xffffffffff9197a3, float:NaN)
            r9.setColor(r0)
            r6 = 0
            r5 = 0
        L_0x006e:
            float r8 = (float) r3
            int r0 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r0 > 0) goto L_0x00a6
            int r0 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x007d
            int r1 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            r0 = 18
            if (r1 != 0) goto L_0x007f
        L_0x007d:
            r0 = 44
        L_0x007f:
            r9.setAlpha(r0)
            r7 = r5
            r4.drawLine(r5, r6, r7, r8, r9)
            r0 = 1065353216(0x3f800000, float:1.0)
            float r11 = r5 - r0
            r12 = 0
            r10 = r4
            r13 = r11
            r14 = r8
            r15 = r9
            r10.drawLine(r11, r12, r13, r14, r15)
            r13 = 0
            r16 = r5
            r12 = r4
            r14 = r5
            r15 = r8
            r17 = r9
            r12.drawLine(r13, r14, r15, r16, r17)
            r16 = r11
            r14 = r11
            r12.drawLine(r13, r14, r15, r16, r17)
            float r0 = (float) r2
            float r5 = r5 + r0
            goto L_0x006e
        L_0x00a6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IR.<init>(X.04Q, X.0IC):void");
    }

    public static void A00(int[] iArr) {
        ArrayList arrayList = A06;
        int size = arrayList.size();
        if (size == 0) {
            iArr[0] = 0;
            iArr[1] = 0;
            return;
        }
        double max = Math.max(1.6d - (((double) size) * 0.1d), 1.1d);
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            i += ((AnonymousClass0IT) arrayList.get(i2)).A00;
        }
        int i3 = ((int) (((double) i) * max)) + 1;
        iArr[0] = i3;
        iArr[1] = Math.max((i3 - i) - 1, 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if (((float) r3.getHeight()) < r1) goto L_0x001d;
     */
    @Override // X.AnonymousClass03S
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07() {
        /*
            r4 = this;
            X.04Q r0 = r4.A09
            X.04L r3 = r0.A0E
            float r1 = r4.A05
            r0 = 1132068864(0x437a0000, float:250.0)
            float r1 = r1 * r0
            int r0 = r3.getWidth()
            float r0 = (float) r0
            r2 = 1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x001d
            int r0 = r3.getHeight()
            float r0 = (float) r0
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            r0 = 1
            if (r1 >= 0) goto L_0x001e
        L_0x001d:
            r0 = 0
        L_0x001e:
            r4.A01 = r0
            X.0I8 r1 = r4.A02
            if (r0 == 0) goto L_0x002c
            boolean r0 = r4.A04
            if (r0 == 0) goto L_0x002c
        L_0x0028:
            r1.A09(r2)
            return
        L_0x002c:
            r2 = 0
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IR.A07():void");
    }

    @Override // X.AnonymousClass0IT, X.AnonymousClass03S
    public void A08(Canvas canvas) {
        long nanoTime = System.nanoTime();
        super.A08(canvas);
        C06160Sk.A0F.A02(System.nanoTime() - nanoTime);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r3 == false) goto L_0x000c;
     */
    @Override // X.AnonymousClass0IT, X.AnonymousClass03S
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(boolean r3) {
        /*
            r2 = this;
            super.A09(r3)
            X.0I8 r1 = r2.A02
            boolean r0 = r2.A01
            if (r0 == 0) goto L_0x000c
            r0 = 1
            if (r3 != 0) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            r1.A09(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IR.A09(boolean):void");
    }

    @Override // X.AnonymousClass0IS, X.AnonymousClass0IT
    public C06440Tp A0A(int i, int i2, int i3) {
        C06440Tp A0A = super.A0A(i, i2, i3);
        if (A0A != null) {
            A0A.A02 = i;
            A0A.A03 = i2;
            A0A.A04 = i3;
        }
        return A0A;
    }
}
