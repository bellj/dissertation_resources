package X;

/* renamed from: X.4bB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94054bB {
    public static final long[] A03 = {128, 64, 32, 16, 8, 4, 2, 1};
    public int A00;
    public int A01;
    public final byte[] A02 = new byte[8];

    public long A00(AnonymousClass5Yf r11, int i, boolean z, boolean z2) {
        if (this.A01 == 0) {
            byte[] bArr = this.A02;
            if (!r11.AZu(bArr, 0, 1, z)) {
                return -1;
            }
            int i2 = bArr[0] & 255;
            int i3 = 0;
            while (true) {
                long[] jArr = A03;
                if (i3 >= jArr.length) {
                    i3 = -1;
                    break;
                }
                int i4 = ((jArr[i3] & ((long) i2)) > 0 ? 1 : ((jArr[i3] & ((long) i2)) == 0 ? 0 : -1));
                i3++;
                if (i4 != 0) {
                    break;
                }
            }
            this.A00 = i3;
            if (i3 != -1) {
                this.A01 = 1;
            } else {
                throw C12960it.A0U("No valid varint length mask found");
            }
        }
        int i5 = this.A00;
        if (i5 > i) {
            this.A01 = 0;
            return -2;
        }
        if (i5 != 1) {
            r11.readFully(this.A02, 1, i5 - 1);
        }
        this.A01 = 0;
        byte[] bArr2 = this.A02;
        int i6 = this.A00;
        long j = ((long) bArr2[0]) & 255;
        if (z2) {
            j &= A03[i6 - 1] ^ -1;
        }
        for (int i7 = 1; i7 < i6; i7++) {
            j = (j << 8) | (((long) bArr2[i7]) & 255);
        }
        return j;
    }
}
