package X;

/* renamed from: X.3ZC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZC implements AbstractC21730xt {
    public final C89484Kd A00;
    public final C15580nU A01;
    public final C17220qS A02;

    public AnonymousClass3ZC(C89484Kd r1, C15580nU r2, C17220qS r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        C20710wC.A02(1008, null);
        C20710wC.A02(3012, null);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        int i;
        AnonymousClass1V8 A0E = r5.A0E("error");
        int i2 = -2;
        if (A0E != null) {
            i2 = A0E.A05("code", -2);
        }
        if (i2 != -2) {
            if (i2 == -1) {
                C20710wC.A02(1008, null);
            } else if (i2 != 400) {
                i = 3010;
                if (i2 != 401) {
                    i = 3011;
                    if (i2 != 403) {
                        if (!(i2 == 404 || i2 == 500)) {
                            return;
                        }
                    }
                }
                C20710wC.A02(i, null);
            }
        }
        i = 3012;
        C20710wC.A02(i, null);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r1, String str) {
    }
}
