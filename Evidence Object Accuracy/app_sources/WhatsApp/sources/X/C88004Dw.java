package X;

import android.webkit.URLUtil;

/* renamed from: X.4Dw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88004Dw {
    public static String A00(String str) {
        if (URLUtil.isHttpUrl(str) || URLUtil.isHttpsUrl(str)) {
            return str;
        }
        return C12960it.A0d(str, C12960it.A0k("https://"));
    }
}
