package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4no  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101614no implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AbstractC36001jA A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101614no(AbstractC36001jA r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AbstractC36001jA r4 = this.A00;
        C12980iv.A1E(r4.A0L, this);
        r4.A03 = r4.A0L.getMeasuredHeight();
        r4.A04 = (int) (((double) r4.A0L.getMeasuredHeight()) * 0.6d);
        r4.A0P(null, false);
    }
}
