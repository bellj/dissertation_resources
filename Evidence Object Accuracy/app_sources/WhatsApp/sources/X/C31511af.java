package X;

/* renamed from: X.1af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31511af {
    public final int A00;
    public final byte[] A01;
    public final byte[] A02;
    public final byte[] A03;

    public C31511af(int i, byte[] bArr) {
        byte[][] A01 = C31241aE.A01(new C31221aC().A02(bArr, new byte[32], "WhisperGroup".getBytes(), 48), 16, 32);
        this.A00 = i;
        this.A03 = bArr;
        this.A02 = A01[0];
        this.A01 = A01[1];
    }
}
