package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.58d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1111158d implements AbstractC116085Ub {
    @Override // X.AbstractC116085Ub
    public boolean Adb(AnonymousClass1IR r3) {
        int i = r3.A02;
        if (i == 18 || i == 105 || i == 112 || i == 409 || i == 421 || i == 107 || i == 108 || i == 406 || i == 407) {
            return true;
        }
        switch (i) {
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return true;
            default:
                switch (i) {
                    case 414:
                    case 415:
                    case 416:
                        return true;
                    default:
                        switch (i) {
                            case 605:
                            case 606:
                            case 607:
                                return true;
                            default:
                                return false;
                        }
                }
        }
    }
}
