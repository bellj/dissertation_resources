package X;

import android.content.res.Resources;

/* renamed from: X.07g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C015307g {
    public final Resources.Theme A00;
    public final Resources A01;

    public C015307g(Resources.Theme theme, Resources resources) {
        this.A01 = resources;
        this.A00 = theme;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C015307g.class != obj.getClass()) {
                return false;
            }
            C015307g r5 = (C015307g) obj;
            if (!this.A01.equals(r5.A01) || !C015407h.A01(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return C015407h.A00(this.A01, this.A00);
    }
}
