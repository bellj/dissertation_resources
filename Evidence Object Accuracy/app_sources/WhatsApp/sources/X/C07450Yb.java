package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0Yb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07450Yb implements AnonymousClass02B {
    public final /* synthetic */ BiometricFragment A00;

    public C07450Yb(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        C04700Ms r4 = (C04700Ms) obj;
        if (r4 != null) {
            BiometricFragment biometricFragment = this.A00;
            biometricFragment.A1H(r4);
            AnonymousClass0EP r2 = biometricFragment.A01;
            AnonymousClass016 r0 = r2.A0A;
            if (r0 == null) {
                r0 = new AnonymousClass016();
                r2.A0A = r0;
            }
            AnonymousClass0EP.A00(r0, null);
        }
    }
}
