package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Wl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30191Wl implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99664kf();
    public final int A00;
    public final int A01;
    public final Integer A02;
    public final Integer A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30191Wl(Parcel parcel) {
        this.A00 = parcel.readInt();
        this.A01 = parcel.readInt();
        if (parcel.readByte() == 0) {
            this.A03 = null;
        } else {
            this.A03 = Integer.valueOf(parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.A02 = null;
        } else {
            this.A02 = Integer.valueOf(parcel.readInt());
        }
    }

    public C30191Wl(Integer num, Integer num2, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A03 = num;
        this.A02 = num2;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C30191Wl)) {
            return false;
        }
        C30191Wl r4 = (C30191Wl) obj;
        if (this.A00 != r4.A00 || this.A01 != r4.A01) {
            return false;
        }
        Integer num = this.A03;
        Integer num2 = r4.A03;
        if (num == null) {
            if (num2 != null) {
                return false;
            }
        } else if (!num.equals(num2)) {
            return false;
        }
        Integer num3 = this.A02;
        Integer num4 = r4.A02;
        if (num3 == null) {
            if (num4 != null) {
                return false;
            }
        } else if (!num3.equals(num4)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i;
        int i2 = ((this.A00 * 31) + this.A01) * 31;
        Integer num = this.A03;
        int i3 = 0;
        if (num != null) {
            i = num.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i2 + i) * 31;
        Integer num2 = this.A02;
        if (num2 != null) {
            i3 = num2.hashCode();
        }
        return i4 + i3;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("dayOfWeek: ");
        sb.append(this.A00);
        sb.append(", mode: ");
        sb.append(this.A01);
        sb.append(", openTime: ");
        sb.append(this.A03);
        sb.append(", closeTime: ");
        sb.append(this.A02);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A01);
        Integer num = this.A03;
        if (num == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(num.intValue());
        }
        Integer num2 = this.A02;
        if (num2 == null) {
            parcel.writeByte((byte) 0);
            return;
        }
        parcel.writeByte((byte) 1);
        parcel.writeInt(num2.intValue());
    }
}
