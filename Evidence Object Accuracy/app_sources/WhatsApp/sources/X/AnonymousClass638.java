package X;

import android.graphics.Point;
import android.hardware.Camera;

/* renamed from: X.638  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass638 implements Camera.AutoFocusCallback {
    public final /* synthetic */ Point A00;
    public final /* synthetic */ AbstractC136156Lf A01;
    public final /* synthetic */ C129305xV A02;

    public AnonymousClass638(Point point, AbstractC136156Lf r2, C129305xV r3) {
        this.A02 = r3;
        this.A00 = point;
        this.A01 = r2;
    }

    @Override // android.hardware.Camera.AutoFocusCallback
    public void onAutoFocus(boolean z, Camera camera) {
        EnumC124565pk r2;
        C129305xV r3 = this.A02;
        r3.A07 = z;
        r3.A08 = false;
        if (r3.A0A) {
            r3.A04 = false;
        }
        if (z) {
            r2 = EnumC124565pk.SUCCESS;
        } else {
            r2 = EnumC124565pk.FAILED;
        }
        Point point = this.A00;
        r3.A00(point, r2, r3.A02);
        r3.A00(point, r2, this.A01);
    }
}
