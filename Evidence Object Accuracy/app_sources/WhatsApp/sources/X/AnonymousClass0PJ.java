package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0PJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PJ {
    public AnonymousClass0JJ A00;
    public String A01;
    public List A02 = null;
    public List A03 = null;

    public AnonymousClass0PJ(AnonymousClass0JJ r2, String str) {
        this.A00 = r2 == null ? AnonymousClass0JJ.DESCENDANT : r2;
        this.A01 = str;
    }

    public void A00(AnonymousClass0JT r3, String str, String str2) {
        List list = this.A02;
        if (list == null) {
            list = new ArrayList();
            this.A02 = list;
        }
        list.add(new AnonymousClass0NS(r3, str, str2));
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0014  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r4 = this;
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            X.0JJ r1 = r4.A00
            X.0JJ r0 = X.AnonymousClass0JJ.CHILD
            if (r1 != r0) goto L_0x005a
            java.lang.String r0 = "> "
        L_0x000d:
            r3.append(r0)
        L_0x0010:
            java.lang.String r0 = r4.A01
            if (r0 != 0) goto L_0x0016
            java.lang.String r0 = "*"
        L_0x0016:
            r3.append(r0)
            java.util.List r0 = r4.A02
            if (r0 == 0) goto L_0x0061
            java.util.Iterator r2 = r0.iterator()
        L_0x0021:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0061
            java.lang.Object r1 = r2.next()
            X.0NS r1 = (X.AnonymousClass0NS) r1
            r0 = 91
            r3.append(r0)
            java.lang.String r0 = r1.A01
            r3.append(r0)
            X.0JT r0 = r1.A00
            int r0 = r0.ordinal()
            switch(r0) {
                case 1: goto L_0x0049;
                case 2: goto L_0x004f;
                case 3: goto L_0x0046;
                default: goto L_0x0040;
            }
        L_0x0040:
            r0 = 93
            r3.append(r0)
            goto L_0x0021
        L_0x0046:
            java.lang.String r0 = "|="
            goto L_0x0051
        L_0x0049:
            r0 = 61
            r3.append(r0)
            goto L_0x0054
        L_0x004f:
            java.lang.String r0 = "~="
        L_0x0051:
            r3.append(r0)
        L_0x0054:
            java.lang.String r0 = r1.A02
            r3.append(r0)
            goto L_0x0040
        L_0x005a:
            X.0JJ r0 = X.AnonymousClass0JJ.FOLLOWS
            if (r1 != r0) goto L_0x0010
            java.lang.String r0 = "+ "
            goto L_0x000d
        L_0x0061:
            java.util.List r0 = r4.A03
            if (r0 == 0) goto L_0x007c
            java.util.Iterator r2 = r0.iterator()
        L_0x0069:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x007c
            java.lang.Object r1 = r2.next()
            r0 = 58
            r3.append(r0)
            r3.append(r1)
            goto L_0x0069
        L_0x007c:
            java.lang.String r0 = r3.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0PJ.toString():java.lang.String");
    }
}
