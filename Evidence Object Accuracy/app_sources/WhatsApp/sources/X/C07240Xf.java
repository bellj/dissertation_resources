package X;

import java.util.ArrayList;

/* renamed from: X.0Xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07240Xf implements AbstractC11690gh {
    public float A00 = 0.0f;
    public AbstractC12730iP A01;
    public AnonymousClass0QC A02 = null;
    public ArrayList A03 = new ArrayList();
    public boolean A04 = false;

    public C07240Xf() {
    }

    public C07240Xf(AnonymousClass0NX r3) {
        this.A01 = new C07220Xd(this, r3);
    }

    public final AnonymousClass0QC A00(AnonymousClass0QC r11, boolean[] zArr) {
        AnonymousClass0JQ r1;
        AbstractC12730iP r7 = this.A01;
        int ACD = r7.ACD();
        AnonymousClass0QC r8 = null;
        float f = 0.0f;
        for (int i = 0; i < ACD; i++) {
            float AHW = r7.AHW(i);
            if (AHW < 0.0f) {
                AnonymousClass0QC AHV = r7.AHV(i);
                if ((zArr == null || !zArr[AHV.A02]) && AHV != r11 && (((r1 = AHV.A06) == AnonymousClass0JQ.SLACK || r1 == AnonymousClass0JQ.ERROR) && AHW < f)) {
                    f = AHW;
                    r8 = AHV;
                }
            }
        }
        return r8;
    }

    public void A01(C07240Xf r4, boolean z) {
        this.A00 += r4.A00 * this.A01.Afd(r4, z);
        if (z) {
            r4.A02.A02(this);
        }
    }

    public void A02(AnonymousClass0QC r5) {
        AnonymousClass0QC r1 = this.A02;
        if (r1 != null) {
            this.A01.AZg(r1, -1.0f);
            this.A02 = null;
        }
        AbstractC12730iP r2 = this.A01;
        float AaE = r2.AaE(r5, true) * -1.0f;
        this.A02 = r5;
        if (AaE != 1.0f) {
            this.A00 /= AaE;
            r2.A95(AaE);
        }
    }

    public void A03(AnonymousClass0QC r5, AnonymousClass0QC r6, AnonymousClass0QC r7, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i = -i;
                z = true;
            }
            this.A00 = (float) i;
        }
        AbstractC12730iP r0 = this.A01;
        if (!z) {
            r0.AZg(r5, -1.0f);
            this.A01.AZg(r6, 1.0f);
            this.A01.AZg(r7, 1.0f);
            return;
        }
        r0.AZg(r5, 1.0f);
        this.A01.AZg(r6, -1.0f);
        this.A01.AZg(r7, -1.0f);
    }

    public void A04(AnonymousClass0QC r5, AnonymousClass0QC r6, AnonymousClass0QC r7, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i = -i;
                z = true;
            }
            this.A00 = (float) i;
        }
        AbstractC12730iP r0 = this.A01;
        if (!z) {
            r0.AZg(r5, -1.0f);
            this.A01.AZg(r6, 1.0f);
            this.A01.AZg(r7, -1.0f);
            return;
        }
        r0.AZg(r5, 1.0f);
        this.A01.AZg(r6, -1.0f);
        this.A01.AZg(r7, 1.0f);
    }

    public void A05(AnonymousClass0QC r5, boolean z) {
        if (r5.A08) {
            AbstractC12730iP r3 = this.A01;
            this.A00 += r5.A00 * r3.AAK(r5);
            r3.AaE(r5, z);
            if (z) {
                r5.A02(this);
            }
        }
    }

    @Override // X.AbstractC11690gh
    public AnonymousClass0QC AFh(C06240Ss r2, boolean[] zArr) {
        return A00(null, zArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0081  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r10 = this;
            X.0QC r2 = r10.A02
            java.lang.String r0 = ""
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            if (r2 != 0) goto L_0x00b5
            r1.append(r0)
            java.lang.String r0 = "0"
            r1.append(r0)
        L_0x0013:
            java.lang.String r0 = r1.toString()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = " = "
            r1.append(r0)
            java.lang.String r7 = r1.toString()
            float r1 = r10.A00
            r6 = 0
            r9 = 0
            int r0 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r0 == 0) goto L_0x00b3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r7)
            r0.append(r1)
            java.lang.String r7 = r0.toString()
            r8 = 1
        L_0x0040:
            X.0iP r0 = r10.A01
            int r5 = r0.ACD()
        L_0x0046:
            if (r6 >= r5) goto L_0x00bd
            X.0iP r0 = r10.A01
            X.0QC r1 = r0.AHV(r6)
            if (r1 == 0) goto L_0x0094
            float r4 = r0.AHW(r6)
            int r0 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r0 == 0) goto L_0x0094
            java.lang.String r3 = r1.toString()
            r2 = -1082130432(0xffffffffbf800000, float:-1.0)
            if (r8 != 0) goto L_0x0097
            int r0 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r0 >= 0) goto L_0x0076
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r7)
            java.lang.String r0 = "- "
        L_0x006e:
            r1.append(r0)
            java.lang.String r7 = r1.toString()
            float r4 = r4 * r2
        L_0x0076:
            r0 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            if (r1 == 0) goto L_0x0089
            r0.append(r7)
            r0.append(r4)
            java.lang.String r7 = " "
        L_0x0089:
            r0.append(r7)
            r0.append(r3)
            java.lang.String r7 = r0.toString()
            r8 = 1
        L_0x0094:
            int r6 = r6 + 1
            goto L_0x0046
        L_0x0097:
            int r0 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            if (r0 <= 0) goto L_0x00ad
            r1.append(r7)
            java.lang.String r0 = " + "
            r1.append(r0)
            java.lang.String r7 = r1.toString()
            goto L_0x0076
        L_0x00ad:
            r1.append(r7)
            java.lang.String r0 = " - "
            goto L_0x006e
        L_0x00b3:
            r8 = 0
            goto L_0x0040
        L_0x00b5:
            r1.append(r0)
            r1.append(r2)
            goto L_0x0013
        L_0x00bd:
            if (r8 != 0) goto L_0x00d0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r7)
            java.lang.String r0 = "0.0"
            r1.append(r0)
            java.lang.String r7 = r1.toString()
        L_0x00d0:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07240Xf.toString():java.lang.String");
    }
}
