package X;

import java.util.ArrayList;

/* renamed from: X.3cI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71043cI implements AbstractC115475Rr {
    public static final ArrayList A05;
    public final AnonymousClass1V8 A00;
    public final C64083Ee A01;
    public final String A02;
    public final String A03;
    public final String A04;

    static {
        String[] strArr = new String[2];
        strArr[0] = "merge";
        A05 = C12960it.A0m("replace", strArr, 1);
    }

    public C71043cI(AbstractC15710nm r14, AnonymousClass1V8 r15) {
        AnonymousClass1V8.A01(r15, "state");
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        AnonymousClass3JT.A04(null, r15, String.class, A0j, A0k, "resource", new String[]{"type"}, false);
        this.A02 = (String) AnonymousClass3JT.A04(null, r15, String.class, A0j, A0k, null, new String[]{"id"}, false);
        this.A04 = (String) AnonymousClass3JT.A03(null, r15, String.class, A0j, A0k, null, new String[]{"params"}, false);
        this.A03 = AnonymousClass3JT.A08(r15, A05, new String[]{"merge"});
        this.A01 = (C64083Ee) AnonymousClass3JT.A02(r14, r15, 10);
        this.A00 = r15;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C71043cI.class != obj.getClass()) {
                return false;
            }
            C71043cI r5 = (C71043cI) obj;
            if (!C29941Vi.A00(this.A03, r5.A03) || !this.A02.equals(r5.A02) || !C29941Vi.A00(this.A04, r5.A04) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[4];
        objArr[0] = this.A03;
        objArr[1] = this.A02;
        objArr[2] = this.A04;
        return C12980iv.A0B(this.A01, objArr, 3);
    }
}
