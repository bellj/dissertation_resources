package X;

/* renamed from: X.5bq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118455bq extends AnonymousClass0Yo {
    public final /* synthetic */ C120565gO A00;
    public final /* synthetic */ C128355vy A01;
    public final /* synthetic */ String A02;

    public C118455bq(C120565gO r1, C128355vy r2, String str) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = str;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117945b1.class)) {
            C128355vy r0 = this.A01;
            C16590pI r3 = r0.A0A;
            AbstractC14440lR r10 = r0.A0k;
            C241414j r5 = r0.A0H;
            C14830m7 r2 = r0.A09;
            C14900mE r1 = r0.A00;
            AnonymousClass018 r4 = r0.A0C;
            C1310060v r9 = r0.A0g;
            C120565gO r8 = this.A00;
            return new C117945b1(r1, r2, r3, r4, r5, r0.A0S, r0.A0W, r8, r9, r10, this.A02);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
