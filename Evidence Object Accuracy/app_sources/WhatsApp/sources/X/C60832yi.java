package X;

import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2yi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60832yi extends AnonymousClass2y1 implements AbstractC42481vH {
    public View A00 = findViewById(R.id.name_in_group);
    public View A01 = findViewById(R.id.sticker_bubble_header);
    public C15890o4 A02;
    public AnonymousClass109 A03;
    public C22370yy A04;
    public AnonymousClass1AB A05;
    public List A06;
    public boolean A07;
    public final ArrayList A08;

    @Override // X.AbstractC28551Oa
    public boolean A0i() {
        return false;
    }

    @Override // X.AnonymousClass1OY
    public boolean A1I() {
        return false;
    }

    @Override // X.AnonymousClass1OY
    public boolean A1J() {
        return false;
    }

    @Override // X.AnonymousClass1OY
    public boolean A1K() {
        return false;
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return 0;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        return 0;
    }

    @Override // X.AbstractC60742yY
    public int getMaxAlbumSize() {
        return 2;
    }

    @Override // X.AbstractC60742yY
    public int getMinAlbumSize() {
        return 2;
    }

    public C60832yi(Context context, AbstractC13890kV r5, AbstractC16130oV r6, AnonymousClass1AB r7) {
        super(context, r5, r6);
        ArrayList A0l = C12960it.A0l();
        this.A08 = A0l;
        this.A05 = r7;
        A0l.add(new AnonymousClass3FR((LinearLayout) findViewById(R.id.sticker_1), this));
        A0l.add(new AnonymousClass3FR((LinearLayout) findViewById(R.id.sticker_2), this));
    }

    private void A0X(boolean z) {
        AbstractC16130oV r0;
        if (this.A06 != null) {
            int i = 0;
            while (true) {
                ArrayList arrayList = this.A08;
                if (i < arrayList.size()) {
                    int size = this.A06.size();
                    AnonymousClass3FR r1 = (AnonymousClass3FR) arrayList.get(i);
                    if (i < size) {
                        r0 = (AbstractC16130oV) this.A06.get(i);
                    } else {
                        r0 = null;
                    }
                    r1.A01(r0, z);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @Override // X.AbstractC28551Oa
    public int A0a() {
        View view = this.A01;
        if (view != null) {
            return ((AbstractC28551Oa) this).A0D.getTop() + view.getBottom();
        }
        return ((AbstractC28551Oa) this).A0D.getBottom();
    }

    @Override // X.AbstractC28551Oa
    public int A0b() {
        View view;
        if (!this.A07 || (view = this.A01) == null) {
            return ((AbstractC28551Oa) this).A0D.getTop();
        }
        return ((AbstractC28551Oa) this).A0D.getTop() + view.getPaddingTop();
    }

    @Override // X.AbstractC28551Oa
    public void A0d() {
        Iterator it = this.A08.iterator();
        while (it.hasNext()) {
            AnonymousClass3FR r2 = (AnonymousClass3FR) it.next();
            StickerView stickerView = r2.A0A;
            stickerView.clearAnimation();
            r2.A00 = 0.0f;
            stickerView.invalidate();
        }
    }

    @Override // X.AbstractC28551Oa
    public void A0e(AnonymousClass1IS r5) {
        Iterator it = this.A08.iterator();
        while (it.hasNext()) {
            AnonymousClass3FR r1 = (AnonymousClass3FR) it.next();
            if (r5.equals(r1.A05.A0z)) {
                StickerView stickerView = r1.A0A;
                stickerView.setBackgroundDrawable(new AnonymousClass2ZQ(r1));
                C73853gu r2 = new C73853gu(r1);
                r2.setDuration(2400);
                r2.setInterpolator(new AccelerateInterpolator());
                stickerView.startAnimation(r2);
                return;
            }
        }
    }

    @Override // X.AbstractC28551Oa
    public boolean A0h() {
        return C30041Vv.A0t(((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A0X(false);
        A1H(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        if (r2 == 100) goto L_0x004d;
     */
    @Override // X.AnonymousClass1OY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0w() {
        /*
            r5 = this;
            r4 = 0
        L_0x0001:
            java.util.ArrayList r1 = r5.A08
            int r0 = r1.size()
            if (r4 >= r0) goto L_0x006b
            java.util.List r0 = r5.A06
            int r0 = r0.size()
            if (r4 >= r0) goto L_0x0066
            java.lang.Object r0 = r1.get(r4)
            X.3FR r0 = (X.AnonymousClass3FR) r0
            X.3Hd r0 = r0.A09
            com.whatsapp.CircularProgressBar r3 = r0.A07
            java.util.List r0 = r5.A06
            java.lang.Object r0 = r0.get(r4)
            X.0oV r0 = (X.AbstractC16130oV) r0
            X.0oX r1 = X.AbstractC15340mz.A00(r0)
            boolean r0 = r1.A0a
            if (r0 == 0) goto L_0x0069
            boolean r0 = r1.A0Y
            if (r0 != 0) goto L_0x0069
            long r0 = r1.A0C
            int r2 = (int) r0
            X.109 r1 = r5.A03
            java.util.List r0 = r5.A06
            java.lang.Object r0 = r0.get(r4)
            X.0oV r0 = (X.AbstractC16130oV) r0
            boolean r0 = r1.A07(r0)
            int r2 = r2 >> 1
            if (r0 == 0) goto L_0x0046
            int r2 = r2 + 50
        L_0x0046:
            if (r2 == 0) goto L_0x004d
            r1 = 100
            r0 = 0
            if (r2 != r1) goto L_0x004e
        L_0x004d:
            r0 = 1
        L_0x004e:
            r3.setIndeterminate(r0)
            r3.setProgress(r2)
            android.content.Context r1 = r5.getContext()
            r0 = 2131100430(0x7f06030e, float:1.7813241E38)
            if (r2 != 0) goto L_0x0060
            r0 = 2131100431(0x7f06030f, float:1.7813243E38)
        L_0x0060:
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            r3.A0C = r0
        L_0x0066:
            int r4 = r4 + 1
            goto L_0x0001
        L_0x0069:
            r2 = 0
            goto L_0x004d
        L_0x006b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60832yi.A0w():void");
    }

    @Override // X.AnonymousClass1OY
    public void A1B(AbstractC15340mz r5) {
        Iterator it = this.A08.iterator();
        while (it.hasNext()) {
            AnonymousClass3FR r3 = (AnonymousClass3FR) it.next();
            AbstractC16130oV r0 = r3.A05;
            if (r0 != null && r5.A0z.equals(r0.A0z)) {
                AbstractC13890kV r2 = ((AbstractC28551Oa) r3.A0B).A0a;
                if (r2 != null && r2.AIM()) {
                    r3.A02.setSelected(r2.Af1(r3.A05));
                    return;
                }
                return;
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1C(AbstractC15340mz r5, int i) {
        if (r5 instanceof AbstractC16130oV) {
            Iterator it = this.A08.iterator();
            while (it.hasNext()) {
                AnonymousClass3FR r2 = (AnonymousClass3FR) it.next();
                AbstractC16130oV r0 = r2.A05;
                if (r0 != null && r5.A0z.equals(r0.A0z)) {
                    r2.A01 = i;
                    r2.A01((AbstractC16130oV) r5, false);
                    return;
                }
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        super.A1D(((AbstractC28551Oa) this).A0O, z);
        if (z) {
            A0X(false);
        }
        A1N();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        if (((X.AbstractC15340mz) r6.get(0)).A0z.A02 != false) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (((X.AbstractC28551Oa) r5).A0O != r6.get(0)) goto L_0x000e;
     */
    @Override // X.AbstractC60742yY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1M(java.util.List r6, boolean r7) {
        /*
            r5 = this;
            java.util.List r0 = r5.A06
            r4 = 0
            if (r0 == 0) goto L_0x000e
            X.0mz r1 = r5.A0O
            java.lang.Object r0 = r6.get(r4)
            r3 = 0
            if (r1 == r0) goto L_0x000f
        L_0x000e:
            r3 = 1
        L_0x000f:
            java.lang.Object r0 = r6.get(r4)
            X.0mz r0 = (X.AbstractC15340mz) r0
            X.1IS r0 = r0.A0z
            X.0lm r0 = r0.A00
            boolean r0 = X.C15380n4.A0J(r0)
            if (r0 == 0) goto L_0x002c
            java.lang.Object r0 = r6.get(r4)
            X.0mz r0 = (X.AbstractC15340mz) r0
            X.1IS r0 = r0.A0z
            boolean r1 = r0.A02
            r0 = 1
            if (r1 == 0) goto L_0x002d
        L_0x002c:
            r0 = 0
        L_0x002d:
            r5.A07 = r0
            int r1 = r6.size()
            r0 = 2
            if (r1 == r0) goto L_0x003c
            java.lang.String r0 = "ConversationRowStickerAlbum/setAlbumMessages improper number of stickers in sticker album"
            com.whatsapp.util.Log.e(r0)
            return
        L_0x003c:
            if (r7 != 0) goto L_0x0059
            java.util.List r0 = r5.A06
            if (r0 == 0) goto L_0x0058
            r2 = 0
        L_0x0043:
            int r0 = r6.size()
            if (r2 >= r0) goto L_0x0059
            java.util.List r0 = r5.A06
            java.lang.Object r1 = r0.get(r2)
            java.lang.Object r0 = r6.get(r2)
            if (r1 != r0) goto L_0x0058
            int r2 = r2 + 1
            goto L_0x0043
        L_0x0058:
            r7 = 1
        L_0x0059:
            r5.A06 = r6
            java.lang.Object r0 = r6.get(r4)
            X.0mz r0 = (X.AbstractC15340mz) r0
            super.A1D(r0, r7)
            if (r3 != 0) goto L_0x0068
            if (r7 == 0) goto L_0x006b
        L_0x0068:
            r5.A0X(r3)
        L_0x006b:
            r5.A1N()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60832yi.A1M(java.util.List, boolean):void");
    }

    public final void A1N() {
        if (this.A06 != null) {
            int i = 0;
            while (true) {
                ArrayList arrayList = this.A08;
                if (i < arrayList.size()) {
                    if (i < this.A06.size()) {
                        AnonymousClass3FR r3 = (AnonymousClass3FR) arrayList.get(i);
                        AbstractC13890kV r2 = ((AbstractC28551Oa) r3.A0B).A0a;
                        if (r2 == null || !r2.AIM()) {
                            C12970iu.A1G(r3.A02);
                        } else {
                            r3.A00();
                            r3.A02.setSelected(r2.AJm(r3.A05));
                        }
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @Override // X.AbstractC42481vH
    public void Ae9() {
        Iterator it = this.A08.iterator();
        while (it.hasNext()) {
            ((AnonymousClass3FR) it.next()).A0A.A02();
        }
    }

    @Override // X.AbstractC28551Oa
    public int getBubbleAlpha() {
        if (this.A07) {
            return 255;
        }
        return 0;
    }

    @Override // X.AbstractC28551Oa
    public int getContentWidth() {
        View view;
        if (!this.A07 || ((AbstractC28551Oa) this).A0O.A0E() != null || this.A01 == null) {
            view = ((AbstractC28551Oa) this).A0D;
        } else {
            view = this.A00;
        }
        return view.getMeasuredWidth();
    }

    @Override // X.AbstractC28551Oa
    public AbstractC16130oV getFMessage() {
        return (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_sticker_album_left;
    }

    @Override // X.AbstractC60742yY, X.AnonymousClass1OY
    public int getMessageCount() {
        return C12970iu.A09(this.A06);
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_sticker_album_right;
    }

    @Override // X.AbstractC28551Oa
    public int getReactionsViewVerticalOverlap() {
        return getResources().getDimensionPixelOffset(R.dimen.space_base);
    }
}
