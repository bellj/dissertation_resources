package X;

/* renamed from: X.3Et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64233Et {
    public final int A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass4X8 A03;
    public final C30031Vu A04;
    public final AbstractC15340mz A05;
    public final AbstractC15340mz A06;
    public final AnonymousClass1IS A07;
    public final Integer A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;

    public C64233Et(AnonymousClass4X8 r1, C30031Vu r2, AbstractC15340mz r3, AbstractC15340mz r4, AnonymousClass1IS r5, Integer num, int i, int i2, int i3, boolean z, boolean z2, boolean z3) {
        this.A09 = z;
        this.A04 = r2;
        this.A02 = i;
        this.A08 = num;
        this.A03 = r1;
        this.A05 = r3;
        this.A0B = z2;
        this.A0A = z3;
        this.A00 = i2;
        this.A01 = i3;
        this.A06 = r4;
        this.A07 = r5;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C64233Et r5 = (C64233Et) obj;
            if (!(this.A09 == r5.A09 && this.A02 == r5.A02 && this.A04.equals(r5.A04) && C29941Vi.A00(this.A08, r5.A08) && C29941Vi.A00(this.A03, r5.A03) && C29941Vi.A00(this.A05, r5.A05) && this.A0B == r5.A0B && this.A0A == r5.A0A && this.A00 == r5.A00 && this.A01 == r5.A01 && this.A06 == r5.A06 && this.A07 == r5.A07)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[12];
        objArr[0] = Boolean.valueOf(this.A09);
        objArr[1] = this.A04;
        C12990iw.A1V(objArr, this.A02);
        objArr[3] = this.A08;
        objArr[4] = this.A03;
        objArr[5] = this.A05;
        objArr[6] = Boolean.valueOf(this.A0B);
        objArr[7] = Boolean.valueOf(this.A0A);
        objArr[8] = Integer.valueOf(this.A00);
        objArr[9] = Integer.valueOf(this.A01);
        objArr[10] = this.A06;
        return C12980iv.A0B(this.A07, objArr, 11);
    }
}
