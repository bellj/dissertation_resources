package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

/* renamed from: X.4ds  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95524ds {
    public static final byte[] A07 = {0, 7, 8, 15};
    public static final byte[] A08 = {0, 119, -120, -1};
    public static final byte[] A09 = {0, 17, 34, 51, 68, 85, 102, 119, -120, -103, -86, -69, -52, -35, -18, -1};
    public Bitmap A00;
    public final Canvas A01 = new Canvas();
    public final Paint A02;
    public final Paint A03;
    public final AnonymousClass4R3 A04 = new AnonymousClass4R3(new int[]{0, -1, -16777216, -8421505}, A03(), A04(), 0);
    public final AnonymousClass4T5 A05 = new AnonymousClass4T5(719, 575, 0, 719, 0, 575);
    public final C91914Ts A06;

    public C95524ds(int i, int i2) {
        Paint paint = new Paint();
        this.A02 = paint;
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        paint.setPathEffect(null);
        Paint paint2 = new Paint();
        this.A03 = paint2;
        paint2.setStyle(Paint.Style.FILL);
        paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
        paint2.setPathEffect(null);
        this.A06 = new C91914Ts(i, i2);
    }

    public static AnonymousClass4R3 A00(C95054d0 r19, int i) {
        int[] iArr;
        int A04;
        int A042;
        int A043;
        int A044;
        int i2 = 8;
        int A045 = r19.A04(8);
        r19.A08(8);
        int i3 = 2;
        int i4 = i - 2;
        int[] iArr2 = {0, -1, -16777216, -8421505};
        int[] A03 = A03();
        int[] A046 = A04();
        while (i4 > 0) {
            int A047 = r19.A04(i2);
            int A048 = r19.A04(i2);
            int i5 = i4 - 2;
            if ((A048 & 128) != 0) {
                iArr = iArr2;
            } else {
                iArr = A046;
                if ((A048 & 64) != 0) {
                    iArr = A03;
                }
            }
            if ((A048 & 1) != 0) {
                A04 = r19.A04(i2);
                A042 = r19.A04(i2);
                A043 = r19.A04(i2);
                A044 = r19.A04(i2);
                i4 = i5 - 4;
            } else {
                A04 = r19.A04(6) << i3;
                A042 = r19.A04(4) << 4;
                A043 = r19.A04(4) << 4;
                A044 = r19.A04(i3) << 6;
                i4 = i5 - 2;
            }
            if (A04 == 0) {
                A042 = 0;
                A043 = 0;
                A044 = 255;
            }
            double d = (double) A04;
            double d2 = (double) (A042 - 128);
            double d3 = (double) (A043 - 128);
            iArr[A047] = (((byte) (255 - (A044 & 255))) << 24) | (C72463ee.A03((int) (d + (1.402d * d2)), 255, 0) << 16) | (C72463ee.A03((int) ((d - (0.34414d * d3)) - (d2 * 0.71414d)), 255, 0) << 8) | C72463ee.A03((int) (d + (d3 * 1.772d)), 255, 0);
            i2 = 8;
            i3 = 2;
        }
        return new AnonymousClass4R3(iArr2, A03, A046, A045);
    }

    public static AnonymousClass4R4 A01(C95054d0 r6) {
        byte[] bArr;
        int A04 = r6.A04(16);
        r6.A08(4);
        int A042 = r6.A04(2);
        boolean A0C = r6.A0C();
        r6.A08(1);
        byte[] bArr2 = AnonymousClass3JZ.A0A;
        if (A042 == 1) {
            r6.A08(r6.A04(8) << 4);
        } else if (A042 == 0) {
            int A043 = r6.A04(16);
            int A044 = r6.A04(16);
            if (A043 > 0) {
                bArr2 = new byte[A043];
                r6.A0B(bArr2, A043);
            }
            if (A044 > 0) {
                bArr = new byte[A044];
                r6.A0B(bArr, A044);
                return new AnonymousClass4R4(bArr2, bArr, A04, A0C);
            }
        }
        bArr = bArr2;
        return new AnonymousClass4R4(bArr2, bArr, A04, A0C);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:26:0x0070 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:64:0x00f6 */
    /* JADX WARN: Type inference failed for: r0v32 */
    /* JADX WARN: Type inference failed for: r0v50 */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01b9, code lost:
        if (r12 != 0) goto L_0x0180;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a4, code lost:
        if (r1 != 0) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x013b, code lost:
        if (r11 == 0) goto L_0x0114;
     */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x011b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0010 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(android.graphics.Canvas r21, android.graphics.Paint r22, byte[] r23, int[] r24, int r25, int r26, int r27) {
        /*
        // Method dump skipped, instructions count: 472
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95524ds.A02(android.graphics.Canvas, android.graphics.Paint, byte[], int[], int, int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        r4 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int[] A03() {
        /*
            r7 = 16
            int[] r6 = new int[r7]
            r0 = 0
            r6[r0] = r0
            r5 = 1
        L_0x0008:
            r4 = 255(0xff, float:3.57E-43)
        L_0x000a:
            r0 = r5 & 2
            r3 = 0
            if (r0 == 0) goto L_0x0011
            r3 = 255(0xff, float:3.57E-43)
        L_0x0011:
            r0 = r5 & 4
            r2 = 0
            if (r0 == 0) goto L_0x0018
            r2 = 255(0xff, float:3.57E-43)
        L_0x0018:
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            int r0 = r4 << 16
            r1 = r1 | r0
            int r0 = r3 << 8
            r1 = r1 | r0
            r1 = r1 | r2
        L_0x0021:
            r6[r5] = r1
            int r5 = r5 + 1
            if (r5 >= r7) goto L_0x004e
            r1 = 8
            r0 = r5 & 1
            if (r5 >= r1) goto L_0x0031
            r4 = 0
            if (r0 == 0) goto L_0x000a
            goto L_0x0008
        L_0x0031:
            r4 = 127(0x7f, float:1.78E-43)
            r3 = 0
            if (r0 == 0) goto L_0x0038
            r3 = 127(0x7f, float:1.78E-43)
        L_0x0038:
            r0 = r5 & 2
            r2 = 0
            if (r0 == 0) goto L_0x003f
            r2 = 127(0x7f, float:1.78E-43)
        L_0x003f:
            r0 = r5 & 4
            if (r0 != 0) goto L_0x0044
            r4 = 0
        L_0x0044:
            r1 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            int r0 = r3 << 16
            r1 = r1 | r0
            int r0 = r2 << 8
            r1 = r1 | r0
            r1 = r1 | r4
            goto L_0x0021
        L_0x004e:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95524ds.A03():int[]");
    }

    public static int[] A04() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int[] iArr = new int[256];
        iArr[0] = 0;
        int i7 = 0;
        do {
            int i8 = 255;
            if (i7 < 8) {
                i4 = 0;
                if ((i7 & 1) != 0) {
                    i4 = 255;
                }
                i5 = 0;
                if ((i7 & 2) != 0) {
                    i5 = 255;
                }
                if ((i7 & 4) == 0) {
                    i8 = 0;
                }
                i6 = 1056964608;
            } else {
                int i9 = i7 & 136;
                int i10 = 170;
                int i11 = 85;
                if (i9 == 0) {
                    int i12 = 0;
                    if ((i7 & 1) != 0) {
                        i12 = 85;
                    }
                    int i13 = 0;
                    if ((i7 & 16) != 0) {
                        i13 = 170;
                    }
                    i = i12 + i13;
                    int i14 = 0;
                    if ((i7 & 2) != 0) {
                        i14 = 85;
                    }
                    int i15 = 0;
                    if ((i7 & 32) != 0) {
                        i15 = 170;
                    }
                    i2 = i14 + i15;
                    if ((i7 & 4) == 0) {
                        i11 = 0;
                    }
                    if ((i7 & 64) == 0) {
                        i10 = 0;
                    }
                } else if (i9 != 8) {
                    i10 = 43;
                    if (i9 != 128) {
                        if (i9 == 136) {
                            int i16 = 0;
                            if ((i7 & 1) != 0) {
                                i16 = 43;
                            }
                            int i17 = 0;
                            if ((i7 & 16) != 0) {
                                i17 = 85;
                            }
                            i = i16 + i17;
                            int i18 = 0;
                            if ((i7 & 2) != 0) {
                                i18 = 43;
                            }
                            int i19 = 0;
                            if ((i7 & 32) != 0) {
                                i19 = 85;
                            }
                            i2 = i18 + i19;
                            if ((i7 & 4) == 0) {
                                i10 = 0;
                            }
                        }
                        i7++;
                    } else {
                        int i20 = 0;
                        if ((i7 & 1) != 0) {
                            i20 = 43;
                        }
                        int i21 = i20 + 127;
                        int i22 = 0;
                        if ((i7 & 16) != 0) {
                            i22 = 85;
                        }
                        i = i21 + i22;
                        int i23 = 0;
                        if ((i7 & 2) != 0) {
                            i23 = 43;
                        }
                        int i24 = i23 + 127;
                        int i25 = 0;
                        if ((i7 & 32) != 0) {
                            i25 = 85;
                        }
                        i2 = i24 + i25;
                        if ((i7 & 4) == 0) {
                            i10 = 0;
                        }
                        i10 += 127;
                    }
                    if ((i7 & 64) == 0) {
                        i11 = 0;
                    }
                } else {
                    int i26 = 0;
                    if ((i7 & 1) != 0) {
                        i26 = 85;
                    }
                    int i27 = 0;
                    if ((i7 & 16) != 0) {
                        i27 = 170;
                    }
                    i4 = i26 + i27;
                    int i28 = 0;
                    if ((i7 & 2) != 0) {
                        i28 = 85;
                    }
                    int i29 = 0;
                    if ((i7 & 32) != 0) {
                        i29 = 170;
                    }
                    i5 = i28 + i29;
                    if ((i7 & 4) == 0) {
                        i11 = 0;
                    }
                    if ((i7 & 64) == 0) {
                        i10 = 0;
                    }
                    i8 = i11 + i10;
                    i6 = 2130706432;
                }
                i3 = -16777216 | (i << 16) | (i2 << 8) | (i10 + i11);
                iArr[i7] = i3;
                i7++;
            }
            i3 = i6 | (i4 << 16) | (i5 << 8) | i8;
            iArr[i7] = i3;
            i7++;
        } while (i7 < 256);
        return iArr;
    }
}
