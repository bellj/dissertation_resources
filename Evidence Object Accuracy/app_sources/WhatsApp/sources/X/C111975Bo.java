package X;

import java.security.PrivilegedAction;

/* renamed from: X.5Bo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C111975Bo implements PrivilegedAction {
    public final /* synthetic */ String A00;

    public C111975Bo(String str) {
        this.A00 = str;
    }

    @Override // java.security.PrivilegedAction
    public Object run() {
        try {
            return Class.forName(this.A00);
        } catch (Exception unused) {
            return null;
        }
    }
}
