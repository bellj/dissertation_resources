package X;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.4eA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95674eA {
    public static final Class A00 = AnonymousClass1TA.A00(C95674eA.class, "java.security.cert.PKIXRevocationChecker");
    public static final String A01 = C114715Mu.A05.A01;
    public static final String A02 = C114715Mu.A06.A01;
    public static final String A03 = C114715Mu.A0B.A01;
    public static final String A04 = C114715Mu.A08.A01;
    public static final String A05 = C114715Mu.A0C.A01;
    public static final String A06 = C114715Mu.A0G.A01;
    public static final String A07 = C114715Mu.A0K.A01;
    public static final String A08 = C114715Mu.A0L.A01;
    public static final String A09 = C114715Mu.A0N.A01;
    public static final String A0A = C114715Mu.A0P.A01;
    public static final String A0B = C114715Mu.A0Q.A01;
    public static final String A0C = C114715Mu.A0U.A01;
    public static final String[] A0D = C72453ed.A1b();

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        r0 = X.AnonymousClass5NG.A01(r1).A0B();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        if (r0 >= r5) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.security.cert.CertPath r3, int r4, int r5) {
        /*
            java.security.cert.X509Certificate r1 = X.C72453ed.A0y(r3, r4)
            java.lang.String r0 = X.C95674eA.A0A     // Catch: Exception -> 0x003a
            X.1TL r0 = X.C95644e7.A07(r0, r1)     // Catch: Exception -> 0x003a
            X.5Na r0 = X.AbstractC114775Na.A04(r0)     // Catch: Exception -> 0x003a
            if (r0 == 0) goto L_0x0039
            java.util.Enumeration r2 = r0.A0C()
        L_0x0014:
            boolean r0 = r2.hasMoreElements()
            if (r0 == 0) goto L_0x0039
            java.lang.Object r0 = r2.nextElement()     // Catch: IllegalArgumentException -> 0x0031
            X.5NU r1 = X.AnonymousClass5NU.A01(r0)     // Catch: IllegalArgumentException -> 0x0031
            int r0 = r1.A00     // Catch: IllegalArgumentException -> 0x0031
            if (r0 != 0) goto L_0x0014
            X.5NG r0 = X.AnonymousClass5NG.A01(r1)     // Catch: IllegalArgumentException -> 0x0031
            int r0 = r0.A0B()     // Catch: IllegalArgumentException -> 0x0031
            if (r0 >= r5) goto L_0x0039
            return r0
        L_0x0031:
            r1 = move-exception
            java.lang.String r0 = "Policy constraints extension contents cannot be decoded."
            X.5Hh r0 = X.C113385Hh.A00(r0, r1, r3, r4)
            throw r0
        L_0x0039:
            return r5
        L_0x003a:
            r1 = move-exception
            java.lang.String r0 = "Policy constraints extension cannot be decoded."
            X.5Hh r0 = X.C113385Hh.A00(r0, r1, r3, r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95674eA.A00(java.security.cert.CertPath, int, int):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        r0 = X.AnonymousClass5NG.A01(r2).A0B();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002f, code lost:
        if (r0 >= r6) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(java.security.cert.CertPath r4, int r5, int r6) {
        /*
            java.security.cert.X509Certificate r1 = X.C72453ed.A0y(r4, r5)
            java.lang.String r0 = X.C95674eA.A0A     // Catch: Exception -> 0x003b
            X.1TL r0 = X.C95644e7.A07(r0, r1)     // Catch: Exception -> 0x003b
            X.5Na r0 = X.AbstractC114775Na.A04(r0)     // Catch: Exception -> 0x003b
            if (r0 == 0) goto L_0x003a
            java.util.Enumeration r3 = r0.A0C()
        L_0x0014:
            boolean r0 = r3.hasMoreElements()
            if (r0 == 0) goto L_0x003a
            java.lang.Object r0 = r3.nextElement()     // Catch: IllegalArgumentException -> 0x0032
            X.5NU r2 = X.AnonymousClass5NU.A01(r0)     // Catch: IllegalArgumentException -> 0x0032
            int r1 = r2.A00     // Catch: IllegalArgumentException -> 0x0032
            r0 = 1
            if (r1 != r0) goto L_0x0014
            X.5NG r0 = X.AnonymousClass5NG.A01(r2)     // Catch: IllegalArgumentException -> 0x0032
            int r0 = r0.A0B()     // Catch: IllegalArgumentException -> 0x0032
            if (r0 >= r6) goto L_0x003a
            return r0
        L_0x0032:
            r1 = move-exception
            java.lang.String r0 = "Policy constraints extension contents cannot be decoded."
            X.5Hh r0 = X.C113385Hh.A00(r0, r1, r4, r5)
            throw r0
        L_0x003a:
            return r6
        L_0x003b:
            r1 = move-exception
            java.lang.String r0 = "Policy constraints extension cannot be decoded."
            X.5Hh r0 = X.C113385Hh.A00(r0, r1, r4, r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95674eA.A01(java.security.cert.CertPath, int, int):int");
    }

    public static int A02(CertPath certPath, int i, int i2) {
        int A0B2;
        try {
            AnonymousClass5NG A002 = AnonymousClass5NG.A00(C95644e7.A07(A06, C72453ed.A0y(certPath, i)));
            return (A002 == null || (A0B2 = A002.A0B()) >= i2) ? i2 : A0B2;
        } catch (Exception e) {
            throw C113385Hh.A00("Inhibit any-policy extension cannot be decoded.", e, certPath, i);
        }
    }

    public static int A03(CertPath certPath, int i, int i2) {
        AnonymousClass5NG r0;
        int intValue;
        try {
            C114585Mh A002 = C114585Mh.A00(C95644e7.A07(A02, C72453ed.A0y(certPath, i)));
            return (A002 == null || (r0 = A002.A01) == null || (intValue = new BigInteger(r0.A01).intValue()) >= i2) ? i2 : intValue;
        } catch (Exception e) {
            throw C113385Hh.A00("Basic constraints extension cannot be decoded.", e, certPath, i);
        }
    }

    public static int A04(CertPath certPath, int i, int i2) {
        try {
            AbstractC114775Na A042 = AbstractC114775Na.A04((Object) C95644e7.A07(A0A, C72453ed.A0y(certPath, i)));
            if (A042 != null) {
                Enumeration A0C2 = A042.A0C();
                while (A0C2.hasMoreElements()) {
                    AnonymousClass5NU r2 = (AnonymousClass5NU) A0C2.nextElement();
                    if (r2.A00 == 0) {
                        try {
                            if (AnonymousClass5NG.A01(r2).A0B() == 0) {
                                return 0;
                            }
                        } catch (Exception e) {
                            throw C113385Hh.A00("Policy constraints requireExplicitPolicy field could not be decoded.", e, certPath, i);
                        }
                    }
                }
            }
            return i2;
        } catch (AnonymousClass4C6 e2) {
            throw C113385Hh.A00("Policy constraints could not be decoded.", e2, certPath, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r10.isEmpty() != false) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x011d, code lost:
        if (r13 != null) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x011f, code lost:
        r8 = r8 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0121, code lost:
        if (r8 < 0) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0123, code lost:
        r3 = r14[r8];
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x012a, code lost:
        if (r2 >= r3.size()) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x012c, code lost:
        r1 = (X.AnonymousClass5C0) r3.get(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x013a, code lost:
        if ((!r1.A03.isEmpty()) != false) goto L_0x0140;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x013c, code lost:
        r13 = X.C95644e7.A08(r13, r1, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0140, code lost:
        r2 = r2 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass5C0 A05(java.security.cert.CertPath r9, java.util.Set r10, java.util.Set r11, X.C112085Bz r12, X.AnonymousClass5C0 r13, java.util.List[] r14, int r15) {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95674eA.A05(java.security.cert.CertPath, java.util.Set, java.util.Set, X.5Bz, X.5C0, java.util.List[], int):X.5C0");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0096, code lost:
        r1 = X.C12970iu.A12();
        r1.add(r7);
        r0 = new X.AnonymousClass5C0(r7, r3, X.C12960it.A0l(), r1, r23, r4, false);
        r3.A03.add(r0);
        r0.A02 = r3;
        r28[r4].add(r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass5C0 A06(java.security.cert.CertPath r25, java.util.Set r26, X.AnonymousClass5C0 r27, java.util.List[] r28, int r29, int r30, boolean r31) {
        /*
        // Method dump skipped, instructions count: 521
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95674eA.A06(java.security.cert.CertPath, java.util.Set, X.5C0, java.util.List[], int, int, boolean):X.5C0");
    }

    public static AnonymousClass5C0 A07(CertPath certPath, AnonymousClass5C0 r3, int i) {
        try {
            if (AbstractC114775Na.A04((Object) C95644e7.A07(A03, C72453ed.A0y(certPath, i))) == null) {
                return null;
            }
            return r3;
        } catch (AnonymousClass4C6 e) {
            throw C113385Hh.A00("Could not read certificate policies extension from certificate.", e, certPath, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009e, code lost:
        r9 = r21[r19].iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a8, code lost:
        if (r9.hasNext() == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00aa, code lost:
        r8 = (X.AnonymousClass5C0) r9.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00ba, code lost:
        if ("2.5.29.32.0".equals(r8.getValidPolicy()) == false) goto L_0x00a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bc, code lost:
        r18 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00be, code lost:
        r9 = X.C95674eA.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c6, code lost:
        r11 = ((X.AbstractC114775Na) X.C95644e7.A07(r9, r4)).A0C();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ce, code lost:
        if (r11.hasMoreElements() == false) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00d0, code lost:
        r10 = r11.nextElement();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d4, code lost:
        if (r10 == null) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d8, code lost:
        if ((r10 instanceof X.AnonymousClass5MR) != false) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        r10 = new X.AnonymousClass5MR(X.AbstractC114775Na.A04(r10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e4, code lost:
        r10 = (X.AnonymousClass5MR) r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ee, code lost:
        if ("2.5.29.32.0".equals(r10.A00.A01) == false) goto L_0x00ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        r18 = X.C95644e7.A06(r10.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00fa, code lost:
        if (r4.getCriticalExtensionOIDs() == null) goto L_0x0133;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00fc, code lost:
        r20 = r4.getCriticalExtensionOIDs().contains(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0104, code lost:
        r15 = (X.AnonymousClass5C0) r8.getParent();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0112, code lost:
        if ("2.5.29.32.0".equals(r15.getValidPolicy()) == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0114, code lost:
        r13 = new X.AnonymousClass5C0(r14, r15, X.C12960it.A0l(), (java.util.Set) r1.get(r14), r18, r19, r20);
        r15.A03.add(r13);
        r13.A02 = r15;
        r21[r19].add(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0133, code lost:
        r20 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0187, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x018e, code lost:
        throw X.C113385Hh.A00("Policy qualifier info set could not be decoded.", r1, r19, r22);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x018f, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0197, code lost:
        throw new java.security.cert.CertPathValidatorException("Policy information could not be decoded.", r4, r19, r22);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0198, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x019f, code lost:
        throw X.C113385Hh.A00("Certificate policies extension could not be decoded.", r1, r19, r22);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x006b, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass5C0 A08(java.security.cert.CertPath r19, X.AnonymousClass5C0 r20, java.util.List[] r21, int r22, int r23) {
        /*
        // Method dump skipped, instructions count: 425
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95674eA.A08(java.security.cert.CertPath, X.5C0, java.util.List[], int, int):X.5C0");
    }

    public static void A09(PublicKey publicKey, CertPath certPath, X509Certificate x509Certificate, Date date, AnonymousClass5N2 r14, AnonymousClass5WR r15, C112085Bz r16, int i, boolean z) {
        X509Certificate A0y = C72453ed.A0y(certPath, i);
        if (!z) {
            try {
                String sigProvider = r16.A01.getSigProvider();
                if (sigProvider == null) {
                    A0y.verify(publicKey);
                } else {
                    A0y.verify(publicKey, sigProvider);
                }
            } catch (GeneralSecurityException e) {
                throw C113385Hh.A00("Could not validate certificate signature.", e, certPath, i);
            }
        }
        try {
            Date A042 = C95644e7.A04(certPath, date, r16.A00, i);
            try {
                A0y.checkValidity(A042);
                if (r15 != null) {
                    r15.AIv(new AnonymousClass4TK(publicKey, certPath, x509Certificate, A042, r16, i));
                    r15.check(A0y);
                }
                AnonymousClass5N2 A012 = C95354dZ.A01(A0y);
                if (!A012.equals(r14)) {
                    StringBuilder A0k = C12960it.A0k("IssuerName(");
                    A0k.append(A012);
                    A0k.append(") does not match SubjectName(");
                    A0k.append(r14);
                    throw C113385Hh.A00(C12960it.A0d(") of signing certificate.", A0k), null, certPath, i);
                }
            } catch (CertificateExpiredException e2) {
                throw C113385Hh.A00(C12960it.A0d(e2.getMessage(), C12960it.A0j("Could not validate certificate: ")), e2, certPath, i);
            } catch (CertificateNotYetValidException e3) {
                throw C113385Hh.A00(C12960it.A0d(e3.getMessage(), C12960it.A0j("Could not validate certificate: ")), e3, certPath, i);
            }
        } catch (AnonymousClass4C6 e4) {
            throw C113385Hh.A00("Could not validate time of certificate.", e4, certPath, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0318, code lost:
        if (X.C114695Ms.A00(r0).A03 == false) goto L_0x031a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x0538  */
    /* JADX WARNING: Removed duplicated region for block: B:390:0x015d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:411:0x0542 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:425:0x00ee A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0A(java.security.PublicKey r26, java.security.cert.X509Certificate r27, java.security.cert.X509Certificate r28, java.util.Date r29, java.util.Date r30, java.util.List r31, X.C114685Mr r32, X.AnonymousClass4TK r33, X.C112085Bz r34, X.AnonymousClass5S2 r35, X.C90584Ol r36, X.C94774cW r37) {
        /*
        // Method dump skipped, instructions count: 1633
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95674eA.A0A(java.security.PublicKey, java.security.cert.X509Certificate, java.security.cert.X509Certificate, java.util.Date, java.util.Date, java.util.List, X.5Mr, X.4TK, X.5Bz, X.5S2, X.4Ol, X.4cW):void");
    }

    public static void A0B(CertPath certPath, int i) {
        try {
            AbstractC114775Na A042 = AbstractC114775Na.A04((Object) C95644e7.A07(A0B, C72453ed.A0y(certPath, i)));
            if (A042 != null) {
                for (int i2 = 0; i2 < A042.A0B(); i2++) {
                    try {
                        AbstractC114775Na A043 = AbstractC114775Na.A04(A042.A0D(i2));
                        AnonymousClass1TK A002 = AnonymousClass1TK.A00(A043.A0D(0));
                        AnonymousClass1TK A003 = AnonymousClass1TK.A00(AbstractC114775Na.A01(A043));
                        if ("2.5.29.32.0".equals(A002.A01)) {
                            throw new CertPathValidatorException("IssuerDomainPolicy is anyPolicy", null, certPath, i);
                        } else if ("2.5.29.32.0".equals(A003.A01)) {
                            throw new CertPathValidatorException("SubjectDomainPolicy is anyPolicy", null, certPath, i);
                        }
                    } catch (Exception e) {
                        throw C113385Hh.A00("Policy mappings extension contents could not be decoded.", e, certPath, i);
                    }
                }
            }
        } catch (AnonymousClass4C6 e2) {
            throw C113385Hh.A00("Policy mappings extension could not be decoded.", e2, certPath, i);
        }
    }

    public static void A0C(CertPath certPath, int i) {
        try {
            C114585Mh A002 = C114585Mh.A00(C95644e7.A07(A02, C72453ed.A0y(certPath, i)));
            if (A002 != null) {
                AnonymousClass5NF r0 = A002.A00;
                if (r0 == null || r0.A00 == 0) {
                    throw new CertPathValidatorException("Not a CA certificate", null, certPath, i);
                }
                return;
            }
            throw new CertPathValidatorException("Intermediate certificate lacks BasicConstraints", null, certPath, i);
        } catch (Exception e) {
            throw C113385Hh.A00("Basic constraints extension cannot be decoded.", e, certPath, i);
        }
    }

    public static void A0D(CertPath certPath, int i) {
        boolean[] keyUsage = C72453ed.A0y(certPath, i).getKeyUsage();
        if (keyUsage == null) {
            return;
        }
        if (keyUsage.length <= 5 || !keyUsage[5]) {
            throw C113385Hh.A00("Issuer certificate keyusage extension is critical and does not permit key signing.", null, certPath, i);
        }
    }

    public static void A0E(CertPath certPath, List list, Set set, int i) {
        Certificate certificate = (Certificate) certPath.getCertificates().get(i);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            try {
                ((PKIXCertPathChecker) it.next()).check(certificate, set);
            } catch (CertPathValidatorException e) {
                throw new CertPathValidatorException(e.getMessage(), e.getCause(), certPath, i);
            }
        }
        if (!set.isEmpty()) {
            throw C113385Hh.A00(C12960it.A0b("Certificate has unsupported critical extension: ", set), null, certPath, i);
        }
    }

    public static void A0F(CertPath certPath, List list, Set set, int i) {
        Certificate certificate = (Certificate) certPath.getCertificates().get(i);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            try {
                ((PKIXCertPathChecker) it.next()).check(certificate, set);
            } catch (CertPathValidatorException e) {
                throw C113385Hh.A00(e.getMessage(), e, certPath, i);
            } catch (Exception e2) {
                throw new CertPathValidatorException("Additional certificate path checker failed.", e2, certPath, i);
            }
        }
        if (!set.isEmpty()) {
            throw C113385Hh.A00(C12960it.A0b("Certificate has unsupported critical extension: ", set), null, certPath, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0182, code lost:
        if (java.util.Arrays.equals(r12, r7) != false) goto L_0x0184;
     */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x051c  */
    /* JADX WARNING: Removed duplicated region for block: B:339:0x0649  */
    /* JADX WARNING: Removed duplicated region for block: B:419:0x0253 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:430:0x01ee A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:469:0x03b0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:480:0x034b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:508:0x0523 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:511:0x0524 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:530:0x0650 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:534:0x0651 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0G(java.security.cert.CertPath r26, X.AnonymousClass4Y9 r27, int r28) {
        /*
        // Method dump skipped, instructions count: 1710
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95674eA.A0G(java.security.cert.CertPath, X.4Y9, int):void");
    }

    public static void A0H(CertPath certPath, AnonymousClass4Y9 r14, int i, boolean z) {
        List<? extends Certificate> certificates = certPath.getCertificates();
        X509Certificate A0K = C72463ee.A0K(certificates, i);
        int size = certificates.size();
        int i2 = size - i;
        if (!C72463ee.A0Z(A0K) || (i2 >= size && !z)) {
            try {
                try {
                    AbstractC114775Na A042 = AbstractC114775Na.A04(C95354dZ.A02(A0K));
                    try {
                        C95534du r1 = r14.A00;
                        r1.A08(AnonymousClass5N2.A00(A042));
                        try {
                            r1.A07(AnonymousClass5N2.A00(A042));
                            try {
                                C114705Mt A002 = C114705Mt.A00(C95644e7.A07(A0C, A0K));
                                AnonymousClass5N2 A003 = AnonymousClass5N2.A00(A042);
                                AnonymousClass1TK r11 = C114895Nm.A0F;
                                C114605Mj[] r10 = A003.A04;
                                int length = r10.length;
                                C114605Mj[] r5 = new C114605Mj[length];
                                int i3 = 0;
                                for (int i4 = 0; i4 != length; i4++) {
                                    C114605Mj r3 = r10[i4];
                                    AnonymousClass5NV r12 = r3.A00;
                                    int length2 = r12.A01.length;
                                    int i5 = 0;
                                    while (true) {
                                        if (i5 >= length2) {
                                            break;
                                        } else if (AnonymousClass5MW.A00(r12.A01[i5]).A01.A04(r11)) {
                                            i3++;
                                            r5[i3] = r3;
                                            break;
                                        } else {
                                            i5++;
                                        }
                                    }
                                }
                                if (i3 < length) {
                                    C114605Mj[] r0 = new C114605Mj[i3];
                                    System.arraycopy(r5, 0, r0, 0, i3);
                                    r5 = r0;
                                }
                                for (int i6 = 0; i6 != r5.length; i6++) {
                                    AnonymousClass5N1 r02 = new AnonymousClass5N1(((AnonymousClass5VP) r5[i6].A03().A00).AGy());
                                    try {
                                        r14.A01(r02);
                                        r14.A00(r02);
                                    } catch (AnonymousClass4C7 e) {
                                        throw new CertPathValidatorException("Subtree check for certificate subject alternative email failed.", e, certPath, i);
                                    }
                                }
                                if (A002 != null) {
                                    try {
                                        AnonymousClass5N1[] r03 = A002.A00;
                                        int length3 = r03.length;
                                        AnonymousClass5N1[] r13 = new AnonymousClass5N1[length3];
                                        System.arraycopy(r03, 0, r13, 0, length3);
                                        for (int i7 = 0; i7 < length3; i7++) {
                                            try {
                                                r14.A01(r13[i7]);
                                                r14.A00(r13[i7]);
                                            } catch (AnonymousClass4C7 e2) {
                                                throw new CertPathValidatorException("Subtree check for certificate subject alternative name failed.", e2, certPath, i);
                                            }
                                        }
                                    } catch (Exception e3) {
                                        throw new CertPathValidatorException("Subject alternative name contents could not be decoded.", e3, certPath, i);
                                    }
                                }
                            } catch (Exception e4) {
                                throw new CertPathValidatorException("Subject alternative name extension could not be decoded.", e4, certPath, i);
                            }
                        } catch (C87514Bu e5) {
                            throw new AnonymousClass4C7(e5.getMessage(), e5);
                        }
                    } catch (C87514Bu e6) {
                        throw new AnonymousClass4C7(e6.getMessage(), e6);
                    }
                } catch (AnonymousClass4C7 e7) {
                    throw new CertPathValidatorException("Subtree check for certificate subject failed.", e7, certPath, i);
                }
            } catch (Exception e8) {
                throw new CertPathValidatorException("Exception extracting subject name when checking subtrees.", e8, certPath, i);
            }
        }
    }
}
