package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.4EV  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4EV {
    public static C460124c A00(C16590pI r8, C32631cT r9) {
        int i;
        Context context = r8.A00;
        String string = context.getString(R.string.notification_payment_step_up_required_title);
        String A02 = r8.A02(R.string.notification_payment_step_up_required_message);
        String string2 = context.getString(R.string.notification_payment_step_up_required_title);
        String str = r9.A03;
        if (str != null) {
            switch (str.hashCode()) {
                case -1504126555:
                    if (str.equals("DOCUMENT_UPLOAD")) {
                        string = r8.A02(R.string.notification_payment_document_upload_step_up_required_title);
                        A02 = r8.A02(R.string.notification_payment_document_upload_step_up_required_message);
                        i = R.string.notification_payment_document_upload_step_up_required_cta_text;
                        string2 = r8.A02(i);
                        break;
                    }
                    break;
                case 64442123:
                    if (str.equals("CS_GC")) {
                        string = r8.A02(R.string.notification_payment_cs_gift_card_title);
                        A02 = r8.A02(R.string.notification_payment_cs_gift_card_message);
                        i = R.string.notification_payment_cs_gift_card_cta_text;
                        string2 = r8.A02(i);
                        break;
                    }
                    break;
                case 91216024:
                    if (str.equals("DOCUMENT_REUPLOAD")) {
                        string = r8.A02(R.string.notification_payment_document_reupload_step_up_required_title);
                        A02 = r8.A02(R.string.notification_payment_document_reupload_step_up_required_message);
                        i = R.string.notification_payment_document_reupload_step_up_required_cta_text;
                        string2 = r8.A02(i);
                        break;
                    }
                    break;
                case 1997714093:
                    if (str.equals("CS_OTA") && "UPI".equals(r9.A01)) {
                        string = r8.A02(R.string.notification_payment_cs_step_up_required_title);
                        A02 = r8.A02(R.string.notification_payment_cs_step_up_required_message);
                        i = R.string.notification_payment_cs_step_up_required_cta_text;
                        string2 = r8.A02(i);
                        break;
                    }
                    break;
            }
        }
        return new C460124c(null, r9, r9.A02, string, A02, string2, "P2P", 1, false);
    }
}
