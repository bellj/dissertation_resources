package X;

import android.content.Intent;

/* renamed from: X.3pP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78703pP extends AbstractDialogInterface$OnClickListenerC472029l {
    public final /* synthetic */ Intent A00;
    public final /* synthetic */ AbstractC116815Wz A01;

    public C78703pP(Intent intent, AbstractC116815Wz r2) {
        this.A00 = intent;
        this.A01 = r2;
    }

    @Override // X.AbstractDialogInterface$OnClickListenerC472029l
    public final void A00() {
        Intent intent = this.A00;
        if (intent != null) {
            this.A01.startActivityForResult(intent, 2);
        }
    }
}
