package X;

import android.content.Context;

/* renamed from: X.0Gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03110Gd extends AnonymousClass0Zt {
    public C03110Gd(Context context, AbstractC11500gO r3) {
        super(C05990Rt.A00(context, r3).A03);
    }

    @Override // X.AnonymousClass0Zt
    public boolean A01(C004401z r2) {
        return r2.A09.A07;
    }

    @Override // X.AnonymousClass0Zt
    public boolean A02(Object obj) {
        return !((Boolean) obj).booleanValue();
    }
}
