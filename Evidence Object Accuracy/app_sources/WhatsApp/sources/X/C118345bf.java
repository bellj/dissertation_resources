package X;

import android.os.Bundle;

/* renamed from: X.5bf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118345bf extends AnonymousClass0Yo {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C128375w0 A01;

    public C118345bf(Bundle bundle, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118015b8.class)) {
            C128375w0 r0 = this.A01;
            C129675y7 r4 = r0.A0p;
            C22900zp r3 = r0.A0y;
            return new C118015b8(this.A00, r4, r0.A0t, r3);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviReviewVideoSelfieViewModel");
    }
}
