package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape0S0101200_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0100100_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1;

/* renamed from: X.4vp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106514vp implements AnonymousClass5XL {
    public final /* synthetic */ C76563lq A00;

    @Override // X.AnonymousClass5XL
    public /* synthetic */ void ATA() {
    }

    @Override // X.AnonymousClass5XL
    public /* synthetic */ void ATB(long j) {
    }

    public /* synthetic */ C106514vp(C76563lq r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5XL
    public void AMY(Exception exc) {
        C92474Wb r2 = this.A00.A0H;
        Handler handler = r2.A00;
        if (handler != null) {
            C12980iv.A18(handler, r2, exc, 5);
        }
    }

    @Override // X.AnonymousClass5XL
    public void ATv(long j) {
        C92474Wb r3 = this.A00.A0H;
        Handler handler = r3.A00;
        if (handler != null) {
            handler.post(new RunnableBRunnable0Shape1S0100100_I1(r3, j, 0));
        }
    }

    @Override // X.AnonymousClass5XL
    public void ATw() {
        this.A00.A0C = true;
    }

    @Override // X.AnonymousClass5XL
    public void AW9(boolean z) {
        C92474Wb r3 = this.A00.A0H;
        Handler handler = r3.A00;
        if (handler != null) {
            handler.post(new RunnableBRunnable0Shape1S0110000_I1(r3, 0, z));
        }
    }

    @Override // X.AnonymousClass5XL
    public void AXy(int i, long j, long j2) {
        C92474Wb r2 = this.A00.A0H;
        Handler handler = r2.A00;
        if (handler != null) {
            handler.post(new RunnableBRunnable0Shape0S0101200_I1(r2, i, 0, j, j2));
        }
    }
}
