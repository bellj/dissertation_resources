package X;

import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

/* renamed from: X.3G1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3G1 {
    public static Bundle A00(ContentResolver contentResolver, Uri uri, Bundle bundle, C63613Cg r5, String str) {
        r5.A00(uri);
        ContentProviderClient acquireUnstableContentProviderClient = contentResolver.acquireUnstableContentProviderClient(uri);
        if (acquireUnstableContentProviderClient != null) {
            try {
                Bundle call = acquireUnstableContentProviderClient.call(str, null, bundle);
                if (Build.VERSION.SDK_INT >= 24) {
                    acquireUnstableContentProviderClient.close();
                    return call;
                }
                acquireUnstableContentProviderClient.release();
                return call;
            } catch (Throwable th) {
                if (Build.VERSION.SDK_INT >= 24) {
                    acquireUnstableContentProviderClient.close();
                    throw th;
                }
                acquireUnstableContentProviderClient.release();
                throw th;
            }
        } else {
            throw C12960it.A0U("Failed to acquire client");
        }
    }

    public static void A01(Bundle bundle, AnonymousClass3CB r4, String str) {
        String str2;
        Bundle bundle2 = bundle.getBundle("@exception@");
        if (bundle2 != null) {
            C93614aS A00 = r4.A00(bundle2);
            AnonymousClass5HN r2 = new AnonymousClass5HN(A00);
            Throwable th = A00.A03;
            if (th == null) {
                String str3 = A00.A02;
                if (str3 == null) {
                    str3 = C12960it.A0f(C12960it.A0k("Remote error code "), A00.A00);
                }
                th = new AnonymousClass5HO(A00, str3);
            }
            C93614aS.A00(th, r2);
            String message = th.getMessage();
            StringBuilder A0k = C12960it.A0k("Exception in provider when invoking ");
            A0k.append(str);
            A0k.append("(): ");
            A0k.append(C12980iv.A0r(th));
            if (message != null) {
                str2 = C12960it.A0d(message, C12960it.A0k(": "));
            } else {
                str2 = "";
            }
            throw new C87404Bj(C12960it.A0d(str2, A0k), th);
        }
    }
}
