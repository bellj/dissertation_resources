package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.0B4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0B4 extends ViewGroup.MarginLayoutParams {
    public AnonymousClass0B4() {
        super(-1, -1);
    }

    public AnonymousClass0B4(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass0B4(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
