package X;

import com.whatsapp.R;

/* renamed from: X.5r9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C125375r9 {
    public static final int[] A00 = {R.attr.charSize, R.attr.charSpacing, R.attr.fontColor, R.attr.lineStroke, R.attr.lineStrokeColor, R.attr.numChars, R.attr.textBottomPadding};
    public static final int[] A01 = {R.attr.showBack, R.attr.showLock, R.attr.title, R.attr.titleAlignment};
    public static final int[] A02 = {R.attr.autoScaleTextSize, R.attr.formatWithCommas};
    public static final int[] A03 = {R.attr.see_more_icon, R.attr.see_more_text, R.attr.size};
}
