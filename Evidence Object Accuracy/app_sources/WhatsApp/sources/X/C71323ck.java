package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.3ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71323ck implements Comparator {
    public final C15570nT A00;
    public final C15610nY A01;
    public final Collator A02;
    public final boolean A03 = true;

    public C71323ck(C15570nT r3, C15610nY r4) {
        this.A00 = r3;
        this.A01 = r4;
        Collator instance = Collator.getInstance(C12970iu.A14(r4.A04));
        instance.setDecomposition(1);
        this.A02 = instance;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARN: Type inference failed for: r0v13 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v8 */
    /* JADX WARN: Type inference failed for: r0v16 */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006d A[RETURN] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* renamed from: A00 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int compare(X.AnonymousClass3FF r18, X.AnonymousClass3FF r19) {
        /*
            r17 = this;
            r3 = r17
            X.0nT r1 = r3.A00
            r6 = r18
            X.0n3 r7 = r6.A02
            com.whatsapp.jid.Jid r0 = r7.A0D
            boolean r4 = r1.A0F(r0)
            r5 = r19
            X.0n3 r12 = r5.A02
            com.whatsapp.jid.Jid r0 = r12.A0D
            boolean r0 = r1.A0F(r0)
            r8 = -1
            r2 = 1
            if (r4 != r0) goto L_0x006a
            int r0 = r6.A01
            r1 = 0
            boolean r4 = X.C12960it.A1S(r0)
            int r0 = r5.A01
            if (r0 == 0) goto L_0x0028
            r1 = 1
        L_0x0028:
            if (r4 != r1) goto L_0x006a
            X.0nY r6 = r3.A01
            boolean r0 = r6.A0L(r7, r2)
            if (r0 == 0) goto L_0x005e
            r1 = 2
        L_0x0033:
            boolean r0 = r6.A0L(r12, r2)
            if (r0 == 0) goto L_0x0055
            r0 = 2
        L_0x003a:
            if (r1 != r0) goto L_0x0067
            java.text.Collator r2 = r3.A02
            r9 = 0
            boolean r10 = r3.A03
            r11 = 0
            java.lang.String r1 = r6.A0C(r7, r8, r9, r10, r11)
            r13 = -1
            r14 = 0
            r16 = 0
            r11 = r6
            r15 = r10
            java.lang.String r0 = r11.A0C(r12, r13, r14, r15, r16)
            int r0 = r2.compare(r1, r0)
            return r0
        L_0x0055:
            boolean r0 = X.C15610nY.A03(r12)
            boolean r0 = X.C12960it.A1S(r0)
            goto L_0x003a
        L_0x005e:
            boolean r0 = X.C15610nY.A03(r7)
            boolean r1 = X.C12960it.A1S(r0)
            goto L_0x0033
        L_0x0067:
            if (r1 >= r0) goto L_0x006d
            return r8
        L_0x006a:
            if (r4 == 0) goto L_0x006d
            return r8
        L_0x006d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C71323ck.compare(X.3FF, X.3FF):int");
    }
}
