package X;

import java.util.List;

/* renamed from: X.1ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31521ag extends Exception {
    public C31521ag() {
    }

    public C31521ag(String str) {
        super(str);
    }

    public C31521ag(Throwable th) {
        super(th);
    }

    public C31521ag(List list) {
        super("No valid sessions.", (Throwable) list.get(0));
    }
}
