package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.0CR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CR extends AnonymousClass03X implements AbstractC12300hh {
    public final /* synthetic */ AnonymousClass0XQ A00;

    @Override // X.AbstractC12300hh
    public boolean ALb() {
        return false;
    }

    @Override // X.AbstractC12300hh
    public boolean ALc() {
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CR(Context context, AnonymousClass0XQ r4) {
        super(context, null, R.attr.actionOverflowButtonStyle);
        this.A00 = r4;
        setClickable(true);
        setFocusable(true);
        setVisibility(0);
        setEnabled(true);
        AnonymousClass0KD.A00(this, getContentDescription());
        setOnTouchListener(new AnonymousClass0CZ(this, this, r4));
    }

    @Override // android.view.View
    public boolean performClick() {
        if (!super.performClick()) {
            playSoundEffect(0);
            this.A00.A03();
        }
        return true;
    }

    @Override // android.widget.ImageView
    public boolean setFrame(int i, int i2, int i3, int i4) {
        boolean frame = super.setFrame(i, i2, i3, i4);
        Drawable drawable = getDrawable();
        Drawable background = getBackground();
        if (!(drawable == null || background == null)) {
            int width = getWidth();
            int height = getHeight();
            int max = Math.max(width, height) >> 1;
            int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) >> 1;
            int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) >> 1;
            C015607k.A0B(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
        }
        return frame;
    }
}
