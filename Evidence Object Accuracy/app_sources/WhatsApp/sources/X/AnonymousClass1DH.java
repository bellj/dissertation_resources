package X;

import android.content.SharedPreferences;

/* renamed from: X.1DH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1DH implements AbstractC16990q5 {
    public final C15450nH A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1DH(C15450nH r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        SharedPreferences.Editor edit = this.A00.A00.edit();
        edit.remove("attachment_picker_refresh");
        edit.remove("deep_link_redirect_enable");
        edit.remove("search_by_image");
        edit.remove("sigquit_anr_detector_release_updated_rollout");
        edit.remove("anr_fast_logs_upload_rollout");
        edit.remove("emoji_search_unreleased_languages");
        edit.remove("business_profile_updated_ui");
        edit.remove("business_profile_c2e");
        edit.remove("payments_mx_transaction_limit");
        edit.remove("payments_indonesia_webview_access");
        edit.remove("payments_upi_qr_scanning");
        edit.remove("payments_upi_mandate_enabled");
        edit.remove("payments_upi_overdraft_account");
        edit.remove("fan_out_group_call_enabled");
        edit.remove("disk_footprint_logging");
        edit.remove("group_call_video_maximization_enabled");
        edit.remove("new_animation_behavior");
        edit.remove("product_media_attachments");
        edit.remove("stringpacks_mmap_enabled");
        edit.remove("pay_protocol_async_iq");
        edit.remove("enhanced_archive");
        edit.remove("enhanced_block_enabled");
        edit.remove("mms_download_nc_cat");
        edit.remove("document_limit_mb");
        edit.remove("bloks_shops_enabled");
        edit.remove("bloks_cache_enabled");
        edit.remove("bloks_http_enabled");
        edit.remove("smb_business_home_enabled");
        edit.remove("smb_business_home_cards_enabled");
        edit.remove("smb_fb_page_linking_v1");
        edit.remove("smb_wa_to_fb_linking_v1");
        edit.remove("smb_more_tools");
        edit.remove("stickers_animation_media");
        edit.remove("smb_my_message_qr_enabled");
        edit.remove("frequently_forwarded_max");
        edit.remove("frequently_forwarded_max_chats");
        edit.remove("multicast_limit_global");
        edit.remove("frequently_forwarded_messages");
        edit.remove("frequently_forwarded_threshold");
        edit.remove("joinable_group_call_client_version");
        edit.remove("joinable_group_call_version");
        edit.remove("inline_joinable_education_enabled");
        edit.remove("list_message_reception_disabled");
        edit.remove("media_files_deduplication");
        edit.remove("local_backup_in_service");
        edit.remove("abprops_config_codes_enabled");
        edit.remove("broadcast_noncontacts");
        edit.remove("multi_share_file_preview");
        edit.apply();
    }
}
