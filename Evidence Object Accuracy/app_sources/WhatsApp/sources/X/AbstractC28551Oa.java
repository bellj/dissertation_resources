package X;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AccelerateInterpolator;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.1Oa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC28551Oa extends AbstractC28561Ob {
    public static Rect A0c = new Rect();
    public static Drawable A0d;
    public static Drawable A0e;
    public static final Rect A0f = new Rect();
    public static final Rect A0g = new Rect();
    public static final Rect A0h = new Rect();
    public float A00;
    public int A01 = -1;
    public int A02;
    public int A03;
    public int A04 = 0;
    public int A05;
    public int A06;
    public int A07;
    public Paint A08;
    public Rect A09 = new Rect();
    public Drawable A0A;
    public Drawable A0B;
    public View A0C;
    public View A0D;
    public View A0E;
    public AbstractC15710nm A0F;
    public AnonymousClass19I A0G;
    public C53182d3 A0H;
    public C53082ci A0I;
    public AnonymousClass01d A0J;
    public AnonymousClass018 A0K;
    public C14850m9 A0L;
    public C22050yP A0M;
    public C244415n A0N;
    public AbstractC15340mz A0O;
    public AnonymousClass1CY A0P;
    public boolean A0Q = true;
    public boolean A0R;
    public boolean A0S;
    public final int A0T;
    public final Drawable A0U;
    public final Drawable A0V;
    public final Drawable A0W;
    public final Drawable A0X;
    public final Drawable A0Y;
    public final Drawable A0Z;
    public final AbstractC13890kV A0a;
    public final C64533Fx A0b;

    public int getBubbleAlpha() {
        return 255;
    }

    public abstract int getBubbleMarginStart();

    public abstract int getCenteredLayoutId();

    public abstract int getIncomingLayoutId();

    public int getMainChildMaxWidth() {
        return 0;
    }

    public abstract int getOutgoingLayoutId();

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public AbstractC28551Oa(Context context, AbstractC13890kV r8, AbstractC15340mz r9) {
        super(context);
        C64533Fx r0;
        int incomingLayoutId;
        this.A0a = r8;
        this.A0O = r9;
        if (r8 != null) {
            r0 = r8.ABj();
        } else {
            AnonymousClass19I r02 = this.A0G;
            if (r02 != null) {
                r0 = r02.A01;
            } else {
                r0 = null;
            }
        }
        this.A0b = r0;
        Drawable A04 = AnonymousClass00T.A04(context, R.drawable.balloon_outgoing_normal);
        AnonymousClass009.A05(A04);
        this.A0Z = A04;
        Drawable A042 = AnonymousClass00T.A04(context, R.drawable.balloon_outgoing_normal_ext);
        AnonymousClass009.A05(A042);
        this.A0Y = A042;
        Drawable A043 = AnonymousClass00T.A04(context, R.drawable.balloon_incoming_normal);
        AnonymousClass009.A05(A043);
        this.A0X = A043;
        Drawable A044 = AnonymousClass00T.A04(context, R.drawable.balloon_incoming_normal_ext);
        AnonymousClass009.A05(A044);
        this.A0W = A044;
        Drawable A045 = AnonymousClass00T.A04(context, R.drawable.balloon_centered_normal);
        AnonymousClass009.A05(A045);
        this.A0U = A045;
        Drawable A046 = AnonymousClass00T.A04(getContext(), R.drawable.balloon_centered_pressed);
        AnonymousClass009.A05(A046);
        this.A0V = A046;
        A04.getPadding(A0h);
        A043.getPadding(A0g);
        A043.getPadding(A0f);
        boolean z = r9.A0z.A02;
        if (z) {
            incomingLayoutId = getOutgoingLayoutId();
        } else {
            incomingLayoutId = getIncomingLayoutId();
        }
        if (LayoutInflater.from(getContext()).inflate(A0g() ? getCenteredLayoutId() : incomingLayoutId, (ViewGroup) this, true) != null) {
            this.A0D = getChildAt(0);
            this.A0T = (int) (((double) ViewConfiguration.get(context).getScaledTouchSlop()) * 1.8d);
            if (A0d == null) {
                A0d = AnonymousClass00T.A04(getContext(), R.drawable.ic_action_reply).mutate();
            }
            if (A0e == null) {
                A0e = AnonymousClass00T.A04(getContext(), R.drawable.forward_background).mutate();
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder("rootview for conversationRow is null, rightLayout=");
        sb.append(z);
        throw new RuntimeException(sb.toString());
    }

    public int A0a() {
        View view;
        if (!(this instanceof C60822yh)) {
            return this.A0D.getBottom();
        }
        C60822yh r3 = (C60822yh) this;
        if (r3.A01) {
            AbstractC15340mz r1 = ((AbstractC28551Oa) r3).A0O;
            if (r1.A0E() == null && r1.A0N == null && (view = r3.A03) != null) {
                return ((AbstractC28551Oa) r3).A0D.getTop() + view.getBottom();
            }
        }
        return ((AbstractC28551Oa) r3).A0D.getBottom();
    }

    public int A0b() {
        View view;
        if (!(this instanceof C60822yh)) {
            return this.A0D.getTop();
        }
        C60822yh r1 = (C60822yh) this;
        if (!r1.A01 || (view = r1.A03) == null) {
            return ((AbstractC28551Oa) r1).A0D.getTop();
        }
        return ((AbstractC28551Oa) r1).A0D.getTop() + view.getPaddingTop();
    }

    public Point A0c(int i, int i2) {
        int i3;
        int dimensionPixelOffset = getResources().getDimensionPixelOffset(R.dimen.space_base);
        View view = this.A0D;
        int measuredWidth = view.getMeasuredWidth();
        int left = view.getLeft();
        int y = (int) view.getY();
        if (!(this instanceof C60782yd) || !this.A0R ? this.A0O.A0z.A02 != (!this.A0K.A04().A06) : (!this.A0K.A04().A06)) {
            i3 = left + dimensionPixelOffset;
        } else {
            i3 = ((left + measuredWidth) - i) - dimensionPixelOffset;
        }
        return new Point(i3, (y + view.getMeasuredHeight()) - getReactionsViewVerticalOverlap());
    }

    public void A0d() {
        clearAnimation();
        this.A00 = 0.0f;
        invalidate();
    }

    public void A0e(AnonymousClass1IS r4) {
        C73843gt r2 = new C73843gt(this);
        r2.setDuration(2400);
        r2.setInterpolator(new AccelerateInterpolator());
        startAnimation(r2);
    }

    public boolean A0f() {
        AnonymousClass1OY r2 = (AnonymousClass1OY) this;
        Conversation A0o = r2.A0o();
        if (A0o == null || A0o.A1y.A05() || !r2.A1J() || r2.getMessageCount() != 1) {
            return false;
        }
        C14850m9 r6 = ((AbstractC28551Oa) r2).A0L;
        if (!C30041Vv.A0S(r2.A0X, r2.A0Y, r2.A0r, r6, r2.A11, r2.getFMessage(), r2.A1N) || ((AbstractC28551Oa) r2).A0D == null) {
            return false;
        }
        return true;
    }

    public boolean A0g() {
        if ((this instanceof C60692yR) || (this instanceof C60712yT) || (this instanceof AnonymousClass2y4) || (this instanceof C60462xs) || (this instanceof C42421vB) || (this instanceof C60472xv) || (this instanceof AnonymousClass2xu) || (this instanceof C60482xw) || (this instanceof AnonymousClass2xt)) {
            return true;
        }
        C64533Fx r0 = this.A0b;
        if (r0 != null) {
            return r0.A05();
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0060, code lost:
        if (r0 == false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x006a, code lost:
        if (r0 == false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x007c, code lost:
        if (r0 == false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0083, code lost:
        r1 = (X.AbstractC16130oV) r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0h() {
        /*
            r2 = this;
            boolean r0 = r2 instanceof X.C60512xz
            if (r0 != 0) goto L_0x0042
            boolean r0 = r2 instanceof X.C60842yj
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C60792ye
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C42511vK
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C60452xr
            if (r0 != 0) goto L_0x006d
            boolean r0 = r2 instanceof X.C60502xy
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C60492xx
            if (r0 != 0) goto L_0x006d
            boolean r0 = r2 instanceof X.AnonymousClass2y0
            if (r0 != 0) goto L_0x006d
            boolean r0 = r2 instanceof X.C60852yk
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C60822yh
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C60632yL
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C60622yK
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C60782yd
            if (r0 != 0) goto L_0x0072
            boolean r0 = r2 instanceof X.C47412Ap
            if (r0 != 0) goto L_0x0063
            boolean r0 = r2 instanceof X.C60762yb
            if (r0 != 0) goto L_0x0059
            boolean r0 = r2 instanceof X.C60752yZ
            if (r0 != 0) goto L_0x0075
            r1 = 0
        L_0x0041:
            return r1
        L_0x0042:
            X.0mz r0 = r2.getFMessage()
            boolean r0 = X.C26151Cf.A00(r0)
            if (r0 != 0) goto L_0x0057
            X.0mz r0 = r2.getFMessage()
            boolean r0 = X.C30041Vv.A0t(r0)
            r1 = 0
            if (r0 == 0) goto L_0x0041
        L_0x0057:
            r1 = 1
            return r1
        L_0x0059:
            r1 = r2
            X.2yb r1 = (X.C60762yb) r1
            boolean r0 = r1 instanceof X.AnonymousClass2y7
            X.0mz r1 = r1.A0O
            if (r0 != 0) goto L_0x0083
            goto L_0x007e
        L_0x0063:
            r1 = r2
            X.2Ap r1 = (X.C47412Ap) r1
            boolean r0 = r1 instanceof X.AnonymousClass2y9
            X.0mz r1 = r1.A0O
            if (r0 != 0) goto L_0x0083
            goto L_0x007e
        L_0x006d:
            X.0mz r1 = r2.getFMessage()
            goto L_0x007e
        L_0x0072:
            X.0mz r1 = r2.A0O
            goto L_0x007e
        L_0x0075:
            r1 = r2
            X.2yZ r1 = (X.C60752yZ) r1
            boolean r0 = r1 instanceof X.C60872ym
            X.0mz r1 = r1.A0O
            if (r0 != 0) goto L_0x0083
        L_0x007e:
            boolean r1 = X.C30041Vv.A0t(r1)
            return r1
        L_0x0083:
            X.0oV r1 = (X.AbstractC16130oV) r1
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC28551Oa.A0h():boolean");
    }

    public boolean A0i() {
        AbstractC13890kV r0 = this.A0a;
        if (r0 == null) {
            return false;
        }
        int ABe = r0.ABe();
        if (ABe == 0 || ABe == 2) {
            return A0l(getFMessage());
        }
        return false;
    }

    public final boolean A0j(MotionEvent motionEvent) {
        this.A07 = (int) motionEvent.getX();
        boolean z = false;
        this.A0S = false;
        if (this.A06 == 2) {
            this.A06 = 1;
            post(new RunnableBRunnable0Shape5S0100000_I0_5(this, 3));
            invalidate();
            z = true;
        }
        View view = this.A0D;
        this.A05 = (int) view.getTranslationX();
        view.animate().cancel();
        return z;
    }

    public final boolean A0k(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        if (this.A06 != 1 && Math.abs(x - this.A07) > this.A0T) {
            this.A06 = 1;
            ViewParent parent = getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
            cancelLongPress();
            setPressed(false);
        }
        if (this.A06 != 1) {
            return false;
        }
        int max = Math.max(0, ((this.A05 + x) - this.A07) - this.A0T);
        float max2 = (float) Math.max(0, max - (getWidth() / 6));
        int min = Math.min(max, getWidth() / 6);
        double d = (double) max2;
        int width = min + ((int) (d / (((0.75d * d) / ((double) (getWidth() / 6))) + 1.0d)));
        float f = (float) width;
        this.A0D.setTranslationX(f);
        C53182d3 r0 = this.A0H;
        if (r0 != null) {
            r0.setTranslationX(f);
        }
        if (width > getWidth() / 6 && !this.A0S) {
            try {
                if (Settings.System.getInt(getContext().getContentResolver(), "haptic_feedback_enabled") != 0) {
                    C51282Tp.A01(this.A0J);
                }
            } catch (Settings.SettingNotFoundException e) {
                Log.e("swipetoreply/vibrate", e);
            }
            this.A0S = true;
        }
        invalidate();
        return true;
    }

    public boolean A0l(AbstractC15340mz r3) {
        if ((r3.A07 & 1) != 1) {
            return false;
        }
        return true;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams;
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new ViewGroup.MarginLayoutParams(layoutParams);
    }

    private int getBubbleSwipeOffset() {
        View view = this.A0D;
        if (view != null) {
            return (int) view.getTranslationX();
        }
        return 0;
    }

    private float getBubbleWidth() {
        Rect rect;
        int i;
        if (A0g()) {
            i = 0;
        } else {
            if (this.A0O.A0z.A02) {
                rect = A0h;
            } else {
                rect = A0g;
            }
            i = rect.right + rect.left;
        }
        return (float) (getContentWidth() + i);
    }

    public int getContentWidth() {
        return this.A0D.getMeasuredWidth();
    }

    public AbstractC15340mz getFMessage() {
        return this.A0O;
    }

    private int getGlowContentBottom() {
        C53182d3 r0 = this.A0H;
        if (r0 == null || r0.getVisibility() != 0) {
            return this.A02;
        }
        return this.A0H.getBottom();
    }

    public int getReactionsViewVerticalOverlap() {
        return getResources().getDimensionPixelOffset(R.dimen.space_tight);
    }

    private int getSwipeReplyActivationThreshold() {
        return getWidth() / 6;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0348, code lost:
        if (r16 == false) goto L_0x034a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x02c5, code lost:
        if (r16 == false) goto L_0x02c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x02c7, code lost:
        r18.save();
        r0 = r2.exactCenterX();
     */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0330  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x033b  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x02b8  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r18) {
        /*
        // Method dump skipped, instructions count: 892
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC28551Oa.onDraw(android.graphics.Canvas):void");
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean A0j;
        if (A0f()) {
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                A0j = A0j(motionEvent);
            } else if (actionMasked == 2) {
                A0j = A0k(motionEvent);
            }
            if (A0j) {
                return true;
            }
        }
        if (motionEvent.getActionMasked() == 0) {
            this.A04 = (int) motionEvent.getY();
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a8, code lost:
        if (r0.A06() == false) goto L_0x00aa;
     */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r10, int r11, int r12, int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 362
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC28551Oa.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int makeMeasureSpec;
        int mainChildMaxWidth;
        int i5 = i2;
        int i6 = i;
        int mode = View.MeasureSpec.getMode(i6);
        int size = View.MeasureSpec.getSize(i6);
        int mode2 = View.MeasureSpec.getMode(i5);
        int size2 = View.MeasureSpec.getSize(i5);
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        View view = this.A0C;
        int i7 = Integer.MIN_VALUE;
        if (!(view == null || view.getVisibility() == 8)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A0C.getLayoutParams();
            int i8 = marginLayoutParams.height;
            if (i8 < 0) {
                if (mode2 != 0) {
                    i8 = View.MeasureSpec.makeMeasureSpec(size2 - paddingTop, Integer.MIN_VALUE);
                } else {
                    i8 = i5;
                }
            }
            int i9 = marginLayoutParams.width;
            if (i9 < 0) {
                if (mode != 0) {
                    int i10 = paddingLeft;
                    if (!this.A0b.A04()) {
                        i10 = 0;
                    }
                    int i11 = ((size - i10) - marginLayoutParams.leftMargin) - marginLayoutParams.rightMargin;
                    int i12 = Integer.MIN_VALUE;
                    if (i9 == -1) {
                        i12 = 1073741824;
                    }
                    i9 = View.MeasureSpec.makeMeasureSpec(i11, i12);
                } else {
                    i9 = i6;
                }
            }
            this.A0C.measure(i9, i8);
            paddingTop = paddingTop + this.A0C.getMeasuredHeight() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
        }
        int i13 = i6;
        C53182d3 r0 = this.A0H;
        int i14 = 0;
        if (r0 == null || r0.getVisibility() == 8) {
            i3 = 0;
        } else {
            int mode3 = View.MeasureSpec.getMode(i6);
            int size3 = View.MeasureSpec.getSize(i6);
            int mode4 = View.MeasureSpec.getMode(i5);
            int size4 = View.MeasureSpec.getSize(i5);
            int paddingLeft2 = getPaddingLeft() + getPaddingRight();
            ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) this.A0H.getLayoutParams();
            int i15 = marginLayoutParams2.height;
            int i16 = Integer.MIN_VALUE;
            if (i15 >= 0) {
                i5 = i15;
            } else if (mode4 != 0) {
                i5 = View.MeasureSpec.makeMeasureSpec(size4 - paddingTop, Integer.MIN_VALUE);
            }
            int i17 = marginLayoutParams2.width;
            if (i17 >= 0) {
                i13 = i17;
            } else if (mode3 != 0) {
                C64533Fx r02 = this.A0b;
                if (r02 == null || r02.A04()) {
                    i14 = paddingLeft2;
                }
                int i18 = ((size3 - i14) - marginLayoutParams2.leftMargin) - marginLayoutParams2.rightMargin;
                if (i17 == -1) {
                    i16 = 1073741824;
                }
                i13 = View.MeasureSpec.makeMeasureSpec(i18, i16);
            }
            this.A0H.measure(i13, i5);
            i3 = this.A0H.getMeasuredHeight();
            if (i3 != 0) {
                paddingTop += i3 - getReactionsViewVerticalOverlap();
            }
        }
        View view2 = this.A0D;
        ViewGroup.MarginLayoutParams marginLayoutParams3 = (ViewGroup.MarginLayoutParams) view2.getLayoutParams();
        int i19 = marginLayoutParams3.height;
        if (i19 >= 0) {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i19 + i3, 1073741824);
        } else {
            if (mode2 != 0) {
                i4 = size2 - paddingTop;
            } else {
                i4 = size2 + i3;
            }
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i4, mode2);
        }
        if (mode != 0) {
            int i20 = ((size - paddingLeft) - marginLayoutParams3.leftMargin) - marginLayoutParams3.rightMargin;
            int i21 = marginLayoutParams3.width;
            if (i21 >= 0 && i21 < i20) {
                i20 = i21;
            }
            if (!this.A0R && (mainChildMaxWidth = getMainChildMaxWidth()) != 0 && i20 > mainChildMaxWidth) {
                i20 = mainChildMaxWidth;
            }
            if (marginLayoutParams3.width >= 0) {
                i7 = 1073741824;
            }
            i6 = View.MeasureSpec.makeMeasureSpec(i20, i7);
        } else {
            int i22 = marginLayoutParams3.width;
            if (i22 >= 0) {
                i6 = View.MeasureSpec.makeMeasureSpec(i22, 1073741824);
            }
        }
        view2.measure(i6, makeMeasureSpec);
        C53082ci r1 = this.A0I;
        if (r1 != null && (r1.A02.getVisibility() == 0 || r1.A01.getVisibility() == 0)) {
            C53082ci r9 = this.A0I;
            int measuredHeight = view2.getMeasuredHeight();
            r9.setOrientation(1);
            r9.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            if (r9.getMeasuredHeight() > measuredHeight) {
                r9.setOrientation(0);
                r9.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            }
            int measuredWidth = size - ((view2.getMeasuredWidth() + getPaddingLeft()) + getPaddingRight());
            C53082ci r03 = this.A0I;
            ViewGroup.MarginLayoutParams marginLayoutParams4 = (ViewGroup.MarginLayoutParams) r03.getLayoutParams();
            int measuredWidth2 = r03.getMeasuredWidth() + marginLayoutParams4.leftMargin + marginLayoutParams4.rightMargin;
            if (measuredWidth < measuredWidth2) {
                view2.measure(View.MeasureSpec.makeMeasureSpec(view2.getMeasuredWidth() - (measuredWidth2 - measuredWidth), View.MeasureSpec.getMode(i6)), makeMeasureSpec);
            }
        }
        setMeasuredDimension(size, paddingTop + view2.getMeasuredHeight() + marginLayoutParams3.topMargin + marginLayoutParams3.bottomMargin);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
        if (r1 != 3) goto L_0x0015;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r7) {
        /*
            r6 = this;
            boolean r0 = r6.A0f()
            if (r0 == 0) goto L_0x0015
            int r1 = r7.getActionMasked()
            r3 = 1
            if (r1 == 0) goto L_0x009b
            if (r1 == r3) goto L_0x002c
            r0 = 2
            if (r1 == r0) goto L_0x0027
            r0 = 3
            if (r1 == r0) goto L_0x002c
        L_0x0015:
            int r0 = r7.getActionMasked()
            if (r0 != 0) goto L_0x0022
            float r0 = r7.getY()
            int r0 = (int) r0
            r6.A04 = r0
        L_0x0022:
            boolean r0 = super.onTouchEvent(r7)
            return r0
        L_0x0027:
            boolean r0 = r6.A0k(r7)
            goto L_0x009f
        L_0x002c:
            int r0 = r6.A06
            if (r0 != r3) goto L_0x0015
            r0 = 2
            r6.A06 = r0
            X.0kV r4 = r6.A0a
            if (r4 == 0) goto L_0x005d
            int r1 = r6.getBubbleSwipeOffset()
            int r0 = r6.getWidth()
            int r0 = r0 / 6
            if (r1 <= r0) goto L_0x005d
            boolean r1 = r4.AJv()
            boolean r2 = r4.AJl()
            X.0mz r0 = r6.getFMessage()
            r4.Ach(r0)
            if (r1 == 0) goto L_0x005d
            X.0mz r1 = r6.getFMessage()
            r0 = r2 ^ 1
            r4.AUy(r1, r0)
        L_0x005d:
            android.view.View r0 = r6.A0D
            android.view.ViewPropertyAnimator r0 = r0.animate()
            r1 = 200(0xc8, double:9.9E-322)
            android.view.ViewPropertyAnimator r0 = r0.setDuration(r1)
            r5 = 0
            android.view.ViewPropertyAnimator r4 = r0.translationX(r5)
            android.view.animation.DecelerateInterpolator r0 = new android.view.animation.DecelerateInterpolator
            r0.<init>()
            android.view.ViewPropertyAnimator r4 = r4.setInterpolator(r0)
            X.3f2 r0 = new X.3f2
            r0.<init>(r6)
            r4.setListener(r0)
            X.2d3 r0 = r6.A0H
            if (r0 == 0) goto L_0x0097
            android.view.ViewPropertyAnimator r0 = r0.animate()
            android.view.ViewPropertyAnimator r0 = r0.setDuration(r1)
            android.view.ViewPropertyAnimator r1 = r0.translationX(r5)
            android.view.animation.DecelerateInterpolator r0 = new android.view.animation.DecelerateInterpolator
            r0.<init>()
            r1.setInterpolator(r0)
        L_0x0097:
            r6.invalidate()
            return r3
        L_0x009b:
            boolean r0 = r6.A0j(r7)
        L_0x009f:
            if (r0 == 0) goto L_0x0015
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC28551Oa.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setDrawCenteredBubble(boolean z) {
        this.A0Q = z;
    }

    public void setFMessage(AbstractC15340mz r1) {
        this.A0O = r1;
    }
}
