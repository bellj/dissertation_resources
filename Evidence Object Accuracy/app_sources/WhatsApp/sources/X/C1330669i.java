package X;

/* renamed from: X.69i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1330669i implements AnonymousClass6MM {
    public final /* synthetic */ AnonymousClass6M0 A00;
    public final /* synthetic */ AnonymousClass605 A01;
    public final /* synthetic */ C128545wH A02;

    public C1330669i(AnonymousClass6M0 r1, AnonymousClass605 r2, C128545wH r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r2) {
        this.A00.AVD(r2);
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        AnonymousClass605 r11 = this.A01;
        C128545wH r12 = this.A02;
        AnonymousClass6M0 r10 = this.A00;
        r11.A08.A07("[Set PIN] called");
        C130775zx r3 = r11.A01;
        C121205hQ r6 = new C121205hQ(r11.A04.A00, r11.A02, r11.A05, r10, r11, r12);
        byte[] A00 = C130775zx.A00(null, null, "CREATE", str, null, new Object[0], C117295Zj.A03(r3.A01));
        AnonymousClass1W9[] r2 = new AnonymousClass1W9[1];
        C12960it.A1M("action", "create-payment-pin", r2, 0);
        C128545wH.A01(r12, r3, r6, A00, r2);
    }
}
