package X;

/* renamed from: X.30t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615430t extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public String A06;

    public C615430t() {
        super(3450, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A06);
        r3.Abe(4, this.A02);
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(7, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamBusinessInteraction {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessInteractionAction", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessInteractionTargetScreen", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessJid", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "entryPointApp", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "entryPointSource", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "internalEntryPoint", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sequenceNumber", this.A05);
        return C12960it.A0d("}", A0k);
    }
}
