package X;

import android.view.View;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.whatsapp.R;

/* renamed from: X.3TV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3TV implements AbstractC117155Ys {
    public final /* synthetic */ CollapsingToolbarLayout A00;

    public AnonymousClass3TV(CollapsingToolbarLayout collapsingToolbarLayout) {
        this.A00 = collapsingToolbarLayout;
    }

    @Override // X.AbstractC115785Sx
    public void ATD(AppBarLayout appBarLayout, int i) {
        int i2;
        int A00;
        CollapsingToolbarLayout collapsingToolbarLayout = this.A00;
        collapsingToolbarLayout.A00 = i;
        C018408o r0 = collapsingToolbarLayout.A0F;
        if (r0 != null) {
            i2 = r0.A06();
        } else {
            i2 = 0;
        }
        int childCount = collapsingToolbarLayout.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = collapsingToolbarLayout.getChildAt(i3);
            C52902bs r2 = (C52902bs) childAt.getLayoutParams();
            C92334Vm r3 = (C92334Vm) childAt.getTag(R.id.view_offset_helper);
            if (r3 == null) {
                r3 = new C92334Vm(childAt);
                childAt.setTag(R.id.view_offset_helper, r3);
            }
            int i4 = r2.A01;
            if (i4 == 1) {
                int i5 = -i;
                A00 = collapsingToolbarLayout.A00(childAt);
                if (i5 < 0) {
                    A00 = 0;
                } else if (i5 <= A00) {
                    A00 = i5;
                }
            } else if (i4 == 2) {
                A00 = Math.round(((float) (-i)) * r2.A00);
            }
            if (r3.A02 != A00) {
                r3.A02 = A00;
                r3.A00();
            }
        }
        collapsingToolbarLayout.A03();
        if (collapsingToolbarLayout.A0B != null && i2 > 0) {
            collapsingToolbarLayout.postInvalidateOnAnimation();
        }
        collapsingToolbarLayout.A0M.A04(((float) Math.abs(i)) / ((float) ((collapsingToolbarLayout.getHeight() - collapsingToolbarLayout.getMinimumHeight()) - i2)));
    }
}
