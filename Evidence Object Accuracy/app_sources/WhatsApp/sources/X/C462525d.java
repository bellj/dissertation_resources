package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C462525d extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C462525d A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;

    static {
        C462525d r0 = new C462525d();
        A05 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A00(1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A00(2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A00(3);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A00(4);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0J(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0J(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0J(3, this.A04);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0J(4, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
