package X;

import android.content.Context;

/* renamed from: X.295  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass295 extends AnonymousClass04L implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass295(Context context, AnonymousClass0O0 r4) {
        super(context, r4);
        if (!this.A01) {
            this.A01 = true;
            ((AnonymousClass294) this).A07 = (AnonymousClass01d) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.ALI.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
