package X;

import java.util.Comparator;

/* renamed from: X.1jX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36231jX implements Comparator {
    public C15550nR A00;
    public C36071jH A01;

    public C36231jX(C15570nT r3, C15550nR r4, C15610nY r5) {
        this.A00 = r4;
        this.A01 = new C36071jH(r3, r5, true);
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        C30751Yr r5 = (C30751Yr) obj2;
        C15550nR r1 = this.A00;
        C15370n3 A0A = r1.A0A(((C30751Yr) obj).A06);
        if (A0A == null) {
            return 1;
        }
        C15370n3 A0A2 = r1.A0A(r5.A06);
        if (A0A2 == null) {
            return -1;
        }
        return this.A01.compare(A0A, A0A2);
    }
}
