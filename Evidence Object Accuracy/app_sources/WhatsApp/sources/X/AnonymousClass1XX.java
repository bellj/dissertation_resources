package X;

/* renamed from: X.1XX  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1XX extends AbstractC15340mz implements AbstractC16420oz {
    public String A00;

    public AnonymousClass1XX(AnonymousClass1IS r2, byte b, long j) {
        super(r2, b, j);
        this.A02 = 0;
    }

    public void A14(AnonymousClass1G9 r4) {
        AnonymousClass1IS r2 = this.A0z;
        AbstractC14640lm r0 = r2.A00;
        AnonymousClass009.A05(r0);
        r4.A07(r0.getRawString());
        boolean z = this instanceof AnonymousClass1XY;
        boolean z2 = r2.A02;
        if (z) {
            z2 = !z2;
        }
        r4.A08(z2);
        String str = this.A00;
        if (str != null) {
            r4.A05(str);
        }
        AbstractC14640lm A0B = A0B();
        if (A0B != null) {
            r4.A06(A0B.getRawString());
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r5) {
        C27081Fy r3;
        int i;
        int i2;
        boolean z = this instanceof AnonymousClass1XY;
        AnonymousClass1G3 r32 = r5.A03;
        C27081Fy r0 = (C27081Fy) r32.A00;
        if (!z) {
            C56942mD r02 = r0.A06;
            if (r02 == null) {
                r02 = C56942mD.A02;
            }
            AnonymousClass1G4 A0T = r02.A0T();
            AnonymousClass1G8 r03 = ((C56942mD) A0T.A00).A01;
            if (r03 == null) {
                r03 = AnonymousClass1G8.A05;
            }
            AnonymousClass1G9 r04 = (AnonymousClass1G9) r03.A0T();
            A14(r04);
            A0T.A03();
            C56942mD r1 = (C56942mD) A0T.A00;
            r1.A01 = (AnonymousClass1G8) r04.A02();
            r1.A00 |= 1;
            r32.A03();
            r3 = (C27081Fy) r32.A00;
            r3.A06 = (C56942mD) A0T.A02();
            i = r3.A00;
            i2 = 524288;
        } else {
            C56952mE r05 = r0.A0A;
            if (r05 == null) {
                r05 = C56952mE.A02;
            }
            AnonymousClass1G4 A0T2 = r05.A0T();
            AnonymousClass1G8 r06 = ((C56952mE) A0T2.A00).A01;
            if (r06 == null) {
                r06 = AnonymousClass1G8.A05;
            }
            AnonymousClass1G9 r07 = (AnonymousClass1G9) r06.A0T();
            A14(r07);
            A0T2.A03();
            C56952mE r12 = (C56952mE) A0T2.A00;
            r12.A01 = (AnonymousClass1G8) r07.A02();
            r12.A00 |= 1;
            r32.A03();
            r3 = (C27081Fy) r32.A00;
            r3.A0A = (C56952mE) A0T2.A02();
            i = r3.A00;
            i2 = 262144;
        }
        r3.A00 = i | i2;
    }
}
