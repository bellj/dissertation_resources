package X;

import android.text.TextUtils;
import java.util.Locale;

/* renamed from: X.06B  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass06B {
    public static int A00(Locale locale) {
        return TextUtils.getLayoutDirectionFromLocale(locale);
    }
}
