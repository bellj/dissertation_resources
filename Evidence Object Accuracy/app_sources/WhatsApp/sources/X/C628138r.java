package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.38r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C628138r extends AbstractC16350or {
    public final C15550nR A00;
    public final C253318z A01;
    public final C17220qS A02;
    public final String A03;
    public final WeakReference A04;

    public C628138r(C15550nR r2, ContactPickerFragment contactPickerFragment, C253318z r4, C17220qS r5, String str) {
        this.A04 = C12970iu.A10(contactPickerFragment);
        this.A03 = str;
        this.A02 = r5;
        this.A01 = r4;
        this.A00 = r2;
    }
}
