package X;

import java.io.Serializable;

/* renamed from: X.032  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass032 implements Serializable {
    public abstract AnonymousClass032 A01(AnonymousClass032 v);

    public abstract AnonymousClass032 A02(AnonymousClass032 v, AnonymousClass032 v2);
}
