package X;

import android.util.SparseArray;

/* renamed from: X.2np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57912np extends AnonymousClass28D {
    public C57912np(int i, int i2) {
        super(i, i2 + 1);
    }

    public void A0Q() {
        boolean z;
        String str;
        String str2;
        SparseArray sparseArray = this.A02;
        Object obj = sparseArray.get(135);
        if ((obj instanceof String) && (str2 = (String) obj) != null) {
            sparseArray.put(135, C12980iv.A0V(str2));
        }
        Object obj2 = sparseArray.get(134);
        if ((obj2 instanceof String) && (str = (String) obj2) != null) {
            sparseArray.put(134, C12980iv.A0V(str));
        }
        if (sparseArray.get(135) == null) {
            int[] A03 = C65093Ic.A03(this);
            int length = A03.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    int i2 = i + 1;
                    AnonymousClass28D A0F = A0F(A03[i]);
                    if (A0F != null && A0F.A0O(148, true)) {
                        break;
                    }
                    i = i2;
                } else {
                    int[] A02 = C65093Ic.A02(this);
                    int length2 = A02.length;
                    int i3 = 0;
                    loop1: while (i3 < length2) {
                        int i4 = i3 + 1;
                        for (AnonymousClass28D r0 : A0L(A02[i3])) {
                            if (r0 == null || !r0.A0O(148, true)) {
                            }
                        }
                        i3 = i4;
                    }
                    z = false;
                }
            }
        }
        z = true;
        sparseArray.put(148, Boolean.valueOf(z));
    }
}
