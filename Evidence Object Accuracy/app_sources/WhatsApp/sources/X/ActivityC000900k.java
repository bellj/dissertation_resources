package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: X.00k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ActivityC000900k extends ActivityC001000l implements AbstractC000300e, AbstractC002100w {
    public static final String A05 = "android:support:fragments";
    public boolean A00;
    public boolean A01;
    public boolean A02 = true;
    public final C011005j A03 = new C011005j(new AnonymousClass05V(this));
    public final C009804x A04 = new C009804x(this);

    @Deprecated
    public final void A0e() {
    }

    @Deprecated
    public void A1T(AnonymousClass01E r1) {
    }

    public ActivityC000900k() {
        A0A();
    }

    public ActivityC000900k(int i) {
        super(i);
        A0A();
    }

    private void A0A() {
        this.A07.A00.A02(new C011105k(this), A05);
        A0R(new C011205l(this));
    }

    public static boolean A0B(AnonymousClass01F r4, AnonymousClass05I r5) {
        boolean z = false;
        for (AnonymousClass01E r2 : r4.A0U.A02()) {
            if (r2 != null) {
                AnonymousClass05V r0 = r2.A0F;
                if (!(r0 == null || r0.A04 == null)) {
                    z |= A0B(r2.A0E(), r5);
                }
                AnonymousClass0Yp r02 = r2.A0I;
                if (r02 != null) {
                    r02.A00();
                    if (r02.A00.A02.compareTo(AnonymousClass05I.STARTED) >= 0) {
                        C009804x r1 = r2.A0I.A00;
                        r1.A06("setCurrentState");
                        r1.A05(r5);
                        z = true;
                    }
                }
                if (r2.A0K.A02.compareTo(AnonymousClass05I.STARTED) >= 0) {
                    C009804x r12 = r2.A0K;
                    r12.A06("setCurrentState");
                    r12.A05(r5);
                    z = true;
                }
            }
        }
        return z;
    }

    public final View A0U(View view, String str, Context context, AttributeSet attributeSet) {
        return this.A03.A00.A03.A0S.onCreateView(view, str, context, attributeSet);
    }

    public AnonymousClass01F A0V() {
        return this.A03.A00.A03;
    }

    @Deprecated
    public AnonymousClass0Q3 A0W() {
        return new AnonymousClass0Q3(this, AHb());
    }

    public void A0X() {
        do {
        } while (A0B(this.A03.A00.A03, AnonymousClass05I.CREATED));
    }

    public void A0Y() {
        this.A04.A04(AnonymousClass074.ON_RESUME);
        AnonymousClass01F r2 = this.A03.A00.A03;
        r2.A0P = false;
        r2.A0Q = false;
        r2.A0A.A01 = false;
        r2.A0N(7);
    }

    public void A0a() {
        AnonymousClass00T.A08(this);
    }

    @Deprecated
    public void A0b() {
        invalidateOptionsMenu();
    }

    public void A0c() {
        AnonymousClass00T.A09(this);
    }

    public void A0d() {
        AnonymousClass00T.A0B(this);
    }

    @Deprecated
    public void A0f(Intent intent, IntentSender intentSender, Bundle bundle, AnonymousClass01E r20, int i, int i2, int i3, int i4) {
        Intent intent2 = intent;
        if (i == -1) {
            startIntentSenderForResult(intentSender, -1, intent2, i2, i3, i4, bundle);
        } else if (r20.A0F != null) {
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb = new StringBuilder();
                sb.append("Fragment ");
                sb.append(r20);
                sb.append(" received the following in startIntentSenderForResult() requestCode: ");
                sb.append(i);
                sb.append(" IntentSender: ");
                sb.append(intentSender);
                sb.append(" fillInIntent: ");
                sb.append(intent2);
                sb.append(" options: ");
                sb.append(bundle);
                Log.v("FragmentManager", sb.toString());
            }
            AnonymousClass01F A0F = r20.A0F();
            if (A0F.A04 != null) {
                if (bundle != null) {
                    if (intent == null) {
                        intent2 = new Intent();
                        intent2.putExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", true);
                    }
                    if (AnonymousClass01F.A01(2)) {
                        StringBuilder sb2 = new StringBuilder("ActivityOptions ");
                        sb2.append(bundle);
                        sb2.append(" were added to fillInIntent ");
                        sb2.append(intent2);
                        sb2.append(" for fragment ");
                        sb2.append(r20);
                        Log.v("FragmentManager", sb2.toString());
                    }
                    intent2.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundle);
                }
                C06800Vd r2 = new C06800Vd(intent2, intentSender, i2, i3);
                A0F.A0D.addLast(new C06790Vc(r20.A0T, i));
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb3 = new StringBuilder("Fragment ");
                    sb3.append(r20);
                    sb3.append("is launching an IntentSender for result ");
                    Log.v("FragmentManager", sb3.toString());
                }
                A0F.A04.A00(null, r2);
                return;
            }
            AnonymousClass05V r0 = A0F.A07;
            if (i == -1) {
                r0.A00.startIntentSenderForResult(intentSender, -1, intent2, i2, i3, i4, bundle);
                return;
            }
            throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Fragment ");
            sb4.append(r20);
            sb4.append(" not attached to Activity");
            throw new IllegalStateException(sb4.toString());
        }
    }

    public void A0g(Intent intent, Bundle bundle, AnonymousClass01E r4, int i) {
        if (i == -1) {
            startActivityForResult(intent, -1, bundle);
        } else {
            r4.A0P(intent, i, bundle);
        }
    }

    public void A0h(Intent intent, AnonymousClass01E r3, int i) {
        A0g(intent, null, r3, i);
    }

    @Deprecated
    public void A0i(Menu menu, View view) {
        super.onPreparePanel(0, view, menu);
    }

    public void A0j(AbstractC000600h r1) {
        AnonymousClass00T.A0C(this, r1);
    }

    public void A0k(AbstractC000600h r1) {
        AnonymousClass00T.A0D(this, r1);
    }

    @Override // android.app.Activity
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("  ");
        String obj = sb.toString();
        printWriter.print(obj);
        printWriter.print("mCreated=");
        printWriter.print(this.A00);
        printWriter.print(" mResumed=");
        printWriter.print(this.A01);
        printWriter.print(" mStopped=");
        printWriter.print(this.A02);
        if (getApplication() != null) {
            new AnonymousClass0Q3(this, AHb()).A03(obj, fileDescriptor, printWriter, strArr);
        }
        this.A03.A00.A03.A0h(str, fileDescriptor, printWriter, strArr);
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        this.A03.A00.A03.A0G();
        super.onActivityResult(i, i2, intent);
    }

    @Override // android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        this.A03.A00.A03.A0G();
        super.onConfigurationChanged(configuration);
        for (AnonymousClass01E r0 : this.A03.A00.A03.A0U.A02()) {
            if (r0 != null) {
                r0.A0Q(configuration);
            }
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A04.A04(AnonymousClass074.ON_CREATE);
        AnonymousClass01F r2 = this.A03.A00.A03;
        r2.A0P = false;
        r2.A0Q = false;
        r2.A0A.A01 = false;
        r2.A0N(1);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onCreatePanelMenu(int i, Menu menu) {
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i, menu);
        if (i != 0) {
            return onCreatePanelMenu;
        }
        C011005j r0 = this.A03;
        return onCreatePanelMenu | r0.A00.A03.A0p(menu, getMenuInflater());
    }

    @Override // android.app.Activity, android.view.LayoutInflater.Factory2
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View A0U = A0U(view, str, context, attributeSet);
        return A0U == null ? super.onCreateView(view, str, context, attributeSet) : A0U;
    }

    @Override // android.app.Activity, android.view.LayoutInflater.Factory
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View A0U = A0U(null, str, context, attributeSet);
        return A0U == null ? super.onCreateView(str, context, attributeSet) : A0U;
    }

    @Override // android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A03.A00.A03.A0F();
        this.A04.A04(AnonymousClass074.ON_DESTROY);
    }

    @Override // android.app.Activity, android.content.ComponentCallbacks
    public void onLowMemory() {
        super.onLowMemory();
        for (AnonymousClass01E r0 : this.A03.A00.A03.A0U.A02()) {
            if (r0 != null) {
                r0.A0L();
            }
        }
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        if (i == 0) {
            return this.A03.A00.A03.A0r(menuItem);
        }
        if (i != 6) {
            return false;
        }
        return this.A03.A00.A03.A0q(menuItem);
    }

    @Override // android.app.Activity
    public void onMultiWindowModeChanged(boolean z) {
        for (AnonymousClass01E r0 : this.A03.A00.A03.A0U.A02()) {
            if (r0 != null) {
                r0.A0Z(z);
            }
        }
    }

    @Override // android.app.Activity
    public void onNewIntent(Intent intent) {
        this.A03.A00.A03.A0G();
        super.onNewIntent(intent);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public void onPanelClosed(int i, Menu menu) {
        if (i == 0) {
            this.A03.A00.A03.A0R(menu);
        }
        super.onPanelClosed(i, menu);
    }

    @Override // android.app.Activity
    public void onPause() {
        super.onPause();
        this.A01 = false;
        this.A03.A00.A03.A0N(5);
        this.A04.A04(AnonymousClass074.ON_PAUSE);
    }

    @Override // android.app.Activity
    public void onPictureInPictureModeChanged(boolean z) {
        for (AnonymousClass01E r0 : this.A03.A00.A03.A0U.A02()) {
            if (r0 != null) {
                r0.A0a(z);
            }
        }
    }

    @Override // android.app.Activity
    public void onPostResume() {
        super.onPostResume();
        A0Y();
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onPreparePanel(int i, View view, Menu menu) {
        if (i == 0) {
            return super.onPreparePanel(0, view, menu) | this.A03.A00.A03.A0o(menu);
        }
        return super.onPreparePanel(i, view, menu);
    }

    @Override // X.ActivityC001000l, android.app.Activity, X.AbstractC000300e
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        this.A03.A00.A03.A0G();
        super.onRequestPermissionsResult(i, strArr, iArr);
    }

    @Override // android.app.Activity
    public void onResume() {
        this.A03.A00.A03.A0G();
        super.onResume();
        this.A01 = true;
        this.A03.A00.A03.A0k(true);
    }

    @Override // android.app.Activity
    public void onStart() {
        this.A03.A00.A03.A0G();
        super.onStart();
        this.A02 = false;
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass01F r2 = this.A03.A00.A03;
            r2.A0P = false;
            r2.A0Q = false;
            r2.A0A.A01 = false;
            r2.A0N(4);
        }
        this.A03.A00.A03.A0k(true);
        this.A04.A04(AnonymousClass074.ON_START);
        AnonymousClass01F r22 = this.A03.A00.A03;
        r22.A0P = false;
        r22.A0Q = false;
        r22.A0A.A01 = false;
        r22.A0N(5);
    }

    @Override // android.app.Activity
    public void onStateNotSaved() {
        this.A03.A00.A03.A0G();
    }

    @Override // android.app.Activity
    public void onStop() {
        super.onStop();
        this.A02 = true;
        A0X();
        AnonymousClass01F r2 = this.A03.A00.A03;
        r2.A0Q = true;
        r2.A0A.A01 = true;
        r2.A0N(4);
        this.A04.A04(AnonymousClass074.ON_STOP);
    }
}
