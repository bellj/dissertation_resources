package X;

import android.content.Context;

/* renamed from: X.5vV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128065vV {
    public final Context A00;
    public final C14900mE A01;
    public final C18650sn A02;
    public final C18610sj A03;
    public final AnonymousClass60T A04;
    public final C30931Zj A05 = C117305Zk.A0V("PaymentKycAction", "network");
    public final C18590sh A06;

    public C128065vV(Context context, C14900mE r4, C18650sn r5, C18610sj r6, AnonymousClass60T r7, C18590sh r8) {
        this.A00 = context;
        this.A01 = r4;
        this.A06 = r8;
        this.A03 = r6;
        this.A02 = r5;
        this.A04 = r7;
    }
}
