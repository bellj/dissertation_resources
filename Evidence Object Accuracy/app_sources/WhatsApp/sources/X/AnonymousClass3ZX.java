package X;

import android.app.Activity;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape1S0700000_I1;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import java.lang.ref.Reference;
import java.util.Set;

/* renamed from: X.3ZX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZX implements AbstractC116765Ws {
    public final /* synthetic */ RunnableBRunnable0Shape1S0700000_I1 A00;

    public AnonymousClass3ZX(RunnableBRunnable0Shape1S0700000_I1 runnableBRunnable0Shape1S0700000_I1) {
        this.A00 = runnableBRunnable0Shape1S0700000_I1;
    }

    @Override // X.AbstractC116765Ws
    public void APl(int i) {
        RunnableBRunnable0Shape1S0700000_I1 runnableBRunnable0Shape1S0700000_I1 = this.A00;
        Activity activity = (Activity) ((Reference) runnableBRunnable0Shape1S0700000_I1.A05).get();
        if (activity != null) {
            activity.runOnUiThread(new RunnableBRunnable0Shape11S0200000_I1_1(runnableBRunnable0Shape1S0700000_I1, 29, activity));
        }
    }

    @Override // X.AbstractC116765Ws
    public void AXX() {
        RunnableBRunnable0Shape1S0700000_I1 runnableBRunnable0Shape1S0700000_I1 = this.A00;
        Activity activity = (Activity) ((Reference) runnableBRunnable0Shape1S0700000_I1.A05).get();
        if (activity != null) {
            activity.runOnUiThread(new RunnableBRunnable0Shape11S0200000_I1_1(runnableBRunnable0Shape1S0700000_I1, 29, activity));
        }
    }

    @Override // X.AbstractC116765Ws
    public void AY0(Set set) {
        Activity activity = (Activity) ((Reference) this.A00.A05).get();
        if (activity != null) {
            activity.runOnUiThread(new RunnableBRunnable0Shape3S0300000_I1(this, activity, set, 23));
        }
    }
}
