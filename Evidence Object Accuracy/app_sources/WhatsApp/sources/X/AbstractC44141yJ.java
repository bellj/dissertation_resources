package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/* renamed from: X.1yJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC44141yJ {
    public final AnonymousClass016 A00 = new AnonymousClass016();
    public final C14900mE A01;
    public final C15570nT A02;
    public final C17050qB A03;
    public final C14950mJ A04;
    public final C19490uC A05;
    public final C15880o3 A06;
    public final C20850wQ A07;
    public final C27021Fs A08;
    public final C19890uq A09;
    public final C20740wF A0A;
    public final C25651Af A0B;
    public final C18350sJ A0C;
    public final C15860o1 A0D;
    public final AbstractC15850o0 A0E;
    public final C15830ny A0F;
    public final AbstractC14440lR A0G;

    public AbstractC44141yJ(C14900mE r2, C15570nT r3, C17050qB r4, C14950mJ r5, C19490uC r6, C15880o3 r7, C20850wQ r8, C27021Fs r9, C19890uq r10, C20740wF r11, C25651Af r12, C18350sJ r13, C15860o1 r14, AbstractC15850o0 r15, C15830ny r16, AbstractC14440lR r17) {
        this.A01 = r2;
        this.A02 = r3;
        this.A0G = r17;
        this.A04 = r5;
        this.A09 = r10;
        this.A05 = r6;
        this.A08 = r9;
        this.A0F = r16;
        this.A0E = r15;
        this.A0D = r14;
        this.A03 = r4;
        this.A06 = r7;
        this.A0A = r11;
        this.A0C = r13;
        this.A07 = r8;
        this.A0B = r12;
    }

    public void A00() {
        if (!(this instanceof C44131yI)) {
            AnonymousClass2F9 r6 = (AnonymousClass2F9) this;
            if (r6.A00) {
                RunnableBRunnable0Shape1S0100000_I0_1 runnableBRunnable0Shape1S0100000_I0_1 = new RunnableBRunnable0Shape1S0100000_I0_1(r6, 28);
                RunnableBRunnable0Shape1S0100000_I0_1 runnableBRunnable0Shape1S0100000_I0_12 = new RunnableBRunnable0Shape1S0100000_I0_1(r6, 29);
                C19490uC r7 = ((AbstractC44141yJ) r6).A05;
                RunnableBRunnable0Shape0S0300000_I0 runnableBRunnable0Shape0S0300000_I0 = new RunnableBRunnable0Shape0S0300000_I0(r7, runnableBRunnable0Shape1S0100000_I0_12, runnableBRunnable0Shape1S0100000_I0_1, 38);
                File A0C = ((AbstractC44141yJ) r6).A06.A0C();
                ArrayList arrayList = new ArrayList();
                for (Object obj : C32781cj.A07(EnumC16570pG.A01(), EnumC16570pG.A00())) {
                    arrayList.addAll(C32781cj.A06(A0C, Collections.singletonList(obj)));
                }
                if (r7.A00(runnableBRunnable0Shape0S0300000_I0, arrayList) != 0) {
                    r6.A03.sendEmptyMessageDelayed(1, 32000);
                    return;
                }
            }
            r6.A0G.Aaz(new AnonymousClass2KI(r6, r6.A0A, r6.A00, r6.A09), new Object[0]);
            return;
        }
        C44131yI r1 = (C44131yI) this;
        Log.i("PrepareDirectTransferMsgStoreHelper/createAndRunPrepareMessageStoreTask");
        C44151yK r2 = new C44151yK(r1);
        r1.A00 = r2;
        r1.A0G.Aaz(r2, new Object[0]);
    }
}
