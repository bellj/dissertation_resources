package X;

/* renamed from: X.5FR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FR implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FR(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r8) {
        long[] jArr = (long[]) obj;
        appendable.append('[');
        boolean z = false;
        for (long j : jArr) {
            z = C72453ed.A1U(appendable, z);
            appendable.append(Long.toString(j));
        }
        appendable.append(']');
    }
}
