package X;

import com.whatsapp.util.Log;
import java.util.Locale;

/* renamed from: X.1OV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1OV {
    public static String A00(AbstractC14640lm r6) {
        String obj = r6.toString();
        return ((r6 instanceof C15580nU) || (r6 instanceof AnonymousClass1JV)) ? obj : String.format(Locale.US, "[obfuscated]@%s", obj.substring(obj.indexOf("@") + 1));
    }

    public static void A01(AnonymousClass1OT r5, String str, String str2, Object... objArr) {
        String str3;
        Locale locale = Locale.US;
        String format = String.format(locale, str2, objArr);
        Object[] objArr2 = new Object[3];
        if (r5 != null) {
            str3 = r5.A07;
        } else {
            str3 = null;
        }
        objArr2[0] = str3;
        objArr2[1] = str;
        objArr2[2] = format;
        Log.i(String.format(locale, "[%s](manage_community_groups) %s %s", objArr2));
    }
}
