package X;

/* renamed from: X.4Gf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88584Gf {
    public static final AbstractC114155Kk A00;

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:28:0x00a8 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.List, java.lang.Iterable] */
    /* JADX WARN: Type inference failed for: r5v2, types: [kotlinx.coroutines.android.AndroidDispatcherFactory] */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.util.List] */
    /* JADX WARNING: Unknown variable types count: 2 */
    static {
        /*
            java.lang.String r0 = "kotlinx.coroutines.fast.service.loader"
            r1 = 1
            java.lang.String r0 = java.lang.System.getProperty(r0)     // Catch: SecurityException -> 0x000d
            if (r0 == 0) goto L_0x000d
            boolean r1 = java.lang.Boolean.parseBoolean(r0)
        L_0x000d:
            java.lang.Class<kotlinx.coroutines.android.AndroidDispatcherFactory> r6 = kotlinx.coroutines.android.AndroidDispatcherFactory.class
            r5 = 0
            if (r1 == 0) goto L_0x0013
            goto L_0x0028
        L_0x0013:
            java.lang.ClassLoader r0 = r6.getClassLoader()     // Catch: all -> 0x00c5
            java.util.ServiceLoader r0 = java.util.ServiceLoader.load(r6, r0)     // Catch: all -> 0x00c5
            java.util.Iterator r0 = r0.iterator()     // Catch: all -> 0x00c5
            X.1WO r0 = X.AnonymousClass1WM.A06(r0)     // Catch: all -> 0x00c5
            java.util.List r4 = X.C11100fk.A02(r0)     // Catch: all -> 0x00c5
            goto L_0x0039
        L_0x0028:
            X.3Gp r3 = new X.3Gp     // Catch: all -> 0x00c5
            r3.<init>()     // Catch: all -> 0x00c5
            boolean r0 = X.C88564Gd.A00     // Catch: all -> 0x00c5
            if (r0 != 0) goto L_0x0062
            java.lang.ClassLoader r0 = r6.getClassLoader()     // Catch: all -> 0x00c5
            java.util.List r4 = r3.A01(r0)     // Catch: all -> 0x00c5
        L_0x0039:
            java.util.Iterator r1 = r4.iterator()     // Catch: all -> 0x00c5
            boolean r0 = r1.hasNext()     // Catch: all -> 0x00c5
            if (r0 == 0) goto L_0x004d
            java.lang.Object r5 = r1.next()     // Catch: all -> 0x00c5
            boolean r0 = r1.hasNext()     // Catch: all -> 0x00c5
            if (r0 != 0) goto L_0x0058
        L_0x004d:
            kotlinx.coroutines.android.AndroidDispatcherFactory r5 = (kotlinx.coroutines.android.AndroidDispatcherFactory) r5     // Catch: all -> 0x00c5
            if (r5 != 0) goto L_0x00b7
            r0 = 0
            X.5L4 r2 = new X.5L4     // Catch: all -> 0x00c5
            r2.<init>(r0, r0)     // Catch: all -> 0x00c5
            goto L_0x00cc
        L_0x0058:
            r1.next()     // Catch: all -> 0x00c5
            boolean r0 = r1.hasNext()     // Catch: all -> 0x00c5
            if (r0 != 0) goto L_0x0058
            goto L_0x004d
        L_0x0062:
            r0 = 2
            java.util.ArrayList r4 = X.C12980iv.A0w(r0)     // Catch: all -> 0x00ae
            java.lang.String r1 = "kotlinx.coroutines.android.AndroidDispatcherFactory"
            r2 = 0
            r8 = 1
            r7 = 0
            java.lang.ClassLoader r0 = r6.getClassLoader()     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            java.lang.Class r1 = java.lang.Class.forName(r1, r8, r0)     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            java.lang.Class[] r0 = new java.lang.Class[r7]     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            java.lang.reflect.Constructor r1 = r1.getDeclaredConstructor(r0)     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            java.lang.Object[] r0 = new java.lang.Object[r7]     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            java.lang.Object r0 = r1.newInstance(r0)     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            java.lang.Object r0 = r6.cast(r0)     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            kotlinx.coroutines.android.AndroidDispatcherFactory r0 = (kotlinx.coroutines.android.AndroidDispatcherFactory) r0     // Catch: ClassNotFoundException -> 0x008b, all -> 0x00ae
            if (r0 == 0) goto L_0x008b
            r4.add(r0)     // Catch: all -> 0x00ae
        L_0x008b:
            java.lang.String r1 = "kotlinx.coroutines.test.internal.TestMainDispatcherFactory"
            java.lang.ClassLoader r0 = r6.getClassLoader()     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            java.lang.Class r1 = java.lang.Class.forName(r1, r8, r0)     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            java.lang.Class[] r0 = new java.lang.Class[r7]     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            java.lang.reflect.Constructor r1 = r1.getDeclaredConstructor(r0)     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            java.lang.Object[] r0 = new java.lang.Object[r7]     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            java.lang.Object r0 = r1.newInstance(r0)     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            java.lang.Object r0 = r6.cast(r0)     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            kotlinx.coroutines.android.AndroidDispatcherFactory r0 = (kotlinx.coroutines.android.AndroidDispatcherFactory) r0     // Catch: ClassNotFoundException -> 0x00a8, all -> 0x00ae
            r2 = r0
        L_0x00a8:
            if (r2 == 0) goto L_0x0039
            r4.add(r2)     // Catch: all -> 0x00ae
            goto L_0x0039
        L_0x00ae:
            java.lang.ClassLoader r0 = r6.getClassLoader()     // Catch: all -> 0x00c5
            java.util.List r4 = r3.A01(r0)     // Catch: all -> 0x00c5
            goto L_0x0039
        L_0x00b7:
            X.5Kk r2 = r5.createDispatcher(r4)     // Catch: all -> 0x00bc
            goto L_0x00cc
        L_0x00bc:
            r1 = move-exception
            java.lang.String r0 = "For tests Dispatchers.setMain from kotlinx-coroutines-test module can be used"
            X.5L4 r2 = new X.5L4     // Catch: all -> 0x00c5
            r2.<init>(r0, r1)     // Catch: all -> 0x00c5
            goto L_0x00cc
        L_0x00c5:
            r1 = move-exception
            r0 = 0
            X.5L4 r2 = new X.5L4
            r2.<init>(r0, r1)
        L_0x00cc:
            X.C88584Gf.A00 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C88584Gf.<clinit>():void");
    }
}
