package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0ea  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10410ea implements Set {
    public final /* synthetic */ AbstractC008904n A00;

    public C10410ea(AbstractC008904n r1) {
        this.A00 = r1;
    }

    @Override // java.util.Set, java.util.Collection
    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean addAll(Collection collection) {
        AbstractC008904n r5 = this.A00;
        int A01 = r5.A01();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            ((C02470Ck) r5).A00.put(entry.getKey(), entry.getValue());
        }
        return A01 != r5.A01();
    }

    @Override // java.util.Set, java.util.Collection
    public void clear() {
        this.A00.A06();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean contains(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            AbstractC008904n r2 = this.A00;
            int A02 = r2.A02(entry.getKey());
            if (A02 >= 0) {
                Object A03 = r2.A03(A02, 1);
                Object value = entry.getValue();
                if (A03 == value) {
                    return true;
                }
                if (A03 == null || !A03.equals(value)) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    @Override // java.util.Set, java.util.Collection
    public boolean containsAll(Collection collection) {
        for (Object obj : collection) {
            if (!contains(obj)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.Set, java.util.Collection, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set = (Set) obj;
            try {
                if (size() != set.size()) {
                    return false;
                }
                if (containsAll(set)) {
                    return true;
                }
                return false;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    @Override // java.util.Set, java.util.Collection, java.lang.Object
    public int hashCode() {
        int hashCode;
        int hashCode2;
        AbstractC008904n r6 = this.A00;
        int i = 0;
        for (int A01 = r6.A01() - 1; A01 >= 0; A01--) {
            Object A03 = r6.A03(A01, 0);
            Object A032 = r6.A03(A01, 1);
            if (A03 == null) {
                hashCode = 0;
            } else {
                hashCode = A03.hashCode();
            }
            if (A032 == null) {
                hashCode2 = 0;
            } else {
                hashCode2 = A032.hashCode();
            }
            i += hashCode ^ hashCode2;
        }
        return i;
    }

    @Override // java.util.Set, java.util.Collection
    public boolean isEmpty() {
        return this.A00.A01() == 0;
    }

    @Override // java.util.Set, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return new C10390eY(this.A00);
    }

    @Override // java.util.Set, java.util.Collection
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Set, java.util.Collection
    public int size() {
        return this.A00.A01();
    }

    @Override // java.util.Set, java.util.Collection
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Set, java.util.Collection
    public Object[] toArray(Object[] objArr) {
        throw new UnsupportedOperationException();
    }
}
