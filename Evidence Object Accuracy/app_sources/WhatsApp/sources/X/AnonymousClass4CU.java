package X;

/* renamed from: X.4CU  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CU extends IllegalStateException {
    public Throwable cause;

    public AnonymousClass4CU(String str) {
        super(str);
    }

    public AnonymousClass4CU(String str, Throwable th) {
        super(str);
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
