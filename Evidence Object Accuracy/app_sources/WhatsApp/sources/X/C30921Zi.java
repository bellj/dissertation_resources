package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import java.io.File;
import java.util.Arrays;

/* renamed from: X.1Zi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30921Zi implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99894l2();
    public long A00;
    public String A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public boolean A07;
    public byte[] A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final int A0C;
    public final int A0D;
    public final long A0E;
    public final String A0F;
    public final String A0G;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C30921Zi(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A0F = readString;
        this.A0E = parcel.readLong();
        this.A0D = parcel.readInt();
        this.A09 = parcel.readInt();
        String readString2 = parcel.readString();
        AnonymousClass009.A05(readString2);
        this.A0G = readString2;
        this.A0A = parcel.readInt();
        this.A0C = parcel.readInt();
        this.A0B = parcel.readInt();
        this.A05 = parcel.readString();
        this.A01 = parcel.readString();
        this.A06 = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            byte[] bArr = new byte[readInt];
            this.A08 = bArr;
            parcel.readByteArray(bArr);
        }
        this.A00 = parcel.readLong();
        this.A04 = parcel.readString();
        this.A03 = parcel.readString();
        this.A02 = parcel.readString();
        this.A07 = parcel.readInt() != 1 ? false : true;
    }

    public C30921Zi(String str, String str2, String str3, String str4, String str5, int i, int i2, int i3, int i4, int i5, long j) {
        this.A0F = str;
        this.A0E = j;
        this.A0D = i;
        this.A09 = i2;
        this.A0G = str2;
        this.A0A = i3;
        this.A0C = i4;
        this.A0B = i5;
        this.A05 = str3;
        this.A01 = str4;
        this.A06 = str5;
    }

    public static C30921Zi A00(C57622nM r20, boolean z) {
        String str = r20.A09;
        long j = r20.A07;
        C30921Zi r8 = new C30921Zi(str, r20.A0A, null, null, null, r20.A06, r20.A01, r20.A02, r20.A04, r20.A03, j);
        if (z) {
            C57532nD r7 = r20.A08;
            if (r7 == null) {
                r7 = C57532nD.A06;
            }
            if ((r7.A00 & 1) == 1) {
                byte[] A04 = r7.A04.A04();
                long j2 = r7.A01;
                String encodeToString = Base64.encodeToString(r7.A03.A04(), 2);
                String encodeToString2 = Base64.encodeToString(r7.A02.A04(), 2);
                String str2 = r7.A05;
                r8.A08 = A04;
                r8.A00 = j2;
                r8.A04 = encodeToString;
                r8.A03 = encodeToString2;
                r8.A02 = str2;
                r8.A07 = true;
            }
        }
        return r8;
    }

    public C57622nM A01() {
        C57532nD r4;
        if (!this.A07 || this.A08 == null) {
            r4 = null;
        } else {
            AnonymousClass1G4 A0T = C57532nD.A06.A0T();
            byte[] bArr = this.A08;
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
            A0T.A03();
            C57532nD r1 = (C57532nD) A0T.A00;
            r1.A00 |= 1;
            r1.A04 = A01;
            long j = this.A00;
            A0T.A03();
            C57532nD r12 = (C57532nD) A0T.A00;
            r12.A00 |= 2;
            r12.A01 = j;
            String str = this.A02;
            A0T.A03();
            C57532nD r13 = (C57532nD) A0T.A00;
            r13.A00 |= 16;
            r13.A05 = str;
            byte[] decode = Base64.decode(this.A04, 2);
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(decode, 0, decode.length);
            A0T.A03();
            C57532nD r14 = (C57532nD) A0T.A00;
            r14.A00 |= 4;
            r14.A03 = A012;
            byte[] decode2 = Base64.decode(this.A03, 2);
            AbstractC27881Jp A013 = AbstractC27881Jp.A01(decode2, 0, decode2.length);
            A0T.A03();
            C57532nD r15 = (C57532nD) A0T.A00;
            r15.A00 |= 8;
            r15.A02 = A013;
            r4 = (C57532nD) A0T.A02();
        }
        AnonymousClass1G4 A0T2 = C57622nM.A0B.A0T();
        String str2 = this.A0F;
        A0T2.A03();
        C57622nM r16 = (C57622nM) A0T2.A00;
        r16.A00 |= 1;
        r16.A09 = str2;
        long j2 = this.A0E;
        A0T2.A03();
        C57622nM r5 = (C57622nM) A0T2.A00;
        r5.A00 |= 2;
        r5.A07 = j2;
        int i = this.A0D;
        A0T2.A03();
        C57622nM r17 = (C57622nM) A0T2.A00;
        r17.A00 |= 4;
        r17.A06 = i;
        int i2 = this.A09;
        A0T2.A03();
        C57622nM r18 = (C57622nM) A0T2.A00;
        r18.A00 |= 8;
        r18.A01 = i2;
        String str3 = this.A0G;
        A0T2.A03();
        C57622nM r19 = (C57622nM) A0T2.A00;
        r19.A00 |= 16;
        r19.A0A = str3;
        int i3 = this.A0A;
        A0T2.A03();
        C57622nM r110 = (C57622nM) A0T2.A00;
        r110.A00 |= 32;
        r110.A02 = i3;
        int i4 = this.A0C;
        A0T2.A03();
        C57622nM r111 = (C57622nM) A0T2.A00;
        r111.A00 |= 64;
        r111.A04 = i4;
        int i5 = this.A0B;
        A0T2.A03();
        C57622nM r112 = (C57622nM) A0T2.A00;
        r112.A00 |= 128;
        r112.A03 = i5;
        if (r4 != null) {
            A0T2.A03();
            C57622nM r113 = (C57622nM) A0T2.A00;
            r113.A08 = r4;
            r113.A00 |= 256;
        }
        return (C57622nM) A0T2.A02();
    }

    public File A02(File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A0F);
        sb.append(".webp");
        return new File(file, sb.toString());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C30921Zi r7 = (C30921Zi) obj;
            if (!this.A0F.equals(r7.A0F) || this.A0E != r7.A0E || this.A0D != r7.A0D || this.A09 != r7.A09 || !this.A0G.equals(r7.A0G) || this.A0A != r7.A0A || this.A0C != r7.A0C || this.A0B != r7.A0B || !C29941Vi.A00(this.A05, r7.A05) || !C29941Vi.A00(this.A01, r7.A01) || !C29941Vi.A00(this.A06, r7.A06) || !Arrays.equals(this.A08, r7.A08) || this.A00 != r7.A00 || !C29941Vi.A00(this.A04, r7.A04) || !C29941Vi.A00(this.A03, r7.A03) || !C29941Vi.A00(this.A02, r7.A02) || this.A07 != r7.A07) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A0F, Long.valueOf(this.A0E), Integer.valueOf(this.A0D), Integer.valueOf(this.A09), this.A0G, Integer.valueOf(this.A0A), Integer.valueOf(this.A0C), Integer.valueOf(this.A0B), this.A05, this.A01, this.A06, this.A08, Long.valueOf(this.A00), this.A04, this.A03, this.A02});
    }

    @Override // java.lang.Object
    public String toString() {
        return "PaymentBackgroundMetadata{}";
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A0F);
        parcel.writeLong(this.A0E);
        parcel.writeInt(this.A0D);
        parcel.writeInt(this.A09);
        parcel.writeString(this.A0G);
        parcel.writeInt(this.A0A);
        parcel.writeInt(this.A0C);
        parcel.writeInt(this.A0B);
        parcel.writeString(this.A05);
        parcel.writeString(this.A01);
        parcel.writeString(this.A06);
        byte[] bArr = this.A08;
        if (bArr != null) {
            parcel.writeInt(bArr.length);
            parcel.writeByteArray(this.A08);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeLong(this.A00);
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeString(this.A02);
        parcel.writeInt(this.A07 ? 1 : 0);
    }
}
