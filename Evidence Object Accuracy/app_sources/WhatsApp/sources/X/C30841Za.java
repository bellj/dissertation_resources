package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* renamed from: X.1Za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C30841Za extends AbstractC28901Pl {
    public static final Parcelable.Creator CREATOR = new C99884l1();
    public long A00;
    public C30821Yy A01;
    public final LinkedHashSet A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30841Za(C17930rd r1, String str, String str2, BigDecimal bigDecimal, LinkedHashSet linkedHashSet, int i, int i2) {
        this.A02 = linkedHashSet;
        AnonymousClass009.A05(r1);
        this.A07 = r1;
        A08(i);
        A07(i2);
        this.A0A = str;
        A0A(str2);
        A0B(r1, bigDecimal);
    }

    public /* synthetic */ C30841Za(Parcel parcel) {
        A09(parcel);
        String readString = parcel.readString();
        int readInt = parcel.readInt();
        this.A02 = new LinkedHashSet();
        for (int i = 0; i < readInt; i++) {
            this.A02.add(AnonymousClass102.A00(parcel));
        }
        A0B(this.A07, new BigDecimal(readString));
    }

    public void A0B(C17930rd r5, BigDecimal bigDecimal) {
        AbstractC30791Yv r1;
        if (bigDecimal != null && r5 != null) {
            LinkedHashSet linkedHashSet = r5.A05;
            LinkedHashSet linkedHashSet2 = this.A02;
            if (!(linkedHashSet2 == null || linkedHashSet == null)) {
                Iterator it = linkedHashSet2.iterator();
                while (it.hasNext()) {
                    r1 = (AbstractC30791Yv) it.next();
                    if (linkedHashSet.contains(r1)) {
                        break;
                    }
                }
            }
            r1 = C30771Yt.A06;
            this.A01 = new C30821Yy(bigDecimal, ((AbstractC30781Yu) r1).A01);
        }
    }

    @Override // X.AbstractC28901Pl, java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("[ WALLET: ");
        sb.append(super.toString());
        sb.append(" balance: ");
        sb.append(this.A01);
        sb.append(" ]");
        return sb.toString();
    }

    @Override // X.AbstractC28901Pl, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A01.toString());
        LinkedHashSet linkedHashSet = this.A02;
        parcel.writeInt(linkedHashSet.size());
        Iterator it = linkedHashSet.iterator();
        while (it.hasNext()) {
            ((AbstractC30791Yv) it.next()).writeToParcel(parcel, i);
        }
    }
}
