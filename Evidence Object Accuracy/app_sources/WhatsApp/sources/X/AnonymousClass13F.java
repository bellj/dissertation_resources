package X;

import org.json.JSONArray;

/* renamed from: X.13F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13F {
    public final C15650ng A00;
    public final AnonymousClass13C A01;

    public AnonymousClass13F(C15650ng r1, AnonymousClass13C r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public String A00(AnonymousClass1IS r4, C15430nF r5) {
        JSONArray jSONArray = new JSONArray();
        JSONArray put = jSONArray.put(1).put(r4.A01).put(r4.A02);
        AbstractC14640lm r0 = r4.A00;
        AnonymousClass009.A05(r0);
        put.put(r0.getRawString());
        return this.A01.A03(r5, jSONArray.toString());
    }
}
