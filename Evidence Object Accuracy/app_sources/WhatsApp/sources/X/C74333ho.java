package X;

import android.view.View;
import com.whatsapp.WaTabLayout;

/* renamed from: X.3ho  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74333ho extends AnonymousClass04v {
    public final /* synthetic */ WaTabLayout A00;

    public C74333ho(WaTabLayout waTabLayout) {
        this.A00 = waTabLayout;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r3) {
        super.A06(view, r3);
        r3.A0I(AnonymousClass0TY.A00(this.A00.A0c.size()));
    }
}
