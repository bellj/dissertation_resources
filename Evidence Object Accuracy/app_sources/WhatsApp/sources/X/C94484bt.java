package X;

import com.facebook.msys.mci.DefaultCrypto;

/* renamed from: X.4bt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94484bt {
    public int A00;
    public int A01;
    public C94484bt A02;
    public C94484bt A03;
    public boolean A04;
    public boolean A05;
    public final byte[] A06;

    public C94484bt() {
        this.A06 = new byte[DefaultCrypto.BUFFER_SIZE];
        this.A04 = true;
        this.A05 = false;
    }

    public C94484bt(byte[] bArr, int i, int i2) {
        C16700pc.A0D(bArr, 1);
        this.A06 = bArr;
        this.A01 = i;
        this.A00 = i2;
        this.A05 = true;
        this.A04 = false;
    }

    public final C94484bt A00() {
        C94484bt r3 = this.A02;
        if (r3 == this) {
            r3 = null;
        }
        C94484bt r1 = this.A03;
        if (r1 == null) {
            throw C72453ed.A0m();
        }
        r1.A02 = r3;
        C94484bt r0 = this.A02;
        if (r0 == null) {
            throw C72453ed.A0m();
        }
        r0.A03 = r1;
        this.A02 = null;
        this.A03 = null;
        return r3;
    }

    public final C94484bt A01() {
        this.A05 = true;
        return new C94484bt(this.A06, this.A01, this.A00);
    }

    public final C94484bt A02(int i) {
        C94484bt A00;
        if (i <= 0 || i > this.A00 - this.A01) {
            throw C12970iu.A0f("byteCount out of range");
        }
        if (i >= 1024) {
            A00 = A01();
        } else {
            A00 = C95204dJ.A00();
            byte[] bArr = this.A06;
            int i2 = this.A01;
            byte[] bArr2 = A00.A06;
            C16700pc.A0D(bArr, 0);
            C16700pc.A0D(bArr2, 2);
            System.arraycopy(bArr, i2, bArr2, 0, i);
        }
        A00.A00 = A00.A01 + i;
        this.A01 += i;
        C94484bt r0 = this.A03;
        if (r0 == null) {
            throw C72453ed.A0m();
        }
        r0.A04(A00);
        return A00;
    }

    public final void A03() {
        C94484bt r4 = this.A03;
        int i = 0;
        if (r4 == this) {
            throw C12960it.A0U("cannot compact");
        } else if (r4 == null) {
            throw C72453ed.A0m();
        } else if (r4.A04) {
            int i2 = this.A00 - this.A01;
            int i3 = 8192 - r4.A00;
            if (!r4.A05) {
                i = r4.A01;
            }
            if (i2 <= i3 + i) {
                A05(r4, i2);
                A00();
                C95204dJ.A01(this);
            }
        }
    }

    public final void A04(C94484bt r2) {
        r2.A03 = this;
        r2.A02 = this.A02;
        C94484bt r0 = this.A02;
        if (r0 == null) {
            throw C72453ed.A0m();
        }
        r0.A03 = r2;
        this.A02 = r2;
    }

    public final void A05(C94484bt r7, int i) {
        if (r7.A04) {
            int i2 = r7.A00;
            int i3 = i2 + i;
            if (i3 > 8192) {
                if (!r7.A05) {
                    int i4 = r7.A01;
                    if (i3 - i4 <= 8192) {
                        byte[] bArr = r7.A06;
                        C16700pc.A0D(bArr, 0);
                        System.arraycopy(bArr, i4, bArr, 0, i2 - i4);
                        i2 = r7.A00 - r7.A01;
                        r7.A00 = i2;
                        r7.A01 = 0;
                    } else {
                        throw C72453ed.A0h();
                    }
                } else {
                    throw C72453ed.A0h();
                }
            }
            byte[] bArr2 = this.A06;
            int i5 = this.A01;
            byte[] bArr3 = r7.A06;
            C16700pc.A0D(bArr2, 0);
            C16700pc.A0D(bArr3, 2);
            System.arraycopy(bArr2, i5, bArr3, i2, i);
            r7.A00 += i;
            this.A01 += i;
            return;
        }
        throw C12960it.A0U("only owner can write");
    }
}
