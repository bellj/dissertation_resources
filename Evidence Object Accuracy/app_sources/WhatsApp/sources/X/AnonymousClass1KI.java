package X;

/* renamed from: X.1KI  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1KI extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass1KI() {
        super(2514, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamMdAppStateLastCompanionDeregistration {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "timeBetweenFirstCompanionRegistrationAndLastCompanionDeregistration", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
