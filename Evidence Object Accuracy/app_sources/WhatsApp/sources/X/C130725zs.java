package X;

/* renamed from: X.5zs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130725zs {
    public AnonymousClass617 A00;

    public C130725zs() {
        this(null, null, 1);
    }

    public /* synthetic */ C130725zs(AnonymousClass617 r7, C51012Sk r8, int i) {
        this.A00 = AnonymousClass617.A00(new AnonymousClass60N(null, null, EnumC124545pi.A03, null, null));
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass617 A00(com.whatsapp.jid.UserJid r9, X.C1315963j r10, X.EnumC124545pi r11, X.C16380ov r12, java.lang.Boolean r13) {
        /*
            r8 = this;
            r6 = r12
            r4 = r10
            r7 = r13
            r3 = r9
            r5 = 0
            if (r13 != 0) goto L_0x0013
            X.617 r0 = r8.A00
            if (r0 == 0) goto L_0x0081
            java.lang.Object r0 = r0.A01
            X.60N r0 = (X.AnonymousClass60N) r0
            if (r0 == 0) goto L_0x0081
            java.lang.Boolean r7 = r0.A04
        L_0x0013:
            if (r9 != 0) goto L_0x0021
            X.617 r0 = r8.A00
            if (r0 == 0) goto L_0x007f
            java.lang.Object r0 = r0.A01
            X.60N r0 = (X.AnonymousClass60N) r0
            if (r0 == 0) goto L_0x007f
            com.whatsapp.jid.UserJid r3 = r0.A00
        L_0x0021:
            if (r10 != 0) goto L_0x002f
            X.617 r0 = r8.A00
            if (r0 == 0) goto L_0x007d
            java.lang.Object r0 = r0.A01
            X.60N r0 = (X.AnonymousClass60N) r0
            if (r0 == 0) goto L_0x007d
            X.63j r4 = r0.A01
        L_0x002f:
            if (r12 != 0) goto L_0x003d
            X.617 r0 = r8.A00
            if (r0 == 0) goto L_0x007b
            java.lang.Object r0 = r0.A01
            X.60N r0 = (X.AnonymousClass60N) r0
            if (r0 == 0) goto L_0x007b
            X.0ov r6 = r0.A03
        L_0x003d:
            if (r11 != 0) goto L_0x004b
            X.617 r0 = r8.A00
            if (r0 == 0) goto L_0x004c
            java.lang.Object r0 = r0.A01
            X.60N r0 = (X.AnonymousClass60N) r0
            if (r0 == 0) goto L_0x004c
            X.5pi r11 = r0.A02
        L_0x004b:
            r5 = r11
        L_0x004c:
            X.60N r2 = new X.60N
            r2.<init>(r3, r4, r5, r6, r7)
            X.63j r0 = r2.A01
            if (r0 == 0) goto L_0x0063
            java.lang.String r1 = "An error occurred"
            java.lang.Throwable r0 = new java.lang.Throwable
            r0.<init>(r1)
            X.617 r0 = X.AnonymousClass617.A02(r2, r0)
        L_0x0060:
            r8.A00 = r0
            return r0
        L_0x0063:
            X.0ov r0 = r2.A03
            if (r0 == 0) goto L_0x0071
            com.whatsapp.jid.UserJid r0 = r2.A00
            if (r0 == 0) goto L_0x0076
            X.5pi r1 = r2.A02
            X.5pi r0 = X.EnumC124545pi.A03
            if (r1 != r0) goto L_0x0076
        L_0x0071:
            X.617 r0 = X.AnonymousClass617.A00(r2)
            goto L_0x0060
        L_0x0076:
            X.617 r0 = X.AnonymousClass617.A01(r2)
            goto L_0x0060
        L_0x007b:
            r6 = r5
            goto L_0x003d
        L_0x007d:
            r4 = r5
            goto L_0x002f
        L_0x007f:
            r3 = r5
            goto L_0x0021
        L_0x0081:
            r7 = r5
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C130725zs.A00(com.whatsapp.jid.UserJid, X.63j, X.5pi, X.0ov, java.lang.Boolean):X.617");
    }
}
