package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.21Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21Q {
    public static Message A00(Jid jid, Jid jid2, Jid jid3, Jid jid4, String str, String str2, String str3, long j) {
        Message obtain = Message.obtain(null, 0, 104, 0);
        Bundle data = obtain.getData();
        data.putParcelable("callerJid", jid);
        data.putParcelable("calleeJid", jid2);
        data.putParcelable("creatorJid", jid3);
        data.putString("callId", str);
        data.putLong("callDuration", j);
        data.putParcelable("terminatorJid", jid4);
        data.putString("terminationReason", str2);
        data.putString("mediaType", str3);
        return obtain;
    }

    public static Message A01(Jid jid, String str, long j, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putString("msgId", str);
        bundle.putBoolean("isValid", z);
        bundle.putParcelable("toJid", jid);
        bundle.putLong("loggableStanzaId", j);
        return Message.obtain(null, 0, 42, 0, bundle);
    }

    public static Message A02(UserJid userJid, String str) {
        Message obtain = Message.obtain(null, 0, 118, 0);
        Bundle data = obtain.getData();
        data.putString("id", str);
        data.putParcelable("jid", userJid);
        return obtain;
    }

    public static Message A03(String str, byte[] bArr, boolean z) {
        Message obtain = Message.obtain(null, 0, 35, 0);
        obtain.getData().putByteArray("rc", bArr);
        obtain.getData().putString("rcJid", str);
        obtain.getData().putBoolean("saveRecoveryToken", z);
        return obtain;
    }
}
