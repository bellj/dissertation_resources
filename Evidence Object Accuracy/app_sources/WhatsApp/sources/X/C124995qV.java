package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5qV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124995qV {
    public static C129895yT A00(AnonymousClass102 r11, AnonymousClass1V8 r12) {
        List A0J = r12.A0J("type");
        ArrayList A0l = C12960it.A0l();
        Iterator it = A0J.iterator();
        while (it.hasNext()) {
            A0l.add(C117295Zj.A0W(C117305Zk.A0d(it), "value"));
        }
        AnonymousClass619 A01 = AnonymousClass619.A01(r12, "title");
        AnonymousClass619 A012 = AnonymousClass619.A01(r12, "body");
        List A0J2 = r12.A0J("cta");
        ArrayList A0l2 = C12960it.A0l();
        Iterator it2 = A0J2.iterator();
        while (it2.hasNext()) {
            AnonymousClass1V8 A0d = C117305Zk.A0d(it2);
            String A0H = A0d.A0H("type");
            String A0W = C117295Zj.A0W(A0d, "text");
            if (A0H.equals("STEP_UP")) {
                A0l2.add(new C121125hI(C125045qa.A00(A0d.A0F("step_up")), A0W));
            } else if (!A0H.equals("LINK")) {
                Log.w("Unsupported CTA type");
            } else {
                A0l2.add(new C121115hH(A0W, A0d.A0H("uri")));
            }
        }
        AnonymousClass1V8 A0E = r12.A0E("balance");
        C1316263m r8 = null;
        if (A0E != null) {
            r8 = C1316263m.A00(r11, A0E);
        }
        return new C129895yT(A012, A01, r8, A0l, A0l2);
    }
}
