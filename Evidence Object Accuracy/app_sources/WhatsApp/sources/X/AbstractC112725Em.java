package X;

/* renamed from: X.5Em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC112725Em implements AbstractC115495Rt {
    public final AbstractC115495Rt A00;
    public final AnonymousClass1J7 A01;

    public AbstractC112725Em(AbstractC115495Rt r2, AnonymousClass1J7 r3) {
        this.A01 = r3;
        this.A00 = r2 instanceof AbstractC112725Em ? null : r2;
    }
}
