package X;

import android.view.View;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaTextView;

/* renamed from: X.2hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C55062hj extends AnonymousClass03U {
    public AnonymousClass367 A00;
    public C469928m A01;
    public final AppCompatRadioButton A02;
    public final WaEditText A03;
    public final WaTextView A04;
    public final AnonymousClass01d A05;
    public final AnonymousClass018 A06;
    public final AnonymousClass19M A07;
    public final C16630pM A08;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55062hj(View view, AnonymousClass01d r3, AnonymousClass018 r4, AnonymousClass19M r5, C16630pM r6) {
        super(view);
        C16700pc.A0H(r5, r3);
        C16700pc.A0E(r4, 4);
        C16700pc.A0E(r6, 5);
        this.A07 = r5;
        this.A05 = r3;
        this.A06 = r4;
        this.A08 = r6;
        this.A04 = (WaTextView) C16700pc.A03(view, R.id.counter);
        this.A03 = (WaEditText) C16700pc.A03(view, R.id.text);
        this.A02 = (AppCompatRadioButton) C16700pc.A03(view, R.id.reason);
    }
}
