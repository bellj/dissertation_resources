package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S1101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape0S1100000_I0;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.3Vq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68503Vq implements AnonymousClass2K1 {
    public String A00;
    public List A01 = C12960it.A0l();
    public List A02 = C12960it.A0l();
    public final C48122Ek A03;
    public final String A04;
    public final boolean A05;
    public volatile int A06 = 0;
    public final /* synthetic */ AnonymousClass2K0 A07;

    public C68503Vq(C48122Ek r2, AnonymousClass2K0 r3, String str, boolean z) {
        this.A07 = r3;
        this.A04 = str;
        this.A03 = r2;
        this.A05 = z;
    }

    @Override // X.AnonymousClass2K1
    public void APl(int i) {
        synchronized (this) {
            AnonymousClass2K0 r1 = this.A07;
            if (r1.A09 != null) {
                this.A06 = i;
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = r1.A09;
                businessDirectorySearchQueryViewModel.A0I.A0H(new RunnableBRunnable0Shape0S1101000_I0(businessDirectorySearchQueryViewModel, this.A04, i, 1));
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v3, types: [X.402] */
    /* JADX WARN: Type inference failed for: r3v5, types: [X.2uy] */
    @Override // X.AnonymousClass2K1
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        boolean z;
        AnonymousClass405 r3;
        AnonymousClass4TW r2 = (AnonymousClass4TW) obj;
        AnonymousClass2K0 r4 = this.A07;
        if (r4.A09 != null) {
            List list = r2.A04;
            C48122Ek r0 = this.A03;
            Double d = r2.A00;
            AnonymousClass3AQ.A00(r0, d, list);
            if (this.A05) {
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = r4.A09;
                businessDirectorySearchQueryViewModel.A0I.A0H(new RunnableBRunnable0Shape0S1200000_I0(businessDirectorySearchQueryViewModel, new AnonymousClass4T8(d, r2.A01, r2.A02, list, C12960it.A0l(), r2.A06), this.A04, 13));
                return;
            }
            synchronized (this) {
                if (this.A06 != 0) {
                    APl(this.A06);
                } else {
                    String str = r2.A03;
                    if (str == null || this.A00 != null) {
                        str = "both";
                    }
                    this.A00 = str;
                    if (this.A02.isEmpty()) {
                        List list2 = r2.A05;
                        this.A02 = list2.subList(0, Math.min(list2.size(), 3));
                    }
                    List list3 = this.A01;
                    if (list3.isEmpty()) {
                        list3.addAll(list);
                    }
                    BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel2 = r4.A09;
                    String str2 = this.A04;
                    List<C30211Wn> list4 = this.A02;
                    boolean z2 = false;
                    List<AnonymousClass3M8> subList = list3.subList(0, Math.min(list3.size(), 3));
                    boolean A1T = C12960it.A1T("both".equals(this.A00) ? 1 : 0);
                    if (list3.size() > 3) {
                        z2 = true;
                    }
                    LinkedList linkedList = businessDirectorySearchQueryViewModel2.A0a;
                    synchronized (linkedList) {
                        businessDirectorySearchQueryViewModel2.A0C.removeCallbacks(businessDirectorySearchQueryViewModel2.A06);
                        if (linkedList.contains(str2)) {
                            while (!str2.equals(linkedList.peek())) {
                                linkedList.poll();
                            }
                            if (!A1T) {
                                String str3 = (String) linkedList.poll();
                                LinkedList linkedList2 = businessDirectorySearchQueryViewModel2.A08;
                                if (!linkedList2.isEmpty() && str3.startsWith((String) linkedList2.peekLast())) {
                                    linkedList2.removeLast();
                                }
                                linkedList2.add(str3);
                            }
                            List A07 = businessDirectorySearchQueryViewModel2.A07();
                            if (A1T || !list4.isEmpty() || !subList.isEmpty() || TextUtils.isEmpty(str2)) {
                                businessDirectorySearchQueryViewModel2.A02++;
                                if (!list4.isEmpty()) {
                                    int size = subList.size();
                                    ArrayList A0l = C12960it.A0l();
                                    for (C30211Wn r02 : list4) {
                                        A0l.add(new C59572ux(new C622936e(businessDirectorySearchQueryViewModel2, r02, str2, list4, size), r02.A01, str2));
                                    }
                                    A07.addAll(A0l);
                                }
                                if (!subList.isEmpty()) {
                                    int size2 = list4.size();
                                    ArrayList A0l2 = C12960it.A0l();
                                    for (AnonymousClass3M8 r03 : subList) {
                                        C48122Ek A04 = businessDirectorySearchQueryViewModel2.A04();
                                        int size3 = subList.size();
                                        int indexOf = subList.indexOf(r03);
                                        LatLng latLng = new LatLng(A04.A03.doubleValue(), A04.A04.doubleValue());
                                        int A01 = A04.A01();
                                        if (A04.A04()) {
                                            z = true;
                                            if (r03.A01()) {
                                                A0l2.add(new C59612v1(latLng, r03, null, new AnonymousClass54W(r03, businessDirectorySearchQueryViewModel2, str2, size2, size3, indexOf), new AbstractC115945Tn() { // from class: X.54Z
                                                    @Override // X.AbstractC115945Tn
                                                    public final void ANQ(C42351v4 r5) {
                                                        BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel3 = BusinessDirectorySearchQueryViewModel.this;
                                                        businessDirectorySearchQueryViewModel3.A0L.A05(Integer.valueOf(businessDirectorySearchQueryViewModel3.A04().A01()), 28, 7);
                                                    }
                                                }, A01, z));
                                                A0l2.add(new C84743zq());
                                            }
                                        }
                                        z = false;
                                        A0l2.add(new C59612v1(latLng, r03, null, new AnonymousClass54W(r03, businessDirectorySearchQueryViewModel2, str2, size2, size3, indexOf), new AbstractC115945Tn() { // from class: X.54Z
                                            @Override // X.AbstractC115945Tn
                                            public final void ANQ(C42351v4 r5) {
                                                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel3 = BusinessDirectorySearchQueryViewModel.this;
                                                businessDirectorySearchQueryViewModel3.A0L.A05(Integer.valueOf(businessDirectorySearchQueryViewModel3.A04().A01()), 28, 7);
                                            }
                                        }, A01, z));
                                        A0l2.add(new C84743zq());
                                    }
                                    A07.addAll(A0l2);
                                    if (z2) {
                                        r3 = new AnonymousClass402(new ViewOnClickCListenerShape0S1100000_I0(3, str2, businessDirectorySearchQueryViewModel2));
                                    }
                                    businessDirectorySearchQueryViewModel2.A0G.A0A(A07);
                                } else if (!A1T) {
                                    businessDirectorySearchQueryViewModel2.A03++;
                                    businessDirectorySearchQueryViewModel2.A0G.A0A(A07);
                                } else {
                                    r3 = new AnonymousClass405(1);
                                }
                            } else {
                                businessDirectorySearchQueryViewModel2.A01++;
                                r3 = new C59582uy(businessDirectorySearchQueryViewModel2, str2, 0);
                            }
                            A07.add(r3);
                            businessDirectorySearchQueryViewModel2.A0G.A0A(A07);
                        }
                    }
                }
            }
        }
    }
}
