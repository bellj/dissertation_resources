package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.DynamicButtonsLayout;
import com.whatsapp.conversation.conversationrow.DynamicButtonsRowContentLayout;
import com.whatsapp.conversation.conversationrow.NativeFlowButtonsRowContentLayout;

/* renamed from: X.2yC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60542yC extends C60782yd {
    public boolean A00;
    public final AbstractC13890kV A01;
    public final DynamicButtonsLayout A02 = ((DynamicButtonsLayout) findViewById(R.id.dynamic_reply_buttons));
    public final DynamicButtonsRowContentLayout A03 = ((DynamicButtonsRowContentLayout) findViewById(R.id.dynamic_reply_buttons_message_content));
    public final NativeFlowButtonsRowContentLayout A04 = ((NativeFlowButtonsRowContentLayout) findViewById(R.id.native_flow_action_button_content));

    public C60542yC(Context context, AbstractC13890kV r3, AnonymousClass1X7 r4) {
        super(context, r3, r4);
        A0Z();
        this.A01 = r3;
        A1P();
    }

    @Override // X.AbstractC60592yH, X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            AnonymousClass1OY.A0O(A08, this);
        }
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public void A0s() {
        A1P();
        super.A0s();
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, (AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1P();
        }
    }

    public final void A1P() {
        this.A03.A00(this);
        AnonymousClass3AZ.A00(this, this.A01, this.A02, this.A04, ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A0F().A00);
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_buttons_image_left;
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_buttons_image_left;
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public Drawable getKeepDrawable() {
        return AnonymousClass2GE.A01(getContext(), R.drawable.keep, R.color.messageStarColor);
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_buttons_image_right;
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public Drawable getStarDrawable() {
        return AnonymousClass2GE.A01(getContext(), R.drawable.message_star, R.color.messageStarColor);
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        AnonymousClass1OY.A0G(this.A02, this);
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth(), AnonymousClass1OY.A04(this, this.A02, getMeasuredHeight()));
    }
}
