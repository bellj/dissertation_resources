package X;

import android.text.TextUtils;

/* renamed from: X.050  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass050 implements Runnable {
    public final /* synthetic */ ActivityC001000l A00;

    public AnonymousClass050(ActivityC001000l r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            AnonymousClass050.super.onBackPressed();
        } catch (IllegalStateException e) {
            if (!TextUtils.equals(e.getMessage(), "Can not perform this action after onSaveInstanceState")) {
                throw e;
            }
        }
    }
}
