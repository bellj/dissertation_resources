package X;

import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import com.whatsapp.util.Log;

/* renamed from: X.2Tp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51282Tp {
    public static void A00(Vibrator vibrator, int i) {
        try {
            if (Build.VERSION.SDK_INT >= 26) {
                vibrator.vibrate(VibrationEffect.createOneShot(30, i));
            } else {
                vibrator.vibrate(30);
            }
        } catch (Exception e) {
            Log.w("vibrationutils/vibrate-failed", e);
        }
    }

    public static void A01(AnonymousClass01d r1) {
        Vibrator A0K = r1.A0K();
        if (A0K != null) {
            A00(A0K, 48);
        }
    }
}
