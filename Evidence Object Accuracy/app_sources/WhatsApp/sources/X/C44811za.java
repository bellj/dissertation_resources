package X;

/* renamed from: X.1za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44811za extends AbstractC44761zV {
    public boolean A00 = true;
    public boolean A01 = true;
    public boolean A02 = true;
    public boolean A03 = true;
    public final C22730zY A04;
    public final C26551Dx A05;
    public final AnonymousClass10J A06;
    public final Object A07 = new Object();

    @Override // X.AbstractC44761zV
    public String toString() {
        return "backup-condition";
    }

    public C44811za(C22730zY r2, C26551Dx r3, AnonymousClass10J r4) {
        this.A05 = r3;
        this.A04 = r2;
        this.A06 = r4;
    }
}
