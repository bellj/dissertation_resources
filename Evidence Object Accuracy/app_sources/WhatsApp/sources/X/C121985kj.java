package X;

import android.view.View;

/* renamed from: X.5kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121985kj extends AbstractC118845cT {
    public C121985kj(View view) {
        super(view);
    }

    @Override // X.AbstractC118845cT
    public void A08(AbstractC128945wv r3) {
        super.A08(r3);
        C121965kh r32 = (C121965kh) r3;
        ((AbstractC118845cT) this).A02.setText(r32.A01);
        ((AbstractC118845cT) this).A01.setText(r32.A00);
    }
}
