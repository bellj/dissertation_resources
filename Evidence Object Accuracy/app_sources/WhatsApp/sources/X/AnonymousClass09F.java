package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.09F  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09F extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass00N A00;
    public final /* synthetic */ AnonymousClass072 A01;

    public AnonymousClass09F(AnonymousClass00N r1, AnonymousClass072 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.remove(animator);
        this.A01.A0C.remove(animator);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A01.A0C.add(animator);
    }
}
