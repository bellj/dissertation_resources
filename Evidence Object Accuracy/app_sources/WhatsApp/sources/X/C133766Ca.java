package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.invites.PaymentInviteFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.6Ca  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133766Ca implements AbstractC1310761d {
    public View A00;
    public ViewGroup A01;
    public ViewGroup A02;
    public ViewGroup A03;
    public Button A04;
    public Button A05;
    public GridView A06;
    public ImageButton A07;
    public TextView A08;
    public TextView A09;
    public PaymentInviteFragment A0A;
    public final C15550nR A0B;
    public final C15610nY A0C;
    public final C21270x9 A0D;
    public final String A0E;
    public final boolean A0F;

    public C133766Ca(C15550nR r1, C15610nY r2, C21270x9 r3, String str, boolean z) {
        this.A0F = z;
        this.A0D = r3;
        this.A0B = r1;
        this.A0C = r2;
        this.A0E = str;
    }

    public void A00(Context context, List list) {
        View view;
        if (list.size() == 1) {
            String A08 = this.A0C.A08(this.A0B.A0B((AbstractC14640lm) list.get(0)));
            this.A04.setText(R.string.payments_invite_button_text);
            this.A08.setText(C12960it.A0X(context, A08, new Object[1], 0, R.string.payment_invite_bottom_sheet_body));
            this.A09.setText(C12960it.A0X(context, A08, new Object[1], 0, R.string.payment_invite_bottom_sheet_title));
            if (this.A0F) {
                C12960it.A0I(this.A00, R.id.incentive_info_text).setText(C12960it.A0X(context, A08, new Object[1], 0, R.string.incentive_invite_desc));
                view = this.A01;
            }
            C117295Zj.A0n(this.A04, this, 126);
            C117295Zj.A0o(this.A07, this, list, 29);
            C117295Zj.A0n(this.A05, this, 127);
            this.A02.setVisibility(0);
        }
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            A0l.add(this.A0B.A0B((AbstractC14640lm) it.next()));
        }
        this.A06.setAdapter((ListAdapter) new C117605aG(context, context, this.A0D.A04(context, "payment-invite-view-component"), this, A0l, A0l));
        view = this.A06;
        view.setVisibility(0);
        C117295Zj.A0n(this.A04, this, 126);
        C117295Zj.A0o(this.A07, this, list, 29);
        C117295Zj.A0n(this.A05, this, 127);
        this.A02.setVisibility(0);
    }

    @Override // X.AnonymousClass5Wu
    public /* bridge */ /* synthetic */ void A6Q(Object obj) {
        AnonymousClass4OZ r5 = (AnonymousClass4OZ) obj;
        Context context = this.A00.getContext();
        AnonymousClass009.A05(r5);
        if (1 == r5.A00) {
            this.A03.setVisibility(0);
            this.A02.setVisibility(8);
            return;
        }
        this.A03.setVisibility(8);
        Object obj2 = r5.A01;
        AnonymousClass009.A05(obj2);
        A00(context, (List) obj2);
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.payment_invite_view_component;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = view;
        this.A04 = (Button) AnonymousClass028.A0D(view, R.id.invite_button);
        this.A05 = (Button) AnonymousClass028.A0D(view, R.id.secondary_button);
        this.A06 = (GridView) AnonymousClass028.A0D(view, R.id.selected_items);
        this.A02 = C117315Zl.A04(view, R.id.invite_ui_content);
        this.A03 = C117315Zl.A04(view, R.id.invite_ui_loader);
        this.A07 = (ImageButton) AnonymousClass028.A0D(view, R.id.back);
        this.A08 = C12960it.A0I(view, R.id.payment_invite_bottom_sheet_body);
        this.A09 = C12960it.A0I(view, R.id.payment_invite_bottom_sheet_title);
        this.A01 = C117315Zl.A04(view, R.id.incentive_info_container);
    }

    @Override // X.AbstractC1310761d
    public void AcV(PaymentInviteFragment paymentInviteFragment) {
        this.A0A = paymentInviteFragment;
    }
}
