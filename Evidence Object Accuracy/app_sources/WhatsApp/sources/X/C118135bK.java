package X;

import android.content.Context;
import android.text.TextUtils;

/* renamed from: X.5bK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118135bK extends AnonymousClass015 {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public C27691It A01 = C13000ix.A03();
    public String A02;
    public boolean A03;
    public final C15450nH A04;
    public final AnonymousClass018 A05;
    public final C128255vo A06;
    public final C1309360o A07;

    public C118135bK(C15450nH r2, AnonymousClass018 r3, C128255vo r4, C1309360o r5) {
        this.A04 = r2;
        this.A07 = r5;
        this.A05 = r3;
        this.A06 = r4;
    }

    public static /* synthetic */ void A00(C118135bK r13) {
        if (!r13.A04.A05(AbstractC15460nI.A0v) || TextUtils.isEmpty(r13.A04().A09)) {
            r13.A05();
            return;
        }
        C128255vo r1 = r13.A06;
        Context context = r1.A01.A00;
        C14900mE r7 = r1.A00;
        C64513Fv r10 = new C64513Fv();
        C120375g5 r5 = new C120375g5(context, r7, r1.A03, r1.A06, r10, r1.A07);
        String str = r13.A04().A03;
        C126135sO r12 = new C126135sO(r13);
        C17220qS r3 = r5.A02;
        String A01 = r3.A01();
        C126565t5 r14 = new C126565t5(new AnonymousClass3CS(A01), str);
        C64513Fv r102 = ((C126705tJ) r5).A00;
        if (r102 != null) {
            r102.A04("upi-verify-qr-code");
        }
        C117325Zm.A06(r3, new C120795gl(r5.A00, r5.A01, r5.A03, r102, r5, r12), r14.A00, A01);
    }

    public C1310661b A04() {
        Object A01 = this.A00.A01();
        AnonymousClass009.A05(A01);
        return (C1310661b) A01;
    }

    public final void A05() {
        if (!this.A04.A05(AbstractC15460nI.A0y) || !A04().A0D || TextUtils.isEmpty(A04().A08)) {
            A06();
        } else {
            C127945vJ.A00(this.A01, 3);
        }
    }

    public final void A06() {
        C127945vJ r2 = new C127945vJ(5);
        r2.A03 = !TextUtils.isEmpty(A04().A05);
        r2.A04 = "DEEP_LINK".equals(this.A02);
        this.A01.A0B(r2);
    }
}
