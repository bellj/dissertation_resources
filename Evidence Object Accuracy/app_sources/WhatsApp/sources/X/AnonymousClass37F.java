package X;

import android.content.Context;
import java.lang.ref.WeakReference;

/* renamed from: X.37F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37F extends AbstractC16350or {
    public final AbstractC14640lm A00;
    public final AnonymousClass1A5 A01;
    public final WeakReference A02;

    public AnonymousClass37F(Context context, AbstractC14640lm r3, AnonymousClass1A5 r4) {
        this.A02 = C12970iu.A10(context);
        this.A01 = r4;
        this.A00 = r3;
    }
}
