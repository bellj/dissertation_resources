package X;

import android.text.Editable;
import com.whatsapp.twofactor.SetEmailFragment;

/* renamed from: X.47M  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47M extends C469928m {
    public final /* synthetic */ SetEmailFragment A00;

    public AnonymousClass47M(SetEmailFragment setEmailFragment) {
        this.A00 = setEmailFragment;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        String trim = editable.toString().trim();
        SetEmailFragment setEmailFragment = this.A00;
        int i = setEmailFragment.A00;
        if (i == 1) {
            setEmailFragment.A06.A04 = trim;
        } else if (i == 2) {
            setEmailFragment.A04.setText("");
            setEmailFragment.A06.A05 = trim;
        }
        setEmailFragment.A1A();
    }
}
