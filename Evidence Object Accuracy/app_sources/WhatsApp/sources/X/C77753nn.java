package X;

import android.os.Parcel;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3nn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77753nn extends AnonymousClass1UI {
    public C77753nn(AnonymousClass1U8 r2) {
        super(AnonymousClass4HW.A02, r2);
    }

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* bridge */ /* synthetic */ AnonymousClass5SX A02(Status status) {
        return new AnonymousClass516(status, C12960it.A0l());
    }

    @Override // X.AnonymousClass1UI
    public final /* bridge */ /* synthetic */ void A07(AnonymousClass5QV r6) {
        C98344iX r4 = (C98344iX) ((AbstractC95064d1) r6).A03();
        BinderC80593sW r1 = new BinderC80593sW(this);
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(r4.A01);
        obtain.writeStrongBinder(r1);
        Parcel obtain2 = Parcel.obtain();
        try {
            C12990iw.A18(r4.A00, obtain, obtain2, 15);
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
