package X;

import android.hardware.camera2.CaptureRequest;
import java.util.concurrent.Callable;

/* renamed from: X.6Kg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135906Kg implements Callable {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ C130185yw A01;
    public final /* synthetic */ AnonymousClass66I A02;

    public CallableC135906Kg(CaptureRequest.Builder builder, C130185yw r2, AnonymousClass66I r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = builder;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C130185yw r2 = this.A01;
        r2.A0A.A00("Cannot schedule reset focus task, not prepared");
        if (!r2.A03.A00.isConnected() || r2.A0E) {
            return null;
        }
        this.A02.A04 = new C128465w9(this);
        return null;
    }
}
