package X;

import com.whatsapp.status.playback.StatusPlaybackActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;

/* renamed from: X.2eD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53402eD extends C006202y {
    public final /* synthetic */ StatusPlaybackContactFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53402eD(StatusPlaybackContactFragment statusPlaybackContactFragment) {
        super(3);
        this.A00 = statusPlaybackContactFragment;
    }

    @Override // X.C006202y
    public /* bridge */ /* synthetic */ void A09(Object obj, Object obj2, Object obj3, boolean z) {
        int i;
        AbstractC33631eh r4 = (AbstractC33631eh) obj2;
        AbstractC116205Un r0 = (AbstractC116205Un) this.A00.A0B();
        if (r0 != null) {
            i = ((StatusPlaybackActivity) r0).A03;
        } else {
            i = 0;
        }
        if (r4 != null) {
            if (r4.A05) {
                r4.A06(i);
            }
            if (r4.A04) {
                r4.A05();
            }
            if (r4.A01) {
                if (r4.A03) {
                    r4.A02();
                }
                r4.A01();
            }
        }
    }
}
