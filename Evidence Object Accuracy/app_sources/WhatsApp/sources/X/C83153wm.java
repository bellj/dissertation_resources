package X;

import java.util.Collection;
import java.util.Collections;

/* renamed from: X.3wm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83153wm extends AnonymousClass4YR {
    public final Collection A00;

    public C83153wm(AnonymousClass5T6 r2) {
        this.A00 = Collections.singletonList(r2);
    }

    public C83153wm(Collection collection) {
        this.A00 = collection;
    }

    public boolean A08(C92364Vp r5, C94394bk r6, Object obj, Object obj2) {
        AnonymousClass4RG r3 = new AnonymousClass4RG(r5, obj, obj2, r6.A06);
        for (AnonymousClass5T6 r0 : this.A00) {
            if (!r0.A65(r3)) {
                return false;
            }
        }
        return true;
    }
}
