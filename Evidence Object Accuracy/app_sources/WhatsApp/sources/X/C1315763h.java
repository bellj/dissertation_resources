package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.63h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315763h implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(39);
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final List A07;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315763h(Parcel parcel) {
        this.A06 = parcel.readString();
        ArrayList A0l = C12960it.A0l();
        this.A07 = A0l;
        parcel.readList(A0l, String.class.getClassLoader());
        this.A05 = parcel.readString();
        this.A04 = parcel.readString();
        this.A03 = parcel.readString();
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
    }

    public C1315763h(JSONObject jSONObject) {
        this.A06 = jSONObject.getString("type");
        JSONArray jSONArray = jSONObject.getJSONArray("url_regex_list");
        this.A07 = C12960it.A0l();
        for (int i = 0; i < jSONArray.length(); i++) {
            this.A07.add(jSONArray.getString(i));
        }
        JSONObject jSONObject2 = jSONObject.getJSONObject("title");
        this.A05 = jSONObject2.getString("name");
        this.A04 = jSONObject2.getString("default_text");
        JSONObject jSONObject3 = jSONObject.getJSONObject("subtitle");
        this.A03 = jSONObject3.getString("name");
        this.A02 = jSONObject3.getString("default_text");
        JSONObject jSONObject4 = jSONObject.getJSONObject("button");
        this.A01 = jSONObject4.getString("name");
        this.A00 = jSONObject4.getString("default_text");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A06);
        parcel.writeList(this.A07);
        parcel.writeString(this.A05);
        parcel.writeString(this.A04);
        parcel.writeString(this.A03);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }
}
