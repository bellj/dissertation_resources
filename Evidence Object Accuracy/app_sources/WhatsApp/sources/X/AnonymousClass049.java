package X;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;

@Deprecated
/* renamed from: X.049  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass049 {
    public final Context A00;

    public AnonymousClass049(Context context) {
        this.A00 = context;
    }

    public static FingerprintManager.AuthenticationCallback A00(AnonymousClass04A r1) {
        return new AnonymousClass0AE(r1);
    }

    public static FingerprintManager.CryptoObject A01(AnonymousClass04B r0) {
        return AnonymousClass04C.A00(r0);
    }

    public static FingerprintManager A02(Context context) {
        return AnonymousClass04C.A02(context);
    }

    public static AnonymousClass04B A03(FingerprintManager.CryptoObject cryptoObject) {
        return AnonymousClass04C.A03(cryptoObject);
    }

    public void A04(AnonymousClass04A r8, AnonymousClass04B r9, AnonymousClass02N r10) {
        FingerprintManager A02;
        CancellationSignal cancellationSignal;
        if (Build.VERSION.SDK_INT >= 23 && (A02 = A02(this.A00)) != null) {
            if (r10 != null) {
                cancellationSignal = (CancellationSignal) r10.A00();
            } else {
                cancellationSignal = null;
            }
            AnonymousClass04C.A04(cancellationSignal, null, A02, A01(r9), A00(r8), 0);
        }
    }

    public boolean A05() {
        FingerprintManager A02;
        if (Build.VERSION.SDK_INT < 23 || (A02 = A02(this.A00)) == null || !AnonymousClass04C.A05(A02)) {
            return false;
        }
        return true;
    }

    public boolean A06() {
        FingerprintManager A02;
        if (Build.VERSION.SDK_INT < 23 || (A02 = A02(this.A00)) == null || !AnonymousClass04C.A06(A02)) {
            return false;
        }
        return true;
    }
}
