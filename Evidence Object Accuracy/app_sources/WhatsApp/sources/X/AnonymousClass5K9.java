package X;

import android.view.View;

/* renamed from: X.5K9  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5K9 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ C74093hI this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5K9(C74093hI r2) {
        super(1);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        View view = (View) obj;
        C16700pc.A0E(view, 0);
        return new C850040t(view, this.this$0.A02);
    }
}
