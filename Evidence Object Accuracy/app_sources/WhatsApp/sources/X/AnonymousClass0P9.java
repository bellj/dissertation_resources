package X;

import android.net.Uri;

/* renamed from: X.0P9  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0P9 {
    public final Uri A00;
    public final boolean A01;

    public AnonymousClass0P9(Uri uri, boolean z) {
        this.A00 = uri;
        this.A01 = z;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass0P9.class != obj.getClass()) {
                return false;
            }
            AnonymousClass0P9 r5 = (AnonymousClass0P9) obj;
            if (this.A01 != r5.A01 || !this.A00.equals(r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (this.A00.hashCode() * 31) + (this.A01 ? 1 : 0);
    }
}
