package X;

import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.calling.callgrid.view.PipViewContainer;
import com.whatsapp.util.Log;

/* renamed from: X.3NI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NI implements View.OnTouchListener {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public long A0D;
    public AnonymousClass4RV A0E;
    public Runnable A0F;
    public boolean A0G;
    public final double A0H;
    public final int A0I = ViewConfiguration.getLongPressTimeout();
    public final /* synthetic */ PipViewContainer A0J;

    public AnonymousClass3NI(PipViewContainer pipViewContainer) {
        this.A0J = pipViewContainer;
        int i = Resources.getSystem().getDisplayMetrics().widthPixels;
        int i2 = Resources.getSystem().getDisplayMetrics().heightPixels;
        this.A0H = Math.sqrt((double) ((i * i) + (i2 * i2)));
        this.A0F = new RunnableBRunnable0Shape14S0100000_I1(this, 44);
    }

    public static void A00(ViewPropertyAnimator viewPropertyAnimator, float f) {
        viewPropertyAnimator.scaleX(f).scaleY(f).setDuration(100).start();
    }

    public final boolean A01() {
        AbstractC55202hx r0 = this.A0J.A04;
        return r0 != null && r0.A07() && ((double) this.A08) < this.A0H / 60.0d;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i;
        String str;
        boolean z;
        int i2;
        int i3;
        int i4;
        float f;
        float rawX;
        int i5;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view);
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action == 1) {
                A00(view.animate(), 1.0f);
                this.A0G = false;
                if (this.A0E == null) {
                    str = "PipViewContainer/onTouch ACTION_UP dispatched before ACTION_DOWN, ignore";
                } else {
                    view.removeCallbacks(this.A0F);
                    if (A01()) {
                        PipViewContainer pipViewContainer = this.A0J;
                        pipViewContainer.A04.A0H.performClick();
                        PipViewContainer.A00(pipViewContainer, false);
                        pipViewContainer.A02();
                        return true;
                    }
                    float f2 = this.A04;
                    float f3 = this.A05;
                    float sqrt = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
                    double d = this.A0H;
                    boolean A1U = C12960it.A1U((((double) sqrt) > (d / 1.0d) ? 1 : (((double) sqrt) == (d / 1.0d) ? 0 : -1)));
                    float rawX2 = motionEvent.getRawX();
                    float rawY = motionEvent.getRawY();
                    if (A1U) {
                        double d2 = (double) ((this.A04 / sqrt) * 64.0f);
                        double d3 = (double) ((this.A05 / sqrt) * 64.0f);
                        while (rawX2 >= 0.0f && rawX2 <= ((float) this.A07) && rawY >= 0.0f && rawY <= ((float) this.A06)) {
                            rawX2 = (float) (((double) rawX2) + d2);
                            rawY = (float) (((double) rawY) + d3);
                        }
                    }
                    PipViewContainer pipViewContainer2 = this.A0J;
                    AnonymousClass018 r3 = pipViewContainer2.A07;
                    boolean A01 = C28141Kv.A01(r3);
                    float f4 = (float) (this.A07 / 2);
                    if (!A01 ? rawX2 > f4 : rawX2 < f4) {
                        z = false;
                    } else {
                        z = true;
                    }
                    boolean A1W = C12990iw.A1W((rawY > ((float) (this.A06 / 2)) ? 1 : (rawY == ((float) (this.A06 / 2)) ? 0 : -1)));
                    pipViewContainer2.A02 = C12990iw.A0L(Boolean.valueOf(z), Boolean.valueOf(A1W));
                    C91984Tz r13 = pipViewContainer2.A06;
                    AnonymousClass009.A05(r13);
                    Point point = pipViewContainer2.A01;
                    AnonymousClass009.A05(point);
                    AnonymousClass4RV A012 = pipViewContainer2.A01(point, new Point(this.A0C, this.A0B), r13);
                    if (z) {
                        i2 = A012.A00;
                    } else {
                        i2 = A012.A02;
                    }
                    if (A1W) {
                        i3 = A012.A01;
                    } else {
                        i3 = A012.A03;
                    }
                    Point point2 = new Point(i2, i3);
                    int i6 = point2.x;
                    if (C28141Kv.A01(r3)) {
                        i4 = A0H.leftMargin;
                    } else {
                        i4 = A0H.rightMargin;
                    }
                    int i7 = i6 - i4;
                    int i8 = point2.y - A0H.topMargin;
                    double sqrt2 = Math.sqrt((double) ((i7 * i7) + (i8 * i8)));
                    long max = (long) Math.max(200, (int) ((500.0d * sqrt2) / d));
                    StringBuilder A0k = C12960it.A0k("PipViewContainer/onTouch ACTION_UP xVelocity: ");
                    A0k.append(this.A04);
                    A0k.append(", yVelocity: ");
                    A0k.append(this.A05);
                    A0k.append(", velocity: ");
                    A0k.append(sqrt);
                    A0k.append(", fling: ");
                    A0k.append(A1U);
                    A0k.append(", finalRawX: ");
                    A0k.append(rawX2);
                    A0k.append(", finalRawY: ");
                    A0k.append(rawY);
                    A0k.append(", screen length: (");
                    A0k.append(d);
                    A0k.append("), container size: ");
                    A0k.append(this.A07);
                    A0k.append("x");
                    A0k.append(this.A06);
                    A0k.append(", pipAtRight: ");
                    A0k.append(z);
                    A0k.append(", pipAtBottom: ");
                    A0k.append(A1W);
                    A0k.append(", moving distance: ");
                    A0k.append(sqrt2);
                    A0k.append(", duration: ");
                    A0k.append(max);
                    C12960it.A1F(A0k);
                    StringBuilder A0k2 = C12960it.A0k("PipViewContainer/animatePiPView with duration: ");
                    A0k2.append(max);
                    A0k2.append(", xOffset: ");
                    A0k2.append(i7);
                    A0k2.append(", yOffset: ");
                    A0k2.append(i8);
                    C12960it.A1F(A0k2);
                    if (max <= 0 || !pipViewContainer2.A0D) {
                        pipViewContainer2.A03();
                        return true;
                    }
                    ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                    pipViewContainer2.A00 = ofFloat;
                    ofFloat.setDuration(max);
                    pipViewContainer2.A00.addUpdateListener(new AnonymousClass3K3(view, pipViewContainer2, i7, i8));
                    C12990iw.A0w(pipViewContainer2.A00, pipViewContainer2, 5);
                    pipViewContainer2.A00.start();
                    return true;
                }
            } else if (action != 2) {
                if (action != 3) {
                    return true;
                }
                A00(view.animate(), 1.0f);
                this.A0G = false;
                view.removeCallbacks(this.A0F);
                return true;
            } else if (this.A0E == null) {
                str = "PipViewContainer/onTouch ACTION_MOVE dispatched before ACTION_DOWN, ignore";
            } else {
                int i9 = this.A09;
                AnonymousClass018 r7 = this.A0J.A07;
                if (C28141Kv.A01(r7)) {
                    f = motionEvent.getRawX();
                    rawX = this.A00;
                } else {
                    f = this.A00;
                    rawX = motionEvent.getRawX();
                }
                int rawY2 = this.A0A + ((int) (motionEvent.getRawY() - this.A01));
                AnonymousClass4RV r5 = this.A0E;
                int max2 = Math.max(r5.A02, Math.min(r5.A00, i9 + ((int) (f - rawX))));
                int max3 = Math.max(r5.A03, Math.min(r5.A01, rawY2));
                motionEvent.getEventTime();
                motionEvent.getRawX();
                motionEvent.getRawY();
                if (C28141Kv.A01(r7)) {
                    i5 = A0H.rightMargin;
                } else {
                    i5 = A0H.leftMargin;
                }
                C42941w9.A09(view, r7, max2, max3, i5, A0H.bottomMargin);
                this.A08 = Math.max(Math.max(C12980iv.A05(max2, this.A09), C12980iv.A05(max3, this.A0A)), this.A08);
                if (!A01() && !this.A0G) {
                    this.A0G = true;
                    A00(view.animate(), 1.1f);
                    view.removeCallbacks(this.A0F);
                }
                long eventTime = motionEvent.getEventTime() - this.A0D;
                if (eventTime > 0) {
                    float f5 = (float) eventTime;
                    this.A04 = ((motionEvent.getRawX() - this.A02) * 1000.0f) / f5;
                    this.A05 = ((motionEvent.getRawY() - this.A03) * 1000.0f) / f5;
                }
                this.A02 = motionEvent.getRawX();
                this.A03 = motionEvent.getRawY();
                this.A0D = motionEvent.getEventTime();
                return true;
            }
            Log.i(str);
            return true;
        }
        view.postDelayed(this.A0F, (long) this.A0I);
        PipViewContainer pipViewContainer3 = this.A0J;
        Point point3 = pipViewContainer3.A01;
        AnonymousClass009.A05(point3);
        this.A07 = point3.x;
        AnonymousClass009.A05(point3);
        this.A06 = point3.y;
        this.A00 = motionEvent.getRawX();
        this.A01 = motionEvent.getRawY();
        if (C28141Kv.A01(pipViewContainer3.A07)) {
            i = A0H.leftMargin;
        } else {
            i = A0H.rightMargin;
        }
        this.A09 = i;
        this.A0A = A0H.topMargin;
        this.A0C = view.getWidth();
        this.A0B = view.getHeight();
        PipViewContainer.A00(pipViewContainer3, true);
        this.A08 = 0;
        ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(pipViewContainer3);
        Point point4 = new Point(A0H2.width, A0H2.height);
        C91984Tz r1 = pipViewContainer3.A06;
        AnonymousClass009.A05(r1);
        Point point5 = pipViewContainer3.A01;
        AnonymousClass009.A05(point5);
        this.A0E = pipViewContainer3.A01(point5, point4, r1);
        this.A05 = 0.0f;
        this.A04 = 0.0f;
        this.A03 = 0.0f;
        this.A02 = 0.0f;
        this.A0D = 0;
        StringBuilder A0k3 = C12960it.A0k("PipViewContainer/onTouch ACTION_DOWN downX: ");
        A0k3.append(this.A00);
        A0k3.append(", downY: ");
        A0k3.append(this.A01);
        A0k3.append(", leftMargin: ");
        A0k3.append(this.A09);
        A0k3.append(", topMargin: ");
        A0k3.append(this.A0A);
        C12960it.A1F(A0k3);
        return true;
    }
}
