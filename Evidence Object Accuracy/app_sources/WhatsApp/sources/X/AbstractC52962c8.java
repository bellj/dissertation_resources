package X;

import android.content.Context;
import android.widget.FrameLayout;

/* renamed from: X.2c8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC52962c8 extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC52962c8(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            ((AnonymousClass34M) this).A00 = C12960it.A0R(((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
