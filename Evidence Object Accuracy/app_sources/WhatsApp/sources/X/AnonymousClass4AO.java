package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AO  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AO extends Enum {
    public static final /* synthetic */ AnonymousClass4AO[] A00;
    public static final AnonymousClass4AO A01;
    public static final AnonymousClass4AO A02;
    public static final AnonymousClass4AO A03;
    public static final AnonymousClass4AO A04;

    static {
        AnonymousClass4AO r6 = new AnonymousClass4AO("READY", 0);
        A04 = r6;
        AnonymousClass4AO r5 = new AnonymousClass4AO("NOT_READY", 1);
        A03 = r5;
        AnonymousClass4AO r4 = new AnonymousClass4AO("DONE", 2);
        A01 = r4;
        AnonymousClass4AO r2 = new AnonymousClass4AO("FAILED", 3);
        A02 = r2;
        AnonymousClass4AO[] r1 = new AnonymousClass4AO[4];
        C12970iu.A1U(r6, r5, r1);
        r1[2] = r4;
        r1[3] = r2;
        A00 = r1;
    }

    public AnonymousClass4AO(String str, int i) {
    }

    public static AnonymousClass4AO valueOf(String str) {
        return (AnonymousClass4AO) Enum.valueOf(AnonymousClass4AO.class, str);
    }

    public static AnonymousClass4AO[] values() {
        return (AnonymousClass4AO[]) A00.clone();
    }
}
