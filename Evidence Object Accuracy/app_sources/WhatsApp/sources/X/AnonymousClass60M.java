package X;

import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.60M  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60M {
    public static final Charset A04;
    public static final HashMap A05 = C12970iu.A11();
    public static final HashSet A06;
    public static final byte[] A07 = {65, 83, 67, 73, 73, 0, 0, 0};
    public static final byte[] A08;
    public static final byte[] A09 = {-1, -40, -1};
    public static final int[] A0A = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1};
    public static final C127265uD[] A0B;
    public static final C127265uD[] A0C;
    public static final C127265uD[] A0D;
    public static final String[] A0E = {"", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE"};
    public static final HashMap[] A0F = new HashMap[3];
    public static final C127265uD[][] A0G;
    public int A00;
    public ByteOrder A01;
    public final Set A02;
    public final HashMap[] A03;

    static {
        int i = 0;
        C127265uD[] r12 = new C127265uD[3];
        A00("Orientation", r12, 274, 3, 0);
        A00("SubIFDPointer", r12, 330, 4, 1);
        A00("ExifIFDPointer", r12, 34665, 4, 2);
        A0D = r12;
        C127265uD[] r11 = new C127265uD[8];
        A00("ExposureTime", r11, 33434, 5, 0);
        A00("PhotographicSensitivity", r11, 34855, 3, 1);
        A00("ShutterSpeedValue", r11, 37377, 10, 2);
        A00("ApertureValue", r11, 37378, 5, 3);
        A00("FocalLength", r11, 37386, 5, 4);
        A00("WhiteBalance", r11, 41987, 3, 5);
        A00("DigitalZoomRatio", r11, 41988, 5, 6);
        A00("FocalLengthIn35mmFilm", r11, 41989, 3, 7);
        A0C = r11;
        A0G = new C127265uD[][]{r12, r11, r12};
        C127265uD[] r1 = new C127265uD[2];
        A00("SubIFDPointer", r1, 330, 4, 0);
        A00("ExifIFDPointer", r1, 34665, 4, 1);
        A0B = r1;
        String[] strArr = new String[2];
        strArr[0] = "DigitalZoomRatio";
        A06 = C12970iu.A13("ExposureTime", strArr, 1);
        Charset forName = Charset.forName("US-ASCII");
        A04 = forName;
        A08 = "Exif\u0000\u0000".getBytes(forName);
        while (true) {
            C127265uD[][] r2 = A0G;
            if (i < r2.length) {
                A0F[i] = C12970iu.A11();
                C127265uD[] r5 = r2[i];
                for (C127265uD r22 : r5) {
                    C12960it.A1J(r22, A0F[i], r22.A00);
                }
                i++;
            } else {
                A05.put(Integer.valueOf(A0B[1].A00), 1);
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0126, code lost:
        throw new java.io.IOException(X.C12960it.A0W(r1, "Invalid first Ifd offset: "));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass60M(java.io.InputStream r15) {
        /*
        // Method dump skipped, instructions count: 394
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass60M.<init>(java.io.InputStream):void");
    }

    public static void A00(String str, Object[] objArr, int i, int i2, int i3) {
        objArr[i3] = new C127265uD(i, str, i2);
    }

    public double A01(String str) {
        for (int i = 0; i < A0G.length; i++) {
            C129545xt r1 = (C129545xt) this.A03[i].get(str);
            if (r1 != null) {
                try {
                    Object A00 = r1.A00(this.A01);
                    if (A00 == null) {
                        throw new NumberFormatException("NULL can't be converted to a double value");
                    } else if (A00 instanceof String) {
                        return Double.parseDouble((String) A00);
                    } else {
                        if (A00 instanceof long[]) {
                            long[] jArr = (long[]) A00;
                            if (jArr.length == 1) {
                                return (double) jArr[0];
                            }
                            throw new NumberFormatException("There are more than one component");
                        } else if (A00 instanceof int[]) {
                            int[] iArr = (int[]) A00;
                            if (iArr.length == 1) {
                                return (double) iArr[0];
                            }
                            throw new NumberFormatException("There are more than one component");
                        } else if (A00 instanceof double[]) {
                            double[] dArr = (double[]) A00;
                            if (dArr.length == 1) {
                                return dArr[0];
                            }
                            throw new NumberFormatException("There are more than one component");
                        } else if (A00 instanceof C128685wV[]) {
                            C128685wV[] r4 = (C128685wV[]) A00;
                            if (r4.length == 1) {
                                C128685wV r2 = r4[0];
                                return ((double) r2.A01) / ((double) r2.A00);
                            }
                            throw new NumberFormatException("There are more than one component");
                        } else {
                            throw new NumberFormatException("Couldn't find a double value");
                        }
                    }
                } catch (NumberFormatException unused) {
                    return -1.0d;
                }
            }
        }
        return -1.0d;
    }

    public int A02(String str) {
        for (int i = 0; i < A0G.length; i++) {
            C129545xt r1 = (C129545xt) this.A03[i].get(str);
            if (r1 != null) {
                try {
                    Object A00 = r1.A00(this.A01);
                    if (A00 == null) {
                        throw new NumberFormatException("NULL can't be converted to a integer value");
                    } else if (A00 instanceof String) {
                        return Integer.parseInt((String) A00);
                    } else {
                        if (A00 instanceof long[]) {
                            long[] jArr = (long[]) A00;
                            if (jArr.length == 1) {
                                return (int) jArr[0];
                            }
                            throw new NumberFormatException("There are more than one component");
                        } else if (A00 instanceof int[]) {
                            int[] iArr = (int[]) A00;
                            if (iArr.length == 1) {
                                return iArr[0];
                            }
                            throw new NumberFormatException("There are more than one component");
                        } else {
                            throw new NumberFormatException("Couldn't find a integer value");
                        }
                    }
                } catch (NumberFormatException unused) {
                    return -1;
                }
            }
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0069, code lost:
        if (r12 == r3) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006c, code lost:
        if (r12 == 7) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0138, code lost:
        if (((long) r22.A00) != r4) goto L_0x00b4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(X.C134536Ez r22, int r23) {
        /*
        // Method dump skipped, instructions count: 316
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass60M.A03(X.6Ez, int):void");
    }
}
