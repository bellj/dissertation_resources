package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.0To  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06430To {
    public static final C06430To A02 = new C06430To("COMPOSITION");
    public AbstractC12480hz A00;
    public final List A01;

    public C06430To(C06430To r3) {
        this.A01 = new ArrayList(r3.A01);
        this.A00 = r3.A00;
    }

    public C06430To(String... strArr) {
        this.A01 = Arrays.asList(strArr);
    }

    public int A00(String str, int i) {
        if (!"__container".equals(str)) {
            List list = this.A01;
            if (!((String) list.get(i)).equals("**")) {
                return 1;
            }
            if (i != list.size() - 1 && ((String) list.get(i + 1)).equals(str)) {
                return 2;
            }
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
        if (r0 != false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(java.lang.String r8, int r9) {
        /*
            r7 = this;
            java.util.List r2 = r7.A01
            int r0 = r2.size()
            r6 = 0
            if (r9 >= r0) goto L_0x0050
            int r0 = r2.size()
            r5 = 1
            int r0 = r0 - r5
            r4 = 0
            if (r9 != r0) goto L_0x0013
            r4 = 1
        L_0x0013:
            java.lang.Object r3 = r2.get(r9)
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r1 = "**"
            boolean r0 = r3.equals(r1)
            if (r0 != 0) goto L_0x0051
            boolean r0 = r3.equals(r8)
            if (r0 != 0) goto L_0x0030
            java.lang.String r0 = "*"
            boolean r0 = r3.equals(r0)
            r3 = 0
            if (r0 == 0) goto L_0x0031
        L_0x0030:
            r3 = 1
        L_0x0031:
            if (r4 != 0) goto L_0x004d
            int r0 = r2.size()
            int r0 = r0 + -2
            if (r9 != r0) goto L_0x0050
            int r0 = r2.size()
            int r0 = r0 + -1
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0050
        L_0x004d:
            if (r3 == 0) goto L_0x0050
        L_0x004f:
            r6 = 1
        L_0x0050:
            return r6
        L_0x0051:
            if (r4 != 0) goto L_0x0095
            int r0 = r9 + 1
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0082
            int r0 = r2.size()
            int r0 = r0 + -2
            if (r9 == r0) goto L_0x004f
            int r0 = r2.size()
            int r0 = r0 + -3
            if (r9 != r0) goto L_0x0050
            int r0 = r2.size()
            int r0 = r0 + -1
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            boolean r3 = r0.equals(r1)
            goto L_0x004d
        L_0x0082:
            int r9 = r9 + r5
            int r0 = r2.size()
            int r0 = r0 - r5
            if (r9 < r0) goto L_0x0050
            java.lang.Object r0 = r2.get(r9)
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = r0.equals(r8)
            return r0
        L_0x0095:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06430To.A01(java.lang.String, int):boolean");
    }

    public boolean A02(String str, int i) {
        if (!"__container".equals(str)) {
            List list = this.A01;
            if (i >= list.size() || (!((String) list.get(i)).equals(str) && !((String) list.get(i)).equals("**") && !((String) list.get(i)).equals("*"))) {
                return false;
            }
        }
        return true;
    }

    public boolean A03(String str, int i) {
        if ("__container".equals(str)) {
            return true;
        }
        List list = this.A01;
        if (i < list.size() - 1 || ((String) list.get(i)).equals("**")) {
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("KeyPath{keys=");
        sb.append(this.A01);
        sb.append(",resolved=");
        boolean z = false;
        if (this.A00 != null) {
            z = true;
        }
        sb.append(z);
        sb.append('}');
        return sb.toString();
    }
}
