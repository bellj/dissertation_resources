package X;

import android.util.Log;

/* renamed from: X.4cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94844cd {
    public final float A00;
    public final int A01;
    public final Integer A02;
    public final String A03;
    public final boolean A04;
    public final boolean A05;

    public C94844cd(Integer num, String str, float f, int i, boolean z, boolean z2) {
        this.A03 = str;
        this.A01 = i;
        this.A02 = num;
        this.A00 = f;
        this.A04 = z;
        this.A05 = z2;
    }

    public static int A00(String str) {
        try {
            int parseInt = Integer.parseInt(str.trim());
            switch (parseInt) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    return parseInt;
            }
        } catch (NumberFormatException unused) {
        }
        Log.w("SsaStyle", C12960it.A0d(str, C12960it.A0k("Ignoring unknown alignment: ")));
        return -1;
    }

    public static boolean A01(String str) {
        try {
            int parseInt = Integer.parseInt(str);
            return parseInt == 1 || parseInt == -1;
        } catch (NumberFormatException e) {
            StringBuilder A0k = C12960it.A0k("Failed to parse bold/italic: '");
            A0k.append(str);
            C64923Hl.A02("SsaStyle", C12960it.A0d("'", A0k), e);
            return false;
        }
    }
}
