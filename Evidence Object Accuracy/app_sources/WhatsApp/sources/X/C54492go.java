package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2go  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54492go extends AnonymousClass02M {
    public AnonymousClass4J0 A00;
    public AnonymousClass2Jw A01;
    public List A02;

    public C54492go(AnonymousClass4J0 r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    public static final Chip A00(Context context) {
        Chip chip = new Chip(context, null);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.product_margin_4dp);
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, -2);
        marginLayoutParams.leftMargin = dimensionPixelSize;
        marginLayoutParams.rightMargin = dimensionPixelSize;
        chip.setLayoutParams(marginLayoutParams);
        chip.setChipEndPadding(context.getResources().getDimension(R.dimen.dir_margin_10));
        chip.setChipStartPadding(context.getResources().getDimension(R.dimen.dir_margin_10));
        chip.setCloseIconResource(R.drawable.ic_chevron_down);
        return chip;
    }

    public static Chip A01(View view) {
        Context context = view.getContext();
        C16700pc.A0B(context);
        return A00(context);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        List list = this.A02;
        if (list != null) {
            return list.size();
        }
        throw C16700pc.A06("filterListItems");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        AbstractC75703kH r22 = (AbstractC75703kH) r2;
        C16700pc.A0E(r22, 0);
        List list = this.A02;
        if (list == null) {
            throw C16700pc.A06("filterListItems");
        }
        r22.A08((AnonymousClass4UW) list.get(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        Context context = viewGroup.getContext();
        switch (i) {
            case 0:
                C16700pc.A0B(context);
                Chip A00 = A00(context);
                AnonymousClass2Jw r0 = this.A01;
                if (r0 != null) {
                    return new C60002vj(A00, r0);
                }
                throw C16700pc.A06("onItemClickListener");
            case 1:
                Chip A01 = A01(viewGroup);
                AnonymousClass2Jw r02 = this.A01;
                if (r02 != null) {
                    return new C59982vh(A01, r02);
                }
                throw C16700pc.A06("onItemClickListener");
            case 2:
                Chip A012 = A01(viewGroup);
                AnonymousClass2Jw r03 = this.A01;
                if (r03 != null) {
                    return new C59992vi(A012, r03);
                }
                throw C16700pc.A06("onItemClickListener");
            case 3:
                Chip A013 = A01(viewGroup);
                AnonymousClass2Jw r04 = this.A01;
                if (r04 != null) {
                    return new C60032vm(A013, r04);
                }
                throw C16700pc.A06("onItemClickListener");
            case 4:
                Chip A014 = A01(viewGroup);
                AnonymousClass2Jw r05 = this.A01;
                if (r05 != null) {
                    return new C60012vk(A014, r05);
                }
                throw C16700pc.A06("onItemClickListener");
            case 5:
                Chip A015 = A01(viewGroup);
                AnonymousClass2Jw r06 = this.A01;
                if (r06 != null) {
                    return new C60022vl(A015, r06);
                }
                throw C16700pc.A06("onItemClickListener");
            case 6:
                Context context2 = viewGroup.getContext();
                C16700pc.A0B(context2);
                View A0O = C12980iv.A0O(LayoutInflater.from(context2), R.layout.biz_dir_filterbar_clear_button);
                A0O.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                A0O.getLayoutParams().height = context2.getResources().getDimensionPixelSize(R.dimen.directory_filterbar_height);
                AnonymousClass2Jw r07 = this.A01;
                if (r07 != null) {
                    return new C848440a(A0O, r07);
                }
                throw C16700pc.A06("onItemClickListener");
            case 7:
                AnonymousClass4J0 r08 = this.A00;
                Chip A016 = A01(viewGroup);
                AnonymousClass2Jw r1 = this.A01;
                if (r1 != null) {
                    return new C60042vn(A016, r1, C12960it.A0R(r08.A00.A04));
                }
                throw C16700pc.A06("onItemClickListener");
            default:
                throw C12960it.A0U(C16700pc.A08("FilterBarAdapter /onCreateViewHolder unhandled view type: ", Integer.valueOf(i)));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        List list = this.A02;
        if (list == null) {
            throw C16700pc.A06("filterListItems");
        }
        Object obj = list.get(i);
        if (obj instanceof AnonymousClass409) {
            return 0;
        }
        if (obj instanceof C59692vE) {
            return 1;
        }
        if (obj instanceof C59682vD) {
            return 2;
        }
        if (obj instanceof AnonymousClass40B) {
            return 7;
        }
        if (obj instanceof AnonymousClass40D) {
            return 3;
        }
        if (obj instanceof AnonymousClass40C) {
            return 4;
        }
        if (obj instanceof AnonymousClass40A) {
            return 5;
        }
        if (obj instanceof AnonymousClass408) {
            return 6;
        }
        throw C12990iw.A0v();
    }
}
