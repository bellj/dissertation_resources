package X;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashSet;

/* renamed from: X.4is  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98554is implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78943pn[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int i;
        int A01 = C95664e9.A01(parcel);
        HashSet A12 = C12970iu.A12();
        String str = null;
        byte[] bArr = null;
        PendingIntent pendingIntent = null;
        C78373oo r4 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i2 = C95664e9.A02(parcel, readInt);
                    i = 1;
                    break;
                case 2:
                    str = C95664e9.A08(parcel, readInt);
                    i = 2;
                    break;
                case 3:
                    i3 = C95664e9.A02(parcel, readInt);
                    i = 3;
                    break;
                case 4:
                    bArr = C95664e9.A0I(parcel, readInt);
                    i = 4;
                    break;
                case 5:
                    pendingIntent = (PendingIntent) C95664e9.A07(parcel, PendingIntent.CREATOR, readInt);
                    i = 5;
                    break;
                case 6:
                    r4 = (C78373oo) C95664e9.A07(parcel, C78373oo.CREATOR, readInt);
                    i = 6;
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    continue;
            }
            C12980iv.A1R(A12, i);
        }
        if (parcel.dataPosition() == A01) {
            return new C78943pn(pendingIntent, r4, str, A12, bArr, i2, i3);
        }
        throw new C113235Gs(parcel, C12960it.A0e("Overread allowed size end=", C12980iv.A0t(37), A01));
    }
}
