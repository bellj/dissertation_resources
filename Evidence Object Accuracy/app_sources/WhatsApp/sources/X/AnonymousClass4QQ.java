package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.4QQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4QQ {
    public final PhotoView A00;
    public final AbstractC16130oV A01;
    public final /* synthetic */ RunnableBRunnable0Shape0S0310000_I0 A02;

    public AnonymousClass4QQ(RunnableBRunnable0Shape0S0310000_I0 runnableBRunnable0Shape0S0310000_I0, PhotoView photoView, AbstractC16130oV r3) {
        this.A02 = runnableBRunnable0Shape0S0310000_I0;
        this.A01 = r3;
        this.A00 = photoView;
    }
}
