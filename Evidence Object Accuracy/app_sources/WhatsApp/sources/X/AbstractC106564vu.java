package X;

import com.google.android.exoplayer2.decoder.SimpleOutputBuffer;
import com.google.android.exoplayer2.ext.opus.OpusDecoder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.List;

/* renamed from: X.4vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC106564vu implements AnonymousClass5XF {
    public int A00;
    public int A01;
    public int A02;
    public AnonymousClass4CJ A03;
    public C76763mA A04;
    public boolean A05;
    public boolean A06;
    public final Object A07 = C12970iu.A0l();
    public final Thread A08;
    public final ArrayDeque A09 = new ArrayDeque();
    public final ArrayDeque A0A = new ArrayDeque();
    public final C76763mA[] A0B;
    public final AbstractC76693m3[] A0C;

    public AbstractC106564vu(C76763mA[] r6, AbstractC76693m3[] r7) {
        AbstractC76693m3 r1;
        C76763mA r0;
        this.A0B = r6;
        this.A00 = r6.length;
        for (int i = 0; i < this.A00; i++) {
            C76763mA[] r2 = this.A0B;
            if (!(this instanceof AbstractC76783mE)) {
                r0 = new C76763mA(2);
            } else {
                r0 = new C76743m8();
            }
            r2[i] = r0;
        }
        this.A0C = r7;
        int length = r7.length;
        this.A01 = length;
        for (int i2 = 0; i2 < length; i2++) {
            if (!(this instanceof AbstractC76783mE)) {
                r1 = new SimpleOutputBuffer(new AnonymousClass5SI() { // from class: X.4vv
                    @Override // X.AnonymousClass5SI
                    public final void Aa8(AbstractC76693m3 r22) {
                        OpusDecoder.this.A06(r22);
                    }
                });
            } else {
                r1 = new C77273n1(new AnonymousClass5SI() { // from class: X.4vw
                    @Override // X.AnonymousClass5SI
                    public final void Aa8(AbstractC76693m3 r22) {
                        AbstractC76783mE.this.A06(r22);
                    }
                });
            }
            r7[i2] = r1;
        }
        AnonymousClass5HC r02 = new AnonymousClass5HC(this);
        this.A08 = r02;
        r02.start();
    }

    public static int A04(List list, int i) {
        return (int) ((ByteBuffer.wrap((byte[]) list.get(i)).order(ByteOrder.nativeOrder()).getLong() * 48000) / 1000000000);
    }

    public AnonymousClass4CJ A05(C76763mA r8, AbstractC76693m3 r9, boolean z) {
        AbstractC76783mE r2 = (AbstractC76783mE) this;
        C76743m8 r82 = (C76743m8) r8;
        AbstractC76773mC r92 = (AbstractC76773mC) r9;
        try {
            ByteBuffer byteBuffer = r82.A01;
            AbstractC116805Wy A07 = r2.A07(byteBuffer.array(), byteBuffer.limit(), z);
            long j = ((C76763mA) r82).A00;
            long j2 = r82.A00;
            r92.timeUs = j;
            r92.A01 = A07;
            if (j2 != Long.MAX_VALUE) {
                j = j2;
            }
            r92.A00 = j;
            r92.clearFlag(Integer.MIN_VALUE);
            return null;
        } catch (C76723m6 e) {
            return e;
        }
    }

    public void A06(AbstractC76693m3 r5) {
        Object obj = this.A07;
        synchronized (obj) {
            r5.clear();
            AbstractC76693m3[] r2 = this.A0C;
            int i = this.A01;
            this.A01 = i + 1;
            r2[i] = r5;
            if (!this.A09.isEmpty() && this.A01 > 0) {
                obj.notify();
            }
        }
    }

    @Override // X.AnonymousClass5XF
    public /* bridge */ /* synthetic */ Object A8n() {
        C76763mA r0;
        synchronized (this.A07) {
            AnonymousClass4CJ r02 = this.A03;
            if (r02 == null) {
                C95314dV.A04(C12980iv.A1X(this.A04));
                int i = this.A00;
                if (i == 0) {
                    r0 = null;
                } else {
                    C76763mA[] r03 = this.A0B;
                    int i2 = i - 1;
                    this.A00 = i2;
                    r0 = r03[i2];
                }
                this.A04 = r0;
            } else {
                throw r02;
            }
        }
        return r0;
    }

    @Override // X.AnonymousClass5XF
    public /* bridge */ /* synthetic */ Object A8o() {
        AbstractC76693m3 r0;
        synchronized (this.A07) {
            AnonymousClass4CJ r02 = this.A03;
            if (r02 == null) {
                ArrayDeque arrayDeque = this.A0A;
                if (arrayDeque.isEmpty()) {
                    r0 = null;
                } else {
                    r0 = (AbstractC76693m3) arrayDeque.removeFirst();
                }
            } else {
                throw r02;
            }
        }
        return r0;
    }

    @Override // X.AnonymousClass5XF
    public /* bridge */ /* synthetic */ void AZl(Object obj) {
        Object obj2 = this.A07;
        synchronized (obj2) {
            AnonymousClass4CJ r0 = this.A03;
            if (r0 == null) {
                C95314dV.A03(C12970iu.A1Z(obj, this.A04));
                ArrayDeque arrayDeque = this.A09;
                arrayDeque.addLast(obj);
                if (!arrayDeque.isEmpty() && this.A01 > 0) {
                    obj2.notify();
                }
                this.A04 = null;
            } else {
                throw r0;
            }
        }
    }

    @Override // X.AnonymousClass5XF
    public final void flush() {
        synchronized (this.A07) {
            this.A05 = true;
            this.A02 = 0;
            C76763mA r3 = this.A04;
            if (r3 != null) {
                r3.clear();
                C76763mA[] r2 = this.A0B;
                int i = this.A00;
                this.A00 = i + 1;
                r2[i] = r3;
                this.A04 = null;
            }
            while (true) {
                ArrayDeque arrayDeque = this.A09;
                if (!arrayDeque.isEmpty()) {
                    C76763mA r32 = (C76763mA) arrayDeque.removeFirst();
                    r32.clear();
                    C76763mA[] r22 = this.A0B;
                    int i2 = this.A00;
                    this.A00 = i2 + 1;
                    r22[i2] = r32;
                }
            }
            while (true) {
                ArrayDeque arrayDeque2 = this.A0A;
                if (!arrayDeque2.isEmpty()) {
                    ((AbstractC76693m3) arrayDeque2.removeFirst()).release();
                }
            }
        }
    }

    @Override // X.AnonymousClass5XF
    public void release() {
        Object obj = this.A07;
        synchronized (obj) {
            this.A06 = true;
            obj.notify();
        }
        try {
            this.A08.join();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
    }
}
