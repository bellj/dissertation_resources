package X;

import android.view.View;
import android.widget.TextView;
import com.google.android.material.chip.ChipGroup;

/* renamed from: X.3Mi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnLayoutChangeListenerC66113Mi implements View.OnLayoutChangeListener {
    public final /* synthetic */ C620934k A00;

    public View$OnLayoutChangeListenerC66113Mi(C620934k r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9;
        if (i3 - i != i7 - i5) {
            C620934k r5 = this.A00;
            if (r5.getWidth() > 0) {
                ChipGroup chipGroup = ((AbstractC621034l) r5).A01;
                if (chipGroup.getChildCount() > 0 && (i9 = r5.getMaxChipWidth()) > 0) {
                    for (int i10 = 0; i10 < chipGroup.getChildCount(); i10++) {
                        ((TextView) chipGroup.getChildAt(i10)).setMaxWidth(i9);
                    }
                    r5.measure(C12980iv.A04(r5.getWidth()), View.MeasureSpec.makeMeasureSpec(0, 0));
                    r5.removeOnLayoutChangeListener(this);
                    r5.layout(i, i2, i3, r5.getMeasuredHeight() + i2);
                    r5.addOnLayoutChangeListener(this);
                }
            }
        }
    }
}
