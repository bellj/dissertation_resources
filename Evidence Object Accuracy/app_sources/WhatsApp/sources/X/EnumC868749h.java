package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC868749h extends Enum {
    public static final /* synthetic */ EnumC868749h[] A00;
    public static final EnumC868749h A01;
    public static final EnumC868749h A02;
    public static final EnumC868749h A03;
    public static final EnumC868749h A04;

    public static EnumC868749h[] values() {
        return (EnumC868749h[]) A00.clone();
    }

    static {
        EnumC868749h r6 = new EnumC868749h("ACTIVITY", 0);
        A01 = r6;
        EnumC868749h r5 = new EnumC868749h("FRAGMENT", 1);
        A03 = r5;
        EnumC868749h r4 = new EnumC868749h("FIRST_FRAGMENT", 2);
        A02 = r4;
        EnumC868749h r2 = new EnumC868749h("NOT_IDENTIFIED", 3);
        A04 = r2;
        EnumC868749h[] r1 = new EnumC868749h[4];
        C12970iu.A1U(r6, r5, r1);
        r1[2] = r4;
        r1[3] = r2;
        A00 = r1;
    }

    public EnumC868749h(String str, int i) {
    }
}
