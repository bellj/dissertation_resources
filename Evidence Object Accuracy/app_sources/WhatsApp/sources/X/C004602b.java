package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/* renamed from: X.02b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C004602b extends TextView implements AnonymousClass012, AnonymousClass02c, AbstractC004702d {
    public Future A00;
    public boolean A01;
    public final AnonymousClass085 A02;
    public final AnonymousClass087 A03;
    public final AnonymousClass086 A04;

    public C004602b(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public C004602b(Context context, AttributeSet attributeSet, int i) {
        super(AnonymousClass083.A00(context), attributeSet, i);
        this.A01 = false;
        AnonymousClass084.A03(getContext(), this);
        AnonymousClass085 r0 = new AnonymousClass085(this);
        this.A02 = r0;
        r0.A05(attributeSet, i);
        AnonymousClass086 r02 = new AnonymousClass086(this);
        this.A04 = r02;
        r02.A0A(attributeSet, i);
        r02.A02();
        this.A03 = new AnonymousClass087(this);
    }

    public static AnonymousClass089 A00(TextView textView) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            return new AnonymousClass089(AnonymousClass088.A00(textView));
        }
        AnonymousClass08A r1 = new AnonymousClass08A(new TextPaint(textView.getPaint()));
        if (i >= 23) {
            r1.A01(AnonymousClass08B.A00(textView));
            r1.A02(AnonymousClass08B.A01(textView));
        }
        if (i >= 18) {
            r1.A03(AnonymousClass04D.A01(textView));
        }
        return r1.A00();
    }

    public static void A01(TextView textView) {
        if (Build.VERSION.SDK_INT >= 29) {
            throw new NullPointerException("getPrecomputedText");
        }
        A00(textView);
        throw new NullPointerException("getParams");
    }

    @Override // android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass085 r0 = this.A02;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass086 r02 = this.A04;
        if (r02 != null) {
            r02.A02();
        }
    }

    @Override // android.widget.TextView
    public int getAutoSizeMaxTextSize() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeMaxTextSize();
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            return Math.round(r0.A0C.A00);
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeMinTextSize() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeMinTextSize();
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            return Math.round(r0.A0C.A01);
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeStepGranularity() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeStepGranularity();
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            return Math.round(r0.A0C.A02);
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int[] getAutoSizeTextAvailableSizes() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeTextAvailableSizes();
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            return r0.A0C.A07;
        }
        return new int[0];
    }

    @Override // android.widget.TextView
    public int getAutoSizeTextType() {
        if (!AnonymousClass02c.A00) {
            AnonymousClass086 r0 = this.A04;
            if (r0 != null) {
                return r0.A0C.A03;
            }
            return 0;
        } else if (super.getAutoSizeTextType() == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override // android.widget.TextView
    public int getFirstBaselineToTopHeight() {
        return getPaddingTop() - getPaint().getFontMetricsInt().top;
    }

    @Override // android.widget.TextView
    public int getLastBaselineToBottomHeight() {
        return getPaddingBottom() + getPaint().getFontMetricsInt().bottom;
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        C016107p r0;
        AnonymousClass085 r02 = this.A02;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C016107p r0;
        AnonymousClass085 r02 = this.A02;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A01;
    }

    public ColorStateList getSupportCompoundDrawablesTintList() {
        C016107p r0 = this.A04.A08;
        if (r0 != null) {
            return r0.A00;
        }
        return null;
    }

    public PorterDuff.Mode getSupportCompoundDrawablesTintMode() {
        C016107p r0 = this.A04.A08;
        if (r0 != null) {
            return r0.A01;
        }
        return null;
    }

    @Override // android.widget.TextView
    public CharSequence getText() {
        Future future = this.A00;
        if (future != null) {
            try {
                this.A00 = null;
                future.get();
                A01(this);
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            } catch (InterruptedException | ExecutionException unused) {
            }
        }
        return super.getText();
    }

    @Override // android.widget.TextView
    public TextClassifier getTextClassifier() {
        AnonymousClass087 r0;
        if (Build.VERSION.SDK_INT >= 28 || (r0 = this.A03) == null) {
            return super.getTextClassifier();
        }
        return r0.A00();
    }

    public AnonymousClass089 getTextMetricsParamsCompat() {
        return A00(this);
    }

    @Override // android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        AnonymousClass086.A01(editorInfo, onCreateInputConnection, this);
        AnonymousClass08D.A00(this, editorInfo, onCreateInputConnection);
        return onCreateInputConnection;
    }

    @Override // android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        AnonymousClass086 r1 = this.A04;
        if (r1 != null && !AnonymousClass02c.A00) {
            r1.A0C.A04();
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        Future future = this.A00;
        if (future != null) {
            try {
                this.A00 = null;
                future.get();
                A01(this);
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            } catch (InterruptedException | ExecutionException unused) {
            }
        }
        super.onMeasure(i, i2);
    }

    @Override // android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        AnonymousClass086 r1 = this.A04;
        if (r1 != null && !AnonymousClass02c.A00) {
            AnonymousClass08C r12 = r1.A0C;
            if ((!(r12.A09 instanceof AnonymousClass011)) && r12.A03 != 0) {
                r12.A04();
            }
        }
    }

    @Override // android.widget.TextView, X.AnonymousClass02c
    public void setAutoSizeTextTypeUniformWithConfiguration(int i, int i2, int i3, int i4) {
        if (AnonymousClass02c.A00) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i, i2, i3, i4);
            return;
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A04(i, i2, i3, i4);
        }
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i) {
        if (AnonymousClass02c.A00) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i);
            return;
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A0B(iArr, i);
        }
    }

    @Override // android.widget.TextView, X.AnonymousClass02c
    public void setAutoSizeTextTypeWithDefaults(int i) {
        if (AnonymousClass02c.A00) {
            super.setAutoSizeTextTypeWithDefaults(i);
            return;
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A03(i);
        }
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AnonymousClass085 r0 = this.A02;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        AnonymousClass085 r0 = this.A02;
        if (r0 != null) {
            r0.A02(i);
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A02();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A02();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        Drawable drawable;
        Drawable drawable2;
        Drawable drawable3;
        Context context = getContext();
        Drawable drawable4 = null;
        if (i != 0) {
            drawable = C012005t.A01().A04(context, i);
        } else {
            drawable = null;
        }
        if (i2 != 0) {
            drawable2 = C012005t.A01().A04(context, i2);
        } else {
            drawable2 = null;
        }
        if (i3 != 0) {
            drawable3 = C012005t.A01().A04(context, i3);
        } else {
            drawable3 = null;
        }
        if (i4 != 0) {
            drawable4 = C012005t.A01().A04(context, i4);
        }
        setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A02();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A02();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(int i, int i2, int i3, int i4) {
        Drawable drawable;
        Drawable drawable2;
        Drawable drawable3;
        Context context = getContext();
        Drawable drawable4 = null;
        if (i != 0) {
            drawable = C012005t.A01().A04(context, i);
        } else {
            drawable = null;
        }
        if (i2 != 0) {
            drawable2 = C012005t.A01().A04(context, i2);
        } else {
            drawable2 = null;
        }
        if (i3 != 0) {
            drawable3 = C012005t.A01().A04(context, i3);
        } else {
            drawable3 = null;
        }
        if (i4 != 0) {
            drawable4 = C012005t.A01().A04(context, i4);
        }
        setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A02();
        }
    }

    @Override // android.widget.TextView
    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A02();
        }
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(AnonymousClass04D.A02(callback, this));
    }

    @Override // android.widget.TextView
    public void setFirstBaselineToTopHeight(int i) {
        if (Build.VERSION.SDK_INT >= 28) {
            super.setFirstBaselineToTopHeight(i);
        } else {
            AnonymousClass04D.A06(this, i);
        }
    }

    @Override // android.widget.TextView
    public void setLastBaselineToBottomHeight(int i) {
        if (Build.VERSION.SDK_INT >= 28) {
            super.setLastBaselineToBottomHeight(i);
        } else {
            AnonymousClass04D.A07(this, i);
        }
    }

    @Override // android.widget.TextView
    public void setLineHeight(int i) {
        if (i >= 0) {
            int fontMetricsInt = getPaint().getFontMetricsInt(null);
            if (i != fontMetricsInt) {
                setLineSpacing((float) (i - fontMetricsInt), 1.0f);
                return;
            }
            return;
        }
        throw new IllegalArgumentException();
    }

    public void setPrecomputedText(AnonymousClass08E r3) {
        A01(this);
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AnonymousClass085 r0 = this.A02;
        if (r0 != null) {
            r0.A03(colorStateList);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AnonymousClass085 r0 = this.A02;
        if (r0 != null) {
            r0.A04(mode);
        }
    }

    @Override // X.AbstractC004702d
    public void setSupportCompoundDrawablesTintList(ColorStateList colorStateList) {
        AnonymousClass086 r0 = this.A04;
        r0.A07(colorStateList);
        r0.A02();
    }

    @Override // X.AbstractC004702d
    public void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode) {
        AnonymousClass086 r0 = this.A04;
        r0.A08(mode);
        r0.A02();
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            r0.A05(context, i);
        }
    }

    @Override // android.widget.TextView
    public void setTextClassifier(TextClassifier textClassifier) {
        AnonymousClass087 r0;
        if (Build.VERSION.SDK_INT >= 28 || (r0 = this.A03) == null) {
            super.setTextClassifier(textClassifier);
        } else {
            r0.A01(textClassifier);
        }
    }

    public void setTextFuture(Future future) {
        this.A00 = future;
        if (future != null) {
            requestLayout();
        }
    }

    public void setTextMetricsParamsCompat(AnonymousClass089 r4) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 18) {
            AnonymousClass08F.A04(this, AnonymousClass04D.A00(r4.A02()));
        }
        if (i < 23) {
            TextPaint textPaint = r4.A04;
            float textScaleX = textPaint.getTextScaleX();
            getPaint().set(textPaint);
            if (textScaleX == getTextScaleX()) {
                setTextScaleX((textScaleX / 2.0f) + 1.0f);
            }
            setTextScaleX(textScaleX);
            return;
        }
        getPaint().set(r4.A04);
        AnonymousClass08B.A04(this, r4.A00());
        AnonymousClass08B.A05(this, r4.A01());
    }

    @Override // android.widget.TextView
    public void setTextSize(int i, float f) {
        if (AnonymousClass02c.A00) {
            super.setTextSize(i, f);
            return;
        }
        AnonymousClass086 r0 = this.A04;
        if (r0 != null) {
            AnonymousClass08C r1 = r0.A0C;
            if (!(!(r1.A09 instanceof AnonymousClass011)) || r1.A03 == 0) {
                r1.A06(i, f);
            }
        }
    }

    @Override // android.widget.TextView
    public void setTypeface(Typeface typeface, int i) {
        if (!this.A01) {
            Typeface typeface2 = null;
            if (typeface != null && i > 0) {
                typeface2 = AnonymousClass082.A00(getContext(), typeface, i);
            }
            this.A01 = true;
            if (typeface2 != null) {
                typeface = typeface2;
            }
            try {
                super.setTypeface(typeface, i);
            } finally {
                this.A01 = false;
            }
        }
    }
}
