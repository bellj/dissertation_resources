package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.protocol.VoipStanzaChildNode;

/* renamed from: X.2PB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PB extends AnonymousClass2MM {
    public final long A00;
    public final Jid A01;
    public final boolean A02;

    public AnonymousClass2PB(Jid jid, UserJid userJid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, long j) {
        super(userJid, voipStanzaChildNode, str, str2, true);
        this.A01 = jid;
        this.A02 = false;
        this.A00 = j;
    }

    public AnonymousClass2PB(Jid jid, UserJid userJid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, long j, boolean z) {
        super(userJid, voipStanzaChildNode, str, str2, true);
        this.A01 = jid;
        this.A02 = z;
        this.A00 = j;
    }
}
