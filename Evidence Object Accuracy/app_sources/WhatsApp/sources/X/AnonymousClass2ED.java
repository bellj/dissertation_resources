package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jobqueue.job.ReceiptProcessingJob;

/* renamed from: X.2ED  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2ED implements AbstractC47922Dh {
    public final int A00;
    public final long A01;
    public final DeviceJid A02;
    public final Jid A03;
    public final C32141bg A04;
    public final AnonymousClass1OT A05;
    public final boolean A06;
    public final AnonymousClass1IS[] A07;

    public AnonymousClass2ED(DeviceJid deviceJid, Jid jid, C32141bg r3, AnonymousClass1OT r4, AnonymousClass1IS[] r5, int i, long j, boolean z) {
        this.A07 = r5;
        this.A03 = jid;
        this.A02 = deviceJid;
        this.A00 = i;
        this.A01 = j;
        this.A05 = r4;
        this.A06 = z;
        this.A04 = r3;
    }

    @Override // X.AbstractC47922Dh
    public boolean AJo() {
        return this.A06;
    }

    @Override // X.AbstractC47922Dh
    public AnonymousClass1IS AKM(int i) {
        return this.A07[i];
    }

    @Override // X.AbstractC47922Dh
    public DeviceJid AYy(int i) {
        return this.A02;
    }

    @Override // X.AbstractC47922Dh
    public C32141bg AZx() {
        return this.A04;
    }

    @Override // X.AbstractC47922Dh
    public Jid AaD() {
        return this.A03;
    }

    @Override // X.AbstractC47922Dh
    public void AbL(C20670w8 r9, int i) {
        AnonymousClass1IS[] r2 = this.A07;
        int length = r2.length - i;
        AnonymousClass1IS[] r4 = new AnonymousClass1IS[length];
        System.arraycopy(r2, i, r4, 0, length);
        Jid jid = this.A03;
        r9.A00(new ReceiptProcessingJob(this.A02, jid, this.A04, r4, this.A00, this.A01));
    }

    @Override // X.AbstractC47922Dh
    public AnonymousClass1OT Ae6() {
        return this.A05;
    }

    @Override // X.AbstractC47922Dh
    public int AeP() {
        return this.A00;
    }

    @Override // X.AbstractC47922Dh
    public long Aeq(int i) {
        return this.A01;
    }

    @Override // X.AbstractC47922Dh
    public int size() {
        return this.A07.length;
    }
}
