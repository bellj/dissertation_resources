package X;

import android.net.Uri;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.LruCache;
import com.facebook.redex.RunnableBRunnable0Shape0S0301000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.whatsapp.Me;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.0wS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20870wS {
    public static final AnonymousClass00E A0N = new AnonymousClass00E(1, 1);
    public static final AnonymousClass00E A0O = new AnonymousClass00E(1, 50, 100);
    public final LruCache A00 = new LruCache(50);
    public final AbstractC15710nm A01;
    public final C15570nT A02;
    public final C14830m7 A03;
    public final C245315w A04;
    public final C19990v2 A05;
    public final C15600nX A06;
    public final C20140vH A07;
    public final C22180yf A08;
    public final AnonymousClass150 A09;
    public final C14850m9 A0A;
    public final C16120oU A0B;
    public final C14840m8 A0C;
    public final C17230qT A0D;
    public final C245615z A0E;
    public final AnonymousClass160 A0F;
    public final C15860o1 A0G;
    public final AnonymousClass161 A0H;
    public final AnonymousClass15H A0I;
    public final C245515y A0J;
    public final ExecutorC27271Gr A0K;
    public final AbstractC14440lR A0L;
    public final boolean A0M;

    public static int A00(int i) {
        if (i < 32) {
            return 1;
        }
        if (i < 64) {
            return 2;
        }
        if (i < 128) {
            return 3;
        }
        if (i < 256) {
            return 4;
        }
        if (i < 512) {
            return 5;
        }
        if (i < 1000) {
            return 6;
        }
        if (i < 1500) {
            return 7;
        }
        if (i < 2000) {
            return 8;
        }
        if (i < 2500) {
            return 9;
        }
        if (i < 3000) {
            return 10;
        }
        if (i < 3500) {
            return 11;
        }
        if (i < 4000) {
            return 12;
        }
        if (i >= 4500) {
            return i < 5000 ? 14 : 15;
        }
        return 13;
    }

    public C20870wS(AbstractC15710nm r4, C15570nT r5, C14830m7 r6, C245315w r7, C19990v2 r8, C15600nX r9, C20140vH r10, C22180yf r11, AnonymousClass150 r12, C14850m9 r13, C16120oU r14, C14840m8 r15, C17230qT r16, C245615z r17, AnonymousClass160 r18, C15860o1 r19, AnonymousClass161 r20, AnonymousClass15H r21, C245515y r22, AbstractC14440lR r23) {
        this.A03 = r6;
        this.A0A = r13;
        this.A01 = r4;
        this.A02 = r5;
        this.A0L = r23;
        this.A05 = r8;
        this.A07 = r10;
        this.A0B = r14;
        this.A08 = r11;
        this.A0G = r19;
        this.A0C = r15;
        this.A0D = r16;
        this.A0J = r22;
        this.A04 = r7;
        this.A0I = r21;
        this.A06 = r9;
        this.A0E = r17;
        this.A0F = r18;
        this.A0H = r20;
        this.A09 = r12;
        this.A0K = new ExecutorC27271Gr(r23, false);
        this.A0M = r13.A07(1191);
    }

    public static int A01(C22180yf r3, AbstractC15340mz r4) {
        AbstractC33751f1 A18;
        C33711ex ACL;
        AbstractC15340mz A0E;
        if (r4 instanceof AnonymousClass1X9) {
            return 34;
        }
        if (r4 instanceof C27671Iq) {
            return 37;
        }
        if (r4 instanceof C27711Iw) {
            return 38;
        }
        if (C30041Vv.A0l(r4)) {
            return 30;
        }
        if (r4.A0y()) {
            return 27;
        }
        if ((r4 instanceof C28861Ph) && (A0E = r4.A0E()) != null && A0E.A0y == 54) {
            return 1;
        }
        if (!(r4 instanceof AbstractC16390ow) || (ACL = ((AbstractC16390ow) r4).ACL()) == null) {
            if ((r4 instanceof AnonymousClass1XE) && (A18 = ((AnonymousClass1XE) r4).A18()) != null) {
                return A18.ADz();
            }
            int A00 = C33761f2.A00(r4.A0y, r4.A08, C30041Vv.A0q(r4));
            if (A00 == 9 && r4.A0x()) {
                if (r3.A0A(C33771f3.A01(r4.A0I()))) {
                    return 22;
                }
                String A01 = C33771f3.A01(r4.A0I());
                if (!TextUtils.isEmpty(A01) && 5 == r3.A05(Uri.parse(A01))) {
                    return 23;
                }
            }
            return A00;
        } else if (ACL instanceof C33721ey) {
            return 33;
        } else {
            if (ACL instanceof C33731ez) {
                return 25;
            }
            if (ACL instanceof C33741f0) {
                return 35;
            }
            return 1;
        }
    }

    public static int A02(Jid jid) {
        if (C15380n4.A0J(jid)) {
            return 2;
        }
        if (C15380n4.A0N(jid)) {
            return 4;
        }
        return C15380n4.A0G(jid) ? 3 : 1;
    }

    public static int A03(AbstractC15340mz r1) {
        AbstractC14640lm r12 = r1.A0z.A00;
        boolean A0N2 = C15380n4.A0N(r12);
        boolean A0J = C15380n4.A0J(r12);
        if (A0N2) {
            return 3;
        }
        return A0J ? 2 : 1;
    }

    public static int A04(AbstractC15340mz r1) {
        AbstractC14640lm r12 = r1.A0z.A00;
        if (C15380n4.A0J(r12)) {
            return 2;
        }
        if (C15380n4.A0N(r12)) {
            return 4;
        }
        return C15380n4.A0F(r12) ? 3 : 1;
    }

    public static Boolean A05(C15570nT r2, AnonymousClass1IS r3) {
        AbstractC14640lm r0;
        r2.A08();
        Me me = r2.A00;
        if (me == null || (r0 = r3.A00) == null) {
            return null;
        }
        return Boolean.valueOf(!r0.getRawString().startsWith(me.cc));
    }

    public static Integer A06(int i) {
        int i2;
        if (i != 7) {
            i2 = 1;
            if (i != 8) {
                return null;
            }
        } else {
            i2 = 0;
        }
        return Integer.valueOf(i2);
    }

    public static Integer A07(int i) {
        int i2 = 10;
        if (i != -10000) {
            i2 = 33;
            if (i != -9999) {
                i2 = 24;
                if (i != -1201) {
                    i2 = 25;
                    if (i != -1200) {
                        i2 = 11;
                        if (i != -1100) {
                            i2 = 26;
                            if (i != -22) {
                                if (i != -12) {
                                    switch (i) {
                                        case -1011:
                                            i2 = 32;
                                            break;
                                        case -1010:
                                            i2 = 5;
                                            break;
                                        case -1009:
                                            i2 = 31;
                                            break;
                                        case -1008:
                                            i2 = 6;
                                            break;
                                        case -1007:
                                            i2 = 1;
                                            break;
                                        case -1006:
                                            i2 = 2;
                                            break;
                                        case -1005:
                                            i2 = 0;
                                            break;
                                        case -1004:
                                            i2 = 29;
                                            break;
                                        case -1003:
                                            i2 = 28;
                                            break;
                                        case -1002:
                                            i2 = 27;
                                            break;
                                        case -1001:
                                            i2 = 23;
                                            break;
                                        case -1000:
                                            i2 = 34;
                                            break;
                                        default:
                                            return null;
                                    }
                                } else {
                                    i2 = 30;
                                }
                            }
                        }
                    }
                }
            }
        }
        return Integer.valueOf(i2);
    }

    public final Integer A08(AbstractC15340mz r4) {
        AbstractC14640lm r2;
        if (r4 == null || (r2 = r4.A0z.A00) == null || !C15380n4.A0J(r2)) {
            return null;
        }
        C19990v2 r1 = this.A05;
        GroupJid groupJid = (GroupJid) r2;
        if (!C15380n4.A0J(groupJid)) {
            return null;
        }
        int intValue = Integer.valueOf(r1.A03(groupJid)).intValue();
        int i = 2;
        if (intValue != 2) {
            i = 3;
            if (intValue != 3) {
                i = 1;
            }
        }
        return Integer.valueOf(i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(X.C31801b8 r5, com.whatsapp.jid.DeviceJid r6, com.whatsapp.jid.Jid r7, X.AbstractC15340mz r8, int r9, int r10, int r11) {
        /*
            r4 = this;
            X.1f6 r3 = new X.1f6
            r3.<init>()
            r0 = 2
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r3.A08 = r0
            int r2 = r5.A00
            r0 = 1
            if (r2 != r0) goto L_0x008b
            r0 = 0
        L_0x0013:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x0017:
            r3.A02 = r0
            int r0 = r5.A01
            if (r0 == 0) goto L_0x0088
            java.lang.Integer r0 = A07(r0)
            r3.A04 = r0
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
        L_0x0025:
            r3.A00 = r0
            java.lang.Integer r0 = A06(r11)
            r3.A07 = r0
            boolean r0 = r7 instanceof com.whatsapp.jid.GroupJid
            if (r0 == 0) goto L_0x007b
            r1 = 1
        L_0x0032:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r3.A03 = r0
            if (r6 == 0) goto L_0x0045
            byte r1 = r6.device
            if (r1 != 0) goto L_0x0079
            r0 = 1
        L_0x003f:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3.A05 = r0
        L_0x0045:
            long r0 = (long) r9
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r3.A0A = r0
            if (r8 == 0) goto L_0x005a
            X.0yf r0 = r4.A08
            int r0 = A01(r0, r8)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r3.A06 = r0
        L_0x005a:
            long r0 = (long) r10
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r3.A09 = r0
            if (r8 == 0) goto L_0x006b
            boolean r0 = r8 instanceof X.AnonymousClass1X5
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r3.A01 = r0
        L_0x006b:
            java.lang.Boolean r0 = r3.A00
            boolean r0 = r0.booleanValue()
            X.0oU r2 = r4.A0B
            if (r0 == 0) goto L_0x0098
            r2.A07(r3)
            return
        L_0x0079:
            r0 = 2
            goto L_0x003f
        L_0x007b:
            boolean r0 = r7 instanceof X.C29901Ve
            if (r0 == 0) goto L_0x0081
            r1 = 2
            goto L_0x0032
        L_0x0081:
            boolean r0 = r7 instanceof X.AnonymousClass1VX
            r1 = 0
            if (r0 == 0) goto L_0x0032
            r1 = 3
            goto L_0x0032
        L_0x0088:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            goto L_0x0025
        L_0x008b:
            r1 = 2
            if (r2 == r1) goto L_0x0013
            r0 = 3
            if (r2 != r0) goto L_0x0096
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            goto L_0x0017
        L_0x0096:
            r0 = 0
            goto L_0x0017
        L_0x0098:
            r1 = 1
            X.00E r0 = new X.00E
            r0.<init>(r1, r1)
            r2.A0B(r3, r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20870wS.A09(X.1b8, com.whatsapp.jid.DeviceJid, com.whatsapp.jid.Jid, X.0mz, int, int, int):void");
    }

    public final void A0A(C33781f4 r7, String str, long j) {
        String obj;
        String obj2;
        String obj3;
        Integer num;
        Integer num2;
        Integer num3;
        C14850m9 r1 = this.A0A;
        int A02 = r1.A02(1073);
        int A022 = r1.A02(1127);
        int A023 = r1.A02(1128);
        if (A02 != 0 && ((num3 = r7.A0H) == null || A02 != num3.intValue())) {
            return;
        }
        if (A022 != 0 && ((num2 = r7.A0G) == null || A022 != num2.intValue())) {
            return;
        }
        if (A023 == 0 || ((num = r7.A0F) != null && A023 == num.intValue())) {
            Integer num4 = r7.A0H;
            if (num4 == null) {
                obj = null;
            } else {
                obj = num4.toString();
            }
            Integer num5 = r7.A0G;
            if (num5 == null) {
                obj2 = null;
            } else {
                obj2 = num5.toString();
            }
            Integer num6 = r7.A0F;
            if (num6 == null) {
                obj3 = null;
            } else {
                obj3 = num6.toString();
            }
            AbstractC15710nm r2 = this.A01;
            StringBuilder sb = new StringBuilder("stage: ");
            sb.append(obj);
            sb.append("; messageType: ");
            sb.append(obj2);
            sb.append("; mediaType: ");
            sb.append(obj3);
            sb.append("; durationTime: ");
            sb.append(j);
            r2.AaV(str, sb.toString(), true);
        }
    }

    public void A0B(DeviceJid deviceJid, int i) {
        int i2;
        int i3;
        C33791f5 r3 = new C33791f5();
        r3.A00 = Integer.valueOf(i);
        if (deviceJid != null) {
            if (deviceJid.device == 0) {
                i3 = 1;
            } else {
                i3 = 2;
            }
            i2 = Integer.valueOf(i3).intValue();
        } else {
            i2 = 1;
        }
        r3.A01 = Integer.valueOf(i2);
        this.A0B.A0B(r3, new AnonymousClass00E(1, 1), true);
    }

    public void A0C(DeviceJid deviceJid, AnonymousClass1IS r5, int i, boolean z) {
        int i2;
        C33871fD r2 = new C33871fD();
        r2.A02 = Integer.valueOf(A02(r5.A00));
        if (deviceJid.device == 0) {
            i2 = 1;
        } else {
            i2 = 2;
        }
        r2.A01 = Integer.valueOf(i2);
        r2.A03 = Long.valueOf((long) i);
        r2.A00 = Boolean.valueOf(z);
        this.A0B.A06(r2);
    }

    public void A0D(DeviceJid deviceJid, AnonymousClass1IS r5, Integer num, int i, int i2) {
        int i3;
        C33881fE r2 = new C33881fE();
        r2.A01 = Integer.valueOf(A02(r5.A00));
        if (deviceJid.device == 0) {
            i3 = 1;
        } else {
            i3 = 2;
        }
        r2.A03 = Integer.valueOf(i3);
        r2.A04 = Long.valueOf((long) i);
        boolean z = false;
        if (A06(i2) != null) {
            z = true;
        }
        r2.A00 = Boolean.valueOf(z);
        r2.A02 = num;
        this.A0B.A07(r2);
    }

    public void A0E(AbstractC15590nW r8, Integer num) {
        int i;
        if (C15380n4.A0J(r8)) {
            i = 2;
        } else if (C15380n4.A0N(r8)) {
            i = 3;
        } else if (C15380n4.A0F(r8)) {
            i = 4;
        } else {
            return;
        }
        Integer valueOf = Integer.valueOf(i);
        if (valueOf != null) {
            this.A0L.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, valueOf, r8, num, 4));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fd, code lost:
        if (r13.A06 == false) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x010b, code lost:
        if (r13.A05 == false) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0132, code lost:
        if (r13.A07 == false) goto L_0x0134;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x014e, code lost:
        if (r0.A06 == false) goto L_0x0150;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(X.AbstractC15340mz r25, int r26, int r27, int r28, int r29, long r30, boolean r32, boolean r33, boolean r34) {
        /*
        // Method dump skipped, instructions count: 1262
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20870wS.A0F(X.0mz, int, int, int, int, long, boolean, boolean, boolean):void");
    }

    public void A0G(AbstractC15340mz r29, Collection collection, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j, long j2, long j3, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        int i9;
        Boolean valueOf;
        int length;
        long longValue;
        int size;
        long j4;
        long j5;
        int i10 = i5;
        int i11 = i4;
        AnonymousClass1IS r12 = r29.A0z;
        if (r12.A02 && r29.A11()) {
            C33781f4 r1 = new C33781f4();
            r1.A0M = Long.valueOf(j3);
            r1.A0K = Long.valueOf(j);
            r1.A0L = Long.valueOf(j2);
            r1.A06 = Boolean.valueOf(r29 instanceof C30331Wz);
            r1.A0F = Integer.valueOf(A01(this.A08, r29));
            r1.A0H = Integer.valueOf(i);
            r1.A0G = Integer.valueOf(A04(r29));
            r1.A0I = A08(r29);
            int i12 = 1;
            r1.A05 = Boolean.valueOf(r29.A12(1));
            byte b = r29.A0y;
            r1.A07 = Boolean.valueOf(C30041Vv.A0I(b));
            r1.A0T = Long.valueOf((long) i3);
            r1.A0S = Long.valueOf((long) i2);
            r1.A04 = Boolean.valueOf(C30041Vv.A0x(r29, this.A0C.A05()));
            r1.A0A = Boolean.valueOf(z);
            C245315w r7 = this.A04;
            AbstractC14640lm r6 = r12.A00;
            synchronized (r7) {
                i9 = 0;
                if (r6 != null) {
                    Integer num = (Integer) r7.A00.get(r6);
                    if (num != null) {
                        i9 = num.intValue();
                    }
                }
            }
            if (i9 > 0) {
                valueOf = Boolean.TRUE;
                r1.A0C = valueOf;
            } else {
                valueOf = Boolean.valueOf(z2);
            }
            r1.A01 = valueOf;
            C14850m9 r4 = this.A0A;
            int A02 = r4.A02(767);
            if (i11 >= A02 || i9 >= A02) {
                r1.A0B = Boolean.TRUE;
            }
            r1.A00 = Boolean.valueOf(z3);
            r1.A03 = Boolean.valueOf(z4);
            if (i == 5) {
                r1.A0O = Long.valueOf((long) i6);
                r1.A0W = Long.valueOf((long) i7);
            }
            if (C15380n4.A0J(r6) || C15380n4.A0F(r6)) {
                if (i8 > 0) {
                    if (i8 <= 32) {
                        j5 = 32;
                    } else {
                        j5 = (long) i8;
                    }
                    r1.A0P = Long.valueOf(j5);
                    int i13 = 1;
                    if (i8 > 33) {
                        i13 = 2;
                        if (i8 > 65) {
                            i13 = 3;
                            if (i8 > 129) {
                                i13 = 4;
                                if (i8 > 257) {
                                    i13 = 5;
                                    if (i8 > 513) {
                                        i13 = 6;
                                        if (i8 > 1001) {
                                            i13 = 7;
                                            if (i8 > 1501) {
                                                i13 = 8;
                                                if (i8 > 2001) {
                                                    i13 = 9;
                                                    if (i8 > 2501) {
                                                        i13 = 10;
                                                        if (i8 > 3001) {
                                                            i13 = 11;
                                                            if (i8 > 3501) {
                                                                i13 = 12;
                                                                if (i8 > 4001) {
                                                                    i13 = 13;
                                                                    if (i8 > 4501) {
                                                                        i13 = 15;
                                                                        if (i8 <= 5001) {
                                                                            i13 = 14;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    r1.A0E = Integer.valueOf(i13);
                }
                if (collection != null && (size = collection.size()) > 0) {
                    if (size <= 32) {
                        j4 = 32;
                    } else {
                        j4 = (long) size;
                    }
                    r1.A0J = Long.valueOf(j4);
                    r1.A0D = Integer.valueOf(A00(size));
                    if (i9 > 0) {
                        i11 = i9;
                    } else if (i4 < 0) {
                        i11 = 0;
                    }
                    double d = (double) size;
                    r1.A0N = Long.valueOf((long) Math.ceil((((double) i11) * 100.0d) / d));
                    if (i5 < 0) {
                        i10 = 0;
                    }
                    r1.A0V = Long.valueOf((long) Math.ceil((((double) i10) * 100.0d) / d));
                }
            } else if (collection != null) {
                Iterator it = collection.iterator();
                int i14 = 0;
                while (it.hasNext()) {
                    DeviceJid deviceJid = (DeviceJid) it.next();
                    C15570nT r3 = this.A02;
                    if (r3.A0F(deviceJid) || r3.A0E(deviceJid)) {
                        i12++;
                    } else {
                        i14++;
                    }
                }
                r1.A0U = Long.valueOf((long) i12);
                r1.A0R = Long.valueOf((long) i14);
            }
            r1.A02 = Boolean.valueOf(z5);
            r1.A09 = Boolean.valueOf(r29 instanceof AbstractC30301Ww);
            this.A0K.execute(new RunnableBRunnable0Shape0S0301000_I0(this, r1, r29, i8, 0));
            r29.A16 = SystemClock.uptimeMillis();
            if (i == 3 && i9 > 0) {
                synchronized (r7) {
                    if (r6 != null) {
                        r7.A00.put(r6, 0);
                    }
                }
            }
            if (this.A0M) {
                C245615z r2 = this.A0E;
                int hashCode = r12.A01.hashCode();
                Integer num2 = r1.A0H;
                if (num2 != null) {
                    if (num2.intValue() == 3) {
                        AnonymousClass1Q5 r32 = r2.A01;
                        C21230x5 r9 = r32.A07;
                        int i15 = r32.A06.A05;
                        r9.AKz("wa_type", i15, hashCode, (long) b);
                        r1.toString();
                        Field[] declaredFields = C33781f4.class.getDeclaredFields();
                        if (declaredFields != null && (length = declaredFields.length) > 0) {
                            int i16 = 0;
                            do {
                                Field field = declaredFields[i16];
                                try {
                                    Object obj = field.get(r1);
                                    if (obj != null) {
                                        String name = field.getName();
                                        if (obj instanceof Integer) {
                                            longValue = (long) ((Number) obj).intValue();
                                        } else if (obj instanceof Long) {
                                            longValue = ((Number) obj).longValue();
                                        } else if (obj instanceof Boolean) {
                                            r9.AL1(name, i15, hashCode, ((Boolean) obj).booleanValue());
                                        } else if (obj instanceof String) {
                                            r9.AL0(name, (String) obj, i15, hashCode);
                                        }
                                        r9.AKz(name, i15, hashCode, longValue);
                                    }
                                } catch (IllegalAccessException unused) {
                                }
                                i16++;
                            } while (i16 < length);
                        }
                    }
                    r2.A04(hashCode, r1.A0H.intValue());
                }
            }
            int A022 = r4.A02(920);
            int A023 = r4.A02(1488);
            if (!z && A023 > 0 && A023 > A022 && r4.A07(919) && j >= ((long) A022) && j <= ((long) A023)) {
                A0A(r1, "MessageLogging/AbsDurationTooHigh", j);
            }
            int A024 = r4.A02(1072);
            int A025 = r4.A02(1489);
            if (!z && !z3 && A025 > 0 && A025 > A024 && r4.A07(1126) && j2 >= ((long) A024) && j2 <= ((long) A025)) {
                A0A(r1, "MessageLogging/RelativeDurationTooHigh", j2);
            }
        }
    }

    public void A0H(Long l, int[] iArr, int i, long j, boolean z) {
        C33811f7 r2 = new C33811f7();
        r2.A02 = Integer.valueOf(i);
        r2.A00 = Boolean.valueOf(z);
        r2.A07 = l;
        if (iArr != null) {
            int length = iArr.length;
            r2.A08 = Long.valueOf((long) length);
            if (length >= 1) {
                r2.A03 = Long.valueOf((long) iArr[0]);
            }
            if (length >= 2) {
                r2.A04 = Long.valueOf((long) iArr[1]);
            }
            if (length >= 3) {
                r2.A05 = Long.valueOf((long) iArr[2]);
            }
            if (length >= 4) {
                r2.A06 = Long.valueOf((long) iArr[3]);
            }
        }
        r2.A01 = Double.valueOf((double) j);
        this.A0B.A05(r2);
    }
}
