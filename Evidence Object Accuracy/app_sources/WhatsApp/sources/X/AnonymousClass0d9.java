package X;

import android.view.View;
import android.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0d9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0d9 implements Runnable {
    public final /* synthetic */ AnonymousClass0FH A00;
    public final /* synthetic */ ArrayList A01;

    public AnonymousClass0d9(AnonymousClass0FH r1, ArrayList arrayList) {
        this.A00 = r1;
        this.A01 = arrayList;
    }

    @Override // java.lang.Runnable
    public void run() {
        ArrayList arrayList = this.A01;
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C04930Np r3 = (C04930Np) it.next();
            AnonymousClass0FH r8 = this.A00;
            AnonymousClass03U r9 = r3.A04;
            int i = r3.A00;
            int i2 = r3.A01;
            int i3 = r3.A02;
            int i4 = r3.A03;
            View view = r9.A0H;
            int i5 = i3 - i;
            int i6 = i4 - i2;
            if (i5 != 0) {
                view.animate().translationX(0.0f);
            }
            if (i6 != 0) {
                view.animate().translationY(0.0f);
            }
            ViewPropertyAnimator animate = view.animate();
            r8.A04.add(r9);
            animate.setDuration(r8.A06()).setListener(new AnonymousClass09O(view, animate, r8, r9, i5, i6)).start();
        }
        arrayList.clear();
        this.A00.A05.remove(arrayList);
    }
}
