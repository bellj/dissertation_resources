package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2vX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59882vX extends AbstractC37191le {
    public final TextEmojiLabel A00;
    public final AnonymousClass1B3 A01;
    public final AnonymousClass1B8 A02;

    public C59882vX(View view, AnonymousClass1B3 r3, AnonymousClass1B8 r4) {
        super(view);
        this.A02 = r4;
        this.A01 = r3;
        this.A00 = C12970iu.A0T(view, R.id.privacy_description);
    }
}
