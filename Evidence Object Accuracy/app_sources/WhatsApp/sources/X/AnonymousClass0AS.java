package X;

import android.os.Handler;
import android.os.Message;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;

/* renamed from: X.0AS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AS extends Handler {
    public final /* synthetic */ PreferenceFragmentCompat A00;

    public AnonymousClass0AS(PreferenceFragmentCompat preferenceFragmentCompat) {
        this.A00 = preferenceFragmentCompat;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        if (message.what == 1) {
            PreferenceFragmentCompat preferenceFragmentCompat = this.A00;
            PreferenceScreen preferenceScreen = preferenceFragmentCompat.A02.A07;
            if (preferenceScreen != null) {
                preferenceFragmentCompat.A03.setAdapter(new AnonymousClass0F1(preferenceScreen));
                preferenceScreen.A06();
            }
        }
    }
}
