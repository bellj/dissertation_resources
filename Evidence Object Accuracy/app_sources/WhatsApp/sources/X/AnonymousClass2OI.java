package X;

/* renamed from: X.2OI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OI {
    public int A00;
    public long A01;
    public String A02;
    public boolean A03;

    public AnonymousClass2OI(String str, int i, long j, boolean z) {
        if (str == null || !(i == 1 || i == 2 || i == 3 || i == 4)) {
            throw new IllegalArgumentException("invalid web status");
        }
        this.A02 = str;
        this.A00 = i;
        this.A01 = j;
        this.A03 = z;
    }
}
