package X;

import android.graphics.PointF;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0SE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SE {
    public PointF A00;
    public boolean A01;
    public final List A02;

    public AnonymousClass0SE() {
        this.A02 = new ArrayList();
    }

    public AnonymousClass0SE(PointF pointF, List list, boolean z) {
        this.A00 = pointF;
        this.A01 = z;
        this.A02 = new ArrayList(list);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ShapeData{numCurves=");
        sb.append(this.A02.size());
        sb.append("closed=");
        sb.append(this.A01);
        sb.append('}');
        return sb.toString();
    }
}
