package X;

import android.content.Context;

/* renamed from: X.2TT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2TT {
    public final Context A00;

    public AnonymousClass2TT(Context context) {
        this.A00 = context.getApplicationContext();
    }

    public String A00(int i) {
        return this.A00.getResources().getString(i);
    }
}
