package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.35k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622135k extends AbstractC75693kG {
    public final View A00;
    public final WaImageView A01;

    public C622135k(View view) {
        super(view);
        View view2 = this.A0H;
        this.A01 = C12980iv.A0X(view2, R.id.icon);
        this.A00 = AnonymousClass028.A0D(view2, R.id.sel_marker);
    }

    @Override // X.AbstractC75693kG
    public void A08(View.OnClickListener onClickListener, AnonymousClass4OU r5, boolean z) {
        if (r5 instanceof AnonymousClass47H) {
            WaImageView waImageView = this.A01;
            C12970iu.A1H(waImageView, ((AnonymousClass47H) r5).A00, z);
            waImageView.setOnClickListener(onClickListener);
            View view = this.A00;
            Context context = view.getContext();
            int i = R.color.transparent;
            if (z) {
                i = R.color.picker_underline_color;
            }
            C12970iu.A18(context, view, i);
            return;
        }
        throw C12970iu.A0f("item should be AvocadoHeaderIconItem");
    }
}
