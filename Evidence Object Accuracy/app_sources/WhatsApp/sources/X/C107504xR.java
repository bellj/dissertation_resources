package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.4xR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107504xR implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(6);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final String A05;
    public final String A06;
    public final byte[] A07;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107504xR(Parcel parcel) {
        this.A03 = parcel.readInt();
        this.A06 = parcel.readString();
        this.A05 = parcel.readString();
        this.A04 = parcel.readInt();
        this.A02 = parcel.readInt();
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A07 = parcel.createByteArray();
    }

    public C107504xR(String str, String str2, byte[] bArr, int i, int i2, int i3, int i4, int i5) {
        this.A03 = i;
        this.A06 = str;
        this.A05 = str2;
        this.A04 = i2;
        this.A02 = i3;
        this.A01 = i4;
        this.A00 = i5;
        this.A07 = bArr;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C107504xR.class != obj.getClass()) {
                return false;
            }
            C107504xR r5 = (C107504xR) obj;
            if (!(this.A03 == r5.A03 && this.A06.equals(r5.A06) && this.A05.equals(r5.A05) && this.A04 == r5.A04 && this.A02 == r5.A02 && this.A01 == r5.A01 && this.A00 == r5.A00 && Arrays.equals(this.A07, r5.A07))) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((((((((((((C72453ed.A05(this.A03) + this.A06.hashCode()) * 31) + this.A05.hashCode()) * 31) + this.A04) * 31) + this.A02) * 31) + this.A01) * 31) + this.A00) * 31) + Arrays.hashCode(this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Picture: mimeType=");
        A0k.append(this.A06);
        A0k.append(", description=");
        return C12960it.A0d(this.A05, A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A03);
        parcel.writeString(this.A06);
        parcel.writeString(this.A05);
        parcel.writeInt(this.A04);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeByteArray(this.A07);
    }
}
