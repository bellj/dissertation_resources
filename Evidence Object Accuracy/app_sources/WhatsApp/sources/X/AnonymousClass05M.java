package X;

import android.os.Build;
import android.os.Trace;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.05M  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass05M {
    public static long A00;
    public static Method A01;

    public static void A00() {
        if (Build.VERSION.SDK_INT >= 18) {
            AnonymousClass0R3.A00();
        }
    }

    public static void A01() {
        if (Build.VERSION.SDK_INT >= 18) {
            AnonymousClass0R3.A01();
        }
    }

    public static boolean A02() {
        try {
            if (A01 == null) {
                return Trace.isEnabled();
            }
        } catch (NoClassDefFoundError | NoSuchMethodError unused) {
        }
        if (Build.VERSION.SDK_INT < 18) {
            return false;
        }
        try {
            Method method = A01;
            if (method == null) {
                A00 = Trace.class.getField("TRACE_TAG_APP").getLong(null);
                method = Trace.class.getMethod("isTagEnabled", Long.TYPE);
                A01 = method;
            }
            return ((Boolean) method.invoke(null, Long.valueOf(A00))).booleanValue();
        } catch (Exception e) {
            if (e instanceof InvocationTargetException) {
                Throwable cause = e.getCause();
                if (cause instanceof RuntimeException) {
                    throw cause;
                }
                throw new RuntimeException(cause);
            }
            StringBuilder sb = new StringBuilder("Unable to call ");
            sb.append("isTagEnabled");
            sb.append(" via reflection");
            Log.v("Trace", sb.toString(), e);
            return false;
        }
    }
}
