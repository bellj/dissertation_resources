package X;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import com.whatsapp.R;

/* renamed from: X.3U4  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3U4 implements AnonymousClass5TB {
    public final /* synthetic */ char A00 = '*';
    public final /* synthetic */ Context A01;

    public /* synthetic */ AnonymousClass3U4(Context context) {
        this.A01 = context;
    }

    @Override // X.AnonymousClass5TB
    public final SpannableStringBuilder AGq(String str) {
        char c = this.A00;
        Context context = this.A01;
        SpannableStringBuilder A0J = C12990iw.A0J(str);
        for (int i = 0; i < A0J.length(); i++) {
            if (A0J.charAt(i) == c) {
                int i2 = i + 1;
                A0J.setSpan(new RelativeSizeSpan(0.9f), i, i2, 33);
                A0J.setSpan(new C52272aX(context, AnonymousClass00T.A00(context, R.color.code_input_hint_color)), i, i2, 33);
            } else if (A0J.charAt(i) != 160) {
                A0J.setSpan(new C52272aX(context, AnonymousClass00T.A00(context, R.color.code_input_text)), i, i + 1, 33);
            }
        }
        return A0J;
    }
}
