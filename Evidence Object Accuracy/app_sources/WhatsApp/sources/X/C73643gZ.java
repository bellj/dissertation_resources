package X;

import android.content.Context;
import android.view.OrientationEventListener;

/* renamed from: X.3gZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73643gZ extends OrientationEventListener {
    public int A00;
    public final /* synthetic */ AnonymousClass27X A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C73643gZ(Context context, AnonymousClass27X r3) {
        super(context);
        this.A01 = r3;
        this.A00 = r3.A0U.getRotation();
    }

    @Override // android.view.OrientationEventListener
    public void enable() {
        super.enable();
        this.A00 = this.A01.A0U.getRotation();
    }

    @Override // android.view.OrientationEventListener
    public void onOrientationChanged(int i) {
        int i2;
        AnonymousClass27X r3 = this.A01;
        int rotation = r3.A0U.getRotation();
        if (!(rotation == -1 || rotation == (i2 = this.A00) || Math.abs(i2 - rotation) % 2 != 0)) {
            r3.surfaceChanged(r3.A0V, 0, 0, 0);
        }
        this.A00 = rotation;
    }
}
