package X;

import android.content.Context;
import com.whatsapp.notification.PopupNotification;

/* renamed from: X.4qQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103234qQ implements AbstractC009204q {
    public final /* synthetic */ PopupNotification A00;

    public C103234qQ(PopupNotification popupNotification) {
        this.A00 = popupNotification;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
