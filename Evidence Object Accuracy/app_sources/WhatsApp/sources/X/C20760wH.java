package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0wH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20760wH {
    public static final long A09 = TimeUnit.MINUTES.toMillis(15);
    public long A00;
    public boolean A01;
    public final C16590pI A02;
    public final C18810t5 A03;
    public final C19930uu A04;
    public final AbstractC14440lR A05;
    public final List A06 = new ArrayList();
    public final List A07 = new ArrayList();
    public final List A08 = new ArrayList();

    public C20760wH(C16590pI r2, C18810t5 r3, C19930uu r4, AbstractC14440lR r5) {
        this.A02 = r2;
        this.A04 = r4;
        this.A05 = r5;
        this.A03 = r3;
    }

    public final synchronized void A00() {
        String[] split;
        if (!this.A01) {
            List list = this.A08;
            list.clear();
            this.A07.clear();
            this.A06.clear();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.A02.A00.getResources().openRawResource(R.raw.domain_fronting_providers), AnonymousClass01V.A08));
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            bufferedReader.close();
                            break;
                        }
                        try {
                            split = TextUtils.split(readLine.replaceAll("\t", " ").replaceAll("^ +| +$|( )+", "$1"), " ");
                        } catch (IllegalArgumentException e) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("domain-fronting-providers/load/bad-line: ");
                            sb.append(readLine);
                            Log.e(sb.toString(), e);
                        }
                        if (split == null || split.length < 3) {
                            throw new IllegalArgumentException();
                            break;
                        }
                        list.add(new C91374Rm(this.A04, split));
                    } catch (Throwable th) {
                        try {
                            bufferedReader.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            } catch (UnsupportedEncodingException unused2) {
            }
        }
        this.A01 = true;
    }
}
