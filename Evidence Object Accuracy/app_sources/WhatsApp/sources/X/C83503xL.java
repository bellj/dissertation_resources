package X;

import android.view.animation.Animation;

/* renamed from: X.3xL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83503xL extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ Abstractanimation.Animation$AnimationListenerC28831Pe A00;
    public final /* synthetic */ C92254Vd A01;

    public C83503xL(Abstractanimation.Animation$AnimationListenerC28831Pe r1, C92254Vd r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        C92254Vd r2 = this.A01;
        r2.A02.ATC((float) r2.A01.getHeight());
        this.A00.onAnimationEnd(animation);
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        this.A00.onAnimationStart(animation);
    }
}
