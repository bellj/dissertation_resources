package X;

import java.util.List;

/* renamed from: X.1gY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34691gY extends AnonymousClass1JQ {
    public final List A00;

    public C34691gY(AnonymousClass1JR r10, String str, List list, long j) {
        super(C27791Jf.A03, r10, str, "regular", 7, j, false);
        this.A00 = list;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        AnonymousClass1G4 A0T = C34681gX.A01.A0T();
        List list = this.A00;
        A0T.A03();
        C34681gX r2 = (C34681gX) A0T.A00;
        AnonymousClass1K6 r1 = r2.A00;
        if (!((AnonymousClass1K7) r1).A00) {
            r1 = AbstractC27091Fz.A0G(r1);
            r2.A00 = r1;
        }
        AnonymousClass1G5.A01(list, r1);
        A01.A03();
        C27831Jk r22 = (C27831Jk) A01.A00;
        r22.A0G = (C34681gX) A0T.A02();
        r22.A00 |= 524288;
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("PrimaryFeatureMutation{");
        sb.append("featureFlags=");
        sb.append(this.A00);
        sb.append(", rowId='");
        sb.append(this.A07);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName='");
        sb.append(this.A06);
        sb.append(", version=");
        sb.append(this.A03);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
