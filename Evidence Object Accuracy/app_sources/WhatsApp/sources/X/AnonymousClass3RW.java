package X;

/* renamed from: X.3RW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3RW implements AbstractC009404s {
    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.equals(C53932fp.class)) {
            return new C53932fp();
        }
        if (cls.equals(C470028n.class)) {
            return new C470028n();
        }
        StringBuilder A0k = C12960it.A0k("Model class must be one of: [");
        A0k.append(C53932fp.class.getCanonicalName());
        A0k.append(", ");
        A0k.append(C470028n.class.getCanonicalName());
        throw C12970iu.A0f(C12960it.A0d("]", A0k));
    }
}
