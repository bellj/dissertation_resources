package X;

/* renamed from: X.30Y  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30Y extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;

    public AnonymousClass30Y() {
        super(2064, new AnonymousClass00E(1, 20, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A00);
        r3.Abe(1, this.A03);
        r3.Abe(3, this.A01);
        r3.Abe(2, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCameraTti {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraApi", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraTtiDuration", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraType", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "launchType", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
