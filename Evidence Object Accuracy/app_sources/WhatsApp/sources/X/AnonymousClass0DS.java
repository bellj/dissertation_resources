package X;

import android.os.Bundle;
import android.view.View;

/* renamed from: X.0DS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DS extends AnonymousClass04v {
    public final AnonymousClass0DV A00;

    public AnonymousClass0DS(AnonymousClass0DV r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        if (super.A03(view, i, bundle)) {
            return true;
        }
        this.A00.A08();
        return false;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        AnonymousClass02H layoutManager;
        super.A06(view, r4);
        AnonymousClass0DV r1 = this.A00;
        if (!r1.A08() && (layoutManager = r1.A01.getLayoutManager()) != null) {
            layoutManager.A0K(view, r4);
        }
    }
}
