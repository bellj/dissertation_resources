package X;

import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.viewpager.widget.ViewPager;
import com.facebook.msys.mci.DefaultCrypto;

/* renamed from: X.07W  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07W extends AnonymousClass04v {
    public final /* synthetic */ ViewPager A00;

    public AnonymousClass07W(ViewPager viewPager) {
        this.A00 = viewPager;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        AnonymousClass01A r0;
        super.A01(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
        ViewPager viewPager = this.A00;
        AnonymousClass01A r02 = viewPager.A0V;
        boolean z = true;
        if (r02 == null || r02.A01() <= 1) {
            z = false;
        }
        accessibilityEvent.setScrollable(z);
        if (accessibilityEvent.getEventType() == 4096 && (r0 = viewPager.A0V) != null) {
            accessibilityEvent.setItemCount(r0.A01());
            accessibilityEvent.setFromIndex(viewPager.A0A);
            accessibilityEvent.setToIndex(viewPager.A0A);
        }
    }

    @Override // X.AnonymousClass04v
    public boolean A03(View view, int i, Bundle bundle) {
        ViewPager viewPager;
        int i2;
        if (!super.A03(view, i, bundle)) {
            if (i != 4096) {
                if (i == 8192) {
                    viewPager = this.A00;
                    if (viewPager.canScrollHorizontally(-1)) {
                        i2 = viewPager.A0A - 1;
                        viewPager.setCurrentItem(i2);
                    }
                }
                return false;
            }
            viewPager = this.A00;
            if (viewPager.canScrollHorizontally(1)) {
                i2 = viewPager.A0A + 1;
                viewPager.setCurrentItem(i2);
            }
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        String name = ViewPager.class.getName();
        AccessibilityNodeInfo accessibilityNodeInfo = r6.A02;
        accessibilityNodeInfo.setClassName(name);
        ViewPager viewPager = this.A00;
        AnonymousClass01A r0 = viewPager.A0V;
        boolean z = true;
        if (r0 == null || r0.A01() <= 1) {
            z = false;
        }
        accessibilityNodeInfo.setScrollable(z);
        if (viewPager.canScrollHorizontally(1)) {
            accessibilityNodeInfo.addAction(4096);
        }
        if (viewPager.canScrollHorizontally(-1)) {
            accessibilityNodeInfo.addAction(DefaultCrypto.BUFFER_SIZE);
        }
    }
}
