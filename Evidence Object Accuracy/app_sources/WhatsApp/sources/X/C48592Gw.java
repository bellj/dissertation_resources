package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.2Gw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48592Gw {
    public final C21700xq A00;

    public C48592Gw(C21700xq r1) {
        this.A00 = r1;
    }

    public static List A00(AbstractC15340mz r7, AbstractC15340mz r8, Set set) {
        if (r7 == r8) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        if (!set.contains("status") && r7.A0C != r8.A0C) {
            arrayList.add("status");
        }
        if (!set.contains("sendRetryReceipt") && r7.A0u != r8.A0u) {
            arrayList.add("sendRetryReceipt");
        }
        if (!set.contains("retryCount") && r7.A0B != r8.A0B) {
            arrayList.add("retryCount");
        }
        if (!set.contains("encoding") && r7.A02 != r8.A02) {
            arrayList.add("encoding");
        }
        if (!set.contains("timestamp") && r7.A0I != r8.A0I) {
            arrayList.add("timestamp");
        }
        if (!set.contains("receivedIncomingTimestamp") && r7.A0G != r8.A0G) {
            arrayList.add("receivedIncomingTimestamp");
        }
        if (!set.contains("origin") && r7.A08 != r8.A08) {
            arrayList.add("origin");
        }
        if (!set.contains("forwardingScore") && r7.A05 != r8.A05) {
            arrayList.add("forwardingScore");
        }
        if (!set.contains("media_wa_type") && r7.A0y != r8.A0y) {
            arrayList.add("media_wa_type");
        }
        if (!set.contains("broadcast") && r7.A0r != r8.A0r) {
            arrayList.add("broadcast");
        }
        if (!set.contains("recipient_count") && r7.A0A != r8.A0A) {
            StringBuilder sb = new StringBuilder();
            sb.append("recipient_count");
            arrayList.add(sb.toString());
        }
        if (!set.contains("server_receipt_timestamp") && r7.A0H != r8.A0H) {
            arrayList.add("server_receipt_timestamp");
        }
        if (!set.contains("row_id") && r7.A11 != r8.A11) {
            arrayList.add("row_id");
        }
        if (!set.contains("sort_id") && r7.A12 != r8.A12) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("sort_id");
            arrayList.add(sb2.toString());
        }
        if (!set.contains("starred") && r7.A0v != r8.A0v) {
            arrayList.add("starred");
        }
        if (!set.contains("quoted_row_id") && r7.A0F != r8.A0F) {
            arrayList.add("quoted_row_id");
        }
        if (!set.contains("messageDecorator") && !C29941Vi.A00(r7.A0F(), r8.A0F())) {
            arrayList.add("messageDecorator");
        }
        if (!set.contains("origination_flags") && r7.A07() != r8.A07()) {
            arrayList.add("origination_flags");
        }
        if (!set.contains("verified_level") && r7.A0E != r8.A0E) {
            arrayList.add("verified_level");
        }
        if (!set.contains("verifiedNameInSync") && r7.A0w != r8.A0w) {
            arrayList.add("verified_level");
        }
        if (!set.contains("privacy_mode") && r7.A0U != r8.A0U) {
            arrayList.add("verified_level");
        }
        if (!set.contains("chat_active") && r7.A0s != r8.A0s) {
            arrayList.add("chat_active");
        }
        if (!set.contains("edited_version") && r7.A01 != r8.A01) {
            arrayList.add("edited_version");
        }
        set.contains("ctwaConversionDelaySeconds");
        if (!set.contains("deleted") && r7.A13 != r8.A13) {
            arrayList.add("deleted");
        }
        if (!set.contains("storageType") && r7.A08() != r8.A08()) {
            arrayList.add("storageType");
        }
        if (!set.contains("expirationDuration") && r7.A04 != r8.A04) {
            arrayList.add("expirationDuration");
        }
        if (!set.contains("key") && !r7.A0z.equals(r8.A0z)) {
            arrayList.add("key");
        }
        if (!set.contains("senderJid") && !C29941Vi.A00(r7.A0B(), r8.A0B())) {
            arrayList.add("senderJid");
        }
        if (!set.contains("entVerifiedNameOnPrivacyConflict") && r7.A0h != r8.A0h) {
            arrayList.add("entVerifiedNameOnPrivacyConflict");
        }
        if ((!(r7 instanceof AnonymousClass1XB) || (r7 instanceof C30461Xm)) && !set.contains("participantList") && !C37871n9.A02(r7.A0R(), r8.A0R())) {
            r7.A0R();
            r8.A0R();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("participantList");
            sb3.append("");
            arrayList.add(sb3.toString());
        }
        if (!set.contains("participant_hash") && !AnonymousClass1US.A0D(r7.A0l, r8.A0l)) {
            arrayList.add("participant_hash");
        }
        if (!set.contains("broadcastParticipantEphemeralSettings") && !C29941Vi.A00(r7.A0q, r8.A0q)) {
            arrayList.add("broadcastParticipantEphemeralSettings");
        }
        if (!set.contains("data") && !AnonymousClass1US.A0D(r7.A0I(), r8.A0I())) {
            arrayList.add("data");
        }
        if (!set.contains("dataBytes") && !Arrays.equals(r7.A13(), r8.A13())) {
            arrayList.add("dataBytes");
        }
        if (!set.contains("from_name") && !AnonymousClass1US.A0D(r7.A0k, r8.A0k)) {
            arrayList.add("from_name");
        }
        if (!set.contains("displayName") && !AnonymousClass1US.A0D(r7.A0g, r8.A0g)) {
            arrayList.add("displayName");
        }
        if (!set.contains("senderPn") && !AnonymousClass1US.A0D(r7.A0n, r8.A0n)) {
            arrayList.add("senderPn");
        }
        if (!set.contains("messageSecretEnabled") && r7.A10() != r8.A10()) {
            arrayList.add("messageSecretEnabled");
        }
        if (!set.contains("web") && r7.A0O != r8.A0O) {
            arrayList.add("web");
        }
        if (!set.contains("mentionedJids") && !C37871n9.A02(r7.A0o, r8.A0o)) {
            arrayList.add("mentionedJids");
        }
        if (!set.contains("offline") && !C29941Vi.A00(r7.A0W, r8.A0W)) {
            arrayList.add("offline");
        }
        if (!set.contains("quotedMessage") && !C29941Vi.A00(r7.A0E(), r8.A0E())) {
            arrayList.add("quotedMessage");
        }
        if (!set.contains("externalAdContentInfo") && !C29941Vi.A00(r7.A0N, r8.A0N)) {
            arrayList.add("externalAdContentInfo");
        }
        if (!set.contains("mmsThumbnailMetadata") && !C29941Vi.A00(r7.A0T, r8.A0T)) {
            arrayList.add("mmsThumbnailMetadata");
        }
        if (!set.contains("verified_name") && !C29941Vi.A00(r7.A0c, r8.A0c)) {
            arrayList.add("verified_name");
        }
        if (!set.contains("trigger_csat_expiration_ts") && !C29941Vi.A00(r7.A0Z, r8.A0Z)) {
            arrayList.add("trigger_csat_expiration_ts");
        }
        if (!set.contains("triggred_block_expiration_ts") && !C29941Vi.A00(r7.A0b, r8.A0b)) {
            arrayList.add("triggred_block_expiration_ts");
        }
        if (!set.contains("triggred_block_cooldown") && !C29941Vi.A00(r7.A0a, r8.A0a)) {
            arrayList.add("triggred_block_cooldown");
        }
        if (!set.contains("ctwaConversionTupleSource") && !AnonymousClass1US.A0D(r7.A0e, r8.A0e)) {
            arrayList.add("ctwaConversionTupleSource");
        }
        if (!set.contains("ctwaConversionTupleData") && !AnonymousClass1US.A0D(r7.A0d, r8.A0d)) {
            arrayList.add("ctwaConversionTupleData");
        }
        if (!set.contains("customerLoggingData") && !C29941Vi.A00(r7.A0J, r8.A0J)) {
            arrayList.add("customerLoggingData");
        }
        if (!set.contains("entryPointConversionSource") && !AnonymousClass1US.A0D(r7.A0j, r8.A0j)) {
            arrayList.add("entryPointConversionSource");
        }
        if (!set.contains("entryPointConversionApp") && !AnonymousClass1US.A0D(r7.A0i, r8.A0i)) {
            arrayList.add("entryPointConversionApp");
        }
        if (!set.contains("entryPointConversionDelaySeconds") && r7.A03 != r8.A03) {
            arrayList.add("entryPointConversionDelaySeconds");
        }
        if (!set.contains("thumbnail") && !C29941Vi.A00(r7.A0G(), r8.A0G())) {
            arrayList.add("thumbnail");
        }
        if (!set.contains("payment_transaction_id") && !AnonymousClass1US.A0D(r7.A0m, r8.A0m)) {
            arrayList.add("payment_transaction_id");
        }
        if (!set.contains("paymentTransactionInfo") && !C29941Vi.A00(r7.A0L, r8.A0L)) {
            arrayList.add("paymentTransactionInfo");
        }
        if (!set.contains("backFillMessageKey") && !C29941Vi.A00(r7.A0R, r8.A0R)) {
            arrayList.add("backFillMessageKey");
        }
        if (!set.contains("ephemeralSettingTimestamp") && !C29941Vi.A00(r7.A0X, r8.A0X)) {
            arrayList.add("ephemeralSettingTimestamp");
        }
        if (!set.contains("expirationExpireTimestamp") && !C29941Vi.A00(r7.A0Y, r8.A0Y)) {
            arrayList.add("expirationExpireTimestamp");
        }
        if (!set.contains("disappearingMessagesInitiator") && !C29941Vi.A00(Integer.valueOf(r7.A00), Integer.valueOf(r8.A00))) {
            arrayList.add("disappearingMessagesInitiator");
        }
        if (!set.contains("messageAddOnFlag") && r7.A07 != r8.A07) {
            arrayList.add("messageAddOnFlag");
        }
        if (!set.contains("messageReactions") && !C29941Vi.A00(r7.A0V, r8.A0V)) {
            arrayList.add("messageReactions");
        }
        if (!set.contains("statusDistributionInfo") && !C29941Vi.A00(r7.A0K, r8.A0K)) {
            arrayList.add("statusDistributionInfo");
        }
        if (!set.contains("keepInChat") && !C29941Vi.A00(Integer.valueOf(r7.A06()), Integer.valueOf(r8.A06()))) {
            arrayList.add("keepInChat");
        }
        return arrayList;
    }

    public static List A01(AnonymousClass1Iv r5, AnonymousClass1Iv r6, Set set) {
        AnonymousClass1IS r1;
        AnonymousClass1IS r0;
        AbstractC14640lm r12;
        AbstractC14640lm r02;
        AbstractC14640lm r13;
        AbstractC14640lm r03;
        ArrayList arrayList = new ArrayList(A00(r5, r6, set));
        if (!set.contains("parentMessageKey") && !C29941Vi.A00(r5.A14(), r6.A14())) {
            C40711sC r04 = r5.A02;
            if (r04 == null) {
                r13 = null;
            } else {
                r13 = r04.A00;
            }
            C40711sC r05 = r6.A02;
            if (r05 == null) {
                r03 = null;
            } else {
                r03 = r05.A00;
            }
            if (!C29941Vi.A00(r13, r03)) {
                arrayList.add("parentMessageKey");
            }
        }
        if (!set.contains("broadcastParentMessageKey")) {
            C40711sC r06 = r5.A01;
            if (r06 == null) {
                r1 = null;
            } else {
                r1 = r06.A01;
            }
            C40711sC r07 = r6.A01;
            if (r07 == null) {
                r0 = null;
            } else {
                r0 = r07.A01;
            }
            if (!C29941Vi.A00(r1, r0)) {
                C40711sC r08 = r5.A01;
                if (r08 == null) {
                    r12 = null;
                } else {
                    r12 = r08.A00;
                }
                C40711sC r09 = r6.A01;
                if (r09 == null) {
                    r02 = null;
                } else {
                    r02 = r09.A00;
                }
                if (!C29941Vi.A00(r12, r02)) {
                    arrayList.add("broadcastParentMessageKey");
                }
            }
        }
        if (!set.contains("parentMessageRowId") && !C29941Vi.A00(Long.valueOf(r5.A00), Long.valueOf(r6.A00))) {
            arrayList.add("parentMessageRowId");
        }
        return arrayList;
    }

    public static List A02(AbstractC30391Xf r12, AbstractC30391Xf r13, Set set) {
        if (r12 == r13) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A00(r12, r13, set));
        if (!set.contains("isLegacy") && r12.A00 != r13.A00) {
            arrayList.add("isLegacy");
        }
        if (!set.contains("isVideoCall") && !AnonymousClass1US.A0D(r12.A0K(), r13.A0K())) {
            arrayList.add("isVideoCall");
        }
        if (!set.contains("callLogs")) {
            List<AnonymousClass1YT> A15 = r12.A15();
            List A152 = r13.A15();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList(A152);
            for (AnonymousClass1YT r3 : A15) {
                Iterator it = arrayList4.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        arrayList3.add(r3);
                        break;
                    }
                    AnonymousClass1YT r2 = (AnonymousClass1YT) it.next();
                    ArrayList arrayList5 = new ArrayList();
                    if (!set.contains("callLog.key") && !C29941Vi.A00(r3.A03(), r2.A03())) {
                        arrayList5.add("callLog.key");
                    }
                    if (!set.contains("callLog.bytesTransferred") && !C29941Vi.A00(Long.valueOf(r3.A02), Long.valueOf(r2.A02))) {
                        arrayList5.add("callLog.bytesTransferred");
                    }
                    if (!set.contains("callLog.callCreatorDeviceJid") && !C29941Vi.A00(r3.A0A, r2.A0A)) {
                        arrayList5.add("callLog.callCreatorDeviceJid");
                    }
                    if (!set.contains("callLog.callRandomId") && !C29941Vi.A00(r3.A07, r2.A07)) {
                        arrayList5.add("callLog.callRandomId");
                    }
                    if (!set.contains("callLog.callResult") && !C29941Vi.A00(Integer.valueOf(r3.A00), Integer.valueOf(r2.A00))) {
                        arrayList5.add("callLog.callResult");
                    }
                    if (!set.contains("callLog.duration") && !C29941Vi.A00(Integer.valueOf(r3.A01), Integer.valueOf(r2.A01))) {
                        arrayList5.add("callLog.duration");
                    }
                    if (!set.contains("callLog.groupJid") && !C29941Vi.A00(r3.A04, r2.A04)) {
                        arrayList5.add("callLog.groupJid");
                    }
                    if (!set.contains("callLog.transactionId") && !C29941Vi.A00(Integer.valueOf(r3.A0B.A00), Integer.valueOf(r2.A0B.A00))) {
                        arrayList5.add("callLog.transactionId");
                    }
                    if (!set.contains("callLog.remoteJid") && !C29941Vi.A00(r3.A0B.A01, r2.A0B.A01)) {
                        arrayList5.add("callLog.remoteJid");
                    }
                    if (arrayList5.size() == 0) {
                        arrayList4.remove(r2);
                        break;
                    }
                }
            }
            if (!C37871n9.A02(arrayList3, arrayList4)) {
                arrayList2.add("callLogs");
            }
            arrayList.addAll(arrayList2);
        }
        return arrayList;
    }

    public static List A03(C16440p1 r4, C16440p1 r5, Set set) {
        if (r4 == r5) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A05(r4, r5, set));
        if (!set.contains("pageCount") && r4.A00 != r5.A00) {
            arrayList.add("pageCount");
        }
        if (!set.contains("mediaText") && !AnonymousClass1US.A0D(r4.A01, r5.A01)) {
            arrayList.add("mediaText");
        }
        return arrayList;
    }

    public static List A04(AnonymousClass1XP r6, AnonymousClass1XP r7, Set set) {
        if (r6 == r7) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A00(r6, r7, set));
        if (!set.contains("latitude") && Double.compare(r6.A00, r7.A00) != 0) {
            arrayList.add("latitude");
        }
        if (!set.contains("longitude") && Double.compare(r6.A01, r7.A01) != 0) {
            arrayList.add("longitude");
        }
        if (!set.contains("downloadStatus") && r6.A02 != r7.A02) {
            arrayList.add("downloadStatus");
        }
        return arrayList;
    }

    public static List A05(AbstractC16130oV r7, AbstractC16130oV r8, Set set) {
        if (r7 == r8) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A00(r7, r8, set));
        if (!set.contains("media_duration_seconds") && r7.A00 != r8.A00) {
            arrayList.add("media_duration_seconds");
        }
        if (!set.contains("media_size") && r7.A01 != r8.A01) {
            arrayList.add("media_size");
        }
        if (!set.contains("mediaDataV2") && !C29941Vi.A00(r7.A02, r8.A02)) {
            arrayList.add("mediaDataV2");
        }
        if (!set.contains("media_caption") && !AnonymousClass1US.A0D(r7.A15(), r8.A15())) {
            arrayList.add("media_caption");
        }
        if (!set.contains("media_enc_hash") && !AnonymousClass1US.A0D(r7.A04, r8.A04)) {
            arrayList.add("media_enc_hash");
        }
        if (!set.contains("media_hash") && !AnonymousClass1US.A0D(r7.A05, r8.A05)) {
            arrayList.add("media_hash");
        }
        if (!set.contains("media_mime_type") && !AnonymousClass1US.A0D(r7.A06, r8.A06)) {
            arrayList.add("media_mime_type");
        }
        if (!set.contains("media_name") && !AnonymousClass1US.A0D(r7.A16(), r8.A16())) {
            arrayList.add("media_name");
        }
        if (!set.contains("media_url") && !AnonymousClass1US.A0D(r7.A08, r8.A08)) {
            arrayList.add("media_url");
        }
        if (!set.contains("original_file_hash") && !AnonymousClass1US.A0D(r7.A0A, r8.A0A)) {
            arrayList.add("original_file_hash");
        }
        if (!set.contains("multicast_id") && !AnonymousClass1US.A0D(r7.A09, r8.A09)) {
            arrayList.add("multicast_id");
        }
        if (!set.contains("sidecar") && !C29941Vi.A00(r7.A14(), r8.A14())) {
            arrayList.add("sidecar");
        }
        return arrayList;
    }

    public static List A06(AbstractC30271Wt r4, AbstractC30271Wt r5, Set set) {
        if (r4 == r5) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A00(r4, r5, set));
        if (!set.contains("recipient") && !C29941Vi.A00(r4.A00, r5.A00)) {
            arrayList.add("recipient");
        }
        if (!set.contains("receivedAck") && !C29941Vi.A00(Boolean.valueOf(r4.A01), Boolean.valueOf(r5.A01))) {
            arrayList.add("receivedAck");
        }
        return arrayList;
    }

    public static List A07(C30331Wz r7, C30331Wz r8, Set set) {
        if (r7 == r8) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A00(r7, r8, set));
        if (!set.contains("revokedMessageId") && !AnonymousClass1US.A0D(r7.A01, r8.A01)) {
            arrayList.add("revokedMessageId");
        }
        if (!set.contains("revokeTimestampMs") && r7.A00 != r8.A00) {
            arrayList.add("revokeTimestampMs");
        }
        return arrayList;
    }

    public static List A08(AnonymousClass1XO r4, AnonymousClass1XO r5, Set set) {
        if (r4 == r5) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A04(r4, r5, set));
        if (!set.contains("placeName") && !AnonymousClass1US.A0D(r4.A01, r5.A01)) {
            arrayList.add("placeName");
        }
        if (!set.contains("address") && !AnonymousClass1US.A0D(r4.A00, r5.A00)) {
            arrayList.add("address");
        }
        if (!set.contains("url") && !AnonymousClass1US.A0D(r4.A02, r5.A02)) {
            arrayList.add("url");
        }
        return arrayList;
    }

    public static List A09(AnonymousClass1XB r4, AnonymousClass1XB r5, Set set) {
        if (r4 == r5) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A00(r4, r5, set));
        if (!set.contains("action") && r4.A00 != r5.A00) {
            arrayList.add("action");
        }
        return arrayList;
    }

    public static List A0A(C30461Xm r4, C30461Xm r5, Set set) {
        if (r4 == r5) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A09(r4, r5, set));
        if (!set.contains("isCurrentUserJoined") && r4.A00 != r5.A00) {
            arrayList.add("isCurrentUserJoined");
        }
        if (!set.contains("groupParticipants") && !C29941Vi.A00(r4.A02, r5.A02)) {
            arrayList.add("groupParticipants");
        }
        return arrayList;
    }

    public static List A0B(C30541Xv r4, C30541Xv r5, Set set) {
        if (r4 == r5) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(A09(r4, r5, set));
        if (!set.contains("senderUserJid") && !C29941Vi.A00(r4.A01, r5.A01)) {
            arrayList.add("senderUserJid");
        }
        if (!set.contains("receiverUserJid") && !C29941Vi.A00(r4.A00, r5.A00)) {
            arrayList.add("receiverUserJid");
        }
        if (!set.contains("amountWithSymbol") && !AnonymousClass1US.A0D(r4.A03, r5.A03)) {
            arrayList.add("amountWithSymbol");
        }
        if (!set.contains("referenceMsgKey") && !C29941Vi.A00(r4.A02, r5.A02)) {
            arrayList.add("referenceMsgKey");
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a0, code lost:
        if (r0 == 0) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00bc, code lost:
        if (java.util.Arrays.equals(r1, r0) == false) goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00be, code lost:
        r0 = new java.lang.StringBuilder();
        r0.append("thumbData");
        r0.append("");
        r3.add(r0.toString());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A0C(X.C28861Ph r4, X.C28861Ph r5, java.util.Set r6) {
        /*
            if (r4 != r5) goto L_0x0007
            java.util.List r0 = java.util.Collections.emptyList()
            return r0
        L_0x0007:
            java.util.List r0 = A00(r4, r5, r6)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r0)
            java.lang.String r2 = "previewType"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x0021
            int r1 = r4.A01
            int r0 = r5.A01
            if (r1 == r0) goto L_0x0021
            r3.add(r2)
        L_0x0021:
            java.lang.String r2 = "inviteLinkGroupType"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x0032
            int r1 = r4.A00
            int r0 = r5.A00
            if (r1 == r0) goto L_0x0032
            r3.add(r2)
        L_0x0032:
            java.lang.String r2 = "description"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x0047
            java.lang.String r1 = r4.A03
            java.lang.String r0 = r5.A03
            boolean r0 = X.AnonymousClass1US.A0D(r1, r0)
            if (r0 != 0) goto L_0x0047
            r3.add(r2)
        L_0x0047:
            java.lang.String r2 = "pageTitle"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x005c
            java.lang.String r1 = r4.A05
            java.lang.String r0 = r5.A05
            boolean r0 = X.AnonymousClass1US.A0D(r1, r0)
            if (r0 != 0) goto L_0x005c
            r3.add(r2)
        L_0x005c:
            java.lang.String r2 = "url"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x0072
            java.lang.String r1 = r4.A06
            java.lang.String r0 = r5.A06
            boolean r0 = X.AnonymousClass1US.A0D(r1, r0)
            if (r0 != 0) goto L_0x0072
            r3.add(r2)
        L_0x0072:
            java.lang.String r2 = "textData"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x0088
            com.whatsapp.TextData r1 = r4.A02
            com.whatsapp.TextData r0 = r5.A02
            boolean r0 = X.C29941Vi.A00(r1, r0)
            if (r0 != 0) goto L_0x0088
            r3.add(r2)
        L_0x0088:
            java.lang.String r2 = "thumbData"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x00a2
            byte[] r1 = r4.A17()
            byte[] r0 = r5.A17()
            if (r1 == r0) goto L_0x00a2
            if (r1 == 0) goto L_0x00d3
            if (r0 != 0) goto L_0x00b8
            int r0 = r1.length
        L_0x00a0:
            if (r0 != 0) goto L_0x00be
        L_0x00a2:
            java.lang.String r2 = "mimeType"
            boolean r0 = r6.contains(r2)
            if (r0 != 0) goto L_0x00b7
            java.lang.String r1 = r4.A04
            java.lang.String r0 = r5.A04
            boolean r0 = X.AnonymousClass1US.A0D(r1, r0)
            if (r0 != 0) goto L_0x00b7
            r3.add(r2)
        L_0x00b7:
            return r3
        L_0x00b8:
            boolean r0 = java.util.Arrays.equals(r1, r0)
            if (r0 != 0) goto L_0x00a2
        L_0x00be:
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r2)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r3.add(r0)
            goto L_0x00a2
        L_0x00d3:
            int r0 = r0.length
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C48592Gw.A0C(X.1Ph, X.1Ph, java.util.Set):java.util.List");
    }

    public static final void A0D(String str) {
        if (str == null) {
            throw new IllegalStateException();
        }
        throw new IllegalStateException(str);
    }

    public void A0E(Object obj, Object obj2, String str) {
        if (obj == null) {
            if (obj2 == null) {
                return;
            }
        } else if (obj.equals(obj2)) {
            return;
        }
        A0G(str);
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:376:0x064c */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.util.List, java.lang.Iterable] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v4, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v5, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v6, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v7, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v8, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v9, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v10, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v11, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v12, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v13, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v14, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v15, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v16, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r1v25, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v160, types: [boolean] */
    /* JADX WARN: Type inference failed for: r4v17, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v18, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v19, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v20, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v21, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v22, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v23, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v24, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v25, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v26, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v27, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v28, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v29, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v30, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v31, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v32, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v33, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v34, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v35, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r1v61, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v272, types: [boolean] */
    /* JADX WARN: Type inference failed for: r4v36, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v37, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v38, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v39, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v40, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v41, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v42, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v43, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v44, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v45, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v46, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v47, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v48, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v49, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v50, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v51, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v52, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v53, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v54, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v55, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v56, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r1v93, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v380, types: [boolean] */
    /* JADX WARN: Type inference failed for: r4v57, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v58, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v59, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v60, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v61, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v62, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v63, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v64, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v65, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v66, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v67, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v68, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v69 */
    /* JADX WARN: Type inference failed for: r4v70, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r1v128, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v488, types: [boolean] */
    /* JADX WARN: Type inference failed for: r4v71, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v72, types: [java.util.AbstractCollection] */
    /* JADX WARN: Type inference failed for: r4v73, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v74, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v75, types: [java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v76, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v77, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v78, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v79, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v80, types: [java.util.AbstractCollection] */
    /* JADX WARN: Type inference failed for: r4v81 */
    /* JADX WARN: Type inference failed for: r4v82, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v83, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v84, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v85, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v86 */
    /* JADX WARN: Type inference failed for: r4v87 */
    /* JADX WARN: Type inference failed for: r4v88 */
    /* JADX WARN: Type inference failed for: r4v89 */
    /* JADX WARN: Type inference failed for: r4v90 */
    /* JADX WARN: Type inference failed for: r4v91 */
    /* JADX WARN: Type inference failed for: r4v92 */
    /* JADX WARN: Type inference failed for: r4v93 */
    /* JADX WARN: Type inference failed for: r4v94 */
    /* JADX WARN: Type inference failed for: r4v95 */
    /* JADX WARN: Type inference failed for: r4v96 */
    /* JADX WARN: Type inference failed for: r4v97 */
    /* JADX WARN: Type inference failed for: r4v98 */
    /* JADX WARN: Type inference failed for: r4v99 */
    /* JADX WARN: Type inference failed for: r4v100 */
    /* JADX WARN: Type inference failed for: r4v101 */
    /* JADX WARN: Type inference failed for: r4v102 */
    /* JADX WARN: Type inference failed for: r4v103 */
    /* JADX WARN: Type inference failed for: r4v104 */
    /* JADX WARN: Type inference failed for: r4v105 */
    /* JADX WARN: Type inference failed for: r4v106 */
    /* JADX WARN: Type inference failed for: r4v107 */
    /* JADX WARN: Type inference failed for: r4v108 */
    /* JADX WARN: Type inference failed for: r4v109 */
    /* JADX WARN: Type inference failed for: r4v110 */
    /* JADX WARN: Type inference failed for: r4v111 */
    /* JADX WARN: Type inference failed for: r4v112 */
    /* JADX WARN: Type inference failed for: r4v113 */
    /* JADX WARN: Type inference failed for: r4v114 */
    /* JADX WARN: Type inference failed for: r4v115 */
    /* JADX WARN: Type inference failed for: r4v116 */
    /* JADX WARN: Type inference failed for: r4v117 */
    /* JADX WARN: Type inference failed for: r4v118 */
    /* JADX WARN: Type inference failed for: r4v119 */
    /* JADX WARN: Type inference failed for: r4v120 */
    /* JADX WARN: Type inference failed for: r4v121 */
    /* JADX WARN: Type inference failed for: r4v122 */
    /* JADX WARN: Type inference failed for: r4v123 */
    /* JADX WARN: Type inference failed for: r4v124 */
    /* JADX WARN: Type inference failed for: r4v125 */
    /* JADX WARN: Type inference failed for: r4v126 */
    /* JADX WARN: Type inference failed for: r4v127 */
    /* JADX WARN: Type inference failed for: r4v128 */
    /* JADX WARN: Type inference failed for: r4v129 */
    /* JADX WARN: Type inference failed for: r4v130 */
    /* JADX WARN: Type inference failed for: r4v131 */
    /* JADX WARN: Type inference failed for: r4v132 */
    /* JADX WARN: Type inference failed for: r4v133 */
    /* JADX WARN: Type inference failed for: r4v134 */
    /* JADX WARN: Type inference failed for: r4v135 */
    /* JADX WARN: Type inference failed for: r4v136 */
    /* JADX WARN: Type inference failed for: r4v137 */
    /* JADX WARN: Type inference failed for: r4v138 */
    /* JADX WARN: Type inference failed for: r4v139 */
    /* JADX WARN: Type inference failed for: r4v140 */
    /* JADX WARN: Type inference failed for: r4v141 */
    /* JADX WARN: Type inference failed for: r4v142 */
    /* JADX WARN: Type inference failed for: r4v143 */
    /* JADX WARN: Type inference failed for: r4v144 */
    /* JADX WARN: Type inference failed for: r4v145 */
    /* JADX WARN: Type inference failed for: r4v146 */
    /* JADX WARN: Type inference failed for: r4v147 */
    /* JADX WARN: Type inference failed for: r4v148 */
    /* JADX WARN: Type inference failed for: r4v149 */
    /* JADX WARNING: Code restructure failed: missing block: B:376:0x064c, code lost:
        if (r1 != r0) goto L_0x0144;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0142, code lost:
        if (r0 != false) goto L_0x000e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:1171:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x04bc  */
    /* JADX WARNING: Unknown variable types count: 5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(java.lang.Object r8, java.lang.Object r9, java.lang.String r10, java.util.Set r11) {
        /*
        // Method dump skipped, instructions count: 5292
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C48592Gw.A0F(java.lang.Object, java.lang.Object, java.lang.String, java.util.Set):void");
    }

    public final void A0G(String str) {
        if (str == null) {
            str = "";
        }
        A0D(str);
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public void A0H(String str, double d, double d2) {
        if (Double.compare(d, d2) != 0 && Math.abs(d - d2) > 1.0E-9d) {
            A0G(str);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
    }

    public void A0I(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str2)) {
            A0K(str, TextUtils.isEmpty(str3));
        } else {
            A0E(str2, str3, str);
        }
    }

    public void A0J(String str, List list, List list2, Set set) {
        StringBuilder sb;
        String str2;
        if (list != list2) {
            if (list == null) {
                sb = new StringBuilder();
                sb.append(str);
                str2 = "expected list is null";
            } else if (list2 == null) {
                sb = new StringBuilder();
                sb.append(str);
                str2 = "actual list is null";
            } else if (list.size() != list2.size()) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append("list sizes are different");
                A0D(sb2.toString());
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            } else {
                for (int i = 0; i < list.size(); i++) {
                    A0F(list.get(i), list2.get(i), str, set);
                }
                return;
            }
            sb.append(str2);
            A0D(sb.toString());
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
    }

    public void A0K(String str, boolean z) {
        if (!z) {
            A0D(str);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
    }

    public void A0L(String str, byte[] bArr, byte[] bArr2) {
        if (bArr != bArr2 && !Arrays.equals(bArr, bArr2)) {
            A0G(str);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
    }
}
