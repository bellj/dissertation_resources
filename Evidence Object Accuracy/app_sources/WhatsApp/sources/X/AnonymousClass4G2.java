package X;

/* renamed from: X.4G2  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4G2 {
    public static final AnonymousClass5RD A00;

    static {
        C93264Zt r3 = C93264Zt.A00;
        AnonymousClass5RD r2 = r3.cache;
        if (r2 == null) {
            r2 = new AnonymousClass52L();
            if (!AnonymousClass0KN.A00(r3, null, r2, C93264Zt.A01)) {
                r2 = r3.cache;
            }
        }
        A00 = r2;
    }
}
