package X;

import android.view.View;

/* renamed from: X.2xP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xP extends AbstractC104124rr {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnonymousClass0QQ A01;
    public final /* synthetic */ AnonymousClass3DA A02;
    public final /* synthetic */ C55262i6 A03;

    public AnonymousClass2xP(View view, AnonymousClass0QQ r2, AnonymousClass3DA r3, C55262i6 r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMC(View view) {
        this.A01.A09(null);
        View view2 = this.A00;
        view2.setAlpha(1.0f);
        view2.setTranslationX(0.0f);
        view2.setTranslationY(0.0f);
        C55262i6 r3 = this.A03;
        AnonymousClass3DA r2 = this.A02;
        r3.A03(r2.A04);
        C12980iv.A1K(r3, r2.A04, r3.A02);
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMD(View view) {
    }
}
