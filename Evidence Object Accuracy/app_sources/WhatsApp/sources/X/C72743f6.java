package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;

/* renamed from: X.3f6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72743f6 extends AnimatorListenerAdapter {
    public final /* synthetic */ ValueAnimator A00;
    public final /* synthetic */ AnonymousClass23G A01;

    public C72743f6(ValueAnimator valueAnimator, AnonymousClass23G r2) {
        this.A01 = r2;
        this.A00 = valueAnimator;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.A01.setAlpha(1.0f);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A00.start();
    }
}
