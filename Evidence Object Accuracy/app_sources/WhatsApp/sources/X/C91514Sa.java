package X;

import android.view.View;

/* renamed from: X.4Sa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91514Sa {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C48842Hz A01;
    public final /* synthetic */ C90944Pv A02;
    public final /* synthetic */ AbstractC115945Tn A03;
    public final /* synthetic */ String A04;

    public C91514Sa(View view, C48842Hz r2, C90944Pv r3, AbstractC115945Tn r4, String str) {
        this.A01 = r2;
        this.A04 = str;
        this.A00 = view;
        this.A02 = r3;
        this.A03 = r4;
    }
}
