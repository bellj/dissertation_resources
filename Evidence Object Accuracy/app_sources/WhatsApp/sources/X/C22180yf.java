package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: X.0yf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22180yf {
    public static final int A04 = C32651cV.A05;
    public final C15570nT A00;
    public final C14850m9 A01;
    public final C22710zW A02;
    public final C15510nN A03;

    public C22180yf(C15570nT r1, C14850m9 r2, C22710zW r3, C15510nN r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00be, code lost:
        if (r8 != null) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00f8, code lost:
        if (r8 != null) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0158, code lost:
        if (r2.length() > 512) goto L_0x015a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0100 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C32671cX A00(android.net.Uri r13, X.C255019q r14, X.AnonymousClass1BK r15, X.C14850m9 r16, boolean r17) {
        /*
        // Method dump skipped, instructions count: 598
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22180yf.A00(android.net.Uri, X.19q, X.1BK, X.0m9, boolean):X.1cX");
    }

    public static UserJid A01(Uri uri) {
        String queryParameter = uri.getQueryParameter("phoneNumber");
        if ("wa.me".equals(uri.getHost())) {
            boolean z = false;
            if (uri.getPathSegments().size() == 2) {
                z = true;
            }
            AnonymousClass009.A0E(z);
            queryParameter = uri.getLastPathSegment();
        }
        if (queryParameter == null) {
            return null;
        }
        try {
            return C27631Ih.A02(queryParameter);
        } catch (AnonymousClass1MW unused) {
            return null;
        }
    }

    public static boolean A02(C14850m9 r4, String str) {
        Uri build;
        if (r4.A07(1483) || r4.A07(1849)) {
            String lowerCase = str.toLowerCase(Locale.US);
            if (lowerCase.startsWith("wa.me")) {
                lowerCase = lowerCase.replace("wa.me", "https://wa.me");
            }
            Uri parse = Uri.parse(lowerCase);
            String str2 = null;
            if (parse.getHost() != null) {
                str2 = parse.getHost().toLowerCase(Locale.US);
            }
            if ("wa.me".equals(str2) && (build = new Uri.Builder().scheme("https").encodedAuthority(str2).encodedPath(parse.getEncodedPath()).encodedQuery(parse.getEncodedQuery()).encodedFragment(parse.getEncodedFragment()).build()) != null) {
                String scheme = build.getScheme();
                if (!TextUtils.isEmpty(scheme)) {
                    String host = build.getHost();
                    if (!TextUtils.isEmpty(host) && A04(scheme, host)) {
                        List<String> pathSegments = build.getPathSegments();
                        if (pathSegments.size() == 1 && Pattern.matches("^(?![0-9.]+$)[a-zA-Z0-9.]{5,35}$", pathSegments.get(0))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static boolean A03(C14850m9 r3, String str) {
        if (!A02(r3, str)) {
            return false;
        }
        try {
            Uri parse = Uri.parse(str.toLowerCase(Locale.US));
            if (parse == null || parse.getQueryParameterNames().isEmpty()) {
                return false;
            }
            if ("1".equals(parse.getQueryParameter("qr"))) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean A04(String str, String str2) {
        return ("http".equals(str) || "https".equals(str)) && "wa.me".equals(str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b7, code lost:
        if (r15.A07(1129) == false) goto L_0x00ba;
     */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x023e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x029f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0307  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:200:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A05(android.net.Uri r24) {
        /*
        // Method dump skipped, instructions count: 888
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22180yf.A05(android.net.Uri):int");
    }

    public final int A06(List list) {
        Object obj;
        String str = "";
        if (list.isEmpty()) {
            obj = str;
        } else {
            obj = list.get(0);
        }
        if (list.size() > 1) {
            str = (String) list.get(1);
        }
        if ("novi".equals(obj)) {
            if (this.A02.A04()) {
                switch (str.hashCode()) {
                    case -1675249184:
                        if (str.equals("add_new_debit_card")) {
                            return 15;
                        }
                        break;
                    case -940242166:
                        if (str.equals("withdraw")) {
                            return 17;
                        }
                        break;
                    case -525117557:
                        if (str.equals("reset_password")) {
                            return 18;
                        }
                        break;
                    case 103669:
                        if (str.equals("hub")) {
                            return 14;
                        }
                        break;
                    case 3046161:
                        if (str.equals("care")) {
                            return 28;
                        }
                        break;
                    case 1192345415:
                        if (str.equals("add_new_bank_account")) {
                            return 16;
                        }
                        break;
                }
            }
        } else if ("upi".equals(obj)) {
            if ("signup".equals(str)) {
                return 19;
            }
        } else if ("br".equals(obj) && "signup".equals(str)) {
            return 19;
        } else {
            if ("virality".equals(obj)) {
                return 21;
            }
            if ("tpp".equals(obj)) {
                if (this.A01.A07(848)) {
                    return 29;
                }
            } else if (this.A02.A07()) {
                return 4;
            }
        }
        return 1;
    }

    public final int A07(List list) {
        if (this.A01.A07(504) && list.size() >= 2) {
            C15570nT r0 = this.A00;
            r0.A08();
            if (r0.A00 != null && this.A03.A01()) {
                Locale locale = Locale.US;
                String lowerCase = ((String) list.get(0)).toLowerCase(locale);
                String lowerCase2 = ((String) list.get(1)).toLowerCase(locale);
                if ("account".equals(lowerCase)) {
                    if ("delete".equals(lowerCase2)) {
                        return 22;
                    }
                    if ("request_info".equals(lowerCase2)) {
                        return 23;
                    }
                } else if ("chats".equals(lowerCase) && "history".equals(lowerCase2)) {
                    return 24;
                }
            }
        }
        return 1;
    }

    public final int A08(List list) {
        if (!list.isEmpty()) {
            C15570nT r0 = this.A00;
            r0.A08();
            if (r0.A00 != null && this.A03.A01()) {
                Object obj = list.get(0);
                if (this.A01.A07(728) && "20210210".equals(obj)) {
                    return 27;
                }
            }
        }
        return 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r0 != false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A09(android.net.Uri r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.getScheme()
            java.lang.String r1 = r3.getHost()
            boolean r0 = A04(r0, r1)
            if (r0 != 0) goto L_0x0017
            java.lang.String r0 = "api.whatsapp.com"
            boolean r0 = r0.equals(r1)
            r1 = 0
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r1 = 1
        L_0x0018:
            java.util.List r0 = r3.getPathSegments()
            int r0 = r0.size()
            if (r0 > r1) goto L_0x0024
            r0 = 0
            return r0
        L_0x0024:
            java.util.List r0 = r3.getPathSegments()
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22180yf.A09(android.net.Uri):java.lang.String");
    }

    public boolean A0A(String str) {
        if (TextUtils.isEmpty(str) || 6 != A05(Uri.parse(str))) {
            return false;
        }
        return true;
    }
}
