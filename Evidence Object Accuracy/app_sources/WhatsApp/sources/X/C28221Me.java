package X;

import java.util.Arrays;

/* renamed from: X.1Me  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28221Me {
    public final Object A00;

    public C28221Me(Object obj) {
        double doubleValue;
        if (obj == null) {
            obj = null;
        } else {
            if (obj instanceof Boolean) {
                doubleValue = ((Boolean) obj).booleanValue() ? 1.0d : 0.0d;
            } else if (obj instanceof Number) {
                doubleValue = ((Number) obj).doubleValue();
            } else if (!(obj instanceof String)) {
                throw new IllegalArgumentException("Attribute type must be Boolean, Number, or String");
            }
            obj = Double.valueOf(doubleValue);
        }
        this.A00 = obj;
    }

    public boolean equals(Object obj) {
        Object obj2;
        if (this != obj) {
            if (obj == null || C28221Me.class != obj.getClass()) {
                return false;
            }
            C28221Me r5 = (C28221Me) obj;
            Object obj3 = this.A00;
            if (obj3 == null || (obj2 = r5.A00) == null) {
                if (obj3 == r5.A00) {
                    return true;
                }
                return false;
            } else if (obj3 != obj2 && !obj3.equals(obj2)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00});
    }
}
