package X;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import java.util.HashMap;

/* renamed from: X.2JI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JI extends AnonymousClass2JJ implements AbstractC35581iK {
    public static final String[] A00 = {"_id", "_data", "mime_type", "media_type", "date_modified", "datetaken", "orientation", "_size"};

    public AnonymousClass2JI(Uri uri, C16590pI r2, C19390u2 r3, String str, int i) {
        super(uri, r2, r3, str, i);
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        String str;
        HashMap hashMap = new HashMap();
        Uri build = this.A04.buildUpon().appendQueryParameter("distinct", "true").build();
        ContentResolver contentResolver = this.A03;
        String[] strArr = {"bucket_display_name", "bucket_id"};
        String str2 = this.A07;
        if (str2 == null) {
            str = "media_type in (1, 3)";
        } else {
            str = "media_type in (1, 3) and bucket_id=?";
        }
        Cursor query = contentResolver.query(build, strArr, str, str2 == null ? null : new String[]{str2}, null);
        if (query == null) {
            return hashMap;
        }
        while (query.moveToNext()) {
            try {
                String string = query.getString(0);
                String string2 = query.getString(1);
                if (string == null) {
                    string = "";
                }
                hashMap.put(string2, string);
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        query.close();
        return hashMap;
    }
}
