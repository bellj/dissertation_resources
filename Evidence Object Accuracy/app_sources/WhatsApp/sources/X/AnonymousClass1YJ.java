package X;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1YJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YJ {
    public Set A00 = new HashSet();

    public AnonymousClass1JO A00() {
        Set set = this.A00;
        AnonymousClass009.A05(set);
        AnonymousClass1JO r0 = new AnonymousClass1JO(set);
        this.A00 = null;
        return r0;
    }

    public void A01(AnonymousClass1JO r4) {
        AnonymousClass009.A05(this.A00);
        Iterator it = r4.iterator();
        while (it.hasNext()) {
            this.A00.add(it.next());
        }
    }

    public void A02(Object obj) {
        Set set = this.A00;
        AnonymousClass009.A05(set);
        set.add(obj);
    }
}
