package X;

import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.PublicKey;
import java.security.Signature;
import java.util.Locale;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.61O  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61O {
    public static final Set A05;
    public String A00;
    public String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    static {
        String[] strArr = new String[2];
        strArr[0] = "NONE";
        A05 = C12970iu.A13("ES256", strArr, 1);
    }

    public AnonymousClass61O(String str, JSONObject jSONObject) {
        this.A04 = true;
        try {
            JSONObject put = C117295Zj.A0a().put("typ", "JWT").put("alg", "ES256");
            if (!TextUtils.isEmpty(str)) {
                put.put("kid", str);
            }
            String obj = put.toString();
            String str2 = AnonymousClass01V.A08;
            this.A02 = Base64.encodeToString(obj.getBytes(str2), 11);
            this.A03 = Base64.encodeToString(jSONObject.toString().getBytes(str2), 11);
        } catch (UnsupportedEncodingException | JSONException e) {
            Log.w(C12960it.A0d(": error", C12960it.A0k("JWT: ")), e);
            throw new Error(e);
        }
    }

    public AnonymousClass61O(PublicKey publicKey, String str) {
        int indexOf = str.indexOf(46);
        int lastIndexOf = str.lastIndexOf(46);
        if (!(indexOf == -1 || lastIndexOf == -1 || indexOf == lastIndexOf)) {
            int i = indexOf + 1;
            if (str.indexOf(46, i) == lastIndexOf) {
                String substring = str.substring(0, indexOf);
                this.A02 = substring;
                String substring2 = str.substring(i, lastIndexOf);
                this.A03 = substring2;
                this.A01 = str.substring(lastIndexOf + 1);
                if (publicKey == null || A03(publicKey)) {
                    try {
                        byte[] decode = Base64.decode(substring, 8);
                        Base64.decode(substring2, 8);
                        Base64.decode(this.A01, 8);
                        String upperCase = C13000ix.A05(new String(decode)).getString("alg").toUpperCase(Locale.US);
                        this.A00 = upperCase;
                        if (A05.contains(upperCase)) {
                            this.A04 = !"NONE".equals(this.A00);
                            return;
                        }
                        throw new C124685pw("JWT algorithm not supported");
                    } catch (IllegalArgumentException unused) {
                        throw new C124685pw("Wrong Base64 encoding.");
                    } catch (JSONException e) {
                        throw new C124685pw(e.getMessage());
                    }
                } else {
                    throw new C124685pw("Signature not valid");
                }
            }
        }
        throw new C124685pw("Invalid JWT Token");
    }

    public static byte[] A00(byte[] bArr) {
        int length;
        int i = 0;
        while (true) {
            length = bArr.length;
            if (i >= length || bArr[i] != 0) {
                break;
            }
            i++;
        }
        if (i == length) {
            return new byte[]{0};
        }
        int i2 = length - i;
        if ((bArr[i] & 255) > 127) {
            byte[] bArr2 = new byte[i2 + 1];
            System.arraycopy(bArr, i, bArr2, 1, i2);
            return bArr2;
        }
        byte[] bArr3 = new byte[i2];
        System.arraycopy(bArr, i, bArr3, 0, i2);
        return bArr3;
    }

    public static byte[] A01(byte[] bArr) {
        int i;
        int length = bArr.length;
        if (length < 8 || bArr[0] != 48) {
            throw new AssertionError("Invalid ECDSA signature format");
        }
        byte b = bArr[1];
        if (b > 0) {
            i = 2;
        } else if (b == -127) {
            i = 3;
        } else {
            throw new AssertionError("Invalid ECDSA signature format");
        }
        byte b2 = bArr[i + 1];
        int i2 = b2;
        while (i2 > 0 && bArr[((i + 2) + b2) - i2] == 0) {
            i2--;
        }
        int i3 = i + 2 + b2;
        byte b3 = bArr[i3 + 1];
        int i4 = b3;
        while (i4 > 0 && bArr[((i3 + 2) + b3) - i4] == 0) {
            i4--;
        }
        int max = Math.max(Math.max(i2, i4), 32);
        int i5 = bArr[i - 1] & 255;
        if (i5 == length - i && i5 == b2 + 2 + 2 + b3 && bArr[i] == 2 && bArr[i3] == 2) {
            int i6 = max << 1;
            byte[] bArr2 = new byte[i6];
            System.arraycopy(bArr, i3 - i2, bArr2, max - i2, i2);
            System.arraycopy(bArr, ((i3 + 2) + b3) - i4, bArr2, i6 - i4, i4);
            return bArr2;
        }
        throw new AssertionError("Invalid ECDSA signature format");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (android.text.TextUtils.isEmpty(r5.A01) == false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A02() {
        /*
            r5 = this;
            boolean r0 = r5.A04
            r4 = 0
            r3 = 1
            if (r0 == 0) goto L_0x000f
            java.lang.String r0 = r5.A01
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            r2 = 0
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            r2 = 1
        L_0x0010:
            java.lang.String r0 = "JWT: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = ":  payload hasn't been signed"
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            X.AnonymousClass009.A0B(r0, r2)
            r0 = 3
            java.lang.String[] r2 = new java.lang.String[r0]
            java.lang.String r0 = r5.A02
            r2[r4] = r0
            java.lang.String r0 = r5.A03
            r2[r3] = r0
            r1 = 2
            java.lang.String r0 = r5.A01
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x003e
            java.lang.String r0 = ""
        L_0x0035:
            r2[r1] = r0
            java.lang.String r0 = "."
            java.lang.String r0 = android.text.TextUtils.join(r0, r2)
            return r0
        L_0x003e:
            java.lang.String r0 = r5.A01
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass61O.A02():java.lang.String");
    }

    public boolean A03(PublicKey publicKey) {
        byte[] bArr;
        int i;
        try {
            Signature instance = Signature.getInstance("SHA256withECDSA");
            instance.initVerify(publicKey);
            instance.update(A04());
            byte[] decode = Base64.decode(this.A01, 8);
            int length = decode.length;
            if (length % 2 == 0) {
                int i2 = length / 2;
                byte[][] A06 = C16050oM.A06(decode, i2, i2);
                byte[] A00 = A00(A06[0]);
                byte[] A002 = A00(A06[1]);
                int length2 = A00.length;
                int length3 = A002.length;
                int i3 = length2 + 4 + length3;
                if (i3 <= 255) {
                    if (i3 > 127) {
                        bArr = new byte[i3 + 3];
                        bArr[0] = 48;
                        bArr[1] = -127;
                        bArr[2] = (byte) i3;
                        i = 3;
                    } else {
                        bArr = new byte[i3 + 2];
                        bArr[0] = 48;
                        bArr[1] = (byte) i3;
                        i = 2;
                    }
                    int i4 = i + 1;
                    bArr[i] = 2;
                    int i5 = i4 + 1;
                    bArr[i4] = (byte) length2;
                    System.arraycopy(A00, 0, bArr, i5, length2);
                    int i6 = i5 + length2;
                    int i7 = i6 + 1;
                    bArr[i6] = 2;
                    bArr[i7] = (byte) length3;
                    System.arraycopy(A002, 0, bArr, i7 + 1, length3);
                    return instance.verify(bArr);
                }
                throw new C124685pw("Invalid JWT Signature");
            }
            throw new C124685pw("Invalid JWT Signature");
        } catch (Exception e) {
            Log.w(C12960it.A0d(": Can't verify signature ", C12960it.A0k("JWT: ")), e);
            return false;
        }
    }

    public byte[] A04() {
        try {
            return C117315Zl.A0a(TextUtils.join(".", new String[]{this.A02, this.A03}));
        } catch (UnsupportedEncodingException e) {
            Log.w(C12960it.A0Z(e, ": getSigningPayload threw ", C12960it.A0k("JWT: ")));
            throw new Error(e);
        }
    }
}
