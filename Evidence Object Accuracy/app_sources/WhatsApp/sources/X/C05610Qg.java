package X;

import android.app.RemoteInput;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import java.util.Set;

/* renamed from: X.0Qg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05610Qg {
    public static RemoteInput A00(C007103o r4) {
        Set<String> set;
        RemoteInput.Builder addExtras = new RemoteInput.Builder(r4.A02).setLabel(r4.A01).setChoices(r4.A05).setAllowFreeFormInput(r4.A04).addExtras(r4.A00);
        if (Build.VERSION.SDK_INT >= 26 && (set = r4.A03) != null) {
            for (String str : set) {
                AnonymousClass0KU.A00(addExtras, str, true);
            }
        }
        if (Build.VERSION.SDK_INT >= 29) {
            AnonymousClass0KV.A00(addExtras, 0);
        }
        return addExtras.build();
    }

    public static Bundle A01(Intent intent) {
        return RemoteInput.getResultsFromIntent(intent);
    }
}
