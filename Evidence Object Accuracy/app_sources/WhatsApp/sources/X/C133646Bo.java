package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.6Bo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133646Bo implements AbstractC16960q2 {
    public final C15610nY A00;
    public final AnonymousClass131 A01;
    public final C16590pI A02;
    public final C17070qD A03;

    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124615pp.class;
    }

    public C133646Bo(C15610nY r1, AnonymousClass131 r2, C16590pI r3, C17070qD r4) {
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
    }

    @Override // X.AbstractC16960q2
    public /* bridge */ /* synthetic */ Object Aam(Enum r6, Object obj, Map map) {
        C15370n3 r7 = (C15370n3) obj;
        EnumC124615pp r62 = (EnumC124615pp) r6;
        boolean A0N = C16700pc.A0N(r7, r62);
        int A01 = C117315Zl.A01(r62, C125205qs.A00);
        if (A01 == A0N) {
            return this.A00.A04(r7);
        }
        if (A01 == 2) {
            AnonymousClass131 r4 = this.A01;
            Context context = this.A02.A00;
            Bitmap A00 = r4.A00(context, r7, context.getResources().getDimension(R.dimen.small_avatar_radius), context.getResources().getDimensionPixelSize(R.dimen.small_avatar_size));
            if (A00 != null) {
                return C37501mV.A07(A00);
            }
            return C37501mV.A07(BitmapFactory.decodeResource(context.getResources(), R.drawable.avatar_contact));
        } else if (A01 == 3) {
            UserJid A05 = C15370n3.A05(r7);
            if (A05 != null) {
                return C117305Zk.A0F(A05, this.A03);
            }
            return null;
        } else {
            throw new C113285Gx();
        }
    }
}
