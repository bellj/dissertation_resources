package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.math.BigDecimal;

/* renamed from: X.0vv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20540vv {
    public final C16510p9 A00;
    public final C18460sU A01;
    public final C16490p7 A02;

    public C20540vv(C16510p9 r1, C18460sU r2, C16490p7 r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public final void A00(ContentValues contentValues, AnonymousClass1XV r5, long j) {
        contentValues.put("message_row_id", Long.valueOf(j));
        UserJid userJid = r5.A01;
        if (userJid != null) {
            contentValues.put("business_owner_jid", Long.valueOf(this.A01.A01(userJid)));
        }
        C30021Vq.A04(contentValues, "product_id", r5.A06);
        C30021Vq.A04(contentValues, "title", r5.A09);
        C30021Vq.A04(contentValues, "description", r5.A04);
        String str = r5.A03;
        if (!(str == null || r5.A0A == null)) {
            contentValues.put("currency_code", str);
            BigDecimal bigDecimal = r5.A0A;
            BigDecimal bigDecimal2 = C30701Ym.A00;
            contentValues.put("amount_1000", Long.valueOf(bigDecimal.multiply(bigDecimal2).longValue()));
            BigDecimal bigDecimal3 = r5.A0B;
            if (bigDecimal3 != null) {
                contentValues.put("sale_amount_1000", Long.valueOf(bigDecimal3.multiply(bigDecimal2).longValue()));
            }
        }
        C30021Vq.A04(contentValues, "retailer_id", r5.A08);
        C30021Vq.A04(contentValues, "url", r5.A07);
        contentValues.put("product_image_count", Integer.valueOf(r5.A00));
        C30021Vq.A04(contentValues, "body", r5.A02);
        C30021Vq.A04(contentValues, "footer", r5.A05);
    }

    public void A01(AnonymousClass1XV r7, long j) {
        boolean z = true;
        boolean z2 = false;
        if (r7.A08() == 2) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("ProductMessageStore/insertOrUpdateQuotedProductMessage/message in main storage; key=");
        sb.append(r7.A0z);
        AnonymousClass009.A0B(sb.toString(), z2);
        try {
            C16310on A02 = this.A02.A02();
            ContentValues contentValues = new ContentValues();
            A00(contentValues, r7, j);
            if (A02.A03.A06(contentValues, "message_quoted_product", 5) != j) {
                z = false;
            }
            AnonymousClass009.A0C("ProductMessageStore/insertOrUpdateQuotedProductMessage/inserted row should have same row_id", z);
            A02.close();
        } catch (SQLiteConstraintException e) {
            StringBuilder sb2 = new StringBuilder("ProductMessageStore/insertOrUpdateQuotedProductMessage/fail to insert. Error message is: ");
            sb2.append(e);
            Log.e(sb2.toString());
        }
    }

    public final void A02(AnonymousClass1XV r8, String str) {
        boolean z = false;
        if (r8.A11 > 0) {
            z = true;
        }
        StringBuilder sb = new StringBuilder("ProductMessageStore/fillProductDataIfAvailable/message must have row_id set; key=");
        sb.append(r8.A0z);
        AnonymousClass009.A0B(sb.toString(), z);
        String[] strArr = {String.valueOf(r8.A11)};
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09(str, strArr);
            if (A09 != null) {
                if (A09.moveToLast()) {
                    r8.A01 = (UserJid) this.A01.A07(UserJid.class, A09.getLong(A09.getColumnIndexOrThrow("business_owner_jid")));
                    r8.A06 = A09.getString(A09.getColumnIndexOrThrow("product_id"));
                    r8.A09 = A09.getString(A09.getColumnIndexOrThrow("title"));
                    r8.A02 = A09.getString(A09.getColumnIndexOrThrow("body"));
                    r8.A05 = A09.getString(A09.getColumnIndexOrThrow("footer"));
                    r8.A04 = A09.getString(A09.getColumnIndexOrThrow("description"));
                    String string = A09.getString(A09.getColumnIndexOrThrow("currency_code"));
                    r8.A03 = string;
                    if (!TextUtils.isEmpty(string)) {
                        try {
                            r8.A0A = C30701Ym.A00(new C30711Yn(r8.A03), A09.getLong(A09.getColumnIndexOrThrow("amount_1000")));
                            r8.A0B = C30701Ym.A00(new C30711Yn(r8.A03), A09.getLong(A09.getColumnIndexOrThrow("sale_amount_1000")));
                        } catch (IllegalArgumentException unused) {
                            r8.A03 = null;
                        }
                    }
                    r8.A08 = A09.getString(A09.getColumnIndexOrThrow("retailer_id"));
                    r8.A07 = A09.getString(A09.getColumnIndexOrThrow("url"));
                    r8.A00 = A09.getInt(A09.getColumnIndexOrThrow("product_image_count"));
                }
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th;
        }
    }
}
