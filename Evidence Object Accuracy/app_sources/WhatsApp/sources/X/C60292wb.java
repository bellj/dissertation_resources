package X;

import java.util.List;

/* renamed from: X.2wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C60292wb extends AnonymousClass4KB {
    public final List A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C60292wb) && C16700pc.A0O(this.A00, ((C60292wb) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public C60292wb(List list) {
        super(list);
        this.A00 = list;
    }

    public String toString() {
        return C12960it.A0a(this.A00, C12960it.A0k("SuccessContinueSearch(successItems="));
    }
}
