package X;

/* renamed from: X.1oT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38661oT extends AbstractC16350or {
    public final int A00;
    public final C16120oU A01;
    public final C235812f A02;
    public final AnonymousClass146 A03;
    public final AbstractC38651oS A04;
    public final AnonymousClass15I A05;
    public final C235512c A06;
    public final String A07;
    public final boolean A08;

    public C38661oT(C16120oU r1, C235812f r2, AnonymousClass146 r3, AbstractC38651oS r4, AnonymousClass15I r5, C235512c r6, String str, int i, boolean z) {
        this.A02 = r2;
        this.A03 = r3;
        this.A06 = r6;
        this.A05 = r5;
        this.A07 = str;
        this.A04 = r4;
        this.A01 = r1;
        this.A00 = i;
        this.A08 = z;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01d3, code lost:
        if (r0 == false) goto L_0x01d5;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r22) {
        /*
        // Method dump skipped, instructions count: 983
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38661oT.A05(java.lang.Object[]):java.lang.Object");
    }

    /* JADX WARNING: Removed duplicated region for block: B:161:0x012c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x0012 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A08(X.AnonymousClass4S5 r12) {
        /*
        // Method dump skipped, instructions count: 810
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C38661oT.A08(X.4S5):void");
    }
}
