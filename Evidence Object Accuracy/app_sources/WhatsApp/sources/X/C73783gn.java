package X;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;
import com.whatsapp.Conversation;
import com.whatsapp.R;

/* renamed from: X.3gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73783gn extends ViewOutlineProvider {
    public final /* synthetic */ Conversation A00;

    public C73783gn(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        int dimensionPixelSize = view.getResources().getDimensionPixelSize(R.dimen.conversation_outline_size);
        outline.setOval(0, 0, dimensionPixelSize, dimensionPixelSize);
    }
}
