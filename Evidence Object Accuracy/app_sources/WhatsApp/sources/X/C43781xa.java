package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.HashSet;

/* renamed from: X.1xa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43781xa implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final C43771xZ A01;
    public final C14820m6 A02;
    public final C17220qS A03;

    public C43781xa(AbstractC15710nm r1, C43771xZ r2, C14820m6 r3, C17220qS r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        C43761xY r3 = this.A01.A00;
        StringBuilder sb = new StringBuilder("blocklistresponsehandler/general_request_timeout jid=");
        sb.append(r3.A06.A02);
        Log.i(sb.toString());
        AnonymousClass1P4 r0 = r3.A0B;
        if (r0 != null) {
            r3.A0C.A0H(r0.A01, 500);
        }
        r3.A03.A0I(r3.A0D);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        int A00 = C41151sz.A00(r5);
        C43761xY r2 = this.A01.A00;
        StringBuilder sb = new StringBuilder("blocklistresponsehandler/general_request_failed ");
        sb.append(A00);
        sb.append(" | ");
        sb.append(r2.A06.A02);
        Log.i(sb.toString());
        r2.A03.A0I(r2.A0E);
        AnonymousClass1P4 r0 = r2.A0B;
        if (r0 != null) {
            r2.A0C.A0H(r0.A01, A00);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r11, String str) {
        AnonymousClass1V8 A0C = r11.A0C();
        AnonymousClass1V8.A01(A0C, "list");
        if (A0C.A0H("matched").equals("false")) {
            HashSet hashSet = new HashSet();
            AnonymousClass1V8[] r9 = A0C.A03;
            if (r9 != null) {
                for (AnonymousClass1V8 r3 : r9) {
                    AnonymousClass1V8.A01(r3, "item");
                    hashSet.add(r3.A0A(this.A00, UserJid.class, "jid"));
                }
            }
            if (TextUtils.equals(A0C.A0I("c_dhash", null), this.A02.A00.getString("block_list_v2_dhash", null))) {
                Log.w("blocklistv2setprotocolhelper/onSuccess/only dhash mis-match.");
                this.A01.A00(A0C.A0I("dhash", null), hashSet, false);
                return;
            }
            Log.w("blocklistv2setprotocolhelper/onSuccess/dhash and c_dhash mis-match.");
            this.A01.A00(null, hashSet, true);
            return;
        }
        C43771xZ r1 = this.A01;
        String A0I = A0C.A0I("dhash", null);
        C43761xY r32 = r1.A00;
        AnonymousClass1P2 r8 = r32.A06;
        UserJid userJid = r8.A02;
        boolean z = r8.A07;
        if (z) {
            C238013b r6 = r32.A05;
            r6.A0Q.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(r6, 19, r8));
        }
        StringBuilder sb = new StringBuilder("blocklistresponsehandler/general_request_success jid=");
        sb.append(userJid);
        Log.i(sb.toString());
        r32.A00 = true;
        C238013b r12 = r32.A05;
        synchronized (r12) {
            if (z) {
                boolean add = r12.A0S.add(userJid);
                if (add) {
                    r12.A0D(userJid, A0I, true);
                }
            } else {
                boolean remove = r12.A0S.remove(userJid);
                if (remove) {
                    r12.A0D(userJid, A0I, false);
                }
            }
        }
        r32.A03.A0I(r32.A0E);
        AnonymousClass1P4 r0 = r32.A0B;
        if (r0 != null) {
            r32.A0C.A0H(r0.A01, 200);
        }
    }
}
