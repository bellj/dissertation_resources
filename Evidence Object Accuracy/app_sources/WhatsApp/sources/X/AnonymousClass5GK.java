package X;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.Extension;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.5GK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GK implements AnonymousClass5WR {
    public static final Map A05;
    public String A00;
    public AnonymousClass4TK A01;
    public boolean A02;
    public final AnonymousClass5S2 A03;
    public final C113425Hn A04;

    public AnonymousClass5GK(AnonymousClass5S2 r1, C113425Hn r2) {
        this.A04 = r2;
        this.A03 = r1;
    }

    @Override // X.AnonymousClass5WR
    public void AIv(AnonymousClass4TK r2) {
        this.A01 = r2;
        this.A02 = C94664cJ.A01("ocsp.enable");
        this.A00 = C94664cJ.A00("ocsp.responderURL");
    }

    static {
        HashMap A11 = C12970iu.A11();
        A05 = A11;
        A11.put(C72453ed.A12("1.2.840.113549.1.1.5"), "SHA1WITHRSA");
        A11.put(AnonymousClass1TJ.A2D, "SHA224WITHRSA");
        A11.put(AnonymousClass1TJ.A2E, "SHA256WITHRSA");
        C72453ed.A1L(AnonymousClass1TJ.A2F, A11);
        C72453ed.A1M(AbstractC116955Xo.A0G, A11);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x011a, code lost:
        if (r0 != false) goto L_0x0127;
     */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x011f A[Catch: CertPathValidatorException -> 0x022c, GeneralSecurityException -> 0x021a, IOException -> 0x0208, TryCatch #2 {IOException -> 0x0208, CertPathValidatorException -> 0x022c, GeneralSecurityException -> 0x021a, blocks: (B:3:0x0002, B:5:0x000a, B:7:0x000e, B:9:0x0018, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x003a, B:17:0x003c, B:19:0x0044, B:21:0x004c, B:22:0x005e, B:23:0x0067, B:25:0x007b, B:27:0x0082, B:29:0x008a, B:32:0x00a8, B:33:0x00b3, B:35:0x00bd, B:36:0x00c4, B:38:0x00c9, B:39:0x00e4, B:42:0x00ea, B:43:0x00ee, B:45:0x00f4, B:49:0x0109, B:54:0x011f, B:55:0x0126, B:58:0x012b, B:59:0x0134, B:61:0x0168, B:63:0x016f, B:64:0x0190, B:67:0x0196, B:68:0x019a, B:70:0x01b0, B:72:0x01b6, B:74:0x01c2, B:75:0x01c5, B:79:0x01de, B:81:0x01f0, B:82:0x01f6, B:85:0x01fa, B:86:0x0200, B:87:0x0201, B:88:0x0207), top: B:97:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x012b A[Catch: CertPathValidatorException -> 0x022c, GeneralSecurityException -> 0x021a, IOException -> 0x0208, TryCatch #2 {IOException -> 0x0208, CertPathValidatorException -> 0x022c, GeneralSecurityException -> 0x021a, blocks: (B:3:0x0002, B:5:0x000a, B:7:0x000e, B:9:0x0018, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x003a, B:17:0x003c, B:19:0x0044, B:21:0x004c, B:22:0x005e, B:23:0x0067, B:25:0x007b, B:27:0x0082, B:29:0x008a, B:32:0x00a8, B:33:0x00b3, B:35:0x00bd, B:36:0x00c4, B:38:0x00c9, B:39:0x00e4, B:42:0x00ea, B:43:0x00ee, B:45:0x00f4, B:49:0x0109, B:54:0x011f, B:55:0x0126, B:58:0x012b, B:59:0x0134, B:61:0x0168, B:63:0x016f, B:64:0x0190, B:67:0x0196, B:68:0x019a, B:70:0x01b0, B:72:0x01b6, B:74:0x01c2, B:75:0x01c5, B:79:0x01de, B:81:0x01f0, B:82:0x01f6, B:85:0x01fa, B:86:0x0200, B:87:0x0201, B:88:0x0207), top: B:97:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0134 A[Catch: CertPathValidatorException -> 0x022c, GeneralSecurityException -> 0x021a, IOException -> 0x0208, TryCatch #2 {IOException -> 0x0208, CertPathValidatorException -> 0x022c, GeneralSecurityException -> 0x021a, blocks: (B:3:0x0002, B:5:0x000a, B:7:0x000e, B:9:0x0018, B:11:0x0022, B:13:0x0026, B:14:0x0028, B:16:0x003a, B:17:0x003c, B:19:0x0044, B:21:0x004c, B:22:0x005e, B:23:0x0067, B:25:0x007b, B:27:0x0082, B:29:0x008a, B:32:0x00a8, B:33:0x00b3, B:35:0x00bd, B:36:0x00c4, B:38:0x00c9, B:39:0x00e4, B:42:0x00ea, B:43:0x00ee, B:45:0x00f4, B:49:0x0109, B:54:0x011f, B:55:0x0126, B:58:0x012b, B:59:0x0134, B:61:0x0168, B:63:0x016f, B:64:0x0190, B:67:0x0196, B:68:0x019a, B:70:0x01b0, B:72:0x01b6, B:74:0x01c2, B:75:0x01c5, B:79:0x01de, B:81:0x01f0, B:82:0x01f6, B:85:0x01fa, B:86:0x0200, B:87:0x0201, B:88:0x0207), top: B:97:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01f9 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(java.security.cert.X509Certificate r12, X.C114545Md r13, X.AnonymousClass4TK r14, X.AnonymousClass5S2 r15, byte[] r16) {
        /*
        // Method dump skipped, instructions count: 558
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5GK.A00(java.security.cert.X509Certificate, X.5Md, X.4TK, X.5S2, byte[]):boolean");
    }

    public final C114555Me A01(AnonymousClass5NG r6, C114725Mv r7, C114565Mf r8) {
        try {
            AnonymousClass5S2 r3 = this.A03;
            AnonymousClass1TK r2 = r7.A01;
            String str = (String) C88614Gj.A00.get(r2);
            if (str == null) {
                str = r2.A01;
            }
            MessageDigest instance = MessageDigest.getInstance(str, ((AnonymousClass5GT) r3).A00);
            C114575Mg r32 = r8.A03;
            return new C114555Me(r6, new AnonymousClass5N5(instance.digest(r32.A06.A02("DER"))), new AnonymousClass5N5(instance.digest(r32.A09.A00.A0B())), r7);
        } catch (Exception e) {
            throw new CertPathValidatorException(C12960it.A0b("problem creating ID: ", e), e);
        }
    }

    public final C114565Mf A02() {
        try {
            return C114565Mf.A00(this.A01.A03.getEncoded());
        } catch (Exception e) {
            throw AnonymousClass4TK.A00(C12960it.A0d(e.getMessage(), C12960it.A0k("cannot process signing cert: ")), e, this.A01);
        }
    }

    @Override // X.AnonymousClass5WR
    public void check(Certificate certificate) {
        boolean z;
        byte[] bArr;
        AnonymousClass5ML r6;
        AnonymousClass5MJ r5;
        AnonymousClass5MO r2;
        X509Certificate x509Certificate = (X509Certificate) certificate;
        C113425Hn r1 = this.A04;
        Map<X509Certificate, byte[]> ocspResponses = r1.getOcspResponses();
        URI ocspResponder = r1.getOcspResponder();
        if (ocspResponder == null) {
            String str = this.A00;
            if (str != null) {
                try {
                    ocspResponder = new URI(str);
                } catch (URISyntaxException e) {
                    throw AnonymousClass4TK.A00(C12960it.A0d(e.getMessage(), C12960it.A0k("configuration error: ")), e, this.A01);
                }
            } else {
                byte[] extensionValue = x509Certificate.getExtensionValue(C114715Mu.A04.A01);
                ocspResponder = null;
                if (extensionValue != null) {
                    byte[] A052 = AnonymousClass5NH.A05(extensionValue);
                    if (A052 instanceof AnonymousClass5MO) {
                        r2 = (AnonymousClass5MO) A052;
                    } else {
                        r2 = A052 != null ? new AnonymousClass5MO(AbstractC114775Na.A04(A052)) : null;
                    }
                    C114595Mi[] r22 = r2.A00;
                    int length = r22.length;
                    C114595Mi[] r8 = new C114595Mi[length];
                    System.arraycopy(r22, 0, r8, 0, length);
                    for (int i = 0; i != length; i++) {
                        C114595Mi r4 = r8[i];
                        if (C114595Mi.A03.A04(r4.A00)) {
                            AnonymousClass5N1 r42 = r4.A01;
                            if (r42.A00 == 6) {
                                try {
                                    ocspResponder = new URI(((AnonymousClass5VP) r42.A01).AGy());
                                    break;
                                } catch (URISyntaxException unused) {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        if (ocspResponses.get(x509Certificate) != null || ocspResponder == null) {
            List<Extension> ocspExtensions = r1.getOcspExtensions();
            bArr = null;
            for (int i2 = 0; i2 != ocspExtensions.size(); i2++) {
                Extension extension = ocspExtensions.get(i2);
                bArr = extension.getValue();
                if ("1.3.6.1.5.5.7.48.1.2".equals(extension.getId())) {
                }
            }
            z = false;
        } else if (this.A00 == null && r1.getOcspResponder() == null && !this.A02) {
            AnonymousClass4TK r0 = this.A01;
            throw new C113375Hg("OCSP disabled by \"ocsp.enable\" setting", r0.A02, r0.A00);
        } else {
            try {
                ocspResponses.put(x509Certificate, AnonymousClass4ZM.A00(ocspResponder, r1.getOcspResponderCert(), r1.getOcspExtensions(), A01(new AnonymousClass5NG(x509Certificate.getSerialNumber()), new C114725Mv(AnonymousClass1TW.A07), A02()), this.A01, this.A03).A01());
                bArr = null;
                z = true;
            } catch (IOException e2) {
                AnonymousClass4TK r02 = this.A01;
                throw new CertPathValidatorException("unable to encode OCSP response", e2, r02.A02, r02.A00);
            }
        }
        if (!ocspResponses.isEmpty()) {
            byte[] bArr2 = ocspResponses.get(x509Certificate);
            AnonymousClass5MI r7 = bArr2 instanceof AnonymousClass5MI ? (AnonymousClass5MI) bArr2 : bArr2 != null ? new AnonymousClass5MI(AbstractC114775Na.A04(bArr2)) : null;
            AnonymousClass5NG r82 = new AnonymousClass5NG(x509Certificate.getSerialNumber());
            if (r7 != null) {
                AnonymousClass5ND r62 = r7.A00.A00;
                if (r62.A0B() == 0) {
                    AnonymousClass5MV r72 = r7.A01;
                    if (r72 == null) {
                        r72 = null;
                    }
                    if (r72.A00.A04(AbstractC116925Xl.A02)) {
                        try {
                            byte[] bArr3 = r72.A01.A00;
                            C114545Md r73 = bArr3 instanceof C114545Md ? (C114545Md) bArr3 : bArr3 != null ? new C114545Md(AbstractC114775Na.A04(bArr3)) : null;
                            if (!z) {
                                if (!A00(r1.getOcspResponderCert(), r73, this.A01, this.A03, bArr)) {
                                    return;
                                }
                            }
                            C114665Mp r03 = r73.A02;
                            if (r03 == null) {
                                r03 = null;
                            }
                            AbstractC114775Na r74 = r03.A02;
                            C114555Me r10 = null;
                            for (int i3 = 0; i3 != r74.A0B(); i3++) {
                                AnonymousClass1TN A0D = r74.A0D(i3);
                                if (A0D instanceof AnonymousClass5ML) {
                                    r6 = (AnonymousClass5ML) A0D;
                                } else {
                                    r6 = A0D != null ? new AnonymousClass5ML(AbstractC114775Na.A04(A0D)) : null;
                                }
                                C114555Me r9 = r6.A02;
                                if (r82.A04(r9.A00)) {
                                    AnonymousClass5NE r12 = r6.A00;
                                    if (r12 == null || !new Date(this.A01.A04.getTime()).after(r12.A0D())) {
                                        if (r10 == null || !r10.A03.equals(r9.A03)) {
                                            r10 = A01(r82, r9.A03, A02());
                                        }
                                        if (r10.equals(r9)) {
                                            C114755My r13 = r6.A03;
                                            int i4 = r13.A00;
                                            if (i4 == 0) {
                                                return;
                                            }
                                            if (i4 == 1) {
                                                AnonymousClass1TN r52 = r13.A01;
                                                if (r52 instanceof AnonymousClass5MJ) {
                                                    r5 = (AnonymousClass5MJ) r52;
                                                } else {
                                                    r5 = r52 != null ? new AnonymousClass5MJ(AbstractC114775Na.A04(r52)) : null;
                                                }
                                                C114675Mq r23 = r5.A01;
                                                StringBuilder A0h = C12960it.A0h();
                                                A0h.append("certificate revoked, reason=(");
                                                A0h.append(r23);
                                                A0h.append("), date=");
                                                throw AnonymousClass4TK.A00(C12970iu.A0s(r5.A00.A0D(), A0h), null, this.A01);
                                            }
                                            throw AnonymousClass4TK.A00("certificate revoked, details unknown", null, this.A01);
                                        }
                                    } else {
                                        throw new C113385Hh();
                                    }
                                }
                            }
                        } catch (CertPathValidatorException e3) {
                            throw e3;
                        } catch (Exception e4) {
                            AnonymousClass4TK r04 = this.A01;
                            throw new CertPathValidatorException("unable to process OCSP response", e4, r04.A02, r04.A00);
                        }
                    }
                } else {
                    throw AnonymousClass4TK.A00(C12970iu.A0s(new BigInteger(r62.A01), C12960it.A0k("OCSP response failed: ")), null, this.A01);
                }
            } else {
                AnonymousClass4TK r05 = this.A01;
                throw new C113375Hg("no OCSP response found for certificate", r05.A02, r05.A00);
            }
        } else {
            AnonymousClass4TK r06 = this.A01;
            throw new C113375Hg("no OCSP response found for any certificate", r06.A02, r06.A00);
        }
    }
}
