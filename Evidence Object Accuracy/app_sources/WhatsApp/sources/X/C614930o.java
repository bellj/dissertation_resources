package X;

/* renamed from: X.30o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614930o extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;

    public C614930o() {
        super(3508, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(5, this.A04);
        r3.Abe(6, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamIqSend {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "iqTimeToGetResponse", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "iqTimeToProcess", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "iqTimeToQueue", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "iqTimeToSend", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "iqTotalTime", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "iqType", this.A05);
        return C12960it.A0d("}", A0k);
    }
}
