package X;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiProvideMoreInfoBottomSheetActivity;
import com.whatsapp.payments.ui.IndiaUpiSimVerificationActivity;
import com.whatsapp.util.Log;

/* renamed from: X.5iP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121505iP extends AbstractActivityC121565iW implements AbstractC136216Lr {
    public AnonymousClass102 A00;
    public C64513Fv A01;
    public AnonymousClass61M A02;
    public C120435gB A03;
    public AnonymousClass69E A04;

    public void A30() {
        String str;
        A2C(R.string.register_wait_message);
        AnonymousClass6BE r4 = ((AbstractActivityC121665jA) this).A0D;
        Integer A0V = C12960it.A0V();
        Integer A0k = C12980iv.A0k();
        if (!(this instanceof IndiaUpiProvideMoreInfoBottomSheetActivity)) {
            str = "notify_verification_screen";
        } else {
            str = "notify_verification_prompt";
        }
        r4.AKg(A0V, A0k, str, ((AbstractActivityC121665jA) this).A0K);
        C120435gB r9 = this.A03;
        Log.i("PAY: IndiaUpiPaymentSetup sendGetPspRoutingAndListKeys called");
        C17220qS r3 = r9.A04;
        String A01 = r3.A01();
        C117295Zj.A1B(r3, new C120585gQ(r9.A01, r9.A02, r9.A07, ((C126705tJ) r9).A00, r9), new C126525t1(new C128665wT(A01)).A00, A01);
    }

    public void A31() {
        AaN();
        AnonymousClass61M.A02(this, null, getString(R.string.payments_generic_error)).show();
    }

    public void A32(C119755f3 r4) {
        Intent A0D = C12990iw.A0D(this, IndiaUpiSimVerificationActivity.class);
        A2v(A0D);
        A0D.putExtra("extra_in_setup", true);
        A0D.putExtra("extra_selected_bank", r4);
        A0D.putExtra("extra_referral_screen", ((AbstractActivityC121665jA) this).A0K);
        startActivity(A0D);
        finish();
    }

    @Override // X.AbstractC136216Lr
    public void AUR(C452120p r4) {
        if (!AnonymousClass69E.A02(this, "upi-get-psp-routing-and-list-keys", r4.A00, false)) {
            C30931Zj r2 = ((AbstractActivityC121665jA) this).A0R;
            StringBuilder A0k = C12960it.A0k("onPspRoutingAndListKeysError: ");
            A0k.append(r4);
            r2.A06(C12960it.A0d("; showGenericError", A0k));
            A31();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        String str;
        AnonymousClass6BE r4 = ((AbstractActivityC121665jA) this).A0D;
        Integer A0V = C12960it.A0V();
        Integer A0h = C12970iu.A0h();
        if (!(this instanceof IndiaUpiProvideMoreInfoBottomSheetActivity)) {
            str = "notify_verification_screen";
        } else {
            str = "notify_verification_prompt";
        }
        r4.AKg(A0V, A0h, str, ((AbstractActivityC121665jA) this).A0K);
        super.onBackPressed();
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        C1308460e r5 = ((AbstractActivityC121665jA) this).A0A;
        this.A01 = r5.A04;
        C14900mE r2 = ((ActivityC13810kN) this).A05;
        C17220qS r4 = ((AbstractActivityC121685jC) this).A0H;
        C17070qD r9 = ((AbstractActivityC121685jC) this).A0P;
        C18610sj r8 = ((AbstractActivityC121685jC) this).A0M;
        this.A03 = new C120435gB(this, r2, this.A00, r4, r5, ((AbstractActivityC121665jA) this).A0B, ((AbstractActivityC121685jC) this).A0K, r8, r9, this);
        onConfigurationChanged(C12980iv.A0H(this));
        AnonymousClass6BE r42 = ((AbstractActivityC121665jA) this).A0D;
        Integer A0i = C12980iv.A0i();
        if (!(this instanceof IndiaUpiProvideMoreInfoBottomSheetActivity)) {
            str = "notify_verification_screen";
        } else {
            str = "notify_verification_prompt";
        }
        r42.AKg(A0i, null, str, ((AbstractActivityC121665jA) this).A0K);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        if (menuItem.getItemId() == 16908332) {
            AnonymousClass6BE r4 = ((AbstractActivityC121665jA) this).A0D;
            Integer A0V = C12960it.A0V();
            Integer A0h = C12970iu.A0h();
            if (!(this instanceof IndiaUpiProvideMoreInfoBottomSheetActivity)) {
                str = "notify_verification_screen";
            } else {
                str = "notify_verification_prompt";
            }
            r4.AKg(A0V, A0h, str, ((AbstractActivityC121665jA) this).A0K);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStop() {
        super.onStop();
        if (isFinishing()) {
            this.A03.A00 = null;
        }
    }
}
