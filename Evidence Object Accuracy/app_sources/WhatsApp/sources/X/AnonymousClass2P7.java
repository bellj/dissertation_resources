package X;

import android.content.Context;
import android.content.ContextWrapper;
import android.view.View;

/* renamed from: X.2P7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2P7 implements AnonymousClass005 {
    public final View A00;
    public final Object A01 = new Object();
    public volatile Object A02;

    public AnonymousClass2P7(View view) {
        this.A00 = view;
    }

    public static AnonymousClass2P7 A00(View view) {
        return new AnonymousClass2P7(view);
    }

    @Override // X.AnonymousClass005
    public Object generatedComponent() {
        if (this.A02 == null) {
            synchronized (this.A01) {
                if (this.A02 == null) {
                    View view = this.A00;
                    Context context = view.getContext();
                    while ((context instanceof ContextWrapper) && !AnonymousClass005.class.isInstance(context)) {
                        context = ((ContextWrapper) context).getBaseContext();
                    }
                    if (context == AnonymousClass1HD.A00(context.getApplicationContext())) {
                        AnonymousClass2PX.A00("%s, Hilt view cannot be created using the application context. Use a Hilt Fragment or Activity context.", new Object[]{view.getClass()}, false);
                        context = null;
                    }
                    if (context instanceof AnonymousClass005) {
                        AnonymousClass2FL r0 = (AnonymousClass2FL) ((AnonymousClass2FJ) AnonymousClass027.A00(AnonymousClass2FJ.class, (AnonymousClass005) context));
                        AnonymousClass2PY r02 = new AnonymousClass2PY(r0.A1C, r0.A1D, r0.A1E);
                        r02.A00 = view;
                        this.A02 = new AnonymousClass2P6(r02.A01, r02.A02, r02.A03);
                    } else {
                        throw new IllegalStateException(String.format("%s, Hilt view must be attached to an @AndroidEntryPoint Fragment or Activity.", view.getClass()));
                    }
                }
            }
        }
        return this.A02;
    }
}
