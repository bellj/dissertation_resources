package X;

import com.whatsapp.WaPreferenceFragment;
import com.whatsapp.settings.SettingsChatHistoryFragment;

/* renamed from: X.3ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70143ao implements AnonymousClass5WM {
    public final /* synthetic */ SettingsChatHistoryFragment A00;

    public C70143ao(SettingsChatHistoryFragment settingsChatHistoryFragment) {
        this.A00 = settingsChatHistoryFragment;
    }

    @Override // X.AnonymousClass5WM
    public void ASv() {
        ActivityC44201yU r1 = ((WaPreferenceFragment) this.A00).A00;
        if (r1 != null) {
            C36021jC.A00(r1, 3);
        }
    }

    @Override // X.AnonymousClass5WM
    public void ATz(boolean z, boolean z2) {
        SettingsChatHistoryFragment settingsChatHistoryFragment = this.A00;
        ActivityC44201yU r1 = ((WaPreferenceFragment) settingsChatHistoryFragment).A00;
        if (r1 != null) {
            C36021jC.A00(r1, 3);
            if (((WaPreferenceFragment) settingsChatHistoryFragment).A00 != null) {
                settingsChatHistoryFragment.A1B();
                C12960it.A1E(new C625537r(((WaPreferenceFragment) settingsChatHistoryFragment).A00, settingsChatHistoryFragment.A03, z, z2), settingsChatHistoryFragment.A0B);
            }
        }
    }
}
