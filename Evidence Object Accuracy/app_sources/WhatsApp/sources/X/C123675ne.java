package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.5ne  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123675ne extends AbstractC117645aL {
    public AnonymousClass01d A00;
    public C253118x A01;
    public final ImageButton A02;
    public final ImageView A03;
    public final TextView A04;
    public final AnonymousClass0BS A05;
    public final TextEmojiLabel A06;

    public C123675ne(Context context) {
        super(context);
        View inflate = LayoutInflater.from(context).inflate(getLayoutRes(), (ViewGroup) this, true);
        this.A04 = C12960it.A0J(inflate, R.id.nux_title);
        this.A05 = (AnonymousClass0BS) AnonymousClass028.A0D(inflate, R.id.nux_cta);
        this.A02 = (ImageButton) AnonymousClass028.A0D(inflate, R.id.nux_close);
        this.A03 = C12970iu.A0K(inflate, R.id.nux_icon);
        this.A06 = C12970iu.A0T(inflate, R.id.nux_description);
    }

    public int getLayoutRes() {
        return R.layout.payment_nux_view;
    }

    public void setCloseButtonOnClickListener(View.OnClickListener onClickListener) {
        this.A02.setOnClickListener(onClickListener);
    }

    public void setCtaButtonOnClickListener(View.OnClickListener onClickListener) {
        this.A05.setOnClickListener(onClickListener);
    }
}
