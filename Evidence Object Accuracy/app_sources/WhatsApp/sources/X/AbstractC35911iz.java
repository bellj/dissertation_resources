package X;

import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1iz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC35911iz {
    public void A00() {
        if (this instanceof C37321m2) {
            C37321m2 r5 = (C37321m2) this;
            AnonymousClass1m3 r4 = new AnonymousClass1m3(r5.A02.A00(), null);
            try {
                AnonymousClass13A r3 = r5.A01;
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("action", r4.A00);
                jSONObject.putOpt("payload", r4.A01);
                r3.A00(jSONObject.toString(), r5.A08, false);
            } catch (JSONException unused) {
                Log.e("StellaEventHandler/failed to create event");
            }
        }
    }
}
