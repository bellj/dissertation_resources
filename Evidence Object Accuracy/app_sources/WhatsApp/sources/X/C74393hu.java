package X;

import android.view.View;
import com.whatsapp.WaTabLayout;

/* renamed from: X.3hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74393hu extends AnonymousClass04v {
    public final /* synthetic */ int A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ WaTabLayout A02;

    public C74393hu(View view, WaTabLayout waTabLayout, int i) {
        this.A02 = waTabLayout;
        this.A01 = view;
        this.A00 = i;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r3) {
        super.A06(view, r3);
        r3.A07(this.A01);
        r3.A0J(C06410Tm.A00(this.A00));
    }
}
