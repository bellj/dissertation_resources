package X;

import com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity;

/* renamed from: X.5k0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121795k0 extends ActivityC121715je {
    public boolean A00 = false;

    public AbstractActivityC121795k0() {
        C117295Zj.A0p(this, 106);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            PaymentTransactionDetailsListActivity paymentTransactionDetailsListActivity = (PaymentTransactionDetailsListActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, paymentTransactionDetailsListActivity);
            ActivityC13810kN.A10(A1M, paymentTransactionDetailsListActivity);
            AbstractActivityC119265dR.A09(A1M, ActivityC13790kL.A0S(r3, A1M, paymentTransactionDetailsListActivity, ActivityC13790kL.A0Y(A1M, paymentTransactionDetailsListActivity)), paymentTransactionDetailsListActivity);
            AbstractActivityC119265dR.A0A(A1M, paymentTransactionDetailsListActivity);
            paymentTransactionDetailsListActivity.A0I = AbstractActivityC119265dR.A02(r3, A1M, paymentTransactionDetailsListActivity, A1M.AEw);
        }
    }
}
