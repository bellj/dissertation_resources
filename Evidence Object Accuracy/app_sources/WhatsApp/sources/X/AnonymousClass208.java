package X;

import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;

/* renamed from: X.208  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass208 {
    public final AbstractC15710nm A00;
    public final C14850m9 A01;
    public final AnonymousClass205 A02;
    public final AnonymousClass209 A03;

    public AnonymousClass208(AbstractC15710nm r1, C14850m9 r2, AnonymousClass205 r3, AnonymousClass209 r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static void A00(OutputStream outputStream, int i) {
        if (i < 0 || i >= 256) {
            StringBuilder sb = new StringBuilder("value out of range; value=");
            sb.append(i);
            throw new IOException(sb.toString());
        }
        outputStream.write(i & 255);
    }

    public static void A01(OutputStream outputStream, int i) {
        int i2 = 0;
        if (i != 0) {
            if (i < 256) {
                outputStream.write(248);
                A00(outputStream, i);
                return;
            } else if (i < 65536) {
                outputStream.write(249);
                if (i < 0 || i >= 65536) {
                    StringBuilder sb = new StringBuilder("value out of range; value=");
                    sb.append(i);
                    throw new IOException(sb.toString());
                }
                outputStream.write((65280 & i) >> 8);
                i2 = i & 255;
            } else {
                StringBuilder sb2 = new StringBuilder("list too long; count=");
                sb2.append(i);
                throw new IOException(sb2.toString());
            }
        }
        outputStream.write(i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        if (r2 != null) goto L_0x004e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(java.io.OutputStream r4, byte[] r5, boolean r6) {
        /*
            int r3 = r5.length
            r0 = 1048576(0x100000, float:1.469368E-39)
            if (r3 < r0) goto L_0x002c
            r0 = 254(0xfe, float:3.56E-43)
            r4.write(r0)
            r0 = 2130706432(0x7f000000, float:1.7014118E38)
            r0 = r0 & r3
            int r0 = r0 >> 24
            r4.write(r0)
            r0 = 16711680(0xff0000, float:2.3418052E-38)
        L_0x0014:
            r0 = r0 & r3
            int r0 = r0 >> 16
            r4.write(r0)
            r0 = 65280(0xff00, float:9.1477E-41)
            r0 = r0 & r3
            int r0 = r0 >> 8
            r4.write(r0)
            r0 = r3 & 255(0xff, float:3.57E-43)
            r4.write(r0)
        L_0x0028:
            r4.write(r5)
            return
        L_0x002c:
            r0 = 256(0x100, float:3.59E-43)
            if (r3 < r0) goto L_0x003c
            r0 = 253(0xfd, float:3.55E-43)
            r4.write(r0)
            r0 = 1048576(0x100000, float:1.469368E-39)
            if (r3 >= r0) goto L_0x0065
            r0 = 983040(0xf0000, float:1.377532E-39)
            goto L_0x0014
        L_0x003c:
            if (r6 == 0) goto L_0x005c
            r0 = 255(0xff, float:3.57E-43)
            byte[] r2 = A03(r5, r0)
            if (r2 != 0) goto L_0x004e
            r0 = 251(0xfb, float:3.52E-43)
            byte[] r2 = A03(r5, r0)
            if (r2 == 0) goto L_0x005c
        L_0x004e:
            r4.write(r0)
            r0 = r3 & 1
            int r1 = r0 << 7
            int r0 = r2.length
            r1 = r1 | r0
            r4.write(r1)
            r5 = r2
            goto L_0x0028
        L_0x005c:
            r0 = 252(0xfc, float:3.53E-43)
            r4.write(r0)
            A00(r4, r3)
            goto L_0x0028
        L_0x0065:
            java.lang.String r1 = "value out of range; value="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r1 = r0.toString()
            java.io.IOException r0 = new java.io.IOException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass208.A02(java.io.OutputStream, byte[], boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] A03(byte[] r10, int r11) {
        /*
            int r7 = r10.length
            r9 = 0
            r0 = 128(0x80, float:1.794E-43)
            if (r7 >= r0) goto L_0x001d
            int r0 = r7 + 1
            int r5 = r0 >> 1
            byte[] r4 = new byte[r5]
            r6 = 0
        L_0x000d:
            r8 = 1
            if (r6 >= r7) goto L_0x0044
            byte r1 = r10[r6]
            r0 = 251(0xfb, float:3.52E-43)
            if (r11 == r0) goto L_0x0021
            r0 = 255(0xff, float:3.57E-43)
            if (r11 != r0) goto L_0x001d
            switch(r1) {
                case 45: goto L_0x001e;
                case 46: goto L_0x001e;
                case 47: goto L_0x001d;
                case 48: goto L_0x0028;
                case 49: goto L_0x0028;
                case 50: goto L_0x0028;
                case 51: goto L_0x0028;
                case 52: goto L_0x0028;
                case 53: goto L_0x0028;
                case 54: goto L_0x0028;
                case 55: goto L_0x0028;
                case 56: goto L_0x0028;
                case 57: goto L_0x0028;
                default: goto L_0x001d;
            }
        L_0x001d:
            return r9
        L_0x001e:
            int r0 = r1 + -45
            goto L_0x002d
        L_0x0021:
            switch(r1) {
                case 48: goto L_0x0028;
                case 49: goto L_0x0028;
                case 50: goto L_0x0028;
                case 51: goto L_0x0028;
                case 52: goto L_0x0028;
                case 53: goto L_0x0028;
                case 54: goto L_0x0028;
                case 55: goto L_0x0028;
                case 56: goto L_0x0028;
                case 57: goto L_0x0028;
                default: goto L_0x0024;
            }
        L_0x0024:
            switch(r1) {
                case 65: goto L_0x002b;
                case 66: goto L_0x002b;
                case 67: goto L_0x002b;
                case 68: goto L_0x002b;
                case 69: goto L_0x002b;
                case 70: goto L_0x002b;
                default: goto L_0x0027;
            }
        L_0x0027:
            return r9
        L_0x0028:
            int r3 = r1 + -48
            goto L_0x002f
        L_0x002b:
            int r0 = r1 + -65
        L_0x002d:
            int r3 = r0 + 10
        L_0x002f:
            r0 = -1
            if (r3 == r0) goto L_0x001d
            int r2 = r6 >> 1
            byte r1 = r4[r2]
            int r0 = r6 % 2
            int r8 = r8 - r0
            int r0 = r8 << 2
            int r3 = r3 << r0
            byte r0 = (byte) r3
            r0 = r0 | r1
            byte r0 = (byte) r0
            r4[r2] = r0
            int r6 = r6 + 1
            goto L_0x000d
        L_0x0044:
            int r0 = r7 % 2
            if (r0 != r8) goto L_0x0050
            int r5 = r5 - r8
            byte r0 = r4[r5]
            r0 = r0 | 15
            byte r0 = (byte) r0
            r4[r5] = r0
        L_0x0050:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass208.A03(byte[], int):byte[]");
    }

    public final void A04(Jid jid, OutputStream outputStream) {
        int agent = jid.getAgent();
        if (agent > 0 || jid.getDevice() > 0) {
            outputStream.write(247);
            A00(outputStream, agent);
            A00(outputStream, jid.getDevice());
            A07(outputStream, jid.user, true, false);
        } else if (jid.isProtocolCompliant()) {
            outputStream.write(250);
            if (TextUtils.isEmpty(jid.user)) {
                outputStream.write((byte) 0);
            } else {
                A07(outputStream, jid.user, true, false);
            }
            A07(outputStream, jid.getServer(), false, false);
        } else {
            StringBuilder sb = new StringBuilder("frame-tree-node-writer/writeJid/failed to write jid: ");
            sb.append(jid);
            Log.e(sb.toString());
            throw new IOException("failed to write jid");
        }
    }

    public void A05(AnonymousClass1V8 r19, int i) {
        boolean z = false;
        if ((i & 1) != 0) {
            z = true;
        }
        boolean z2 = false;
        if ((i & 2) != 0) {
            z2 = true;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(DefaultCrypto.BUFFER_SIZE);
        if (!z2) {
            byteArrayOutputStream.write(0);
        }
        A06(r19, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        if (z2) {
            int length = byteArray.length;
            byte[] bArr = {2};
            try {
                ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream(DefaultCrypto.BUFFER_SIZE);
                DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream2);
                byteArrayOutputStream2.write(bArr);
                deflaterOutputStream.write(byteArray, 0, length);
                deflaterOutputStream.close();
                byteArray = byteArrayOutputStream2.toByteArray();
                deflaterOutputStream.close();
                byteArrayOutputStream2.close();
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
        int length2 = byteArray.length;
        if (length2 > 16777216 && this.A01.A07(1151)) {
            try {
                String str = r19.A00;
                String A0I = r19.A0I("type", "null");
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("(type=");
                sb.append(A0I);
                sb.append(")");
                String obj = sb.toString();
                AnonymousClass1V8 A0D = r19.A0D(0);
                if (A0D != null) {
                    String str2 = A0D.A00;
                    String A0I2 = A0D.A0I("type", "null");
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(obj);
                    sb2.append(", ");
                    sb2.append(str2);
                    sb2.append("(type=");
                    sb2.append(A0I2);
                    sb2.append(")");
                    obj = sb2.toString();
                }
                int i2 = (length2 / EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) / EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(obj);
                sb3.append(", size: ");
                sb3.append(i2);
                this.A00.AaV("message-exceed-16mb-v2", sb3.toString(), true);
            } catch (Exception e2) {
                Log.e("FrameTreeNodeWriter.logMessageExceeds16Mb", e2);
            }
        }
        AnonymousClass209 r2 = this.A03;
        AnonymousClass20P r0 = r2.A01;
        byte[] A00 = r0.A04.A00(new byte[0], byteArray, length2, r0.A01.getAndIncrement());
        OutputStream outputStream = r2.A00;
        outputStream.write(A00);
        if (z) {
            outputStream.flush();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r6.length <= 0) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(X.AnonymousClass1V8 r11, java.io.OutputStream r12) {
        /*
            r10 = this;
            X.1V8[] r6 = r11.A03
            r5 = 0
            r8 = 1
            if (r6 == 0) goto L_0x000a
            int r0 = r6.length
            r2 = 1
            if (r0 > 0) goto L_0x000b
        L_0x000a:
            r2 = 0
        L_0x000b:
            byte[] r7 = r11.A01
            if (r7 == 0) goto L_0x0011
            int r2 = r2 + 1
        L_0x0011:
            if (r2 > r8) goto L_0x0067
            X.1W9[] r4 = r11.A0L()
            if (r4 != 0) goto L_0x004b
            r0 = 0
        L_0x001a:
            int r0 = r0 + r8
            int r0 = r0 + r2
            A01(r12, r0)
            java.lang.String r0 = r11.A00
            r10.A07(r12, r0, r5, r8)
            if (r4 == 0) goto L_0x004f
            int r9 = r4.length
            r3 = 0
        L_0x0028:
            if (r3 >= r9) goto L_0x004f
            r2 = r4[r3]
            java.lang.String r0 = r2.A02
            r10.A07(r12, r0, r5, r5)
            com.whatsapp.jid.Jid r1 = r2.A01
            byte r0 = r2.A00
            if (r8 != r0) goto L_0x0045
            if (r1 == 0) goto L_0x0045
            boolean r0 = r1.isProtocolCompliant()
            if (r0 == 0) goto L_0x0045
            r10.A04(r1, r12)
        L_0x0042:
            int r3 = r3 + 1
            goto L_0x0028
        L_0x0045:
            java.lang.String r0 = r2.A03
            r10.A07(r12, r0, r8, r8)
            goto L_0x0042
        L_0x004b:
            int r0 = r4.length
            int r0 = r0 << 1
            goto L_0x001a
        L_0x004f:
            if (r7 == 0) goto L_0x0055
            A02(r12, r7, r5)
        L_0x0054:
            return
        L_0x0055:
            if (r6 == 0) goto L_0x0054
            int r1 = r6.length
            if (r1 <= 0) goto L_0x0054
            A01(r12, r1)
        L_0x005d:
            r0 = r6[r5]
            r10.A06(r0, r12)
            int r5 = r5 + 1
            if (r5 >= r1) goto L_0x0054
            goto L_0x005d
        L_0x0067:
            java.lang.String r1 = "more than one source of inner data for node; countValues="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            java.io.IOException r0 = new java.io.IOException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass208.A06(X.1V8, java.io.OutputStream):void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:(4:(3:12|(2:14|(1:16)(2:22|(1:24)(2:25|(1:27))))(1:28)|(2:20|21))|35|36|37)(0)|56|34|36|37) */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x005d, code lost:
        if (r5 != null) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A07(java.io.OutputStream r4, java.lang.String r5, boolean r6, boolean r7) {
        /*
            r3 = this;
            boolean r0 = r3 instanceof X.AnonymousClass20W
            if (r0 != 0) goto L_0x0053
            java.util.Map r0 = X.AnonymousClass20V.A00
            if (r0 != 0) goto L_0x000b
            X.AnonymousClass20V.A00()
        L_0x000b:
            java.util.Map r0 = X.AnonymousClass20V.A00
        L_0x000d:
            java.lang.Object r2 = r0.get(r5)
            X.20X r2 = (X.AnonymousClass20X) r2
            if (r2 != 0) goto L_0x006b
            if (r7 == 0) goto L_0x005d
            if (r5 == 0) goto L_0x0066
            r0 = 64
            int r1 = r5.indexOf(r0)
            r0 = 1
            if (r1 >= r0) goto L_0x004e
            java.lang.String r0 = "s.whatsapp.net"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0038
            X.1VY r1 = X.AnonymousClass1VY.A00
        L_0x002c:
            if (r1 == 0) goto L_0x005f
            boolean r0 = r1.isProtocolCompliant()
            if (r0 == 0) goto L_0x005f
            r3.A04(r1, r4)
            return
        L_0x0038:
            java.lang.String r0 = "g.us"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0043
            X.1Vn r1 = X.C29991Vn.A00
            goto L_0x002c
        L_0x0043:
            java.lang.String r0 = "call"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x005f
            X.1Vo r1 = X.C30001Vo.A00
            goto L_0x002c
        L_0x004e:
            com.whatsapp.jid.Jid r1 = com.whatsapp.jid.Jid.getNullable(r5)
            goto L_0x002c
        L_0x0053:
            java.util.Map r0 = X.AnonymousClass20V.A01
            if (r0 != 0) goto L_0x005a
            X.AnonymousClass20V.A00()
        L_0x005a:
            java.util.Map r0 = X.AnonymousClass20V.A01
            goto L_0x000d
        L_0x005d:
            if (r5 == 0) goto L_0x0066
        L_0x005f:
            java.lang.String r0 = X.AnonymousClass01V.A08     // Catch: UnsupportedEncodingException -> 0x0066
            byte[] r0 = r5.getBytes(r0)     // Catch: UnsupportedEncodingException -> 0x0066
            goto L_0x0067
        L_0x0066:
            r0 = 0
        L_0x0067:
            A02(r4, r0, r6)
            return
        L_0x006b:
            boolean r0 = r2.A02
            if (r0 == 0) goto L_0x007b
            short r1 = r2.A01
            if (r1 < 0) goto L_0x0090
            r0 = 255(0xff, float:3.57E-43)
            if (r1 > r0) goto L_0x0090
            byte r0 = (byte) r1
            r4.write(r0)
        L_0x007b:
            short r1 = r2.A00
            if (r1 < 0) goto L_0x0088
            r0 = 255(0xff, float:3.57E-43)
            if (r1 > r0) goto L_0x0088
            byte r0 = (byte) r1
            r4.write(r0)
            return
        L_0x0088:
            java.lang.String r1 = "invalid token"
            java.io.IOException r0 = new java.io.IOException
            r0.<init>(r1)
            throw r0
        L_0x0090:
            java.lang.String r1 = "invalid token"
            java.io.IOException r0 = new java.io.IOException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass208.A07(java.io.OutputStream, java.lang.String, boolean, boolean):void");
    }
}
