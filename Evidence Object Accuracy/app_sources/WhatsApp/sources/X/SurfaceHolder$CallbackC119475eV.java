package X;

import android.content.Context;
import android.hardware.Camera;
import android.view.Display;
import android.view.SurfaceHolder;

/* renamed from: X.5eV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class SurfaceHolder$CallbackC119475eV extends AbstractC119655eo implements SurfaceHolder.Callback {
    public int A00;
    public int A01;
    public Camera A02;
    public final SurfaceHolder A03;

    @Override // X.AnonymousClass27X, android.view.SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
    }

    public SurfaceHolder$CallbackC119475eV(Context context) {
        super(context);
        SurfaceHolder holder = getHolder();
        this.A03 = holder;
        holder.addCallback(this);
    }

    @Override // X.AnonymousClass27X, X.AnonymousClass1s9
    public void Aap() {
        A0A(this.A03);
    }

    public int getDisplayOrientation() {
        Display defaultDisplay = AnonymousClass01d.A02(getContext()).getDefaultDisplay();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(((AnonymousClass27X) this).A00, cameraInfo);
        int rotation = defaultDisplay.getRotation();
        int i = 0;
        boolean A1V = C12960it.A1V(cameraInfo.facing, 1);
        int i2 = cameraInfo.orientation;
        if (rotation != 0) {
            if (rotation == 1) {
                i = 90;
            } else if (rotation == 2) {
                i = 180;
            } else if (rotation == 3) {
                i = 270;
            }
        }
        int i3 = (i2 - i) + 360;
        if (A1V) {
            i3 = 360 - ((i2 + i) % 360);
        }
        int i4 = i3 % 360;
        StringBuilder A0k = C12960it.A0k("bloks_camera/startpreview display:");
        A0k.append(i);
        A0k.append(" camera:");
        A0k.append(i2);
        A0k.append(" preview:");
        A0k.append(i4);
        A0k.append(" front:");
        A0k.append(A1V);
        C12960it.A1F(A0k);
        return i4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006b, code lost:
        if (r5.contains(r2) != false) goto L_0x006d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00bb A[Catch: all -> 0x0157, TryCatch #0 {, blocks: (B:8:0x0012, B:10:0x0016, B:11:0x001a, B:13:0x0021, B:14:0x0026, B:16:0x0035, B:18:0x0055, B:20:0x005d, B:22:0x0065, B:24:0x006d, B:25:0x0070, B:27:0x0074, B:29:0x0078, B:32:0x0086, B:33:0x008c, B:34:0x008f, B:36:0x009c, B:37:0x009e, B:39:0x00a2, B:40:0x00b5, B:42:0x00bb, B:44:0x00d6, B:48:0x00e6, B:49:0x00ef, B:51:0x00f5, B:55:0x0111, B:56:0x0117, B:57:0x0141, B:58:0x0146, B:60:0x0152), top: B:67:0x0012, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00e6 A[Catch: all -> 0x0157, TryCatch #0 {, blocks: (B:8:0x0012, B:10:0x0016, B:11:0x001a, B:13:0x0021, B:14:0x0026, B:16:0x0035, B:18:0x0055, B:20:0x005d, B:22:0x0065, B:24:0x006d, B:25:0x0070, B:27:0x0074, B:29:0x0078, B:32:0x0086, B:33:0x008c, B:34:0x008f, B:36:0x009c, B:37:0x009e, B:39:0x00a2, B:40:0x00b5, B:42:0x00bb, B:44:0x00d6, B:48:0x00e6, B:49:0x00ef, B:51:0x00f5, B:55:0x0111, B:56:0x0117, B:57:0x0141, B:58:0x0146, B:60:0x0152), top: B:67:0x0012, inners: #1, #2 }] */
    @Override // X.AnonymousClass27X, android.view.SurfaceHolder.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void surfaceChanged(android.view.SurfaceHolder r22, int r23, int r24, int r25) {
        /*
        // Method dump skipped, instructions count: 347
        */
        throw new UnsupportedOperationException("Method not decompiled: X.SurfaceHolder$CallbackC119475eV.surfaceChanged(android.view.SurfaceHolder, int, int, int):void");
    }
}
