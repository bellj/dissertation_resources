package X;

import java.util.List;

/* renamed from: X.4S8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4S8 {
    public final int A00;
    public final AbstractC15340mz A01;
    public final List A02;
    public final C30721Yo A03;

    public AnonymousClass4S8(AbstractC15340mz r1, List list, C30721Yo r3, int i) {
        this.A01 = r1;
        this.A03 = r3;
        this.A00 = i;
        this.A02 = list;
    }
}
