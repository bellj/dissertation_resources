package X;

import android.os.Bundle;
import android.util.Log;
import java.io.IOException;
import java.util.concurrent.Executor;

/* renamed from: X.0jq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13510jq {
    public final C13030j1 A00;
    public final C13430ji A01;
    public final C13520jr A02;
    public final C13130jC A03;
    public final Executor A04;

    public C13510jq(C13030j1 r3, C13430ji r4, C13130jC r5, Executor executor) {
        r3.A02();
        C13520jr r0 = new C13520jr(r3.A00, r4);
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r0;
        this.A04 = executor;
        this.A03 = r5;
    }

    public static /* synthetic */ String A00(Bundle bundle) {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null || (string = bundle.getString("unregistered")) != null) {
                return string;
            }
            String string2 = bundle.getString("error");
            if ("RST".equals(string2)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string2 != null) {
                throw new IOException(string2);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(valueOf.length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:94:0x0174 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:58:0x0174 */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0174, code lost:
        if (r7 == null) goto L_0x0176;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C13600jz A01(android.os.Bundle r12, java.lang.String r13, java.lang.String r14, java.lang.String r15) {
        /*
        // Method dump skipped, instructions count: 490
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13510jq.A01(android.os.Bundle, java.lang.String, java.lang.String, java.lang.String):X.0jz");
    }

    public final C13600jz A02(C13600jz r6) {
        Executor executor = C88444Fr.A00;
        C1091550p r3 = new C1091550p();
        C13600jz r2 = new C13600jz();
        r6.A03.A00(new AnonymousClass513(r3, r2, executor));
        r6.A04();
        return r2;
    }

    public final C13600jz A03(C13600jz r6) {
        Executor executor = this.A04;
        AnonymousClass3TM r3 = new AnonymousClass3TM(this);
        C13600jz r2 = new C13600jz();
        r6.A03.A00(new AnonymousClass513(r3, r2, executor));
        r6.A04();
        return r2;
    }

    public final C13600jz A04(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        bundle.putString("delete", "1");
        return A02(A03(A01(bundle, str, str2, str3)));
    }

    public final C13600jz A05(String str, String str2, String str3) {
        String str4;
        String str5;
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        if (valueOf.length() != 0) {
            str4 = "/topics/".concat(valueOf);
        } else {
            str4 = new String("/topics/");
        }
        bundle.putString("gcm.topic", str4);
        String valueOf2 = String.valueOf(str3);
        if (valueOf2.length() != 0) {
            str5 = "/topics/".concat(valueOf2);
        } else {
            str5 = new String("/topics/");
        }
        return A02(A03(A01(bundle, str, str2, str5)));
    }

    public final C13600jz A06(String str, String str2, String str3) {
        String str4;
        String str5;
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        if (valueOf.length() != 0) {
            str4 = "/topics/".concat(valueOf);
        } else {
            str4 = new String("/topics/");
        }
        bundle.putString("gcm.topic", str4);
        bundle.putString("delete", "1");
        String valueOf2 = String.valueOf(str3);
        if (valueOf2.length() != 0) {
            str5 = "/topics/".concat(valueOf2);
        } else {
            str5 = new String("/topics/");
        }
        return A02(A03(A01(bundle, str, str2, str5)));
    }
}
