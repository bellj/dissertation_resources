package X;

import java.util.List;

/* renamed from: X.19L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19L {
    public final C47432Ar A00;

    public AnonymousClass19L(C14900mE r2, AnonymousClass01d r3, AnonymousClass018 r4, AbstractC14440lR r5) {
        this.A00 = new C47432Ar(r2, r3, r4, r5);
    }

    public void A00() {
        C47432Ar r5 = this.A00;
        AnonymousClass009.A01();
        List<AnonymousClass21S> list = r5.A07;
        list.size();
        List<AnonymousClass21S> list2 = r5.A06;
        list2.size();
        for (AnonymousClass21S r2 : list2) {
            C47442As r0 = r2.A0B;
            if (r0 != null) {
                C47412Ap r1 = r0.A00;
                r1.A1R();
                r1.A1S(false);
            }
            r2.A08();
        }
        list2.clear();
        for (AnonymousClass21S r22 : list) {
            C47442As r02 = r22.A0B;
            if (r02 != null) {
                C47412Ap r12 = r02.A00;
                r12.A1R();
                r12.A1S(false);
            }
            r22.A08();
        }
        list.clear();
        r5.A00 = 0;
    }
}
