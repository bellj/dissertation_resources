package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.4id  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98404id implements IInterface {
    public final IBinder A00;
    public final String A01;

    public C98404id(IBinder iBinder, String str) {
        this.A00 = iBinder;
        this.A01 = str;
    }

    public final Parcel A00(Parcel parcel) {
        try {
            parcel = Parcel.obtain();
            C12990iw.A18(this.A00, parcel, parcel, 1);
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }
}
