package X;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.calling.callgrid.view.PipViewContainer;

/* renamed from: X.3K3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3K3 implements ValueAnimator.AnimatorUpdateListener {
    public int A00;
    public int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ int A03;
    public final /* synthetic */ View A04;
    public final /* synthetic */ PipViewContainer A05;

    public AnonymousClass3K3(View view, PipViewContainer pipViewContainer, int i, int i2) {
        this.A05 = pipViewContainer;
        this.A04 = view;
        this.A02 = i;
        this.A03 = i2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        int i;
        int i2;
        float animatedFraction = valueAnimator.getAnimatedFraction();
        PipViewContainer pipViewContainer = this.A05;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(pipViewContainer);
        AnonymousClass018 r6 = pipViewContainer.A07;
        AnonymousClass009.A05(r6);
        if (animatedFraction == 0.0f) {
            this.A01 = A0H.topMargin;
            if (C28141Kv.A01(r6)) {
                i2 = A0H.leftMargin;
            } else {
                i2 = A0H.rightMargin;
            }
            this.A00 = i2;
            pipViewContainer.getWidth();
            pipViewContainer.getHeight();
        }
        View view = this.A04;
        int i3 = ((int) (((float) this.A02) * animatedFraction)) + this.A00;
        int i4 = A0H.topMargin;
        if (C28141Kv.A01(r6)) {
            i = A0H.rightMargin;
        } else {
            i = A0H.leftMargin;
        }
        C42941w9.A09(view, r6, i3, i4, i, A0H.bottomMargin);
        ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(pipViewContainer);
        A0H2.topMargin = this.A01 + ((int) (((float) this.A03) * animatedFraction));
        pipViewContainer.setLayoutParams(A0H2);
    }
}
