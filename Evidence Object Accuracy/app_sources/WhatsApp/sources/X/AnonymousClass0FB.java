package X;

import android.view.View;
import android.widget.Scroller;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0FB  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0FB extends AnonymousClass0LA {
    public Scroller A00;
    public RecyclerView A01;
    public final AbstractC05270Ox A02 = new AnonymousClass0FD(this);

    public abstract int A00(AnonymousClass02H v, int i, int i2);

    public abstract View A01(AnonymousClass02H v);

    public abstract int[] A04(View view, AnonymousClass02H v);

    @Deprecated
    public AnonymousClass0FE A02(AnonymousClass02H r3) {
        if (!(r3 instanceof AnonymousClass02I)) {
            return null;
        }
        return new AnonymousClass0Ex(this.A01.getContext(), this);
    }

    public void A03() {
        AnonymousClass02H layoutManager;
        View A01;
        RecyclerView recyclerView = this.A01;
        if (recyclerView != null && (layoutManager = recyclerView.getLayoutManager()) != null && (A01 = A01(layoutManager)) != null) {
            int[] A04 = A04(A01, layoutManager);
            int i = A04[0];
            if (i != 0 || A04[1] != 0) {
                this.A01.A0d(i, A04[1]);
            }
        }
    }
}
