package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.work.impl.utils.ForceStopRunnable$BroadcastReceiver;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0eJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10270eJ implements Runnable {
    public static final long A03 = TimeUnit.DAYS.toMillis(3650);
    public static final String A04 = C06390Tk.A01("ForceStopRunnable");
    public int A00 = 0;
    public final Context A01;
    public final AnonymousClass022 A02;

    public RunnableC10270eJ(Context context, AnonymousClass022 r3) {
        this.A01 = context.getApplicationContext();
        this.A02 = r3;
    }

    public static void A00(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        int i = 134217728;
        if (C04070Kg.A00()) {
            i = 167772160;
        }
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, ForceStopRunnable$BroadcastReceiver.class));
        intent.setAction("ACTION_FORCE_STOP_RESCHEDULE");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, -1, intent, i);
        long currentTimeMillis = System.currentTimeMillis() + A03;
        if (alarmManager == null) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, currentTimeMillis, broadcast);
        } else {
            alarmManager.set(0, currentTimeMillis, broadcast);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01cc, code lost:
        if (r3 == false) goto L_0x01ce;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 492
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC10270eJ.run():void");
    }
}
