package X;

/* renamed from: X.2Lw  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2Lw extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public AnonymousClass2Lw() {
        super(C49692Lu.A05);
    }

    public void A05(AnonymousClass2Lx r3) {
        A03();
        C49692Lu r1 = (C49692Lu) this.A00;
        r1.A04 = r3.A02();
        r1.A01 = 2;
    }

    public void A06(AnonymousClass2Lx r3) {
        A03();
        C49692Lu r1 = (C49692Lu) this.A00;
        r1.A03 = (AnonymousClass2Ly) r3.A02();
        r1.A00 |= 8;
    }
}
