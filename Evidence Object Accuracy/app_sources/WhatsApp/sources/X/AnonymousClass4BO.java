package X;

/* renamed from: X.4BO  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BO {
    AND(0),
    NOT(1),
    OR(2);
    
    public final String operatorString;

    AnonymousClass4BO(int i) {
        this.operatorString = r2;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.operatorString;
    }
}
