package X;

import java.util.List;

/* renamed from: X.5H0  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5H0 extends RuntimeException {
    public final List zza = null;

    public AnonymousClass5H0() {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
