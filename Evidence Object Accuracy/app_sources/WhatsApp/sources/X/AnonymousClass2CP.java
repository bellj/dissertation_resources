package X;

import android.content.Context;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.view.Surface;
import android.widget.MediaController;

/* renamed from: X.2CP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CP extends AnonymousClass2CQ implements MediaController.MediaPlayerControl {
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = -1;
    public int A03 = 0;
    public int A04;
    public int A05;
    public MediaPlayer.OnCompletionListener A06;
    public MediaPlayer.OnErrorListener A07;
    public MediaPlayer.OnPreparedListener A08;
    public MediaPlayer A09;
    public Surface A0A;
    public String A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final Matrix A0I = new Matrix();
    public final /* synthetic */ AnonymousClass39E A0J;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2CP(Context context, AnonymousClass39E r4) {
        super(context);
        this.A0J = r4;
        A00();
    }

    public void A00() {
        MediaPlayer mediaPlayer = new MediaPlayer();
        this.A09 = mediaPlayer;
        mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() { // from class: X.4iG
            @Override // android.media.MediaPlayer.OnVideoSizeChangedListener
            public final void onVideoSizeChanged(MediaPlayer mediaPlayer2, int i, int i2) {
                AnonymousClass2CP r6 = AnonymousClass2CP.this;
                r6.A05 = i;
                r6.A04 = i2;
                if (!(i == 0 || i2 == 0)) {
                    int width = r6.getWidth();
                    int height = r6.getHeight();
                    int i3 = r6.A05;
                    int i4 = i3 * height;
                    int i5 = r6.A04;
                    int i6 = i5 * width;
                    if (i4 > i6) {
                        height = i6 / i3;
                    } else {
                        width = i4 / i5;
                    }
                    int width2 = r6.getWidth();
                    r6.setTop((r6.getHeight() - height) >> 1);
                    r6.setBottom(r6.getTop() + height);
                    r6.setLeft((width2 - width) >> 1);
                    r6.setRight(r6.getLeft() + width);
                }
                r6.requestLayout();
            }
        });
        this.A09.setOnErrorListener(new MediaPlayer.OnErrorListener() { // from class: X.4i9
            @Override // android.media.MediaPlayer.OnErrorListener
            public final boolean onError(MediaPlayer mediaPlayer2, int i, int i2) {
                AnonymousClass2CP r2 = AnonymousClass2CP.this;
                r2.A00 = -1;
                r2.A03 = -1;
                MediaPlayer.OnErrorListener onErrorListener = r2.A07;
                if (onErrorListener == null) {
                    return true;
                }
                onErrorListener.onError(r2.A09, i, i2);
                return true;
            }
        });
        this.A09.setOnPreparedListener(new MediaPlayer.OnPreparedListener() { // from class: X.4iE
            @Override // android.media.MediaPlayer.OnPreparedListener
            public final void onPrepared(MediaPlayer mediaPlayer2) {
                AnonymousClass2CP r2 = AnonymousClass2CP.this;
                r2.A00 = 2;
                if (r2.A0G) {
                    mediaPlayer2.setVolume(0.0f, 0.0f);
                }
                if (r2.A0F) {
                    mediaPlayer2.setLooping(true);
                }
                r2.A0E = true;
                r2.A0D = true;
                r2.A0C = true;
                int i = r2.A02;
                if (i >= 0) {
                    r2.seekTo(i);
                }
                if (r2.A03 == 3) {
                    r2.start();
                }
                MediaPlayer.OnPreparedListener onPreparedListener = r2.A08;
                if (onPreparedListener != null) {
                    onPreparedListener.onPrepared(mediaPlayer2);
                }
            }
        });
        this.A09.setOnCompletionListener(new MediaPlayer.OnCompletionListener() { // from class: X.4i5
            @Override // android.media.MediaPlayer.OnCompletionListener
            public final void onCompletion(MediaPlayer mediaPlayer2) {
                AnonymousClass2CP r2 = AnonymousClass2CP.this;
                r2.A00 = 5;
                r2.A03 = 5;
                MediaPlayer.OnCompletionListener onCompletionListener = r2.A06;
                if (onCompletionListener != null) {
                    onCompletionListener.onCompletion(r2.A09);
                }
            }
        });
        setSurfaceTextureListener(new AnonymousClass2CO(this));
    }

    public boolean A01() {
        int i;
        return (this.A09 == null || (i = this.A00) == -1 || i == 0 || i == 1) ? false : true;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean canPause() {
        return this.A0C;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean canSeekBackward() {
        return this.A0D;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean canSeekForward() {
        return this.A0E;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getAudioSessionId() {
        AnonymousClass009.A0A("Not implemented", false);
        return 0;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getBufferPercentage() {
        AnonymousClass009.A0A("Not implemented", false);
        return 0;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getCurrentPosition() {
        if (!A01()) {
            return 0;
        }
        MediaPlayer mediaPlayer = this.A09;
        AnonymousClass009.A05(mediaPlayer);
        return mediaPlayer.getCurrentPosition();
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public int getDuration() {
        if (!A01()) {
            return -1;
        }
        MediaPlayer mediaPlayer = this.A09;
        AnonymousClass009.A05(mediaPlayer);
        return mediaPlayer.getDuration();
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public boolean isPlaying() {
        if (A01()) {
            MediaPlayer mediaPlayer = this.A09;
            AnonymousClass009.A05(mediaPlayer);
            if (mediaPlayer.isPlaying()) {
                return true;
            }
        }
        return false;
    }

    @Override // android.view.TextureView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        MediaPlayer mediaPlayer = this.A09;
        if (mediaPlayer != null && this.A00 == 4) {
            mediaPlayer.start();
        }
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        MediaPlayer mediaPlayer = this.A09;
        if (mediaPlayer != null && this.A00 == 3) {
            mediaPlayer.pause();
        }
    }

    @Override // android.view.View
    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        MediaPlayer mediaPlayer = this.A09;
        if (mediaPlayer != null && this.A00 == 4) {
            mediaPlayer.start();
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        float f;
        super.onMeasure(i, i2);
        if (this.A05 != 0 && this.A04 != 0) {
            int i3 = this.A01;
            int measuredWidth = getMeasuredWidth();
            int measuredHeight = getMeasuredHeight();
            if (i3 != 1) {
                int i4 = this.A05;
                int i5 = i4 * measuredHeight;
                int i6 = this.A04;
                int i7 = i6 * measuredWidth;
                if (i5 > i7) {
                    measuredHeight = i7 / i4;
                } else {
                    measuredWidth = i5 / i6;
                }
                setMeasuredDimension(measuredWidth, measuredHeight);
                return;
            }
            Matrix matrix = this.A0I;
            matrix.reset();
            int i8 = this.A05;
            int i9 = i8 * measuredHeight;
            int i10 = this.A04;
            int i11 = i10 * measuredWidth;
            float f2 = 1.0f;
            if (i9 > i11) {
                f2 = (((float) i8) * ((float) measuredHeight)) / ((float) i11);
                f = 1.0f;
            } else {
                f = (((float) i10) * ((float) measuredWidth)) / ((float) i9);
            }
            matrix.setScale(f2, f, (float) (measuredWidth >> 1), (float) (measuredHeight >> 1));
            setTransform(matrix);
        }
    }

    @Override // android.view.View
    public void onStartTemporaryDetach() {
        super.onStartTemporaryDetach();
        MediaPlayer mediaPlayer = this.A09;
        if (mediaPlayer != null && this.A00 == 3) {
            mediaPlayer.pause();
        }
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public void pause() {
        if (A01()) {
            MediaPlayer mediaPlayer = this.A09;
            AnonymousClass009.A05(mediaPlayer);
            if (mediaPlayer.isPlaying()) {
                this.A09.pause();
                this.A00 = 4;
            }
        }
        this.A03 = 4;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public void seekTo(int i) {
        if (A01()) {
            MediaPlayer mediaPlayer = this.A09;
            AnonymousClass009.A05(mediaPlayer);
            mediaPlayer.seekTo(i);
            i = -1;
        }
        this.A02 = i;
    }

    public void setLooping(boolean z) {
        this.A0F = z;
        MediaPlayer mediaPlayer = this.A09;
        if (mediaPlayer != null) {
            mediaPlayer.setLooping(z);
        }
    }

    public void setMute(boolean z) {
        this.A0G = z;
        MediaPlayer mediaPlayer = this.A09;
        if (mediaPlayer != null) {
            float f = 1.0f;
            if (z) {
                f = 0.0f;
            }
            mediaPlayer.setVolume(f, f);
        }
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.A06 = onCompletionListener;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener) {
        this.A07 = onErrorListener;
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.A08 = onPreparedListener;
    }

    public void setScaleType(int i) {
        int i2 = this.A01;
        this.A01 = i;
        if (i2 != i) {
            requestLayout();
        }
    }

    public void setVideoPath(String str) {
        this.A0B = str;
    }

    @Override // android.widget.MediaController.MediaPlayerControl
    public void start() {
        AnonymousClass39E r1;
        AnonymousClass5V4 r0;
        if (A01() && (r0 = (r1 = this.A0J).A03) != null) {
            r0.AWJ(r1);
        }
        boolean A01 = A01();
        MediaPlayer mediaPlayer = this.A09;
        if (A01) {
            AnonymousClass009.A05(mediaPlayer);
            mediaPlayer.start();
            this.A00 = 3;
        } else if (mediaPlayer == null) {
            A00();
        }
        this.A03 = 3;
    }
}
