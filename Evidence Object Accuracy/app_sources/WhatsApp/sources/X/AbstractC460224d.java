package X;

import android.os.Parcelable;

/* renamed from: X.24d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC460224d implements Parcelable {
    public String A00;
    public final String A01;

    public AbstractC460224d(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }
}
