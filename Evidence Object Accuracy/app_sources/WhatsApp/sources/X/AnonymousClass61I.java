package X;

/* renamed from: X.61I  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61I {
    public static AnonymousClass3FW A00(C14830m7 r7, C30821Yy r8, AnonymousClass2S0 r9, String str, boolean z) {
        AnonymousClass3FW[] r0;
        C50942Ry r6;
        int A00;
        if (r9 == null || (r6 = r9.A01) == null || (A00 = r9.A00(C117295Zj.A03(r7))) == 0 || A00 == 3) {
            r0 = new AnonymousClass3FW[0];
        } else {
            C121045hA r5 = new C121045hA();
            r5.A02("is_ended_early", C12960it.A1V(A00, 4));
            if (z) {
                r5.A02("is_sender_receiver_eligible", C12960it.A1T(r9.A00));
            }
            C30821Yy r02 = r6.A09.A00.A02;
            if (r8 != null) {
                boolean z2 = false;
                if (r8.A00.compareTo(r02.A00) < 0) {
                    z2 = true;
                }
                r5.A02("is_amount_low", z2);
            }
            r0 = new AnonymousClass3FW[]{r5};
        }
        AnonymousClass3FW r1 = new AnonymousClass3FW(null, r0);
        if (str != null) {
            r1.A01("section", str);
        }
        if (r1.A01.length() > 0) {
            return r1;
        }
        return null;
    }

    public static void A01(AnonymousClass3FW r2, AbstractC16870pt r3, Integer num, String str, String str2, int i) {
        AnonymousClass009.A05(r3);
        if (r2 != null) {
            r3.AKi(r2, Integer.valueOf(i), num, str, str2);
        } else {
            r3.AKg(Integer.valueOf(i), num, str, str2);
        }
    }

    public static void A02(AnonymousClass3FW r6, AbstractC16870pt r7, String str) {
        A01(r6, r7, 51, str, null, 4);
    }

    public static void A03(AnonymousClass3FW r6, AbstractC16870pt r7, String str, String str2) {
        A01(r6, r7, null, str, str2, 0);
    }
}
