package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.util.Log;

/* renamed from: X.3Zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69393Zb implements AnonymousClass5WE {
    public final /* synthetic */ AcceptInviteLinkActivity A00;

    public C69393Zb(AcceptInviteLinkActivity acceptInviteLinkActivity) {
        this.A00 = acceptInviteLinkActivity;
    }

    @Override // X.AnonymousClass5WE
    public void AUJ(AbstractC14640lm r2, String str, int i, long j) {
        Log.w(C12960it.A0W(i, "acceptlink/failed-to-get-group-photo/"));
    }

    @Override // X.AnonymousClass5WE
    public void AUK(C41831uE r6, long j) {
        byte[] bArr = r6.A00;
        if (bArr != null) {
            AcceptInviteLinkActivity acceptInviteLinkActivity = this.A00;
            ((ActivityC13810kN) acceptInviteLinkActivity).A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(acceptInviteLinkActivity, 33, bArr));
        }
    }
}
