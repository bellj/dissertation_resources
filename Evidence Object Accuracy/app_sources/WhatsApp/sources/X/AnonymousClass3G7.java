package X;

import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.3G7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3G7 {
    public static Object A00(AnonymousClass5X9 r4) {
        AnonymousClass39n AZ7 = r4.AZ7();
        AnonymousClass39n r1 = AnonymousClass39n.A0A;
        if (AZ7 != r1) {
            AnonymousClass39n r0 = AnonymousClass39n.A09;
            if (AZ7 == r0) {
                ArrayList A0l = C12960it.A0l();
                if (AZ7 == r0) {
                    while (r4.ALh() != AnonymousClass39n.A02) {
                        A0l.add(A00(r4));
                    }
                }
                return A0l;
            } else if (AZ7 == AnonymousClass39n.A07) {
                return null;
            } else {
                if (AZ7 == AnonymousClass39n.A01) {
                    return Boolean.valueOf(r4.AZ8().A6g());
                }
                if (AZ7 == AnonymousClass39n.A08) {
                    Number ALj = r4.AZ8().ALj();
                    if (ALj instanceof Long) {
                        return Long.valueOf(ALj.longValue());
                    }
                    return Double.valueOf(ALj.doubleValue());
                } else if (AZ7 == AnonymousClass39n.A0B) {
                    return r4.AZ8().AeX();
                } else {
                    throw C12960it.A0U(C12960it.A0b("unsupported token type ", AZ7));
                }
            }
        } else if (r4.AZ7() != r1) {
            return null;
        } else {
            HashMap A11 = C12970iu.A11();
            while (r4.ALh() != AnonymousClass39n.A04) {
                String AZ6 = r4.AZ6();
                r4.ALh();
                A11.put(AZ6, A00(r4));
            }
            return A11;
        }
    }
}
