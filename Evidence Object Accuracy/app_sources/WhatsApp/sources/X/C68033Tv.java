package X;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import java.util.concurrent.TimeUnit;

/* renamed from: X.3Tv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68033Tv implements AbstractC116635Wf {
    public int A00;
    public View A01;
    public TextView A02;
    public TextEmojiLabel A03;
    public WaImageView A04;
    public final Context A05;
    public final AnonymousClass12P A06;
    public final C53042cM A07;
    public final C252818u A08;
    public final AnonymousClass1D6 A09;
    public final C14820m6 A0A;
    public final AnonymousClass018 A0B;
    public final C16120oU A0C;

    public C68033Tv(Context context, AnonymousClass12P r2, C53042cM r3, C252818u r4, AnonymousClass1D6 r5, C14820m6 r6, AnonymousClass018 r7, C16120oU r8) {
        this.A05 = context;
        this.A0C = r8;
        this.A06 = r2;
        this.A08 = r4;
        this.A0B = r7;
        this.A07 = r3;
        this.A09 = r5;
        this.A0A = r6;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A01);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        AnonymousClass1D6 r5 = this.A09;
        if (r5.A00() == 1) {
            long currentTimeMillis = System.currentTimeMillis();
            C14820m6 r7 = r5.A05;
            if (currentTimeMillis >= C12980iv.A0E(r7.A00, "backup_quota_imposed_timestamp") + TimeUnit.DAYS.toMillis(7)) {
                r7.A0R(3);
                int A00 = r5.A00();
                this.A00 = A00;
                return C12960it.A1S(A00);
            }
        }
        C14820m6 r2 = r5.A05;
        if (C12970iu.A01(r2.A00, "gdrive_backup_quota_warning_visibility") == 2) {
            r2.A0R(1);
        }
        int A00 = r5.A00();
        this.A00 = A00;
        return C12960it.A1S(A00);
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        WaImageView waImageView;
        int i;
        String A0I;
        if (this.A01 == null) {
            C53042cM r2 = this.A07;
            View A0F = C12960it.A0F(C12960it.A0E(r2), r2, R.layout.backup_quota_banner);
            this.A01 = A0F;
            C12960it.A10(A0F, this, 30);
            C12960it.A10(AnonymousClass028.A0D(this.A01, R.id.dismiss_backup_quota_banner_container), this, 29);
            this.A02 = C12960it.A0I(this.A01, R.id.backup_quota_banner_title);
            this.A03 = C12970iu.A0T(this.A01, R.id.backup_quota_banner_message);
            this.A04 = C12980iv.A0X(this.A01, R.id.backup_quota_banner_icon);
            r2.addView(this.A01);
        }
        View view = this.A01;
        if (view != null) {
            if (!(this.A02 == null || this.A03 == null)) {
                String A05 = AnonymousClass1US.A05(view.getContext(), R.color.banner_bolded_text);
                C14820m6 r22 = this.A0A;
                AnonymousClass009.A05(r22.A09());
                AnonymousClass018 r11 = this.A0B;
                String str = (String) C44891zj.A00(r11, r22.A08(r22.A09()), false, false).first;
                if (str != null) {
                    str = str.replace(' ', (char) 160);
                }
                int i2 = this.A00;
                if (i2 == 1) {
                    this.A02.setText(R.string.gdrive_backup_quota_imposed_title);
                    TextEmojiLabel textEmojiLabel = this.A03;
                    Context context = this.A07.getContext();
                    Object[] objArr = new Object[2];
                    objArr[0] = this.A09.A03();
                    AbstractC28491Nn.A06(textEmojiLabel, C12960it.A0X(context, A05, objArr, 1, R.string.gdrive_backup_quota_imposed_message));
                } else if (i2 == 2) {
                    this.A02.setText(R.string.gdrive_backup_quota_approaching_title);
                    TextEmojiLabel textEmojiLabel2 = this.A03;
                    Context context2 = this.A07.getContext();
                    Object[] objArr2 = new Object[3];
                    objArr2[0] = this.A09.A03();
                    objArr2[1] = str;
                    AbstractC28491Nn.A06(textEmojiLabel2, C12960it.A0X(context2, A05, objArr2, 2, R.string.gdrive_backup_quota_approaching_message));
                } else if (i2 == 3) {
                    long A00 = (long) C38121nY.A00(C12980iv.A0E(r22.A00, "backup_quota_user_notice_period_end_timestamp"), System.currentTimeMillis());
                    if (A00 == 1) {
                        A0I = this.A07.getContext().getString(R.string.gdrive_backup_quota_reached_title_tomorrow);
                    } else {
                        Object[] objArr3 = new Object[1];
                        C12980iv.A1U(objArr3, 0, A00);
                        A0I = r11.A0I(objArr3, R.plurals.gdrive_backup_quota_reached_title, A00);
                    }
                    this.A02.setText(A0I);
                    TextEmojiLabel textEmojiLabel3 = this.A03;
                    Context context3 = this.A07.getContext();
                    Object[] objArr4 = new Object[3];
                    objArr4[0] = this.A09.A03();
                    objArr4[1] = str;
                    AbstractC28491Nn.A06(textEmojiLabel3, C12960it.A0X(context3, A05, objArr4, 2, R.string.gdrive_backup_quota_reached_message));
                    View view2 = this.A01;
                    if (!(view2 == null || this.A04 == null)) {
                        view2.setBackgroundResource(R.color.banner_alert_bg);
                        this.A04.setImageResource(R.drawable.ic_warning);
                        this.A04.setColorFilter(AnonymousClass00T.A00(this.A05, R.color.banner_alert_icon_tint));
                        waImageView = this.A04;
                        i = R.drawable.banner_alert_circle;
                        waImageView.setBackgroundResource(i);
                    }
                } else {
                    throw C12960it.A0U(C12960it.A0W(i2, "Unexpected value: "));
                }
                View view3 = this.A01;
                if (!(view3 == null || this.A04 == null)) {
                    view3.setBackgroundResource(R.color.banner_info_bg);
                    this.A04.setImageResource(R.drawable.ic_backup_small);
                    this.A04.setColorFilter(AnonymousClass00T.A00(this.A05, R.color.banner_info_icon_tint));
                    waImageView = this.A04;
                    i = R.drawable.banner_info_circle;
                    waImageView.setBackgroundResource(i);
                }
            }
            this.A01.setVisibility(0);
            AnonymousClass2KB r1 = new AnonymousClass2KB();
            r1.A02 = Integer.valueOf(this.A00);
            this.A0C.A07(r1);
            this.A07.A00(27, 1);
        }
    }
}
