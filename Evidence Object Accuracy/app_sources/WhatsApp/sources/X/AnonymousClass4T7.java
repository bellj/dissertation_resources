package X;

import java.util.List;

/* renamed from: X.4T7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4T7 {
    public final double A00;
    public final double A01;
    public final double A02;
    public final double A03;
    public final String A04;
    public final List A05;

    public AnonymousClass4T7(String str, List list, double d, double d2, double d3, double d4) {
        this.A04 = str;
        this.A01 = d;
        this.A02 = d2;
        this.A03 = d3;
        this.A05 = list;
        this.A00 = d4;
    }
}
