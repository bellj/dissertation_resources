package X;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.chromium.net.UrlRequest;

/* renamed from: X.14P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14P {
    public final AnonymousClass01d A00;
    public final C15690nk A01;

    public AnonymousClass14P(AnonymousClass01d r1, C15690nk r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static String A00(String str) {
        if (str != null) {
            char c = 65535;
            switch (str.hashCode()) {
                case -1487394660:
                    if (str.equals("image/jpeg")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1487018032:
                    if (str.equals("image/webp")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1248334925:
                    if (str.equals("application/pdf")) {
                        c = 2;
                        break;
                    }
                    break;
                case -1248332507:
                    if (str.equals("application/rtf")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1073633483:
                    if (str.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
                        c = 4;
                        break;
                    }
                    break;
                case -1071817359:
                    if (str.equals("application/vnd.ms-powerpoint")) {
                        c = 5;
                        break;
                    }
                    break;
                case -1050893613:
                    if (str.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                        c = 6;
                        break;
                    }
                    break;
                case -1004747228:
                    if (str.equals("text/csv")) {
                        c = 7;
                        break;
                    }
                    break;
                case -1004732798:
                    if (str.equals("text/rtf")) {
                        c = '\b';
                        break;
                    }
                    break;
                case -879258763:
                    if (str.equals("image/png")) {
                        c = '\t';
                        break;
                    }
                    break;
                case -366307023:
                    if (str.equals("application/vnd.ms-excel")) {
                        c = '\n';
                        break;
                    }
                    break;
                case 817335912:
                    if (str.equals("text/plain")) {
                        c = 11;
                        break;
                    }
                    break;
                case 904647503:
                    if (str.equals("application/msword")) {
                        c = '\f';
                        break;
                    }
                    break;
                case 1993842850:
                    if (str.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        c = '\r';
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    return "jpg";
                case 1:
                    return "webp";
                case 2:
                    return "pdf";
                case 3:
                case '\b':
                    return "rtf";
                case 4:
                    return "pptx";
                case 5:
                    return "ppt";
                case 6:
                    return "docx";
                case 7:
                    return "csv";
                case '\t':
                    return "png";
                case '\n':
                    return "xls";
                case 11:
                    return "txt";
                case '\f':
                    return "doc";
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    return "xlsx";
                default:
                    String A07 = AnonymousClass152.A07(str, false);
                    if (TextUtils.isEmpty(A07)) {
                        A07 = null;
                        if (!TextUtils.isEmpty(str)) {
                            if ("video/mp4".equals(str)) {
                                A07 = "mp4";
                            } else if ("video/3gpp".equals(str)) {
                                A07 = "3gp";
                            }
                        }
                        if (TextUtils.isEmpty(A07)) {
                            String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(str);
                            if (extensionFromMimeType != null) {
                                return extensionFromMimeType;
                            }
                        }
                    }
                    return A07;
            }
        }
        return "";
    }

    public static boolean A01(C14370lK r2) {
        return r2 == C14370lK.A0B || r2 == C14370lK.A0Z || r2 == C14370lK.A0V || r2 == C14370lK.A0R || r2 == C14370lK.A06 || r2 == C14370lK.A0G || r2 == C14370lK.A0P || r2 == C14370lK.A0L || r2 == C14370lK.A07;
    }

    public static boolean A02(C14370lK r2) {
        return r2 == C14370lK.A04 || r2 == C14370lK.A0X || r2 == C14370lK.A0a || r2 == C14370lK.A0W || r2 == C14370lK.A0U;
    }

    public Bitmap A03(BitmapFactory.Options options, Matrix matrix, Uri uri) {
        InputStream A04 = A04(uri);
        try {
            Bitmap A02 = C37501mV.A02(options, A04);
            if (A02 == null || A02.getWidth() == 0 || A02.getHeight() == 0) {
                throw new C37511mW();
            }
            A04.close();
            return C22200yh.A0B(A02, matrix, 100, 100);
        } catch (Throwable th) {
            try {
                A04.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final InputStream A04(Uri uri) {
        InputStream openInputStream;
        Uri build = uri.buildUpon().query(null).build();
        File A03 = C14350lI.A03(build);
        if (A03 != null) {
            openInputStream = new FileInputStream(A03);
        } else {
            ContentResolver A0C = this.A00.A0C();
            if (A0C != null) {
                openInputStream = A0C.openInputStream(build);
                if (openInputStream == null) {
                    StringBuilder sb = new StringBuilder("Unable to open stream for uri=");
                    sb.append(build);
                    throw new IOException(sb.toString());
                }
            } else {
                throw new IOException("Could not get content resolver");
            }
        }
        if (openInputStream instanceof FileInputStream) {
            this.A01.A05((FileInputStream) openInputStream);
        }
        return openInputStream;
    }
}
