package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.5o3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123855o3 extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AbstractActivityC121475iK A00;

    public C123855o3(AbstractActivityC121475iK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AbstractActivityC121475iK r1 = this.A00;
        C004802e r2 = new C004802e(r1, R.style.FbPayDialogTheme);
        r2.A07(R.string.dyi_delete_report_diablog_title);
        r2.A0A(r1.getBaseContext().getString(R.string.dyi_delete_report_dialog));
        C72463ee.A0S(r2);
        C117295Zj.A0q(r2, this, 9, R.string.delete);
        C12970iu.A1J(r2);
    }
}
