package X;

/* renamed from: X.5bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118355bg extends AnonymousClass0Yo {
    public final /* synthetic */ C128375w0 A00;
    public final /* synthetic */ String A01;

    public C118355bg(C128375w0 r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118105bH.class)) {
            C128375w0 r0 = this.A00;
            C16590pI r1 = r0.A0B;
            AbstractC14440lR r6 = r0.A0z;
            return new C118105bH(r1, r0.A0C, r0.A0V, r0.A0m, r0.A0u, r6, this.A01);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviCreateClaimViewModel");
    }
}
