package X;

/* renamed from: X.1u7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1u7 extends AbstractC16110oT {
    public Integer A00;

    public AnonymousClass1u7() {
        super(3286, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamGroupCreateInit {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "groupCreateEntryPoint", obj);
        sb.append("}");
        return sb.toString();
    }
}
