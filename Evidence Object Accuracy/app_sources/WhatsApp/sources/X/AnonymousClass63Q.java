package X;

import android.media.MediaRecorder;

/* renamed from: X.63Q  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63Q implements MediaRecorder.OnErrorListener {
    public final /* synthetic */ AnonymousClass66O A00;

    public AnonymousClass63Q(AnonymousClass66O r1) {
        this.A00 = r1;
    }

    @Override // android.media.MediaRecorder.OnErrorListener
    public void onError(MediaRecorder mediaRecorder, int i, int i2) {
        this.A00.A03.AST(mediaRecorder, i, i2, true);
    }
}
