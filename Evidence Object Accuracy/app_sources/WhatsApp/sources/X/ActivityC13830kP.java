package X;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Pair;
import com.facebook.redex.IDxAListenerShape6S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.AbstractCollection;

/* renamed from: X.0kP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ActivityC13830kP extends AbstractActivityC13840kQ {
    public AbstractC18390sN A00;
    public AnonymousClass018 A01;
    public C15230mm A02;
    public C21230x5 A03;
    public AbstractC21180x0 A04;
    public AbstractC14440lR A05;
    public AnonymousClass046 A06;
    public boolean A07;

    public void A20() {
    }

    public boolean A21() {
        return false;
    }

    public static AnonymousClass2FJ A1K(AbstractActivityC13850kR r0) {
        return (AnonymousClass2FJ) r0.A1l().generatedComponent();
    }

    public static AnonymousClass2FL A1L(AbstractActivityC13850kR r0) {
        return (AnonymousClass2FL) ((AnonymousClass2FJ) r0.A1l().generatedComponent());
    }

    public static AnonymousClass01J A1M(AnonymousClass2FL r1, ActivityC13830kP r2) {
        AnonymousClass01J r12 = r1.A1E;
        r2.A05 = (AbstractC14440lR) r12.ANe.get();
        return r12;
    }

    public static AnonymousClass12U A1N(AnonymousClass01J r0) {
        return (AnonymousClass12U) r0.AJd.get();
    }

    public static void A1O(Activity activity) {
        activity.getWindow().setBackgroundDrawable(new ColorDrawable(AnonymousClass00T.A00(activity, R.color.popup_dim)));
        activity.getWindow().addFlags(2621440);
    }

    public static void A1P(ActivityC001000l r1, int i) {
        r1.A0R(new IDxAListenerShape6S0100000_2_I1(r1, i));
    }

    public static void A1Q(Object obj, AbstractCollection abstractCollection, int i) {
        abstractCollection.add(Pair.create(obj, Integer.valueOf(i)));
    }

    @Override // X.AbstractActivityC13840kQ, X.ActivityC000800j, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        AnonymousClass01J r3 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
        this.A01 = r3.Ag8();
        C18380sM A1v = r3.A1v();
        this.A00 = A1v;
        super.attachBaseContext(new C48502Gn(context, A1v, this.A01));
        this.A02 = r3.AeO();
        AnonymousClass1Q5 r1 = ((AbstractActivityC13840kQ) this).A00.A01;
        this.A04 = r1.A08;
        this.A03 = r1.A07;
    }

    @Override // X.ActivityC000800j, android.content.Context, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public Resources getResources() {
        AnonymousClass046 r0 = this.A06;
        if (r0 != null) {
            return r0;
        }
        if (this.A01 == null) {
            Log.i("wabaseappcompatactivity/get resources object/returning super resources");
            return super.getResources();
        }
        AnonymousClass046 A00 = AnonymousClass046.A00(super.getResources(), this.A01);
        this.A06 = A00;
        return A00;
    }

    @Override // X.ActivityC000800j, X.ActivityC000900k, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        AnonymousClass018 r0 = this.A01;
        if (r0 != null) {
            r0.A0L();
        }
        super.onConfigurationChanged(configuration);
    }

    @Override // X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        this.A01.A0L();
        super.onCreate(bundle);
    }

    @Override // X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (!this.A07) {
            if (A21()) {
                this.A05.Ab2(new RunnableBRunnable0Shape1S0100000_I0_1(this, 32));
            }
            this.A07 = true;
        }
    }
}
