package X;

/* renamed from: X.0IK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IK extends AnonymousClass0eL {
    public final /* synthetic */ C06440Tp A00;

    public AnonymousClass0IK(C06440Tp r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        C06440Tp r1 = this.A00;
        r1.A0C = 0;
        if (r1.A08 == null && r1.A09 == null) {
            r1.A02();
        }
    }
}
