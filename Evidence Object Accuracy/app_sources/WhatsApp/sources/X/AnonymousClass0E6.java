package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0E6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0E6 extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VI();
    public int A00;
    public boolean A01;

    public AnonymousClass0E6(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        this.A00 = parcel.readInt();
        this.A01 = parcel.readInt() != 0;
    }

    public AnonymousClass0E6(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A01 ? 1 : 0);
    }
}
