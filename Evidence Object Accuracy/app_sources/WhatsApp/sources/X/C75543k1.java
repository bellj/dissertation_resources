package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.CatalogImageListActivity;
import com.whatsapp.R;

/* renamed from: X.3k1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75543k1 extends AnonymousClass03U {
    public boolean A00;
    public final ImageView A01;
    public final AnonymousClass2TT A02;
    public final /* synthetic */ CatalogImageListActivity A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C75543k1(View view, CatalogImageListActivity catalogImageListActivity, AnonymousClass2TT r4) {
        super(view);
        this.A03 = catalogImageListActivity;
        this.A02 = r4;
        this.A01 = C12970iu.A0L(view, R.id.catalog_image_list_image_view);
    }
}
