package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.0CO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CO extends C05530Px {
    public final /* synthetic */ AnonymousClass0XQ A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CO(Context context, View view, AnonymousClass0CK r12, AnonymousClass0XQ r13) {
        super(context, view, r12, R.attr.actionOverflowMenuStyle, 0, false);
        this.A00 = r13;
        if ((((C07340Xp) r12.getItem()).A02 & 32) != 32) {
            View view2 = r13.A0G;
            this.A01 = view2 == null ? (View) r13.A0C : view2;
        }
        AnonymousClass0XK r1 = r13.A0N;
        this.A04 = r1;
        AnonymousClass0XP r0 = this.A03;
        if (r0 != null) {
            r0.Abr(r1);
        }
    }

    @Override // X.C05530Px
    public void A02() {
        this.A00.A0D = null;
        super.A02();
    }
}
