package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.IntentChooserBottomSheetDialogFragment;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54202gL extends AnonymousClass02M {
    public List A00;
    public final /* synthetic */ IntentChooserBottomSheetDialogFragment A01;

    public C54202gL(IntentChooserBottomSheetDialogFragment intentChooserBottomSheetDialogFragment, List list) {
        this.A01 = intentChooserBottomSheetDialogFragment;
        this.A00 = list;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r6, int i) {
        Drawable drawable;
        C75393jm r62 = (C75393jm) r6;
        C42781vo r2 = (C42781vo) this.A00.get(i);
        int i2 = r2.A06;
        r62.A01.setText(r2.A07);
        C12960it.A13(r62.A0H, this, r2, 2);
        try {
            ImageView imageView = r62.A00;
            Context context = imageView.getContext();
            Integer num = r2.A02;
            if (i2 == 0) {
                drawable = context.getPackageManager().getApplicationIcon("com.whatsapp");
            } else {
                drawable = AnonymousClass00T.A04(context, i2);
            }
            if (!(drawable == null || num == null)) {
                drawable = C015607k.A03(drawable);
                C015607k.A0A(drawable, num.intValue());
            }
            imageView.setImageDrawable(drawable);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        boolean A07 = this.A01.A04.A07(689);
        int i2 = R.layout.intent_selector_item;
        if (A07) {
            i2 = R.layout.intent_selector_item_group_profile_editor;
        }
        return new C75393jm(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, i2));
    }
}
