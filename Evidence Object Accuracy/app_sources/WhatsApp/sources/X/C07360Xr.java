package X;

import android.animation.Animator;

/* renamed from: X.0Xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07360Xr implements AbstractC11730gl {
    public final /* synthetic */ Animator A00;
    public final /* synthetic */ AnonymousClass0EJ A01;

    public C07360Xr(Animator animator, AnonymousClass0EJ r2) {
        this.A01 = r2;
        this.A00 = animator;
    }

    @Override // X.AbstractC11730gl
    public void ANe() {
        this.A00.end();
    }
}
