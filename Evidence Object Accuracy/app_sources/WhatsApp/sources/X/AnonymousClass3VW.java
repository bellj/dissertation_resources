package X;

import com.whatsapp.biz.catalog.view.activity.CatalogListActivity;

/* renamed from: X.3VW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3VW implements AbstractC116525Vu {
    public final /* synthetic */ CatalogListActivity A00;

    public AnonymousClass3VW(CatalogListActivity catalogListActivity) {
        this.A00 = catalogListActivity;
    }

    @Override // X.AbstractC116525Vu
    public void ARn(C44691zO r3, long j) {
        CatalogListActivity catalogListActivity = this.A00;
        C12960it.A0w(((ActivityC13810kN) catalogListActivity).A00, ((ActivityC13830kP) catalogListActivity).A01, j);
    }

    @Override // X.AbstractC116525Vu
    public void AUV(C44691zO r9, String str, String str2, int i, long j) {
        C53842fM r0 = ((AbstractActivityC37081lH) this.A00).A0F;
        r0.A0H.A01(r9, r0.A0M, str, str2, j);
    }
}
