package X;

import java.util.List;

/* renamed from: X.4S5  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4S5 {
    public final AnonymousClass1KZ A00;
    public final String A01;
    public final List A02;
    public final boolean A03;

    public AnonymousClass4S5(AnonymousClass1KZ r1, String str, List list, boolean z) {
        this.A03 = z;
        this.A00 = r1;
        this.A02 = list;
        this.A01 = str;
    }
}
