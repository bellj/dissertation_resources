package X;

/* renamed from: X.4Zf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93134Zf {
    public static final AnonymousClass5F1 A00 = new AnonymousClass5F1();
    public static final AnonymousClass4VA A01 = new AnonymousClass4VA("COMPLETING_ALREADY");
    public static final AnonymousClass4VA A02 = new AnonymousClass4VA("COMPLETING_RETRY");
    public static final AnonymousClass4VA A03 = new AnonymousClass4VA("COMPLETING_WAITING_CHILDREN");
    public static final AnonymousClass4VA A04 = new AnonymousClass4VA("SEALED");
    public static final AnonymousClass4VA A05 = new AnonymousClass4VA("TOO_LATE_TO_CANCEL");

    public static final Object A00(Object obj) {
        return obj instanceof AnonymousClass5WP ? new AnonymousClass4LY((AnonymousClass5WP) obj) : obj;
    }
}
