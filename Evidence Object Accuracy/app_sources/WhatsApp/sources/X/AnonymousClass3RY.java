package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;

/* renamed from: X.3RY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3RY implements AbstractC009404s {
    public final /* synthetic */ C49002It A00;
    public final /* synthetic */ C15580nU A01;

    public AnonymousClass3RY(C49002It r1, C15580nU r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C49002It r0 = this.A00;
        C15580nU r11 = this.A01;
        AnonymousClass01J r1 = r0.A00.A03;
        C15570nT A0S = C12970iu.A0S(r1);
        AbstractC14440lR A0T = C12960it.A0T(r1);
        C36691kN r3 = new C36691kN(A0S, (C237913a) r1.ACt.get(), (AnonymousClass1BG) r1.A3d.get(), C12960it.A0O(r1), C12990iw.A0Z(r1), (AnonymousClass11A) r1.A8o.get(), (C244215l) r1.A8y.get(), r11, A0T);
        C237913a r02 = r3.A03;
        r02.A05.A03(r3.A02);
        r3.A07.A03(r3.A06);
        r3.A0B.A03(r3.A0A);
        AnonymousClass11A r03 = r3.A09;
        r03.A00.add(r3.A08);
        r3.A0E.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 43));
        return r3;
    }
}
