package X;

import java.io.DataInputStream;
import java.io.File;

/* renamed from: X.3YQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YQ implements AnonymousClass5XW {
    public long A00 = 0;
    public final long A01;
    public final DataInputStream A02;

    public AnonymousClass3YQ(File file) {
        this.A01 = file.length();
        this.A02 = new DataInputStream(C12990iw.A0h(file));
    }

    @Override // X.AnonymousClass5XW
    public boolean AIK() {
        return this.A00 < this.A01;
    }

    @Override // X.AnonymousClass5XW
    public void AZq(byte[] bArr) {
        this.A02.read(bArr);
        this.A00 += (long) bArr.length;
    }

    @Override // X.AnonymousClass5XW
    public long AaC() {
        return this.A01 - this.A00;
    }

    @Override // X.AnonymousClass5XW
    public void AcZ(long j) {
        AZq(new byte[(int) (j - this.A00)]);
    }

    @Override // X.AnonymousClass5XW
    public void close() {
        this.A02.close();
    }

    @Override // X.AnonymousClass5XW
    public long position() {
        return this.A00;
    }

    @Override // X.AnonymousClass5XW
    public byte readByte() {
        byte readByte = this.A02.readByte();
        this.A00++;
        return readByte;
    }

    @Override // X.AnonymousClass5XW
    public int readInt() {
        int readInt = this.A02.readInt();
        this.A00 += 4;
        return readInt;
    }

    @Override // X.AnonymousClass5XW
    public long readLong() {
        this.A00 += 8;
        return this.A02.readLong();
    }

    @Override // X.AnonymousClass5XW
    public short readShort() {
        short readShort = this.A02.readShort();
        this.A00 += 2;
        return readShort;
    }
}
