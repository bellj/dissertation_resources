package X;

import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.68V  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68V implements AbstractC28771Oy {
    public final /* synthetic */ AnonymousClass60U A00;
    public final /* synthetic */ C128625wP A01;
    public final /* synthetic */ String A02;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public AnonymousClass68V(AnonymousClass60U r1, C128625wP r2, String str) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = r2;
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        StringBuilder A0k = C12960it.A0k("dyiReportManager/download-report/on-download-canceled transferred -> ");
        A0k.append(z);
        C12960it.A1F(A0k);
        if (!z) {
            AnonymousClass60U r3 = this.A00;
            C14330lG r0 = r3.A02;
            String str = this.A02;
            File A0G = r0.A0G(str);
            if (A0G.exists() && !A0G.delete()) {
                Log.e("dyiReportManager/reset/failed-delete-report-file");
            }
            r3.A0A.A0B(2, str);
        }
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r5, C28781Oz r6) {
        StringBuilder A0k = C12960it.A0k("dyiReportManager/download-report/on-download-canceled success -> ");
        boolean A02 = r5.A02();
        A0k.append(A02);
        C12960it.A1F(A0k);
        if (A02) {
            AnonymousClass60U r3 = this.A00;
            String str = this.A02;
            synchronized (r3) {
                r3.A0A.A0B(4, str);
            }
            C128625wP r1 = this.A01;
            Log.i("DyiViewModel/download-report/on-success");
            C123595nP r0 = r1.A00;
            AnonymousClass60U.A01(r0.A02, r0.A08, r0.A0A);
            return;
        }
        this.A01.A00();
        this.A00.A0A.A0B(2, this.A02);
    }
}
