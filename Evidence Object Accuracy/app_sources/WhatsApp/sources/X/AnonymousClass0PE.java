package X;

import java.util.List;

/* renamed from: X.0PE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PE {
    public int A00 = 0;
    public List A01 = null;

    public void A00() {
        this.A00 += 1000;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Object obj : this.A01) {
            sb.append(obj);
            sb.append(' ');
        }
        sb.append('[');
        sb.append(this.A00);
        sb.append(']');
        return sb.toString();
    }
}
