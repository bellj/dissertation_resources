package X;

import android.content.Intent;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.2xD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xD extends AnonymousClass3UA {
    public Intent A00;
    public final /* synthetic */ VoipActivityV2 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2xD(AbstractActivityC28171Kz r1, VoipActivityV2 voipActivityV2) {
        super(r1);
        this.A01 = voipActivityV2;
    }
}
