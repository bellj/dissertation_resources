package X;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1JO  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1JO implements Iterable {
    public static final AnonymousClass1JO A01 = new AnonymousClass1JO(Collections.emptySet());
    public final Set A00;

    public AnonymousClass1JO(Set set) {
        this.A00 = set;
    }

    public static AnonymousClass1JO A00(Collection collection) {
        AnonymousClass1YJ r1 = new AnonymousClass1YJ();
        Set set = r1.A00;
        AnonymousClass009.A05(set);
        set.addAll(collection);
        return r1.A00();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass1JO.class != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((AnonymousClass1JO) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.A00.hashCode();
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return new AnonymousClass204(this.A00.iterator());
    }

    @Override // java.lang.Object
    public String toString() {
        Iterator it = iterator();
        if (!it.hasNext()) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        char c = '[';
        while (true) {
            sb.append(c);
            Object next = it.next();
            if (next == this) {
                next = "(this Collection)";
            }
            sb.append(next);
            if (!it.hasNext()) {
                sb.append(']');
                return sb.toString();
            }
            sb.append(',');
            c = ' ';
        }
    }
}
