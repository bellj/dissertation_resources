package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* renamed from: X.0ho  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12370ho {
    void setSupportButtonTintList(ColorStateList colorStateList);

    void setSupportButtonTintMode(PorterDuff.Mode mode);
}
