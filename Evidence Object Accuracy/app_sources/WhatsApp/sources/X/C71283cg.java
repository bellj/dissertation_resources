package X;

import java.util.Comparator;
import java.util.Set;

/* renamed from: X.3cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71283cg implements Comparator {
    public Set A00;
    public final C36071jH A01;
    public final /* synthetic */ C54512gq A02;

    public C71283cg(C15570nT r3, C15610nY r4, C54512gq r5) {
        this.A02 = r5;
        this.A01 = new C36071jH(r3, r4, true);
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        C15370n3 r3 = (C15370n3) obj;
        C15370n3 r4 = (C15370n3) obj2;
        Set set = this.A00;
        AnonymousClass009.A05(set);
        if (!C15370n3.A07(r3, set) || !C15370n3.A07(r4, this.A00)) {
            if (C15370n3.A07(r3, this.A00)) {
                return -1;
            }
            if (C15370n3.A07(r4, this.A00)) {
                return 1;
            }
        }
        return this.A01.compare(r3, r4);
    }
}
