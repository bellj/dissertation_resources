package X;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.5qW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125005qW {
    public static void A00(Activity activity, C1316663q r6, C129675y7 r7, int i) {
        if (i == 2 || i == 1) {
            Log.e(C12960it.A0W(i, "PAY: NoviChainedStepUpAlertHelper Wavi doesn't support chained step-up for login and P2P : "));
        }
        C130625zi r1 = r6.A01;
        if (r1 == null) {
            Log.e("PAY: NoviChainedStepUpAlertHelper chained step-up message is null");
            C127415uS.A00(r6, r7, "CANCELED", i);
            activity.finish();
            return;
        }
        C004802e A0S = C12980iv.A0S(activity);
        A0S.setTitle(r1.A03);
        A0S.A0A(r1.A00);
        A0S.A03(null, r1.A01);
        A0S.A0B(false);
        String str = r1.A02;
        if (!TextUtils.isEmpty(str)) {
            A0S.A01(null, str);
        }
        AnonymousClass04S create = A0S.create();
        create.setOnShowListener(new DialogInterface.OnShowListener(activity, create, r6, r7, i) { // from class: X.635
            public final /* synthetic */ int A00;
            public final /* synthetic */ Activity A01;
            public final /* synthetic */ AnonymousClass04S A02;
            public final /* synthetic */ C1316663q A03;
            public final /* synthetic */ C129675y7 A04;

            {
                this.A02 = r2;
                this.A01 = r1;
                this.A04 = r4;
                this.A03 = r3;
                this.A00 = r5;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                AnonymousClass04S r4 = this.A02;
                Activity activity2 = this.A01;
                C129675y7 r62 = this.A04;
                C1316663q r5 = this.A03;
                int i2 = this.A00;
                AnonymousClass0U5 r2 = r4.A00;
                C117295Zj.A0o(r2.A0G, activity2, r4, 1);
                r2.A0E.setOnClickListener(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0019: INVOKE  
                      (wrap: android.widget.Button : 0x0012: IGET  (r0v1 android.widget.Button A[REMOVE]) = (r2v0 'r2' X.0U5) X.0U5.A0E android.widget.Button)
                      (wrap: X.64e : 0x0016: CONSTRUCTOR  (r2v1 X.64e A[REMOVE]) = (r3v0 'activity2' android.app.Activity), (r4v0 'r4' X.04S), (r5v0 'r5' X.63q), (r6v0 'r62' X.5y7), (r7v0 'i2' int) call: X.64e.<init>(android.app.Activity, X.04S, X.63q, X.5y7, int):void type: CONSTRUCTOR)
                     type: VIRTUAL call: android.view.View.setOnClickListener(android.view.View$OnClickListener):void in method: X.635.onShow(android.content.DialogInterface):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0016: CONSTRUCTOR  (r2v1 X.64e A[REMOVE]) = (r3v0 'activity2' android.app.Activity), (r4v0 'r4' X.04S), (r5v0 'r5' X.63q), (r6v0 'r62' X.5y7), (r7v0 'i2' int) call: X.64e.<init>(android.app.Activity, X.04S, X.63q, X.5y7, int):void type: CONSTRUCTOR in method: X.635.onShow(android.content.DialogInterface):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.64e, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    X.04S r4 = r8.A02
                    android.app.Activity r3 = r8.A01
                    X.5y7 r6 = r8.A04
                    X.63q r5 = r8.A03
                    int r7 = r8.A00
                    X.0U5 r2 = r4.A00
                    android.widget.Button r1 = r2.A0G
                    r0 = 1
                    X.C117295Zj.A0o(r1, r3, r4, r0)
                    android.widget.Button r0 = r2.A0E
                    X.64e r2 = new X.64e
                    r2.<init>(r3, r4, r5, r6, r7)
                    r0.setOnClickListener(r2)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass635.onShow(android.content.DialogInterface):void");
            }
        });
        create.show();
    }
}
