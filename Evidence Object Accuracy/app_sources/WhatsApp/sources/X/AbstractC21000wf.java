package X;

import android.content.Context;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.View;
import android.widget.ImageView;
import com.google.android.material.textfield.TextInputEditText;
import java.util.List;
import java.util.Locale;

/* renamed from: X.0wf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC21000wf {
    void A6X(Context context, View view, AnonymousClass024 v, AnonymousClass024 v2, AnonymousClass024 v3, Integer num, Integer num2, String str, String str2, String str3, int i, boolean z);

    void A6Z(Context context, View view, C90884Pp v, String str, String str2, String str3, int i, boolean z);

    void A6a(ImageView imageView, AbstractC11740gm v, AbstractC11740gm v2, Object obj, String str, String str2, String str3, String str4);

    void A6b(Context context, View view, AnonymousClass024 v, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, boolean z2);

    void A6c(View view, C90894Pq v, float f, int i, int i2, int i3, int i4);

    void A6d(View view, String str, boolean z);

    void A94(ImageView imageView, String str, String str2);

    Spannable AD8(Context context, Context context2, AnonymousClass024 v, String str, List list, List list2, List list3, List list4);

    TextWatcher AE6(TextInputEditText textInputEditText, Integer num, String str, String str2);

    InputFilter AEg();

    CharacterStyle AHg(Runnable runnable, int i, int i2, int i3);

    Locale AHl();

    void Ad9(View view, long j);

    void AeY();

    void AfA(View view);
}
