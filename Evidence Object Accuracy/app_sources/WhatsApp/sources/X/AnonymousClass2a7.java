package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2a7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2a7 extends Handler {
    public final /* synthetic */ AnonymousClass35T A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2a7(Looper looper, AnonymousClass35T r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        AnonymousClass35T r5 = this.A00;
        AbstractC28651Ol r0 = r5.A00;
        if (r0 != null) {
            int A03 = r0.A03();
            if (A03 > 0) {
                int max = Math.max(0, A03 - r5.A00.A02());
                r5.A06.A02.setDuration((int) TimeUnit.MILLISECONDS.toSeconds((long) max));
                if (max == 0) {
                    ((AbstractC33641ei) r5).A05.A00();
                }
            }
            C12990iw.A17(this);
        }
    }
}
