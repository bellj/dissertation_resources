package X;

/* renamed from: X.1w0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42861w0 {
    public final AbstractC15710nm A00;
    public final C20710wC A01;
    public final C15580nU A02;
    public final C17220qS A03;
    public final AbstractC14440lR A04;

    public C42861w0(AbstractC15710nm r1, C20710wC r2, C15580nU r3, C17220qS r4, AbstractC14440lR r5) {
        this.A02 = r3;
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
        this.A01 = r2;
    }

    public void A00() {
        C17220qS r6 = this.A03;
        String A01 = r6.A01();
        C15580nU r3 = this.A02;
        r6.A09(new C49312Kg(this.A00, this.A01, r3, this.A04), new AnonymousClass1V8(new AnonymousClass1V8("sub_groups", null), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "w:g2"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9(r3, "to")}), A01, 297, 32000);
    }
}
