package X;

import android.content.ContentValues;
import android.database.Cursor;

/* renamed from: X.0tX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19080tX extends AbstractC18500sY implements AbstractC19010tQ {
    public final C19790ug A00;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19080tX(C19790ug r3, C18480sW r4) {
        super(r4, "message_future", 1);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("media_duration");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("raw_data");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("future_message_type");
        C16310on A02 = this.A05.A02();
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            try {
                j = cursor.getLong(columnIndexOrThrow);
                int i2 = cursor.getInt(columnIndexOrThrow2);
                byte[] blob = cursor.getBlob(columnIndexOrThrow3);
                int i3 = cursor.getInt(columnIndexOrThrow4);
                ContentValues contentValues = new ContentValues();
                contentValues.put("message_row_id", Long.valueOf(j));
                contentValues.put("version", Integer.valueOf(i2));
                C30021Vq.A06(contentValues, "data", blob);
                contentValues.put("future_message_type", Integer.valueOf(i3));
                A02.A03.A02(contentValues, "message_future");
                i++;
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("message_future", null, null);
            C21390xL r1 = this.A06;
            r1.A03("future_ready");
            r1.A03("migration_message_future_index");
            r1.A03("migration_message_future_retry");
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
