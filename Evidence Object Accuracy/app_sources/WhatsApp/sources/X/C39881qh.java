package X;

import java.io.File;

/* renamed from: X.1qh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39881qh {
    public final C14370lK A00;
    public final File A01;
    public final String A02;
    public final boolean A03;

    public C39881qh(C14370lK r1, File file, String str, boolean z) {
        this.A00 = r1;
        this.A01 = file;
        this.A02 = str;
        this.A03 = z;
    }
}
