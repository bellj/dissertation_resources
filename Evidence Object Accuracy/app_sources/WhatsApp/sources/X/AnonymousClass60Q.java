package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.FileDescriptor;
import org.chromium.net.UrlRequest;

/* renamed from: X.60Q  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60Q {
    public static final C119115cu A0H = new C119115cu(5);
    public static final C119115cu A0I = new C119115cu(3);
    public static final C119115cu A0J = new C119115cu(2);
    public static final C119115cu A0K = new C119115cu(4);
    public static final C125535rP A0L = new C125535rP(7);
    public static final C125535rP A0M = new C125535rP(0);
    public static final C125535rP A0N = new C125535rP(6);
    public static final C125535rP A0O = new C125535rP(12);
    public static final C125535rP A0P = new C125535rP(13);
    public static final C125535rP A0Q = new C125535rP(14);
    public static final C125535rP A0R = new C125535rP(8);
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final AnonymousClass60Q A0A;
    public final FileDescriptor A0B;
    public final Integer A0C;
    public final Integer A0D;
    public final Integer A0E;
    public final String A0F;
    public final boolean A0G;

    public /* synthetic */ AnonymousClass60Q(C129375xc r3) {
        String str = r3.A0F;
        if (str == null && r3.A0B == null) {
            throw C12970iu.A0f("one of file path or FileDescriptor must be set");
        }
        this.A0F = str;
        this.A0B = r3.A0B;
        this.A07 = r3.A03;
        this.A06 = r3.A02;
        this.A08 = r3.A04;
        this.A05 = r3.A01;
        this.A0G = r3.A0G;
        this.A04 = r3.A00;
        this.A09 = r3.A05;
        this.A0C = r3.A0C;
        this.A0D = r3.A0D;
        this.A0E = r3.A0E;
        this.A00 = r3.A06;
        this.A01 = r3.A07;
        this.A02 = r3.A08;
        this.A03 = r3.A09;
        this.A0A = r3.A0A;
    }

    public Object A00(C119115cu r3) {
        int i;
        int i2 = r3.A00;
        if (i2 == 2) {
            i = this.A07;
        } else if (i2 == 3) {
            i = this.A06;
        } else if (i2 == 4) {
            i = this.A08;
        } else if (i2 == 5) {
            i = this.A05;
        } else {
            throw C12990iw.A0m(C12960it.A0W(i2, "Invalid required video capture result key: "));
        }
        return Integer.valueOf(i);
    }

    public Object A01(C125535rP r3) {
        long j;
        int i = r3.A00;
        if (i == 0) {
            return this.A0F;
        }
        if (i == 1) {
            return this.A0B;
        }
        switch (i) {
            case 6:
                return Boolean.valueOf(this.A0G);
            case 7:
                return Integer.valueOf(this.A04);
            case 8:
                return Integer.valueOf(this.A09);
            case 9:
                return this.A0C;
            case 10:
                return this.A0D;
            case 11:
                return this.A0E;
            case 12:
                j = this.A00;
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                j = this.A01;
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                j = this.A02;
                break;
            case 15:
                j = this.A03;
                break;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return this.A0A;
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Invalid required video capture result key: "));
        }
        return Long.valueOf(j);
    }

    public void A02(C125535rP r6, Object obj) {
        String str;
        String str2;
        int i = r6.A00;
        switch (i) {
            case 12:
                if (this.A00 != -1) {
                    str = "VideoCaptureRequest";
                    str2 = "Start request time was already set, cannot set it again";
                    break;
                } else {
                    this.A00 = C12980iv.A0G(obj);
                    return;
                }
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                if (this.A01 != -1) {
                    str = "VideoCaptureRequest";
                    str2 = "Start time was already set, cannot set it again";
                    break;
                } else {
                    this.A01 = C12980iv.A0G(obj);
                    return;
                }
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                if (this.A02 != -1) {
                    str = "VideoCaptureRequest";
                    str2 = "Stop request time was already set, cannot set it again";
                    break;
                } else {
                    this.A02 = C12980iv.A0G(obj);
                    return;
                }
            case 15:
                if (this.A03 != -1) {
                    str = "VideoCaptureRequest";
                    str2 = "Stop time was already set, cannot set it again";
                    break;
                } else {
                    this.A03 = C12980iv.A0G(obj);
                    return;
                }
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Value is immutable, cannot modify: "));
        }
        AnonymousClass616.A01(str, str2);
    }
}
