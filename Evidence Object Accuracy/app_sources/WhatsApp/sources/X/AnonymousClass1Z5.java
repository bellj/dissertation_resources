package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Z5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Z5 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30821Yy(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30821Yy[i];
    }
}
