package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.3jU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75213jU extends AnonymousClass03U {
    public final WaTextView A00;

    public C75213jU(View view) {
        super(view);
        this.A00 = (WaTextView) view.findViewById(R.id.product_section);
    }
}
