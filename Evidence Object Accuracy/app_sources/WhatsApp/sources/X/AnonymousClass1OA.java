package X;

import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;

/* renamed from: X.1OA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1OA {
    public final long A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final boolean A05;

    public AnonymousClass1OA(String str, String str2, String str3, String str4, long j, boolean z) {
        this.A05 = z;
        this.A01 = str;
        this.A04 = str2;
        this.A00 = j;
        this.A03 = str3;
        this.A02 = str4;
    }

    public static AnonymousClass1OA A00(Throwable th) {
        boolean z;
        Throwable th2 = th;
        while (true) {
            if (!(th2 instanceof OutOfMemoryError)) {
                th2 = th2.getCause();
                if (th2 == null) {
                    z = false;
                    break;
                }
            } else {
                z = true;
                break;
            }
        }
        Throwable th3 = th;
        while (th3.getCause() != null) {
            th3 = th3.getCause();
        }
        String obj = th3.toString();
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return new AnonymousClass1OA(obj, "2.22.17.70", stringWriter.toString(), AnonymousClass180.A08, 388062852, z);
    }

    public String A01() {
        return new JSONObject().put("isOom", this.A05).put("deepestThrowable", this.A01).put("versionName", "2.22.17.70").put("mobileBuildId", 388062852L).put("stacktrace", this.A03).put("sessionId", this.A02).toString();
    }
}
