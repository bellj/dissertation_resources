package X;

import android.os.Parcel;
import java.math.BigDecimal;

/* renamed from: X.1ZZ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1ZZ extends AnonymousClass1ZY {
    public long A00;
    public long A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public BigDecimal A06;

    public AnonymousClass1ZZ() {
    }

    public AnonymousClass1ZZ(Parcel parcel) {
        BigDecimal bigDecimal;
        String readString = parcel.readString();
        if (readString != null) {
            bigDecimal = new BigDecimal(readString);
        } else {
            bigDecimal = null;
        }
        this.A06 = bigDecimal;
        this.A00 = parcel.readLong();
        this.A01 = parcel.readLong();
        this.A02 = parcel.readString();
        this.A03 = parcel.readString();
        this.A04 = parcel.readString();
        this.A05 = parcel.readString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        String str;
        BigDecimal bigDecimal = this.A06;
        if (bigDecimal != null) {
            str = bigDecimal.toString();
        } else {
            str = null;
        }
        parcel.writeString(str);
        parcel.writeLong(this.A00);
        parcel.writeLong(this.A01);
        parcel.writeString(this.A02);
        parcel.writeString(this.A03);
        parcel.writeString(this.A04);
        parcel.writeString(this.A05);
    }
}
