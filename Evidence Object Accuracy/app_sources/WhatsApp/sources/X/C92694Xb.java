package X;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.4Xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92694Xb {
    public MessageDigest A00;
    public MessageDigest A01 = null;

    public C92694Xb(String str) {
        try {
            this.A00 = MessageDigest.getInstance(str);
        } catch (NoSuchAlgorithmException e) {
            throw C72453ed.A0f(e);
        }
    }

    public void A00(byte[] bArr) {
        if (bArr != null) {
            try {
                MessageDigest messageDigest = this.A00;
                this.A01 = (MessageDigest) messageDigest.clone();
                messageDigest.update(bArr);
            } catch (CloneNotSupportedException e) {
                throw C72453ed.A0f(e);
            }
        } else {
            throw AnonymousClass1NR.A00("Cannot add null transcript.", (byte) 80);
        }
    }

    public byte[] A01() {
        try {
            return ((MessageDigest) this.A00.clone()).digest();
        } catch (CloneNotSupportedException e) {
            throw C72453ed.A0f(e);
        }
    }

    public byte[] A02() {
        try {
            return ((MessageDigest) this.A01.clone()).digest();
        } catch (CloneNotSupportedException e) {
            throw C72453ed.A0f(e);
        }
    }
}
