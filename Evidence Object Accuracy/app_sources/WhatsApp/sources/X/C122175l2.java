package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import java.util.List;

/* renamed from: X.5l2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122175l2 extends AbstractC118815cQ {
    public final WaImageView A00;
    public final WaTextView A01;
    public final WaTextView A02;
    public final WaTextView A03;
    public final WaTextView A04;
    public final C37071lG A05;
    public final AnonymousClass018 A06;
    public final AnonymousClass19O A07;

    public C122175l2(View view, C37071lG r3, AnonymousClass018 r4, AnonymousClass19O r5) {
        super(view);
        this.A00 = C12980iv.A0X(view, R.id.item_thumbnail);
        this.A04 = C12960it.A0N(view, R.id.item_title);
        this.A02 = C12960it.A0N(view, R.id.item_quantity);
        this.A01 = C12960it.A0N(view, R.id.item_price);
        this.A03 = C12960it.A0N(view, R.id.item_sale_price);
        this.A05 = r3;
        this.A06 = r4;
        this.A07 = r5;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: android.text.SpannableString */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AbstractC118815cQ
    public void A08(C125965s6 r20) {
        String A03;
        String A032;
        C122015km r0 = (C122015km) r20;
        AbstractC16390ow r7 = r0.A02;
        AnonymousClass1ZD A0b = C117305Zk.A0b(r7);
        C66023Lz r5 = r0.A01;
        C44741zT r15 = r0.A00;
        WaImageView waImageView = this.A00;
        Resources A09 = C12960it.A09(waImageView);
        this.A04.setText(r5.A03);
        WaTextView waTextView = this.A02;
        int i = r5.A00;
        waTextView.setText(C12990iw.A0o(A09, Integer.valueOf(i), new Object[1], 0, R.string.order_item_quantity_in_list));
        AnonymousClass1ZI r10 = r5.A02;
        if (r10 == null) {
            WaTextView waTextView2 = this.A01;
            AnonymousClass1ZI r11 = r5.A01;
            if (r11 == null) {
                A032 = null;
            } else {
                A032 = A0b.A03(this.A06, new AnonymousClass1ZI(r11.A00, r11.A02, r11.A01 * ((long) i)));
            }
            waTextView2.setText(A032);
            this.A03.setVisibility(8);
        } else {
            WaTextView waTextView3 = this.A01;
            long j = (long) i;
            AnonymousClass1ZI r102 = new AnonymousClass1ZI(r10.A00, r10.A02, r10.A01 * j);
            AnonymousClass018 r112 = this.A06;
            waTextView3.setText(A0b.A03(r112, r102));
            WaTextView waTextView4 = this.A03;
            waTextView4.setVisibility(0);
            AnonymousClass1ZI r13 = r5.A01;
            if (r13 == null) {
                A03 = null;
            } else {
                A03 = A0b.A03(r112, new AnonymousClass1ZI(r13.A00, r13.A02, r13.A01 * j));
                if (A03 != null) {
                    SpannableString spannableString = new SpannableString(A03);
                    spannableString.setSpan(new StrikethroughSpan(), 0, spannableString.length(), 33);
                    A03 = spannableString;
                }
            }
            waTextView4.setText(A03);
        }
        if (r5.A00().startsWith("custom-item")) {
            waImageView.setImageDrawable(AnonymousClass2GE.A01(this.A0H.getContext(), R.drawable.ic_tools_custom, R.color.order_reference_color));
            waImageView.setScaleX(0.5f);
            waImageView.setScaleY(0.5f);
        } else if (r15 == null) {
            List list = A0b.A04.A08;
            if (!"digital-goods".equals(A0b.A09) || list.size() != 1) {
                waImageView.setImageDrawable(new ColorDrawable(C12960it.A09(waImageView).getColor(R.color.wds_cool_gray_100)));
                return;
            }
            this.A07.A07(waImageView, (AbstractC15340mz) r7, new C134166Do(this));
        } else {
            this.A05.A02(waImageView, r15, null, new AnonymousClass2E5() { // from class: X.66q
                @Override // X.AnonymousClass2E5
                public final void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
                    ImageView imageView = (ImageView) r4.A09.get();
                    if (imageView != null) {
                        imageView.setBackgroundColor(0);
                        imageView.setImageBitmap(bitmap);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                }
            }, 2);
        }
    }
}
