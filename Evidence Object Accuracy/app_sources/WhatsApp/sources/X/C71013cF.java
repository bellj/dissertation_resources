package X;

/* renamed from: X.3cF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71013cF implements AbstractC115475Rr {
    public final AnonymousClass1V8 A00;
    public final C64083Ee A01;

    public C71013cF(AbstractC15710nm r9, AnonymousClass1V8 r10) {
        AnonymousClass1V8.A01(r10, "state");
        String[] A08 = C13000ix.A08();
        A08[0] = "type";
        AnonymousClass3JT.A04(null, r10, String.class, C12970iu.A0j(), C12970iu.A0k(), "succeed", A08, false);
        this.A01 = (C64083Ee) AnonymousClass3JT.A02(r9, r10, 27);
        this.A00 = r10;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C71013cF.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((C71013cF) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
