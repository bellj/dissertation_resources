package X;

import java.io.IOException;

/* renamed from: X.5Fh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112915Fh implements AbstractC117235Zb {
    public C92754Xh A00;

    public C112915Fh(C92754Xh r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return new AnonymousClass5NY(this.A00.A01());
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        try {
            return ADw();
        } catch (IOException e) {
            throw C12960it.A0U(e.getMessage());
        }
    }
}
