package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;
import org.chromium.net.UrlRequest;

/* renamed from: X.2T5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2T5 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2T5 A0E;
    public static volatile AnonymousClass255 A0F;
    public int A00;
    public int A01;
    public int A02;
    public AnonymousClass25Q A03;
    public String A04 = "";
    public String A05 = "";
    public String A06 = "";
    public String A07 = "";
    public String A08 = "";
    public String A09 = "";
    public String A0A = "";
    public String A0B = "";
    public String A0C = "";
    public String A0D = "";

    static {
        AnonymousClass2T5 r0 = new AnonymousClass2T5();
        A0E = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        int A02;
        AnonymousClass26V r1;
        switch (r8.ordinal()) {
            case 0:
                return A0E;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass2T5 r10 = (AnonymousClass2T5) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                int i = this.A01;
                boolean z2 = true;
                if ((r10.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r9.Afp(i, r10.A01, z, z2);
                this.A03 = (AnonymousClass25Q) r9.Aft(this.A03, r10.A03);
                int i2 = this.A00;
                boolean z3 = false;
                if ((i2 & 4) == 4) {
                    z3 = true;
                }
                String str = this.A09;
                int i3 = r10.A00;
                boolean z4 = false;
                if ((i3 & 4) == 4) {
                    z4 = true;
                }
                this.A09 = r9.Afy(str, r10.A09, z3, z4);
                boolean z5 = false;
                if ((i2 & 8) == 8) {
                    z5 = true;
                }
                String str2 = this.A0A;
                boolean z6 = false;
                if ((i3 & 8) == 8) {
                    z6 = true;
                }
                this.A0A = r9.Afy(str2, r10.A0A, z5, z6);
                boolean z7 = false;
                if ((i2 & 16) == 16) {
                    z7 = true;
                }
                String str3 = this.A0C;
                boolean z8 = false;
                if ((i3 & 16) == 16) {
                    z8 = true;
                }
                this.A0C = r9.Afy(str3, r10.A0C, z7, z8);
                boolean z9 = false;
                if ((i2 & 32) == 32) {
                    z9 = true;
                }
                String str4 = this.A08;
                boolean z10 = false;
                if ((i3 & 32) == 32) {
                    z10 = true;
                }
                this.A08 = r9.Afy(str4, r10.A08, z9, z10);
                boolean z11 = false;
                if ((i2 & 64) == 64) {
                    z11 = true;
                }
                String str5 = this.A05;
                boolean z12 = false;
                if ((i3 & 64) == 64) {
                    z12 = true;
                }
                this.A05 = r9.Afy(str5, r10.A05, z11, z12);
                boolean z13 = false;
                if ((i2 & 128) == 128) {
                    z13 = true;
                }
                String str6 = this.A0B;
                boolean z14 = false;
                if ((i3 & 128) == 128) {
                    z14 = true;
                }
                this.A0B = r9.Afy(str6, r10.A0B, z13, z14);
                boolean z15 = false;
                if ((i2 & 256) == 256) {
                    z15 = true;
                }
                String str7 = this.A0D;
                boolean z16 = false;
                if ((i3 & 256) == 256) {
                    z16 = true;
                }
                this.A0D = r9.Afy(str7, r10.A0D, z15, z16);
                boolean z17 = false;
                if ((i2 & 512) == 512) {
                    z17 = true;
                }
                int i4 = this.A02;
                boolean z18 = false;
                if ((i3 & 512) == 512) {
                    z18 = true;
                }
                this.A02 = r9.Afp(i4, r10.A02, z17, z18);
                boolean z19 = false;
                if ((i2 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z19 = true;
                }
                String str8 = this.A07;
                boolean z20 = false;
                if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z20 = true;
                }
                this.A07 = r9.Afy(str8, r10.A07, z19, z20);
                boolean z21 = false;
                if ((i2 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z21 = true;
                }
                String str9 = this.A06;
                boolean z22 = false;
                if ((i3 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z22 = true;
                }
                this.A06 = r9.Afy(str9, r10.A06, z21, z22);
                boolean z23 = false;
                if ((i2 & 4096) == 4096) {
                    z23 = true;
                }
                String str10 = this.A04;
                boolean z24 = false;
                if ((i3 & 4096) == 4096) {
                    z24 = true;
                }
                this.A04 = r9.Afy(str10, r10.A04, z23, z24);
                if (r9 == C463025i.A00) {
                    this.A00 = i2 | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            int i5 = 1;
                            switch (A03) {
                                case 0:
                                    break;
                                case 8:
                                    A02 = r92.A02();
                                    switch (A02) {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        case 4:
                                        case 5:
                                        case 6:
                                        case 7:
                                        case 8:
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                        case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                        case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                        case 15:
                                        case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                        case 17:
                                        case 18:
                                        case 19:
                                        case C43951xu.A01:
                                        case 21:
                                        case 22:
                                        case 23:
                                        case 24:
                                        case 25:
                                        case 26:
                                        case 27:
                                        case 28:
                                            this.A00 = 1 | this.A00;
                                            this.A01 = A02;
                                            continue;
                                    }
                                    break;
                                case 18:
                                    if ((this.A00 & 2) == 2) {
                                        r1 = (AnonymousClass26V) this.A03.A0T();
                                    } else {
                                        r1 = null;
                                    }
                                    AnonymousClass25Q r0 = (AnonymousClass25Q) r92.A09(r102, AnonymousClass25Q.A06.A0U());
                                    this.A03 = r0;
                                    if (r1 != null) {
                                        r1.A04(r0);
                                        this.A03 = (AnonymousClass25Q) r1.A01();
                                    }
                                    this.A00 |= 2;
                                    continue;
                                case 26:
                                    String A0A = r92.A0A();
                                    this.A00 |= 4;
                                    this.A09 = A0A;
                                    continue;
                                case 34:
                                    String A0A2 = r92.A0A();
                                    this.A00 |= 8;
                                    this.A0A = A0A2;
                                    continue;
                                case 42:
                                    String A0A3 = r92.A0A();
                                    this.A00 |= 16;
                                    this.A0C = A0A3;
                                    continue;
                                case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                    String A0A4 = r92.A0A();
                                    this.A00 |= 32;
                                    this.A08 = A0A4;
                                    continue;
                                case 58:
                                    String A0A5 = r92.A0A();
                                    this.A00 |= 64;
                                    this.A05 = A0A5;
                                    continue;
                                case 66:
                                    String A0A6 = r92.A0A();
                                    this.A00 |= 128;
                                    this.A0B = A0A6;
                                    continue;
                                case 74:
                                    String A0A7 = r92.A0A();
                                    this.A00 |= 256;
                                    this.A0D = A0A7;
                                    continue;
                                case 80:
                                    A02 = r92.A02();
                                    if (A02 != 0 && A02 != 1 && A02 != 2 && A02 != 3) {
                                        i5 = 10;
                                        break;
                                    } else {
                                        this.A00 |= 512;
                                        this.A02 = A02;
                                        continue;
                                    }
                                    break;
                                case 90:
                                    String A0A8 = r92.A0A();
                                    this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A07 = A0A8;
                                    continue;
                                case 98:
                                    String A0A9 = r92.A0A();
                                    this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A06 = A0A9;
                                    continue;
                                case 106:
                                    String A0A10 = r92.A0A();
                                    this.A00 |= 4096;
                                    this.A04 = A0A10;
                                    continue;
                                default:
                                    if (!A0a(r92, A03)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            super.A0X(i5, A02);
                        } catch (IOException e) {
                            C28971Pt r12 = new C28971Pt(e.getMessage());
                            r12.unfinishedMessage = this;
                            throw new RuntimeException(r12);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AnonymousClass2T5();
            case 5:
                return new AnonymousClass2T6();
            case 6:
                break;
            case 7:
                if (A0F == null) {
                    synchronized (AnonymousClass2T5.class) {
                        if (A0F == null) {
                            A0F = new AnonymousClass255(A0E);
                        }
                    }
                }
                return A0F;
            default:
                throw new UnsupportedOperationException();
        }
        return A0E;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            AnonymousClass25Q r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass25Q.A06;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            i2 += CodedOutputStream.A07(3, this.A09);
        }
        if ((this.A00 & 8) == 8) {
            i2 += CodedOutputStream.A07(4, this.A0A);
        }
        if ((this.A00 & 16) == 16) {
            i2 += CodedOutputStream.A07(5, this.A0C);
        }
        if ((this.A00 & 32) == 32) {
            i2 += CodedOutputStream.A07(6, this.A08);
        }
        if ((this.A00 & 64) == 64) {
            i2 += CodedOutputStream.A07(7, this.A05);
        }
        if ((this.A00 & 128) == 128) {
            i2 += CodedOutputStream.A07(8, this.A0B);
        }
        if ((this.A00 & 256) == 256) {
            i2 += CodedOutputStream.A07(9, this.A0D);
        }
        int i4 = this.A00;
        if ((i4 & 512) == 512) {
            i2 += CodedOutputStream.A02(10, this.A02);
        }
        if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A07(11, this.A07);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A07(12, this.A06);
        }
        if ((this.A00 & 4096) == 4096) {
            i2 += CodedOutputStream.A07(13, this.A04);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass25Q r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass25Q.A06;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A09);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A0A);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A0C);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(6, this.A08);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A05);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A0B);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0I(9, this.A0D);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0E(10, this.A02);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0I(11, this.A07);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0I(12, this.A06);
        }
        if ((this.A00 & 4096) == 4096) {
            codedOutputStream.A0I(13, this.A04);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
