package X;

import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.5fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120305fx extends AbstractC124175oj {
    public final int A00;
    public final C128995x0 A01;
    public final AnonymousClass6MM A02;
    public final List A03;

    public C120305fx(C18640sm r7, C18600si r8, C18610sj r9, C128995x0 r10, AnonymousClass6MM r11, AnonymousClass1BY r12, C22120yY r13, List list, int i) {
        super(r7, r8, r9, r12, r13);
        this.A03 = list;
        this.A02 = r11;
        this.A00 = i;
        this.A01 = r10;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C452120p A0L;
        AnonymousClass01T r7 = (AnonymousClass01T) obj;
        String str = (String) r7.A00;
        C452120p r4 = (C452120p) r7.A01;
        AnonymousClass6MM r3 = this.A02;
        if (r3 != null) {
            if (str == null) {
                Log.i(C30931Zj.A01("PinTokenizer", C12970iu.A0s(r4, C12960it.A0j("PaymentPinTokenTask token error: "))));
                if (r4 != null) {
                    A0L = r4;
                } else {
                    A0L = C117305Zk.A0L();
                }
                r3.APo(A0L);
            } else {
                r3.AX5(str);
            }
        }
        C128995x0 r32 = this.A01;
        if (r32 == null) {
            return;
        }
        if (str == null) {
            Log.i(C30931Zj.A01("PinTokenizer", C12970iu.A0s(r4, C12960it.A0j("PaymentPinTokenTask token error: "))));
            if (r4 == null) {
                r4 = C117305Zk.A0L();
            }
            if (r32.A01.compareAndSet(false, true)) {
                r32.A02.decrementAndGet();
                r32.A00.APo(r4);
                return;
            }
            return;
        }
        r32.A00(this.A00, str);
    }
}
