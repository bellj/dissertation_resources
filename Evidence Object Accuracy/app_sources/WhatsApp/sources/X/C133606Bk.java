package X;

import java.util.Map;

/* renamed from: X.6Bk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133606Bk implements AbstractC16960q2 {
    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124605po.class;
    }

    @Override // X.AbstractC16960q2
    public Object Aam(Enum r5, Object obj, Map map) {
        AnonymousClass1ZO r6 = (AnonymousClass1ZO) obj;
        EnumC124605po r52 = (EnumC124605po) r5;
        boolean A0N = C16700pc.A0N(r6, r52);
        if (!(r6 instanceof C119705ey)) {
            return null;
        }
        int A01 = C117315Zl.A01(r52, C125225qu.A00);
        if (A01 == A0N) {
            AnonymousClass1ZR r0 = ((C119705ey) r6).A02;
            if (r0 != null) {
                return r0.A00;
            }
            return null;
        } else if (A01 == 2) {
            return ((C119705ey) r6).A03;
        } else {
            throw new C113285Gx();
        }
    }
}
