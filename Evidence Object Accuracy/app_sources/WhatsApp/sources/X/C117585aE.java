package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity;
import java.util.List;

/* renamed from: X.5aE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117585aE extends ArrayAdapter {
    public List A00;
    public final LayoutInflater A01;
    public final /* synthetic */ PaymentGroupParticipantPickerActivity A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C117585aE(Context context, PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity, List list) {
        super(context, (int) R.layout.group_chat_info_row, list);
        this.A02 = paymentGroupParticipantPickerActivity;
        this.A01 = LayoutInflater.from(context);
        this.A00 = C12980iv.A0x(list);
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        List list = this.A00;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A00.get(i);
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        C127885vD r0;
        TextView textView;
        int i2;
        C28811Pc r1;
        if (view == null) {
            view = this.A01.inflate(R.layout.group_chat_info_row, viewGroup, false);
            r0 = new C127885vD();
            PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity = this.A02;
            r0.A03 = new C28801Pb(view, paymentGroupParticipantPickerActivity.A05, paymentGroupParticipantPickerActivity.A0I, (int) R.id.name);
            r0.A00 = C12970iu.A0L(view, R.id.avatar);
            r0.A02 = (TextEmojiLabel) view.findViewById(R.id.group_participant_picker_push_name);
            r0.A01 = C12960it.A0J(view, R.id.status);
            view.setTag(r0);
        } else {
            r0 = (C127885vD) view.getTag();
        }
        r0.A03.A08(null);
        r0.A03.A04(AnonymousClass00T.A00(getContext(), R.color.list_item_title));
        r0.A03.A01.setAlpha(1.0f);
        r0.A02.setVisibility(8);
        r0.A01.setVisibility(8);
        r0.A01.setText(R.string.participant_cant_receive_payments);
        C126985tl r7 = (C126985tl) this.A00.get(i);
        AnonymousClass009.A05(r7);
        C15370n3 r5 = r7.A00;
        r0.A04 = r7;
        r0.A03.A06(r5);
        ImageView imageView = r0.A00;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(new AnonymousClass2TT(getContext()).A00(R.string.transition_avatar));
        AnonymousClass028.A0k(imageView, C12960it.A0d(C15380n4.A03(r5.A0A()), A0h));
        PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity2 = this.A02;
        paymentGroupParticipantPickerActivity2.A06.A06(r0.A00, r5);
        r0.A00.setOnClickListener(new View.OnClickListener(r5, r0, this) { // from class: X.64K
            public final /* synthetic */ C15370n3 A00;
            public final /* synthetic */ C127885vD A01;
            public final /* synthetic */ C117585aE A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                C117585aE r2 = this.A02;
                C15370n3 r12 = this.A00;
                C127885vD r52 = this.A01;
                PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity3 = r2.A02;
                AnonymousClass3DG r13 = new AnonymousClass3DG(((ActivityC13810kN) paymentGroupParticipantPickerActivity3).A0C, (AbstractC14640lm) r12.A0B(AbstractC14640lm.class), null);
                r13.A02 = AnonymousClass028.A0J(r52.A00);
                r13.A00(paymentGroupParticipantPickerActivity3, view2);
            }
        });
        if (paymentGroupParticipantPickerActivity2.A0C.A00((UserJid) r5.A0B(UserJid.class)) != 2) {
            r0.A03.A01.setAlpha(0.5f);
            r0.A01.setVisibility(0);
            C28811Pc r12 = r5.A0C;
            if (r12 != null && !TextUtils.isEmpty(r12.A01)) {
                textView = r0.A01;
                i2 = R.string.contact_cant_receive_payments;
                textView.setText(i2);
            }
        } else {
            if (paymentGroupParticipantPickerActivity2.A02.A0I((UserJid) r5.A0B(UserJid.class))) {
                r0.A03.A01.setAlpha(0.5f);
                r0.A01.setVisibility(0);
                textView = r0.A01;
                i2 = R.string.unblock_to_send_payments;
            } else if (((ActivityC13810kN) paymentGroupParticipantPickerActivity2).A0C.A07(733) || ((ActivityC13810kN) paymentGroupParticipantPickerActivity2).A0C.A07(544)) {
                AnonymousClass1ZO r2 = r7.A01;
                AbstractC38141na AFL = paymentGroupParticipantPickerActivity2.A0D.A02().AFL();
                if (!(AFL == null || r2 == null || r2.A06(AFL.AFW()) != 2)) {
                    r0.A01.setVisibility(0);
                    textView = r0.A01;
                    i2 = R.string.payments_multi_invite_picker_subtitle;
                }
            }
            textView.setText(i2);
        }
        if (r5.A0U == null || ((r1 = r5.A0C) != null && !TextUtils.isEmpty(r1.A01))) {
            return view;
        }
        r0.A02.setVisibility(0);
        r0.A02.A0E(paymentGroupParticipantPickerActivity2.A05.A09(r5));
        return view;
    }
}
