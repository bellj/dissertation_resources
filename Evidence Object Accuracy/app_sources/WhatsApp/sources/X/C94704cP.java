package X;

/* renamed from: X.4cP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94704cP {
    public static final AnonymousClass5ZQ A00 = new AnonymousClass5KP();
    public static final AnonymousClass5ZQ A01 = new AnonymousClass5KQ();
    public static final AnonymousClass5ZQ A02 = new AnonymousClass5KR();
    public static final AnonymousClass4VA A03 = new AnonymousClass4VA("NO_THREAD_ELEMENTS");

    public static final Object A00(Object obj, AnonymousClass5X4 r2) {
        if (obj == null) {
            obj = r2.fold(C12980iv.A0i(), A00);
            C16700pc.A0C(obj);
        }
        if (obj == C12980iv.A0i()) {
            return A03;
        }
        if (obj instanceof Integer) {
            return r2.fold(new C90554Oi(r2, C12960it.A05(obj)), A02);
        }
        throw C12980iv.A0n("updateThreadContext");
    }

    public static final void A01(Object obj, AnonymousClass5X4 r2) {
        if (obj == A03) {
            return;
        }
        if (obj instanceof C90554Oi) {
            AnonymousClass5ZR[] r1 = ((C90554Oi) obj).A01;
            int length = r1.length - 1;
            if (length >= 0) {
                C16700pc.A0C(r1[length]);
                throw C12980iv.A0n("restoreThreadContext");
            }
        } else if (r2.fold(null, A01) != null) {
            throw C12980iv.A0n("restoreThreadContext");
        } else {
            throw C12980iv.A0n("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        }
    }
}
