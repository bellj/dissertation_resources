package X;

/* renamed from: X.04c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C008004c extends AnonymousClass032 {
    public long realtimeMs;
    public long uptimeMs;

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A01(AnonymousClass032 r3) {
        C008004c r32 = (C008004c) r3;
        this.uptimeMs = r32.uptimeMs;
        this.realtimeMs = r32.realtimeMs;
        return this;
    }

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A02(AnonymousClass032 r5, AnonymousClass032 r6) {
        C008004c r52 = (C008004c) r5;
        C008004c r62 = (C008004c) r6;
        if (r62 == null) {
            r62 = new C008004c();
        }
        if (r52 == null) {
            r62.uptimeMs = this.uptimeMs;
            r62.realtimeMs = this.realtimeMs;
            return r62;
        }
        r62.uptimeMs = this.uptimeMs - r52.uptimeMs;
        r62.realtimeMs = this.realtimeMs - r52.realtimeMs;
        return r62;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C008004c r7 = (C008004c) obj;
            if (!(this.uptimeMs == r7.uptimeMs && this.realtimeMs == r7.realtimeMs)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        long j = this.uptimeMs;
        long j2 = this.realtimeMs;
        return (((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)));
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("TimeMetrics{uptimeMs=");
        sb.append(this.uptimeMs);
        sb.append(", realtimeMs=");
        sb.append(this.realtimeMs);
        sb.append('}');
        return sb.toString();
    }
}
