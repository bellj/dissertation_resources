package X;

import android.widget.TextView;
import com.whatsapp.R;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/* renamed from: X.1nZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38131nZ {
    public static CharSequence A00(AnonymousClass018 r5, long j) {
        int i;
        String A04;
        long currentTimeMillis = System.currentTimeMillis();
        int A00 = C38121nY.A00(currentTimeMillis, j);
        if (A00 == 0) {
            i = R.string.web_session_last_today_at;
        } else if (A00 == 1) {
            i = R.string.web_session_last_yesterday_at;
        } else {
            if (C38121nY.A0B(currentTimeMillis, j)) {
                A04 = AnonymousClass1MY.A00(r5, j);
            } else {
                A04 = AnonymousClass1MY.A04(r5, j);
            }
            return r5.A0B(R.string.web_session_last_date, C38121nY.A05(r5, A04, AnonymousClass3JK.A00(r5, j)));
        }
        return AnonymousClass3JK.A01(r5, r5.A0B(i, AnonymousClass3JK.A00(r5, j)), j);
    }

    public static CharSequence A01(AnonymousClass018 r8, long j) {
        String str;
        Locale locale;
        int i;
        long currentTimeMillis = System.currentTimeMillis();
        int A00 = C38121nY.A00(currentTimeMillis, j);
        if (A00 == 0) {
            int i2 = (int) ((currentTimeMillis - j) / 60000);
            if (i2 < 1) {
                return r8.A09(R.string.just_now);
            }
            if (i2 < 60) {
                return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i2))}, 269, (long) i2);
            }
            locale = AnonymousClass018.A00(r8.A00);
            i = 270;
        } else if (A00 == 1) {
            locale = AnonymousClass018.A00(r8.A00);
            i = 294;
        } else {
            if (C38121nY.A0B(currentTimeMillis, j)) {
                str = AnonymousClass1MY.A00(r8, j);
            } else {
                str = AnonymousClass1MY.A04(r8, j);
            }
            return C38121nY.A05(r8, str, AnonymousClass3JK.A00(r8, j));
        }
        str = AnonymousClass1MY.A05(locale, r8.A08(i));
        return C38121nY.A05(r8, str, AnonymousClass3JK.A00(r8, j));
    }

    public static String A02(AnonymousClass018 r8, int i, int i2) {
        int i3;
        if (i2 != 7) {
            switch (i2) {
                case 0:
                    i3 = 287;
                    return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i))}, i3, (long) i);
                case 1:
                    i3 = 281;
                    return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i))}, i3, (long) i);
                case 2:
                    i3 = 276;
                    return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i))}, i3, (long) i);
                case 3:
                    i3 = 273;
                    return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i))}, i3, (long) i);
                case 4:
                    i3 = 290;
                    return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i))}, i3, (long) i);
                case 5:
                    i3 = 284;
                    return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i))}, i3, (long) i);
                case 6:
                    i3 = 292;
                    return r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(i))}, i3, (long) i);
            }
        } else if (i == -1) {
            return r8.A09(R.string.mute_always);
        }
        throw new IllegalArgumentException("Invalid duration unit");
    }

    public static String A03(AnonymousClass018 r6, int i, long j) {
        int i2;
        Object[] objArr;
        String A01;
        String str;
        String A06 = r6.A06();
        if ("en".equals(A06) || "de".equals(A06) || "es".equals(A06)) {
            if (i <= 30) {
                i2 = R.string.time_and_date;
                objArr = new Object[1];
                A01 = AnonymousClass1MY.A01(r6, j);
            } else {
                if (AnonymousClass1MY.A01 == null) {
                    AnonymousClass1MY.A01 = DateFormat.getDateInstance(2, AnonymousClass018.A00(r6.A00));
                }
                return r6.A0B(R.string.date, ((DateFormat) AnonymousClass1MY.A01.clone()).format(new Date(j)));
            }
        } else if (i <= 30) {
            i2 = R.string.time_and_date;
            objArr = new Object[1];
            A01 = AnonymousClass1MY.A04(r6, j);
        } else {
            i2 = R.string.date;
            objArr = new Object[1];
            str = AnonymousClass1MY.A04(r6, j);
            objArr[0] = str;
            return r6.A0B(i2, objArr);
        }
        str = C38121nY.A05(r6, A01, AnonymousClass3JK.A00(r6, j));
        objArr[0] = str;
        return r6.A0B(i2, objArr);
    }

    public static String A04(AnonymousClass018 r18, long j) {
        long j2;
        long j3;
        String str;
        Object[] objArr;
        Long valueOf;
        if (j >= 3600) {
            j2 = j / 3600;
            j -= 3600 * j2;
        } else {
            j2 = 0;
        }
        if (j >= 60) {
            j3 = j / 60;
            j -= 60 * j3;
        } else {
            j3 = 0;
        }
        int i = 221;
        if (j2 > 0) {
            i = 220;
        }
        String A08 = r18.A08(i);
        int length = A08.length();
        StringBuilder sb = new StringBuilder(length);
        Locale A00 = AnonymousClass018.A00(r18.A00);
        int i2 = 0;
        while (i2 < length) {
            char charAt = A08.charAt(i2);
            if (charAt == 'h' || charAt == 'm' || charAt == 's') {
                int i3 = i2 + 1;
                if (i3 >= length || A08.charAt(i3) != charAt) {
                    str = "%d";
                    i3 = i2;
                } else {
                    str = "%02d";
                }
                if (charAt == 'h') {
                    objArr = new Object[1];
                    valueOf = Long.valueOf(j2);
                } else if (charAt != 'm') {
                    if (charAt == 's') {
                        objArr = new Object[1];
                        valueOf = Long.valueOf(j);
                    }
                    i2 = i3;
                } else {
                    objArr = new Object[1];
                    valueOf = Long.valueOf(j3);
                }
                objArr[0] = valueOf;
                sb.append(String.format(A00, str, objArr));
                i2 = i3;
            } else {
                sb.append(charAt);
            }
            i2++;
        }
        return sb.toString();
    }

    public static String A05(AnonymousClass018 r9, long j) {
        boolean z = false;
        if (j >= 0) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        long j2 = (j + 60000) - 1;
        long j3 = j2 / 3600000;
        if (j3 == 0) {
            return A02(r9, (int) (j2 / 60000), 1);
        }
        return A02(r9, (int) j3, 2);
    }

    public static String A06(AnonymousClass018 r10, long j) {
        boolean z = false;
        if (j >= 0) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        int i = (int) (j / 3600000);
        long j2 = j % 3600000;
        int i2 = (int) (j2 / 60000);
        String A02 = A02(r10, (int) ((j2 % 60000) / 1000), 0);
        if (i > 0) {
            return r10.A0A(237, r10.A0A(242, A02(r10, i, 2), A02(r10, i2, 1)), A02);
        }
        return i2 > 0 ? r10.A0A(243, A02(r10, i2, 1), A02) : A02;
    }

    public static String A07(AnonymousClass018 r17, long j) {
        String A0A;
        int i;
        int i2;
        boolean z = false;
        if (j >= 0) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        long j2 = j / 3600000;
        long j3 = j - (3600000 * j2);
        long j4 = j3 / 60000;
        if (j3 - (60000 * j4) > 0) {
            j4++;
        }
        if (j4 == 60) {
            j2++;
            j4 = 0;
        }
        if (j2 == 0) {
            i2 = (int) j4;
            A0A = A02(r17, i2, 1);
            i = R.plurals.time_left;
        } else {
            int i3 = (int) j2;
            int i4 = (int) j4;
            A0A = r17.A0A(238, r17.A0H(new Object[]{String.format(AnonymousClass018.A00(r17.A00), "%d", Integer.valueOf(i3))}, 278, (long) i3), r17.A0H(new Object[]{String.format(AnonymousClass018.A00(r17.A00), "%d", Integer.valueOf(i4))}, 283, (long) i4));
            i = R.plurals.time_left;
            i2 = (int) (j2 + j4);
        }
        return r17.A0I(new Object[]{A0A}, i, (long) i2);
    }

    public static String A08(AnonymousClass018 r12, long j) {
        long j2 = j / 3600000;
        long j3 = (j - (3600000 * j2)) / 60000;
        if (j2 == 0) {
            if (j3 == 0) {
                return A02(r12, (int) (j / 1000), 0);
            }
            return A02(r12, (int) j3, 1);
        } else if (j3 == 0) {
            return A02(r12, (int) j2, 2);
        } else {
            return r12.A0A(243, A02(r12, (int) j2, 2), A02(r12, (int) j3, 1));
        }
    }

    public static String A09(AnonymousClass018 r2, long j) {
        Locale A00;
        int i;
        int A002 = C38121nY.A00(System.currentTimeMillis(), j);
        if (A002 == 0) {
            A00 = AnonymousClass018.A00(r2.A00);
            i = 270;
        } else if (A002 != 1) {
            return AnonymousClass1MY.A03(r2, j);
        } else {
            A00 = AnonymousClass018.A00(r2.A00);
            i = 294;
        }
        return AnonymousClass1MY.A05(A00, r2.A08(i));
    }

    public static String A0A(AnonymousClass018 r2, long j, boolean z) {
        int A00 = C38121nY.A00(System.currentTimeMillis(), j);
        if (A00 == 0) {
            return AnonymousClass3JK.A00(r2, j);
        }
        if (A00 == 1) {
            return AnonymousClass1MY.A05(AnonymousClass018.A00(r2.A00), r2.A08(294));
        }
        if (z) {
            return AnonymousClass1MY.A07(r2, 0).format(new Date(j));
        }
        return AnonymousClass1MY.A04(r2, j);
    }

    public static String A0B(AnonymousClass018 r5, Object[] objArr, int i, int i2, int i3, long j, boolean z) {
        Object[] copyOf;
        String A04;
        int A00 = C38121nY.A00(System.currentTimeMillis(), j);
        int length = objArr.length;
        if (length == 0) {
            copyOf = new String[1];
        } else {
            copyOf = Arrays.copyOf(objArr, length + 1);
        }
        if (A00 == 0 || A00 == 1) {
            copyOf[length] = r5.A0F(AnonymousClass3JK.A00(r5, j));
            if (A00 != 0) {
                i = i2;
            }
            return AnonymousClass3JK.A01(r5, r5.A0B(i, copyOf), j);
        }
        if (A00 > 30 || !z) {
            A04 = AnonymousClass1MY.A04(r5, j);
        } else {
            A04 = C38121nY.A05(r5, AnonymousClass1MY.A04(r5, j), AnonymousClass3JK.A00(r5, j));
        }
        copyOf[length] = r5.A0F(A04);
        return r5.A0B(i3, copyOf);
    }

    public static void A0C(TextView textView, AnonymousClass018 r2, long j) {
        textView.setText(A04(r2, j));
    }
}
