package X;

import java.util.Collections;

/* renamed from: X.1Gx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27331Gx extends AbstractC250017s {
    public final C250217u A00;
    public final C233411h A01;
    public final C21370xJ A02;
    public final C14830m7 A03;
    public final C14820m6 A04;
    public final C16510p9 A05;
    public final C19990v2 A06;
    public final C15680nj A07;
    public final C15860o1 A08;

    public C27331Gx(C250217u r1, C233411h r2, C21370xJ r3, C14830m7 r4, C14820m6 r5, C16510p9 r6, C19990v2 r7, C15680nj r8, C233511i r9, C15860o1 r10) {
        super(r9);
        this.A03 = r4;
        this.A05 = r6;
        this.A06 = r7;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A08 = r10;
        this.A04 = r5;
        this.A07 = r8;
    }

    public final void A08(AnonymousClass1PE r7, C34631gS r8) {
        boolean z = r7.A0f;
        boolean z2 = r8.A02;
        if (z != z2) {
            r7.A0f = z2;
            this.A05.A09(r7);
            this.A02.A00();
            if (z2) {
                C15860o1 r2 = this.A08;
                AbstractC14640lm r5 = r8.A01;
                if (r2.A08(r5.getRawString()).A0F) {
                    r2.A0A(r5, 0, false);
                    this.A01.A06(1);
                    super.A00.A0F(Collections.singleton(new C34651gU(r5, r8.A04, false)));
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        if (r1.A04 >= r10.A04) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005b, code lost:
        if (r1.A04 >= r10.A04) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a7, code lost:
        if (r1.getBoolean("notify_new_message_for_archived_chats", false) == false) goto L_0x00a9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A09(X.C34631gS r10) {
        /*
            r9 = this;
            boolean r6 = r10.A02
            r5 = 0
            if (r6 == 0) goto L_0x005e
            X.0lm r1 = r10.A01
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]
            java.lang.String r0 = "pin_v1"
            r2[r5] = r0
            java.lang.String r1 = r1.getRawString()
            r0 = 1
            r2[r0] = r1
            java.lang.String r8 = X.AnonymousClass1JQ.A00(r2)
            X.11i r7 = r9.A00
            X.1JQ r1 = r7.A06(r8)
            if (r1 == 0) goto L_0x003e
            X.1Jk r0 = r1.A02()
            X.AnonymousClass009.A05(r0)
            X.1gT r0 = r0.A0F
            if (r0 != 0) goto L_0x002e
            X.1gT r0 = X.C34641gT.A02
        L_0x002e:
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x003e
            long r2 = r1.A04
            long r0 = r10.A04
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 < 0) goto L_0x003e
        L_0x003a:
            r9.A05(r10)
            return
        L_0x003e:
            X.1JQ r1 = r7.A05(r8)
            if (r1 == 0) goto L_0x005e
            X.1Jk r0 = r1.A02()
            X.AnonymousClass009.A05(r0)
            X.1gT r0 = r0.A0F
            if (r0 != 0) goto L_0x0051
            X.1gT r0 = X.C34641gT.A02
        L_0x0051:
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x005e
            long r3 = r1.A04
            long r1 = r10.A04
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x005e
            goto L_0x003a
        L_0x005e:
            X.0lm r1 = r10.A01
            X.0v2 r0 = r9.A06
            X.1PE r3 = r0.A06(r1)
            if (r3 == 0) goto L_0x00b0
            if (r6 == 0) goto L_0x00a9
            X.17u r0 = r9.A00
            X.1g1 r1 = r0.A04(r1, r5)
            X.1g1 r0 = r10.A00
            int r2 = X.C34361g1.A00(r1, r0)
            r0 = 1
            if (r2 == 0) goto L_0x0095
            if (r2 == r0) goto L_0x00ad
            r0 = 2
            if (r2 == r0) goto L_0x00ad
            r0 = 3
            if (r2 == r0) goto L_0x0095
            java.lang.String r1 = "archive-chat-handler/applyMutation RangeEnclosedState is not recognized or not used = "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0095:
            X.0m6 r0 = r9.A04
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r0 = "archive_v2_enabled"
            boolean r0 = r1.getBoolean(r0, r5)
            if (r0 == 0) goto L_0x003a
            java.lang.String r0 = "notify_new_message_for_archived_chats"
            boolean r0 = r1.getBoolean(r0, r5)
            if (r0 != 0) goto L_0x003a
        L_0x00a9:
            r9.A08(r3, r10)
            goto L_0x003a
        L_0x00ad:
            r9.A08(r3, r10)
        L_0x00b0:
            r9.A06(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27331Gx.A09(X.1gS):void");
    }
}
