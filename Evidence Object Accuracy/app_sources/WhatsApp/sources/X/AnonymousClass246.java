package X;

/* renamed from: X.246  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass246 extends AbstractC16110oT {
    public Long A00;
    public String A01;

    public AnonymousClass246() {
        super(1376, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamBusinessMute {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "muteT", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "muteeId", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
