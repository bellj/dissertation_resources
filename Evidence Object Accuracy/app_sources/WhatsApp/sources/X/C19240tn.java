package X;

import android.database.Cursor;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

/* renamed from: X.0tn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19240tn extends AbstractC19250to implements AbstractC19010tQ {
    public static final AnonymousClass2H5 A05 = new AnonymousClass2H5();
    public AnonymousClass2H5 A00 = A05;
    public final C15570nT A01;
    public final C16370ot A02;
    public final C20140vH A03;
    public final C20150vI A04;

    public C19240tn(C15570nT r2, C16370ot r3, C20140vH r4, C18480sW r5, C20150vI r6) {
        super(r5);
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r6;
    }

    @Override // X.AbstractC19250to, X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        return super.A09(cursor);
    }

    @Override // X.AbstractC19250to, X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("message_main_verification_done", 1);
    }

    @Override // X.AbstractC19250to, X.AbstractC18500sY
    public void A0I() {
        super.A0I();
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A0B("DROP VIEW IF EXISTS message_view_old_schema");
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC19250to, X.AbstractC18500sY
    public void A0J() {
        super.A0J();
        C16310on A02 = this.A05.A02();
        try {
            A02.A03.A0B("CREATE VIEW IF NOT EXISTS message_view_old_schema AS SELECT messages._id AS _id, messages._id AS sort_id, chat._id AS chat_row_id, key_from_me AS from_me, key_id, -1 AS sender_jid_row_id, remote_resource AS sender_jid_raw_string, status, needs_push AS broadcast, recipient_count, participant_hash, forwarded AS origination_flags, origin, timestamp, received_timestamp, receipt_server_timestamp, CAST (CASE WHEN (messages.media_wa_type = 0 AND messages.status=6) THEN 7 ELSE messages.media_wa_type END AS INTEGER) AS message_type, '' as text_data, starred, lookup_tables, data, media_url, media_mime_type, media_size, media_name, media_caption, media_hash, media_duration, latitude, longitude, thumb_image, raw_data, quoted_row_id, mentioned_jids, multicast_id, edit_version, media_enc_hash, payment_transaction_id, preview_type, receipt_device_timestamp, read_device_timestamp, played_device_timestamp, future_message_type, message_add_on_flags, 1 AS table_version FROM messages JOIN jid AS chat_jid ON messages.key_remote_jid= chat_jid.raw_string JOIN chat AS chat ON chat.jid_row_id = chat_jid._id");
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC19250to
    public String A0W(Cursor cursor) {
        byte b;
        byte b2;
        String str;
        AbstractC14640lm A052;
        C30381Xe r1;
        AnonymousClass1YT A00;
        String obj;
        StringBuilder sb;
        String str2;
        C30761Ys A01;
        C16370ot r6 = this.A02;
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("chat_row_id"));
        C16510p9 r11 = r6.A05;
        AbstractC14640lm A053 = r11.A05(j);
        AbstractC30391Xf r2 = null;
        if (A053 != null) {
            C20090vC r12 = r6.A0G;
            AnonymousClass1IS A002 = C20090vC.A00(cursor, A053);
            if (A002 == null) {
                sb = new StringBuilder();
                str2 = "CachedMessageStore/getMessageFromOldSchemaForDebug/can't read key; jid=";
            } else {
                AbstractC15340mz A03 = r12.A03(cursor, A002);
                if ((A03 instanceof AnonymousClass1XL) && (A01 = r6.A0W.A01(A03.A11)) != null) {
                    AnonymousClass1XL r13 = (AnonymousClass1XL) A03;
                    r13.A01 = A01.A05;
                    r13.A00 = A01.A02;
                }
                if (r6.A06(A03)) {
                    sb = new StringBuilder();
                    str2 = "CachedMessageStore/getMessage/message is deleted for jid=";
                } else {
                    A03.A0Z(1);
                    if (A03 instanceof AbstractC16130oV) {
                        r6.A0H.A08((AbstractC16130oV) A03);
                    }
                    long j2 = A03.A0F;
                    if (j2 != 0 || (A03.A15 & 2) == 2) {
                        C20520vt r8 = r6.A0U;
                        if (j2 > 0) {
                            try {
                                C16310on A012 = r8.A0C.get();
                                try {
                                    boolean z = false;
                                    Cursor A09 = A012.A03.A09(C32371c3.A00, new String[]{String.valueOf(A03.A0F)});
                                    if (A09.moveToLast()) {
                                        AbstractC14640lm A054 = r8.A03.A05(A09.getLong(A09.getColumnIndexOrThrow("chat_row_id")));
                                        if (A054 != null) {
                                            C20090vC r14 = r8.A08;
                                            AnonymousClass1IS A003 = C20090vC.A00(A09, A054);
                                            if (A003 == null) {
                                                obj = "QuotedMessageStore/fillQuotedMessageFromLegacyTableForVerification unable to fetch key from mainMessageStore";
                                            } else {
                                                AbstractC15340mz A032 = r14.A03(A09, A003);
                                                A032.A0b(A09);
                                                A032.A0Z(2);
                                                A03.A0g(A032);
                                                if (!TextUtils.isEmpty(A032.A0m)) {
                                                    A032.A0L = r8.A0F.A0O(A032.A0z.A01, A032.A0m);
                                                }
                                                r8.A04(A032, A03.A11);
                                            }
                                        }
                                        A09.close();
                                        A012.close();
                                    } else {
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("QuotedMessageStore/fillQuotedMessageFromLegacyTableForVerification unable to read quoted message from db. cursor is empty. is quoted_row_id same as row_id ? ");
                                        if (A03.A11 == A03.A0F) {
                                            z = true;
                                        }
                                        sb2.append(z);
                                        obj = sb2.toString();
                                    }
                                    Log.w(obj);
                                    A09.close();
                                    A012.close();
                                } catch (Throwable th) {
                                    try {
                                        A012.close();
                                    } catch (Throwable unused) {
                                    }
                                    throw th;
                                }
                            } catch (IllegalArgumentException | IllegalStateException e) {
                                A03.A0P = null;
                                r8.A00.AaV("QuotedMessageStore/loadForVerification/failed", e.toString(), true);
                            }
                        }
                    }
                    if (!TextUtils.isEmpty(A03.A0m)) {
                        A03.A0L = r6.A0Q.A0O(A03.A0z.A01, A03.A0m);
                    }
                    if (A03.A12(1)) {
                        r6.A0B.A00(A03);
                    }
                    if (A03 instanceof C30401Xg) {
                        C30401Xg r15 = (C30401Xg) A03;
                        r15.A16(r6.A03.A03(r15));
                    }
                    if ((A03 instanceof C30381Xe) && (A00 = r6.A0M.A00((r1 = (C30381Xe) A03))) != null) {
                        r1.A16(Collections.singletonList(A00));
                    }
                    if (A03 instanceof AbstractC28871Pi) {
                        r6.A0W.A02(A03);
                    }
                    if (A03 instanceof AnonymousClass1XV) {
                        r6.A0T.A02((AnonymousClass1XV) A03, "SELECT message_row_id, business_owner_jid, product_id, title, description, currency_code, amount_1000, retailer_id, url, product_image_count, sale_amount_1000, body, footer FROM message_product WHERE message_row_id= ?");
                    }
                    if (A03 instanceof AnonymousClass1XJ) {
                        r6.A04.A02((AnonymousClass1XJ) A03, "SELECT message_row_id, business_owner_jid, title, description FROM message_product WHERE message_row_id=?");
                    }
                    if (A03 instanceof C28581Od) {
                        r6.A0D.A03((C28581Od) A03);
                    }
                    if (A03 instanceof AnonymousClass1XF) {
                        r6.A0O.A03((AnonymousClass1XF) A03, "SELECT message_row_id, order_id, thumbnail, order_title, item_count, status, surface, message, seller_jid, token, currency_code,total_amount_1000 FROM message_order WHERE message_row_id=?", false);
                    }
                    if (A03 instanceof AnonymousClass1XK) {
                        r6.A09.A05((AnonymousClass1XK) A03);
                    }
                    if (A03 instanceof C32211bn) {
                        C32211bn r22 = (C32211bn) A03;
                        r22.A00 = r6.A09.A00(r22.A11);
                    }
                    if (A03 instanceof C32221bo) {
                        C32221bo r23 = (C32221bo) A03;
                        r23.A00 = r6.A09.A00(r23.A11);
                    }
                    if (A03 instanceof C32251br) {
                        C32251br r24 = (C32251br) A03;
                        r24.A00 = r6.A09.A00(r24.A11);
                    }
                    if (A03 instanceof AbstractC16390ow) {
                        r6.A0E.A0G((AbstractC16390ow) A03, "SELECT element_type, element_content FROM message_ui_elements WHERE message_row_id = ?", A03.A11);
                    }
                    if (A03 instanceof AnonymousClass1XE) {
                        r6.A0E.A0C((AnonymousClass1XE) A03, "SELECT message_row_id, element_type, reply_values, reply_description FROM message_ui_elements_reply WHERE message_row_id=?");
                    }
                    if (A03 instanceof AnonymousClass1XD) {
                        r6.A0E.A09((AnonymousClass1XD) A03, "SELECT message_row_id, element_type, reply_values, reply_description FROM message_ui_elements_reply WHERE message_row_id=?");
                    }
                    if (A03.A12(256)) {
                        r6.A08.A02(A03);
                    }
                    if (A03.A12(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)) {
                        r6.A0A.A00(A03);
                    }
                    if (A03 instanceof AnonymousClass1XH) {
                        r6.A0Z.A01(A03);
                    }
                    if (A03.A12(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH)) {
                        r6.A0N.A00(A03);
                    }
                    if (A03.A12(4096)) {
                        r6.A0S.A02(A03);
                    }
                    if (A03 instanceof AnonymousClass1XC) {
                        r6.A0P.A00((AnonymousClass1XC) A03, "SELECT message_row_id, service, expiration_timestamp FROM message_payment_invite WHERE message_row_id = ?");
                    }
                    if (A03 instanceof C27671Iq) {
                        r6.A0R.A01((C27671Iq) A03);
                    }
                    if (A03.A10()) {
                        byte[] A013 = r6.A0K.A01(A03.A11);
                        if (A03.A10()) {
                            A03.A1D = A013;
                        }
                    }
                    r2 = A03;
                }
            }
            sb.append(str2);
            sb.append(A053);
            Log.w(sb.toString());
        }
        if (r2 == null) {
            return null;
        }
        try {
            long j3 = r2.A11;
            C16310on A014 = r6.A0L.get();
            Cursor A092 = A014.A03.A09(C32301bw.A0C, new String[]{String.valueOf(j3)});
            try {
                AbstractC15340mz A02 = (!A092.moveToLast() || (A052 = r11.A05(A092.getLong(A092.getColumnIndexOrThrow("chat_row_id")))) == null) ? null : r6.A02(A092, A052, false, false);
                A092.close();
                A014.close();
                if (((r2 instanceof AbstractC30391Xf) && r2.A00) || A02 == null) {
                    return null;
                }
                A0Z(r2, A02);
                r6.A05(A02.A0z);
                this.A04.A00(r2, A02, "");
                return null;
            } catch (Throwable th2) {
                if (A092 != null) {
                    try {
                        A092.close();
                    } catch (Throwable unused2) {
                    }
                }
                throw th2;
            }
        } catch (IllegalStateException e2) {
            if (!(r2 instanceof AnonymousClass1XB)) {
                b = r2.A0y;
            } else {
                b = 7;
            }
            AbstractC15340mz A0E = r2.A0E();
            if (A0E == null) {
                b2 = -1;
            } else if (!(A0E instanceof AnonymousClass1XB)) {
                b2 = A0E.A0y;
            } else {
                b2 = 7;
            }
            AbstractC14640lm r16 = r2.A0z.A00;
            if (C15380n4.A0J(r16)) {
                str = "group";
            } else if (C15380n4.A0N(r16)) {
                str = "status";
            } else {
                str = C15380n4.A0F(r16) ? "broadcast" : "individual";
            }
            Locale locale = Locale.US;
            new SimpleDateFormat("d MMM yyyy", locale);
            return String.format(locale, " message type : %d quoted message type: %d chat type : %s created time : %s  Failed message check: %s ", Integer.valueOf(b), Integer.valueOf(b2), str, "", e2.getMessage());
        }
    }

    public final void A0Z(AbstractC15340mz r7, AbstractC15340mz r8) {
        AbstractC15340mz A0E;
        String str;
        C30341Xa r3;
        C30751Yr r5;
        if (r7.A03() != r8.A03()) {
            r7.A02 = r8.A02;
        }
        if (r8.A03() == 0) {
            if (r8.A0I() == null) {
                r7.A0l(null);
            }
        } else if (r8.A13() == null) {
            r7.A0w(null);
        }
        if (r7 instanceof C28861Ph) {
            C28861Ph r2 = (C28861Ph) r7;
            if (TextUtils.isEmpty(r2.A03)) {
                r2.A03 = null;
            }
            C28861Ph r1 = (C28861Ph) r8;
            if (TextUtils.isEmpty(r1.A03)) {
                r1.A03 = null;
            }
            if (TextUtils.isEmpty(r2.A05)) {
                r2.A05 = null;
            }
            if (TextUtils.isEmpty(r1.A05)) {
                r1.A05 = null;
            }
            if (TextUtils.isEmpty(r2.A06)) {
                r2.A06 = null;
            }
            if (TextUtils.isEmpty(r1.A06)) {
                r1.A06 = null;
            }
        }
        if (r7 instanceof C30351Xb) {
            C30351Xb r0 = (C30351Xb) r7;
            r0.A14();
            r7.A0w(null);
            r8.A0w(null);
            List A14 = r0.A14();
            List A142 = ((C30351Xb) r8).A14();
            if (!A14.equals(A142) && !A14.isEmpty() && A14.size() == A142.size()) {
                Collections.sort(A14);
                Collections.sort(A142);
            }
        }
        if (r7 instanceof C30331Wz) {
            r7.A01 = 7;
        }
        if ((r7 instanceof C30341Xa) && r7.A0z.A02 && (r5 = (r3 = (C30341Xa) r7).A02) != null) {
            UserJid userJid = r5.A06;
            C15570nT r02 = this.A01;
            r02.A08();
            C27631Ih r12 = r02.A05;
            if (r12 != null && !userJid.equals(r12)) {
                C30751Yr r22 = new C30751Yr(r12);
                r22.A00 = r5.A00;
                r22.A01 = r5.A01;
                r22.A03 = r5.A03;
                r22.A02 = r5.A02;
                r22.A04 = r5.A04;
                r22.A05 = r5.A05;
                r3.A02 = r22;
            }
        }
        r7.A0G = r8.A0G;
        List list = r7.A0o;
        if (list != null) {
            ArrayList arrayList = new ArrayList(new HashSet(list));
            Collections.sort(arrayList);
            r7.A0v(arrayList);
        }
        List list2 = r8.A0o;
        if (list2 != null) {
            ArrayList arrayList2 = new ArrayList(new HashSet(list2));
            Collections.sort(arrayList2);
            r8.A0v(arrayList2);
        }
        r8.A0F = r7.A0F;
        if ((r7 instanceof C30501Xq) && (r8 instanceof C30501Xq) && ((C30501Xq) r7).A00 == null) {
            C30501Xq r13 = (C30501Xq) r8;
            if (r13.A00 != null) {
                r13.A00 = null;
            }
        }
        if ((r7 instanceof C16440p1) && (r8 instanceof C16440p1)) {
            AbstractC16130oV r14 = (AbstractC16130oV) r7;
            r14.A07 = r14.A16();
            AbstractC16130oV r15 = (AbstractC16130oV) r8;
            r15.A07 = r15.A16();
        }
        AbstractC15340mz A0E2 = r7.A0E();
        if (A0E2 != null && (A0E = r8.A0E()) != null) {
            A0E2.A11 = A0E.A11;
            r7.A0E().A12 = r8.A0E().A12;
            r7.A0E().A0S();
            r8.A0E().A0S();
            AbstractC15340mz A0E3 = r7.A0E();
            AbstractC15340mz A0E4 = r8.A0E();
            A0E3.A0l = A0E4.A0l;
            A0E3.A0G = A0E4.A0G;
            A0E3.A0A = A0E4.A0A;
            A0E3.A0X(A0E4.A05());
            if (!(A0E4 instanceof AbstractC16130oV)) {
                str = null;
            } else {
                str = ((AbstractC16130oV) A0E4).A09;
            }
            A0E3.A0m(str);
            AbstractC15340mz A0E5 = r7.A0E();
            A0E5.A0U(A0E5.A07());
            AbstractC15340mz A0E6 = r7.A0E();
            A0E6.A0r = false;
            A0E6.A0e(r8.A0E().A0B());
            AbstractC15340mz A0E7 = r7.A0E();
            if (A0E7 instanceof C28861Ph) {
                C28861Ph r32 = (C28861Ph) A0E7;
                C28861Ph r16 = (C28861Ph) r8.A0E();
                r32.A03 = r16.A03;
                r32.A05 = r16.A05;
                r32.A06 = r16.A06;
                r32.A16(r16.A17());
                byte[] A17 = r32.A17();
                r32.A02 = null;
                r32.A16(A17);
            }
            if (A0E7 instanceof AbstractC16130oV) {
                C16150oX r4 = ((AbstractC16130oV) A0E7).A02;
                C16150oX r33 = ((AbstractC16130oV) r8.A0E()).A02;
                if (!(r4 == null || r33 == null)) {
                    r4.A0Q = r33.A0Q;
                    r4.A0S = r33.A0S;
                    r4.A0T = r33.A0T;
                    r4.A00 = r33.A00;
                    r4.A0C = r33.A0C;
                    r4.A0L = r33.A0L;
                    r4.A09 = r33.A09;
                    r4.A0V = r33.A0V;
                    r4.A0O = r33.A0O;
                    r4.A0M = r33.A0M;
                    r4.A05 = r33.A05;
                    r4.A02 = r33.A02;
                    r4.A03 = r33.A03;
                    r4.A0D = r33.A0D;
                    r4.A0E = r33.A0E;
                    r4.A07 = r33.A07;
                }
            }
            if (A0E7 instanceof AnonymousClass1XP) {
                ((AnonymousClass1XP) A0E7).A02 = ((AnonymousClass1XP) r8.A0E()).A02;
            }
            if (A0E7 instanceof C30341Xa) {
                C30341Xa r42 = (C30341Xa) A0E7;
                C30341Xa r34 = (C30341Xa) r8.A0E();
                if (r34 != null) {
                    r42.A01 = r34.A01;
                    r42.A00 = r34.A00;
                    r42.A02 = r34.A02;
                }
            }
            if (A0E7 instanceof C30331Wz) {
                ((C30331Wz) A0E7).A01 = ((C30331Wz) r8.A0E()).A01;
            }
            if (A0E7 instanceof C30361Xc) {
                ((C30361Xc) A0E7).A01 = ((C30361Xc) r8.A0E()).A01;
            }
            A0Z(A0E7, r8.A0E());
        }
    }
}
