package X;

/* renamed from: X.0bB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08430bB implements AbstractC12120hP {
    public String toString() {
        return "empty";
    }

    @Override // X.AbstractC12120hP
    public boolean ALK(AnonymousClass0K2 r3, AnonymousClass0I1 r4) {
        if (!(r4 instanceof AbstractC12490i0) || ((AbstractC12490i0) r4).ABO().size() == 0) {
            return true;
        }
        return false;
    }
}
