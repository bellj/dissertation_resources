package X;

import android.os.Process;
import java.nio.CharBuffer;

/* renamed from: X.0IX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IX extends AnonymousClass0SQ {
    public static final long A02 = ((long) Process.myUid());
    public static final CharBuffer A03 = CharBuffer.wrap("dummy0");
    public static final CharBuffer A04 = CharBuffer.wrap("lo");
    public static final CharBuffer A05 = CharBuffer.wrap("wlan0");
    public AnonymousClass033 A00;
    public final CharBuffer A01 = CharBuffer.allocate(128);

    @Override // X.AnonymousClass0SQ
    public boolean A01() {
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0069, code lost:
        if (X.AnonymousClass0IX.A04.compareTo(r3) != 0) goto L_0x006b;
     */
    @Override // X.AnonymousClass0SQ
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(long[] r11) {
        /*
            r10 = this;
            r9 = 0
            X.033 r1 = r10.A00     // Catch: 0fA -> 0x00c3
            if (r1 != 0) goto L_0x000e
            java.lang.String r0 = "/proc/net/xt_qtaguid/stats"
            X.033 r1 = new X.033     // Catch: 0fA -> 0x00c3
            r1.<init>(r0)     // Catch: 0fA -> 0x00c3
            r10.A00 = r1     // Catch: 0fA -> 0x00c3
        L_0x000e:
            r1.A02()     // Catch: 0fA -> 0x00c3
            X.033 r1 = r10.A00     // Catch: 0fA -> 0x00c3
            boolean r0 = r1.A05     // Catch: 0fA -> 0x00c3
            if (r0 == 0) goto L_0x00c2
            boolean r0 = r1.A08()     // Catch: 0fA -> 0x00c3
            if (r0 == 0) goto L_0x00c2
            r1 = 0
            java.util.Arrays.fill(r11, r1)     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A03()     // Catch: 0fA -> 0x00c3
        L_0x0027:
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            boolean r0 = r0.A08()     // Catch: 0fA -> 0x00c3
            r5 = 1
            if (r0 == 0) goto L_0x00c1
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A04()     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            java.nio.CharBuffer r3 = r10.A01     // Catch: 0fA -> 0x00c3
            r0.A07(r3)     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A04()     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A04()     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            long r7 = r0.A00()     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A04()     // Catch: 0fA -> 0x00c3
            java.nio.CharBuffer r0 = X.AnonymousClass0IX.A05     // Catch: 0fA -> 0x00c3
            int r0 = r0.compareTo(r3)     // Catch: 0fA -> 0x00c3
            r6 = 1
            if (r0 == 0) goto L_0x00bf
            r6 = 0
            java.nio.CharBuffer r0 = X.AnonymousClass0IX.A03     // Catch: 0fA -> 0x00c3
            int r0 = r0.compareTo(r3)     // Catch: 0fA -> 0x00c3
            if (r0 == 0) goto L_0x00bf
            java.nio.CharBuffer r0 = X.AnonymousClass0IX.A04     // Catch: 0fA -> 0x00c3
            int r0 = r0.compareTo(r3)     // Catch: 0fA -> 0x00c3
            if (r0 == 0) goto L_0x00bf
        L_0x006b:
            long r3 = X.AnonymousClass0IX.A02     // Catch: 0fA -> 0x00c3
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x00b8
            if (r6 != 0) goto L_0x0076
            if (r5 != 0) goto L_0x0076
            goto L_0x00b8
        L_0x0076:
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            long r4 = r0.A00()     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A04()     // Catch: 0fA -> 0x00c3
            r8 = 2
            if (r6 == 0) goto L_0x0085
            r8 = 0
        L_0x0085:
            r8 = r8 | r9
            int r3 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            r0 = 0
            if (r3 != 0) goto L_0x008c
            r0 = 4
        L_0x008c:
            r8 = r8 | r0
            r7 = r8 | 0
            r5 = r11[r7]     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            long r3 = r0.A00()     // Catch: 0fA -> 0x00c3
            long r5 = r5 + r3
            r11[r7] = r5     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A04()     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A04()     // Catch: 0fA -> 0x00c3
            r7 = r8 | 1
            r5 = r11[r7]     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            long r3 = r0.A00()     // Catch: 0fA -> 0x00c3
            long r5 = r5 + r3
            r11[r7] = r5     // Catch: 0fA -> 0x00c3
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A03()     // Catch: 0fA -> 0x00c3
            goto L_0x0027
        L_0x00b8:
            X.033 r0 = r10.A00     // Catch: 0fA -> 0x00c3
            r0.A03()     // Catch: 0fA -> 0x00c3
            goto L_0x0027
        L_0x00bf:
            r5 = 0
            goto L_0x006b
        L_0x00c1:
            return r5
        L_0x00c2:
            return r9
        L_0x00c3:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IX.A02(long[]):boolean");
    }
}
