package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.2Tn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51262Tn {
    public static Integer A00(AnonymousClass1M2 r3) {
        int i;
        if (r3 != null) {
            String str = r3.A07;
            if (str != null && str.startsWith("smb:")) {
                i = 2;
            } else if (r3.A01()) {
                i = 3;
            }
            return Integer.valueOf(i);
        }
        return null;
    }

    public static String A01(int i, String str) {
        if (i == 0) {
            return str.substring(17);
        }
        if (i != 2) {
            StringBuilder sb = new StringBuilder("ContactQrUtils/getUniqueIdFromContactQrCode/invalid code type");
            sb.append(i);
            Log.e(sb.toString());
        } else {
            String lastPathSegment = Uri.parse(str).getLastPathSegment();
            if (lastPathSegment != null) {
                return lastPathSegment;
            }
        }
        return "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001d, code lost:
        if (r1 != 2) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006d, code lost:
        if (r7 != null) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0075, code lost:
        if ((!r7.A00()) != false) goto L_0x0077;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(X.AnonymousClass1M2 r6, X.C42351v4 r7, X.C16120oU r8, X.C48182Et r9, java.lang.Boolean r10, java.lang.Integer r11, int r12, boolean r13) {
        /*
            if (r11 != 0) goto L_0x000d
            if (r7 == 0) goto L_0x0056
            int r0 = r7.A00
            if (r0 != 0) goto L_0x0056
            r0 = 3
        L_0x0009:
            java.lang.Integer r11 = java.lang.Integer.valueOf(r0)
        L_0x000d:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r12)
            if (r9 == 0) goto L_0x001f
            int r1 = r9.A01
            r2 = 1
            if (r1 == 0) goto L_0x0020
            r0 = 1
            r2 = 3
            if (r1 == r0) goto L_0x0020
            r2 = 2
            if (r1 == r2) goto L_0x0020
        L_0x001f:
            r2 = 7
        L_0x0020:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            java.lang.Integer r3 = A00(r6)
            r0 = 0
            if (r11 != 0) goto L_0x002c
            r0 = 1
        L_0x002c:
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r13)
            X.2Ty r1 = new X.2Ty
            r1.<init>()
            r1.A03 = r5
            r1.A04 = r11
            r1.A05 = r4
            r1.A01 = r2
            r1.A00 = r10
            r1.A02 = r3
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0052
            r0 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.A06 = r0
        L_0x0052:
            r8.A06(r1)
            return
        L_0x0056:
            if (r9 == 0) goto L_0x006d
            int r0 = r9.A00
            if (r0 != 0) goto L_0x0077
            if (r7 == 0) goto L_0x0079
            boolean r0 = r7.A00()
            if (r0 == 0) goto L_0x006f
            int r1 = r9.A01
            if (r1 == 0) goto L_0x006f
            r0 = 2
            if (r1 == r0) goto L_0x006f
            r0 = 4
            goto L_0x0009
        L_0x006d:
            if (r7 == 0) goto L_0x0079
        L_0x006f:
            boolean r0 = r7.A00()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0079
        L_0x0077:
            r0 = 2
            goto L_0x0009
        L_0x0079:
            r11 = 0
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C51262Tn.A02(X.1M2, X.1v4, X.0oU, X.2Et, java.lang.Boolean, java.lang.Integer, int, boolean):void");
    }

    public static boolean A03(String str) {
        String queryParameter;
        if (TextUtils.isEmpty(str) || (queryParameter = Uri.parse(str).getQueryParameter("src")) == null || !queryParameter.equals("qr")) {
            return false;
        }
        return true;
    }
}
