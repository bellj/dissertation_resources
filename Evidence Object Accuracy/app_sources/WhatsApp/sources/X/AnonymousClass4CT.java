package X;

/* renamed from: X.4CT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CT extends IllegalStateException {
    public Throwable cause;

    public AnonymousClass4CT(String str, Throwable th) {
        super(str);
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
