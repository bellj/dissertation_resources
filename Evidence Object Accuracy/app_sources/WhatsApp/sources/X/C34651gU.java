package X;

/* renamed from: X.1gU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34651gU extends AnonymousClass1JQ implements AbstractC34381g3, AbstractC34531gI {
    public final AbstractC14640lm A00;
    public final boolean A01;

    public C34651gU(AbstractC14640lm r9, long j, boolean z) {
        this(null, r9, null, j, z, false);
    }

    public C34651gU(AnonymousClass1JR r10, AbstractC14640lm r11, String str, long j, boolean z, boolean z2) {
        super(C27791Jf.A03, r10, str, "regular_low", 5, j, z2);
        this.A00 = r11;
        this.A01 = z;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass1G4 A0T = C34641gT.A02.A0T();
        boolean z = this.A01;
        A0T.A03();
        C34641gT r1 = (C34641gT) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = z;
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        A01.A03();
        C27831Jk r12 = (C27831Jk) A01.A00;
        r12.A0F = (C34641gT) A0T.A02();
        r12.A00 |= 16;
        return A01;
    }

    @Override // X.AbstractC34381g3
    public AbstractC14640lm ABH() {
        return this.A00;
    }

    @Override // X.AbstractC34531gI
    public boolean AKD() {
        return !this.A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("PinChatMutation{rowId=");
        sb.append(this.A07);
        sb.append(", chatJid=");
        sb.append(this.A00);
        sb.append(", isPinned=");
        sb.append(this.A01);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
