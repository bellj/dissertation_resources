package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.3Cf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63603Cf {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3BE A01;

    public C63603Cf(AnonymousClass3BE r10, AnonymousClass3BG r11, String str) {
        C41141sy r2 = new C41141sy("fds");
        if (AnonymousClass3JT.A0E(str, 0, 9007199254740991L, false)) {
            C41141sy.A01(r2, "config", str);
        }
        r2.A05(r11.A00);
        r2.A07(r10.A00, C12960it.A0l());
        this.A01 = r10;
        this.A00 = r2.A03();
    }

    public void A00(C41141sy r4, List list) {
        r4.A09(this.A00, list, C12960it.A0l());
        ArrayList A0x = C12980iv.A0x(Arrays.asList(new String[0]));
        A0x.addAll(0, list);
        AnonymousClass3BE r0 = this.A01;
        if (r0 != null) {
            r4.A09(r0.A00, A0x, C12960it.A0l());
        }
    }
}
