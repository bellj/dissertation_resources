package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57572nH extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57572nH A08;
    public static volatile AnonymousClass255 A09;
    public int A00;
    public long A01;
    public long A02;
    public C27081Fy A03;
    public AnonymousClass2n0 A04;
    public C57622nM A05;
    public String A06 = "";
    public String A07 = "";

    static {
        C57572nH r0 = new C57572nH();
        A08 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C82403vZ r2;
        C82393vY r22;
        AnonymousClass1G3 r23;
        switch (r15.ordinal()) {
            case 0:
                return A08;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57572nH r1 = (C57572nH) obj2;
                this.A03 = (C27081Fy) r7.Aft(this.A03, r1.A03);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                String str = this.A06;
                int i2 = r1.A00;
                this.A06 = r7.Afy(str, r1.A06, A1V, C12960it.A1V(i2 & 2, 2));
                this.A01 = r7.Afs(this.A01, r1.A01, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A07 = r7.Afy(this.A07, r1.A07, C12960it.A1V(i & 8, 8), C12960it.A1V(i2 & 8, 8));
                this.A02 = r7.Afs(this.A02, r1.A02, C12960it.A1V(i & 16, 16), C12960it.A1V(i2 & 16, 16));
                this.A04 = (AnonymousClass2n0) r7.Aft(this.A04, r1.A04);
                this.A05 = (C57622nM) r7.Aft(this.A05, r1.A05);
                if (r7 == C463025i.A00) {
                    this.A00 |= r1.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r72.A0A();
                            this.A00 |= 2;
                            this.A06 = A0A;
                        } else if (A03 == 16) {
                            this.A00 |= 4;
                            this.A01 = r72.A06();
                        } else if (A03 == 26) {
                            String A0A2 = r72.A0A();
                            this.A00 |= 8;
                            this.A07 = A0A2;
                        } else if (A03 == 34) {
                            if ((this.A00 & 1) == 1) {
                                r23 = (AnonymousClass1G3) this.A03.A0T();
                            } else {
                                r23 = null;
                            }
                            C27081Fy r0 = (C27081Fy) AbstractC27091Fz.A0H(r72, r12, C27081Fy.A0i);
                            this.A03 = r0;
                            if (r23 != null) {
                                this.A03 = (C27081Fy) AbstractC27091Fz.A0C(r23, r0);
                            }
                            this.A00 |= 1;
                        } else if (A03 == 40) {
                            this.A00 |= 16;
                            this.A02 = r72.A06();
                        } else if (A03 == 50) {
                            if ((this.A00 & 32) == 32) {
                                r22 = (C82393vY) this.A04.A0T();
                            } else {
                                r22 = null;
                            }
                            AnonymousClass2n0 r02 = (AnonymousClass2n0) AbstractC27091Fz.A0H(r72, r12, AnonymousClass2n0.A04);
                            this.A04 = r02;
                            if (r22 != null) {
                                this.A04 = (AnonymousClass2n0) AbstractC27091Fz.A0C(r22, r02);
                            }
                            this.A00 |= 32;
                        } else if (A03 == 58) {
                            if ((this.A00 & 64) == 64) {
                                r2 = (C82403vZ) this.A05.A0T();
                            } else {
                                r2 = null;
                            }
                            C57622nM r03 = (C57622nM) AbstractC27091Fz.A0H(r72, r12, C57622nM.A0B);
                            this.A05 = r03;
                            if (r2 != null) {
                                this.A05 = (C57622nM) AbstractC27091Fz.A0C(r2, r03);
                            }
                            this.A00 |= 64;
                        } else if (!A0a(r72, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57572nH();
            case 5:
                return new C82323vR();
            case 6:
                break;
            case 7:
                if (A09 == null) {
                    synchronized (C57572nH.class) {
                        if (A09 == null) {
                            A09 = AbstractC27091Fz.A09(A08);
                        }
                    }
                }
                return A09;
            default:
                throw C12970iu.A0z();
        }
        return A08;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(1, this.A06, 0);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A06(2, this.A01);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(3, this.A07, i2);
        }
        if ((this.A00 & 1) == 1) {
            C27081Fy r0 = this.A03;
            if (r0 == null) {
                r0 = C27081Fy.A0i;
            }
            i2 = AbstractC27091Fz.A08(r0, 4, i2);
        }
        int i4 = this.A00;
        if ((i4 & 16) == 16) {
            i2 += CodedOutputStream.A05(5, this.A02);
        }
        if ((i4 & 32) == 32) {
            AnonymousClass2n0 r02 = this.A04;
            if (r02 == null) {
                r02 = AnonymousClass2n0.A04;
            }
            i2 = AbstractC27091Fz.A08(r02, 6, i2);
        }
        if ((this.A00 & 64) == 64) {
            C57622nM r03 = this.A05;
            if (r03 == null) {
                r03 = C57622nM.A0B;
            }
            i2 = AbstractC27091Fz.A08(r03, 7, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(1, this.A06);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0H(2, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(3, this.A07);
        }
        if ((this.A00 & 1) == 1) {
            C27081Fy r0 = this.A03;
            if (r0 == null) {
                r0 = C27081Fy.A0i;
            }
            codedOutputStream.A0L(r0, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0H(5, this.A02);
        }
        if ((this.A00 & 32) == 32) {
            AnonymousClass2n0 r02 = this.A04;
            if (r02 == null) {
                r02 = AnonymousClass2n0.A04;
            }
            codedOutputStream.A0L(r02, 6);
        }
        if ((this.A00 & 64) == 64) {
            C57622nM r03 = this.A05;
            if (r03 == null) {
                r03 = C57622nM.A0B;
            }
            codedOutputStream.A0L(r03, 7);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
