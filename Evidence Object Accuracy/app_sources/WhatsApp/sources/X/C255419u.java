package X;

/* renamed from: X.19u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255419u {
    public final AnonymousClass1Q5 A00;

    public C255419u(C14830m7 r8, C16120oU r9, C21230x5 r10, AbstractC21180x0 r11) {
        AnonymousClass1Q5 r0 = new AnonymousClass1Q5(r8, r9, r10, r11, "StarMessagePerfTracker", 703933362);
        this.A00 = r0;
        r0.A06.A03 = true;
    }

    public void A00(int i, int i2) {
        AnonymousClass1Q5 r3 = this.A00;
        r3.A0D("StarMessagePerfTracker", -1);
        r3.A0A("origin", String.valueOf(i), true);
        r3.A0A("num_messages", String.valueOf(i2), true);
    }
}
