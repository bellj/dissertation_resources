package X;

import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Size;
import android.util.SizeF;
import android.util.SparseArray;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.07E  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass07E {
    public static final Class[] A04;
    public final AnonymousClass05A A00;
    public final Map A01;
    public final Map A02;
    public final Map A03;

    static {
        Class cls;
        Class[] clsArr = new Class[29];
        clsArr[0] = Boolean.TYPE;
        clsArr[1] = boolean[].class;
        clsArr[2] = Double.TYPE;
        clsArr[3] = double[].class;
        Class<SizeF> cls2 = Integer.TYPE;
        clsArr[4] = cls2;
        clsArr[5] = int[].class;
        clsArr[6] = Long.TYPE;
        clsArr[7] = long[].class;
        clsArr[8] = String.class;
        clsArr[9] = String[].class;
        clsArr[10] = Binder.class;
        clsArr[11] = Bundle.class;
        clsArr[12] = Byte.TYPE;
        clsArr[13] = byte[].class;
        clsArr[14] = Character.TYPE;
        clsArr[15] = char[].class;
        clsArr[16] = CharSequence.class;
        clsArr[17] = CharSequence[].class;
        clsArr[18] = ArrayList.class;
        clsArr[19] = Float.TYPE;
        clsArr[20] = float[].class;
        clsArr[21] = Parcelable.class;
        clsArr[22] = Parcelable[].class;
        clsArr[23] = Serializable.class;
        clsArr[24] = Short.TYPE;
        clsArr[25] = short[].class;
        clsArr[26] = SparseArray.class;
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            cls = Size.class;
        } else {
            cls = cls2;
        }
        clsArr[27] = cls;
        if (i >= 21) {
            cls2 = SizeF.class;
        }
        clsArr[28] = cls2;
        A04 = clsArr;
    }

    public AnonymousClass07E() {
        this.A03 = new HashMap();
        this.A01 = new HashMap();
        this.A00 = new C018708u(this);
        this.A02 = new HashMap();
    }

    public AnonymousClass07E(Map map) {
        this.A03 = new HashMap();
        this.A01 = new HashMap();
        this.A00 = new C018708u(this);
        this.A02 = new HashMap(map);
    }

    public static AnonymousClass07E A00(Bundle bundle, Bundle bundle2) {
        if (bundle == null && bundle2 == null) {
            return new AnonymousClass07E();
        }
        HashMap hashMap = new HashMap();
        if (bundle2 != null) {
            for (String str : bundle2.keySet()) {
                hashMap.put(str, bundle2.get(str));
            }
        }
        if (bundle != null) {
            ArrayList parcelableArrayList = bundle.getParcelableArrayList("keys");
            ArrayList parcelableArrayList2 = bundle.getParcelableArrayList("values");
            if (parcelableArrayList == null || parcelableArrayList2 == null || parcelableArrayList.size() != parcelableArrayList2.size()) {
                throw new IllegalStateException("Invalid bundle passed as restored state");
            }
            for (int i = 0; i < parcelableArrayList.size(); i++) {
                hashMap.put(parcelableArrayList.get(i), parcelableArrayList2.get(i));
            }
        }
        return new AnonymousClass07E(hashMap);
    }

    public AnonymousClass016 A01(Object obj, String str) {
        Map map = this.A01;
        AnonymousClass016 r0 = (AnonymousClass016) map.get(str);
        if (r0 != null) {
            return r0;
        }
        Map map2 = this.A02;
        if (map2.containsKey(str)) {
            obj = map2.get(str);
        }
        C018808v r02 = new C018808v(this, obj, str);
        map.put(str, r02);
        return r02;
    }

    public AnonymousClass016 A02(String str) {
        Map map = this.A01;
        AnonymousClass016 r1 = (AnonymousClass016) map.get(str);
        if (r1 == null) {
            Map map2 = this.A02;
            if (map2.containsKey(str)) {
                r1 = new C018808v(this, map2.get(str), str);
            } else {
                r1 = new C018808v(this, str);
            }
            map.put(str, r1);
        }
        return r1;
    }

    public void A03(String str) {
        this.A02.remove(str);
        C018808v r1 = (C018808v) this.A01.remove(str);
        if (r1 != null) {
            r1.A00 = null;
        }
    }

    public void A04(String str, Object obj) {
        if (obj != null) {
            for (Class cls : A04) {
                if (!cls.isInstance(obj)) {
                }
            }
            StringBuilder sb = new StringBuilder("Can't put value with type ");
            sb.append(obj.getClass());
            sb.append(" into saved state");
            throw new IllegalArgumentException(sb.toString());
        }
        AnonymousClass017 r0 = (AnonymousClass017) this.A01.get(str);
        if (r0 != null) {
            r0.A0B(obj);
        } else {
            this.A02.put(str, obj);
        }
    }
}
