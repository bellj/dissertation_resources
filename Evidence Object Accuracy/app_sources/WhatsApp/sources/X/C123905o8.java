package X;

import android.content.Intent;
import android.view.View;
import com.whatsapp.util.Log;

/* renamed from: X.5o8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123905o8 extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AbstractView$OnClickListenerC121485iL A00;

    public C123905o8(AbstractView$OnClickListenerC121485iL r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AbstractView$OnClickListenerC121485iL r3 = this.A00;
        Intent ACd = r3.A0N.A07.A02().ACd(r3, "personal", "FB");
        if (ACd == null) {
            Log.e("PAY: BrazilPaymentAccountActionsContainerPresenter/onRequestPaymentInfoSelected - Invalid dyi report intent");
        } else {
            r3.startActivity(ACd);
        }
    }
}
