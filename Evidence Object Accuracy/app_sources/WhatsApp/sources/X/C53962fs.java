package X;

import com.whatsapp.group.GroupSettingsActivity;
import com.whatsapp.group.GroupSettingsViewModel;

/* renamed from: X.2fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53962fs extends AnonymousClass0Yo {
    public final /* synthetic */ GroupSettingsActivity A00;

    public C53962fs(GroupSettingsActivity groupSettingsActivity) {
        this.A00 = groupSettingsActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(GroupSettingsViewModel.class)) {
            GroupSettingsActivity groupSettingsActivity = this.A00;
            return new GroupSettingsViewModel(groupSettingsActivity.A01, ((ActivityC13830kP) groupSettingsActivity).A05);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
