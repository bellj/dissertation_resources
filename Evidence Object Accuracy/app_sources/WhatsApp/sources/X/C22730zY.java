package X;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.ConditionVariable;
import android.os.Environment;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0zY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22730zY implements AbstractC20260vT {
    public int A00;
    public int A01 = 0;
    public int A02 = 0;
    public WifiManager.WifiLock A03;
    public boolean A04 = false;
    public boolean A05 = false;
    public boolean A06 = false;
    public boolean A07 = false;
    public boolean A08 = false;
    public boolean A09 = false;
    public final ConditionVariable A0A = new ConditionVariable(false);
    public final ConditionVariable A0B = new ConditionVariable(false);
    public final ConditionVariable A0C = new ConditionVariable(false);
    public final ConditionVariable A0D = new ConditionVariable(false);
    public final ConditionVariable A0E = new ConditionVariable(false);
    public final ConditionVariable A0F = new ConditionVariable(false);
    public final ConditionVariable A0G = new ConditionVariable(false);
    public final C15570nT A0H;
    public final C20640w5 A0I;
    public final C15450nH A0J;
    public final C16240og A0K;
    public final AnonymousClass10N A0L;
    public final AbstractC44761zV A0M = new C44751zU(this);
    public final C19380u1 A0N;
    public final C18280sC A0O;
    public final C18640sm A0P;
    public final AnonymousClass01d A0Q;
    public final C16590pI A0R;
    public final C14820m6 A0S;
    public final C16490p7 A0T;
    public final C17220qS A0U;
    public final C16630pM A0V;
    public final C22660zR A0W;
    public final C15510nN A0X;
    public final AbstractC14440lR A0Y;
    public final AtomicBoolean A0Z = new AtomicBoolean(false);
    public final AtomicBoolean A0a = new AtomicBoolean(false);
    public final AtomicBoolean A0b = new AtomicBoolean(false);
    public final AtomicBoolean A0c = new AtomicBoolean(false);
    public final AtomicBoolean A0d = new AtomicBoolean(false);
    public final AtomicBoolean A0e = new AtomicBoolean(false);
    public final AtomicBoolean A0f = new AtomicBoolean(false);
    public final AtomicBoolean A0g = new AtomicBoolean(false);
    public final AtomicBoolean A0h = new AtomicBoolean(false);
    public final AtomicBoolean A0i = new AtomicBoolean(false);
    public final AtomicBoolean A0j = new AtomicBoolean(false);
    public final AtomicBoolean A0k = new AtomicBoolean(false);

    public C22730zY(C15570nT r3, C20640w5 r4, C15450nH r5, C16240og r6, AnonymousClass10N r7, C19380u1 r8, C18280sC r9, C18640sm r10, AnonymousClass01d r11, C16590pI r12, C14820m6 r13, C16490p7 r14, C17220qS r15, C16630pM r16, C22660zR r17, C15510nN r18, AbstractC14440lR r19) {
        this.A0R = r12;
        this.A0H = r3;
        this.A0Y = r19;
        this.A0I = r4;
        this.A0J = r5;
        this.A0W = r17;
        this.A0U = r15;
        this.A0N = r8;
        this.A0Q = r11;
        this.A0K = r6;
        this.A0T = r14;
        this.A0S = r13;
        this.A0O = r9;
        this.A0X = r18;
        this.A0V = r16;
        this.A0P = r10;
        this.A0L = r7;
    }

    public void A00() {
        if (this.A03 == null) {
            AnonymousClass01d r3 = this.A0Q;
            WifiManager wifiManager = r3.A0E;
            if (wifiManager == null) {
                wifiManager = (WifiManager) r3.A0R("wifi", false);
                r3.A0E = wifiManager;
                if (wifiManager == null) {
                    Log.w("gdrive-conditions-manager/create-wifi-lock wm=null");
                }
            }
            WifiManager.WifiLock createWifiLock = wifiManager.createWifiLock(1, "backup-lock");
            this.A03 = createWifiLock;
            createWifiLock.setReferenceCounted(false);
        }
        WifiManager.WifiLock wifiLock = this.A03;
        if (wifiLock != null && !wifiLock.isHeld()) {
            this.A03.acquire();
        }
    }

    public void A01() {
        this.A04 = true;
        this.A0Y.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 23));
    }

    public void A02() {
        this.A0S.A00.edit().putString("gdrive_media_restore_network_setting", String.valueOf(1)).apply();
        A04();
        this.A0Y.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 23));
    }

    public void A03() {
        StringBuilder sb;
        boolean z;
        String obj;
        AnonymousClass009.A00();
        C14820m6 r5 = this.A0S;
        if (C44771zW.A0H(r5)) {
            AtomicBoolean atomicBoolean = this.A0e;
            if (!atomicBoolean.get()) {
                A08(Environment.getExternalStorageState());
                A04();
                A06();
                AtomicBoolean atomicBoolean2 = this.A0i;
                if (!atomicBoolean2.get() || !this.A07 || !this.A09) {
                    sb = new StringBuilder("gdrive-conditions-manager/trigger-nothing media-restore-pending: ");
                    sb.append(C44771zW.A0H(r5));
                    sb.append(" media-restore-running: ");
                    sb.append(atomicBoolean.get());
                    sb.append(" network_available_for_media_restore: ");
                    sb.append(atomicBoolean2.get());
                    sb.append(" battery_available_for_media_restore: ");
                    z = this.A07;
                    sb.append(z);
                    sb.append(" sdcard_available: ");
                    sb.append(this.A09);
                    obj = sb.toString();
                    Log.i(obj);
                }
                Context context = this.A0R.A00;
                AnonymousClass1Tv.A00(context, C14960mK.A0U(context, "action_restore_media"));
                obj = "gdrive-conditions-manager/trigger-pending-media-restore";
                Log.i(obj);
            }
        }
        if (C44771zW.A0G(r5)) {
            AtomicBoolean atomicBoolean3 = this.A0c;
            if (!atomicBoolean3.get()) {
                A08(Environment.getExternalStorageState());
                A04();
                A06();
                AtomicBoolean atomicBoolean4 = this.A0g;
                if (!atomicBoolean4.get() || !this.A05 || !this.A09) {
                    sb = new StringBuilder("gdrive-conditions-manager/trigger-nothing is-backup-pending: ");
                    sb.append(C44771zW.A0G(r5));
                    sb.append(" is-backup-running: ");
                    sb.append(atomicBoolean3.get());
                    sb.append(" network_available_for_backup: ");
                    sb.append(atomicBoolean4.get());
                    sb.append(" battery_available_for_backup: ");
                    z = this.A05;
                    sb.append(z);
                    sb.append(" sdcard_available: ");
                    sb.append(this.A09);
                    obj = sb.toString();
                    Log.i(obj);
                }
                Context context2 = this.A0R.A00;
                Intent A0U = C14960mK.A0U(context2, "action_backup");
                A0U.putExtra("only_if_pending", true);
                AnonymousClass1Tv.A00(context2, A0U);
                obj = "gdrive-conditions-manager/trigger-pending-backup";
                Log.i(obj);
            }
        }
        if (this.A0j.get() || this.A0k.get()) {
            Log.i("gdrive-conditions-manager/service-running/recalculate-network-and-sdcard");
            A08(Environment.getExternalStorageState());
            A04();
            A06();
            return;
        }
        obj = "gdrive-conditions-manager/trigger-nothing/nothing-pending";
        Log.i(obj);
    }

    public void A04() {
        int i;
        try {
            C14820m6 r1 = this.A0S;
            this.A01 = r1.A02();
            try {
                i = Integer.parseInt(r1.A00.getString("gdrive_media_restore_network_setting", String.valueOf(0)));
            } catch (NumberFormatException e) {
                Log.e("wa-shared-preferences/get-media-restore-network-setting", e);
                i = 0;
            }
            this.A02 = i;
        } catch (NumberFormatException e2) {
            Log.e(e2);
        }
    }

    public void A05() {
        WifiManager.WifiLock wifiLock = this.A03;
        if (wifiLock != null && wifiLock.isHeld()) {
            this.A03.release();
        }
    }

    public synchronized void A06() {
        boolean z;
        AtomicBoolean atomicBoolean;
        boolean z2;
        AtomicBoolean atomicBoolean2;
        AtomicBoolean atomicBoolean3;
        boolean z3;
        boolean z4;
        AnonymousClass009.A00();
        C18640sm r0 = this.A0P;
        boolean z5 = true;
        int A05 = r0.A05(true);
        this.A00 = A05;
        boolean z6 = false;
        if (A05 == 0) {
            Log.i("gdrive-conditions-manager/can-use-network/active_network/none");
            this.A0E.close();
            this.A0F.close();
            this.A0D.close();
            atomicBoolean = this.A0h;
            boolean andSet = atomicBoolean.getAndSet(false);
            atomicBoolean2 = this.A0i;
            z = andSet | atomicBoolean2.getAndSet(false);
            atomicBoolean3 = this.A0g;
            z2 = atomicBoolean3.getAndSet(false);
        } else if (A05 == 1) {
            AnonymousClass1I0 A07 = r0.A07();
            if (A07 == null) {
                Log.i("gdrive-conditions-manager/can-use-network/active_network/wifi active network info is null, no connection");
                this.A0E.close();
                this.A0F.close();
                this.A0D.close();
                atomicBoolean = this.A0h;
                boolean andSet2 = atomicBoolean.getAndSet(false);
                atomicBoolean2 = this.A0i;
                z = andSet2 | atomicBoolean2.getAndSet(false);
                atomicBoolean3 = this.A0g;
                z2 = atomicBoolean3.getAndSet(false);
            } else if (!A07.A06 || !C18640sm.A02()) {
                Log.i("gdrive-conditions-manager/can-use-network/active_network/wifi");
                this.A0E.open();
                this.A0F.open();
                this.A0D.open();
                atomicBoolean = this.A0h;
                boolean z7 = false;
                if (!atomicBoolean.getAndSet(true)) {
                    z7 = true;
                }
                atomicBoolean2 = this.A0i;
                if (!atomicBoolean2.getAndSet(true)) {
                    z6 = true;
                }
                z = z7 | z6;
                atomicBoolean3 = this.A0g;
                z3 = atomicBoolean3.getAndSet(true);
                z2 = true ^ z3;
            } else {
                Log.i("gdrive-conditions-manager/can-use-network/active_network/wifi/captive");
                this.A0E.close();
                this.A0F.close();
                this.A0D.close();
                atomicBoolean = this.A0h;
                boolean andSet3 = atomicBoolean.getAndSet(false);
                atomicBoolean2 = this.A0i;
                z = andSet3 | atomicBoolean2.getAndSet(false);
                atomicBoolean3 = this.A0g;
                z2 = atomicBoolean3.getAndSet(false);
            }
        } else if (A05 == 2) {
            Log.i("gdrive-conditions-manager/can-use-network/active_network/cellular");
            this.A0E.open();
            atomicBoolean = this.A0h;
            boolean z8 = false;
            if (!atomicBoolean.getAndSet(true)) {
                z8 = true;
            }
            if (this.A04 || this.A01 == 1) {
                this.A0D.open();
                atomicBoolean3 = this.A0g;
                z4 = !atomicBoolean3.getAndSet(true);
            } else {
                this.A0D.close();
                atomicBoolean3 = this.A0g;
                z4 = atomicBoolean3.getAndSet(false);
            }
            z = z8 | z4;
            if (this.A02 == 1) {
                this.A0F.open();
                atomicBoolean2 = this.A0i;
                z3 = atomicBoolean2.getAndSet(true);
                z2 = true ^ z3;
            } else {
                this.A0F.close();
                atomicBoolean2 = this.A0i;
                z2 = atomicBoolean2.getAndSet(false);
            }
        } else if (A05 == 3) {
            Log.i("gdrive-conditions-manager/can-use-network/active_network/roaming");
            this.A0E.open();
            this.A0F.close();
            this.A0D.close();
            atomicBoolean = this.A0h;
            if (atomicBoolean.getAndSet(true)) {
                z5 = false;
            }
            atomicBoolean2 = this.A0i;
            z = atomicBoolean2.getAndSet(false) | z5;
            atomicBoolean3 = this.A0g;
            z2 = atomicBoolean3.getAndSet(false);
        }
        if (z || z2) {
            StringBuilder sb = new StringBuilder();
            sb.append("gdrive-conditions-manager/can-use-network/message-restore/");
            sb.append(atomicBoolean.get());
            Log.i(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("gdrive-conditions-manager/can-use-network/media-restore/");
            sb2.append(atomicBoolean2.get());
            Log.i(sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("gdrive-conditions-manager/can-use-network/backup/");
            sb3.append(atomicBoolean3.get());
            Log.i(sb3.toString());
        }
    }

    public void A07(C32441cA r7) {
        int i;
        double A00 = r7.A00();
        boolean z = false;
        if (!Double.isNaN(A00)) {
            i = (int) A00;
        } else {
            i = 0;
        }
        this.A0B.open();
        boolean z2 = true;
        this.A06 = true;
        if (this.A08 || r7.A01()) {
            this.A0A.open();
            this.A0C.open();
            if (!this.A05 || !this.A07) {
                z = true;
            }
            this.A05 = true;
            this.A07 = true;
            z2 = z;
        } else {
            this.A0A.close();
            this.A0C.close();
            if (!this.A05 && !this.A07) {
                z2 = false;
            }
            this.A05 = false;
            this.A07 = false;
        }
        if (z2) {
            StringBuilder sb = new StringBuilder("gdrive-conditions-manager/can-use-battery/battery-level/");
            sb.append(i);
            Log.i(sb.toString());
            StringBuilder sb2 = new StringBuilder("gdrive-conditions-manager/can-use-battery-for-backup/");
            sb2.append(this.A05);
            Log.i(sb2.toString());
            StringBuilder sb3 = new StringBuilder("gdrive-conditions-manager/can-use-battery-for-media-restore/");
            sb3.append(this.A07);
            Log.i(sb3.toString());
            StringBuilder sb4 = new StringBuilder("gdrive-conditions-manager/ignore-battery-status/");
            sb4.append(this.A08);
            Log.i(sb4.toString());
        }
    }

    public void A08(String str) {
        AnonymousClass009.A00();
        boolean equals = "mounted".equals(str);
        ConditionVariable conditionVariable = this.A0G;
        if (equals) {
            conditionVariable.open();
            if (!this.A09) {
                this.A09 = true;
                if (A09()) {
                    A03();
                    return;
                }
                return;
            }
            return;
        }
        conditionVariable.close();
        this.A09 = false;
    }

    public boolean A09() {
        if (!this.A0J.A05(AbstractC15460nI.A0W)) {
            Log.i("gdrive-conditions-manager/is-access-possible gdrive disabled");
            return false;
        }
        Context context = this.A0R.A00;
        int A00 = AnonymousClass1UB.A00(context);
        if (A00 == 0) {
            return true;
        }
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            StringBuilder sb = new StringBuilder();
            sb.append("gdrive-conditions-manager/is-access-possible Google Play services are missing and can be installed,  status code: ");
            sb.append(C44771zW.A03(A00));
            Log.i(sb.toString());
            return true;
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("gdrive-conditions-manager/is-access-possible Google Play services are missing and cannot be installed, status code: ");
            sb2.append(C44771zW.A03(A00));
            Log.i(sb2.toString());
            Log.i("gdrive-conditions-manager/is-access-possible", e);
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006f, code lost:
        if (r4.A09() == null) goto L_0x0071;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0A() {
        /*
            r10 = this;
            X.0nT r0 = r10.A0H
            r0.A08()
            r9 = 0
            boolean r0 = r10.A09()
            if (r0 != 0) goto L_0x0012
            java.lang.String r0 = "gdrive-conditions-manager/should-start/false/gdrive-access-not-allowed"
        L_0x000e:
            com.whatsapp.util.Log.i(r0)
        L_0x0011:
            return r9
        L_0x0012:
            X.0nN r0 = r10.A0X
            boolean r0 = r0.A01()
            if (r0 != 0) goto L_0x001d
            java.lang.String r0 = "gdrive-conditions-manager/should-start/false/reg-not-verified"
            goto L_0x000e
        L_0x001d:
            X.0zR r0 = r10.A0W
            boolean r0 = r0.A00
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = "gdrive-conditions-manager/should-start/false/login-failed"
            goto L_0x000e
        L_0x0026:
            X.0w5 r1 = r10.A0I
            boolean r0 = r1.A03()
            if (r0 == 0) goto L_0x0031
            java.lang.String r0 = "gdrive-conditions-manager/should-start/false/clock-wrong"
            goto L_0x000e
        L_0x0031:
            boolean r0 = r1.A02()
            if (r0 == 0) goto L_0x003a
            java.lang.String r0 = "gdrive-conditions-manager/should-start/false/software-expired"
            goto L_0x000e
        L_0x003a:
            X.0p7 r0 = r10.A0T
            int r0 = r0.A00()
            r8 = 1
            if (r0 > r8) goto L_0x0046
            java.lang.String r0 = "gdrive-conditions-manager/should-start/false/message-count-low"
            goto L_0x000e
        L_0x0046:
            X.0m6 r4 = r10.A0S
            int r2 = r4.A01()
            if (r2 == 0) goto L_0x0071
            if (r2 == r8) goto L_0x006b
            r0 = 2
            if (r2 == r0) goto L_0x006b
            r0 = 3
            if (r2 == r0) goto L_0x006b
            r0 = 4
            if (r2 == r0) goto L_0x006b
            java.lang.String r1 = "gdrive-conditions-manager/should-start/unexpected-backup-freq/"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
            goto L_0x0071
        L_0x006b:
            java.lang.String r0 = r4.A09()
            if (r0 != 0) goto L_0x0011
        L_0x0071:
            java.lang.String r3 = "gdrive_next_prompt_for_setup_timestamp"
            r1 = -1
            android.content.SharedPreferences r0 = r4.A00     // Catch: NumberFormatException -> 0x00a4
            long r6 = r0.getLong(r3, r1)     // Catch: NumberFormatException -> 0x00a4
            long r4 = java.lang.System.currentTimeMillis()
            java.util.Locale r3 = java.util.Locale.ENGLISH
            r0 = 3
            java.lang.Object[] r2 = new java.lang.Object[r0]
            java.lang.Long r0 = java.lang.Long.valueOf(r6)
            r2[r9] = r0
            java.lang.Long r0 = java.lang.Long.valueOf(r4)
            r2[r8] = r0
            r1 = 2
            long r4 = r4 - r6
            java.lang.Long r0 = java.lang.Long.valueOf(r4)
            r2[r1] = r0
            java.lang.String r0 = "saved time: %d, current time: %d, difference: %d"
            java.lang.String.format(r3, r0, r2)
            r1 = 0
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x00b4
            goto L_0x00ab
        L_0x00a4:
            r1 = move-exception
            java.lang.String r0 = "gdrive-conditions-manager/sufficient-time-passed-since-last-user-prompt/"
            com.whatsapp.util.Log.e(r0, r1)
            goto L_0x00b4
        L_0x00ab:
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0011
            java.lang.String r0 = "gdrive-conditions-manager/sufficient-time-passed-since-last-user-prompt/true"
            com.whatsapp.util.Log.i(r0)
        L_0x00b4:
            r9 = 1
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22730zY.A0A():boolean");
    }

    public boolean A0B() {
        String str;
        if (this.A0c.get() || this.A0d.get()) {
            if (this.A0D.block(1800000)) {
                return true;
            }
            str = "gdrive-conditions-manager/network-wait/backup 1800000 milliseconds, giving up now.";
        } else if (this.A0e.get()) {
            if (this.A0F.block(1800000)) {
                return true;
            }
            Log.e("gdrive-conditions-manager/network-wait/media-restore 1800000 milliseconds, giving up now.");
            return true;
        } else if (this.A0E.block(1800000)) {
            return true;
        } else {
            str = "gdrive-conditions-manager/network-wait/message-restore 1800000 milliseconds, giving up now.";
        }
        Log.e(str);
        return false;
    }

    public boolean A0C(int i) {
        AnonymousClass009.A00();
        if (i == 0 || i == 1) {
            this.A01 = i;
            A06();
            this.A0S.A00.edit().putString("interface_gdrive_backup_network_setting", String.valueOf(i)).apply();
            return true;
        }
        StringBuilder sb = new StringBuilder("gdrive-conditions-manager/set-backup-network-setting/incorrect-value/");
        sb.append(i);
        Log.e(sb.toString());
        return false;
    }

    public boolean A0D(long j) {
        if (this.A0G.block(j)) {
            return true;
        }
        StringBuilder sb = new StringBuilder("gdrive-conditions-manager/sdcard-wait ");
        sb.append(j);
        sb.append(" milliseconds, giving up now.");
        Log.e(sb.toString());
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:111:0x022e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0E(X.C44781zX r19, X.C44791zY r20) {
        /*
        // Method dump skipped, instructions count: 602
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22730zY.A0E(X.1zX, X.1zY):boolean");
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r4) {
        this.A0Y.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 24));
    }
}
