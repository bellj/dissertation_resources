package X;

import java.util.List;

/* renamed from: X.3I0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3I0 {
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008c, code lost:
        if (r4 == 2) goto L_0x007a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AbstractC15340mz A00(X.AbstractC14640lm r23, com.whatsapp.jid.UserJid r24, X.C49692Lu r25, X.C20320vZ r26, java.lang.Long r27, java.lang.Long r28, java.lang.String r29, int r30, int r31, long r32, boolean r34, boolean r35) {
        /*
        // Method dump skipped, instructions count: 219
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3I0.A00(X.0lm, com.whatsapp.jid.UserJid, X.2Lu, X.0vZ, java.lang.Long, java.lang.Long, java.lang.String, int, int, long, boolean, boolean):X.0mz");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01bb, code lost:
        if (r25.A0b().A0b() == X.AnonymousClass39r.A04) goto L_0x01bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01bd, code lost:
        r15 = (X.AnonymousClass1XO) r26.A01(new X.AnonymousClass1IS(r23, r32, false), (byte) 30, r37);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x01cd, code lost:
        if (r25.A01 != 1) goto L_0x01e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x01cf, code lost:
        r2 = r25.A0b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01d6, code lost:
        if (r2.A01 != 5) goto L_0x01f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01d8, code lost:
        r0 = r2.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x01da, code lost:
        r0 = (X.C57672nR) r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01dc, code lost:
        r15.A19(r0, r40);
        A02(r24, r15, r27, r28, r30, r31, r33, r34, r35);
        r15 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x01e3, code lost:
        r2 = r25.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x01e5, code lost:
        if (r2 != null) goto L_0x01e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x01e7, code lost:
        r2 = X.AnonymousClass2Ly.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x01ec, code lost:
        if (r2.A01 != 5) goto L_0x01f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x01ee, code lost:
        r0 = r2.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x01f1, code lost:
        r0 = X.C57672nR.A0D;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x01fe, code lost:
        com.whatsapp.util.Log.e("HSMTemplateMessageUtil/buildFMessage/error cannot build any message.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0203, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0051, code lost:
        if (r0.A0b() == X.EnumC630139s.A03) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0054, code lost:
        r11 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x008b, code lost:
        if (r25.A0b().A0b() == X.AnonymousClass39r.A01) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r25.A0b().A0b() == X.AnonymousClass39r.A03) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x011e, code lost:
        if (r0.A0b() == X.EnumC630139s.A04) goto L_0x01bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x012c, code lost:
        if (r25.A0b().A0b() == X.AnonymousClass39r.A02) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x014d, code lost:
        if (r25.A0b().A0b() == X.AnonymousClass39r.A05) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x015a, code lost:
        if (r25.A0b().A0b() == X.AnonymousClass39r.A06) goto L_0x015c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x017e A[Catch: 1wi -> 0x01a6, TryCatch #0 {1wi -> 0x01a6, blocks: (B:91:0x015c, B:93:0x0165, B:94:0x0168, B:95:0x016b, B:97:0x016f, B:98:0x0171, B:100:0x0176, B:101:0x0178, B:102:0x017a, B:104:0x017e, B:105:0x018e, B:106:0x0195), top: B:134:0x007f }] */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0195 A[Catch: 1wi -> 0x01a6, TRY_LEAVE, TryCatch #0 {1wi -> 0x01a6, blocks: (B:91:0x015c, B:93:0x0165, B:94:0x0168, B:95:0x016b, B:97:0x016f, B:98:0x0171, B:100:0x0176, B:101:0x0178, B:102:0x017a, B:104:0x017e, B:105:0x018e, B:106:0x0195), top: B:134:0x007f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AbstractC15340mz A01(X.AbstractC14640lm r23, com.whatsapp.jid.UserJid r24, X.C49692Lu r25, X.C20320vZ r26, java.lang.Long r27, java.lang.Long r28, java.lang.String r29, java.lang.String r30, java.lang.String r31, java.lang.String r32, java.lang.String r33, java.util.List r34, int r35, int r36, long r37, boolean r39, boolean r40) {
        /*
        // Method dump skipped, instructions count: 516
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3I0.A01(X.0lm, com.whatsapp.jid.UserJid, X.2Lu, X.0vZ, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.List, int, int, long, boolean, boolean):X.0mz");
    }

    public static void A02(AbstractC14640lm r5, AbstractC15340mz r6, Long l, Long l2, String str, String str2, String str3, List list, int i) {
        r6.A0e(r5);
        if (l != null) {
            r6.A0c = l;
        }
        r6.A0E = i;
        ((AbstractC28871Pi) r6).Acz(new C28891Pk(l2, str, str2, str3, list));
    }
}
