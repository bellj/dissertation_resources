package X;

import android.content.Context;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.46x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC863546x extends AbstractC863646y {
    public AbstractC863546x(Context context) {
        super(context);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.search_attachment_height_regular));
    }
}
