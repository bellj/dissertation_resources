package X;

import java.util.Map;

/* renamed from: X.0rM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17770rM {
    public final C19630uQ A00;

    public AbstractC17770rM(C19630uQ r1) {
        this.A00 = r1;
    }

    public String A00() {
        if (this instanceof C17910rb) {
            return "native_flow_npci_common_library";
        }
        if (this instanceof C19660uT) {
            return "client_dasl_query";
        }
        if (this instanceof C19740ub) {
            return "open_bloks_screen_static";
        }
        if (!(this instanceof C19710uY)) {
            return !(this instanceof C19760ud) ? "dismiss_bottom_sheet" : "send_fds_iq";
        }
        return "open_bloks_screen_graphql";
    }

    public void A01() {
        if (this instanceof C19740ub) {
            C19740ub r1 = (C19740ub) this;
            r1.A01.A00().A03(r1);
        } else if (this instanceof C19710uY) {
            C19710uY r12 = (C19710uY) this;
            r12.A02.A00().A03(r12);
        }
    }

    public void A02() {
        if (this instanceof C19740ub) {
            C19740ub r3 = (C19740ub) this;
            r3.A01.A00().A00(new AbstractC50172Ok() { // from class: X.59v
                @Override // X.AbstractC50172Ok
                public final void APz(Object obj) {
                    C19740ub.A01(C19740ub.this, (AnonymousClass6E8) obj);
                }
            }, AnonymousClass6E8.class, r3);
        } else if (this instanceof C19710uY) {
            C19710uY r32 = (C19710uY) this;
            r32.A02.A00().A00(new AbstractC50172Ok() { // from class: X.59u
                @Override // X.AbstractC50172Ok
                public final void APz(Object obj) {
                    C19710uY.A00(C19710uY.this, (AnonymousClass6E8) obj);
                }
            }, AnonymousClass6E8.class, r32);
        }
    }

    public void A03(Map map) {
        C19640uR r0;
        if (this instanceof C17910rb) {
            C17910rb r1 = (C17910rb) this;
            AnonymousClass2DR r02 = r1.A00;
            C16700pc.A0C(r02);
            r02.A01(map);
            r1.A00 = null;
        } else if (!(this instanceof C19660uT)) {
            if (this instanceof C19740ub) {
                r0 = ((C19740ub) this).A00;
            } else if (this instanceof C19710uY) {
                r0 = ((C19710uY) this).A01;
            } else {
                return;
            }
            r0.A00(map);
        }
    }
}
