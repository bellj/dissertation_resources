package X;

import java.io.IOException;

/* renamed from: X.496  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass496 extends IOException {
    public AnonymousClass496() {
    }

    public AnonymousClass496(String str) {
        super(str);
    }

    public AnonymousClass496(String str, Throwable th) {
        super(str, th);
    }

    public AnonymousClass496(Throwable th) {
        super(th);
    }

    public static AnonymousClass496 A00(String str) {
        return new AnonymousClass496(str);
    }
}
