package X;

/* renamed from: X.0Fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02870Fe extends AbstractC05330Pd {
    public final /* synthetic */ C07740a0 A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C02870Fe(AnonymousClass0QN r1, C07740a0 r2) {
        super(r1);
        this.A00 = r2;
    }
}
