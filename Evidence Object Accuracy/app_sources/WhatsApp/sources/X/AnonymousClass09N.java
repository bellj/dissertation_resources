package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;

/* renamed from: X.09N  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09N extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ViewPropertyAnimator A01;
    public final /* synthetic */ AnonymousClass0FH A02;
    public final /* synthetic */ AnonymousClass03U A03;

    public AnonymousClass09N(View view, ViewPropertyAnimator viewPropertyAnimator, AnonymousClass0FH r3, AnonymousClass03U r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = view;
        this.A01 = viewPropertyAnimator;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A00.setAlpha(1.0f);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.setListener(null);
        AnonymousClass0FH r2 = this.A02;
        AnonymousClass03U r1 = this.A03;
        r2.A03(r1);
        r2.A00.remove(r1);
        if (!r2.A0B()) {
            r2.A02();
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
    }
}
