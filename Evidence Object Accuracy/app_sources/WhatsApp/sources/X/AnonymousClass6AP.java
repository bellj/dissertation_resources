package X;

import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.6AP  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AP implements AnonymousClass6MQ {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    public AnonymousClass6AP(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r3) {
        if (r3 != null && !AnonymousClass1ZS.A02(r3.A01)) {
            ((AbstractActivityC121665jA) this.A00).A06 = r3.A01;
        }
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r5) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m.A05(C12970iu.A0s(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C, C12960it.A0k("could not get payee name for jio: ")));
    }
}
