package X;

import android.text.TextUtils;

/* renamed from: X.5nA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123445nA extends AbstractC118075bE {
    public final AnonymousClass016 A00 = C12980iv.A0T();

    public C123445nA(C15450nH r2, C14830m7 r3, C15650ng r4, AbstractC16870pt r5, C20320vZ r6) {
        super(r2, r3, r4, r5, r6);
    }

    @Override // X.AbstractC118075bE
    public void A08(String str) {
        AnonymousClass016 r2;
        C127075tu r1;
        super.A08(str);
        String A03 = this.A02.A03(AbstractC15460nI.A2N);
        if (TextUtils.isEmpty(A03) || !C31001Zq.A09(str)) {
            boolean isEmpty = TextUtils.isEmpty(A03);
            r2 = this.A00;
            if (!isEmpty) {
                r1 = new C127075tu(A03, null);
            } else {
                r2.A0B(null);
                return;
            }
        } else {
            r2 = this.A00;
            r1 = new C127075tu(A03, str);
        }
        r2.A0B(r1);
    }
}
