package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.1Be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25881Be {
    public final C17070qD A00;

    public C25881Be(C17070qD r1) {
        this.A00 = r1;
    }

    public Intent A00(Context context, AnonymousClass1ZD r5, AnonymousClass1IS r6, String str) {
        Class AFc = this.A00.A02().AFc();
        if (AFc == null) {
            Log.e("PAY: PaymentIntents/getTransactionDetailsIntent -> transactionDetailClass is null");
            return null;
        }
        Intent intent = new Intent(context, AFc);
        if (str != null) {
            intent.putExtra("extra_transaction_id", str);
        }
        if (r6 != null) {
            C38211ni.A00(intent, r6);
        }
        if (r5 != null && !TextUtils.isEmpty(r5.A01)) {
            intent.putExtra("extra_payment_receipt_type", "non_native");
        }
        intent.setFlags(603979776);
        return intent;
    }
}
