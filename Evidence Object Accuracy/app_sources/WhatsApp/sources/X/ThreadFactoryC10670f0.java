package X;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0f0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ThreadFactoryC10670f0 implements ThreadFactory {
    public final AtomicInteger A00 = new AtomicInteger(0);
    public final /* synthetic */ C05180Oo A01;
    public final /* synthetic */ boolean A02;

    public ThreadFactoryC10670f0(C05180Oo r3, boolean z) {
        this.A01 = r3;
        this.A02 = z;
    }

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        String str;
        if (this.A02) {
            str = "WM.task-";
        } else {
            str = "androidx.work-";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.A00.incrementAndGet());
        return new Thread(runnable, sb.toString());
    }
}
