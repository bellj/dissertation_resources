package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1IA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IA extends ThreadPoolExecutor {
    public final String A00;

    public AnonymousClass1IA(String str, BlockingQueue blockingQueue, ThreadFactory threadFactory, TimeUnit timeUnit, int i, int i2, long j) {
        super(i, i2, j, timeUnit, blockingQueue, threadFactory);
        this.A00 = str;
    }
}
