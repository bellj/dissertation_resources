package X;

/* renamed from: X.1qw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40031qw extends AnonymousClass1JW {
    public final byte A00;
    public final C14850m9 A01;
    public final EnumC40021qv A02;
    public final C20320vZ A03;
    public final C30331Wz A04;
    public final String A05;

    public C40031qw(AbstractC15710nm r1, C15450nH r2, C14850m9 r3, EnumC40021qv r4, C20320vZ r5, C30331Wz r6, String str, byte b, boolean z) {
        super(r1, r2, z);
        this.A01 = r3;
        this.A04 = r6;
        this.A03 = r5;
        this.A05 = str;
        this.A02 = r4;
        this.A00 = b;
    }
}
