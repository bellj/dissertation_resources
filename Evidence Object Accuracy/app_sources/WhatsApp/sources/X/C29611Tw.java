package X;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1Tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29611Tw extends AbstractC16280ok {
    public final Context A00;
    public final C231410n A01;

    public C29611Tw(Context context, AbstractC15710nm r9, C231410n r10, C14850m9 r11) {
        super(context, r9, "web_sessions.db", null, 7, r11.A07(781));
        this.A00 = context;
        this.A01 = r10;
    }

    public static final void A00(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        String str3 = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select sql from sqlite_master where type='table' and name='");
            sb.append("sessions");
            sb.append("';");
            Cursor rawQuery = sQLiteDatabase.rawQuery(sb.toString(), null);
            if (rawQuery != null) {
                if (rawQuery.moveToNext()) {
                    str3 = rawQuery.getString(0);
                }
                rawQuery.close();
            }
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("websessionstore/schema ");
            sb2.append("sessions");
            Log.e(sb2.toString(), e);
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append(" ");
        sb3.append(str2);
        if (!str3.contains(sb3.toString())) {
            try {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("ALTER TABLE sessions ADD ");
                sb4.append(str);
                sb4.append(" ");
                sb4.append(str2);
                sQLiteDatabase.execSQL(sb4.toString());
            } catch (SQLiteException e2) {
                StringBuilder sb5 = new StringBuilder("WebSessionsStore/addColumnIfNotExists/alter_table ");
                sb5.append(str);
                Log.w(sb5.toString(), e2);
            }
        }
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        String str;
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteDatabaseCorruptException e) {
            Log.w("websessionstore/corrupt/removing", e);
            A04();
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteException e2) {
            String obj = e2.toString();
            if (obj.contains("file is encrypted")) {
                str = "websessionstore/encrypted/removing";
                Log.w(str);
                A04();
                return AnonymousClass1Tx.A01(super.A00(), this.A01);
            } else if (obj.contains("upgrade read-only database")) {
                Log.w("websessionstore/switching-to-writable");
                return AnonymousClass1Tx.A01(super.A00(), this.A01);
            } else {
                throw e2;
            }
        } catch (StackOverflowError e3) {
            Log.w("websessionstore/stackoverflowerror");
            for (StackTraceElement stackTraceElement : e3.getStackTrace()) {
                if (stackTraceElement.getMethodName().equals("onCorruption")) {
                    str = "websessionstore/stackoverflowerror/corrupt/removing";
                    Log.w(str);
                    A04();
                    return AnonymousClass1Tx.A01(super.A00(), this.A01);
                }
            }
            throw e3;
        }
    }

    public synchronized void A04() {
        close();
        Log.i("websessionstore/delete-database");
        File databasePath = this.A00.getDatabasePath("web_sessions.db");
        boolean delete = databasePath.delete();
        AnonymousClass1Tx.A04(databasePath, "websessionstore");
        StringBuilder sb = new StringBuilder();
        sb.append("websessionstore/delete-database/result=");
        sb.append(delete);
        Log.i(sb.toString());
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sessions");
        sQLiteDatabase.execSQL("CREATE TABLE sessions (_id INTEGER PRIMARY KEY AUTOINCREMENT,browser_id TEXT,secret TEXT,token TEXT,os TEXT,browser_type TEXT,login_time INTEGER,lat REAL,lon REAL,accuracy REAL,place_name TEXT,last_active INTEGER,timeout BOOLEAN,expiration INTEGER,last_push_sent INTEGER,syncd_release INTEGER);");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS browser_id_index ON sessions(browser_id);");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("websessionstore/downgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        onCreate(sQLiteDatabase);
    }

    @Override // X.AbstractC16280ok, android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.getVersion() == 3) {
            Cursor rawQuery = sQLiteDatabase.rawQuery("select sql from sqlite_master where type='table' and name='sessions'", null);
            try {
                if (rawQuery.moveToNext() && rawQuery.getString(0).contains("fserviceBOOLEAN")) {
                    sQLiteDatabase.beginTransaction();
                    sQLiteDatabase.execSQL("DROP INDEX IF EXISTS browser_id_index");
                    sQLiteDatabase.execSQL("ALTER TABLE sessions RENAME TO sessions_old");
                    onCreate(sQLiteDatabase);
                    sQLiteDatabase.execSQL("INSERT INTO sessions (_id, browser_id, secret, token, os, browser_type, lat, lon, accuracy, place_name, last_active, timeout, expiration) SELECT _id, browser_id, secret, token, os, browser_type, lat, lon, accuracy, place_name, last_active, timeout, expiration FROM sessions_old");
                    sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sessions_old");
                    sQLiteDatabase.setTransactionSuccessful();
                    sQLiteDatabase.endTransaction();
                }
                rawQuery.close();
            } catch (Throwable th) {
                if (rawQuery != null) {
                    try {
                        rawQuery.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("websessionstore/upgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        switch (i) {
            case 1:
                sQLiteDatabase.execSQL("ALTER TABLE sessions ADD timeout BOOLEAN");
                sQLiteDatabase.execSQL("ALTER TABLE sessions ADD expiration INTEGER");
            case 2:
                sQLiteDatabase.execSQL("ALTER TABLE sessions ADD fservice BOOLEAN");
            case 3:
                sQLiteDatabase.execSQL("ALTER TABLE sessions ADD last_push_sent INTEGER");
            case 4:
            case 5:
                A00(sQLiteDatabase, "login_time", "INTEGER");
                break;
            case 6:
                break;
            default:
                Log.e("websessionstore/upgrade unknown old version");
                onCreate(sQLiteDatabase);
                return;
        }
        A00(sQLiteDatabase, "syncd_release", "INTEGER DEFAULT 0");
    }
}
