package X;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import java.util.ArrayList;

/* renamed from: X.3MK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MK implements TextWatcher {
    public EditText A00;
    public CharSequence A01;
    public String A02;
    public ArrayList A03;
    public boolean A04 = false;
    public boolean A05 = false;

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AnonymousClass3MK(EditText editText, String str) {
        this.A02 = str;
        A00();
        this.A00 = editText;
    }

    public final void A00() {
        this.A03 = C12960it.A0l();
        int i = 0;
        while (true) {
            String str = this.A02;
            if (i < str.length()) {
                if (str.charAt(i) != '#') {
                    C12980iv.A1R(this.A03, i);
                }
                i++;
            } else {
                return;
            }
        }
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        EditText editText = this.A00;
        if (editText != null) {
            int selectionStart = editText.getSelectionStart();
            if (!this.A04 && !this.A05) {
                this.A05 = true;
            } else if (!this.A05) {
                this.A05 = true;
                if (this.A03.contains(Integer.valueOf(selectionStart))) {
                    while (true) {
                        if (selectionStart <= 0) {
                            break;
                        }
                        int i = selectionStart - 1;
                        if (this.A02.charAt(i) == '#') {
                            editable.delete(i, selectionStart);
                            selectionStart = i;
                            break;
                        }
                        editable.delete(i, selectionStart);
                        selectionStart--;
                    }
                }
            } else {
                return;
            }
            StringBuilder sb = new StringBuilder(editable);
            int i2 = 0;
            while (i2 < sb.length()) {
                String str = this.A02;
                if (i2 >= str.length()) {
                    break;
                }
                if (str.charAt(i2) == '#') {
                    if (!Character.isDigit(sb.charAt(i2))) {
                        sb.replace(i2, i2 + 1, "");
                        if (i2 < selectionStart) {
                            selectionStart--;
                        }
                        i2--;
                    }
                } else if (this.A02.charAt(i2) != sb.charAt(i2)) {
                    sb.insert(i2, this.A02.substring(i2, i2 + 1));
                    if (i2 <= selectionStart) {
                        selectionStart++;
                    }
                }
                i2++;
            }
            if (sb.length() > this.A02.length()) {
                sb.delete(i2, sb.length());
                selectionStart = i2;
            }
            while (true) {
                String str2 = this.A02;
                if (i2 >= str2.length() || str2.charAt(i2) == '#') {
                    break;
                }
                sb.append(this.A02.charAt(i2));
                if (selectionStart == i2) {
                    selectionStart++;
                }
                i2++;
            }
            this.A00.setText(sb);
            this.A00.setSelection(selectionStart);
            this.A05 = false;
        }
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.A04 = C12990iw.A1Y(i3, i2);
        this.A01 = charSequence;
    }
}
