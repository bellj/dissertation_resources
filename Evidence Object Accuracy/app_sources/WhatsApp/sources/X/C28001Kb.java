package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import com.facebook.animated.webp.WebPImage;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: X.1Kb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28001Kb {
    public int A00;
    public int A01;
    public long A02;
    public Bitmap A03;
    public Bitmap A04;
    public Canvas A05;
    public boolean A06;
    public final int A07;
    public final int A08;
    public final Bitmap A09;
    public final C14900mE A0A;
    public final C64253Ev A0B;
    public final C39601qB A0C;
    public final AnonymousClass4OT A0D;
    public final Runnable A0E = new RunnableBRunnable0Shape12S0100000_I0_12(this);
    public final Set A0F = Collections.newSetFromMap(new WeakHashMap());
    public volatile boolean A0G;

    public C28001Kb(Bitmap bitmap, WebPImage webPImage, C14900mE r11, C39601qB r12, String str, int i, int i2) {
        this.A0A = r11;
        this.A0C = r12;
        this.A09 = bitmap;
        this.A0D = new AnonymousClass4OT(webPImage.getFrameDurations(), webPImage.getFrameCount());
        this.A08 = i;
        this.A07 = i2;
        this.A0B = new C64253Ev(bitmap, webPImage, str, i, i2);
    }

    public void A00() {
        AnonymousClass4OT r6;
        int i;
        if (this.A0G && (i = (r6 = this.A0D).A00) > 1) {
            Set set = this.A0F;
            if (set.size() != 0) {
                long uptimeMillis = SystemClock.uptimeMillis();
                long j = this.A02 + ((long) this.A01);
                if (uptimeMillis >= j) {
                    Bitmap bitmap = this.A03;
                    if (bitmap != null) {
                        Bitmap bitmap2 = this.A04;
                        if (bitmap2 != null) {
                            bitmap2.eraseColor(0);
                        }
                        if (bitmap.isRecycled()) {
                            Log.e("AnimatedSticker/StickerAnimationController/updateFrame/was trying to use a recycled bitmap");
                        } else {
                            if (this.A04 == null) {
                                Bitmap createBitmap = Bitmap.createBitmap(this.A08, this.A07, Bitmap.Config.ARGB_8888);
                                this.A04 = createBitmap;
                                this.A05 = new Canvas(createBitmap);
                            }
                            Canvas canvas = this.A05;
                            if (canvas == null) {
                                canvas = new Canvas(this.A04);
                                this.A05 = canvas;
                            }
                            canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
                        }
                        this.A03 = null;
                    }
                    ArrayList arrayList = new ArrayList(set);
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        ((Drawable) ((AbstractC39641qG) it.next())).invalidateSelf();
                    }
                    if (this.A06) {
                        Iterator it2 = arrayList.iterator();
                        while (it2.hasNext()) {
                            C39631qF r3 = (C39631qF) ((AbstractC39641qG) it2.next());
                            if (!r3.A03) {
                                int i2 = r3.A00 + 1;
                                r3.A00 = i2;
                                boolean z = r3.A05;
                                int i3 = r3.A01;
                                if (z) {
                                    i3++;
                                }
                                if (i2 >= i3 || SystemClock.uptimeMillis() - r3.A02 > ((long) C39631qF.A09)) {
                                    r3.stop();
                                }
                            }
                        }
                        this.A06 = false;
                    }
                    int i4 = (this.A00 + 1) % i;
                    this.A00 = i4;
                    if (i4 == 0) {
                        this.A06 = true;
                    }
                    this.A02 = uptimeMillis;
                    int i5 = r6.A01[i4];
                    this.A01 = i5;
                    C39601qB r1 = this.A0C;
                    C27991Ka r9 = new C27991Ka(this.A0B, this, i4, uptimeMillis + ((long) i5));
                    C39681qL r8 = r1.A04;
                    synchronized (r8) {
                        PriorityQueue priorityQueue = r8.A01;
                        Iterator it3 = priorityQueue.iterator();
                        while (it3.hasNext()) {
                            C27991Ka r32 = (C27991Ka) it3.next();
                            if (r32.A00 >= r9.A00 && r32.A02 == r9.A02 && r9.A01 > r32.A01) {
                                it3.remove();
                            }
                        }
                        priorityQueue.add(r9);
                        r8.notifyAll();
                    }
                    if (r1.A00 == null) {
                        C39621qD r33 = new C39621qD(r1.A01, r1.A02, r1.A03, new AnonymousClass4LG(r1), r8);
                        r1.A00 = r33;
                        synchronized (r8) {
                            r8.A00 = r33;
                        }
                        r1.A00.start();
                        return;
                    }
                    return;
                }
                this.A0A.A0J(this.A0E, j - uptimeMillis);
                return;
            }
        }
        this.A0G = false;
        Bitmap bitmap3 = this.A04;
        if (bitmap3 != null) {
            bitmap3.recycle();
            this.A04 = null;
        }
        this.A03 = null;
    }

    public void finalize() {
        this.A0B.finalize();
        Bitmap bitmap = this.A04;
        if (bitmap != null) {
            bitmap.recycle();
            this.A04 = null;
        }
        super.finalize();
    }
}
