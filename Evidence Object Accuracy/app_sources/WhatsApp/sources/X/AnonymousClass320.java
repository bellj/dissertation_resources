package X;

/* renamed from: X.320  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass320 extends C54462gl {
    public final /* synthetic */ AnonymousClass3XR A00;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass320(X.AnonymousClass3XR r8) {
        /*
            r7 = this;
            r1 = r7
            r5 = r8
            r7.A00 = r8
            X.19d r4 = r8.A06
            X.1AC r0 = r8.A05
            X.0oU r3 = r0.A07
            X.01d r2 = r0.A03
            X.0pM r6 = r0.A08
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass320.<init>(X.3XR):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if (r7.A02 != false) goto L_0x001d;
     */
    @Override // X.C54462gl
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0E(X.AnonymousClass3EY r7) {
        /*
            r6 = this;
            super.A0E(r7)
            X.3XR r0 = r6.A00
            X.3DT r5 = r0.A01
            X.2gl r4 = r0.A03
            android.view.View r0 = r5.A01
            r3 = 8
            r0.setVisibility(r3)
            android.view.View r2 = r5.A03
            int r0 = r4.A0D()
            if (r0 != 0) goto L_0x001d
            boolean r1 = r7.A02
            r0 = 0
            if (r1 == 0) goto L_0x001f
        L_0x001d:
            r0 = 8
        L_0x001f:
            r2.setVisibility(r0)
            android.view.View r1 = r5.A04
            int r0 = r4.A0D()
            if (r0 != 0) goto L_0x002f
            boolean r0 = r7.A02
            if (r0 == 0) goto L_0x002f
            r3 = 0
        L_0x002f:
            r1.setVisibility(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass320.A0E(X.3EY):void");
    }
}
