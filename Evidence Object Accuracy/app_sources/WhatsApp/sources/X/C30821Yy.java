package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Yy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30821Yy implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass1Z5();
    public final BigDecimal A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C30821Yy(Parcel parcel) {
        this.A00 = (BigDecimal) parcel.readSerializable();
    }

    public C30821Yy(BigDecimal bigDecimal, int i) {
        this.A00 = bigDecimal.setScale(i, 6);
    }

    public static C30821Yy A00(String str, int i) {
        try {
            return new C30821Yy(new BigDecimal(str), i);
        } catch (NumberFormatException e) {
            Log.i("Pay: PaymentTransactionInfo.MethodInfo createFromParcel threw: ", e);
            return null;
        }
    }

    public JSONObject A01() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("amount", toString());
            return jSONObject;
        } catch (JSONException e) {
            Log.e("PAY: BasePaymentCurrency toJsonObject threw: ", e);
            return jSONObject;
        }
    }

    public boolean A02() {
        BigDecimal bigDecimal = this.A00;
        return bigDecimal != null && bigDecimal.compareTo(BigDecimal.ZERO) > 0;
    }

    public boolean A03() {
        BigDecimal bigDecimal = this.A00;
        return bigDecimal != null && bigDecimal.compareTo(BigDecimal.ZERO) >= 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && getClass() == obj.getClass()) {
                BigDecimal bigDecimal = this.A00;
                BigDecimal bigDecimal2 = ((C30821Yy) obj).A00;
                if (bigDecimal == null) {
                    if (bigDecimal2 != null) {
                    }
                } else if (bigDecimal.compareTo(bigDecimal2) != 0) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        BigDecimal bigDecimal = this.A00;
        return 31 + (bigDecimal == null ? 0 : bigDecimal.hashCode());
    }

    @Override // java.lang.Object
    public String toString() {
        return this.A00.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.A00);
    }
}
