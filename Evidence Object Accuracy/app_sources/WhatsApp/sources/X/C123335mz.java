package X;

import com.whatsapp.R;

/* renamed from: X.5mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123335mz extends AbstractC125975s7 {
    public final int A00;
    public final int A01;
    public final CharSequence A02;
    public final CharSequence A03;

    public C123335mz(CharSequence charSequence, CharSequence charSequence2, int i, int i2) {
        super(1005);
        this.A02 = charSequence;
        this.A03 = charSequence2;
        this.A01 = i;
        this.A00 = i2;
    }

    public static C123335mz A00(CharSequence charSequence, CharSequence charSequence2, int i) {
        int i2;
        int i3;
        if (i == 1) {
            i2 = R.dimen.novi_payment_transaction_detail_breakdown_first_item_top_margin;
        } else if (i == 2) {
            i2 = R.dimen.novi_payment_transaction_detail_breakdown_item_default_top_margin;
        } else if (i != 3) {
            return new C123335mz(charSequence, charSequence2, R.dimen.payment_settings_default_margin, R.dimen.payment_settings_default_margin);
        } else {
            i2 = R.dimen.novi_payment_transaction_detail_breakdown_item_default_top_margin;
            i3 = R.dimen.novi_payment_transaction_detail_breakdown_last_item_bottom_margin;
            return new C123335mz(charSequence, charSequence2, i2, i3);
        }
        i3 = R.dimen.novi_payment_transaction_detail_breakdown_item_default_bottom_margin;
        return new C123335mz(charSequence, charSequence2, i2, i3);
    }
}
