package X;

import android.location.Location;

/* renamed from: X.37m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625037m extends AbstractC16350or {
    public int A00;
    public Location A01;
    public String A02;
    public boolean A03;
    public final /* synthetic */ AbstractC36001jA A04;

    public C625037m(Location location, AbstractC36001jA r2, String str, int i, boolean z) {
        this.A04 = r2;
        this.A01 = location;
        this.A02 = str;
        this.A00 = i;
        this.A03 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02fe, code lost:
        if (r12 != null) goto L_0x0300;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0300, code lost:
        r6 = r12.optJSONObject("icon");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0306, code lost:
        if (r6 == null) goto L_0x031e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0308, code lost:
        r5 = r6.optString("prefix");
        r11.A05 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0310, code lost:
        if (r5 == null) goto L_0x031e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0312, code lost:
        r11.A05 = X.C12960it.A0d("64.png", X.C12960it.A0j(r5));
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x03c4  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x007a A[ADDED_TO_REGION, EDGE_INSN: B:135:0x007a->B:15:0x007a ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ab  */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r32) {
        /*
        // Method dump skipped, instructions count: 988
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C625037m.A05(java.lang.Object[]):java.lang.Object");
    }
}
