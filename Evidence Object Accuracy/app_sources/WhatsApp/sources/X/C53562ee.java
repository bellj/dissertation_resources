package X;

import android.view.View;
import com.whatsapp.conversation.conversationrow.InteractiveButtonsRowContentLayout;

/* renamed from: X.2ee  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53562ee extends AnonymousClass04v {
    public final /* synthetic */ C91274Rc A00;
    public final /* synthetic */ InteractiveButtonsRowContentLayout A01;

    public C53562ee(C91274Rc r1, InteractiveButtonsRowContentLayout interactiveButtonsRowContentLayout) {
        this.A01 = interactiveButtonsRowContentLayout;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r3) {
        super.A06(view, r3);
        C12970iu.A1O(r3, this.A00.A02);
    }
}
