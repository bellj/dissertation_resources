package X;

import com.whatsapp.jid.GroupJid;
import java.util.Arrays;

/* renamed from: X.1YS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YS {
    public long A00;
    public GroupJid A01;
    public boolean A02;
    public final String A03;
    public final boolean A04;

    public AnonymousClass1YS(GroupJid groupJid, String str, long j, boolean z) {
        this.A03 = str;
        this.A00 = j;
        this.A04 = z;
        this.A01 = groupJid;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1YS r7 = (AnonymousClass1YS) obj;
            if (!(r7.A03.equals(this.A03) && r7.A00 == this.A00 && r7.A04 == this.A04 && C29941Vi.A00(r7.A01, this.A01) && r7.A02 == this.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, Long.valueOf(this.A00), Boolean.valueOf(this.A04), this.A01, Boolean.valueOf(this.A02)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("JoinableCallLog[callId=");
        sb.append(this.A03);
        sb.append(", callLogRowId=");
        sb.append(this.A00);
        sb.append(", videoCall=");
        sb.append(this.A04);
        sb.append(", groupJid=");
        sb.append(this.A01);
        sb.append(", needsCommit=");
        sb.append(this.A02);
        sb.append("]");
        return sb.toString();
    }
}
