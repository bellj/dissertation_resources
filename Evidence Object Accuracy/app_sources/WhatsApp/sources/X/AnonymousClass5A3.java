package X;

import java.util.Map;

/* renamed from: X.5A3  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5A3 implements AbstractC50162Oj {
    public final AnonymousClass4AI A00;
    public final String A01;
    public final String A02;
    public final Map A03;

    public AnonymousClass5A3(AnonymousClass4AI r1, String str, String str2, Map map) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = str2;
        this.A03 = map;
    }
}
