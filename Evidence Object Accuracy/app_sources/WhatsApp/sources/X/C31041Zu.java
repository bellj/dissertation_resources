package X;

import java.net.InetAddress;
import java.util.Locale;

/* renamed from: X.1Zu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31041Zu {
    public final int A00;
    public final Long A01;
    public final Short A02;
    public final InetAddress A03;
    public final boolean A04;
    public final boolean A05;

    public C31041Zu(Long l, Short sh, InetAddress inetAddress, int i, boolean z, boolean z2) {
        this.A01 = l;
        this.A03 = inetAddress;
        this.A02 = sh;
        this.A05 = z;
        this.A04 = z2;
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C31041Zu)) {
            return false;
        }
        C31041Zu r4 = (C31041Zu) obj;
        Long l = this.A01;
        Long l2 = r4.A01;
        if (l != l2 && (l == null || !l.equals(l2))) {
            return false;
        }
        InetAddress inetAddress = this.A03;
        InetAddress inetAddress2 = r4.A03;
        if (inetAddress != inetAddress2 && (inetAddress == null || !inetAddress.equals(inetAddress2))) {
            return false;
        }
        Short sh = this.A02;
        Short sh2 = r4.A02;
        if (sh == sh2 || (sh != null && sh.equals(sh2))) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int i2;
        Long l = this.A01;
        int i3 = 0;
        if (l != null) {
            i = l.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i + 41) * 41;
        InetAddress inetAddress = this.A03;
        if (inetAddress != null) {
            i2 = inetAddress.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 41;
        Short sh = this.A02;
        if (sh != null) {
            i3 = sh.hashCode();
        }
        return i5 + i3;
    }

    public String toString() {
        return String.format(Locale.US, "%s:%d EXPIRE: %tc", this.A03, this.A02, this.A01);
    }
}
