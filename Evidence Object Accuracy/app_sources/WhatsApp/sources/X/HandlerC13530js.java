package X;

import android.os.Looper;
import android.os.Message;

/* renamed from: X.0js  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC13530js extends HandlerC13540jt {
    public final /* synthetic */ C13520jr A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC13530js(Looper looper, C13520jr r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        C13520jr.A00(message, this.A00);
    }
}
