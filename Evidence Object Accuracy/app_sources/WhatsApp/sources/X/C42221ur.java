package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;

/* renamed from: X.1ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42221ur {
    public HashSet A00;
    public final C16590pI A01;

    public C42221ur(C16590pI r1) {
        this.A01 = r1;
    }

    public HashSet A00() {
        HashSet hashSet = this.A00;
        if (hashSet != null) {
            return hashSet;
        }
        File file = new File(this.A01.A00.getFilesDir(), "invalid_numbers");
        if (file.exists() && file.canRead()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                    this.A00 = (HashSet) objectInputStream.readObject();
                    objectInputStream.close();
                    fileInputStream.close();
                } catch (Throwable th) {
                    try {
                        fileInputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (IOException | ClassNotFoundException | IllegalArgumentException e) {
                Log.e(e);
            }
        }
        HashSet hashSet2 = this.A00;
        if (hashSet2 != null) {
            return hashSet2;
        }
        HashSet hashSet3 = new HashSet();
        this.A00 = hashSet3;
        return hashSet3;
    }
}
