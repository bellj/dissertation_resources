package X;

/* renamed from: X.2S0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2S0 {
    public final int A00;
    public final C50942Ry A01;
    public final C50932Rx A02;

    public AnonymousClass2S0(C50942Ry r2, C50932Rx r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = 6;
    }

    public AnonymousClass2S0(C50942Ry r1, C50932Rx r2, int i) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = i;
    }

    public int A00(long j) {
        C50942Ry r4 = this.A01;
        if (r4 != null) {
            int i = r4.A04;
            if (i != 2 && i != 1) {
                C50932Rx r0 = this.A02;
                if (r0 != null && r0.A01 == r4.A03) {
                    return 2;
                }
                if (r4.A06 > j || j >= r4.A05) {
                    return 3;
                }
                return 1;
            } else if (r4.A06 <= j && j < r4.A05) {
                return 4;
            }
        }
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IncentiveResult{offerInfo=");
        sb.append(this.A01);
        sb.append(", accountIncentiveEligibility=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
