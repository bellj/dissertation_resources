package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.Map;
import java.util.UUID;

/* renamed from: X.1Ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25661Ag {
    public Boolean A00 = null;
    public String A01;
    public final AnonymousClass01H A02;
    public final AnonymousClass01H A03;
    public final AnonymousClass01H A04;
    public final AnonymousClass01H A05;
    public final AnonymousClass01H A06;

    public C25661Ag(AnonymousClass01H r2, AnonymousClass01H r3, AnonymousClass01H r4, AnonymousClass01H r5, AnonymousClass01H r6) {
        this.A06 = r3;
        this.A05 = r4;
        this.A04 = r5;
        this.A02 = r2;
        this.A03 = r6;
    }

    public void A00(String str) {
        String str2;
        Boolean bool = this.A00;
        if (bool == null) {
            bool = true;
            this.A00 = bool;
        }
        if (bool.booleanValue()) {
            AnonymousClass4V7 r2 = new AnonymousClass4V7();
            synchronized (this) {
                str2 = this.A01;
                if (str2 == null) {
                    str2 = UUID.randomUUID().toString();
                    this.A01 = str2;
                }
            }
            r2.A00("funnel_id", str2);
            A04(str, r2.A00);
        }
    }

    public void A01(String str, String str2) {
        Boolean bool = this.A00;
        if (bool == null) {
            bool = true;
            this.A00 = bool;
        }
        if (bool.booleanValue()) {
            A00(str);
            A02(str, str2);
        }
    }

    public void A02(String str, String str2) {
        Boolean bool = this.A00;
        if (bool == null) {
            bool = true;
            this.A00 = bool;
        }
        if (bool.booleanValue()) {
            SharedPreferences sharedPreferences = ((C14820m6) this.A05.get()).A00;
            sharedPreferences.edit().putString("previous_registration_screen", str).apply();
            sharedPreferences.edit().putString("previous_registration_action", str2).apply();
        }
    }

    public void A03(String str, String str2, long j) {
        String str3;
        Boolean bool = this.A00;
        if (bool == null) {
            bool = true;
            this.A00 = bool;
        }
        if (bool.booleanValue()) {
            AnonymousClass01H r3 = this.A03;
            String string = ((C25671Ah) r3.get()).A00().getString("google_migrate_ios_funnel_id", null);
            String string2 = ((C25671Ah) r3.get()).A00().getString("google_migrate_ios_export_duration", null);
            AnonymousClass4V7 r32 = new AnonymousClass4V7();
            synchronized (this) {
                str3 = this.A01;
                if (str3 == null) {
                    str3 = UUID.randomUUID().toString();
                    this.A01 = str3;
                }
            }
            r32.A00("funnel_id", str3);
            r32.A00("ios_attempt_id", string);
            r32.A00("ios_export_duration", string2);
            r32.A00("google_migrate_import_error", str2);
            if (j > 0) {
                r32.A00.put("google_migrate_import_duration", Long.toString(j).getBytes());
            }
            A04(str, r32.A00);
        }
    }

    public final void A04(String str, Map map) {
        AnonymousClass01H r3 = this.A05;
        String string = ((C14820m6) r3.get()).A00.getString("previous_registration_screen", null);
        if (string == null) {
            string = "unknown";
        }
        String string2 = ((C14820m6) r3.get()).A00.getString("previous_registration_action", null);
        if (string2 == null) {
            string2 = "unknown";
        }
        StringBuilder sb = new StringBuilder("funnel-logger/");
        sb.append(string);
        sb.append("/");
        sb.append(string2);
        sb.append("/");
        sb.append(str);
        Log.i(sb.toString());
        ((AbstractC14440lR) this.A06.get()).Ab2(new Runnable(str, string, string2, map) { // from class: X.2iy
            public final /* synthetic */ String A01;
            public final /* synthetic */ String A02;
            public final /* synthetic */ String A03;
            public final /* synthetic */ Map A04;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
                this.A04 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C25661Ag r1 = C25661Ag.this;
                String str2 = this.A01;
                String str3 = this.A02;
                String str4 = this.A03;
                Map map2 = this.A04;
                C20800wL r2 = (C20800wL) r1.A04.get();
                AnonymousClass01H r12 = r1.A05;
                String A0B = ((C14820m6) r12.get()).A0B();
                String A0C = ((C14820m6) r12.get()).A0C();
                r2.A05();
                byte[] A08 = r2.A08(A0B, A0C);
                byte[] A07 = r2.A07(A0C);
                Log.i("http/registration/wamsys/sendclientfunnellog");
                AbstractC29111Qx.A00(new AnonymousClass1RU(r2.A00, r2.A0J, A0B, A0C, str2, str3, str4, map2, A08, A07));
            }
        });
    }
}
