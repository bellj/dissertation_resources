package X;

/* renamed from: X.30R  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30R extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Long A02;

    public AnonymousClass30R() {
        super(978, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamLowFreeInternalStorageSpaceEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "freeSpaceRequired", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "skipAllowed", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userAction", C12960it.A0Y(this.A01));
        return C12960it.A0d("}", A0k);
    }
}
