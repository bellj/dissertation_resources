package X;

/* renamed from: X.2R3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2R3 {
    public final byte A00 = 5;
    public final int A01;
    public final long A02;
    public final AnonymousClass1IS A03;
    public final C29211Rh A04;
    public final C29211Rh A05;
    public final AnonymousClass1OT A06;
    public final String A07;
    public final boolean A08;
    public final byte[] A09;
    public final byte[] A0A;
    public final byte[] A0B;

    public AnonymousClass2R3(AnonymousClass1IS r2, C29211Rh r3, C29211Rh r4, AnonymousClass1OT r5, String str, byte[] bArr, byte[] bArr2, byte[] bArr3, int i, long j, boolean z) {
        this.A06 = r5;
        this.A03 = r2;
        this.A0B = bArr;
        this.A01 = i;
        this.A02 = j;
        this.A0A = bArr2;
        this.A09 = bArr3;
        this.A05 = r3;
        this.A04 = r4;
        this.A08 = z;
        this.A07 = str;
    }
}
