package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4OQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4OQ {
    public final long A00;
    public final UserJid A01;

    public AnonymousClass4OQ(UserJid userJid, long j) {
        this.A01 = userJid;
        this.A00 = j;
    }
}
