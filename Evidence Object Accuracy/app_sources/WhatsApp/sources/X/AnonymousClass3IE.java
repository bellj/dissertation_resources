package X;

import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.3IE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IE {
    public static final Map A00;
    public static final Map A01;

    static {
        HashMap A11 = C12970iu.A11();
        Integer A0i = C12980iv.A0i();
        A11.put("auto", A0i);
        Integer A0V = C12960it.A0V();
        A11.put("yes", A0V);
        Integer A0g = C12970iu.A0g();
        A11.put("no", A0g);
        A11.put("no_hide_descendants", C12980iv.A0j());
        A00 = Collections.unmodifiableMap(A11);
        HashMap A112 = C12970iu.A11();
        A112.put("none", A0i);
        A112.put("polite", A0V);
        A112.put("assertive", A0g);
        A01 = Collections.unmodifiableMap(A112);
    }

    public static void A00(View view, C14260l7 r17, AnonymousClass28D r18, AnonymousClass28D r19) {
        AnonymousClass4U5 r12 = (AnonymousClass4U5) AnonymousClass3JV.A04(r17, r18);
        if (r12 != null) {
            r12.A03 = (String) view.getContentDescription();
            r12.A00 = Integer.valueOf(view.getImportantForAccessibility());
            r12.A01 = Integer.valueOf(AnonymousClass028.A03(view));
            r12.A05 = view.isFocusable();
            r12.A08 = view.isSelected();
            r12.A04 = view.isEnabled();
            r12.A06 = AnonymousClass028.A0p(view);
            r12.A07 = AnonymousClass028.A0u(view);
            AnonymousClass028.A0g(view, new C53622ek(r17, r18, r19));
            int i = 35;
            String A0J = r18.A0J(35, r12.A03);
            Number number = (Number) A00.get(r18.A0I(42));
            Integer num = (Integer) A01.get(r18.A0I(44));
            boolean A0O = r18.A0O(40, r12.A05);
            boolean A0O2 = r18.A0O(49, r12.A08);
            boolean A0O3 = r18.A0O(38, r12.A04);
            boolean A0O4 = r18.A0O(41, r12.A06);
            boolean A0O5 = r18.A0O(48, r12.A07);
            Object obj = r18.A02.get(43);
            List<AnonymousClass28D> A0M = r18.A0M(55);
            if (A0M != null && !A0M.isEmpty()) {
                for (AnonymousClass28D r2 : A0M) {
                    String A0I = r2.A0I(i);
                    AbstractC14200l1 A0G = r2.A0G(38);
                    if (A0I != null) {
                        Map map = C53622ek.A05;
                        if (map.containsKey(A0I)) {
                            int A05 = C12960it.A05(map.get(A0I));
                            Object obj2 = map.get("click");
                            AnonymousClass4D3.A00(obj2);
                            int A052 = C12960it.A05(obj2);
                            Object obj3 = map.get("long_click");
                            AnonymousClass4D3.A00(obj3);
                            int A053 = C12960it.A05(obj3);
                            if (A05 == A052) {
                                if (A0G != null && Build.VERSION.SDK_INT <= 25) {
                                    r12.A09 = true;
                                    view.setOnClickListener(new View$OnClickListenerC76333lT(A052));
                                }
                            } else if (A05 == A053 && A0G != null) {
                                r12.A0A = true;
                                view.setOnLongClickListener(new View$OnLongClickListenerC101094my(A053));
                            }
                        }
                    }
                    i = 35;
                }
            }
            String str = r12.A02;
            view.setContentDescription(A0J);
            r12.A02 = A0J;
            if (Build.VERSION.SDK_INT >= 21) {
                A01(view, num, str, A0J);
            }
            if (number == null) {
                number = r12.A00;
            }
            view.setImportantForAccessibility(number.intValue());
            if (num == null) {
                num = r12.A01;
            }
            AnonymousClass028.A0Z(view, num.intValue());
            view.setFocusable(A0O);
            if (obj != null) {
                C64953Ho.A00(view, r17, obj);
            }
            view.setSelected(A0O2);
            view.setEnabled(A0O3);
            AnonymousClass028.A0l(view, A0O4);
            new AnonymousClass0DY().A02(view, Boolean.valueOf(A0O5));
            C64953Ho.A02(r19, r18.A0M(56));
        }
    }

    public static void A01(View view, Integer num, String str, String str2) {
        if ((num == null || num.intValue() == 0) && !TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str) && !str2.equals(str) && view.isAccessibilityFocused()) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain();
            obtain.setEventType(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
            obtain.setContentChangeTypes(4);
            obtain.setContentDescription(str2);
            view.sendAccessibilityEventUnchecked(obtain);
        }
    }
}
