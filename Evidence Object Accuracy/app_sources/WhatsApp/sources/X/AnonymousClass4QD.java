package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.4QD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4QD {
    public final View A00;
    public final View A01;
    public final ImageView A02;

    public AnonymousClass4QD(ViewGroup viewGroup) {
        ImageView A0K = C12970iu.A0K(viewGroup, R.id.emoji_tab);
        this.A02 = A0K;
        this.A00 = AnonymousClass028.A0D(viewGroup, R.id.emoji_group_layout);
        this.A01 = AnonymousClass028.A0D(viewGroup, R.id.pager);
        A0K.setVisibility(0);
    }
}
