package X;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.IGmsServiceBroker;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.4d1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95064d1 {
    public static final C78603pB[] A0T = new C78603pB[0];
    public int A00;
    public int A01;
    public int A02 = 1;
    public long A03;
    public long A04;
    public long A05;
    public IInterface A06;
    public C56492ky A07 = null;
    public AnonymousClass5Sa A08;
    public IGmsServiceBroker A09;
    public ServiceConnectionC97794he A0A;
    public AnonymousClass4RC A0B;
    public AtomicInteger A0C = new AtomicInteger(0);
    public boolean A0D = false;
    public final int A0E;
    public final Context A0F;
    public final Handler A0G;
    public final Looper A0H;
    public final C471929k A0I;
    public final AbstractC115055Qa A0J;
    public final AbstractC115065Qb A0K;
    public final C94924cl A0L;
    public final Object A0M = C12970iu.A0l();
    public final Object A0N = C12970iu.A0l();
    public final String A0O;
    public final ArrayList A0P = C12960it.A0l();
    public volatile C56472kw A0Q = null;
    public volatile String A0R;
    public volatile String A0S = null;

    public String A07() {
        return "com.google.android.gms";
    }

    @Override // X.AbstractC72443eb
    public abstract int AET();

    @Override // X.AbstractC72443eb
    public boolean AZd() {
        return false;
    }

    @Override // X.AbstractC72443eb
    public boolean Aad() {
        return true;
    }

    @Override // X.AbstractC72443eb
    public boolean Aae() {
        return false;
    }

    public AbstractC95064d1(Context context, Looper looper, C471929k r6, AbstractC115055Qa r7, AbstractC115065Qb r8, C94924cl r9, String str, int i) {
        C13020j0.A02(context, "Context must not be null");
        this.A0F = context;
        C13020j0.A02(looper, "Looper must not be null");
        this.A0H = looper;
        C13020j0.A02(r9, "Supervisor must not be null");
        this.A0L = r9;
        C13020j0.A02(r6, "API availability must not be null");
        this.A0I = r6;
        this.A0G = new HandlerC79583qp(looper, this);
        this.A0E = i;
        this.A0J = r7;
        this.A0K = r8;
        this.A0O = str;
    }

    public static /* bridge */ /* synthetic */ boolean A01(IInterface iInterface, AbstractC95064d1 r3, int i, int i2) {
        synchronized (r3.A0M) {
            if (r3.A02 != i) {
                return false;
            }
            r3.A09(iInterface, i2);
            return true;
        }
    }

    public Bundle A02() {
        if (this instanceof C77913o4) {
            return ((C77913o4) this).A00;
        }
        if (this instanceof C77923o5) {
            return ((C77923o5) this).A00;
        }
        if (!(this instanceof C77903o3)) {
            return C12970iu.A0D();
        }
        return ((C77903o3) this).A00.A00();
    }

    public final IInterface A03() {
        IInterface iInterface;
        synchronized (this.A0M) {
            if (this.A02 == 5) {
                throw new DeadObjectException();
            } else if (isConnected()) {
                iInterface = this.A06;
                C13020j0.A02(iInterface, "Client is connected but service is null");
            } else {
                throw C12960it.A0U("Not connected. Call connect() and wait for onConnected() to be called.");
            }
        }
        return iInterface;
    }

    public IInterface A04(IBinder iBinder) {
        if (this instanceof C77953o8) {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
            if (!(queryLocalInterface instanceof C79053py)) {
                return new C79053py(iBinder);
            }
            return queryLocalInterface;
        } else if (this instanceof C77893o2) {
            IInterface queryLocalInterface2 = iBinder.queryLocalInterface("com.google.android.gms.clearcut.internal.IClearcutLoggerService");
            if (!(queryLocalInterface2 instanceof AnonymousClass5Y9)) {
                return new C98454ii(iBinder);
            }
            return queryLocalInterface2;
        } else if (this instanceof C77933o6) {
            IInterface queryLocalInterface3 = iBinder.queryLocalInterface("com.google.android.gms.auth.blockstore.internal.IBlockstoreService");
            if (!(queryLocalInterface3 instanceof C79003pt)) {
                return new C79003pt(iBinder);
            }
            return queryLocalInterface3;
        } else if (this instanceof C77913o4) {
            IInterface queryLocalInterface4 = iBinder.queryLocalInterface("com.google.android.gms.auth.api.accounttransfer.internal.IAccountTransferService");
            if (!(queryLocalInterface4 instanceof AnonymousClass5Y7)) {
                return new C78903pj(iBinder);
            }
            return queryLocalInterface4;
        } else if (this instanceof C77923o5) {
            IInterface queryLocalInterface5 = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
            if (!(queryLocalInterface5 instanceof AnonymousClass5Y5)) {
                return new C78883ph(iBinder);
            }
            return queryLocalInterface5;
        } else if (this instanceof C77903o3) {
            IInterface queryLocalInterface6 = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
            if (!(queryLocalInterface6 instanceof C78863pf)) {
                return new C78863pf(iBinder);
            }
            return queryLocalInterface6;
        } else if (!(this instanceof C77883o1)) {
            IInterface queryLocalInterface7 = iBinder.queryLocalInterface("com.google.android.gms.common.internal.service.IClientTelemetryService");
            if (!(queryLocalInterface7 instanceof C79043px)) {
                return new C79043px(iBinder);
            }
            return queryLocalInterface7;
        } else {
            IInterface queryLocalInterface8 = iBinder.queryLocalInterface("com.google.android.gms.auth.api.phone.internal.ISmsRetrieverApiService");
            if (!(queryLocalInterface8 instanceof AnonymousClass5Y4)) {
                return new C98444ih(iBinder);
            }
            return queryLocalInterface8;
        }
    }

    public String A05() {
        if (this instanceof C77953o8) {
            return "com.google.android.gms.signin.internal.ISignInService";
        }
        if (this instanceof C77893o2) {
            return "com.google.android.gms.clearcut.internal.IClearcutLoggerService";
        }
        if (this instanceof C77933o6) {
            return "com.google.android.gms.auth.blockstore.internal.IBlockstoreService";
        }
        if (this instanceof C77913o4) {
            return "com.google.android.gms.auth.api.accounttransfer.internal.IAccountTransferService";
        }
        if (this instanceof C77923o5) {
            return "com.google.android.gms.auth.api.internal.IAuthService";
        }
        if (!(this instanceof C77903o3)) {
            return !(this instanceof C77883o1) ? "com.google.android.gms.common.internal.service.IClientTelemetryService" : "com.google.android.gms.auth.api.phone.internal.ISmsRetrieverApiService";
        }
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    public String A06() {
        if (this instanceof C77953o8) {
            return "com.google.android.gms.signin.service.START";
        }
        if (this instanceof C77893o2) {
            return "com.google.android.gms.clearcut.service.START";
        }
        if (this instanceof C77933o6) {
            return "com.google.android.gms.auth.blockstore.service.START";
        }
        if (this instanceof C77913o4) {
            return "com.google.android.gms.auth.api.accounttransfer.service.START";
        }
        if (this instanceof C77923o5) {
            return "com.google.android.gms.auth.service.START";
        }
        if (!(this instanceof C77903o3)) {
            return !(this instanceof C77883o1) ? "com.google.android.gms.common.telemetry.service.START" : "com.google.android.gms.auth.api.phone.service.SmsRetrieverApiService.START";
        }
        return "com.google.android.gms.auth.api.credentials.service.START";
    }

    public void A08(Bundle bundle, IBinder iBinder, int i, int i2) {
        Handler handler = this.A0G;
        handler.sendMessage(handler.obtainMessage(1, i2, -1, new C78723pR(bundle, iBinder, this, i)));
    }

    public final void A09(IInterface iInterface, int i) {
        boolean z;
        AnonymousClass4RC r0;
        boolean z2 = true;
        if (i != 4) {
            z2 = false;
        }
        boolean z3 = true;
        if (iInterface == null) {
            z3 = false;
        }
        if (z2 == z3) {
            synchronized (this.A0M) {
                this.A02 = i;
                this.A06 = iInterface;
                if (i == 1) {
                    ServiceConnectionC97794he r6 = this.A0A;
                    if (r6 != null) {
                        C94924cl r5 = this.A0L;
                        AnonymousClass4RC r02 = this.A0B;
                        String str = r02.A01;
                        C13020j0.A01(str);
                        r5.A01(r6, new C65083Ib(str, r02.A02, r02.A00, r02.A03));
                        this.A0A = null;
                    }
                } else if (i == 2 || i == 3) {
                    ServiceConnectionC97794he r62 = this.A0A;
                    if (!(r62 == null || (r0 = this.A0B) == null)) {
                        String str2 = r0.A01;
                        String str3 = r0.A02;
                        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str2) + 70 + C12970iu.A07(str3));
                        A0t.append("Calling connect() while still connected, missing disconnect() for ");
                        A0t.append(str2);
                        A0t.append(" on ");
                        Log.e("GmsClient", C12960it.A0d(str3, A0t));
                        C94924cl r52 = this.A0L;
                        AnonymousClass4RC r03 = this.A0B;
                        String str4 = r03.A01;
                        C13020j0.A01(str4);
                        r52.A01(r62, new C65083Ib(str4, r03.A02, r03.A00, r03.A03));
                        this.A0C.incrementAndGet();
                    }
                    AtomicInteger atomicInteger = this.A0C;
                    ServiceConnectionC97794he r9 = new ServiceConnectionC97794he(this, atomicInteger.get());
                    this.A0A = r9;
                    String A07 = A07();
                    String A06 = A06();
                    if ((this instanceof C77933o6) || (this instanceof C77943o7)) {
                        z = true;
                    } else {
                        z = C12990iw.A1X(AET(), 211700000);
                    }
                    AnonymousClass4RC r2 = new AnonymousClass4RC(A07, A06, z);
                    this.A0B = r2;
                    boolean z4 = r2.A03;
                    if (!z4 || AET() >= 17895000) {
                        C94924cl r63 = this.A0L;
                        String str5 = r2.A01;
                        C13020j0.A01(str5);
                        String str6 = r2.A02;
                        int i2 = r2.A00;
                        String str7 = this.A0O;
                        if (str7 == null) {
                            str7 = C12980iv.A0s(this.A0F);
                        }
                        if (!r63.A02(r9, new C65083Ib(str5, str6, i2, z4), str7)) {
                            AnonymousClass4RC r04 = this.A0B;
                            String str8 = r04.A01;
                            String str9 = r04.A02;
                            StringBuilder A0t2 = C12980iv.A0t(C12970iu.A07(str8) + 34 + C12970iu.A07(str9));
                            A0t2.append("unable to connect to service: ");
                            A0t2.append(str8);
                            A0t2.append(" on ");
                            Log.w("GmsClient", C12960it.A0d(str9, A0t2));
                            int i3 = atomicInteger.get();
                            Handler handler = this.A0G;
                            handler.sendMessage(handler.obtainMessage(7, i3, -1, new C78713pQ(this, 16)));
                        }
                    } else {
                        throw C12960it.A0U(C72453ed.A0r("Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: ", r2.A01));
                    }
                } else if (i == 4) {
                    C13020j0.A01(iInterface);
                    this.A04 = System.currentTimeMillis();
                }
            }
            return;
        }
        throw C72453ed.A0h();
    }

    public boolean A0A() {
        return this instanceof C77933o6;
    }

    public C78603pB[] A0B() {
        if (this instanceof C77933o6) {
            return C88854Hj.A06;
        }
        if (!(this instanceof C77943o7)) {
            return A0T;
        }
        return C88734Gx.A01;
    }

    @Override // X.AbstractC72443eb
    public void A7X(AnonymousClass5Sa r3) {
        C13020j0.A02(r3, "Connection progress callbacks cannot be null.");
        this.A08 = r3;
        A09(null, 2);
    }

    @Override // X.AbstractC72443eb
    public void A8y() {
        this.A0C.incrementAndGet();
        ArrayList arrayList = this.A0P;
        synchronized (arrayList) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                AnonymousClass4VW r1 = (AnonymousClass4VW) arrayList.get(i);
                synchronized (r1) {
                    r1.A00 = null;
                }
            }
            arrayList.clear();
        }
        synchronized (this.A0N) {
            this.A09 = null;
        }
        A09(null, 1);
    }

    public void AG7(IAccountAccessor iAccountAccessor, Set set) {
        Bundle A02 = A02();
        C78593pA r7 = new C78593pA(this.A0E, this.A0R);
        r7.A05 = this.A0F.getPackageName();
        r7.A03 = A02;
        if (set != null) {
            r7.A0B = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (Aae()) {
            r7.A02 = new Account("<<default account>>", "com.google");
            if (iAccountAccessor != null) {
                r7.A04 = iAccountAccessor.asBinder();
            }
        }
        r7.A09 = A0T;
        r7.A0A = A0B();
        if (A0A()) {
            r7.A08 = true;
        }
        try {
            try {
                synchronized (this.A0N) {
                    IGmsServiceBroker iGmsServiceBroker = this.A09;
                    if (iGmsServiceBroker != null) {
                        BinderC56532lA r1 = new BinderC56532lA(this, this.A0C.get());
                        C108414z1 r6 = (C108414z1) iGmsServiceBroker;
                        Parcel obtain = Parcel.obtain();
                        Parcel obtain2 = Parcel.obtain();
                        obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                        obtain.writeStrongBinder(r1.asBinder());
                        obtain.writeInt(1);
                        C100484lz.A00(obtain, r7, 0);
                        r6.A00.transact(46, obtain, obtain2, 0);
                        obtain2.readException();
                        obtain2.recycle();
                        obtain.recycle();
                    } else {
                        Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                    }
                }
            } catch (RemoteException | RuntimeException e) {
                Log.w("GmsClient", "IGmsServiceBroker.getService failed", e);
                A08(null, null, 8, this.A0C.get());
            }
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e2);
            Handler handler = this.A0G;
            handler.sendMessage(handler.obtainMessage(6, this.A0C.get(), 3));
        } catch (SecurityException e3) {
            throw e3;
        }
    }

    @Override // X.AbstractC72443eb
    public Intent AGl() {
        throw C12980iv.A0u("Not a sign in API");
    }

    public boolean AJG() {
        boolean z;
        synchronized (this.A0M) {
            int i = this.A02;
            z = true;
            if (!(i == 2 || i == 3)) {
                z = false;
            }
        }
        return z;
    }

    public boolean isConnected() {
        boolean A1V;
        synchronized (this.A0M) {
            A1V = C12960it.A1V(this.A02, 4);
        }
        return A1V;
    }
}
