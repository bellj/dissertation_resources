package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: X.096  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass096 extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C03070Fz A01;

    public AnonymousClass096(View view, C03070Fz r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass028.A0f(this.A00, null);
    }
}
