package X;

/* renamed from: X.1ts  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41651ts extends AnonymousClass1JW {
    public final AbstractC14640lm A00;

    public C41651ts(AbstractC15710nm r1, C15450nH r2, AbstractC14640lm r3, String str, String str2, String str3, int i, int i2, long j, boolean z, boolean z2, boolean z3) {
        super(r1, r2);
        this.A00 = r3;
        this.A0I = str;
        this.A0K = str2;
        ((AnonymousClass1JX) this).A00 = i;
        this.A0P = z;
        this.A0O = z2;
        this.A0J = str3;
        this.A0R = z3;
        this.A02 = i2;
        this.A06 = j;
    }
}
