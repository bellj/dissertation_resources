package X;

import java.security.SecureRandom;

/* renamed from: X.5GC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GC implements AnonymousClass5Wv {
    @Override // X.AnonymousClass5Wv
    public int A5m(byte[] bArr, int i) {
        int length = bArr.length;
        int i2 = length - i;
        byte b = Byte.MIN_VALUE;
        while (true) {
            bArr[i] = b;
            i++;
            if (i >= length) {
                return i2;
            }
            b = 0;
        }
    }

    @Override // X.AnonymousClass5Wv
    public void AIb(SecureRandom secureRandom) {
    }

    @Override // X.AnonymousClass5Wv
    public int AYq(byte[] bArr) {
        int length = bArr.length;
        do {
            length--;
            if (length <= 0) {
                break;
            }
        } while (bArr[length] == 0);
        if (bArr[length] == Byte.MIN_VALUE) {
            return length - length;
        }
        throw new C114965Nt("pad block corrupted");
    }
}
