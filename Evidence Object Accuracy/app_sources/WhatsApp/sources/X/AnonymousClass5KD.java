package X;

/* renamed from: X.5KD  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KD extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1VC $future;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KD(AnonymousClass1VC r2) {
        super(1);
        this.$future = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        C16700pc.A0E(obj, 0);
        this.$future.A01(obj);
        return AnonymousClass1WZ.A00;
    }
}
