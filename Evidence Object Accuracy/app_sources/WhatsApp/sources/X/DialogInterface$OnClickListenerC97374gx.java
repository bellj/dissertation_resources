package X;

import android.content.DialogInterface;

/* renamed from: X.4gx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class DialogInterface$OnClickListenerC97374gx implements DialogInterface.OnClickListener {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public AnonymousClass016 A01 = C12980iv.A0T();
    public AnonymousClass016 A02 = C12980iv.A0T();

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        AnonymousClass016 r0;
        if (i == -3) {
            r0 = this.A01;
        } else if (i == -2) {
            r0 = this.A00;
        } else if (i == -1) {
            r0 = this.A02;
        } else {
            return;
        }
        r0.A0A(dialogInterface);
    }
}
