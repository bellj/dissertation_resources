package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32301bw {
    public static final String A00;
    public static final String A01;
    public static final String A02;
    public static final String A03;
    public static final String A04;
    public static final String A05;
    public static final String A06;
    public static final String A07;
    public static final String A08;
    public static final String A09;
    public static final String A0A;
    public static final String A0B;
    public static final String A0C;
    public static final String A0D;
    public static final String A0E;
    public static final String A0F;
    public static final String A0G;
    public static final String A0H;
    public static final String A0I;
    public static final String A0J;
    public static final String A0K;
    public static final String A0L;
    public static final String A0M;
    public static final String A0N;
    public static final String A0O;
    public static final String A0P;
    public static final String A0Q;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        String str = C16500p8.A00;
        sb.append(AnonymousClass1Ux.A01("message", str));
        sb.append(" FROM ");
        sb.append("available_message_view AS message");
        sb.append(" JOIN ");
        sb.append("chat AS chat_chat ON message.chat_row_id = chat_chat._id");
        sb.append(" JOIN ");
        sb.append("jid AS chat_jid ON chat_chat.jid_row_id = chat_jid._id");
        sb.append(" JOIN ");
        sb.append("chat_view AS chat ON chat.raw_string_jid = chat_jid.raw_string");
        sb.append(" WHERE ");
        sb.append("message.from_me = 0");
        sb.append(" AND ");
        sb.append("status != 16");
        sb.append(" AND ");
        sb.append("status != 10");
        sb.append(" AND ");
        sb.append("(chat.last_read_message_row_id >= message._id OR status = 17)");
        sb.append(" AND ");
        sb.append("chat.last_read_receipt_sent_message_row_id < message._id");
        sb.append(" AND ");
        sb.append("chat.last_read_receipt_sent_message_row_id > 0");
        sb.append(" AND ");
        sb.append("message.message_type NOT IN ('8', '10', '15')");
        sb.append(" AND ");
        sb.append("(chat.hidden IS NULL OR chat.hidden = 0)");
        sb.append(" ORDER BY message._id DESC");
        sb.append(" LIMIT 4096");
        A0P = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT ");
        sb2.append(str);
        sb2.append(" FROM ");
        sb2.append("available_message_view");
        sb2.append(" WHERE ");
        sb2.append("chat_row_id = ?");
        sb2.append(" AND ");
        sb2.append("from_me = 0");
        sb2.append(" AND ");
        sb2.append("sort_id <= ?");
        sb2.append(" AND ");
        sb2.append("sort_id > ?");
        sb2.append(" AND ");
        sb2.append("status NOT IN (16, 10)");
        sb2.append(" AND ");
        sb2.append("message_type NOT IN ('8', '10', '15')");
        sb2.append(" ORDER BY sort_id DESC ");
        sb2.append("LIMIT 4096");
        A0Q = sb2.toString();
        String join = TextUtils.join(", ", Arrays.asList("key_from_me", "key_id", "status", "needs_push", "data", "timestamp", "media_url", "media_mime_type", "media_wa_type", "media_size", "media_name", "media_caption", "media_hash", "media_duration", "origin", "latitude", "longitude", "thumb_image", "remote_resource", "raw_data", "recipient_count", "participant_hash", "starred", "quoted_row_id", "mentioned_jids", "multicast_id", "edit_version", "receipt_server_timestamp", "media_enc_hash", "payment_transaction_id", "forwarded", "preview_type", "received_timestamp", "lookup_tables", "future_message_type", "message_add_on_flags", "_id"));
        A00 = join;
        StringBuilder sb3 = new StringBuilder("   SELECT ");
        sb3.append(str);
        sb3.append(" FROM ");
        sb3.append("available_message_view");
        sb3.append(" WHERE ");
        sb3.append("chat_row_id = ?");
        String obj = sb3.toString();
        A0K = obj;
        StringBuilder sb4 = new StringBuilder();
        sb4.append(obj);
        sb4.append(" AND ");
        sb4.append("message_type = ?");
        sb4.append(" ORDER BY sort_id DESC");
        A09 = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(obj);
        sb5.append(" AND ");
        sb5.append("message_type");
        sb5.append(" NOT IN ('");
        sb5.append(8);
        sb5.append("', '");
        sb5.append(10);
        sb5.append("', '");
        sb5.append(7);
        sb5.append("', '");
        sb5.append(15);
        sb5.append("', '");
        sb5.append(19);
        sb5.append("') AND ");
        sb5.append("from_me = 0");
        sb5.append(" ORDER BY sort_id DESC");
        sb5.append(" LIMIT ?");
        A08 = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append(obj);
        sb6.append(" AND ");
        sb6.append("(message_type != '8')");
        String obj2 = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append(obj2);
        sb7.append(" AND ");
        sb7.append("_id >= ?");
        sb7.append(" ORDER BY _id ASC");
        A0B = sb7.toString();
        StringBuilder sb8 = new StringBuilder();
        sb8.append(obj2);
        sb8.append(" ORDER BY ");
        sb8.append("sort_id DESC");
        sb8.append(" LIMIT ?");
        A0A = sb8.toString();
        StringBuilder sb9 = new StringBuilder();
        sb9.append(obj2);
        sb9.append(" ORDER BY sort_id DESC");
        sb9.append(" LIMIT ?");
        A0F = sb9.toString();
        StringBuilder sb10 = new StringBuilder();
        sb10.append("   SELECT ");
        sb10.append(str);
        sb10.append(", ");
        sb10.append("chat_row_id");
        sb10.append(" FROM ");
        sb10.append("available_message_view");
        sb10.append(" WHERE ");
        sb10.append("_id >= ? ");
        sb10.append(" AND ");
        sb10.append("_id < ? ");
        sb10.append(" AND (expire_timestamp IS NULL OR expire_timestamp >= ? OR keep_in_chat = 1) ");
        sb10.append(" ORDER BY _id DESC");
        sb10.append(" LIMIT ? ");
        A0M = sb10.toString();
        StringBuilder sb11 = new StringBuilder();
        sb11.append("   SELECT ");
        sb11.append(str);
        sb11.append(", ");
        sb11.append("chat_row_id");
        sb11.append(" FROM ");
        sb11.append("available_message_view");
        sb11.append(" WHERE ");
        sb11.append("_id > ? ");
        sb11.append(" AND ");
        sb11.append("_id <= ? ");
        sb11.append(" AND (expire_timestamp IS NULL OR expire_timestamp >= ? OR keep_in_chat = 1) ");
        sb11.append(" ORDER BY _id ASC");
        sb11.append(" LIMIT ? ");
        A0J = sb11.toString();
        StringBuilder sb12 = new StringBuilder();
        sb12.append("   SELECT ");
        sb12.append(str);
        sb12.append(", ");
        sb12.append("chat_row_id");
        sb12.append(" FROM ");
        sb12.append("available_message_view");
        sb12.append(" WHERE ");
        sb12.append("_id > ? ");
        sb12.append(" AND ");
        sb12.append("_id <= ? ");
        sb12.append(" AND ");
        sb12.append("chat_row_id = ?");
        sb12.append(" AND (expire_timestamp IS NULL OR expire_timestamp >= ? OR keep_in_chat = 1) ");
        sb12.append(" ORDER BY _id ASC");
        sb12.append(" LIMIT ? ");
        A0L = sb12.toString();
        StringBuilder sb13 = new StringBuilder();
        sb13.append("SELECT ");
        sb13.append(str);
        sb13.append(" FROM ");
        sb13.append("available_message_view");
        sb13.append(" WHERE ");
        sb13.append("chat_row_id = ?");
        sb13.append(" AND ");
        sb13.append("starred = 1");
        sb13.append(" AND ");
        sb13.append("(message_type != '7')");
        String obj3 = sb13.toString();
        StringBuilder sb14 = new StringBuilder();
        sb14.append(obj3);
        sb14.append(" ORDER BY sort_id DESC");
        A04 = sb14.toString();
        StringBuilder sb15 = new StringBuilder();
        sb15.append(obj3);
        sb15.append(" AND message_type = '13'");
        sb15.append(" ORDER BY sort_id DESC");
        A02 = sb15.toString();
        StringBuilder sb16 = new StringBuilder();
        sb16.append("SELECT ");
        sb16.append(str);
        sb16.append(" FROM ");
        sb16.append("available_message_view");
        sb16.append(" WHERE ");
        sb16.append("starred = 1");
        sb16.append(" AND ");
        sb16.append("(message_type != '7')");
        String obj4 = sb16.toString();
        StringBuilder sb17 = new StringBuilder();
        sb17.append(obj4);
        sb17.append(" AND message_type = '13'");
        sb17.append(" ORDER BY timestamp DESC");
        A03 = sb17.toString();
        StringBuilder sb18 = new StringBuilder();
        sb18.append(obj4);
        sb18.append(" ORDER BY timestamp DESC");
        A05 = sb18.toString();
        StringBuilder sb19 = new StringBuilder();
        sb19.append("SELECT ");
        sb19.append(str);
        sb19.append(" FROM ");
        sb19.append("available_message_view");
        sb19.append(" WHERE ");
        sb19.append("chat_row_id = ?");
        sb19.append(" AND ");
        sb19.append("_id IN (SELECT message_row_id FROM message_ephemeral WHERE keep_in_chat = 1)");
        String obj5 = sb19.toString();
        StringBuilder sb20 = new StringBuilder();
        sb20.append(obj5);
        sb20.append(" ORDER BY sort_id DESC");
        A01 = sb20.toString();
        StringBuilder sb21 = new StringBuilder();
        sb21.append("SELECT ");
        sb21.append(str);
        sb21.append(" FROM ");
        sb21.append("available_message_view");
        sb21.append(" WHERE ");
        sb21.append("_id = ?");
        A0C = sb21.toString();
        StringBuilder sb22 = new StringBuilder();
        sb22.append("SELECT ");
        sb22.append(str);
        sb22.append(" FROM ");
        sb22.append("available_message_view");
        sb22.append(" WHERE ");
        sb22.append("sort_id = ?");
        A0D = sb22.toString();
        StringBuilder sb23 = new StringBuilder();
        sb23.append("SELECT ");
        sb23.append(str);
        sb23.append(" FROM ");
        sb23.append("available_message_view");
        sb23.append(" WHERE ");
        sb23.append("chat_row_id = ?");
        sb23.append(" ORDER BY sort_id DESC");
        sb23.append(" LIMIT 1");
        A0I = sb23.toString();
        StringBuilder sb24 = new StringBuilder();
        sb24.append("SELECT ");
        sb24.append(str);
        sb24.append(" FROM ");
        sb24.append("available_message_view");
        sb24.append(" WHERE ");
        sb24.append("chat_row_id = ?");
        sb24.append(" AND ");
        sb24.append("message_type NOT IN ('8', '10', '12') ");
        sb24.append(" ORDER BY sort_id DESC");
        sb24.append(" LIMIT ?");
        A0E = sb24.toString();
        StringBuilder sb25 = new StringBuilder();
        sb25.append("SELECT ");
        sb25.append(str);
        sb25.append(" FROM ");
        sb25.append("available_message_view");
        sb25.append(" WHERE ");
        sb25.append("message_type = 9");
        sb25.append(" AND ");
        sb25.append("chat_row_id = ? ");
        A07 = sb25.toString();
        StringBuilder sb26 = new StringBuilder();
        sb26.append("SELECT ");
        sb26.append(str);
        sb26.append(" FROM ");
        sb26.append("available_message_view");
        sb26.append(" WHERE ");
        sb26.append("message_type IN ('9' , '26' )");
        sb26.append(" AND ");
        sb26.append("origin != 7");
        sb26.append(" AND ");
        sb26.append("chat_row_id = ?");
        sb26.append(" ORDER BY sort_id DESC");
        A06 = sb26.toString();
        StringBuilder sb27 = new StringBuilder();
        sb27.append("SELECT ");
        sb27.append(join);
        sb27.append(", ");
        sb27.append("key_remote_jid");
        sb27.append(" FROM ");
        sb27.append("legacy_available_messages_view");
        sb27.append(" WHERE ");
        sb27.append("media_wa_type=12");
        sb27.append(" ORDER BY _id");
        A0H = sb27.toString();
        StringBuilder sb28 = new StringBuilder();
        sb28.append("SELECT ");
        sb28.append(str);
        sb28.append(" FROM ");
        sb28.append("available_message_view");
        sb28.append(" WHERE ");
        sb28.append("message_type = 12");
        sb28.append(" ORDER BY _id");
        A0G = sb28.toString();
        StringBuilder sb29 = new StringBuilder();
        sb29.append("SELECT ");
        sb29.append(str);
        sb29.append(" FROM ");
        sb29.append("available_message_view");
        sb29.append(" WHERE ");
        sb29.append("from_me = 1");
        sb29.append(" AND ");
        sb29.append("status < 4");
        sb29.append(" AND ");
        sb29.append("sort_id > ?");
        sb29.append(" ORDER BY sort_id ASC");
        A0O = sb29.toString();
        StringBuilder sb30 = new StringBuilder();
        sb30.append("SELECT ");
        sb30.append(str);
        sb30.append(" FROM ");
        sb30.append("available_message_view");
        sb30.append(" WHERE ");
        sb30.append("from_me = 1 ");
        sb30.append(" AND ");
        sb30.append("chat_row_id = ? ");
        sb30.append(" AND ");
        sb30.append("status < 4");
        sb30.append(" AND ");
        sb30.append("timestamp > ? ");
        sb30.append(" AND ");
        sb30.append("timestamp < ? ");
        sb30.append(" ORDER BY timestamp ASC");
        A0N = sb30.toString();
    }
}
