package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.37V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37V extends AbstractC16350or {
    public final C15550nR A00;
    public final C20730wE A01;
    public final C15680nj A02;
    public final WeakReference A03;

    public AnonymousClass37V(C15550nR r2, ContactPickerFragment contactPickerFragment, C20730wE r4, C15680nj r5) {
        this.A03 = C12970iu.A10(contactPickerFragment);
        this.A00 = r2;
        this.A01 = r4;
        this.A02 = r5;
    }
}
