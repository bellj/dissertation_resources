package X;

import android.animation.Animator;
import android.animation.ValueAnimator;
import java.util.ArrayList;

/* renamed from: X.2Qt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50742Qt {
    public ValueAnimator A00 = null;
    public AnonymousClass2RA A01 = null;
    public final Animator.AnimatorListener A02 = new AnonymousClass2RD(this);
    public final ArrayList A03 = new ArrayList();

    public void A00(ValueAnimator valueAnimator, int[] iArr) {
        AnonymousClass2RA r1 = new AnonymousClass2RA(valueAnimator, iArr);
        valueAnimator.addListener(this.A02);
        this.A03.add(r1);
    }
}
