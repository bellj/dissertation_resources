package X;

import java.util.Iterator;
import java.util.List;

/* renamed from: X.529  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass529 implements AnonymousClass5T8 {
    @Override // X.AnonymousClass5T8
    public Object AJ9(AbstractC111885Be r6, C94394bk r7, Object obj, String str, List list) {
        AbstractC117035Xw r4 = r7.A01.A00;
        if (list != null && list.size() > 0) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C95264dP r2 = (C95264dP) it.next();
                if (obj instanceof List) {
                    r4.Abk(obj, r4.AKQ(obj), r2.A01.get());
                }
            }
        }
        return obj;
    }
}
