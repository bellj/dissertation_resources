package X;

import android.content.IntentFilter;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3p6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78553p6 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99364kB();
    public final AnonymousClass5YN A00;
    public final String A01;
    public final String A02;
    public final IntentFilter[] A03;

    public C78553p6() {
        this.A00 = null;
        throw C12980iv.A0n("zzr");
    }

    public C78553p6(IBinder iBinder, String str, String str2, IntentFilter[] intentFilterArr) {
        AnonymousClass5YN r1;
        if (iBinder != null) {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.wearable.internal.IWearableListener");
            if (queryLocalInterface instanceof AnonymousClass5YN) {
                r1 = (AnonymousClass5YN) queryLocalInterface;
            } else {
                r1 = new C80533sP(iBinder);
            }
            this.A00 = r1;
        } else {
            this.A00 = null;
        }
        this.A03 = intentFilterArr;
        this.A01 = str;
        this.A02 = str2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        IBinder asBinder;
        int A00 = C95654e8.A00(parcel);
        AnonymousClass5YN r0 = this.A00;
        if (r0 == null) {
            asBinder = null;
        } else {
            asBinder = r0.asBinder();
        }
        C95654e8.A04(asBinder, parcel, 2);
        C95654e8.A0I(parcel, this.A03, 3, i);
        C95654e8.A0D(parcel, this.A01, 4, false);
        C95654e8.A0D(parcel, this.A02, 5, false);
        C95654e8.A06(parcel, A00);
    }
}
