package X;

/* renamed from: X.0lY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14510lY {
    public final AbstractC14500lX A00;
    public final C14430lQ A01;
    public final C14380lL A02;
    public final C14480lV A03;
    public final C14520lZ A04;

    public C14510lY(AbstractC14500lX r2, C14430lQ r3, C14380lL r4, C14480lV r5) {
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
        this.A00 = r2;
        this.A04 = new C14520lZ();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C14510lY(X.AnonymousClass1KC r4, X.C14380lL r5) {
        /*
            r3 = this;
            X.0lX r2 = r4.A0I
            X.0lQ r1 = r4.A0J
            X.AnonymousClass009.A05(r1)
            X.1K9 r0 = r4.A0L
            X.0lV r0 = r0.A02
            r3.<init>(r2, r1, r5, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14510lY.<init>(X.1KC, X.0lL):void");
    }

    public int A00() {
        return !(this instanceof C37991nL) ? 0 : 2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[job_id=");
        sb.append(this.A01.A0D);
        sb.append("]");
        return sb.toString();
    }
}
