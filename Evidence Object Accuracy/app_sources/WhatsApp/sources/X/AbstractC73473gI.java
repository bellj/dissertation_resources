package X;

import android.text.style.ClickableSpan;

/* renamed from: X.3gI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC73473gI extends ClickableSpan {
    public String A00;
    public String A01;

    public AbstractC73473gI(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }
}
