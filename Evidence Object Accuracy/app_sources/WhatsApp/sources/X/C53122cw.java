package X;

import android.content.Context;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53122cw extends LinearLayout implements AnonymousClass004 {
    public C15570nT A00;
    public AnonymousClass11L A01;
    public AnonymousClass2P7 A02;
    public boolean A03;
    public final TextEmojiLabel A04;
    public final AbstractC33041dC A05;

    public C53122cw(Context context, AbstractC33041dC r5) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A00 = C12970iu.A0S(A00);
            this.A01 = (AnonymousClass11L) A00.ALG.get();
        }
        this.A05 = r5;
        setOrientation(0);
        setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        LinearLayout.inflate(getContext(), R.layout.community_tab_activity, this);
        this.A04 = C12970iu.A0T(this, R.id.community_activity_preview);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }
}
