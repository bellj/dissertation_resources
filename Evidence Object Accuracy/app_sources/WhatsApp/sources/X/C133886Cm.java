package X;

import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.widget.MultiExclusionChip;

/* renamed from: X.6Cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C133886Cm implements AbstractC136116Lb {
    public final /* synthetic */ PaymentTransactionHistoryActivity A00;
    public final /* synthetic */ MultiExclusionChip A01;
    public final /* synthetic */ MultiExclusionChip A02;
    public final /* synthetic */ MultiExclusionChip A03;
    public final /* synthetic */ MultiExclusionChip A04;

    public /* synthetic */ C133886Cm(PaymentTransactionHistoryActivity paymentTransactionHistoryActivity, MultiExclusionChip multiExclusionChip, MultiExclusionChip multiExclusionChip2, MultiExclusionChip multiExclusionChip3, MultiExclusionChip multiExclusionChip4) {
        this.A00 = paymentTransactionHistoryActivity;
        this.A01 = multiExclusionChip;
        this.A02 = multiExclusionChip2;
        this.A03 = multiExclusionChip3;
        this.A04 = multiExclusionChip4;
    }
}
