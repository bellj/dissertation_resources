package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2vK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59752vK extends AbstractC37191le {
    public final WaTextView A00;

    public C59752vK(View view) {
        super(view);
        this.A00 = C12960it.A0N(view, R.id.title);
        C12960it.A0I(view, R.id.description).setText(R.string.biz_area_outside_supported_region);
    }
}
