package X;

import android.net.Uri;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.21d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C453321d {
    public final AnonymousClass016 A00;
    public final AnonymousClass016 A01;
    public final AnonymousClass016 A02;
    public final AnonymousClass016 A03;
    public final AnonymousClass016 A04 = new AnonymousClass016(0);
    public final AnonymousClass016 A05;
    public final C22700zV A06;
    public final C453421e A07;
    public final C22190yg A08;
    public final List A09;
    public final List A0A;
    public final boolean A0B;

    public C453321d(C22700zV r3, C32731ce r4, C453421e r5, C22190yg r6, List list, List list2, int i, int i2, boolean z) {
        AnonymousClass016 r1;
        ArrayList arrayList = new ArrayList();
        this.A0A = arrayList;
        this.A08 = r6;
        this.A07 = r5;
        this.A06 = r3;
        arrayList.addAll(list);
        this.A02 = new AnonymousClass016(Collections.unmodifiableList(arrayList));
        this.A01 = new AnonymousClass016(Integer.valueOf(i));
        this.A00 = new AnonymousClass016(Collections.unmodifiableList(list2));
        this.A09 = Collections.unmodifiableList(list2);
        if (i2 != -1) {
            r1 = new AnonymousClass016(Integer.valueOf(i2));
        } else {
            r1 = new AnonymousClass02P();
        }
        this.A05 = r1;
        r1.A0B(Integer.valueOf(A01()));
        this.A0B = z;
        this.A03 = new AnonymousClass016(r4);
    }

    public byte A00() {
        AnonymousClass016 r1 = this.A02;
        if (0 >= ((List) r1.A01()).size()) {
            return 0;
        }
        return this.A08.A05(this.A07.A00((Uri) ((List) r1.A01()).get(0)));
    }

    public final int A01() {
        byte byteValue;
        AnonymousClass016 r4 = this.A00;
        for (AbstractC14640lm r1 : (List) r4.A01()) {
            if ((r1 instanceof UserJid) && this.A06.A02((UserJid) r1)) {
                return 0;
            }
        }
        if (C15380n4.A0P((List) r4.A01())) {
            return 0;
        }
        AnonymousClass016 r2 = this.A02;
        if (((List) r2.A01()).size() != 1 || (byteValue = Byte.valueOf(A00()).byteValue()) == 13 || byteValue == 29) {
            return 0;
        }
        if (!(!this.A07.A00((Uri) ((List) r2.A01()).get(0)).A0F())) {
            return 1;
        }
        Number number = (Number) this.A05.A01();
        return (number == null || number.intValue() != 3) ? 2 : 3;
    }

    public Uri A02() {
        int intValue = ((Number) this.A01.A01()).intValue();
        if (intValue < 0) {
            return null;
        }
        List list = this.A0A;
        if (intValue < list.size()) {
            return (Uri) list.get(intValue);
        }
        return null;
    }

    public final void A03() {
        int size;
        AnonymousClass016 r1 = this.A02;
        List list = this.A0A;
        r1.A0A(Collections.unmodifiableList(list));
        if (list.isEmpty()) {
            size = -1;
        } else {
            if (((Number) this.A01.A01()).intValue() >= list.size()) {
                size = list.size() - 1;
            }
            A06(A01());
        }
        A04(size);
        A06(A01());
    }

    public void A04(int i) {
        AnonymousClass016 r1 = this.A01;
        if (((Number) r1.A01()).intValue() != i) {
            r1.A0B(Integer.valueOf(i));
            A05(0);
        }
    }

    public void A05(int i) {
        AnonymousClass016 r1 = this.A04;
        if (i != ((Number) r1.A01()).intValue()) {
            r1.A0B(Integer.valueOf(i));
        }
    }

    public final void A06(int i) {
        AnonymousClass016 r1 = this.A05;
        if (i != ((Number) r1.A01()).intValue()) {
            r1.A0B(Integer.valueOf(i));
        }
    }

    public boolean A07() {
        Uri A02 = A02();
        if (A02 == null || this.A08.A05(this.A07.A00(A02)) != 1) {
            return false;
        }
        return true;
    }

    public boolean A08() {
        return ((List) this.A02.A01()).size() > 1;
    }
}
