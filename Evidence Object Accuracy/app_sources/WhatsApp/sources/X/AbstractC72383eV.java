package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3eV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC72383eV {
    void A5n();

    void A7A();

    void A9s(UserJid userJid, int i);

    int AFx(UserJid userJid);

    AbstractC116285Uv AHC(C44691zO v, UserJid userJid, boolean z);

    boolean AIA(UserJid userJid);

    void AIo(UserJid userJid);

    void AQR(UserJid userJid);

    boolean AdS();
}
