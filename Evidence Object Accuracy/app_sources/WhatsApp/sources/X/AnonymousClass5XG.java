package X;

import android.os.Bundle;
import android.os.Handler;
import android.view.Surface;
import java.nio.ByteBuffer;

/* renamed from: X.5XG  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XG {
    ByteBuffer ADV(int i);

    ByteBuffer AEo(int i);

    void Aa7(int i, long j);

    void AcN(Handler handler, AnonymousClass5SL v);

    void AcR(Surface surface);

    void AcS(Bundle bundle);
}
