package X;

import com.whatsapp.registration.RegisterName;
import java.lang.ref.WeakReference;

/* renamed from: X.38U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38U extends AbstractC16350or {
    public WeakReference A00;
    public final C14900mE A01;
    public final C14820m6 A02;
    public final C20850wQ A03;
    public final C27011Fr A04;
    public final AnonymousClass4L4 A05;

    public AnonymousClass38U(C14900mE r2, C14820m6 r3, C20850wQ r4, RegisterName registerName, C27011Fr r6, AnonymousClass4L4 r7) {
        this.A01 = r2;
        this.A05 = r7;
        this.A02 = r3;
        this.A04 = r6;
        this.A03 = r4;
        this.A00 = C12970iu.A10(registerName);
    }
}
