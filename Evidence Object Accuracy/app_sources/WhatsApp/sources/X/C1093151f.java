package X;

/* renamed from: X.51f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1093151f implements AnonymousClass5XA {
    public Object A00;

    @Override // X.AnonymousClass5XA
    public boolean A6g() {
        return C64983Hr.A02(this.A00);
    }

    @Override // X.AnonymousClass5XA
    public boolean AJn() {
        return C12980iv.A1X(this.A00);
    }

    @Override // X.AnonymousClass5XA
    public long AKo() {
        return C12980iv.A0G(this.A00);
    }

    @Override // X.AnonymousClass5XA
    public Number ALj() {
        return (Number) this.A00;
    }

    @Override // X.AnonymousClass5XA
    public String AeX() {
        return (String) this.A00;
    }
}
