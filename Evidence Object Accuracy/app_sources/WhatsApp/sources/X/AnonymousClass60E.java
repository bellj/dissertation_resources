package X;

import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.60E  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass60E {
    public static final ArrayList A06;
    public final long A00;
    public final AnonymousClass1V8 A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A06 = C12960it.A0m("1", strArr, 1);
    }

    public AnonymousClass60E(AnonymousClass1V8 r22) {
        AnonymousClass1V8.A01(r22, "bank");
        AnonymousClass3JT.A04(null, r22, String.class, C12970iu.A0j(), C12970iu.A0k(), "BR", new String[]{"country"}, false);
        this.A00 = C12980iv.A0G(AnonymousClass3JT.A04(null, r22, Long.TYPE, 1L, 100L, null, new String[]{"bank-code"}, false));
        this.A03 = (String) AnonymousClass3JT.A04(null, r22, String.class, 1L, 1000L, null, new String[]{"bank-name"}, false);
        this.A05 = (String) AnonymousClass3JT.A04(null, r22, String.class, 1L, 1000L, null, new String[]{"short-name"}, false);
        this.A04 = (String) AnonymousClass3JT.A04(null, r22, String.class, 1L, Long.valueOf((long) C26061Bw.A0L), null, new String[]{"image"}, false);
        this.A02 = AnonymousClass3JT.A08(r22, A06, new String[]{"accept-savings"});
        this.A01 = r22;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass60E.class != obj.getClass()) {
                return false;
            }
            AnonymousClass60E r7 = (AnonymousClass60E) obj;
            if (!C29941Vi.A00(this.A02, r7.A02) || this.A00 != r7.A00 || !this.A03.equals(r7.A03) || !this.A05.equals(r7.A05) || !this.A04.equals(r7.A04)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[5];
        objArr[0] = this.A02;
        C117315Zl.A0Z(objArr, this.A00);
        objArr[2] = this.A03;
        objArr[3] = this.A05;
        objArr[4] = this.A04;
        return Arrays.hashCode(objArr);
    }
}
