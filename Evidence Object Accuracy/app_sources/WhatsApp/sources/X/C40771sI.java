package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1sI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40771sI extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40771sI A08;
    public static volatile AnonymousClass255 A09;
    public byte A00 = -1;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public AnonymousClass1K6 A05;
    public AnonymousClass1K6 A06;
    public String A07 = "";

    static {
        C40771sI r0 = new C40771sI();
        A08 = r0;
        r0.A0W();
    }

    public C40771sI() {
        AnonymousClass277 r0 = AnonymousClass277.A01;
        this.A06 = r0;
        this.A05 = r0;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A01 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A07) + 0;
        } else {
            i = 0;
        }
        int i3 = this.A01;
        if ((i3 & 2) == 2) {
            i += CodedOutputStream.A05(2, this.A04);
        }
        if ((i3 & 4) == 4) {
            i += CodedOutputStream.A05(3, this.A03);
        }
        if ((i3 & 8) == 8) {
            i += CodedOutputStream.A05(4, this.A02);
        }
        int i4 = 0;
        for (int i5 = 0; i5 < this.A06.size(); i5++) {
            i4 += CodedOutputStream.A0B((String) this.A06.get(i5));
        }
        int size = i + i4 + this.A06.size();
        int i6 = 0;
        for (int i7 = 0; i7 < this.A05.size(); i7++) {
            i6 += CodedOutputStream.A0B((String) this.A05.get(i7));
        }
        int size2 = size + i6 + this.A05.size() + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = size2;
        return size2;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0I(1, this.A07);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0H(2, this.A04);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0H(3, this.A03);
        }
        if ((this.A01 & 8) == 8) {
            codedOutputStream.A0H(4, this.A02);
        }
        for (int i = 0; i < this.A06.size(); i++) {
            codedOutputStream.A0I(5, (String) this.A06.get(i));
        }
        for (int i2 = 0; i2 < this.A05.size(); i2++) {
            codedOutputStream.A0I(6, (String) this.A05.get(i2));
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
