package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import android.util.Base64;
import android.util.Log;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.4df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95414df {
    public static Context A06;
    public static final Object A07 = C12970iu.A0l();
    public static volatile Boolean A08;
    public final AnonymousClass4PM A00;
    public final Object A01;
    public final String A02;
    public final String A03;
    public volatile SharedPreferences A04;
    public volatile C94134bJ A05 = null;

    public static Object A00(AbstractC115585Sd r3) {
        try {
            return r3.AhR();
        } catch (SecurityException unused) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return r3.AhR();
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }

    public static void A01(Context context) {
        Context applicationContext;
        if (A06 == null) {
            synchronized (A07) {
                if ((Build.VERSION.SDK_INT < 24 || !context.isDeviceProtectedStorage()) && (applicationContext = context.getApplicationContext()) != null) {
                    context = applicationContext;
                }
                if (A06 != context) {
                    A08 = null;
                }
                A06 = context;
            }
        }
    }

    public /* synthetic */ AbstractC95414df(AnonymousClass4PM r2, Object obj, String str) {
        if (r2.A00 != null) {
            this.A00 = r2;
            this.A03 = C72453ed.A0r(String.valueOf(r2.A01), str);
            this.A02 = C72453ed.A0r(String.valueOf(r2.A02), str);
            this.A01 = obj;
            return;
        }
        throw C12970iu.A0f("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    public static boolean A02() {
        String str;
        if (A08 == null) {
            Context context = A06;
            boolean z = false;
            if (context == null) {
                return false;
            }
            if (Binder.getCallingPid() == Process.myPid()) {
                str = context.getPackageName();
            } else {
                str = null;
            }
            if (AnonymousClass0KX.A00(context, "com.google.android.providers.gsf.permission.READ_GSERVICES", str, Binder.getCallingPid(), Binder.getCallingUid()) == 0) {
                z = true;
            }
            A08 = Boolean.valueOf(z);
        }
        return A08.booleanValue();
    }

    public final Object A03() {
        String str;
        if (A06 != null) {
            Object A04 = A04();
            return (A04 == null && (!A02() || (str = (String) A00(new AbstractC115585Sd(this) { // from class: X.4zB
                public final AbstractC95414df A00;

                {
                    this.A00 = r1;
                }

                @Override // X.AbstractC115585Sd
                public final Object AhR() {
                    return C95244dN.A00(AbstractC95414df.A06.getContentResolver(), this.A00.A03);
                }
            })) == null || (A04 = A05(str)) == null)) ? this.A01 : A04;
        }
        throw C12960it.A0U("Must call PhenotypeFlag.init() first");
    }

    public final Object A04() {
        boolean z;
        if (A02()) {
            z = C12970iu.A1Y(A00(new C108504zC("gms:phenotype:phenotype_flag:debug_bypass_phenotype")));
        } else {
            z = false;
        }
        if (!z) {
            Uri uri = this.A00.A00;
            if (uri != null) {
                if (this.A05 == null) {
                    ContentResolver contentResolver = A06.getContentResolver();
                    ConcurrentHashMap concurrentHashMap = C94134bJ.A07;
                    C94134bJ r3 = (C94134bJ) concurrentHashMap.get(uri);
                    if (r3 == null) {
                        r3 = new C94134bJ(contentResolver, uri);
                        C94134bJ r0 = (C94134bJ) concurrentHashMap.putIfAbsent(uri, r3);
                        if (r0 == null) {
                            r3.A00.registerContentObserver(r3.A02, false, r3.A01);
                        } else {
                            r3 = r0;
                        }
                    }
                    this.A05 = r3;
                }
                String str = (String) A00(new AbstractC115585Sd(this.A05, this) { // from class: X.4zD
                    public final C94134bJ A00;
                    public final AbstractC95414df A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC115585Sd
                    public final Object AhR() {
                        boolean z2;
                        AbstractC95414df r4 = this.A01;
                        C94134bJ r32 = this.A00;
                        if (AbstractC95414df.A02()) {
                            z2 = C12970iu.A1Y(AbstractC95414df.A00(new C108504zC("gms:phenotype:phenotype_flag:debug_disable_caching")));
                        } else {
                            z2 = false;
                        }
                        Map A00 = z2 ? r32.A00() : r32.A06;
                        if (A00 == null) {
                            synchronized (r32.A03) {
                                A00 = r32.A06;
                                if (A00 == null) {
                                    A00 = r32.A00();
                                    r32.A06 = A00;
                                }
                            }
                        }
                        if (A00 == null) {
                            A00 = Collections.emptyMap();
                        }
                        return A00.get(r4.A02);
                    }
                });
                if (str != null) {
                    return A05(str);
                }
            }
        } else {
            Log.w("PhenotypeFlag", C72453ed.A0r("Bypass reading Phenotype values for flag: ", this.A02));
        }
        return null;
    }

    public Object A05(String str) {
        Object obj;
        if (this instanceof C79113q4) {
            C79113q4 r2 = (C79113q4) this;
            try {
                synchronized (r2.A02) {
                    if (!str.equals(r2.A01)) {
                        byte[] decode = Base64.decode(str, 3);
                        AbstractC79123q5 r9 = (AbstractC79123q5) C79343qR.zzbir.A05(4);
                        try {
                            C93984b4 r6 = C93984b4.A02;
                            Class<?> cls = r9.getClass();
                            r6.A00(cls).Agx(new AnonymousClass4PN(), r9, decode, 0, decode.length);
                            r6.A00(cls).AhF(r9);
                            if (r9.zzex == 0) {
                                byte byteValue = ((Byte) r9.A05(1)).byteValue();
                                if (byteValue != 1) {
                                    if (byteValue != 0) {
                                        boolean AhQ = r6.A00(cls).AhQ(r9);
                                        r9.A05(2);
                                        if (AhQ) {
                                        }
                                    }
                                    C868048x r0 = new C868048x(new C113305Gz().getMessage());
                                    r0.zzkw = r9;
                                    throw r0;
                                }
                                r2.A01 = str;
                                r2.A00 = (C79343qR) r9;
                            } else {
                                throw new RuntimeException();
                            }
                        } catch (IOException e) {
                            if (e.getCause() instanceof C868048x) {
                                throw e.getCause();
                            }
                            C868048x r02 = new C868048x(e.getMessage());
                            r02.zzkw = r9;
                            throw r02;
                        } catch (IndexOutOfBoundsException unused) {
                            C868048x A00 = C868048x.A00();
                            A00.zzkw = r9;
                            throw A00;
                        }
                    }
                    obj = r2.A00;
                }
                return obj;
            } catch (IOException | IllegalArgumentException unused2) {
                String str2 = ((AbstractC95414df) r2).A02;
                StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str2) + 27 + C12970iu.A07(str));
                A0t.append("Invalid byte[] value for ");
                C12990iw.A1T(A0t, str2);
                Log.e("PhenotypeFlag", C12960it.A0d(str, A0t));
                return null;
            }
        } else if (this instanceof C79103q3) {
            return str;
        } else {
            if (C95244dN.A0B.matcher(str).matches()) {
                return Boolean.TRUE;
            }
            if (C95244dN.A0C.matcher(str).matches()) {
                return Boolean.FALSE;
            }
            String str3 = this.A02;
            StringBuilder A0t2 = C12980iv.A0t(C12970iu.A07(str3) + 28 + C12970iu.A07(str));
            A0t2.append("Invalid boolean value for ");
            C12990iw.A1T(A0t2, str3);
            Log.e("PhenotypeFlag", C12960it.A0d(str, A0t2));
            return null;
        }
    }
}
