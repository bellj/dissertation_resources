package X;

import com.whatsapp.util.Log;

/* renamed from: X.0tb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19120tb extends AbstractC18500sY implements AbstractC19010tQ {
    public final C20940wZ A00;
    public final C20930wY A01;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19120tb(C20940wZ r3, C20930wY r4, C18480sW r5) {
        super(r5, "message_system", 2);
        this.A01 = r4;
        this.A00 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b2, code lost:
        if (r13 == 7) goto L_0x00b4;
     */
    @Override // X.AbstractC18500sY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass2Ez A09(android.database.Cursor r24) {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19120tb.A09(android.database.Cursor):X.2Ez");
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r3 = A02.A03;
            r3.A01("message_system", null, null);
            r3.A01("message_system_group", null, null);
            r3.A01("message_system_photo_change", null, null);
            r3.A01("message_system_number_change", null, null);
            r3.A01("message_system_device_change", null, null);
            r3.A01("message_system_initial_privacy_provider", null, null);
            r3.A01("message_system_value_change", null, null);
            r3.A01("message_system_chat_participant", null, null);
            r3.A01("message_payment", null, null);
            r3.A01("message_payment_status_update", null, null);
            r3.A01("message_system_block_contact", null, null);
            r3.A01("message_system_ephemeral_setting_not_applied", null, null);
            r3.A01("message_payment_transaction_reminder", null, null);
            r3.A01("message_system_payment_invite_setup", null, null);
            C21390xL r1 = this.A06;
            r1.A03("system_message_ready");
            r1.A03("migration_message_system_index");
            r1.A03("migration_message_system_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("SystemMessageStore/SystemMessageStoreDatabaseMigration/resetMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
