package X;

import java.util.HashMap;
import java.util.LinkedHashSet;

/* renamed from: X.4ZW  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4ZW {
    public static HashMap A00 = C12970iu.A11();
    public static HashMap A01 = C12970iu.A11();

    static {
        A00(C88284Ez.class);
        A00(AnonymousClass3JM.class);
    }

    public static void A00(Class cls) {
        HashMap hashMap = A01;
        synchronized (hashMap) {
            LinkedHashSet linkedHashSet = (LinkedHashSet) hashMap.get(Object.class);
            if (linkedHashSet == null) {
                linkedHashSet = new LinkedHashSet();
                hashMap.put(Object.class, linkedHashSet);
            }
            linkedHashSet.add(cls);
        }
    }
}
