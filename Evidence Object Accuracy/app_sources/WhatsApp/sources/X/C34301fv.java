package X;

/* renamed from: X.1fv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34301fv extends AbstractC16110oT {
    public Boolean A00;

    public C34301fv() {
        super(2788, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamArchiveSettings {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "keepChatsArchived", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
