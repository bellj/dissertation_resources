package X;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.WallPaperView;

/* renamed from: X.2r2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58592r2 extends C28791Pa {
    public final Activity A00;
    public final ViewGroup A01;
    public final AnonymousClass3UV A02;
    public final AbstractC14640lm A03;
    public final AbstractC15850o0 A04;
    public final WallPaperView A05;
    public final AbstractC14440lR A06;

    public C58592r2(Activity activity, ViewGroup viewGroup, AbstractC13860kS r12, C14900mE r13, AnonymousClass4NU r14, AnonymousClass01d r15, AbstractC14640lm r16, AbstractC15850o0 r17, WallPaperView wallPaperView, AbstractC14440lR r19, Runnable runnable) {
        this.A03 = r16;
        this.A00 = activity;
        this.A06 = r19;
        this.A04 = r17;
        this.A01 = viewGroup;
        this.A05 = wallPaperView;
        this.A02 = new AnonymousClass3UV(activity, r12, r13, new AnonymousClass3WS(this, wallPaperView, runnable), r14, r15, r17);
    }

    public final void A00(Drawable drawable) {
        ViewGroup viewGroup;
        int i;
        if (drawable != null) {
            this.A05.setDrawable(drawable);
            viewGroup = this.A01;
            i = 0;
        } else {
            WallPaperView wallPaperView = this.A05;
            wallPaperView.A04 = false;
            wallPaperView.setImageDrawable(null);
            wallPaperView.invalidate();
            viewGroup = this.A01;
            i = R.color.conversation_background;
        }
        viewGroup.setBackgroundResource(i);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        super.onActivityCreated(activity, bundle);
        A00(null);
        AbstractC14440lR r5 = this.A06;
        AbstractC14640lm r4 = this.A03;
        C12960it.A1E(new AnonymousClass37Y(this.A00, new AnonymousClass4KU(this), r4, this.A04), r5);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
        super.onActivityResumed(activity);
        AbstractC15850o0 r5 = this.A04;
        if (r5.A00) {
            C12960it.A1E(new AnonymousClass37Y(this.A00, new AnonymousClass4KU(this), this.A03, r5), this.A06);
            r5.A00 = false;
        }
    }
}
