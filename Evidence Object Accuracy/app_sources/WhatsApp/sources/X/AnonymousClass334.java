package X;

import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;

/* renamed from: X.334  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass334 extends C64453Fp {
    public final /* synthetic */ ImageComposerFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass334(ImageComposerFragment imageComposerFragment) {
        super(imageComposerFragment);
        this.A00 = imageComposerFragment;
    }

    @Override // X.C64453Fp
    public void A01() {
        MediaComposerActivity mediaComposerActivity = (MediaComposerActivity) ((AnonymousClass21Y) this.A00.A0B());
        AnonymousClass3YE r2 = mediaComposerActivity.A0e;
        boolean A08 = mediaComposerActivity.A0b.A08();
        mediaComposerActivity.A0b.A07();
        r2.A02(A08);
    }

    @Override // X.C64453Fp
    public void A02() {
        ImageComposerFragment imageComposerFragment = this.A00;
        ((AnonymousClass21Y) imageComposerFragment.A0B()).APM();
        AnonymousClass3MP r0 = imageComposerFragment.A06.A05;
        if (r0.A00 <= r0.A03 && ((MediaComposerFragment) imageComposerFragment).A0D.A0E.A05.getVisibility() != 0) {
            imageComposerFragment.A1K(true, false);
        }
    }
}
