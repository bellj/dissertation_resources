package X;

/* renamed from: X.5zy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130785zy {
    public C452120p A00;
    public C1316663q A01;
    public final Object A02;

    public C130785zy(C452120p r1, Object obj) {
        this.A02 = obj;
        this.A00 = r1;
    }

    public static String A00(C130785zy r1, AbstractActivityC123635nW r2) {
        if (r1.A01 != null) {
            return "on_step_up";
        }
        r2.A05.A02(r1.A00, null, null);
        return "on_failure_handled_natively";
    }

    public static void A01(AnonymousClass017 r1, C452120p r2, Object obj) {
        r1.A0B(new C130785zy(r2, obj));
    }

    public static void A02(AnonymousClass017 r1, C452120p r2, Object obj) {
        r1.A0A(new C130785zy(r2, obj));
    }

    public static void A03(C452120p r2, AbstractC136196Lo r3) {
        C130785zy r0 = new C130785zy(r2, null);
        r0.A01 = null;
        r3.AV8(r0);
    }

    public static void A04(C452120p r1, AbstractC136196Lo r2, Object obj) {
        r2.AV8(new C130785zy(r1, obj));
    }

    public static void A05(AbstractC136196Lo r4, C130785zy r5) {
        C452120p r2 = r5.A00;
        C1316663q r1 = r5.A01;
        C130785zy r0 = new C130785zy(r2, null);
        r0.A01 = r1;
        r4.AV8(r0);
    }

    public boolean A06() {
        return this.A02 != null && this.A00 == null && this.A01 == null;
    }
}
