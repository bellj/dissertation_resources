package X;

import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.math.BigDecimal;

/* renamed from: X.606  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass606 {
    public static final C28601Of A0E;
    public static final AnonymousClass1JO A0F;
    public static final AnonymousClass1JO A0G;
    public static final AnonymousClass1JO A0H;
    public final C15610nY A00;
    public final AnonymousClass018 A01;
    public final C20830wO A02;
    public final C20370ve A03;
    public final AbstractC30791Yv A04;
    public final C129925yW A05;
    public final C18610sj A06;
    public final C17900ra A07;
    public final C22710zW A08;
    public final C30931Zj A09;
    public final AbstractActivityC121665jA A0A;
    public final C125885ry A0B;
    public final AbstractC14440lR A0C;
    public final AnonymousClass01H A0D;

    static {
        AnonymousClass1YJ r1 = new AnonymousClass1YJ();
        A00(r1, 404);
        A00(r1, 440);
        A00(r1, 442);
        A00(r1, 443);
        AnonymousClass1JO A00 = r1.A00();
        A0G = A00;
        AnonymousClass1YJ r12 = new AnonymousClass1YJ();
        r12.A01(A00);
        r12.A02(11502);
        r12.A02(17010);
        A00(r12, 11455);
        A00(r12, 11466);
        A00(r12, 4002);
        A00(r12, 11481);
        A00(r12, 11478);
        A00(r12, 11480);
        A00(r12, 11465);
        A00(r12, 11479);
        A00(r12, 12750);
        A00(r12, 20951);
        AnonymousClass1JO A002 = r12.A00();
        A0H = A002;
        AnonymousClass1YJ r13 = new AnonymousClass1YJ();
        r13.A01(A00);
        r13.A02(11502);
        r13.A02(17010);
        A00(r13, 11503);
        A00(r13, 11495);
        AnonymousClass1JO A003 = r13.A00();
        A0F = A003;
        AnonymousClass1YB r14 = new AnonymousClass1YB();
        r14.A01("pay-precheck", A002);
        r14.A01("upi-accept-collect", A003);
        A0E = r14.A00();
    }

    public AnonymousClass606(C15610nY r2, AnonymousClass018 r3, C20830wO r4, C20370ve r5, C129925yW r6, C18610sj r7, C17900ra r8, C22710zW r9, C30931Zj r10, AbstractActivityC121665jA r11, C125885ry r12, AbstractC14440lR r13, AnonymousClass01H r14) {
        AbstractC30791Yv r0 = C30771Yt.A05;
        this.A0A = r11;
        this.A0C = r13;
        this.A00 = r2;
        this.A01 = r3;
        this.A09 = r10;
        this.A04 = r0;
        this.A06 = r7;
        this.A08 = r9;
        this.A07 = r8;
        this.A05 = r6;
        this.A03 = r5;
        this.A02 = r4;
        this.A0D = r14;
        this.A0B = r12;
    }

    public static void A00(AnonymousClass1YJ r1, int i) {
        r1.A02(Integer.valueOf(i));
    }

    public void A01(C127865vB r9, String str, int i) {
        C30931Zj r2;
        StringBuilder sb;
        String str2;
        int i2;
        Object[] objArr;
        C125885ry r1;
        String string;
        C30821Yy r22;
        Object obj = A0E.A00.get(str);
        AnonymousClass009.A05(obj);
        if (((AnonymousClass1JO) obj).A00.contains(Integer.valueOf(i))) {
            AbstractActivityC121665jA r3 = this.A0A;
            if (!AnonymousClass69E.A02(r3, str, i, false)) {
                if (i != 4002) {
                    if (i != 11455) {
                        if (i == 11495) {
                            this.A09.A06(C12960it.A0W(i, "collect request expired; showErrorAndFinish; error code: "));
                            this.A0C.Ab2(new Runnable(r9) { // from class: X.6Hu
                                public final /* synthetic */ C127865vB A01;

                                {
                                    this.A01 = r2;
                                }

                                @Override // java.lang.Runnable
                                public final void run() {
                                    AnonymousClass606 r32 = AnonymousClass606.this;
                                    C127865vB r23 = this.A01;
                                    C30931Zj r12 = r32.A09;
                                    StringBuilder A0k = C12960it.A0k("onPayRequestFromNonWa; request is expired; transaction id: ");
                                    String str3 = r23.A03;
                                    r12.A06(C12960it.A0d(str3, A0k));
                                    C20370ve r13 = r32.A03;
                                    r13.A0c(r13.A0N(null, str3));
                                }
                            });
                            C17900ra r12 = this.A07;
                            if (r12.A00() == null || (r22 = r9.A00) == null) {
                                string = r3.getString(R.string.unknown_amount_payment);
                            } else {
                                string = r12.A00().AAA(this.A01, r22, 0);
                            }
                            this.A0B.A00.A3X(new AnonymousClass60V(R.string.payments_request_system_message_to_me_expired), r9.A04, string);
                            return;
                        } else if (i == 12750) {
                            this.A09.A06(C12960it.A0W(i, "request has been cancelled; showErrorAndFinish; error code: "));
                            String str3 = r9.A02;
                            UserJid userJid = r9.A01;
                            if (userJid != null) {
                                str3 = this.A00.A04(this.A02.A01(userJid));
                            }
                            r1 = this.A0B;
                            i2 = R.string.payments_request_cancelled;
                            objArr = new Object[]{str3};
                            r1.A00.A3X(new AnonymousClass60V(i2), objArr);
                            return;
                        } else if (i == 17010) {
                            C36021jC.A01(r3, 26);
                            return;
                        } else if (i != 20951) {
                            if (i != 11465) {
                                if (i != 11466) {
                                    if (i != 11502) {
                                        if (i != 11503) {
                                            switch (i) {
                                                case 11478:
                                                case 11480:
                                                case 11481:
                                                    break;
                                                case 11479:
                                                    break;
                                                default:
                                                    return;
                                            }
                                        } else {
                                            this.A0B.A00.A3X(new AnonymousClass60V(R.string.transaction_details_issuer_max_amount, this.A05.A00(i)), new Object[0]);
                                            return;
                                        }
                                    }
                                }
                            }
                            ((C120495gH) this.A0D.get()).A00(r9.A01, null, null);
                            r2 = this.A09;
                            sb = C12960it.A0h();
                            str2 = "invalid receiver vpa; showErrorAndFinish; error code: ";
                        } else {
                            Bundle A0D = C12970iu.A0D();
                            A0D.putInt("error_code", i);
                            C36021jC.A02(r3, A0D, 33);
                            return;
                        }
                    }
                    this.A09.A06("sender max transactions or max amount per day limit; showErrorAndFinish");
                    String AAB = this.A04.AAB(this.A01, new BigDecimal(100000), 0);
                    r1 = this.A0B;
                    i2 = R.string.payments_max_transactions_or_max_amount_sent_per_day_limit_reached;
                    objArr = new Object[2];
                    C12960it.A1P(objArr, 10, 0);
                    objArr[1] = AAB;
                    r1.A00.A3X(new AnonymousClass60V(i2), objArr);
                    return;
                }
                this.A06.A08(null, 2);
                r2 = this.A09;
                sb = C12960it.A0h();
                str2 = "invalid sender vpa; showErrorAndFinish; get-methods; error code: ";
            } else {
                return;
            }
        } else {
            r2 = this.A09;
            sb = C12960it.A0j(str);
            str2 = " error; showErrorAndFinish; error code: ";
        }
        sb.append(str2);
        sb.append(i);
        C117295Zj.A1F(r2, sb);
        this.A0B.A00.A39();
    }
}
