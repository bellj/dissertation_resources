package X;

import com.whatsapp.jid.Jid;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collections;

/* renamed from: X.61S  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61S {
    public Jid A00;
    public AnonymousClass601 A01;
    public Object A02;
    public String A03;

    public AnonymousClass61S(int i) {
        this.A03 = "version";
        this.A02 = Integer.valueOf(i);
    }

    public AnonymousClass61S(String str, double d) {
        this.A03 = str;
        this.A02 = Double.valueOf(d);
    }

    public AnonymousClass61S(String str, long j) {
        this.A03 = str;
        this.A02 = Long.valueOf(j);
    }

    public AnonymousClass61S(String str, String str2) {
        this.A03 = str;
        this.A02 = str2;
    }

    public AnonymousClass61S(String str, boolean z) {
        this.A03 = str;
        this.A02 = Boolean.valueOf(z);
    }

    public static AnonymousClass61S A00(String str, String str2) {
        return new AnonymousClass61S(str, str2);
    }

    public static C1310460z A01(String str) {
        return new C1310460z("account", new ArrayList(Collections.singletonList(new AnonymousClass61S("action", str))));
    }

    public static ArrayList A02(String str, String str2) {
        return new ArrayList(Collections.singletonList(new AnonymousClass61S(str, str2)));
    }

    public static void A03(String str, String str2, AbstractCollection abstractCollection) {
        abstractCollection.add(new AnonymousClass61S(str, str2));
    }

    public static void A04(String str, String str2, Object[] objArr) {
        objArr[0] = new AnonymousClass61S(str, str2);
    }

    public static void A05(String str, String str2, Object[] objArr, int i) {
        objArr[i] = new AnonymousClass61S(str, str2);
    }
}
