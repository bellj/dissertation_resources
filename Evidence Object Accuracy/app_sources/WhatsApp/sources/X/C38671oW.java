package X;

import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.ViewOnClickCListenerShape0S0201000_I0;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStoreMyTabFragment;
import com.whatsapp.stickers.StickerStoreTabFragment;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.util.List;

/* renamed from: X.1oW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38671oW extends AnonymousClass02M {
    public List A00;
    public final /* synthetic */ StickerStoreTabFragment A01;

    public C38671oW(StickerStoreTabFragment stickerStoreTabFragment, List list) {
        this.A01 = stickerStoreTabFragment;
        this.A00 = list;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        List list = this.A00;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r13, int i) {
        String str;
        C55132hq r132 = (C55132hq) r13;
        AnonymousClass1KZ r2 = (AnonymousClass1KZ) this.A00.get(i);
        r132.A0B.setText(r2.A0H);
        long j = r2.A08;
        int i2 = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        TextView textView = r132.A0C;
        if (i2 > 0) {
            textView.setText(C44891zj.A03(this.A01.A06, j));
            textView.setVisibility(0);
            r132.A01.setVisibility(0);
        } else {
            textView.setVisibility(4);
            r132.A01.setVisibility(4);
        }
        r132.A0D.setText(r2.A0F);
        View view = r132.A02;
        view.setClickable(true);
        view.setOnClickListener(new ViewOnClickCListenerShape0S0201000_I0(this, r2, i, 1));
        StickerStoreTabFragment stickerStoreTabFragment = this.A01;
        int dimensionPixelSize = stickerStoreTabFragment.A02().getDimensionPixelSize(R.dimen.sticker_store_row_preview_item);
        if (r132.A00 == null) {
            C38721ob A04 = stickerStoreTabFragment.A0C.A04();
            AnonymousClass1AB r5 = stickerStoreTabFragment.A0A;
            if (!(stickerStoreTabFragment instanceof StickerStoreMyTabFragment)) {
                str = "sticker_store_featured_tab";
            } else {
                str = "sticker_store_my_tab";
            }
            r132.A00 = new C54472gm(r5, null, A04, dimensionPixelSize, 0, false, "sticker_store_my_tab".equals(str));
        }
        C91014Qc r1 = new C91014Qc();
        r1.A02 = r2;
        r1.A01 = new SparseBooleanArray();
        r1.A00 = new SparseBooleanArray();
        C54472gm r0 = r132.A00;
        r0.A04 = r1;
        int i3 = stickerStoreTabFragment.A00;
        r0.A00 = i3;
        r132.A0F.A1h(i3);
        r132.A00.A02();
        r132.A0G.setAdapter(r132.A00);
        r132.A0E.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(r2, 33, this));
        boolean z = r2.A0M;
        ImageView imageView = r132.A05;
        if (z) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(8);
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        StickerStoreTabFragment stickerStoreTabFragment = this.A01;
        View inflate = stickerStoreTabFragment.A01.inflate(R.layout.sticker_store_row, viewGroup, false);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.sticker_row_recycler);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.A0l(new C74943j2(this, stickerStoreTabFragment.A02().getDimensionPixelSize(R.dimen.sticker_store_row_preview_padding)));
        int dimensionPixelSize = stickerStoreTabFragment.A02().getDimensionPixelSize(R.dimen.sticker_store_row_preview_item);
        if (stickerStoreTabFragment.A00 == 0) {
            stickerStoreTabFragment.A00 = Math.min(5, Math.max(viewGroup.getWidth() / dimensionPixelSize, 1));
        }
        return new C55132hq(inflate, stickerStoreTabFragment);
    }
}
