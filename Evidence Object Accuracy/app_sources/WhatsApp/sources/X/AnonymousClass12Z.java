package X;

import android.text.TextUtils;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.12Z  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass12Z {
    public Object A00;

    public void A00(JSONObject jSONObject, long j) {
        String str;
        AnonymousClass4OF r5;
        boolean z;
        long j2;
        JSONObject jSONObject2;
        if (this instanceof AnonymousClass18A) {
            AnonymousClass18A r0 = (AnonymousClass18A) this;
            if (!(r0 instanceof AnonymousClass18C)) {
                str = "whatsapp_support_ban_appeal_status";
            } else {
                str = "whatsapp_support_process_ban_appeal_request";
            }
            r0.A00 = jSONObject.getJSONObject(str);
        } else if (this instanceof C25741Ao) {
            JSONObject jSONObject3 = jSONObject.getJSONObject("whatsapp_commerce_message_type_getmetadata");
            this.A00 = new AnonymousClass3M3(jSONObject3.getString("title"), jSONObject3.getString("subtitle"), jSONObject3.getString("stitched_image_url"), jSONObject3.getString("stitched_image_bytes"));
        } else if (this instanceof AnonymousClass32A) {
            JSONObject jSONObject4 = jSONObject.getJSONObject("whatsapp_biz_integrity_p2b_report");
            String optString = jSONObject4.optString("status");
            if (!TextUtils.isEmpty(optString) && !"INVALID".equals(optString) && !"NOT_FOUND".equals(optString)) {
                if ("PENDING".equals(optString)) {
                    r5 = new AnonymousClass4OF(null, "PENDING");
                } else {
                    JSONObject optJSONObject = jSONObject4.optJSONObject("detail");
                    if (optJSONObject != null) {
                        String optString2 = optJSONObject.optString("report_url");
                        String optString3 = optJSONObject.optString("file_name");
                        String optString4 = optJSONObject.optString("media_key");
                        String optString5 = optJSONObject.optString("file_sha256");
                        String optString6 = optJSONObject.optString("encrypted_file_sha256");
                        String optString7 = optJSONObject.optString("direct_path");
                        int optInt = optJSONObject.optInt("file_size_bytes");
                        if (!TextUtils.isEmpty(optString2) && !TextUtils.isEmpty(optString3) && !TextUtils.isEmpty(optString4) && !TextUtils.isEmpty(optString5) && !TextUtils.isEmpty(optString6) && !TextUtils.isEmpty(optString7)) {
                            r5 = new AnonymousClass4OF(new C91804Te(optString2, optString3, optString4, optInt, optString5, optString6, optString7), "AVAILABLE");
                        }
                    }
                }
                this.A00 = r5;
            }
            r5 = new AnonymousClass4OF(null, "INVALID");
            this.A00 = r5;
        } else if (!(this instanceof AnonymousClass1ER)) {
            if (!(this instanceof AnonymousClass329)) {
                if (!(this instanceof AnonymousClass328)) {
                    if (this instanceof C236012h) {
                        if (jSONObject == null) {
                            z = false;
                        } else {
                            z = !jSONObject.isNull("avatar_static_config");
                        }
                        this.A00 = new C65913Lo(z);
                    } else if (!(this instanceof AnonymousClass12Y)) {
                        AnonymousClass1FE r02 = (AnonymousClass1FE) this;
                        ((AnonymousClass12Z) r02).A00 = r02.A00.A7i(jSONObject, j);
                    } else {
                        if (jSONObject == null || (jSONObject2 = jSONObject.getJSONObject("avatar_delete")) == null) {
                            j2 = 0;
                        } else {
                            j2 = jSONObject2.getLong("client_mutation_id");
                        }
                        this.A00 = new AnonymousClass4XZ(j2);
                    }
                } else if (jSONObject != null && jSONObject.has("fetch__WAAvatar")) {
                    JSONObject jSONObject5 = jSONObject.getJSONObject("fetch__WAAvatar");
                    if (jSONObject5.has("wa_stickers")) {
                        JSONObject jSONObject6 = jSONObject5.getJSONObject("wa_stickers");
                        if (jSONObject6.has("stickers")) {
                            JSONArray jSONArray = jSONObject6.getJSONArray("stickers");
                            if (jSONArray.length() != 0) {
                                this.A00 = new C100494m0(jSONArray.getJSONObject(0).getString("url"));
                            }
                        }
                    }
                }
            } else if (jSONObject != null && jSONObject.has("fetch__WAAvatar")) {
                JSONObject jSONObject7 = jSONObject.getJSONObject("fetch__WAAvatar");
                if (jSONObject7.has("wa_stickers")) {
                    JSONObject jSONObject8 = jSONObject7.getJSONObject("wa_stickers");
                    if (jSONObject8.has("stickers")) {
                        JSONArray jSONArray2 = jSONObject8.getJSONArray("stickers");
                        if (jSONArray2.length() != 0) {
                            ArrayList arrayList = new ArrayList();
                            int i = 0;
                            int length = jSONArray2.length();
                            while (i < length) {
                                int i2 = i + 1;
                                String string = jSONArray2.getJSONObject(i).getString("url");
                                C16700pc.A0B(string);
                                arrayList.add(string);
                                i = i2;
                            }
                            this.A00 = new C65923Lp(arrayList);
                        }
                    }
                }
            }
        } else if (jSONObject != null && jSONObject.has("fetch__WAAvatar")) {
            JSONObject jSONObject9 = jSONObject.getJSONObject("fetch__WAAvatar");
            if (jSONObject9.has("id") && jSONObject9.has("wa_stickers")) {
                String string2 = jSONObject9.getString("id");
                JSONObject jSONObject10 = jSONObject9.getJSONObject("wa_stickers");
                if (jSONObject10.has("stickers")) {
                    JSONArray jSONArray3 = jSONObject10.getJSONArray("stickers");
                    if (jSONArray3.length() != 0) {
                        ArrayList arrayList2 = new ArrayList();
                        int length2 = jSONArray3.length();
                        int i3 = 0;
                        while (i3 < length2) {
                            int i4 = i3 + 1;
                            JSONObject jSONObject11 = jSONArray3.getJSONObject(i3);
                            JSONArray jSONArray4 = jSONObject11.getJSONArray("emojis");
                            ArrayList arrayList3 = new ArrayList();
                            int length3 = jSONArray4.length();
                            int i5 = 0;
                            while (i5 < length3) {
                                int i6 = i5 + 1;
                                String string3 = jSONArray4.getString(i5);
                                C16700pc.A0B(string3);
                                arrayList3.add(string3);
                                i5 = i6;
                            }
                            String string4 = jSONObject11.getString("url");
                            C16700pc.A0B(string4);
                            String join = TextUtils.join(" ", arrayList3);
                            C16700pc.A0B(join);
                            int i7 = jSONObject11.getInt("file_size");
                            String string5 = jSONObject11.getString("mimetype");
                            C16700pc.A0B(string5);
                            int i8 = jSONObject11.getInt("height");
                            int i9 = jSONObject11.getInt("width");
                            String string6 = jSONObject11.getString("file_hash");
                            C16700pc.A0B(string6);
                            arrayList2.add(new C65943Lr(string4, join, string5, string6, i7, i8, i9));
                            i3 = i4;
                        }
                        this.A00 = new C65933Lq(string2, new ArrayList(arrayList2));
                    }
                }
            }
        }
    }
}
