package X;

import android.os.Build;
import android.util.Log;
import android.util.Pair;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;

/* renamed from: X.03i  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03i {
    public static SimpleDateFormat A0I;
    public static SimpleDateFormat A0J;
    public static final Charset A0K;
    public static final HashMap A0L = new HashMap();
    public static final HashSet A0M = new HashSet(Arrays.asList("FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"));
    public static final List A0N = Arrays.asList(2, 7, 4, 5);
    public static final List A0O = Arrays.asList(1, 6, 3, 8);
    public static final Pattern A0P = Pattern.compile("^(\\d{4}):(\\d{2}):(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
    public static final Pattern A0Q = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
    public static final Pattern A0R = Pattern.compile("^(\\d{2}):(\\d{2}):(\\d{2})$");
    public static final Pattern A0S = Pattern.compile(".*[1-9].*");
    public static final boolean A0T = Log.isLoggable("ExifInterface", 3);
    public static final byte[] A0U = {65, 83, 67, 73, 73, 0, 0, 0};
    public static final byte[] A0V = {104, 101, 105, 99};
    public static final byte[] A0W = {109, 105, 102, 49};
    public static final byte[] A0X = {102, 116, 121, 112};
    public static final byte[] A0Y;
    public static final byte[] A0Z;
    public static final byte[] A0a = {-1, -40, -1};
    public static final byte[] A0b = {79, 76, 89, 77, 80, 0};
    public static final byte[] A0c = {79, 76, 89, 77, 80, 85, 83, 0, 73, 73};
    public static final byte[] A0d = {101, 88, 73, 102};
    public static final byte[] A0e = {73, 69, 78, 68};
    public static final byte[] A0f = {73, 72, 68, 82};
    public static final byte[] A0g = {-119, 80, 78, 71, 13, 10, 26, 10};
    public static final byte[] A0h = "ANIM".getBytes(Charset.defaultCharset());
    public static final byte[] A0i = "ANMF".getBytes(Charset.defaultCharset());
    public static final byte[] A0j = {69, 88, 73, 70};
    public static final byte[] A0k = "VP8 ".getBytes(Charset.defaultCharset());
    public static final byte[] A0l = "VP8L".getBytes(Charset.defaultCharset());
    public static final byte[] A0m = "VP8X".getBytes(Charset.defaultCharset());
    public static final byte[] A0n = {82, 73, 70, 70};
    public static final byte[] A0o = {87, 69, 66, 80};
    public static final byte[] A0p = {-99, 1, 42};
    public static final int[] A0q = {8};
    public static final int[] A0r = {8, 8, 8};
    public static final int[] A0s = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1};
    public static final C05950Rp[] A0t = {new C05950Rp(330, "SubIFDPointer", 4), new C05950Rp(34665, "ExifIFDPointer", 4), new C05950Rp(34853, "GPSInfoIFDPointer", 4), new C05950Rp(40965, "InteroperabilityIFDPointer", 4), new C05950Rp(8224, "CameraSettingsIFDPointer", 1), new C05950Rp(8256, "ImageProcessingIFDPointer", 1)};
    public static final C05950Rp[] A0u;
    public static final C05950Rp[] A0v;
    public static final C05950Rp[] A0w;
    public static final C05950Rp[] A0x;
    public static final C05950Rp[] A0y;
    public static final C05950Rp[] A0z;
    public static final C05950Rp[] A10;
    public static final C05950Rp[] A11;
    public static final C05950Rp[] A12;
    public static final String[] A13 = {"", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD"};
    public static final HashMap[] A14 = new HashMap[10];
    public static final HashMap[] A15 = new HashMap[10];
    public static final C05950Rp[][] A16;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public FileDescriptor A08;
    public String A09;
    public ByteOrder A0A;
    public Set A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public byte[] A0G;
    public final HashMap[] A0H;

    static {
        C05950Rp[] r8 = {new C05950Rp(254, "NewSubfileType", 4), new C05950Rp(255, "SubfileType", 4), new C05950Rp("ImageWidth", 256, 3, 4), new C05950Rp("ImageLength", 257, 3, 4), new C05950Rp(258, "BitsPerSample", 3), new C05950Rp(259, "Compression", 3), new C05950Rp(262, "PhotometricInterpretation", 3), new C05950Rp(270, "ImageDescription", 2), new C05950Rp(271, "Make", 2), new C05950Rp(272, "Model", 2), new C05950Rp("StripOffsets", 273, 3, 4), new C05950Rp(274, "Orientation", 3), new C05950Rp(277, "SamplesPerPixel", 3), new C05950Rp("RowsPerStrip", 278, 3, 4), new C05950Rp("StripByteCounts", 279, 3, 4), new C05950Rp(282, "XResolution", 5), new C05950Rp(283, "YResolution", 5), new C05950Rp(284, "PlanarConfiguration", 3), new C05950Rp(296, "ResolutionUnit", 3), new C05950Rp(301, "TransferFunction", 3), new C05950Rp(305, "Software", 2), new C05950Rp(306, "DateTime", 2), new C05950Rp(315, "Artist", 2), new C05950Rp(318, "WhitePoint", 5), new C05950Rp(319, "PrimaryChromaticities", 5), new C05950Rp(330, "SubIFDPointer", 4), new C05950Rp(513, "JPEGInterchangeFormat", 4), new C05950Rp(514, "JPEGInterchangeFormatLength", 4), new C05950Rp(529, "YCbCrCoefficients", 5), new C05950Rp(530, "YCbCrSubSampling", 3), new C05950Rp(531, "YCbCrPositioning", 3), new C05950Rp(532, "ReferenceBlackWhite", 5), new C05950Rp(33432, "Copyright", 2), new C05950Rp(34665, "ExifIFDPointer", 4), new C05950Rp(34853, "GPSInfoIFDPointer", 4), new C05950Rp(4, "SensorTopBorder", 4), new C05950Rp(5, "SensorLeftBorder", 4), new C05950Rp(6, "SensorBottomBorder", 4), new C05950Rp(7, "SensorRightBorder", 4), new C05950Rp(23, "ISO", 3), new C05950Rp(46, "JpgFromRaw", 7), new C05950Rp(700, "Xmp", 1)};
        A0y = r8;
        C05950Rp[] r6 = {new C05950Rp(33434, "ExposureTime", 5), new C05950Rp(33437, "FNumber", 5), new C05950Rp(34850, "ExposureProgram", 3), new C05950Rp(34852, "SpectralSensitivity", 2), new C05950Rp(34855, "PhotographicSensitivity", 3), new C05950Rp(34856, "OECF", 7), new C05950Rp(34864, "SensitivityType", 3), new C05950Rp(34865, "StandardOutputSensitivity", 4), new C05950Rp(34866, "RecommendedExposureIndex", 4), new C05950Rp(34867, "ISOSpeed", 4), new C05950Rp(34868, "ISOSpeedLatitudeyyy", 4), new C05950Rp(34869, "ISOSpeedLatitudezzz", 4), new C05950Rp(36864, "ExifVersion", 2), new C05950Rp(36867, "DateTimeOriginal", 2), new C05950Rp(36868, "DateTimeDigitized", 2), new C05950Rp(36880, "OffsetTime", 2), new C05950Rp(36881, "OffsetTimeOriginal", 2), new C05950Rp(36882, "OffsetTimeDigitized", 2), new C05950Rp(37121, "ComponentsConfiguration", 7), new C05950Rp(37122, "CompressedBitsPerPixel", 5), new C05950Rp(37377, "ShutterSpeedValue", 10), new C05950Rp(37378, "ApertureValue", 5), new C05950Rp(37379, "BrightnessValue", 10), new C05950Rp(37380, "ExposureBiasValue", 10), new C05950Rp(37381, "MaxApertureValue", 5), new C05950Rp(37382, "SubjectDistance", 5), new C05950Rp(37383, "MeteringMode", 3), new C05950Rp(37384, "LightSource", 3), new C05950Rp(37385, "Flash", 3), new C05950Rp(37386, "FocalLength", 5), new C05950Rp(37396, "SubjectArea", 3), new C05950Rp(37500, "MakerNote", 7), new C05950Rp(37510, "UserComment", 7), new C05950Rp(37520, "SubSecTime", 2), new C05950Rp(37521, "SubSecTimeOriginal", 2), new C05950Rp(37522, "SubSecTimeDigitized", 2), new C05950Rp(40960, "FlashpixVersion", 7), new C05950Rp(40961, "ColorSpace", 3), new C05950Rp("PixelXDimension", 40962, 3, 4), new C05950Rp("PixelYDimension", 40963, 3, 4), new C05950Rp(40964, "RelatedSoundFile", 2), new C05950Rp(40965, "InteroperabilityIFDPointer", 4), new C05950Rp(41483, "FlashEnergy", 5), new C05950Rp(41484, "SpatialFrequencyResponse", 7), new C05950Rp(41486, "FocalPlaneXResolution", 5), new C05950Rp(41487, "FocalPlaneYResolution", 5), new C05950Rp(41488, "FocalPlaneResolutionUnit", 3), new C05950Rp(41492, "SubjectLocation", 3), new C05950Rp(41493, "ExposureIndex", 5), new C05950Rp(41495, "SensingMethod", 3), new C05950Rp(41728, "FileSource", 7), new C05950Rp(41729, "SceneType", 7), new C05950Rp(41730, "CFAPattern", 7), new C05950Rp(41985, "CustomRendered", 3), new C05950Rp(41986, "ExposureMode", 3), new C05950Rp(41987, "WhiteBalance", 3), new C05950Rp(41988, "DigitalZoomRatio", 5), new C05950Rp(41989, "FocalLengthIn35mmFilm", 3), new C05950Rp(41990, "SceneCaptureType", 3), new C05950Rp(41991, "GainControl", 3), new C05950Rp(41992, "Contrast", 3), new C05950Rp(41993, "Saturation", 3), new C05950Rp(41994, "Sharpness", 3), new C05950Rp(41995, "DeviceSettingDescription", 7), new C05950Rp(41996, "SubjectDistanceRange", 3), new C05950Rp(42016, "ImageUniqueID", 2), new C05950Rp(42032, "CameraOwnerName", 2), new C05950Rp(42033, "BodySerialNumber", 2), new C05950Rp(42034, "LensSpecification", 5), new C05950Rp(42035, "LensMake", 2), new C05950Rp(42036, "LensModel", 2), new C05950Rp(42240, "Gamma", 5), new C05950Rp(50706, "DNGVersion", 1), new C05950Rp("DefaultCropSize", 50720, 3, 4)};
        A0u = r6;
        C05950Rp[] r4 = {new C05950Rp(0, "GPSVersionID", 1), new C05950Rp(1, "GPSLatitudeRef", 2), new C05950Rp("GPSLatitude", 2, 5, 10), new C05950Rp(3, "GPSLongitudeRef", 2), new C05950Rp("GPSLongitude", 4, 5, 10), new C05950Rp(5, "GPSAltitudeRef", 1), new C05950Rp(6, "GPSAltitude", 5), new C05950Rp(7, "GPSTimeStamp", 5), new C05950Rp(8, "GPSSatellites", 2), new C05950Rp(9, "GPSStatus", 2), new C05950Rp(10, "GPSMeasureMode", 2), new C05950Rp(11, "GPSDOP", 5), new C05950Rp(12, "GPSSpeedRef", 2), new C05950Rp(13, "GPSSpeed", 5), new C05950Rp(14, "GPSTrackRef", 2), new C05950Rp(15, "GPSTrack", 5), new C05950Rp(16, "GPSImgDirectionRef", 2), new C05950Rp(17, "GPSImgDirection", 5), new C05950Rp(18, "GPSMapDatum", 2), new C05950Rp(19, "GPSDestLatitudeRef", 2), new C05950Rp(20, "GPSDestLatitude", 5), new C05950Rp(21, "GPSDestLongitudeRef", 2), new C05950Rp(22, "GPSDestLongitude", 5), new C05950Rp(23, "GPSDestBearingRef", 2), new C05950Rp(24, "GPSDestBearing", 5), new C05950Rp(25, "GPSDestDistanceRef", 2), new C05950Rp(26, "GPSDestDistance", 5), new C05950Rp(27, "GPSProcessingMethod", 7), new C05950Rp(28, "GPSAreaInformation", 7), new C05950Rp(29, "GPSDateStamp", 2), new C05950Rp(30, "GPSDifferential", 3), new C05950Rp(31, "GPSHPositioningError", 5)};
        A0v = r4;
        C05950Rp[] r3 = {new C05950Rp(1, "InteroperabilityIndex", 2)};
        A0w = r3;
        C05950Rp[] r2 = {new C05950Rp(254, "NewSubfileType", 4), new C05950Rp(255, "SubfileType", 4), new C05950Rp("ThumbnailImageWidth", 256, 3, 4), new C05950Rp("ThumbnailImageLength", 257, 3, 4), new C05950Rp(258, "BitsPerSample", 3), new C05950Rp(259, "Compression", 3), new C05950Rp(262, "PhotometricInterpretation", 3), new C05950Rp(270, "ImageDescription", 2), new C05950Rp(271, "Make", 2), new C05950Rp(272, "Model", 2), new C05950Rp("StripOffsets", 273, 3, 4), new C05950Rp(274, "ThumbnailOrientation", 3), new C05950Rp(277, "SamplesPerPixel", 3), new C05950Rp("RowsPerStrip", 278, 3, 4), new C05950Rp("StripByteCounts", 279, 3, 4), new C05950Rp(282, "XResolution", 5), new C05950Rp(283, "YResolution", 5), new C05950Rp(284, "PlanarConfiguration", 3), new C05950Rp(296, "ResolutionUnit", 3), new C05950Rp(301, "TransferFunction", 3), new C05950Rp(305, "Software", 2), new C05950Rp(306, "DateTime", 2), new C05950Rp(315, "Artist", 2), new C05950Rp(318, "WhitePoint", 5), new C05950Rp(319, "PrimaryChromaticities", 5), new C05950Rp(330, "SubIFDPointer", 4), new C05950Rp(513, "JPEGInterchangeFormat", 4), new C05950Rp(514, "JPEGInterchangeFormatLength", 4), new C05950Rp(529, "YCbCrCoefficients", 5), new C05950Rp(530, "YCbCrSubSampling", 3), new C05950Rp(531, "YCbCrPositioning", 3), new C05950Rp(532, "ReferenceBlackWhite", 5), new C05950Rp(700, "Xmp", 1), new C05950Rp(33432, "Copyright", 2), new C05950Rp(34665, "ExifIFDPointer", 4), new C05950Rp(34853, "GPSInfoIFDPointer", 4), new C05950Rp(50706, "DNGVersion", 1), new C05950Rp("DefaultCropSize", 50720, 3, 4)};
        A0x = r2;
        C05950Rp[] r13 = {new C05950Rp(256, "ThumbnailImage", 7), new C05950Rp(8224, "CameraSettingsIFDPointer", 4), new C05950Rp(8256, "ImageProcessingIFDPointer", 4)};
        A11 = r13;
        C05950Rp[] r14 = {new C05950Rp(257, "PreviewImageStart", 4), new C05950Rp(258, "PreviewImageLength", 4)};
        A0z = r14;
        C05950Rp[] r12 = {new C05950Rp(4371, "AspectFrame", 3)};
        A10 = r12;
        C05950Rp[] r5 = {new C05950Rp(55, "ColorSpace", 3)};
        A12 = r5;
        A16 = new C05950Rp[][]{r8, r6, r4, r3, r2, r8, r13, r14, r12, r5};
        Charset forName = Charset.forName("US-ASCII");
        A0K = forName;
        A0Y = "Exif\u0000\u0000".getBytes(forName);
        A0Z = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(forName);
        Locale locale = Locale.US;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss", locale);
        A0I = simpleDateFormat;
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);
        A0J = simpleDateFormat2;
        simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("UTC"));
        int i = 0;
        while (true) {
            C05950Rp[][] r22 = A16;
            if (i < r22.length) {
                A14[i] = new HashMap();
                A15[i] = new HashMap();
                C05950Rp[] r52 = r22[i];
                for (C05950Rp r23 : r52) {
                    A14[i].put(Integer.valueOf(r23.A00), r23);
                    A15[i].put(r23.A03, r23);
                }
                i++;
            } else {
                HashMap hashMap = A0L;
                C05950Rp[] r32 = A0t;
                hashMap.put(Integer.valueOf(r32[0].A00), 5);
                hashMap.put(Integer.valueOf(r32[1].A00), 1);
                hashMap.put(Integer.valueOf(r32[2].A00), 2);
                hashMap.put(Integer.valueOf(r32[3].A00), 3);
                hashMap.put(Integer.valueOf(r32[4].A00), 7);
                hashMap.put(Integer.valueOf(r32[5].A00), 8);
                return;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x01b7, code lost:
        if (r9 == null) goto L_0x01bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:331:0x0717, code lost:
        if (X.AnonymousClass03i.A0T == false) goto L_0x071c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x01c6  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x03bd A[Catch: IOException | UnsupportedOperationException -> 0x0700, IOException | UnsupportedOperationException -> 0x0700, all -> 0x0720, TryCatch #2 {IOException | UnsupportedOperationException -> 0x0700, blocks: (B:17:0x004c, B:18:0x0058, B:19:0x006b, B:21:0x0070, B:23:0x0076, B:24:0x0081, B:26:0x0084, B:29:0x008d, B:64:0x0100, B:64:0x0100, B:65:0x0107, B:65:0x0107, B:75:0x0120, B:75:0x0120, B:83:0x013e, B:83:0x013e, B:84:0x0142, B:84:0x0142, B:92:0x0161, B:92:0x0161, B:93:0x0165, B:93:0x0165, B:97:0x016f, B:97:0x016f, B:99:0x0174, B:99:0x0174, B:102:0x017b, B:102:0x017b, B:104:0x017f, B:104:0x017f, B:106:0x0184, B:106:0x0184, B:108:0x018a, B:108:0x018a, B:110:0x018e, B:110:0x018e, B:112:0x0193, B:112:0x0193, B:114:0x019d, B:114:0x019d, B:120:0x01ab, B:120:0x01ab, B:121:0x01ae, B:121:0x01ae, B:124:0x01b2, B:124:0x01b2, B:125:0x01b5, B:125:0x01b5, B:128:0x01b9, B:128:0x01b9, B:129:0x01bc, B:129:0x01bc, B:132:0x01c1, B:132:0x01c1, B:140:0x01d2, B:140:0x01d2, B:142:0x01dd, B:142:0x01dd, B:144:0x01e5, B:144:0x01e5, B:183:0x0305, B:183:0x0305, B:195:0x0333, B:195:0x0333, B:196:0x0336, B:196:0x0336, B:197:0x0337, B:197:0x0337, B:198:0x033e, B:198:0x033e, B:201:0x0342, B:201:0x0342, B:203:0x0354, B:203:0x0354, B:206:0x037d, B:206:0x037d, B:209:0x0385, B:209:0x0385, B:210:0x0388, B:210:0x0388, B:213:0x03a0, B:213:0x03a0, B:214:0x03af, B:214:0x03af, B:216:0x03bd, B:216:0x03bd, B:218:0x03c7, B:218:0x03c7, B:220:0x03cb, B:220:0x03cb, B:222:0x03d3, B:222:0x03d3, B:224:0x03da, B:224:0x03da, B:226:0x03e0, B:226:0x03e0, B:227:0x03e5, B:227:0x03e5, B:230:0x0401, B:230:0x0401, B:232:0x0405, B:232:0x0405, B:233:0x0418, B:233:0x0418, B:235:0x042a, B:235:0x042a, B:236:0x0438, B:236:0x0438, B:239:0x044d, B:239:0x044d, B:240:0x0453, B:240:0x0453, B:241:0x0457, B:241:0x0457, B:242:0x0479, B:242:0x0479, B:243:0x0484, B:243:0x0484, B:245:0x048e, B:245:0x048e, B:248:0x0497, B:248:0x0497, B:250:0x049b, B:250:0x049b, B:251:0x04ae, B:251:0x04ae, B:252:0x04b8, B:252:0x04b8, B:254:0x04c5, B:254:0x04c5, B:256:0x04ca, B:256:0x04ca, B:258:0x04d2, B:258:0x04d2, B:259:0x04d9, B:259:0x04d9, B:260:0x04da, B:260:0x04da, B:262:0x04e2, B:262:0x04e2, B:264:0x04ea, B:264:0x04ea, B:266:0x04f2, B:266:0x04f2, B:268:0x0508, B:268:0x0508, B:269:0x0522, B:269:0x0522, B:270:0x0529, B:270:0x0529, B:271:0x054b, B:271:0x054b, B:272:0x054c, B:272:0x054c, B:273:0x0566, B:273:0x0566, B:274:0x0567, B:274:0x0567, B:275:0x056e, B:275:0x056e, B:276:0x056f, B:276:0x056f, B:277:0x0576, B:277:0x0576, B:280:0x057b, B:280:0x057b, B:282:0x0581, B:282:0x0581, B:283:0x0592, B:283:0x0592, B:285:0x05e1, B:285:0x05e1, B:288:0x05f6, B:288:0x05f6, B:290:0x0602, B:290:0x0602, B:291:0x0631, B:291:0x0631, B:293:0x0639, B:293:0x0639, B:296:0x0658, B:296:0x0658, B:298:0x065c, B:298:0x065c, B:299:0x066f, B:299:0x066f, B:300:0x0688, B:300:0x0688, B:301:0x068b, B:301:0x068b, B:303:0x0693, B:303:0x0693, B:305:0x06a1, B:305:0x06a1, B:307:0x06a9, B:307:0x06a9, B:308:0x06c0, B:308:0x06c0, B:310:0x06c5, B:310:0x06c5, B:311:0x06c7, B:311:0x06c7, B:315:0x06cd, B:315:0x06cd, B:316:0x06e7, B:316:0x06e7, B:317:0x06e8, B:317:0x06e8, B:318:0x06ef, B:318:0x06ef, B:319:0x06f0, B:319:0x06f0, B:320:0x06f7, B:320:0x06f7, B:321:0x06f8, B:321:0x06f8, B:322:0x06ff, B:322:0x06ff), top: B:344:0x004c, outer: #0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070 A[Catch: IOException | UnsupportedOperationException -> 0x0700, all -> 0x0720, TryCatch #2 {IOException | UnsupportedOperationException -> 0x0700, blocks: (B:17:0x004c, B:18:0x0058, B:19:0x006b, B:21:0x0070, B:23:0x0076, B:24:0x0081, B:26:0x0084, B:29:0x008d, B:64:0x0100, B:64:0x0100, B:65:0x0107, B:65:0x0107, B:75:0x0120, B:75:0x0120, B:83:0x013e, B:83:0x013e, B:84:0x0142, B:84:0x0142, B:92:0x0161, B:92:0x0161, B:93:0x0165, B:93:0x0165, B:97:0x016f, B:97:0x016f, B:99:0x0174, B:99:0x0174, B:102:0x017b, B:102:0x017b, B:104:0x017f, B:104:0x017f, B:106:0x0184, B:106:0x0184, B:108:0x018a, B:108:0x018a, B:110:0x018e, B:110:0x018e, B:112:0x0193, B:112:0x0193, B:114:0x019d, B:114:0x019d, B:120:0x01ab, B:120:0x01ab, B:121:0x01ae, B:121:0x01ae, B:124:0x01b2, B:124:0x01b2, B:125:0x01b5, B:125:0x01b5, B:128:0x01b9, B:128:0x01b9, B:129:0x01bc, B:129:0x01bc, B:132:0x01c1, B:132:0x01c1, B:140:0x01d2, B:140:0x01d2, B:142:0x01dd, B:142:0x01dd, B:144:0x01e5, B:144:0x01e5, B:183:0x0305, B:183:0x0305, B:195:0x0333, B:195:0x0333, B:196:0x0336, B:196:0x0336, B:197:0x0337, B:197:0x0337, B:198:0x033e, B:198:0x033e, B:201:0x0342, B:201:0x0342, B:203:0x0354, B:203:0x0354, B:206:0x037d, B:206:0x037d, B:209:0x0385, B:209:0x0385, B:210:0x0388, B:210:0x0388, B:213:0x03a0, B:213:0x03a0, B:214:0x03af, B:214:0x03af, B:216:0x03bd, B:216:0x03bd, B:218:0x03c7, B:218:0x03c7, B:220:0x03cb, B:220:0x03cb, B:222:0x03d3, B:222:0x03d3, B:224:0x03da, B:224:0x03da, B:226:0x03e0, B:226:0x03e0, B:227:0x03e5, B:227:0x03e5, B:230:0x0401, B:230:0x0401, B:232:0x0405, B:232:0x0405, B:233:0x0418, B:233:0x0418, B:235:0x042a, B:235:0x042a, B:236:0x0438, B:236:0x0438, B:239:0x044d, B:239:0x044d, B:240:0x0453, B:240:0x0453, B:241:0x0457, B:241:0x0457, B:242:0x0479, B:242:0x0479, B:243:0x0484, B:243:0x0484, B:245:0x048e, B:245:0x048e, B:248:0x0497, B:248:0x0497, B:250:0x049b, B:250:0x049b, B:251:0x04ae, B:251:0x04ae, B:252:0x04b8, B:252:0x04b8, B:254:0x04c5, B:254:0x04c5, B:256:0x04ca, B:256:0x04ca, B:258:0x04d2, B:258:0x04d2, B:259:0x04d9, B:259:0x04d9, B:260:0x04da, B:260:0x04da, B:262:0x04e2, B:262:0x04e2, B:264:0x04ea, B:264:0x04ea, B:266:0x04f2, B:266:0x04f2, B:268:0x0508, B:268:0x0508, B:269:0x0522, B:269:0x0522, B:270:0x0529, B:270:0x0529, B:271:0x054b, B:271:0x054b, B:272:0x054c, B:272:0x054c, B:273:0x0566, B:273:0x0566, B:274:0x0567, B:274:0x0567, B:275:0x056e, B:275:0x056e, B:276:0x056f, B:276:0x056f, B:277:0x0576, B:277:0x0576, B:280:0x057b, B:280:0x057b, B:282:0x0581, B:282:0x0581, B:283:0x0592, B:283:0x0592, B:285:0x05e1, B:285:0x05e1, B:288:0x05f6, B:288:0x05f6, B:290:0x0602, B:290:0x0602, B:291:0x0631, B:291:0x0631, B:293:0x0639, B:293:0x0639, B:296:0x0658, B:296:0x0658, B:298:0x065c, B:298:0x065c, B:299:0x066f, B:299:0x066f, B:300:0x0688, B:300:0x0688, B:301:0x068b, B:301:0x068b, B:303:0x0693, B:303:0x0693, B:305:0x06a1, B:305:0x06a1, B:307:0x06a9, B:307:0x06a9, B:308:0x06c0, B:308:0x06c0, B:310:0x06c5, B:310:0x06c5, B:311:0x06c7, B:311:0x06c7, B:315:0x06cd, B:315:0x06cd, B:316:0x06e7, B:316:0x06e7, B:317:0x06e8, B:317:0x06e8, B:318:0x06ef, B:318:0x06ef, B:319:0x06f0, B:319:0x06f0, B:320:0x06f7, B:320:0x06f7, B:321:0x06f8, B:321:0x06f8, B:322:0x06ff, B:322:0x06ff), top: B:344:0x004c, outer: #0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x048e A[Catch: IOException | UnsupportedOperationException -> 0x0700, IOException | UnsupportedOperationException -> 0x0700, all -> 0x0720, TryCatch #2 {IOException | UnsupportedOperationException -> 0x0700, blocks: (B:17:0x004c, B:18:0x0058, B:19:0x006b, B:21:0x0070, B:23:0x0076, B:24:0x0081, B:26:0x0084, B:29:0x008d, B:64:0x0100, B:64:0x0100, B:65:0x0107, B:65:0x0107, B:75:0x0120, B:75:0x0120, B:83:0x013e, B:83:0x013e, B:84:0x0142, B:84:0x0142, B:92:0x0161, B:92:0x0161, B:93:0x0165, B:93:0x0165, B:97:0x016f, B:97:0x016f, B:99:0x0174, B:99:0x0174, B:102:0x017b, B:102:0x017b, B:104:0x017f, B:104:0x017f, B:106:0x0184, B:106:0x0184, B:108:0x018a, B:108:0x018a, B:110:0x018e, B:110:0x018e, B:112:0x0193, B:112:0x0193, B:114:0x019d, B:114:0x019d, B:120:0x01ab, B:120:0x01ab, B:121:0x01ae, B:121:0x01ae, B:124:0x01b2, B:124:0x01b2, B:125:0x01b5, B:125:0x01b5, B:128:0x01b9, B:128:0x01b9, B:129:0x01bc, B:129:0x01bc, B:132:0x01c1, B:132:0x01c1, B:140:0x01d2, B:140:0x01d2, B:142:0x01dd, B:142:0x01dd, B:144:0x01e5, B:144:0x01e5, B:183:0x0305, B:183:0x0305, B:195:0x0333, B:195:0x0333, B:196:0x0336, B:196:0x0336, B:197:0x0337, B:197:0x0337, B:198:0x033e, B:198:0x033e, B:201:0x0342, B:201:0x0342, B:203:0x0354, B:203:0x0354, B:206:0x037d, B:206:0x037d, B:209:0x0385, B:209:0x0385, B:210:0x0388, B:210:0x0388, B:213:0x03a0, B:213:0x03a0, B:214:0x03af, B:214:0x03af, B:216:0x03bd, B:216:0x03bd, B:218:0x03c7, B:218:0x03c7, B:220:0x03cb, B:220:0x03cb, B:222:0x03d3, B:222:0x03d3, B:224:0x03da, B:224:0x03da, B:226:0x03e0, B:226:0x03e0, B:227:0x03e5, B:227:0x03e5, B:230:0x0401, B:230:0x0401, B:232:0x0405, B:232:0x0405, B:233:0x0418, B:233:0x0418, B:235:0x042a, B:235:0x042a, B:236:0x0438, B:236:0x0438, B:239:0x044d, B:239:0x044d, B:240:0x0453, B:240:0x0453, B:241:0x0457, B:241:0x0457, B:242:0x0479, B:242:0x0479, B:243:0x0484, B:243:0x0484, B:245:0x048e, B:245:0x048e, B:248:0x0497, B:248:0x0497, B:250:0x049b, B:250:0x049b, B:251:0x04ae, B:251:0x04ae, B:252:0x04b8, B:252:0x04b8, B:254:0x04c5, B:254:0x04c5, B:256:0x04ca, B:256:0x04ca, B:258:0x04d2, B:258:0x04d2, B:259:0x04d9, B:259:0x04d9, B:260:0x04da, B:260:0x04da, B:262:0x04e2, B:262:0x04e2, B:264:0x04ea, B:264:0x04ea, B:266:0x04f2, B:266:0x04f2, B:268:0x0508, B:268:0x0508, B:269:0x0522, B:269:0x0522, B:270:0x0529, B:270:0x0529, B:271:0x054b, B:271:0x054b, B:272:0x054c, B:272:0x054c, B:273:0x0566, B:273:0x0566, B:274:0x0567, B:274:0x0567, B:275:0x056e, B:275:0x056e, B:276:0x056f, B:276:0x056f, B:277:0x0576, B:277:0x0576, B:280:0x057b, B:280:0x057b, B:282:0x0581, B:282:0x0581, B:283:0x0592, B:283:0x0592, B:285:0x05e1, B:285:0x05e1, B:288:0x05f6, B:288:0x05f6, B:290:0x0602, B:290:0x0602, B:291:0x0631, B:291:0x0631, B:293:0x0639, B:293:0x0639, B:296:0x0658, B:296:0x0658, B:298:0x065c, B:298:0x065c, B:299:0x066f, B:299:0x066f, B:300:0x0688, B:300:0x0688, B:301:0x068b, B:301:0x068b, B:303:0x0693, B:303:0x0693, B:305:0x06a1, B:305:0x06a1, B:307:0x06a9, B:307:0x06a9, B:308:0x06c0, B:308:0x06c0, B:310:0x06c5, B:310:0x06c5, B:311:0x06c7, B:311:0x06c7, B:315:0x06cd, B:315:0x06cd, B:316:0x06e7, B:316:0x06e7, B:317:0x06e8, B:317:0x06e8, B:318:0x06ef, B:318:0x06ef, B:319:0x06f0, B:319:0x06f0, B:320:0x06f7, B:320:0x06f7, B:321:0x06f8, B:321:0x06f8, B:322:0x06ff, B:322:0x06ff), top: B:344:0x004c, outer: #0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x0493  */
    /* JADX WARNING: Removed duplicated region for block: B:344:0x004c A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:369:0x01c0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:378:0x01a5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x016b  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0174 A[Catch: IOException | UnsupportedOperationException -> 0x0700, IOException | UnsupportedOperationException -> 0x0700, all -> 0x0720, TryCatch #2 {IOException | UnsupportedOperationException -> 0x0700, blocks: (B:17:0x004c, B:18:0x0058, B:19:0x006b, B:21:0x0070, B:23:0x0076, B:24:0x0081, B:26:0x0084, B:29:0x008d, B:64:0x0100, B:64:0x0100, B:65:0x0107, B:65:0x0107, B:75:0x0120, B:75:0x0120, B:83:0x013e, B:83:0x013e, B:84:0x0142, B:84:0x0142, B:92:0x0161, B:92:0x0161, B:93:0x0165, B:93:0x0165, B:97:0x016f, B:97:0x016f, B:99:0x0174, B:99:0x0174, B:102:0x017b, B:102:0x017b, B:104:0x017f, B:104:0x017f, B:106:0x0184, B:106:0x0184, B:108:0x018a, B:108:0x018a, B:110:0x018e, B:110:0x018e, B:112:0x0193, B:112:0x0193, B:114:0x019d, B:114:0x019d, B:120:0x01ab, B:120:0x01ab, B:121:0x01ae, B:121:0x01ae, B:124:0x01b2, B:124:0x01b2, B:125:0x01b5, B:125:0x01b5, B:128:0x01b9, B:128:0x01b9, B:129:0x01bc, B:129:0x01bc, B:132:0x01c1, B:132:0x01c1, B:140:0x01d2, B:140:0x01d2, B:142:0x01dd, B:142:0x01dd, B:144:0x01e5, B:144:0x01e5, B:183:0x0305, B:183:0x0305, B:195:0x0333, B:195:0x0333, B:196:0x0336, B:196:0x0336, B:197:0x0337, B:197:0x0337, B:198:0x033e, B:198:0x033e, B:201:0x0342, B:201:0x0342, B:203:0x0354, B:203:0x0354, B:206:0x037d, B:206:0x037d, B:209:0x0385, B:209:0x0385, B:210:0x0388, B:210:0x0388, B:213:0x03a0, B:213:0x03a0, B:214:0x03af, B:214:0x03af, B:216:0x03bd, B:216:0x03bd, B:218:0x03c7, B:218:0x03c7, B:220:0x03cb, B:220:0x03cb, B:222:0x03d3, B:222:0x03d3, B:224:0x03da, B:224:0x03da, B:226:0x03e0, B:226:0x03e0, B:227:0x03e5, B:227:0x03e5, B:230:0x0401, B:230:0x0401, B:232:0x0405, B:232:0x0405, B:233:0x0418, B:233:0x0418, B:235:0x042a, B:235:0x042a, B:236:0x0438, B:236:0x0438, B:239:0x044d, B:239:0x044d, B:240:0x0453, B:240:0x0453, B:241:0x0457, B:241:0x0457, B:242:0x0479, B:242:0x0479, B:243:0x0484, B:243:0x0484, B:245:0x048e, B:245:0x048e, B:248:0x0497, B:248:0x0497, B:250:0x049b, B:250:0x049b, B:251:0x04ae, B:251:0x04ae, B:252:0x04b8, B:252:0x04b8, B:254:0x04c5, B:254:0x04c5, B:256:0x04ca, B:256:0x04ca, B:258:0x04d2, B:258:0x04d2, B:259:0x04d9, B:259:0x04d9, B:260:0x04da, B:260:0x04da, B:262:0x04e2, B:262:0x04e2, B:264:0x04ea, B:264:0x04ea, B:266:0x04f2, B:266:0x04f2, B:268:0x0508, B:268:0x0508, B:269:0x0522, B:269:0x0522, B:270:0x0529, B:270:0x0529, B:271:0x054b, B:271:0x054b, B:272:0x054c, B:272:0x054c, B:273:0x0566, B:273:0x0566, B:274:0x0567, B:274:0x0567, B:275:0x056e, B:275:0x056e, B:276:0x056f, B:276:0x056f, B:277:0x0576, B:277:0x0576, B:280:0x057b, B:280:0x057b, B:282:0x0581, B:282:0x0581, B:283:0x0592, B:283:0x0592, B:285:0x05e1, B:285:0x05e1, B:288:0x05f6, B:288:0x05f6, B:290:0x0602, B:290:0x0602, B:291:0x0631, B:291:0x0631, B:293:0x0639, B:293:0x0639, B:296:0x0658, B:296:0x0658, B:298:0x065c, B:298:0x065c, B:299:0x066f, B:299:0x066f, B:300:0x0688, B:300:0x0688, B:301:0x068b, B:301:0x068b, B:303:0x0693, B:303:0x0693, B:305:0x06a1, B:305:0x06a1, B:307:0x06a9, B:307:0x06a9, B:308:0x06c0, B:308:0x06c0, B:310:0x06c5, B:310:0x06c5, B:311:0x06c7, B:311:0x06c7, B:315:0x06cd, B:315:0x06cd, B:316:0x06e7, B:316:0x06e7, B:317:0x06e8, B:317:0x06e8, B:318:0x06ef, B:318:0x06ef, B:319:0x06f0, B:319:0x06f0, B:320:0x06f7, B:320:0x06f7, B:321:0x06f8, B:321:0x06f8, B:322:0x06ff, B:322:0x06ff), top: B:344:0x004c, outer: #0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass03i(java.lang.String r22) {
        /*
        // Method dump skipped, instructions count: 1852
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03i.<init>(java.lang.String):void");
    }

    public static Pair A00(String str) {
        int intValue;
        int i;
        Integer valueOf;
        if (str.contains(",")) {
            String[] split = str.split(",", -1);
            Pair A00 = A00(split[0]);
            if (((Number) A00.first).intValue() != 2) {
                for (int i2 = 1; i2 < split.length; i2++) {
                    Pair A002 = A00(split[i2]);
                    Object obj = A002.first;
                    if (obj.equals(A00.first) || A002.second.equals(A00.first)) {
                        intValue = ((Number) A00.first).intValue();
                    } else {
                        intValue = -1;
                    }
                    if (((Number) A00.second).intValue() == -1 || (!obj.equals(A00.second) && !A002.second.equals(A00.second))) {
                        i = -1;
                    } else {
                        i = ((Number) A00.second).intValue();
                    }
                    if (intValue == -1 && i == -1) {
                        return new Pair(2, -1);
                    }
                    if (intValue == -1) {
                        valueOf = Integer.valueOf(i);
                    } else if (i == -1) {
                        valueOf = Integer.valueOf(intValue);
                    }
                    A00 = new Pair(valueOf, -1);
                }
            }
            return A00;
        } else if (str.contains("/")) {
            String[] split2 = str.split("/", -1);
            if (split2.length == 2) {
                long parseDouble = (long) Double.parseDouble(split2[0]);
                long parseDouble2 = (long) Double.parseDouble(split2[1]);
                if (parseDouble < 0 || parseDouble2 < 0) {
                    return new Pair(10, -1);
                }
                if (parseDouble > 2147483647L || parseDouble2 > 2147483647L) {
                    return new Pair(5, -1);
                }
                return new Pair(10, 5);
            }
            return new Pair(2, -1);
        } else {
            try {
                long longValue = Long.valueOf(Long.parseLong(str)).longValue();
                if (longValue >= 0 && longValue <= 65535) {
                    return new Pair(3, 4);
                }
                if (longValue < 0) {
                    return new Pair(9, -1);
                }
                return new Pair(4, -1);
            } catch (NumberFormatException unused) {
                Double.parseDouble(str);
                return new Pair(12, -1);
            }
        }
    }

    public static String A01(byte[] bArr) {
        int length = bArr.length;
        StringBuilder sb = new StringBuilder(length << 1);
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02x", Byte.valueOf(bArr[i])));
        }
        return sb.toString();
    }

    public static final ByteOrder A02(C03660Iq r2) {
        short readShort = r2.readShort();
        if (readShort == 18761) {
            if (A0T) {
                Log.d("ExifInterface", "readExifSegment: Byte Align II");
            }
            return ByteOrder.LITTLE_ENDIAN;
        } else if (readShort == 19789) {
            if (A0T) {
                Log.d("ExifInterface", "readExifSegment: Byte Align MM");
            }
            return ByteOrder.BIG_ENDIAN;
        } else {
            StringBuilder sb = new StringBuilder("Invalid byte order: ");
            sb.append(Integer.toHexString(readShort));
            throw new IOException(sb.toString());
        }
    }

    public static void A03(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
    }

    public static void A04(FileDescriptor fileDescriptor) {
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                AnonymousClass0T8.A01(fileDescriptor);
            } catch (Exception unused) {
                Log.e("ExifInterfaceUtils", "Error closing fd.");
            }
        } else {
            Log.e("ExifInterfaceUtils", "closeFileDescriptor is called in API < 21, which must be wrong.");
        }
    }

    public static void A05(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static void A06(InputStream inputStream, OutputStream outputStream, int i) {
        byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
        while (i > 0) {
            int min = Math.min(i, (int) DefaultCrypto.BUFFER_SIZE);
            int read = inputStream.read(bArr, 0, min);
            if (read == min) {
                i -= read;
                outputStream.write(bArr, 0, read);
            } else {
                throw new IOException("Failed to copy the given amount of bytes from the inputstream to the output stream.");
            }
        }
    }

    public int A07(int i) {
        AnonymousClass0UH A08 = A08("Orientation");
        if (A08 == null) {
            return i;
        }
        try {
            return A08.A04(this.A0A);
        } catch (NumberFormatException unused) {
            return i;
        }
    }

    public final AnonymousClass0UH A08(String str) {
        if (str != null) {
            if ("ISOSpeedRatings".equals(str)) {
                if (A0T) {
                    Log.d("ExifInterface", "getExifAttribute: Replacing TAG_ISO_SPEED_RATINGS with TAG_PHOTOGRAPHIC_SENSITIVITY.");
                }
                str = "PhotographicSensitivity";
            }
            for (int i = 0; i < A16.length; i++) {
                AnonymousClass0UH r0 = (AnonymousClass0UH) this.A0H[i].get(str);
                if (r0 != null) {
                    return r0;
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    public String A09(String str) {
        double d;
        String obj;
        if (str != null) {
            AnonymousClass0UH A08 = A08(str);
            if (A08 != null) {
                if (!A0M.contains(str)) {
                    return A08.A06(this.A0A);
                }
                if (str.equals("GPSTimeStamp")) {
                    int i = A08.A00;
                    if (i == 5 || i == 10) {
                        AnonymousClass0OJ[] r6 = (AnonymousClass0OJ[]) A08.A05(this.A0A);
                        if (r6 == null || r6.length != 3) {
                            StringBuilder sb = new StringBuilder("Invalid GPS Timestamp array. array=");
                            sb.append(Arrays.toString(r6));
                            obj = sb.toString();
                        } else {
                            AnonymousClass0OJ r2 = r6[0];
                            AnonymousClass0OJ r22 = r6[1];
                            AnonymousClass0OJ r23 = r6[2];
                            return String.format("%02d:%02d:%02d", Integer.valueOf((int) (((float) r2.A01) / ((float) r2.A00))), Integer.valueOf((int) (((float) r22.A01) / ((float) r22.A00))), Integer.valueOf((int) (((float) r23.A01) / ((float) r23.A00))));
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder("GPS Timestamp format is not rational. format=");
                        sb2.append(i);
                        obj = sb2.toString();
                    }
                    Log.w("ExifInterface", obj);
                    return null;
                }
                try {
                    Object A05 = A08.A05(this.A0A);
                    if (A05 != null) {
                        if (A05 instanceof String) {
                            d = Double.parseDouble((String) A05);
                        } else if (A05 instanceof long[]) {
                            long[] jArr = (long[]) A05;
                            if (jArr.length == 1) {
                                d = (double) jArr[0];
                            } else {
                                throw new NumberFormatException("There are more than one component");
                            }
                        } else if (A05 instanceof int[]) {
                            int[] iArr = (int[]) A05;
                            if (iArr.length == 1) {
                                d = (double) iArr[0];
                            } else {
                                throw new NumberFormatException("There are more than one component");
                            }
                        } else if (A05 instanceof double[]) {
                            double[] dArr = (double[]) A05;
                            if (dArr.length == 1) {
                                d = dArr[0];
                            } else {
                                throw new NumberFormatException("There are more than one component");
                            }
                        } else if (A05 instanceof AnonymousClass0OJ[]) {
                            AnonymousClass0OJ[] r4 = (AnonymousClass0OJ[]) A05;
                            if (r4.length == 1) {
                                AnonymousClass0OJ r24 = r4[0];
                                d = ((double) r24.A01) / ((double) r24.A00);
                            } else {
                                throw new NumberFormatException("There are more than one component");
                            }
                        } else {
                            throw new NumberFormatException("Couldn't find a double value");
                        }
                        return Double.toString(d);
                    }
                    throw new NumberFormatException("NULL can't be converted to a double value");
                } catch (NumberFormatException unused) {
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }

    /* JADX WARNING: Removed duplicated region for block: B:186:0x0439 A[Catch: Exception -> 0x0480, all -> 0x047c, TryCatch #23 {Exception -> 0x0480, all -> 0x047c, blocks: (B:140:0x02d9, B:142:0x02e4, B:143:0x02f7, B:144:0x02fa, B:145:0x0312, B:147:0x031a, B:149:0x0325, B:151:0x032e, B:152:0x0330, B:155:0x0340, B:157:0x034b, B:158:0x0352, B:160:0x035f, B:162:0x036d, B:163:0x036f, B:164:0x0373, B:165:0x037c, B:167:0x0384, B:169:0x038c, B:171:0x0395, B:172:0x0397, B:174:0x03a4, B:176:0x03b1, B:178:0x03b9, B:179:0x03c9, B:181:0x03d1, B:183:0x03d7, B:184:0x03eb, B:186:0x0439, B:187:0x0443, B:188:0x0446, B:189:0x044b, B:191:0x0453, B:195:0x0464, B:196:0x046b, B:197:0x046c, B:198:0x0473, B:199:0x0474, B:200:0x047b), top: B:300:0x02d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x044b A[Catch: Exception -> 0x0480, all -> 0x047c, TryCatch #23 {Exception -> 0x0480, all -> 0x047c, blocks: (B:140:0x02d9, B:142:0x02e4, B:143:0x02f7, B:144:0x02fa, B:145:0x0312, B:147:0x031a, B:149:0x0325, B:151:0x032e, B:152:0x0330, B:155:0x0340, B:157:0x034b, B:158:0x0352, B:160:0x035f, B:162:0x036d, B:163:0x036f, B:164:0x0373, B:165:0x037c, B:167:0x0384, B:169:0x038c, B:171:0x0395, B:172:0x0397, B:174:0x03a4, B:176:0x03b1, B:178:0x03b9, B:179:0x03c9, B:181:0x03d1, B:183:0x03d7, B:184:0x03eb, B:186:0x0439, B:187:0x0443, B:188:0x0446, B:189:0x044b, B:191:0x0453, B:195:0x0464, B:196:0x046b, B:197:0x046c, B:198:0x0473, B:199:0x0474, B:200:0x047b), top: B:300:0x02d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x04d9 A[Catch: Exception -> 0x050e, all -> 0x050a, TRY_ENTER, TryCatch #21 {Exception -> 0x050e, all -> 0x050a, blocks: (B:232:0x04d9, B:234:0x04df, B:235:0x04ee, B:237:0x04f7), top: B:303:0x04d7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x04ee A[Catch: Exception -> 0x050e, all -> 0x050a, TryCatch #21 {Exception -> 0x050e, all -> 0x050a, blocks: (B:232:0x04d9, B:234:0x04df, B:235:0x04ee, B:237:0x04f7), top: B:303:0x04d7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x054a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A() {
        /*
        // Method dump skipped, instructions count: 1397
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03i.A0A():void");
    }

    public final void A0B() {
        String A09 = A09("DateTimeOriginal");
        if (A09 != null && A09("DateTime") == null) {
            this.A0H[0].put("DateTime", AnonymousClass0UH.A00(A09));
        }
        if (A09("ImageWidth") == null) {
            this.A0H[0].put("ImageWidth", AnonymousClass0UH.A02(this.A0A, new long[]{0}));
        }
        if (A09("ImageLength") == null) {
            this.A0H[0].put("ImageLength", AnonymousClass0UH.A02(this.A0A, new long[]{0}));
        }
        if (A09("Orientation") == null) {
            this.A0H[0].put("Orientation", AnonymousClass0UH.A02(this.A0A, new long[]{0}));
        }
        if (A09("LightSource") == null) {
            this.A0H[1].put("LightSource", AnonymousClass0UH.A02(this.A0A, new long[]{0}));
        }
    }

    public final void A0C() {
        int i = 0;
        while (true) {
            HashMap[] hashMapArr = this.A0H;
            if (i < hashMapArr.length) {
                StringBuilder sb = new StringBuilder("The size of tag group[");
                sb.append(i);
                sb.append("]: ");
                sb.append(hashMapArr[i].size());
                Log.d("ExifInterface", sb.toString());
                for (Map.Entry entry : hashMapArr[i].entrySet()) {
                    AnonymousClass0UH r2 = (AnonymousClass0UH) entry.getValue();
                    StringBuilder sb2 = new StringBuilder("tagName: ");
                    sb2.append((String) entry.getKey());
                    sb2.append(", tagType: ");
                    sb2.append(r2.toString());
                    sb2.append(", tagValue: '");
                    sb2.append(r2.A06(this.A0A));
                    sb2.append("'");
                    Log.d("ExifInterface", sb2.toString());
                }
                i++;
            } else {
                return;
            }
        }
    }

    public final void A0D() {
        A0E(0, 5);
        A0E(0, 4);
        A0E(5, 4);
        HashMap[] hashMapArr = this.A0H;
        Object obj = hashMapArr[1].get("PixelXDimension");
        Object obj2 = hashMapArr[1].get("PixelYDimension");
        if (!(obj == null || obj2 == null)) {
            hashMapArr[0].put("ImageWidth", obj);
            hashMapArr[0].put("ImageLength", obj2);
        }
        if (hashMapArr[4].isEmpty() && A0Q(hashMapArr[5])) {
            hashMapArr[4] = hashMapArr[5];
            hashMapArr[5] = new HashMap();
        }
        if (!A0Q(hashMapArr[4])) {
            Log.d("ExifInterface", "No image meets the size requirements of a thumbnail image.");
        }
        A0F(0, "ThumbnailOrientation", "Orientation");
        A0F(0, "ThumbnailImageLength", "ImageLength");
        A0F(0, "ThumbnailImageWidth", "ImageWidth");
        A0F(5, "ThumbnailOrientation", "Orientation");
        A0F(5, "ThumbnailImageLength", "ImageLength");
        A0F(5, "ThumbnailImageWidth", "ImageWidth");
        A0F(4, "Orientation", "ThumbnailOrientation");
        A0F(4, "ImageLength", "ThumbnailImageLength");
        A0F(4, "ImageWidth", "ThumbnailImageWidth");
    }

    public final void A0E(int i, int i2) {
        String str;
        HashMap[] hashMapArr = this.A0H;
        HashMap hashMap = hashMapArr[i];
        if (!hashMap.isEmpty() && !hashMapArr[i2].isEmpty()) {
            AnonymousClass0UH r6 = (AnonymousClass0UH) hashMap.get("ImageLength");
            AnonymousClass0UH r2 = (AnonymousClass0UH) hashMapArr[i].get("ImageWidth");
            AnonymousClass0UH r1 = (AnonymousClass0UH) hashMapArr[i2].get("ImageLength");
            AnonymousClass0UH r4 = (AnonymousClass0UH) hashMapArr[i2].get("ImageWidth");
            if (r6 == null || r2 == null) {
                if (A0T) {
                    str = "First image does not contain valid size information";
                } else {
                    return;
                }
            } else if (r1 != null && r4 != null) {
                int A04 = r6.A04(this.A0A);
                int A042 = r2.A04(this.A0A);
                int A043 = r1.A04(this.A0A);
                int A044 = r4.A04(this.A0A);
                if (A04 < A043 && A042 < A044) {
                    HashMap hashMap2 = hashMapArr[i];
                    hashMapArr[i] = hashMapArr[i2];
                    hashMapArr[i2] = hashMap2;
                    return;
                }
                return;
            } else if (A0T) {
                str = "Second image does not contain valid size information";
            } else {
                return;
            }
        } else if (A0T) {
            str = "Cannot perform swap since only one image data exists";
        } else {
            return;
        }
        Log.d("ExifInterface", str);
    }

    public final void A0F(int i, String str, String str2) {
        HashMap[] hashMapArr = this.A0H;
        HashMap hashMap = hashMapArr[i];
        if (!hashMap.isEmpty() && hashMap.get(str) != null) {
            HashMap hashMap2 = hashMapArr[i];
            hashMap2.put(str2, hashMap2.get(str));
            hashMapArr[i].remove(str);
        }
    }

    public final void A0G(C03660Iq r4) {
        ByteOrder A02 = A02(r4);
        this.A0A = A02;
        r4.A01 = A02;
        int readUnsignedShort = r4.readUnsignedShort();
        int i = this.A00;
        if (i == 7 || i == 10 || readUnsignedShort == 42) {
            int readInt = r4.readInt();
            if (readInt >= 8) {
                int i2 = readInt - 8;
                if (i2 > 0) {
                    r4.A00(i2);
                    return;
                }
                return;
            }
            StringBuilder sb = new StringBuilder("Invalid first Ifd offset: ");
            sb.append(readInt);
            throw new IOException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder("Invalid start code: ");
        sb2.append(Integer.toHexString(readUnsignedShort));
        throw new IOException(sb2.toString());
    }

    public final void A0H(C03660Iq r22) {
        String str;
        String str2;
        long[] jArr;
        long[] jArr2;
        String str3;
        int length;
        int length2;
        StringBuilder sb;
        AnonymousClass0UH r1;
        int A04;
        HashMap hashMap = this.A0H[4];
        AnonymousClass0UH r12 = (AnonymousClass0UH) hashMap.get("Compression");
        if (r12 != null) {
            int A042 = r12.A04(this.A0A);
            this.A05 = A042;
            if (A042 != 1) {
                if (A042 != 6) {
                    if (A042 != 7) {
                        return;
                    }
                }
            }
            AnonymousClass0UH r13 = (AnonymousClass0UH) hashMap.get("BitsPerSample");
            if (r13 != null) {
                int[] iArr = (int[]) r13.A05(this.A0A);
                int[] iArr2 = A0r;
                if (Arrays.equals(iArr2, iArr) || (this.A00 == 3 && (r1 = (AnonymousClass0UH) hashMap.get("PhotometricInterpretation")) != null && ((A04 = r1.A04(this.A0A)) != 1 ? !(A04 != 6 || !Arrays.equals(iArr, iArr2)) : Arrays.equals(iArr, A0q)))) {
                    AnonymousClass0UH r14 = (AnonymousClass0UH) hashMap.get("StripOffsets");
                    AnonymousClass0UH r5 = (AnonymousClass0UH) hashMap.get("StripByteCounts");
                    if (!(r14 == null || r5 == null)) {
                        Object A05 = r14.A05(this.A0A);
                        if (A05 instanceof int[]) {
                            int[] iArr3 = (int[]) A05;
                            int length3 = iArr3.length;
                            jArr = new long[length3];
                            for (int i = 0; i < length3; i++) {
                                jArr[i] = (long) iArr3[i];
                            }
                        } else if (A05 instanceof long[]) {
                            jArr = (long[]) A05;
                        } else {
                            jArr = null;
                        }
                        Object A052 = r5.A05(this.A0A);
                        if (A052 instanceof int[]) {
                            int[] iArr4 = (int[]) A052;
                            int length4 = iArr4.length;
                            jArr2 = new long[length4];
                            for (int i2 = 0; i2 < length4; i2++) {
                                jArr2[i2] = (long) iArr4[i2];
                            }
                        } else if (A052 instanceof long[]) {
                            jArr2 = (long[]) A052;
                        } else {
                            jArr2 = null;
                        }
                        str = "ExifInterface";
                        if (jArr == null || (length = jArr.length) == 0) {
                            str3 = "stripOffsets should not be null or have zero length.";
                        } else if (jArr2 == null || (length2 = jArr2.length) == 0) {
                            str3 = "stripByteCounts should not be null or have zero length.";
                        } else if (length != length2) {
                            str3 = "stripOffsets and stripByteCounts should have same length.";
                        } else {
                            long j = 0;
                            int i3 = 0;
                            do {
                                j += jArr2[i3];
                                i3++;
                            } while (i3 < length2);
                            int i4 = (int) j;
                            byte[] bArr = new byte[i4];
                            this.A0C = true;
                            this.A0E = true;
                            this.A0D = true;
                            int i5 = 0;
                            int i6 = 0;
                            int i7 = 0;
                            do {
                                int i8 = (int) jArr[i5];
                                int i9 = (int) jArr2[i5];
                                if (i5 < length - 1 && ((long) (i8 + i9)) != jArr[i5 + 1]) {
                                    this.A0C = false;
                                }
                                int i10 = i8 - i6;
                                if (i10 < 0) {
                                    str2 = "Invalid strip offset value";
                                } else {
                                    long j2 = (long) i10;
                                    if (r22.skip(j2) != j2) {
                                        sb = new StringBuilder("Failed to skip ");
                                        sb.append(i10);
                                    } else {
                                        int i11 = i6 + i10;
                                        byte[] bArr2 = new byte[i9];
                                        if (r22.read(bArr2) != i9) {
                                            sb = new StringBuilder("Failed to read ");
                                            sb.append(i9);
                                        } else {
                                            i6 = i11 + i9;
                                            System.arraycopy(bArr2, 0, bArr, i7, i9);
                                            i7 += i9;
                                            i5++;
                                        }
                                    }
                                    sb.append(" bytes.");
                                    str2 = sb.toString();
                                }
                                Log.d(str, str2);
                            } while (i5 < length);
                            this.A0G = bArr;
                            if (this.A0C) {
                                this.A07 = (int) jArr[0];
                                this.A06 = i4;
                                return;
                            }
                            return;
                        }
                        Log.w(str, str3);
                        return;
                    }
                    return;
                }
            }
            if (A0T) {
                Log.d("ExifInterface", "Unsupported data type value");
                return;
            }
            return;
        }
        this.A05 = 6;
        AnonymousClass0UH r2 = (AnonymousClass0UH) hashMap.get("JPEGInterchangeFormat");
        AnonymousClass0UH r15 = (AnonymousClass0UH) hashMap.get("JPEGInterchangeFormatLength");
        if (!(r2 == null || r15 == null)) {
            int A043 = r2.A04(this.A0A);
            int A044 = r15.A04(this.A0A);
            if (this.A00 == 7) {
                A043 += this.A02;
            }
            if (A043 > 0 && A044 > 0) {
                this.A0D = true;
                if (this.A09 == null && this.A08 == null) {
                    byte[] bArr3 = new byte[A044];
                    r22.skip((long) A043);
                    r22.read(bArr3);
                    this.A0G = bArr3;
                }
                this.A07 = A043;
                this.A06 = A044;
            }
            if (A0T) {
                StringBuilder sb2 = new StringBuilder("Setting thumbnail attributes with offset: ");
                sb2.append(A043);
                sb2.append(", length: ");
                sb2.append(A044);
                str2 = sb2.toString();
                str = "ExifInterface";
                Log.d(str, str2);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:78:0x019e, code lost:
        r25.A01 = r24.A0A;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01a2, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x00a8, code lost:
        continue;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ae A[FALL_THROUGH] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0I(X.C03660Iq r25, int r26, int r27) {
        /*
        // Method dump skipped, instructions count: 542
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03i.A0I(X.0Iq, int, int):void");
    }

    public final void A0J(C03660Iq r5, C03630In r6, byte[] bArr, byte[] bArr2) {
        String obj;
        while (true) {
            byte[] bArr3 = new byte[4];
            if (r5.read(bArr3) != 4) {
                StringBuilder sb = new StringBuilder("Encountered invalid length while copying WebP chunks up tochunk type ");
                Charset charset = A0K;
                sb.append(new String(bArr, charset));
                if (bArr2 == null) {
                    obj = "";
                } else {
                    StringBuilder sb2 = new StringBuilder(" or ");
                    sb2.append(new String(bArr2, charset));
                    obj = sb2.toString();
                }
                sb.append(obj);
                throw new IOException(sb.toString());
            }
            int readInt = r5.readInt();
            r6.write(bArr3);
            r6.A00(readInt);
            if (readInt % 2 == 1) {
                readInt++;
            }
            A06(r5, r6, readInt);
            if (Arrays.equals(bArr3, bArr)) {
                return;
            }
            if (bArr2 != null && Arrays.equals(bArr3, bArr2)) {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:118:0x0332  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0268  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0K(X.C03630In r20) {
        /*
        // Method dump skipped, instructions count: 844
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03i.A0K(X.0In):void");
    }

    public final void A0L(AnonymousClass0E9 r6) {
        A0G(r6);
        A0M(r6, 0);
        A0N(r6, 0);
        A0N(r6, 5);
        A0N(r6, 4);
        A0D();
        if (this.A00 == 8) {
            HashMap[] hashMapArr = this.A0H;
            AnonymousClass0UH r0 = (AnonymousClass0UH) hashMapArr[1].get("MakerNote");
            if (r0 != null) {
                AnonymousClass0E9 r1 = new AnonymousClass0E9(r0.A03);
                r1.A01 = this.A0A;
                r1.A00(6);
                A0M(r1, 9);
                Object obj = hashMapArr[9].get("ColorSpace");
                if (obj != null) {
                    hashMapArr[1].put("ColorSpace", obj);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0207, code lost:
        if (r7 == 7) goto L_0x0209;
     */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x02aa  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x022b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0M(X.AnonymousClass0E9 r32, int r33) {
        /*
        // Method dump skipped, instructions count: 845
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03i.A0M(X.0E9, int):void");
    }

    public final void A0N(AnonymousClass0E9 r13, int i) {
        StringBuilder sb;
        String str;
        AnonymousClass0UH A01;
        AnonymousClass0UH A012;
        HashMap[] hashMapArr = this.A0H;
        AnonymousClass0UH r8 = (AnonymousClass0UH) hashMapArr[i].get("DefaultCropSize");
        AnonymousClass0UH r7 = (AnonymousClass0UH) hashMapArr[i].get("SensorTopBorder");
        AnonymousClass0UH r9 = (AnonymousClass0UH) hashMapArr[i].get("SensorLeftBorder");
        AnonymousClass0UH r2 = (AnonymousClass0UH) hashMapArr[i].get("SensorBottomBorder");
        AnonymousClass0UH r4 = (AnonymousClass0UH) hashMapArr[i].get("SensorRightBorder");
        if (r8 != null) {
            int i2 = r8.A00;
            Object A05 = r8.A05(this.A0A);
            if (i2 == 5) {
                AnonymousClass0OJ[] r82 = (AnonymousClass0OJ[]) A05;
                if (r82 == null || r82.length != 2) {
                    sb = new StringBuilder();
                    sb.append("Invalid crop size values. cropSize=");
                    str = Arrays.toString(r82);
                    sb.append(str);
                    Log.w("ExifInterface", sb.toString());
                    return;
                }
                A01 = AnonymousClass0UH.A03(this.A0A, new AnonymousClass0OJ[]{r82[0]});
                A012 = AnonymousClass0UH.A03(this.A0A, new AnonymousClass0OJ[]{r82[1]});
                hashMapArr[i].put("ImageWidth", A01);
                hashMapArr[i].put("ImageLength", A012);
            }
            int[] iArr = (int[]) A05;
            if (iArr == null || iArr.length != 2) {
                sb = new StringBuilder();
                sb.append("Invalid crop size values. cropSize=");
                str = Arrays.toString(iArr);
                sb.append(str);
                Log.w("ExifInterface", sb.toString());
                return;
            }
            A01 = AnonymousClass0UH.A01(this.A0A, new int[]{iArr[0]});
            A012 = AnonymousClass0UH.A01(this.A0A, new int[]{iArr[1]});
            hashMapArr[i].put("ImageWidth", A01);
            hashMapArr[i].put("ImageLength", A012);
        } else if (r7 == null || r9 == null || r2 == null || r4 == null) {
            Object obj = hashMapArr[i].get("ImageLength");
            Object obj2 = hashMapArr[i].get("ImageWidth");
            if (obj == null || obj2 == null) {
                AnonymousClass0UH r22 = (AnonymousClass0UH) hashMapArr[i].get("JPEGInterchangeFormat");
                Object obj3 = hashMapArr[i].get("JPEGInterchangeFormatLength");
                if (r22 != null && obj3 != null) {
                    int A04 = r22.A04(this.A0A);
                    int A042 = r22.A04(this.A0A);
                    r13.A01((long) A04);
                    byte[] bArr = new byte[A042];
                    r13.read(bArr);
                    A0I(new C03660Iq(bArr), A04, i);
                }
            }
        } else {
            int A043 = r7.A04(this.A0A);
            int A044 = r2.A04(this.A0A);
            int A045 = r4.A04(this.A0A);
            int A046 = r9.A04(this.A0A);
            if (A044 > A043 && A045 > A046) {
                AnonymousClass0UH A013 = AnonymousClass0UH.A01(this.A0A, new int[]{A044 - A043});
                AnonymousClass0UH A014 = AnonymousClass0UH.A01(this.A0A, new int[]{A045 - A046});
                hashMapArr[i].put("ImageLength", A013);
                hashMapArr[i].put("ImageWidth", A014);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0042, code lost:
        if (r4 == false) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005f, code lost:
        if (r4 != false) goto L_0x0061;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0O(java.lang.String r19) {
        /*
        // Method dump skipped, instructions count: 940
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03i.A0O(java.lang.String):void");
    }

    public final void A0P(String str) {
        for (int i = 0; i < A16.length; i++) {
            this.A0H[i].remove(str);
        }
    }

    public final boolean A0Q(HashMap hashMap) {
        AnonymousClass0UH r1 = (AnonymousClass0UH) hashMap.get("ImageLength");
        AnonymousClass0UH r2 = (AnonymousClass0UH) hashMap.get("ImageWidth");
        if (!(r1 == null || r2 == null)) {
            int A04 = r1.A04(this.A0A);
            int A042 = r2.A04(this.A0A);
            if (A04 <= 512 && A042 <= 512) {
                return true;
            }
        }
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 5, insn: 0x008d: MOVE  (r9 I:??[OBJECT, ARRAY]) = (r5 I:??[OBJECT, ARRAY]), block:B:45:0x008d
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public byte[] A0R() {
        /*
            r10 = this;
            java.lang.String r6 = "ExifInterface"
            boolean r0 = r10.A0D
            r9 = 0
            if (r0 == 0) goto L_0x0099
            byte[] r0 = r10.A0G
            if (r0 == 0) goto L_0x000c
            return r0
        L_0x000c:
            java.lang.String r0 = r10.A09     // Catch: Exception -> 0x007b, all -> 0x0078
            if (r0 == 0) goto L_0x0017
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch: Exception -> 0x007b, all -> 0x0078
            r5.<init>(r0)     // Catch: Exception -> 0x007b, all -> 0x0078
            r4 = r9
            goto L_0x0035
        L_0x0017:
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch: Exception -> 0x007b, all -> 0x0078
            r0 = 21
            if (r1 < r0) goto L_0x0033
            java.io.FileDescriptor r0 = r10.A08     // Catch: Exception -> 0x007b, all -> 0x0078
            java.io.FileDescriptor r4 = X.AnonymousClass0T8.A00(r0)     // Catch: Exception -> 0x007b, all -> 0x0078
            r1 = 0
            int r0 = android.system.OsConstants.SEEK_SET     // Catch: Exception -> 0x0030, all -> 0x008f
            X.AnonymousClass0T8.A02(r4, r0, r1)     // Catch: Exception -> 0x0030, all -> 0x008f
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch: Exception -> 0x0030, all -> 0x008f
            r5.<init>(r4)     // Catch: Exception -> 0x0030, all -> 0x008f
            goto L_0x0035
        L_0x0030:
            r1 = move-exception
            r5 = r9
            goto L_0x007e
        L_0x0033:
            r5 = r9
            r4 = r9
        L_0x0035:
            if (r5 == 0) goto L_0x0070
            int r1 = r10.A07     // Catch: Exception -> 0x0076, all -> 0x008c
            int r0 = r10.A01     // Catch: Exception -> 0x0076, all -> 0x008c
            int r1 = r1 + r0
            long r0 = (long) r1     // Catch: Exception -> 0x0076, all -> 0x008c
            long r7 = r5.skip(r0)     // Catch: Exception -> 0x0076, all -> 0x008c
            int r1 = r10.A07     // Catch: Exception -> 0x0076, all -> 0x008c
            int r0 = r10.A01     // Catch: Exception -> 0x0076, all -> 0x008c
            int r1 = r1 + r0
            long r1 = (long) r1
            java.lang.String r3 = "Corrupted image"
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x006a
            int r0 = r10.A06     // Catch: Exception -> 0x0076, all -> 0x008c
            byte[] r2 = new byte[r0]     // Catch: Exception -> 0x0076, all -> 0x008c
            int r1 = r5.read(r2)     // Catch: Exception -> 0x0076, all -> 0x008c
            int r0 = r10.A06     // Catch: Exception -> 0x0076, all -> 0x008c
            if (r1 != r0) goto L_0x0064
            r10.A0G = r2     // Catch: Exception -> 0x0076, all -> 0x008c
            A03(r5)
            if (r4 == 0) goto L_0x0063
            A04(r4)
        L_0x0063:
            return r2
        L_0x0064:
            java.io.IOException r0 = new java.io.IOException     // Catch: Exception -> 0x0076, all -> 0x008c
            r0.<init>(r3)     // Catch: Exception -> 0x0076, all -> 0x008c
            throw r0     // Catch: Exception -> 0x0076, all -> 0x008c
        L_0x006a:
            java.io.IOException r0 = new java.io.IOException     // Catch: Exception -> 0x0076, all -> 0x008c
            r0.<init>(r3)     // Catch: Exception -> 0x0076, all -> 0x008c
            throw r0     // Catch: Exception -> 0x0076, all -> 0x008c
        L_0x0070:
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException     // Catch: Exception -> 0x0076, all -> 0x008c
            r0.<init>()     // Catch: Exception -> 0x0076, all -> 0x008c
            throw r0     // Catch: Exception -> 0x0076, all -> 0x008c
        L_0x0076:
            r1 = move-exception
            goto L_0x007e
        L_0x0078:
            r0 = move-exception
            r4 = r9
            goto L_0x0090
        L_0x007b:
            r1 = move-exception
            r5 = r9
            r4 = r9
        L_0x007e:
            java.lang.String r0 = "Encountered exception while getting thumbnail"
            android.util.Log.d(r6, r0, r1)     // Catch: all -> 0x008c
            A03(r5)
            if (r4 == 0) goto L_0x0099
            A04(r4)
            return r9
        L_0x008c:
            r0 = move-exception
            r9 = r5
            goto L_0x0090
        L_0x008f:
            r0 = move-exception
        L_0x0090:
            A03(r9)
            if (r4 == 0) goto L_0x0098
            A04(r4)
        L_0x0098:
            throw r0
        L_0x0099:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass03i.A0R():byte[]");
    }
}
