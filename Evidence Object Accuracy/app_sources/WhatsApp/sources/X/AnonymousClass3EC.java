package X;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

/* renamed from: X.3EC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3EC {
    public final Matrix A00 = C13000ix.A01();
    public final C64243Eu A01;
    public final float[] A02 = new float[2];

    public AnonymousClass3EC(C64243Eu r2) {
        this.A01 = r2;
    }

    public PointF A00(float f, float f2) {
        float height;
        Matrix matrix = this.A00;
        matrix.reset();
        C64243Eu r2 = this.A01;
        matrix.preRotate((float) (-r2.A02));
        int i = r2.A02;
        float f3 = 0.0f;
        if (i == 90) {
            matrix.preTranslate((float) (-((int) r2.A0B.width())), 0.0f);
        } else {
            if (i == 180) {
                RectF rectF = r2.A0B;
                f3 = (float) (-((int) rectF.width()));
                height = rectF.height();
            } else if (i == 270) {
                height = r2.A0B.height();
            } else if (i != 0) {
                throw new IllegalArgumentException();
            }
            matrix.preTranslate(f3, (float) (-((int) height)));
        }
        Rect rect = r2.A05;
        if (rect != null) {
            f = ((f * ((float) rect.width())) / ((float) r2.A04)) + ((float) rect.left);
            f2 = ((f2 * ((float) rect.height())) / ((float) r2.A03)) + ((float) rect.top);
        }
        float[] fArr = this.A02;
        RectF rectF2 = r2.A0B;
        fArr[0] = f - rectF2.left;
        fArr[1] = f2 - rectF2.top;
        matrix.mapPoints(fArr);
        RectF rectF3 = r2.A07;
        float f4 = rectF3.left;
        RectF rectF4 = r2.A06;
        float f5 = f4 - rectF4.left;
        float f6 = fArr[0];
        float f7 = r2.A00;
        return new PointF(f5 + (f6 / f7), (rectF3.top - rectF4.top) + (fArr[1] / f7));
    }

    public PointF A01(PointF pointF) {
        float height;
        float[] fArr = this.A02;
        float f = pointF.x;
        C64243Eu r2 = this.A01;
        RectF rectF = r2.A07;
        float f2 = rectF.left;
        RectF rectF2 = r2.A06;
        float f3 = r2.A00;
        fArr[0] = (f - (f2 - rectF2.left)) * f3;
        fArr[1] = (pointF.y - (rectF.top - rectF2.top)) * f3;
        Matrix matrix = this.A00;
        matrix.reset();
        int i = r2.A02;
        float f4 = 0.0f;
        if (i == 90) {
            matrix.preTranslate((float) ((int) r2.A0B.width()), 0.0f);
        } else {
            if (i == 180) {
                RectF rectF3 = r2.A0B;
                f4 = (float) ((int) rectF3.width());
                height = rectF3.height();
            } else if (i == 270) {
                height = r2.A0B.height();
            } else if (i != 0) {
                throw new IllegalArgumentException();
            }
            matrix.preTranslate(f4, (float) ((int) height));
        }
        matrix.preRotate((float) r2.A02);
        matrix.mapPoints(fArr);
        float f5 = fArr[0];
        float f6 = fArr[1];
        RectF rectF4 = r2.A0B;
        float f7 = f5 + rectF4.left;
        float f8 = f6 + rectF4.top;
        Rect rect = r2.A05;
        if (rect != null) {
            f7 = ((f7 - ((float) rect.left)) * ((float) r2.A04)) / ((float) rect.width());
            f8 = ((f8 - ((float) rect.top)) * ((float) r2.A03)) / ((float) rect.height());
        }
        return new PointF(f7, f8);
    }
}
