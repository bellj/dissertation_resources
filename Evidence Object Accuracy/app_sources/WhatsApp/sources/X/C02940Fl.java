package X;

/* renamed from: X.0Fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02940Fl extends AnonymousClass0OL {
    public C02940Fl() {
        super(8, 9);
    }

    @Override // X.AnonymousClass0OL
    public void A00(AbstractC12920im r3) {
        ((AnonymousClass0ZE) r3).A00.execSQL("ALTER TABLE workspec ADD COLUMN `run_in_foreground` INTEGER NOT NULL DEFAULT 0");
    }
}
