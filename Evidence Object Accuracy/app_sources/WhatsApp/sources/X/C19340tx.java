package X;

import android.content.SharedPreferences;

/* renamed from: X.0tx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19340tx implements AbstractC16990q5 {
    public final C17170qN A00;
    public final C14820m6 A01;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public C19340tx(C17170qN r1, C14820m6 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        SharedPreferences.Editor edit = this.A01.A00.edit();
        edit.remove("registration_start_time").remove("forced_migration_failed_upload_timestamp").remove("biz_show_welcome_banner").remove("group_participant_migration_reset_fix").remove("privacy_dm_setting").remove("green_alert_dual_tos_shown").remove("payment_background_store_language").remove("google_migrate_has_whatsapp_data").remove("tos_v2_current_stage_id").remove("tos_v2_last_stage_1_display_time").remove("tos_v2_page_2_ack").remove("tos_v2_accepted_time").remove("tos_v2_accepted_ack").remove("tos_v2_accepted_time").remove("tos_v2_current_stage_id").remove("tos_v2_stage_start_time").remove("tos_v2_stage_start_ack").remove("tos_v2_last_stage_1_display_time").remove("tos_v2_page_2_ack").remove("smb_tos_accept_0518").remove("smb_tos_accepted_ack_0518").remove("smb_tos_prop_ack_0518").remove("ptt_fast_playback_fail_count").remove("ptt_exo_playback_fail_count").remove("device_security_notifications").remove("device_list_update_error_security_notifications").remove("sticker_db_migration_completed").remove("sticker_db_migration_retry_count").remove("pref_revoke_nux").remove("prefs_migration_version");
        int i = 1;
        do {
            StringBuilder sb = new StringBuilder();
            sb.append("tos_v2_stage_start_time");
            sb.append(i);
            edit.remove(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("tos_v2_stage_start_ack");
            sb2.append(i);
            edit.remove(sb2.toString());
            i++;
        } while (i <= 3);
        edit.apply();
    }
}
