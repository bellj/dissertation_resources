package X;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.io.File;
import java.util.ArrayList;

/* renamed from: X.2VR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VR implements AbstractC39381po {
    public final /* synthetic */ ViewOnClickCListenerShape4S0200000_I0 A00;

    @Override // X.AbstractC39381po
    public void AQA(Exception exc) {
    }

    public AnonymousClass2VR(ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I0) {
        this.A00 = viewOnClickCListenerShape4S0200000_I0;
    }

    @Override // X.AbstractC39381po
    public void AQX(File file, String str, byte[] bArr) {
        ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I0 = this.A00;
        TextStatusComposerActivity textStatusComposerActivity = (TextStatusComposerActivity) viewOnClickCListenerShape4S0200000_I0.A00;
        textStatusComposerActivity.A0V.setImageProgressBarVisibility(false);
        ((View) viewOnClickCListenerShape4S0200000_I0.A01).setVisibility(0);
        if (file == null) {
            Log.e("textstatus/gif-preview/file is null");
            return;
        }
        Uri fromFile = Uri.fromFile(new File(file.getAbsolutePath()));
        ArrayList arrayList = new ArrayList();
        arrayList.add(fromFile);
        String A04 = AbstractC32741cf.A04(textStatusComposerActivity.A0T.getStringText());
        C39341ph r1 = new C39341ph(fromFile);
        r1.A0D(A04);
        r1.A0C((byte) 13);
        C453421e r3 = new C453421e(r1);
        AnonymousClass24W r2 = new AnonymousClass24W(textStatusComposerActivity);
        r2.A0C = arrayList;
        r2.A08 = AnonymousClass1VX.A00.getRawString();
        r2.A01 = 9;
        r2.A0G = true;
        Bundle bundle = new Bundle();
        r3.A02(bundle);
        r2.A06 = bundle;
        textStatusComposerActivity.A2E(r2.A00(), 1);
    }
}
