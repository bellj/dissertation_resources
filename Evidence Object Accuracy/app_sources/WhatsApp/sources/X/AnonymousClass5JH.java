package X;

import com.whatsapp.R;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoImageView;

/* renamed from: X.5JH  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5JH extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AvatarProfilePhotoImageView this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5JH(AvatarProfilePhotoImageView avatarProfilePhotoImageView) {
        super(0);
        this.this$0 = avatarProfilePhotoImageView;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return Float.valueOf(this.this$0.getResources().getDimension(R.dimen.avatar_pose_border_stroke_idle));
    }
}
