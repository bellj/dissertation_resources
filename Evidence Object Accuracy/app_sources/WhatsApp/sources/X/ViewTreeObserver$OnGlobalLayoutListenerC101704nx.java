package X;

import android.view.ViewTreeObserver;
import androidx.core.widget.NestedScrollView;
import com.whatsapp.greenalert.GreenAlertActivity;

/* renamed from: X.4nx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101704nx implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ NestedScrollView A00;
    public final /* synthetic */ GreenAlertActivity A01;

    public ViewTreeObserver$OnGlobalLayoutListenerC101704nx(NestedScrollView nestedScrollView, GreenAlertActivity greenAlertActivity) {
        this.A01 = greenAlertActivity;
        this.A00 = nestedScrollView;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C12980iv.A1E(this.A00, this);
        GreenAlertActivity greenAlertActivity = this.A01;
        greenAlertActivity.A2h(greenAlertActivity.A06.getCurrentLogicalItem());
    }
}
