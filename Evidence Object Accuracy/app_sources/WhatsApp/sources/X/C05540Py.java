package X;

import android.graphics.Rect;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* renamed from: X.0Py  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05540Py {
    public float A00;
    public float A01;
    public float A02;
    public int A03 = 0;
    public Rect A04;
    public AnonymousClass036 A05;
    public C013506i A06;
    public List A07;
    public List A08;
    public Map A09;
    public Map A0A;
    public Map A0B;
    public boolean A0C;
    public final C04840Ng A0D = new C04840Ng();
    public final HashSet A0E = new HashSet();

    public AnonymousClass0NQ A00(String str) {
        int size = this.A08.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass0NQ r4 = (AnonymousClass0NQ) this.A08.get(i);
            String str2 = r4.A02;
            if (str2.equalsIgnoreCase(str) || (str2.endsWith("\r") && str2.substring(0, str2.length() - 1).equalsIgnoreCase(str))) {
                return r4;
            }
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("LottieComposition:\n");
        for (AnonymousClass0PU r1 : this.A07) {
            sb.append(r1.A00("\t"));
        }
        return sb.toString();
    }
}
