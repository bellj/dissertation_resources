package X;

import com.whatsapp.Mp4Ops;

/* renamed from: X.31z  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31z extends AbstractC628938z {
    public final C16590pI A00;

    public AnonymousClass31z(AbstractC15710nm r12, Mp4Ops mp4Ops, C18790t3 r14, C17050qB r15, C14830m7 r16, C16590pI r17, AnonymousClass1O6 r18, AbstractC39381po r19, String str) {
        super(r12, mp4Ops, r14, r15, r16, r18, r19, str, false);
        this.A00 = r17;
    }
}
