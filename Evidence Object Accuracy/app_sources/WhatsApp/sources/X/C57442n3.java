package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57442n3 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57442n3 A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public C56932mC A02;
    public C57122mW A03;
    public String A04 = "";

    static {
        C57442n3 r0 = new C57442n3();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        C81793ua r1;
        C81783uZ r12;
        switch (r5.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C57442n3 r7 = (C57442n3) obj2;
                this.A04 = r6.Afy(this.A04, r7.A04, C12960it.A1R(this.A00), C12960it.A1R(r7.A00));
                this.A02 = (C56932mC) r6.Aft(this.A02, r7.A02);
                this.A01 = r6.Afp(this.A01, r7.A01, C12960it.A1V(this.A00 & 4, 4), C12960it.A1V(r7.A00 & 4, 4));
                this.A03 = (C57122mW) r6.Aft(this.A03, r7.A03);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r62.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r62.A0A();
                            this.A00 = 1 | this.A00;
                            this.A04 = A0A;
                        } else if (A03 == 18) {
                            if ((this.A00 & 2) == 2) {
                                r12 = (C81783uZ) this.A02.A0T();
                            } else {
                                r12 = null;
                            }
                            C56932mC r0 = (C56932mC) AbstractC27091Fz.A0H(r62, r72, C56932mC.A02);
                            this.A02 = r0;
                            if (r12 != null) {
                                this.A02 = (C56932mC) AbstractC27091Fz.A0C(r12, r0);
                            }
                            this.A00 |= 2;
                        } else if (A03 == 24) {
                            int A02 = r62.A02();
                            if (A02 == 0 || A02 == 1 || A02 == 2) {
                                this.A00 |= 4;
                                this.A01 = A02;
                            } else {
                                super.A0X(3, A02);
                            }
                        } else if (A03 == 34) {
                            if ((this.A00 & 8) == 8) {
                                r1 = (C81793ua) this.A03.A0T();
                            } else {
                                r1 = null;
                            }
                            C57122mW r02 = (C57122mW) AbstractC27091Fz.A0H(r62, r72, C57122mW.A03);
                            this.A03 = r02;
                            if (r1 != null) {
                                this.A03 = (C57122mW) AbstractC27091Fz.A0C(r1, r02);
                            }
                            this.A00 |= 8;
                        } else if (!A0a(r62, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C57442n3();
            case 5:
                return new C81773uY();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C57442n3.class) {
                        if (A06 == null) {
                            A06 = AbstractC27091Fz.A09(A05);
                        }
                    }
                }
                return A06;
            default:
                throw C12970iu.A0z();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A04, 0);
        }
        if ((this.A00 & 2) == 2) {
            C56932mC r0 = this.A02;
            if (r0 == null) {
                r0 = C56932mC.A02;
            }
            i2 = AbstractC27091Fz.A08(r0, 2, i2);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A03(3, this.A01, i2);
        }
        if ((i3 & 8) == 8) {
            C57122mW r02 = this.A03;
            if (r02 == null) {
                r02 = C57122mW.A03;
            }
            i2 = AbstractC27091Fz.A08(r02, 4, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A04);
        }
        if ((this.A00 & 2) == 2) {
            C56932mC r0 = this.A02;
            if (r0 == null) {
                r0 = C56932mC.A02;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0E(3, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            C57122mW r02 = this.A03;
            if (r02 == null) {
                r02 = C57122mW.A03;
            }
            codedOutputStream.A0L(r02, 4);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
