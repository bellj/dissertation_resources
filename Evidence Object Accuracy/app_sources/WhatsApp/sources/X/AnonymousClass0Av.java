package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/* renamed from: X.0Av  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Av extends GestureDetector.SimpleOnGestureListener {
    public boolean A00 = true;
    public final /* synthetic */ AnonymousClass0F6 A01;

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    public AnonymousClass0Av(AnonymousClass0F6 r2) {
        this.A01 = r2;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
        AnonymousClass0F6 r3;
        View A05;
        AnonymousClass03U A0E;
        int i;
        if (this.A00 && (A05 = (r3 = this.A01).A05(motionEvent)) != null && (A0E = r3.A0M.A0E(A05)) != null) {
            AbstractC06200So r4 = r3.A0I;
            if ((r4.A00(A0E, r3.A0M) & 16711680) != 0 && motionEvent.getPointerId(0) == (i = r3.A09)) {
                int findPointerIndex = motionEvent.findPointerIndex(i);
                float x = motionEvent.getX(findPointerIndex);
                float y = motionEvent.getY(findPointerIndex);
                r3.A02 = x;
                r3.A03 = y;
                r3.A01 = 0.0f;
                r3.A00 = 0.0f;
                if (r4.A05()) {
                    r3.A0A(A0E, 2);
                }
            }
        }
    }
}
