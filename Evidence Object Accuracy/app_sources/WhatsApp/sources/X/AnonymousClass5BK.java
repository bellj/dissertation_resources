package X;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.5BK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5BK implements FilenameFilter {
    @Override // java.io.FilenameFilter
    public boolean accept(File file, String str) {
        return str.startsWith("override-") && str.endsWith(".log");
    }
}
