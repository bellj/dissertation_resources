package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.Calendar;

/* renamed from: X.32s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618332s extends AbstractC75713kI {
    public final TextView A00;
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C618332s(View view, AbstractView$OnCreateContextMenuListenerC35851ir r3) {
        super(view, r3);
        this.A01 = r3;
        this.A00 = C12960it.A0J(view, R.id.live_location_end_text);
    }

    @Override // X.AbstractC75713kI
    public void A08(C15370n3 r12, C30751Yr r13) {
        String A0X;
        int i;
        TextView textView = this.A00;
        AbstractView$OnCreateContextMenuListenerC35851ir r9 = this.A01;
        C14830m7 r2 = r9.A1A;
        long A02 = r2.A02(r13.A05);
        int A00 = C38121nY.A00(r2.A00(), A02);
        if (A00 <= 6) {
            if (A00 != 0) {
                if (A00 != 1) {
                    Calendar instance = Calendar.getInstance();
                    instance.setTimeInMillis(A02);
                    switch (instance.get(7)) {
                        case 1:
                            i = R.string.live_location_final_update_sunday;
                            break;
                        case 2:
                            i = R.string.live_location_final_update_monday;
                            break;
                        case 3:
                            i = R.string.live_location_final_update_tuesday;
                            break;
                        case 4:
                            i = R.string.live_location_final_update_wednesday;
                            break;
                        case 5:
                            i = R.string.live_location_final_update_thursday;
                            break;
                        case 6:
                            i = R.string.live_location_final_update_friday;
                            break;
                        case 7:
                            i = R.string.live_location_final_update_saturday;
                            break;
                        default:
                            i = 0;
                            break;
                    }
                } else {
                    i = R.string.live_location_final_update_yesterday;
                }
            } else {
                i = R.string.live_location_final_update_today;
            }
            AnonymousClass018 r5 = r9.A1C;
            A0X = AnonymousClass3JK.A01(r5, C12960it.A0X(r9.A0E, AnonymousClass3JK.A00(r5, A02), new Object[1], 0, i), A02);
        } else {
            A0X = C12960it.A0X(r9.A0E, C38131nZ.A03(r9.A1C, A00, A02), new Object[1], 0, R.string.live_location_final_update);
        }
        textView.setText(A0X);
    }
}
