package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: X.4mC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100614mC implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(0);
    public int A00;
    public final float A01;
    public final float A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final int A0C;
    public final int A0D;
    public final int A0E;
    public final int A0F;
    public final int A0G;
    public final int A0H;
    public final int A0I;
    public final long A0J;
    public final C112295Cv A0K;
    public final C100624mD A0L;
    public final C100604mB A0M;
    public final Class A0N;
    public final String A0O;
    public final String A0P;
    public final String A0Q;
    public final String A0R;
    public final String A0S;
    public final String A0T;
    public final List A0U;
    public final byte[] A0V;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C100614mC(C93844ap r6) {
        this.A0Q = r6.A0O;
        this.A0R = r6.A0P;
        this.A0S = AnonymousClass3JZ.A0A(r6.A0Q);
        this.A0G = r6.A0E;
        this.A0D = r6.A0B;
        int i = r6.A03;
        this.A04 = i;
        int i2 = r6.A0A;
        this.A0C = i2;
        this.A05 = i2 != -1 ? i2 : i;
        this.A0O = r6.A0M;
        this.A0L = r6.A0J;
        this.A0P = r6.A0N;
        this.A0T = r6.A0R;
        this.A0A = r6.A08;
        List list = r6.A0S;
        this.A0U = list == null ? Collections.emptyList() : list;
        C112295Cv r2 = r6.A0I;
        this.A0K = r2;
        this.A0J = r6.A0H;
        this.A0I = r6.A0G;
        this.A09 = r6.A07;
        this.A01 = r6.A00;
        int i3 = r6.A0C;
        int i4 = 0;
        this.A0E = i3 == -1 ? 0 : i3;
        float f = r6.A01;
        this.A02 = f == -1.0f ? 1.0f : f;
        this.A0V = r6.A0T;
        this.A0H = r6.A0F;
        this.A0M = r6.A0K;
        this.A06 = r6.A04;
        this.A0F = r6.A0D;
        this.A0B = r6.A09;
        int i5 = r6.A05;
        this.A07 = i5 == -1 ? 0 : i5;
        int i6 = r6.A06;
        this.A08 = i6 != -1 ? i6 : i4;
        this.A03 = r6.A02;
        Class<C106634w1> cls = r6.A0L;
        if (cls == null && r2 != null) {
            cls = C106634w1.class;
        }
        this.A0N = cls;
    }

    public C100614mC(Parcel parcel) {
        byte[] bArr;
        this.A0Q = parcel.readString();
        this.A0R = parcel.readString();
        this.A0S = parcel.readString();
        this.A0G = parcel.readInt();
        this.A0D = parcel.readInt();
        int readInt = parcel.readInt();
        this.A04 = readInt;
        int readInt2 = parcel.readInt();
        this.A0C = readInt2;
        this.A05 = readInt2 != -1 ? readInt2 : readInt;
        this.A0O = parcel.readString();
        this.A0L = (C100624mD) C12990iw.A0I(parcel, C100624mD.class);
        this.A0P = parcel.readString();
        this.A0T = parcel.readString();
        this.A0A = parcel.readInt();
        int readInt3 = parcel.readInt();
        this.A0U = C12980iv.A0w(readInt3);
        for (int i = 0; i < readInt3; i++) {
            this.A0U.add(parcel.createByteArray());
        }
        C112295Cv r2 = (C112295Cv) C12990iw.A0I(parcel, C112295Cv.class);
        this.A0K = r2;
        this.A0J = parcel.readLong();
        this.A0I = parcel.readInt();
        this.A09 = parcel.readInt();
        this.A01 = parcel.readFloat();
        this.A0E = parcel.readInt();
        this.A02 = parcel.readFloat();
        Class cls = null;
        if (C12960it.A1S(parcel.readInt())) {
            bArr = parcel.createByteArray();
        } else {
            bArr = null;
        }
        this.A0V = bArr;
        this.A0H = parcel.readInt();
        this.A0M = (C100604mB) C12990iw.A0I(parcel, C100604mB.class);
        this.A06 = parcel.readInt();
        this.A0F = parcel.readInt();
        this.A0B = parcel.readInt();
        this.A07 = parcel.readInt();
        this.A08 = parcel.readInt();
        this.A03 = parcel.readInt();
        this.A0N = r2 != null ? C106634w1.class : cls;
    }

    public boolean A00(C100614mC r7) {
        List list = this.A0U;
        int size = list.size();
        List list2 = r7.A0U;
        if (size == list2.size()) {
            for (int i = 0; i < list.size(); i++) {
                if (Arrays.equals(C72463ee.A0b(list, i), C72463ee.A0b(list2, i))) {
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        int i;
        if (this == obj) {
            return true;
        }
        if (obj != null && C100614mC.class == obj.getClass()) {
            C100614mC r7 = (C100614mC) obj;
            int i2 = this.A00;
            if ((i2 == 0 || (i = r7.A00) == 0 || i2 == i) && this.A0G == r7.A0G && this.A0D == r7.A0D && this.A04 == r7.A04 && this.A0C == r7.A0C && this.A0A == r7.A0A && this.A0J == r7.A0J && this.A0I == r7.A0I && this.A09 == r7.A09 && this.A0E == r7.A0E && this.A0H == r7.A0H && this.A06 == r7.A06 && this.A0F == r7.A0F && this.A0B == r7.A0B && this.A07 == r7.A07 && this.A08 == r7.A08 && this.A03 == r7.A03 && Float.compare(this.A01, r7.A01) == 0 && Float.compare(this.A02, r7.A02) == 0 && AnonymousClass3JZ.A0H(this.A0N, r7.A0N) && AnonymousClass3JZ.A0H(this.A0Q, r7.A0Q) && AnonymousClass3JZ.A0H(this.A0R, r7.A0R) && AnonymousClass3JZ.A0H(this.A0O, r7.A0O) && AnonymousClass3JZ.A0H(this.A0P, r7.A0P) && AnonymousClass3JZ.A0H(this.A0T, r7.A0T) && AnonymousClass3JZ.A0H(this.A0S, r7.A0S) && Arrays.equals(this.A0V, r7.A0V) && AnonymousClass3JZ.A0H(this.A0L, r7.A0L) && AnonymousClass3JZ.A0H(this.A0M, r7.A0M) && AnonymousClass3JZ.A0H(this.A0K, r7.A0K) && A00(r7)) {
                return true;
            }
            return false;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        int A04 = (((((((((((((((((((((((((((((((((((((((((((((((((527 + C72463ee.A04(this.A0Q)) * 31) + C72453ed.A0E(this.A0R)) * 31) + C72463ee.A04(this.A0S)) * 31) + this.A0G) * 31) + this.A0D) * 31) + this.A04) * 31) + this.A0C) * 31) + C72463ee.A04(this.A0O)) * 31) + C72453ed.A0D(this.A0L)) * 31) + C72463ee.A04(this.A0P)) * 31) + C72463ee.A04(this.A0T)) * 31) + this.A0A) * 31) + ((int) this.A0J)) * 31) + this.A0I) * 31) + this.A09) * 31) + Float.floatToIntBits(this.A01)) * 31) + this.A0E) * 31) + Float.floatToIntBits(this.A02)) * 31) + this.A0H) * 31) + this.A06) * 31) + this.A0F) * 31) + this.A0B) * 31) + this.A07) * 31) + this.A08) * 31) + this.A03) * 31;
        Class cls = this.A0N;
        if (cls != null) {
            i2 = cls.hashCode();
        }
        int i3 = A04 + i2;
        this.A00 = i3;
        return i3;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Format(");
        A0k.append(this.A0Q);
        A0k.append(", ");
        A0k.append(this.A0R);
        A0k.append(", ");
        A0k.append(this.A0P);
        A0k.append(", ");
        A0k.append(this.A0T);
        A0k.append(", ");
        A0k.append(this.A0O);
        A0k.append(", ");
        A0k.append(this.A05);
        A0k.append(", ");
        A0k.append(this.A0S);
        A0k.append(", [");
        A0k.append(this.A0I);
        A0k.append(", ");
        A0k.append(this.A09);
        A0k.append(", ");
        A0k.append(this.A01);
        A0k.append("], [");
        A0k.append(this.A06);
        A0k.append(", ");
        A0k.append(this.A0F);
        return C12960it.A0d("])", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A0Q);
        parcel.writeString(this.A0R);
        parcel.writeString(this.A0S);
        parcel.writeInt(this.A0G);
        parcel.writeInt(this.A0D);
        parcel.writeInt(this.A04);
        parcel.writeInt(this.A0C);
        parcel.writeString(this.A0O);
        int i2 = 0;
        parcel.writeParcelable(this.A0L, 0);
        parcel.writeString(this.A0P);
        parcel.writeString(this.A0T);
        parcel.writeInt(this.A0A);
        List list = this.A0U;
        int size = list.size();
        parcel.writeInt(size);
        for (int i3 = 0; i3 < size; i3++) {
            parcel.writeByteArray(C72463ee.A0b(list, i3));
        }
        parcel.writeParcelable(this.A0K, 0);
        parcel.writeLong(this.A0J);
        parcel.writeInt(this.A0I);
        parcel.writeInt(this.A09);
        parcel.writeFloat(this.A01);
        parcel.writeInt(this.A0E);
        parcel.writeFloat(this.A02);
        byte[] bArr = this.A0V;
        if (bArr != null) {
            i2 = 1;
        }
        parcel.writeInt(i2);
        if (bArr != null) {
            parcel.writeByteArray(bArr);
        }
        parcel.writeInt(this.A0H);
        parcel.writeParcelable(this.A0M, i);
        parcel.writeInt(this.A06);
        parcel.writeInt(this.A0F);
        parcel.writeInt(this.A0B);
        parcel.writeInt(this.A07);
        parcel.writeInt(this.A08);
        parcel.writeInt(this.A03);
    }
}
