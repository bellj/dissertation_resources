package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.6Dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134126Dk implements AnonymousClass5Wu {
    public View A00;
    public TextView A01;
    public TextView A02;
    public TextView A03;
    public TextView A04;
    public TextView A05;
    public TextView A06;

    /* renamed from: A00 */
    public void A6Q(AnonymousClass4OZ r4) {
        if (r4 != null) {
            int i = r4.A00;
            if (!(i == -2 || i == -1)) {
                if (i == 0) {
                    this.A00.setVisibility(8);
                    return;
                } else if (!(i == 1 || i == 2)) {
                    return;
                }
            }
            this.A00.setVisibility(0);
            C1315563f r2 = (C1315563f) r4.A01;
            if (r2 != null) {
                this.A01.setText(r2.A03);
                this.A02.setText(r2.A00);
                this.A03.setText(r2.A04);
                this.A04.setText(r2.A01);
                this.A05.setText(r2.A05);
                this.A06.setText(r2.A02);
            }
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_send_money_review_extras;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = view;
        this.A01 = C12960it.A0I(view, R.id.novi_send_money_review_extras_sender_amount_title);
        this.A02 = C12960it.A0I(view, R.id.novi_send_money_review_extras_sender_amount);
        this.A03 = C12960it.A0I(view, R.id.novi_send_money_review_extras_fee_title);
        this.A04 = C12960it.A0I(view, R.id.novi_send_money_review_extras_fee_amount);
        this.A05 = C12960it.A0I(view, R.id.novi_send_money_review_extras_total_title);
        this.A06 = C12960it.A0I(view, R.id.novi_send_money_review_extras_total_amount);
    }
}
