package X;

import android.animation.ArgbEvaluator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;

/* renamed from: X.0A9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0A9 extends Drawable implements Animatable {
    public static final ArgbEvaluator A08 = new ArgbEvaluator();
    public static final TimeInterpolator A09 = new C06600Uh();
    public boolean A00;
    public final float A01;
    public final int A02;
    public final ValueAnimator.AnimatorUpdateListener A03;
    public final ValueAnimator A04;
    public final Paint A05;
    public final RectF A06;
    public final AnonymousClass0J7 A07;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass0A9(Context context, AnonymousClass0NC r13, AnonymousClass0NC r14, AnonymousClass0J7 r15, C14260l7 r16, float f, int i, int i2) {
        this(context, r13, r14, r15, r16, f, i, i2, 200);
    }

    public AnonymousClass0A9(Context context, AnonymousClass0NC r9, AnonymousClass0NC r10, AnonymousClass0J7 r11, C14260l7 r12, float f, int i, int i2, long j) {
        float f2;
        float f3;
        C06700Ur r1 = new C06700Ur(this);
        this.A03 = r1;
        this.A07 = r11;
        this.A06 = new RectF();
        Paint paint = new Paint();
        this.A05 = paint;
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        this.A02 = i2;
        this.A01 = AnonymousClass0LQ.A00(context, f);
        ValueAnimator valueAnimator = new ValueAnimator();
        this.A04 = valueAnimator;
        valueAnimator.setRepeatCount(-1);
        valueAnimator.addUpdateListener(r1);
        valueAnimator.setStartDelay(((long) (i % 10)) * 200);
        valueAnimator.setDuration(2000L);
        valueAnimator.setInterpolator(A09);
        valueAnimator.setEvaluator(A08);
        if (AnonymousClass0TG.A02(context, r12)) {
            f2 = r10.A00;
            f3 = r10.A01;
        } else {
            f2 = r9.A00;
            f3 = r9.A01;
        }
        valueAnimator.setFloatValues(f2, f3);
        paint.setColor(AnonymousClass0LP.A00(i2, f2));
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int ordinal = this.A07.ordinal();
        RectF rectF = this.A06;
        if (ordinal != 0) {
            float f = this.A01;
            canvas.drawRoundRect(rectF, f, f, this.A05);
            return;
        }
        canvas.drawCircle(rectF.centerX(), rectF.centerY(), Math.min(rectF.width(), rectF.height()) / 2.0f, this.A05);
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.A04.isStarted();
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.A06.set(rect);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        Paint paint = this.A05;
        if (paint.getAlpha() != i) {
            paint.setAlpha(i);
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        if (!z) {
            this.A04.cancel();
        } else if (this.A00) {
            this.A04.start();
        }
        return super.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        this.A04.start();
        this.A00 = true;
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        this.A04.cancel();
        this.A00 = false;
    }
}
