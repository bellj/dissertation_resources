package X;

import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.5Ff  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112895Ff implements AbstractC117265Ze {
    public C72353eS A00;

    public C112895Ff(C72353eS r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return new AnonymousClass5N5(this.A00.A01());
    }

    @Override // X.AbstractC117265Ze
    public InputStream AEi() {
        return this.A00;
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        try {
            return ADw();
        } catch (IOException e) {
            throw new AnonymousClass4CU(C12960it.A0d(e.getMessage(), C12960it.A0k("IOException converting stream to byte array: ")), e);
        }
    }
}
