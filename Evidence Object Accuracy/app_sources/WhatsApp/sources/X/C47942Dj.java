package X;

import java.util.Arrays;

/* renamed from: X.2Dj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47942Dj {
    public final int A00;
    public final String A01;

    public C47942Dj(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C47942Dj r5 = (C47942Dj) obj;
            if (this.A00 != r5.A00 || !C29941Vi.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), this.A01});
    }
}
