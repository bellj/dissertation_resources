package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;

/* renamed from: X.2cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53072cg extends LinearLayout.LayoutParams {
    public int A00 = 1;
    public Interpolator A01;

    public C53072cg() {
        super(-1, -2);
    }

    public C53072cg(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C50572Qb.A01);
        this.A00 = obtainStyledAttributes.getInt(0, 0);
        if (obtainStyledAttributes.hasValue(1)) {
            this.A01 = AnimationUtils.loadInterpolator(context, obtainStyledAttributes.getResourceId(1, 0));
        }
        obtainStyledAttributes.recycle();
    }

    public C53072cg(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public C53072cg(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }

    public C53072cg(LinearLayout.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
