package X;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/* renamed from: X.3IM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IM {
    public final int A00;
    public final short A01;
    public final String[] A02;

    public AnonymousClass3IM(String[] strArr, int i, short s) {
        this.A02 = strArr;
        this.A01 = s;
        this.A00 = i;
    }

    public static AnonymousClass3IM A00(byte[] bArr, int i) {
        short s;
        int i2;
        if (bArr != null) {
            int length = bArr.length;
            if (length >= 1) {
                ArrayList A0l = C12960it.A0l();
                StringBuilder A0h = C12960it.A0h();
                int i3 = i;
                while (true) {
                    if (bArr[i3] == 0 || (bArr[i3] & 192) != 0) {
                        break;
                    }
                    byte b = (byte) (bArr[i3] & 63);
                    int i4 = i3 + 1;
                    i3 = i4 + b;
                    if (length >= i3 + 1) {
                        A0h.setLength(0);
                        for (byte b2 = 0; b2 < b; b2 = (byte) (b2 + 1)) {
                            A0h.append((char) bArr[i4 + b2]);
                        }
                        A0l.add(A0h.toString());
                    } else {
                        throw C12970iu.A0f("bytes is incomplete");
                    }
                }
                byte b3 = bArr[i3];
                if (b3 == 0) {
                    i2 = i3 + 1;
                    s = 0;
                } else {
                    s = (short) (((((byte) (b3 & 63)) << 8) & 65280) | (bArr[i3 + 1] & 255));
                    i2 = i3 + 2;
                }
                return new AnonymousClass3IM((String[]) A0l.toArray(new String[0]), i2 - i, s);
            }
            throw C12970iu.A0f("insufficient data");
        }
        throw C12980iv.A0n("bytes may not be null");
    }

    public static AnonymousClass3IM A01(String[] strArr) {
        int i = 0;
        for (String str : strArr) {
            try {
                int length = str.getBytes(AnonymousClass01V.A08).length;
                if (length <= 63) {
                    i += length + 1;
                } else {
                    throw C12960it.A0U("token may not be longer than 63 bytes");
                }
            } catch (UnsupportedEncodingException e) {
                throw new Error(e);
            }
        }
        return new AnonymousClass3IM(strArr, i + 1, 0);
    }

    public void A02(OutputStream outputStream) {
        for (String str : this.A02) {
            byte[] bytes = str.getBytes(AnonymousClass01V.A08);
            int length = bytes.length;
            if (length <= 63) {
                outputStream.write(length);
                outputStream.write(bytes);
            } else {
                throw C12960it.A0U("token may not be longer than 63 bytes");
            }
        }
        short s = this.A01;
        if (s != 0) {
            outputStream.write(((s >>> 8) & 63) | 192);
            outputStream.write(s & 255);
            return;
        }
        outputStream.write(0);
    }
}
