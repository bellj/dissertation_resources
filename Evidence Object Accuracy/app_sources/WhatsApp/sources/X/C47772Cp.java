package X;

import com.facebook.msys.mci.Execution;

/* renamed from: X.2Cp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47772Cp extends ThreadLocal {
    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        Execution.assertInitialized("execution_initial_value");
        return Integer.valueOf(Execution.nativeGetExecutionContext());
    }
}
