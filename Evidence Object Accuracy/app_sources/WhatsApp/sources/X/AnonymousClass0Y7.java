package X;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

/* renamed from: X.0Y7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Y7 implements AnonymousClass07F {
    public final /* synthetic */ CoordinatorLayout A00;

    public AnonymousClass0Y7(CoordinatorLayout coordinatorLayout) {
        this.A00 = coordinatorLayout;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0014, code lost:
        if (r8.A06() <= 0) goto L_0x0016;
     */
    @Override // X.AnonymousClass07F
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C018408o AMH(android.view.View r7, X.C018408o r8) {
        /*
            r6 = this;
            androidx.coordinatorlayout.widget.CoordinatorLayout r5 = r6.A00
            X.08o r0 = r5.A06
            boolean r0 = X.C015407h.A01(r0, r8)
            if (r0 != 0) goto L_0x0050
            r5.A06 = r8
            r2 = 1
            if (r8 == 0) goto L_0x0016
            int r1 = r8.A06()
            r0 = 1
            if (r1 > 0) goto L_0x0017
        L_0x0016:
            r0 = 0
        L_0x0017:
            r5.A08 = r0
            if (r0 != 0) goto L_0x0054
            android.graphics.drawable.Drawable r0 = r5.getBackground()
            if (r0 != 0) goto L_0x0054
        L_0x0021:
            r5.setWillNotDraw(r2)
            X.0St r4 = r8.A00
            boolean r0 = r4.A0E()
            if (r0 != 0) goto L_0x004d
            r3 = 0
            int r2 = r5.getChildCount()
        L_0x0031:
            if (r3 >= r2) goto L_0x004d
            android.view.View r1 = r5.getChildAt(r3)
            boolean r0 = r1.getFitsSystemWindows()
            if (r0 == 0) goto L_0x0051
            android.view.ViewGroup$LayoutParams r0 = r1.getLayoutParams()
            X.0B5 r0 = (X.AnonymousClass0B5) r0
            X.04r r0 = r0.A0A
            if (r0 == 0) goto L_0x0051
            boolean r0 = r4.A0E()
            if (r0 == 0) goto L_0x0051
        L_0x004d:
            r5.requestLayout()
        L_0x0050:
            return r8
        L_0x0051:
            int r3 = r3 + 1
            goto L_0x0031
        L_0x0054:
            r2 = 0
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Y7.AMH(android.view.View, X.08o):X.08o");
    }
}
