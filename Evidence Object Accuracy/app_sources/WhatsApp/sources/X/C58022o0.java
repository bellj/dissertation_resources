package X;

import android.view.animation.Animation;
import com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment;
import com.whatsapp.contact.picker.SharedTextPreviewDialogFragment;

/* renamed from: X.2o0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58022o0 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ SharedTextPreviewDialogFragment A00;

    public C58022o0(SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment) {
        this.A00 = sharedTextPreviewDialogFragment;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = this.A00;
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0G.setVisibility(8);
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A02.setVisibility(8);
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A01.setVisibility(8);
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0G = null;
        sharedTextPreviewDialogFragment.A1K();
        sharedTextPreviewDialogFragment.A1L();
        sharedTextPreviewDialogFragment.A0N = false;
    }
}
