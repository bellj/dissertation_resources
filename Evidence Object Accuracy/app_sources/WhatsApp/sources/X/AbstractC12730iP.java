package X;

/* renamed from: X.0iP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12730iP {
    void A5c(AnonymousClass0QC v, float f, boolean z);

    boolean A7d(AnonymousClass0QC v);

    void A95(float f);

    float AAK(AnonymousClass0QC v);

    int ACD();

    AnonymousClass0QC AHV(int i);

    float AHW(int i);

    void AJ2();

    void AZg(AnonymousClass0QC v, float f);

    float AaE(AnonymousClass0QC v, boolean z);

    float Afd(C07240Xf v, boolean z);

    void clear();
}
