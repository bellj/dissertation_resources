package X;

import com.whatsapp.stickers.StickerStoreActivity;

/* renamed from: X.51G  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass51G implements AbstractC467627l {
    public final /* synthetic */ StickerStoreActivity A00;

    @Override // X.AbstractC467727m
    public void AXN(AnonymousClass3FN r1) {
    }

    public AnonymousClass51G(StickerStoreActivity stickerStoreActivity) {
        this.A00 = stickerStoreActivity;
    }

    @Override // X.AbstractC467727m
    public void AXO(AnonymousClass3FN r3) {
        this.A00.A01.setCurrentItem(r3.A00);
    }
}
