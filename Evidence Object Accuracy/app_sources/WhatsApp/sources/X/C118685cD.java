package X;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.payments.ui.NoviPayHubTransactionHistoryActivity;

/* renamed from: X.5cD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118685cD extends AbstractC05270Ox {
    public final /* synthetic */ LinearLayoutManager A00;
    public final /* synthetic */ NoviPayHubTransactionHistoryActivity A01;

    public C118685cD(LinearLayoutManager linearLayoutManager, NoviPayHubTransactionHistoryActivity noviPayHubTransactionHistoryActivity) {
        this.A01 = noviPayHubTransactionHistoryActivity;
        this.A00 = linearLayoutManager;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        NoviPayHubTransactionHistoryActivity noviPayHubTransactionHistoryActivity = this.A01;
        if (!noviPayHubTransactionHistoryActivity.A0C.get() && this.A00.A1B() == noviPayHubTransactionHistoryActivity.A08.A0D() - 1) {
            noviPayHubTransactionHistoryActivity.A2e();
        }
    }
}
