package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.SystemClock;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.concurrent.Semaphore;

/* renamed from: X.03N  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass03N {
    public static long A00;
    public static BroadcastReceiver A01;
    public static Context A02;
    public static String A03;
    public static String A04;
    public static final AnonymousClass03O A05;
    public static final AnonymousClass03O A06 = new AnonymousClass03O("https://maps.instagram.com/maps/tile/?", "https://maps.instagram.com/maps/static/?", null, null, null, Integer.MAX_VALUE);
    public static final AnonymousClass03O A07 = new AnonymousClass03O("https://expresswifi.com/maps/tile/?", "https://expresswifi.com/maps/static/?", null, null, null, Integer.MAX_VALUE);
    public static final List A08 = new LinkedList();
    public static final Semaphore A09 = new Semaphore(1);
    public static volatile AnonymousClass03O A0A;
    public static volatile String A0B;
    public static volatile String A0C = "https://graph.facebook.com/v2.2/maps_configs?fields=base_url,static_base_url,osm_config,url_override_config&pretty=0&access_token=";

    static {
        AnonymousClass03O r0 = new AnonymousClass03O("https://www.facebook.com/maps/tile/?", "https://www.facebook.com/maps/static/?", null, null, null, Integer.MAX_VALUE);
        A05 = r0;
        A0A = r0;
        A01();
    }

    public static void A00() {
        if (A0B != null && A02 != null) {
            Semaphore semaphore = A09;
            if (semaphore.tryAcquire()) {
                long j = A00;
                if (j == 0 || SystemClock.uptimeMillis() - j >= 3600000) {
                    AnonymousClass0UE.A02(new AnonymousClass0IP(), "MapConfigUpdateDispatchable");
                } else {
                    semaphore.release();
                }
            }
        }
    }

    public static void A01() {
        String str;
        String str2;
        String language = Locale.getDefault().getLanguage();
        if (language.length() == 2) {
            String country = Locale.getDefault().getCountry();
            StringBuilder sb = new StringBuilder();
            sb.append(language);
            if (country.length() == 2) {
                StringBuilder sb2 = new StringBuilder("_");
                sb2.append(country);
                str2 = sb2.toString();
            } else {
                str2 = "";
            }
            sb.append(str2);
            str = sb.toString();
        } else {
            str = "en";
        }
        A03 = str;
        A04 = str.toLowerCase(Locale.US);
        try {
            Locale.getDefault().getISO3Language();
        } catch (MissingResourceException unused) {
        }
    }
}
