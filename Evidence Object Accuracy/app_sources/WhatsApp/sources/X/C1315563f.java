package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* renamed from: X.63f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315563f implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(40);
    public final CharSequence A00;
    public final CharSequence A01;
    public final CharSequence A02;
    public final String A03;
    public final String A04;
    public final String A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315563f(Parcel parcel) {
        this.A01 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A00 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A02 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A04 = parcel.readString();
        this.A05 = parcel.readString();
        this.A03 = parcel.readString();
    }

    public C1315563f(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, String str, String str2, String str3) {
        this.A01 = charSequence2;
        this.A02 = charSequence3;
        this.A00 = charSequence;
        this.A04 = str2;
        this.A05 = str3;
        this.A03 = str;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        TextUtils.writeToParcel(this.A01, parcel, i);
        TextUtils.writeToParcel(this.A02, parcel, i);
        TextUtils.writeToParcel(this.A00, parcel, i);
        parcel.writeString(this.A04);
        parcel.writeString(this.A05);
        parcel.writeString(this.A03);
    }
}
