package X;

import com.whatsapp.stickers.AddThirdPartyStickerPackActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.38X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38X extends AbstractC16350or {
    public AddThirdPartyStickerPackActivity.AddStickerPackDialogFragment A00;
    public final C16120oU A01;
    public final AnonymousClass2I1 A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final WeakReference A06;

    public AnonymousClass38X(ActivityC000900k r2, C16120oU r3, AnonymousClass2I1 r4, String str, String str2, String str3) {
        this.A01 = r3;
        this.A04 = str;
        this.A03 = str2;
        this.A05 = str3;
        this.A02 = r4;
        this.A06 = C12970iu.A10(r2);
    }
}
