package X;

/* renamed from: X.4co  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94954co {
    public static final AnonymousClass1TN[] A03 = new AnonymousClass1TN[0];
    public int A00;
    public boolean A01;
    public AnonymousClass1TN[] A02;

    public static AnonymousClass1TN[] A04(AnonymousClass1TN[] r2) {
        return r2.length < 1 ? A03 : (AnonymousClass1TN[]) r2.clone();
    }

    public AnonymousClass1TN[] A07() {
        int i = this.A00;
        if (i == 0) {
            return A03;
        }
        AnonymousClass1TN[] r2 = this.A02;
        if (r2.length == i) {
            this.A01 = true;
            return r2;
        }
        AnonymousClass1TN[] r1 = new AnonymousClass1TN[i];
        System.arraycopy(r2, 0, r1, 0, i);
        return r1;
    }

    public C94954co(int i) {
        if (i >= 0) {
            this.A02 = i == 0 ? A03 : new AnonymousClass1TN[i];
            this.A00 = 0;
            this.A01 = false;
            return;
        }
        throw C12970iu.A0f("'initialCapacity' must not be negative");
    }

    public static C94954co A00() {
        return new C94954co(2);
    }

    public static AnonymousClass5NZ A01(AnonymousClass1TN r0, C94954co r1) {
        r1.A06(r0);
        return new AnonymousClass5NZ(r1);
    }

    public static void A02(AnonymousClass1TN r1, C94954co r2, int i, boolean z) {
        r2.A06(new C114835Ng(r1, i, z));
    }

    public static void A03(AnonymousClass1TN r1, C94954co r2, boolean z) {
        r2.A06(new C114835Ng(r1, z ? 1 : 0, z));
    }

    public AnonymousClass1TN A05(int i) {
        int i2 = this.A00;
        if (i < i2) {
            return this.A02[i];
        }
        StringBuilder A0h = C12960it.A0h();
        A0h.append(i);
        throw new ArrayIndexOutOfBoundsException(C12960it.A0e(" >= ", A0h, i2));
    }

    public void A06(AnonymousClass1TN r8) {
        if (r8 != null) {
            AnonymousClass1TN[] r6 = this.A02;
            AnonymousClass1TN[] r5 = r6;
            int length = r6.length;
            int i = this.A00;
            boolean z = true;
            int i2 = i + 1;
            if (i2 <= length) {
                z = false;
            }
            if (this.A01 || z) {
                r5 = new AnonymousClass1TN[Math.max(length, (i2 >> 1) + i2)];
                System.arraycopy(r6, 0, r5, 0, i);
                this.A02 = r5;
                this.A01 = false;
            }
            r5[this.A00] = r8;
            this.A00 = i2;
            return;
        }
        throw C12980iv.A0n("'element' cannot be null");
    }
}
