package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.penmode.PenModeView;

/* renamed from: X.3Cc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63573Cc {
    public final /* synthetic */ C47322Ae A00;
    public final /* synthetic */ DialogC51702Ye A01;

    public C63573Cc(C47322Ae r1, DialogC51702Ye r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(int i) {
        PenModeView penModeView = this.A01.A08;
        for (View view : penModeView.A01) {
            view.setBackground(null);
        }
        int i2 = R.id.pen_mode_thin;
        if (i != 1) {
            i2 = R.id.pen_mode_medium;
            if (i != 2) {
                i2 = R.id.pen_mode_thick;
                if (i != 3) {
                    if (i == 4) {
                        i2 = R.id.pen_mode_blur;
                    } else {
                        return;
                    }
                }
            }
        }
        AnonymousClass028.A0D(penModeView, i2).setBackgroundResource(R.drawable.pen_mode_selected_background);
    }
}
