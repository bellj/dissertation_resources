package X;

/* renamed from: X.5sf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126305sf {
    public final AnonymousClass1V8 A00;

    public C126305sf(C126315sg r11, AnonymousClass3CT r12, String str, String str2, String str3) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-select-otp-verification-method");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 1000, false)) {
            C41141sy.A01(A0N, "identifier", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 1000, false)) {
            C41141sy.A01(A0N, "nonce", str3);
        }
        if (r11 != null) {
            A0N.A05(r11.A00);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r12);
    }
}
