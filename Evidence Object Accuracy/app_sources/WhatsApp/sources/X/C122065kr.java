package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.5kr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122065kr extends C125965s6 {
    public final long A00;
    public final Drawable A01;
    public final AbstractC14640lm A02;
    public final AbstractC136536Mx A03;
    public final AbstractC16390ow A04;
    public final String A05;
    public final String A06;
    public final boolean A07;
    public final boolean A08;

    public C122065kr(Drawable drawable, AbstractC14640lm r3, AbstractC136536Mx r4, AbstractC16390ow r5, String str, String str2, long j, boolean z, boolean z2) {
        super(5);
        this.A07 = z;
        this.A01 = drawable;
        this.A08 = z2;
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
        this.A06 = str;
        this.A00 = j;
        this.A05 = str2;
    }
}
