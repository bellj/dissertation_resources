package X;

import java.io.Serializable;

/* renamed from: X.5Eq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112765Eq implements AnonymousClass5X4, Serializable {
    public final AnonymousClass5ZU element;
    public final AnonymousClass5X4 left;

    public C112765Eq(AnonymousClass5ZU r1, AnonymousClass5X4 r2) {
        C16700pc.A0G(r2, r1);
        this.left = r2;
        this.element = r1;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C112765Eq)) {
            return false;
        }
        C112765Eq r6 = (C112765Eq) obj;
        int i = 2;
        C112765Eq r1 = r6;
        while (true) {
            AnonymousClass5X4 r12 = r1.left;
            if (!(r12 instanceof C112765Eq) || (r1 = (C112765Eq) r12) == null) {
                break;
            }
            i++;
        }
        C112765Eq r13 = this;
        int i2 = 2;
        C112765Eq r2 = this;
        while (true) {
            AnonymousClass5X4 r22 = r2.left;
            if (!(r22 instanceof C112765Eq) || (r2 = (C112765Eq) r22) == null) {
                break;
            }
            i2++;
        }
        if (i != i2) {
            return false;
        }
        while (true) {
            AnonymousClass5ZU r23 = r13.element;
            if (!C16700pc.A0O(r6.get(r23.getKey()), r23)) {
                return false;
            }
            AnonymousClass5X4 r14 = r13.left;
            if (r14 instanceof C112765Eq) {
                r13 = (C112765Eq) r14;
            } else {
                AnonymousClass5ZU r15 = (AnonymousClass5ZU) r14;
                return C16700pc.A0O(r6.get(r15.getKey()), r15);
            }
        }
    }

    @Override // X.AnonymousClass5X4
    public Object fold(Object obj, AnonymousClass5ZQ r4) {
        C16700pc.A0E(r4, 1);
        return r4.AJ5(this.left.fold(obj, r4), this.element);
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5ZU get(AbstractC115495Rt r3) {
        C16700pc.A0E(r3, 0);
        C112765Eq r1 = this;
        while (true) {
            AnonymousClass5ZU r0 = r1.element.get(r3);
            if (r0 != null) {
                return r0;
            }
            AnonymousClass5X4 r12 = r1.left;
            if (!(r12 instanceof C112765Eq)) {
                return r12.get(r3);
            }
            r1 = (C112765Eq) r12;
        }
    }

    @Override // java.lang.Object
    public int hashCode() {
        return C12990iw.A08(this.element, this.left.hashCode());
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 minusKey(AbstractC115495Rt r4) {
        C16700pc.A0E(r4, 0);
        if (this.element.get(r4) != null) {
            return this.left;
        }
        AnonymousClass5X4 minusKey = this.left.minusKey(r4);
        if (minusKey == this.left) {
            return this;
        }
        if (minusKey == C112775Er.A00) {
            return this.element;
        }
        return new C112765Eq(this.element, minusKey);
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 plus(AnonymousClass5X4 r3) {
        C16700pc.A0E(r3, 1);
        if (r3 != C112775Er.A00) {
            return (AnonymousClass5X4) r3.fold(this, new AnonymousClass5KL());
        }
        return this;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("[");
        A0k.append((String) fold("", new AnonymousClass5KK()));
        return C72453ed.A0t(A0k);
    }

    private final Object writeReplace() {
        int i = 2;
        C112765Eq r1 = this;
        while (true) {
            AnonymousClass5X4 r12 = r1.left;
            if (!(r12 instanceof C112765Eq) || (r1 = (C112765Eq) r12) == null) {
                break;
            }
            i++;
        }
        AnonymousClass5X4[] r3 = new AnonymousClass5X4[i];
        AnonymousClass5BP r2 = new AnonymousClass5BP();
        fold(AnonymousClass1WZ.A00, new AnonymousClass5KV(r2, r3));
        if (r2.element == i) {
            return new AnonymousClass5BT(r3);
        }
        throw C12960it.A0U("Check failed.");
    }
}
