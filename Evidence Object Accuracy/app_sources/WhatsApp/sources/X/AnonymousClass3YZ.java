package X;

import com.whatsapp.migration.export.ui.ExportMigrationViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.3YZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YZ implements AnonymousClass2F5 {
    public final /* synthetic */ ExportMigrationViewModel A00;

    public /* synthetic */ AnonymousClass3YZ(ExportMigrationViewModel exportMigrationViewModel) {
        this.A00 = exportMigrationViewModel;
    }

    @Override // X.AnonymousClass2F5
    public void ANg() {
        this.A00.A04(0);
    }

    @Override // X.AnonymousClass2F5
    public void ANh() {
        this.A00.A04(5);
    }

    @Override // X.AnonymousClass2F5
    public void AOL() {
        this.A00.A04(2);
    }

    @Override // X.AnonymousClass2F5
    public void APl(int i) {
        ExportMigrationViewModel exportMigrationViewModel = this.A00;
        Log.i(C12960it.A0W(1, "ExportMigrationViewModel/setErrorCode: "));
        Integer num = 1;
        AnonymousClass016 r1 = exportMigrationViewModel.A00;
        if (!num.equals(r1.A01())) {
            r1.A0A(num);
        }
    }

    @Override // X.AnonymousClass2F5
    public void AQ5() {
        this.A00.A04(1);
    }

    @Override // X.AnonymousClass2F5
    public void AUL(int i) {
        ExportMigrationViewModel exportMigrationViewModel = this.A00;
        Integer valueOf = Integer.valueOf(i);
        AnonymousClass016 r1 = exportMigrationViewModel.A01;
        if (!C29941Vi.A00(valueOf, r1.A01())) {
            if (i > 100) {
                i = 100;
            } else if (i < 0) {
                i = 0;
            }
            C12970iu.A1Q(r1, i);
        }
    }
}
