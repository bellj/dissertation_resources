package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import org.npci.commonlibrary.ATMPinFragment;

/* renamed from: X.64f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class View$OnClickListenerC1318164f implements View.OnClickListener {
    public final /* synthetic */ Drawable A00;
    public final /* synthetic */ Drawable A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ ATMPinFragment A04;
    public final /* synthetic */ AbstractC136546My A05;

    public View$OnClickListenerC1318164f(Drawable drawable, Drawable drawable2, String str, String str2, ATMPinFragment aTMPinFragment, AbstractC136546My r6) {
        this.A04 = aTMPinFragment;
        this.A05 = r6;
        this.A02 = str;
        this.A03 = str2;
        this.A00 = drawable;
        this.A01 = drawable2;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        String str;
        Drawable drawable;
        AbstractC136546My r1 = this.A05;
        if (r1.Af0()) {
            str = this.A02;
            drawable = this.A00;
        } else {
            str = this.A03;
            drawable = this.A01;
        }
        r1.Aez(drawable, this, str, 0, true, true);
    }
}
