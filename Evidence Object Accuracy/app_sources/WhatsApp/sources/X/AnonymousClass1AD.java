package X;

/* renamed from: X.1AD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AD {
    public final AbstractC15710nm A00;
    public final C15450nH A01;
    public final AnonymousClass01d A02;
    public final C14820m6 A03;
    public final AnonymousClass018 A04;
    public final AnonymousClass1AC A05;
    public final AnonymousClass19M A06;
    public final C231510o A07;
    public final AnonymousClass193 A08;
    public final C14850m9 A09;
    public final C16120oU A0A;
    public final C253719d A0B;
    public final AbstractC253919f A0C;
    public final C16630pM A0D;
    public final C22210yi A0E;
    public final AnonymousClass15H A0F;
    public final AnonymousClass1AB A0G;
    public final AnonymousClass146 A0H;
    public final C235512c A0I;
    public final C255519v A0J;
    public final C255619w A0K;
    public final C252718t A0L;
    public final AbstractC14440lR A0M;

    public AnonymousClass1AD(AbstractC15710nm r2, C15450nH r3, AnonymousClass01d r4, C14820m6 r5, AnonymousClass018 r6, AnonymousClass1AC r7, AnonymousClass19M r8, C231510o r9, AnonymousClass193 r10, C14850m9 r11, C16120oU r12, C253719d r13, AbstractC253919f r14, C16630pM r15, C22210yi r16, AnonymousClass15H r17, AnonymousClass1AB r18, AnonymousClass146 r19, C235512c r20, C255519v r21, C255619w r22, C252718t r23, AbstractC14440lR r24) {
        this.A09 = r11;
        this.A0B = r13;
        this.A0L = r23;
        this.A00 = r2;
        this.A0M = r24;
        this.A0A = r12;
        this.A06 = r8;
        this.A01 = r3;
        this.A07 = r9;
        this.A0E = r16;
        this.A02 = r4;
        this.A04 = r6;
        this.A0H = r19;
        this.A0C = r14;
        this.A0I = r20;
        this.A08 = r10;
        this.A03 = r5;
        this.A0G = r18;
        this.A0F = r17;
        this.A0K = r22;
        this.A0D = r15;
        this.A05 = r7;
        this.A0J = r21;
    }

    public AnonymousClass4TX A00() {
        return new AnonymousClass4TX(this.A05, this.A06, this.A07, this.A08);
    }

    public AnonymousClass2B7 A01(AnonymousClass1BQ r12, AnonymousClass1KT r13) {
        C22210yi r2 = this.A0E;
        AnonymousClass146 r5 = this.A0H;
        C235512c r6 = this.A0I;
        AnonymousClass1AC r1 = this.A05;
        AnonymousClass1AB r4 = this.A0G;
        return new AnonymousClass2B7(r1, r2, this.A0F, r4, r5, r6, this.A0J, r12, this.A0K, r13);
    }
}
