package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.whatsapp.R;
import java.util.AbstractMap;

/* renamed from: X.0l0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14190l0 {
    public boolean A00;
    public boolean A01;
    public final long A02;
    public final Handler A03 = new Handler(Looper.getMainLooper());
    public final AnonymousClass010 A04;
    public final Runnable A05;
    public final boolean A06;

    public C14190l0(AnonymousClass010 r3, long j, boolean z) {
        this.A02 = j;
        this.A06 = z;
        this.A00 = false;
        this.A01 = true;
        this.A04 = r3;
        this.A05 = new RunnableBRunnable0Shape0S0100000_I0(this, 15);
    }

    public static C14190l0 A00(AnonymousClass010 r5, long j, boolean z) {
        C14190l0 r4 = new C14190l0(r5, j, z);
        r4.A01 = false;
        r4.A03.postDelayed(r4.A05, r4.A02);
        return r4;
    }

    public void A01() {
        this.A00 = true;
        this.A03.removeCallbacks(this.A05);
        AnonymousClass010 r1 = this.A04;
        C14260l7 r2 = (C14260l7) r1.A04.get();
        if (r2 != null) {
            ((AbstractMap) r2.A02(R.id.bk_context_key_timers)).remove(r1.A03);
        }
    }

    public void A02() {
        if (!this.A00) {
            if (!this.A01) {
                A03();
            }
            this.A01 = false;
            this.A03.postDelayed(this.A05, this.A02);
        }
    }

    public void A03() {
        this.A01 = true;
        this.A03.removeCallbacks(this.A05);
    }
}
