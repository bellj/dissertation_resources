package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.4YR  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4YR {
    public int A00 = -1;
    public AnonymousClass4YR A01;
    public AnonymousClass4YR A02;
    public Boolean A03 = null;
    public Boolean A04 = null;

    public AnonymousClass4YR A00() {
        AnonymousClass4YR r0 = this.A01;
        if (r0 != null) {
            return r0;
        }
        throw C12960it.A0U("Current path token is a leaf");
    }

    public String A01() {
        if (this instanceof C83113wi) {
            return "[*]";
        }
        if (this instanceof C83163wn) {
            return "..";
        }
        if (this instanceof C83143wl) {
            return ((C83143wl) this).A02;
        }
        if (this instanceof C83123wj) {
            C83123wj r1 = (C83123wj) this;
            StringBuilder A0k = C12960it.A0k("[");
            A0k.append(C95094d8.A00(r1.A01, ",", r1.A00));
            return C12960it.A0d("]", A0k);
        } else if (this instanceof C83153wm) {
            C83153wm r3 = (C83153wm) this;
            StringBuilder A0k2 = C12960it.A0k("[");
            for (int i = 0; i < r3.A00.size(); i++) {
                if (i != 0) {
                    A0k2.append(",");
                }
                A0k2.append("?");
            }
            return C12960it.A0d("]", A0k2);
        } else if (this instanceof C83133wk) {
            return C12960it.A0d(((C83133wk) this).A02, C12960it.A0k("."));
        } else if (!(this instanceof C83093wg)) {
            return ((C83083wf) this).A00.toString();
        } else {
            return ((C83093wg) this).A00.toString();
        }
    }

    public void A02(AbstractC111885Be r15, C94394bk r16, Object obj, String str) {
        AnonymousClass5TA r0;
        String A0s;
        AbstractC111885Be r9 = r15;
        if (this instanceof C83113wi) {
            C92364Vp r5 = r16.A01;
            AbstractC117035Xw r4 = r5.A00;
            if (obj instanceof Map) {
                for (Object obj2 : r4.AFy(obj)) {
                    A04(r16, obj, str, Collections.singletonList(obj2));
                }
            } else if (obj instanceof List) {
                for (int i = 0; i < r4.AKQ(obj); i++) {
                    try {
                        A03(r16, obj, str, i);
                    } catch (C82803wD e) {
                        if (r5.A03.contains(EnumC87084Ad.REQUIRE_PROPERTIES)) {
                            throw e;
                        }
                    }
                }
            }
        } else if (this instanceof C83163wn) {
            AnonymousClass4YR A00 = A00();
            if (A00 instanceof C83123wj) {
                r0 = new AnonymousClass52K(r16, A00);
            } else if (A00 instanceof AbstractC83103wh) {
                r0 = new AnonymousClass52I(r16);
            } else if (A00 instanceof C83113wi) {
                r0 = new AnonymousClass52H();
            } else if (A00 instanceof C83153wm) {
                r0 = new AnonymousClass52J(r16, A00);
            } else {
                r0 = C83163wn.A00;
            }
            C83163wn.A00(r15, r16, A00, r0, obj, str);
        } else if (this instanceof C83143wl) {
            C83143wl r2 = (C83143wl) this;
            if (((AnonymousClass4YR) r2).A01 == null) {
                if (!r16.A08) {
                    r9 = AbstractC111885Be.A01;
                }
                r16.A02(r9, obj, r2.A02);
                return;
            }
            r2.A00().A02(r15, r16, obj, r2.A02);
        } else if (this instanceof C83123wj) {
            C83123wj r42 = (C83123wj) this;
            C92364Vp r1 = r16.A01;
            AbstractC117035Xw r6 = r1.A00;
            if (obj instanceof Map) {
                List<Object> list = r42.A01;
                if (list.size() == 1 || (((AnonymousClass4YR) r42).A01 == null && list.size() > 1)) {
                    r42.A04(r16, obj, str, list);
                    return;
                }
                ArrayList A0w = C12980iv.A0w(1);
                A0w.add(null);
                for (Object obj3 : list) {
                    A0w.set(0, obj3);
                    r42.A04(r16, obj, str, A0w);
                }
            } else if (r42.A07() && !r1.A03.contains(EnumC87084Ad.SUPPRESS_EXCEPTIONS)) {
                if (obj == null) {
                    A0s = "null";
                } else {
                    A0s = C12980iv.A0s(obj);
                }
                throw new C82803wD(String.format("Expected to find an object with property %s in path %s but found '%s'. This is not a json object according to the JsonProvider: '%s'.", r42.A01(), str, A0s, C12980iv.A0s(r6)));
            }
        } else if (this instanceof C83153wm) {
            C83153wm r43 = (C83153wm) this;
            C92364Vp r52 = r16.A01;
            AbstractC117035Xw r12 = r52.A00;
            if (!(obj instanceof Map)) {
                int i2 = 0;
                if (obj instanceof List) {
                    for (Object obj4 : r12.Aet(obj)) {
                        if (r43.A08(r52, r16, obj4, r16.A04)) {
                            r43.A03(r16, obj, str, i2);
                        }
                        i2++;
                    }
                } else if (r43.A07()) {
                    Object[] A1a = C12980iv.A1a();
                    A1a[0] = r43.toString();
                    A1a[1] = obj;
                    throw C82843wH.A00(String.format("Filter: %s can not be applied to primitives. Current context is: %s", A1a));
                }
            } else if (r43.A08(r52, r16, obj, r16.A04)) {
                if (!r16.A08) {
                    r9 = AbstractC111885Be.A01;
                }
                if (r43.A01 == null) {
                    r16.A02(r9, obj, str);
                } else {
                    r43.A00().A02(r9, r16, obj, str);
                }
            }
        } else if (this instanceof C83133wk) {
            C83133wk r44 = (C83133wk) this;
            String str2 = r44.A01;
            Class cls = (Class) AnonymousClass4G1.A00.get(str2);
            if (cls != null) {
                try {
                    AnonymousClass5T8 r8 = (AnonymousClass5T8) cls.newInstance();
                    List<C95264dP> list2 = r44.A00;
                    if (list2 != null) {
                        for (C95264dP r22 : list2) {
                            switch (r22.A00.ordinal()) {
                                case 0:
                                    if (!r22.A03.booleanValue()) {
                                        r22.A01 = new AnonymousClass52E(r22, r16.A01.A00);
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 1:
                                    AnonymousClass52F r13 = new AnonymousClass52F(r16.A01, r22.A02, r16.A04);
                                    if (!r22.A03.booleanValue() || !r13.equals(r22.A01)) {
                                        r22.A01 = r13;
                                        break;
                                    } else {
                                        continue;
                                    }
                                    break;
                            }
                            r22.A03 = Boolean.TRUE;
                        }
                    }
                    Object AJ9 = r8.AJ9(r9, r16, obj, str, r44.A00);
                    StringBuilder A0j = C12960it.A0j(str);
                    A0j.append(".");
                    r16.A02(r15, AJ9, C12960it.A0d(str2, A0j));
                    if (((AnonymousClass4YR) r44).A01 != null) {
                        r44.A00().A02(r15, r16, AJ9, str);
                    }
                } catch (Exception e2) {
                    StringBuilder A0k = C12960it.A0k("Function of name: ");
                    A0k.append(str2);
                    throw new C82843wH(C12960it.A0d(" cannot be created", A0k), e2);
                }
            } else {
                StringBuilder A0k2 = C12960it.A0k("Function with name: ");
                A0k2.append(str2);
                throw C82843wH.A00(C12960it.A0d(" does not exist.", A0k2));
            }
        } else if (!(this instanceof C83093wg)) {
            C83083wf r3 = (C83083wf) this;
            if (r3.A08(r16, obj, str)) {
                List<Object> list3 = r3.A00.A00;
                if (list3.size() == 1) {
                    r3.A03(r16, obj, str, C72453ed.A0G(list3, 0));
                    return;
                }
                for (Object obj5 : list3) {
                    r3.A03(r16, obj, str, C12960it.A05(obj5));
                }
            }
        } else {
            C83093wg r23 = (C83093wg) this;
            if (r23.A08(r16, obj, str)) {
                AnonymousClass4VY r45 = r23.A00;
                switch (r45.A00.ordinal()) {
                    case 0:
                        int AKQ = r16.A01.A00.AKQ(obj);
                        int intValue = r45.A01.intValue();
                        if (intValue < 0) {
                            intValue += AKQ;
                        }
                        int max = Math.max(0, intValue);
                        r23.toString();
                        if (AKQ != 0 && max < AKQ) {
                            while (max < AKQ) {
                                r23.A03(r16, obj, str, max);
                                max++;
                            }
                            return;
                        }
                        return;
                    case 1:
                        int AKQ2 = r16.A01.A00.AKQ(obj);
                        if (AKQ2 != 0) {
                            int intValue2 = r45.A02.intValue();
                            if (intValue2 < 0) {
                                intValue2 += AKQ2;
                            }
                            int min = Math.min(AKQ2, intValue2);
                            r23.toString();
                            for (int i3 = 0; i3 < min; i3++) {
                                r23.A03(r16, obj, str, i3);
                            }
                            return;
                        }
                        return;
                    case 2:
                        int AKQ3 = r16.A01.A00.AKQ(obj);
                        int intValue3 = r45.A01.intValue();
                        int min2 = Math.min(AKQ3, r45.A02.intValue());
                        if (intValue3 < min2 && AKQ3 != 0) {
                            r23.toString();
                            while (intValue3 < min2) {
                                r23.A03(r16, obj, str, intValue3);
                                intValue3++;
                            }
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void A03(C94394bk r5, Object obj, String str, int i) {
        AbstractC111885Be r2;
        String A02 = C95094d8.A02(str, "[", String.valueOf(i), "]");
        if (r5.A08) {
            r2 = new C82893wM(obj, i);
        } else {
            r2 = AbstractC111885Be.A01;
        }
        if (i < 0) {
            i += r5.A01.A00.AKQ(obj);
        }
        try {
            Object obj2 = ((List) obj).get(i);
            if (this.A01 == null) {
                r5.A02(r2, obj2, A02);
            } else {
                A00().A02(r2, r5, obj2, A02);
            }
        } catch (IndexOutOfBoundsException unused) {
        }
    }

    public void A04(C94394bk r11, Object obj, String str, List list) {
        AbstractC111885Be r0;
        Object obj2;
        Object obj3;
        AbstractC111885Be r3;
        Object obj4 = null;
        if (list.size() == 1) {
            String A0g = C12960it.A0g(list, 0);
            String A02 = C95094d8.A02(str, "['", A0g, "']");
            Map map = (Map) obj;
            if (!map.containsKey(A0g)) {
                obj3 = AbstractC117035Xw.A00;
            } else {
                obj3 = map.get(A0g);
            }
            if (obj3 != AbstractC117035Xw.A00) {
                obj4 = obj3;
            } else if (this.A01 == null) {
                Set set = r11.A01.A03;
                if (!set.contains(EnumC87084Ad.DEFAULT_PATH_LEAF_TO_NULL)) {
                    if (!set.contains(EnumC87084Ad.SUPPRESS_EXCEPTIONS) && set.contains(EnumC87084Ad.REQUIRE_PROPERTIES)) {
                        throw new C82803wD(C12960it.A0d(A02, C12960it.A0k("No results for path: ")));
                    }
                    return;
                }
            } else if (((A07() && A06()) || r11.A01.A03.contains(EnumC87084Ad.REQUIRE_PROPERTIES)) && !r11.A01.A03.contains(EnumC87084Ad.SUPPRESS_EXCEPTIONS)) {
                throw new C82803wD(C12960it.A0d(A02, C12960it.A0j("Missing property in path ")));
            } else {
                return;
            }
            if (r11.A08) {
                r3 = new C82883wL(obj, A0g);
            } else {
                r3 = AbstractC111885Be.A01;
            }
            if (this.A01 == null) {
                StringBuilder A0j = C12960it.A0j("[");
                A0j.append(String.valueOf(this.A00));
                String A0d = C12960it.A0d("]", A0j);
                if (A0d.equals("[-1]") || r11.A02.A00.A01.A02.A01().equals(A0d)) {
                    r11.A02(r3, obj4, A02);
                    return;
                }
                return;
            }
            A00().A02(r3, r11, obj4, A02);
            return;
        }
        StringBuilder A0j2 = C12960it.A0j(str);
        A0j2.append("[");
        A0j2.append(C95094d8.A00(list, ", ", "'"));
        String A0d2 = C12960it.A0d("]", A0j2);
        C92364Vp r6 = r11.A01;
        AbstractC117035Xw r4 = r6.A00;
        Object A022 = ((AnonymousClass52M) r4).A01.A02();
        for (Object obj5 : list) {
            if (r4.AFy(obj).contains(obj5)) {
                Map map2 = (Map) obj;
                if (!map2.containsKey(obj5)) {
                    obj2 = AbstractC117035Xw.A00;
                } else {
                    obj2 = map2.get(obj5);
                }
                if (obj2 == AbstractC117035Xw.A00) {
                    if (r6.A03.contains(EnumC87084Ad.DEFAULT_PATH_LEAF_TO_NULL)) {
                        obj2 = null;
                    }
                }
            } else {
                Set set2 = r6.A03;
                if (set2.contains(EnumC87084Ad.DEFAULT_PATH_LEAF_TO_NULL)) {
                    obj2 = null;
                } else if (set2.contains(EnumC87084Ad.REQUIRE_PROPERTIES)) {
                    throw new C82803wD(C12960it.A0d(A0d2, C12960it.A0j("Missing property in path ")));
                }
            }
            r4.Acf(A022, obj5, obj2);
        }
        if (r11.A08) {
            r0 = new C82873wK(obj, list);
        } else {
            r0 = AbstractC111885Be.A01;
        }
        r11.A02(r0, A022, A0d2);
    }

    public boolean A05() {
        AnonymousClass4YR r0;
        Boolean bool = this.A03;
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean A06 = A06();
        if (A06 && (r0 = this.A01) != null) {
            A06 = r0.A05();
        }
        this.A03 = Boolean.valueOf(A06);
        return A06;
    }

    public boolean A06() {
        if ((this instanceof C83113wi) || (this instanceof C83163wn)) {
            return false;
        }
        if (this instanceof C83143wl) {
            return true;
        }
        if (this instanceof C83123wj) {
            C83123wj r3 = (C83123wj) this;
            List list = r3.A01;
            if (list.size() != 1) {
                return ((AnonymousClass4YR) r3).A01 == null && list.size() > 1;
            }
            return true;
        } else if (this instanceof C83153wm) {
            return false;
        } else {
            if (this instanceof C83133wk) {
                return true;
            }
            if (!(this instanceof C83093wg)) {
                return C12970iu.A1W(((C83083wf) this).A00.A00.size());
            }
            return false;
        }
    }

    public boolean A07() {
        boolean z;
        Boolean bool = this.A04;
        if (bool == null) {
            AnonymousClass4YR r0 = this.A02;
            if (r0 == null || (r0.A06() && this.A02.A07())) {
                z = true;
            } else {
                z = false;
            }
            bool = Boolean.valueOf(z);
            this.A04 = bool;
        }
        return bool.booleanValue();
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public String toString() {
        if (this.A01 == null) {
            return A01();
        }
        StringBuilder A0h = C12960it.A0h();
        A0h.append(A01());
        return C12960it.A0d(A00().toString(), A0h);
    }
}
