package X;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.3LS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LS implements LocationListener {
    public final /* synthetic */ AnonymousClass1JU A00;
    public final /* synthetic */ C22100yW A01;

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public AnonymousClass3LS(AnonymousClass1JU r1, C22100yW r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        if (location != null) {
            StringBuilder A0k = C12960it.A0k("CompanionDevice/location/changed ");
            A0k.append(location.getTime());
            A0k.append(" ");
            A0k.append(location.getAccuracy());
            C12960it.A1F(A0k);
            C22100yW r4 = this.A01;
            r4.A0N.Ab2(new RunnableBRunnable0Shape3S0300000_I1(this, this.A00, location, 21));
            r4.A06.A04(this);
        }
    }
}
