package X;

import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.facebook.redex.ViewOnClickCListenerShape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.util.Log;
import java.nio.ByteBuffer;
import java.util.UUID;

/* renamed from: X.27T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27T extends AnonymousClass27U {
    public int A00 = 0;
    public final /* synthetic */ RegisterName A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass27T(AnonymousClass01d r8, C14830m7 r9, AnonymousClass018 r10, RegisterName registerName) {
        super(registerName, r8, r9, r10, R.layout.initialise_new_user);
        this.A01 = registerName;
    }

    public void A00(int i) {
        byte[] bArr;
        Integer num;
        StringBuilder sb = new StringBuilder("registername/updatestate/state ");
        sb.append(i);
        Log.i(sb.toString());
        this.A00 = i;
        if (i != 1) {
            RegisterName registerName = this.A01;
            if (!((ActivityC13790kL) registerName).A0B.A01()) {
                registerName.A0y.A02();
                findViewById(R.id.initial_sync_progress).setVisibility(0);
                findViewById(R.id.photo_progress).setVisibility(0);
                return;
            }
        }
        findViewById(R.id.initial_sync_progress).setVisibility(4);
        findViewById(R.id.photo_progress).setVisibility(4);
        RegisterName registerName2 = this.A01;
        ((ActivityC13810kN) registerName2).A09.A00.edit().remove("com.whatsapp.registername.initializer_start_time").apply();
        View view = registerName2.A03;
        if (view != null) {
            view.setVisibility(4);
        }
        registerName2.A1O.removeMessages(0);
        Log.i("registername/sync/finished");
        registerName2.startActivity(C14960mK.A04(registerName2));
        registerName2.finish();
        RegisterName.A1S = null;
        C36021jC.A00(registerName2, 0);
        if (((ActivityC13810kN) registerName2).A09.A00.getLong("eula_accepted_time", 0) > 0) {
            AnonymousClass31I r5 = new AnonymousClass31I();
            r5.A08 = Long.valueOf(System.currentTimeMillis() - ((ActivityC13810kN) registerName2).A09.A00.getLong("eula_accepted_time", 0));
            r5.A09 = Long.valueOf(System.currentTimeMillis() - ((ActivityC13810kN) registerName2).A09.A00.getLong("message_store_verified_time", 0));
            r5.A00 = Boolean.valueOf(((ActivityC13810kN) registerName2).A09.A00.getBoolean("registration_attempt_skip_with_no_vertical", false));
            r5.A03 = Boolean.valueOf(((ActivityC13810kN) registerName2).A09.A00.getBoolean("registration_retry_fetching_biz_profile", false));
            r5.A07 = registerName2.A1I;
            r5.A04 = registerName2.A1H;
            r5.A05 = registerName2.A1F;
            r5.A02 = Boolean.valueOf(registerName2.A1J);
            if (registerName2.A0V != null) {
                boolean z = registerName2.A1L;
                r5.A01 = Boolean.valueOf(z);
                if (z && (num = registerName2.A1G) != null) {
                    r5.A06 = num;
                }
            }
            String A0A = ((ActivityC13810kN) registerName2).A09.A0A();
            try {
                UUID fromString = UUID.fromString(A0A);
                ByteBuffer allocate = ByteBuffer.allocate(16);
                allocate.putLong(fromString.getMostSignificantBits());
                allocate.putLong(fromString.getLeastSignificantBits());
                bArr = allocate.array();
            } catch (IllegalArgumentException unused) {
                StringBuilder sb2 = new StringBuilder("RegistrationUtils/getBytesFromUUIDString/invalid-input ");
                sb2.append(A0A);
                Log.e(sb2.toString());
                bArr = new byte[0];
            }
            r5.A0A = Base64.encodeToString(bArr, 11);
            registerName2.A0d.A05(r5);
            registerName2.A0d.A01();
            ((ActivityC13830kP) registerName2).A05.Ab2(new RunnableBRunnable0Shape10S0100000_I0_10(this, 39));
        }
        ((ActivityC13810kN) registerName2).A09.A00.edit().remove("message_store_verified_time").remove("eula_accepted_time").remove("registration_retry_fetching_biz_profile").remove("registration_attempt_skip_with_no_vertical").remove("registration_sibling_app_phone_number").remove("registration_sibling_app_country_code").remove("registration_sibling_app_min_storage_needed").remove("sister_app_content_provider_is_enabled").remove("direct_migration_start_time").remove("direct_db_migration_timeout_in_secs").remove("migrate_from_consumer_app_directly").remove("direct_migration_session_id").remove("google_migrate_import_start_time").apply();
    }

    @Override // X.AnonymousClass27U, android.app.Dialog
    public void onCreate(Bundle bundle) {
        int i;
        super.onCreate(bundle);
        C41691tw.A05(getContext(), getWindow(), R.color.lightStatusBarBackgroundColor);
        getWindow().setFormat(1);
        getWindow().addFlags(4096);
        getWindow().setSoftInputMode(3);
        if (bundle == null) {
            i = 0;
        } else {
            i = bundle.getInt("state");
        }
        A00(i);
        RegisterName registerName = this.A01;
        View findViewById = findViewById(R.id.pay_ed_contact_support);
        registerName.A03 = findViewById;
        if (findViewById != null) {
            findViewById.setOnClickListener(new ViewOnClickCListenerShape3S0100000_I0_3(this, 32));
        }
    }

    @Override // android.app.Dialog
    public Bundle onSaveInstanceState() {
        Bundle onSaveInstanceState = super.onSaveInstanceState();
        onSaveInstanceState.putInt("state", this.A00);
        return onSaveInstanceState;
    }
}
