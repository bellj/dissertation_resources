package X;

import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.1TK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1TK extends AnonymousClass1TL {
    public static final ConcurrentMap A02 = new ConcurrentHashMap();
    public byte[] A00;
    public final String A01;

    public AnonymousClass1TK(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = true;
        long j = 0;
        BigInteger bigInteger = null;
        for (int i = 0; i != bArr.length; i++) {
            int i2 = bArr[i] & 255;
            if (j <= 72057594037927808L) {
                long j2 = j + ((long) (i2 & 127));
                if ((i2 & 128) == 0) {
                    if (z) {
                        if (j2 < 40) {
                            stringBuffer.append('0');
                        } else if (j2 < 80) {
                            stringBuffer.append('1');
                            j2 -= 40;
                        } else {
                            stringBuffer.append('2');
                            j2 -= 80;
                        }
                        z = false;
                    }
                    stringBuffer.append('.');
                    stringBuffer.append(j2);
                    j = 0;
                } else {
                    j = j2 << 7;
                }
            } else {
                BigInteger or = (bigInteger == null ? BigInteger.valueOf(j) : bigInteger).or(BigInteger.valueOf((long) (i2 & 127)));
                if ((i2 & 128) == 0) {
                    if (z) {
                        stringBuffer.append('2');
                        or = or.subtract(BigInteger.valueOf(80));
                        z = false;
                    }
                    stringBuffer.append('.');
                    stringBuffer.append(or);
                    j = 0;
                    bigInteger = null;
                } else {
                    bigInteger = or.shiftLeft(7);
                }
            }
        }
        this.A01 = stringBuffer.toString();
        this.A00 = AnonymousClass1TT.A02(bArr);
    }

    public static AnonymousClass1TK A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass1TK)) {
            return (AnonymousClass1TK) obj;
        }
        if (obj instanceof AnonymousClass1TN) {
            AnonymousClass1TL Aer = ((AnonymousClass1TN) obj).Aer();
            if (Aer instanceof AnonymousClass1TK) {
                return (AnonymousClass1TK) Aer;
            }
        }
        if (obj instanceof byte[]) {
            try {
                return (AnonymousClass1TK) AnonymousClass1TL.A03((byte[]) obj);
            } catch (IOException e) {
                StringBuilder sb = new StringBuilder("failed to construct object identifier from byte[]: ");
                sb.append(e.getMessage());
                throw new IllegalArgumentException(sb.toString());
            }
        } else {
            StringBuilder sb2 = new StringBuilder("illegal object in getInstance: ");
            sb2.append(obj.getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        if (r7.charAt(r6 + 1) != '0') goto L_0x0005;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        if (r4 == 0) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        if (r4 <= 1) goto L_0x0005;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A01(java.lang.String r7, int r8) {
        /*
            int r6 = r7.length()
            r5 = 0
        L_0x0005:
            r4 = 0
        L_0x0006:
            int r6 = r6 + -1
            r3 = 48
            r2 = 1
            if (r6 < r8) goto L_0x002b
            char r1 = r7.charAt(r6)
            r0 = 46
            if (r1 != r0) goto L_0x0022
            if (r4 == 0) goto L_0x0021
            if (r4 <= r2) goto L_0x0005
            int r0 = r6 + 1
            char r0 = r7.charAt(r0)
            if (r0 != r3) goto L_0x0005
        L_0x0021:
            return r5
        L_0x0022:
            if (r3 > r1) goto L_0x0021
            r0 = 57
            if (r1 > r0) goto L_0x0021
            int r4 = r4 + 1
            goto L_0x0006
        L_0x002b:
            if (r4 == 0) goto L_0x0021
            if (r4 <= r2) goto L_0x0037
            int r6 = r6 + r2
            char r0 = r7.charAt(r6)
            if (r0 != r3) goto L_0x0037
            return r5
        L_0x0037:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TK.A01(java.lang.String, int):boolean");
    }

    public AnonymousClass1TK A0B() {
        AnonymousClass1TS r2 = new AnonymousClass1TS(A0C());
        ConcurrentMap concurrentMap = A02;
        AnonymousClass1TK r0 = (AnonymousClass1TK) concurrentMap.get(r2);
        return (r0 == null && (r0 = (AnonymousClass1TK) concurrentMap.putIfAbsent(r2, this)) == null) ? this : r0;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return this.A01.hashCode();
    }

    public AnonymousClass1TK(String str) {
        char charAt;
        if (str == null) {
            throw new NullPointerException("'identifier' cannot be null");
        } else if (str.length() < 3 || str.charAt(1) != '.' || (charAt = str.charAt(0)) < '0' || charAt > '2' || !A01(str, 2)) {
            StringBuilder sb = new StringBuilder("string ");
            sb.append(str);
            sb.append(" not an OID");
            throw new IllegalArgumentException(sb.toString());
        } else {
            this.A01 = str;
        }
    }

    public AnonymousClass1TK(String str, AnonymousClass1TK r4) {
        if (A01(str, 0)) {
            StringBuilder sb = new StringBuilder();
            sb.append(r4.A01);
            sb.append(".");
            sb.append(str);
            this.A01 = sb.toString();
            return;
        }
        StringBuilder sb2 = new StringBuilder("string ");
        sb2.append(str);
        sb2.append(" not a valid OID branch");
        throw new IllegalArgumentException(sb2.toString());
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003f A[Catch: all -> 0x00b2, LOOP:0: B:9:0x0037->B:11:0x003f, LOOP_END, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0005, B:7:0x0027, B:8:0x002d, B:9:0x0037, B:11:0x003f, B:12:0x004a, B:13:0x0058, B:15:0x0063, B:16:0x0067, B:18:0x006e, B:19:0x0080, B:20:0x008b, B:21:0x0090, B:23:0x0095, B:25:0x009f, B:26:0x00a4, B:27:0x00aa), top: B:32:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063 A[Catch: all -> 0x00b2, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0005, B:7:0x0027, B:8:0x002d, B:9:0x0037, B:11:0x003f, B:12:0x004a, B:13:0x0058, B:15:0x0063, B:16:0x0067, B:18:0x006e, B:19:0x0080, B:20:0x008b, B:21:0x0090, B:23:0x0095, B:25:0x009f, B:26:0x00a4, B:27:0x00aa), top: B:32:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067 A[Catch: all -> 0x00b2, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0005, B:7:0x0027, B:8:0x002d, B:9:0x0037, B:11:0x003f, B:12:0x004a, B:13:0x0058, B:15:0x0063, B:16:0x0067, B:18:0x006e, B:19:0x0080, B:20:0x008b, B:21:0x0090, B:23:0x0095, B:25:0x009f, B:26:0x00a4, B:27:0x00aa), top: B:32:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095 A[Catch: all -> 0x00b2, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0005, B:7:0x0027, B:8:0x002d, B:9:0x0037, B:11:0x003f, B:12:0x004a, B:13:0x0058, B:15:0x0063, B:16:0x0067, B:18:0x006e, B:19:0x0080, B:20:0x008b, B:21:0x0090, B:23:0x0095, B:25:0x009f, B:26:0x00a4, B:27:0x00aa), top: B:32:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00aa A[Catch: all -> 0x00b2, TRY_LEAVE, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0005, B:7:0x0027, B:8:0x002d, B:9:0x0037, B:11:0x003f, B:12:0x004a, B:13:0x0058, B:15:0x0063, B:16:0x0067, B:18:0x006e, B:19:0x0080, B:20:0x008b, B:21:0x0090, B:23:0x0095, B:25:0x009f, B:26:0x00a4, B:27:0x00aa), top: B:32:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008b A[EDGE_INSN: B:34:0x008b->B:20:0x008b ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized byte[] A0C() {
        /*
            r11 = this;
            monitor-enter(r11)
            byte[] r0 = r11.A00     // Catch: all -> 0x00b2
            if (r0 != 0) goto L_0x00b0
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch: all -> 0x00b2
            r4.<init>()     // Catch: all -> 0x00b2
            java.lang.String r0 = r11.A01     // Catch: all -> 0x00b2
            X.1TR r3 = new X.1TR     // Catch: all -> 0x00b2
            r3.<init>(r0)     // Catch: all -> 0x00b2
            java.lang.String r0 = r3.A00()     // Catch: all -> 0x00b2
            int r0 = java.lang.Integer.parseInt(r0)     // Catch: all -> 0x00b2
            int r6 = r0 * 40
            java.lang.String r1 = r3.A00()     // Catch: all -> 0x00b2
            int r0 = r1.length()     // Catch: all -> 0x00b2
            r2 = 18
            if (r0 > r2) goto L_0x004a
            long r5 = (long) r6     // Catch: all -> 0x00b2
            long r0 = java.lang.Long.parseLong(r1)     // Catch: all -> 0x00b2
            long r5 = r5 + r0
        L_0x002d:
            r0 = 9
            byte[] r9 = new byte[r0]     // Catch: all -> 0x00b2
            int r0 = (int) r5     // Catch: all -> 0x00b2
            r0 = r0 & 127(0x7f, float:1.78E-43)
            byte r0 = (byte) r0     // Catch: all -> 0x00b2
            r1 = 8
        L_0x0037:
            r9[r1] = r0     // Catch: all -> 0x00b2
            r7 = 128(0x80, double:6.32E-322)
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 < 0) goto L_0x008b
            r0 = 7
            long r5 = r5 >> r0
            int r1 = r1 + -1
            int r0 = (int) r5     // Catch: all -> 0x00b2
            r0 = r0 & 127(0x7f, float:1.78E-43)
            r0 = r0 | 128(0x80, float:1.794E-43)
            byte r0 = (byte) r0     // Catch: all -> 0x00b2
            goto L_0x0037
        L_0x004a:
            java.math.BigInteger r5 = new java.math.BigInteger     // Catch: all -> 0x00b2
            r5.<init>(r1)     // Catch: all -> 0x00b2
            long r0 = (long) r6     // Catch: all -> 0x00b2
            java.math.BigInteger r0 = java.math.BigInteger.valueOf(r0)     // Catch: all -> 0x00b2
            java.math.BigInteger r10 = r5.add(r0)     // Catch: all -> 0x00b2
        L_0x0058:
            int r0 = r10.bitLength()     // Catch: all -> 0x00b2
            int r8 = r0 + 6
            r9 = 7
            int r8 = r8 / r9
            r7 = 0
            if (r8 != 0) goto L_0x0067
            r4.write(r7)     // Catch: all -> 0x00b2
            goto L_0x0090
        L_0x0067:
            byte[] r6 = new byte[r8]     // Catch: all -> 0x00b2
            int r5 = r8 + -1
            r1 = r5
        L_0x006c:
            if (r1 < 0) goto L_0x0080
            int r0 = r10.intValue()     // Catch: all -> 0x00b2
            r0 = r0 & 127(0x7f, float:1.78E-43)
            r0 = r0 | 128(0x80, float:1.794E-43)
            byte r0 = (byte) r0     // Catch: all -> 0x00b2
            r6[r1] = r0     // Catch: all -> 0x00b2
            java.math.BigInteger r10 = r10.shiftRight(r9)     // Catch: all -> 0x00b2
            int r1 = r1 + -1
            goto L_0x006c
        L_0x0080:
            byte r0 = r6[r5]     // Catch: all -> 0x00b2
            r0 = r0 & 127(0x7f, float:1.78E-43)
            byte r0 = (byte) r0     // Catch: all -> 0x00b2
            r6[r5] = r0     // Catch: all -> 0x00b2
            r4.write(r6, r7, r8)     // Catch: all -> 0x00b2
            goto L_0x0090
        L_0x008b:
            int r0 = 9 - r1
            r4.write(r9, r1, r0)     // Catch: all -> 0x00b2
        L_0x0090:
            int r1 = r3.A00     // Catch: all -> 0x00b2
            r0 = -1
            if (r1 == r0) goto L_0x00aa
            java.lang.String r1 = r3.A00()     // Catch: all -> 0x00b2
            int r0 = r1.length()     // Catch: all -> 0x00b2
            if (r0 > r2) goto L_0x00a4
            long r5 = java.lang.Long.parseLong(r1)     // Catch: all -> 0x00b2
            goto L_0x002d
        L_0x00a4:
            java.math.BigInteger r10 = new java.math.BigInteger     // Catch: all -> 0x00b2
            r10.<init>(r1)     // Catch: all -> 0x00b2
            goto L_0x0058
        L_0x00aa:
            byte[] r0 = r4.toByteArray()     // Catch: all -> 0x00b2
            r11.A00 = r0     // Catch: all -> 0x00b2
        L_0x00b0:
            monitor-exit(r11)
            return r0
        L_0x00b2:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TK.A0C():byte[]");
    }

    public String toString() {
        return this.A01;
    }
}
