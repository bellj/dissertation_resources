package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32171bj {
    public final UserJid A00;

    public C32171bj(UserJid userJid) {
        this.A00 = userJid;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("VNameCertificateEvent{jid='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
