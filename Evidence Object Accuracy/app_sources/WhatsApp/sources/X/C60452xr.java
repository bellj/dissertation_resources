package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C60452xr extends AnonymousClass1OY {
    public boolean A00;
    public final TextEmojiLabel A01 = C12970iu.A0U(this, R.id.message_text);
    public final String A02;

    @Override // X.AnonymousClass1OY
    public int A0m(int i) {
        return 0;
    }

    @Override // X.AnonymousClass1OY
    public int A0n(int i) {
        return 0;
    }

    @Override // X.AbstractC28551Oa
    public int getBubbleAlpha() {
        return 191;
    }

    public C60452xr(Context context, AbstractC13890kV r5, AnonymousClass1XZ r6) {
        super(context, r5, r6);
        A0Z();
        StringBuilder A0h = C12960it.A0h();
        String str = AnonymousClass01V.A06;
        A0h.append(str);
        A0h.append(context.getString(R.string.rejected_hsm_message));
        this.A02 = C12960it.A0d(str, A0h);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, getFMessage());
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    public void A1M() {
        TextEmojiLabel textEmojiLabel = this.A01;
        textEmojiLabel.setText(this.A02);
        C12960it.A0s(getContext(), textEmojiLabel, R.color.tombstone_text_color);
        textEmojiLabel.setTypeface(textEmojiLabel.getTypeface(), 2);
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setFocusable(false);
        textEmojiLabel.setClickable(false);
        textEmojiLabel.setLongClickable(false);
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_text_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_text_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_text_right;
    }
}
