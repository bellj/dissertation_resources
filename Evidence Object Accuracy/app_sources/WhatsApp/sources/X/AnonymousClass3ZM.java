package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* renamed from: X.3ZM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3ZM implements AbstractC21730xt {
    public AnonymousClass4TV A00;
    public final UserJid A01;
    public final C17220qS A02;

    public AnonymousClass3ZM(UserJid userJid, C17220qS r3) {
        C16700pc.A0E(r3, 2);
        this.A01 = userJid;
        this.A02 = r3;
    }

    public final void A00() {
        AnonymousClass4TV r1 = this.A00;
        if (r1 != null) {
            Log.e("GalaxyConnectionManager/loadBusinessCertInfo/onGetBusinessCertInfoError");
            AnonymousClass1WA r0 = r1.A01;
            if (r0 != null) {
                r0.A00();
            }
            r1.A00.A00.AaV("galaxy-connection-business-cert-error-response", "", false);
        }
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        A00();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C16700pc.A0E(str, 0);
        Log.w(C16700pc.A08("GetBusinessCertInfo/delivery-error with iqId ", str));
        A00();
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r14, String str) {
        Date date;
        AnonymousClass1WA r9;
        String str2;
        String str3;
        AbstractC15710nm r1;
        String str4;
        C16700pc.A0E(r14, 1);
        AnonymousClass1V8 A0E = r14.A0E("business_cert_info");
        if (A0E != null) {
            AnonymousClass1V8 A0E2 = A0E.A0E("ttl_timestamp");
            AnonymousClass1V8 A0E3 = A0E.A0E("issuer_cn");
            AnonymousClass1V8 A0E4 = A0E.A0E("business_domain");
            if (!(A0E2 == null || A0E3 == null || A0E4 == null)) {
                String A0G = A0E2.A0G();
                String A0G2 = A0E4.A0G();
                String A0G3 = A0E3.A0G();
                if (!TextUtils.isEmpty(A0G) && !TextUtils.isEmpty(A0G3) && !TextUtils.isEmpty(A0G2)) {
                    AnonymousClass4TV r5 = this.A00;
                    if (r5 != null) {
                        UserJid userJid = this.A01;
                        C16700pc.A0C(A0G);
                        C16700pc.A0C(A0G3);
                        C16700pc.A0C(A0G2);
                        Log.i("GalaxyConnectionManager/loadBusinessCertInfo/onGetBusinessCertInfoSuccess");
                        C247016n r7 = r5.A00;
                        try {
                            date = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'", Locale.US).parse(A0G);
                        } catch (ParseException e) {
                            Log.w(C12960it.A0d(e.getMessage(), C12960it.A0k("GalaxyConnectionManager/getTtlTimestampAsDate/")));
                            date = null;
                        }
                        if (date == null) {
                            AnonymousClass1WA r0 = r5.A01;
                            if (r0 != null) {
                                r0.A00();
                            }
                            r7.A00.AaV("galaxy-connection-invalid-expired-business-cert", "", false);
                            return;
                        }
                        if (!A0G2.equals(r5.A02)) {
                            Log.e("GalaxyConnectionManager/loadBusinessCertInfo/Incorrect Business domain in certificate");
                            r1 = r7.A00;
                            str4 = "galaxy-connection-invalid-cn-in-certificate";
                        } else if (!A0G3.equals(r5.A04)) {
                            Log.e("GalaxyConnectionManager/loadBusinessCertInfo/Incorrect Issuer CN in certificate");
                            r1 = r7.A00;
                            str4 = "galaxy-connection-invalid-issuer-in-certificate";
                        } else {
                            C12970iu.A1C(C12960it.A08(r7.A02), C12960it.A0d(userJid.getRawString(), C12960it.A0k("galaxy_business_cert_expired_timestamp_")), date.getTime());
                            String str5 = r5.A03;
                            if (str5 != null && (r9 = r5.A01) != null && (str2 = r5.A06) != null && (str3 = r5.A05) != null) {
                                r7.A00(userJid, r9, str5, str2, str3);
                                return;
                            }
                            return;
                        }
                        r1.AaV(str4, "", false);
                        r7.A02.A0a(userJid.getRawString());
                        AnonymousClass1WA r02 = r5.A01;
                        if (r02 != null) {
                            r02.A00();
                            return;
                        }
                        return;
                    }
                    return;
                }
            }
        }
        A00();
    }
}
