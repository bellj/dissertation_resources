package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1sH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40761sH extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40761sH A04;
    public static volatile AnonymousClass255 A05;
    public byte A00 = -1;
    public int A01;
    public long A02;
    public AnonymousClass1G6 A03;

    static {
        C40761sH r0 = new C40761sH();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A01 & 1) == 1) {
            AnonymousClass1G6 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G6.A0k;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A01 & 2) == 2) {
            i2 += CodedOutputStream.A06(2, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            AnonymousClass1G6 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G6.A0k;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0H(2, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
