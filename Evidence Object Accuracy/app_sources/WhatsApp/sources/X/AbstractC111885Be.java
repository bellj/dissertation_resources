package X;

/* renamed from: X.5Be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC111885Be implements Comparable {
    public static final AbstractC111885Be A01 = new C82853wI();
    public Object A00;

    public /* synthetic */ AbstractC111885Be(Object obj) {
        this.A00 = obj;
    }

    /* renamed from: A00 */
    public int compareTo(AbstractC111885Be r3) {
        return -A01().toString().compareTo(r3.A01().toString());
    }

    public Object A01() {
        if (this instanceof C82863wJ) {
            return "$";
        }
        if (this instanceof C82883wL) {
            return ((C82883wL) this).A00;
        }
        if (this instanceof C82873wK) {
            return C95094d8.A00(((C82873wK) this).A00, "&&", "");
        }
        if (!(this instanceof C82893wM)) {
            return null;
        }
        return Integer.valueOf(((C82893wM) this).A00);
    }
}
