package X;

/* renamed from: X.2lF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56582lF extends AbstractC15040mS {
    public static C56582lF A00;

    public C56582lF(C14160kx r1) {
        super(r1);
    }

    public static final String A00(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Integer) {
            obj = C12980iv.A0l(C12960it.A05(obj));
        }
        String str = "-";
        if (obj instanceof Long) {
            long abs = Math.abs(C12980iv.A0G(obj));
            int i = (abs > 100 ? 1 : (abs == 100 ? 0 : -1));
            String valueOf = String.valueOf(obj);
            if (i < 0) {
                return valueOf;
            }
            if (valueOf.charAt(0) != '-') {
                str = "";
            }
            String valueOf2 = String.valueOf(abs);
            StringBuilder A0j = C12960it.A0j(str);
            int length = valueOf2.length();
            A0j.append(Math.round(Math.pow(10.0d, (double) (length - 1))));
            A0j.append("...");
            A0j.append(str);
            return C12970iu.A0w(A0j, Math.round(Math.pow(10.0d, (double) length) - 4.0d));
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            return obj instanceof Throwable ? obj.getClass().getCanonicalName() : str;
        }
    }
}
