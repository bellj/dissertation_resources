package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.16d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246016d {
    public final C18460sU A00;
    public final C16490p7 A01;
    public final AnonymousClass1Y9 A02 = new AnonymousClass1Y9();
    public final AbstractC14440lR A03;

    public C246016d(C18460sU r2, C16490p7 r3, AbstractC14440lR r4) {
        this.A00 = r2;
        this.A03 = r4;
        this.A01 = r3;
    }

    public Map A00(Set set) {
        Object obj;
        C18460sU r9 = this.A00;
        if (!r9.A0C() || set.isEmpty()) {
            return new HashMap();
        }
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Jid jid = (Jid) it.next();
            Map map = this.A02.A00;
            if (map.containsKey(jid)) {
                obj = map.get(jid);
            } else {
                obj = null;
            }
            if (obj != null) {
                hashMap.put(jid, obj);
            } else {
                arrayList.add(Long.toString(r9.A01(jid)));
            }
        }
        C29841Uw r1 = new C29841Uw(arrayList.toArray(AnonymousClass01V.A0H), 975);
        HashMap hashMap2 = new HashMap();
        C16310on A01 = this.A01.get();
        try {
            synchronized (this) {
                HashSet hashSet = new HashSet();
                ArrayList arrayList2 = new ArrayList();
                Iterator it2 = r1.iterator();
                while (it2.hasNext()) {
                    String[] strArr = (String[]) it2.next();
                    C16330op r3 = A01.A03;
                    int length = strArr.length;
                    StringBuilder sb = new StringBuilder("SELECT user_jid_row_id, device_jid_row_id, key_index FROM user_device WHERE user_jid_row_id IN ");
                    sb.append(AnonymousClass1Ux.A00(length));
                    Cursor A09 = r3.A09(sb.toString(), strArr);
                    try {
                        int columnIndexOrThrow = A09.getColumnIndexOrThrow("user_jid_row_id");
                        int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("device_jid_row_id");
                        int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("key_index");
                        while (A09.moveToNext()) {
                            long j = A09.getLong(columnIndexOrThrow);
                            long j2 = A09.getLong(columnIndexOrThrow2);
                            long j3 = A09.getLong(columnIndexOrThrow3);
                            hashSet.add(Long.valueOf(j2));
                            UserJid userJid = (UserJid) r9.A07(UserJid.class, j);
                            AnonymousClass009.A05(userJid);
                            arrayList2.add(new AnonymousClass1YA(userJid, j2, j3));
                        }
                        A09.close();
                    } catch (Throwable th) {
                        if (A09 != null) {
                            try {
                                A09.close();
                            } catch (Throwable unused) {
                            }
                        }
                        throw th;
                    }
                }
                Map A092 = r9.A09(DeviceJid.class, hashSet);
                Iterator it3 = arrayList2.iterator();
                while (it3.hasNext()) {
                    AnonymousClass1YA r4 = (AnonymousClass1YA) it3.next();
                    UserJid userJid2 = r4.A02;
                    Map map2 = (Map) hashMap2.get(userJid2);
                    if (map2 == null) {
                        map2 = new HashMap();
                        hashMap2.put(userJid2, map2);
                    }
                    DeviceJid deviceJid = (DeviceJid) A092.get(Long.valueOf(r4.A00));
                    AnonymousClass009.A05(deviceJid);
                    map2.put(deviceJid, Long.valueOf(r4.A01));
                }
                for (Map.Entry entry : hashMap2.entrySet()) {
                    UserJid userJid3 = (UserJid) entry.getKey();
                    AnonymousClass1YB r32 = new AnonymousClass1YB();
                    for (Map.Entry entry2 : ((Map) entry.getValue()).entrySet()) {
                        r32.A01((DeviceJid) entry2.getKey(), (Long) entry2.getValue());
                    }
                    C28601Of A00 = r32.A00();
                    this.A02.A00.put(userJid3, A00);
                    hashMap.put(userJid3, A00);
                }
                Iterator it4 = set.iterator();
                while (it4.hasNext()) {
                    UserJid userJid4 = (UserJid) it4.next();
                    if (!hashMap.containsKey(userJid4)) {
                        hashMap.put(userJid4, C28601Of.A01);
                    }
                }
            }
            A01.close();
            return hashMap;
        } catch (Throwable th2) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public void A01(AnonymousClass1JO r16, UserJid userJid) {
        C16490p7 r10 = this.A01;
        C16310on A02 = r10.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            Iterator it = r16.iterator();
            while (it.hasNext()) {
                C18460sU r0 = this.A00;
                long A01 = r0.A01(userJid);
                long A012 = r0.A01((DeviceJid) it.next());
                A02 = r10.A02();
                try {
                    A02.A03.A01("user_device", "user_jid_row_id= ? AND device_jid_row_id = ?", new String[]{String.valueOf(A01), String.valueOf(A012)});
                    A02.close();
                } finally {
                }
            }
            A00.A00();
            A02(A02, userJid);
            A00.close();
            A02.close();
        } finally {
        }
    }

    public final void A02(C16310on r4, UserJid userJid) {
        RunnableBRunnable0Shape4S0200000_I0_4 runnableBRunnable0Shape4S0200000_I0_4 = new RunnableBRunnable0Shape4S0200000_I0_4(this, 44, userJid);
        AnonymousClass009.A0F(r4.A03.A00.inTransaction());
        C29621Ty r0 = r4.A02;
        AnonymousClass1YC r1 = new AnonymousClass1YC(r0, runnableBRunnable0Shape4S0200000_I0_4);
        Object obj = r0.A01.get();
        AnonymousClass009.A05(obj);
        ((AbstractMap) obj).put(userJid, r1);
    }

    public void A03(DeviceJid deviceJid, UserJid userJid, long j) {
        C16310on A02 = this.A01.A02();
        try {
            C18460sU r0 = this.A00;
            long A01 = r0.A01(userJid);
            long A012 = r0.A01(deviceJid);
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("user_jid_row_id", Long.valueOf(A01));
            contentValues.put("device_jid_row_id", Long.valueOf(A012));
            contentValues.put("key_index", Long.valueOf(j));
            A02.A03.A06(contentValues, "user_device", 4);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
