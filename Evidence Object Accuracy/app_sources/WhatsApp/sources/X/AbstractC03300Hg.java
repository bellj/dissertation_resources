package X;

import android.graphics.Matrix;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0Hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC03300Hg extends AnonymousClass0I1 implements AbstractC12490i0 {
    public Matrix A00;
    public AnonymousClass0JB A01;
    public Boolean A02;
    public String A03;
    public List A04 = new ArrayList();

    @Override // X.AbstractC12490i0
    public void A5f(AnonymousClass0OO r3) {
        if (r3 instanceof C03320Hi) {
            this.A04.add(r3);
            return;
        }
        StringBuilder sb = new StringBuilder("Gradient elements cannot contain ");
        sb.append(r3);
        sb.append(" elements.");
        throw new C11160fq(sb.toString());
    }

    @Override // X.AbstractC12490i0
    public List ABO() {
        return this.A04;
    }
}
