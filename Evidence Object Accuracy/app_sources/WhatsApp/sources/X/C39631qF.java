package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.1qF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39631qF extends Drawable implements Animatable, AbstractC39641qG {
    public static int A09 = 4000;
    public int A00;
    public int A01;
    public long A02;
    public boolean A03;
    public boolean A04;
    public boolean A05 = false;
    public final Paint A06 = new Paint(1);
    public final C28001Kb A07;
    public final List A08 = new ArrayList();

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public C39631qF(C28001Kb r3) {
        this.A07 = r3;
        if (C28111Kr.A00) {
            A09 = SearchActionVerificationClientService.NOTIFICATION_ID;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Bitmap bitmap;
        boolean z = this.A04;
        C28001Kb r0 = this.A07;
        if (!z) {
            bitmap = r0.A09;
        } else {
            bitmap = r0.A04;
        }
        if (bitmap == null) {
            bitmap = r0.A09;
        }
        canvas.drawBitmap(bitmap, (Rect) null, getBounds(), this.A06);
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.A04;
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        this.A02 = SystemClock.uptimeMillis();
        if (!this.A04 || C28111Kr.A00) {
            this.A00 = 0;
            this.A04 = true;
            C28001Kb r4 = this.A07;
            int i = r4.A00;
            int i2 = r4.A0D.A00;
            boolean z = true;
            if (i <= Math.max(i2 / 5, 1)) {
                z = false;
            }
            this.A05 = z;
            r4.A0F.add(this);
            if (!r4.A0G && i2 > 1) {
                r4.A0G = true;
                r4.A00();
            }
            Iterator it = this.A08.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        if (this.A04) {
            this.A04 = false;
            C28001Kb r4 = this.A07;
            Set set = r4.A0F;
            set.remove(this);
            if (set.isEmpty()) {
                r4.A0G = false;
                r4.A00 = 0;
                C64253Ev r3 = r4.A0B;
                synchronized (r3) {
                    r3.A00 = 0;
                    r3.A02 = null;
                    Bitmap bitmap = r3.A01;
                    if (bitmap != null) {
                        r3.A03 = null;
                        bitmap.recycle();
                        r3.A01 = null;
                    }
                }
                r4.A06 = false;
                Bitmap bitmap2 = r4.A04;
                if (bitmap2 != null) {
                    bitmap2.recycle();
                    r4.A04 = null;
                }
                r4.A03 = null;
                r4.A0A.A0G(r4.A0E);
                C39681qL r2 = r4.A0C.A04;
                synchronized (r2) {
                    Iterator it = r2.A01.iterator();
                    while (it.hasNext()) {
                        if (((C27991Ka) it.next()).A02 == r3) {
                            it.remove();
                        }
                    }
                }
            }
            for (AnonymousClass03J r0 : this.A08) {
                r0.A01(this);
            }
            invalidateSelf();
        }
    }
}
