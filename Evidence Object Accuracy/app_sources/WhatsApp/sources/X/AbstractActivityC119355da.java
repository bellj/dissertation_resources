package X;

import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperLinkActivity;

/* renamed from: X.5da  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119355da extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119355da() {
        C117295Zj.A0p(this, 111);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiMapperLinkActivity indiaUpiMapperLinkActivity = (IndiaUpiMapperLinkActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, indiaUpiMapperLinkActivity);
            ActivityC13810kN.A10(A1M, indiaUpiMapperLinkActivity);
            ((ActivityC13790kL) indiaUpiMapperLinkActivity).A08 = ActivityC13790kL.A0S(r3, A1M, indiaUpiMapperLinkActivity, ActivityC13790kL.A0Y(A1M, indiaUpiMapperLinkActivity));
            indiaUpiMapperLinkActivity.A01 = (C1329668y) A1M.A9d.get();
            indiaUpiMapperLinkActivity.A02 = C117305Zk.A0T(A1M);
        }
    }
}
