package X;

import android.os.Bundle;
import android.view.View;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.NoviConfirmPaymentFragment;
import com.whatsapp.payments.ui.NoviEditTransactionDescriptionFragment;
import com.whatsapp.payments.ui.NoviTransactionMethodDetailsFragment;
import com.whatsapp.payments.ui.NoviTransactionReviewDetailsFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.util.List;

/* renamed from: X.6CO  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CO implements AnonymousClass6N0 {
    public final /* synthetic */ long A00;
    public final /* synthetic */ C14580lf A01;
    public final /* synthetic */ AbstractC14640lm A02;
    public final /* synthetic */ UserJid A03;
    public final /* synthetic */ AnonymousClass6F2 A04;
    public final /* synthetic */ C1315863i A05;
    public final /* synthetic */ C1316263m A06;
    public final /* synthetic */ C128095vY A07;
    public final /* synthetic */ NoviConfirmPaymentFragment A08;
    public final /* synthetic */ PaymentBottomSheet A09;
    public final /* synthetic */ C118145bL A0A;
    public final /* synthetic */ AnonymousClass1KS A0B;
    public final /* synthetic */ Integer A0C;
    public final /* synthetic */ List A0D;

    @Override // X.AnonymousClass6N0
    public void ATc(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void ATg(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    public AnonymousClass6CO(C14580lf r1, AbstractC14640lm r2, UserJid userJid, AnonymousClass6F2 r4, C1315863i r5, C1316263m r6, C128095vY r7, NoviConfirmPaymentFragment noviConfirmPaymentFragment, PaymentBottomSheet paymentBottomSheet, C118145bL r10, AnonymousClass1KS r11, Integer num, List list, long j) {
        this.A0A = r10;
        this.A01 = r1;
        this.A09 = paymentBottomSheet;
        this.A02 = r2;
        this.A00 = j;
        this.A03 = userJid;
        this.A0D = list;
        this.A07 = r7;
        this.A0B = r11;
        this.A0C = num;
        this.A04 = r4;
        this.A08 = noviConfirmPaymentFragment;
        this.A06 = r6;
        this.A05 = r5;
    }

    @Override // X.AnonymousClass6N0
    public void AOW(View view, View view2, AnonymousClass1ZO r21, AbstractC28901Pl r22, PaymentBottomSheet paymentBottomSheet) {
        C27691It r1;
        String str;
        Boolean bool = Boolean.TRUE;
        C118145bL r12 = this.A0A;
        if (!bool.equals(r12.A0b.A03().A01())) {
            view.setVisibility(0);
            view2.setVisibility(8);
            r1 = r12.A09;
            str = "loginScreen";
        } else if (r12.A0c.A03() != null) {
            r1 = r12.A09;
            str = "limitationInterstitial";
        } else {
            C14580lf r4 = this.A01;
            if (r4 != null) {
                view2.setVisibility(0);
                AnonymousClass61P r3 = r12.A0i;
                C117315Zl.A0R(r3.A00, r4, new C134376Ej(r12.A12, new C133806Ce(view2, r22, this), r3));
                return;
            }
            this.A09.A1G(false);
            AbstractC14640lm r8 = this.A02;
            long j = this.A00;
            C118145bL.A01(r22, r8, this.A03, null, this.A07, r12, null, null, this.A0D, j);
            return;
        }
        C126025sD.A00(r1, str);
    }

    @Override // X.AnonymousClass6N0
    public void ATW(PaymentBottomSheet paymentBottomSheet, int i) {
        this.A0A.A00.A00(new AbstractC14590lg(this.A04, this.A07, this.A08, paymentBottomSheet, this) { // from class: X.6El
            public final /* synthetic */ AnonymousClass6F2 A00;
            public final /* synthetic */ C128095vY A01;
            public final /* synthetic */ NoviConfirmPaymentFragment A02;
            public final /* synthetic */ PaymentBottomSheet A03;
            public final /* synthetic */ AnonymousClass6CO A04;

            {
                this.A04 = r5;
                this.A00 = r1;
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AnonymousClass6CO r0 = this.A04;
                AnonymousClass6F2 r8 = this.A00;
                C128095vY r3 = this.A01;
                NoviConfirmPaymentFragment noviConfirmPaymentFragment = this.A02;
                PaymentBottomSheet paymentBottomSheet2 = this.A03;
                C118145bL r4 = r0.A0A;
                Object A01 = r4.A0s.A01();
                Object A012 = r4.A0p.A01();
                Object A013 = r4.A0r.A01();
                Object A014 = r4.A0E.A01();
                AnonymousClass009.A05(A01);
                AnonymousClass009.A05(A012);
                AnonymousClass61F r02 = r4.A0b;
                AnonymousClass009.A05(A013);
                List A05 = r02.A05((C1315463e) A013, (List) obj);
                AnonymousClass009.A05(A014);
                AbstractC30791Yv r42 = ((AnonymousClass63Y) A014).A02;
                AnonymousClass63X r1 = r3.A00;
                NoviTransactionMethodDetailsFragment noviTransactionMethodDetailsFragment = new NoviTransactionMethodDetailsFragment();
                Bundle A0D = C12970iu.A0D();
                A0D.putParcelable("arg_novi_balance", (C1316263m) A01);
                A0D.putParcelable("arg_exchange_quote", (C1315863i) A012);
                A0D.putParcelable("arg_payment_amount", r8);
                A0D.putParcelable("arg_deposit_draft", r1);
                A0D.putParcelable("arg_transaction_currency", r42);
                A0D.putParcelableArrayList("arg_methods", C12980iv.A0x(A05));
                noviTransactionMethodDetailsFragment.A0U(A0D);
                C117315Zl.A0P(noviTransactionMethodDetailsFragment, noviConfirmPaymentFragment, paymentBottomSheet2);
            }
        });
    }

    @Override // X.AnonymousClass6N0
    public void ATZ(AbstractC28901Pl r5, PaymentMethodRow paymentMethodRow) {
        C118145bL r3 = this.A0A;
        C1316263m r2 = this.A06;
        AnonymousClass63X A06 = r3.A06(r5, this.A04, this.A05, r2);
        r3.A0Y.A09 = A06;
        this.A07.A00 = A06;
    }

    @Override // X.AnonymousClass6N0
    public void AXm(PaymentBottomSheet paymentBottomSheet) {
        UserJid userJid = this.A03;
        C1316263m r6 = this.A06;
        C128095vY r0 = this.A07;
        C1315863i r5 = r0.A04;
        C1315163b r4 = r0.A05;
        AnonymousClass63X r3 = r0.A00;
        NoviTransactionReviewDetailsFragment noviTransactionReviewDetailsFragment = new NoviTransactionReviewDetailsFragment();
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("arg_receiver_jid", userJid);
        A0D.putParcelable("arg_account_balance", r6);
        A0D.putParcelable("arg_exchange_quote", r5);
        A0D.putParcelable("arg_transaction_data", r4);
        A0D.putParcelable("arg_deposit_draft", r3);
        noviTransactionReviewDetailsFragment.A0U(A0D);
        C117315Zl.A0P(noviTransactionReviewDetailsFragment, this.A08, paymentBottomSheet);
    }

    @Override // X.AnonymousClass6N0
    public void AXn(String str) {
        this.A0A.A0A = str;
        this.A07.A01 = str;
    }

    @Override // X.AnonymousClass6N0
    public void AXo(PaymentBottomSheet paymentBottomSheet) {
        C128365vz r2 = new AnonymousClass610("ADD_MESSAGE_CLICK", "SEND_MONEY", "REVIEW_TRANSACTION", "BODY").A00;
        r2.A0n = "P2P";
        C118145bL r1 = this.A0A;
        C128365vz.A01(r2, r1.A0A);
        r1.A0a.A06(r2);
        C117315Zl.A0P(NoviEditTransactionDescriptionFragment.A00(r1.A0A), this.A08, paymentBottomSheet);
    }
}
