package X;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.5I6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5I6 extends AbstractSet<Map.Entry<K, V>> {
    public final /* synthetic */ AnonymousClass5I4 this$0;

    public AnonymousClass5I6(AnonymousClass5I4 r1) {
        this.this$0 = r1;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public void clear() {
        this.this$0.clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean contains(Object obj) {
        AnonymousClass5I4 r1 = this.this$0;
        Map delegateOrNull = r1.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.entrySet().contains(obj);
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        int i = r1.indexOf(entry.getKey());
        if (i == -1 || !AnonymousClass28V.A00(this.this$0.value(i), entry.getValue())) {
            return false;
        }
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator iterator() {
        return this.this$0.entrySetIterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean remove(Object obj) {
        AnonymousClass5I4 r1 = this.this$0;
        Map delegateOrNull = r1.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.entrySet().remove(obj);
        }
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            if (!r1.needsAllocArrays()) {
                int i = r1.hashTableMask();
                Object key = entry.getKey();
                Object value = entry.getValue();
                AnonymousClass5I4 r0 = this.this$0;
                int remove = C95584e0.remove(key, value, i, r0.requireTable(), r0.requireEntries(), r0.requireKeys(), r0.requireValues());
                if (remove != -1) {
                    this.this$0.moveLastEntry(remove, i);
                    AnonymousClass5I4 r02 = this.this$0;
                    AnonymousClass5I4.access$1210(r02);
                    r02.incrementModCount();
                    return true;
                }
            }
        }
        return false;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return this.this$0.size();
    }
}
