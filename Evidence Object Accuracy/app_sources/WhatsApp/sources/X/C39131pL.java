package X;

import android.os.PowerManager;
import com.whatsapp.Mp4Ops;

/* renamed from: X.1pL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39131pL extends AbstractRunnableC39141pM {
    public final PowerManager.WakeLock A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final Mp4Ops A03;
    public final C15450nH A04;
    public final C17050qB A05;
    public final C16590pI A06;
    public final C14820m6 A07;
    public final C19350ty A08;
    public final C14850m9 A09;
    public final C39121pK A0A;
    public final C239713s A0B;
    public final C26831Ez A0C;
    public final AnonymousClass152 A0D;

    public C39131pL(PowerManager.WakeLock wakeLock, AbstractC15710nm r2, C14330lG r3, Mp4Ops mp4Ops, C15450nH r5, C17050qB r6, C16590pI r7, C14820m6 r8, C19350ty r9, C14850m9 r10, C39121pK r11, C239713s r12, C26831Ez r13, AnonymousClass152 r14) {
        super(r11);
        this.A06 = r7;
        this.A03 = mp4Ops;
        this.A09 = r10;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A0B = r12;
        this.A08 = r9;
        this.A05 = r6;
        this.A0D = r14;
        this.A07 = r8;
        this.A0C = r13;
        this.A0A = r11;
        this.A00 = wakeLock;
    }

    /* JADX WARNING: Removed duplicated region for block: B:118:0x0353 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01e4 A[Catch: IllegalStateException -> 0x032a, IllegalArgumentException -> 0x030c, 47W -> 0x02fe, FileNotFoundException -> 0x02dd, IOException -> 0x02aa, 1pl -> 0x028b, all -> 0x0363, TryCatch #5 {IllegalArgumentException -> 0x030c, blocks: (B:23:0x0114, B:24:0x0117, B:26:0x011d, B:29:0x012c, B:31:0x0132, B:32:0x0137, B:34:0x0142, B:36:0x0153, B:38:0x015f, B:40:0x0169, B:43:0x0174, B:49:0x0185, B:50:0x0190, B:51:0x01ba, B:57:0x01c8, B:59:0x01d5, B:61:0x01e0, B:63:0x01e4, B:65:0x01ed, B:67:0x01f2, B:68:0x01fd, B:69:0x01fe, B:70:0x0202, B:73:0x0211, B:74:0x0219, B:75:0x021a, B:93:0x0275, B:94:0x027c, B:95:0x027d, B:96:0x028a), top: B:135:0x0114, outer: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x021a A[Catch: IllegalStateException -> 0x032a, IllegalArgumentException -> 0x030c, 47W -> 0x02fe, FileNotFoundException -> 0x02dd, IOException -> 0x02aa, 1pl -> 0x028b, all -> 0x0363, TRY_LEAVE, TryCatch #5 {IllegalArgumentException -> 0x030c, blocks: (B:23:0x0114, B:24:0x0117, B:26:0x011d, B:29:0x012c, B:31:0x0132, B:32:0x0137, B:34:0x0142, B:36:0x0153, B:38:0x015f, B:40:0x0169, B:43:0x0174, B:49:0x0185, B:50:0x0190, B:51:0x01ba, B:57:0x01c8, B:59:0x01d5, B:61:0x01e0, B:63:0x01e4, B:65:0x01ed, B:67:0x01f2, B:68:0x01fd, B:69:0x01fe, B:70:0x0202, B:73:0x0211, B:74:0x0219, B:75:0x021a, B:93:0x0275, B:94:0x027c, B:95:0x027d, B:96:0x028a), top: B:135:0x0114, outer: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0225  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0235  */
    @Override // X.AbstractRunnableC39141pM
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC39731qS A00() {
        /*
        // Method dump skipped, instructions count: 889
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39131pL.A00():X.1qS");
    }
}
