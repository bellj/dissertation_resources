package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78023oF extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99454kK();
    public final int A00;

    public C78023oF(int i) {
        this.A00 = i;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
