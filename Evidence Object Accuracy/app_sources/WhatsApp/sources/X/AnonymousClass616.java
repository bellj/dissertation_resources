package X;

import android.os.SystemClock;
import java.util.List;

/* renamed from: X.616  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass616 {
    public static final C129775yH A00 = new C129775yH();

    public static void A00() {
        SystemClock.elapsedRealtime();
        List list = A00.A00;
        if (0 < list.size()) {
            list.get(0);
            throw C12980iv.A0n("getLoggerHandler");
        }
    }

    public static void A01(String str, String str2) {
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(": ");
        A0j.append(str2);
        A0j.toString();
        A00();
    }

    public static void A02(String str, String str2) {
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(": ");
        A0j.append(str2);
        A0j.toString();
        A00();
    }
}
