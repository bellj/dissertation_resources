package X;

import java.lang.reflect.Array;

/* renamed from: X.3dG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71643dG implements AnonymousClass5XE {
    public static final byte[] A04 = {99, 124, 119, 123, -14, 107, 111, -59, 48, 1, 103, 43, -2, -41, -85, 118, -54, -126, -55, 125, -6, 89, 71, -16, -83, -44, -94, -81, -100, -92, 114, -64, -73, -3, -109, 38, 54, 63, -9, -52, 52, -91, -27, -15, 113, -40, 49, 21, 4, -57, 35, -61, 24, -106, 5, -102, 7, 18, Byte.MIN_VALUE, -30, -21, 39, -78, 117, 9, -125, 44, 26, 27, 110, 90, -96, 82, 59, -42, -77, 41, -29, 47, -124, 83, -47, 0, -19, 32, -4, -79, 91, 106, -53, -66, 57, 74, 76, 88, -49, -48, -17, -86, -5, 67, 77, 51, -123, 69, -7, 2, Byte.MAX_VALUE, 80, 60, -97, -88, 81, -93, 64, -113, -110, -99, 56, -11, -68, -74, -38, 33, 16, -1, -13, -46, -51, 12, 19, -20, 95, -105, 68, 23, -60, -89, 126, 61, 100, 93, 25, 115, 96, -127, 79, -36, 34, 42, -112, -120, 70, -18, -72, 20, -34, 94, 11, -37, -32, 50, 58, 10, 73, 6, 36, 92, -62, -45, -84, 98, -111, -107, -28, 121, -25, -56, 55, 109, -115, -43, 78, -87, 108, 86, -12, -22, 101, 122, -82, 8, -70, 120, 37, 46, 28, -90, -76, -58, -24, -35, 116, 31, 75, -67, -117, -118, 112, 62, -75, 102, 72, 3, -10, 14, 97, 53, 87, -71, -122, -63, 29, -98, -31, -8, -104, 17, 105, -39, -114, -108, -101, 30, -121, -23, -50, 85, 40, -33, -116, -95, -119, 13, -65, -26, 66, 104, 65, -103, 45, 15, -80, 84, -69, 22};
    public static final byte[] A05 = {82, 9, 106, -43, 48, 54, -91, 56, -65, 64, -93, -98, -127, -13, -41, -5, 124, -29, 57, -126, -101, 47, -1, -121, 52, -114, 67, 68, -60, -34, -23, -53, 84, 123, -108, 50, -90, -62, 35, 61, -18, 76, -107, 11, 66, -6, -61, 78, 8, 46, -95, 102, 40, -39, 36, -78, 118, 91, -94, 73, 109, -117, -47, 37, 114, -8, -10, 100, -122, 104, -104, 22, -44, -92, 92, -52, 93, 101, -74, -110, 108, 112, 72, 80, -3, -19, -71, -38, 94, 21, 70, 87, -89, -115, -99, -124, -112, -40, -85, 0, -116, -68, -45, 10, -9, -28, 88, 5, -72, -77, 69, 6, -48, 44, 30, -113, -54, 63, 15, 2, -63, -81, -67, 3, 1, 19, -118, 107, 58, -111, 17, 65, 79, 103, -36, -22, -105, -14, -49, -50, -16, -76, -26, 115, -106, -84, 116, 34, -25, -83, 53, -123, -30, -7, 55, -24, 28, 117, -33, 110, 71, -15, 26, 113, 29, 41, -59, -119, 111, -73, 98, 14, -86, 24, -66, 27, -4, 86, 62, 75, -58, -46, 121, 32, -102, -37, -64, -2, 120, -51, 90, -12, 31, -35, -88, 51, -120, 7, -57, 49, -79, 18, 16, 89, 39, Byte.MIN_VALUE, -20, 95, 96, 81, Byte.MAX_VALUE, -87, 25, -75, 74, 13, 45, -27, 122, -97, -109, -55, -100, -17, -96, -32, 59, 77, -82, 42, -11, -80, -56, -21, -69, 60, -125, 83, -103, 97, 23, 43, 4, 126, -70, 119, -42, 38, -31, 105, 20, 99, 85, 33, 12, 125};
    public static final int[] A06 = {-1520213050, -2072216328, -1720223762, -1921287178, 234025727, -1117033514, -1318096930, 1422247313, 1345335392, 50397442, -1452841010, 2099981142, 436141799, 1658312629, -424957107, -1703512340, 1170918031, -1652391393, 1086966153, -2021818886, 368769775, -346465870, -918075506, 200339707, -324162239, 1742001331, -39673249, -357585083, -1080255453, -140204973, -1770884380, 1539358875, -1028147339, 486407649, -1366060227, 1780885068, 1513502316, 1094664062, 49805301, 1338821763, 1546925160, -190470831, 887481809, 150073849, -1821281822, 1943591083, 1395732834, 1058346282, 201589768, 1388824469, 1696801606, 1589887901, 672667696, -1583966665, 251987210, -1248159185, 151455502, 907153956, -1686077413, 1038279391, 652995533, 1764173646, -843926913, -1619692054, 453576978, -1635548387, 1949051992, 773462580, 756751158, -1301385508, -296068428, -73359269, -162377052, 1295727478, 1641469623, -827083907, 2066295122, 1055122397, 1898917726, -1752923117, -179088474, 1758581177, 0, 753790401, 1612718144, 536673507, -927878791, -312779850, -1100322092, 1187761037, -641810841, 1262041458, -565556588, -733197160, -396863312, 1255133061, 1808847035, 720367557, -441800113, 385612781, -985447546, -682799718, 1429418854, -1803188975, -817543798, 284817897, 100794884, -2122350594, -263171936, 1144798328, -1163944155, -475486133, -212774494, -22830243, -1069531008, -1970303227, -1382903233, -1130521311, 1211644016, 83228145, -541279133, -1044990345, 1977277103, 1663115586, 806359072, 452984805, 250868733, 1842533055, 1288555905, 336333848, 890442534, 804056259, -513843266, -1567123659, -867941240, 957814574, 1472513171, -223893675, -2105639172, 1195195770, -1402706744, -413311558, 723065138, -1787595802, -1604296512, -1736343271, -783331426, 2145180835, 1713513028, 2116692564, -1416589253, -2088204277, -901364084, 703524551, -742868885, 1007948840, 2044649127, -497131844, 487262998, 1994120109, 1004593371, 1446130276, 1312438900, 503974420, -615954030, 168166924, 1814307912, -463709000, 1573044895, 1859376061, -273896381, -1503501628, -1466855111, -1533700815, 937747667, -1954973198, 854058965, 1137232011, 1496790894, -1217565222, -1936880383, 1691735473, -766620004, -525751991, -1267962664, -95005012, 133494003, 636152527, -1352309302, -1904575756, -374428089, 403179536, -709182865, -2005370640, 1864705354, 1915629148, 605822008, -240736681, -944458637, 1371981463, 602466507, 2094914977, -1670089496, 555687742, -582268010, -591544991, -2037675251, -2054518257, -1871679264, 1111375484, -994724495, -1436129588, -666351472, 84083462, 32962295, 302911004, -1553899070, 1597322602, -111716434, -793134743, -1853454825, 1489093017, 656219450, -1180787161, 954327513, 335083755, -1281845205, 856756514, -1150719534, 1893325225, -1987146233, -1483434957, -1231316179, 572399164, -1836611819, 552200649, 1238290055, -11184726, 2015897680, 2061492133, -1886614525, -123625127, -2138470135, 386731290, -624967835, 837215959, -968736124, -1201116976, -1019133566, -1332111063, 1999449434, 286199582, -877612933, -61582168, -692339859, 974525996};
    public static final int[] A07 = {1353184337, 1399144830, -1012656358, -1772214470, -882136261, -247096033, -1420232020, -1828461749, 1442459680, -160598355, -1854485368, 625738485, -52959921, -674551099, -2143013594, -1885117771, 1230680542, 1729870373, -1743852987, -507445667, 41234371, 317738113, -1550367091, -956705941, -413167869, -1784901099, -344298049, -631680363, 763608788, -752782248, 694804553, 1154009486, 1787413109, 2021232372, 1799248025, -579749593, -1236278850, 397248752, 1722556617, -1271214467, 407560035, -2110711067, 1613975959, 1165972322, -529046351, -2068943941, 480281086, -1809118983, 1483229296, 436028815, -2022908268, -1208452270, 601060267, -503166094, 1468997603, 715871590, 120122290, 63092015, -1703164538, -1526188077, -226023376, -1297760477, -1167457534, 1552029421, 723308426, -1833666137, -252573709, -1578997426, -839591323, -708967162, 526529745, -1963022652, -1655493068, -1604979806, 853641733, 1978398372, 971801355, -1427152832, 111112542, 1360031421, -108388034, 1023860118, -1375387939, 1186850381, -1249028975, 90031217, 1876166148, -15380384, 620468249, -1746289194, -868007799, 2006899047, -1119688528, -2004121337, 945494503, -605108103, 1191869601, -384875908, -920746760, 0, -2088337399, 1223502642, -1401941730, 1316117100, -67170563, 1446544655, 517320253, 658058550, 1691946762, 564550760, -783000677, 976107044, -1318647284, 266819475, -761860428, -1634624741, 1338359936, -1574904735, 1766553434, 370807324, 179999714, -450191168, 1138762300, 488053522, 185403662, -1379431438, -1180125651, -928440812, -2061897385, 1275557295, -1143105042, -44007517, -1624899081, -1124765092, -985962940, 880737115, 1982415755, -590994485, 1761406390, 1676797112, -891538985, 277177154, 1076008723, 538035844, 2099530373, -130171950, 288553390, 1839278535, 1261411869, -214912292, -330136051, -790380169, 1813426987, -1715900247, -95906799, 577038663, -997393240, 440397984, -668172970, -275762398, -951170681, -1043253031, -22885748, 906744984, -813566554, 685669029, 646887386, -1530942145, -459458004, 227702864, -1681105046, 1648787028, -1038905866, -390539120, 1593260334, -173030526, -1098883681, 2090061929, -1456614033, -1290656305, 999926984, -1484974064, 1852021992, 2075868123, 158869197, -199730834, 28809964, -1466282109, 1701746150, 2129067946, 147831841, -420997649, -644094022, -835293366, -737566742, -696471511, -1347247055, 824393514, 815048134, -1067015627, 935087732, -1496677636, -1328508704, 366520115, 1251476721, -136647615, 240176511, 804688151, -1915335306, 1303441219, 1414376140, -553347356, -474623586, 461924940, -1205916479, 2136040774, 82468509, 1563790337, 1937016826, 776014843, 1511876531, 1389550482, 861278441, 323475053, -1939744870, 2047648055, -1911228327, -1992551445, -299390514, 902390199, -303751967, 1018251130, 1507840668, 1064563285, 2043548696, -1086863501, -355600557, 1537932639, 342834655, -2032450440, -2114736182, 1053059257, 741614648, 1598071746, 1925389590, 203809468, -1958134744, 1100287487, 1895934009, -558691320, -1662733096, -1866377628, 1636092795, 1890988757, 1952214088, 1113045200};
    public static final int[] A08 = {1, 2, 4, 8, 16, 32, 64, 128, 27, 54, C43951xu.A03, 216, 171, 77, 154, 47, 94, 188, 99, 198, 151, 53, 106, 212, 179, 125, 250, 239, 197, 145};
    public int A00;
    public boolean A01;
    public byte[] A02;
    public int[][] A03 = null;

    public static int A00(int i) {
        byte[] bArr = A04;
        return (bArr[(i >> 24) & 255] << 24) | (bArr[i & 255] & 255) | ((bArr[(i >> 8) & 255] & 255) << 8) | ((bArr[(i >> 16) & 255] & 255) << 16);
    }

    @Override // X.AnonymousClass5XE
    public String AAf() {
        return "AES";
    }

    @Override // X.AnonymousClass5XE
    public int AAt() {
        return 16;
    }

    @Override // X.AnonymousClass5XE
    public void reset() {
    }

    public static int A01(byte[] bArr, int i, int i2) {
        return i2 ^ (bArr[(i >> 24) & 255] << 24);
    }

    public static int A02(byte[] bArr, int i, int i2, int i3) {
        return i3 ^ ((bArr[i & 255] & 255) << i2);
    }

    public static int A03(byte[] bArr, int i, int i2, int i3) {
        int i4 = i + 1;
        bArr[i] = (byte) i2;
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i2 >> 8);
        int i6 = i5 + 1;
        bArr[i5] = (byte) (i2 >> 16);
        int i7 = i6 + 1;
        bArr[i6] = (byte) (i2 >> 24);
        int i8 = i7 + 1;
        bArr[i7] = (byte) i3;
        int i9 = i8 + 1;
        bArr[i8] = (byte) (i3 >> 8);
        int i10 = i9 + 1;
        bArr[i9] = (byte) (i3 >> 16);
        return i10;
    }

    public static int A04(int[] iArr, int i, int i2, int i3) {
        int i4 = iArr[i & 255];
        int i5 = iArr[(i2 >> 8) & 255];
        return ((i5 << i3) | (i5 >>> 24)) ^ i4;
    }

    public static int A05(int[] iArr, int i, int i2, int i3, int i4) {
        int i5 = iArr[i & 255];
        return i4 ^ ((i5 << i3) | (i5 >>> i2));
    }

    @Override // X.AnonymousClass5XE
    public void AIf(AnonymousClass20L r20, boolean z) {
        int i;
        if (r20 instanceof AnonymousClass20K) {
            byte[] bArr = ((AnonymousClass20K) r20).A00;
            int length = bArr.length;
            if (length < 16 || length > 32 || (length & 7) != 0) {
                throw C12970iu.A0f("Key length not 128/192/256 bits.");
            }
            int i2 = length >>> 2;
            int i3 = i2 + 6;
            this.A00 = i3;
            int[][] iArr = (int[][]) Array.newInstance(int.class, i3 + 1, 4);
            if (i2 == 4) {
                int A00 = AbstractC95434di.A00(bArr, 0);
                int[] iArr2 = iArr[0];
                iArr2[0] = A00;
                int A0B = C12990iw.A0B(bArr, iArr2, 4, 1);
                int A0B2 = C12990iw.A0B(bArr, iArr2, 8, 2);
                int A0B3 = C12990iw.A0B(bArr, iArr2, 12, 3);
                int i4 = 1;
                do {
                    i = -8;
                    A00 ^= A00((A0B3 << i) | (A0B3 >>> 8)) ^ A08[i4 - 1];
                    int[] iArr3 = iArr[i4];
                    A0B = C12990iw.A0C(iArr3, A00, 0, A0B, 1);
                    A0B2 ^= A0B;
                    A0B3 = C12990iw.A0C(iArr3, A0B2, 2, A0B3, 3);
                    i4++;
                } while (i4 <= 10);
            } else if (i2 == 6) {
                int A002 = AbstractC95434di.A00(bArr, 0);
                int[] iArr4 = iArr[0];
                iArr4[0] = A002;
                int A0B4 = C12990iw.A0B(bArr, iArr4, 4, 1);
                int A0B5 = C12990iw.A0B(bArr, iArr4, 8, 2);
                int A0B6 = C12990iw.A0B(bArr, iArr4, 12, 3);
                int A003 = AbstractC95434di.A00(bArr, 16);
                int A004 = AbstractC95434di.A00(bArr, 20);
                int i5 = 1;
                int i6 = 1;
                while (true) {
                    int[] iArr5 = iArr[i5];
                    iArr5[0] = A003;
                    iArr5[1] = A004;
                    i = -8;
                    int A005 = A00((A004 << i) | (A004 >>> 8)) ^ i6;
                    int i7 = i6 << 1;
                    int i8 = A002 ^ A005;
                    int A0C = C12990iw.A0C(iArr5, i8, 2, A0B4, 3);
                    int i9 = A0B5 ^ A0C;
                    int[] iArr6 = iArr[i5 + 1];
                    int A0C2 = C12990iw.A0C(iArr6, i9, 0, A0B6, 1);
                    int i10 = A003 ^ A0C2;
                    int A0C3 = C12990iw.A0C(iArr6, i10, 2, A004, 3);
                    int A006 = A00((A0C3 << i) | (A0C3 >>> 8)) ^ i7;
                    i6 = i7 << 1;
                    A002 = i8 ^ A006;
                    int[] iArr7 = iArr[i5 + 2];
                    A0B4 = C12990iw.A0C(iArr7, A002, 0, A0C, 1);
                    A0B5 = i9 ^ A0B4;
                    A0B6 = C12990iw.A0C(iArr7, A0B5, 2, A0C2, 3);
                    i5 += 3;
                    if (i5 >= 13) {
                        break;
                    }
                    A003 = i10 ^ A0B6;
                    A004 = A0C3 ^ A003;
                }
            } else if (i2 == 8) {
                int A007 = AbstractC95434di.A00(bArr, 0);
                int[] iArr8 = iArr[0];
                iArr8[0] = A007;
                int A0B7 = C12990iw.A0B(bArr, iArr8, 4, 1);
                int A0B8 = C12990iw.A0B(bArr, iArr8, 8, 2);
                int A0B9 = C12990iw.A0B(bArr, iArr8, 12, 3);
                int A008 = AbstractC95434di.A00(bArr, 16);
                int[] iArr9 = iArr[1];
                iArr9[0] = A008;
                int A0B10 = C12990iw.A0B(bArr, iArr9, 20, 1);
                int A0B11 = C12990iw.A0B(bArr, iArr9, 24, 2);
                int A0B12 = C12990iw.A0B(bArr, iArr9, 28, 3);
                int i11 = 1;
                int i12 = 2;
                while (true) {
                    i = -8;
                    int A009 = A00((A0B12 << i) | (A0B12 >>> 8)) ^ i11;
                    i11 <<= 1;
                    A007 ^= A009;
                    int[] iArr10 = iArr[i12];
                    A0B7 = C12990iw.A0C(iArr10, A007, 0, A0B7, 1);
                    A0B8 ^= A0B7;
                    A0B9 = C12990iw.A0C(iArr10, A0B8, 2, A0B9, 3);
                    int i13 = i12 + 1;
                    if (i13 >= 15) {
                        break;
                    }
                    A008 ^= A00(A0B9);
                    int[] iArr11 = iArr[i13];
                    A0B10 = C12990iw.A0C(iArr11, A008, 0, A0B10, 1);
                    A0B11 ^= A0B10;
                    A0B12 = C12990iw.A0C(iArr11, A0B11, 2, A0B12, 3);
                    i12 = i13 + 1;
                }
            } else {
                throw C12960it.A0U("Should never get here");
            }
            if (!z) {
                for (int i14 = 1; i14 < this.A00; i14++) {
                    int i15 = 0;
                    do {
                        i15 = C12960it.A07(iArr, i14, i15, i);
                    } while (i15 < 4);
                }
            }
            this.A03 = iArr;
            this.A01 = z;
            this.A02 = AnonymousClass1TT.A02(z ? A04 : A05);
            return;
        }
        throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(r20), C12960it.A0k("invalid parameter passed to AES init - ")));
    }

    @Override // X.AnonymousClass5XE
    public int AZY(byte[] bArr, byte[] bArr2, int i, int i2) {
        char c;
        int[] iArr;
        byte[] bArr3;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int[][] iArr2 = this.A03;
        if (iArr2 == null) {
            throw C12960it.A0U("AES engine not initialised");
        } else if (i + 16 > bArr.length) {
            throw new AnonymousClass5O2("input buffer too short");
        } else if (i2 + 16 <= bArr2.length) {
            boolean z = this.A01;
            int i8 = i + 1;
            int i9 = i8 + 1;
            int i10 = i9 + 1;
            int i11 = i10 + 1;
            int i12 = (bArr[i] & 255) | ((bArr[i8] & 255) << 8) | ((bArr[i9] & 255) << 16) | (bArr[i10] << 24);
            int i13 = i11 + 1;
            int i14 = i13 + 1;
            int i15 = ((bArr[i13] & 255) << 8) | (bArr[i11] & 255);
            int i16 = i14 + 1;
            int i17 = i16 + 1;
            int i18 = i15 | ((bArr[i14] & 255) << 16) | (bArr[i16] << 24);
            int i19 = i17 + 1;
            int i20 = bArr[i17] & 255;
            int i21 = i19 + 1;
            int i22 = ((bArr[i19] & 255) << 8) | i20;
            int i23 = i21 + 1;
            int i24 = i23 + 1;
            int i25 = i22 | ((bArr[i21] & 255) << 16) | (bArr[i23] << 24);
            int i26 = i24 + 1;
            int i27 = i26 + 1;
            int i28 = (bArr[i27 + 1] << 24) | ((bArr[i26] & 255) << 8) | (bArr[i24] & 255) | ((bArr[i27] & 255) << 16);
            if (z) {
                int[] iArr3 = iArr2[0];
                int i29 = i12 ^ iArr3[0];
                int i30 = i18 ^ iArr3[1];
                int i31 = i25 ^ iArr3[2];
                c = 3;
                int i32 = i28 ^ iArr3[3];
                int i33 = 1;
                while (i33 < this.A00 - 1) {
                    int[] iArr4 = A06;
                    int i34 = iArr4[i29 & 255];
                    int i35 = iArr4[(i30 >> 8) & 255];
                    int i36 = -24;
                    int i37 = i34 ^ ((i35 << i36) | (i35 >>> 24));
                    int i38 = iArr4[(i31 >> 16) & 255];
                    int i39 = -16;
                    int i40 = i37 ^ ((i38 << i39) | (i38 >>> 16));
                    int i41 = iArr4[(i32 >> 24) & 255];
                    int i42 = -8;
                    int[] iArr5 = iArr2[i33];
                    int i43 = (i40 ^ ((i41 << i42) | (i41 >>> 8))) ^ iArr5[0];
                    int A052 = A05(iArr4, i29 >> 24, 8, i42, A05(iArr4, i32 >> 16, 16, i39, A04(iArr4, i30, i31, i36))) ^ iArr5[1];
                    int A053 = A05(iArr4, i30 >> 24, 8, i42, A05(iArr4, i29 >> 16, 16, i39, A04(iArr4, i31, i32, i36))) ^ iArr5[2];
                    int i44 = i33 + 1;
                    int A054 = A05(iArr4, i31 >> 24, 8, i42, A05(iArr4, i30 >> 16, 16, i39, A04(iArr4, i32, i29, i36))) ^ iArr5[3];
                    int A055 = A05(iArr4, A054 >> 24, 8, i42, A05(iArr4, A053 >> 16, 16, i39, A05(iArr4, A052 >> 8, 24, i36, iArr4[i43 & 255])));
                    int[] iArr6 = iArr2[i44];
                    i29 = A055 ^ iArr6[0];
                    i30 = A05(iArr4, i43 >> 24, 8, i42, A05(iArr4, A054 >> 16, 16, i39, A05(iArr4, A053 >> 8, 24, i36, iArr4[A052 & 255]))) ^ iArr6[1];
                    i31 = A05(iArr4, A052 >> 24, 8, i42, A05(iArr4, i43 >> 16, 16, i39, A05(iArr4, A054 >> 8, 24, i36, iArr4[A053 & 255]))) ^ iArr6[2];
                    i33 = i44 + 1;
                    i32 = A05(iArr4, A053 >> 24, 8, i42, A05(iArr4, A052 >> 16, 16, i39, A05(iArr4, i43 >> 8, 24, i36, iArr4[A054 & 255]))) ^ iArr6[3];
                }
                int[] iArr7 = A06;
                int i45 = iArr7[i29 & 255];
                int i46 = iArr7[(i30 >> 8) & 255];
                int i47 = -24;
                int i48 = i45 ^ ((i46 << i47) | (i46 >>> 24));
                int i49 = iArr7[(i31 >> 16) & 255];
                int i50 = -16;
                int i51 = i48 ^ ((i49 << i50) | (i49 >>> 16));
                int i52 = iArr7[(i32 >> 24) & 255];
                int i53 = -8;
                int[] iArr8 = iArr2[i33];
                int i54 = (i51 ^ ((i52 << i53) | (i52 >>> 8))) ^ iArr8[0];
                int A056 = A05(iArr7, i29 >> 24, 8, i53, A05(iArr7, i32 >> 16, 16, i50, A05(iArr7, i31 >> 8, 24, i47, iArr7[i30 & 255]))) ^ iArr8[1];
                i3 = A05(iArr7, i30 >> 24, 8, i53, A05(iArr7, i29 >> 16, 16, i50, A05(iArr7, i32 >> 8, 24, i47, iArr7[i31 & 255]))) ^ iArr8[2];
                int i55 = iArr7[i32 & 255];
                int i56 = iArr7[(i29 >> 8) & 255];
                int i57 = i31 >> 24;
                int A057 = A05(iArr7, i57, 8, i53, A05(iArr7, i30 >> 16, 16, i50, ((i56 << i47) | (i56 >>> 24)) ^ i55)) ^ iArr8[3];
                bArr3 = A04;
                int A02 = A02(bArr3, A056 >> 8, 8, bArr3[i54 & 255] & 255);
                byte[] bArr4 = this.A02;
                int A01 = A01(bArr4, A057, A02(bArr4, i3 >> 16, 16, A02));
                iArr = iArr2[i33 + 1];
                i6 = A01 ^ iArr[0];
                i4 = A01(bArr4, i54, A02(bArr3, A057 >> 16, 16, A02(bArr3, i3 >> 8, 8, bArr4[A056 & 255] & 255))) ^ iArr[1];
                i5 = A01(bArr3, A056, A02(bArr3, i54 >> 16, 16, A02(bArr3, A057 >> 8, 8, bArr4[i3 & 255] & 255))) ^ iArr[2];
                i7 = A02(bArr4, A056 >> 16, 16, A02(bArr4, i54 >> 8, 8, bArr4[A057 & 255] & 255));
            } else {
                int i58 = this.A00;
                int[] iArr9 = iArr2[i58];
                int i59 = i12 ^ iArr9[0];
                int i60 = 1;
                int i61 = i18 ^ iArr9[1];
                int i62 = i25 ^ iArr9[2];
                int i63 = i58 - 1;
                c = 3;
                int i64 = iArr9[3] ^ i28;
                while (i63 > i60) {
                    int[] iArr10 = A07;
                    int i65 = iArr10[i59 & 255];
                    int i66 = iArr10[(i64 >> 8) & 255];
                    int i67 = -24;
                    int i68 = i65 ^ ((i66 << i67) | (i66 >>> 24));
                    int i69 = iArr10[(i62 >> 16) & 255];
                    int i70 = -16;
                    int i71 = iArr10[(i61 >> 24) & 255];
                    int i72 = -8;
                    int[] iArr11 = iArr2[i63];
                    int i73 = ((i68 ^ ((i69 << i70) | (i69 >>> 16))) ^ ((i71 << i72) | (i71 >>> 8))) ^ iArr11[0];
                    int A058 = A05(iArr10, i62 >> 24, 8, i72, A05(iArr10, i64 >> 16, 16, i70, A04(iArr10, i61, i59, i67))) ^ iArr11[i60];
                    int A059 = A05(iArr10, i64 >> 24, 8, i72, A05(iArr10, i59 >> 16, 16, i70, A04(iArr10, i62, i61, i67))) ^ iArr11[2];
                    int A0510 = A05(iArr10, i61 >> 16, 16, i70, A05(iArr10, i62 >> 8, 24, i67, iArr10[i64 & 255]));
                    int i74 = iArr10[(i59 >> 24) & 255];
                    int i75 = i63 - 1;
                    int i76 = (((i74 << i72) | (i74 >>> 8)) ^ A0510) ^ iArr11[3];
                    int A0511 = A05(iArr10, A058 >> 24, 8, i72, A05(iArr10, A059 >> 16, 16, i70, A05(iArr10, i76 >> 8, 24, i67, iArr10[i73 & 255])));
                    int[] iArr12 = iArr2[i75];
                    i59 = A0511 ^ iArr12[0];
                    i61 = A05(iArr10, A059 >> 24, 8, i72, A05(iArr10, i76 >> 16, 16, i70, A05(iArr10, i73 >> 8, 24, i67, iArr10[A058 & 255]))) ^ iArr12[1];
                    i62 = A05(iArr10, i76 >> 24, 8, i72, A05(iArr10, i73 >> 16, 16, i70, A05(iArr10, A058 >> 8, 24, i67, iArr10[A059 & 255]))) ^ iArr12[2];
                    i63 = i75 - 1;
                    i64 = iArr12[3] ^ A05(iArr10, i73 >> 24, 8, i72, A05(iArr10, A058 >> 16, 16, i70, A05(iArr10, A059 >> 8, 24, i67, iArr10[i76 & 255])));
                    i60 = 1;
                }
                int[] iArr13 = A07;
                int i77 = iArr13[i59 & 255];
                int i78 = iArr13[(i64 >> 8) & 255];
                int i79 = -24;
                int i80 = iArr13[(i62 >> 16) & 255];
                int i81 = -16;
                int i82 = iArr13[(i61 >> 24) & 255];
                int i83 = -8;
                int i84 = ((i77 ^ ((i78 << i79) | (i78 >>> 24))) ^ ((i80 << i81) | (i80 >>> 16))) ^ ((i82 << i83) | (i82 >>> 8));
                int[] iArr14 = iArr2[i63];
                i3 = i84 ^ iArr14[0];
                int A0512 = A05(iArr13, i62 >> 24, 8, i83, A05(iArr13, i64 >> 16, 16, i81, A05(iArr13, i59 >> 8, 24, i79, iArr13[i61 & 255]))) ^ iArr14[i60];
                int A0513 = A05(iArr13, i64 >> 24, 8, i83, A05(iArr13, i59 >> 16, 16, i81, A05(iArr13, i61 >> 8, 24, i79, iArr13[i62 & 255]))) ^ iArr14[2];
                int A0514 = A05(iArr13, i61 >> 16, 16, i81, A05(iArr13, i62 >> 8, 24, i79, iArr13[i64 & 255]));
                int i85 = iArr13[(i59 >> 24) & 255];
                int i86 = (((i85 << i83) | (i85 >>> 8)) ^ A0514) ^ iArr14[3];
                byte[] bArr5 = A05;
                int i87 = bArr5[i3 & 255] & 255;
                bArr3 = this.A02;
                int A012 = A01(bArr5, A0512, A02(bArr3, A0513 >> 16, 16, A02(bArr3, i86 >> 8, 8, i87)));
                iArr = iArr2[0];
                i6 = A012 ^ iArr[0];
                i4 = A01(bArr3, A0513, A02(bArr5, i86 >> 16, 16, A02(bArr3, i3 >> 8, 8, bArr3[A0512 & 255] & 255))) ^ iArr[1];
                i5 = A01(bArr3, i86, A02(bArr5, i3 >> 16, 16, A02(bArr5, A0512 >> 8, 8, bArr3[A0513 & 255] & 255))) ^ iArr[2];
                i7 = A02(bArr3, A0512 >> 16, 16, A02(bArr3, A0513 >> 8, 8, bArr5[i86 & 255] & 255));
            }
            int A013 = A01(bArr3, i3, i7) ^ iArr[c];
            int A03 = A03(bArr2, i2, i6, i4);
            bArr2[A03] = (byte) (i4 >> 24);
            bArr2[A03(bArr2, A03 + 1, i5, A013)] = (byte) (A013 >> 24);
            return 16;
        } else {
            throw new C114975Nu("output buffer too short");
        }
    }
}
