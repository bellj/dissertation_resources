package X;

/* renamed from: X.1Nb  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Nb extends AbstractC16110oT {
    public String A00;
    public String A01;
    public String A02;

    public AnonymousClass1Nb() {
        super(1684, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamCriticalEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "context", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "debug", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "name", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
