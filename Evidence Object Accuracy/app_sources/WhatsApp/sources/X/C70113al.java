package X;

import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;
import java.util.Collections;

/* renamed from: X.3al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70113al implements AnonymousClass5WL {
    public final /* synthetic */ ConversationsFragment.DeleteContactDialogFragment A00;
    public final /* synthetic */ C15370n3 A01;

    public C70113al(ConversationsFragment.DeleteContactDialogFragment deleteContactDialogFragment, C15370n3 r2) {
        this.A00 = deleteContactDialogFragment;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5WL
    public void ASv() {
        this.A00.A1B();
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        Log.i("conversations/delete-contact");
        ConversationsFragment.DeleteContactDialogFragment deleteContactDialogFragment = this.A00;
        deleteContactDialogFragment.A1B();
        C15370n3 r0 = this.A01;
        C63063Ac.A00((ActivityC13810kN) deleteContactDialogFragment.A0B(), deleteContactDialogFragment.A00, deleteContactDialogFragment.A03, deleteContactDialogFragment.A04, deleteContactDialogFragment.A07, Collections.singletonList(r0), z);
    }
}
