package X;

import com.google.android.gms.location.LocationRequest;

/* renamed from: X.1UM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1UM extends AnonymousClass1UG {
    public final /* synthetic */ AnonymousClass1U3 A00;
    public final /* synthetic */ LocationRequest A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1UM(AnonymousClass1U8 r1, AnonymousClass1U3 r2, LocationRequest locationRequest) {
        super(r1);
        this.A01 = locationRequest;
        this.A00 = r2;
    }
}
