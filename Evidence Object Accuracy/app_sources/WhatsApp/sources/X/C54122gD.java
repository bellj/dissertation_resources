package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import java.util.Map;

/* renamed from: X.2gD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54122gD extends AnonymousClass02L {
    public long A00 = 1;
    public final Map A01 = C12970iu.A11();

    public C54122gD() {
        super(new AnonymousClass0SC(new C74583iL()).A00());
        A07(true);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        Map map = this.A01;
        String A00 = ((AbstractC92594Wn) A0E(i)).A00();
        Number number = (Number) map.get(A00);
        if (number == null) {
            long j = this.A00;
            this.A00 = 1 + j;
            number = Long.valueOf(j);
            map.put(A00, number);
        }
        return number.longValue();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r7, int i) {
        int i2;
        C75403jn r72 = (C75403jn) r7;
        AbstractC92594Wn r5 = (AbstractC92594Wn) A0E(i);
        View view = r72.A0H;
        String string = view.getContext().getString(R.string.edit_business_categories_label_popular_categories);
        WaTextView waTextView = r72.A01;
        boolean z = r5.A01;
        if (z || string.equals(r5.A01())) {
            i2 = Integer.MAX_VALUE;
        } else {
            i2 = AnonymousClass3G9.A01(view.getContext(), 120.0f);
        }
        waTextView.setMaxWidth(i2);
        C12960it.A0z(waTextView, r5, 7);
        waTextView.setText(r5.A01());
        r72.A00.setVisibility(C13010iy.A00(z ? 1 : 0));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75403jn(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.item_bread_crumbs));
    }
}
