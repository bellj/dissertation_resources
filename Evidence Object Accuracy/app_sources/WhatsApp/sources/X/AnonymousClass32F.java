package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import org.json.JSONObject;

/* renamed from: X.32F  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass32F extends AnonymousClass19V {
    public final C14650lo A00;

    public AnonymousClass32F(C18790t3 r16, C14650lo r17, C14820m6 r18, C14850m9 r19, AnonymousClass18L r20, AnonymousClass01H r21, AnonymousClass01N r22, AnonymousClass01N r23, long j) {
        super(r16, r18, r19, r20, r21, "WA|787118555984857|7bb1544a3599aa180ac9a3f7688ba243", null, r23, r22, j);
        this.A00 = r17;
        String str = "/catalog";
        super.A00 = !str.startsWith("/") ? C12960it.A0d(str, C12960it.A0j("/")) : str;
    }

    public void A03(UserJid userJid, String str, JSONObject jSONObject, JSONObject jSONObject2) {
        String A01 = this.A00.A07.A01(userJid);
        if (!TextUtils.isEmpty(A01)) {
            jSONObject2.put("direct_connection_encrypted_info", A01);
        }
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put(str, jSONObject2);
        JSONObject jSONObject4 = new JSONObject();
        jSONObject4.put("request", jSONObject3);
        jSONObject.put("variables", jSONObject4);
    }
}
