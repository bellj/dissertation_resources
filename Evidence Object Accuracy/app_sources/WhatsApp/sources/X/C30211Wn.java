package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Wn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30211Wn implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99644kd();
    public final String A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30211Wn(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A04(readString);
        this.A00 = readString;
        String readString2 = parcel.readString();
        AnonymousClass009.A04(readString2);
        this.A01 = readString2;
    }

    public C30211Wn(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    public static String A00(String str, List list) {
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C30211Wn r0 = (C30211Wn) it.next();
            if (r0 != null) {
                String str2 = r0.A01;
                if (!TextUtils.isEmpty(str2)) {
                    sb.append(str2);
                    sb.append(str);
                }
            }
            Log.e(new NullPointerException("Category is null"));
        }
        int length = sb.length();
        int length2 = str.length();
        return length > length2 ? sb.substring(0, sb.length() - length2) : "";
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C30211Wn)) {
            return false;
        }
        C30211Wn r4 = (C30211Wn) obj;
        if (!this.A00.equals(r4.A00) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (this.A00.hashCode() * 31) + this.A01.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("BizCategory:{'id'='");
        sb.append(this.A00);
        sb.append("', 'name'='");
        sb.append(this.A01);
        sb.append("'}");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }
}
