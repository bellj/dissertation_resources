package X;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1wC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42971wC {
    public static Spannable A00(Context context, String str) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new C52292aZ(context), 0, str.length(), 0);
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder A01(String str, Map map) {
        Spanned fromHtml = Html.fromHtml(str);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fromHtml);
        URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                Object obj = map.get(uRLSpan.getURL());
                if (obj != null) {
                    int spanStart = spannableStringBuilder.getSpanStart(uRLSpan);
                    int spanEnd = spannableStringBuilder.getSpanEnd(uRLSpan);
                    int spanFlags = spannableStringBuilder.getSpanFlags(uRLSpan);
                    spannableStringBuilder.removeSpan(uRLSpan);
                    spannableStringBuilder.setSpan(obj, spanStart, spanEnd, spanFlags);
                }
            }
        }
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder A02(String str, Spannable... spannableArr) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        int i = 0;
        while (i < spannableArr.length) {
            StringBuilder sb = new StringBuilder("%");
            int i2 = i + 1;
            sb.append(i2);
            sb.append("$s");
            String obj = sb.toString();
            int indexOf = spannableStringBuilder.toString().indexOf(obj);
            if (indexOf != -1) {
                spannableStringBuilder.replace(indexOf, obj.length() + indexOf, (CharSequence) spannableArr[i]);
            } else {
                StringBuilder sb2 = new StringBuilder("RichTextUtils/formatSpannableString: skipping placeholder of index ");
                sb2.append(i2);
                sb2.append(" as we cannot find it in template: ");
                sb2.append(str);
                sb2.append(" with args: ");
                sb2.append(spannableArr);
                Log.e(sb2.toString());
            }
            i = i2;
        }
        return spannableStringBuilder;
    }

    public static CharSequence A03(AnonymousClass01d r2, C16630pM r3, CharSequence charSequence) {
        return A04(r2, r3, charSequence, -16777216, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0032, code lost:
        continue;
     */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0187 A[EDGE_INSN: B:150:0x0187->B:121:0x0187 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence A04(X.AnonymousClass01d r16, X.C16630pM r17, java.lang.CharSequence r18, int r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 542
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42971wC.A04(X.01d, X.0pM, java.lang.CharSequence, int, boolean):java.lang.CharSequence");
    }

    public static ArrayList A05(Spannable spannable) {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        Object[] spans = spannable.getSpans(0, spannable.length(), URLSpan.class);
        if (spans == null || spans.length == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(Arrays.asList(spans));
        Collections.sort(arrayList, new Comparator(spannable) { // from class: X.5Cm
            public final /* synthetic */ Spannable A00;

            {
                this.A00 = r1;
            }

            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                Spannable spannable2 = this.A00;
                if (spannable2.getSpanStart(obj) < spannable2.getSpanStart(obj2)) {
                    return -1;
                }
                if (spannable2.getSpanStart(obj) > spannable2.getSpanStart(obj2)) {
                    return 1;
                }
                return AnonymousClass048.A00(spannable2.getSpanEnd(obj2), spannable2.getSpanEnd(obj));
            }
        });
        int size = arrayList.size();
        while (i4 < size - 1) {
            Object obj = arrayList.get(i4);
            int i5 = i4 + 1;
            Object obj2 = arrayList.get(i5);
            int spanStart = spannable.getSpanStart(obj);
            int spanEnd = spannable.getSpanEnd(obj);
            int spanStart2 = spannable.getSpanStart(obj2);
            int spanEnd2 = spannable.getSpanEnd(obj2);
            if (spanStart <= spanStart2 && spanEnd > spanStart2) {
                if (spanEnd2 <= spanEnd || (i2 = spanEnd - spanStart) > (i3 = spanEnd2 - spanStart2)) {
                    spannable.removeSpan(obj2);
                    i = i5;
                } else if (i2 < i3) {
                    spannable.removeSpan(obj);
                    i = i4;
                }
                if (i != -1) {
                    arrayList.remove(i);
                    size--;
                }
            }
            i4 = i5;
        }
        return arrayList;
    }

    public static void A06(Context context, Paint paint, Editable editable, AnonymousClass01d r10, AnonymousClass19M r11, C16630pM r12) {
        A07(context, paint, editable, r10, r11, r12, 1.3f);
    }

    public static void A07(Context context, Paint paint, Editable editable, AnonymousClass01d r8, AnonymousClass19M r9, C16630pM r10, float f) {
        AbstractC36671kL.A07(context, paint, editable, r9, f);
        ForegroundColorSpan[] foregroundColorSpanArr = (ForegroundColorSpan[]) editable.getSpans(0, editable.length(), ForegroundColorSpan.class);
        for (ForegroundColorSpan foregroundColorSpan : foregroundColorSpanArr) {
            if (!(foregroundColorSpan instanceof AbstractC115425Rm)) {
                editable.removeSpan(foregroundColorSpan);
            }
        }
        A0A(editable, StyleSpan.class, 0, editable.length());
        A0A(editable, StrikethroughSpan.class, 0, editable.length());
        A0A(editable, TypefaceSpan.class, 0, editable.length());
        A04(r8, r10, editable, paint.getColor(), true);
    }

    public static void A08(Context context, Uri uri, AnonymousClass12P r9, C14900mE r10, TextEmojiLabel textEmojiLabel, AnonymousClass01d r12, String str, String str2) {
        A09(context, r9, r10, textEmojiLabel, r12, str, new AnonymousClass5IG(str2, uri));
    }

    public static void A09(Context context, AnonymousClass12P r11, C14900mE r12, TextEmojiLabel textEmojiLabel, AnonymousClass01d r14, String str, Map map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            hashMap.put(entry.getKey(), new C58272oQ(context, r11, r12, r14, entry.getValue().toString()));
        }
        SpannableStringBuilder A01 = A01(str, hashMap);
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAccessibilityHelper(new AnonymousClass2eq(textEmojiLabel, r14));
        textEmojiLabel.setText(A01);
    }

    public static void A0A(Editable editable, Class cls, int i, int i2) {
        Object[] spans = editable.getSpans(i, i2, cls);
        if (spans != null) {
            for (Object obj : spans) {
                editable.removeSpan(obj);
            }
        }
    }

    public static void A0B(ArrayList arrayList, int i, int i2) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            AnonymousClass4S7 r1 = (AnonymousClass4S7) it.next();
            int i3 = r1.A00;
            if (i3 > i) {
                r1.A00 = i3 - i2;
            }
            int i4 = r1.A01;
            if (i4 > i) {
                r1.A01 = i4 - i2;
            }
        }
    }

    public static boolean A0C(AnonymousClass01d r0, C16630pM r1, CharSequence charSequence) {
        return charSequence != null && !AnonymousClass1US.A0C(A03(r0, r1, charSequence));
    }
}
