package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.5kx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122125kx extends AbstractC118815cQ {
    public final WaImageView A00;

    public C122125kx(View view) {
        super(view);
        this.A00 = C12980iv.A0X(view, R.id.order_detail_edge);
    }
}
