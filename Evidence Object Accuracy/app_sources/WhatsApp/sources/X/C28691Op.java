package X;

import android.util.Log;

/* renamed from: X.1Op  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28691Op {
    public static void A00(String str, String str2) {
        StringBuilder sb = new StringBuilder("[");
        sb.append(str);
        sb.append("] ");
        sb.append(str2);
        Log.e("Whatsapp", sb.toString());
    }

    public static void A01(String str, String str2, Throwable th) {
        StringBuilder sb = new StringBuilder("[");
        sb.append(str);
        sb.append("]");
        sb.append(str2);
        Log.e("Whatsapp", sb.toString(), th);
    }

    public static void A02(String str, Throwable th) {
        StringBuilder sb = new StringBuilder("[");
        sb.append(str);
        sb.append("] ");
        Log.e("Whatsapp", sb.toString(), th);
    }
}
