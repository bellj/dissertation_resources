package X;

/* renamed from: X.23t  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23t implements AbstractC14590lg {
    public int A00 = -1;
    public int A01;
    public final /* synthetic */ C38421o4 A02;
    public final /* synthetic */ AnonymousClass109 A03;
    public final /* synthetic */ AnonymousClass1KC A04;

    public AnonymousClass23t(C38421o4 r2, AnonymousClass109 r3, AnonymousClass1KC r4) {
        this.A03 = r3;
        this.A02 = r2;
        this.A04 = r4;
    }

    @Override // X.AbstractC14590lg
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        Number number = (Number) obj;
        int intValue = number.intValue();
        if (intValue != this.A00) {
            this.A00 = intValue;
            if (intValue >= this.A01 + 5) {
                this.A01 = intValue;
                this.A02.A01.size();
            }
            AnonymousClass109 r3 = this.A03;
            C26751Er r2 = r3.A0F;
            C38421o4 r1 = this.A02;
            C26751Er.A00(r1, new AbstractC458623l() { // from class: X.57Z
                @Override // X.AbstractC458623l
                public final boolean A67(C16150oX r32, AbstractC16130oV r4, Object obj2) {
                    r32.A0C = (long) C12960it.A05(obj2);
                    return true;
                }
            }, number);
            r3.A09.A09(r1.A01, 8);
        }
    }
}
