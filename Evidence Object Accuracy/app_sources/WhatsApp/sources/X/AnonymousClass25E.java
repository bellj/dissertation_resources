package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25E  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25E extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25E A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02 = AbstractC27881Jp.A01;

    static {
        AnonymousClass25E r0 = new AnonymousClass25E();
        A03 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A04(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A09(this.A02, 2);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A02, 2);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
