package X;

import java.io.InputStream;
import java.io.RandomAccessFile;

/* renamed from: X.49B  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49B extends InputStream {
    public final /* synthetic */ C64443Fo A00;
    public final /* synthetic */ RandomAccessFile A01;

    public AnonymousClass49B(C64443Fo r1, RandomAccessFile randomAccessFile) {
        this.A00 = r1;
        this.A01 = randomAccessFile;
    }

    @Override // java.io.InputStream
    public int read() {
        return this.A01.read();
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        return this.A01.read(bArr);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        return this.A01.read(bArr, i, i2);
    }
}
