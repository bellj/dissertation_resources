package X;

import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.util.UUID;

/* renamed from: X.0mT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15050mT {
    public final C14160kx A00;

    public C15050mT(C14160kx r1) {
        C13020j0.A01(r1);
        this.A00 = r1;
    }

    public static String A04(Object obj) {
        if (obj == null) {
            return "";
        }
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Boolean) {
            return obj == Boolean.TRUE ? "true" : "false";
        }
        return obj.toString();
    }

    public static String A05(Object obj, Object obj2, Object obj3, String str) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String A04 = A04(obj);
        String A042 = A04(obj2);
        String A043 = A04(obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        String str3 = ", ";
        if (!TextUtils.isEmpty(A04)) {
            sb.append(str2);
            sb.append(A04);
            str2 = str3;
        }
        if (!TextUtils.isEmpty(A042)) {
            sb.append(str2);
            sb.append(A042);
        } else {
            str3 = str2;
        }
        if (!TextUtils.isEmpty(A043)) {
            sb.append(str3);
            sb.append(A043);
        }
        return sb.toString();
    }

    public static final void A06(C15050mT r8, Object obj, Object obj2, Object obj3, String str, int i) {
        C56582lF r4;
        C14160kx r0 = r8.A00;
        if (r0 != null) {
            r4 = r0.A0C;
        } else {
            r4 = null;
        }
        String str2 = (String) C88904Hw.A0K.A00();
        boolean isLoggable = Log.isLoggable(str2, i);
        if (r4 != null) {
            if (isLoggable) {
                Log.println(i, str2, A05(obj, obj2, obj3, str));
            }
            if (i >= 5) {
                synchronized (r4) {
                    C13020j0.A01(str);
                    C14160kx r2 = ((C15050mT) r4).A00;
                    C63713Cq r3 = r2.A09;
                    if (r3.A03 == null) {
                        synchronized (r3) {
                            if (r3.A03 == null) {
                                C14160kx r1 = r3.A02;
                                ApplicationInfo applicationInfo = r1.A00.getApplicationInfo();
                                String A00 = C13210jK.A00();
                                if (applicationInfo != null) {
                                    String str3 = applicationInfo.processName;
                                    boolean z = false;
                                    if (str3 != null && str3.equals(A00)) {
                                        z = true;
                                    }
                                    r3.A03 = Boolean.valueOf(z);
                                }
                                if ((r3.A03 == null || !r3.A03.booleanValue()) && "com.google.android.gms.analytics".equals(A00)) {
                                    r3.A03 = Boolean.TRUE;
                                }
                                if (r3.A03 == null) {
                                    r3.A03 = Boolean.TRUE;
                                    C56582lF r12 = r1.A0C;
                                    C14160kx.A01(r12);
                                    r12.A08("My process not in the list of running processes");
                                }
                            }
                        }
                    }
                    char c = 'c';
                    if (r3.A03.booleanValue()) {
                        c = 'C';
                    }
                    char charAt = "01VDIWEA?".charAt(i);
                    String str4 = AnonymousClass4H2.A00;
                    String A05 = A05(C56582lF.A00(obj), C56582lF.A00(obj2), C56582lF.A00(obj3), str);
                    int length = String.valueOf(str4).length();
                    StringBuilder sb = new StringBuilder(length + 4 + String.valueOf(A05).length());
                    sb.append("3");
                    sb.append(charAt);
                    sb.append(c);
                    sb.append(str4);
                    sb.append(":");
                    sb.append(A05);
                    String obj4 = sb.toString();
                    if (obj4.length() > 1024) {
                        obj4 = obj4.substring(0, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
                    }
                    C15070mW r13 = r2.A0D;
                    if (r13 != null && ((AbstractC15040mS) r13).A00) {
                        C64563Ga r7 = r13.A03;
                        C15070mW r11 = r7.A01;
                        if (r11.A01.getLong("monitoring".concat(":start"), 0) == 0) {
                            C64563Ga.A00(r7);
                        }
                        if (obj4 == null) {
                            obj4 = "";
                        }
                        synchronized (r7) {
                            SharedPreferences sharedPreferences = r11.A01;
                            String concat = "monitoring".concat(":count");
                            long j = sharedPreferences.getLong(concat, 0);
                            if (j <= 0) {
                                SharedPreferences.Editor edit = r11.A01.edit();
                                edit.putString("monitoring".concat(":value"), obj4);
                                edit.putLong(concat, 1);
                                edit.apply();
                            } else {
                                long j2 = j + 1;
                                SharedPreferences.Editor edit2 = r11.A01.edit();
                                if ((UUID.randomUUID().getLeastSignificantBits() & Long.MAX_VALUE) < Long.MAX_VALUE / j2) {
                                    edit2.putString("monitoring".concat(":value"), obj4);
                                }
                                edit2.putLong(concat, j2);
                                edit2.apply();
                            }
                        }
                    }
                }
            }
        } else if (isLoggable) {
            Log.println(i, str2, A05(obj, obj2, obj3, str));
        }
    }

    public final void A07(Object obj, Object obj2, String str) {
        A06(this, obj, obj2, null, str, 5);
    }

    public final void A08(String str) {
        A06(this, null, null, null, str, 6);
    }

    public final void A09(String str) {
        A06(this, null, null, null, str, 2);
    }

    public final void A0A(String str) {
        A06(this, null, null, null, str, 5);
    }

    public final void A0B(String str, Object obj) {
        A06(this, obj, null, null, str, 3);
    }

    public final void A0C(String str, Object obj) {
        A06(this, obj, null, null, str, 6);
    }

    public final void A0D(String str, Object obj) {
        A06(this, obj, null, null, str, 2);
    }

    public final void A0E(String str, Object obj) {
        A06(this, obj, null, null, str, 5);
    }
}
