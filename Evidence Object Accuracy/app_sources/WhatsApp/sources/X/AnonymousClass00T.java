package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import java.io.File;
import java.util.Arrays;
import java.util.concurrent.Executor;

/* renamed from: X.00T  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00T {
    public static final Object A00 = new Object();

    public static int A00(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return AnonymousClass00W.A00(context, i);
        }
        return context.getResources().getColor(i);
    }

    public static int A01(Context context, String str) {
        if (str != null) {
            return context.checkPermission(str, Process.myPid(), Process.myUid());
        }
        throw new NullPointerException("permission must be non-null");
    }

    public static Context A02(Context context) {
        if (Build.VERSION.SDK_INT >= 24) {
            return AnonymousClass00U.A00(context);
        }
        return null;
    }

    public static ColorStateList A03(Context context, int i) {
        return AnonymousClass00X.A01(context.getTheme(), context.getResources(), i);
    }

    public static Drawable A04(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass00Y.A00(context, i);
        }
        return context.getResources().getDrawable(i);
    }

    public static View A05(Activity activity, int i) {
        if (Build.VERSION.SDK_INT >= 28) {
            return (View) C000500g.A00(activity, i);
        }
        View findViewById = activity.findViewById(i);
        if (findViewById != null) {
            return findViewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this Activity");
    }

    public static File A06(Context context) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass00Y.A01(context);
        }
        File file = new File(context.getApplicationInfo().dataDir, "no_backup");
        synchronized (A00) {
            if (!file.exists() && !file.mkdirs()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to create files subdir ");
                sb.append(file.getPath());
                Log.w("ContextCompat", sb.toString());
            }
        }
        return file;
    }

    public static Executor A07(Context context) {
        if (Build.VERSION.SDK_INT >= 28) {
            return AnonymousClass00a.A00(context);
        }
        return new ExecutorC000000b(new Handler(context.getMainLooper()));
    }

    public static void A08(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass00V.A00(activity);
        } else {
            activity.finish();
        }
    }

    public static void A09(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass00V.A01(activity);
        }
    }

    public static void A0A(Activity activity) {
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
        } else {
            new Handler(activity.getMainLooper()).post(new Runnable(activity) { // from class: X.00c
                public final /* synthetic */ Activity A00;

                {
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    Activity activity2 = this.A00;
                    if (!activity2.isFinishing() && !AnonymousClass0RV.A00(activity2)) {
                        activity2.recreate();
                    }
                }
            });
        }
    }

    public static void A0B(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass00V.A02(activity);
        }
    }

    public static void A0C(Activity activity, AbstractC000600h r3) {
        AnonymousClass09Y r0;
        if (Build.VERSION.SDK_INT >= 21) {
            if (r3 != null) {
                r0 = new AnonymousClass09Y(r3);
            } else {
                r0 = null;
            }
            AnonymousClass00V.A03(activity, r0);
        }
    }

    public static void A0D(Activity activity, AbstractC000600h r3) {
        AnonymousClass09Y r0;
        if (Build.VERSION.SDK_INT >= 21) {
            if (r3 != null) {
                r0 = new AnonymousClass09Y(r3);
            } else {
                r0 = null;
            }
            AnonymousClass00V.A04(activity, r0);
        }
    }

    public static void A0E(Activity activity, String[] strArr, int i) {
        for (String str : strArr) {
            if (TextUtils.isEmpty(str)) {
                StringBuilder sb = new StringBuilder("Permission request for permissions ");
                sb.append(Arrays.toString(strArr));
                sb.append(" must not contain null or empty values");
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (Build.VERSION.SDK_INT >= 23) {
            C000200d.A00(activity, strArr, i);
        } else if (activity instanceof AbstractC000300e) {
            new Handler(Looper.getMainLooper()).post(new RunnableC000400f(activity, strArr, i));
        }
    }

    public static void A0F(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= 26) {
            C000700i.A00(context, intent);
        } else {
            context.startService(intent);
        }
    }

    public static boolean A0G(Activity activity, String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return C000200d.A02(activity, str);
        }
        return false;
    }

    public static boolean A0H(Context context) {
        if (Build.VERSION.SDK_INT >= 24) {
            return AnonymousClass00U.A01(context);
        }
        return false;
    }

    public static File[] A0I(Context context) {
        return Build.VERSION.SDK_INT >= 19 ? AnonymousClass00Z.A00(context) : new File[]{context.getExternalCacheDir()};
    }

    public static File[] A0J(Context context) {
        return Build.VERSION.SDK_INT >= 19 ? AnonymousClass00Z.A01(context, null) : new File[]{context.getExternalFilesDir(null)};
    }
}
