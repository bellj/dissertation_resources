package X;

import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperConfirmationActivity;

/* renamed from: X.5df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119375df extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119375df() {
        C117295Zj.A0p(this, 110);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiMapperConfirmationActivity indiaUpiMapperConfirmationActivity = (IndiaUpiMapperConfirmationActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, indiaUpiMapperConfirmationActivity);
            ActivityC13810kN.A10(A1M, indiaUpiMapperConfirmationActivity);
            ((ActivityC13790kL) indiaUpiMapperConfirmationActivity).A08 = ActivityC13790kL.A0S(r3, A1M, indiaUpiMapperConfirmationActivity, ActivityC13790kL.A0Y(A1M, indiaUpiMapperConfirmationActivity));
            indiaUpiMapperConfirmationActivity.A02 = (C1329668y) A1M.A9d.get();
            indiaUpiMapperConfirmationActivity.A01 = (AnonymousClass130) A1M.A41.get();
            indiaUpiMapperConfirmationActivity.A03 = C117305Zk.A0T(A1M);
        }
    }
}
