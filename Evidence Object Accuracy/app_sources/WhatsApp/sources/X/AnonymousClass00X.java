package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import com.whatsapp.R;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: X.00X  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass00X {
    public static final Object A00 = new Object();
    public static final ThreadLocal A01 = new ThreadLocal();
    public static final WeakHashMap A02 = new WeakHashMap(0);

    public static int A00(Resources.Theme theme, Resources resources) {
        if (Build.VERSION.SDK_INT >= 23) {
            return C05630Qi.A00(theme, resources, R.color.primary_surface);
        }
        return resources.getColor(R.color.primary_surface);
    }

    public static ColorStateList A01(Resources.Theme theme, Resources resources, int i) {
        WeakHashMap weakHashMap;
        ColorStateList colorStateList;
        C018508q r5;
        Resources.Theme theme2;
        C015307g r4 = new C015307g(theme, resources);
        Object obj = A00;
        synchronized (obj) {
            weakHashMap = A02;
            SparseArray sparseArray = (SparseArray) weakHashMap.get(r4);
            if (!(sparseArray == null || sparseArray.size() <= 0 || (r5 = (C018508q) sparseArray.get(i)) == null)) {
                if (!r5.A02.equals(r4.A01.getConfiguration()) || (!((theme2 = r4.A00) == null && r5.A00 == 0) && (theme2 == null || r5.A00 != theme2.hashCode()))) {
                    sparseArray.remove(i);
                } else {
                    colorStateList = r5.A01;
                }
            }
            colorStateList = null;
        }
        if (colorStateList != null) {
            return colorStateList;
        }
        ThreadLocal threadLocal = A01;
        TypedValue typedValue = (TypedValue) threadLocal.get();
        if (typedValue == null) {
            typedValue = new TypedValue();
            threadLocal.set(typedValue);
        }
        boolean z = true;
        resources.getValue(i, typedValue, true);
        int i2 = typedValue.type;
        if (i2 < 28 || i2 > 31) {
            z = false;
        }
        ColorStateList colorStateList2 = null;
        if (!z) {
            try {
                colorStateList2 = C015207f.A01(theme, resources, resources.getXml(i));
            } catch (Exception e) {
                Log.w("ResourcesCompat", "Failed to inflate ColorStateList, leaving it to the framework", e);
            }
        }
        if (colorStateList2 != null) {
            synchronized (obj) {
                SparseArray sparseArray2 = (SparseArray) weakHashMap.get(r4);
                if (sparseArray2 == null) {
                    sparseArray2 = new SparseArray();
                    weakHashMap.put(r4, sparseArray2);
                }
                sparseArray2.append(i, new C018508q(colorStateList2, r4.A01.getConfiguration(), theme));
            }
            return colorStateList2;
        } else if (Build.VERSION.SDK_INT >= 23) {
            return C05630Qi.A01(theme, resources, i);
        } else {
            return resources.getColorStateList(i);
        }
    }

    public static Typeface A02(Context context) {
        if (context.isRestricted()) {
            return null;
        }
        return A03(context, new TypedValue(), null, R.font.payment_icons_regular, 0, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01e8, code lost:
        if (r33 == null) goto L_0x01ea;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Typeface A03(android.content.Context r31, android.util.TypedValue r32, X.AnonymousClass08K r33, int r34, int r35, boolean r36) {
        /*
        // Method dump skipped, instructions count: 949
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00X.A03(android.content.Context, android.util.TypedValue, X.08K, int, int, boolean):android.graphics.Typeface");
    }

    public static Drawable A04(Resources.Theme theme, Resources resources, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return C04010Ka.A00(theme, resources, i);
        }
        return resources.getDrawable(i);
    }

    public static void A05(XmlPullParser xmlPullParser) {
        int i = 1;
        while (true) {
            int next = xmlPullParser.next();
            if (next == 2) {
                i++;
            } else if (next == 3) {
                i--;
            } else {
                continue;
            }
            if (i <= 0) {
                return;
            }
        }
    }
}
