package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* renamed from: X.1n9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37871n9 {
    public static Map A00(AnonymousClass02O r5, List list) {
        HashMap hashMap = new HashMap();
        for (Object obj : list) {
            Object apply = r5.apply(obj);
            Object obj2 = hashMap.get(apply);
            if (obj2 == null) {
                obj2 = new ArrayList();
                hashMap.put(apply, obj2);
            }
            ((List) obj2).add(obj);
        }
        return hashMap;
    }

    public static boolean A01(Object obj, Object[] objArr) {
        for (Object obj2 : objArr) {
            if (obj2 == obj) {
                return true;
            }
            if (!(obj == null || obj2 == null || !obj.equals(obj2))) {
                return true;
            }
        }
        return false;
    }

    public static boolean A02(List list, List list2) {
        if (list != null) {
            if (list2 == null) {
                return list.isEmpty();
            }
            if (!list.isEmpty() && !list2.isEmpty()) {
                return new HashSet(list).equals(new HashSet(list2));
            }
            if (!list.isEmpty()) {
                return false;
            }
        } else if (list2 == null) {
            return true;
        }
        if (list2.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean A03(int[] iArr, int i) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }
}
