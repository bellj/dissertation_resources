package X;

/* renamed from: X.0qu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17500qu implements AbstractC17490qt {
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        if (r9 == null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r9.containsKey("isIntermediateScreen") != true) goto L_0x000d;
     */
    @Override // X.AbstractC17490qt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AZB(android.app.Activity r7, X.C90184Mx r8, java.util.Map r9) {
        /*
            r6 = this;
            java.lang.String r5 = "isIntermediateScreen"
            r4 = 1
            r3 = 0
            if (r9 == 0) goto L_0x000d
            boolean r0 = r9.containsKey(r5)
            r2 = 1
            if (r0 == r4) goto L_0x000e
        L_0x000d:
            r2 = 0
        L_0x000e:
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Boolean"
            if (r2 == 0) goto L_0x0050
            java.lang.Object r0 = r9.get(r5)
            if (r0 == 0) goto L_0x0062
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r2 = r0.booleanValue()
        L_0x001e:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            boolean r0 = r9.containsKey(r0)
            if (r0 != r4) goto L_0x0035
            java.lang.String r0 = "showLoading"
            java.lang.Object r0 = r9.get(r0)
            if (r0 == 0) goto L_0x005c
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r3 = r0.booleanValue()
        L_0x0035:
            if (r7 == 0) goto L_0x0054
            com.whatsapp.wabloks.commerce.ui.view.WaExtensionsBottomsheetModalActivity r7 = (com.whatsapp.wabloks.commerce.ui.view.WaExtensionsBottomsheetModalActivity) r7
            com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel r0 = r7.A03
            X.016 r1 = r0.A02
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            r1.A0B(r0)
            com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel r0 = r7.A03
            X.016 r1 = r0.A03
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r3)
            r1.A0B(r0)
            return
        L_0x0050:
            r2 = 0
            if (r9 != 0) goto L_0x001e
            goto L_0x0035
        L_0x0054:
            java.lang.String r1 = "null cannot be cast to non-null type com.whatsapp.wabloks.commerce.ui.view.WaExtensionsBottomsheetModalActivity"
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        L_0x005c:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        L_0x0062:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17500qu.AZB(android.app.Activity, X.4Mx, java.util.Map):void");
    }
}
