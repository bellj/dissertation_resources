package X;

/* renamed from: X.2P0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2P0 extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;

    public AnonymousClass2P0() {
        super(3226, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamDirectoryClientEntryPoints {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directoryClientEntryPointEventType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "directorySessionId", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "entrypointVersion", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
