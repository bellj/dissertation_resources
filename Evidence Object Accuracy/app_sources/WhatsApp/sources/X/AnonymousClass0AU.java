package X;

import android.os.IInterface;
import android.os.RemoteCallbackList;
import androidx.room.MultiInstanceInvalidationService;

/* renamed from: X.0AU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AU extends RemoteCallbackList {
    public final /* synthetic */ MultiInstanceInvalidationService A00;

    public AnonymousClass0AU(MultiInstanceInvalidationService multiInstanceInvalidationService) {
        this.A00 = multiInstanceInvalidationService;
    }

    @Override // android.os.RemoteCallbackList
    public /* bridge */ /* synthetic */ void onCallbackDied(IInterface iInterface, Object obj) {
        this.A00.A03.remove(Integer.valueOf(((Number) obj).intValue()));
    }
}
