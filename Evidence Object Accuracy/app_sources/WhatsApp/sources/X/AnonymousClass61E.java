package X;

import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.61E  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61E {
    public final AnonymousClass049 A00;
    public final C18600si A01;
    public final C30931Zj A02 = C117305Zk.A0V("PaymentFingerprintKeyStore", "payment-settings");

    public AnonymousClass61E(C16590pI r3, C18600si r4) {
        this.A01 = r4;
        this.A00 = new AnonymousClass049(r3.A00);
    }

    public static AnonymousClass0U4 A00() {
        Signature signature;
        Log.i("FingerprintHelper-helper/get-biometric-crypto-object");
        AnonymousClass04B A01 = A01();
        if (A01 == null || (signature = A01.A00) == null) {
            return null;
        }
        return new AnonymousClass0U4(signature);
    }

    public static AnonymousClass04B A01() {
        Log.i("FingerprintHelper-helper/get-crypto-object");
        try {
            Signature instance = Signature.getInstance("SHA256withECDSA");
            KeyStore instance2 = KeyStore.getInstance("AndroidKeyStore");
            instance2.load(null);
            instance.initSign((PrivateKey) instance2.getKey("payment_bio_key_alias", null));
            return new AnonymousClass04B(instance);
        } catch (IOException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException | CertificateException e) {
            StringBuilder A0k = C12960it.A0k("FingerprintHelper/getCryptoObject: api=");
            A0k.append(Build.VERSION.SDK_INT);
            A0k.append(" error: ");
            Log.e(C12960it.A0d(e.toString(), A0k));
            return null;
        }
    }

    public synchronized int A02() {
        int i;
        JSONObject optJSONObject;
        i = 0;
        try {
            String A04 = this.A01.A04();
            if (!TextUtils.isEmpty(A04) && (optJSONObject = C13000ix.A05(A04).optJSONObject("bio")) != null) {
                i = optJSONObject.optInt("bioState", 0);
                if (i == 1 && A01() == null) {
                    A05();
                    i = 3;
                }
            }
        } catch (JSONException e) {
            this.A02.A0A("getState threw: ", e);
        }
        return i;
    }

    public Pair A03() {
        try {
            return new Pair(A04(2), C124875qG.A00());
        } catch (RuntimeException e) {
            A04(0);
            throw e;
        }
    }

    public final synchronized String A04(int i) {
        String str;
        str = null;
        try {
            C18600si r7 = this.A01;
            JSONObject A0b = C117295Zj.A0b(r7);
            JSONObject optJSONObject = A0b.optJSONObject("bio");
            if (optJSONObject == null) {
                optJSONObject = C117295Zj.A0a();
            }
            optJSONObject.put("v", "1");
            if (i == 0) {
                optJSONObject.remove("bioId");
                optJSONObject.remove("bioPublicKey");
            } else if (i == 2) {
                str = C12990iw.A0n().replace("-", "");
                optJSONObject.put("bioId", str);
            }
            optJSONObject.put("bioState", i);
            A0b.put("bio", optJSONObject);
            C117295Zj.A1E(r7, A0b);
        } catch (JSONException e) {
            this.A02.A0A("PaymentFingerprintKeyStore setState threw: ", e);
        }
        return str;
    }

    public void A05() {
        Log.i("FingerprintHelper-helper/remove-key");
        try {
            KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
            instance.load(null);
            instance.deleteEntry("payment_bio_key_alias");
            A04(0);
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            StringBuilder A0k = C12960it.A0k("FingerprintHelper/removeKey: api=");
            A0k.append(Build.VERSION.SDK_INT);
            A0k.append(" error: ");
            Log.i(C12960it.A0d(e.toString(), A0k));
        }
    }

    public void A06(String str) {
        String str2;
        JSONObject optJSONObject;
        if (str != null) {
            synchronized (this) {
                str2 = null;
                try {
                    String A04 = this.A01.A04();
                    if (!TextUtils.isEmpty(A04) && (optJSONObject = C13000ix.A05(A04).optJSONObject("bio")) != null) {
                        str2 = optJSONObject.optString("bioId", null);
                    }
                } catch (JSONException e) {
                    this.A02.A0A("getId threw: ", e);
                }
            }
            if (str.equals(str2)) {
                A04(1);
                return;
            }
        }
        A05();
    }

    public boolean A07() {
        AnonymousClass049 r1 = this.A00;
        return r1.A06() && r1.A05();
    }

    public boolean A08(AnonymousClass02N r4, AnonymousClass21K r5, byte[] bArr) {
        AnonymousClass04B A01 = A01();
        if (A01 != null) {
            this.A00.A04(new C117855ap(r5, this, bArr), A01, r4);
            return true;
        }
        this.A02.A06("sign: cryptoObject is null");
        A05();
        return false;
    }

    public synchronized byte[] A09() {
        byte[] bArr;
        bArr = null;
        try {
            JSONObject optJSONObject = C117295Zj.A0c(this.A01).optJSONObject("bio");
            if (optJSONObject != null) {
                bArr = Base64.decode(optJSONObject.getString("bioPublicKey"), 2);
            }
        } catch (JSONException e) {
            this.A02.A0A("setPublicKey threw", e);
        }
        return bArr;
    }
}
