package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.whatsapp.R;

/* renamed from: X.2eW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53482eW extends AnonymousClass04v {
    public final /* synthetic */ AnonymousClass1OY A00;

    public C53482eW(AnonymousClass1OY r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        AccessibilityNodeInfo accessibilityNodeInfo = r6.A02;
        accessibilityNodeInfo.setClickable(false);
        if (accessibilityNodeInfo.isLongClickable()) {
            r6.A09(new C007804a(32, view.getResources().getString(R.string.accessibility_action_long_click_react_and_select_messages)));
            boolean hasOnClickListeners = this.A00.hasOnClickListeners();
            accessibilityNodeInfo.setClickable(hasOnClickListeners);
            if (!hasOnClickListeners) {
                r6.A0A(C007804a.A05);
            }
        }
    }
}
