package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;
import java.util.Map;

/* renamed from: X.36A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36A extends C52302aa {
    public final AnonymousClass4LJ A00;
    public final String A01;
    public final Map A02;

    public AnonymousClass36A(Context context, AnonymousClass4LJ r2, String str, String str2, Map map) {
        super(context, str);
        this.A01 = str2;
        this.A02 = map;
        this.A00 = r2;
    }

    @Override // android.text.style.URLSpan, android.text.style.ClickableSpan, X.AbstractC116465Vn
    public void onClick(View view) {
        AnonymousClass4LJ r0 = this.A00;
        String str = this.A01;
        Map map = this.A02;
        UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment = r0.A00;
        userNoticeBottomSheetDialogFragment.A0B.A00(userNoticeBottomSheetDialogFragment.A01(), str, map);
        AnonymousClass12S r2 = userNoticeBottomSheetDialogFragment.A0C;
        int i = 8;
        if (!TextUtils.isEmpty(userNoticeBottomSheetDialogFragment.A0E.A03)) {
            i = 5;
        }
        r2.A01(Integer.valueOf(i));
    }
}
