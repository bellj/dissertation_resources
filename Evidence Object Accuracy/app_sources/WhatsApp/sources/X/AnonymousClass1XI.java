package X;

/* renamed from: X.1XI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XI extends AnonymousClass1X8 implements AnonymousClass1XH, AbstractC16400ox, AbstractC16420oz {
    public int A00 = 0;

    public AnonymousClass1XI(C16150oX r10, AnonymousClass1IS r11, AnonymousClass1XI r12, long j) {
        super(r10, r11, r12, r12.A0y, j, true);
        this.A00 = r12.A00;
    }

    public AnonymousClass1XI(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 42, j);
    }

    @Override // X.AnonymousClass1X8, X.AbstractC15340mz
    public C16460p3 A0G() {
        C16460p3 A0G = super.A0G();
        AnonymousClass009.A05(A0G);
        return A0G;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r9) {
        AnonymousClass1G3 r4 = r9.A03;
        C56962mF r0 = ((C27081Fy) r4.A00).A0G;
        if (r0 == null) {
            r0 = C56962mF.A02;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        C27081Fy r02 = ((C56962mF) A0T.A00).A01;
        if (r02 == null) {
            r02 = C27081Fy.A0i;
        }
        AnonymousClass1G4 A0T2 = r02.A0T();
        C40821sO r03 = ((C27081Fy) A0T2.A00).A0J;
        if (r03 == null) {
            r03 = C40821sO.A0R;
        }
        C81963ur A1B = A1B((C81963ur) r03.A0T(), r9);
        if (A1B != null) {
            A1B.A03();
            C40821sO r3 = (C40821sO) A1B.A00;
            r3.A00 |= 524288;
            r3.A0Q = true;
            A0T2.A03();
            C27081Fy r1 = (C27081Fy) A0T2.A00;
            r1.A0J = (C40821sO) A1B.A02();
            r1.A00 |= 4;
            A0T.A03();
            C56962mF r12 = (C56962mF) A0T.A00;
            r12.A01 = (C27081Fy) A0T2.A02();
            r12.A00 |= 1;
            r4.A03();
            C27081Fy r2 = (C27081Fy) r4.A00;
            r2.A0G = (C56962mF) A0T.A02();
            r2.A00 |= 268435456;
        }
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r7) {
        long j = this.A0I;
        C16150oX r1 = ((AbstractC16130oV) this).A02;
        AnonymousClass009.A05(r1);
        return new AnonymousClass1XI(r1, r7, this, j);
    }

    @Override // X.AnonymousClass1XH
    public int AHc() {
        return this.A00;
    }

    @Override // X.AnonymousClass1XH
    public void Ad8(int i) {
        this.A00 = i;
    }
}
