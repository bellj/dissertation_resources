package X;

/* renamed from: X.5MR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MR extends AnonymousClass1TM {
    public AnonymousClass1TK A00;
    public AbstractC114775Na A01;

    public AnonymousClass5MR(AbstractC114775Na r4) {
        if (r4.A0B() < 1 || r4.A0B() > 2) {
            throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r4.A0B()));
        }
        this.A00 = AnonymousClass1TK.A00(AbstractC114775Na.A00(r4));
        if (r4.A0B() > 1) {
            this.A01 = AbstractC114775Na.A04(r4.A0D(1));
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A00);
        AbstractC114775Na r0 = this.A01;
        if (r0 != null) {
            A00.A06(r0);
        }
        return new AnonymousClass5NZ(A00);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Policy information: ");
        stringBuffer.append(this.A00);
        AbstractC114775Na r4 = this.A01;
        if (r4 != null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            for (int i = 0; i < r4.A0B(); i++) {
                if (stringBuffer2.length() != 0) {
                    stringBuffer2.append(", ");
                }
                AnonymousClass1TN A0D = r4.A0D(i);
                if (!(A0D instanceof AnonymousClass5MK)) {
                    A0D = A0D != null ? new AnonymousClass5MK(AbstractC114775Na.A04(A0D)) : null;
                }
                stringBuffer2.append(A0D);
            }
            stringBuffer.append("[");
            stringBuffer.append(stringBuffer2);
            stringBuffer.append("]");
        }
        return stringBuffer.toString();
    }
}
