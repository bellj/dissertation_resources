package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5vi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128195vi {
    public UserJid A00;
    public AnonymousClass1ZR A01;
    public AnonymousClass1ZR A02;
    public AnonymousClass1ZR A03;
    public String A04;
    public String A05;
    public boolean A06 = false;
    public boolean A07;
    public boolean A08;

    public C128195vi(UserJid userJid, AnonymousClass1ZR r3, AnonymousClass1ZR r4, AnonymousClass1ZR r5, String str, String str2, boolean z, boolean z2) {
        this.A01 = r3;
        this.A04 = str;
        this.A00 = userJid;
        this.A03 = r4;
        this.A02 = r5;
        this.A07 = z;
        this.A08 = z2;
        this.A05 = str2;
    }
}
