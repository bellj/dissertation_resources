package X;

import android.content.Context;
import com.whatsapp.group.NewGroup;

/* renamed from: X.4qE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103114qE implements AbstractC009204q {
    public final /* synthetic */ NewGroup A00;

    public C103114qE(NewGroup newGroup) {
        this.A00 = newGroup;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
