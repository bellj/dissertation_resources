package X;

import com.whatsapp.jid.Jid;
import java.util.List;

/* renamed from: X.2PP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PP extends AnonymousClass2PA {
    public final String A00;
    public final List A01;

    public AnonymousClass2PP(Jid jid, String str, String str2, List list, long j) {
        super(jid, str, j);
        this.A00 = str2;
        this.A01 = list;
    }
}
