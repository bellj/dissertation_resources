package X;

import android.graphics.Bitmap;
import android.view.ViewTreeObserver;
import com.whatsapp.Conversation;

/* renamed from: X.3Nh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC66363Nh implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C89114Is A01;
    public final /* synthetic */ Conversation A02;
    public final /* synthetic */ AnonymousClass4NS A03;
    public final /* synthetic */ AnonymousClass1IS A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ Bitmap[] A07;

    public ViewTreeObserver$OnGlobalLayoutListenerC66363Nh(C89114Is r1, Conversation conversation, AnonymousClass4NS r3, AnonymousClass1IS r4, String str, String str2, Bitmap[] bitmapArr, int i) {
        this.A02 = conversation;
        this.A01 = r1;
        this.A06 = str;
        this.A05 = str2;
        this.A04 = r4;
        this.A03 = r3;
        this.A00 = i;
        this.A07 = bitmapArr;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        Conversation conversation = this.A02;
        if (conversation.A3t != null) {
            C12980iv.A1E(conversation.A3s, this);
            conversation.A3s.setVisibility(0);
            conversation.A3t.AeJ(this.A01, conversation.A3s);
            AbstractC35401hl r0 = conversation.A3t;
            String str = this.A06;
            String str2 = this.A05;
            AnonymousClass1IS r2 = this.A04;
            r0.A9r(this.A03, r2, str, str2, this.A07, this.A00);
        }
    }
}
