package X;

import android.net.UrlQuerySanitizer;
import com.whatsapp.util.Log;
import java.net.URL;
import java.util.Set;

/* renamed from: X.11a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C232711a {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18790t3 A03;
    public final C16590pI A04;
    public final C14820m6 A05;
    public final AnonymousClass018 A06;
    public final C16120oU A07;
    public final C18810t5 A08;
    public final C18800t4 A09;
    public final C232811b A0A;
    public final C19930uu A0B;

    public C232711a(AbstractC15710nm r1, C14900mE r2, C15570nT r3, C18790t3 r4, C16590pI r5, C14820m6 r6, AnonymousClass018 r7, C16120oU r8, C18810t5 r9, C18800t4 r10, C232811b r11, C19930uu r12) {
        this.A04 = r5;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A0B = r12;
        this.A03 = r4;
        this.A07 = r8;
        this.A06 = r7;
        this.A09 = r10;
        this.A08 = r9;
        this.A05 = r6;
        this.A0A = r11;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
        if (r3.A00.A0B() == false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1KZ A00(X.AbstractC33611ef r7, java.lang.String r8) {
        /*
            r6 = this;
            X.11b r3 = r6.A0A
            X.0m6 r0 = r3.A01
            android.content.SharedPreferences r4 = r0.A00
            java.lang.String r2 = "sticker_store_backoff_time"
            r0 = 0
            long r4 = r4.getLong(r2, r0)
            long r1 = java.lang.System.currentTimeMillis()
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0020
            X.0sm r0 = r3.A00
            boolean r1 = r0.A0B()
            r0 = 1
            if (r1 != 0) goto L_0x0021
        L_0x0020:
            r0 = 0
        L_0x0021:
            r2 = 0
            if (r0 != 0) goto L_0x002a
            java.lang.String r0 = "StickerPackNetworkProvider/getStickerPackById skipped due to backoff time"
            com.whatsapp.util.Log.i(r0)
        L_0x0029:
            return r2
        L_0x002a:
            java.lang.String r0 = "https://static.whatsapp.net/sticker?id="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: 1oD -> 0x005f
            r1.<init>(r0)     // Catch: 1oD -> 0x005f
            r1.append(r8)     // Catch: 1oD -> 0x005f
            java.lang.String r0 = "&lg="
            r1.append(r0)     // Catch: 1oD -> 0x005f
            X.018 r0 = r6.A06     // Catch: 1oD -> 0x005f
            android.content.Context r0 = r0.A00     // Catch: 1oD -> 0x005f
            java.util.Locale r0 = X.AnonymousClass018.A00(r0)     // Catch: 1oD -> 0x005f
            java.lang.String r0 = X.AbstractC27291Gt.A05(r0)     // Catch: 1oD -> 0x005f
            r1.append(r0)     // Catch: 1oD -> 0x005f
            java.lang.String r0 = r1.toString()     // Catch: 1oD -> 0x005f
            X.1oI r0 = r6.A01(r7, r0, r2)     // Catch: 1oD -> 0x005f
            r3.A00()     // Catch: 1oD -> 0x005f
            if (r0 == 0) goto L_0x0029
            java.util.List r1 = r0.A01     // Catch: 1oD -> 0x005f
            r0 = 0
            java.lang.Object r0 = r1.get(r0)     // Catch: 1oD -> 0x005f
            X.1KZ r0 = (X.AnonymousClass1KZ) r0     // Catch: 1oD -> 0x005f
            return r0
        L_0x005f:
            r1 = move-exception
            r3.A01()
            java.lang.String r0 = "StickerPackNetworkProvider/getStickerPackById failed"
            com.whatsapp.util.Log.e(r0, r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C232711a.A00(X.1ef, java.lang.String):X.1KZ");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 19, insn: 0x02a1: MOVE  (r2 I:??[OBJECT, ARRAY]) = (r19 I:??[OBJECT, ARRAY]), block:B:76:0x02a1
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final X.C38561oI A01(
/*
[686] Method generation error in method: X.11a.A01(X.1ef, java.lang.String, java.lang.String):X.1oI, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r21v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public void A02(URL url, long j) {
        int i;
        Set<String> parameterSet = new UrlQuerySanitizer(url.toString()).getParameterSet();
        if (parameterSet.contains("cat")) {
            i = 0;
        } else if (parameterSet.contains("id")) {
            i = 2;
        } else {
            i = 1;
            if (!parameterSet.contains("img")) {
                StringBuilder sb = new StringBuilder("StickerPackNetworkProvider/log query type ");
                sb.append(-1);
                sb.append("is not supported: ");
                sb.append(url);
                Log.e(sb.toString());
                return;
            }
        }
        C38571oK r1 = new C38571oK();
        r1.A01 = Long.valueOf(j);
        r1.A02 = url.getQuery();
        r1.A00 = Integer.valueOf(i);
        this.A07.A07(r1);
    }
}
