package X;

import android.os.Handler;
import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.2L4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2L4 extends Handler implements AnonymousClass2L5 {
    public final /* synthetic */ HandlerThreadC26611Ed A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2L4(HandlerThreadC26611Ed r2) {
        super(r2.getLooper());
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            Log.i("xmpp/connection/recv/sending_channel_ready");
            HandlerThreadC26611Ed r3 = this.A00;
            r3.A05 = (AbstractC43471x5) message.obj;
            ((Handler) r3.A02).obtainMessage(0, new HandlerC43461x4(r3)).sendToTarget();
        } else if (i == 1) {
            HandlerThreadC26611Ed r2 = this.A00;
            if (message.obj == r2.A07) {
                r2.A06(true);
            }
        } else if (i == 2) {
            HandlerThreadC26611Ed r0 = this.A00;
            Object obj = message.obj;
            HandlerC49412Kq r32 = r0.A04;
            r32.sendMessageDelayed(r32.obtainMessage(1, obj), 45000);
        }
    }
}
