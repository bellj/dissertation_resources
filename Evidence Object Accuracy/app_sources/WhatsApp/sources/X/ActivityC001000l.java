package X;

import X.AbstractC001200n;
import X.ActivityC001000l;
import X.AnonymousClass074;
import X.AnonymousClass0K6;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.activity.ImmLeaksCleaner;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.00l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ActivityC001000l extends AbstractActivityC001100m implements AbstractC001200n, AbstractC001400p, AbstractC001500q, AbstractC001600r, AbstractC001700s, AbstractC001800t, AbstractC001900u, AbstractC002000v {
    public static final String A09 = "android:support:activity-result";
    public int A00;
    public AbstractC009404s A01;
    public AnonymousClass05C A02;
    public final AnonymousClass052 A03;
    public final AnonymousClass051 A04;
    public final C009704w A05;
    public final C009804x A06;
    public final C010004z A07;
    public final AtomicInteger A08;

    @Deprecated
    public static void A0E() {
    }

    public ActivityC001000l() {
        this.A05 = new C009704w();
        this.A06 = new C009804x(this);
        this.A07 = new C010004z(this);
        this.A04 = new AnonymousClass051(new AnonymousClass050(this));
        this.A08 = new AtomicInteger();
        this.A03 = new AnonymousClass052(this);
        C009804x r1 = this.A06;
        if (r1 != null) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 19) {
                r1.A00(new AnonymousClass054() { // from class: androidx.activity.ComponentActivity$3
                    @Override // X.AnonymousClass054
                    public void AWQ(AnonymousClass074 r2, AbstractC001200n r3) {
                        Window window;
                        View peekDecorView;
                        if (r2 == AnonymousClass074.ON_STOP && (window = ActivityC001000l.this.getWindow()) != null && (peekDecorView = window.peekDecorView()) != null) {
                            AnonymousClass0K6.A00(peekDecorView);
                        }
                    }
                });
            }
            this.A06.A00(new AnonymousClass054() { // from class: androidx.activity.ComponentActivity$4
                @Override // X.AnonymousClass054
                public void AWQ(AnonymousClass074 r4, AbstractC001200n r5) {
                    if (r4 == AnonymousClass074.ON_DESTROY) {
                        ActivityC001000l r2 = ActivityC001000l.this;
                        r2.A05.A01 = null;
                        if (!r2.isChangingConfigurations()) {
                            r2.AHb().A00();
                        }
                    }
                }
            });
            this.A06.A00(new AnonymousClass054() { // from class: androidx.activity.ComponentActivity$5
                @Override // X.AnonymousClass054
                public void AWQ(AnonymousClass074 r2, AbstractC001200n r3) {
                    ActivityC001000l r0 = ActivityC001000l.this;
                    r0.A0N();
                    r0.A06.A01(this);
                }
            });
            if (19 <= i && i <= 23) {
                this.A06.A00(new ImmLeaksCleaner(this));
            }
            this.A07.A00.A02(new AnonymousClass05A() { // from class: X.059
                @Override // X.AnonymousClass05A
                public final Bundle AbH() {
                    return ActivityC001000l.A0C(ActivityC001000l.this);
                }
            }, A09);
            A0R(new AbstractC009204q() { // from class: X.05B
                @Override // X.AbstractC009204q
                public final void AOc(Context context) {
                    ActivityC001000l.A0I(ActivityC001000l.this);
                }
            });
            return;
        }
        throw new IllegalStateException("getLifecycle() returned null in ComponentActivity's constructor. Please make sure you are lazily constructing your Lifecycle in the first call to getLifecycle() rather than relying on field initialization.");
    }

    public ActivityC001000l(int i) {
        this();
        this.A00 = i;
    }

    public static /* synthetic */ Bundle A0C(ActivityC001000l r4) {
        Bundle bundle = new Bundle();
        AnonymousClass052 r42 = r4.A03;
        Map map = r42.A04;
        bundle.putIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS", new ArrayList<>(map.values()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS", new ArrayList<>(map.keySet()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS", new ArrayList<>(r42.A00));
        bundle.putBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT", (Bundle) r42.A02.clone());
        bundle.putSerializable("KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT", r42.A01);
        return bundle;
    }

    private void A0D() {
        getWindow().getDecorView().setTag(R.id.view_tree_lifecycle_owner, this);
        getWindow().getDecorView().setTag(R.id.view_tree_view_model_store_owner, this);
        getWindow().getDecorView().setTag(R.id.view_tree_saved_state_registry_owner, this);
    }

    public static /* synthetic */ void A0I(ActivityC001000l r2) {
        Bundle A00 = r2.A07.A00.A00(A09);
        if (A00 != null) {
            r2.A03.A03(A00);
        }
    }

    public void A0N() {
        if (this.A02 == null) {
            AnonymousClass05D r0 = (AnonymousClass05D) getLastNonConfigurationInstance();
            if (r0 != null) {
                this.A02 = r0.A00;
            }
            if (this.A02 == null) {
                this.A02 = new AnonymousClass05C();
            }
        }
    }

    @Deprecated
    public void A0O() {
        getLastNonConfigurationInstance();
    }

    public void A0P() {
    }

    public final void A0Q(AnonymousClass052 r3, AnonymousClass05J r4, AnonymousClass05K r5) {
        StringBuilder sb = new StringBuilder("activity_rq#");
        sb.append(this.A08.getAndIncrement());
        r3.A01(r4, r5, this, sb.toString());
    }

    public final void A0R(AbstractC009204q r3) {
        C009704w r1 = this.A05;
        if (r1.A01 != null) {
            r3.AOc(r1.A01);
        }
        r1.A00.add(r3);
    }

    public final void A0S(AbstractC009204q r2) {
        this.A05.A00.remove(r2);
    }

    public final void A0T(AnonymousClass05J r2, AnonymousClass05K r3) {
        A0Q(this.A03, r2, r3);
    }

    @Override // X.AbstractC001600r
    public final AnonymousClass052 AAb() {
        return this.A03;
    }

    @Override // X.AbstractC001800t
    public AbstractC009404s ACV() {
        Bundle bundle;
        if (getApplication() != null) {
            AbstractC009404s r2 = this.A01;
            if (r2 != null) {
                return r2;
            }
            Application application = getApplication();
            if (getIntent() != null) {
                bundle = getIntent().getExtras();
            } else {
                bundle = null;
            }
            AnonymousClass05E r22 = new AnonymousClass05E(application, bundle, this);
            this.A01 = r22;
            return r22;
        }
        throw new IllegalStateException("Your activity is not yet attached to the Application instance. You can't request ViewModel before onCreate call.");
    }

    @Override // X.AbstractActivityC001100m, X.AbstractC001200n
    public AbstractC009904y ADr() {
        return this.A06;
    }

    @Override // X.AbstractC001700s
    public final AnonymousClass051 AEk() {
        return this.A04;
    }

    @Override // X.AbstractC001500q
    public final AnonymousClass058 AGO() {
        return this.A07.A00;
    }

    @Override // X.AbstractC001400p
    public AnonymousClass05C AHb() {
        if (getApplication() != null) {
            A0N();
            return this.A02;
        }
        throw new IllegalStateException("Your activity is not yet attached to the Application instance. You can't request ViewModel before onCreate call.");
    }

    @Override // android.app.Activity
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A0D();
        super.addContentView(view, layoutParams);
    }

    @Override // android.app.Activity
    @Deprecated
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!this.A03.A06(intent, i, i2)) {
            super.onActivityResult(i, i2, intent);
        }
    }

    @Override // android.app.Activity
    public void onBackPressed() {
        this.A04.A00();
    }

    @Override // X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        this.A07.A00(bundle);
        C009704w r0 = this.A05;
        r0.A01 = this;
        for (AbstractC009204q r02 : r0.A00) {
            r02.AOc(this);
        }
        super.onCreate(bundle);
        AnonymousClass05H.A00(this);
        int i = this.A00;
        if (i != 0) {
            setContentView(i);
        }
    }

    @Override // android.app.Activity, X.AbstractC000300e
    @Deprecated
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (!this.A03.A06(new Intent().putExtra("androidx.activity.result.contract.extra.PERMISSIONS", strArr).putExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS", iArr), i, -1) && Build.VERSION.SDK_INT >= 23) {
            super.onRequestPermissionsResult(i, strArr, iArr);
        }
    }

    @Override // android.app.Activity
    public final Object onRetainNonConfigurationInstance() {
        AnonymousClass05D r0;
        AnonymousClass05C r1 = this.A02;
        if (r1 == null && ((r0 = (AnonymousClass05D) getLastNonConfigurationInstance()) == null || (r1 = r0.A00) == null)) {
            return null;
        }
        AnonymousClass05D r02 = new AnonymousClass05D();
        r02.A00 = r1;
        return r02;
    }

    @Override // X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        C009804x r2 = this.A06;
        if (r2 != null) {
            AnonymousClass05I r1 = AnonymousClass05I.CREATED;
            r2.A06("setCurrentState");
            r2.A05(r1);
        }
        super.onSaveInstanceState(bundle);
        this.A07.A01(bundle);
    }

    @Override // android.app.Activity
    public void reportFullyDrawn() {
        try {
            if (AnonymousClass05M.A02()) {
                AnonymousClass05M.A00();
            }
            int i = Build.VERSION.SDK_INT;
            if (i > 19 || (i == 19 && AnonymousClass00T.A01(this, "android.permission.UPDATE_DEVICE_STATS") == 0)) {
                super.reportFullyDrawn();
            }
        } finally {
            AnonymousClass05M.A01();
        }
    }

    @Override // android.app.Activity
    public void setContentView(int i) {
        A0D();
        super.setContentView(i);
    }

    @Override // android.app.Activity
    public void setContentView(View view) {
        A0D();
        super.setContentView(view);
    }

    @Override // android.app.Activity
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A0D();
        super.setContentView(view, layoutParams);
    }
}
