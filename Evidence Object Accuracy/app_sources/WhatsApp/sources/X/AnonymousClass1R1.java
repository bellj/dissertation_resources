package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1R1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1R1 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100444lv();
    public final String A00;
    public final String A01;
    public final String A02;
    public final boolean A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1R1(Parcel parcel) {
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A03 = parcel.readByte() != 0;
        this.A00 = parcel.readString();
    }

    public AnonymousClass1R1(String str, String str2, String str3, boolean z) {
        this.A02 = str;
        this.A01 = str2;
        this.A03 = z;
        this.A00 = str3;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeByte(this.A03 ? (byte) 1 : 0);
        parcel.writeString(this.A00);
    }
}
