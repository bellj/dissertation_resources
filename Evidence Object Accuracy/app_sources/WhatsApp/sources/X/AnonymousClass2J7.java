package X;

/* renamed from: X.2J7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2J7 implements AnonymousClass2J8 {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2J7(C48302Fl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2J8
    public C59462ul A86(AnonymousClass2K4 r20, AnonymousClass2K3 r21, C48122Ek r22, AnonymousClass2K1 r23, AnonymousClass1B4 r24, String str, String str2, boolean z, boolean z2) {
        AnonymousClass01J r1 = this.A00.A03;
        AbstractC15710nm r2 = (AbstractC15710nm) r1.A4o.get();
        AnonymousClass018 r12 = (AnonymousClass018) r1.ANb.get();
        C14850m9 r13 = (C14850m9) r1.A04.get();
        C16340oq r10 = (C16340oq) r1.A5z.get();
        C17170qN r11 = (C17170qN) r1.AMt.get();
        return new C59462ul(r2, (C14900mE) r1.A8X.get(), (C16430p0) r1.A5y.get(), r20, r21, r22, r23, r24, r10, r11, r12, r13, (AbstractC14440lR) r1.ANe.get(), str, str2, z, z2);
    }
}
