package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import com.google.android.finsky.externalreferrer.IGetInstallReferrerService;

/* renamed from: X.0V9  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0V9 implements ServiceConnection {
    public final AbstractC12060hJ A00;
    public final /* synthetic */ C06110Sf A01;

    public /* synthetic */ AnonymousClass0V9(C06110Sf r1, AbstractC12060hJ r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public static IGetInstallReferrerService A00(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
        if (queryLocalInterface instanceof IGetInstallReferrerService) {
            return (IGetInstallReferrerService) queryLocalInterface;
        }
        return new C65883Lj(iBinder);
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        AnonymousClass0R6.A00("Install Referrer service connected.");
        C06110Sf r1 = this.A01;
        r1.A02 = A00(iBinder);
        r1.A00 = 2;
        this.A00.ARQ(0);
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        AnonymousClass0R6.A01("Install Referrer service disconnected.");
        C06110Sf r1 = this.A01;
        r1.A02 = null;
        r1.A00 = 0;
    }
}
