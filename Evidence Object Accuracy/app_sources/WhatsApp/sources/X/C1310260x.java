package X;

import java.util.Arrays;

/* renamed from: X.60x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C1310260x {
    public final int A00;
    public final int A01;
    public final String A02;
    public final String A03;
    public final Object[] A04;

    public C1310260x(String str, int i) {
        this.A03 = str;
        this.A00 = 0;
        this.A02 = null;
        this.A01 = i;
        this.A04 = null;
    }

    public C1310260x(String str, Object[] objArr, int i) {
        this.A03 = null;
        this.A02 = str;
        this.A00 = i;
        this.A01 = 0;
        this.A04 = objArr;
    }

    public C1310260x(Object[] objArr, int i) {
        this(null, objArr, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C1310260x.class != obj.getClass()) {
            return false;
        }
        C1310260x r5 = (C1310260x) obj;
        if (this.A01 == r5.A01 && this.A00 == r5.A00) {
            String str = this.A03;
            String str2 = r5.A03;
            if (str != null ? !(str2 == null || !str.equals(str2)) : str2 == null) {
                String str3 = this.A02;
                String str4 = r5.A02;
                if (str3 == null) {
                    if (str4 == null) {
                        return true;
                    }
                } else if (str4 != null && str3.equals(str4)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int hashCode() {
        int i;
        String str = this.A03;
        if (str != null) {
            i = str.hashCode() + 31;
        } else {
            i = 1;
        }
        String str2 = this.A02;
        if (str2 != null) {
            i = (i * 31) + str2.hashCode();
        }
        return (((i * 31) + this.A00) * 31) + this.A01;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("TextConfiguration{text='");
        A0k.append(this.A03);
        A0k.append('\'');
        A0k.append(", textResId=");
        A0k.append(this.A00);
        A0k.append(", formatArgs=");
        A0k.append(Arrays.toString(this.A04));
        return C12970iu.A0v(A0k);
    }
}
