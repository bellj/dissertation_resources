package X;

/* renamed from: X.1u1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41741u1 implements AbstractC21730xt {
    public final /* synthetic */ C20790wK A00;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r1, String str) {
    }

    public C41741u1(C20790wK r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r7, String str) {
        AnonymousClass1V8 A0C = r7.A0C();
        AnonymousClass1V8.A01(A0C, "disappearing_mode");
        this.A00.A04.A05(A0C.A06(A0C.A0H("duration"), "duration"), A0C.A09(A0C.A0H("t"), "t") * 1000);
    }
}
