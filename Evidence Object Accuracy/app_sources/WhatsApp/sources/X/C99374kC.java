package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.wearable.ConnectionConfiguration;

/* renamed from: X.4kC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99374kC implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        ConnectionConfiguration[] connectionConfigurationArr = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                connectionConfigurationArr = (ConnectionConfiguration[]) C95664e9.A0K(parcel, ConnectionConfiguration.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78183oV(connectionConfigurationArr, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78183oV[i];
    }
}
