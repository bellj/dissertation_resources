package X;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

/* renamed from: X.5Hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113515Hw extends AbstractCollection<V> {
    public final C113515Hw ancestor;
    public final Collection ancestorDelegate;
    public Collection delegate;
    public final Object key;
    public final /* synthetic */ AbstractC80933tC this$0;

    public C113515Hw(AbstractC80933tC r2, Object obj, Collection collection, C113515Hw r5) {
        Collection delegate;
        this.this$0 = r2;
        this.key = obj;
        this.delegate = collection;
        this.ancestor = r5;
        if (r5 == null) {
            delegate = null;
        } else {
            delegate = r5.getDelegate();
        }
        this.ancestorDelegate = delegate;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean add(Object obj) {
        refreshIfEmpty();
        boolean isEmpty = this.delegate.isEmpty();
        boolean add = this.delegate.add(obj);
        if (add) {
            AbstractC80933tC.access$208(this.this$0);
            if (isEmpty) {
                addToMap();
            }
        }
        return add;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean addAll(Collection collection) {
        if (collection.isEmpty()) {
            return false;
        }
        int size = size();
        boolean addAll = this.delegate.addAll(collection);
        if (!addAll) {
            return addAll;
        }
        AbstractC80933tC.access$212(this.this$0, this.delegate.size() - size);
        if (size != 0) {
            return addAll;
        }
        addToMap();
        return addAll;
    }

    public void addToMap() {
        C113515Hw r0 = this.ancestor;
        if (r0 != null) {
            r0.addToMap();
        } else {
            this.this$0.map.put(this.key, this.delegate);
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public void clear() {
        int size = size();
        if (size != 0) {
            this.delegate.clear();
            AbstractC80933tC.access$220(this.this$0, size);
            removeIfEmpty();
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        refreshIfEmpty();
        return this.delegate.contains(obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean containsAll(Collection collection) {
        refreshIfEmpty();
        return this.delegate.containsAll(collection);
    }

    @Override // java.util.Collection, java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        refreshIfEmpty();
        return this.delegate.equals(obj);
    }

    public C113515Hw getAncestor() {
        return this.ancestor;
    }

    public Collection getDelegate() {
        return this.delegate;
    }

    public Object getKey() {
        return this.key;
    }

    @Override // java.util.Collection, java.lang.Object
    public int hashCode() {
        refreshIfEmpty();
        return this.delegate.hashCode();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        refreshIfEmpty();
        return new AnonymousClass5DK(this);
    }

    public void refreshIfEmpty() {
        Collection collection;
        C113515Hw r0 = this.ancestor;
        if (r0 != null) {
            r0.refreshIfEmpty();
            if (this.ancestor.getDelegate() != this.ancestorDelegate) {
                throw new ConcurrentModificationException();
            }
        } else if (this.delegate.isEmpty() && (collection = (Collection) this.this$0.map.get(this.key)) != null) {
            this.delegate = collection;
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean remove(Object obj) {
        refreshIfEmpty();
        boolean remove = this.delegate.remove(obj);
        if (remove) {
            AbstractC80933tC.access$210(this.this$0);
            removeIfEmpty();
        }
        return remove;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean removeAll(Collection collection) {
        if (collection.isEmpty()) {
            return false;
        }
        int size = size();
        boolean removeAll = this.delegate.removeAll(collection);
        if (!removeAll) {
            return removeAll;
        }
        AbstractC80933tC.access$212(this.this$0, this.delegate.size() - size);
        removeIfEmpty();
        return removeAll;
    }

    public void removeIfEmpty() {
        C113515Hw r0 = this.ancestor;
        if (r0 != null) {
            r0.removeIfEmpty();
        } else if (this.delegate.isEmpty()) {
            this.this$0.map.remove(this.key);
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean retainAll(Collection collection) {
        int size = size();
        boolean retainAll = this.delegate.retainAll(collection);
        if (retainAll) {
            AbstractC80933tC.access$212(this.this$0, this.delegate.size() - size);
            removeIfEmpty();
        }
        return retainAll;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public int size() {
        refreshIfEmpty();
        return this.delegate.size();
    }

    @Override // java.util.AbstractCollection, java.lang.Object
    public String toString() {
        refreshIfEmpty();
        return this.delegate.toString();
    }
}
