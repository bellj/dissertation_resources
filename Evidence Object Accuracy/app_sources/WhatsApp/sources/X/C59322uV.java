package X;

import com.whatsapp.biz.catalog.view.CatalogHeader;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape3S0300000_I1;

/* renamed from: X.2uV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59322uV extends AbstractC75723kJ {
    public final CatalogHeader A00;

    public C59322uV(AnonymousClass12P r3, C15570nT r4, CatalogHeader catalogHeader, UserJid userJid) {
        super(catalogHeader);
        this.A00 = catalogHeader;
        catalogHeader.setUp(userJid);
        if (!r4.A0F(userJid)) {
            catalogHeader.setOnTextClickListener(new ViewOnClickCListenerShape3S0300000_I1(this, r3, userJid, 2));
        }
    }

    @Override // X.AbstractC75723kJ
    public /* bridge */ /* synthetic */ void A09(AbstractC89244Jf r1) {
    }
}
