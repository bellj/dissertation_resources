package X;

import java.io.Serializable;

/* renamed from: X.1My  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28381My implements Serializable {
    public static final long serialVersionUID = 0;
    public final Object[] elements;

    public C28381My(Object[] objArr) {
        this.elements = objArr;
    }

    public Object readResolve() {
        return AnonymousClass1Mr.copyOf(this.elements);
    }
}
