package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.inappsupport.ui.ContactUsActivity;
import com.whatsapp.util.Log;

/* renamed from: X.2oN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58242oN extends AbstractC52172aN {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ ContactUsActivity A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58242oN(Context context, Context context2, ContactUsActivity contactUsActivity) {
        super(context);
        this.A01 = contactUsActivity;
        this.A00 = context2;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        ContactUsActivity contactUsActivity = this.A01;
        Class AFa = contactUsActivity.A0I.A02().AFa();
        Log.i(C12960it.A0b("PAY: ContactUsActivity starting settings ", AFa));
        Context context = this.A00;
        context.startActivity(C12990iw.A0D(context, AFa));
        AbstractC16870pt ACx = contactUsActivity.A0I.A02().ACx();
        if (ACx != null) {
            AnonymousClass3FW r3 = new AnonymousClass3FW(null, new AnonymousClass3FW[0]);
            r3.A01("hc_entrypoint", "wa_settings_support");
            r3.A01("app_type", "consumer");
            ACx.AKi(r3, C12960it.A0V(), 39, "settings_contact_us", null);
        }
    }
}
