package X;

import android.os.ConditionVariable;

/* renamed from: X.0og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16240og extends AbstractC16220oe {
    public int A00;
    public boolean A01;
    public boolean A02;
    public final ConditionVariable A03 = new ConditionVariable(false);
    public volatile int A04 = 3;
    public volatile boolean A05;
    public volatile boolean A06;

    public C16240og(AnonymousClass01H r3) {
        super(r3);
    }

    public void A05(AbstractC18870tC r3) {
        int i = this.A04;
        if (i == 1) {
            r3.ARA();
        } else if (i == 2) {
            r3.AR9();
        } else if (i == 3) {
            r3.ARB();
        }
        A03(r3);
    }
}
