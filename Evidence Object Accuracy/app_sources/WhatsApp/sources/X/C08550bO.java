package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

/* renamed from: X.0bO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08550bO implements AbstractC12510i2 {
    public final BlockingQueue A00 = new DelayQueue();
    public final C10800fE[] A01;

    public C08550bO(int i) {
        this.A01 = new C10800fE[i];
        int i2 = 0;
        while (true) {
            C10800fE[] r1 = this.A01;
            if (i2 < r1.length) {
                r1[i2] = new C10800fE(this);
                C10800fE r2 = this.A01[i2];
                StringBuilder sb = new StringBuilder("GCD-Thread #");
                sb.append(i2);
                r2.setName(sb.toString());
                this.A01[i2].start();
                i2++;
            } else {
                return;
            }
        }
    }
}
