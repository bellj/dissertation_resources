package X;

import android.app.ActivityManager;
import android.os.Build;

/* renamed from: X.0LU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0LU {
    public static boolean A00() {
        if (Build.VERSION.SDK_INT >= 29) {
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (runningAppProcessInfo.importance != 100) {
                return false;
            }
        }
        return true;
    }
}
