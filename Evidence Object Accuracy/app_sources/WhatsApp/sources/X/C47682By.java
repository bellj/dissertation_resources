package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

/* renamed from: X.2By  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47682By {
    public static final byte[] A08 = {87, 65, 77, 80, 83, 73, 68, 1};
    public int A00;
    public int A01;
    public C94114bH A02;
    public String A03 = "00000000-0000-0000-0000-000000000000";
    public ArrayList A04 = new ArrayList();
    public final int A05;
    public final AnonymousClass16F A06;
    public final RandomAccessFile A07;

    public C47682By(AnonymousClass16F r2, RandomAccessFile randomAccessFile, int i) {
        this.A05 = i;
        this.A07 = randomAccessFile;
        this.A06 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.util.SparseArray A00() {
        /*
            r12 = this;
            android.util.SparseArray r2 = new android.util.SparseArray
            r2.<init>()
            r9 = 0
        L_0x0006:
            int r0 = r12.A01
            if (r9 >= r0) goto L_0x007b
            int r0 = r12.A00
            if (r9 != r0) goto L_0x005f
            X.4bH r6 = r12.A02
        L_0x0010:
            int r3 = r6.A02
            r1 = 8
            r0 = 0
            if (r3 > r1) goto L_0x0018
            r0 = 1
        L_0x0018:
            if (r0 != 0) goto L_0x005c
            r5 = 8
            if (r3 <= r1) goto L_0x004b
            byte[] r4 = new byte[r3]
            java.io.RandomAccessFile r3 = r6.A05     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            long r0 = r6.A03     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            r3.seek(r0)     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            r1 = 0
            int r0 = r6.A02     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            r3.readFully(r4, r1, r0)     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            int r0 = r6.A01     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            int r0 = r0 * 9
            int r0 = r0 + 56
            int r0 = r0 + r5
            long r0 = (long) r0     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            r3.seek(r0)     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            r0 = 1
            r3.writeBoolean(r0)     // Catch: EOFException -> 0x003d, IOException -> 0x0043
            goto L_0x004c
        L_0x003d:
            X.16F r0 = r6.A04
            r0.A06()
            goto L_0x0048
        L_0x0043:
            X.16F r0 = r6.A04
            r0.A08()
        L_0x0048:
            r0.A05()
        L_0x004b:
            r4 = 0
        L_0x004c:
            if (r4 == 0) goto L_0x005c
            r2.put(r9, r4)
            java.util.ArrayList r0 = r12.A04
            java.lang.Object r1 = r0.get(r9)
            X.2C3 r1 = (X.AnonymousClass2C3) r1
            r0 = 1
            r1.A02 = r0
        L_0x005c:
            int r9 = r9 + 1
            goto L_0x0006
        L_0x005f:
            java.io.RandomAccessFile r8 = r12.A07
            java.util.ArrayList r1 = r12.A04
            java.lang.Object r0 = r1.get(r9)
            X.2C3 r0 = (X.AnonymousClass2C3) r0
            int r10 = r0.A01
            java.lang.Object r0 = r1.get(r9)
            X.2C3 r0 = (X.AnonymousClass2C3) r0
            int r11 = r0.A00
            X.16F r7 = r12.A06
            X.4bH r6 = new X.4bH
            r6.<init>(r7, r8, r9, r10, r11)
            goto L_0x0010
        L_0x007b:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47682By.A00():android.util.SparseArray");
    }

    public void A01() {
        RandomAccessFile randomAccessFile = this.A07;
        randomAccessFile.seek((long) A08.length);
        byte[] bArr = new byte[36];
        randomAccessFile.read(bArr);
        this.A03 = new String(bArr, DefaultCrypto.UTF_8);
        this.A01 = randomAccessFile.read();
        this.A00 = randomAccessFile.read();
        ArrayList arrayList = this.A04;
        arrayList.clear();
        boolean z = false;
        for (int i = 0; i < this.A01; i++) {
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            boolean readBoolean = randomAccessFile.readBoolean();
            if (readBoolean && readInt <= 8) {
                Log.e("queuefile/loadFromFile see locked empty mini event buffer");
                z = true;
                readBoolean = false;
            }
            arrayList.add(new AnonymousClass2C3(readInt, readInt2, readBoolean));
        }
        int i2 = this.A00;
        this.A02 = new C94114bH(this.A06, randomAccessFile, i2, ((AnonymousClass2C3) arrayList.get(i2)).A01, ((AnonymousClass2C3) arrayList.get(i2)).A00);
        if (z) {
            A02();
        }
    }

    public final void A02() {
        try {
            RandomAccessFile randomAccessFile = this.A07;
            randomAccessFile.seek(54);
            randomAccessFile.writeByte(this.A01);
            randomAccessFile.writeByte(this.A00);
            for (int i = 0; i < this.A01; i++) {
                ArrayList arrayList = this.A04;
                randomAccessFile.writeInt(((AnonymousClass2C3) arrayList.get(i)).A01);
                randomAccessFile.writeInt(((AnonymousClass2C3) arrayList.get(i)).A00);
                randomAccessFile.writeBoolean(((AnonymousClass2C3) arrayList.get(i)).A02);
            }
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder("queuefile/flushMetaToFile failed to write ");
            sb.append(e.getMessage());
            Log.e(sb.toString());
        }
    }

    public void A03(int i) {
        ArrayList arrayList = this.A04;
        ((AnonymousClass2C3) arrayList.get(i)).A01 = 8;
        ((AnonymousClass2C3) arrayList.get(i)).A02 = false;
        try {
            RandomAccessFile randomAccessFile = this.A07;
            randomAccessFile.seek((long) ((i * 9) + 56));
            randomAccessFile.writeInt(8);
            randomAccessFile.skipBytes(4);
            randomAccessFile.writeBoolean(false);
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder("queuefile/dropsentdata ");
            sb.append(e.getMessage());
            Log.e(sb.toString());
        }
    }

    public void A04(String str, int i) {
        if (str.length() > 36) {
            str = str.substring(0, 36);
        }
        this.A03 = str;
        this.A01 = 1;
        this.A00 = 0;
        RandomAccessFile randomAccessFile = this.A07;
        randomAccessFile.setLength(33024);
        randomAccessFile.write(A08);
        randomAccessFile.write(this.A03.getBytes(DefaultCrypto.UTF_8));
        randomAccessFile.writeByte(1);
        randomAccessFile.writeByte(0);
        randomAccessFile.writeInt(8);
        randomAccessFile.writeInt(32768);
        randomAccessFile.writeBoolean(false);
        randomAccessFile.seek(256);
        byte[] bArr = C94114bH.A06;
        bArr[5] = (byte) (i & 255);
        bArr[6] = (byte) ((i >> 8) & 255);
        randomAccessFile.write(bArr);
        this.A04.add(new AnonymousClass2C3(8, 32768, false));
        this.A02 = new C94114bH(this.A06, randomAccessFile, this.A00, 8, 32768);
    }

    public boolean A05() {
        for (int i = 0; i < this.A01; i++) {
            if (((AnonymousClass2C3) this.A04.get(i)).A01 > 8) {
                return true;
            }
        }
        return false;
    }

    public boolean A06(byte[] bArr, int i) {
        int i2;
        ArrayList arrayList = this.A04;
        int i3 = this.A00;
        if (((AnonymousClass2C3) arrayList.get(i3)).A01 <= 8 && ((AnonymousClass2C3) arrayList.get(i3)).A02) {
            Log.e("queuefile/writeBytes in privatestats see locked empty mini event buffer");
            i3 = this.A00;
            ((AnonymousClass2C3) arrayList.get(i3)).A02 = false;
        }
        if (!((AnonymousClass2C3) arrayList.get(i3)).A02) {
            int length = bArr.length - 0;
            if (length < i) {
                i = length;
            }
            C94114bH r2 = this.A02;
            if (i <= r2.A00 - r2.A02) {
                ((AnonymousClass2C3) arrayList.get(this.A00)).A01 += r2.A00(bArr, i);
                A02();
                return true;
            } else if (((AnonymousClass2C3) arrayList.get(i3)).A00 < 65536) {
                try {
                    this.A07.setLength(65792);
                    ((AnonymousClass2C3) arrayList.get(this.A00)).A00 = 65536;
                    C94114bH r1 = this.A02;
                    r1.A00 = 65536;
                    if (i <= 65536 - r1.A02) {
                        i2 = r1.A00(bArr, i);
                        ((AnonymousClass2C3) arrayList.get(this.A00)).A01 += i2;
                    } else {
                        i2 = 0;
                    }
                    A02();
                    if (i2 > 0) {
                        return true;
                    }
                } catch (IOException e) {
                    StringBuilder sb = new StringBuilder("queuefile/writeBytes failed to write ");
                    sb.append(e.getMessage());
                    Log.e(sb.toString());
                }
            }
        }
        return false;
    }
}
