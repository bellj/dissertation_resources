package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.6B5  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B5 implements AbstractC136276Lx {
    public final byte[] A00;

    public AnonymousClass6B5(byte[] bArr) {
        this.A00 = bArr;
    }

    @Override // X.AbstractC136276Lx
    public byte[] A9N(byte[] bArr, byte[] bArr2) {
        C29361Rw A00 = C29361Rw.A00();
        byte[] bArr3 = A00.A02.A01;
        byte[][] A06 = C16050oM.A06(C32891cu.A01(AnonymousClass4F7.A00(A00.A01, new AnonymousClass2ST(this.A00)), bArr3, null, 64), 32, 32);
        byte[] bArr4 = A06[0];
        byte[] bArr5 = A06[1];
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr4, "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec, new IvParameterSpec(bArr2));
            byte[] doFinal = instance.doFinal(bArr);
            try {
                Mac instance2 = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
                instance2.init(new SecretKeySpec(bArr5, DefaultCrypto.HMAC_SHA256));
                instance2.update(bArr2);
                byte[] A05 = C16050oM.A05(bArr2, bArr3, instance2.doFinal(doFinal), doFinal);
                A00.destroy();
                return A05;
            } catch (InvalidKeyException | NoSuchAlgorithmException e) {
                throw new AssertionError(e);
            }
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e2) {
            throw new AssertionError(e2);
        }
    }
}
