package X;

import android.text.TextUtils;
import com.whatsapp.group.GroupParticipantsSearchFragment;

/* renamed from: X.3Oz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66803Oz implements AnonymousClass07L {
    public final /* synthetic */ GroupParticipantsSearchFragment A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66803Oz(GroupParticipantsSearchFragment groupParticipantsSearchFragment) {
        this.A00 = groupParticipantsSearchFragment;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        C36661kH r1 = this.A00.A05;
        r1.A00 = str;
        if (TextUtils.isEmpty(str)) {
            r1.A01(r1.A02);
            return false;
        }
        r1.getFilter().filter(str);
        return false;
    }
}
