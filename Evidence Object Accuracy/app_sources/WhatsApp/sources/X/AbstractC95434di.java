package X;

/* renamed from: X.4di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95434di {
    public static int A00(byte[] bArr, int i) {
        int i2 = i + 1;
        int i3 = i2 + 1;
        return (bArr[i3 + 1] << 24) | (bArr[i] & 255) | ((bArr[i2] & 255) << 8) | ((bArr[i3] & 255) << 16);
    }

    public static void A03(byte[] bArr, int i, long j) {
        A01(bArr, (int) (j >>> 32), i);
        A01(bArr, (int) (j & 4294967295L), i + 4);
    }

    public static void A04(byte[] bArr, int i, long j) {
        A02(bArr, (int) (4294967295L & j), i);
        A02(bArr, (int) (j >>> 32), i + 4);
    }

    public static void A01(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) (i >>> 24);
        int i3 = i2 + 1;
        bArr[i3] = (byte) (i >>> 16);
        int i4 = i3 + 1;
        C72463ee.A0W(bArr, i, i4);
        bArr[i4 + 1] = (byte) i;
    }

    public static void A02(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) i;
        int i3 = i2 + 1;
        C72463ee.A0W(bArr, i, i3);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i >>> 16);
        bArr[i4 + 1] = (byte) (i >>> 24);
    }
}
