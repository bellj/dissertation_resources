package X;

import java.util.Arrays;

/* renamed from: X.1Ji  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27821Ji {
    public final long A00;
    public final C34321fx A01;
    public final byte[] A02;

    public String toString() {
        return "SyncdKeyData";
    }

    public C27821Ji(C34321fx r1, byte[] bArr, long j) {
        this.A02 = bArr;
        this.A00 = j;
        this.A01 = r1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C27821Ji)) {
            return false;
        }
        C27821Ji r7 = (C27821Ji) obj;
        if (this.A00 != r7.A00 || !Arrays.equals(this.A02, r7.A02) || !this.A01.equals(r7.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (Arrays.hashCode(new Object[]{Long.valueOf(this.A00), this.A01}) * 31) + Arrays.hashCode(this.A02);
    }
}
