package X;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;

/* renamed from: X.2dr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC53322dr extends AnonymousClass04T {
    public AnonymousClass2UH A00;
    public BottomSheetBehavior A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DialogC53322dr(android.content.Context r5, int r6) {
        /*
            r4 = this;
            if (r6 != 0) goto L_0x0017
            android.util.TypedValue r3 = new android.util.TypedValue
            r3.<init>()
            android.content.res.Resources$Theme r2 = r5.getTheme()
            r1 = 2130968678(0x7f040066, float:1.7546016E38)
            r0 = 1
            boolean r0 = r2.resolveAttribute(r1, r3, r0)
            if (r0 == 0) goto L_0x002a
            int r6 = r3.resourceId
        L_0x0017:
            r4.<init>(r5, r6)
            r0 = 1
            r4.A02 = r0
            r4.A03 = r0
            X.3sa r0 = new X.3sa
            r0.<init>(r4)
            r4.A00 = r0
            r4.A01()
            return
        L_0x002a:
            r6 = 2131952431(0x7f13032f, float:1.9541305E38)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.DialogC53322dr.<init>(android.content.Context, int):void");
    }

    public final View A03(View view, ViewGroup.LayoutParams layoutParams, int i) {
        View A0N = C12980iv.A0N(getContext(), R.layout.design_bottom_sheet_dialog);
        ViewGroup A0P = C12980iv.A0P(A0N, R.id.coordinator);
        if (i != 0 && view == null) {
            view = C12960it.A0F(getLayoutInflater(), A0P, i);
        }
        ViewGroup A0P2 = C12980iv.A0P(A0P, R.id.design_bottom_sheet);
        BottomSheetBehavior A00 = BottomSheetBehavior.A00(A0P2);
        this.A01 = A00;
        A00.A0E = this.A00;
        A00.A0J = this.A02;
        if (layoutParams == null) {
            A0P2.addView(view);
        } else {
            A0P2.addView(view, layoutParams);
        }
        C12960it.A10(A0P.findViewById(R.id.touch_outside), this, 1);
        AnonymousClass028.A0g(A0P2, new C53582eg(this));
        A0P2.setOnTouchListener(new View$OnTouchListenerC101294nI(this));
        return A0N;
    }

    @Override // X.AnonymousClass04T, android.app.Dialog
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.clearFlags(67108864);
                window.addFlags(Integer.MIN_VALUE);
            }
            window.setLayout(-1, -1);
        }
    }

    @Override // android.app.Dialog
    public void onStart() {
        super.onStart();
        BottomSheetBehavior bottomSheetBehavior = this.A01;
        if (bottomSheetBehavior != null && bottomSheetBehavior.A0B == 5) {
            bottomSheetBehavior.A0M(4);
        }
    }

    @Override // android.app.Dialog
    public void setCancelable(boolean z) {
        super.setCancelable(z);
        if (this.A02 != z) {
            this.A02 = z;
            BottomSheetBehavior bottomSheetBehavior = this.A01;
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.A0J = z;
            }
        }
    }

    @Override // android.app.Dialog
    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        if (z && !this.A02) {
            this.A02 = true;
        }
        this.A03 = z;
        this.A04 = true;
    }

    @Override // X.AnonymousClass04T, android.app.Dialog
    public void setContentView(int i) {
        super.setContentView(A03(null, null, i));
    }

    @Override // X.AnonymousClass04T, android.app.Dialog
    public void setContentView(View view) {
        super.setContentView(A03(view, null, 0));
    }

    @Override // X.AnonymousClass04T, android.app.Dialog
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(A03(view, layoutParams, 0));
    }
}
