package X;

import android.util.Pair;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1n4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37821n4 {
    public final List A00;

    public C37821n4(List list) {
        this.A00 = list;
    }

    public static double A00(List list) {
        Iterator it = list.iterator();
        long j = 0;
        while (it.hasNext()) {
            j += (long) ((Number) it.next()).intValue();
        }
        return ((double) j) / ((double) list.size());
    }

    public final Pair A01() {
        List<Number> list = this.A00;
        double A00 = A00(list);
        double A002 = A00(list);
        double d = 0.0d;
        for (Number number : list) {
            double intValue = ((double) number.intValue()) - A002;
            d += intValue * intValue;
        }
        double sqrt = Math.sqrt(d / ((double) list.size()));
        return new Pair(Integer.valueOf((int) (A00 - (2.0d * sqrt))), Integer.valueOf((int) (A00 + (sqrt * 1.0d))));
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(int r7) {
        /*
            r6 = this;
            java.util.List r5 = r6.A00
            int r1 = r5.size()
            r0 = 10
            if (r1 < r0) goto L_0x003a
            android.util.Pair r2 = r6.A01()
            java.lang.Object r1 = r2.first
            java.lang.Number r1 = (java.lang.Number) r1
            int r0 = r1.intValue()
            if (r7 < r0) goto L_0x0036
            java.lang.Object r1 = r2.second
            java.lang.Number r1 = (java.lang.Number) r1
            int r0 = r1.intValue()
            if (r7 > r0) goto L_0x0036
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)
            r5.add(r0)
        L_0x0029:
            int r1 = r5.size()
            r0 = 20
            if (r1 <= r0) goto L_0x0035
            r0 = 0
            r5.remove(r0)
        L_0x0035:
            return
        L_0x0036:
            r5.add(r1)
            goto L_0x0029
        L_0x003a:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r7)
            r5.add(r0)
            int r1 = r5.size()
            r0 = 10
            if (r1 < r0) goto L_0x0035
            android.util.Pair r4 = r6.A01()
            r3 = 0
        L_0x004e:
            int r0 = r5.size()
            if (r3 >= r0) goto L_0x0035
            java.lang.Object r0 = r5.get(r3)
            java.lang.Number r0 = (java.lang.Number) r0
            int r2 = r0.intValue()
            java.lang.Object r1 = r4.first
            java.lang.Number r1 = (java.lang.Number) r1
            int r0 = r1.intValue()
            if (r2 < r0) goto L_0x0072
            java.lang.Object r1 = r4.second
            java.lang.Number r1 = (java.lang.Number) r1
            int r0 = r1.intValue()
            if (r2 <= r0) goto L_0x0075
        L_0x0072:
            r5.set(r3, r1)
        L_0x0075:
            int r3 = r3 + 1
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37821n4.A02(int):void");
    }
}
