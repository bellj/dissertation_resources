package X;

import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0PN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PN {
    public long A00 = -1;
    public Interpolator A01;
    public AbstractC12530i4 A02;
    public boolean A03;
    public final AnonymousClass0Y9 A04 = new C02630Dh(this);
    public final ArrayList A05 = new ArrayList();

    public void A00() {
        if (this.A03) {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                ((AnonymousClass0QQ) it.next()).A00();
            }
            this.A03 = false;
        }
    }

    public void A01() {
        if (!this.A03) {
            Iterator it = this.A05.iterator();
            while (it.hasNext()) {
                AnonymousClass0QQ r5 = (AnonymousClass0QQ) it.next();
                long j = this.A00;
                if (j >= 0) {
                    r5.A07(j);
                }
                Interpolator interpolator = this.A01;
                if (interpolator != null) {
                    r5.A08(interpolator);
                }
                if (this.A02 != null) {
                    r5.A09(this.A04);
                }
                r5.A01();
            }
            this.A03 = true;
        }
    }
}
