package X;

import android.content.Context;
import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Locale;

/* renamed from: X.3Yk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69223Yk implements AbstractC116745Wq {
    public int A00;
    public AnonymousClass242 A01;
    public final Context A02;
    public final View A03;
    public final LinearLayoutManager A04;
    public final RecyclerView A05;
    public final AnonymousClass018 A06;
    public final C54342gZ A07;
    public final ArrayList A08 = C12960it.A0l();

    public C69223Yk(Context context, View view, AnonymousClass018 r6) {
        this.A02 = context;
        this.A06 = r6;
        this.A03 = view.findViewById(R.id.avocado_picker_header);
        RecyclerView A0R = C12990iw.A0R(view, R.id.avocado_header_recycler);
        this.A05 = A0R;
        A0R.A0h = false;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        this.A04 = linearLayoutManager;
        linearLayoutManager.A1Q(0);
        A0R.setLayoutManager(linearLayoutManager);
        C54342gZ r0 = new C54342gZ(this);
        this.A07 = r0;
        A0R.setAdapter(r0);
    }

    @Override // X.AbstractC116745Wq
    public View AHZ() {
        return this.A03;
    }

    @Override // X.AbstractC116745Wq
    public void ATQ(int i) {
        ArrayList arrayList;
        C74753ij r1;
        int i2 = this.A00;
        if (i != i2) {
            int i3 = 0;
            while (true) {
                arrayList = this.A08;
                if (i3 < arrayList.size()) {
                    if (i2 == ((AnonymousClass4OU) arrayList.get(i3)).A00) {
                        break;
                    }
                    i3++;
                } else {
                    i3 = -1;
                    break;
                }
            }
            this.A00 = i;
            int i4 = 0;
            while (true) {
                if (i4 < arrayList.size()) {
                    if (i == ((AnonymousClass4OU) arrayList.get(i4)).A00) {
                        break;
                    }
                    i4++;
                } else {
                    i4 = -1;
                    break;
                }
            }
            if (i4 != i3) {
                LinearLayoutManager linearLayoutManager = this.A04;
                int A19 = linearLayoutManager.A19();
                int A1B = linearLayoutManager.A1B();
                int i5 = ((A1B - A19) << 1) / 5;
                int i6 = i4 - i5;
                if (i6 < A19) {
                    if (i6 < 0) {
                        i6 = 0;
                    }
                    r1 = new C74753ij(this.A03.getContext(), this);
                    ((AbstractC05520Pw) r1).A00 = i6;
                } else {
                    int i7 = i4 + i5;
                    if (i7 > A1B) {
                        if (i7 >= linearLayoutManager.A07()) {
                            i7 = linearLayoutManager.A07() - 1;
                        }
                        r1 = new C74753ij(this.A03.getContext(), this);
                        ((AbstractC05520Pw) r1).A00 = i7;
                    }
                }
                linearLayoutManager.A0R(r1);
            }
            this.A07.A02();
        }
    }

    @Override // X.AbstractC116745Wq
    public void Abx(AnonymousClass242 r4) {
        this.A01 = r4;
        if (r4 != null) {
            int A00 = r4.A00();
            if (A00 < 0) {
                C12990iw.A1R("AvocadoPickerHeader/setContentPicker/getCurrentPageIndex < 0", Locale.US, new Object[0]);
                A00 = 0;
            }
            ATQ(A00);
        }
    }
}
