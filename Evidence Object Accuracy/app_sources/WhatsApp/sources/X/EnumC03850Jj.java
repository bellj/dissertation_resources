package X;

/* renamed from: X.0Jj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03850Jj {
    DEFAULT(0),
    /* Fake field, exist only in values array */
    FADE(1),
    /* Fake field, exist only in values array */
    NONE(2);
    
    public final String value;

    EnumC03850Jj(int i) {
        this.value = r2;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.value;
    }
}
