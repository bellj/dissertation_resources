package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.3Hb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC64833Hb {
    public Canvas A00;
    public final float A01;
    public final Paint A02;
    public final List A03 = C12960it.A0l();

    public AbstractC64833Hb(Bitmap bitmap, Paint paint, PointF pointF, float f, int i) {
        if (bitmap != null) {
            Canvas canvas = new Canvas(bitmap);
            float f2 = (float) i;
            canvas.scale(f2, f2);
            if (pointF != null) {
                canvas.translate(-pointF.x, -pointF.y);
            }
            this.A00 = canvas;
        }
        this.A01 = f;
        this.A02 = paint;
    }

    public static List A00(JSONObject jSONObject) {
        JSONArray jSONArray = jSONObject.getJSONArray("points");
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < jSONArray.length(); i += 2) {
            A0l.add(new PointF(((float) jSONArray.getInt(i)) / 100.0f, ((float) jSONArray.getInt(i + 1)) / 100.0f));
        }
        return A0l;
    }

    public void A01(Bitmap bitmap, PointF pointF, int i) {
        Canvas canvas = new Canvas(bitmap);
        float f = (float) i;
        canvas.scale(f, f);
        if (pointF != null) {
            canvas.translate(-pointF.x, -pointF.y);
        }
        this.A00 = canvas;
    }

    public void A02(Canvas canvas) {
        AnonymousClass33P r2 = (AnonymousClass33P) this;
        if (canvas != null) {
            for (PointF pointF : ((AbstractC64833Hb) r2).A03) {
                r2.A06(canvas, ((AbstractC64833Hb) r2).A02, pointF.x, pointF.y, (int) r2.A01);
            }
        }
    }

    public void A03(PointF pointF, long j) {
    }

    public void A04(PointF pointF, long j) {
        AnonymousClass33P r2 = (AnonymousClass33P) this;
        List list = ((AbstractC64833Hb) r2).A03;
        if (list.isEmpty() || !list.get(C12980iv.A0C(list)).equals(pointF)) {
            list.add(pointF);
            Canvas canvas = ((AbstractC64833Hb) r2).A00;
            if (canvas != null) {
                r2.A06(canvas, ((AbstractC64833Hb) r2).A02, pointF.x, pointF.y, (int) r2.A01);
            }
        }
    }

    public void A05(JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        for (PointF pointF : this.A03) {
            jSONArray.put((int) (pointF.x * 100.0f));
            jSONArray.put((int) (pointF.y * 100.0f));
        }
        jSONObject.put("points", jSONArray);
        jSONObject.put("width", (int) (this.A01 * 100.0f));
    }
}
