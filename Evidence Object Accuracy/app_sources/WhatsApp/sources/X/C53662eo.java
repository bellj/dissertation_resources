package X;

import android.os.Bundle;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.whatsapp.ClearableEditText;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2eo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53662eo extends AnonymousClass0DW {
    public final /* synthetic */ ClearableEditText A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53662eo(View view, ClearableEditText clearableEditText) {
        super(view);
        this.A00 = clearableEditText;
    }

    @Override // X.AnonymousClass0DW
    public int A07(float f, float f2) {
        int i = (int) f;
        int i2 = (int) f2;
        return this.A00.getClearBounds() ? 1 : Integer.MIN_VALUE;
    }

    @Override // X.AnonymousClass0DW
    public void A0C(AnonymousClass04Z r5, int i) {
        if (i == 1) {
            AccessibilityNodeInfo accessibilityNodeInfo = r5.A02;
            accessibilityNodeInfo.setClickable(true);
            accessibilityNodeInfo.addAction(16);
            ClearableEditText clearableEditText = this.A00;
            accessibilityNodeInfo.setContentDescription(clearableEditText.getContext().getString(R.string.entry_clear_button_content_description));
            accessibilityNodeInfo.setBoundsInParent(clearableEditText.getClearBounds());
        }
    }

    @Override // X.AnonymousClass0DW
    public void A0D(List list) {
        if (this.A00.getClearIconDrawable() != null) {
            list.add(C12960it.A0V());
        }
    }

    @Override // X.AnonymousClass0DW
    public boolean A0G(int i, int i2, Bundle bundle) {
        if (i != 1 || i2 != 16) {
            return false;
        }
        ClearableEditText clearableEditText = this.A00;
        View.OnClickListener onClickListener = clearableEditText.A01;
        if (onClickListener != null) {
            onClickListener.onClick(clearableEditText);
        }
        C12990iw.A1G(clearableEditText);
        clearableEditText.requestFocus();
        return true;
    }
}
