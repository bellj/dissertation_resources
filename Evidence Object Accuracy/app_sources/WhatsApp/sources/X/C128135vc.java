package X;

/* renamed from: X.5vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128135vc {
    public String A00 = "PENDING";
    public final C14900mE A01;
    public final C18640sm A02;
    public final C16590pI A03;
    public final C18650sn A04;
    public final C18610sj A05;
    public final C129565xv A06;
    public final C30931Zj A07 = C117305Zk.A0V("PaymentsComplianceManager", "infra");

    public C128135vc(C14900mE r3, C18640sm r4, C16590pI r5, C18650sn r6, C18610sj r7, C129565xv r8) {
        this.A03 = r5;
        this.A01 = r3;
        this.A06 = r8;
        this.A05 = r7;
        this.A02 = r4;
        this.A04 = r6;
    }
}
