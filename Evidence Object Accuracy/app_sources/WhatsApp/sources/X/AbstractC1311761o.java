package X;

import android.graphics.Matrix;
import android.os.Handler;
import java.io.File;

/* renamed from: X.61o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public interface AbstractC1311761o {
    void A5k(AbstractC136166Lg v);

    void A5l(C128425w5 v);

    void A7Y(AbstractC129405xf v, C129535xs v2, AbstractC1311561m v3, AnonymousClass6LQ v4, AnonymousClass6LR v5, String str, int i, int i2);

    boolean A8z(AbstractC129405xf v);

    void AA3(int i, int i2);

    int ABB();

    AbstractC130695zp ABG();

    int AGc(int i);

    int AHu();

    boolean AI9(int i);

    void AIr(Matrix matrix, int i, int i2, int i3);

    boolean AJz();

    boolean AK6();

    boolean AKp(float[] fArr);

    void ALV(AbstractC129405xf v, C128385w1 v2);

    void ATI(int i);

    void AaL(AbstractC136166Lg v);

    void AaM(C128425w5 v);

    void Abs(Handler handler);

    void Ac9(AbstractC136156Lf v);

    void AcO(C125385rA v);

    void Acd(AbstractC129405xf v, int i);

    void AdE(AbstractC129405xf v, int i);

    boolean AdG(Matrix matrix, int i, int i2, int i3, int i4, boolean z);

    void AeN(AbstractC129405xf v, File file);

    void AeV(AbstractC129405xf v, boolean z);

    void Aef(AbstractC129405xf v);

    void Aeh(C129455xk v, AnonymousClass60B v2);

    boolean isConnected();
}
