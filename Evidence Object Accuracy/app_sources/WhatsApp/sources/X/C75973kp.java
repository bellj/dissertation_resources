package X;

import com.facebook.msys.mci.DataTask;
import com.facebook.msys.mci.NetworkSession;

/* renamed from: X.3kp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75973kp extends AbstractRunnableC47782Cq {
    public final /* synthetic */ DataTask A00;
    public final /* synthetic */ NetworkSession A01;
    public final /* synthetic */ C64183Eo A02;
    public final /* synthetic */ C37561md A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C75973kp(DataTask dataTask, NetworkSession networkSession, C64183Eo r4, C37561md r5) {
        super("StreamingUploadDataTask_initial_ask_for_data");
        this.A02 = r4;
        this.A03 = r5;
        this.A01 = networkSession;
        this.A00 = dataTask;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A01.canHandleStreamingUploadUpdate(this.A00.mTaskIdentifier);
    }
}
