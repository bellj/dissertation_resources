package X;

import android.util.SparseArray;
import com.whatsapp.R;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0l4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14230l4 extends C14240l5 {
    public final C14260l7 A00;
    public final C94244bU A01;
    public final List A02;
    public final Map A03;
    public final Set A04;

    public C14230l4(AnonymousClass4Yz r2, C14260l7 r3, C94244bU r4, C90764Pd r5, AbstractC17450qp r6, List list, Map map, Set set) {
        super(r2, r5, r6);
        this.A00 = r3;
        this.A03 = map;
        this.A01 = r4;
        this.A04 = set;
        if (list == null) {
            this.A02 = Collections.emptyList();
        } else {
            this.A02 = list;
        }
    }

    public static C14230l4 A00(C14260l7 r8, List list) {
        C90764Pd A01 = C65093Ic.A01(r8.A02.A01);
        SparseArray sparseArray = r8.A01;
        AbstractC17450qp r5 = (AbstractC17450qp) sparseArray.get(R.id.bk_context_key_interpreter_extensions);
        if (r5 == null) {
            r5 = C65093Ic.A00().A0A;
        }
        sparseArray.get(R.id.bk_context_key_logging_id);
        return new C14230l4(AnonymousClass4Yz.A00, r8, null, A01, r5, list, null, null);
    }
}
