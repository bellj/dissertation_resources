package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.4aM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93554aM {
    public static final int[] A03 = {R.string.settings_media_quality_auto, R.string.settings_media_quality_best_quality, R.string.settings_media_quality_data_saver};
    public final Context A00;
    public final C14820m6 A01;
    public final AnonymousClass018 A02;

    public C93554aM(Context context, C14820m6 r2, AnonymousClass018 r3) {
        this.A00 = context;
        this.A02 = r3;
        this.A01 = r2;
    }
}
