package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;

/* renamed from: X.1Jc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27761Jc implements AbstractC21730xt {
    public C41311tK A00;
    public final C15570nT A01;
    public final C41191t7 A02;
    public final C234711u A03;
    public final C233411h A04;
    public final C18930tI A05;
    public final C232510y A06;
    public final C17220qS A07;
    public final AbstractC14440lR A08;

    public C27761Jc(C15570nT r1, C41191t7 r2, C234711u r3, C233411h r4, C18930tI r5, C232510y r6, C17220qS r7, AbstractC14440lR r8) {
        this.A01 = r1;
        this.A08 = r8;
        this.A07 = r7;
        this.A04 = r4;
        this.A05 = r5;
        this.A06 = r6;
        this.A03 = r3;
        this.A02 = r2;
    }

    public final synchronized C41311tK A00() {
        C41311tK r1;
        r1 = this.A00;
        AnonymousClass009.A05(r1);
        this.A00 = null;
        return r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("sync-request-handler/onDeliveryFailure iqId:");
        sb.append(str);
        Log.e(sb.toString());
        this.A08.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this, 12));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r10, String str) {
        StringBuilder sb = new StringBuilder("sync-request-handler/onError iqId:");
        sb.append(str);
        Log.e(sb.toString());
        AnonymousClass1V8 A0E = r10.A0E("error");
        if (A0E != null) {
            int A05 = A0E.A05("code", -1);
            if (A05 != -1) {
                String A0I = A0E.A0I("text", null);
                Long l = null;
                try {
                    long A08 = A0E.A08("backoff", -1);
                    if (A08 != -1) {
                        l = Long.valueOf(A08 * 1000);
                    }
                } catch (AnonymousClass1V9 e) {
                    Log.e("SyncRequestHandler/getServerProvidedBackoffInMs the provided backoff is not an integral value.", e);
                }
                this.A08.Ab2(new RunnableBRunnable0Shape0S1201000_I0(this, l, A0I, A05, 1));
                return;
            }
            StringBuilder sb2 = new StringBuilder("Expected attribute code in ");
            sb2.append(r10);
            throw new AnonymousClass1V9(sb2.toString());
        }
        StringBuilder sb3 = new StringBuilder("Expected child error in ");
        sb3.append(r10);
        throw new AnonymousClass1V9(sb3.toString());
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        StringBuilder sb = new StringBuilder("sync-request-handler/onSuccess iqId:");
        sb.append(str);
        Log.i(sb.toString());
        try {
            this.A08.Ab2(new RunnableBRunnable0Shape2S0200000_I0_2(this, 16, new C41321tL(r5)));
        } catch (AnonymousClass1K1 | AnonymousClass1t4 e) {
            A00();
            this.A08.Ab2(new RunnableBRunnable0Shape2S0200000_I0_2(this, 17, e));
        }
    }
}
