package X;

import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RadioGroup;
import com.whatsapp.R;
import com.whatsapp.backup.google.GoogleDriveNewUserSetupActivity;

/* renamed from: X.3NM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NM implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ GoogleDriveNewUserSetupActivity A00;

    public AnonymousClass3NM(GoogleDriveNewUserSetupActivity googleDriveNewUserSetupActivity) {
        this.A00 = googleDriveNewUserSetupActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        int i;
        int measuredHeight;
        GoogleDriveNewUserSetupActivity googleDriveNewUserSetupActivity = this.A00;
        int i2 = 0;
        int measuredHeight2 = ((ViewGroup) googleDriveNewUserSetupActivity.findViewById(R.id.scrollbars)).getChildAt(0).getMeasuredHeight();
        if (googleDriveNewUserSetupActivity.A04.getVisibility() == 0) {
            i = googleDriveNewUserSetupActivity.A04.getMeasuredHeight();
        } else {
            i = 0;
        }
        int i3 = measuredHeight2 - i;
        if (googleDriveNewUserSetupActivity.A03.getVisibility() == 0) {
            measuredHeight = 0;
        } else {
            measuredHeight = googleDriveNewUserSetupActivity.A03.getMeasuredHeight();
        }
        int i4 = i3 + measuredHeight;
        Point point = new Point();
        C12970iu.A17(googleDriveNewUserSetupActivity, point);
        boolean z = false;
        if (((double) ((float) point.y)) < ((double) ((float) i4)) * 0.7d) {
            z = true;
        }
        googleDriveNewUserSetupActivity.A04.setVisibility(C12990iw.A0A(z));
        RadioGroup radioGroup = googleDriveNewUserSetupActivity.A03;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        int i7 = z ? 1 : 0;
        radioGroup.setVisibility(C13010iy.A00(i5));
        View findViewById = googleDriveNewUserSetupActivity.findViewById(R.id.gdrive_new_user_setup_select_frequency_message);
        if (z) {
            i2 = 8;
        }
        findViewById.setVisibility(i2);
        C12980iv.A1F(googleDriveNewUserSetupActivity.A03, this);
    }
}
