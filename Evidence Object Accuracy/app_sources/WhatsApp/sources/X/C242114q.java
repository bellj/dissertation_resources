package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.14q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242114q implements AbstractC20010v4 {
    public final C14830m7 A00;
    public final AnonymousClass018 A01;
    public final C22320yt A02;
    public final C16510p9 A03;
    public final C19990v2 A04;
    public final C20290vW A05;
    public final C21610xh A06;
    public final C15240mn A07;
    public final C21380xK A08;
    public final C20090vC A09;
    public final C21620xi A0A;
    public final AnonymousClass12H A0B;
    public final C20850wQ A0C;
    public final C16490p7 A0D;

    public C242114q(C14830m7 r1, AnonymousClass018 r2, C22320yt r3, C16510p9 r4, C19990v2 r5, C20290vW r6, C21610xh r7, C15240mn r8, C21380xK r9, C20090vC r10, C21620xi r11, AnonymousClass12H r12, C20850wQ r13, C16490p7 r14) {
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = r5;
        this.A08 = r9;
        this.A01 = r2;
        this.A07 = r8;
        this.A09 = r10;
        this.A0B = r12;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
        this.A0A = r11;
        this.A0D = r14;
        this.A0C = r13;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0056, code lost:
        if (r2 != null) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A00(X.AbstractC14640lm r7) {
        /*
            r6 = this;
            X.0p7 r0 = r6.A0D
            X.0on r3 = r0.get()
            X.0op r5 = r3.A03     // Catch: all -> 0x0066
            java.lang.String r4 = "SELECT COUNT(*) FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') AND starred = 1 AND (message_type != '7')"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x0066
            X.0p9 r0 = r6.A03     // Catch: all -> 0x0066
            long r0 = r0.A02(r7)     // Catch: all -> 0x0066
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x0066
            r1 = 0
            r2[r1] = r0     // Catch: all -> 0x0066
            android.database.Cursor r2 = r5.A09(r4, r2)     // Catch: all -> 0x0066
            if (r2 == 0) goto L_0x0040
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x005f
            if (r0 == 0) goto L_0x002b
            long r0 = r2.getLong(r1)     // Catch: all -> 0x005f
            goto L_0x0058
        L_0x002b:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x005f
            r1.<init>()     // Catch: all -> 0x005f
            java.lang.String r0 = "msgstore/countStarredMessages/db no message for "
            r1.append(r0)     // Catch: all -> 0x005f
            r1.append(r7)     // Catch: all -> 0x005f
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x005f
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x005f
            goto L_0x0054
        L_0x0040:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x005f
            r1.<init>()     // Catch: all -> 0x005f
            java.lang.String r0 = "msgstore/countStarredMessages/db no cursor for "
            r1.append(r0)     // Catch: all -> 0x005f
            r1.append(r7)     // Catch: all -> 0x005f
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x005f
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x005f
        L_0x0054:
            r0 = 0
            if (r2 == 0) goto L_0x005b
        L_0x0058:
            r2.close()     // Catch: all -> 0x0066
        L_0x005b:
            r3.close()
            return r0
        L_0x005f:
            r0 = move-exception
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch: all -> 0x0065
        L_0x0065:
            throw r0     // Catch: all -> 0x0066
        L_0x0066:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x006a
        L_0x006a:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C242114q.A00(X.0lm):long");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 4, insn: 0x01df: IGET  (r0 I:X.0wQ) = (r4 I:X.14q) X.14q.A0C X.0wQ, block:B:68:0x01dc
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final void A01(
/*
[485] Method generation error in method: X.14q.A01(java.util.Collection, boolean, boolean):void, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r29v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public boolean A02(AbstractC14640lm r19, Long l) {
        long uptimeMillis = SystemClock.uptimeMillis();
        Set A04 = this.A06.A04();
        if (r19 == null) {
            if (!A04.isEmpty()) {
                return false;
            }
        } else if (A04.contains(r19)) {
            return false;
        }
        try {
            C16310on A02 = this.A0D.A02();
            AnonymousClass1Lx A00 = A02.A00();
            try {
                C20090vC r9 = this.A09;
                C16490p7 r8 = r9.A05;
                C16310on A022 = r8.A02();
                AnonymousClass1Lx A002 = A022.A00();
                r8.A04();
                if (r8.A05.A0E(A022)) {
                    C16310on A023 = r8.A02();
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put("starred", (Integer) 0);
                    String str = "starred = ? AND (status IS NULL OR status!=6)";
                    ArrayList arrayList = new ArrayList();
                    arrayList.add("1");
                    if (r19 != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append(" AND key_remote_jid = ? ");
                        str = sb.toString();
                        arrayList.add(r19.getRawString());
                    }
                    if (l != null) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append(" AND _id <= ? ");
                        str = sb2.toString();
                        arrayList.add(String.valueOf(l));
                    }
                    int A003 = A023.A03.A00("messages", contentValues, str, (String[]) arrayList.toArray(AnonymousClass01V.A0H));
                    if (A003 != 0 && !r9.A0A()) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("MainMessageStore/unStarAllMessageV1/rowChanged=");
                        sb3.append(A003);
                        Log.i(sb3.toString());
                    }
                    A023.close();
                }
                C16310on A024 = r8.A02();
                ContentValues contentValues2 = new ContentValues(1);
                contentValues2.put("starred", (Integer) 0);
                String str2 = "starred = ? AND message_type != ?";
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add("1");
                arrayList2.add(String.valueOf(7));
                if (r19 != null) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str2);
                    sb4.append(" AND chat_row_id = ? ");
                    str2 = sb4.toString();
                    arrayList2.add(String.valueOf(r9.A03.A02(r19)));
                }
                if (l != null) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(str2);
                    sb5.append(" AND sort_id <= ?");
                    str2 = sb5.toString();
                    arrayList2.add(String.valueOf(l));
                }
                int A004 = A024.A03.A00("message", contentValues2, str2, (String[]) arrayList2.toArray(AnonymousClass01V.A0H));
                if (A004 != 0 && r9.A0A()) {
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("MainMessageStore/unStarAllMessageV1/rowChanged=");
                    sb6.append(A004);
                    Log.i(sb6.toString());
                }
                A024.close();
                A002.A00();
                A002.close();
                A022.close();
                A00.A00();
                this.A0A.A00(new AnonymousClass1YZ(l) { // from class: X.1iX
                    public final /* synthetic */ Long A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AnonymousClass1YZ
                    public final void AfJ(AbstractC15340mz r6) {
                        AbstractC14640lm r2 = AbstractC14640lm.this;
                        Long l2 = this.A01;
                        if (r2 != null) {
                            AbstractC14640lm r0 = r6.A0z.A00;
                            AnonymousClass009.A05(r0);
                            if (!r0.equals(r2)) {
                                return;
                            }
                        }
                        if (l2 == null || r6.A12 <= l2.longValue()) {
                            r6.A0v = false;
                        }
                    }
                });
                this.A05.A00("StarredMessageStore/unstarAll", SystemClock.uptimeMillis() - uptimeMillis);
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e(e);
            this.A0C.A02();
        }
        this.A08.A02.post(new RunnableBRunnable0Shape4S0200000_I0_4(this, 26, r19));
        return true;
    }

    public boolean A03(Collection collection, boolean z) {
        boolean z2;
        long j;
        Set A04 = this.A06.A04();
        Iterator it = collection.iterator();
        while (true) {
            if (!it.hasNext()) {
                z2 = true;
                break;
            }
            AbstractC15340mz r2 = (AbstractC15340mz) it.next();
            AbstractC14640lm r1 = r2.A0z.A00;
            if (A04.contains(r1)) {
                long j2 = r2.A12;
                C19990v2 r0 = this.A04;
                AnonymousClass009.A05(r1);
                AnonymousClass1PE A06 = r0.A06(r1);
                if (A06 == null) {
                    j = Long.MIN_VALUE;
                } else {
                    j = A06.A0E;
                }
                if (j2 < j) {
                    z2 = false;
                    break;
                }
            }
        }
        if (!z2) {
            return false;
        }
        A01(collection, false, z);
        return true;
    }

    @Override // X.AbstractC20010v4
    public Cursor AEK(AnonymousClass02N r14, String str) {
        Cursor cursor;
        long uptimeMillis = SystemClock.uptimeMillis();
        C15240mn r8 = this.A07;
        long A04 = r8.A04();
        try {
            C16310on A01 = this.A0D.get();
            String str2 = null;
            if (TextUtils.isEmpty(str)) {
                cursor = A01.A03.A07(r14, C32301bw.A05, null);
            } else if (A04 == 1) {
                if (!TextUtils.isEmpty(str)) {
                    str2 = r8.A0G(str);
                }
                cursor = A01.A03.A07(r14, C34971h0.A0C, new String[]{str2});
            } else {
                C15250mo r0 = new C15250mo(this.A01);
                r0.A03(str);
                cursor = A01.A03.A07(r14, C34971h0.A0D, new String[]{r8.A0B(r14, r0, null)});
            }
            A01.close();
            return cursor;
        } finally {
            this.A05.A00("StarredMessageStore/getStarredMessages", SystemClock.uptimeMillis() - uptimeMillis);
        }
    }

    @Override // X.AbstractC20010v4
    public Cursor AEL(AnonymousClass02N r18, AbstractC14640lm r19, String str) {
        Cursor cursor;
        long uptimeMillis = SystemClock.uptimeMillis();
        C15240mn r3 = this.A07;
        long A04 = r3.A04();
        try {
            C16310on A01 = this.A0D.get();
            if (!TextUtils.isEmpty(str)) {
                String str2 = null;
                if (A04 == 1) {
                    if (!TextUtils.isEmpty(str)) {
                        str2 = r3.A0G(str);
                    }
                    cursor = A01.A03.A07(r18, C34971h0.A0A, new String[]{String.valueOf(this.A03.A02(r19)), str2});
                } else {
                    C15250mo r0 = new C15250mo(this.A01);
                    r0.A03(str);
                    r0.A04 = r19;
                    cursor = A01.A03.A07(r18, C34971h0.A0B, new String[]{r3.A0B(r18, r0, null)});
                }
            } else {
                cursor = A01.A03.A07(r18, C32301bw.A04, new String[]{String.valueOf(this.A03.A02(r19))});
            }
            A01.close();
            return cursor;
        } finally {
            this.A05.A00("StarredMessageStore/getStarredMessagesForJid", SystemClock.uptimeMillis() - uptimeMillis);
        }
    }
}
