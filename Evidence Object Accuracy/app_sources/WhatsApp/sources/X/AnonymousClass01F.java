package X;

import X.AbstractC001200n;
import X.AbstractC009904y;
import X.AbstractC11790gs;
import X.AnonymousClass01F;
import X.AnonymousClass074;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager$6;
import com.whatsapp.R;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.01F  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass01F implements AnonymousClass01G {
    public int A00 = -1;
    public AnonymousClass051 A01;
    public AnonymousClass05L A02;
    public AnonymousClass05L A03;
    public AnonymousClass05L A04;
    public AnonymousClass01E A05;
    public AnonymousClass01E A06;
    public AnonymousClass05V A07;
    public AnonymousClass05W A08;
    public C010605f A09 = new C010605f(this);
    public AnonymousClass06U A0A;
    public AbstractC010805h A0B = new C010705g(this);
    public Runnable A0C = new RunnableC010905i(this);
    public ArrayDeque A0D = new ArrayDeque();
    public ArrayList A0E;
    public ArrayList A0F;
    public ArrayList A0G;
    public ArrayList A0H;
    public ArrayList A0I;
    public ArrayList A0J;
    public Map A0K = Collections.synchronizedMap(new HashMap());
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public final AbstractC010305c A0R = new C010205b(this);
    public final LayoutInflater$Factory2C010105a A0S = new LayoutInflater$Factory2C010105a(this);
    public final AnonymousClass04J A0T = new AnonymousClass04J(this);
    public final AnonymousClass05Z A0U = new AnonymousClass05Z();
    public final AbstractC010505e A0V = new C010405d(this);
    public final ArrayList A0W = new ArrayList();
    public final Map A0X = Collections.synchronizedMap(new HashMap());
    public final Map A0Y = Collections.synchronizedMap(new HashMap());
    public final CopyOnWriteArrayList A0Z = new CopyOnWriteArrayList();
    public final AtomicInteger A0a = new AtomicInteger();

    public static void A00(AnonymousClass01E r2) {
        if (A01(2)) {
            StringBuilder sb = new StringBuilder("show: ");
            sb.append(r2);
            Log.v("FragmentManager", sb.toString());
        }
        if (r2.A0a) {
            r2.A0a = false;
            r2.A0b = !r2.A0b;
        }
    }

    public static boolean A01(int i) {
        return Log.isLoggable("FragmentManager", i);
    }

    public static final boolean A02(AnonymousClass01E r2) {
        AnonymousClass01E r0;
        if (r2.A0Z && r2.A0e) {
            return true;
        }
        AnonymousClass05Z r02 = r2.A0G.A0U;
        ArrayList<AnonymousClass01E> arrayList = new ArrayList();
        for (C06460Ts r03 : r02.A02.values()) {
            if (r03 != null) {
                r0 = r03.A02;
            } else {
                r0 = null;
            }
            arrayList.add(r0);
        }
        for (AnonymousClass01E r04 : arrayList) {
            if (r04 != null && A02(r04)) {
                return true;
            }
        }
        return false;
    }

    public int A03() {
        ArrayList arrayList = this.A0E;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0138  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.Parcelable A04() {
        /*
        // Method dump skipped, instructions count: 349
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01F.A04():android.os.Parcelable");
    }

    public final ViewGroup A05(AnonymousClass01E r4) {
        ViewGroup viewGroup = r4.A0B;
        if (viewGroup != null) {
            return viewGroup;
        }
        if (r4.A01 > 0 && this.A08.A01()) {
            View A00 = this.A08.A00(r4.A01);
            if (A00 instanceof ViewGroup) {
                return (ViewGroup) A00;
            }
        }
        return null;
    }

    public C06780Vb A06(AnonymousClass01E r5) {
        Bundle A00;
        C06460Ts r3 = (C06460Ts) this.A0U.A02.get(r5.A0T);
        if (r3 != null) {
            AnonymousClass01E r1 = r3.A02;
            if (r1.equals(r5)) {
                if (r1.A03 <= -1 || (A00 = r3.A00()) == null) {
                    return null;
                }
                return new C06780Vb(A00);
            }
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(r5);
        sb.append(" is not currently in the FragmentManager");
        A0f(new IllegalStateException(sb.toString()));
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public AnonymousClass01E A07(int i) {
        AnonymousClass05Z r4 = this.A0U;
        ArrayList arrayList = r4.A01;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size >= 0) {
                AnonymousClass01E r2 = (AnonymousClass01E) arrayList.get(size);
                if (r2 != null && r2.A02 == i) {
                    return r2;
                }
            } else {
                for (C06460Ts r0 : r4.A02.values()) {
                    if (r0 != null) {
                        AnonymousClass01E r22 = r0.A02;
                        if (r22.A02 == i) {
                            return r22;
                        }
                    }
                }
                return null;
            }
        }
    }

    public AnonymousClass01E A08(Bundle bundle, String str) {
        String string = bundle.getString(str);
        if (string == null) {
            return null;
        }
        AnonymousClass01E A09 = A09(string);
        if (A09 != null) {
            return A09;
        }
        StringBuilder sb = new StringBuilder("Fragment no longer exists for key ");
        sb.append(str);
        sb.append(": unique id ");
        sb.append(string);
        A0f(new IllegalStateException(sb.toString()));
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public AnonymousClass01E A09(String str) {
        C06460Ts r0 = (C06460Ts) this.A0U.A02.get(str);
        if (r0 != null) {
            return r0.A02;
        }
        return null;
    }

    public AnonymousClass01E A0A(String str) {
        AnonymousClass05Z r4 = this.A0U;
        if (str == null) {
            return null;
        }
        ArrayList arrayList = r4.A01;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size >= 0) {
                AnonymousClass01E r2 = (AnonymousClass01E) arrayList.get(size);
                if (r2 != null && str.equals(r2.A0R)) {
                    return r2;
                }
            } else {
                for (C06460Ts r0 : r4.A02.values()) {
                    if (r0 != null) {
                        AnonymousClass01E r22 = r0.A02;
                        if (str.equals(r22.A0R)) {
                            return r22;
                        }
                    }
                }
                return null;
            }
        }
    }

    public C010605f A0B() {
        AnonymousClass01E r0 = this.A05;
        if (r0 != null) {
            return r0.A0H.A0B();
        }
        return this.A09;
    }

    public C06460Ts A0C(AnonymousClass01E r4) {
        if (A01(2)) {
            StringBuilder sb = new StringBuilder("add: ");
            sb.append(r4);
            Log.v("FragmentManager", sb.toString());
        }
        C06460Ts A0D = A0D(r4);
        r4.A0H = this;
        AnonymousClass05Z r1 = this.A0U;
        r1.A04(A0D);
        if (!r4.A0X) {
            r1.A03(r4);
            r4.A0g = false;
            if (r4.A0A == null) {
                r4.A0b = false;
            }
            if (A02(r4)) {
                this.A0O = true;
            }
        }
        return A0D;
    }

    public C06460Ts A0D(AnonymousClass01E r4) {
        AnonymousClass05Z r2 = this.A0U;
        C06460Ts r1 = (C06460Ts) r2.A02.get(r4.A0T);
        if (r1 != null) {
            return r1;
        }
        C06460Ts r12 = new C06460Ts(r4, this.A0T, r2);
        r12.A06(this.A07.A01.getClassLoader());
        r12.A00 = this.A00;
        return r12;
    }

    public final Set A0E() {
        HashSet hashSet = new HashSet();
        for (C06460Ts r0 : this.A0U.A01()) {
            ViewGroup viewGroup = r0.A02.A0B;
            if (viewGroup != null) {
                Object tag = viewGroup.getTag(R.id.special_effects_controller_view_tag);
                if (!(tag instanceof AbstractC06180Sm)) {
                    tag = new AnonymousClass0EJ(viewGroup);
                    viewGroup.setTag(R.id.special_effects_controller_view_tag, tag);
                }
                hashSet.add(tag);
            }
        }
        return hashSet;
    }

    public void A0F() {
        this.A0L = true;
        A0k(true);
        for (AbstractC06180Sm r0 : A0E()) {
            r0.A03();
        }
        A0N(-1);
        this.A07 = null;
        this.A08 = null;
        this.A05 = null;
        if (this.A01 != null) {
            Iterator it = this.A0R.A00.iterator();
            while (it.hasNext()) {
                ((AnonymousClass06T) it.next()).cancel();
            }
            this.A01 = null;
        }
        AnonymousClass05L r02 = this.A03;
        if (r02 != null) {
            AnonymousClass06Z r03 = (AnonymousClass06Z) r02;
            r03.A01.A05(r03.A03);
            AnonymousClass06Z r04 = (AnonymousClass06Z) this.A04;
            r04.A01.A05(r04.A03);
            AnonymousClass06Z r05 = (AnonymousClass06Z) this.A02;
            r05.A01.A05(r05.A03);
        }
    }

    public void A0G() {
        if (this.A07 != null) {
            this.A0P = false;
            this.A0Q = false;
            this.A0A.A01 = false;
            for (AnonymousClass01E r0 : this.A0U.A02()) {
                if (r0 != null) {
                    r0.A0G.A0G();
                }
            }
        }
    }

    public void A0H() {
        A0c(new AnonymousClass0YI(this, null, -1, 0), false);
    }

    public final void A0I() {
        this.A0M = false;
        this.A0I.clear();
        this.A0J.clear();
    }

    public final void A0J() {
        for (AbstractC06180Sm r1 : A0E()) {
            if (r1.A00) {
                r1.A00 = false;
                r1.A02();
            }
        }
    }

    public final void A0K() {
        for (C06460Ts r2 : this.A0U.A01()) {
            AnonymousClass01E r1 = r2.A02;
            if (r1.A0W) {
                if (this.A0M) {
                    this.A0N = true;
                } else {
                    r1.A0W = false;
                    r2.A04();
                }
            }
        }
    }

    public final void A0L() {
        ArrayList arrayList = this.A0W;
        synchronized (arrayList) {
            boolean z = true;
            if (!arrayList.isEmpty()) {
                this.A0R.A01 = true;
                return;
            }
            AbstractC010305c r1 = this.A0R;
            if (A03() <= 0 || !A0s(this.A05)) {
                z = false;
            }
            r1.A01 = z;
        }
    }

    public void A0M(int i) {
        if (i >= 0) {
            A0c(new AnonymousClass0YI(this, null, i, 1), false);
            return;
        }
        StringBuilder sb = new StringBuilder("Bad id: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    /* JADX INFO: finally extract failed */
    public final void A0N(int i) {
        try {
            this.A0M = true;
            for (C06460Ts r0 : this.A0U.A02.values()) {
                if (r0 != null) {
                    r0.A00 = i;
                }
            }
            A0O(i, false);
            for (AbstractC06180Sm r02 : A0E()) {
                r02.A03();
            }
            this.A0M = false;
            A0k(true);
        } catch (Throwable th) {
            this.A0M = false;
            throw th;
        }
    }

    public void A0O(int i, boolean z) {
        AnonymousClass05V r2;
        if (this.A07 == null && i != -1) {
            throw new IllegalStateException("No activity");
        } else if (z || i != this.A00) {
            this.A00 = i;
            AnonymousClass05Z r5 = this.A0U;
            Iterator it = r5.A01.iterator();
            while (it.hasNext()) {
                C06460Ts r0 = (C06460Ts) r5.A02.get(((AnonymousClass01E) it.next()).A0T);
                if (r0 != null) {
                    r0.A04();
                }
            }
            for (C06460Ts r22 : r5.A02.values()) {
                if (r22 != null) {
                    r22.A04();
                    AnonymousClass01E r1 = r22.A02;
                    if (r1.A0g && r1.A00 <= 0) {
                        r5.A05(r22);
                    }
                }
            }
            A0K();
            if (this.A0O && (r2 = this.A07) != null && this.A00 == 7) {
                r2.A04.A0b();
                this.A0O = false;
            }
        }
    }

    public void A0P(Bundle bundle, AnonymousClass01E r4, String str) {
        if (r4.A0H != this) {
            StringBuilder sb = new StringBuilder("Fragment ");
            sb.append(r4);
            sb.append(" is not currently in the FragmentManager");
            A0f(new IllegalStateException(sb.toString()));
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        bundle.putString(str, r4.A0T);
    }

    public void A0Q(Parcelable parcelable) {
        AnonymousClass01E r0;
        AnonymousClass01E r6;
        C06460Ts r9;
        if (parcelable != null) {
            C06810Ve r4 = (C06810Ve) parcelable;
            if (r4.A02 != null) {
                AnonymousClass05Z r13 = this.A0U;
                HashMap hashMap = r13.A02;
                hashMap.clear();
                Iterator it = r4.A02.iterator();
                while (it.hasNext()) {
                    C06850Vi r12 = (C06850Vi) it.next();
                    if (r12 != null) {
                        AnonymousClass01E r62 = (AnonymousClass01E) this.A0A.A03.get(r12.A07);
                        if (r62 != null) {
                            if (A01(2)) {
                                StringBuilder sb = new StringBuilder("restoreSaveState: re-attaching retained ");
                                sb.append(r62);
                                Log.v("FragmentManager", sb.toString());
                            }
                            r9 = new C06460Ts(r62, this.A0T, r12, r13);
                        } else {
                            r9 = new C06460Ts(this.A0T, A0B(), r12, r13, this.A07.A01.getClassLoader());
                        }
                        AnonymousClass01E r63 = r9.A02;
                        r63.A0H = this;
                        if (A01(2)) {
                            StringBuilder sb2 = new StringBuilder("restoreSaveState: active (");
                            sb2.append(r63.A0T);
                            sb2.append("): ");
                            sb2.append(r63);
                            Log.v("FragmentManager", sb2.toString());
                        }
                        r9.A06(this.A07.A01.getClassLoader());
                        r13.A04(r9);
                        r9.A00 = this.A00;
                    }
                }
                Iterator it2 = new ArrayList(this.A0A.A03.values()).iterator();
                while (it2.hasNext()) {
                    AnonymousClass01E r64 = (AnonymousClass01E) it2.next();
                    if (hashMap.get(r64.A0T) == null) {
                        if (A01(2)) {
                            StringBuilder sb3 = new StringBuilder("Discarding retained Fragment ");
                            sb3.append(r64);
                            sb3.append(" that was not found in the set of active Fragments ");
                            sb3.append(r4.A02);
                            Log.v("FragmentManager", sb3.toString());
                        }
                        this.A0A.A04(r64);
                        r64.A0H = this;
                        C06460Ts r1 = new C06460Ts(r64, this.A0T, r13);
                        r1.A00 = 1;
                        r1.A04();
                        r64.A0g = true;
                        r1.A04();
                    }
                }
                ArrayList arrayList = r4.A03;
                r13.A01.clear();
                if (arrayList != null) {
                    Iterator it3 = arrayList.iterator();
                    while (it3.hasNext()) {
                        String str = (String) it3.next();
                        C06460Ts r02 = (C06460Ts) hashMap.get(str);
                        if (r02 == null || (r6 = r02.A02) == null) {
                            StringBuilder sb4 = new StringBuilder("No instantiated fragment for (");
                            sb4.append(str);
                            sb4.append(")");
                            throw new IllegalStateException(sb4.toString());
                        }
                        if (A01(2)) {
                            StringBuilder sb5 = new StringBuilder("restoreSaveState: added (");
                            sb5.append(str);
                            sb5.append("): ");
                            sb5.append(r6);
                            Log.v("FragmentManager", sb5.toString());
                        }
                        r13.A03(r6);
                    }
                }
                C06820Vf[] r03 = r4.A07;
                if (r03 != null) {
                    this.A0E = new ArrayList(r03.length);
                    int i = 0;
                    while (true) {
                        C06820Vf[] r14 = r4.A07;
                        if (i >= r14.length) {
                            break;
                        }
                        C06820Vf r92 = r14[i];
                        C004902f r7 = new C004902f(this);
                        int i2 = 0;
                        int i3 = 0;
                        while (true) {
                            int[] iArr = r92.A0D;
                            if (i2 >= iArr.length) {
                                break;
                            }
                            AnonymousClass0Ta r122 = new AnonymousClass0Ta();
                            int i4 = i2 + 1;
                            r122.A00 = iArr[i2];
                            if (A01(2)) {
                                StringBuilder sb6 = new StringBuilder("Instantiate ");
                                sb6.append(r7);
                                sb6.append(" op #");
                                sb6.append(i3);
                                sb6.append(" base fragment #");
                                sb6.append(iArr[i4]);
                                Log.v("FragmentManager", sb6.toString());
                            }
                            String str2 = (String) r92.A07.get(i3);
                            if (str2 != null) {
                                r0 = A09(str2);
                            } else {
                                r0 = null;
                            }
                            r122.A05 = r0;
                            r122.A07 = AnonymousClass05I.values()[r92.A0C[i3]];
                            r122.A06 = AnonymousClass05I.values()[r92.A0B[i3]];
                            int i5 = i4 + 1;
                            int i6 = iArr[i4];
                            r122.A01 = i6;
                            int i7 = i5 + 1;
                            int i8 = iArr[i5];
                            r122.A02 = i8;
                            int i9 = i7 + 1;
                            int i10 = iArr[i7];
                            r122.A03 = i10;
                            i2 = i9 + 1;
                            int i11 = iArr[i9];
                            r122.A04 = i11;
                            r7.A02 = i6;
                            r7.A03 = i8;
                            r7.A05 = i10;
                            r7.A06 = i11;
                            r7.A0D(r122);
                            i3++;
                        }
                        r7.A07 = r92.A03;
                        r7.A0A = r92.A06;
                        r7.A04 = r92.A02;
                        r7.A0E = true;
                        r7.A01 = r92.A01;
                        r7.A09 = r92.A05;
                        r7.A00 = r92.A00;
                        r7.A08 = r92.A04;
                        r7.A0C = r92.A08;
                        r7.A0D = r92.A09;
                        r7.A0H = r92.A0A;
                        r7.A04(1);
                        if (A01(2)) {
                            StringBuilder sb7 = new StringBuilder("restoreAllState: back stack #");
                            sb7.append(i);
                            sb7.append(" (index ");
                            sb7.append(r7.A04);
                            sb7.append("): ");
                            sb7.append(r7);
                            Log.v("FragmentManager", sb7.toString());
                            PrintWriter printWriter = new PrintWriter(new C03690It());
                            r7.A0E(printWriter, "  ", false);
                            printWriter.close();
                        }
                        this.A0E.add(r7);
                        i++;
                    }
                } else {
                    this.A0E = null;
                }
                this.A0a.set(r4.A00);
                String str3 = r4.A01;
                if (str3 != null) {
                    AnonymousClass01E A09 = A09(str3);
                    this.A06 = A09;
                    A0X(A09);
                }
                ArrayList arrayList2 = r4.A05;
                if (arrayList2 != null) {
                    for (int i12 = 0; i12 < arrayList2.size(); i12++) {
                        Bundle bundle = (Bundle) r4.A06.get(i12);
                        bundle.setClassLoader(this.A07.A01.getClassLoader());
                        this.A0Y.put(arrayList2.get(i12), bundle);
                    }
                }
                this.A0D = new ArrayDeque(r4.A04);
            }
        }
    }

    public void A0R(Menu menu) {
        if (this.A00 >= 1) {
            for (AnonymousClass01E r0 : this.A0U.A02()) {
                if (r0 != null) {
                    r0.A0W(menu);
                }
            }
        }
    }

    public void A0S(AnonymousClass01E r5) {
        if (A01(2)) {
            StringBuilder sb = new StringBuilder("attach: ");
            sb.append(r5);
            Log.v("FragmentManager", sb.toString());
        }
        if (r5.A0X) {
            r5.A0X = false;
            if (!r5.A0U) {
                this.A0U.A03(r5);
                if (A01(2)) {
                    StringBuilder sb2 = new StringBuilder("add from attach: ");
                    sb2.append(r5);
                    Log.v("FragmentManager", sb2.toString());
                }
                if (A02(r5)) {
                    this.A0O = true;
                }
            }
        }
    }

    public void A0T(AnonymousClass01E r6) {
        if (A01(2)) {
            StringBuilder sb = new StringBuilder("detach: ");
            sb.append(r6);
            Log.v("FragmentManager", sb.toString());
        }
        if (!r6.A0X) {
            r6.A0X = true;
            if (r6.A0U) {
                if (A01(2)) {
                    StringBuilder sb2 = new StringBuilder("remove from detach: ");
                    sb2.append(r6);
                    Log.v("FragmentManager", sb2.toString());
                }
                ArrayList arrayList = this.A0U.A01;
                synchronized (arrayList) {
                    arrayList.remove(r6);
                }
                r6.A0U = false;
                if (A02(r6)) {
                    this.A0O = true;
                }
                A0Y(r6);
            }
        }
    }

    public void A0U(AnonymousClass01E r3) {
        if (A01(2)) {
            StringBuilder sb = new StringBuilder("hide: ");
            sb.append(r3);
            Log.v("FragmentManager", sb.toString());
        }
        if (!r3.A0a) {
            r3.A0a = true;
            r3.A0b = true ^ r3.A0b;
            A0Y(r3);
        }
    }

    public void A0V(AnonymousClass01E r4) {
        if (A01(2)) {
            StringBuilder sb = new StringBuilder("remove: ");
            sb.append(r4);
            sb.append(" nesting=");
            sb.append(r4.A00);
            Log.v("FragmentManager", sb.toString());
        }
        boolean z = false;
        if (r4.A00 > 0) {
            z = true;
        }
        boolean z2 = !z;
        if (!r4.A0X || z2) {
            ArrayList arrayList = this.A0U.A01;
            synchronized (arrayList) {
                arrayList.remove(r4);
            }
            r4.A0U = false;
            if (A02(r4)) {
                this.A0O = true;
            }
            r4.A0g = true;
            A0Y(r4);
        }
    }

    public void A0W(AnonymousClass01E r3) {
        if (r3 == null || (r3.equals(A09(r3.A0T)) && (r3.A0F == null || r3.A0H == this))) {
            AnonymousClass01E r0 = this.A06;
            this.A06 = r3;
            A0X(r0);
            A0X(this.A06);
            return;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(r3);
        sb.append(" is not an active fragment of FragmentManager ");
        sb.append(this);
        throw new IllegalArgumentException(sb.toString());
    }

    public final void A0X(AnonymousClass01E r3) {
        if (r3 != null && r3.equals(A09(r3.A0T))) {
            boolean A0s = r3.A0H.A0s(r3);
            Boolean bool = r3.A0O;
            if (bool == null || bool.booleanValue() != A0s) {
                r3.A0O = Boolean.valueOf(A0s);
                AnonymousClass01F r1 = r3.A0G;
                r1.A0L();
                r1.A0X(r1.A06);
            }
        }
    }

    public final void A0Y(AnonymousClass01E r5) {
        AnonymousClass0O9 r2;
        boolean z;
        ViewGroup A05 = A05(r5);
        if (A05 != null && (r2 = r5.A0C) != null && r2.A01 + r2.A02 + r2.A04 + r2.A05 > 0) {
            if (A05.getTag(R.id.visible_removing_fragment_view_tag) == null) {
                A05.setTag(R.id.visible_removing_fragment_view_tag, r5);
            }
            AnonymousClass01E r22 = (AnonymousClass01E) A05.getTag(R.id.visible_removing_fragment_view_tag);
            AnonymousClass0O9 r0 = r5.A0C;
            if (r0 == null) {
                z = false;
            } else {
                z = r0.A0G;
            }
            if (r22.A0C != null) {
                r22.A07().A0G = z;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Z(X.AnonymousClass01E r6, X.AnonymousClass05V r7, X.AnonymousClass05W r8) {
        /*
        // Method dump skipped, instructions count: 286
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01F.A0Z(X.01E, X.05V, X.05W):void");
    }

    public void A0a(AnonymousClass01E r3, AnonymousClass05I r4) {
        if (!r3.equals(A09(r3.A0T)) || !(r3.A0F == null || r3.A0H == this)) {
            StringBuilder sb = new StringBuilder("Fragment ");
            sb.append(r3);
            sb.append(" is not an active fragment of FragmentManager ");
            sb.append(this);
            throw new IllegalArgumentException(sb.toString());
        }
        r3.A0J = r4;
    }

    public void A0b(AnonymousClass01E r3, boolean z) {
        ViewGroup A05 = A05(r3);
        if (A05 != null && (A05 instanceof FragmentContainerView)) {
            ((FragmentContainerView) A05).A03 = !z;
        }
    }

    public void A0c(AnonymousClass02g r4, boolean z) {
        if (!z) {
            if (this.A07 == null) {
                if (this.A0L) {
                    throw new IllegalStateException("FragmentManager has been destroyed");
                }
                throw new IllegalStateException("FragmentManager has not been attached to a host.");
            } else if (A0m()) {
                throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
            }
        }
        ArrayList arrayList = this.A0W;
        synchronized (arrayList) {
            if (this.A07 != null) {
                arrayList.add(r4);
                if (arrayList.size() == 1) {
                    Handler handler = this.A07.A02;
                    Runnable runnable = this.A0C;
                    handler.removeCallbacks(runnable);
                    this.A07.A02.post(runnable);
                    A0L();
                }
            } else if (!z) {
                throw new IllegalStateException("Activity has been destroyed");
            }
        }
    }

    public void A0d(AnonymousClass02g r3, boolean z) {
        if (!z || (this.A07 != null && !this.A0L)) {
            A0l(z);
            if (r3.AAG(this.A0J, this.A0I)) {
                this.A0M = true;
                try {
                    A0i(this.A0J, this.A0I);
                } finally {
                    A0I();
                }
            }
            A0L();
            if (this.A0N) {
                this.A0N = false;
                A0K();
            }
            this.A0U.A02.values().removeAll(Collections.singleton(null));
        }
    }

    public final void A0e(AbstractC11790gs r5, AbstractC001200n r6, String str) {
        AbstractC009904y ADr = r6.ADr();
        if (((C009804x) ADr).A02 != AnonymousClass05I.DESTROYED) {
            FragmentManager$6 fragmentManager$6 = new AnonymousClass054(r5, ADr, str) { // from class: androidx.fragment.app.FragmentManager$6
                public final /* synthetic */ AbstractC11790gs A01;
                public final /* synthetic */ AbstractC009904y A02;
                public final /* synthetic */ String A03;

                {
                    this.A03 = r4;
                    this.A01 = r2;
                    this.A02 = r3;
                }

                @Override // X.AnonymousClass054
                public void AWQ(AnonymousClass074 r52, AbstractC001200n r62) {
                    if (r52 == AnonymousClass074.ON_START) {
                        Map map = AnonymousClass01F.this.A0Y;
                        String str2 = this.A03;
                        Bundle bundle = (Bundle) map.get(str2);
                        if (bundle != null) {
                            this.A01.AQn(str2, bundle);
                            map.remove(str2);
                        }
                    }
                    if (r52 == AnonymousClass074.ON_DESTROY) {
                        this.A02.A01(this);
                        AnonymousClass01F.this.A0X.remove(this.A03);
                    }
                }
            };
            ADr.A00(fragmentManager$6);
            AnonymousClass0YK r0 = (AnonymousClass0YK) this.A0X.put(str, new AnonymousClass0YK(r5, ADr, fragmentManager$6));
            if (r0 != null) {
                r0.A01.A01(r0.A02);
            }
        }
    }

    public final void A0f(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new C03690It());
        AnonymousClass05V r5 = this.A07;
        try {
            if (r5 != null) {
                r5.A04.dump("  ", null, printWriter, new String[0]);
                throw runtimeException;
            }
            A0h("  ", null, printWriter, new String[0]);
            throw runtimeException;
        } catch (Exception e) {
            Log.e("FragmentManager", "Failed dumping state", e);
            throw runtimeException;
        }
    }

    public void A0g(String str, int i) {
        A0c(new AnonymousClass0YI(this, str, -1, i), false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0119 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0h(java.lang.String r7, java.io.FileDescriptor r8, java.io.PrintWriter r9, java.lang.String[] r10) {
        /*
        // Method dump skipped, instructions count: 441
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01F.A0h(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void");
    }

    public final void A0i(ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList.isEmpty()) {
            return;
        }
        if (arrayList.size() == arrayList2.size()) {
            int size = arrayList.size();
            int i = 0;
            int i2 = 0;
            while (i < size) {
                if (!((C004902f) arrayList.get(i)).A0H) {
                    if (i2 != i) {
                        A0j(arrayList, arrayList2, i2, i);
                    }
                    i2 = i + 1;
                    if (((Boolean) arrayList2.get(i)).booleanValue()) {
                        while (i2 < size && ((Boolean) arrayList2.get(i2)).booleanValue() && !((C004902f) arrayList.get(i2)).A0H) {
                            i2++;
                        }
                    }
                    A0j(arrayList, arrayList2, i, i2);
                    i = i2 - 1;
                }
                i++;
            }
            if (i2 != size) {
                A0j(arrayList, arrayList2, i2, size);
                return;
            }
            return;
        }
        throw new IllegalStateException("Internal error with the back stack records");
    }

    public final void A0j(ArrayList arrayList, ArrayList arrayList2, int i, int i2) {
        ViewGroup viewGroup;
        int i3 = i;
        boolean z = ((C004902f) arrayList.get(i3)).A0H;
        ArrayList arrayList3 = this.A0H;
        if (arrayList3 == null) {
            arrayList3 = new ArrayList();
            this.A0H = arrayList3;
        } else {
            arrayList3.clear();
        }
        AnonymousClass05Z r0 = this.A0U;
        arrayList3.addAll(r0.A02());
        AnonymousClass01E r8 = this.A06;
        boolean z2 = false;
        for (int i4 = i3; i4 < i2; i4++) {
            C004902f r9 = (C004902f) arrayList.get(i4);
            boolean booleanValue = ((Boolean) arrayList2.get(i4)).booleanValue();
            ArrayList arrayList4 = this.A0H;
            if (!booleanValue) {
                int i5 = 0;
                while (true) {
                    ArrayList arrayList5 = r9.A0B;
                    if (i5 < arrayList5.size()) {
                        AnonymousClass0Ta r11 = (AnonymousClass0Ta) arrayList5.get(i5);
                        int i6 = r11.A00;
                        if (i6 != 1) {
                            if (i6 == 2) {
                                AnonymousClass01E r6 = r11.A05;
                                int i7 = r6.A01;
                                boolean z3 = false;
                                for (int size = arrayList4.size() - 1; size >= 0; size--) {
                                    AnonymousClass01E r5 = (AnonymousClass01E) arrayList4.get(size);
                                    if (r5.A01 == i7) {
                                        if (r5 == r6) {
                                            z3 = true;
                                        } else {
                                            if (r5 == r8) {
                                                arrayList5.add(i5, new AnonymousClass0Ta(r5, 9));
                                                i5++;
                                                r8 = null;
                                            }
                                            AnonymousClass0Ta r15 = new AnonymousClass0Ta(r5, 3);
                                            r15.A01 = r11.A01;
                                            r15.A03 = r11.A03;
                                            r15.A02 = r11.A02;
                                            r15.A04 = r11.A04;
                                            arrayList5.add(i5, r15);
                                            arrayList4.remove(r5);
                                            i5++;
                                        }
                                    }
                                }
                                if (z3) {
                                    arrayList5.remove(i5);
                                    i5--;
                                } else {
                                    r11.A00 = 1;
                                    arrayList4.add(r6);
                                }
                            } else if (i6 == 3 || i6 == 6) {
                                arrayList4.remove(r11.A05);
                                AnonymousClass01E r62 = r11.A05;
                                if (r62 == r8) {
                                    arrayList5.add(i5, new AnonymousClass0Ta(r62, 9));
                                    i5++;
                                    r8 = null;
                                }
                            } else if (i6 != 7) {
                                if (i6 == 8) {
                                    arrayList5.add(i5, new AnonymousClass0Ta(r8, 9));
                                    i5++;
                                    r8 = r11.A05;
                                }
                            }
                            i5++;
                        }
                        arrayList4.add(r11.A05);
                        i5++;
                    }
                }
            } else {
                ArrayList arrayList6 = r9.A0B;
                for (int size2 = arrayList6.size() - 1; size2 >= 0; size2--) {
                    AnonymousClass0Ta r63 = (AnonymousClass0Ta) arrayList6.get(size2);
                    int i8 = r63.A00;
                    if (i8 != 1) {
                        if (i8 != 3) {
                            switch (i8) {
                                case 8:
                                    r8 = null;
                                    break;
                                case 9:
                                    r8 = r63.A05;
                                    break;
                                case 10:
                                    r63.A06 = r63.A07;
                                    break;
                            }
                        }
                        arrayList4.add(r63.A05);
                    }
                    arrayList4.remove(r63.A05);
                }
            }
            if (!z2) {
                z2 = false;
                if (!r9.A0E) {
                }
            }
            z2 = true;
        }
        this.A0H.clear();
        if (!z && this.A00 >= 1) {
            for (int i9 = i3; i9 < i2; i9++) {
                Iterator it = ((C004902f) arrayList.get(i9)).A0B.iterator();
                while (it.hasNext()) {
                    AnonymousClass01E r7 = ((AnonymousClass0Ta) it.next()).A05;
                    if (!(r7 == null || r7.A0H == null)) {
                        r0.A04(A0D(r7));
                    }
                }
            }
        }
        for (int i10 = i3; i10 < i2; i10++) {
            C004902f r92 = (C004902f) arrayList.get(i10);
            if (((Boolean) arrayList2.get(i10)).booleanValue()) {
                r92.A04(-1);
                ArrayList arrayList7 = r92.A0B;
                for (int size3 = arrayList7.size() - 1; size3 >= 0; size3--) {
                    AnonymousClass0Ta r14 = (AnonymousClass0Ta) arrayList7.get(size3);
                    AnonymousClass01E r82 = r14.A05;
                    if (r82 != null) {
                        if (r82.A0C != null) {
                            r82.A07().A0G = true;
                        }
                        int i11 = r92.A07;
                        int i12 = 8194;
                        if (i11 != 4097) {
                            if (i11 != 4099) {
                                i12 = 4097;
                                if (i11 != 8194) {
                                    i12 = 0;
                                }
                            } else {
                                i12 = 4099;
                            }
                        }
                        if (!(r82.A0C == null && i12 == 0)) {
                            r82.A07();
                            r82.A0C.A03 = i12;
                        }
                        ArrayList arrayList8 = r92.A0D;
                        ArrayList arrayList9 = r92.A0C;
                        r82.A07();
                        AnonymousClass0O9 r02 = r82.A0C;
                        r02.A0D = arrayList8;
                        r02.A0E = arrayList9;
                    }
                    int i13 = r14.A00;
                    switch (i13) {
                        case 1:
                            r82.A0O(r14.A01, r14.A02, r14.A03, r14.A04);
                            AnonymousClass01F r03 = r92.A0J;
                            r03.A0b(r82, true);
                            r03.A0V(r82);
                            break;
                        case 2:
                        default:
                            StringBuilder sb = new StringBuilder("Unknown cmd: ");
                            sb.append(i13);
                            throw new IllegalArgumentException(sb.toString());
                        case 3:
                            r82.A0O(r14.A01, r14.A02, r14.A03, r14.A04);
                            r92.A0J.A0C(r82);
                            break;
                        case 4:
                            r82.A0O(r14.A01, r14.A02, r14.A03, r14.A04);
                            A00(r82);
                            break;
                        case 5:
                            r82.A0O(r14.A01, r14.A02, r14.A03, r14.A04);
                            AnonymousClass01F r04 = r92.A0J;
                            r04.A0b(r82, true);
                            r04.A0U(r82);
                            break;
                        case 6:
                            r82.A0O(r14.A01, r14.A02, r14.A03, r14.A04);
                            r92.A0J.A0S(r82);
                            break;
                        case 7:
                            r82.A0O(r14.A01, r14.A02, r14.A03, r14.A04);
                            AnonymousClass01F r05 = r92.A0J;
                            r05.A0b(r82, true);
                            r05.A0T(r82);
                            break;
                        case 8:
                            r92.A0J.A0W(null);
                            break;
                        case 9:
                            r92.A0J.A0W(r82);
                            break;
                        case 10:
                            r92.A0J.A0a(r82, r14.A07);
                            break;
                    }
                }
                continue;
            } else {
                r92.A04(1);
                ArrayList arrayList10 = r92.A0B;
                int size4 = arrayList10.size();
                for (int i14 = 0; i14 < size4; i14++) {
                    AnonymousClass0Ta r13 = (AnonymousClass0Ta) arrayList10.get(i14);
                    AnonymousClass01E r12 = r13.A05;
                    if (r12 != null) {
                        if (r12.A0C != null) {
                            r12.A07().A0G = false;
                        }
                        int i15 = r92.A07;
                        if (!(r12.A0C == null && i15 == 0)) {
                            r12.A07();
                            r12.A0C.A03 = i15;
                        }
                        ArrayList arrayList11 = r92.A0C;
                        ArrayList arrayList12 = r92.A0D;
                        r12.A07();
                        AnonymousClass0O9 r06 = r12.A0C;
                        r06.A0D = arrayList11;
                        r06.A0E = arrayList12;
                    }
                    int i16 = r13.A00;
                    switch (i16) {
                        case 1:
                            r12.A0O(r13.A01, r13.A02, r13.A03, r13.A04);
                            AnonymousClass01F r07 = r92.A0J;
                            r07.A0b(r12, false);
                            r07.A0C(r12);
                            break;
                        case 2:
                        default:
                            StringBuilder sb2 = new StringBuilder("Unknown cmd: ");
                            sb2.append(i16);
                            throw new IllegalArgumentException(sb2.toString());
                        case 3:
                            r12.A0O(r13.A01, r13.A02, r13.A03, r13.A04);
                            r92.A0J.A0V(r12);
                            break;
                        case 4:
                            r12.A0O(r13.A01, r13.A02, r13.A03, r13.A04);
                            r92.A0J.A0U(r12);
                            break;
                        case 5:
                            r12.A0O(r13.A01, r13.A02, r13.A03, r13.A04);
                            r92.A0J.A0b(r12, false);
                            A00(r12);
                            break;
                        case 6:
                            r12.A0O(r13.A01, r13.A02, r13.A03, r13.A04);
                            r92.A0J.A0T(r12);
                            break;
                        case 7:
                            r12.A0O(r13.A01, r13.A02, r13.A03, r13.A04);
                            AnonymousClass01F r08 = r92.A0J;
                            r08.A0b(r12, false);
                            r08.A0S(r12);
                            break;
                        case 8:
                            r92.A0J.A0W(r12);
                            break;
                        case 9:
                            r92.A0J.A0W(null);
                            break;
                        case 10:
                            r92.A0J.A0a(r12, r13.A06);
                            break;
                    }
                }
                continue;
            }
        }
        boolean booleanValue2 = ((Boolean) arrayList2.get(i2 - 1)).booleanValue();
        for (int i17 = i3; i17 < i2; i17++) {
            C004902f r10 = (C004902f) arrayList.get(i17);
            ArrayList arrayList13 = r10.A0B;
            if (booleanValue2) {
                for (int size5 = arrayList13.size() - 1; size5 >= 0; size5--) {
                    AnonymousClass01E r09 = ((AnonymousClass0Ta) r10.A0B.get(size5)).A05;
                    if (r09 != null) {
                        A0D(r09).A04();
                    }
                }
            } else {
                Iterator it2 = arrayList13.iterator();
                while (it2.hasNext()) {
                    AnonymousClass01E r010 = ((AnonymousClass0Ta) it2.next()).A05;
                    if (r010 != null) {
                        A0D(r010).A04();
                    }
                }
            }
        }
        A0O(this.A00, true);
        HashSet hashSet = new HashSet();
        for (int i18 = i3; i18 < i2; i18++) {
            Iterator it3 = ((C004902f) arrayList.get(i18)).A0B.iterator();
            while (it3.hasNext()) {
                AnonymousClass01E r011 = ((AnonymousClass0Ta) it3.next()).A05;
                if (!(r011 == null || (viewGroup = r011.A0B) == null)) {
                    hashSet.add(AbstractC06180Sm.A01(viewGroup));
                }
            }
        }
        Iterator it4 = hashSet.iterator();
        while (it4.hasNext()) {
            AbstractC06180Sm r012 = (AbstractC06180Sm) it4.next();
            r012.A01 = booleanValue2;
            r012.A04();
            r012.A02();
        }
        while (i3 < i2) {
            C004902f r1 = (C004902f) arrayList.get(i3);
            if (((Boolean) arrayList2.get(i3)).booleanValue() && r1.A04 >= 0) {
                r1.A04 = -1;
            }
            i3++;
        }
        if (z2 && this.A0F != null) {
            int i19 = 0;
            while (true) {
                ArrayList arrayList14 = this.A0F;
                if (i19 < arrayList14.size()) {
                    ((AbstractC11780gr) arrayList14.get(i19)).onBackStackChanged();
                    i19++;
                } else {
                    return;
                }
            }
        }
    }

    public void A0k(boolean z) {
        A0l(z);
        while (true) {
            ArrayList arrayList = this.A0J;
            ArrayList arrayList2 = this.A0I;
            ArrayList arrayList3 = this.A0W;
            synchronized (arrayList3) {
                if (arrayList3.isEmpty()) {
                    break;
                }
                int size = arrayList3.size();
                boolean z2 = false;
                for (int i = 0; i < size; i++) {
                    z2 |= ((AnonymousClass02g) arrayList3.get(i)).AAG(arrayList, arrayList2);
                }
                arrayList3.clear();
                this.A07.A02.removeCallbacks(this.A0C);
                if (!z2) {
                    break;
                }
                this.A0M = true;
                try {
                    A0i(this.A0J, this.A0I);
                } finally {
                    A0I();
                }
            }
        }
        A0L();
        if (this.A0N) {
            this.A0N = false;
            A0K();
        }
        this.A0U.A02.values().removeAll(Collections.singleton(null));
    }

    public final void A0l(boolean z) {
        if (this.A0M) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        } else if (this.A07 == null) {
            if (this.A0L) {
                throw new IllegalStateException("FragmentManager has been destroyed");
            }
            throw new IllegalStateException("FragmentManager has not been attached to a host.");
        } else if (Looper.myLooper() != this.A07.A02.getLooper()) {
            throw new IllegalStateException("Must be called from main thread of fragment host");
        } else if (z || !A0m()) {
            if (this.A0J == null) {
                this.A0J = new ArrayList();
                this.A0I = new ArrayList();
            }
            this.A0M = true;
            this.A0M = false;
        } else {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
    }

    public boolean A0m() {
        return this.A0P || this.A0Q;
    }

    public boolean A0n() {
        boolean z;
        int size;
        A0k(false);
        A0l(true);
        AnonymousClass01E r0 = this.A06;
        if (r0 != null && r0.A0E().A0n()) {
            return true;
        }
        ArrayList arrayList = this.A0J;
        ArrayList arrayList2 = this.A0I;
        ArrayList arrayList3 = this.A0E;
        if (arrayList3 == null || (size = arrayList3.size() - 1) < 0) {
            z = false;
        } else {
            arrayList.add(arrayList3.remove(size));
            arrayList2.add(Boolean.TRUE);
            z = true;
            this.A0M = true;
            try {
                A0i(arrayList, arrayList2);
            } finally {
                A0I();
            }
        }
        A0L();
        if (this.A0N) {
            this.A0N = false;
            A0K();
        }
        this.A0U.A02.values().removeAll(Collections.singleton(null));
        return z;
    }

    public boolean A0o(Menu menu) {
        boolean z = false;
        if (this.A00 >= 1) {
            for (AnonymousClass01E r1 : this.A0U.A02()) {
                if (r1 != null && r1.A0d() && r1.A0f(menu)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public boolean A0p(Menu menu, MenuInflater menuInflater) {
        int i = 0;
        if (this.A00 < 1) {
            return false;
        }
        ArrayList arrayList = null;
        boolean z = false;
        for (AnonymousClass01E r1 : this.A0U.A02()) {
            if (r1 != null && r1.A0d() && r1.A0g(menu, menuInflater)) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(r1);
                z = true;
            }
        }
        if (this.A0G != null) {
            while (true) {
                ArrayList arrayList2 = this.A0G;
                if (i >= arrayList2.size()) {
                    break;
                }
                Object obj = arrayList2.get(i);
                if (arrayList != null) {
                    arrayList.contains(obj);
                }
                i++;
            }
        }
        this.A0G = arrayList;
        return z;
    }

    public boolean A0q(MenuItem menuItem) {
        if (this.A00 >= 1) {
            for (AnonymousClass01E r0 : this.A0U.A02()) {
                if (r0 != null && r0.A0h(menuItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean A0r(MenuItem menuItem) {
        if (this.A00 >= 1) {
            for (AnonymousClass01E r0 : this.A0U.A02()) {
                if (r0 != null && r0.A0i(menuItem)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean A0s(AnonymousClass01E r4) {
        if (r4 != null) {
            AnonymousClass01F r1 = r4.A0H;
            if (!r4.equals(r1.A06) || !A0s(r1.A05)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        int identityHashCode;
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        AnonymousClass01E r1 = this.A05;
        if (r1 != null) {
            sb.append(r1.getClass().getSimpleName());
            sb.append("{");
            identityHashCode = System.identityHashCode(r1);
        } else {
            AnonymousClass05V r12 = this.A07;
            if (r12 != null) {
                sb.append(r12.getClass().getSimpleName());
                sb.append("{");
                identityHashCode = System.identityHashCode(r12);
            } else {
                sb.append("null");
                sb.append("}}");
                return sb.toString();
            }
        }
        sb.append(Integer.toHexString(identityHashCode));
        sb.append("}");
        sb.append("}}");
        return sb.toString();
    }
}
