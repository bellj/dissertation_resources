package X;

import android.hardware.Camera;
import android.os.Handler;

/* renamed from: X.4hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C97874hm implements Camera.AutoFocusCallback {
    public final /* synthetic */ AnonymousClass27M A00;

    public C97874hm(AnonymousClass27M r1) {
        this.A00 = r1;
    }

    @Override // android.hardware.Camera.AutoFocusCallback
    public void onAutoFocus(boolean z, Camera camera) {
        AnonymousClass27M r4 = this.A00;
        Handler handler = r4.A04;
        Runnable runnable = r4.A0L;
        if (handler != null) {
            handler.postDelayed(runnable, 2000);
        } else {
            r4.postDelayed(runnable, 2000);
        }
    }
}
