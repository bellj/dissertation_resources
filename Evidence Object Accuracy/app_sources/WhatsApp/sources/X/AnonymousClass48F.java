package X;

import com.whatsapp.R;

/* renamed from: X.48F  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass48F extends AnonymousClass4V8 {
    public final float A00;

    public AnonymousClass48F(float f) {
        super(R.dimen.wds_profile_badge_medium);
        this.A00 = f;
    }

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass48F) && C16700pc.A0O(Float.valueOf(this.A00), Float.valueOf(((AnonymousClass48F) obj).A00)));
    }

    public int hashCode() {
        return Float.floatToIntBits(this.A00);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Medium(strokeWidth=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
