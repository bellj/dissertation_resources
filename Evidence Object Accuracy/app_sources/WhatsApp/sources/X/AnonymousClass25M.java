package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25M  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25M extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25M A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public AnonymousClass25P A01;
    public AnonymousClass25O A02;
    public AnonymousClass25N A03;

    static {
        AnonymousClass25M r0 = new AnonymousClass25M();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass25O r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass25O.A04;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 2);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass25N r02 = this.A03;
            if (r02 == null) {
                r02 = AnonymousClass25N.A04;
            }
            i2 += CodedOutputStream.A0A(r02, 3);
        }
        if ((this.A00 & 4) == 4) {
            AnonymousClass25P r03 = this.A01;
            if (r03 == null) {
                r03 = AnonymousClass25P.A03;
            }
            i2 += CodedOutputStream.A0A(r03, 4);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass25O r0 = this.A02;
            if (r0 == null) {
                r0 = AnonymousClass25O.A04;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass25N r02 = this.A03;
            if (r02 == null) {
                r02 = AnonymousClass25N.A04;
            }
            codedOutputStream.A0L(r02, 3);
        }
        if ((this.A00 & 4) == 4) {
            AnonymousClass25P r03 = this.A01;
            if (r03 == null) {
                r03 = AnonymousClass25P.A03;
            }
            codedOutputStream.A0L(r03, 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
