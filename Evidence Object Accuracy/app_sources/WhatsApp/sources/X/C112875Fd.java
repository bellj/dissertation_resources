package X;

import java.io.IOException;

/* renamed from: X.5Fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112875Fd implements AnonymousClass1TN, AnonymousClass5VQ {
    public final int A00;
    public final C92754Xh A01;

    public C112875Fd(C92754Xh r1, int i) {
        this.A00 = i;
        this.A01 = r1;
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return new AnonymousClass5M7(this.A01.A01(), this.A00);
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        try {
            return ADw();
        } catch (IOException e) {
            throw new AnonymousClass4CU(e.getMessage(), e);
        }
    }
}
