package X;

import com.whatsapp.jid.UserJid;
import java.util.Set;

/* renamed from: X.12u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C237312u extends AbstractC16230of {
    public void A05(UserJid userJid, Set set, Set set2) {
        if (!set.isEmpty() || !set2.isEmpty()) {
            for (AbstractC35861it r0 : A01()) {
                r0.AY9(userJid, set, set2);
            }
        }
    }
}
