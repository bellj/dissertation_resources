package X;

import android.app.Activity;

/* renamed from: X.5y1  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5y1 {
    public boolean A00 = true;
    public final int A01;
    public final Activity A02;
    public final C15890o4 A03;

    public AnonymousClass5y1(Activity activity, C15890o4 r3, int i) {
        this.A02 = activity;
        this.A03 = r3;
        this.A01 = i;
    }
}
