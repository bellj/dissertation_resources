package X;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.4oO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101974oO implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C48522Gp A01;

    public ViewTreeObserver$OnPreDrawListenerC101974oO(View view, C48522Gp r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        C48522Gp r2 = this.A01;
        AnonymousClass009.A01();
        if (r2.A01) {
            C12980iv.A1G(this.A00, this);
            return true;
        }
        r2.A00 = SystemClock.elapsedRealtime();
        return true;
    }
}
