package X;

import android.content.Context;
import android.os.Looper;

@Deprecated
/* renamed from: X.3A4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3A4 {
    @Deprecated
    public static C47492Ax A00(Context context, AnonymousClass5Pv r12, AnonymousClass5SH r13, AnonymousClass4M7 r14) {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        AnonymousClass5Xd r10 = AnonymousClass5Xd.A00;
        return new C47492Ax(context, myLooper, r12, r13, new C106424vg(r10), new C107574xY(new C106874wQ(), new C107874y5(context)), r14, C67733Sr.A00(context), r10);
    }
}
