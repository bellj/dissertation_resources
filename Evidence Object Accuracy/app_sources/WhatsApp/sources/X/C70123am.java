package X;

import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;
import java.util.Collections;

/* renamed from: X.3am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70123am implements AnonymousClass5WL {
    public final /* synthetic */ ConversationsFragment.DeleteGroupDialogFragment A00;
    public final /* synthetic */ C15370n3 A01;

    public C70123am(ConversationsFragment.DeleteGroupDialogFragment deleteGroupDialogFragment, C15370n3 r2) {
        this.A00 = deleteGroupDialogFragment;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5WL
    public void ASv() {
        this.A00.A1B();
    }

    @Override // X.AnonymousClass5WL
    public void ATy(boolean z) {
        Log.i("conversations/user-deleteGroup");
        ConversationsFragment.DeleteGroupDialogFragment deleteGroupDialogFragment = this.A00;
        deleteGroupDialogFragment.A1B();
        C15370n3 r0 = this.A01;
        C63063Ac.A00((ActivityC13810kN) deleteGroupDialogFragment.A0B(), deleteGroupDialogFragment.A00, deleteGroupDialogFragment.A03, deleteGroupDialogFragment.A04, deleteGroupDialogFragment.A08, Collections.singletonList(r0), z);
    }
}
