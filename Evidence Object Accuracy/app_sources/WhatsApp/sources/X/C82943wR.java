package X;

/* renamed from: X.3wR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82943wR extends AbstractC94534c0 {
    public final Class A00;

    public C82943wR(Class cls) {
        this.A00 = cls;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C82943wR)) {
                return false;
            }
            Class cls = this.A00;
            Class cls2 = ((C82943wR) obj).A00;
            if (cls != null) {
                if (!cls.equals(cls2)) {
                    return false;
                }
            } else if (cls2 != null) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        return this.A00.getName();
    }
}
