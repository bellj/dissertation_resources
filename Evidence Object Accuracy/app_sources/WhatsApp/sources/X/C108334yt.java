package X;

import android.os.Bundle;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108334yt implements AbstractC116625We {
    public final /* synthetic */ C108354yv A00;

    public /* synthetic */ C108334yt(C108354yv r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116625We
    public final void AgN(C56492ky r3) {
        C108354yv r0 = this.A00;
        Lock lock = r0.A0D;
        lock.lock();
        try {
            r0.A02 = r3;
            C108354yv.A00(r0);
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AbstractC116625We
    public final void AgP(Bundle bundle) {
        C108354yv r2 = this.A00;
        Lock lock = r2.A0D;
        lock.lock();
        try {
            Bundle bundle2 = r2.A01;
            if (bundle2 == null) {
                r2.A01 = bundle;
            } else if (bundle != null) {
                bundle2.putAll(bundle);
            }
            r2.A02 = C56492ky.A04;
            C108354yv.A00(r2);
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AbstractC116625We
    public final void AgS(int i, boolean z) {
        C56492ky r0;
        C108354yv r3 = this.A00;
        Lock lock = r3.A0D;
        lock.lock();
        try {
            if (r3.A04 || (r0 = r3.A03) == null || r0.A01 != 0) {
                r3.A04 = false;
                r3.A08.AgS(i, false);
                r3.A03 = null;
                r3.A02 = null;
            } else {
                r3.A04 = true;
                r3.A0A.onConnectionSuspended(i);
            }
        } finally {
            lock.unlock();
        }
    }
}
