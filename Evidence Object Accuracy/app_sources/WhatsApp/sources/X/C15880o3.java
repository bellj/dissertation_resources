package X;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0o3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15880o3 {
    public int A00 = 3;
    public File A01;
    public File A02;
    public final AnonymousClass016 A03;
    public final AbstractC15710nm A04;
    public final C14900mE A05;
    public final C15570nT A06;
    public final C33241dg A07;
    public final C15820nx A08;
    public final C15810nw A09;
    public final C17050qB A0A;
    public final C16590pI A0B;
    public final C18360sK A0C;
    public final C15890o4 A0D;
    public final C14820m6 A0E;
    public final C14950mJ A0F;
    public final C19490uC A0G;
    public final C18750sx A0H;
    public final C238113c A0I;
    public final C25641Ae A0J;
    public final C15660nh A0K;
    public final AnonymousClass12I A0L;
    public final C20850wQ A0M;
    public final C16490p7 A0N;
    public final C26041Bu A0O;
    public final C19770ue A0P;
    public final C16120oU A0Q;
    public final C21780xy A0R;
    public final C20740wF A0S;
    public final C16630pM A0T;
    public final C26981Fo A0U;
    public final AnonymousClass15E A0V;
    public final C16600pJ A0W;
    public final AnonymousClass15D A0X;
    public final Object A0Y = new Object();
    public final Set A0Z = new HashSet();

    public C15880o3(AbstractC15710nm r3, C14900mE r4, C15570nT r5, C15820nx r6, C15810nw r7, C17050qB r8, C16590pI r9, C18360sK r10, C15890o4 r11, C14820m6 r12, C14950mJ r13, C19490uC r14, C18750sx r15, C238113c r16, C25641Ae r17, C15660nh r18, AnonymousClass12I r19, C20850wQ r20, C16490p7 r21, C26041Bu r22, C19770ue r23, C21630xj r24, C16120oU r25, C21780xy r26, C20740wF r27, C16630pM r28, C26981Fo r29, AnonymousClass15E r30, C16600pJ r31, AnonymousClass15D r32) {
        this.A0B = r9;
        this.A05 = r4;
        this.A04 = r3;
        this.A0X = r32;
        this.A06 = r5;
        this.A0Q = r25;
        this.A09 = r7;
        this.A0F = r13;
        this.A0G = r14;
        this.A08 = r6;
        this.A0I = r16;
        this.A0H = r15;
        this.A0K = r18;
        this.A0A = r8;
        this.A0P = r23;
        this.A0U = r29;
        this.A0L = r19;
        this.A0S = r27;
        this.A0N = r21;
        this.A0D = r11;
        this.A0E = r12;
        this.A0C = r10;
        this.A0W = r31;
        this.A0J = r17;
        this.A0T = r28;
        this.A0O = r22;
        this.A0V = r30;
        this.A0M = r20;
        this.A0R = r26;
        this.A03 = new AnonymousClass016();
        this.A07 = new C33241dg(r24);
    }

    public static int A00(File file) {
        try {
            SQLiteDatabase openDatabase = SQLiteDatabase.openDatabase(file.getAbsolutePath(), null, 536870928);
            Cursor rawQuery = openDatabase.rawQuery("SELECT COUNT(*) FROM messages", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToNext()) {
                        int i = rawQuery.getInt(0) - 1;
                        rawQuery.close();
                        openDatabase.close();
                        return i;
                    }
                    rawQuery.close();
                } catch (Throwable th) {
                    try {
                        rawQuery.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            openDatabase.close();
            return -1;
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("msgstore/getMessageCountInDb error while trying to retrieve messages count in ");
            sb.append(file.getAbsolutePath());
            Log.e(sb.toString(), e);
            return -1;
        }
    }

    public static boolean A01(File file) {
        try {
            if (!file.exists()) {
                return false;
            }
            SQLiteDatabase openDatabase = SQLiteDatabase.openDatabase(file.getAbsolutePath(), null, 536870928);
            String stringForQuery = DatabaseUtils.stringForQuery(openDatabase, "PRAGMA integrity_check", null);
            StringBuilder sb = new StringBuilder();
            sb.append("msgstore/fieldstat/isdatabaseintegrityok ");
            sb.append(stringForQuery);
            Log.i(sb.toString());
            boolean equalsIgnoreCase = "ok".equalsIgnoreCase(stringForQuery);
            if (openDatabase != null) {
                openDatabase.close();
            }
            return equalsIgnoreCase;
        } catch (Exception e) {
            Log.e("msgstore/fieldstat/isdatabaseintegrityok/error ", e);
            return false;
        }
    }

    public static final boolean A02(File file, String str) {
        File parentFile = file.getParentFile();
        AnonymousClass009.A05(parentFile);
        if (parentFile.exists()) {
            return true;
        }
        StringBuilder sb = new StringBuilder("msgstore/");
        sb.append(str);
        sb.append("/createdir");
        Log.i(sb.toString());
        if (parentFile.mkdirs()) {
            return true;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("msgstore/");
        sb2.append(str);
        sb2.append("/createdir failed");
        Log.w(sb2.toString());
        return false;
    }

    public int A03() {
        C16490p7 r0 = this.A0N;
        r0.A04();
        long length = r0.A07.length();
        long A01 = this.A0F.A01();
        if (A01 > 7 * length) {
            return 7;
        }
        int max = Math.max((int) (A01 / length), 2);
        StringBuilder sb = new StringBuilder("msgstore/backup/backupexpirationInDays not enough space to store full backup history, saving backups only for ");
        sb.append(max);
        sb.append(" days");
        Log.i(sb.toString());
        return max;
    }

    public int A04() {
        for (EnumC16570pG r0 : EnumC16570pG.values()) {
            A0D(r0);
        }
        return A0E().size();
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0504 A[Catch: all -> 0x05a9, TRY_LEAVE, TryCatch #11 {all -> 0x05a9, blocks: (B:35:0x00ef, B:37:0x0113, B:38:0x0116, B:39:0x0118, B:112:0x04bb, B:114:0x04d6, B:115:0x04ee, B:117:0x04f0, B:118:0x04fb, B:120:0x0504, B:121:0x0509, B:123:0x0516, B:125:0x051f, B:126:0x0527, B:132:0x0580), top: B:165:0x00ef, inners: #1, #6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0580 A[Catch: all -> 0x05a9, TRY_ENTER, TRY_LEAVE, TryCatch #11 {all -> 0x05a9, blocks: (B:35:0x00ef, B:37:0x0113, B:38:0x0116, B:39:0x0118, B:112:0x04bb, B:114:0x04d6, B:115:0x04ee, B:117:0x04f0, B:118:0x04fb, B:120:0x0504, B:121:0x0509, B:123:0x0516, B:125:0x051f, B:126:0x0527, B:132:0x0580), top: B:165:0x00ef, inners: #1, #6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x058e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A05(X.C16610pK r44) {
        /*
        // Method dump skipped, instructions count: 1509
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15880o3.A05(X.0pK):int");
    }

    public long A06() {
        long j = 0;
        try {
            File A09 = A09();
            if (A09 == null) {
                return 0;
            }
            j = A09.lastModified();
            return j;
        } catch (IOException e) {
            Log.i("msgstore/lastbackupfiletime", e);
            return j;
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public final X.C29851Uy A07(X.C44581zC r54, java.util.List r55, int r56) {
        /*
        // Method dump skipped, instructions count: 3631
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15880o3.A07(X.1zC, java.util.List, int):X.1Uy");
    }

    public C29851Uy A08(AbstractC44571zB r10, boolean z) {
        C29851Uy r1;
        AbstractC16130oV r12;
        C16150oX r0;
        C16490p7 r3 = this.A0N;
        r3.A04();
        ReentrantReadWriteLock.WriteLock writeLock = r3.A08;
        writeLock.lock();
        try {
            Log.i("msgstore-manager/initialize");
            synchronized (this) {
                r3.A04();
                if (!r3.A01) {
                    C29851Uy Aaq = r10.Aaq();
                    boolean z2 = false;
                    if (Aaq.A00 == 1) {
                        z2 = true;
                    }
                    try {
                        r3.A04();
                        r3.A05.AHr();
                    } catch (SQLiteException unused) {
                    }
                    if (z2) {
                        r3.A04();
                        r3.A01 = true;
                        C238113c r5 = this.A0I;
                        r5.A02.A01(new RunnableBRunnable0Shape6S0100000_I0_6(r5, 4), 32);
                        this.A0H.A07();
                        C15660nh r8 = this.A0K;
                        C16310on A02 = r8.A0C.A02();
                        Cursor A09 = A02.A03.A09(C44521z6.A01, new String[0]);
                        while (A09.moveToNext()) {
                            AbstractC15340mz A01 = r8.A07.A0K.A01(A09);
                            if ((A01 instanceof AbstractC16130oV) && (r0 = (r12 = (AbstractC16130oV) A01).A02) != null) {
                                r0.A0X = true;
                                r8.A08.A07(r12);
                            }
                        }
                        A09.close();
                        A02.close();
                        this.A0U.A02();
                        return Aaq;
                    }
                    if (z) {
                        Log.i("msgstore-manager/initialize/re-creating db");
                        C16490p7 r02 = this.A0M.A02;
                        r02.A04();
                        r02.A06();
                        Log.i("msgstore-manager/initialize/db recreated");
                        r1 = new C29851Uy(2);
                    }
                    return Aaq;
                }
                Log.i("msgstore-manager/initialize/restoring-from-backup/6");
                r1 = new C29851Uy(6);
                return r1;
            }
        } finally {
            r3.A04();
            writeLock.unlock();
        }
    }

    public File A09() {
        File file;
        String externalStorageState = Environment.getExternalStorageState();
        C15890o4 r1 = this.A0D;
        if (("mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState)) && r1.A02("android.permission.READ_EXTERNAL_STORAGE") == 0) {
            ArrayList A0E = A0E();
            int size = A0E.size();
            do {
                size--;
                if (size < 0) {
                    return null;
                }
                file = (File) A0E.get(size);
            } while (file.length() <= 0);
            StringBuilder sb = new StringBuilder("msgstore/lastbackupfile/file ");
            sb.append(file.getName());
            sb.append(" size=");
            sb.append(file.length());
            Log.i(sb.toString());
            return file;
        }
        StringBuilder sb2 = new StringBuilder("msgstore/lastbackupfiletime/media_unavailable ");
        sb2.append(externalStorageState);
        Log.i(sb2.toString());
        throw new IOException("External media not readable");
    }

    public File A0A() {
        File[] A0J = A0J();
        int length = A0J.length;
        if (length != 0) {
            int i = 0;
            do {
                File file = A0J[i];
                if (file.exists()) {
                    StringBuilder sb = new StringBuilder("msgstore/get-latest-db-backup-for-gdrive ");
                    sb.append(file.getAbsolutePath());
                    Log.i(sb.toString());
                    return file;
                }
                i++;
            } while (i < length);
            StringBuilder sb2 = new StringBuilder("msgstore/get-latest-db-backup-for-gdrive/no-file-exists ");
            sb2.append(A0J[0].getAbsolutePath());
            Log.i(sb2.toString());
            return A0J[0];
        }
        throw new IllegalStateException("msgstore/backup/list-of-backup-files-is-null");
    }

    public final File A0B() {
        File file;
        synchronized (this.A0Y) {
            file = this.A01;
            if (file == null) {
                file = this.A0B.A00.getDatabasePath("msgstore.db-backup");
                this.A01 = file;
            }
        }
        return file;
    }

    public final File A0C() {
        File file;
        synchronized (this.A0Y) {
            file = this.A02;
            if (file == null) {
                file = new File(this.A09.A03(), "msgstore.db");
                this.A02 = file;
            }
        }
        return file;
    }

    public File A0D(EnumC16570pG r5) {
        if (r5 == EnumC16570pG.A08) {
            return A0C();
        }
        File A03 = this.A09.A03();
        StringBuilder sb = new StringBuilder();
        sb.append("msgstore.db");
        StringBuilder sb2 = new StringBuilder(".crypt");
        sb2.append(r5.version);
        sb.append(sb2.toString());
        return new File(A03, sb.toString());
    }

    public ArrayList A0E() {
        ArrayList A06 = C32781cj.A06(A0C(), C32781cj.A07(EnumC16570pG.A01(), EnumC16570pG.A00()));
        C32781cj.A0C(A0C(), A06);
        return A06;
    }

    public void A0F() {
        for (EnumC16570pG r0 : EnumC16570pG.values()) {
            C004502a.A04(A0D(r0), "", -1, false);
        }
        C004502a.A04(A0C(), "", -1, false);
    }

    public final void A0G() {
        C16490p7 r3 = this.A0N;
        r3.A04();
        File file = r3.A07;
        if (file.exists()) {
            r3.A04();
            if (!file.delete()) {
                Log.w("msgstore/copybackuptodb/failed to delete db before copying from backup up.");
            }
        }
        File A0B = A0B();
        if (A0B.exists()) {
            C21780xy r0 = this.A0R;
            r3.A04();
            C14350lI.A0L(r0, A0B, file);
            return;
        }
        Log.w("msgstore/copybackuptodb/no backup db to copy.");
    }

    public final void A0H(boolean z) {
        if (z) {
            this.A0S.A03(true);
        }
        this.A03.A0A(Boolean.FALSE);
        C16490p7 r0 = this.A0N;
        r0.A04();
        r0.A08.unlock();
    }

    public boolean A0I() {
        EnumC16570pG[] r1;
        try {
            File A09 = A09();
            if (A09 == null || A09.getName() == null) {
                return false;
            }
            synchronized (EnumC16570pG.class) {
                r1 = new EnumC16570pG[]{EnumC16570pG.A07};
            }
            if (Arrays.asList(r1).contains(C32811cm.A00(A09.getName()))) {
                return true;
            }
            return false;
        } catch (IOException e) {
            Log.i("msgstore/lastbackupfileencrypted", e);
            return false;
        }
    }

    public File[] A0J() {
        EnumC16570pG[] A04 = EnumC16570pG.A04(EnumC16570pG.A01(), EnumC16570pG.A00());
        int length = A04.length;
        File[] fileArr = new File[length];
        for (int i = 0; i < length; i++) {
            fileArr[i] = A0D(A04[(length - i) - 1]);
        }
        TextUtils.join(", ", fileArr);
        return fileArr;
    }
}
