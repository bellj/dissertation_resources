package X;

import android.database.Cursor;
import android.database.SQLException;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.0tY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19090tY extends AbstractC18500sY {
    public final C16370ot A00;
    public final C20090vC A01;
    public final C20210vO A02;

    public C19090tY(C16370ot r3, C20090vC r4, C20210vO r5, C18480sW r6) {
        super(r6, "missed_calls", Integer.MIN_VALUE);
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = r5;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            j = cursor.getLong(columnIndexOrThrow);
            try {
                C16370ot r4 = this.A00;
                C30381Xe r3 = (C30381Xe) r4.A01(cursor);
                if (r3 != null) {
                    this.A02.A01(r3);
                    r3.A0o(null);
                    try {
                        this.A01.A08(r3, false);
                    } catch (IOException unused) {
                        Log.e("MissedCallsLogDatabaseMigration/processBatch/failed to update missed call message in main message store.");
                    }
                    r4.A05(r3.A0z);
                    i++;
                }
            } catch (SQLException e) {
                throw e;
            } catch (Exception e2) {
                StringBuilder sb = new StringBuilder("MissedCallsLogDatabaseMigration/processBatch/failed to read message with id = ");
                sb.append(j);
                Log.e(sb.toString(), e2);
            }
        }
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("missed_calls_ready", 1);
    }
}
