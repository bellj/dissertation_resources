package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.0ab  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC08110ab implements AbstractC12590iA {
    public final List A00;

    public AbstractC08110ab(List list) {
        this.A00 = list;
    }

    @Override // X.AbstractC12590iA
    public List ADl() {
        return this.A00;
    }

    @Override // X.AbstractC12590iA
    public boolean AK5() {
        List list = this.A00;
        return list.isEmpty() || (list.size() == 1 && ((AnonymousClass0U8) list.get(0)).A02());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        List list = this.A00;
        if (!list.isEmpty()) {
            sb.append("values=");
            sb.append(Arrays.toString(list.toArray()));
        }
        return sb.toString();
    }
}
