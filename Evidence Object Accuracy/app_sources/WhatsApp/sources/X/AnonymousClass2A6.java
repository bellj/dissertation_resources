package X;

import android.graphics.Point;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.voipcalling.VideoPort;

/* renamed from: X.2A6  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass2A6 {
    VideoPort AHY(VideoCallParticipantView videoCallParticipantView);

    void Afb(Point point, VideoCallParticipantView videoCallParticipantView);
}
