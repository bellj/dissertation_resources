package X;

/* renamed from: X.30K  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30K extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;

    public AnonymousClass30K() {
        super(3160, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidContactListStartNewChat {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "androidContactListStartNewChatFrequentlyContacted", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "androidContactListStartNewChatSearch", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "androidContactListStartNewChatType", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
