package X;

import java.util.List;

/* renamed from: X.4x2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107254x2 implements AnonymousClass5Q8 {
    public final List A00;

    public C107254x2() {
        this(0);
    }

    public C107254x2(int i) {
        this.A00 = AnonymousClass1Mr.of();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:27:0x0079 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r8v2 */
    /* JADX WARN: Type inference failed for: r8v3, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r8v4 */
    /* JADX WARN: Type inference failed for: r8v5 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A00(X.AnonymousClass4R0 r13) {
        /*
            r12 = this;
            byte[] r0 = r13.A03
            X.4dT r9 = new X.4dT
            r9.<init>(r0)
            java.util.List r8 = r12.A00
        L_0x0009:
            int r0 = X.C95304dT.A00(r9)
            if (r0 <= 0) goto L_0x007d
            int r1 = r9.A0C()
            int r0 = r9.A0C()
            int r7 = r9.A01
            int r7 = r7 + r0
            r0 = 134(0x86, float:1.88E-43)
            if (r1 != r0) goto L_0x0079
            java.util.ArrayList r8 = X.C12960it.A0l()
            int r0 = r9.A0C()
            r6 = r0 & 31
            r5 = 0
        L_0x0029:
            if (r5 >= r6) goto L_0x0079
            r0 = 3
            java.lang.String r10 = r9.A0O(r0)
            int r1 = r9.A0C()
            r0 = r1 & 128(0x80, float:1.794E-43)
            r11 = 1
            if (r0 == 0) goto L_0x0074
            r2 = 1
            r4 = r1 & 63
            java.lang.String r3 = "application/cea-708"
        L_0x003e:
            int r0 = r9.A0C()
            byte r1 = (byte) r0
            r9.A0T(r11)
            r0 = 0
            if (r2 == 0) goto L_0x005a
            r0 = r1 & 64
            if (r0 != 0) goto L_0x004e
            r11 = 0
        L_0x004e:
            r2 = 0
            r1 = 1
            byte[] r0 = new byte[r1]
            if (r11 == 0) goto L_0x0071
            r0[r2] = r1
        L_0x0056:
            java.util.List r0 = java.util.Collections.singletonList(r0)
        L_0x005a:
            X.4ap r1 = X.C93844ap.A00()
            r1.A0R = r3
            r1.A0Q = r10
            r1.A02 = r4
            r1.A0S = r0
            X.4mC r0 = new X.4mC
            r0.<init>(r1)
            r8.add(r0)
            int r5 = r5 + 1
            goto L_0x0029
        L_0x0071:
            r0[r2] = r2
            goto L_0x0056
        L_0x0074:
            r2 = 0
            java.lang.String r3 = "application/cea-608"
            r4 = 1
            goto L_0x003e
        L_0x0079:
            r9.A0S(r7)
            goto L_0x0009
        L_0x007d:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107254x2.A00(X.4R0):java.util.List");
    }
}
