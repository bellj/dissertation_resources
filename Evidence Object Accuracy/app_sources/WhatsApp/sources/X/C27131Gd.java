package X;

import android.widget.Filter;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.chatinfo.ListChatInfo;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.ephemeral.ChangeEphemeralSettingActivity;
import com.whatsapp.group.GroupAdminPickerActivity;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.notification.PopupNotification;
import com.whatsapp.profile.ProfileInfoActivity;
import com.whatsapp.profile.ViewProfilePhoto;
import com.whatsapp.search.SearchFragment;
import com.whatsapp.settings.Settings;
import com.whatsapp.status.SetStatus;
import com.whatsapp.status.StatusesFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1Gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27131Gd {
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x0390, code lost:
        if (r4.A04 != -1) goto L_0x0392;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x0397, code lost:
        if (r4.A04 == 0) goto L_0x0399;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AbstractC14640lm r9) {
        /*
        // Method dump skipped, instructions count: 1295
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27131Gd.A00(X.0lm):void");
    }

    public void A01(AbstractC14640lm r5) {
        if (this instanceof C36851kk) {
            ViewProfilePhoto viewProfilePhoto = ((C36851kk) this).A00;
            C15550nR r2 = viewProfilePhoto.A02;
            Jid A0B = viewProfilePhoto.A0A.A0B(AbstractC14640lm.class);
            AnonymousClass009.A05(A0B);
            C15370n3 A0B2 = r2.A0B((AbstractC14640lm) A0B);
            viewProfilePhoto.A0A = A0B2;
            if (r5.equals(A0B2.A0B(AbstractC14640lm.class))) {
                StringBuilder sb = new StringBuilder("viewprofilephoto/onProfilePhotoStartChanging photo_full_id:");
                C15370n3 r1 = viewProfilePhoto.A0A;
                sb.append(r1.A04);
                sb.append(" thumb_full_id:");
                sb.append(r1.A05);
                Log.i(sb.toString());
                if (viewProfilePhoto.A0J) {
                    viewProfilePhoto.A0J = false;
                } else {
                    viewProfilePhoto.A0K = true;
                }
            }
        } else if (this instanceof C36431js) {
            C36431js r12 = (C36431js) this;
            if (C15380n4.A0J(r5)) {
                GroupChatInfo groupChatInfo = r12.A00;
                if (r5.equals(groupChatInfo.A1C)) {
                    groupChatInfo.A0B.setVisibility(0);
                }
            }
        }
    }

    public void A02(UserJid userJid) {
        if (this instanceof C36391jo) {
            C36391jo r4 = (C36391jo) this;
            if (userJid != null) {
                SetStatus setStatus = r4.A00;
                C15570nT r0 = ((ActivityC13790kL) setStatus).A01;
                r0.A08();
                if (userJid.equals(r0.A05)) {
                    String A00 = setStatus.A01.A00();
                    if (setStatus.A06) {
                        setStatus.A06 = false;
                        Iterator it = SetStatus.A09.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (((String) it.next()).equals(A00)) {
                                    break;
                                }
                            } else {
                                SetStatus.A09.add(0, A00);
                                break;
                            }
                        }
                        setStatus.A2f();
                    }
                    TextEmojiLabel textEmojiLabel = setStatus.A02;
                    textEmojiLabel.setText(AbstractC36671kL.A03(textEmojiLabel.getContext(), setStatus.A02.getPaint(), ((ActivityC13810kN) setStatus).A0B, A00));
                    setStatus.A04.notifyDataSetInvalidated();
                    setStatus.A00.setOnClickListener(new ViewOnClickCListenerShape4S0100000_I0_4(r4, 35));
                }
            }
        } else if (this instanceof C36401jp) {
            Settings settings = ((C36401jp) this).A00;
            if (((ActivityC13790kL) settings).A01.A0F(userJid)) {
                settings.A08.A0G(null, settings.A05.A00());
            }
        } else if (this instanceof C36411jq) {
            SearchFragment.A01(userJid, ((C36411jq) this).A00);
        } else if (this instanceof C36421jr) {
            C36421jr r02 = (C36421jr) this;
            if (userJid != null) {
                ProfileInfoActivity profileInfoActivity = r02.A00;
                C15570nT r03 = ((ActivityC13790kL) profileInfoActivity).A01;
                r03.A08();
                if (userJid.equals(r03.A05)) {
                    profileInfoActivity.A0F.setSubText(profileInfoActivity.A04.A00());
                }
            }
        } else if (this instanceof C36431js) {
            C36431js r04 = (C36431js) this;
            if (userJid != null) {
                GroupChatInfo groupChatInfo = r04.A00;
                if (((ActivityC13790kL) groupChatInfo).A01.A0F(userJid)) {
                    groupChatInfo.A0y.notifyDataSetChanged();
                    return;
                }
                groupChatInfo.A13.A04(new C29921Vg(((AbstractActivityC33001d7) groupChatInfo).A06.A0B(userJid)));
            }
        } else if (this instanceof C36441jt) {
            C36441jt r1 = (C36441jt) this;
            if (userJid != null && !C15380n4.A0J(userJid)) {
                GroupAdminPickerActivity groupAdminPickerActivity = r1.A00;
                if (GroupAdminPickerActivity.A02(groupAdminPickerActivity, userJid)) {
                    C15370n3.A06(new C29921Vg(groupAdminPickerActivity.A08.A0B(userJid)), groupAdminPickerActivity.A0Q);
                    groupAdminPickerActivity.A0I.A02();
                }
            }
        } else if (this instanceof C36451ju) {
            ConversationsFragment.A04(((C36451ju) this).A00, userJid);
        } else if (this instanceof C36461jv) {
            ((C36461jv) this).A00.A0A.A0E();
        } else if (this instanceof C36471jw) {
            C36641kF r12 = ((C36471jw) this).A00;
            if (C29941Vi.A00(userJid, r12.A01)) {
                r12.A04();
            }
        } else if (this instanceof C36481jx) {
            AnonymousClass1PZ r13 = ((C36481jx) this).A00;
            if (AnonymousClass1PZ.A00(r13, userJid)) {
                r13.A03();
            }
        } else if (this instanceof C36491jy) {
            AbstractActivityC36611kC r2 = ((C36491jy) this).A00;
            if (C15370n3.A06(new C29921Vg(r2.A0J.A0B(userJid)), r2.A0X)) {
                r2.A0P.notifyDataSetChanged();
            }
        } else if (this instanceof C36501jz) {
            ContactPickerFragment contactPickerFragment = ((C36501jz) this).A00;
            if (C15370n3.A06(new C29921Vg(contactPickerFragment.A0d.A0B(userJid)), contactPickerFragment.A22)) {
                contactPickerFragment.A0l.notifyDataSetChanged();
            }
        } else if (this instanceof C36511k0) {
            C36511k0 r3 = (C36511k0) this;
            r3.A00.A0E.execute(new RunnableBRunnable0Shape2S0200000_I0_2(r3, 7, userJid));
        } else if (this instanceof C36521k1) {
            ListChatInfo listChatInfo = ((C36521k1) this).A00;
            if (!((ActivityC13790kL) listChatInfo).A01.A0F(userJid)) {
                C15370n3.A06(new C29921Vg(((AbstractActivityC33001d7) listChatInfo).A06.A0B(userJid)), listChatInfo.A0d);
                listChatInfo.A06.notifyDataSetChanged();
            }
        } else if (this instanceof C36531k2) {
            ContactInfoActivity contactInfoActivity = ((C36531k2) this).A00;
            if (userJid.equals(ContactInfoActivity.A02(contactInfoActivity))) {
                contactInfoActivity.A31();
            }
        } else if (this instanceof C36541k3) {
            AbstractActivityC36551k4 r22 = ((C36541k3) this).A00;
            if (C15370n3.A06(new C29921Vg(r22.A09.A0B(userJid)), r22.A0J)) {
                r22.A0N.notifyDataSetChanged();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00a9, code lost:
        if (r10.equals(r3.A0O) == false) goto L_0x00ab;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(com.whatsapp.jid.UserJid r10) {
        /*
        // Method dump skipped, instructions count: 814
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27131Gd.A03(com.whatsapp.jid.UserJid):void");
    }

    public void A04(Collection collection) {
        if (this instanceof C35811im) {
            C35811im r4 = (C35811im) this;
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                AbstractC14640lm r2 = (AbstractC14640lm) it.next();
                if (r2 == null) {
                    Log.e("LocationSharingManager/ContactObserver/found jid == null");
                } else {
                    C16030oK r1 = r4.A00;
                    r1.A0P(r2);
                    r1.A0Q(r2, null);
                }
            }
        } else if (this instanceof C36701kO) {
            ChangeEphemeralSettingActivity changeEphemeralSettingActivity = ((C36701kO) this).A00;
            AbstractC14640lm r22 = changeEphemeralSettingActivity.A0E;
            if ((r22 instanceof UserJid) && changeEphemeralSettingActivity.A04.A0I(UserJid.of(r22)) && !changeEphemeralSettingActivity.isFinishing()) {
                C14900mE r23 = ((ActivityC13810kN) changeEphemeralSettingActivity).A05;
                int i = changeEphemeralSettingActivity.A02;
                int i2 = R.string.ephemeral_unblock_to_turn_setting_on;
                if (i == 0) {
                    i2 = R.string.ephemeral_unblock_to_turn_setting_off;
                }
                r23.A07(i2, 1);
                changeEphemeralSettingActivity.finish();
            }
        } else if (this instanceof C36471jw) {
            C36471jw r42 = (C36471jw) this;
            for (Object obj : collection) {
                C36641kF r12 = r42.A00;
                if (C29941Vi.A00(obj, r12.A01)) {
                    r12.A0N.A0B(Boolean.TRUE);
                    return;
                }
            }
        } else if (this instanceof C36491jy) {
            ((C36491jy) this).A00.A0P.notifyDataSetChanged();
        } else if (this instanceof C36501jz) {
            ((C36501jz) this).A00.A0l.notifyDataSetChanged();
        } else if (this instanceof C36531k2) {
            ContactInfoActivity contactInfoActivity = ((C36531k2) this).A00;
            contactInfoActivity.A30();
            contactInfoActivity.A33();
        } else if (this instanceof C36541k3) {
            ((C36541k3) this).A00.A0N.notifyDataSetChanged();
        }
    }

    public void A05(Collection collection) {
        AbstractC14640lm r1;
        if (this instanceof C36711kQ) {
            VoipActivityV2 voipActivityV2 = ((C36711kQ) this).A00;
            CallInfo A2i = voipActivityV2.A2i();
            if (A2i != null) {
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    Jid jid = ((C15370n3) it.next()).A0D;
                    if (jid != null && jid.equals(A2i.groupJid)) {
                        voipActivityV2.A0j.A03(A2i);
                        VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = voipActivityV2.A1N;
                        if (voipCallControlBottomSheetV2 != null) {
                            voipCallControlBottomSheetV2.A0G.A0G();
                        }
                    }
                }
            }
        } else if (this instanceof C27121Gc) {
            ((C27121Gc) this).A08();
        } else if (this instanceof C36721kR) {
            C36721kR r4 = (C36721kR) this;
            Iterator it2 = collection.iterator();
            while (it2.hasNext()) {
                Jid jid2 = ((C15370n3) it2.next()).A0D;
                if (jid2 != null) {
                    C36801kb r3 = r4.A00;
                    if (jid2.equals(r3.A00.A0D)) {
                        C20830wO r2 = r3.A03;
                        Jid jid3 = r3.A00.A0D;
                        if (jid3 instanceof AbstractC14640lm) {
                            r1 = (AbstractC14640lm) jid3;
                        } else {
                            r1 = null;
                        }
                        C15370n3 A01 = r2.A01(r1);
                        r3.A00 = A01;
                        r3.A05.A0A(Boolean.valueOf(r3.A04.A00(A01)));
                    }
                }
            }
        } else if (this instanceof C36731kS) {
            C36731kS r42 = (C36731kS) this;
            Iterator it3 = collection.iterator();
            while (it3.hasNext()) {
                Jid jid4 = ((C15370n3) it3.next()).A0D;
                if (jid4 != null) {
                    AbstractC36781kZ r12 = r42.A00;
                    if (jid4.equals(r12.A0R)) {
                        AbstractC36781kZ.A01(r12);
                        r12.A01.invalidateOptionsMenu();
                        return;
                    }
                }
            }
        } else if (this instanceof C36741kT) {
            C36741kT r32 = (C36741kT) this;
            Iterator it4 = collection.iterator();
            while (it4.hasNext()) {
                C15370n3 r13 = (C15370n3) it4.next();
                if (r13.A0J() || r13.A0I()) {
                    UserJid userJid = (UserJid) r13.A0B(UserJid.class);
                    if (userJid != null) {
                        r32.A00.A07(userJid);
                    }
                }
            }
        } else if (this instanceof C36751kU) {
            C36821kh.A00(((C36751kU) this).A00, collection);
        } else if (this instanceof C36761kV) {
            ((C36761kV) this).A00.A07.notifyDataSetChanged();
        }
    }

    public void A06(Collection collection) {
        Filter filter;
        CharSequence charSequence;
        if (!(this instanceof C36831ki)) {
            if (this instanceof C36841kj) {
                StatusesFragment statusesFragment = ((C36841kj) this).A00;
                filter = statusesFragment.A0g.getFilter();
                charSequence = statusesFragment.A0t;
            } else if (this instanceof C33311dn) {
                ((C33311dn) this).A00.A0D();
                return;
            } else if (this instanceof C36411jq) {
                C36911kq r0 = ((C36411jq) this).A00.A1F;
                if (r0 != null) {
                    r0.A02();
                    return;
                }
                return;
            } else if (this instanceof C36851kk) {
                ViewProfilePhoto.A02(((C36851kk) this).A00);
                return;
            } else if (this instanceof C36861kl) {
                PopupNotification.A03(((C36861kl) this).A00);
                return;
            } else if (this instanceof C36431js) {
                GroupChatInfo.A03(((C36431js) this).A00);
                return;
            } else if (this instanceof C36441jt) {
                GroupAdminPickerActivity groupAdminPickerActivity = ((C36441jt) this).A00;
                groupAdminPickerActivity.A2h(groupAdminPickerActivity.A0N);
                return;
            } else if (this instanceof C36451ju) {
                C36451ju r3 = (C36451ju) this;
                if (collection == null || collection.isEmpty()) {
                    ConversationsFragment.A03(r3.A00);
                    return;
                }
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    ConversationsFragment.A04(r3.A00, (AbstractC14640lm) it.next());
                }
                ConversationsFragment conversationsFragment = r3.A00;
                conversationsFragment.A13.A03 = true;
                if (conversationsFragment.A0y != null) {
                    conversationsFragment.A1K();
                    return;
                }
                return;
            } else if (this instanceof C36461jv) {
                C33061dE r32 = ((C36461jv) this).A00.A0A;
                r32.A07.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(r32, 1));
                return;
            } else if (this instanceof C36471jw) {
                C36641kF r02 = ((C36471jw) this).A00;
                r02.A04();
                r02.A0O.A0B(Boolean.TRUE);
                return;
            } else if (this instanceof C36871km) {
                ((C36871km) this).A00.A01.notifyDataSetChanged();
                return;
            } else if (this instanceof C36481jx) {
                ((C36481jx) this).A00.A02();
                return;
            } else if (this instanceof C36731kS) {
                AbstractC36781kZ.A01(((C36731kS) this).A00);
                return;
            } else if (this instanceof C36881kn) {
                C36941ku r4 = ((C36881kn) this).A00.A0K;
                r4.A0E.clear();
                AnonymousClass02P r33 = r4.A02;
                AnonymousClass016 r2 = r4.A06;
                r33.A0C(r2);
                r4.A0C.A00(new C36951kv(r4), r2, r33);
                return;
            } else if (this instanceof C36491jy) {
                ((C36491jy) this).A00.A2t();
                return;
            } else if (this instanceof C36501jz) {
                ContactPickerFragment contactPickerFragment = ((C36501jz) this).A00;
                if (!ContactPickerFragment.A2d) {
                    contactPickerFragment.A1L();
                    return;
                }
                return;
            } else if (this instanceof C36521k1) {
                ListChatInfo.A02(((C36521k1) this).A00);
                return;
            } else if (this instanceof C36531k2) {
                ContactInfoActivity contactInfoActivity = ((C36531k2) this).A00;
                contactInfoActivity.A31();
                contactInfoActivity.A0b();
                contactInfoActivity.A3A(true, false);
                return;
            } else if (this instanceof C36761kV) {
                CallsHistoryFragment callsHistoryFragment = ((C36761kV) this).A00;
                C36961kx.A00((C36961kx) callsHistoryFragment.A07.getFilter());
                filter = callsHistoryFragment.A07.getFilter();
                charSequence = callsHistoryFragment.A0d;
            } else if (this instanceof C36541k3) {
                ((C36541k3) this).A00.A2i();
                return;
            } else {
                return;
            }
            filter.filter(charSequence);
            return;
        }
        ((C36831ki) this).A00.A0J.A02();
    }

    public void A07(Collection collection) {
        if (this instanceof C27121Gc) {
            ((C27121Gc) this).A08();
        } else if (this instanceof C36751kU) {
            C36821kh.A00(((C36751kU) this).A00, collection);
        }
    }
}
