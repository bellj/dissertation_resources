package X;

/* renamed from: X.237  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass237 {
    public long A00;
    public AnonymousClass21S A01;
    public final C14900mE A02;

    public AnonymousClass237(C14900mE r3, AnonymousClass109 r4, AnonymousClass1X4 r5) {
        long j;
        C39871qg r0;
        this.A02 = r3;
        AnonymousClass1KC A01 = r4.A01(r5);
        if (A01 == null || (r0 = (C39871qg) A01.A08.A00()) == null) {
            j = 0;
        } else {
            j = r0.A01.length();
        }
        this.A00 = j;
    }
}
