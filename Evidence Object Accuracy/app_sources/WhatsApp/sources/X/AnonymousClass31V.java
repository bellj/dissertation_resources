package X;

/* renamed from: X.31V  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31V extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Double A05;
    public Double A06;
    public Double A07;
    public Integer A08;
    public Integer A09;
    public Integer A0A;
    public Integer A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public Long A0Q;
    public Long A0R;
    public Long A0S;
    public Long A0T;
    public String A0U;
    public String A0V;
    public String A0W;
    public String A0X;
    public String A0Y;

    public AnonymousClass31V() {
        super(1590, new AnonymousClass00E(1, 1, 5), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(31, this.A08);
        r3.Abe(24, this.A0U);
        r3.Abe(22, this.A0V);
        r3.Abe(20, this.A05);
        r3.Abe(15, this.A0G);
        r3.Abe(18, this.A0H);
        r3.Abe(17, this.A00);
        r3.Abe(19, this.A01);
        r3.Abe(16, this.A0I);
        r3.Abe(37, this.A09);
        r3.Abe(14, this.A0J);
        r3.Abe(21, this.A0K);
        r3.Abe(36, this.A06);
        r3.Abe(41, this.A02);
        r3.Abe(38, this.A0L);
        r3.Abe(30, this.A0A);
        r3.Abe(4, this.A0M);
        r3.Abe(39, this.A0B);
        r3.Abe(10, this.A0N);
        r3.Abe(29, this.A0W);
        r3.Abe(27, this.A0O);
        r3.Abe(5, this.A0X);
        r3.Abe(11, this.A0C);
        r3.Abe(35, this.A0D);
        r3.Abe(25, this.A0E);
        r3.Abe(13, this.A0P);
        r3.Abe(28, this.A03);
        r3.Abe(26, this.A04);
        r3.Abe(7, this.A07);
        r3.Abe(1, this.A0F);
        r3.Abe(6, this.A0Q);
        r3.Abe(9, this.A0R);
        r3.Abe(3, this.A0S);
        r3.Abe(8, this.A0T);
        r3.Abe(40, this.A0Y);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMediaDownload2 {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "connectionType", C12960it.A0Y(this.A08));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "debugMediaException", this.A0U);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "debugMediaIp", this.A0V);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadBytesTransferred", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadConnectT", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadHttpCode", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadIsReuse", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadIsStreaming", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadNetworkT", this.A0I);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadQuality", C12960it.A0Y(this.A09));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadResumePoint", this.A0J);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "downloadTimeToFirstByteT", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "estimatedBandwidth", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isViewOnce", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaId", this.A0L);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "networkStack", C12960it.A0Y(this.A0A));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallAttemptCount", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallBackendStore", C12960it.A0Y(this.A0B));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallConnBlockFetchT", this.A0N);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallConnectionClass", this.A0W);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallCumT", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallDomain", this.A0X);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallDownloadMode", C12960it.A0Y(this.A0C));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallDownloadOrigin", C12960it.A0Y(this.A0D));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallDownloadResult", C12960it.A0Y(this.A0E));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallFileValidationT", this.A0P);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallIsEncrypted", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallIsFinal", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallMediaSize", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallMediaType", C12960it.A0Y(this.A0F));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallMmsVersion", this.A0Q);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallQueueT", this.A0R);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallRetryCount", this.A0S);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "overallT", this.A0T);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "usedFallbackHint", this.A0Y);
        return C12960it.A0d("}", A0k);
    }
}
