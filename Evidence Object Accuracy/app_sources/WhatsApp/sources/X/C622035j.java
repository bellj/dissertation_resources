package X;

import java.util.HashSet;
import java.util.List;

/* renamed from: X.35j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622035j extends C38711oa implements AbstractC33511eG {
    public final C14820m6 A00;
    public final AnonymousClass1KT A01;
    public final boolean A02;
    public volatile int A03;
    public volatile int A04;
    public volatile List A05 = C12980iv.A0w(0);
    public volatile List A06 = C12980iv.A0w(0);

    public C622035j(C14820m6 r3, C235512c r4, AnonymousClass1KT r5, int i, boolean z) {
        super(r4, i);
        this.A01 = r5;
        this.A00 = r3;
        ((C38711oa) this).A00 = this;
        this.A02 = z;
    }

    @Override // X.C38711oa
    public Void A08(Void... voidArr) {
        AnonymousClass1KT r3 = this.A01;
        List A0D = r3.A0C.A0D(0);
        this.A04 = A0D.size();
        C22210yi r1 = r3.A09;
        this.A03 = r1.A02().size();
        if (this.A02) {
            this.A05 = r1.A0A();
            this.A06 = A0D;
        }
        return super.A08(voidArr);
    }

    @Override // X.AbstractC33511eG
    public void ATJ(AnonymousClass1KZ r5) {
        AnonymousClass1KT r3 = this.A01;
        HashSet hashSet = r3.A0G;
        String str = r5.A0D;
        hashSet.remove(str);
        if (r3.A05 != null) {
            for (int i = 0; i < r3.A05.size(); i++) {
                if (((AnonymousClass1KZ) r3.A05.get(i)).A0D.equals(str)) {
                    r3.A05.set(i, r5);
                    r3.A05(r3.A05);
                    return;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r1 >= 3) goto L_0x0024;
     */
    @Override // X.AbstractC33511eG
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ATK(java.util.List r11) {
        /*
            r10 = this;
            int r0 = r10.A03
            r7 = 1
            r6 = 0
            boolean r5 = X.C12960it.A1U(r0)
            int r0 = r10.A04
            if (r0 > 0) goto L_0x000d
            r7 = 0
        L_0x000d:
            X.1KT r3 = r10.A01
            r3.A06 = r5
            r3.A07 = r7
            X.0m6 r4 = r10.A00
            int r2 = r10.A04
            r9 = r11
            int r1 = r11.size()
            r0 = 50
            if (r2 >= r0) goto L_0x0024
            r0 = 3
            r2 = 0
            if (r1 < r0) goto L_0x0025
        L_0x0024:
            r2 = 1
        L_0x0025:
            android.content.SharedPreferences$Editor r1 = X.C12960it.A08(r4)
            java.lang.String r0 = "sticker_contextual_suggestion_eligibility"
            X.C12960it.A0t(r1, r0, r2)
            if (r5 == 0) goto L_0x0052
            java.lang.String r5 = "recents"
        L_0x0033:
            java.util.HashSet r8 = r3.A0G
            r8.clear()
            java.util.Iterator r2 = r11.iterator()
        L_0x003c:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0069
            java.lang.Object r1 = r2.next()
            X.1KZ r1 = (X.AnonymousClass1KZ) r1
            boolean r0 = r1.A0O
            if (r0 == 0) goto L_0x003c
            java.lang.String r0 = r1.A0D
            r8.add(r0)
            goto L_0x003c
        L_0x0052:
            if (r7 == 0) goto L_0x0058
            java.lang.String r5 = "starred"
            goto L_0x0033
        L_0x0058:
            int r0 = r11.size()
            if (r0 <= 0) goto L_0x0067
            java.lang.Object r0 = r11.get(r6)
            X.1KZ r0 = (X.AnonymousClass1KZ) r0
            java.lang.String r5 = r0.A0D
            goto L_0x0033
        L_0x0067:
            r5 = 0
            goto L_0x0033
        L_0x0069:
            if (r5 != 0) goto L_0x0090
            r3.A05(r11)
        L_0x006e:
            boolean r0 = r10.A02
            if (r0 == 0) goto L_0x008f
            java.util.List r2 = r10.A05
            java.util.List r1 = r10.A06
            X.3aP r0 = r3.A01
            if (r0 == 0) goto L_0x008f
            java.util.List r2 = r3.A00(r2, r1)
            r0.A05 = r2
            com.whatsapp.picker.search.PickerSearchDialogFragment r1 = r0.A03
            boolean r0 = r1 instanceof com.whatsapp.picker.search.StickerSearchDialogFragment
            if (r0 == 0) goto L_0x008f
            com.whatsapp.picker.search.StickerSearchDialogFragment r1 = (com.whatsapp.picker.search.StickerSearchDialogFragment) r1
            X.2fo r0 = r1.A0B
            X.016 r0 = r0.A00
            r0.A0B(r2)
        L_0x008f:
            return
        L_0x0090:
            r3.A05 = r11
            X.1KU r4 = r3.A04
            if (r4 == 0) goto L_0x006e
            java.util.HashMap r6 = r3.A0F
            java.util.HashMap r7 = r3.A0E
            r4.Acu(r5, r6, r7, r8, r9)
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C622035j.ATK(java.util.List):void");
    }

    @Override // X.AbstractC33511eG
    public void ATL() {
        this.A01.A03 = null;
    }

    @Override // X.AbstractC33511eG
    public void ATM(String str) {
        AnonymousClass1KT r2 = this.A01;
        r2.A0G.remove(str);
        if (r2.A05 != null) {
            for (int i = 0; i < r2.A05.size(); i++) {
                if (((AnonymousClass1KZ) r2.A05.get(i)).A0D.equals(str)) {
                    r2.A05.remove(i);
                    r2.A05(r2.A05);
                    return;
                }
            }
        }
    }
}
