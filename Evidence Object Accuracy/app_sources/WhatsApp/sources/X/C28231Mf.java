package X;

import android.util.Pair;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.1Mf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28231Mf {
    public C42341v3 A00;
    public C42061ub A01;
    public Long A02;
    public final AbstractC15710nm A03;
    public final C18850tA A04;
    public final C22700zV A05;
    public final AnonymousClass10W A06;
    public final AnonymousClass118 A07;
    public final AnonymousClass1E4 A08;
    public final C20840wP A09;
    public final C42221ur A0A;
    public final C42331v2 A0B;
    public final AbstractC42321v1 A0C = new C42311v0(this);
    public final C14830m7 A0D;
    public final C15680nj A0E;
    public final C22130yZ A0F;
    public final C18770sz A0G;
    public final C14850m9 A0H;
    public final C17220qS A0I;
    public final C22710zW A0J;
    public final C17070qD A0K;
    public final C20750wG A0L;
    public final Map A0M = new HashMap();
    public final Map A0N = new HashMap();
    public final Map A0O = new HashMap();
    public final Map A0P = new HashMap();

    public C28231Mf(AbstractC15710nm r10, C18850tA r11, C22700zV r12, AnonymousClass10W r13, AnonymousClass118 r14, AnonymousClass1E4 r15, C20840wP r16, C42221ur r17, AnonymousClass01d r18, C14830m7 r19, C16590pI r20, C15890o4 r21, C14820m6 r22, AnonymousClass018 r23, C15680nj r24, C22130yZ r25, C18770sz r26, C14850m9 r27, C17220qS r28, C22710zW r29, C17070qD r30, C20750wG r31) {
        this.A0D = r19;
        this.A0H = r27;
        this.A03 = r10;
        this.A04 = r11;
        this.A0I = r28;
        this.A0A = r17;
        this.A0K = r30;
        this.A0G = r26;
        this.A0L = r31;
        this.A05 = r12;
        this.A0E = r24;
        this.A0F = r25;
        this.A0J = r29;
        this.A06 = r13;
        this.A08 = r15;
        this.A09 = r16;
        this.A07 = r14;
        this.A0B = new C42331v2(r16, r17, r18, r20, r21, r22, r23, r27);
    }

    public static void A00(C28231Mf r1, AnonymousClass1Mg r2, Object obj, Map map) {
        r2.A05 = (String) map.get(obj);
        r2.A07 = (String) r1.A0O.get(obj);
    }

    public static final void A01(Collection collection, List list, Map map) {
        StringBuilder sb;
        String str;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C15370n3 r3 = (C15370n3) it.next();
            C28811Pc r0 = r3.A0C;
            AnonymousClass009.A05(r0);
            String str2 = r0.A01;
            C42051ua r1 = (C42051ua) map.get(str2);
            if (r1 == null) {
                sb = new StringBuilder();
                str = "sync/phone-number/missing_response/";
            } else {
                int i = r1.A04;
                if (i == 0) {
                    sb = new StringBuilder();
                    str = "sync/phone-number/unassigned/";
                } else {
                    boolean z = true;
                    if (i != 1) {
                        z = false;
                    }
                    UserJid userJid = r1.A0C;
                    if (r3.A0f != z || !C29941Vi.A00(r3.A0D, userJid)) {
                        r3.A0f = z;
                        r3.A0D = userJid;
                        if (collection != null) {
                            collection.add(r3);
                        }
                    }
                }
            }
            sb.append(str);
            sb.append(AnonymousClass1US.A02(4, str2));
            Log.w(sb.toString());
        }
    }

    public final C42351v4 A02(AnonymousClass02O r6, String str) {
        C28181Ma r3;
        C42351v4 r0;
        try {
            r3 = new C28181Ma(str);
            try {
                r0 = (C42351v4) r6.apply(str);
            } catch (RuntimeException e) {
                Log.e("ContactSyncHelper/runAndHandleExceptions", e);
                this.A03.AaV("ContactSyncHelper/runAndHandleExceptions", e.getMessage(), true);
                r0 = C42351v4.A02;
            }
            return r0;
        } finally {
            r3.A01();
        }
    }

    public final synchronized C42341v3 A03() {
        C42341v3 r4;
        r4 = this.A00;
        if (r4 == null) {
            AbstractC15710nm r3 = this.A03;
            C17220qS r2 = this.A0I;
            boolean z = false;
            if (this.A0F.A0D.A05()) {
                z = true;
            }
            r4 = new C42341v3(r3, this.A0C, r2, z);
            this.A00 = r4;
        }
        return r4;
    }

    public final boolean A04(C28501No r7, String str, Future future) {
        try {
            future.get(64000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | TimeoutException unused) {
        } catch (RuntimeException e) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("/exception");
            Log.e(sb.toString(), e);
            this.A03.AaV(str, e.getMessage(), true);
        } catch (ExecutionException e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("/exception");
            Log.e(sb2.toString(), e2);
            if ((e2.getCause() instanceof RuntimeException) || ((e2.getCause() instanceof Error) && !(e2.getCause() instanceof AssertionError) && !(e2.getCause() instanceof OutOfMemoryError))) {
                this.A03.AaV(str, e2.getMessage(), true);
                return false;
            }
        }
        if (this.A01 != null) {
            return true;
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append("/no result");
        Log.i(sb3.toString());
        Long l = this.A02;
        if (l != null) {
            r7.A09 = l;
            return false;
        }
        return false;
    }

    public final boolean A05(List list, List list2, List list3) {
        boolean z;
        C15370n3 A0A;
        UserJid userJid;
        UserJid userJid2;
        C18850tA r5 = this.A04;
        C15570nT r1 = r5.A04;
        r1.A08();
        synchronized (r5) {
            boolean z2 = false;
            if (r5.A0Z.A03()) {
                r1.A08();
                if (((AnonymousClass1H1) r5.A0M.A03("contact")) != null && r5.A0Q()) {
                    z2 = true;
                }
                Set hashSet = new HashSet();
                Set hashSet2 = new HashSet();
                if (z2) {
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        C15370n3 r12 = (C15370n3) it.next();
                        if (r12.A0f && (userJid2 = (UserJid) r12.A0B(UserJid.class)) != null) {
                            arrayList.add(userJid2);
                        }
                    }
                    Iterator it2 = list2.iterator();
                    while (it2.hasNext()) {
                        C15370n3 r13 = (C15370n3) it2.next();
                        if (r13.A0f && (userJid = (UserJid) r13.A0B(UserJid.class)) != null) {
                            arrayList2.add(userJid);
                        }
                    }
                    Pair A03 = r5.A03(arrayList, arrayList2);
                    hashSet = (Set) A03.first;
                    hashSet2 = (Set) A03.second;
                }
                C16310on A02 = r5.A0g.A02();
                AnonymousClass1Lx A00 = A02.A00();
                if (z2) {
                    try {
                        C21560xc r14 = r5.A0f;
                        r14.A06("SYNC_MANAGER_CONTACTS_JID_ADDED", hashSet);
                        r14.A06("SYNC_MANAGER_CONTACTS_JID_REMOVED", hashSet2);
                    } catch (Throwable th) {
                        try {
                            A00.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                C15550nR r15 = r5.A0O;
                r15.A0Z(r5.A0S.A0A(list2));
                r15.A0U(list);
                A00.A00();
                A00.close();
                A02.close();
                if (z2) {
                    r5.A0D();
                }
            }
        }
        if (!list2.isEmpty()) {
            AnonymousClass10W r8 = this.A06;
            C18850tA r16 = r8.A02;
            if (r16.A0Z.A03()) {
                r16.A04.A08();
            } else {
                r8.A01.A08();
                C15550nR r3 = r8.A03;
                r3.A0Z(r8.A05.A0A(list2));
                ArrayList A022 = C15550nR.A02(list2);
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                Iterator it3 = A022.iterator();
                while (it3.hasNext()) {
                    C15370n3 r17 = (C15370n3) it3.next();
                    AbstractC14640lm r0 = (AbstractC14640lm) r17.A0B(UserJid.class);
                    if (!(r0 == null || (A0A = r3.A0A(r0)) == null)) {
                        arrayList4.add(r17);
                        arrayList3.add(A0A);
                    }
                }
                A022.removeAll(arrayList4);
                C22410z2 r32 = r8.A06;
                if (r32.A03.A06 && r32.A0D.A02() && A022.size() != 0) {
                    Iterator it4 = A022.iterator();
                    while (it4.hasNext()) {
                        ((C15370n3) it4.next()).A0K = null;
                    }
                    r32.A04(null, A022);
                }
                r32.A05(arrayList3);
            }
            z = true;
        } else {
            z = false;
        }
        if (!list.isEmpty()) {
            AnonymousClass10W r2 = this.A06;
            C18850tA r18 = r2.A02;
            if (r18.A0Z.A03()) {
                r18.A04.A08();
            } else {
                r2.A01.A08();
                r2.A03.A0U(list);
                r2.A06.A05(new ArrayList(list));
            }
            z = true;
        }
        if (list3.isEmpty()) {
            return z;
        }
        r5.A0M(list3, false);
        this.A06.A00(list3);
        return true;
    }
}
