package X;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiChangePinActivity;
import com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiPauseMandateActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiStepUpActivity;
import com.whatsapp.payments.ui.widget.PaymentView;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.commonlibrary.GetCredential;

/* renamed from: X.5iU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121545iU extends AbstractActivityC121585iY implements AnonymousClass6MS {
    public static final HashMap A0J;
    public int A00;
    public AnonymousClass018 A01;
    public AnonymousClass102 A02;
    public C129925yW A03;
    public AbstractC136436Mn A04 = new AnonymousClass68X(this);
    public C129315xW A05;
    public C64513Fv A06;
    public AnonymousClass162 A07;
    public C120475gF A08;
    public C120525gK A09;
    public AnonymousClass69E A0A;
    public C121265hX A0B;
    public C18590sh A0C;
    public String A0D;
    public String A0E;
    public C130165yu A0F;
    public boolean A0G;
    public boolean A0H;
    public final C30931Zj A0I = C117295Zj.A0G("IndiaUpiPinHandlerActivity");

    static {
        HashMap A11 = C12970iu.A11();
        A0J = A11;
        A11.put("karur vysya bank", 8);
        A11.put("dena bank", 4);
    }

    public static AnonymousClass60V A1m(AbstractActivityC121545iU r3) {
        AnonymousClass60V A04 = r3.A0A.A04(r3.A06, 0);
        r3.A2r();
        if (A04.A00 == 0) {
            A04.A00 = R.string.payments_generic_error;
        }
        return A04;
    }

    public static final JSONObject A1n(String str, boolean z) {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("payerBankName", str);
            A0a.put("backgroundColor", "#FFFFFF");
            A0a.put("color", "#00FF00");
            if (z) {
                A0a.put("resendOTPFeature", "true");
            }
            return A0a;
        } catch (JSONException e) {
            throw C117315Zl.A0J(e);
        }
    }

    public Dialog A30(C30861Zc r10, int i) {
        if (i == 11) {
            return A31(new Runnable(r10, this) { // from class: X.6IE
                public final /* synthetic */ C30861Zc A00;
                public final /* synthetic */ AbstractActivityC121545iU A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121545iU r2 = this.A01;
                    C30861Zc r1 = this.A00;
                    C36021jC.A00(r2, 11);
                    AbstractActivityC119235dO.A1a(r1, r2, true);
                }
            }, getString(R.string.check_balance_pin_max_retries), i, R.string.forgot_upi_pin, R.string.ok);
        } else if (i != 28) {
            return super.onCreateDialog(i);
        } else {
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.payments_generic_error);
            C117295Zj.A0q(A0S, this, 50, R.string.ok);
            return A0S.create();
        }
    }

    public Dialog A31(Runnable runnable, String str, int i, int i2, int i3) {
        C30931Zj r2 = this.A0I;
        StringBuilder A0k = C12960it.A0k("IndiaUpiPinHandlerActivity showMessageDialog id:");
        A0k.append(i);
        A0k.append(" message:");
        r2.A06(C12960it.A0d(str, A0k));
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0A(str);
        A0S.setPositiveButton(i2, new DialogInterface.OnClickListener(runnable, i) { // from class: X.62i
            public final /* synthetic */ int A00;
            public final /* synthetic */ Runnable A02;

            {
                this.A00 = r3;
                this.A02 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                AbstractActivityC121545iU r1 = AbstractActivityC121545iU.this;
                int i5 = this.A00;
                Runnable runnable2 = this.A02;
                C36021jC.A00(r1, i5);
                if (runnable2 != null) {
                    new Handler(r1.getMainLooper()).post(runnable2);
                }
            }
        });
        A0S.setNegativeButton(i3, new DialogInterface.OnClickListener(i) { // from class: X.62X
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                AbstractActivityC119235dO.A1e(AbstractActivityC121545iU.this, this.A00);
            }
        });
        A0S.A0B(true);
        A0S.A08(new DialogInterface.OnCancelListener(i) { // from class: X.62A
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                AbstractActivityC119235dO.A1e(AbstractActivityC121545iU.this, this.A00);
            }
        });
        return A0S.create();
    }

    public Dialog A32(Runnable runnable, String str, String str2, int i, int i2, int i3) {
        C30931Zj r2 = this.A0I;
        StringBuilder A0k = C12960it.A0k("IndiaUpiPinHandlerActivity showMessageDialog id:");
        A0k.append(i);
        A0k.append(" message:");
        A0k.append(str2);
        A0k.append("title: ");
        r2.A06(C12960it.A0d(str, A0k));
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0A(str2);
        A0S.setTitle(str);
        A0S.setPositiveButton(i2, new DialogInterface.OnClickListener(runnable, i) { // from class: X.62j
            public final /* synthetic */ int A00;
            public final /* synthetic */ Runnable A02;

            {
                this.A00 = r3;
                this.A02 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                AbstractActivityC121545iU r1 = AbstractActivityC121545iU.this;
                int i5 = this.A00;
                Runnable runnable2 = this.A02;
                C36021jC.A00(r1, i5);
                new Handler(r1.getMainLooper()).post(runnable2);
            }
        });
        A0S.setNegativeButton(i3, new DialogInterface.OnClickListener(i) { // from class: X.62W
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                AbstractActivityC119235dO.A1e(AbstractActivityC121545iU.this, this.A00);
            }
        });
        A0S.A0B(true);
        A0S.A08(new DialogInterface.OnCancelListener(i) { // from class: X.629
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                AbstractActivityC119235dO.A1e(AbstractActivityC121545iU.this, this.A00);
            }
        });
        return A0S.create();
    }

    public final String A33(int i) {
        try {
            JSONObject A0a = C117295Zj.A0a();
            JSONArray A0L = C117315Zl.A0L();
            if (i <= 0) {
                i = 4;
            }
            JSONObject A0a2 = C117295Zj.A0a();
            A0a2.put("type", "PIN");
            A0a2.put("subtype", "MPIN");
            A0a2.put("dType", "NUM");
            A0a2.put("dLength", i);
            A0L.put(A0a2);
            return C117305Zk.A0l(A0L, "CredAllowed", A0a);
        } catch (JSONException e) {
            this.A0I.A0A("createCredRequired threw: ", e);
            return null;
        }
    }

    public final JSONArray A34(C30821Yy r6, String str, String str2, String str3, String str4, String str5) {
        JSONArray A0L = C117315Zl.A0L();
        try {
            if (!TextUtils.isEmpty(str)) {
                A0L.put(C117295Zj.A0a().putOpt("name", getString(R.string.payinfo_payeename)).putOpt("value", str));
            }
            if (!TextUtils.isEmpty(str2)) {
                A0L.put(C117295Zj.A0a().putOpt("name", getString(R.string.payinfo_account)).putOpt("value", str2));
            }
            if (!TextUtils.isEmpty(str4)) {
                A0L.put(C117295Zj.A0a().putOpt("name", getString(R.string.payinfo_refid)).putOpt("value", str4));
            }
            if (r6 != null) {
                A0L.put(C117295Zj.A0a().putOpt("name", getString(R.string.payinfo_txnamount)).putOpt("value", r6.toString()));
            }
            if (!TextUtils.isEmpty(str3)) {
                A0L.put(C117295Zj.A0a().putOpt("name", getString(R.string.payinfo_mobilenumber)).putOpt("value", str3));
            }
            if (!TextUtils.isEmpty(str5)) {
                A0L.put(C117295Zj.A0a().putOpt("name", getString(R.string.payinfo_refurl)).putOpt("value", str5));
            }
            return A0L;
        } catch (JSONException e) {
            throw C117315Zl.A0J(e);
        }
    }

    public final JSONObject A35(String str) {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("txnId", str);
            A0a.put("deviceId", this.A0D);
            A0a.put("appId", "com.whatsapp");
            A0a.put("mobileNumber", this.A0E);
            return A0a;
        } catch (JSONException e) {
            throw C117315Zl.A0J(e);
        }
    }

    public void A36() {
        C129315xW r0 = this.A05;
        if (r0 != null) {
            r0.A00();
            return;
        }
        C12960it.A1E(new AnonymousClass5oV(this, true), ((ActivityC13830kP) this).A05);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0012, code lost:
        if ((r1 instanceof com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity) == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A37() {
        /*
            r1 = this;
            boolean r0 = r1 instanceof com.whatsapp.payments.ui.IndiaUpiStepUpActivity
            if (r0 != 0) goto L_0x0019
            boolean r0 = r1 instanceof X.AbstractActivityC121525iS
            if (r0 != 0) goto L_0x001a
            boolean r0 = r1 instanceof com.whatsapp.payments.ui.IndiaUpiPauseMandateActivity
            if (r0 != 0) goto L_0x001d
            boolean r0 = r1 instanceof com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity
            if (r0 != 0) goto L_0x001d
            boolean r0 = r1 instanceof com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity
            if (r0 != 0) goto L_0x001d
        L_0x0014:
            r0 = 19
            X.C36021jC.A01(r1, r0)
        L_0x0019:
            return
        L_0x001a:
            r0 = 0
            r1.A0G = r0
        L_0x001d:
            r1.AaN()
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121545iU.A37():void");
    }

    public void A38() {
        A2C(R.string.register_wait_message);
        this.A0G = true;
        C36021jC.A00(this, 19);
        this.A0H = true;
        this.A00++;
        this.A0I.A06("showUPIAppErrorAndConfirmRetry got yes; deleting tokens and keys");
        ((AbstractActivityC121665jA) this).A0B.A0D();
        A36();
    }

    public void A39() {
        PaymentView paymentView;
        AnonymousClass60V r2;
        if (!(this instanceof IndiaUpiStepUpActivity)) {
            if (!(this instanceof AbstractActivityC121525iS)) {
                if (this instanceof IndiaUpiPauseMandateActivity) {
                    r2 = A1m(this);
                    overridePendingTransition(0, 0);
                } else if (this instanceof IndiaUpiMandatePaymentActivity) {
                    r2 = this.A0A.A04(this.A06, 0);
                    A2r();
                    if (r2.A00 == 0) {
                        r2.A00 = R.string.payments_generic_error;
                    }
                    overridePendingTransition(0, 0);
                } else if (!(this instanceof IndiaUpiCheckBalanceActivity)) {
                    if (!(this instanceof IndiaUpiChangePinActivity)) {
                        AbstractActivityC121515iQ r3 = (AbstractActivityC121515iQ) this;
                        r3.A3H(((AbstractActivityC121545iU) r3).A0A.A04(((AbstractActivityC121545iU) r3).A06, 0));
                        return;
                    }
                    AnonymousClass60V A04 = this.A0A.A04(this.A06, 0);
                    A2r();
                    if (A04.A00 == 0) {
                        A04.A00 = R.string.payments_change_pin_error;
                    }
                    Adp(A04.A01(this));
                    return;
                }
                AbstractActivityC119235dO.A1J(this, r2);
                return;
            }
            AbstractActivityC121525iS r32 = (AbstractActivityC121525iS) this;
            ((AbstractActivityC121545iU) r32).A0B.A03(123, "network_op_error_code", (long) ((AbstractActivityC121545iU) r32).A06.A00);
            C121265hX r5 = ((AbstractActivityC121545iU) r32).A0B;
            r5.A03(123, "error_code", (long) new C452120p(AnonymousClass69E.A00(((AbstractActivityC121545iU) r32).A06, 0)).A00);
            r5.A04(123, 3);
            r32.AaN();
            AnonymousClass60V A042 = ((AbstractActivityC121545iU) r32).A0A.A04(((AbstractActivityC121545iU) r32).A06, 0);
            if (!(A042.A00 != R.string.payments_bank_generic_error || (paymentView = r32.A0W) == null || paymentView.A00 == 1)) {
                A042.A00 = R.string.payments_bank_error_when_pay;
            }
            r32.A3X(A042, new Object[0]);
            return;
        }
        AbstractActivityC119235dO.A1J(this, A1m(this));
    }

    public void A3A() {
        UserJid userJid;
        C15370n3 A01;
        UserJid userJid2;
        String str;
        if (!(this instanceof IndiaUpiStepUpActivity) && !(this instanceof IndiaUpiPauseMandateActivity) && !(this instanceof IndiaUpiMandatePaymentActivity)) {
            if (this instanceof IndiaUpiSendPaymentActivity) {
                IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = (IndiaUpiSendPaymentActivity) this;
                AbstractC14640lm r1 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0E;
                if (C15380n4.A0J(r1)) {
                    userJid = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0G;
                    if (userJid == null) {
                        indiaUpiSendPaymentActivity.A2i(C12990iw.A0H(indiaUpiSendPaymentActivity));
                        return;
                    }
                } else {
                    userJid = UserJid.of(r1);
                }
                ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C = userJid;
                if (indiaUpiSendPaymentActivity.A3Y()) {
                    A01 = null;
                } else {
                    A01 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A08.A01(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C);
                }
                ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A08 = A01;
                boolean z = false;
                if (AnonymousClass1ZS.A02(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08) && ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C != null) {
                    C124245oq r12 = new C124245oq(indiaUpiSendPaymentActivity);
                    indiaUpiSendPaymentActivity.A02 = r12;
                    C12990iw.A1N(r12, ((ActivityC13830kP) indiaUpiSendPaymentActivity).A05);
                    indiaUpiSendPaymentActivity.A2C(R.string.register_wait_message);
                } else if ((AnonymousClass1ZS.A02(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08) || !((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0F.AJE(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08)) && ((userJid2 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C) == null || !((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A00.A0I(UserJid.of(userJid2)))) {
                    indiaUpiSendPaymentActivity.A3d();
                } else {
                    ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0J.A00(indiaUpiSendPaymentActivity, new AnonymousClass1P3() { // from class: X.66s
                        @Override // X.AnonymousClass1P3
                        public final void AVM(boolean z2) {
                            IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity2 = IndiaUpiSendPaymentActivity.this;
                            if (z2) {
                                indiaUpiSendPaymentActivity2.A3d();
                            } else {
                                C36021jC.A01(indiaUpiSendPaymentActivity2, 22);
                            }
                        }
                    }, ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C, ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08, true, false);
                }
                if (((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0Q == null && AbstractActivityC119235dO.A1l(indiaUpiSendPaymentActivity)) {
                    C129515xq r2 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0X;
                    boolean A3Y = indiaUpiSendPaymentActivity.A3Y();
                    if (((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0F != null) {
                        z = true;
                    }
                    if (A3Y && !z && r2.A01.A07(1718)) {
                        ((ActivityC13830kP) indiaUpiSendPaymentActivity).A05.Ab6(new Runnable() { // from class: X.6GF
                            @Override // java.lang.Runnable
                            public final void run() {
                                IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity2 = IndiaUpiSendPaymentActivity.this;
                                ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0m.A04("Getting PLE encryption key in background...");
                                C14900mE r6 = ((ActivityC13810kN) indiaUpiSendPaymentActivity2).A05;
                                C120395g7 r3 = new C120395g7(indiaUpiSendPaymentActivity2, ((ActivityC13810kN) indiaUpiSendPaymentActivity2).A03, r6, ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0H, ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity2).A0A, ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0K, ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0M);
                                C125895rz r10 = new C125895rz(indiaUpiSendPaymentActivity2);
                                Log.i("PAY: getPleServerPublicKey called");
                                C17220qS r22 = r3.A03;
                                String A012 = r22.A01();
                                C126365sl r11 = new C126365sl(new C128665wT(A012));
                                C117295Zj.A1B(r22, new C120825go(r3.A00, r3.A02, r3.A04, ((C126705tJ) r3).A00, r3, r10, r11), r11.A00, A012);
                            }
                        });
                    }
                }
            } else if (!(this instanceof IndiaUpiCheckOrderDetailsActivity) && !(this instanceof IndiaUpiCheckBalanceActivity)) {
                AbstractActivityC121515iQ r22 = (AbstractActivityC121515iQ) this;
                if (!((AbstractActivityC121545iU) r22).A06.A07.contains("pin-entry-ui")) {
                    C30931Zj r3 = r22.A07;
                    StringBuilder A0k = C12960it.A0k("showMainPaneAfterPayAppRegistered: bankAccount: ");
                    A0k.append(r22.A00);
                    A0k.append(" inSetup: ");
                    A0k.append(((AbstractActivityC121665jA) r22).A0N);
                    C117295Zj.A1F(r3, A0k);
                    ((AbstractActivityC121545iU) r22).A06.A02("pin-entry-ui");
                    C30861Zc r0 = r22.A00;
                    if (r0 != null) {
                        C119755f3 r13 = (C119755f3) r0.A08;
                        if (r13 == null) {
                            str = "could not find bank info to reset pin";
                        } else if (!((AbstractActivityC121665jA) r22).A0N || !C12970iu.A1Y(r13.A05.A00)) {
                            r22.A3C();
                            return;
                        } else {
                            r3.A06("showOrCheckPin insetup and upi pin already set; showSuccessAndFinish");
                            ((AbstractActivityC121685jC) r22).A0I.A07("2fa");
                            r22.AaN();
                            AbstractActivityC119235dO.A1f(r22);
                            return;
                        }
                    } else {
                        str = "could not find bank account";
                    }
                    r3.A06(str);
                    r22.A39();
                }
            }
        }
    }

    public void A3B() {
        int i;
        if (!(this instanceof IndiaUpiStepUpActivity)) {
            if (this instanceof AbstractActivityC121525iS) {
                i = R.string.payments_still_working;
            } else if (!(this instanceof IndiaUpiPauseMandateActivity) && !(this instanceof IndiaUpiMandatePaymentActivity) && !(this instanceof IndiaUpiCheckBalanceActivity)) {
                if (!(this instanceof IndiaUpiChangePinActivity)) {
                    i = R.string.payments_upi_pin_setup_connecting_to_npci;
                } else {
                    ((IndiaUpiChangePinActivity) this).A01.setText(R.string.payments_still_working);
                    return;
                }
            } else {
                return;
            }
            A2C(i);
        }
    }

    public void A3C() {
        int i = this.A00;
        if (i < 3) {
            C120525gK r0 = this.A09;
            if (r0 != null) {
                r0.A00();
                return;
            }
            return;
        }
        C30931Zj r2 = this.A0I;
        StringBuilder A0k = C12960it.A0k("startShowPinFlow at count: ");
        A0k.append(i);
        A0k.append(" max: ");
        A0k.append(3);
        r2.A06(C12960it.A0d("; showErrorAndFinish", A0k));
        A39();
    }

    public void A3D(C30821Yy r28, AnonymousClass1ZR r29, C119835fB r30, String str, String str2, String str3, String str4, String str5) {
        Object obj;
        C30931Zj r4 = this.A0I;
        r4.A06("getCredentials for pin check called");
        String A33 = A33(C12960it.A05(r29.A00));
        AnonymousClass1ZR A05 = ((AbstractActivityC121665jA) this).A0B.A05();
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(A33) || (obj = A05.A00) == null) {
            r4.A06("getCredentials for set got empty xml or controls or token");
            A37();
            return;
        }
        JSONObject A1n = A1n(str2, false);
        String str6 = r30.A0J;
        if (!TextUtils.isEmpty(str6)) {
            str6 = str6.toLowerCase(Locale.US);
        }
        String str7 = r30.A0N;
        Object obj2 = r28.toString();
        Object obj3 = r30.A0L;
        JSONObject A35 = A35(str7);
        try {
            A35.put("txnAmount", obj2);
            A35.put("payerAddr", obj3);
            A35.put("payeeAddr", str6);
            r4.A04("getKeySaltWithTransactionDetails");
            String A00 = C130495zV.A00(r30.A0N, r28.toString(), "com.whatsapp", this.A0D, this.A0E, r30.A0L, str6);
            r4.A04("decrypted trust params");
            try {
                String encodeToString = Base64.encodeToString(AnonymousClass61J.A04(AnonymousClass61J.A02(A00), (byte[]) obj), 2);
                this.A08.A01 = A35;
                A3F(str, A33, encodeToString, A34(r28, str4, str3, str5, ((AbstractActivityC121665jA) this).A0L, ((AbstractActivityC121665jA) this).A0J), A1n, A35);
            } catch (Exception e) {
                throw C117315Zl.A0J(e);
            }
        } catch (JSONException e2) {
            throw C117315Zl.A0J(e2);
        }
    }

    public void A3E(C119755f3 r29, String str, String str2, String str3, String str4, int i) {
        String str5;
        Object obj;
        Object obj2;
        Number number;
        C30931Zj r12 = this.A0I;
        r12.A06("getCredentials for pin setup called.");
        if (r29 != null) {
            if (i == 1) {
                AnonymousClass1ZR r2 = r29.A07;
                AnonymousClass1ZR r14 = r29.A08;
                AnonymousClass1ZR r10 = r29.A04;
                str5 = null;
                try {
                    JSONObject A0a = C117295Zj.A0a();
                    JSONArray A0L = C117315Zl.A0L();
                    if (C12960it.A05(r29.A07.A00) == 0) {
                        AnonymousClass1ZR r0 = r29.A06;
                        if (r0 == null) {
                            obj2 = null;
                        } else {
                            obj2 = r0.A00;
                        }
                        String optString = C13000ix.A05((String) obj2).optString("bank_name");
                        if (optString != null) {
                            number = (Number) A0J.get(optString.toLowerCase(Locale.US));
                        } else {
                            number = null;
                        }
                        r2 = C117305Zk.A0I(C117305Zk.A0J(), Integer.class, Integer.valueOf(number != null ? number.intValue() : 6), "otpLength");
                        r12.A06(C12960it.A0Z(r2, "createCredRequired otpLength override: ", C12960it.A0h()));
                    }
                    Object obj3 = r2.A00;
                    if (((Number) obj3).intValue() > 0) {
                        JSONObject A0a2 = C117295Zj.A0a();
                        A0a2.put("type", "OTP");
                        A0a2.put("subtype", "SMS");
                        A0a2.put("dType", "NUM");
                        A0a2.put("dLength", obj3);
                        A0L.put(A0a2);
                    }
                    AnonymousClass2SM A0J2 = C117305Zk.A0J();
                    int A05 = C12960it.A05(r14.A00);
                    if (A05 <= 0) {
                        A05 = 4;
                    }
                    Object obj4 = C117305Zk.A0I(A0J2, Integer.class, Integer.valueOf(A05), "pinLength").A00;
                    if (((Number) obj4).intValue() > 0) {
                        JSONObject A0a3 = C117295Zj.A0a();
                        A0a3.put("type", "PIN");
                        A0a3.put("subtype", "MPIN");
                        A0a3.put("dType", "NUM");
                        A0a3.put("dLength", obj4);
                        A0L.put(A0a3);
                    }
                    if (r29.A01 == 2) {
                        Object obj5 = r10.A00;
                        if (C12960it.A05(obj5) > 0) {
                            JSONObject A0a4 = C117295Zj.A0a();
                            A0a4.put("type", "PIN");
                            A0a4.put("subtype", "ATMPIN");
                            A0a4.put("dType", "NUM");
                            A0a4.put("dLength", obj5);
                            A0L.put(A0a4);
                        }
                    }
                    A0a.put("CredAllowed", A0L);
                    str5 = A0a.toString();
                } catch (JSONException e) {
                    r12.A0A("createCredRequired threw: ", e);
                }
            } else if (i == 2) {
                int A052 = C12960it.A05(r29.A08.A00);
                try {
                    JSONObject A0a5 = C117295Zj.A0a();
                    JSONArray A0L2 = C117315Zl.A0L();
                    if (A052 <= 0) {
                        A052 = 4;
                    }
                    JSONObject A0a6 = C117295Zj.A0a();
                    A0a6.put("type", "PIN");
                    A0a6.put("subtype", "MPIN");
                    A0a6.put("dType", "NUM");
                    A0a6.put("dLength", A052);
                    A0L2.put(A0a6);
                    JSONObject A0a7 = C117295Zj.A0a();
                    A0a7.put("type", "PIN");
                    A0a7.put("subtype", "NMPIN");
                    A0a7.put("dType", "NUM");
                    A0a7.put("dLength", A052);
                    A0L2.put(A0a7);
                    str5 = C117305Zk.A0l(A0L2, "CredAllowed", A0a5);
                } catch (JSONException e2) {
                    r12.A0A("createCredRequired threw: ", e2);
                    str5 = null;
                }
            } else if (i == 3) {
                str5 = A33(C12960it.A05(r29.A08.A00));
            }
            AnonymousClass1ZR A053 = ((AbstractActivityC121665jA) this).A0B.A05();
            if (!TextUtils.isEmpty(str) || TextUtils.isEmpty(str5) || (obj = A053.A00) == null) {
                r12.A06("getCredentials for set got empty xml or controls or token");
                A37();
            }
            JSONObject A1n = A1n(str2, true);
            JSONObject A35 = A35(str3);
            StringBuilder A0j = C12960it.A0j(str3);
            A0j.append("|");
            A0j.append("com.whatsapp");
            A0j.append("|");
            A0j.append(this.A0E);
            A0j.append("|");
            try {
                A3F(str, str5, Base64.encodeToString(AnonymousClass61J.A04(AnonymousClass61J.A02(C12960it.A0d(this.A0D, A0j)), (byte[]) obj), 2), A34(null, null, str4, null, ((AbstractActivityC121665jA) this).A0L, ((AbstractActivityC121665jA) this).A0J), A1n, A35);
                return;
            } catch (Exception e3) {
                throw C117315Zl.A0J(e3);
            }
        }
        str5 = null;
        AnonymousClass1ZR A053 = ((AbstractActivityC121665jA) this).A0B.A05();
        if (!TextUtils.isEmpty(str)) {
        }
        r12.A06("getCredentials for set got empty xml or controls or token");
        A37();
    }

    public final void A3F(String str, String str2, String str3, JSONArray jSONArray, JSONObject jSONObject, JSONObject jSONObject2) {
        if (((AbstractActivityC121665jA) this).A0C.A01().getBoolean("payment_account_recovered", false)) {
            C1329668y r1 = ((AbstractActivityC121665jA) this).A0B;
            if (!r1.A0N(r1.A07())) {
                A2G(C12990iw.A0D(this, IndiaUpiPaymentsAccountSetupActivity.class), true);
                A2q();
                return;
            }
        }
        if (C130265z4.A00(((ActivityC13810kN) this).A0C, ((AbstractActivityC121665jA) this).A05, ((ActivityC13830kP) this).A04, "pinEntry")) {
            Adr(new Object[0], R.string.rooted_device_error_title, R.string.rooted_device_error_message);
            return;
        }
        Intent putExtra = C12990iw.A0D(getApplicationContext(), GetCredential.class).putExtra("keyCode", "NPCI").putExtra("keyXmlPayload", str).putExtra("controls", str2).putExtra("configuration", jSONObject.toString()).putExtra("salt", jSONObject2.toString()).putExtra("payInfo", jSONArray.toString()).putExtra("trust", str3).putExtra("languagePref", C12970iu.A14(this.A01).toString());
        putExtra.setFlags(536870912);
        A2E(putExtra, 200);
    }

    public void A3G(HashMap hashMap) {
        String str;
        if (this instanceof IndiaUpiStepUpActivity) {
            IndiaUpiStepUpActivity indiaUpiStepUpActivity = (IndiaUpiStepUpActivity) this;
            indiaUpiStepUpActivity.A06.A06("onGetCredentials called");
            C127475uY r1 = new C127475uY(2);
            r1.A02 = hashMap;
            indiaUpiStepUpActivity.A03.A04(r1);
        } else if (this instanceof AbstractActivityC121525iS) {
            AbstractActivityC121525iS r2 = (AbstractActivityC121525iS) this;
            if (r2.A0B != null) {
                ((AbstractActivityC121665jA) r2).A0A.A07 = hashMap;
                r2.A3N();
                r2.AaN();
                r2.A2C(R.string.register_wait_message);
                r2.A3W(r2.A3I(r2.A0A, ((AbstractActivityC121685jC) r2).A01));
            }
        } else if (this instanceof IndiaUpiCheckBalanceActivity) {
            IndiaUpiCheckBalanceActivity indiaUpiCheckBalanceActivity = (IndiaUpiCheckBalanceActivity) this;
            indiaUpiCheckBalanceActivity.A05.A06("onGetCredentials called");
            C117925az r3 = indiaUpiCheckBalanceActivity.A02;
            C127115ty.A00(r3.A02.A00, r3.A01, R.string.getting_balance_wait_message);
            C30861Zc r12 = r3.A04;
            C119755f3 r0 = (C119755f3) r12.A08;
            C120515gJ r14 = r3.A05;
            AnonymousClass1ZR r6 = r0.A09;
            String str2 = r0.A0F;
            AnonymousClass1ZR r5 = r0.A06;
            AnonymousClass1ZR r4 = r3.A00;
            String str3 = r12.A0A;
            C128635wQ r02 = new C128635wQ(r3);
            C17220qS r22 = r14.A06;
            String A01 = r22.A01();
            if (hashMap != null) {
                str = C1308460e.A00("MPIN", hashMap);
            } else {
                str = null;
            }
            C126475sw r52 = new C126475sw(new AnonymousClass3CS(A01), str3, C117315Zl.A0K(r4), r14.A0E, str, C117315Zl.A0K(r6), str2, (String) AnonymousClass1ZS.A01(r5));
            C64513Fv r13 = ((C126705tJ) r14).A00;
            if (r13 != null) {
                r13.A04("upi-check-balance");
            }
            r22.A09(new C120775gj(r14.A00, r14.A01, r14.A09, r13, r14, r02), r52.A00, A01, 204, 0);
        } else if (this instanceof IndiaUpiChangePinActivity) {
            IndiaUpiChangePinActivity indiaUpiChangePinActivity = (IndiaUpiChangePinActivity) this;
            AnonymousClass1ZY r42 = indiaUpiChangePinActivity.A02.A08;
            AnonymousClass009.A06(r42, indiaUpiChangePinActivity.A05.A02("IndiaUpiChangePinActivity could not cast country data to IndiaUpiMethodData"));
            C119755f3 r43 = (C119755f3) r42;
            C120525gK r142 = ((AbstractActivityC121545iU) indiaUpiChangePinActivity).A09;
            AnonymousClass1ZR r32 = r43.A09;
            String str4 = r43.A0F;
            AnonymousClass1ZR r03 = r43.A06;
            String str5 = indiaUpiChangePinActivity.A02.A0A;
            String str6 = indiaUpiChangePinActivity.A03;
            if (AnonymousClass1ZS.A02(r32)) {
                Context context = r142.A01;
                C14850m9 r132 = r142.A06;
                C14900mE r122 = r142.A02;
                C15570nT r11 = r142.A03;
                C17220qS r10 = r142.A07;
                C17070qD r9 = r142.A0C;
                C21860y6 r7 = r142.A09;
                C18610sj r62 = ((C126705tJ) r142).A01;
                AnonymousClass102 r53 = r142.A05;
                AnonymousClass6BE r44 = r142.A0D;
                new C120495gH(context, r122, r11, r53, r132, r10, r142.A08, r7, r142.A0A, null, r62, r9, r44, r142.A0E).A01(new AnonymousClass6AV(r03, r142, str5, str6, hashMap));
                return;
            }
            r142.A02(r32, r03, str4, str5, str6, hashMap);
        } else if (!(this instanceof AbstractActivityC121515iQ)) {
            AbstractActivityC121655j9 r23 = (AbstractActivityC121655j9) this;
            r23.A0H.A06("onGetCredentials called");
            r23.A3J(r23.A02, hashMap);
        } else {
            AbstractActivityC121515iQ r24 = (AbstractActivityC121515iQ) this;
            r24.A2C(R.string.payments_upi_pin_setup_wait_message);
            C30861Zc r54 = r24.A00;
            AnonymousClass1ZY r45 = r54.A08;
            AnonymousClass009.A06(r45, "could not cast country data to IndiaUpiMethodData");
            C119755f3 r46 = (C119755f3) r45;
            C120525gK r143 = ((AbstractActivityC121545iU) r24).A09;
            AnonymousClass1ZR r33 = r46.A09;
            String str7 = r46.A0F;
            AnonymousClass1ZR r04 = r46.A06;
            String str8 = r54.A0A;
            String str9 = r24.A04;
            String str10 = r24.A02;
            String str11 = r24.A03;
            String str12 = r24.A05;
            if (AnonymousClass1ZS.A02(r33)) {
                Context context2 = r143.A01;
                C14850m9 r133 = r143.A06;
                C14900mE r123 = r143.A02;
                C15570nT r112 = r143.A03;
                C17220qS r102 = r143.A07;
                C17070qD r92 = r143.A0C;
                C21860y6 r72 = r143.A09;
                C18610sj r63 = ((C126705tJ) r143).A01;
                AnonymousClass102 r55 = r143.A05;
                AnonymousClass6BE r47 = r143.A0D;
                new C120495gH(context2, r123, r112, r55, r133, r102, r143.A08, r72, r143.A0A, null, r63, r92, r47, r143.A0E).A01(new AnonymousClass6AW(r04, r143, str8, str9, str10, str11, str12, hashMap));
                return;
            }
            r143.A01(r33, r04, str7, str8, str9, str10, str11, str12, hashMap);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 200) {
            boolean z = false;
            if (i2 == 250) {
                HashMap hashMap = (HashMap) intent.getSerializableExtra("credBlocks");
                this.A0I.A07(C12960it.A0b("onLibraryResult for credentials: ", hashMap));
                if (hashMap != null && !hashMap.isEmpty()) {
                    z = true;
                }
                AnonymousClass009.A0F(z);
                A3G(hashMap);
            } else if (i2 == 251) {
                A37();
            } else if (i2 == 252) {
                this.A0I.A06("user canceled");
                this.A0H = false;
                if (this.A0G) {
                    this.A0G = false;
                    AaN();
                    return;
                }
                A2q();
                finish();
            }
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C117295Zj.A0e(this);
        String A04 = ((ActivityC13790kL) this).A01.A04();
        AnonymousClass009.A05(A04);
        this.A0E = A04;
        this.A0D = this.A0C.A01();
        this.A06 = ((AbstractActivityC121665jA) this).A0A.A04;
        C12990iw.A1N(new AnonymousClass5oV(this, false), ((ActivityC13830kP) this).A05);
        if (getIntent() != null) {
            getIntent().getStringExtra("extra_request_id");
        }
        if (bundle != null) {
            this.A0H = bundle.getBoolean("payAppShowPinErrorSavedInst");
            this.A00 = bundle.getInt("showPinConfirmCountSavedInst");
            ((AbstractActivityC121665jA) this).A03 = bundle.getInt("setupModeSavedInst", 1);
        }
        C14850m9 r0 = ((ActivityC13810kN) this).A0C;
        C14900mE r02 = ((ActivityC13810kN) this).A05;
        C15570nT r15 = ((ActivityC13790kL) this).A01;
        C17220qS r13 = ((AbstractActivityC121685jC) this).A0H;
        C18590sh r14 = this.A0C;
        C17070qD r12 = ((AbstractActivityC121685jC) this).A0P;
        C21860y6 r10 = ((AbstractActivityC121685jC) this).A0I;
        C1308460e r9 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r8 = ((AbstractActivityC121685jC) this).A0M;
        AnonymousClass102 r7 = this.A02;
        C17900ra r6 = ((AbstractActivityC121685jC) this).A0N;
        AnonymousClass6BE r5 = ((AbstractActivityC121665jA) this).A0D;
        this.A09 = new C120525gK(this, r02, r15, ((ActivityC13810kN) this).A07, r7, r0, r13, r9, ((AbstractActivityC121665jA) this).A0B, r10, ((AbstractActivityC121685jC) this).A0K, r8, r6, r12, this, r5, this.A0B, r14);
        this.A08 = new C120475gF(((ActivityC13790kL) this).A05, r0, r13, r9, r8);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i != 19) {
            return super.onCreateDialog(i);
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A06(R.string.payments_pin_encryption_error);
        C117295Zj.A0q(A0S, this, 48, R.string.yes);
        C117305Zk.A18(A0S, this, 49, R.string.no);
        A0S.A0B(true);
        A0S.A08(new DialogInterface.OnCancelListener() { // from class: X.624
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                C36021jC.A00(AbstractActivityC121545iU.this, 19);
            }
        });
        return A0S.create();
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C120525gK r1 = this.A09;
        if (r1 != null) {
            r1.A00 = null;
        }
        this.A04 = null;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("payAppShowPinErrorSavedInst", this.A0H);
        bundle.putInt("showPinConfirmCountSavedInst", this.A00);
        bundle.putInt("setupModeSavedInst", ((AbstractActivityC121665jA) this).A03);
    }
}
