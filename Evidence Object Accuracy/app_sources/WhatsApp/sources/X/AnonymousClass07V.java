package X;

import androidx.viewpager.widget.ViewPager;

/* renamed from: X.07V  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07V implements Runnable {
    public final /* synthetic */ ViewPager A00;

    public AnonymousClass07V(ViewPager viewPager) {
        this.A00 = viewPager;
    }

    @Override // java.lang.Runnable
    public void run() {
        ViewPager viewPager = this.A00;
        viewPager.setScrollState(0);
        viewPager.A09(viewPager.A0A);
    }
}
