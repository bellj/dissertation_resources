package X;

import java.lang.ref.ReferenceQueue;

/* renamed from: X.4HT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4HT {
    public static final AnonymousClass4I4 A00 = new AnonymousClass4I4();
    public static final AnonymousClass4I5 A01 = new AnonymousClass4I5();
    public static final Thread A02;
    public static final ReferenceQueue A03 = new ReferenceQueue();

    static {
        AnonymousClass5H9 r0 = new AnonymousClass5H9();
        A02 = r0;
        r0.start();
    }
}
