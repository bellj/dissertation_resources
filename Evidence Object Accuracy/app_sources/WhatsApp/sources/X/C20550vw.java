package X;

import android.content.ContentValues;
import android.text.TextUtils;

/* renamed from: X.0vw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20550vw {
    public final C16490p7 A00;

    public C20550vw(C16490p7 r1) {
        this.A00 = r1;
    }

    public void A00(C30241Wq r6, long j) {
        C16310on A02 = this.A00.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("message_row_id", Long.valueOf(j));
            if (!TextUtils.isEmpty(r6.A01)) {
                contentValues.put("parent_group_jid", r6.A01);
            }
            if (!TextUtils.isEmpty(r6.A00)) {
                contentValues.put("group_subject", r6.A00);
            }
            A02.A03.A06(contentValues, "message_quoted_blank_reply", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
