package X;

import com.whatsapp.util.Log;
import java.util.Collection;

/* renamed from: X.15X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15X implements AbstractC20460vn {
    @Override // X.AbstractC20460vn
    public void A5w(int i, String str) {
        StringBuilder sb = new StringBuilder("QPL: annotationKeyTooLong for marker: ");
        sb.append(i);
        sb.append(" (");
        sb.append(str);
        sb.append(")");
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void A5x(int i, String str, int i2) {
        StringBuilder sb = new StringBuilder("QPL: annotationSizeLimitExceeded for marker: ");
        sb.append(i);
        sb.append(" (");
        sb.append(str);
        sb.append(": ");
        sb.append(i2);
        sb.append(")");
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void A6J() {
        Log.e("QPL: backgroundListenerEventsFull");
    }

    @Override // X.AbstractC20460vn
    public void A9a(String str) {
        StringBuilder sb = new StringBuilder("QPL: errorCompressingFile: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void A9b(String str) {
        StringBuilder sb = new StringBuilder("QPL: errorDeletingFile: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void A9c(String str) {
        StringBuilder sb = new StringBuilder("QPL: errorParsingConfig: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void A9d(String str) {
        StringBuilder sb = new StringBuilder("QPL: errorUploadingFile: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void A9e(String str) {
        StringBuilder sb = new StringBuilder("QPL: errorWritingToFile: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void AIV(String str, double d, int i) {
        StringBuilder sb = new StringBuilder("QPL: illegalDoubleAnnotation for marker: ");
        sb.append(i);
        sb.append(" (");
        sb.append(str);
        sb.append(": ");
        sb.append(d);
        sb.append(")");
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void AKL(int i, String str) {
        StringBuilder sb = new StringBuilder("QPL: jsonFormatError for marker: ");
        sb.append(i);
        sb.append(" msg: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void ALM() {
        Log.e("QPL: maxFileCountReached");
    }

    @Override // X.AbstractC20460vn
    public void ALN(int i) {
        StringBuilder sb = new StringBuilder("QPL: maxMarkerCountExceeded for marker: ");
        sb.append(i);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void ALO(int i) {
        StringBuilder sb = new StringBuilder("QPL: maxPointCountExceeded for marker: ");
        sb.append(i);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void AZI(int i, String str) {
        StringBuilder sb = new StringBuilder("QPL: pointNameTooLong for marker: ");
        sb.append(i);
        sb.append(" (");
        sb.append(str);
        sb.append(")");
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void AZJ(int i, String str) {
        StringBuilder sb = new StringBuilder("QPL: pointToEndAtNotFound for marker: ");
        sb.append(i);
        sb.append(" and pointName: ");
        sb.append("start_foreground_service");
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void Af3(Collection collection) {
        StringBuilder sb = new StringBuilder("QPL: tooManyOpenMarkersToWrite: ");
        sb.append(collection);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC20460vn
    public void AfC() {
        Log.e("QPL: unfinishedListenerEventRemaining");
    }
}
