package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.34c  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34c extends AbstractC51472Uz {
    public AnonymousClass34Y A00;
    public boolean A01;
    public final C14900mE A02;
    public final AnonymousClass130 A03;
    public final AnonymousClass1J1 A04;
    public final C39091pH A05;

    public AnonymousClass34c(Context context, C14900mE r16, C15570nT r17, AnonymousClass130 r18, C15550nR r19, C15610nY r20, AnonymousClass1J1 r21, C63563Cb r22, C63543Bz r23, AnonymousClass01d r24, C14830m7 r25, AnonymousClass018 r26, AnonymousClass19M r27, C16630pM r28, AnonymousClass12F r29, C39091pH r30) {
        super(context, r17, r19, r20, r22, r23, r24, r25, r26, r27, r28, r29);
        A00();
        this.A02 = r16;
        this.A03 = r18;
        this.A04 = r21;
        this.A05 = r30;
        A01();
    }

    public void A07(C30411Xh r2, List list) {
        super.A05(r2, list);
        this.A00.setMessage(r2, list);
    }

    public void A08(C30351Xb r2, List list) {
        super.A05(r2, list);
        this.A00.setMessage(r2, list);
    }
}
