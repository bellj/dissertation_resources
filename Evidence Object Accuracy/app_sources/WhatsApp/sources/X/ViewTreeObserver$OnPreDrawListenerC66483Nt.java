package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.material.transformation.ExpandableBehavior;

/* renamed from: X.3Nt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66483Nt implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AnonymousClass2QX A02;
    public final /* synthetic */ ExpandableBehavior A03;

    public ViewTreeObserver$OnPreDrawListenerC66483Nt(View view, AnonymousClass2QX r2, ExpandableBehavior expandableBehavior, int i) {
        this.A03 = expandableBehavior;
        this.A01 = view;
        this.A00 = i;
        this.A02 = r2;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A01;
        C12980iv.A1G(view, this);
        ExpandableBehavior expandableBehavior = this.A03;
        if (expandableBehavior.A00 == this.A00) {
            AnonymousClass2QX r0 = this.A02;
            expandableBehavior.A0I((View) r0, view, ((AnonymousClass2QV) r0).A0F.A01, false);
        }
        return false;
    }
}
