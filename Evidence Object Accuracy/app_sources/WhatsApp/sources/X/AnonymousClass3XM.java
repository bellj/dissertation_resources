package X;

import com.whatsapp.contact.picker.BaseSharedPreviewDialogFragment;
import com.whatsapp.contact.picker.SharedTextPreviewDialogFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.io.File;

/* renamed from: X.3XM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XM implements AbstractC39381po {
    public final /* synthetic */ ViewOnClickCListenerShape14S0100000_I0_1 A00;

    @Override // X.AbstractC39381po
    public void AQA(Exception exc) {
    }

    public AnonymousClass3XM(ViewOnClickCListenerShape14S0100000_I0_1 viewOnClickCListenerShape14S0100000_I0_1) {
        this.A00 = viewOnClickCListenerShape14S0100000_I0_1;
    }

    @Override // X.AbstractC39381po
    public void AQX(File file, String str, byte[] bArr) {
        SharedTextPreviewDialogFragment sharedTextPreviewDialogFragment = (SharedTextPreviewDialogFragment) this.A00.A00;
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0G.setImageProgressBarVisibility(false);
        ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0G.setImageThumbVisibility(true);
        if (file == null) {
            Log.e("sharedtextpreviewdialogfragment/gif-preview/file is null");
        } else {
            sharedTextPreviewDialogFragment.startActivityForResult(AnonymousClass3AH.A00(sharedTextPreviewDialogFragment.A0B(), sharedTextPreviewDialogFragment.A0E, null, file, ((BaseSharedPreviewDialogFragment) sharedTextPreviewDialogFragment).A0H), 27);
        }
    }
}
