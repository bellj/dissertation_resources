package X;

import android.content.Context;

/* renamed from: X.34n  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass34n extends AnonymousClass2V0 {
    public boolean A00;

    public AnonymousClass34n(Context context, C15570nT r2, C15550nR r3, C15610nY r4, C63563Cb r5, C63543Bz r6, AnonymousClass01d r7, C14830m7 r8, AnonymousClass018 r9, AnonymousClass19M r10, C16630pM r11, AnonymousClass12F r12) {
        super(context, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
        A00();
    }
}
