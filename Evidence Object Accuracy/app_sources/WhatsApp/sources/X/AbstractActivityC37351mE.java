package X;

import com.whatsapp.report.ReportActivity;

/* renamed from: X.1mE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC37351mE extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC37351mE() {
        A0R(new C103404qh(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ReportActivity reportActivity = (ReportActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) reportActivity).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) reportActivity).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) reportActivity).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) reportActivity).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) reportActivity).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) reportActivity).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) reportActivity).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) reportActivity).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) reportActivity).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) reportActivity).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) reportActivity).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) reportActivity).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) reportActivity).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) reportActivity).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) reportActivity).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) reportActivity).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) reportActivity).A09 = r3.A06();
            ((ActivityC13790kL) reportActivity).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) reportActivity).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) reportActivity).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) reportActivity).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) reportActivity).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) reportActivity).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) reportActivity).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) reportActivity).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) reportActivity).A08 = (C249317l) r2.A8B.get();
            reportActivity.A0M = (C26531Dv) r2.A8T.get();
            reportActivity.A0K = (C20660w7) r2.AIB.get();
            reportActivity.A0E = (AnonymousClass18U) r2.AAU.get();
            reportActivity.A0Q = (C252018m) r2.A7g.get();
            reportActivity.A0I = (AnonymousClass018) r2.ANb.get();
            reportActivity.A0J = (AnonymousClass12H) r2.AC5.get();
            reportActivity.A0H = (C18360sK) r2.AN0.get();
        }
    }
}
