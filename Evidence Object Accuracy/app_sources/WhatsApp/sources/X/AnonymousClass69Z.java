package X;

import java.util.ArrayList;

/* renamed from: X.69Z  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69Z implements AbstractC43531xB {
    public final AnonymousClass102 A00;
    public final C17070qD A01;
    public final C130155yt A02;
    public final C130125yq A03;
    public final AnonymousClass61F A04;
    public final C22980zx A05;
    public final AbstractC14440lR A06;

    @Override // X.AbstractC43531xB
    public int AH0() {
        return 1;
    }

    public AnonymousClass69Z(AnonymousClass102 r1, C17070qD r2, C130155yt r3, C130125yq r4, AnonymousClass61F r5, C22980zx r6, AbstractC14440lR r7) {
        this.A06 = r7;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A05 = r6;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // X.AbstractC43531xB
    public ArrayList AYv(AnonymousClass102 r2, AnonymousClass1V8 r3) {
        throw C12980iv.A0u("Synchronous parsing is not supported in Async Mode");
    }

    @Override // X.AbstractC43531xB
    public C14580lf AYw(AnonymousClass1V8 r4) {
        C14580lf r2 = new C14580lf();
        this.A06.Ab2(new Runnable(r2, this, r4) { // from class: X.6JR
            public final /* synthetic */ C14580lf A00;
            public final /* synthetic */ AnonymousClass69Z A01;
            public final /* synthetic */ AnonymousClass1V8 A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r8v5, types: [X.5f4, X.1Zd, X.1ZP] */
            /* JADX WARNING: Code restructure failed: missing block: B:40:0x00cb, code lost:
                if (r8 != null) goto L_0x00cd;
             */
            /* JADX WARNING: Unknown variable types count: 1 */
            @Override // java.lang.Runnable
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                // Method dump skipped, instructions count: 498
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6JR.run():void");
            }
        });
        return r2;
    }
}
