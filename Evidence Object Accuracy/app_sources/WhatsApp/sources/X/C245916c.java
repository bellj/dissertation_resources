package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.data.device.DeviceChangeManager;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.16c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C245916c {
    public DeviceChangeManager A00;
    public final C15570nT A01;
    public final C16490p7 A02;
    public final C246116e A03;
    public final C246216f A04;
    public final C246016d A05;
    public volatile String A06;

    public C245916c(C15570nT r1, C16490p7 r2, C246116e r3, C246216f r4, C246016d r5) {
        this.A01 = r1;
        this.A05 = r5;
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
    }

    public C28601Of A00() {
        Iterator it = this.A04.A00().A01().iterator();
        AnonymousClass1YB r6 = new AnonymousClass1YB();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (((AnonymousClass1JU) entry.getValue()).A01 <= 0) {
                r6.A01(entry.getKey(), entry.getValue());
            }
        }
        return r6.A00();
    }

    public C28601Of A01(UserJid userJid) {
        C28601Of A00;
        C28601Of r0;
        AnonymousClass009.A0C("only get user for others", !this.A01.A0F(userJid));
        C246016d r8 = this.A05;
        C18460sU r13 = r8.A00;
        if (!r13.A0C()) {
            return C28601Of.A01;
        }
        Map map = r8.A02.A00;
        if (map.containsKey(userJid) && (r0 = (C28601Of) map.get(userJid)) != null) {
            return r0;
        }
        long A01 = r13.A01(userJid);
        C16310on A012 = r8.A01.get();
        try {
            synchronized (r8) {
                Cursor A09 = A012.A03.A09("SELECT device_jid_row_id, key_index FROM user_device WHERE user_jid_row_id = ?", new String[]{Long.toString(A01)});
                AnonymousClass1YB r5 = new AnonymousClass1YB();
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("device_jid_row_id");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("key_index");
                HashSet hashSet = new HashSet();
                while (A09.moveToNext()) {
                    long j = A09.getLong(columnIndexOrThrow);
                    long j2 = A09.getLong(columnIndexOrThrow2);
                    DeviceJid of = DeviceJid.of(r13.A03(j));
                    AnonymousClass009.A05(of);
                    boolean z = false;
                    if (of.device == 0) {
                        z = true;
                    }
                    if ((!z || j2 != 0) && (!(!z) || j2 <= 0)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("DeviceStore/getDevicesForUser/invalid devices jid=");
                        sb.append(of);
                        sb.append("; keyIndex=");
                        sb.append(j2);
                        Log.e(sb.toString());
                        hashSet.add(of);
                    }
                    r5.A01(of, Long.valueOf(j2));
                }
                if (!hashSet.isEmpty()) {
                    r8.A03.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(r8, userJid, hashSet, 1));
                }
                A00 = r5.A00();
                map.put(userJid, A00);
                AnonymousClass009.A05(A00);
                A09.close();
            }
            A012.close();
            return A00;
        } catch (Throwable th) {
            try {
                A012.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02() {
        String str;
        synchronized (this) {
            C15570nT r2 = this.A01;
            r2.A08();
            if (r2.A04 == null) {
                str = null;
            } else {
                HashSet hashSet = new HashSet(this.A04.A00().A02().A00);
                r2.A08();
                hashSet.add(r2.A04);
                str = AnonymousClass1YK.A00(hashSet);
            }
            this.A06 = str;
        }
    }

    public void A03(AnonymousClass1JO r4) {
        if (!r4.A00.isEmpty()) {
            C16310on A02 = this.A02.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                this.A04.A01(r4);
                A00.A00();
                A00.close();
                A02.close();
                A02();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public final void A04(AnonymousClass1JO r15, AnonymousClass1JO r16, AnonymousClass1JO r17, UserJid userJid) {
        boolean z;
        DeviceChangeManager deviceChangeManager = this.A00;
        if (deviceChangeManager != null) {
            Set set = r17.A00;
            if (!set.isEmpty()) {
                deviceChangeManager.A05.A00.execute(new RunnableBRunnable0Shape4S0200000_I0_4(deviceChangeManager, 43, r17));
            }
            if (deviceChangeManager.A0C.A05()) {
                Set set2 = r16.A00;
                if (!set2.isEmpty() && !set.isEmpty()) {
                    C15600nX r5 = deviceChangeManager.A08;
                    AnonymousClass1YJ r3 = new AnonymousClass1YJ();
                    r3.A01(r15);
                    AnonymousClass009.A05(r3.A00);
                    Iterator it = r17.iterator();
                    while (it.hasNext()) {
                        r3.A00.remove(it.next());
                    }
                    r3.A01(r16);
                    AnonymousClass1JO A00 = r3.A00();
                    C18680sq r52 = r5.A09;
                    if (r52.A0C() && !A00.A00.isEmpty()) {
                        StringBuilder sb = new StringBuilder("participant-user-store/onDevicesRefreshed/");
                        sb.append(userJid);
                        sb.append("/");
                        sb.append(A00);
                        Log.i(sb.toString());
                        Set<AbstractC15590nW> A03 = r52.A03(userJid);
                        HashMap hashMap = new HashMap();
                        boolean A07 = r52.A0C.A07(1108);
                        for (AbstractC15590nW r2 : A03) {
                            AnonymousClass1YM A002 = r52.A06.A00(r52.A05, r2);
                            AnonymousClass1YN A04 = A002.A04(A00, userJid, A07);
                            if (A04.A00 || A04.A01) {
                                hashMap.put(A002, Boolean.valueOf(A04.A02));
                            }
                        }
                        if (!hashMap.isEmpty()) {
                            C16310on A02 = r52.A08.A02();
                            try {
                                AnonymousClass1Lx A003 = A02.A00();
                                for (Map.Entry entry : hashMap.entrySet()) {
                                    r52.A07((AnonymousClass1YM) entry.getKey(), userJid, ((Boolean) entry.getValue()).booleanValue());
                                }
                                A003.A00();
                                A003.close();
                                A02.close();
                            } catch (Throwable th) {
                                try {
                                    A02.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        }
                    }
                } else if (!set2.isEmpty()) {
                    C18680sq r9 = deviceChangeManager.A08.A09;
                    if (r9.A0C() && !set2.isEmpty()) {
                        StringBuilder sb2 = new StringBuilder("participant-user-store/onDevicesAdded/");
                        sb2.append(userJid);
                        sb2.append("/");
                        sb2.append(r16);
                        Log.i(sb2.toString());
                        Set<AbstractC15590nW> A032 = r9.A03(userJid);
                        HashSet hashSet = new HashSet();
                        for (AbstractC15590nW r22 : A032) {
                            AnonymousClass1YM A004 = r9.A06.A00(r9.A05, r22);
                            AnonymousClass1YO r11 = (AnonymousClass1YO) A004.A02.get(userJid);
                            if (r11 == null) {
                                StringBuilder sb3 = new StringBuilder("GroupParticipants/addDevices/participant ");
                                sb3.append(userJid);
                                sb3.append(" doesn't exist");
                                Log.w(sb3.toString());
                            } else {
                                A004.A06 = true;
                                Iterator it2 = r16.iterator();
                                while (it2.hasNext()) {
                                    AnonymousClass1YP r32 = new AnonymousClass1YP((DeviceJid) it2.next(), false);
                                    ConcurrentHashMap concurrentHashMap = r11.A04;
                                    DeviceJid deviceJid = r32.A01;
                                    if (!concurrentHashMap.containsKey(deviceJid)) {
                                        concurrentHashMap.put(deviceJid, r32);
                                    }
                                }
                                if (!set2.isEmpty()) {
                                    A004.A0D();
                                }
                            }
                            hashSet.add(A004);
                        }
                        r9.A0A(userJid, hashSet, false);
                    }
                } else if (!set.isEmpty()) {
                    C18680sq r10 = deviceChangeManager.A08.A09;
                    if (r10.A0C() && !set.isEmpty()) {
                        StringBuilder sb4 = new StringBuilder("participant-user-store/onDevicesRemoved/");
                        sb4.append(userJid);
                        sb4.append("/");
                        sb4.append(r17);
                        Log.i(sb4.toString());
                        boolean A072 = r10.A0C.A07(1108);
                        Set<AbstractC15590nW> A033 = r10.A03(userJid);
                        HashSet hashSet2 = new HashSet();
                        boolean z2 = !A072;
                        for (AbstractC15590nW r23 : A033) {
                            AnonymousClass1YM A005 = r10.A06.A00(r10.A05, r23);
                            AnonymousClass1YO r112 = (AnonymousClass1YO) A005.A02.get(userJid);
                            if (r112 == null) {
                                StringBuilder sb5 = new StringBuilder("GroupParticipants/removeDevices/participant ");
                                sb5.append(userJid);
                                sb5.append(" doesn't exist");
                                Log.w(sb5.toString());
                                z = false;
                            } else {
                                z = z2;
                                Iterator it3 = r17.iterator();
                                while (it3.hasNext()) {
                                    AnonymousClass1YP r0 = (AnonymousClass1YP) r112.A04.remove(it3.next());
                                    if (r0 != null) {
                                        z |= r0.A00;
                                    }
                                }
                                if (!set.isEmpty()) {
                                    if (z) {
                                        A005.A0E();
                                    }
                                    A005.A0D();
                                }
                            }
                            z2 |= z;
                            hashSet2.add(A005);
                        }
                        r10.A0A(userJid, hashSet2, z2);
                    }
                }
            }
        }
    }

    public final void A05(AnonymousClass1JO r12, AnonymousClass1JO r13, AnonymousClass1JO r14, UserJid userJid, boolean z) {
        Set A01;
        DeviceChangeManager deviceChangeManager = this.A00;
        if (deviceChangeManager != null) {
            Set set = r14.A00;
            if (!set.isEmpty() && deviceChangeManager.A0C.A05()) {
                if (deviceChangeManager.A0B.A07(1108)) {
                    A01 = deviceChangeManager.A08.A06(userJid, new HashSet(set));
                } else {
                    A01 = deviceChangeManager.A01(userJid);
                }
                C18240s8 r0 = deviceChangeManager.A05;
                r0.A00.execute(new RunnableBRunnable0Shape0S0400000_I0(deviceChangeManager, A01, r14, userJid, 21));
            }
            if (!r13.A00.isEmpty() || !set.isEmpty() || !z) {
                deviceChangeManager.A02(r12, r13, r14, userJid, z);
            } else if (deviceChangeManager.A09.A0C.A07(903) && deviceChangeManager.A03.A00.getBoolean("security_notifications", false)) {
                if (deviceChangeManager.A06.A0D(userJid)) {
                    deviceChangeManager.A07.A0p(deviceChangeManager.A0D.A02(userJid, userJid, deviceChangeManager.A02.A00()));
                }
                for (AbstractC14640lm r4 : deviceChangeManager.A00(userJid)) {
                    deviceChangeManager.A07.A0p(deviceChangeManager.A0D.A02(r4, userJid, deviceChangeManager.A02.A00()));
                }
            }
        }
    }

    public void A06(AnonymousClass1JO r21, boolean z) {
        Log.i("DeviceManager/removeMyDevices/start");
        C15570nT r3 = this.A01;
        r3.A08();
        C27481Hq r0 = r3.A04;
        Set set = r21.A00;
        AnonymousClass009.A0C("never remove my primary device.", !set.contains(r0));
        if (!set.isEmpty()) {
            r3.A08();
            C27631Ih r4 = r3.A05;
            AnonymousClass009.A05(r4);
            C16310on A02 = this.A02.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                C246216f r6 = this.A04;
                AnonymousClass1JO A022 = r6.A00().A02();
                if (z) {
                    C16310on A023 = r6.A02.A02();
                    AnonymousClass1Lx A002 = A023.A00();
                    try {
                        synchronized (r6) {
                            long A003 = r6.A01.A00();
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("logout_time", Long.valueOf(A003));
                            String[] A0Q = C15380n4.A0Q(new HashSet(set));
                            String join = TextUtils.join(", ", Collections.nCopies(A0Q.length, "?"));
                            StringBuilder sb = new StringBuilder();
                            sb.append("device_id IN (");
                            sb.append(join);
                            sb.append(")");
                            A023.A03.A00("devices", contentValues, sb.toString(), A0Q);
                            A002.A00();
                            r6.A00 = null;
                        }
                        A002.close();
                        A023.close();
                    } catch (Throwable th) {
                        try {
                            A002.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } else {
                    r6.A01(r21);
                }
                AnonymousClass1JO r02 = AnonymousClass1JO.A01;
                A05(A022, r02, r21, r4, false);
                A00.A00();
                A00.close();
                A02.close();
                A02();
                A04(A022, r02, r21, r4);
            } catch (Throwable th2) {
                try {
                    A02.close();
                } catch (Throwable unused2) {
                }
                throw th2;
            }
        }
        Log.i("DeviceManager/removeMyDevices/done");
    }
}
