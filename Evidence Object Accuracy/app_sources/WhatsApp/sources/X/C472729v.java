package X;

import android.os.Build;

/* renamed from: X.29v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C472729v {
    public static boolean A00() {
        return Build.VERSION.SDK_INT >= 18;
    }

    public static boolean A01() {
        return Build.VERSION.SDK_INT >= 20;
    }

    public static boolean A02() {
        return Build.VERSION.SDK_INT >= 21;
    }

    public static boolean A03() {
        return Build.VERSION.SDK_INT >= 26;
    }

    public static boolean A04() {
        return Build.VERSION.SDK_INT >= 30;
    }
}
