package X;

import com.whatsapp.util.Log;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.16o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C247116o {
    public AnonymousClass1WT A00 = new AnonymousClass1WT(this);
    public boolean A01;
    public final C247416r A02;
    public final C16590pI A03;
    public final C14820m6 A04;
    public final AnonymousClass018 A05;
    public final C14850m9 A06;
    public final AtomicBoolean A07 = new AtomicBoolean(false);

    public C247116o(C247416r r3, C16590pI r4, C14820m6 r5, AnonymousClass018 r6, C14850m9 r7) {
        C16700pc.A0E(r5, 1);
        C16700pc.A0E(r6, 2);
        C16700pc.A0E(r7, 4);
        C16700pc.A0E(r4, 5);
        this.A04 = r5;
        this.A05 = r6;
        this.A02 = r3;
        this.A06 = r7;
        this.A03 = r4;
    }

    public final C30081Wa A00() {
        String string = this.A04.A00.getString("commerce_metadata_tanslations", null);
        if (string != null) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                JSONArray optJSONArray = jSONObject.optJSONArray("strings");
                int i = 0;
                if (optJSONArray != null) {
                    int length = optJSONArray.length();
                    while (i < length) {
                        int i2 = i + 1;
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        if (optJSONObject != null) {
                            String string2 = optJSONObject.getString("name");
                            C16700pc.A0B(string2);
                            String string3 = optJSONObject.getString("value");
                            C16700pc.A0B(string3);
                            linkedHashMap.put(string2, string3);
                        }
                        i = i2;
                    }
                }
                String string4 = jSONObject.getString("locale");
                C16700pc.A0B(string4);
                C30081Wa r2 = new C30081Wa(string4, linkedHashMap, jSONObject.getLong("expiresAt"));
                if (C16700pc.A0O(r2.A01, AnonymousClass018.A00(this.A05.A00).getLanguage())) {
                    return r2;
                }
                Log.e("CommerceTranslationsMetadataManager/CommerceMetadataTranslations/translation locale is different than system locale ");
                return null;
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public final void A01() {
        AtomicBoolean atomicBoolean = this.A07;
        if (!atomicBoolean.get()) {
            atomicBoolean.set(true);
            C247416r r9 = this.A02;
            r9.A00 = this.A00;
            C17220qS r8 = r9.A02;
            String A01 = r8.A01();
            C16700pc.A0B(A01);
            r8.A09(r9, new AnonymousClass1V8(new AnonymousClass1V8(new AnonymousClass1V8("translations", new AnonymousClass1W9[]{new AnonymousClass1W9("locale", r9.A01.A06())}), "commerce_metadata", new AnonymousClass1W9[0]), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("xmlns", "fb:thrift_iq"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("smax_id", "91"), new AnonymousClass1W9("id", A01)}), A01, 334, 32000);
        }
    }

    public final boolean A02() {
        boolean z;
        String str;
        Boolean valueOf;
        C30081Wa A00 = A00();
        if (A00 != null) {
            int i = (A00.A00 > (new Date().getTime() / 1000) ? 1 : (A00.A00 == (new Date().getTime() / 1000) ? 0 : -1));
            boolean z2 = false;
            if (i < 0) {
                z2 = true;
            }
            Boolean valueOf2 = Boolean.valueOf(z2);
            if (valueOf2 != null) {
                z = valueOf2.booleanValue();
                C30081Wa A002 = A00();
                boolean booleanValue = (A002 != null || (str = A002.A01) == null || (valueOf = Boolean.valueOf(str.equals(AnonymousClass018.A00(this.A05.A00).getLanguage()))) == null) ? false : valueOf.booleanValue();
                if (z && booleanValue) {
                    return false;
                }
            }
        }
        z = true;
        C30081Wa A002 = A00();
        if (A002 != null) {
        }
        return z ? true : true;
    }
}
