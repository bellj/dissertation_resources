package X;

import android.content.SharedPreferences;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.11k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C233711k {
    public static final Set A02;
    public SharedPreferences A00;
    public final C16630pM A01;

    static {
        HashSet hashSet = new HashSet();
        A02 = hashSet;
        hashSet.add("first_transient_server_failure_timestamp");
        Set set = A02;
        set.add("syncd_dirty");
        set.add("syncd_dirty_reason");
        set.add("syncd_last_companion_dereg_time");
        set.add("syncd_last_companion_dereg_logging_time");
        set.add("syncd_first_companion_reg_logging_time");
        set.add("syncd_bootstrap_state");
        set.add("syncd_bootstrapped_mutations");
        set.add("syncd_bootstrap_fail_time");
        set.add("syncd_last_lthash_consistency_check_time");
        set.add("syncd_one_time_cleanup_for_non_md_user");
    }

    public C233711k(C16630pM r1) {
        this.A01 = r1;
    }

    public synchronized long A00(String str) {
        long j;
        j = A01().getLong(str, 0);
        A01().edit().remove(str).apply();
        return j;
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("syncd_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public Set A02() {
        Set<String> stringSet;
        synchronized ("syncd_bootstrapped_mutations") {
            stringSet = A01().getStringSet("syncd_bootstrapped_mutations", Collections.emptySet());
            AnonymousClass009.A05(stringSet);
        }
        return stringSet;
    }

    public void A03(int i) {
        if (A01().getInt("syncd_dirty", -1) == -1) {
            A05(0);
            A01().edit().remove("syncd_last_companion_dereg_time").apply();
            A01().edit().putInt("syncd_dirty_reason", i).apply();
        }
    }

    public void A04(int i) {
        A01().edit().putInt("syncd_bootstrap_state", i).apply();
    }

    public void A05(int i) {
        A01().edit().putInt("syncd_dirty", i).apply();
    }

    public synchronized void A06(String str, long j) {
        A01().edit().putLong(str, A01().getLong(str, 0) + j).apply();
    }

    public void A07(Set set) {
        synchronized ("syncd_bootstrapped_mutations") {
            set.addAll(A02());
            A01().edit().putStringSet("syncd_bootstrapped_mutations", set).apply();
        }
    }

    public void A08(boolean z) {
        A01().edit().putBoolean("syncd_one_time_cleanup_for_non_md_user", z).apply();
    }
}
