package X;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.io.File;

/* renamed from: X.0SX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SX {
    public AnonymousClass0O7 A00;
    public final int A01;
    public final AnonymousClass0PA A02;
    public final String A03 = "c103703e120ae8cc73c9248622f3cd1e";
    public final String A04 = "49f946663a8deb7054212b8adda248c6";

    public AnonymousClass0SX(AnonymousClass0O7 r4, AnonymousClass0PA r5) {
        this.A01 = r5.A00;
        this.A00 = r4;
        this.A02 = r5;
    }

    public static final void A00(String str) {
        if (!str.equalsIgnoreCase(":memory:") && str.trim().length() != 0) {
            StringBuilder sb = new StringBuilder("deleting the database file: ");
            sb.append(str);
            Log.w("SupportSQLite", sb.toString());
            try {
                SQLiteDatabase.deleteDatabase(new File(str));
            } catch (Exception e) {
                Log.w("SupportSQLite", "delete failed: ", e);
            }
        }
    }

    public final void A01(AbstractC12920im r5) {
        SQLiteDatabase sQLiteDatabase = ((AnonymousClass0ZE) r5).A00;
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        String str = this.A03;
        StringBuilder sb = new StringBuilder("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '");
        sb.append(str);
        sb.append("')");
        sQLiteDatabase.execSQL(sb.toString());
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:73:0x000f */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v0, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r5v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r5v2, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007e, code lost:
        if (r1.moveToNext() == false) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0080, code lost:
        r2.add(r1.getString(0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0089, code lost:
        r1.close();
        r3 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0094, code lost:
        if (r3.hasNext() == false) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0096, code lost:
        r2 = (java.lang.String) r3.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a2, code lost:
        if (r2.startsWith("room_fts_content_sync_") == false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a4, code lost:
        r0 = new java.lang.StringBuilder("DROP TRIGGER IF EXISTS ");
        r0.append(r2);
        r6.A00.execSQL(r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b8, code lost:
        r1 = r5.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c0, code lost:
        if (r1.hasNext() == false) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c2, code lost:
        ((X.AnonymousClass0OL) r1.next()).A00(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00cc, code lost:
        r2 = r4.A00(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00d2, code lost:
        if (r2.A01 == false) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00d4, code lost:
        A01(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d7, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d8, code lost:
        r1 = new java.lang.StringBuilder("Migration didn't properly handle: ");
        r1.append(r2.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ed, code lost:
        throw new java.lang.IllegalStateException(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ee, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ef, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00f2, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000d, code lost:
        if (r5 != 0) goto L_0x000f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000f, code lost:
        r4 = r8.A02;
        r2 = new java.util.ArrayList();
        r6 = (X.AnonymousClass0ZE) r9;
        r1 = r6.AZi(new X.AnonymousClass0ZK("SELECT name FROM sqlite_master WHERE type = 'trigger'"));
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00f3 A[EDGE_INSN: B:74:0x00f3->B:53:0x00f3 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(X.AbstractC12920im r9, int r10, int r11) {
        /*
        // Method dump skipped, instructions count: 357
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0SX.A02(X.0im, int, int):void");
    }
}
