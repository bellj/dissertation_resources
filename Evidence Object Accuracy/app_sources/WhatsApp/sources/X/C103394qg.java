package X;

import android.content.Context;
import com.whatsapp.registration.VerifyTwoFactorAuth;

/* renamed from: X.4qg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103394qg implements AbstractC009204q {
    public final /* synthetic */ VerifyTwoFactorAuth A00;

    public C103394qg(VerifyTwoFactorAuth verifyTwoFactorAuth) {
        this.A00 = verifyTwoFactorAuth;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
