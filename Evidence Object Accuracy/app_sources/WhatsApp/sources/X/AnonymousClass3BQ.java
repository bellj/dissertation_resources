package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3BQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BQ {
    public long A00;
    public final String A01;
    public final String A02;
    public final Map A03;
    public final boolean A04;

    public AnonymousClass3BQ(String str, String str2, Map map, long j, boolean z) {
        Map emptyMap;
        C13020j0.A05(str);
        C13020j0.A05(str2);
        this.A01 = str;
        this.A02 = str2;
        this.A04 = z;
        this.A00 = j;
        if (map != null) {
            emptyMap = new HashMap(map);
        } else {
            emptyMap = Collections.emptyMap();
        }
        this.A03 = emptyMap;
    }
}
