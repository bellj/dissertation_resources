package X;

import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;

/* renamed from: X.16S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16S implements AbstractC21730xt {
    public Runnable A00;
    public final C15570nT A01;
    public final C15450nH A02;
    public final AnonymousClass16T A03;
    public final C233411h A04;
    public final C233711k A05;
    public final C14830m7 A06;
    public final AnonymousClass1GD A07 = new C41051so(this);
    public final C22100yW A08;
    public final C17220qS A09;
    public final AbstractC14440lR A0A;

    public AnonymousClass16S(C15570nT r2, C15450nH r3, AnonymousClass16T r4, C233411h r5, C233711k r6, C14830m7 r7, C22100yW r8, C17220qS r9, AbstractC14440lR r10) {
        this.A06 = r7;
        this.A01 = r2;
        this.A0A = r10;
        this.A02 = r3;
        this.A09 = r9;
        this.A04 = r5;
        this.A08 = r8;
        this.A05 = r6;
        this.A03 = r4;
    }

    public void A00() {
        C15570nT r4 = this.A01;
        r4.A08();
        synchronized (this) {
            Runnable runnable = this.A00;
            if (runnable != null) {
                this.A0A.AaP(runnable);
                this.A00 = null;
                Log.i("SyncdDeleteAllDataHandler/resetSchedule removed scheduled sync");
            }
        }
        if (A04()) {
            C17220qS r5 = this.A09;
            C16240og r1 = r5.A02;
            if (r1.A06 && r1.A04 == 2) {
                C233711k r3 = this.A05;
                r3.A05(r3.A01().getInt("syncd_dirty", -1) + 1);
                C22100yW r2 = this.A08;
                if (!r2.A08().isEmpty()) {
                    Log.i("SyncdDeleteAllDataApiHandler/handleDirtyState: logoutAllCompanionDevices");
                    r2.A0E("syncd_failure", false);
                    return;
                }
                Log.i("SyncdDeleteAllDataApiHandler/handleDirtyState: sendDeleteAllDataIq");
                r4.A08();
                if (r4.A04 != null) {
                    String A01 = r5.A01();
                    StringBuilder sb = new StringBuilder("SyncdDeleteAllDataApiHandler/sendIqWithCallback ");
                    sb.append(A01);
                    Log.i(sb.toString());
                    C41141sy r32 = new C41141sy("iq");
                    r32.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
                    r32.A04(new AnonymousClass1W9("xmlns", "w:sync:app:state"));
                    r32.A04(new AnonymousClass1W9("type", "set"));
                    r32.A04(new AnonymousClass1W9("id", A01));
                    r32.A05(new C41141sy("delete_all_data").A03());
                    r5.A0D(this, r32.A03(), A01, 250, 32000);
                }
            }
        }
    }

    public synchronized void A01() {
        boolean A04 = A04();
        StringBuilder sb = new StringBuilder();
        sb.append("SyncdDeleteAllDataHandler/schedule isSyncdDirtyAndShouldRetry = ");
        sb.append(A04);
        Log.i(sb.toString());
        if (A04) {
            this.A00 = this.A0A.AbK(new RunnableBRunnable0Shape4S0100000_I0_4(this, 13), "SyncdDeleteAllHandler/schedule", 1000);
        } else {
            this.A0A.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this.A03, 15));
        }
    }

    public void A02(int i) {
        this.A01.A08();
        Log.i("SyncdDeleteAllDataApiHandler/markSyncdDirty");
        C233711k r1 = this.A05;
        r1.A03(i);
        r1.A01().edit().putLong("syncd_last_fatal_error_time", this.A06.A00()).apply();
    }

    public boolean A03() {
        return this.A05.A01().getInt("syncd_dirty", -1) != -1;
    }

    public boolean A04() {
        int i = this.A05.A01().getInt("syncd_dirty", -1);
        return i != -1 && i < 4;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("SyncdDeleteAllDataApiHandler/onDeliveryFailure ");
        sb.append(str);
        Log.e(sb.toString());
        A01();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        Pair A01 = C41151sz.A01(r4);
        StringBuilder sb = new StringBuilder("SyncdDeleteAllDataApiHandler/onError ");
        sb.append(A01);
        Log.e(sb.toString());
        A01();
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        StringBuilder sb = new StringBuilder("SyncdDeleteAllDataApiHandler/onSuccess ");
        sb.append(str);
        sb.append(" response: ");
        sb.append(r4);
        Log.i(sb.toString());
        this.A0A.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(this, 14));
    }
}
