package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.0VD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VD implements Drawable.Callback {
    public final /* synthetic */ AnonymousClass07M A00;

    public AnonymousClass0VD(AnonymousClass07M r1) {
        this.A00 = r1;
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        this.A00.invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        this.A00.scheduleSelf(runnable, j);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        this.A00.unscheduleSelf(runnable);
    }
}
