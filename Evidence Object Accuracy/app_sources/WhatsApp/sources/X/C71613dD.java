package X;

import com.whatsapp.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.impl.client.EntityEnclosingRequestWrapper;
import org.apache.http.impl.client.RequestWrapper;
import org.apache.http.protocol.HttpContext;

/* renamed from: X.3dD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71613dD implements HttpRequestInterceptor {
    public int A00;
    public final int A01 = 3;
    public final C18790t3 A02;

    public C71613dD(C18790t3 r2) {
        this.A02 = r2;
    }

    public void process(HttpRequest httpRequest, HttpContext httpContext) {
        StringBuilder sb;
        this.A00++;
        if (httpRequest instanceof EntityEnclosingRequestWrapper) {
            EntityEnclosingRequestWrapper entityEnclosingRequestWrapper = (EntityEnclosingRequestWrapper) httpRequest;
            HttpEntity entity = entityEnclosingRequestWrapper.getEntity();
            if (entity == null) {
                httpRequest.getRequestLine();
                return;
            }
            long contentLength = entity.getContentLength();
            if (contentLength <= 0) {
                sb = C12960it.A0k("gdrive-api/request-interceptor/process/length/");
                sb.append(contentLength);
            } else {
                entityEnclosingRequestWrapper.setEntity(new C71593dB(this, entity));
                return;
            }
        } else if (!(httpRequest instanceof RequestWrapper)) {
            sb = C12960it.A0k("gdrive-request-interceptor/process/request-is-not-a-wrapper ");
            sb.append(httpRequest);
        } else {
            return;
        }
        Log.e(sb.toString());
    }
}
