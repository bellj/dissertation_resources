package X;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.2VJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VJ extends AnonymousClass02M {
    public final ContentResolver A00;
    public final Handler A01;
    public final C18720su A02;
    public final C16590pI A03;
    public final AnonymousClass2FO A04;
    public final AnonymousClass2VK A05;
    public final AnonymousClass4L9 A06;
    public final AbstractC14440lR A07;
    public final List A08;
    public final Map A09;

    public AnonymousClass2VJ(ContentResolver contentResolver, Handler handler, C18720su r5, C16590pI r6, AnonymousClass2FO r7, AnonymousClass2VK r8, AbstractC14440lR r9, List list) {
        HashMap hashMap = new HashMap();
        this.A09 = hashMap;
        this.A06 = new AnonymousClass4L9(hashMap);
        this.A08 = list;
        this.A05 = r8;
        this.A01 = handler;
        this.A00 = contentResolver;
        this.A02 = r5;
        this.A03 = r6;
        this.A07 = r9;
        this.A04 = r7;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A08.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r4, int i) {
        AbstractC75753kM r42 = (AbstractC75753kM) r4;
        r42.A0H.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(r42, 28, this));
        r42.A09((Integer) this.A08.get(i));
        AbstractC16350or A08 = r42.A08();
        if (A08 != null) {
            this.A09.put(Integer.valueOf(i), A08);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        if (i == 5) {
            return new AnonymousClass475(LayoutInflater.from(context).inflate(R.layout.wallpaper_category_use_default_wallpaper, (ViewGroup) null), this.A06);
        } else if (i == 4) {
            return new AnonymousClass474(LayoutInflater.from(context).inflate(R.layout.wallpaper_category_remove_custom_wallpaper, (ViewGroup) null), this.A06);
        } else if (i != 3) {
            return new AnonymousClass35G(this.A06, new AnonymousClass2d1(context));
        } else {
            Handler handler = this.A01;
            ContentResolver contentResolver = this.A00;
            C18720su r1 = this.A02;
            C16590pI r5 = this.A03;
            AbstractC14440lR r10 = this.A07;
            return new AnonymousClass35H(r5, this.A04, new C457522x(contentResolver, handler, r1, "wallpaper-category"), this.A06, new AnonymousClass2d1(context), r10);
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((Number) this.A08.get(i)).intValue();
    }
}
