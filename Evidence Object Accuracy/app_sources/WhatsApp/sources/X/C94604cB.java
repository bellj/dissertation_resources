package X;

import android.util.Log;

/* renamed from: X.4cB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94604cB {
    public static volatile AnonymousClass4D6 A00;

    public static void A00() {
        if (A00 == null) {
            synchronized (C94604cB.class) {
                if (A00 == null) {
                    A00 = new AnonymousClass4D6();
                }
            }
        }
    }

    public static void A01(String str) {
        A00();
        if (AnonymousClass4FY.A00) {
            Log.e(C12960it.A0d(str, C12960it.A0j("RenderCore:")), "More than 4 recursive mount attempts. Skipping mounting the latest version.", null);
        }
    }
}
