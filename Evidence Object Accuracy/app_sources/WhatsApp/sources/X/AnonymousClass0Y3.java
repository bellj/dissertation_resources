package X;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

/* renamed from: X.0Y3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Y3 implements AbstractC12360hn {
    public final GestureDetector A00;

    public AnonymousClass0Y3(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this.A00 = new GestureDetector(context, onGestureListener, null);
    }

    @Override // X.AbstractC12360hn
    public boolean AXZ(MotionEvent motionEvent) {
        return this.A00.onTouchEvent(motionEvent);
    }

    @Override // X.AbstractC12360hn
    public void AcG(boolean z) {
        this.A00.setIsLongpressEnabled(z);
    }
}
