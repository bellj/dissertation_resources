package X;

import android.view.View;

/* renamed from: X.5oE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123965oE extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ C122055kq A00;
    public final /* synthetic */ C122145kz A01;

    public C123965oE(C122055kq r1, C122145kz r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        C122055kq r0 = this.A00;
        r0.A01.ATb(r0.A00, r0.A02, r0.A03, r0.A04, r0.A05);
    }
}
