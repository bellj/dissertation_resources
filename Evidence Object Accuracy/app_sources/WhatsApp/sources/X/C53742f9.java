package X;

import android.view.View;
import com.whatsapp.PagerSlidingTabStrip;
import com.whatsapp.R;

/* renamed from: X.2f9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53742f9 extends AnonymousClass019 implements AbstractC14800m4 {
    public final AnonymousClass4OE[] A00 = new AnonymousClass4OE[2];
    public final /* synthetic */ AnonymousClass34P A01;

    @Override // X.AnonymousClass01A
    public int A01() {
        return 2;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53742f9(AnonymousClass01F r2, AnonymousClass34P r3) {
        super(r2, 0);
        this.A01 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000b, code lost:
        if (r5 != 1) goto L_0x000d;
     */
    @Override // X.AnonymousClass01A
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.CharSequence A04(int r5) {
        /*
            r4 = this;
            X.34P r3 = r4.A01
            X.018 r0 = r3.A0G
            boolean r2 = X.C28141Kv.A01(r0)
            r1 = 1
            if (r5 == 0) goto L_0x0014
            if (r5 == r1) goto L_0x0016
        L_0x000d:
            java.lang.String r0 = "The item position should be less than: 2"
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x0014:
            r2 = r2 ^ 1
        L_0x0016:
            r0 = 2131887403(0x7f12052b, float:1.9409412E38)
            if (r2 == 0) goto L_0x0020
            if (r2 != r1) goto L_0x000d
            r0 = 2131887414(0x7f120536, float:1.9409434E38)
        L_0x0020:
            java.lang.String r0 = r3.getString(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53742f9.A04(int):java.lang.CharSequence");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000b, code lost:
        if (r3 != 1) goto L_0x000d;
     */
    @Override // X.AnonymousClass019
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass01E A0G(int r3) {
        /*
            r2 = this;
            X.34P r0 = r2.A01
            X.018 r0 = r0.A0G
            boolean r1 = X.C28141Kv.A01(r0)
            r0 = 1
            if (r3 == 0) goto L_0x0014
            if (r3 == r0) goto L_0x0016
        L_0x000d:
            java.lang.String r0 = "The item position should be less than: 2"
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x0014:
            r1 = r1 ^ 1
        L_0x0016:
            if (r1 == 0) goto L_0x0020
            if (r1 != r0) goto L_0x000d
            com.whatsapp.qrcode.contactqr.QrScanCodeFragment r0 = new com.whatsapp.qrcode.contactqr.QrScanCodeFragment
            r0.<init>()
            return r0
        L_0x0020:
            com.whatsapp.qrcode.contactqr.ContactQrMyCodeFragment r0 = new com.whatsapp.qrcode.contactqr.ContactQrMyCodeFragment
            r0.<init>()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53742f9.A0G(int):X.01E");
    }

    @Override // X.AbstractC14800m4
    public View AEv(int i) {
        AnonymousClass4OE[] r3 = this.A00;
        if (r3[i] == null) {
            PagerSlidingTabStrip pagerSlidingTabStrip = this.A01.A06;
            AnonymousClass4OE r2 = new AnonymousClass4OE(C12960it.A0F(C12960it.A0E(pagerSlidingTabStrip), pagerSlidingTabStrip, R.layout.qr_tab));
            CharSequence A04 = A04(i);
            AnonymousClass009.A05(A04);
            r2.A01.setText(A04);
            r3[i] = r2;
        }
        return r3[i].A00;
    }
}
