package X;

import android.os.Parcel;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3nQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77523nQ extends AbstractC77743nm {
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* bridge */ /* synthetic */ AnonymousClass5SX A02(Status status) {
        return status;
    }

    public C77523nQ(AnonymousClass1U8 r1) {
        super(r1);
    }

    @Override // X.AnonymousClass1UI
    public final /* bridge */ /* synthetic */ void A07(AnonymousClass5QV r6) {
        C56362kl r62 = (C56362kl) r6;
        C98374ia r4 = (C98374ia) r62.A03();
        BinderC77503nO r1 = new BinderC77503nO(this);
        GoogleSignInOptions googleSignInOptions = r62.A00;
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(r4.A01);
        obtain.writeStrongBinder(r1.asBinder());
        obtain.writeInt(1);
        googleSignInOptions.writeToParcel(obtain, 0);
        r4.A00(102, obtain);
    }
}
