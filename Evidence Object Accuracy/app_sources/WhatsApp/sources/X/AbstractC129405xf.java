package X;

import android.os.SystemClock;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;

/* renamed from: X.5xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC129405xf {
    public void A00(Exception exc) {
        AnonymousClass643 r2;
        if (this instanceof C119035cm) {
            C119035cm r1 = (C119035cm) this;
            r1.A01.A00(exc);
            r1.A02.A0A.A00(null, "restart_preview_video_recording_failed", new AnonymousClass6KN(r1));
        } else if (this instanceof C118995ci) {
            C118995ci r22 = (C118995ci) this;
            r22.A01.A0e = false;
            AbstractC129405xf r0 = r22.A00;
            if (r0 != null) {
                r0.A00(exc);
            }
        } else if (this instanceof C119025cl) {
            C119025cl r4 = (C119025cl) this;
            AnonymousClass616.A00();
            AnonymousClass661 r3 = r4.A01;
            C1308560f r23 = r3.A0T;
            r23.A05(new RunnableC135126Hg(r4, exc), r3.A0S.A03);
            if (!C1308760h.A02(C130405zM.A01) || !r3.A0W.get()) {
                r23.A07("take_photo_exception_restart_preview", new IDxCallableShape15S0100000_3_I1(r4, 5));
            }
        } else if (this instanceof C118965cf) {
            C129305xV r32 = ((C118965cf) this).A00.A0K;
            r32.A00(null, EnumC124565pk.EXCEPTION, r32.A02);
        } else if (!(this instanceof C118905cZ)) {
            if (this instanceof C118955ce) {
                r2 = ((C118955ce) this).A00;
            } else if (this instanceof C118945cd) {
                r2 = ((C118945cd) this).A00;
            } else if (this instanceof C119015ck) {
                C119015ck r5 = (C119015ck) this;
                AnonymousClass643 r42 = r5.A00;
                synchronized (r42.A0U) {
                    if (r42.A0X) {
                        r42.A0X = false;
                        AnonymousClass4K3 r24 = r42.A0W;
                        r42.A0W = null;
                        if (r24 != null) {
                            Object[] A1a = C12980iv.A1a();
                            C12990iw.A1P(r24, exc, A1a);
                            AnonymousClass643.A00(r42, A1a, 10);
                        }
                        if (r5.A02) {
                            r5.A01.countDown();
                            return;
                        }
                        return;
                    }
                    return;
                }
            } else if (this instanceof C118935cc) {
                return;
            } else {
                if (!(this instanceof C118925cb)) {
                    AnonymousClass643 r43 = ((C118915ca) this).A00;
                    synchronized (r43.A0U) {
                        if (r43.A0X) {
                            r43.A0X = false;
                            AnonymousClass4K3 r25 = r43.A0W;
                            r43.A0W = null;
                            if (r25 != null) {
                                Object[] A1a2 = C12980iv.A1a();
                                C12990iw.A1P(r25, exc, A1a2);
                                AnonymousClass643.A00(r43, A1a2, 10);
                            }
                        }
                    }
                    return;
                }
                ((C118925cb) this).A00.A08 = null;
                return;
            }
            if (!r2.A0E) {
                Object[] A1a3 = C12980iv.A1a();
                C12990iw.A1P(r2.A0T.A00, exc, A1a3);
                AnonymousClass643.A00(r2, A1a3, 3);
            }
        }
    }

    public void A01(Object obj) {
        AbstractC129405xf r0;
        AnonymousClass643 r2;
        int i;
        AbstractC1311761o r1;
        AnonymousClass4K3 r22;
        if (this instanceof C119035cm) {
            C119035cm r3 = (C119035cm) this;
            r3.A02.A00 = SystemClock.elapsedRealtime();
            r0 = r3.A01;
        } else if (this instanceof C118995ci) {
            r0 = ((C118995ci) this).A00;
            if (r0 == null) {
                return;
            }
        } else if (!(this instanceof C119025cl) && !(this instanceof C118965cf) && !(this instanceof C118905cZ)) {
            if (this instanceof C118955ce) {
                C127255uC r7 = (C127255uC) obj;
                r2 = ((C118955ce) this).A00;
                r2.A08 = r7;
                if (!r2.A0E && (r1 = r2.A0N) != null && r1.isConnected()) {
                    int ABB = r1.ABB();
                    int i2 = 0;
                    if (ABB != 0) {
                        i2 = 1;
                        if (1 != ABB) {
                            throw C12990iw.A0m(C12960it.A0W(ABB, "Could not convert camera facing from optic: "));
                        }
                    }
                    r2.A00 = i2;
                    r2.A0H(r7);
                    i = 2;
                } else {
                    return;
                }
            } else if (this instanceof C118945cd) {
                C127255uC r72 = (C127255uC) obj;
                r2 = ((C118945cd) this).A00;
                r2.A08 = r72;
                if (!r2.A0E) {
                    r2.A0H(r72);
                    AbstractC1311761o r12 = r2.A0N;
                    r12.AcO(r2.A0K);
                    if (r2.A09 != null) {
                        r12.A5k(r2.A0P);
                    }
                    i = 1;
                } else {
                    return;
                }
            } else if (this instanceof C119015ck) {
                C119015ck r5 = (C119015ck) this;
                AnonymousClass643 r4 = r5.A00;
                synchronized (r4.A0U) {
                    if (r4.A0X) {
                        r4.A0X = false;
                        AnonymousClass4K3 r23 = r4.A0W;
                        r4.A0W = null;
                        if (r23 != null) {
                            Object[] A1a = C12980iv.A1a();
                            C12990iw.A1P(r23, obj, A1a);
                            AnonymousClass643.A00(r4, A1a, 9);
                        }
                        if (r5.A02) {
                            r5.A01.countDown();
                            return;
                        }
                        return;
                    }
                    return;
                }
            } else if (this instanceof C118935cc) {
                AnonymousClass643 r24 = ((C118935cc) this).A00;
                AnonymousClass643.A00(r24, new Object[]{r24, r24.A08, Integer.valueOf(r24.A06), Integer.valueOf(r24.A05)}, 15);
                return;
            } else if (!(this instanceof C118925cb)) {
                AnonymousClass643 r42 = ((C118915ca) this).A00;
                synchronized (r42.A0U) {
                    if (r42.A0X && (r22 = r42.A0W) != null) {
                        Object[] A1a2 = C12980iv.A1a();
                        C12990iw.A1P(r22, obj, A1a2);
                        AnonymousClass643.A00(r42, A1a2, 8);
                    }
                }
                return;
            } else {
                ((C118925cb) this).A00.A08 = null;
                return;
            }
            AnonymousClass643.A00(r2, r2.A0T.A00, i);
            return;
        } else {
            return;
        }
        r0.A01(obj);
    }
}
