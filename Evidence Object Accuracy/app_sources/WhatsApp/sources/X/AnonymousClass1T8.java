package X;

import java.security.PrivilegedAction;

/* renamed from: X.1T8  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1T8 implements PrivilegedAction {
    @Override // java.security.PrivilegedAction
    public Object run() {
        return System.getProperty("line.separator");
    }
}
