package X;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.3Bl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63403Bl {
    public int A00;
    public int A01;
    public int A02;
    public String A03;
    public ArrayList A04;
    public final C18790t3 A05;
    public final C18810t5 A06;
    public final File A07;
    public final List A08 = Arrays.asList("es-AR", "en-AU", "de-AT", "nl-BE", "fr-BE", "pt-BR", "en-CA", "fr-CA", "es-CL", "da-DK", "fi-FI", "fr-FR", "de-DE", "zh-HK", "en-IN", "en-ID", "en-IE", "it-IT", "ja-JP", "ko-KR", "en-MY", "es-MX", "nl-NL", "en-NZ", "no-NO", "zh-CN", "pl-PL", "pt-PT", "en-PH", "ru-RU", "ar-SA", "en-ZA", "es-ES", "sv-SE", "fr-CH", "de-CH", "zh-TW", "tr-TR", "en-GB", "en-US", "es-US");

    public C63403Bl(C18790t3 r5, C16590pI r6, C18810t5 r7, String str) {
        this.A05 = r5;
        this.A06 = r7;
        this.A01 = 50;
        this.A03 = str;
        File file = new File(r6.A00.getCacheDir(), "Bing");
        this.A07 = file;
        file.mkdirs();
    }
}
