package X;

import android.text.TextUtils;
import com.facebook.redex.IDxAListenerShape1S0200000_3_I1;
import com.whatsapp.payments.ui.NoviPayBloksActivity;

/* renamed from: X.5dN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119225dN extends ActivityC13790kL {
    public AbstractActivityC119225dN() {
        C117295Zj.A0p(this, 0);
    }

    public static C18660so A02(AnonymousClass01J r1, AbstractActivityC121705jc r2) {
        r2.A0N = (AnonymousClass61E) r1.AEY.get();
        r2.A0J = (AnonymousClass60T) r1.AFI.get();
        return (C18660so) r1.AEf.get();
    }

    public static AnonymousClass61D A03(AbstractActivityC123635nW r6) {
        return new AnonymousClass61D(r6.A00, ((ActivityC13790kL) r6).A05, ((ActivityC13810kN) r6).A0C, r6.A04, r6.A06, r6.A0C);
    }

    public static Object A09(AnonymousClass01J r1, AbstractActivityC121705jc r2) {
        r2.A0O = (AnonymousClass605) r1.AEj.get();
        r2.A00 = (C25841Ba) r1.A1d.get();
        r2.A01 = (AnonymousClass1BZ) r1.A1l.get();
        r2.A0K = (AnonymousClass17V) r1.AEX.get();
        r2.A0C = (C18600si) r1.AEo.get();
        r2.A0V = (C22120yY) r1.ANn.get();
        r2.A09 = (C21860y6) r1.AE6.get();
        return r1.AN1.get();
    }

    public static AnonymousClass01N A0A(AnonymousClass01J r1, AbstractActivityC119645em r2, C249317l r3) {
        ((ActivityC13790kL) r2).A08 = r3;
        r2.A07 = (C16120oU) r1.ANE.get();
        r2.A01 = (C128695wW) r1.A1m.get();
        return r1.A9C;
    }

    public static void A0B(AnonymousClass2FL r1, AnonymousClass01J r2, AbstractActivityC121705jc r3, AnonymousClass01N r4) {
        ((AbstractActivityC119645em) r3).A02 = (AnonymousClass60K) r4.get();
        ((AbstractActivityC119645em) r3).A06 = r2.A2U();
        ((AbstractActivityC119645em) r3).A00 = (C48912Ik) r1.A19.get();
        C90204Mz r12 = new C90204Mz();
        r12.A01 = C18000rk.A00(r2.AGe);
        ((AbstractActivityC119645em) r3).A04 = r12;
        ((AbstractActivityC119645em) r3).A08 = r2.A4d();
        r3.A06 = (C20660w7) r2.AIB.get();
        r3.A0H = (C129135xE) r2.AFj.get();
        r3.A0U = (AnonymousClass1BY) r2.ALj.get();
        r3.A0W = (C252018m) r2.A7g.get();
        r3.A0G = (C17070qD) r2.AFC.get();
        r3.A0T = (C18590sh) r2.AES.get();
        r3.A04 = (C15650ng) r2.A4m.get();
    }

    public static void A0D(AnonymousClass01J r1, C18660so r2, AbstractActivityC123635nW r3) {
        ((AbstractActivityC121705jc) r3).A0B = r2;
        r3.A0S = (C129395xe) r1.AEy.get();
        r3.A07 = (AnonymousClass60Y) r1.ADK.get();
        r3.A00 = C20910wW.A00();
        r3.A04 = (C130155yt) r1.ADA.get();
        r3.A08 = (AnonymousClass61F) r1.AD9.get();
        r3.A0B = (C129235xO) r1.ADR.get();
        r3.A06 = (C130125yq) r1.ADJ.get();
        r3.A0A = (C130105yo) r1.ADZ.get();
        r3.A03 = (AnonymousClass61M) r1.AF1.get();
        r3.A0C = r1.A3w();
        r3.A02 = (C22480z9) r1.AEc.get();
        r3.A09 = (C129435xi) r1.ADL.get();
    }

    public static void A0K(AnonymousClass01J r1, AbstractActivityC121705jc r2, Object obj) {
        r2.A03 = (C15890o4) obj;
        r2.A0Q = new C125055qc();
        r2.A0D = (C18610sj) r1.AF0.get();
        r2.A0P = (C130015yf) r1.AEk.get();
        r2.A02 = (C248917h) r1.AMf.get();
        r2.A0M = (C129095xA) r1.ACy.get();
        r2.A0E = (C17900ra) r1.AF5.get();
        r2.A07 = (C129925yW) r1.AEW.get();
        r2.A05 = (C20370ve) r1.AEu.get();
        r2.A0F = (C18620sk) r1.AFB.get();
        r2.A0I = (C128135vc) r1.AF2.get();
        r2.A0A = (C18650sn) r1.AEe.get();
    }

    public static void A0L(AnonymousClass3FE r2, C130785zy r3) {
        r2.A02("on_success", NoviPayBloksActivity.A0p((AnonymousClass1V8) r3.A02));
    }

    public static void A0M(AnonymousClass3FE r1, CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            r1.A00("");
        }
    }

    public static void A0N(C130155yt r6, C1310460z r7, Object obj, Object obj2, int i) {
        r6.A09(new IDxAListenerShape1S0200000_3_I1(obj, i, obj2), r7, 13, "set", 2);
    }
}
