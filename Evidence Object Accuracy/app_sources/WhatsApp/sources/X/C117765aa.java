package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5aa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117765aa extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public Button A02;
    public ImageView A03;
    public TextView A04;
    public AnonymousClass2P7 A05;
    public boolean A06;

    public C117765aa(Context context) {
        super(context);
        if (!this.A06) {
            this.A06 = true;
            generatedComponent();
        }
        C12960it.A0E(this).inflate(R.layout.card_details_alert, (ViewGroup) this, true);
        setOrientation(1);
        setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.product_margin_16dp));
        this.A02 = (Button) findViewById(R.id.card_details_alert_button);
        this.A04 = C12960it.A0J(this, R.id.card_details_alert_message);
        this.A03 = C12970iu.A0L(this, R.id.card_details_alert_icon);
        this.A00 = findViewById(R.id.card_details_alert_icon_container);
        this.A01 = findViewById(R.id.card_details_alert_divider);
        setAlertType(1);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A05;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A05 = r0;
        }
        return r0.generatedComponent();
    }

    public void setAlertButtonClickListener(View.OnClickListener onClickListener) {
        this.A02.setOnClickListener(onClickListener);
    }

    public void setAlertButtonText(String str) {
        this.A02.setText(str);
    }

    public void setAlertButtonVisibility(int i) {
        this.A02.setVisibility(i);
    }

    private void setAlertIcon(Drawable drawable) {
        this.A03.setImageDrawable(drawable);
    }

    private void setAlertIconTint(int i) {
        AnonymousClass2GE.A07(this.A03, i);
    }

    private void setAlertMessageText(String str) {
        this.A04.setText(str);
    }

    public void setAlertType(int i) {
        Context context;
        int i2;
        Context context2;
        int i3;
        Resources resources;
        int i4;
        String string;
        if (i != 0) {
            if (i == 2) {
                setAlertButtonVisibility(8);
                setAlertMessageText(getContext().getString(R.string.card_updated_alert_message));
                resources = getResources();
                i4 = R.drawable.ic_settings_info;
            } else if (i != 3) {
                if (i != 4) {
                    setAlertButtonVisibility(0);
                    setAlertButtonText(getContext().getString(R.string.verify_card));
                    string = getContext().getString(R.string.verify_payment_card_message);
                } else {
                    setAlertButtonVisibility(0);
                    setAlertButtonText(getResources().getString(R.string.verify_card));
                    string = getResources().getString(R.string.verify_inactive_payment_card_message);
                }
                setAlertMessageText(string);
                resources = getResources();
                i4 = R.drawable.ic_settings_secure;
            } else {
                setAlertButtonVisibility(0);
                setAlertButtonText(getContext().getString(R.string.remove_card));
                context = getContext();
                i2 = R.string.card_suspended_alert_message;
            }
            setAlertIcon(resources.getDrawable(i4));
            context2 = getContext();
            i3 = R.color.payment_method_verify_icon_tint;
            setAlertIconTint(AnonymousClass00T.A00(context2, i3));
        }
        setAlertButtonVisibility(0);
        setAlertButtonText(getContext().getString(R.string.remove_card));
        context = getContext();
        i2 = R.string.card_deleted_alert_message;
        setAlertMessageText(context.getString(i2));
        setAlertIcon(getResources().getDrawable(R.drawable.ic_settings_warning));
        context2 = getContext();
        i3 = R.color.payment_method_remove_card_icon_tint;
        setAlertIconTint(AnonymousClass00T.A00(context2, i3));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void setAlertType(String str) {
        Context context;
        int i;
        Context context2;
        int i2;
        switch (str.hashCode()) {
            case -1757659853:
                if (str.equals("VOIDED")) {
                    setAlertButtonVisibility(8);
                    context = getContext();
                    i = R.string.card_deleted_alert_message;
                    setAlertMessageText(context.getString(i));
                    setAlertIcon(getResources().getDrawable(R.drawable.ic_fbpay_error));
                    context2 = getContext();
                    i2 = R.color.payment_method_remove_card_icon_tint;
                    break;
                }
                setAlertMessageText(getContext().getString(R.string.verify_payment_card_message));
                setAlertIcon(getResources().getDrawable(R.drawable.ic_settings_secure));
                context2 = getContext();
                i2 = R.color.payment_method_verify_icon_tint;
                break;
            case -591252731:
                if (str.equals("EXPIRED")) {
                    context = getContext();
                    i = R.string.payment_card_date_expired_error;
                    setAlertMessageText(context.getString(i));
                    setAlertIcon(getResources().getDrawable(R.drawable.ic_fbpay_error));
                    context2 = getContext();
                    i2 = R.color.payment_method_remove_card_icon_tint;
                    break;
                }
                setAlertMessageText(getContext().getString(R.string.verify_payment_card_message));
                setAlertIcon(getResources().getDrawable(R.drawable.ic_settings_secure));
                context2 = getContext();
                i2 = R.color.payment_method_verify_icon_tint;
                break;
            case 1124965819:
                if (str.equals("SUSPENDED")) {
                    context = getContext();
                    i = R.string.card_suspended_alert_message;
                    setAlertMessageText(context.getString(i));
                    setAlertIcon(getResources().getDrawable(R.drawable.ic_fbpay_error));
                    context2 = getContext();
                    i2 = R.color.payment_method_remove_card_icon_tint;
                    break;
                }
                setAlertMessageText(getContext().getString(R.string.verify_payment_card_message));
                setAlertIcon(getResources().getDrawable(R.drawable.ic_settings_secure));
                context2 = getContext();
                i2 = R.color.payment_method_verify_icon_tint;
                break;
            default:
                setAlertMessageText(getContext().getString(R.string.verify_payment_card_message));
                setAlertIcon(getResources().getDrawable(R.drawable.ic_settings_secure));
                context2 = getContext();
                i2 = R.color.payment_method_verify_icon_tint;
                break;
        }
        setAlertIconTint(AnonymousClass00T.A00(context2, i2));
    }

    public void setTopDividerVisibility(int i) {
        this.A01.setVisibility(i);
    }
}
