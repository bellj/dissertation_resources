package X;

import android.content.Intent;
import android.view.View;
import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.36U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36U extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ ContactPickerFragment A00;

    public AnonymousClass36U(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AnonymousClass01H r0;
        ContactPickerFragment contactPickerFragment = this.A00;
        AnonymousClass01M.A00(contactPickerFragment.A01(), AnonymousClass01J.class);
        Map map = contactPickerFragment.A2a;
        Iterator A0o = C12960it.A0o(map);
        while (true) {
            if (!A0o.hasNext()) {
                break;
            } else if (C15380n4.A0N(C12970iu.A0a(A0o).A0D)) {
                if (contactPickerFragment.A1I.A03 && (r0 = contactPickerFragment.A1l) != null) {
                    r0.get();
                }
            }
        }
        if (contactPickerFragment.A2V) {
            contactPickerFragment.A1a(null);
        } else if (contactPickerFragment.A2N || contactPickerFragment.A2T) {
            Intent A0A = C12970iu.A0A();
            A0A.putStringArrayListExtra("jids", C15380n4.A06(map.keySet()));
            A0A.putExtra("file_path", contactPickerFragment.A1B().getString("file_path"));
            A0A.putExtra("all_contacts_count", contactPickerFragment.A22.size());
            if (contactPickerFragment.A2N) {
                contactPickerFragment.A1P.A0F(false, map.size());
            }
            AnonymousClass1IS A03 = C38211ni.A03(contactPickerFragment.A1B(), "");
            if (A03 != null) {
                C38211ni.A00(A0A, A03);
            }
            C32731ce r1 = contactPickerFragment.A1I;
            if (r1 != null) {
                A0A.putExtra("status_distribution", r1);
            }
            contactPickerFragment.A0m.A01(A0A);
            contactPickerFragment.A0m.A00();
        }
    }
}
