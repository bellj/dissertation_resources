package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1RD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1RD implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100454lw();
    public int A00 = 0;
    public int A01;
    public int A02;
    public long A03;
    public AnonymousClass1R1 A04;
    public AnonymousClass1RC A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public boolean A0I;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1RD(AnonymousClass1RC r2) {
        this.A05 = r2;
    }

    public AnonymousClass1RD(Parcel parcel) {
        boolean z = false;
        this.A05 = AnonymousClass1RC.values()[parcel.readInt()];
        this.A0A = parcel.readString();
        this.A0D = parcel.readString();
        this.A00 = parcel.readInt();
        this.A08 = parcel.readString();
        this.A01 = parcel.readInt();
        this.A0I = parcel.readByte() != 0 ? true : z;
        this.A0C = parcel.readString();
        this.A0E = parcel.readString();
        this.A0F = parcel.readString();
        this.A0G = parcel.readString();
        this.A03 = parcel.readLong();
        this.A0B = parcel.readString();
        this.A02 = parcel.readInt();
        this.A07 = parcel.readString();
        this.A09 = parcel.readString();
        this.A04 = (AnonymousClass1R1) parcel.readParcelable(AnonymousClass1R1.class.getClassLoader());
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A05.ordinal());
        parcel.writeString(this.A0A);
        parcel.writeString(this.A0D);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A08);
        parcel.writeInt(this.A01);
        parcel.writeByte(this.A0I ? (byte) 1 : 0);
        parcel.writeString(this.A0C);
        parcel.writeString(this.A0E);
        parcel.writeString(this.A0F);
        parcel.writeString(this.A0G);
        parcel.writeLong(this.A03);
        parcel.writeString(this.A0B);
        parcel.writeInt(this.A02);
        parcel.writeString(this.A07);
        parcel.writeString(this.A09);
        parcel.writeParcelable(this.A04, i);
    }
}
