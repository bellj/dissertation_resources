package X;

import java.util.Map;

/* renamed from: X.3nB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77373nB extends AnonymousClass495 {
    public final Map headerFields;
    public final byte[] responseBody;
    public final int responseCode;
    public final String responseMessage;

    public C77373nB(AnonymousClass3H3 r2, String str, Map map, byte[] bArr, int i) {
        super(r2, C12960it.A0W(i, "Response code: "));
        this.responseCode = i;
        this.responseMessage = str;
        this.headerFields = map;
        this.responseBody = bArr;
    }
}
