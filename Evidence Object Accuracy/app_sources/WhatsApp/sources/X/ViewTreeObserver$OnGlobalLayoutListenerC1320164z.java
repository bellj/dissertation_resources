package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.64z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC1320164z implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ C119205d8 A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC1320164z(C119205d8 r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C119205d8 r4 = this.A00;
        View view = (View) r4.A05;
        view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        if (!r4.isShowing()) {
            r4.showAtLocation(view, 48, 0, 1000000);
        }
    }
}
