package X;

import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;

/* renamed from: X.0Wu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07130Wu implements AdapterView.OnItemClickListener {
    public final /* synthetic */ AnonymousClass0OC A00;
    public final /* synthetic */ AnonymousClass0U5 A01;

    public C07130Wu(AnonymousClass0OC r1, AnonymousClass0U5 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        AnonymousClass0OC r3 = this.A00;
        DialogInterface.OnClickListener onClickListener = r3.A05;
        AnonymousClass04T r1 = this.A01.A0W;
        onClickListener.onClick(r1, i);
        if (!r3.A0L) {
            r1.dismiss();
        }
    }
}
