package X;

import android.content.Context;
import com.whatsapp.location.LocationPicker2;

/* renamed from: X.4qM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103194qM implements AbstractC009204q {
    public final /* synthetic */ LocationPicker2 A00;

    public C103194qM(LocationPicker2 locationPicker2) {
        this.A00 = locationPicker2;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
