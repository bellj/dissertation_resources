package X;

import android.content.Context;
import java.util.concurrent.Executor;

/* renamed from: X.00a  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00a {
    public static Executor A00(Context context) {
        return context.getMainExecutor();
    }
}
