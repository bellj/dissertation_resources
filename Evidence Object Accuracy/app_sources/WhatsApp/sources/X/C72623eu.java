package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: X.3eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72623eu extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AbstractC14670lq A01;

    public C72623eu(View view, AbstractC14670lq r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        View view = this.A00;
        view.setVisibility(8);
        view.setAlpha(1.0f);
    }
}
