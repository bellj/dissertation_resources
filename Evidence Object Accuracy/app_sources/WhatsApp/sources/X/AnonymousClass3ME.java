package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.mentions.MentionableEntry;

/* renamed from: X.3ME  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ME implements TextWatcher {
    public int A00;
    public boolean A01;
    public C73543gP[] A02;
    public final /* synthetic */ MentionableEntry A03;

    public AnonymousClass3ME(MentionableEntry mentionableEntry) {
        this.A03 = mentionableEntry;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        if (this.A00 > 0) {
            MentionableEntry mentionableEntry = this.A03;
            C73543gP[] r7 = this.A02;
            int i = 0;
            if (!this.A01) {
                C73543gP[] r3 = (C73543gP[]) mentionableEntry.getEditableText().getSpans(mentionableEntry.getSelectionStart(), mentionableEntry.getSelectionEnd(), C73543gP.class);
                mentionableEntry.A0C(mentionableEntry.A0C);
                mentionableEntry.A0C = null;
                int length = r3.length;
                while (i < length) {
                    C73543gP r1 = r3[i];
                    mentionableEntry.A0C(r1.A00);
                    mentionableEntry.A0C(r1);
                    i++;
                }
            } else {
                int length2 = r7.length;
                while (i < length2) {
                    C73543gP r32 = r7[i];
                    int spanStart = editable.getSpanStart(r32.A00);
                    int spanEnd = editable.getSpanEnd(r32);
                    if (!(spanStart == -1 || spanEnd == -1)) {
                        mentionableEntry.A0C(r32.A00);
                        mentionableEntry.A0C(r32);
                        editable.delete(spanStart, spanEnd);
                    }
                    i++;
                }
            }
        }
        this.A03.A09(editable);
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        MentionableEntry mentionableEntry = this.A03;
        int selectionEnd = mentionableEntry.getSelectionEnd();
        this.A02 = (C73543gP[]) mentionableEntry.getEditableText().getSpans(selectionEnd, selectionEnd, C73543gP.class);
        this.A01 = C12960it.A1V(mentionableEntry.getSelectionStart(), mentionableEntry.getSelectionEnd());
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.A00 = i2;
    }
}
