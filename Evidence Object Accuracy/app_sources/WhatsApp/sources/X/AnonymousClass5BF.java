package X;

import android.os.SharedMemory;
import android.system.ErrnoException;
import android.util.Log;
import java.io.Closeable;
import java.nio.ByteBuffer;

/* renamed from: X.5BF  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5BF implements Closeable, AnonymousClass5XR {
    public SharedMemory A00;
    public ByteBuffer A01;
    public final long A02;

    public AnonymousClass5BF() {
        this.A00 = null;
        this.A01 = null;
        this.A02 = (long) System.identityHashCode(this);
    }

    public AnonymousClass5BF(int i) {
        AnonymousClass0RA.A00(C12960it.A1U(i));
        try {
            SharedMemory create = SharedMemory.create("AshmemMemoryChunk", i);
            this.A00 = create;
            this.A01 = create.mapReadWrite();
            this.A02 = (long) System.identityHashCode(this);
        } catch (ErrnoException e) {
            throw new RuntimeException("Fail to create AshmemMemory", e);
        }
    }

    public final void A00(AnonymousClass5XR r4, int i) {
        if (r4 instanceof AnonymousClass5BF) {
            AnonymousClass0RA.A01(!isClosed());
            AnonymousClass0RA.A01(!r4.isClosed());
            C87794Cz.A00(0, r4.AGm(), 0, i, AGm());
            this.A01.position(0);
            r4.AB3().position(0);
            byte[] bArr = new byte[i];
            this.A01.get(bArr, 0, i);
            r4.AB3().put(bArr, 0, i);
            return;
        }
        throw C12970iu.A0f("Cannot copy two incompatible MemoryChunks");
    }

    @Override // X.AnonymousClass5XR
    public void A7m(AnonymousClass5XR r9, int i, int i2, int i3) {
        long AHP = r9.AHP();
        long j = this.A02;
        if (AHP == j) {
            StringBuilder A0k = C12960it.A0k("Copying from AshmemMemoryChunk ");
            A0k.append(Long.toHexString(j));
            A0k.append(" to AshmemMemoryChunk ");
            A0k.append(Long.toHexString(AHP));
            Log.w("AshmemMemoryChunk", C12960it.A0d(" which are the same ", A0k));
            AnonymousClass0RA.A00(false);
        }
        if (AHP < j) {
            synchronized (r9) {
                synchronized (this) {
                    A00(r9, i3);
                }
            }
            return;
        }
        synchronized (this) {
            synchronized (r9) {
                A00(r9, i3);
            }
        }
    }

    @Override // X.AnonymousClass5XR
    public ByteBuffer AB3() {
        return this.A01;
    }

    @Override // X.AnonymousClass5XR
    public int AGm() {
        AnonymousClass0RA.A01(!isClosed());
        return this.A00.getSize();
    }

    @Override // X.AnonymousClass5XR
    public long AHP() {
        return this.A02;
    }

    @Override // X.AnonymousClass5XR
    public synchronized byte AZm(int i) {
        boolean z = true;
        AnonymousClass0RA.A01(C12960it.A1T(isClosed() ? 1 : 0));
        AnonymousClass0RA.A00(C12990iw.A1W(i));
        if (i >= AGm()) {
            z = false;
        }
        AnonymousClass0RA.A00(z);
        return this.A01.get(i);
    }

    @Override // X.AnonymousClass5XR
    public synchronized int AZr(byte[] bArr, int i, int i2, int i3) {
        int min;
        AnonymousClass0RA.A01(C12960it.A1T(isClosed() ? 1 : 0));
        min = Math.min(Math.max(0, AGm() - i), i3);
        C87794Cz.A00(i, bArr.length, i2, min, AGm());
        this.A01.position(i);
        this.A01.get(bArr, i2, min);
        return min;
    }

    @Override // X.AnonymousClass5XR
    public synchronized int AgC(byte[] bArr, int i, int i2, int i3) {
        int min;
        AnonymousClass0RA.A01(C12960it.A1T(isClosed() ? 1 : 0));
        min = Math.min(Math.max(0, AGm() - i), i3);
        C87794Cz.A00(i, bArr.length, i2, min, AGm());
        this.A01.position(i);
        this.A01.put(bArr, i2, min);
        return min;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable, X.AnonymousClass5XR
    public synchronized void close() {
        if (!isClosed()) {
            SharedMemory.unmap(this.A01);
            this.A00.close();
            this.A01 = null;
            this.A00 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0008, code lost:
        if (r2.A00 == null) goto L_0x000a;
     */
    @Override // X.AnonymousClass5XR
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean isClosed() {
        /*
            r2 = this;
            monitor-enter(r2)
            java.nio.ByteBuffer r0 = r2.A01     // Catch: all -> 0x000d
            if (r0 == 0) goto L_0x000a
            android.os.SharedMemory r1 = r2.A00     // Catch: all -> 0x000d
            r0 = 0
            if (r1 != 0) goto L_0x000b
        L_0x000a:
            r0 = 1
        L_0x000b:
            monitor-exit(r2)
            return r0
        L_0x000d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5BF.isClosed():boolean");
    }
}
