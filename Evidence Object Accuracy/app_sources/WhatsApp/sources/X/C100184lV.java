package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100184lV implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1ZD(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1ZD[i];
    }
}
