package X;

import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPreviewActivity;

/* renamed from: X.3S5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S5 implements AnonymousClass070 {
    public final /* synthetic */ DownloadableWallpaperPreviewActivity A00;

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public AnonymousClass3S5(DownloadableWallpaperPreviewActivity downloadableWallpaperPreviewActivity) {
        this.A00 = downloadableWallpaperPreviewActivity;
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        DownloadableWallpaperPreviewActivity downloadableWallpaperPreviewActivity = this.A00;
        ((AnonymousClass35C) downloadableWallpaperPreviewActivity).A00.setEnabled(downloadableWallpaperPreviewActivity.A08.contains(Integer.valueOf(i)));
    }
}
