package X;

/* renamed from: X.68M  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68M implements AbstractC116695Wl {
    public final /* synthetic */ C91064Qh A00;
    public final /* synthetic */ AnonymousClass5WN A01;
    public final /* synthetic */ C124395pK A02;
    public final /* synthetic */ Runnable A03;

    public AnonymousClass68M(C91064Qh r1, AnonymousClass5WN r2, C124395pK r3, Runnable runnable) {
        this.A02 = r3;
        this.A03 = runnable;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC116695Wl
    public void AOy() {
        this.A01.AVH(this.A00);
    }

    @Override // X.AbstractC116695Wl
    public void APp(Exception exc) {
        this.A01.AVH(this.A00);
    }

    @Override // X.AbstractC116695Wl
    public void AWx(C64063Ec r2) {
        this.A03.run();
    }
}
