package X;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4;
import com.facebook.redex.ViewOnClickCListenerShape11S0100000_I1_5;
import com.facebook.redex.ViewOnClickCListenerShape2S0200000_I1;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.facebook.redex.ViewOnClickCListenerShape6S0100000_I1;
import com.facebook.redex.ViewOnClickCListenerShape7S0100000_I1_1;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.facebook.redex.ViewOnClickCListenerShape9S0100000_I1_3;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.base.WaFragment;
import com.whatsapp.calling.callgrid.view.VoiceParticipantAudioWave;
import com.whatsapp.components.CircularRevealView;
import com.whatsapp.util.Log;
import java.net.URI;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* renamed from: X.0it  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C12960it {
    public static float A00(ValueAnimator valueAnimator) {
        return ((Number) valueAnimator.getAnimatedValue()).floatValue();
    }

    public static float A01(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static int A02(int i) {
        if (i != 0) {
            return 0;
        }
        return 8;
    }

    public static int A03(View view) {
        return (view.getHeight() - view.getPaddingTop()) - view.getPaddingBottom();
    }

    public static int A04(View view, int i) {
        return (i - view.getPaddingLeft()) - view.getPaddingRight();
    }

    public static int A05(Object obj) {
        return ((Number) obj).intValue();
    }

    public static int A06(Object obj, Object[] objArr) {
        objArr[1] = obj;
        return Arrays.hashCode(objArr);
    }

    public static int A07(int[][] iArr, int i, int i2, int i3) {
        int[] iArr2 = iArr[i];
        int i4 = iArr2[i2];
        int i5 = ((i4 << i3) | (i4 >>> 8)) ^ i4;
        int i6 = i4 ^ ((((-2139062144 & i5) >>> 7) * 27) ^ ((2139062143 & i5) << 1));
        int i7 = i6 & -1061109568;
        int i8 = i7 ^ (i7 >>> 1);
        int i9 = i5 ^ ((i8 >>> 5) ^ (((1061109567 & i6) << 2) ^ (i8 >>> 2)));
        iArr2[i2] = i6 ^ (i9 ^ ((i9 << (-16)) | (i9 >>> 16)));
        return i2 + 1;
    }

    public static SharedPreferences.Editor A08(C14820m6 r0) {
        return r0.A00.edit();
    }

    public static Resources A09(View view) {
        return view.getContext().getResources();
    }

    public static Paint A0A() {
        return new Paint(1);
    }

    public static RectF A0B(CircularRevealView circularRevealView) {
        float sqrt = (float) (Math.sqrt((double) ((circularRevealView.getWidth() * circularRevealView.getWidth()) + (circularRevealView.getHeight() * circularRevealView.getHeight()))) * ((double) circularRevealView.A00));
        RectF rectF = circularRevealView.A08;
        float f = -sqrt;
        rectF.set(f, f, sqrt, sqrt);
        rectF.offset((float) circularRevealView.A01, (float) circularRevealView.A02);
        return rectF;
    }

    public static Uri A0C(URI uri) {
        return new Uri.Builder().scheme(uri.getScheme()).encodedAuthority(uri.getRawAuthority()).encodedPath(uri.getRawPath()).encodedQuery(uri.getRawQuery()).encodedFragment(uri.getRawFragment()).build();
    }

    public static Pair A0D(Object obj, int i) {
        return new Pair(obj, Integer.valueOf(i));
    }

    public static LayoutInflater A0E(View view) {
        return LayoutInflater.from(view.getContext());
    }

    public static View A0F(LayoutInflater layoutInflater, ViewGroup viewGroup, int i) {
        return layoutInflater.inflate(i, viewGroup, false);
    }

    public static ScaleAnimation A0G(float f, float f2) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(f, f2, f, f2, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(125);
        scaleAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleAnimation.setFillBefore(true);
        return scaleAnimation;
    }

    public static TranslateAnimation A0H(int i) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) i, 0.0f);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation.setDuration(120);
        return translateAnimation;
    }

    public static TextView A0I(View view, int i) {
        return (TextView) AnonymousClass028.A0D(view, i);
    }

    public static TextView A0J(View view, int i) {
        return (TextView) view.findViewById(i);
    }

    public static C004802e A0K(AnonymousClass01E r1) {
        return new C004802e(r1.A01());
    }

    public static AnonymousClass04S A0L(DialogInterface.OnClickListener onClickListener, C004802e r2, int i) {
        r2.setPositiveButton(i, onClickListener);
        r2.setNegativeButton(R.string.cancel, null);
        return r2.create();
    }

    public static AnonymousClass015 A0M(AnonymousClass01E r1) {
        return new AnonymousClass02A(r1.A0C()).A00(EncBackupViewModel.class);
    }

    public static WaTextView A0N(View view, int i) {
        return (WaTextView) AnonymousClass028.A0D(view, i);
    }

    public static C15550nR A0O(AnonymousClass01J r0) {
        return (C15550nR) r0.A45.get();
    }

    public static C15610nY A0P(AnonymousClass01J r0) {
        return (C15610nY) r0.AMe.get();
    }

    public static AnonymousClass01d A0Q(AnonymousClass01J r0) {
        return (AnonymousClass01d) r0.ALI.get();
    }

    public static AnonymousClass018 A0R(AnonymousClass01J r0) {
        return (AnonymousClass018) r0.ANb.get();
    }

    public static C14850m9 A0S(AnonymousClass01J r0) {
        return (C14850m9) r0.A04.get();
    }

    public static AbstractC14440lR A0T(AnonymousClass01J r0) {
        return (AbstractC14440lR) r0.ANe.get();
    }

    public static IllegalStateException A0U(String str) {
        return new IllegalStateException(str);
    }

    public static Integer A0V() {
        return 1;
    }

    public static String A0W(int i, String str) {
        StringBuilder sb = new StringBuilder(str);
        sb.append(i);
        return sb.toString();
    }

    public static String A0X(Context context, Object obj, Object[] objArr, int i, int i2) {
        objArr[i] = obj;
        return context.getString(i2, objArr);
    }

    public static String A0Y(Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    public static String A0Z(Object obj, String str, StringBuilder sb) {
        sb.append(str);
        sb.append(obj);
        return sb.toString();
    }

    public static String A0a(Object obj, StringBuilder sb) {
        sb.append(obj);
        sb.append(')');
        return sb.toString();
    }

    public static String A0b(String str, Object obj) {
        StringBuilder sb = new StringBuilder(str);
        sb.append(obj);
        return sb.toString();
    }

    public static String A0c(String str, String str2) {
        if (str.length() != 0) {
            return str2.concat(str);
        }
        return new String(str2);
    }

    public static String A0d(String str, StringBuilder sb) {
        sb.append(str);
        return sb.toString();
    }

    public static String A0e(String str, StringBuilder sb, int i) {
        sb.append(str);
        sb.append(i);
        return sb.toString();
    }

    public static String A0f(StringBuilder sb, int i) {
        sb.append(i);
        return sb.toString();
    }

    public static String A0g(List list, int i) {
        return (String) list.get(i);
    }

    public static StringBuilder A0h() {
        return new StringBuilder();
    }

    public static StringBuilder A0i(Object obj, int i) {
        return new StringBuilder(i + String.valueOf(obj).length());
    }

    public static StringBuilder A0j(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        return sb;
    }

    public static StringBuilder A0k(String str) {
        return new StringBuilder(str);
    }

    public static ArrayList A0l() {
        return new ArrayList();
    }

    public static ArrayList A0m(Object obj, Object[] objArr, int i) {
        objArr[i] = obj;
        return new ArrayList(Arrays.asList(objArr));
    }

    public static Iterator A0n(Map map) {
        return map.entrySet().iterator();
    }

    public static Iterator A0o(Map map) {
        return map.values().iterator();
    }

    public static Random A0p(VoiceParticipantAudioWave voiceParticipantAudioWave) {
        voiceParticipantAudioWave.A00();
        voiceParticipantAudioWave.A0E = new double[0];
        voiceParticipantAudioWave.A0G = new double[0];
        voiceParticipantAudioWave.A0F = new double[0];
        voiceParticipantAudioWave.A08 = new Paint(1);
        return new Random();
    }

    public static void A0q(Activity activity, Intent intent) {
        activity.setResult(-1, intent);
        activity.finish();
    }

    public static void A0r(Context context, View view, int i) {
        view.setContentDescription(context.getString(i));
    }

    public static void A0s(Context context, TextView textView, int i) {
        textView.setTextColor(AnonymousClass00T.A00(context, i));
    }

    public static void A0t(SharedPreferences.Editor editor, String str, boolean z) {
        editor.putBoolean(str, z).apply();
    }

    public static void A0u(SharedPreferences sharedPreferences, String str, int i) {
        sharedPreferences.edit().putInt(str, i).apply();
    }

    public static void A0v(View view) {
        view.dispatchKeyEvent(new KeyEvent(0, 67));
    }

    public static void A0w(View view, AnonymousClass018 r5, long j) {
        C34271fr.A00(view, r5.A0I(new Object[]{Long.valueOf(j)}, R.plurals.quantity_selector_max_reached, j), -1).A03();
    }

    public static void A0x(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape9S0100000_I1_3(obj, i));
    }

    public static void A0y(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape8S0100000_I1_2(obj, i));
    }

    public static void A0z(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape7S0100000_I1_1(obj, i));
    }

    public static void A10(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape6S0100000_I1(obj, i));
    }

    public static void A11(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape11S0100000_I1_5(obj, i));
    }

    public static void A12(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape10S0100000_I1_4(obj, i));
    }

    public static void A13(View view, Object obj, Object obj2, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape2S0200000_I1(obj, i, obj2));
    }

    public static void A14(View view, Object obj, Object obj2, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape3S0200000_I1_1(obj, i, obj2));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: android.graphics.drawable.Drawable[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static void A15(ImageView imageView, Object obj, Drawable[] drawableArr) {
        drawableArr[1] = obj;
        TransitionDrawable transitionDrawable = new TransitionDrawable(drawableArr);
        transitionDrawable.setCrossFadeEnabled(true);
        transitionDrawable.startTransition(150);
        imageView.setImageDrawable(transitionDrawable);
    }

    public static void A16(DialogFragment dialogFragment, ActivityC000900k r3) {
        dialogFragment.A1F(r3.A0V(), null);
    }

    public static void A17(AbstractC001200n r1, AnonymousClass017 r2, int i) {
        r2.A05(r1, new IDxObserverShape4S0100000_2_I1(r1, i));
    }

    public static void A18(AbstractC001200n r1, AnonymousClass017 r2, int i) {
        r2.A05(r1, new IDxObserverShape3S0100000_1_I1(r1, i));
    }

    public static void A19(AbstractC001200n r1, AnonymousClass017 r2, Object obj, int i) {
        r2.A05(r1, new IDxObserverShape3S0100000_1_I1(obj, i));
    }

    public static void A1A(AnonymousClass017 r1, int i) {
        r1.A0B(Integer.valueOf(i));
    }

    public static void A1B(AnonymousClass01J r1, WaFragment waFragment) {
        waFragment.A00 = (AnonymousClass182) r1.A94.get();
        waFragment.A01 = (AnonymousClass180) r1.ALt.get();
    }

    public static void A1C(CircularRevealView circularRevealView) {
        circularRevealView.A0E = true;
        circularRevealView.A04 = 300;
        circularRevealView.A03 = -1;
        circularRevealView.A06 = new Paint(1);
        circularRevealView.A07 = new Path();
        circularRevealView.A08 = new RectF();
        circularRevealView.A09 = new animation.Animation$AnimationListenerC102154og(circularRevealView);
        circularRevealView.A05 = new IDxLAdapterShape1S0100000_2_I1(circularRevealView, 9);
    }

    public static void A1D(C28231Mf r2, AnonymousClass1Mg r3, int i) {
        if (i != 0) {
            C17070qD r0 = r2.A0K;
            r0.A03();
            r3.A04 = r0.A0F.A00(r3.A0K, r3.A0L);
        }
    }

    public static void A1E(AbstractC16350or r1, AbstractC14440lR r2) {
        r2.Aaz(r1, new Void[0]);
    }

    public static void A1F(Object obj) {
        Log.i(obj.toString());
    }

    public static void A1G(Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        objArr[20] = obj;
        objArr[21] = obj2;
        objArr[22] = obj3;
        objArr[23] = obj4;
    }

    public static void A1H(Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        objArr[25] = obj;
        objArr[26] = obj2;
        objArr[27] = obj3;
        objArr[28] = obj4;
    }

    public static void A1I(Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        objArr[32] = obj;
        objArr[33] = obj2;
        objArr[34] = obj3;
        objArr[35] = obj4;
    }

    public static void A1J(Object obj, AbstractMap abstractMap, int i) {
        abstractMap.put(Integer.valueOf(i), obj);
    }

    public static void A1K(Object obj, AbstractMap abstractMap, int i) {
        abstractMap.put(obj, Integer.valueOf(i));
    }

    public static void A1L(String str, String str2, String str3, StringBuilder sb) {
        sb.append(str);
        sb.append(str2);
        sb.append(str3);
    }

    public static void A1M(String str, String str2, Object[] objArr, int i) {
        objArr[i] = new AnonymousClass1W9(str, str2);
    }

    public static void A1N(StringBuilder sb, String str, Object obj) {
        sb.append(str);
        sb.append(obj.hashCode());
        sb.append(" for ");
    }

    public static void A1O(Object[] objArr, int i) {
        objArr[0] = Integer.valueOf(i);
    }

    public static void A1P(Object[] objArr, int i, int i2) {
        objArr[i2] = Integer.valueOf(i);
    }

    public static void A1Q(Object[] objArr, Object obj) {
        objArr[20] = obj;
        int[] iArr = new int[1];
        iArr[0] = 128158;
        objArr[21] = iArr;
        int[] iArr2 = new int[1];
        iArr2[0] = 128147;
        objArr[22] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 128151;
        objArr[23] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 128150;
        objArr[24] = iArr4;
        int[] iArr5 = new int[1];
        iArr5[0] = 128152;
        objArr[25] = iArr5;
        int[] iArr6 = new int[1];
        iArr6[0] = 128157;
        objArr[26] = iArr6;
        int[] iArr7 = new int[1];
        iArr7[0] = 128159;
        objArr[27] = iArr7;
        int[] iArr8 = new int[1];
        iArr8[0] = 9829;
        objArr[28] = iArr8;
        int[] iArr9 = new int[1];
        iArr9[0] = 128140;
        objArr[29] = iArr9;
        int[] iArr10 = new int[1];
        iArr10[0] = 128139;
        objArr[30] = iArr10;
    }

    public static boolean A1R(int i) {
        return (i & 1) == 1;
    }

    public static boolean A1S(int i) {
        if (i != 0) {
            return true;
        }
        return false;
    }

    public static boolean A1T(int i) {
        return i == 0;
    }

    public static boolean A1U(int i) {
        return i > 0;
    }

    public static boolean A1V(int i, int i2) {
        return i == i2;
    }

    public static boolean A1W(Object obj) {
        return obj != null;
    }

    public static boolean A1X(Object obj, Object obj2) {
        return obj != obj2;
    }

    public static int[] A1Y(Object[] objArr) {
        int[] iArr = new int[1];
        iArr[0] = 128512;
        objArr[0] = iArr;
        int[] iArr2 = new int[1];
        iArr2[0] = 128515;
        objArr[1] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 128516;
        objArr[2] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 128513;
        objArr[3] = iArr4;
        return new int[]{128518};
    }

    public static int[] A1Z(Object[] objArr) {
        int[] iArr = new int[1];
        iArr[0] = 127881;
        objArr[0] = iArr;
        int[] iArr2 = new int[1];
        iArr2[0] = 127882;
        objArr[1] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 127873;
        objArr[2] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 127880;
        objArr[3] = iArr4;
        return new int[]{128111};
    }

    public static int[] A1a(Object[] objArr) {
        int[] iArr = new int[1];
        iArr[0] = 128559;
        objArr[0] = iArr;
        int[] iArr2 = new int[1];
        iArr2[0] = 128550;
        objArr[1] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 128551;
        objArr[2] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 128558;
        objArr[3] = iArr4;
        return new int[]{128562};
    }

    public static int[][] A1b() {
        int[][] iArr = new int[21];
        int[] iArr2 = new int[1];
        iArr2[0] = 9785;
        iArr[0] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 128547;
        iArr[1] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 128534;
        iArr[2] = iArr4;
        int[] iArr5 = new int[1];
        iArr5[0] = 128555;
        iArr[3] = iArr5;
        int[] iArr6 = new int[1];
        iArr6[0] = 128553;
        iArr[4] = iArr6;
        return iArr;
    }
}
