package X;

/* renamed from: X.1ug  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42111ug {
    public final Integer A00;
    public final Long A01;
    public final Long A02;
    public final boolean A03;

    public C42111ug(Integer num, Long l, Long l2, boolean z) {
        this.A03 = z;
        this.A02 = l;
        this.A01 = l2;
        this.A00 = num;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[success=");
        sb.append(this.A03);
        StringBuilder sb2 = new StringBuilder(sb.toString());
        Long l = this.A02;
        if (l != null) {
            sb2.append(" refresh=");
            sb2.append(l);
        }
        Long l2 = this.A01;
        if (l2 != null) {
            sb2.append(" backoff=");
            sb2.append(l2);
        }
        Integer num = this.A00;
        if (num != null) {
            sb2.append(" errorCode=");
            sb2.append(num);
        }
        sb2.append("]");
        return sb2.toString();
    }
}
