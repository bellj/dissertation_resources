package X;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.0mf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15160mf {
    public static final Handler A08 = new Handler(Looper.getMainLooper(), new AnonymousClass3LZ());
    public static final boolean A09;
    public static final int[] A0A = {R.attr.snackbarStyle};
    public int A00;
    public List A01;
    public final Context A02;
    public final ViewGroup A03;
    public final AccessibilityManager A04;
    public final C34291fu A05;
    public final AnonymousClass5R7 A06;
    public final AnonymousClass5R8 A07 = new AnonymousClass51C(this);

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT <= 19) {
            z = true;
        }
        A09 = z;
    }

    public AbstractC15160mf(View view, ViewGroup viewGroup, AnonymousClass5R7 r9) {
        if (view == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null content");
        } else if (r9 != null) {
            this.A03 = viewGroup;
            this.A06 = r9;
            Context context = viewGroup.getContext();
            this.A02 = context;
            C50582Qc.A03(context, "Theme.AppCompat", C50582Qc.A00);
            LayoutInflater from = LayoutInflater.from(context);
            TypedArray obtainStyledAttributes = this.A02.obtainStyledAttributes(A0A);
            int resourceId = obtainStyledAttributes.getResourceId(0, -1);
            obtainStyledAttributes.recycle();
            C34291fu r1 = (C34291fu) from.inflate(resourceId != -1 ? R.layout.mtrl_layout_snackbar : R.layout.design_layout_snackbar, viewGroup, false);
            this.A05 = r1;
            r1.addView(view);
            AnonymousClass028.A0Z(r1, 1);
            AnonymousClass028.A0a(r1, 1);
            r1.setFitsSystemWindows(true);
            AnonymousClass028.A0h(r1, new C104084rn(this));
            AnonymousClass028.A0g(r1, new C74413hw(this));
            this.A04 = (AccessibilityManager) context.getSystemService("accessibility");
        } else {
            throw new IllegalArgumentException("Transient bottom bar must have non-null callback");
        }
    }

    public void A00() {
        C34291fu r2 = this.A05;
        int height = r2.getHeight();
        ViewGroup.LayoutParams layoutParams = r2.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            height += ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
        }
        if (A09) {
            AnonymousClass028.A0Y(r2, height);
        } else {
            r2.setTranslationY((float) height);
        }
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(height, 0);
        valueAnimator.setInterpolator(C50732Qs.A02);
        valueAnimator.setDuration(250L);
        valueAnimator.addListener(new AnonymousClass2YL(this));
        valueAnimator.addUpdateListener(new C65423Jn(this, height));
        valueAnimator.start();
    }

    public void A01() {
        C64883Hh A00 = C64883Hh.A00();
        AnonymousClass5R8 r0 = this.A07;
        synchronized (A00.A03) {
            if (A00.A05(r0)) {
                A00.A00 = null;
                if (A00.A01 != null) {
                    A00.A01();
                }
            }
        }
        List list = this.A01;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                ((AnonymousClass4UR) this.A01.get(size)).A00();
            }
        }
        C34291fu r2 = this.A05;
        ViewParent parent = r2.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(r2);
        }
    }

    public void A02() {
        C64883Hh A00 = C64883Hh.A00();
        AnonymousClass5R8 r0 = this.A07;
        synchronized (A00.A03) {
            if (A00.A05(r0)) {
                A00.A04(A00.A00);
            }
        }
        List list = this.A01;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size >= 0) {
                    this.A01.get(size);
                } else {
                    return;
                }
            }
        }
    }

    public void A03() {
        int i;
        C64883Hh A00 = C64883Hh.A00();
        C34271fr r1 = (C34271fr) this;
        if (!r1.A00 || !r1.A01.isTouchExplorationEnabled()) {
            i = ((AbstractC15160mf) r1).A00;
        } else {
            i = -2;
        }
        AnonymousClass5R8 r3 = this.A07;
        synchronized (A00.A03) {
            if (A00.A05(r3)) {
                AnonymousClass4PT r12 = A00.A00;
                r12.A00 = i;
                A00.A02.removeCallbacksAndMessages(r12);
                A00.A04(A00.A00);
            } else {
                AnonymousClass4PT r13 = A00.A01;
                if (r13 == null || r3 == null || r13.A02.get() != r3) {
                    A00.A01 = new AnonymousClass4PT(r3, i);
                } else {
                    r13.A00 = i;
                }
                AnonymousClass4PT r14 = A00.A00;
                if (r14 == null || !A00.A06(r14, 4)) {
                    A00.A00 = null;
                    A00.A01();
                }
            }
        }
    }

    public void A04(int i) {
        C64883Hh A00 = C64883Hh.A00();
        AnonymousClass5R8 r3 = this.A07;
        synchronized (A00.A03) {
            if (A00.A05(r3)) {
                A00.A06(A00.A00, i);
            } else {
                AnonymousClass4PT r1 = A00.A01;
                if (!(r1 == null || r3 == null || r1.A02.get() != r3)) {
                    A00.A06(r1, i);
                }
            }
        }
    }

    public boolean A05() {
        boolean A05;
        C64883Hh A00 = C64883Hh.A00();
        AnonymousClass5R8 r0 = this.A07;
        synchronized (A00.A03) {
            A05 = A00.A05(r0);
        }
        return A05;
    }
}
