package X;

import java.util.Arrays;

/* renamed from: X.4Wl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92574Wl {
    public final C83143wl A00;
    public final boolean A01;

    public C92574Wl(C83143wl r6, boolean z) {
        if ((r6.A01 instanceof C83133wk) && (r6.A00() instanceof C83163wn)) {
            AnonymousClass4YR r4 = r6;
            AnonymousClass4YR r1 = null;
            while (true) {
                r4 = r4.A00();
                if (r4 instanceof C83133wk) {
                    break;
                }
                r1 = r4;
            }
            r1.A01 = null;
            r6.A01 = r1;
            C95264dP r3 = new C95264dP();
            r3.A02 = new C92574Wl(r6, true);
            r3.A00 = EnumC870149w.PATH;
            ((C83133wk) r4).A00 = Arrays.asList(r3);
            r6 = new C83143wl('$');
            r6.A01 = r4;
            ((AnonymousClass4YR) r6).A01 = r4;
        }
        this.A00 = r6;
        this.A01 = z;
    }

    public C94394bk A00(C92364Vp r5, Object obj, Object obj2) {
        C94394bk r3 = new C94394bk(r5, this, obj2, false);
        try {
            this.A00.A02(AbstractC111885Be.A01, r3, obj, "");
        } catch (AnonymousClass5H1 unused) {
        }
        return r3;
    }

    public String toString() {
        return this.A00.toString();
    }
}
