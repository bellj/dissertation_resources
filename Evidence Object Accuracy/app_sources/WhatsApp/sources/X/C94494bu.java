package X;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

/* renamed from: X.4bu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94494bu {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public Surface A0B;
    public boolean A0C;
    public final WindowManager A0D;
    public final C92394Vs A0E = new C92394Vs();
    public final C97954hu A0F;
    public final Choreographer$FrameCallbackC98294iS A0G;

    public C94494bu(Context context) {
        C97954hu r3 = null;
        if (context != null) {
            Context applicationContext = context.getApplicationContext();
            WindowManager windowManager = (WindowManager) applicationContext.getSystemService("window");
            this.A0D = windowManager;
            if (windowManager != null) {
                this.A0F = AnonymousClass3JZ.A01 >= 17 ? A01(applicationContext) : r3;
                this.A0G = Choreographer$FrameCallbackC98294iS.A05;
            }
        }
        this.A09 = -9223372036854775807L;
        this.A0A = -9223372036854775807L;
        this.A00 = -1.0f;
        this.A01 = 1.0f;
    }

    public static void A00(Surface surface, float f) {
        int i = 1;
        if (f == 0.0f) {
            i = 0;
        }
        try {
            surface.setFrameRate(f, i);
        } catch (IllegalStateException e) {
            C64923Hl.A01("VideoFrameReleaseHelper", "Failed to call Surface.setFrameRate", e);
        }
    }

    public final C97954hu A01(Context context) {
        DisplayManager displayManager = (DisplayManager) context.getSystemService("display");
        if (displayManager == null) {
            return null;
        }
        return new C97954hu(displayManager, this);
    }

    public void A02() {
        if (this.A0D != null) {
            C97954hu r0 = this.A0F;
            if (r0 != null) {
                r0.A01();
            }
            this.A0G.A02.sendEmptyMessage(2);
        }
    }

    public void A03() {
        if (this.A0D != null) {
            this.A0G.A02.sendEmptyMessage(1);
            C97954hu r0 = this.A0F;
            if (r0 != null) {
                r0.A00();
            }
            A04();
        }
    }

    public final void A04() {
        long j;
        Display defaultDisplay = this.A0D.getDefaultDisplay();
        if (defaultDisplay != null) {
            long refreshRate = (long) (1.0E9d / ((double) defaultDisplay.getRefreshRate()));
            this.A09 = refreshRate;
            j = (refreshRate * 80) / 100;
        } else {
            Log.w("VideoFrameReleaseHelper", "Unable to query display refresh rate");
            j = -9223372036854775807L;
            this.A09 = -9223372036854775807L;
        }
        this.A0A = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        if (r9.A06 < 5000000000L) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (r9.A00 != 0) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05() {
        /*
            r12 = this;
            int r0 = X.AnonymousClass3JZ.A01
            r7 = 30
            if (r0 < r7) goto L_0x0063
            android.view.Surface r0 = r12.A0B
            if (r0 == 0) goto L_0x0063
            X.4Vs r8 = r12.A0E
            X.4XQ r9 = r8.A03
            long r1 = r9.A03
            r3 = 15
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x001b
            int r0 = r9.A00
            r11 = 1
            if (r0 == 0) goto L_0x001c
        L_0x001b:
            r11 = 0
        L_0x001c:
            if (r11 == 0) goto L_0x006d
            r4 = 4741671816366391296(0x41cdcd6500000000, double:1.0E9)
            long r2 = r9.A05
            r0 = 0
            int r6 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r6 == 0) goto L_0x002e
            long r0 = r9.A06
            long r0 = r0 / r2
        L_0x002e:
            double r2 = (double) r0
            double r4 = r4 / r2
            float r6 = (float) r4
        L_0x0031:
            float r10 = r12.A02
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 == 0) goto L_0x0063
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r5 = 0
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0064
            int r0 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0064
            if (r11 == 0) goto L_0x0052
            long r3 = r9.A06
            r1 = 5000000000(0x12a05f200, double:2.470328229E-314)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r1 = 1017370378(0x3ca3d70a, float:0.02)
            if (r0 >= 0) goto L_0x0054
        L_0x0052:
            r1 = 1065353216(0x3f800000, float:1.0)
        L_0x0054:
            float r0 = r6 - r10
            float r0 = java.lang.Math.abs(r0)
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0063
        L_0x005e:
            r12.A02 = r6
            r12.A06(r5)
        L_0x0063:
            return
        L_0x0064:
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x005e
            int r0 = r8.A00
            if (r0 < r7) goto L_0x0063
            goto L_0x005e
        L_0x006d:
            float r6 = r12.A00
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94494bu.A05():void");
    }

    public final void A06(boolean z) {
        Surface surface;
        if (AnonymousClass3JZ.A01 >= 30 && (surface = this.A0B) != null) {
            float f = 0.0f;
            if (this.A0C) {
                float f2 = this.A02;
                if (f2 != -1.0f) {
                    f = this.A01 * f2;
                }
            }
            if (z || this.A03 != f) {
                this.A03 = f;
                A00(surface, f);
            }
        }
    }
}
