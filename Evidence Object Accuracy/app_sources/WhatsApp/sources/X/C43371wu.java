package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.util.Log;

/* renamed from: X.1wu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43371wu {
    public int A00;
    public C43631xL A01;
    public C43621xK A02;
    public AnonymousClass1VL A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final Handler A08;
    public final C15450nH A09;
    public final C16240og A0A;
    public final C18230s7 A0B;
    public final AnonymousClass01d A0C;
    public final C16590pI A0D;
    public volatile long A0E;

    public C43371wu(Looper looper, C15450nH r3, C16240og r4, C18230s7 r5, AnonymousClass01d r6, C16590pI r7) {
        this.A0B = r5;
        this.A0D = r7;
        this.A09 = r3;
        this.A0C = r6;
        this.A08 = new Handler(looper);
        this.A0A = r4;
    }

    public void A00() {
        Log.i("xmpp/client-ping/on-demand-ping");
        this.A08.post(new RunnableBRunnable0Shape8S0100000_I0_8(this, 22));
    }

    public final void A01() {
        Log.i("xmpp/client-ping/timeout/cancel-alarm");
        A07(new Intent("com.whatsapp.alarm.CLIENT_PING_TIMEOUT").setPackage("com.whatsapp"));
    }

    public final void A02() {
        Log.i("xmpp/client-ping/on-disconnected");
        Handler handler = this.A08;
        AnonymousClass009.A02(handler);
        A03();
        if (!this.A05) {
            Log.w("xmpp/client-ping/on-disconnected; not connected, ignoring...");
            return;
        }
        if (this.A0E > 0) {
            A01();
        }
        AnonymousClass009.A02(handler);
        C43621xK r1 = this.A02;
        if (r1 != null) {
            this.A0D.A00.unregisterReceiver(r1);
            this.A02 = null;
        }
        Log.i("xmpp/client-ping/periodic/cancel-alarm");
        A07(new Intent("com.whatsapp.alarm.CLIENT_PING_PERIODIC").setPackage("com.whatsapp"));
        AnonymousClass009.A02(handler);
        C43631xL r12 = this.A01;
        if (r12 != null) {
            this.A0D.A00.unregisterReceiver(r12);
            this.A01 = null;
        }
        this.A03 = null;
        this.A05 = false;
    }

    public final void A03() {
        AnonymousClass009.A02(this.A08);
        if (!this.A06) {
            A07(new Intent("com.whatsapp.MessageHandler.CLIENT_PINGER_ACTION"));
            A07(new Intent("com.whatsapp.MessageHandler.CLIENT_PINGER_ACTION").setPackage("com.whatsapp"));
            this.A06 = true;
        }
    }

    public final void A04() {
        String str;
        Log.i("xmpp/client-ping/ping-timeout");
        AnonymousClass009.A02(this.A08);
        if (!this.A05 || this.A03 == null) {
            str = "xmpp/client-ping/ping-timeout; not connected, ignoring.";
        } else if (!this.A0A.A06) {
            str = "xmpp/client-ping/ping-timeout; xmpp connection is not ready, ignoring.";
        } else if (this.A07) {
            str = "xmpp/client-ping/ping-timeout; already notified about timeout, ignoring.";
        } else {
            ((Handler) this.A03).obtainMessage(8).sendToTarget();
            this.A07 = true;
            A01();
            return;
        }
        Log.w(str);
    }

    public final void A05() {
        Log.i("xmpp/client-ping/send-ping");
        AnonymousClass009.A02(this.A08);
        if (!this.A05 || this.A03 == null) {
            Log.w("xmpp/client-ping/send-ping; not connected, ignoring.");
        } else if (this.A0E > 0) {
            Log.w("xmpp/client-ping/send-ping; skipping ping request, pending ping already exists.");
            if (this.A0E > 0 && SystemClock.elapsedRealtime() > this.A0E + Math.min(32000L, Math.max(8000L, (long) (this.A09.A02(AbstractC15460nI.A22) * 1000)))) {
                A04();
            }
        } else {
            Log.i("xmpp/client-ping/periodic/cancel-alarm");
            A07(new Intent("com.whatsapp.alarm.CLIENT_PING_PERIODIC").setPackage("com.whatsapp"));
            this.A0E = SystemClock.elapsedRealtime();
            this.A07 = false;
            Log.i("xmpp/client-ping/timeout/schedule-alarm");
            if (!this.A0B.A02(AnonymousClass1UY.A01(this.A0D.A00, 0, new Intent("com.whatsapp.alarm.CLIENT_PING_TIMEOUT").setPackage("com.whatsapp"), 134217728), 2, SystemClock.elapsedRealtime() + Math.min(32000L, Math.max(8000L, (long) (this.A09.A02(AbstractC15460nI.A22) * 1000))))) {
                Log.w("xmpp/client-ping/timeout/schedule-alarm; failed to schedule alarm");
            }
            ((Handler) this.A03).obtainMessage(5).sendToTarget();
            this.A00++;
        }
    }

    public final void A06() {
        long j;
        Log.i("xmpp/client-ping/periodic/schedule-alarm");
        Context context = this.A0D.A00;
        AlarmManager A04 = this.A0C.A04();
        if (A04 == null) {
            Log.w("xmpp/client-ping/periodic/schedule-alarm; alarm manager is null");
            return;
        }
        PendingIntent A01 = AnonymousClass1UY.A01(context, 0, new Intent("com.whatsapp.alarm.CLIENT_PING_PERIODIC").setPackage("com.whatsapp"), 134217728);
        if (this.A00 == 0) {
            j = 15000;
        } else {
            j = 240000;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() + j;
        int i = 2;
        if (this.A04) {
            i = 3;
        }
        A04.set(i, elapsedRealtime, A01);
    }

    public void A07(Intent intent) {
        PendingIntent A01 = AnonymousClass1UY.A01(this.A0D.A00, 0, intent, 536870912);
        if (A01 != null) {
            AlarmManager A04 = this.A0C.A04();
            if (A04 == null) {
                Log.w("xmpp/client-ping/cancel-alarm; service is null");
                return;
            }
            A04.cancel(A01);
            A01.cancel();
        }
    }
}
