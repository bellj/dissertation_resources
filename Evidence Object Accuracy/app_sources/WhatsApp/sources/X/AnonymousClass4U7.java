package X;

import android.util.SparseArray;

/* renamed from: X.4U7  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4U7 {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public boolean A05;
    public boolean A06;
    public byte[] A07;
    public final SparseArray A08 = new SparseArray();
    public final SparseArray A09 = new SparseArray();
    public final AnonymousClass5X6 A0A;
    public final AnonymousClass4YP A0B;

    public AnonymousClass4U7(AnonymousClass5X6 r4) {
        this.A0A = r4;
        new AnonymousClass4DG();
        byte[] bArr = new byte[128];
        this.A07 = bArr;
        this.A0B = new AnonymousClass4YP(bArr, 0, 0);
        this.A05 = false;
    }
}
