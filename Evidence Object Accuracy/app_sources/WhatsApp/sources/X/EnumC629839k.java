package X;

import com.whatsapp.R;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class EnumC629839k extends Enum {
    public static final EnumC629839k A00 = new EnumC629839k(new C92734Xf(R.color.wdsButtonBorderlessContent, R.color.wdsButtonBorderlessContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonBorderlessBackgroundDefault, R.color.wdsButtonBorderlessBackgroundPressed, R.color.wdsButtonBorderlessBackgroundDefault), null, new C92734Xf(R.color.wdsButtonDestructiveBorderlessContent, R.color.wdsButtonDestructiveBorderlessContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonBorderlessBackgroundDefault, R.color.wdsButtonDestructiveBorderlessBackgroundPressed, R.color.wdsButtonBorderlessBackgroundDefault), null, new C92734Xf(R.color.wdsButtonMediaBorderlessContent, R.color.wdsButtonMediaBorderlessContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonMediaBorderlessBackgroundDefault, R.color.wdsButtonMediaBorderlessBackgroundPressed, R.color.wdsButtonMediaBorderlessBackgroundDefault), null, "BORDERLESS", 3);
    public static final EnumC629839k A01 = new EnumC629839k(new C92734Xf(R.color.wdsButtonFilledContent, R.color.wdsButtonFilledContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonFilledBackgroundDefault, R.color.wdsButtonFilledBackgroundPressed, R.color.wdsBackgroundDisabled), null, new C92734Xf(R.color.wdsButtonFilledContent, R.color.wdsButtonFilledContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonDestructiveFilledBackgroundDefault, R.color.wdsButtonDestructiveFilledBackgroundPressed, R.color.wdsBackgroundDisabled), null, new C92734Xf(R.color.wdsButtonMediaFilledContent, R.color.wdsButtonMediaFilledContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonMediaFilledBackgroundDefault, R.color.wdsButtonMediaBackgroundPressed, R.color.wdsBackgroundDisabled), null, "FILLED", 0);
    public static final EnumC629839k A02 = new EnumC629839k(new C92734Xf(R.color.wdsButtonOutlineContent, R.color.wdsButtonOutlineContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonOutlineBackgroundDefault, R.color.wdsButtonOutlineBackgroundPressed, R.color.wdsButtonOutlineBackgroundDefault), new C92734Xf(R.color.wdsButtonOutlineStroke, R.color.wdsButtonOutlineStroke, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonDestructiveOutlineContent, R.color.wdsButtonDestructiveOutlineContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonOutlineBackgroundDefault, R.color.wdsButtonDestructiveOutlineBackgroundPressed, R.color.wdsButtonOutlineBackgroundDefault), new C92734Xf(R.color.wdsButtonOutlineStroke, R.color.wdsButtonOutlineStroke, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonMediaOutlineContent, R.color.wdsButtonMediaOutlineContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonMediaOutlineBackgroundDefault, R.color.wdsButtonMediaBackgroundPressed, R.color.wdsButtonMediaOutlineBackgroundDefault), new C92734Xf(R.color.wdsButtonMediaOutlineStroke, R.color.wdsButtonMediaOutlineStroke, R.color.wdsContentDisabled), "OUTLINE", 2);
    public static final EnumC629839k A03 = new EnumC629839k(new C92734Xf(R.color.wdsButtonTonalContent, R.color.wdsButtonTonalContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonTonalBackgroundDefault, R.color.wdsButtonTonalBackgroundPressed, R.color.wdsBackgroundDisabled), null, new C92734Xf(R.color.wdsButtonDestructiveTonalContent, R.color.wdsButtonDestructiveTonalContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonDestructiveTonalBackgroundDefault, R.color.wdsButtonDestructiveTonalBackgroundPressed, R.color.wdsBackgroundDisabled), null, new C92734Xf(R.color.wdsButtonMediaTonalContent, R.color.wdsButtonMediaTonalContent, R.color.wdsContentDisabled), new C92734Xf(R.color.wdsButtonMediaTonalBackgroundDefault, R.color.wdsButtonMediaBackgroundPressed, R.color.wdsBackgroundDisabled), null, "TONAL", 1);
    public final C92734Xf backgroundDestructive;
    public final C92734Xf backgroundMedia;
    public final C92734Xf backgroundNormal;
    public final C92734Xf contentDestructive;
    public final C92734Xf contentMedia;
    public final C92734Xf contentNormal;
    public final C92734Xf strokeDestructive;
    public final C92734Xf strokeMedia;
    public final C92734Xf strokeNormal;

    public EnumC629839k(C92734Xf r1, C92734Xf r2, C92734Xf r3, C92734Xf r4, C92734Xf r5, C92734Xf r6, C92734Xf r7, C92734Xf r8, C92734Xf r9, String str, int i) {
        this.contentNormal = r1;
        this.backgroundNormal = r2;
        this.strokeNormal = r3;
        this.contentDestructive = r4;
        this.backgroundDestructive = r5;
        this.strokeDestructive = r6;
        this.contentMedia = r7;
        this.backgroundMedia = r8;
        this.strokeMedia = r9;
    }
}
