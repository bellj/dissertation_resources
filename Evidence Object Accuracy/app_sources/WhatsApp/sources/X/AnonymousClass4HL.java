package X;

import android.content.pm.Signature;

/* renamed from: X.4HL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4HL {
    public static final Signature A00;
    public static final Signature A01;
    public static final Signature[] A02;

    static {
        Signature signature = new Signature(C88714Gt.A00);
        A00 = signature;
        Signature signature2 = new Signature(C88714Gt.A01);
        A01 = signature2;
        A02 = new Signature[]{signature, signature2};
    }
}
