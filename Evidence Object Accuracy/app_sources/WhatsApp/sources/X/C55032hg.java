package X;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55032hg extends AnonymousClass03U {
    public final int A00;
    public final View A01;
    public final FrameLayout A02;
    public final TextEmojiLabel A03;
    public final C28801Pb A04;
    public final ThumbnailButton A05;
    public final /* synthetic */ C54512gq A06;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55032hg(FrameLayout frameLayout, C54512gq r7) {
        super(frameLayout);
        int i;
        this.A06 = r7;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 19 || (i2 < 21 && "samsung".equalsIgnoreCase(Build.MANUFACTURER))) {
            i = 1711315455;
        } else {
            i = 419430400;
        }
        this.A00 = i;
        this.A02 = frameLayout;
        ThumbnailButton thumbnailButton = (ThumbnailButton) frameLayout.findViewById(R.id.contact_photo);
        this.A05 = thumbnailButton;
        thumbnailButton.setEnabled(false);
        C28801Pb r1 = new C28801Pb(frameLayout, r7.A0C, r7.A0I, (int) R.id.contact_name);
        this.A04 = r1;
        r1.A04(r7.A00);
        TextEmojiLabel A0U = C12970iu.A0U(frameLayout, R.id.push_name);
        this.A03 = A0U;
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(this.A00));
        frameLayout.setForeground(stateListDrawable);
        this.A01 = frameLayout.findViewById(R.id.separator);
        A0U.setTextColor(r7.A02);
    }
}
