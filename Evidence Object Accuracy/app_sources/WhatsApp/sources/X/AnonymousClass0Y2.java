package X;

import android.content.ClipData;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContentInfo;
import java.util.Locale;

/* renamed from: X.0Y2  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Y2 implements AbstractC12630iE {
    public final int A00;
    public final int A01;
    public final ClipData A02;
    public final Uri A03;
    public final Bundle A04;

    @Override // X.AbstractC12630iE
    public ContentInfo AHo() {
        return null;
    }

    public AnonymousClass0Y2(AnonymousClass0Y0 r9) {
        this.A02 = r9.A02;
        int i = r9.A01;
        if (i <= 5) {
            this.A01 = i;
            int i2 = r9.A00;
            if ((i2 & 1) == i2) {
                this.A00 = i2;
                this.A03 = r9.A03;
                this.A04 = r9.A04;
                return;
            }
            StringBuilder sb = new StringBuilder("Requested flags 0x");
            sb.append(Integer.toHexString(i2));
            sb.append(", but only 0x");
            sb.append(Integer.toHexString(1));
            sb.append(" are allowed");
            throw new IllegalArgumentException(sb.toString());
        }
        throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%d, %d] (too high)", "source", 0, 5));
    }

    @Override // X.AbstractC12630iE
    public ClipData ABQ() {
        return this.A02;
    }

    @Override // X.AbstractC12630iE
    public int AD4() {
        return this.A00;
    }

    @Override // X.AbstractC12630iE
    public int AGp() {
        return this.A01;
    }

    public String toString() {
        String str;
        String valueOf;
        String obj;
        StringBuilder sb = new StringBuilder("ContentInfoCompat{clip=");
        sb.append(this.A02.getDescription());
        sb.append(", source=");
        int i = this.A01;
        if (i != 0) {
            str = i != 1 ? i != 2 ? i != 3 ? i != 4 ? i != 5 ? String.valueOf(i) : "SOURCE_PROCESS_TEXT" : "SOURCE_AUTOFILL" : "SOURCE_DRAG_AND_DROP" : "SOURCE_INPUT_METHOD" : "SOURCE_CLIPBOARD";
        } else {
            str = "SOURCE_APP";
        }
        sb.append(str);
        sb.append(", flags=");
        int i2 = this.A00;
        if ((i2 & 1) != 0) {
            valueOf = "FLAG_CONVERT_TO_PLAIN_TEXT";
        } else {
            valueOf = String.valueOf(i2);
        }
        sb.append(valueOf);
        Uri uri = this.A03;
        String str2 = "";
        if (uri == null) {
            obj = str2;
        } else {
            StringBuilder sb2 = new StringBuilder(", hasLinkUri(");
            sb2.append(uri.toString().length());
            sb2.append(")");
            obj = sb2.toString();
        }
        sb.append(obj);
        if (this.A04 != null) {
            str2 = ", hasExtras";
        }
        sb.append(str2);
        sb.append("}");
        return sb.toString();
    }
}
