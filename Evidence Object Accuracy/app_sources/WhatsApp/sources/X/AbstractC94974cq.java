package X;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.4cq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94974cq {
    public volatile int A00 = -1;

    public static String A01(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (i != 0) {
                if (Character.isUpperCase(charAt)) {
                    stringBuffer.append('_');
                } else {
                    stringBuffer.append(charAt);
                }
            }
            charAt = Character.toLowerCase(charAt);
            stringBuffer.append(charAt);
        }
        return stringBuffer.toString();
    }

    public abstract int A03();

    public AbstractC94974cq A04() {
        return (AbstractC94974cq) super.clone();
    }

    public abstract void A05(C95484do v);

    @Override // java.lang.Object
    public /* synthetic */ Object clone() {
        throw null;
    }

    public static void A02(Object obj, String str, StringBuffer stringBuffer, StringBuffer stringBuffer2) {
        String str2;
        if (obj != null) {
            if (obj instanceof AbstractC94974cq) {
                int length = stringBuffer.length();
                if (str != null) {
                    stringBuffer2.append(stringBuffer);
                    stringBuffer2.append(A01(str));
                    stringBuffer2.append(" <\n");
                    stringBuffer.append("  ");
                }
                Class<?> cls = obj.getClass();
                Field[] fields = cls.getFields();
                for (Field field : fields) {
                    int modifiers = field.getModifiers();
                    String name = field.getName();
                    if (!"cachedSize".equals(name) && (modifiers & 1) == 1 && (modifiers & 8) != 8 && !name.startsWith("_") && !name.endsWith("_")) {
                        Class<?> type = field.getType();
                        Object obj2 = field.get(obj);
                        if (!type.isArray() || type.getComponentType() == Byte.TYPE) {
                            A02(obj2, name, stringBuffer, stringBuffer2);
                        } else if (obj2 != null) {
                            int length2 = Array.getLength(obj2);
                            for (int i = 0; i < length2; i++) {
                                A02(Array.get(obj2, i), name, stringBuffer, stringBuffer2);
                            }
                        }
                    }
                }
                for (Method method : cls.getMethods()) {
                    String name2 = method.getName();
                    if (name2.startsWith("set")) {
                        String substring = name2.substring(3);
                        try {
                            if (C12970iu.A1Y(cls.getMethod(C72453ed.A0r("has", substring), new Class[0]).invoke(obj, new Object[0]))) {
                                A02(cls.getMethod(C72453ed.A0r("get", substring), new Class[0]).invoke(obj, new Object[0]), substring, stringBuffer, stringBuffer2);
                            }
                        } catch (NoSuchMethodException unused) {
                        }
                    }
                }
                if (str != null) {
                    stringBuffer.setLength(length);
                    stringBuffer2.append(stringBuffer);
                    str2 = ">\n";
                } else {
                    return;
                }
            } else {
                String A01 = A01(str);
                stringBuffer2.append(stringBuffer);
                stringBuffer2.append(A01);
                stringBuffer2.append(": ");
                if (obj instanceof String) {
                    String str3 = (String) obj;
                    if (!str3.startsWith("http") && str3.length() > 200) {
                        str3 = String.valueOf(str3.substring(0, 200)).concat("[...]");
                    }
                    int length3 = str3.length();
                    StringBuilder A0t = C12980iv.A0t(length3);
                    for (int i2 = 0; i2 < length3; i2++) {
                        char charAt = str3.charAt(i2);
                        if (charAt < ' ' || charAt > '~' || charAt == '\"' || charAt == '\'') {
                            Object[] objArr = new Object[1];
                            C12960it.A1P(objArr, charAt, 0);
                            A0t.append(String.format("\\u%04x", objArr));
                        } else {
                            A0t.append(charAt);
                        }
                    }
                    String obj3 = A0t.toString();
                    stringBuffer2.append("\"");
                    stringBuffer2.append(obj3);
                    stringBuffer2.append("\"");
                } else if (obj instanceof byte[]) {
                    stringBuffer2.append('\"');
                    for (byte b : (byte[]) obj) {
                        int i3 = b & 255;
                        if (i3 == 92 || i3 == 34) {
                            stringBuffer2.append('\\');
                        } else if (i3 < 32 || i3 >= 127) {
                            Object[] objArr2 = new Object[1];
                            C12960it.A1P(objArr2, i3, 0);
                            stringBuffer2.append(String.format("\\%03o", objArr2));
                        }
                        stringBuffer2.append((char) i3);
                    }
                    stringBuffer2.append('\"');
                } else {
                    stringBuffer2.append(obj);
                }
                str2 = "\n";
            }
            stringBuffer2.append(str2);
        }
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            A02(this, null, new StringBuffer(), stringBuffer);
            return stringBuffer.toString();
        } catch (IllegalAccessException | InvocationTargetException e) {
            return C72453ed.A0r("Error printing proto: ", e.getMessage());
        }
    }
}
