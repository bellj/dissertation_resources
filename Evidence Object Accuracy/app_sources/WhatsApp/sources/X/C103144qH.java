package X;

import android.content.Context;
import com.whatsapp.invites.ViewGroupInviteActivity;

/* renamed from: X.4qH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103144qH implements AbstractC009204q {
    public final /* synthetic */ ViewGroupInviteActivity A00;

    public C103144qH(ViewGroupInviteActivity viewGroupInviteActivity) {
        this.A00 = viewGroupInviteActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
