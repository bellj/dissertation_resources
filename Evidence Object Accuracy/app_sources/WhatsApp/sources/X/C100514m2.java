package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

/* renamed from: X.4m2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100514m2 implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(56);
    public final ArrayList A00;
    public final boolean A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C100514m2(Parcel parcel) {
        this.A01 = C12960it.A1S(parcel.readByte());
        this.A00 = parcel.createStringArrayList();
    }

    public C100514m2(ArrayList arrayList, boolean z) {
        this.A01 = z;
        this.A00 = arrayList;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.A01 ? (byte) 1 : 0);
        parcel.writeStringList(this.A00);
    }
}
