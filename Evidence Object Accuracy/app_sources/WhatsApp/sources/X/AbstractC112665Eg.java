package X;

import java.io.Serializable;

/* renamed from: X.5Eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC112665Eg implements AnonymousClass5WO, AnonymousClass5VE, Serializable {
    public final AnonymousClass5WO completion;

    public AbstractC112665Eg(AnonymousClass5WO r1) {
        this.completion = r1;
    }

    public void A00() {
        C114215Kq r1;
        if (this instanceof AbstractC113685Io) {
            AbstractC113685Io r3 = (AbstractC113685Io) this;
            AnonymousClass5WO r2 = r3.A00;
            if (!(r2 == null || r2 == r3)) {
                C16700pc.A0C(r3.ABi().get(AnonymousClass5ZS.A00));
                C114205Kp r22 = (C114205Kp) r2;
                do {
                } while (r22._reusableCancellableContinuation == AnonymousClass4HG.A00);
                Object obj = r22._reusableCancellableContinuation;
                if ((obj instanceof C114215Kq) && (r1 = (C114215Kq) obj) != null) {
                    r1.A04();
                }
            }
            r3.A00 = new C112645Ee();
        }
    }

    public Object A03(Object obj) {
        if (this instanceof C113695Ip) {
            C113695Ip r3 = (C113695Ip) this;
            int i = r3.label;
            if (i == 0) {
                r3.label = 1;
                C88174Eo.A00(obj);
                AnonymousClass5ZQ r1 = r3.$this_createCoroutineUnintercepted$inlined;
                C94564c6.A00(r1);
                return r1.AJ5(r3.$receiver$inlined, r3);
            } else if (i == 1) {
                r3.label = 2;
            } else {
                throw C12960it.A0U("This coroutine had already completed");
            }
        } else if (this instanceof C113675In) {
            C113675In r2 = (C113675In) this;
            Throwable A00 = AnonymousClass5BU.A00(obj);
            if (A00 != null) {
                r2.lastEmissionContext = new C112685Ei(A00);
            }
            AnonymousClass5WO r0 = r2.completion;
            if (r0 != null) {
                r0.Aas(obj);
            }
            return AnonymousClass4A6.A01;
        } else if (!(this instanceof C113645Ik)) {
            C113655Il r32 = (C113655Il) this;
            int i2 = r32.label;
            if (i2 == 0) {
                r32.label = 1;
                C88174Eo.A00(obj);
                AnonymousClass5ZQ r12 = r32.$this_createCoroutineUnintercepted$inlined;
                C94564c6.A00(r12);
                return r12.AJ5(r32.$receiver$inlined, r32);
            } else if (i2 == 1) {
                r32.label = 2;
            } else {
                throw C12960it.A0U("This coroutine had already completed");
            }
        } else {
            C113645Ik r22 = (C113645Ik) this;
            r22.result = obj;
            r22.label |= Integer.MIN_VALUE;
            return r22.this$0.A7Q(r22, null);
        }
        C88174Eo.A00(obj);
        return obj;
    }

    public AnonymousClass5WO A04(Object obj, AnonymousClass5WO r3) {
        throw C12980iv.A0u("create(Any?;Continuation) has not been overridden");
    }

    @Override // X.AnonymousClass5VE
    public AnonymousClass5VE ABA() {
        AnonymousClass5WO r1;
        if (!(this instanceof C113675In)) {
            r1 = this.completion;
        } else {
            r1 = ((C113675In) this).completion;
        }
        if (r1 instanceof AnonymousClass5VE) {
            return (AnonymousClass5VE) r1;
        }
        return null;
    }

    @Override // X.AnonymousClass5WO
    public final void Aas(Object obj) {
        AnonymousClass5WO r2 = this;
        while (true) {
            AbstractC112665Eg r22 = (AbstractC112665Eg) r2;
            AnonymousClass5WO r1 = r22.completion;
            C16700pc.A0C(r1);
            try {
                obj = r22.A03(obj);
                if (obj == AnonymousClass4A6.A01) {
                    return;
                }
            } catch (Throwable th) {
                obj = new AnonymousClass5BR(th);
            }
            r22.A00();
            if (r1 instanceof AbstractC112665Eg) {
                r2 = r1;
            } else {
                r1.Aas(obj);
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d1  */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
        // Method dump skipped, instructions count: 262
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC112665Eg.toString():java.lang.String");
    }
}
