package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.whatsapp.util.Log;

/* renamed from: X.1YR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YR extends Handler {
    public final /* synthetic */ C22320yt A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1YR(Looper looper, C22320yt r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void dispatchMessage(Message message) {
        long uptimeMillis = SystemClock.uptimeMillis();
        super.dispatchMessage(message);
        long uptimeMillis2 = SystemClock.uptimeMillis() - uptimeMillis;
        if (uptimeMillis2 > 1000) {
            StringBuilder sb = new StringBuilder("AsyncCommitManager/receipt/dispatching id: ");
            sb.append(message.arg1);
            sb.append(" msg:");
            sb.append(message.getCallback());
            sb.append(" took:");
            sb.append(uptimeMillis2);
            Log.i(sb.toString());
        }
    }
}
