package X;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;
import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.1iF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35551iF extends AbstractC35561iG {
    public C624537h A00;
    public final AbstractC18860tB A01 = new C35261hX(this);
    public final AnonymousClass12H A02;
    public final C14850m9 A03;

    public C35551iF(AnonymousClass12P r29, AbstractC15710nm r30, C14900mE r31, Mp4Ops mp4Ops, C239613r r33, C18790t3 r34, C16170oZ r35, AnonymousClass1J1 r36, C16590pI r37, AnonymousClass018 r38, C15650ng r39, AnonymousClass12H r40, C22810zg r41, C18470sV r42, C14850m9 r43, C244415n r44, AnonymousClass109 r45, AnonymousClass1CH r46, AbstractC15340mz r47, C15860o1 r48, AnonymousClass1BD r49, AnonymousClass1CW r50, AnonymousClass1CZ r51, C48252Fe r52, AnonymousClass1CY r53, AbstractC14440lR r54, C17020q8 r55) {
        super(r29, r30, r31, mp4Ops, r33, r34, r35, r36, r37, r38, r39, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55);
        this.A03 = r43;
        this.A02 = r40;
    }

    @Override // X.AbstractC33621eg, X.AbstractC33631eh
    public void A01() {
        super.A01();
        this.A02.A04(this.A01);
    }

    @Override // X.AbstractC33621eg
    public void A0C() {
        super.A0C();
        C624537h r1 = this.A00;
        if (r1 != null) {
            r1.A03(true);
            this.A00 = null;
        }
        AbstractC15340mz r2 = ((AbstractC35561iG) this).A09;
        C111675Ah r4 = new AnonymousClass5V5(A0B()) { // from class: X.5Ah
            public final /* synthetic */ C35571iH A01;

            {
                this.A01 = r2;
            }

            @Override // X.AnonymousClass5V5
            public final void AVG(C90494Oc r5) {
                AnonymousClass1IS r12;
                AbstractC15340mz r3 = AbstractC15340mz.this;
                C35571iH r22 = this.A01;
                C91054Qg r0 = r5.A01;
                if (r0 != null && (r12 = r0.A01) != null && r12.equals(r3.A0z)) {
                    r22.A0A.callOnClick();
                }
            }
        };
        C15650ng r3 = ((AbstractC33621eg) this).A0E;
        C17020q8 r12 = this.A0S;
        AbstractC14640lm A0B = r2.A0B();
        AnonymousClass009.A05(A0B);
        C624537h r22 = new C624537h(r3, A0B, r4, r12);
        this.A00 = r22;
        this.A0R.Aaz(r22, new Void[0]);
    }

    @Override // X.AbstractC33621eg
    public void A0E() {
        super.A0E();
        BottomSheetBehavior bottomSheetBehavior = ((AbstractC33621eg) this).A00;
        if (bottomSheetBehavior.A0B != 4) {
            bottomSheetBehavior.A0M(4);
        } else {
            ((AbstractC33621eg) this).A01.A06.setVisibility(8);
            ((AbstractC33621eg) this).A01.A06.setAlpha(0.0f);
        }
        ((AbstractC33621eg) this).A01.A0D.setVisibility(0);
    }

    @Override // X.AbstractC33621eg
    public void A0I(int i) {
        super.A0I(i);
        if (i == 3) {
            A0R();
        }
    }

    @Override // X.AbstractC33621eg
    public void A0K(View view) {
        super.A0K(view);
        C35571iH A0B = A0B();
        ViewGroup viewGroup = A0B.A08;
        int i = 0;
        if (C15380n4.A0M(((AbstractC35561iG) this).A09.A0B())) {
            i = 8;
        }
        viewGroup.setVisibility(i);
        C27531Hw.A06(A0B.A0D);
        A0B.A0D.setText(R.string.notification_quick_reply);
        A0B.A0D.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(A0B, 30, this));
        A0P(false);
        A0H();
        this.A02.A03(this.A01);
    }

    public final void A0R() {
        ActivityC000900k A0B;
        StringBuilder sb = new StringBuilder("playbackPage/reply page=");
        sb.append(this);
        sb.append("; host=");
        StatusPlaybackBaseFragment statusPlaybackBaseFragment = this.A0L.A01;
        sb.append(statusPlaybackBaseFragment);
        Log.i(sb.toString());
        View view = ((AbstractC33631eh) this).A00;
        AnonymousClass009.A03(view);
        ActivityC13810kN r0 = (ActivityC13810kN) AbstractC35731ia.A01(view.getContext(), ActivityC13810kN.class);
        if (r0 == null || !r0.AJN()) {
            A0D();
            ((AbstractC33621eg) this).A01.A0D.setVisibility(8);
            View view2 = ((AbstractC33631eh) this).A00;
            AnonymousClass009.A03(view2);
            Context context = view2.getContext();
            Intent intent = new Intent();
            intent.setClassName(context.getPackageName(), "com.whatsapp.status.playback.StatusReplyActivity");
            C38211ni.A00(intent, ((AbstractC35561iG) this).A09.A0z);
            intent.putExtra("isStatusReply", true);
            if (statusPlaybackBaseFragment == null || (A0B = statusPlaybackBaseFragment.A0B()) == null || A0B.isFinishing()) {
                View view3 = ((AbstractC33631eh) this).A00;
                AnonymousClass009.A03(view3);
                view3.getContext().startActivity(intent);
                return;
            }
            if (this.A03.A07(1455)) {
                A0B.getWindow().setSoftInputMode(48);
            }
            A0B.startActivityForResult(intent, 10);
            return;
        }
        StringBuilder sb2 = new StringBuilder("playbackPage/reply reply-already-ended page=");
        sb2.append(this);
        sb2.append("; host=");
        sb2.append(statusPlaybackBaseFragment);
        Log.i(sb2.toString());
    }
}
