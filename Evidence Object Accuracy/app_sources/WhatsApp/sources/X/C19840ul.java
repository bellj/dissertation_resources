package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0ul  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19840ul {
    public final C18420sQ A00;
    public final C14850m9 A01;
    public final Map A02 = new HashMap();

    public C19840ul(C18420sQ r2, C14850m9 r3) {
        this.A01 = r3;
        this.A00 = r2;
    }

    public static AnonymousClass1Q5 A00(C19840ul r2) {
        return (AnonymousClass1Q5) r2.A02.get("catalog_collections_view_tag");
    }

    public void A01(int i, String str, String str2) {
        Map map = this.A02;
        AnonymousClass1Q5 r2 = (AnonymousClass1Q5) map.get(str);
        if (r2 == null) {
            C18420sQ r3 = this.A00;
            AnonymousClass1Q6 r22 = new AnonymousClass1Q6(i);
            if (this.A01.A07(1272)) {
                r22.A03 = true;
            }
            r2 = r3.A00(r22, str);
            map.put(str, r2);
        }
        r2.A0D(str2, -1);
    }

    public void A02(String str) {
        AnonymousClass1Q5 r1 = (AnonymousClass1Q5) this.A02.get(str);
        if (r1 != null) {
            r1.A07("datasource");
        }
    }

    public void A03(String str) {
        AnonymousClass1Q5 r1 = (AnonymousClass1Q5) this.A02.get(str);
        if (r1 != null) {
            r1.A08("datasource");
        }
    }

    public void A04(String str, String str2, String str3) {
        AnonymousClass1Q5 r1 = (AnonymousClass1Q5) this.A02.get(str);
        if (r1 != null) {
            r1.A0A(str2, str3, true);
        }
    }

    public void A05(String str, String str2, boolean z) {
        AnonymousClass1Q5 r1 = (AnonymousClass1Q5) this.A02.get(str);
        if (r1 != null) {
            r1.A0B(str2, z, true);
        }
    }

    public void A06(String str, boolean z) {
        Map map = this.A02;
        AnonymousClass1Q5 r1 = (AnonymousClass1Q5) map.get(str);
        if (r1 != null) {
            short s = 3;
            if (z) {
                s = 2;
            }
            r1.A0C(s);
            map.remove(str);
        }
    }
}
