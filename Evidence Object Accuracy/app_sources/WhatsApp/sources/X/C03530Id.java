package X;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.view.View;

/* renamed from: X.0Id  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03530Id extends AnonymousClass2k7 {
    public C03530Id(C14260l7 r1, AnonymousClass28D r2) {
        super(r1, r2);
    }

    @Override // X.AnonymousClass2k7
    public void A06(View view, C14260l7 r8, AnonymousClass28D r9, Object obj) {
        C02330Bg r7 = (C02330Bg) view;
        AnonymousClass28D A0F = r9.A0F(35);
        int i = 24;
        int i2 = -16777216;
        if (A0F != null) {
            try {
                i = (int) AnonymousClass3JW.A02(A0F.A0I(36));
            } catch (AnonymousClass491 unused) {
            }
            AnonymousClass28D A0F2 = A0F.A0F(35);
            if (A0F2 != null) {
                i2 = AnonymousClass4Di.A00(r8, A0F2);
            }
        }
        AnonymousClass0A8 r2 = new AnonymousClass0A8(r8.A00(), i2, i);
        AnonymousClass0A8 r0 = null;
        if (r2 instanceof Animatable) {
            r0 = r2;
        }
        r7.A00 = r0;
        r7.setImageDrawable(r2);
        Animatable animatable = r7.A00;
        if (animatable != null) {
            animatable.start();
        }
        r7.A01 = true;
    }

    @Override // X.AnonymousClass2k7
    public void A07(View view, C14260l7 r3, AnonymousClass28D r4, Object obj) {
        C02330Bg r2 = (C02330Bg) view;
        Animatable animatable = r2.A00;
        if (animatable != null) {
            animatable.stop();
        }
        r2.A01 = false;
    }

    @Override // X.AnonymousClass2k7, X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        return new C02330Bg(context);
    }
}
