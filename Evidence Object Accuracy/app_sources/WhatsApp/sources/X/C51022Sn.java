package X;

import android.view.MenuItem;
import android.view.SubMenu;
import com.whatsapp.gallery.GalleryTabHostFragment;

/* renamed from: X.2Sn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51022Sn extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ SubMenu $overflowSubMenu;
    public final /* synthetic */ GalleryTabHostFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C51022Sn(SubMenu subMenu, GalleryTabHostFragment galleryTabHostFragment) {
        super(1);
        this.$overflowSubMenu = subMenu;
        this.this$0 = galleryTabHostFragment;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Iterable<AnonymousClass3BM> iterable = (Iterable) obj;
        C16700pc.A0E(iterable, 0);
        SubMenu subMenu = this.$overflowSubMenu;
        GalleryTabHostFragment galleryTabHostFragment = this.this$0;
        for (AnonymousClass3BM r2 : iterable) {
            MenuItem add = subMenu.add(r2.A02);
            C16700pc.A0B(add);
            add.setIcon(r2.A01);
            add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener(r2, galleryTabHostFragment) { // from class: X.4mc
                public final /* synthetic */ AnonymousClass3BM A00;
                public final /* synthetic */ GalleryTabHostFragment A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.view.MenuItem.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    GalleryTabHostFragment galleryTabHostFragment2 = this.A01;
                    AnonymousClass3BM r1 = this.A00;
                    C16700pc.A0E(galleryTabHostFragment2, 0);
                    C16700pc.A0E(r1, 1);
                    galleryTabHostFragment2.startActivityForResult(r1.A00, 91);
                    return false;
                }
            });
        }
        return AnonymousClass1WZ.A00;
    }
}
