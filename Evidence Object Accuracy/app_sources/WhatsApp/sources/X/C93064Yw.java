package X;

import android.util.SparseIntArray;

/* renamed from: X.4Yw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93064Yw {
    public static final SparseIntArray A00 = new SparseIntArray(0);

    public static C93604aR A00() {
        int min = (int) Math.min(Runtime.getRuntime().maxMemory(), 2147483647L);
        int i = min >> 1;
        if (min > 16777216) {
            i = (min >> 2) * 3;
        }
        return new C93604aR(A00, 0, i, -1);
    }
}
