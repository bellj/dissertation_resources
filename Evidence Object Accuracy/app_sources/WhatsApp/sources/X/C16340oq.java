package X;

import android.util.Base64;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.0oq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16340oq {
    public final AbstractC15710nm A00;
    public final AnonymousClass1B7 A01;
    public final C18640sm A02;
    public final C14850m9 A03;

    public C16340oq(AbstractC15710nm r1, AnonymousClass1B7 r2, C18640sm r3, C14850m9 r4) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public String A00(C16250oh r7) {
        byte[] bArr;
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            instance.init(new SecretKeySpec(r7.A02, instance.getAlgorithm()));
            bArr = instance.doFinal("".getBytes());
        } catch (Exception unused) {
            Log.e("DirectoryTokenManagerImpl/computeHMAC Failed to compute HMAC");
            this.A00.AaV("DirectoryTokenManagerImpl/computeHMAC", "Failed to compute HMAC", true);
            bArr = null;
        }
        if (bArr != null) {
            return Base64.encodeToString(bArr, 10);
        }
        return null;
    }
}
