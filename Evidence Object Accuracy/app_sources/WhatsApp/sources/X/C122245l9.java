package X;

import com.whatsapp.R;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* renamed from: X.5l9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122245l9 extends AnonymousClass6L2 {
    public C122245l9(AnonymousClass018 r1, Calendar calendar, int i) {
        super(r1, calendar, i);
    }

    @Override // X.AnonymousClass6L2, java.util.Calendar, java.lang.Object
    public String toString() {
        long timeInMillis = getTimeInMillis();
        int i = (timeInMillis > 0 ? 1 : (timeInMillis == 0 ? 0 : -1));
        AnonymousClass018 r1 = this.whatsAppLocale;
        if (i <= 0) {
            return r1.A09(R.string.unknown);
        }
        return new SimpleDateFormat(r1.A08(177), C12970iu.A14(r1)).format(new Date(timeInMillis));
    }
}
