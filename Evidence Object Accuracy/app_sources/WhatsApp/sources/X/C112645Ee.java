package X;

/* renamed from: X.5Ee  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112645Ee implements AnonymousClass5WO {
    public String toString() {
        return "This continuation is already complete";
    }

    @Override // X.AnonymousClass5WO
    public AnonymousClass5X4 ABi() {
        throw C12960it.A0U("This continuation is already complete");
    }

    @Override // X.AnonymousClass5WO
    public void Aas(Object obj) {
        throw C12960it.A0U("This continuation is already complete");
    }
}
