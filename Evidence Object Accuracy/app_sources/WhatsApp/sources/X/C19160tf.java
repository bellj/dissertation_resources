package X;

import com.whatsapp.util.Log;

/* renamed from: X.0tf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19160tf extends AbstractC18500sY implements AbstractC19010tQ {
    public final C16510p9 A00;
    public final C20060v9 A01;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19160tf(C16510p9 r3, C20060v9 r4, C18480sW r5) {
        super(r5, "message_location", 2);
        this.A00 = r3;
        this.A01 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00e4, code lost:
        if (r13 != 30) goto L_0x0174;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x017e A[Catch: SQLiteException -> 0x021d, all -> 0x022c, TRY_ENTER, TryCatch #5 {SQLiteException -> 0x021d, blocks: (B:6:0x005b, B:8:0x0076, B:10:0x0084, B:11:0x0099, B:18:0x00e8, B:20:0x0117, B:22:0x0121, B:23:0x014a, B:25:0x0156, B:26:0x0159, B:27:0x015c, B:28:0x016b, B:29:0x0174, B:32:0x017e, B:33:0x0185, B:56:0x01ea, B:57:0x01f2, B:59:0x01fb, B:60:0x0200, B:62:0x020e, B:63:0x0219), top: B:83:0x005b, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0190 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // X.AbstractC18500sY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass2Ez A09(android.database.Cursor r33) {
        /*
        // Method dump skipped, instructions count: 561
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19160tf.A09(android.database.Cursor):X.2Ez");
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("message_location", null, null);
            C21390xL r1 = this.A06;
            r1.A03("location_ready");
            r1.A03("migration_message_location_index");
            r1.A03("migration_message_location_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("LocationMessageStore/LocationMessageDatabaseMigration/resetMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
