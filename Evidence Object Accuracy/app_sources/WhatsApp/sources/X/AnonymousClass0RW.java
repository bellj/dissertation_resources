package X;

import android.os.Build;

/* renamed from: X.0RW  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0RW {
    public final AnonymousClass0PY A00;

    public AnonymousClass0RW() {
        AnonymousClass0PY r1;
        int i = Build.VERSION.SDK_INT;
        if (i >= 30) {
            r1 = new C02650Dj();
        } else if (i >= 29) {
            r1 = new AnonymousClass0Dk();
        } else if (i >= 20) {
            r1 = new C02660Dl();
        } else {
            r1 = new AnonymousClass0PY(new C018408o());
        }
        this.A00 = r1;
    }

    public AnonymousClass0RW(C018408o r3) {
        AnonymousClass0PY r0;
        int i = Build.VERSION.SDK_INT;
        if (i >= 30) {
            r0 = new C02650Dj(r3);
        } else if (i >= 29) {
            r0 = new AnonymousClass0Dk(r3);
        } else if (i >= 20) {
            r0 = new C02660Dl(r3);
        } else {
            r0 = new AnonymousClass0PY(r3);
        }
        this.A00 = r0;
    }
}
