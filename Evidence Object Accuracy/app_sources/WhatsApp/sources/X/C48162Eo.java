package X;

import java.nio.charset.Charset;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.2Eo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48162Eo {
    public static final Charset A00 = AnonymousClass01V.A0A;

    public static String A00(AbstractC15710nm r5, AnonymousClass124 r6, String str) {
        byte[] bArr;
        try {
            C31091Zz A002 = AnonymousClass122.A00(new JSONArray(str));
            if (A002 == null) {
                bArr = null;
            } else {
                bArr = r6.A01(A002, AnonymousClass029.A0M);
            }
            if (bArr != null) {
                return new String(bArr, A00);
            }
            r5.AaV("BusinessDirectoryStorageUtil/decryptStringData", "Failed to decrypt string data", true);
            throw new Exception("Failed to decrypt string data");
        } catch (JSONException e) {
            r5.AaV("BusinessDirectoryStorageUtil/decryptSearchLocation", e.getMessage(), true);
            throw e;
        }
    }

    public static String A01(AbstractC15710nm r3, AnonymousClass124 r4, String str) {
        String A002;
        C31091Zz A003 = r4.A00(AnonymousClass029.A0M, str.getBytes(A00));
        if (A003 != null && (A002 = A003.A00()) != null && str.equals(A00(r3, r4, A002))) {
            return A002;
        }
        r3.AaV("BusinessDirectoryStorageUtil/encryptStringData", "Failed to encrypt string data", true);
        throw new Exception("Failed to encrypt string data");
    }
}
