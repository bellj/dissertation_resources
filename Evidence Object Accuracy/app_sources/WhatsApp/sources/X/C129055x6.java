package X;

import java.io.File;

/* renamed from: X.5x6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129055x6 {
    public C38721ob A00;
    public final C14900mE A01;
    public final C18790t3 A02;
    public final C16590pI A03;
    public final C18810t5 A04;

    public C129055x6(C14900mE r1, C18790t3 r2, C16590pI r3, C18810t5 r4) {
        this.A03 = r3;
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
    }

    public C38721ob A00() {
        C38721ob r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        C38771og r1 = new C38771og(this.A01, this.A02, this.A04, new File(this.A03.A00.getCacheDir(), "bloks_images"), "bloks-bridge-manager");
        r1.A00 = Integer.MAX_VALUE;
        r1.A05 = true;
        C38721ob A00 = r1.A00();
        this.A00 = A00;
        return A00;
    }
}
