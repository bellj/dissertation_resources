package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1wQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43111wQ {
    public final NativeHolder A00;

    public C43111wQ(NativeHolder nativeHolder) {
        this.A00 = nativeHolder;
    }

    public C43111wQ(String str, String str2, int i) {
        JniBridge.getInstance();
        this.A00 = new C43111wQ((NativeHolder) JniBridge.jvidispatchOIOO(0, (long) i, str, str2)).A00;
    }
}
