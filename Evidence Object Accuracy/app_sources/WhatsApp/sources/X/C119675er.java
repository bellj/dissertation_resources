package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.5er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119675er extends C27131Gd {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    public C119675er(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r3) {
        if (r3 != null) {
            IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
            if (r3.equals(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C)) {
                indiaUpiSendPaymentActivity.A3N();
            }
        }
    }

    @Override // X.C27131Gd
    public void A01(AbstractC14640lm r3) {
        if (r3 != null) {
            IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
            if (r3.equals(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C)) {
                indiaUpiSendPaymentActivity.A3N();
            }
        }
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        if (userJid != null) {
            IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
            if (userJid.equals(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C)) {
                indiaUpiSendPaymentActivity.A3N();
            }
        }
    }
}
