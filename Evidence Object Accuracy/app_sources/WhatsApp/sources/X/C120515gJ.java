package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.voipcalling.CallLinkInfo;
import java.util.ArrayList;

/* renamed from: X.5gJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120515gJ extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final AnonymousClass102 A04;
    public final C14850m9 A05;
    public final C17220qS A06;
    public final C1329668y A07;
    public final C21860y6 A08;
    public final C18650sn A09;
    public final C18610sj A0A;
    public final C17070qD A0B;
    public final AnonymousClass6BE A0C;
    public final C121265hX A0D;
    public final String A0E;

    public C120515gJ(Context context, C14900mE r3, C15570nT r4, C18640sm r5, AnonymousClass102 r6, C14850m9 r7, C17220qS r8, C1308460e r9, C1329668y r10, C21860y6 r11, C18650sn r12, C18610sj r13, C17070qD r14, AnonymousClass6BE r15, C121265hX r16, C18590sh r17) {
        super(r9.A04, r13);
        this.A00 = context;
        this.A05 = r7;
        this.A01 = r3;
        this.A02 = r4;
        this.A06 = r8;
        this.A0B = r14;
        this.A0E = r17.A01();
        this.A08 = r11;
        this.A0A = r13;
        this.A04 = r6;
        this.A0C = r15;
        this.A03 = r5;
        this.A09 = r12;
        this.A07 = r10;
        this.A0D = r16;
    }

    public final void A00(AnonymousClass1ZR r4, AnonymousClass1ZR r5, AnonymousClass1FK r6, String str, String str2) {
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("action", "upi-remove-credential", A0l);
        C117295Zj.A1M("vpa", C117315Zl.A0K(r4), A0l);
        if (!TextUtils.isEmpty(str)) {
            C117295Zj.A1M("vpa-id", str, A0l);
        }
        C117295Zj.A1M("upi-bank-info", (String) C117295Zj.A0R(r5), A0l);
        C117295Zj.A1M("device-id", this.A0E, A0l);
        C117295Zj.A1M("credential-id", str2, A0l);
        this.A0A.A09(r6, C117295Zj.A0K(A0l));
    }

    public void A01(AnonymousClass1ZR r4, AnonymousClass1ZR r5, AnonymousClass1FK r6, String str, String str2, boolean z) {
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("action", "upi-edit-default-credential", A0l);
        C117295Zj.A1M("vpa", C117315Zl.A0K(r4), A0l);
        if (!TextUtils.isEmpty(str)) {
            C117295Zj.A1M("vpa-id", str, A0l);
        }
        C117295Zj.A1M("upi-bank-info", (String) C117295Zj.A0R(r5), A0l);
        C117295Zj.A1M("device-id", this.A0E, A0l);
        C117295Zj.A1M("credential-id", str2, A0l);
        C117295Zj.A1M(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID, Integer.toString(z ? 1 : 0), A0l);
        this.A0A.A0A(r6, C117295Zj.A0K(A0l));
    }
}
