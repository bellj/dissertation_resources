package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0cR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09150cR implements Runnable {
    public final /* synthetic */ BiometricFragment A00;

    public RunnableC09150cR(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.A01.A0L = false;
    }
}
