package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1lL  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1lL extends AnonymousClass02M {
    public final List A00 = new ArrayList();

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void A0A(AnonymousClass03U r1) {
        ((AbstractC75723kJ) r1).A08();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AbstractC89244Jf) this.A00.get(i)).A00;
    }
}
