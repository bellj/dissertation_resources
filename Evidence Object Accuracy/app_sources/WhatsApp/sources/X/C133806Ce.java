package X;

import android.view.View;
import com.whatsapp.jid.UserJid;

/* renamed from: X.6Ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133806Ce implements AnonymousClass61g {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AbstractC28901Pl A01;
    public final /* synthetic */ AnonymousClass6CO A02;

    @Override // X.AbstractC136476Mr
    public void AaH() {
    }

    public C133806Ce(View view, AbstractC28901Pl r2, AnonymousClass6CO r3) {
        this.A02 = r3;
        this.A00 = view;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass61g
    public void A91() {
        this.A02.A09.A1B();
    }

    @Override // X.AnonymousClass61g
    public void ASe(AnonymousClass1KC r14) {
        this.A00.setVisibility(0);
        AnonymousClass6CO r1 = this.A02;
        r1.A09.A1G(false);
        C118145bL r7 = r1.A0A;
        AbstractC14640lm r3 = r1.A02;
        long j = r1.A00;
        UserJid userJid = r1.A03;
        C118145bL.A01(this.A01, r3, userJid, r14, r1.A07, r7, r1.A0B, r1.A0C, r1.A0D, j);
    }

    @Override // X.AbstractC136476Mr
    public void AaN() {
        this.A00.setVisibility(8);
    }

    @Override // X.AbstractC136476Mr
    public void AaQ() {
        this.A02.A0A.A0q.A0B(Boolean.FALSE);
    }
}
