package X;

import android.content.Context;
import java.util.Map;

/* renamed from: X.2Pv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50512Pv {
    public final Map A00;

    public C50512Pv(Map map) {
        this.A00 = map;
    }

    public void A00(Context context, AbstractC15340mz r6, int i) {
        C16470p4 ABf;
        int i2;
        Map map;
        int i3;
        if ((r6 instanceof AbstractC16390ow) && (ABf = ((AbstractC16390ow) r6).ABf()) != null && (i2 = ABf.A00) != 0) {
            int i4 = 2;
            if (i2 != 1) {
                if (i2 != 2) {
                    i4 = 3;
                    if (i2 != 3) {
                        i4 = 4;
                        if (i2 != 4) {
                            i4 = 5;
                            if (i2 != 5) {
                                return;
                            }
                        }
                    }
                } else {
                    map = this.A00;
                    i3 = 1;
                    ((AbstractC50412Pl) map.get(i3)).AZA(context, r6, ABf, i);
                }
            }
            map = this.A00;
            i3 = Integer.valueOf(i4);
            ((AbstractC50412Pl) map.get(i3)).AZA(context, r6, ABf, i);
        }
    }
}
