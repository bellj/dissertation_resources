package X;

import android.database.ContentObserver;
import android.os.Handler;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.util.Log;

/* renamed from: X.2TD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2TD extends ContentObserver {
    public final /* synthetic */ MediaGalleryFragmentBase A00;

    @Override // android.database.ContentObserver
    public boolean deliverSelfNotifications() {
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2TD(Handler handler, MediaGalleryFragmentBase mediaGalleryFragmentBase) {
        super(handler);
        this.A00 = mediaGalleryFragmentBase;
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z) {
        StringBuilder sb = new StringBuilder("mediagalleryfragmentbase/onchange ");
        sb.append(z);
        Log.i(sb.toString());
        MediaGalleryFragmentBase mediaGalleryFragmentBase = this.A00;
        AbstractC35581iK r0 = mediaGalleryFragmentBase.A0H;
        if (r0 != null) {
            if (!z) {
                r0.AaX();
                mediaGalleryFragmentBase.A1E();
            }
            mediaGalleryFragmentBase.A00 = mediaGalleryFragmentBase.A0H.getCount();
        }
        mediaGalleryFragmentBase.A06.A02();
    }
}
