package X;

import com.whatsapp.jid.Jid;
import java.util.ArrayList;

/* renamed from: X.2PH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PH extends AnonymousClass2PA {
    public final String A00;
    public final ArrayList A01;

    public AnonymousClass2PH(Jid jid, String str, String str2, ArrayList arrayList, long j) {
        super(jid, str, j);
        this.A01 = arrayList;
        this.A00 = str2;
    }
}
