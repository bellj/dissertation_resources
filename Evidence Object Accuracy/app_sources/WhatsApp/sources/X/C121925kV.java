package X;

import android.content.Context;
import android.view.ViewGroup;
import com.whatsapp.payments.ui.NoviPaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow;
import java.util.List;

/* renamed from: X.5kV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121925kV extends C118645c9 {
    public final /* synthetic */ NoviPaymentTransactionHistoryActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C121925kV(Context context, AnonymousClass018 r14, C15650ng r15, AnonymousClass1In r16, C30931Zj r17, NoviPaymentTransactionHistoryActivity noviPaymentTransactionHistoryActivity, AnonymousClass6LY r19, C129795yJ r20, AnonymousClass14X r21, List list) {
        super(context, r14, r15, r16, r17, r19, r20, r21, list, 3);
        this.A00 = noviPaymentTransactionHistoryActivity;
    }

    @Override // X.C118645c9
    public PeerPaymentTransactionRow A0E(ViewGroup viewGroup, int i) {
        if (i == 2001) {
            return new C123685nf(this.A03);
        }
        return super.A0E(viewGroup, i);
    }

    @Override // X.C118645c9, X.AnonymousClass02M
    public int getItemViewType(int i) {
        int i2;
        AnonymousClass1IR r1 = (AnonymousClass1IR) ((C118645c9) this).A01.get(i);
        if (r1.A01 == 3 && ((i2 = r1.A03) == 1 || i2 == 2 || i2 == 3 || i2 == 4 || i2 == 6 || i2 == 7 || i2 == 8 || i2 == 10 || i2 == 20 || i2 == 30 || i2 == 40 || i2 == 100 || i2 == 200 || i2 == 300)) {
            return 2001;
        }
        return super.getItemViewType(i);
    }
}
