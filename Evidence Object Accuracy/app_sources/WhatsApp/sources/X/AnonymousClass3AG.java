package X;

import android.util.Base64;
import com.facebook.minscript.compiler.MinsCompilerImpl$Helper;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReferenceArray;

/* renamed from: X.3AG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AG {
    /* JADX INFO: finally extract failed */
    public static Object A00(C14220l3 r18, AbstractC14200l1 r19, C14240l5 r20) {
        boolean z;
        AnonymousClass2DE r6;
        ByteBuffer doCompile;
        StringBuilder A0k;
        if (r19 instanceof C1093751l) {
            C1093751l r7 = (C1093751l) r19;
            C90764Pd r5 = r20.A01;
            if (r7 != null) {
                C90024Mh r3 = r7.A01;
                if (r3 == null) {
                    C90014Mg r8 = r7.A00;
                    if (r8 != null) {
                        AnonymousClass3IR r12 = r8.A00;
                        if (r12 == null) {
                            String str = r8.A01;
                            if (str != null) {
                                boolean startsWith = str.startsWith("sraxr");
                                if (!startsWith && !str.startsWith(" ")) {
                                    if (str.startsWith("lispx")) {
                                        String substring = str.substring(5, 15);
                                        String str2 = r5.A02;
                                        if (!substring.equals(str2)) {
                                            A0k = C12960it.A0k("MinScript compiler detected version id mismatch (client ");
                                            A0k.append(str2);
                                            A0k.append(", got ");
                                            A0k.append(substring);
                                            A0k.append("): ");
                                        }
                                    } else {
                                        A0k = C12960it.A0k("MinScript compiler detected raw Lispy: ");
                                    }
                                    String A0d = C12960it.A0d(str.substring(0, Math.min(str.length(), 200)), A0k);
                                    if (r5.A01 != null) {
                                        C28691Op.A00("MinScript", A0d);
                                    }
                                }
                                if (startsWith) {
                                    doCompile = ByteBuffer.wrap(Base64.decode(str, 0));
                                } else {
                                    if (str.startsWith("lispx")) {
                                        str = str.substring(15);
                                    }
                                    doCompile = MinsCompilerImpl$Helper.doCompile(str);
                                }
                                doCompile.order(ByteOrder.nativeOrder());
                                ByteBuffer slice = doCompile.slice();
                                slice.order(doCompile.order());
                                int capacity = doCompile.capacity();
                                int i = slice.getInt();
                                int i2 = slice.getShort() & 65535;
                                int i3 = 65535 & slice.getShort();
                                if (i == -1397639502 && i2 == 1 && i3 == 0) {
                                    int i4 = slice.getInt();
                                    int i5 = slice.getInt();
                                    int position = doCompile.position();
                                    if (i4 > capacity) {
                                        StringBuilder A0k2 = C12960it.A0k("buffer is smaller than encoded size ");
                                        A0k2.append(i4);
                                        A0k2.append(" ");
                                        A0k2.append(capacity);
                                        A0k2.append(" byteBuffer.order:");
                                        A0k2.append(doCompile.order());
                                        A0k2.append(" native order:");
                                        throw new AnonymousClass5H7(C12970iu.A0s(ByteOrder.nativeOrder(), A0k2));
                                    } else if (i4 >= position) {
                                        int i6 = position + (i5 << 4);
                                        if (i6 <= i4) {
                                            C90774Pe[] r62 = new C90774Pe[i5];
                                            for (int i7 = 0; i7 < i5; i7++) {
                                                C90774Pe r52 = new C90774Pe();
                                                r52.A00 = slice.getInt();
                                                r52.A02 = slice.getInt();
                                                r52.A01 = slice.getInt();
                                                slice.getInt();
                                                int i8 = r52.A02;
                                                if (i8 % 4 == 0) {
                                                    int i9 = r52.A01;
                                                    if (i6 > i8 || i8 + i9 > i4) {
                                                        Object[] objArr = new Object[2];
                                                        C12960it.A1P(objArr, i7, 0);
                                                        C12960it.A1P(objArr, r52.A00, 1);
                                                        throw new AnonymousClass5H7(String.format("section index %d kind %d invalid offset/size", objArr));
                                                    }
                                                    r62[i7] = r52;
                                                } else {
                                                    Object[] objArr2 = new Object[2];
                                                    C12960it.A1P(objArr2, i7, 0);
                                                    C12960it.A1P(objArr2, r52.A00, 1);
                                                    throw new AnonymousClass5H7(String.format("section index %d kind %d invalid alignment", objArr2));
                                                }
                                            }
                                            if (i5 >= 4) {
                                                int i10 = 0;
                                                while (r62[i10].A00 == i10) {
                                                    i10++;
                                                    if (i10 >= 4) {
                                                        AtomicReferenceArray atomicReferenceArray = new AtomicReferenceArray(r62[0].A01 >> 4);
                                                        AtomicReferenceArray atomicReferenceArray2 = new AtomicReferenceArray(r62[1].A01 >> 4);
                                                        String str3 = "";
                                                        for (int i11 = 4; i11 < i5; i11++) {
                                                            C90774Pe r11 = r62[i11];
                                                            if (r11.A00 == 4) {
                                                                ByteBuffer duplicate = slice.duplicate();
                                                                duplicate.position(r11.A02);
                                                                duplicate.limit(r11.A02 + r11.A01);
                                                                byte[] bArr = new byte[duplicate.remaining()];
                                                                duplicate.get(bArr);
                                                                str3 = new String(bArr, AnonymousClass3IR.A05);
                                                            }
                                                        }
                                                        r12 = new AnonymousClass3IR(str3, slice, atomicReferenceArray, atomicReferenceArray2, r62);
                                                        r8.A00 = r12;
                                                    }
                                                }
                                                throw new AnonymousClass5H7(C12960it.A0W(i10, "missing required section "));
                                            }
                                            throw new AnonymousClass5H7("less than required number of sections");
                                        }
                                        throw new AnonymousClass5H7("encoded size not enough for section headers");
                                    } else {
                                        throw new AnonymousClass5H7("encoded size is too small");
                                    }
                                } else {
                                    throw new AnonymousClass5H7("invalid magic or version");
                                }
                            } else {
                                throw C12960it.A0U("mUnparsed is null");
                            }
                        }
                        r3 = new C90024Mh(r12.A00(r7.A02), null);
                        r7.A01 = r3;
                    } else {
                        throw C12960it.A0U("The Lispy expression cannot be parsed");
                    }
                }
                try {
                    List<Object> unmodifiableList = Collections.unmodifiableList(r18.A00);
                    ThreadLocal threadLocal = C88514Fy.A00;
                    if (threadLocal.get() == null) {
                        z = true;
                        r6 = new AnonymousClass2DE();
                        threadLocal.set(r6);
                    } else {
                        z = false;
                        r6 = (AnonymousClass2DE) threadLocal.get();
                    }
                    try {
                        try {
                            C89094Iq r82 = new C89094Iq(r20);
                            int i12 = r6.A01;
                            r6.A0O(unmodifiableList.size() + 1 + 5);
                            r6.A0T(null);
                            for (Object obj : unmodifiableList) {
                                r6.A0T(obj);
                            }
                            r6.A0U(new C94724cR(new C1093751l(null, r3, -1)), null, unmodifiableList.size(), r6.A02);
                            C89094Iq r1 = r6.A03;
                            r6.A03 = r82;
                            try {
                                r6.A0L();
                                r6.A03 = r1;
                                Object obj2 = r6.A05[(r6.A01 - 1) - 0];
                                r6.A0M();
                                int i13 = r6.A01;
                                if (i13 != i12) {
                                    Object[] A1a = C12980iv.A1a();
                                    C12960it.A1P(A1a, i13, 0);
                                    C12960it.A1P(A1a, i12, 1);
                                    AnonymousClass2DE.A0A(String.format("Execution ended prematurely: stack size = %d, initial stack size = %d", A1a));
                                    throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                                }
                                if (z) {
                                    threadLocal.remove();
                                }
                                return obj2;
                            } catch (Throwable th) {
                                r6.A03 = r1;
                                throw th;
                            }
                        } catch (AnonymousClass5H7 e) {
                            if (z) {
                                throw new C87444Bn(e);
                            }
                            throw e;
                        }
                    } catch (AnonymousClass5H7 e2) {
                        throw e2;
                    } catch (RuntimeException e3) {
                        throw new AnonymousClass5H7(e3);
                    }
                } catch (C87444Bn e4) {
                    throw e4;
                }
            } else {
                throw C12990iw.A0m("ensureParsed expects OpaqueExpression");
            }
        } else {
            throw C12990iw.A0m("Interpreter.evaluate expects OpaqueExpression");
        }
    }
}
