package X;

import java.util.Collection;
import java.util.Comparator;
import java.util.SortedMap;
import java.util.SortedSet;

/* renamed from: X.3sz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80803sz extends AbstractC80933tC<K, V>.AsMap implements SortedMap<K, Collection<V>> {
    public SortedSet sortedKeySet;
    public final /* synthetic */ AbstractC80933tC this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80803sz(AbstractC80933tC r1, SortedMap sortedMap) {
        super(r1, sortedMap);
        this.this$0 = r1;
    }

    @Override // java.util.SortedMap
    public Comparator comparator() {
        return sortedMap().comparator();
    }

    public SortedSet createKeySet() {
        return new C80823t1(this.this$0, sortedMap());
    }

    @Override // java.util.SortedMap
    public Object firstKey() {
        return sortedMap().firstKey();
    }

    @Override // java.util.SortedMap
    public SortedMap headMap(Object obj) {
        return new C80803sz(this.this$0, sortedMap().headMap(obj));
    }

    @Override // java.util.SortedMap, java.util.Map
    public SortedSet keySet() {
        SortedSet sortedSet = this.sortedKeySet;
        if (sortedSet != null) {
            return sortedSet;
        }
        SortedSet createKeySet = createKeySet();
        this.sortedKeySet = createKeySet;
        return createKeySet;
    }

    @Override // java.util.SortedMap
    public Object lastKey() {
        return sortedMap().lastKey();
    }

    public SortedMap sortedMap() {
        return (SortedMap) this.submap;
    }

    @Override // java.util.SortedMap
    public SortedMap subMap(Object obj, Object obj2) {
        return new C80803sz(this.this$0, sortedMap().subMap(obj, obj2));
    }

    @Override // java.util.SortedMap
    public SortedMap tailMap(Object obj) {
        return new C80803sz(this.this$0, sortedMap().tailMap(obj));
    }
}
