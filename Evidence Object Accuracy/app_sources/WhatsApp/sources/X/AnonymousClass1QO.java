package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.1QO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1QO extends AnonymousClass1QJ {
    public final int A00;
    public final File A01;
    public final String A02 = "^lib/([^/]+)/([^/]+\\.so)$";

    public AnonymousClass1QO(Context context, File file, String str, int i) {
        super(context, str);
        this.A01 = file;
        this.A00 = i;
    }
}
