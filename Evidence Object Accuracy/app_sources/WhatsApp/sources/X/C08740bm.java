package X;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0bm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08740bm implements AbstractFutureC44231yX {
    public final AbstractC08750bn A00 = new C02500Cn(this);
    public final WeakReference A01;

    public C08740bm(C05380Pi r2) {
        this.A01 = new WeakReference(r2);
    }

    public void A00(Throwable th) {
        this.A00.A06(th);
    }

    public boolean A01(Object obj) {
        return this.A00.A07(obj);
    }

    @Override // X.AbstractFutureC44231yX
    public void A5i(Runnable runnable, Executor executor) {
        this.A00.A5i(runnable, executor);
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        C05380Pi r1 = (C05380Pi) this.A01.get();
        boolean cancel = this.A00.cancel(z);
        if (cancel && r1 != null) {
            r1.A00();
        }
        return cancel;
    }

    @Override // java.util.concurrent.Future
    public Object get() {
        return this.A00.get();
    }

    @Override // java.util.concurrent.Future
    public Object get(long j, TimeUnit timeUnit) {
        return this.A00.get(j, timeUnit);
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        return this.A00.isCancelled();
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        return this.A00.isDone();
    }

    @Override // java.lang.Object
    public String toString() {
        return this.A00.toString();
    }
}
