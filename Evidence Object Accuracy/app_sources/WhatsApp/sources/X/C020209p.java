package X;

import android.database.DataSetObserver;

/* renamed from: X.09p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020209p extends DataSetObserver {
    public final /* synthetic */ AnonymousClass0BR A00;

    public C020209p(AnonymousClass0BR r1) {
        this.A00 = r1;
    }

    @Override // android.database.DataSetObserver
    public void onChanged() {
        AnonymousClass0BR r1 = this.A00;
        r1.A07 = true;
        r1.notifyDataSetChanged();
    }

    @Override // android.database.DataSetObserver
    public void onInvalidated() {
        AnonymousClass0BR r1 = this.A00;
        r1.A07 = false;
        r1.notifyDataSetInvalidated();
    }
}
