package X;

/* renamed from: X.1Dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26471Dp {
    public final C15450nH A00;
    public final AnonymousClass01d A01;
    public final C14820m6 A02;
    public final C14850m9 A03;
    public final AnonymousClass155 A04;
    public final C16630pM A05;

    public C26471Dp(C15450nH r1, AnonymousClass01d r2, C14820m6 r3, C14850m9 r4, AnonymousClass155 r5, C16630pM r6) {
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A05 = r6;
    }

    public AnonymousClass1KA A00(byte b, boolean z) {
        Float A01;
        if (b != 1 && b != 23 && b != 37 && b != 42) {
            return null;
        }
        AnonymousClass1KA A012 = A01(z);
        if (z) {
            return A012;
        }
        C14850m9 r8 = this.A03;
        if (!r8.A07(700)) {
            return A012;
        }
        C15450nH r2 = this.A00;
        int A02 = r2.A02(AbstractC15460nI.A1R);
        int A022 = r2.A02(AbstractC15460nI.A1S);
        if (500 > A02 || A02 > 4000 || 20 > A022 || A022 > 100) {
            return A012;
        }
        int i = this.A02.A00.getInt("photo_quality", 0);
        if (!r8.A07(702) || i == 0) {
            if (!r8.A07(701)) {
                return A012;
            }
            if (AnonymousClass2BK.A02(this.A01, this.A05) < 2013 || (A01 = this.A04.A01(0)) == null) {
                return A012;
            }
            float floatValue = A01.floatValue();
            if (floatValue <= 20.0f || floatValue < ((float) r2.A02(AbstractC15460nI.A1Q))) {
                return A012;
            }
        } else if (i != 1) {
            return A012;
        }
        return new AnonymousClass1KA(r2.A02(AbstractC15460nI.A1U), A022, A02, A012.A00);
    }

    public final AnonymousClass1KA A01(boolean z) {
        int A02;
        C16140oW r0;
        C15450nH r1 = this.A00;
        int A022 = r1.A02(AbstractC15460nI.A1U);
        if (z) {
            A02 = r1.A02(AbstractC15460nI.A27);
            r0 = AbstractC15460nI.A26;
        } else {
            A02 = r1.A02(AbstractC15460nI.A1V);
            r0 = AbstractC15460nI.A1T;
        }
        int A023 = r1.A02(r0);
        return new AnonymousClass1KA(A022, A02, A023, A023);
    }
}
