package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.166  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass166 {
    public final Map A00 = new HashMap();

    public synchronized void A00(int i, int i2, int i3) {
        Map map = (Map) this.A00.get(Integer.valueOf(i));
        if (map != null) {
            Integer valueOf = Integer.valueOf(i2);
            if (!map.containsKey(valueOf)) {
                map.put(valueOf, Integer.valueOf(i3));
            }
        }
    }
}
