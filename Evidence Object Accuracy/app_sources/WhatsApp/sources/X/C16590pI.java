package X;

import android.content.Context;
import android.content.res.Resources;

/* renamed from: X.0pI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16590pI {
    public Context A00;

    public C16590pI(Context context) {
        this.A00 = context;
    }

    public static Resources A00(C16590pI r0) {
        return r0.A00.getResources();
    }

    public Context A01() {
        return this.A00;
    }

    public String A02(int i) {
        return this.A00.getResources().getString(i);
    }
}
