package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.4Ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90294Ni {
    public final float A00;
    public final List A01;

    public C90294Ni(List list, float f) {
        ArrayList A0l = C12960it.A0l();
        this.A01 = A0l;
        A0l.addAll(list);
        this.A00 = f;
    }
}
