package X;

import android.net.Uri;
import android.view.View;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import java.util.Set;

/* renamed from: X.36X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36X extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ AnonymousClass35V A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ Set A02;

    public AnonymousClass36X(AnonymousClass35V r1, String str, Set set) {
        this.A00 = r1;
        this.A02 = set;
        this.A01 = str;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        Set set = this.A02;
        AnonymousClass35V r3 = this.A00;
        String str = r3.A0B.A0z.A01;
        if (set == null) {
            AnonymousClass1BD.A0K.put(str, C12970iu.A0g());
            ((AbstractC33641ei) r3).A00.Ab9(r3.A02(), Uri.parse(this.A01));
            return;
        }
        AnonymousClass1BD.A0K.put(str, C12960it.A0V());
        ((ActivityC13810kN) AnonymousClass12P.A01(r3.A02(), ActivityC13810kN.class)).Adm(new SuspiciousLinkWarningDialogFragment(null, this.A01, str, set));
    }
}
