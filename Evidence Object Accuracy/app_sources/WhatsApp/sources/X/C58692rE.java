package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoImageView;

/* renamed from: X.2rE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58692rE extends AbstractC75643kB {
    public final float A00;
    public final AvatarProfilePhotoImageView A01;

    public C58692rE(View view) {
        super(view);
        this.A01 = (AvatarProfilePhotoImageView) view;
        this.A00 = view.getResources().getDimension(R.dimen.avatar_profile_photo_item_padding);
    }

    @Override // X.AbstractC75643kB
    public void A08(AbstractC87964Ds r6, AnonymousClass1J7 r7) {
        EnumC870249x r1;
        EnumC870249x r12;
        AbstractC83923y7 r62 = (AbstractC83923y7) r6;
        if (r62 instanceof C58702rF) {
            C58702rF r63 = (C58702rF) r62;
            boolean z = r63.A02;
            int intValue = Integer.valueOf(r63.A00).intValue();
            if (z) {
                r12 = EnumC870249x.A02;
            } else if (!z) {
                r12 = EnumC870249x.A01;
            } else {
                throw C12990iw.A0v();
            }
            AvatarProfilePhotoImageView avatarProfilePhotoImageView = this.A01;
            avatarProfilePhotoImageView.A06(r12, this.A00, intValue);
            View view = this.A0H;
            avatarProfilePhotoImageView.setAvatarPoseBackgroundColor(AnonymousClass00T.A00(view.getContext(), R.color.transparent));
            avatarProfilePhotoImageView.setImageBitmap(r63.A01);
            C12960it.A13(view, r7, r63, 4);
        } else if (r62 instanceof C58712rG) {
            C58712rG r64 = (C58712rG) r62;
            View view2 = this.A0H;
            int A00 = AnonymousClass00T.A00(view2.getContext(), R.color.wds_cool_gray_200);
            AvatarProfilePhotoImageView avatarProfilePhotoImageView2 = this.A01;
            avatarProfilePhotoImageView2.setAvatarPoseBackgroundColor(A00);
            Integer num = r64.A00;
            if (num != null) {
                boolean z2 = r64.A01;
                int intValue2 = num.intValue();
                if (z2) {
                    r1 = EnumC870249x.A02;
                } else if (!z2) {
                    r1 = EnumC870249x.A01;
                } else {
                    throw C12990iw.A0v();
                }
                avatarProfilePhotoImageView2.A06(r1, this.A00, intValue2);
            } else {
                avatarProfilePhotoImageView2.A04();
            }
            view2.setOnClickListener(null);
        }
    }
}
