package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.view.View;

/* renamed from: X.3gf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73703gf extends View {
    public final /* synthetic */ AbstractC64383Fi A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C73703gf(Context context, AbstractC64383Fi r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        AbstractC64383Fi r0 = this.A00;
        Path A00 = r0.A00();
        if (A00 != null) {
            canvas.drawPath(A00, r0.A02);
        }
    }
}
