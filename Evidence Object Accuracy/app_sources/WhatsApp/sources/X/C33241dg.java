package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1dg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33241dg {
    public final int A00 = 1;
    public final C21630xj A01;
    public final List A02;

    public C33241dg(C21630xj r3) {
        this.A01 = r3;
        ArrayList arrayList = new ArrayList();
        this.A02 = arrayList;
        arrayList.add("call_log");
        arrayList.add("labeled_jid");
        arrayList.add("message_fts");
        arrayList.add("blank_me_jid");
        arrayList.add("message_link");
        arrayList.add("message_main");
        arrayList.add("message_text");
        arrayList.add("missed_calls");
        arrayList.add("receipt_user");
        arrayList.add("message_media");
        arrayList.add("message_vcard");
        arrayList.add("message_future");
        arrayList.add("message_quoted");
        arrayList.add("message_system");
        arrayList.add("receipt_device");
        arrayList.add("message_mention");
        arrayList.add("message_revoked");
        arrayList.add("broadcast_me_jid");
        arrayList.add("message_frequent");
        arrayList.add("message_location");
        arrayList.add("participant_user");
        arrayList.add("message_thumbnail");
        arrayList.add("message_send_count");
        arrayList.add("migration_jid_store");
        arrayList.add("payment_transaction");
        arrayList.add("migration_chat_store");
        arrayList.add("quoted_order_message");
        arrayList.add("media_migration_fixer");
        arrayList.add("quoted_order_message_v2");
        arrayList.add("message_main_verification");
        arrayList.add("quoted_ui_elements_reply_message");
        arrayList.add("alter_message_ephemeral_to_message_ephemeral_remove_column");
        arrayList.add("alter_message_ephemeral_setting_to_message_ephemeral_setting_remove_column");
    }

    public static final boolean A00(C21630xj r0, String str) {
        AbstractC18500sY A01 = r0.A01(str);
        if (A01 == null || A01.A04() != 1) {
            return false;
        }
        return true;
    }

    public final int A01(Map map) {
        boolean z;
        loop0: while (true) {
            z = true;
            for (Boolean bool : map.values()) {
                if (!z || !bool.booleanValue()) {
                    z = false;
                }
            }
        }
        if (z) {
            return this.A00;
        }
        return 0;
    }

    public final Map A02() {
        HashMap hashMap = new HashMap();
        C21630xj r3 = this.A01;
        hashMap.put("call_log", Boolean.valueOf(A00(r3, "call_log")));
        hashMap.put("labeled_jid", Boolean.valueOf(A00(r3, "labeled_jid")));
        hashMap.put("message_fts", Boolean.valueOf(A00(r3, "message_fts")));
        hashMap.put("blank_me_jid", Boolean.valueOf(A00(r3, "blank_me_jid")));
        hashMap.put("message_link", Boolean.valueOf(A00(r3, "message_link")));
        hashMap.put("message_main", Boolean.valueOf(A00(r3, "message_main")));
        hashMap.put("message_text", Boolean.valueOf(A00(r3, "message_text")));
        hashMap.put("missed_calls", Boolean.valueOf(A00(r3, "missed_calls")));
        hashMap.put("receipt_user", Boolean.valueOf(A00(r3, "receipt_user")));
        hashMap.put("message_media", Boolean.valueOf(A00(r3, "message_media")));
        hashMap.put("message_vcard", Boolean.valueOf(A00(r3, "message_vcard")));
        hashMap.put("message_future", Boolean.valueOf(A00(r3, "message_future")));
        hashMap.put("message_quoted", Boolean.valueOf(A00(r3, "message_quoted")));
        hashMap.put("message_system", Boolean.valueOf(A00(r3, "message_system")));
        hashMap.put("receipt_device", Boolean.valueOf(A00(r3, "receipt_device")));
        hashMap.put("message_mention", Boolean.valueOf(A00(r3, "message_mention")));
        hashMap.put("message_revoked", Boolean.valueOf(A00(r3, "message_revoked")));
        hashMap.put("broadcast_me_jid", Boolean.valueOf(A00(r3, "broadcast_me_jid")));
        hashMap.put("message_frequent", Boolean.valueOf(A00(r3, "message_frequent")));
        hashMap.put("message_location", Boolean.valueOf(A00(r3, "message_location")));
        hashMap.put("participant_user", Boolean.valueOf(A00(r3, "participant_user")));
        hashMap.put("message_thumbnail", Boolean.valueOf(A00(r3, "message_thumbnail")));
        hashMap.put("message_send_count", Boolean.valueOf(A00(r3, "message_send_count")));
        hashMap.put("migration_jid_store", Boolean.valueOf(A00(r3, "migration_jid_store")));
        hashMap.put("payment_transaction", Boolean.valueOf(A00(r3, "payment_transaction")));
        hashMap.put("migration_chat_store", Boolean.valueOf(A00(r3, "migration_chat_store")));
        hashMap.put("quoted_order_message", Boolean.valueOf(A00(r3, "quoted_order_message")));
        hashMap.put("media_migration_fixer", Boolean.valueOf(A00(r3, "media_migration_fixer")));
        hashMap.put("quoted_order_message_v2", Boolean.valueOf(A00(r3, "quoted_order_message_v2")));
        hashMap.put("message_main_verification", Boolean.valueOf(A00(r3, "message_main_verification")));
        hashMap.put("quoted_ui_elements_reply_message", Boolean.valueOf(A00(r3, "quoted_ui_elements_reply_message")));
        Boolean bool = Boolean.TRUE;
        hashMap.put("alter_message_ephemeral_to_message_ephemeral_remove_column", bool);
        hashMap.put("alter_message_ephemeral_setting_to_message_ephemeral_setting_remove_column", bool);
        return hashMap;
    }

    public void A03(C44621zH r6) {
        Map A02 = A02();
        int A01 = A01(A02);
        r6.A03();
        C44631zI r1 = (C44631zI) r6.A00;
        r1.A01 |= 8;
        r1.A00 = A01;
        Object obj = A02.get("call_log");
        AnonymousClass009.A05(obj);
        boolean booleanValue = ((Boolean) obj).booleanValue();
        r6.A03();
        C44631zI r12 = (C44631zI) r6.A00;
        r12.A01 |= 16;
        r12.A0B = booleanValue;
        Object obj2 = A02.get("labeled_jid");
        AnonymousClass009.A05(obj2);
        boolean booleanValue2 = ((Boolean) obj2).booleanValue();
        r6.A03();
        C44631zI r13 = (C44631zI) r6.A00;
        r13.A01 |= 32;
        r13.A0C = booleanValue2;
        Object obj3 = A02.get("message_fts");
        AnonymousClass009.A05(obj3);
        boolean booleanValue3 = ((Boolean) obj3).booleanValue();
        r6.A03();
        C44631zI r14 = (C44631zI) r6.A00;
        r14.A01 |= 64;
        r14.A0F = booleanValue3;
        Object obj4 = A02.get("blank_me_jid");
        AnonymousClass009.A05(obj4);
        boolean booleanValue4 = ((Boolean) obj4).booleanValue();
        r6.A03();
        C44631zI r15 = (C44631zI) r6.A00;
        r15.A01 |= 128;
        r15.A09 = booleanValue4;
        Object obj5 = A02.get("message_link");
        AnonymousClass009.A05(obj5);
        boolean booleanValue5 = ((Boolean) obj5).booleanValue();
        r6.A03();
        C44631zI r16 = (C44631zI) r6.A00;
        r16.A01 |= 256;
        r16.A0H = booleanValue5;
        Object obj6 = A02.get("message_main");
        AnonymousClass009.A05(obj6);
        boolean booleanValue6 = ((Boolean) obj6).booleanValue();
        r6.A03();
        C44631zI r17 = (C44631zI) r6.A00;
        r17.A01 |= 512;
        r17.A0J = booleanValue6;
        Object obj7 = A02.get("message_text");
        AnonymousClass009.A05(obj7);
        boolean booleanValue7 = ((Boolean) obj7).booleanValue();
        r6.A03();
        C44631zI r18 = (C44631zI) r6.A00;
        r18.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
        r18.A0R = booleanValue7;
        Object obj8 = A02.get("missed_calls");
        AnonymousClass009.A05(obj8);
        boolean booleanValue8 = ((Boolean) obj8).booleanValue();
        r6.A03();
        C44631zI r19 = (C44631zI) r6.A00;
        r19.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        r19.A0W = booleanValue8;
        Object obj9 = A02.get("receipt_user");
        AnonymousClass009.A05(obj9);
        boolean booleanValue9 = ((Boolean) obj9).booleanValue();
        r6.A03();
        C44631zI r110 = (C44631zI) r6.A00;
        r110.A01 |= 4096;
        r110.A0d = booleanValue9;
        Object obj10 = A02.get("message_media");
        AnonymousClass009.A05(obj10);
        boolean booleanValue10 = ((Boolean) obj10).booleanValue();
        r6.A03();
        C44631zI r111 = (C44631zI) r6.A00;
        r111.A01 |= DefaultCrypto.BUFFER_SIZE;
        r111.A0L = booleanValue10;
        Object obj11 = A02.get("message_vcard");
        AnonymousClass009.A05(obj11);
        boolean booleanValue11 = ((Boolean) obj11).booleanValue();
        r6.A03();
        C44631zI r112 = (C44631zI) r6.A00;
        r112.A01 |= 16384;
        r112.A0T = booleanValue11;
        Object obj12 = A02.get("message_future");
        AnonymousClass009.A05(obj12);
        boolean booleanValue12 = ((Boolean) obj12).booleanValue();
        r6.A03();
        C44631zI r2 = (C44631zI) r6.A00;
        r2.A01 |= 32768;
        r2.A0G = booleanValue12;
        Object obj13 = A02.get("message_quoted");
        AnonymousClass009.A05(obj13);
        boolean booleanValue13 = ((Boolean) obj13).booleanValue();
        r6.A03();
        C44631zI r22 = (C44631zI) r6.A00;
        r22.A01 |= 65536;
        r22.A0N = booleanValue13;
        Object obj14 = A02.get("message_system");
        AnonymousClass009.A05(obj14);
        boolean booleanValue14 = ((Boolean) obj14).booleanValue();
        r6.A03();
        C44631zI r23 = (C44631zI) r6.A00;
        r23.A01 |= C25981Bo.A0F;
        r23.A0Q = booleanValue14;
        Object obj15 = A02.get("receipt_device");
        AnonymousClass009.A05(obj15);
        boolean booleanValue15 = ((Boolean) obj15).booleanValue();
        r6.A03();
        C44631zI r24 = (C44631zI) r6.A00;
        r24.A01 |= 262144;
        r24.A0c = booleanValue15;
        Object obj16 = A02.get("message_mention");
        AnonymousClass009.A05(obj16);
        boolean booleanValue16 = ((Boolean) obj16).booleanValue();
        r6.A03();
        C44631zI r25 = (C44631zI) r6.A00;
        r25.A01 |= 524288;
        r25.A0M = booleanValue16;
        Object obj17 = A02.get("message_revoked");
        AnonymousClass009.A05(obj17);
        boolean booleanValue17 = ((Boolean) obj17).booleanValue();
        r6.A03();
        C44631zI r26 = (C44631zI) r6.A00;
        r26.A01 |= 1048576;
        r26.A0O = booleanValue17;
        Object obj18 = A02.get("broadcast_me_jid");
        AnonymousClass009.A05(obj18);
        boolean booleanValue18 = ((Boolean) obj18).booleanValue();
        r6.A03();
        C44631zI r27 = (C44631zI) r6.A00;
        r27.A01 |= 2097152;
        r27.A0A = booleanValue18;
        Object obj19 = A02.get("message_frequent");
        AnonymousClass009.A05(obj19);
        boolean booleanValue19 = ((Boolean) obj19).booleanValue();
        r6.A03();
        C44631zI r28 = (C44631zI) r6.A00;
        r28.A01 |= 4194304;
        r28.A0E = booleanValue19;
        Object obj20 = A02.get("message_location");
        AnonymousClass009.A05(obj20);
        boolean booleanValue20 = ((Boolean) obj20).booleanValue();
        r6.A03();
        C44631zI r29 = (C44631zI) r6.A00;
        r29.A01 |= 8388608;
        r29.A0I = booleanValue20;
        Object obj21 = A02.get("participant_user");
        AnonymousClass009.A05(obj21);
        boolean booleanValue21 = ((Boolean) obj21).booleanValue();
        r6.A03();
        C44631zI r210 = (C44631zI) r6.A00;
        r210.A01 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
        r210.A0X = booleanValue21;
        Object obj22 = A02.get("message_thumbnail");
        AnonymousClass009.A05(obj22);
        boolean booleanValue22 = ((Boolean) obj22).booleanValue();
        r6.A03();
        C44631zI r211 = (C44631zI) r6.A00;
        r211.A01 |= 33554432;
        r211.A0S = booleanValue22;
        Object obj23 = A02.get("message_send_count");
        AnonymousClass009.A05(obj23);
        boolean booleanValue23 = ((Boolean) obj23).booleanValue();
        r6.A03();
        C44631zI r212 = (C44631zI) r6.A00;
        r212.A01 |= 67108864;
        r212.A0P = booleanValue23;
        Object obj24 = A02.get("migration_jid_store");
        AnonymousClass009.A05(obj24);
        boolean booleanValue24 = ((Boolean) obj24).booleanValue();
        r6.A03();
        C44631zI r213 = (C44631zI) r6.A00;
        r213.A01 |= 134217728;
        r213.A0V = booleanValue24;
        Object obj25 = A02.get("payment_transaction");
        AnonymousClass009.A05(obj25);
        boolean booleanValue25 = ((Boolean) obj25).booleanValue();
        r6.A03();
        C44631zI r214 = (C44631zI) r6.A00;
        r214.A01 |= 268435456;
        r214.A0Y = booleanValue25;
        Object obj26 = A02.get("migration_chat_store");
        AnonymousClass009.A05(obj26);
        boolean booleanValue26 = ((Boolean) obj26).booleanValue();
        r6.A03();
        C44631zI r215 = (C44631zI) r6.A00;
        r215.A01 |= 536870912;
        r215.A0U = booleanValue26;
        Object obj27 = A02.get("quoted_order_message");
        AnonymousClass009.A05(obj27);
        boolean booleanValue27 = ((Boolean) obj27).booleanValue();
        r6.A03();
        C44631zI r216 = (C44631zI) r6.A00;
        r216.A01 |= 1073741824;
        r216.A0Z = booleanValue27;
        Object obj28 = A02.get("media_migration_fixer");
        AnonymousClass009.A05(obj28);
        boolean booleanValue28 = ((Boolean) obj28).booleanValue();
        r6.A03();
        C44631zI r217 = (C44631zI) r6.A00;
        r217.A01 |= Integer.MIN_VALUE;
        r217.A0D = booleanValue28;
        Object obj29 = A02.get("quoted_order_message_v2");
        AnonymousClass009.A05(obj29);
        boolean booleanValue29 = ((Boolean) obj29).booleanValue();
        r6.A03();
        C44631zI r113 = (C44631zI) r6.A00;
        r113.A02 |= 1;
        r113.A0a = booleanValue29;
        Object obj30 = A02.get("message_main_verification");
        AnonymousClass009.A05(obj30);
        boolean booleanValue30 = ((Boolean) obj30).booleanValue();
        r6.A03();
        C44631zI r114 = (C44631zI) r6.A00;
        r114.A02 |= 2;
        r114.A0K = booleanValue30;
        Object obj31 = A02.get("quoted_ui_elements_reply_message");
        AnonymousClass009.A05(obj31);
        boolean booleanValue31 = ((Boolean) obj31).booleanValue();
        r6.A03();
        C44631zI r115 = (C44631zI) r6.A00;
        r115.A02 |= 4;
        r115.A0b = booleanValue31;
        r6.A03();
        C44631zI r116 = (C44631zI) r6.A00;
        r116.A02 |= 8;
        r116.A08 = true;
        r6.A03();
        C44631zI r117 = (C44631zI) r6.A00;
        r117.A02 |= 16;
        r117.A07 = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:127:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x01ea A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(java.io.File r7) {
        /*
        // Method dump skipped, instructions count: 626
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33241dg.A04(java.io.File):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004f A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A05(org.json.JSONObject r7) {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x0015
            java.lang.String r1 = "backup_version"
            boolean r0 = r7.has(r1)
            if (r0 == 0) goto L_0x0015
            int r5 = r7.getInt(r1)     // Catch: JSONException -> 0x000f
            goto L_0x0016
        L_0x000f:
            r1 = move-exception
            java.lang.String r0 = "BackupExpiryManager/getBackupVersion/failed to parse version from json"
            com.whatsapp.util.Log.w(r0, r1)
        L_0x0015:
            r5 = 0
        L_0x0016:
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.List r0 = r6.A02
            java.util.Iterator r3 = r0.iterator()
        L_0x0021:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0045
            java.lang.Object r2 = r3.next()
            java.lang.String r2 = (java.lang.String) r2
            if (r7 == 0) goto L_0x003c
            boolean r0 = r7.has(r2)     // Catch: JSONException -> 0x003c
            if (r0 == 0) goto L_0x003c
            boolean r1 = r7.getBoolean(r2)     // Catch: JSONException -> 0x003c
            r0 = 1
            if (r1 != 0) goto L_0x003d
        L_0x003c:
            r0 = 0
        L_0x003d:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r4.put(r2, r0)
            goto L_0x0021
        L_0x0045:
            r0 = 0
            if (r5 >= r0) goto L_0x004f
            java.lang.String r0 = "BackupExpiryManager/backup expired based on version"
            com.whatsapp.util.Log.w(r0)
            r0 = 0
            return r0
        L_0x004f:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33241dg.A05(org.json.JSONObject):boolean");
    }
}
