package X;

/* renamed from: X.55u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1105055u implements AbstractC32851cq {
    public final /* synthetic */ AnonymousClass10D A00;
    public final /* synthetic */ long[] A01;

    public C1105055u(AnonymousClass10D r1, long[] jArr) {
        this.A00 = r1;
        this.A01 = jArr;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        this.A01[0] = -2;
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        this.A01[0] = -2;
    }
}
