package X;

/* renamed from: X.4VE  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4VE {
    public int A00;
    public final C95304dT A01 = C95304dT.A05(8);

    public final long A00(AnonymousClass5Yf r9) {
        C95304dT r7 = this.A01;
        int i = 0;
        r9.AZ4(r7.A02, 0, 1);
        byte[] bArr = r7.A02;
        int i2 = bArr[0] & 255;
        if (i2 == 0) {
            return Long.MIN_VALUE;
        }
        int i3 = 128;
        int i4 = 0;
        while ((i2 & i3) == 0) {
            i3 >>= 1;
            i4++;
        }
        int i5 = i2 & (i3 ^ -1);
        r9.AZ4(bArr, 1, i4);
        while (i < i4) {
            i++;
            i5 = (r7.A02[i] & 255) + (i5 << 8);
        }
        this.A00 += i4 + 1;
        return (long) i5;
    }
}
