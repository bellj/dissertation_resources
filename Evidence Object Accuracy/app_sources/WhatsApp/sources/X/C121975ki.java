package X;

import android.view.View;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.5ki  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121975ki extends AbstractC118845cT {
    public C121975ki(View view) {
        super(view);
    }

    @Override // X.AbstractC118845cT
    public void A08(AbstractC128945wv r7) {
        String A0t;
        String A0t2;
        super.A08(r7);
        C121955kg r72 = (C121955kg) r7;
        TextEmojiLabel textEmojiLabel = ((AbstractC118845cT) this).A02;
        C1315763h r5 = r72.A01;
        C247116o r4 = r72.A00;
        String str = r5.A05;
        String str2 = r5.A04;
        C30081Wa A00 = r4.A00();
        if (!(A00 == null || (A0t2 = C12970iu.A0t(str, A00.A02)) == null)) {
            str2 = A0t2;
        }
        textEmojiLabel.setText(str2);
        TextEmojiLabel textEmojiLabel2 = ((AbstractC118845cT) this).A01;
        String str3 = r5.A03;
        String str4 = r5.A02;
        C30081Wa A002 = r4.A00();
        if (!(A002 == null || (A0t = C12970iu.A0t(str3, A002.A02)) == null)) {
            str4 = A0t;
        }
        textEmojiLabel2.setText(str4);
    }
}
