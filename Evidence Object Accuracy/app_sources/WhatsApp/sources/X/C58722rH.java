package X;

/* renamed from: X.2rH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58722rH extends AbstractC87964Ds {
    public final int A00;
    public final int A01;
    public final boolean A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C58722rH) {
                C58722rH r5 = (C58722rH) obj;
                if (!(this.A00 == r5.A00 && this.A01 == r5.A01 && this.A02 == r5.A02)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = ((this.A00 * 31) + this.A01) * 31;
        boolean z = this.A02;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return i + i2;
    }

    public C58722rH(int i, int i2, boolean z) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = z;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AvatarBackground(backgroundColor=");
        A0k.append(this.A00);
        A0k.append(", ringColor=");
        A0k.append(this.A01);
        A0k.append(", isSelected=");
        A0k.append(this.A02);
        return C12970iu.A0u(A0k);
    }
}
