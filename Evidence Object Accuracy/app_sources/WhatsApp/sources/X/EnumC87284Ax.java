package X;

import com.whatsapp.R;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87284Ax extends Enum {
    public static final /* synthetic */ EnumC87284Ax[] A00;
    public static final EnumC87284Ax A01;
    public static final EnumC87284Ax A02;
    public final int statusColor;

    public static EnumC87284Ax valueOf(String str) {
        return (EnumC87284Ax) Enum.valueOf(EnumC87284Ax.class, str);
    }

    public static EnumC87284Ax[] values() {
        return (EnumC87284Ax[]) A00.clone();
    }

    static {
        EnumC87284Ax r4 = new EnumC87284Ax(0, "NEW", R.color.wds_green_400);
        A01 = r4;
        EnumC87284Ax r1 = new EnumC87284Ax(1, "VIEWED", R.color.wds_cool_gray_300);
        A02 = r1;
        EnumC87284Ax[] r0 = new EnumC87284Ax[2];
        C72453ed.A1J(r4, r1, r0);
        A00 = r0;
    }

    public EnumC87284Ax(int i, String str, int i2) {
        this.statusColor = i2;
    }
}
