package X;

import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import java.math.BigDecimal;

/* renamed from: X.5oM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124045oM extends AbstractC16350or {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C124045oM(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        super(indiaUpiSendPaymentActivity);
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m.A04("Verifying VPA in background...");
        indiaUpiSendPaymentActivity.A00 = 1;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0N.A00(((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08, null, new AnonymousClass692(indiaUpiSendPaymentActivity, new Runnable() { // from class: X.6GH
            @Override // java.lang.Runnable
            public final void run() {
                BigDecimal bigDecimal;
                IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity2 = C124045oM.this.A00;
                indiaUpiSendPaymentActivity2.A00 = 5;
                indiaUpiSendPaymentActivity2.A3N();
                if (!((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0i && (bigDecimal = indiaUpiSendPaymentActivity2.A06) != null) {
                    indiaUpiSendPaymentActivity2.A04.A00 = ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity2).A0X.A00(String.valueOf(bigDecimal), ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0k, ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity2).A0i);
                }
                indiaUpiSendPaymentActivity2.A3b();
            }
        }));
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m.A04("Background VPA verification done.");
        indiaUpiSendPaymentActivity.A01 = null;
    }
}
