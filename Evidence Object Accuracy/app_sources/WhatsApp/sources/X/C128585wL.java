package X;

import java.util.HashMap;

/* renamed from: X.5wL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128585wL {
    public final /* synthetic */ AnonymousClass3FE A00;

    public /* synthetic */ C128585wL(AnonymousClass3FE r1) {
        this.A00 = r1;
    }

    public final void A00(C452120p r4) {
        AnonymousClass3FE r2 = this.A00;
        if (r4 == null) {
            C117315Zl.A0T(r2);
            return;
        }
        HashMap A11 = C12970iu.A11();
        C117295Zj.A1D(r4, A11);
        r2.A01("on_failure", A11);
    }
}
