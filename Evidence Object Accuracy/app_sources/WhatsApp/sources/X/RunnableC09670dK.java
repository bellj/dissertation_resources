package X;

/* renamed from: X.0dK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09670dK implements Runnable {
    public final /* synthetic */ RunnableC10230eF A00;
    public final /* synthetic */ AnonymousClass040 A01;

    public RunnableC09670dK(RunnableC10230eF r1, AnonymousClass040 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A01.A08(this.A00.A02.A00());
    }
}
