package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4wn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107104wn implements AnonymousClass5X7 {
    public int A00;
    public int A01;
    public long A02;
    public boolean A03;
    public final List A04;
    public final AnonymousClass5X6[] A05;

    public C107104wn(List list) {
        this.A04 = list;
        this.A05 = new AnonymousClass5X6[list.size()];
    }

    @Override // X.AnonymousClass5X7
    public void A7a(C95304dT r7) {
        boolean z = this.A03;
        boolean z2 = z;
        if (z) {
            int i = this.A00;
            if (i == 2) {
                if (C95304dT.A00(r7) != 0) {
                    if (r7.A0C() != 32) {
                        this.A03 = false;
                        z = false;
                        z2 = false;
                    }
                    i = 1;
                    this.A00 = 1;
                    if (!z) {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (i == 1) {
                if (C95304dT.A00(r7) != 0) {
                    if (r7.A0C() != 0) {
                        this.A03 = false;
                        z2 = false;
                    }
                    this.A00 = i - 1;
                    if (!z2) {
                        return;
                    }
                } else {
                    return;
                }
            }
            int i2 = r7.A01;
            int i3 = r7.A00 - i2;
            AnonymousClass5X6[] r2 = this.A05;
            for (AnonymousClass5X6 r0 : r2) {
                r7.A0S(i2);
                r0.AbC(r7, i3);
            }
            this.A01 += i3;
        }
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r7, C92824Xo r8) {
        int i = 0;
        while (true) {
            AnonymousClass5X6[] r4 = this.A05;
            if (i < r4.length) {
                AnonymousClass4M0 r3 = (AnonymousClass4M0) this.A04.get(i);
                r8.A03();
                AnonymousClass5X6 Af4 = r7.Af4(r8.A01(), 3);
                C93844ap A00 = C93844ap.A00();
                A00.A0O = r8.A02();
                A00.A0R = "application/dvbsubs";
                A00.A0S = Collections.singletonList(r3.A01);
                A00.A0Q = r3.A00;
                C72453ed.A17(A00, Af4);
                r4[i] = Af4;
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass5X7
    public void AYo() {
        if (this.A03) {
            for (AnonymousClass5X6 r3 : this.A05) {
                r3.AbG(null, 1, this.A01, 0, this.A02);
            }
            this.A03 = false;
        }
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        if ((i & 4) != 0) {
            this.A03 = true;
            this.A02 = j;
            this.A01 = 0;
            this.A00 = 2;
        }
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A03 = false;
    }
}
