package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.community.CommunityTabViewModel;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.42W  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42W extends AbstractC18860tB {
    public final /* synthetic */ CommunityTabViewModel A00;

    public AnonymousClass42W(CommunityTabViewModel communityTabViewModel) {
        this.A00 = communityTabViewModel;
    }

    @Override // X.AbstractC18860tB
    public void A01(AbstractC15340mz r5, int i) {
        GroupJid groupJid;
        if ((r5 instanceof AnonymousClass1Y3) && (groupJid = ((AnonymousClass1Y3) r5).A01) != null) {
            this.A00.A0O.execute(new RunnableBRunnable0Shape11S0200000_I1_1(this, 9, groupJid));
        }
    }
}
