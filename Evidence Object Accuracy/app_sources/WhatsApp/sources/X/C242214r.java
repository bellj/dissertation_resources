package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.14r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242214r {
    public final C16510p9 A00;

    public C242214r(C16510p9 r1) {
        this.A00 = r1;
    }

    public static String A00(int[] iArr) {
        int length = iArr.length;
        if (length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder("message_type");
        sb.append(" NOT IN (");
        sb.append(iArr[0]);
        for (int i = 1; i < length; i++) {
            sb.append(",");
            sb.append(iArr[i]);
        }
        sb.append(")");
        return sb.toString();
    }

    public static void A01(String str, StringBuilder sb, boolean z, boolean z2) {
        String str2;
        if ("document".equals(str)) {
            str2 = C32301bw.A07;
        } else if ("url".equals(str)) {
            if (z2) {
                str2 = C32311bx.A00;
            } else {
                str2 = C32321by.A01;
            }
        } else if ("all_media".equals(str)) {
            str2 = C32331bz.A08;
        } else {
            sb.append(C32301bw.A0K);
            A03(sb, z);
            if (str != null) {
                StringBuilder sb2 = new StringBuilder("sqlStatementsBuilder/getStartSqlForType/unexpected type string=");
                sb2.append(str);
                Log.w(sb2.toString());
                return;
            }
            return;
        }
        sb.append(str2);
    }

    public static void A02(StringBuilder sb, boolean z) {
        String str;
        if (z) {
            sb.append(" AND sort_id < ?");
            str = " ORDER BY sort_id DESC";
        } else {
            sb.append(" AND sort_id > ?");
            str = " ORDER BY sort_id ASC";
        }
        sb.append(str);
    }

    public static void A03(StringBuilder sb, boolean z) {
        String str;
        if (z) {
            sb.append(" AND message_type != '");
            sb.append(8);
            str = "' ";
        } else {
            str = " ";
        }
        sb.append(str);
    }

    public AnonymousClass01T A04(AbstractC15340mz r11) {
        String str;
        ArrayList arrayList = new ArrayList();
        List<AbstractC14640lm> A0R = r11.A0R();
        if (A0R == null || A0R.size() < r11.A0A) {
            arrayList.add(String.valueOf(r11.A0I));
            arrayList.add(r11.A0z.A01);
            arrayList.add(String.valueOf(r11.A11));
            str = " WHERE timestamp = ? AND from_me = 1 AND key_id = ? AND _id!=?";
        } else {
            arrayList.add(r11.A0z.A01);
            C16510p9 r9 = this.A00;
            HashMap hashMap = new HashMap();
            for (AbstractC14640lm r1 : A0R) {
                long A02 = r9.A02(r1);
                if (A02 != -1) {
                    hashMap.put(r1, Long.valueOf(A02));
                }
            }
            StringBuilder sb = new StringBuilder(" WHERE from_me=1 AND key_id=? AND chat_row_id IN ");
            sb.append(AnonymousClass1Ux.A00(hashMap.size()));
            str = sb.toString();
            for (Object obj : hashMap.values()) {
                arrayList.add(obj.toString());
            }
        }
        return new AnonymousClass01T(str, arrayList);
    }
}
