package X;

/* renamed from: X.24m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C461024m extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;

    public C461024m() {
        super(1536, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A00);
        r3.Abe(1, this.A01);
        r3.Abe(7, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamCamera {");
        String str = null;
        Integer num = this.A00;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "cameraOrigin", str);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "cameraPresentationT", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "cameraViewType", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
