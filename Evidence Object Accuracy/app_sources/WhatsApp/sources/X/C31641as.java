package X;

/* renamed from: X.1as  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31641as {
    public final C31491ad A00;

    public C31641as(C31491ad r1) {
        this.A00 = r1;
    }

    public C31641as(byte[] bArr) {
        this.A00 = C31481ac.A00(bArr);
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C31641as)) {
            return false;
        }
        return this.A00.equals(((C31641as) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
