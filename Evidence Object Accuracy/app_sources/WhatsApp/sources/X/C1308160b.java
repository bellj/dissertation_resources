package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.60b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1308160b {
    public static final char[] A0D = "0123456789abcdef".toCharArray();
    public int A00 = 0;
    public SharedPreferences A01;
    public C127385uP A02;
    public C129815yL A03;
    public C129995yd A04;
    public C130005ye A05;
    public KeyPair A06;
    public Map A07 = Collections.synchronizedMap(C12970iu.A11());
    public boolean A08;
    public final C16590pI A09;
    public final AnonymousClass61E A0A;
    public final C16630pM A0B;
    public final HashMap A0C = C12970iu.A11();

    public C1308160b(C16590pI r2, AnonymousClass61E r3, C16630pM r4) {
        this.A0B = r4;
        this.A09 = r2;
        this.A0A = r3;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A01;
        if (sharedPreferences == null) {
            sharedPreferences = this.A0B.A01("novi.key");
            this.A01 = sharedPreferences;
        }
        AnonymousClass009.A05(sharedPreferences);
        return sharedPreferences;
    }

    public final KeyPair A01(String str) {
        A05();
        if (this.A05 == null) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        instance.add(10, -24);
        Calendar instance2 = Calendar.getInstance();
        instance2.add(10, 2160);
        return this.A05.A02(str, instance, instance2);
    }

    public final KeyPair A02(String str) {
        KeyPair A03;
        if (Build.VERSION.SDK_INT >= 23 && (A03 = A03(str)) != null) {
            return A03;
        }
        A05();
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(".");
        String A0d = C12960it.A0d("public", A0j);
        StringBuilder A0j2 = C12960it.A0j(str);
        A0j2.append(".");
        String A0d2 = C12960it.A0d("private", A0j2);
        C129815yL r0 = this.A03;
        AnonymousClass009.A05(r0);
        byte[] A032 = r0.A03(A0d, A0d);
        byte[] A033 = this.A03.A03(A0d2, A0d2);
        KeyPair keyPair = null;
        if (!(A032 == null || A033 == null)) {
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(A032);
            PKCS8EncodedKeySpec pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(A033);
            try {
                KeyFactory instance = KeyFactory.getInstance("EC");
                keyPair = new KeyPair(instance.generatePublic(x509EncodedKeySpec), instance.generatePrivate(pKCS8EncodedKeySpec));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException unused) {
                Log.e("PAY: NoviKeyStore/retrieveSigningKeyFromSharedPreference can't retrieve key pair");
            }
            Arrays.fill(A032, (byte) 0);
            Arrays.fill(A033, (byte) 0);
        }
        return keyPair;
    }

    public final KeyPair A03(String str) {
        A05();
        C130005ye r0 = this.A05;
        if (r0 != null) {
            return r0.A01(str);
        }
        return null;
    }

    public final KeyPair A04(String str, boolean z) {
        KeyPair keyPair;
        A05();
        if (!z) {
            Map map = this.A07;
            if (map.get(str) != null) {
                return (KeyPair) map.get(str);
            }
            keyPair = A02(str);
            if (keyPair != null) {
                map.put(str, keyPair);
                return keyPair;
            }
        }
        if (Build.VERSION.SDK_INT < 23 || (keyPair = A01(str)) == null) {
            try {
                keyPair = C117305Zk.A0o();
            } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException unused) {
                Log.e("PAY: NoviKeyStore/createInMemorySigningKeyPair can't create signing key");
                keyPair = null;
            }
            if (keyPair != null) {
                A05();
                byte[] encoded = keyPair.getPublic().getEncoded();
                byte[] encoded2 = keyPair.getPrivate().getEncoded();
                StringBuilder A0j = C12960it.A0j(str);
                A0j.append(".");
                String A0d = C12960it.A0d("public", A0j);
                StringBuilder A0j2 = C12960it.A0j(str);
                A0j2.append(".");
                String A0d2 = C12960it.A0d("private", A0j2);
                Calendar instance = Calendar.getInstance();
                instance.add(10, -24);
                Calendar instance2 = Calendar.getInstance();
                instance2.add(10, 2160);
                C129815yL r7 = this.A03;
                AnonymousClass009.A05(r7);
                Context context = this.A09.A00;
                r7.A01(context, A0d, A0d, instance.getTime(), instance2.getTime(), encoded);
                this.A03.A01(context, A0d2, A0d2, instance.getTime(), instance2.getTime(), encoded2);
                if (encoded != null) {
                    Arrays.fill(encoded, (byte) 0);
                }
                if (encoded2 != null) {
                    Arrays.fill(encoded2, (byte) 0);
                }
            }
        }
        if (keyPair != null) {
            this.A07.put(str, keyPair);
            return keyPair;
        }
        return keyPair;
    }

    public final synchronized void A05() {
        KeyStore keyStore;
        C129995yd r2;
        if (!this.A08) {
            int i = Build.VERSION.SDK_INT;
            boolean z = false;
            if (i >= 18) {
                z = true;
            }
            C130005ye r3 = null;
            if (z) {
                try {
                    keyStore = KeyStore.getInstance("AndroidKeyStore");
                    try {
                        keyStore.load(null);
                    } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException unused) {
                        Log.e("PAY: NoviKeyStore/initialize can't construct KeyStore");
                        if (i >= 18) {
                        }
                        this.A04 = null;
                        r2 = null;
                        this.A05 = r3;
                        this.A03 = new C129815yL(A00(), r2);
                        this.A08 = true;
                    }
                } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException unused2) {
                    keyStore = null;
                }
            } else {
                keyStore = null;
            }
            if (i >= 18 || keyStore == null) {
                this.A04 = null;
                r2 = null;
            } else {
                r2 = new C129995yd(keyStore);
                this.A04 = r2;
                r3 = new C130005ye(keyStore);
            }
            this.A05 = r3;
            this.A03 = new C129815yL(A00(), r2);
            this.A08 = true;
        }
    }

    public void A06(String str) {
        A05();
        Map map = this.A07;
        if (map != null) {
            map.remove(str);
        }
        A05();
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(".");
        String A0d = C12960it.A0d("public", A0j);
        StringBuilder A0j2 = C12960it.A0j(str);
        A0j2.append(".");
        String A0d2 = C12960it.A0d("private", A0j2);
        C129815yL r0 = this.A03;
        AnonymousClass009.A05(r0);
        r0.A02(A0d, A0d);
        this.A03.A02(A0d2, A0d2);
        C130005ye r02 = this.A05;
        if (r02 != null) {
            r02.A03(str);
        }
    }
}
