package X;

import android.net.ConnectivityManager;
import android.net.Network;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

/* renamed from: X.2Zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51892Zn extends ConnectivityManager.NetworkCallback {
    public final /* synthetic */ AnonymousClass2CS A00;
    public final /* synthetic */ ScheduledFuture A01;
    public final /* synthetic */ boolean A02;

    public C51892Zn(AnonymousClass2CS r1, ScheduledFuture scheduledFuture, boolean z) {
        this.A00 = r1;
        this.A01 = scheduledFuture;
        this.A02 = z;
    }

    public static /* synthetic */ void A00(Network network, C51892Zn r3, ScheduledFuture scheduledFuture, boolean z) {
        scheduledFuture.cancel(false);
        AnonymousClass2CS r1 = r3.A00;
        if (r1.A00 == null) {
            Log.i("voip/weak-wifi/onAvailable: network callback is already unregistered");
        } else if (r1.A02 != null) {
            Log.i("voip/weak-wifi/onAvailable: onAvailable() is called multiple times");
            Voip.notifyLostOfAlternativeNetwork();
        } else {
            r1.A09(network, z);
        }
    }

    public static /* synthetic */ void A01(C51892Zn r1, ScheduledFuture scheduledFuture) {
        scheduledFuture.cancel(false);
        if (r1.A00.A00 == null) {
            Log.i("voip/weak-wifi/onLost: network callback is already unregistered");
        } else {
            Voip.notifyLostOfAlternativeNetwork();
        }
    }

    public static /* synthetic */ void A02(C51892Zn r1, ScheduledFuture scheduledFuture, boolean z) {
        scheduledFuture.cancel(false);
        AnonymousClass2CS r12 = r1.A00;
        if (r12.A00 == null) {
            Log.i("voip/weak-wifi/onUnavailable: network callback is already unregistered");
            return;
        }
        r12.A00 = null;
        r12.A01 = null;
        Voip.notifyFailureToCreateAlternativeSocket(z);
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onAvailable(Network network) {
        Log.i("voip/weak-wifi/onAvailable");
        ScheduledExecutorService scheduledExecutorService = this.A00.A05;
        if (scheduledExecutorService.isShutdown()) {
            Log.i("voip/weak-wifi/executor service shut down before response");
        } else {
            scheduledExecutorService.execute(new Runnable(network, this, this.A01, this.A02) { // from class: X.3lG
                public final /* synthetic */ Network A00;
                public final /* synthetic */ C51892Zn A01;
                public final /* synthetic */ ScheduledFuture A02;
                public final /* synthetic */ boolean A03;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A03 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C51892Zn.A00(this.A00, this.A01, this.A02, this.A03);
                }
            });
        }
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onLost(Network network) {
        Log.i("voip/weak-wifi/onLost");
        ScheduledExecutorService scheduledExecutorService = this.A00.A05;
        if (scheduledExecutorService.isShutdown()) {
            Log.i("voip/weak-wifi/executor service shut down before response");
        } else {
            scheduledExecutorService.execute(new Runnable(this.A01) { // from class: X.3l4
                public final /* synthetic */ ScheduledFuture A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C51892Zn.A01(C51892Zn.this, this.A01);
                }
            });
        }
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onUnavailable() {
        Log.i("voip/weak-wifi/onUnavailable");
        ScheduledExecutorService scheduledExecutorService = this.A00.A05;
        if (scheduledExecutorService.isShutdown()) {
            Log.i("voip/weak-wifi/executor service shut down before response");
        } else {
            scheduledExecutorService.execute(new Runnable(this.A01, this.A02) { // from class: X.3lB
                public final /* synthetic */ ScheduledFuture A01;
                public final /* synthetic */ boolean A02;

                {
                    this.A01 = r2;
                    this.A02 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    C51892Zn.A02(C51892Zn.this, this.A01, this.A02);
                }
            });
        }
    }
}
