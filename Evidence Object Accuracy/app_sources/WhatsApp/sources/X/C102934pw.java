package X;

import android.content.Context;
import com.whatsapp.community.CommunityHomeActivity;

/* renamed from: X.4pw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102934pw implements AbstractC009204q {
    public final /* synthetic */ CommunityHomeActivity A00;

    public C102934pw(CommunityHomeActivity communityHomeActivity) {
        this.A00 = communityHomeActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
