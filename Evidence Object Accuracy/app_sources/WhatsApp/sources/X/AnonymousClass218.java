package X;

import java.util.List;

/* renamed from: X.218  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass218 {
    public final long A00;
    public final long A01;
    public final AnonymousClass21A A02;
    public final String A03;
    public final List A04;

    public AnonymousClass218(AnonymousClass21A r1, String str, List list, long j, long j2) {
        this.A03 = str;
        this.A01 = j;
        this.A00 = j2;
        this.A04 = list;
        this.A02 = r1;
    }
}
