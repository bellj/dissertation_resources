package X;

import android.content.Context;
import android.location.Location;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.whatsapp.R;

/* renamed from: X.3FV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FV {
    public final C35961j4 A00;

    public AnonymousClass3FV(C35961j4 r1) {
        this.A00 = r1;
    }

    public int A00() {
        Location A01 = A01();
        C56442kt A02 = this.A00.A00().A02();
        Location location = new Location("");
        LatLng latLng = A02.A02;
        double d = latLng.A00;
        LatLng latLng2 = A02.A03;
        location.setLatitude((d + latLng2.A00) / 2.0d);
        location.setLongitude((latLng.A01 + latLng2.A01) / 2.0d);
        return (int) A01.distanceTo(location);
    }

    public Location A01() {
        LatLng latLng = this.A00.A02().A03;
        Location location = new Location("");
        location.setLatitude(latLng.A00);
        location.setLongitude(latLng.A01);
        return location;
    }

    public void A02(Context context, LatLngBounds latLngBounds, boolean z) {
        if (z) {
            C35961j4 r8 = this.A00;
            LatLng latLng = latLngBounds.A01;
            double d = latLng.A00;
            LatLng latLng2 = latLngBounds.A00;
            double d2 = (d + latLng2.A00) / 2.0d;
            double d3 = latLng2.A01;
            double d4 = latLng.A01;
            if (d4 > d3) {
                d3 += 360.0d;
            }
            r8.A09(C65193Io.A02(new LatLng(d2, (d3 + d4) / 2.0d), 15.0f));
            return;
        }
        this.A00.A09(C65193Io.A03(latLngBounds, context.getResources().getDimensionPixelSize(R.dimen.google_map_coordinator_camera_zoom)));
    }

    public void A03(Location location, AbstractC116405Vh r7, Float f, Integer num, boolean z) {
        float floatValue;
        if (location != null) {
            LatLng A01 = AnonymousClass1U5.A01(location);
            C35961j4 r3 = this.A00;
            float f2 = r3.A02().A02;
            if (f == null) {
                floatValue = 0.0f;
            } else {
                floatValue = f.floatValue();
            }
            float f3 = f2 + floatValue;
            if (num != null) {
                r3.A08(0, 0, 0, num.intValue());
            }
            AnonymousClass4IX A02 = C65193Io.A02(A01, f3);
            if (z) {
                r3.A0C(A02, r7);
            } else {
                r3.A0A(A02);
            }
        }
    }
}
