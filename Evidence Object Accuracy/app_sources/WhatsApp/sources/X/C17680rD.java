package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0rD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17680rD {
    public final AbstractC15710nm A00;
    public final C17670rC A01;
    public final C16630pM A02;
    public final String A03 = "ctwa_ads_entry_points";

    public C17680rD(AbstractC15710nm r2, C17670rC r3, C16630pM r4) {
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
    }

    public void A00(UserJid userJid) {
        this.A02.A01(this.A03).edit().remove(userJid.getRawString()).apply();
    }

    public void A01(C456222j r5, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("/");
        sb.append(r5.getMessage());
        String obj = sb.toString();
        this.A00.AaV("JidKeyedSharedPreferencesStoreTransformationException", obj, true);
        StringBuilder sb2 = new StringBuilder("JidKeyedSharedPreferencesStore/");
        sb2.append(obj);
        Log.e(sb2.toString(), r5);
    }
}
