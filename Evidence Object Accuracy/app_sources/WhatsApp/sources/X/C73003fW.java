package X;

import android.database.DataSetObserver;

/* renamed from: X.3fW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73003fW extends DataSetObserver {
    public final /* synthetic */ AbstractC54432gi A00;

    public /* synthetic */ C73003fW(AbstractC54432gi r1) {
        this.A00 = r1;
    }

    @Override // android.database.DataSetObserver
    public void onChanged() {
        super.onChanged();
        AbstractC54432gi r1 = this.A00;
        r1.A03 = true;
        r1.A02();
    }

    @Override // android.database.DataSetObserver
    public void onInvalidated() {
        super.onInvalidated();
        AbstractC54432gi r1 = this.A00;
        r1.A03 = false;
        r1.A02();
    }
}
