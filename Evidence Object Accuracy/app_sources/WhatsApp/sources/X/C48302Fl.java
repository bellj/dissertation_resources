package X;

import android.app.Activity;
import android.app.Application;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.base.WaListFragment;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.camera.bottomsheet.CameraMediaPickerFragment;
import com.whatsapp.ctwa.bizpreview.BusinessPreviewInitializer;
import com.whatsapp.gallery.GalleryTabHostFragment;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.2Fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48302Fl implements AnonymousClass01N {
    public final int A00;
    public final AnonymousClass2FL A01;
    public final C48722Hj A02;
    public final AnonymousClass01J A03;

    public C48302Fl(AnonymousClass2FL r1, C48722Hj r2, AnonymousClass01J r3, int i) {
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        int i = this.A00;
        switch (i) {
            case 0:
                AnonymousClass2FL r5 = this.A01;
                AnonymousClass01J r1 = this.A03;
                AnonymousClass01N r3 = r5.A08;
                BusinessPreviewInitializer businessPreviewInitializer = new BusinessPreviewInitializer((C14900mE) r1.A8X.get(), (AnonymousClass232) r3.get(), (AbstractC14440lR) r1.ANe.get());
                AnonymousClass01J r12 = r5.A1E;
                businessPreviewInitializer.A00 = (C14900mE) r12.A8X.get();
                businessPreviewInitializer.A02 = (AbstractC14440lR) r12.ANe.get();
                businessPreviewInitializer.A01 = (AnonymousClass232) r3.get();
                return businessPreviewInitializer;
            case 1:
                AnonymousClass2FL r4 = this.A01;
                AnonymousClass01J r2 = this.A03;
                AnonymousClass232 r22 = new AnonymousClass232((C14650lo) r2.A2V.get(), (C14830m7) r2.ALb.get(), (C255919z) r2.A2Q.get());
                AnonymousClass01J r13 = r4.A1E;
                r22.A02 = (C14830m7) r13.ALb.get();
                r22.A03 = (C255919z) r13.A2Q.get();
                r22.A01 = (C14650lo) r13.A2V.get();
                return r22;
            case 2:
                return new AnonymousClass2J0(this);
            case 3:
                return new AnonymousClass2IH(this);
            case 4:
                return new AnonymousClass2II(this);
            case 5:
                return new AnonymousClass2IJ(this);
            case 6:
                return new AnonymousClass2IK(this);
            case 7:
                return new AnonymousClass2I9(this);
            case 8:
                return new AnonymousClass2IB(this);
            case 9:
                AnonymousClass01J r14 = this.A03;
                C15600nX r7 = (C15600nX) r14.A8x.get();
                AnonymousClass161 r9 = (AnonymousClass161) r14.AJ4.get();
                AnonymousClass1AM r10 = (AnonymousClass1AM) r14.AJG.get();
                AnonymousClass1EB r6 = (AnonymousClass1EB) r14.ACN.get();
                AnonymousClass1AN r8 = (AnonymousClass1AN) r14.A5T.get();
                return new AnonymousClass2IL((AnonymousClass1AO) r14.AFg.get(), r6, r7, r8, r9, r10, (AbstractC14440lR) r14.ANe.get());
            case 10:
                return new AnonymousClass2IM(this);
            case 11:
                return new AnonymousClass2IN(this);
            case 12:
                return new AnonymousClass2IO(this);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new AnonymousClass2IP(this);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new AnonymousClass2IQ(this);
            case 15:
                return new AnonymousClass2IR(this);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new AnonymousClass2IS(this);
            case 17:
                return new AnonymousClass2IT(this);
            case 18:
                return new AnonymousClass2IU(this);
            case 19:
                return new AnonymousClass2IV(this);
            case C43951xu.A01:
                AnonymousClass2FL r42 = this.A01;
                C28241Mh builderWithExpectedSize = AbstractC17190qP.builderWithExpectedSize(7);
                AnonymousClass01J r52 = r42.A1E;
                builderWithExpectedSize.put(AnonymousClass1E0.class, r52.A4V());
                builderWithExpectedSize.put(C26571Dz.class, r52.A4W());
                builderWithExpectedSize.put(AnonymousClass110.class, new AnonymousClass1t0(r52.ANx));
                builderWithExpectedSize.put(C232610z.class, r52.A4X());
                builderWithExpectedSize.put(AnonymousClass115.class, r52.A4Y());
                AnonymousClass01H A00 = C18000rk.A00(r42.A0B);
                if (A00 != null) {
                    builderWithExpectedSize.put(CallsHistoryFragment.class, A00);
                    AnonymousClass01H A002 = C18000rk.A00(r42.A18);
                    if (A002 != null) {
                        builderWithExpectedSize.put(AnonymousClass2IW.class, A002);
                        return new C18980tN(builderWithExpectedSize.build());
                    }
                    throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
                }
                throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
            case 21:
                AnonymousClass2FL r32 = this.A01;
                CallsHistoryFragment callsHistoryFragment = new CallsHistoryFragment();
                AnonymousClass01J r23 = r32.A1E;
                ((WaListFragment) callsHistoryFragment).A00 = (AnonymousClass182) r23.A94.get();
                ((WaListFragment) callsHistoryFragment).A01 = (AnonymousClass180) r23.ALt.get();
                callsHistoryFragment.A0J = (C14830m7) r23.ALb.get();
                callsHistoryFragment.A0P = (C14850m9) r23.A04.get();
                callsHistoryFragment.A03 = (C14900mE) r23.A8X.get();
                callsHistoryFragment.A0U = (C253018w) r23.AJS.get();
                callsHistoryFragment.A04 = (C15570nT) r23.AAr.get();
                callsHistoryFragment.A0W = (AbstractC14440lR) r23.ANe.get();
                callsHistoryFragment.A0V = (AnonymousClass12U) r23.AJd.get();
                callsHistoryFragment.A05 = (C15450nH) r23.AII.get();
                callsHistoryFragment.A0O = (C21250x7) r23.AJh.get();
                callsHistoryFragment.A06 = (AnonymousClass1AR) r23.ALM.get();
                callsHistoryFragment.A0Z = (AnonymousClass19Z) r23.A2o.get();
                callsHistoryFragment.A02 = (AnonymousClass12P) r23.A0H.get();
                callsHistoryFragment.A0G = (C21270x9) r23.A4A.get();
                callsHistoryFragment.A0S = (AnonymousClass17R) r23.AIz.get();
                callsHistoryFragment.A0b = (C21280xA) r23.AMU.get();
                callsHistoryFragment.A0B = (C15550nR) r23.A45.get();
                callsHistoryFragment.A0I = (AnonymousClass01d) r23.ALI.get();
                callsHistoryFragment.A0D = (C15610nY) r23.AMe.get();
                callsHistoryFragment.A0K = (AnonymousClass018) r23.ANb.get();
                callsHistoryFragment.A0X = (AnonymousClass12G) r23.A2h.get();
                callsHistoryFragment.A0C = (AnonymousClass10S) r23.A46.get();
                callsHistoryFragment.A0Q = (C20710wC) r23.A8m.get();
                callsHistoryFragment.A0T = (AnonymousClass12F) r23.AJM.get();
                callsHistoryFragment.A0H = r32.A05();
                callsHistoryFragment.A0L = (C18750sx) r23.A2p.get();
                callsHistoryFragment.A0A = (AnonymousClass116) r23.A3z.get();
                callsHistoryFragment.A0N = (C236812p) r23.AAC.get();
                callsHistoryFragment.A0c = (C17140qK) r23.AMZ.get();
                callsHistoryFragment.A0M = (C15600nX) r23.A8x.get();
                callsHistoryFragment.A0R = (C244215l) r23.A8y.get();
                callsHistoryFragment.A0a = (C237512w) r23.AAD.get();
                callsHistoryFragment.A09 = (C22330yu) r23.A3I.get();
                callsHistoryFragment.A0Y = (C21260x8) r23.A2k.get();
                return callsHistoryFragment;
            case 22:
                return new AnonymousClass2IW();
            case 23:
                return new AnonymousClass2IX(this);
            case 24:
                return new AnonymousClass2IY(this);
            case 25:
                return new AnonymousClass2IZ(this);
            case 26:
                AnonymousClass01J r0 = this.A03;
                C14830m7 r15 = (C14830m7) r0.ALb.get();
                C18720su r142 = (C18720su) r0.A2c.get();
                C14900mE r122 = (C14900mE) r0.A8X.get();
                C14330lG r102 = (C14330lG) r0.A7B.get();
                C15450nH r92 = (C15450nH) r0.AII.get();
                AnonymousClass2FO A06 = this.A01.A06();
                AnonymousClass01d r62 = (AnonymousClass01d) r0.ALI.get();
                AnonymousClass018 r53 = (AnonymousClass018) r0.ANb.get();
                C15890o4 r43 = (C15890o4) r0.AN1.get();
                C14820m6 r33 = (C14820m6) r0.AN3.get();
                C15690nk r24 = (C15690nk) r0.A74.get();
                return new AnonymousClass2GJ(r102, r122, r92, r142, (AnonymousClass1AX) r0.A2r.get(), r62, r15, (C16590pI) r0.AMg.get(), r43, r33, r53, (C14850m9) r0.A04.get(), A06, (C16630pM) r0.AIc.get(), r24, (AbstractC14440lR) r0.ANe.get(), (C21260x8) r0.A2k.get(), (C21280xA) r0.AMU.get());
            case 27:
                C48862Id r16 = new C48862Id(new GalleryTabHostFragment());
                Object cameraMediaPickerFragment = new CameraMediaPickerFragment();
                if (((C14850m9) this.A03.A04.get()).A07(1857)) {
                    cameraMediaPickerFragment = r16.reference;
                }
                if (cameraMediaPickerFragment != null) {
                    return cameraMediaPickerFragment;
                }
                throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
            case 28:
                return new AnonymousClass2FR(this);
            case 29:
                return new C48882Ih(this);
            case C25991Bp.A0S:
                return new C48892Ii(this);
            case 31:
                return new C48902Ij(this);
            case 32:
                return new C48912Ik(this);
            case 33:
                return new C48922Il(this);
            case 34:
                return new C48832Hy(this);
            case 35:
                return new C48932Im(this);
            case 36:
                return new C48952Io(this);
            case 37:
                return new C48962Ip(this);
            case 38:
                return new C48972Iq(this);
            case 39:
                return new C48982Ir(this);
            case 40:
                return new C48992Is(this);
            case 41:
                return new C49002It(this);
            case 42:
                return new C49012Iu(this);
            case 43:
                return new C49022Iv(this);
            case 44:
                return new C49032Iw(this);
            case 45:
                AnonymousClass01J r17 = this.A03;
                return new C123435n9((C15450nH) r17.AII.get(), (C14830m7) r17.ALb.get(), (C15650ng) r17.A4m.get(), (C14850m9) r17.A04.get(), (AbstractC16870pt) r17.A20.get(), (C20320vZ) r17.A7A.get());
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                AnonymousClass01J r18 = this.A03;
                return new C123405n6((C15450nH) r18.AII.get(), (C14830m7) r18.ALb.get(), (C15650ng) r18.A4m.get(), (AbstractC16870pt) r18.A20.get(), (C20320vZ) r18.A7A.get());
            case 47:
                AnonymousClass01J r19 = this.A03;
                return new C123445nA((C15450nH) r19.AII.get(), (C14830m7) r19.ALb.get(), (C15650ng) r19.A4m.get(), (AbstractC16870pt) r19.A20.get(), (C20320vZ) r19.A7A.get());
            case 48:
                AnonymousClass01J r110 = this.A03;
                return new C123415n7((C15450nH) r110.AII.get(), (C14830m7) r110.ALb.get(), (C15650ng) r110.A4m.get(), (AbstractC16870pt) r110.A20.get(), (C20320vZ) r110.A7A.get());
            case 49:
                AnonymousClass01J r111 = this.A03;
                return new C123425n8((C15450nH) r111.AII.get(), (C14830m7) r111.ALb.get(), (C15650ng) r111.A4m.get(), (AbstractC16870pt) r111.A20.get(), (C20320vZ) r111.A7A.get());
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                AnonymousClass01J r112 = this.A03;
                AbstractC16870pt r82 = (AbstractC16870pt) r112.A20.get();
                C20370ve r72 = (C20370ve) r112.AEu.get();
                AnonymousClass14X r93 = (AnonymousClass14X) r112.AFM.get();
                return new C123455nB((C15450nH) r112.AII.get(), (C15550nR) r112.A45.get(), (C14830m7) r112.ALb.get(), (AnonymousClass018) r112.ANb.get(), (C15650ng) r112.A4m.get(), r72, r82, r93, (C20320vZ) r112.A7A.get());
            case 51:
                return new C127425uT(this.A01.A1B, (C14830m7) this.A03.ALb.get());
            case 52:
                return new AnonymousClass1J2(this);
            case 53:
                return new AnonymousClass1J3(this);
            case 54:
                AnonymousClass01J r02 = this.A03;
                C15570nT r113 = (C15570nT) r02.AAr.get();
                C16590pI r114 = (C16590pI) r02.AMg.get();
                C14330lG r115 = (C14330lG) r02.A7B.get();
                C15450nH r116 = (C15450nH) r02.AII.get();
                C17200qQ r117 = (C17200qQ) r02.A0j.get();
                C14950mJ r118 = (C14950mJ) r02.AKf.get();
                C20040v7 r119 = (C20040v7) r02.AAK.get();
                C22670zS r120 = (C22670zS) r02.A0V.get();
                C15550nR r121 = (C15550nR) r02.A45.get();
                C241714m r123 = (C241714m) r02.A4l.get();
                AnonymousClass01d r124 = (AnonymousClass01d) r02.ALI.get();
                AnonymousClass018 r125 = (AnonymousClass018) r02.ANb.get();
                C15820nx r152 = (C15820nx) r02.A6U.get();
                C22050yP r143 = (C22050yP) r02.A7v.get();
                C240514a r126 = (C240514a) r02.AJL.get();
                C16490p7 r11 = (C16490p7) r02.ACJ.get();
                C15890o4 r103 = (C15890o4) r02.AN1.get();
                C14820m6 r94 = (C14820m6) r02.AN3.get();
                C22710zW r83 = (C22710zW) r02.AF7.get();
                C22720zX r73 = (C22720zX) r02.ALK.get();
                C15510nN r63 = (C15510nN) r02.AHZ.get();
                C18640sm r54 = (C18640sm) r02.A3u.get();
                AnonymousClass197 r44 = (AnonymousClass197) r02.AAJ.get();
                C20820wN r34 = (C20820wN) r02.ACG.get();
                AnonymousClass1CC r25 = (AnonymousClass1CC) r02.A5C.get();
                return new AnonymousClass1DN(r117, r115, r113, r116, r120, r152, (C22730zY) r02.A8Y.get(), r121, r54, r124, r114, r103, r94, r125, r118, r123, r119, r34, r11, (AnonymousClass150) r02.A63.get(), r44, (C14850m9) r02.A04.get(), r25, r143, (C16120oU) r02.ANE.get(), r83, (C20420vj) r02.A4k.get(), r63, r73, r126, (C235512c) r02.AKS.get());
            case 55:
                return new AnonymousClass1A5((C18980tN) this.A01.A0G.get());
            case 56:
                return new AnonymousClass2JG();
            case 57:
                return new C49042Ix(this);
            case 58:
                return new C49052Iy(this.A01.A1B, (C14820m6) this.A03.AN3.get());
            case 59:
                return new C49062Iz((ActivityC000900k) this.A01.A14.get(), (C14820m6) this.A03.AN3.get());
            case 60:
                Activity activity = this.A01.A1B;
                try {
                    ActivityC000900k r45 = (ActivityC000900k) activity;
                    if (r45 != null) {
                        return r45;
                    }
                    throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
                } catch (ClassCastException e) {
                    StringBuilder sb = new StringBuilder("Expected activity to be a FragmentActivity: ");
                    sb.append(activity);
                    throw new IllegalStateException(sb.toString(), e);
                }
            case 61:
                return new AnonymousClass2J1(this);
            case 62:
                return new AnonymousClass2J3(this);
            case 63:
                return new AnonymousClass2J5(this);
            case 64:
                return new AnonymousClass2J7(this);
            case 65:
                return new AnonymousClass2J9(this);
            case 66:
                AnonymousClass2FL r55 = this.A01;
                AnonymousClass01J r127 = this.A03;
                Application A003 = AbstractC250617y.A00(r127.AO3);
                AnonymousClass01N r35 = r55.A08;
                AnonymousClass233 r26 = new AnonymousClass233(A003, (AnonymousClass232) r35.get(), (AbstractC14440lR) r127.ANe.get());
                AnonymousClass01J r128 = r55.A1E;
                r26.A03 = (AbstractC14440lR) r128.ANe.get();
                r26.A00 = AbstractC250617y.A00(r128.AO3);
                r26.A02 = (AnonymousClass232) r35.get();
                return r26;
            case 67:
                AnonymousClass2FL r129 = this.A01;
                return new C37261lu((AnonymousClass2JB) r129.A0p.get(), (AnonymousClass2JC) r129.A0q.get(), (AnonymousClass2JD) r129.A0r.get(), (AnonymousClass2JE) r129.A0s.get(), (AnonymousClass2JF) r129.A0t.get(), (C14850m9) this.A03.A04.get());
            case 68:
                return new AnonymousClass2JB(this);
            case 69:
                return new AnonymousClass2JC(this);
            case 70:
                return new AnonymousClass2JD(this);
            case 71:
                return new AnonymousClass2JE(this);
            case C43951xu.A02:
                return new AnonymousClass2JF(this);
            default:
                throw new AssertionError(i);
        }
    }
}
