package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99444kJ implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        C78653pG r1 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                r1 = (C78653pG) C95664e9.A07(parcel, C78653pG.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78243ob(r1, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78243ob[i];
    }
}
