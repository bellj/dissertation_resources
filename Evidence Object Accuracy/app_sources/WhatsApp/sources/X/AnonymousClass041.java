package X;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.041  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass041 implements AbstractFutureC44231yX {
    public static final AnonymousClass0Q1 A00;
    public static final Object A01 = new Object();
    public static final Logger A02 = Logger.getLogger(AnonymousClass041.class.getName());
    public static final boolean A03 = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    public volatile C05940Ro listeners;
    public volatile Object value;
    public volatile C06340Tf waiters;

    static {
        AnonymousClass0Q1 r4;
        Throwable th;
        try {
            r4 = new C03200Gp(AtomicReferenceFieldUpdater.newUpdater(C06340Tf.class, Thread.class, "thread"), AtomicReferenceFieldUpdater.newUpdater(C06340Tf.class, C06340Tf.class, "next"), AtomicReferenceFieldUpdater.newUpdater(AnonymousClass041.class, C06340Tf.class, "waiters"), AtomicReferenceFieldUpdater.newUpdater(AnonymousClass041.class, C05940Ro.class, "listeners"), AtomicReferenceFieldUpdater.newUpdater(AnonymousClass041.class, Object.class, "value"));
            th = null;
        } catch (Throwable th2) {
            th = th2;
            r4 = new C03190Go();
        }
        A00 = r4;
        if (th != null) {
            A02.log(Level.SEVERE, "SafeAtomicHelper is broken!", th);
        }
    }

    public static Object A01(AbstractFutureC44231yX r5) {
        Throwable th;
        if (r5 instanceof AnonymousClass041) {
            Object obj = ((AnonymousClass041) r5).value;
            if (!(obj instanceof C05870Rh)) {
                return obj;
            }
            C05870Rh r1 = (C05870Rh) obj;
            if (!r1.A01) {
                return obj;
            }
            Throwable th2 = r1.A00;
            if (th2 != null) {
                return new C05870Rh(th2, false);
            }
        } else {
            boolean isCancelled = r5.isCancelled();
            if (!(!A03) || !isCancelled) {
                try {
                    Object A032 = A03(r5);
                    if (A032 == null) {
                        return A01;
                    }
                    return A032;
                } catch (CancellationException e) {
                    if (isCancelled) {
                        return new C05870Rh(e, false);
                    }
                    StringBuilder sb = new StringBuilder("get() threw CancellationException, despite reporting isCancelled() == false: ");
                    sb.append(r5);
                    th = new IllegalArgumentException(sb.toString(), e);
                    return new AnonymousClass0RZ(th);
                } catch (ExecutionException e2) {
                    th = e2.getCause();
                    return new AnonymousClass0RZ(th);
                } catch (Throwable th3) {
                    th = th3;
                    return new AnonymousClass0RZ(th);
                }
            }
        }
        return C05870Rh.A02;
    }

    public static final Object A02(Object obj) {
        if (obj instanceof C05870Rh) {
            Throwable th = ((C05870Rh) obj).A00;
            CancellationException cancellationException = new CancellationException("Task was cancelled.");
            cancellationException.initCause(th);
            throw cancellationException;
        } else if (obj instanceof AnonymousClass0RZ) {
            throw new ExecutionException(((AnonymousClass0RZ) obj).A00);
        } else if (obj == A01) {
            return null;
        } else {
            return obj;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:13:0x0001 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.util.concurrent.Future] */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v4, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object A03(java.util.concurrent.Future r1) {
        /*
            r0 = 0
        L_0x0001:
            java.lang.Object r1 = r1.get()     // Catch: InterruptedException -> 0x0008, all -> 0x0012
            if (r0 == 0) goto L_0x0011
            goto L_0x000a
        L_0x0008:
            r0 = 1
            goto L_0x0001
        L_0x000a:
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x0011:
            return r1
        L_0x0012:
            r1 = move-exception
            if (r0 == 0) goto L_0x001c
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x001c:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass041.A03(java.util.concurrent.Future):java.lang.Object");
    }

    public static void A04(AnonymousClass041 r5) {
        C05940Ro r1;
        C05940Ro r4 = null;
        while (true) {
            C06340Tf r2 = r5.waiters;
            AnonymousClass0Q1 r3 = A00;
            if (r3.A03(r2, C06340Tf.A00, r5)) {
                while (r2 != null) {
                    Thread thread = r2.thread;
                    if (thread != null) {
                        r2.thread = null;
                        LockSupport.unpark(thread);
                    }
                    r2 = r2.next;
                }
                do {
                    r1 = r5.listeners;
                } while (!r3.A02(r1, C05940Ro.A03, r5));
                while (r1 != null) {
                    C05940Ro r0 = r1.A00;
                    r1.A00 = r4;
                    r4 = r1;
                    r1 = r0;
                }
                while (r4 != null) {
                    r4 = r4.A00;
                    Runnable runnable = r4.A01;
                    if (runnable instanceof RunnableC09700dN) {
                        RunnableC09700dN r12 = (RunnableC09700dN) runnable;
                        r5 = r12.A00;
                        if (r5.value == r12 && r3.A04(r5, r12, A01(r12.A01))) {
                            break;
                        }
                    } else {
                        A05(runnable, r4.A02);
                    }
                }
                return;
            }
        }
    }

    public static void A05(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e) {
            Logger logger = A02;
            Level level = Level.SEVERE;
            StringBuilder sb = new StringBuilder("RuntimeException while executing runnable ");
            sb.append(runnable);
            sb.append(" with executor ");
            sb.append(executor);
            logger.log(level, sb.toString(), (Throwable) e);
        }
    }

    public String A06() {
        String valueOf;
        Object obj = this.value;
        if (obj instanceof RunnableC09700dN) {
            StringBuilder sb = new StringBuilder("setFuture=[");
            AbstractFutureC44231yX r0 = ((RunnableC09700dN) obj).A01;
            if (r0 == this) {
                valueOf = "this future";
            } else {
                valueOf = String.valueOf(r0);
            }
            sb.append(valueOf);
            sb.append("]");
            return sb.toString();
        } else if (!(this instanceof ScheduledFuture)) {
            return null;
        } else {
            StringBuilder sb2 = new StringBuilder("remaining delay=[");
            sb2.append(((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS));
            sb2.append(" ms]");
            return sb2.toString();
        }
    }

    public final void A07(C06340Tf r6) {
        r6.thread = null;
        while (true) {
            C06340Tf r3 = this.waiters;
            if (r3 != C06340Tf.A00) {
                C06340Tf r2 = null;
                while (r3 != null) {
                    C06340Tf r1 = r3.next;
                    if (r3.thread != null) {
                        r2 = r3;
                    } else if (r2 != null) {
                        r2.next = r1;
                        if (r2.thread == null) {
                            break;
                        }
                    } else if (!A00.A03(r3, r1, this)) {
                        break;
                    }
                    r3 = r1;
                }
                return;
            }
            return;
        }
    }

    public void A08(AbstractFutureC44231yX r6) {
        AnonymousClass0RZ r0;
        Object obj = this.value;
        if (obj == null) {
            if (r6.isDone()) {
                if (A00.A04(this, null, A01(r6))) {
                    A04(this);
                    return;
                }
                return;
            }
            RunnableC09700dN r3 = new RunnableC09700dN(this, r6);
            AnonymousClass0Q1 r2 = A00;
            if (r2.A04(this, null, r3)) {
                try {
                    r6.A5i(r3, EnumC03940Jt.A01);
                    return;
                } catch (Throwable th) {
                    try {
                        r0 = new AnonymousClass0RZ(th);
                    } catch (Throwable unused) {
                        r0 = AnonymousClass0RZ.A01;
                    }
                    r2.A04(this, r3, r0);
                    return;
                }
            } else {
                obj = this.value;
            }
        }
        if (obj instanceof C05870Rh) {
            r6.cancel(((C05870Rh) obj).A01);
        }
    }

    public void A09(Object obj) {
        if (obj == null) {
            obj = A01;
        }
        if (A00.A04(this, null, obj)) {
            A04(this);
        }
    }

    public void A0A(Throwable th) {
        if (A00.A04(this, null, new AnonymousClass0RZ(th))) {
            A04(this);
        }
    }

    @Override // X.AbstractFutureC44231yX
    public final void A5i(Runnable runnable, Executor executor) {
        C05940Ro r3 = this.listeners;
        C05940Ro r2 = C05940Ro.A03;
        if (r3 != r2) {
            C05940Ro r1 = new C05940Ro(runnable, executor);
            do {
                r1.A00 = r3;
                if (!A00.A02(r3, r1, this)) {
                    r3 = this.listeners;
                } else {
                    return;
                }
            } while (r3 != r2);
            A05(runnable, executor);
        }
        A05(runnable, executor);
    }

    @Override // java.util.concurrent.Future
    public final boolean cancel(boolean z) {
        C05870Rh r3;
        Object obj = this.value;
        boolean z2 = false;
        if (obj == null) {
            z2 = true;
        }
        if (!z2 && !(obj instanceof RunnableC09700dN)) {
            return false;
        }
        if (A03) {
            r3 = new C05870Rh(new CancellationException("Future.cancel() was called."), z);
        } else if (z) {
            r3 = C05870Rh.A03;
        } else {
            r3 = C05870Rh.A02;
        }
        boolean z3 = false;
        AnonymousClass041 r2 = this;
        while (true) {
            if (A00.A04(r2, obj, r3)) {
                A04(r2);
                if (!(obj instanceof RunnableC09700dN)) {
                    break;
                }
                AbstractFutureC44231yX r22 = ((RunnableC09700dN) obj).A01;
                if (!(r22 instanceof AnonymousClass041)) {
                    r22.cancel(z);
                    break;
                }
                r2 = (AnonymousClass041) r22;
                obj = r2.value;
                boolean z4 = false;
                if (obj == null) {
                    z4 = true;
                }
                if (!z4 && !(obj instanceof RunnableC09700dN)) {
                    break;
                }
                z3 = true;
            } else {
                obj = r2.value;
                if (!(obj instanceof RunnableC09700dN)) {
                    return z3;
                }
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        java.util.concurrent.locks.LockSupport.park(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        if (java.lang.Thread.interrupted() != false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003d, code lost:
        r3 = r5.value;
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        if (r3 == null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0047, code lost:
        if ((r1 & (!(r3 instanceof X.RunnableC09700dN))) == false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004a, code lost:
        A07(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0052, code lost:
        throw new java.lang.InterruptedException();
     */
    @Override // java.util.concurrent.Future
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object get() {
        /*
            r5 = this;
            boolean r0 = java.lang.Thread.interrupted()
            if (r0 != 0) goto L_0x0053
            java.lang.Object r3 = r5.value
            r4 = 1
            r1 = 0
            if (r3 == 0) goto L_0x000d
            r1 = 1
        L_0x000d:
            boolean r0 = r3 instanceof X.RunnableC09700dN
            r0 = r0 ^ r4
            r1 = r1 & r0
            if (r1 != 0) goto L_0x002f
            X.0Tf r3 = r5.waiters
            X.0Tf r1 = X.C06340Tf.A00
            if (r3 == r1) goto L_0x002d
            X.0Tf r2 = new X.0Tf
            r2.<init>()
        L_0x001e:
            r2.A00(r3)
            X.0Q1 r0 = X.AnonymousClass041.A00
            boolean r0 = r0.A03(r3, r2, r5)
            if (r0 != 0) goto L_0x0034
            X.0Tf r3 = r5.waiters
            if (r3 != r1) goto L_0x001e
        L_0x002d:
            java.lang.Object r3 = r5.value
        L_0x002f:
            java.lang.Object r0 = A02(r3)
            return r0
        L_0x0034:
            java.util.concurrent.locks.LockSupport.park(r5)
            boolean r0 = java.lang.Thread.interrupted()
            if (r0 != 0) goto L_0x004a
            java.lang.Object r3 = r5.value
            r1 = 0
            if (r3 == 0) goto L_0x0043
            r1 = 1
        L_0x0043:
            boolean r0 = r3 instanceof X.RunnableC09700dN
            r0 = r0 ^ r4
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0034
            goto L_0x002f
        L_0x004a:
            r5.A07(r2)
            java.lang.InterruptedException r0 = new java.lang.InterruptedException
            r0.<init>()
            throw r0
        L_0x0053:
            java.lang.InterruptedException r0 = new java.lang.InterruptedException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass041.get():java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0059, code lost:
        java.util.concurrent.locks.LockSupport.parkNanos(r17, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0060, code lost:
        if (java.lang.Thread.interrupted() != false) goto L_0x018a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0062, code lost:
        r4 = r17.value;
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0065, code lost:
        if (r4 == null) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0067, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006d, code lost:
        if ((r1 & (!(r4 instanceof X.RunnableC09700dN))) != false) goto L_0x0185;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006f, code lost:
        r0 = r10 - java.lang.System.nanoTime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0077, code lost:
        if (r0 >= 1000) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0079, code lost:
        A07(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ee, code lost:
        if (r2 > 1000) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x018a, code lost:
        A07(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0192, code lost:
        throw new java.lang.InterruptedException();
     */
    @Override // java.util.concurrent.Future
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object get(long r18, java.util.concurrent.TimeUnit r20) {
        /*
        // Method dump skipped, instructions count: 409
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass041.get(long, java.util.concurrent.TimeUnit):java.lang.Object");
    }

    @Override // java.util.concurrent.Future
    public final boolean isCancelled() {
        return this.value instanceof C05870Rh;
    }

    @Override // java.util.concurrent.Future
    public final boolean isDone() {
        Object obj = this.value;
        boolean z = false;
        if (obj != null) {
            z = true;
        }
        return (!(obj instanceof RunnableC09700dN)) & z;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:8|(3:31|10|(2:17|(1:19))(3:16|6|7))|33|20|(2:22|24)(1:23)|25|6|7) */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007c, code lost:
        r3.append("UNKNOWN, cause=[");
        r3.append(r1.getClass());
        r0 = " thrown from get()]";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008b, code lost:
        r0 = "CANCELLED";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
        r3.append("FAILURE, cause=[");
        r3.append(r1.getCause());
        r3.append("]");
     */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r5 = this;
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = super.toString()
            r3.append(r0)
            java.lang.String r0 = "[status="
            r3.append(r0)
            boolean r0 = r5.isCancelled()
            java.lang.String r2 = "]"
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = "CANCELLED"
        L_0x001b:
            r3.append(r0)
        L_0x001e:
            r3.append(r2)
            java.lang.String r0 = r3.toString()
            return r0
        L_0x0026:
            boolean r0 = r5.isDone()
            if (r0 != 0) goto L_0x0061
            java.lang.String r1 = r5.A06()     // Catch: RuntimeException -> 0x0031
            goto L_0x0044
        L_0x0031:
            r4 = move-exception
            java.lang.String r0 = "Exception thrown from implementation: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.Class r0 = r4.getClass()
            r1.append(r0)
            java.lang.String r1 = r1.toString()
        L_0x0044:
            if (r1 == 0) goto L_0x0058
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x0058
            java.lang.String r0 = "PENDING, info=["
            r3.append(r0)
            r3.append(r1)
            r3.append(r2)
            goto L_0x001e
        L_0x0058:
            boolean r0 = r5.isDone()
            if (r0 != 0) goto L_0x0061
            java.lang.String r0 = "PENDING"
            goto L_0x001b
        L_0x0061:
            java.lang.Object r1 = A03(r5)     // Catch: ExecutionException -> 0x008e, CancellationException -> 0x008b, RuntimeException -> 0x007b
            java.lang.String r0 = "SUCCESS, result=["
            r3.append(r0)     // Catch: ExecutionException -> 0x008e, CancellationException -> 0x008b, RuntimeException -> 0x007b
            if (r1 != r5) goto L_0x006d
            goto L_0x0072
        L_0x006d:
            java.lang.String r0 = java.lang.String.valueOf(r1)     // Catch: ExecutionException -> 0x008e, CancellationException -> 0x008b, RuntimeException -> 0x007b
            goto L_0x0074
        L_0x0072:
            java.lang.String r0 = "this future"
        L_0x0074:
            r3.append(r0)     // Catch: ExecutionException -> 0x008e, CancellationException -> 0x008b, RuntimeException -> 0x007b
            r3.append(r2)     // Catch: ExecutionException -> 0x008e, CancellationException -> 0x008b, RuntimeException -> 0x007b
            goto L_0x001e
        L_0x007b:
            r1 = move-exception
            java.lang.String r0 = "UNKNOWN, cause=["
            r3.append(r0)
            java.lang.Class r0 = r1.getClass()
            r3.append(r0)
            java.lang.String r0 = " thrown from get()]"
            goto L_0x001b
        L_0x008b:
            java.lang.String r0 = "CANCELLED"
            goto L_0x001b
        L_0x008e:
            r1 = move-exception
            java.lang.String r0 = "FAILURE, cause=["
            r3.append(r0)
            java.lang.Throwable r0 = r1.getCause()
            r3.append(r0)
            r3.append(r2)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass041.toString():java.lang.String");
    }
}
