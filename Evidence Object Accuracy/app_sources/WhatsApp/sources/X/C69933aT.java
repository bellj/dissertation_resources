package X;

import android.text.Layout;
import android.text.StaticLayout;
import android.widget.TextView;

/* renamed from: X.3aT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69933aT implements AnonymousClass5WJ {
    @Override // X.AnonymousClass5WJ
    public Layout A8L(TextView textView, CharSequence charSequence, int i) {
        Layout layout = textView.getLayout();
        return StaticLayout.Builder.obtain(AnonymousClass1US.A01(charSequence), 0, charSequence.length(), textView.getPaint(), i).setAlignment(layout.getAlignment()).setLineSpacing(layout.getSpacingAdd(), layout.getSpacingMultiplier()).setBreakStrategy(textView.getBreakStrategy()).setHyphenationFrequency(textView.getHyphenationFrequency()).build();
    }

    @Override // X.AnonymousClass5WJ
    public void Acs(TextView textView) {
        textView.setBreakStrategy(0);
    }
}
