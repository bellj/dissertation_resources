package X;

import com.whatsapp.notification.PopupNotification;

/* renamed from: X.3UH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UH implements AbstractC116455Vm {
    public final /* synthetic */ PopupNotification A00;

    public AnonymousClass3UH(PopupNotification popupNotification) {
        this.A00 = popupNotification;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A0c);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        PopupNotification popupNotification = this.A00;
        if (popupNotification.A1G.A0P == null) {
            AbstractC36671kL.A08(popupNotification.A0c, iArr, 0);
        }
    }
}
