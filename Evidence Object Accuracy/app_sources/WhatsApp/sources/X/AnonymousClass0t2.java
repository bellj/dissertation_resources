package X;

import android.database.Cursor;

/* renamed from: X.0t2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass0t2 extends AbstractC18500sY {
    public final C16510p9 A00;

    public AnonymousClass0t2(C16510p9 r3, C18480sW r4) {
        super(r4, "migration_chat_store", Integer.MIN_VALUE);
        AnonymousClass009.A05(r3);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public long A06() {
        return super.A06();
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        return new AnonymousClass2Ez(0, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x031a, code lost:
        if (r0 == 0) goto L_0x031c;
     */
    @Override // X.AbstractC18500sY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0V(X.C29001Pw r55) {
        /*
        // Method dump skipped, instructions count: 1282
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0t2.A0V(X.1Pw):boolean");
    }
}
