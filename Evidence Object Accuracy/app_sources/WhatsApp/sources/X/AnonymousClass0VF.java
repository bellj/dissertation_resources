package X;

import android.os.Handler;
import android.os.Message;

/* renamed from: X.0VF  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0VF implements Handler.Callback {
    public final Handler.Callback A00;

    public AnonymousClass0VF(Handler.Callback callback) {
        this.A00 = callback;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        String str;
        C16700pc.A0E(message, 0);
        Object obj = message.obj;
        if (!(obj instanceof C04590Mh)) {
            str = null;
        } else if (obj != null) {
            str = ((C04590Mh) obj).A00;
            if (str != null) {
                C1093551j.A00.A6N(str);
            }
        } else {
            throw new NullPointerException("null cannot be cast to non-null type com.bloks.foa.core.surface.MessageParam");
        }
        try {
            return this.A00.handleMessage(message);
        } finally {
            if (str != null) {
                C1093551j.A00.A9T();
            }
        }
    }
}
