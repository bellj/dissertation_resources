package X;

import android.view.animation.Animation;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;

/* renamed from: X.2ny  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58002ny extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AnonymousClass3NY A00;

    public C58002ny(AnonymousClass3NY r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        ImageView imageView = this.A00.A01;
        imageView.post(new RunnableBRunnable0Shape10S0200000_I1(this, 30, imageView));
    }
}
