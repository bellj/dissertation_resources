package X;

import android.view.ViewConfiguration;

/* renamed from: X.0Qu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05750Qu {
    public static int A00(ViewConfiguration viewConfiguration) {
        return viewConfiguration.getScaledHoverSlop();
    }

    public static boolean A01(ViewConfiguration viewConfiguration) {
        return viewConfiguration.shouldShowMenuShortcutsWhenKeyboardPresent();
    }
}
