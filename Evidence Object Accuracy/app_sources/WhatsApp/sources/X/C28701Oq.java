package X;

import android.app.Application;
import android.content.Context;
import android.util.SparseArray;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1Oq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28701Oq {
    public static C14230l4 A00(C28671On r4, C14260l7 r5, List list, Map map) {
        C93634aU r42 = r4.A00;
        Map A04 = AnonymousClass3JI.A04(r42.A01, map);
        C14270l8 A03 = AnonymousClass3JV.A03(r5);
        if (A03.A09) {
            C28691Op.A00("BloksTreeManager", "Trying to add resources to a destroyed BloksTreeManager");
        } else {
            AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
            A03.A00(Collections.singletonList(r42));
            HashMap hashMap = new HashMap(A03.A08);
            if (r42.A02 != null) {
                hashMap.putAll(A03.A04.A03);
            }
            hashMap.putAll(A04);
            if (!hashMap.isEmpty()) {
                A03.A08 = hashMap;
            }
        }
        return C14230l4.A00(r5, list);
    }

    public static Object A01(C14260l7 r1, AnonymousClass28D r2, C14220l3 r3, AbstractC14200l1 r4) {
        return C14250l6.A00(C14230l4.A00(r1, r2.A06), r3, r4);
    }

    public static void A02(Context context, C28671On r6, C14220l3 r7, C64173En r8, Map map) {
        AnonymousClass28D r4 = new AnonymousClass28D(-1);
        C14270l8 r3 = new C14270l8(r6.A00, r4, AnonymousClass4ZD.A00, C1093551j.A00);
        if (context instanceof Application) {
            C28691Op.A00("BloksInterpreterHelper", "Creating BloksContext with Application Context. This may break the ability to execute navigation actions correctly");
        }
        C14260l7 A01 = AnonymousClass3JV.A01(context, new SparseArray(), r3, r8, r6.A02);
        r3.A01(A01, new AnonymousClass51W(), Collections.emptyMap());
        C14250l6.A00(A00(r6, A01, null, map), r7, r6.A01);
    }
}
