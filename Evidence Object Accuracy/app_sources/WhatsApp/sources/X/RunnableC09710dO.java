package X;

import androidx.work.impl.workers.ConstraintTrackingWorker;

/* renamed from: X.0dO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09710dO implements Runnable {
    public final /* synthetic */ ConstraintTrackingWorker A00;
    public final /* synthetic */ AbstractFutureC44231yX A01;

    public RunnableC09710dO(ConstraintTrackingWorker constraintTrackingWorker, AbstractFutureC44231yX r2) {
        this.A00 = constraintTrackingWorker;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        ConstraintTrackingWorker constraintTrackingWorker = this.A00;
        synchronized (constraintTrackingWorker.A03) {
            if (constraintTrackingWorker.A04) {
                constraintTrackingWorker.A04();
            } else {
                constraintTrackingWorker.A02.A08(this.A01);
            }
        }
    }
}
