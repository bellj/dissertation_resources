package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3qB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79183qB extends AbstractC113535Hy<Integer> implements AnonymousClass5Z5<Integer>, RandomAccess {
    public static final C79183qB A02;
    public int A00;
    public int[] A01;

    public C79183qB(int[] iArr, int i) {
        this.A01 = iArr;
        this.A00 = i;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C79183qB)) {
            return super.addAll(collection);
        }
        C79183qB r7 = (C79183qB) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.A01;
            if (i3 > iArr.length) {
                iArr = Arrays.copyOf(iArr, i3);
                this.A01 = iArr;
            }
            System.arraycopy(r7.A01, 0, iArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C79183qB)) {
                return super.equals(obj);
            }
            C79183qB r8 = (C79183qB) obj;
            int i = this.A00;
            if (i == r8.A00) {
                int[] iArr = r8.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == iArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + this.A01[i2];
        }
        return i;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean remove(Object obj) {
        A02();
        for (int i = 0; i < this.A00; i++) {
            if (obj.equals(Integer.valueOf(this.A01[i]))) {
                int[] iArr = this.A01;
                System.arraycopy(iArr, i + 1, iArr, i, this.A00 - i);
                this.A00--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            int[] iArr = this.A01;
            System.arraycopy(iArr, i2, iArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }

    static {
        C79183qB r0 = new C79183qB(new int[10], 0);
        A02 = r0;
        ((AbstractC113535Hy) r0).A00 = false;
    }

    public final void A03(int i) {
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
    }

    public final void A04(int i, int i2) {
        int i3;
        A02();
        if (i < 0 || i > (i3 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        int[] iArr = this.A01;
        if (i3 < iArr.length) {
            C72463ee.A0T(iArr, i, i3);
        } else {
            int[] iArr2 = new int[((i3 * 3) >> 1) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.A01, i, iArr2, i + 1, this.A00 - i);
            this.A01 = iArr2;
        }
        this.A01[i] = i2;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5Z5
    public final /* synthetic */ AnonymousClass5Z5 AhN(int i) {
        if (i >= this.A00) {
            return new C79183qB(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        A04(i, C12960it.A05(obj));
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        A03(i);
        return Integer.valueOf(this.A01[i]);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        A02();
        A03(i);
        int[] iArr = this.A01;
        int i2 = iArr[i];
        AbstractC113535Hy.A01(iArr, this.A00, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Integer.valueOf(i2);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        int A05 = C12960it.A05(obj);
        A02();
        A03(i);
        int[] iArr = this.A01;
        int i2 = iArr[i];
        iArr[i] = A05;
        return Integer.valueOf(i2);
    }
}
