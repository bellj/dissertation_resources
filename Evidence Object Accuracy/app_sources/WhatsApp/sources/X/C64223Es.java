package X;

import android.os.Build;

/* renamed from: X.3Es  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64223Es {
    public String A00;
    public final AnonymousClass11P A01;
    public final AnonymousClass018 A02;
    public final C21310xD A03;
    public final C22630zO A04;
    public final AbstractC15340mz A05;
    public final C237612x A06;
    public final C21280xA A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;

    public C64223Es(AnonymousClass11P r1, AnonymousClass018 r2, C21310xD r3, C22630zO r4, AbstractC15340mz r5, C237612x r6, C21280xA r7, boolean z, boolean z2, boolean z3) {
        this.A07 = r7;
        this.A02 = r2;
        this.A04 = r4;
        this.A03 = r3;
        this.A01 = r1;
        this.A06 = r6;
        this.A0A = z;
        this.A08 = z2;
        this.A09 = z3;
        this.A05 = r5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00de, code lost:
        if (r26.A0B() == false) goto L_0x00e0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C005602s A00(android.app.PendingIntent r21, android.app.PendingIntent r22, android.content.Context r23, X.C15370n3 r24, X.C42951wA r25, X.C33181da r26, X.C33181da r27, java.lang.CharSequence r28, java.lang.CharSequence r29, java.lang.StringBuilder r30, int r31, int r32, boolean r33, boolean r34) {
        /*
        // Method dump skipped, instructions count: 372
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64223Es.A00(android.app.PendingIntent, android.app.PendingIntent, android.content.Context, X.0n3, X.1wA, X.1da, X.1da, java.lang.CharSequence, java.lang.CharSequence, java.lang.StringBuilder, int, int, boolean, boolean):X.02s");
    }

    public final String A01(C33181da r4, C33181da r5, boolean z, boolean z2, boolean z3) {
        boolean z4;
        C35191hP r0;
        if (!z2 || (Build.VERSION.SDK_INT >= 28 && (!this.A06.A04().A05.isEmpty()))) {
            z4 = false;
        } else {
            z4 = true;
        }
        boolean equalsIgnoreCase = "Silent".equalsIgnoreCase(r4.A07());
        if (this.A08 || z || this.A09 || equalsIgnoreCase || !z3 || z4 || (((r0 = this.A01.A00) != null && r0.A0w) || this.A03.A00)) {
            return ((C33271dj) r4).A0D();
        }
        if (r5 != null) {
            return ((C33271dj) r5).A0C();
        }
        return ((C33271dj) r4).A0C();
    }
}
