package X;

import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1KB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KB {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;
    public final C37471mS[] A08;

    public AnonymousClass1KB(String str, String str2, String str3, String str4, String str5, C37471mS[] r6, boolean z, boolean z2, boolean z3) {
        this.A08 = r6;
        this.A01 = str;
        this.A02 = str2;
        this.A04 = str3;
        this.A03 = str4;
        this.A00 = str5;
        this.A06 = z;
        this.A07 = z2;
        this.A05 = z3;
    }

    public static AnonymousClass1KB A00(byte[] bArr) {
        String str;
        String str2;
        C37471mS[] r12;
        Object opt;
        if (bArr != null) {
            try {
                JSONObject jSONObject = new JSONObject(new String(bArr, AnonymousClass01V.A08));
                ArrayList arrayList = new ArrayList();
                if (jSONObject.has("emojis") && (opt = jSONObject.opt("emojis")) != null) {
                    if (opt instanceof JSONArray) {
                        JSONArray jSONArray = (JSONArray) opt;
                        for (int i = 0; i < jSONArray.length(); i++) {
                            String optString = jSONArray.optString(i, null);
                            if (optString != null) {
                                arrayList.add(C38491oB.A00(optString));
                            }
                        }
                    } else if (opt instanceof String) {
                        String str3 = (String) opt;
                        int length = str3.length();
                        if (length > 2) {
                            String[] split = str3.substring(1, length - 1).split(",");
                            for (String str4 : split) {
                                if (str4 != null) {
                                    arrayList.add(C38491oB.A00(str4));
                                }
                            }
                        }
                    } else {
                        StringBuilder sb = new StringBuilder("StickerMetadata/createFromWebpMetadata unrecognizable type of emoji metadata:");
                        sb.append(opt.getClass());
                        Log.w(sb.toString());
                    }
                }
                if (jSONObject.has("sticker-pack-id")) {
                    str2 = jSONObject.optString("sticker-pack-id", null);
                } else {
                    str2 = null;
                }
                if (!arrayList.isEmpty()) {
                    r12 = (C37471mS[]) arrayList.toArray(new C37471mS[0]);
                } else {
                    r12 = null;
                }
                String optString2 = jSONObject.optString("sticker-pack-name", null);
                String optString3 = jSONObject.optString("sticker-pack-publisher", null);
                String optString4 = jSONObject.optString("android-app-store-link", null);
                String optString5 = jSONObject.optString("ios-app-store-link", null);
                int optInt = jSONObject.optInt("is-first-party-sticker", 0);
                int optInt2 = jSONObject.optInt("is-from-sticker-maker", 0);
                int optInt3 = jSONObject.optInt("is-avatar-sticker", 0);
                boolean z = false;
                if (optInt == 1) {
                    z = true;
                }
                boolean z2 = false;
                if (optInt2 == 1) {
                    z2 = true;
                }
                boolean z3 = false;
                if (optInt3 == 1) {
                    z3 = true;
                }
                return new AnonymousClass1KB(str2, optString2, optString3, optString4, optString5, r12, z, z2, z3);
            } catch (UnsupportedEncodingException unused) {
                str = "WebpUtils/extractWebpMetadata invalid metadata encoding";
                Log.e(str);
                return null;
            } catch (JSONException unused2) {
                str = "WebpUtils/extractWebpMetadata invalid metadata";
                Log.e(str);
                return null;
            }
        }
        return null;
    }

    public byte[] A01() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("sticker-pack-id", this.A01);
            jSONObject.put("sticker-pack-name", this.A02);
            jSONObject.put("sticker-pack-publisher", this.A04);
            String str = this.A03;
            if (str != null) {
                jSONObject.put("android-app-store-link", str);
            }
            String str2 = this.A00;
            if (str2 != null) {
                jSONObject.put("ios-app-store-link", str2);
            }
            C37471mS[] r5 = this.A08;
            if (r5 != null) {
                int length = r5.length;
                ArrayList arrayList = new ArrayList(length);
                for (C37471mS r0 : r5) {
                    arrayList.add(r0.toString());
                }
                jSONObject.put("emojis", new JSONArray((Collection) arrayList));
            }
            if (this.A06) {
                jSONObject.put("is-first-party-sticker", 1);
            }
            if (this.A07) {
                jSONObject.put("is-from-sticker-maker", 1);
            }
            if (this.A05) {
                jSONObject.put("is-avatar-sticker", 1);
            }
            return jSONObject.toString().getBytes();
        } catch (JSONException e) {
            Log.e("StickerMetadata/convertToBytes error during JSON conversion", e);
            return null;
        }
    }

    public String toString() {
        String obj;
        StringBuffer stringBuffer = new StringBuffer("StickerMetadata{");
        stringBuffer.append("emojis=");
        C37471mS[] r0 = this.A08;
        if (r0 == null) {
            obj = "null";
        } else {
            obj = Arrays.asList(r0).toString();
        }
        stringBuffer.append(obj);
        stringBuffer.append(", isFirstPartySticker=");
        stringBuffer.append(this.A06);
        stringBuffer.append(", isFromStickerMaker=");
        stringBuffer.append(this.A07);
        stringBuffer.append(", isAvatarSticker=");
        stringBuffer.append(this.A05);
        stringBuffer.append('}');
        return stringBuffer.toString();
    }
}
