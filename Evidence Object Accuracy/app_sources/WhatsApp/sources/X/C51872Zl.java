package X;

import android.net.ConnectivityManager;
import android.net.Network;
import com.whatsapp.util.Log;

/* renamed from: X.2Zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51872Zl extends ConnectivityManager.NetworkCallback {
    public volatile Network A00;
    public final /* synthetic */ C43391ww A01;

    public C51872Zl(C43391ww r1) {
        this.A01 = r1;
    }

    public final void A00(Network network) {
        if (this.A00 == null) {
            return;
        }
        if (network == null || network.equals(this.A00)) {
            this.A00 = null;
            C43391ww r4 = this.A01;
            r4.A03.A00();
            C19890uq r0 = r4.A04;
            r0.A0D(-1, false);
            r0.A0I(false, false);
        }
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onAvailable(Network network) {
        StringBuilder A0k = C12960it.A0k("xmpp/handler/network/network-callback onAvailable:");
        A0k.append(network);
        A0k.append(" handle:");
        A0k.append(network.getNetworkHandle());
        C12960it.A1F(A0k);
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onBlockedStatusChanged(Network network, boolean z) {
        StringBuilder A0k = C12960it.A0k("xmpp/handler/network/network-callback onBlockedStatusChanged network:");
        A0k.append(network);
        A0k.append(" blocked:");
        A0k.append(z);
        A0k.append(" handle:");
        A0k.append(network.getNetworkHandle());
        C12960it.A1F(A0k);
        if (z) {
            A00(network);
            return;
        }
        this.A00 = network;
        C43391ww r1 = this.A01;
        boolean A01 = C43391ww.A01(network, r1);
        long networkHandle = network.getNetworkHandle();
        r1.A03.A00();
        C19890uq r12 = r1.A04;
        r12.A0D(networkHandle, C12960it.A1T(A01 ? 1 : 0));
        r12.A0I(A01, false);
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onLost(Network network) {
        Log.i(C12960it.A0b("xmpp/handler/network/network-callback onLost:", network));
        A00(network);
    }

    @Override // android.net.ConnectivityManager.NetworkCallback
    public void onUnavailable() {
        Log.i("xmpp/handler/network/network-callback onUnavailable");
        A00(null);
    }
}
