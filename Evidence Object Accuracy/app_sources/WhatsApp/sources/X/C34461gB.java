package X;

/* renamed from: X.1gB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34461gB extends AnonymousClass1JQ {
    public final int A00;

    public C34461gB(AnonymousClass1JR r10, String str, String str2, int i, long j, boolean z) {
        super(C27791Jf.A03, r10, str, str2, 3, j, z);
        this.A00 = i;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        AnonymousClass1G4 A0T = C34451gA.A02.A0T();
        int i = this.A00;
        A0T.A03();
        C34451gA r1 = (C34451gA) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = i;
        A01.A03();
        C27831Jk r12 = (C27831Jk) A01.A00;
        r12.A09 = (C34451gA) A0T.A02();
        r12.A00 |= 16384;
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("SentinelMutation{rowId=");
        sb.append(this.A07);
        sb.append(", expiredKeyEpoch=");
        sb.append(this.A00);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
