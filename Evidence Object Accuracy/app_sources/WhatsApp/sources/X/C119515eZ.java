package X;

import android.graphics.Color;
import org.json.JSONObject;

/* renamed from: X.5eZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119515eZ extends C130745zu {
    public final int A00;

    public C119515eZ(AnonymousClass1V8 r2) {
        super(r2);
        this.A00 = Color.parseColor(r2.A0H("color"));
    }

    public C119515eZ(JSONObject jSONObject) {
        super(jSONObject);
        int i;
        if (jSONObject.has("hex_rgb_color_with_pound_key")) {
            i = Color.parseColor(jSONObject.getString("hex_rgb_color_with_pound_key"));
        } else {
            i = jSONObject.getInt("color");
        }
        this.A00 = i;
    }

    @Override // X.C130745zu
    public JSONObject A01() {
        JSONObject A01 = super.A01();
        Object[] A1b = C12970iu.A1b();
        C12960it.A1O(A1b, this.A00);
        return A01.put("hex_rgb_color_with_pound_key", String.format("#%08X", A1b));
    }
}
