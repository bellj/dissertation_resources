package X;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1pH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39091pH extends AbstractC14550lc {
    public final C14650lo A00;
    public final C15550nR A01;
    public final AnonymousClass10T A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;
    public final C26501Ds A05;

    public C39091pH(C14650lo r3, C15550nR r4, AnonymousClass10T r5, C16590pI r6, AnonymousClass018 r7, C26501Ds r8, ThreadPoolExecutor threadPoolExecutor) {
        super(new C002601e(threadPoolExecutor, null));
        this.A03 = r6;
        this.A05 = r8;
        this.A01 = r4;
        this.A04 = r7;
        this.A02 = r5;
        this.A00 = r3;
    }

    public static ThreadPoolExecutor A00(AbstractC14440lR r10) {
        AnonymousClass1ID r0 = new AnonymousClass1ID((AnonymousClass168) r10, "VCardLoader", new PriorityBlockingQueue(), new AnonymousClass1I8("VCardLoader", 0), TimeUnit.SECONDS, 1, 1, 5, false);
        r0.allowCoreThreadTimeOut(true);
        return r0;
    }

    public void A06() {
        A02(new AbstractC14590lg() { // from class: X.5AI
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C39091pH.this.A05(obj);
            }
        });
        synchronized (this) {
            ((ThreadPoolExecutor) super.A00.get()).shutdown();
        }
    }
}
