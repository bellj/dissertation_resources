package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;

/* renamed from: X.0Gq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03210Gq extends AbstractC08040aU {
    public AnonymousClass0QR A00;
    public final AnonymousClass0QR A01;
    public final AbstractC08070aX A02;
    public final String A03;
    public final boolean A04;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C03210Gq(X.AnonymousClass0AA r13, X.C08190aj r14, X.AbstractC08070aX r15) {
        /*
            r12 = this;
            X.0Iw r0 = r14.A05
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x0042;
                case 1: goto L_0x003f;
                default: goto L_0x0009;
            }
        L_0x0009:
            android.graphics.Paint$Cap r3 = android.graphics.Paint.Cap.SQUARE
        L_0x000b:
            X.0Jg r0 = r14.A06
            android.graphics.Paint$Join r4 = r0.A00()
            float r11 = r14.A00
            X.0HA r8 = r14.A04
            X.0H9 r6 = r14.A03
            java.util.List r10 = r14.A08
            X.0H9 r7 = r14.A02
            r2 = r12
            r5 = r13
            r9 = r15
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            r12.A02 = r15
            java.lang.String r0 = r14.A07
            r12.A03 = r0
            boolean r0 = r14.A09
            r12.A04 = r0
            X.0H4 r0 = r14.A01
            java.util.List r0 = r0.A00
            X.0Gz r1 = new X.0Gz
            r1.<init>(r0)
            r12.A01 = r1
            java.util.List r0 = r1.A07
            r0.add(r12)
            r15.A03(r1)
            return
        L_0x003f:
            android.graphics.Paint$Cap r3 = android.graphics.Paint.Cap.ROUND
            goto L_0x000b
        L_0x0042:
            android.graphics.Paint$Cap r3 = android.graphics.Paint.Cap.BUTT
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03210Gq.<init>(X.0AA, X.0aj, X.0aX):void");
    }

    @Override // X.AbstractC08040aU, X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r3, Object obj) {
        super.A5q(r3, obj);
        if (obj == AbstractC12810iX.A0T) {
            this.A01.A08(r3);
        } else if (obj == AbstractC12810iX.A00) {
            AnonymousClass0QR r1 = this.A00;
            if (r1 != null) {
                this.A02.A0O.remove(r1);
            }
            if (r3 == null) {
                this.A00 = null;
                return;
            }
            AnonymousClass0Gu r0 = new AnonymousClass0Gu(r3, null);
            this.A00 = r0;
            r0.A07.add(this);
            this.A02.A03(this.A01);
        }
    }

    @Override // X.AbstractC08040aU, X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        if (!this.A04) {
            Paint paint = super.A01;
            C03230Gz r2 = (C03230Gz) this.A01;
            AnonymousClass0U8 AC6 = r2.A06.AC6();
            AnonymousClass0MI.A00();
            paint.setColor(r2.A09(AC6, r2.A01()));
            AnonymousClass0QR r0 = this.A00;
            if (r0 != null) {
                paint.setColorFilter((ColorFilter) r0.A03());
            }
            super.A9C(canvas, matrix, i);
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A03;
    }
}
