package X;

import android.media.AudioDeviceCallback;
import android.media.AudioDeviceInfo;

/* renamed from: X.3fm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73163fm extends AudioDeviceCallback {
    public final /* synthetic */ AnonymousClass39M A00;

    public C73163fm(AnonymousClass39M r1) {
        this.A00 = r1;
    }

    @Override // android.media.AudioDeviceCallback
    public void onAudioDevicesAdded(AudioDeviceInfo[] audioDeviceInfoArr) {
        this.A00.A00();
    }

    @Override // android.media.AudioDeviceCallback
    public void onAudioDevicesRemoved(AudioDeviceInfo[] audioDeviceInfoArr) {
        this.A00.A00();
    }
}
