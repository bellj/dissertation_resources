package X;

/* renamed from: X.1BM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BM {
    public static final AnonymousClass1W9[] A00(String str, String str2) {
        int i = 2;
        if (str2 != null) {
            i = 3;
        }
        AnonymousClass1W9[] r4 = new AnonymousClass1W9[i];
        r4[0] = new AnonymousClass1W9("name", str);
        r4[1] = new AnonymousClass1W9("value", "contact_blacklist");
        if (str2 != null) {
            r4[2] = new AnonymousClass1W9("dhash", str2);
        }
        return r4;
    }
}
