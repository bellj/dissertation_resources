package X;

import android.text.Editable;

/* renamed from: X.3gE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73433gE extends Editable.Factory {
    @Override // android.text.Editable.Factory
    public Editable newEditable(CharSequence charSequence) {
        return new C73463gH(charSequence);
    }
}
