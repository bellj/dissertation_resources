package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1k6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36571k6 extends BaseAdapter {
    public List A00 = new ArrayList();
    public final /* synthetic */ AbstractActivityC36551k4 A01;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    public /* synthetic */ C36571k6(AbstractActivityC36551k4 r2) {
        this.A01 = r2;
    }

    public final void A00(SelectionCheckView selectionCheckView, boolean z) {
        int i;
        AbstractActivityC36551k4 r1 = this.A01;
        if (r1.A0L) {
            i = R.string.status_contact_not_excluded_description;
            if (z) {
                i = R.string.status_contact_excluded_description;
            }
        } else {
            i = R.string.status_contact_not_selected_description;
            if (z) {
                i = R.string.status_contact_selected_description;
            }
        }
        selectionCheckView.setContentDescription(r1.getString(i));
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A00.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A00.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass4RH r0;
        C15370n3 r7 = (C15370n3) this.A00.get(i);
        if (view == null) {
            AbstractActivityC36551k4 r2 = this.A01;
            view = r2.getLayoutInflater().inflate(R.layout.status_contact_picker_row, viewGroup, false);
            r0 = new AnonymousClass4RH();
            view.setTag(r0);
            r0.A00 = (ImageView) view.findViewById(R.id.contactpicker_row_photo);
            r0.A01 = new C28801Pb(view, r2.A0B, r2.A0F, (int) R.id.contactpicker_row_name);
            r0.A02 = (SelectionCheckView) view.findViewById(R.id.selection_check);
            C27531Hw.A06(r0.A01.A01);
        } else {
            r0 = (AnonymousClass4RH) view.getTag();
        }
        view.setClickable(false);
        view.setLongClickable(false);
        Jid A0B = r7.A0B(UserJid.class);
        AnonymousClass009.A05(A0B);
        r0.A03 = (UserJid) A0B;
        AbstractActivityC36551k4 r5 = this.A01;
        r5.A0C.A06(r0.A00, r7);
        AnonymousClass028.A0a(r0.A00, 2);
        r0.A01.A07(r7, r5.A0I, -1);
        boolean contains = r5.A0U.contains(r7.A0B(UserJid.class));
        boolean z = r5.A0L;
        SelectionCheckView selectionCheckView = r0.A02;
        int i2 = R.drawable.teal_circle;
        if (z) {
            i2 = R.drawable.red_circle;
        }
        selectionCheckView.setSelectionBackground(i2);
        if (r5.A0T.remove(r7.A0B(UserJid.class))) {
            r0.A02.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC102004oR(this, r0, contains));
        } else {
            boolean A0I = r5.A06.A0I((UserJid) r7.A0B(UserJid.class));
            SelectionCheckView selectionCheckView2 = r0.A02;
            if (A0I) {
                selectionCheckView2.A04(r5.A0L, false);
                r0.A02.setContentDescription(r5.getString(R.string.tap_unblock));
                view.setAlpha(0.5f);
                return view;
            }
            selectionCheckView2.A04(contains, false);
            A00(r0.A02, contains);
        }
        view.setAlpha(1.0f);
        return view;
    }
}
