package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.3ma  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77003ma extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(11);
    public final byte[] A00;

    public C77003ma(Parcel parcel) {
        super(parcel.readString());
        this.A00 = parcel.createByteArray();
    }

    public C77003ma(String str, byte[] bArr) {
        super(str);
        this.A00 = bArr;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77003ma.class != obj.getClass()) {
                return false;
            }
            C77003ma r5 = (C77003ma) obj;
            if (!super.A00.equals(((AbstractC107404xH) r5).A00) || !Arrays.equals(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return C72453ed.A05(super.A00.hashCode()) + Arrays.hashCode(this.A00);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(super.A00);
        parcel.writeByteArray(this.A00);
    }
}
