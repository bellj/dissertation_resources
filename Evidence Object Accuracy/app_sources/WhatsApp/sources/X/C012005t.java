package X;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.TypedValue;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/* renamed from: X.05t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C012005t {
    public static C012005t A07;
    public static final PorterDuff.Mode A08 = PorterDuff.Mode.SRC_IN;
    public static final C012105u A09 = new C012105u();
    public TypedValue A00;
    public AnonymousClass061 A01;
    public AnonymousClass00O A02;
    public C013506i A03;
    public WeakHashMap A04;
    public boolean A05;
    public final WeakHashMap A06 = new WeakHashMap(0);

    public static synchronized PorterDuffColorFilter A00(PorterDuff.Mode mode, int i) {
        PorterDuffColorFilter porterDuffColorFilter;
        synchronized (C012005t.class) {
            C012105u r3 = A09;
            int i2 = (31 + i) * 31;
            porterDuffColorFilter = (PorterDuffColorFilter) r3.A04(Integer.valueOf(mode.hashCode() + i2));
            if (porterDuffColorFilter == null) {
                porterDuffColorFilter = new PorterDuffColorFilter(i, mode);
                r3.A08(Integer.valueOf(i2 + mode.hashCode()), porterDuffColorFilter);
            }
        }
        return porterDuffColorFilter;
    }

    public static synchronized C012005t A01() {
        C012005t r0;
        synchronized (C012005t.class) {
            if (A07 == null) {
                C012005t r2 = new C012005t();
                A07 = r2;
                if (Build.VERSION.SDK_INT < 24) {
                    r2.A08(new C012205v(), "vector");
                    r2.A08(new C012405x(), "animated-vector");
                    r2.A08(new C012505y(), "animated-selector");
                    r2.A08(new C012605z(), "drawable");
                }
            }
            r0 = A07;
        }
        return r0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(android.graphics.drawable.Drawable r3, X.C016107p r4, int[] r5) {
        /*
            boolean r0 = X.C014706y.A03(r3)
            if (r0 == 0) goto L_0x0014
            android.graphics.drawable.Drawable r0 = r3.mutate()
            if (r0 == r3) goto L_0x0014
            java.lang.String r1 = "ResourceManagerInternal"
            java.lang.String r0 = "Mutated drawable is not the same instance as the input."
            android.util.Log.d(r1, r0)
        L_0x0013:
            return
        L_0x0014:
            boolean r0 = r4.A02
            if (r0 != 0) goto L_0x0046
            boolean r0 = r4.A03
            if (r0 == 0) goto L_0x0042
            r2 = 0
        L_0x001d:
            boolean r0 = r4.A03
            if (r0 == 0) goto L_0x003f
            android.graphics.PorterDuff$Mode r1 = r4.A01
        L_0x0023:
            if (r2 == 0) goto L_0x003d
            if (r1 == 0) goto L_0x003d
            r0 = 0
            int r0 = r2.getColorForState(r5, r0)
            android.graphics.PorterDuffColorFilter r0 = A00(r1, r0)
        L_0x0030:
            r3.setColorFilter(r0)
        L_0x0033:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 > r0) goto L_0x0013
            r3.invalidateSelf()
            return
        L_0x003d:
            r0 = 0
            goto L_0x0030
        L_0x003f:
            android.graphics.PorterDuff$Mode r1 = X.C012005t.A08
            goto L_0x0023
        L_0x0042:
            r3.clearColorFilter()
            goto L_0x0033
        L_0x0046:
            android.content.res.ColorStateList r2 = r4.A00
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C012005t.A02(android.graphics.drawable.Drawable, X.07p, int[]):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x00fc A[Catch: all -> 0x011e, TryCatch #0 {, blocks: (B:3:0x0001, B:5:0x0006, B:7:0x000e, B:9:0x0016, B:11:0x001a, B:17:0x0031, B:19:0x0042, B:21:0x0048, B:22:0x0069, B:23:0x0072, B:28:0x009f, B:35:0x00b7, B:37:0x00bf, B:38:0x00c7, B:41:0x00d3, B:47:0x00eb, B:48:0x00f4, B:51:0x00fc, B:53:0x0100, B:54:0x0107, B:56:0x010f, B:57:0x0119), top: B:62:0x0001 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.content.res.ColorStateList A03(android.content.Context r9, int r10) {
        /*
        // Method dump skipped, instructions count: 289
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C012005t.A03(android.content.Context, int):android.content.res.ColorStateList");
    }

    public synchronized Drawable A04(Context context, int i) {
        return A05(context, i, false);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:50:0x00d5 */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.graphics.drawable.Drawable A05(android.content.Context r11, int r12, boolean r13) {
        /*
        // Method dump skipped, instructions count: 681
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C012005t.A05(android.content.Context, int, boolean):android.graphics.drawable.Drawable");
    }

    public final synchronized Drawable A06(Context context, long j) {
        WeakReference weakReference;
        AnonymousClass036 r3 = (AnonymousClass036) this.A06.get(context);
        if (!(r3 == null || (weakReference = (WeakReference) r3.A04(j, null)) == null)) {
            Drawable.ConstantState constantState = (Drawable.ConstantState) weakReference.get();
            if (constantState != null) {
                return constantState.newDrawable(context.getResources());
            }
            r3.A07(j);
        }
        return null;
    }

    public final synchronized void A07(Context context, Drawable drawable, long j) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState != null) {
            WeakHashMap weakHashMap = this.A06;
            AnonymousClass036 r1 = (AnonymousClass036) weakHashMap.get(context);
            if (r1 == null) {
                r1 = new AnonymousClass036();
                weakHashMap.put(context, r1);
            }
            r1.A09(j, new WeakReference(constantState));
        }
    }

    public final void A08(AbstractC012305w r2, String str) {
        AnonymousClass00O r0 = this.A02;
        if (r0 == null) {
            r0 = new AnonymousClass00O();
            this.A02 = r0;
        }
        r0.put(str, r2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A09(android.content.Context r6, android.graphics.drawable.Drawable r7, int r8) {
        /*
            r5 = this;
            X.061 r1 = r5.A01
            if (r1 == 0) goto L_0x0061
            X.060 r1 = (X.AnonymousClass060) r1
            android.graphics.PorterDuff$Mode r3 = X.C011905s.A02
            int[] r0 = r1.A02
            boolean r0 = X.AnonymousClass060.A02(r0, r8)
            r4 = 16842801(0x1010031, float:2.3693695E-38)
            r2 = -1
            if (r0 == 0) goto L_0x0034
            r4 = 2130968802(0x7f0400e2, float:1.7546268E38)
        L_0x0017:
            r1 = -1
        L_0x0018:
            boolean r0 = X.C014706y.A03(r7)
            if (r0 == 0) goto L_0x0022
            android.graphics.drawable.Drawable r7 = r7.mutate()
        L_0x0022:
            int r0 = X.AnonymousClass084.A01(r6, r4)
            android.graphics.PorterDuffColorFilter r0 = X.C011905s.A00(r3, r0)
            r7.setColorFilter(r0)
            if (r1 == r2) goto L_0x0032
            r7.setAlpha(r1)
        L_0x0032:
            r0 = 1
            return r0
        L_0x0034:
            int[] r0 = r1.A01
            boolean r0 = X.AnonymousClass060.A02(r0, r8)
            if (r0 == 0) goto L_0x0040
            r4 = 2130968800(0x7f0400e0, float:1.7546264E38)
            goto L_0x0017
        L_0x0040:
            int[] r0 = r1.A00
            boolean r0 = X.AnonymousClass060.A02(r0, r8)
            if (r0 == 0) goto L_0x004b
            android.graphics.PorterDuff$Mode r3 = android.graphics.PorterDuff.Mode.MULTIPLY
            goto L_0x0017
        L_0x004b:
            r0 = 2131231035(0x7f08013b, float:1.807814E38)
            if (r8 != r0) goto L_0x005b
            r4 = 16842800(0x1010030, float:2.3693693E-38)
            r0 = 1109603123(0x42233333, float:40.8)
            int r1 = java.lang.Math.round(r0)
            goto L_0x0018
        L_0x005b:
            r0 = 2131231017(0x7f080129, float:1.8078103E38)
            if (r8 != r0) goto L_0x0061
            goto L_0x0017
        L_0x0061:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C012005t.A09(android.content.Context, android.graphics.drawable.Drawable, int):boolean");
    }
}
