package X;

import android.os.SystemClock;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.MediaData;
import com.whatsapp.TextData;
import com.whatsapp.data.ProfilePhotoChange;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15340mz {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public long A0F;
    public long A0G;
    public long A0H;
    public long A0I;
    public AbstractC455722e A0J;
    public C32731ce A0K;
    public AnonymousClass1IR A0L;
    public AbstractC14640lm A0M;
    public C32361c2 A0N;
    public EnumC35661iT A0O;
    public AbstractC15340mz A0P;
    public C30431Xj A0Q;
    public AnonymousClass1IS A0R;
    public C16460p3 A0S;
    public C14360lJ A0T;
    public C32141bg A0U;
    public C40481rf A0V;
    public Integer A0W;
    public Long A0X;
    public Long A0Y;
    public Long A0Z;
    public Long A0a;
    public Long A0b;
    public Long A0c;
    public String A0d;
    public String A0e;
    public String A0f;
    public String A0g;
    public String A0h;
    public String A0i;
    public String A0j;
    public String A0k;
    public String A0l;
    public String A0m;
    public String A0n;
    public List A0o;
    public List A0p;
    public Map A0q;
    public boolean A0r;
    public boolean A0s;
    public boolean A0t;
    public boolean A0u;
    public boolean A0v;
    public boolean A0w;
    public byte[] A0x;
    public final byte A0y;
    public final AnonymousClass1IS A0z;
    public final Object A10;
    public volatile long A11;
    public volatile long A12;
    public volatile boolean A13;
    public transient long A14;
    public transient long A15;
    public transient long A16;
    public transient DeviceJid A17;
    public transient C30311Wx A18;
    public transient boolean A19;
    public transient boolean A1A;
    public transient boolean A1B;
    public transient boolean A1C;
    public transient byte[] A1D;
    public final transient long A1E;
    public volatile transient int A1F;

    public AbstractC15340mz(AbstractC15340mz r5, AnonymousClass1IS r6, byte b, long j, boolean z) {
        this(r6, b, j);
        ArrayList arrayList;
        AnonymousClass1IR r2;
        synchronized (r5.A10) {
            this.A0f = r5.A0f;
            this.A0x = r5.A0x;
        }
        this.A02 = r5.A02;
        this.A08 = r5.A08;
        A0v(r5.A0o);
        this.A0R = r5.A0R;
        if (z) {
            this.A0M = r5.A0B();
            this.A17 = r5.A17;
            if (r5.A0R() != null) {
                arrayList = new ArrayList(r5.A0R());
            } else {
                arrayList = null;
            }
            this.A0p = arrayList;
            this.A0l = r5.A0l;
            this.A0k = r5.A0k;
            this.A0g = r5.A0g;
            this.A0r = r5.A0r;
            this.A0A = r5.A0A;
            this.A0O = r5.A0O;
            this.A0W = r5.A0W;
            this.A0F = r5.A0F;
            this.A0P = r5.A0P;
            this.A0c = r5.A0c;
            this.A0E = r5.A0E;
            this.A0U = r5.A0U;
            this.A0b = r5.A0b;
            this.A0a = r5.A0a;
            this.A0Z = r5.A0Z;
            this.A01 = r5.A01;
            this.A09 = r5.A09;
            this.A05 = r5.A05;
            A02(r5, this, false);
            AnonymousClass1IR r3 = r5.A0L;
            if (r3 != null && (r2 = this.A0L) != null) {
                synchronized (r3) {
                    if (TextUtils.isEmpty(r2.A0K) || r2.A0K.equals(r3.A0K)) {
                        r2.A0K = r3.A0K;
                        r2.A02 = r3.A02;
                        r2.A06 = r3.A06;
                    }
                }
            }
        }
    }

    public AbstractC15340mz(AbstractC15340mz r8, AnonymousClass1IS r9, long j, boolean z) {
        this(r8, r9, r8.A0y, j, z);
    }

    public AbstractC15340mz(AnonymousClass1IS r5, byte b, long j) {
        this.A1B = false;
        this.A1C = false;
        this.A0H = -1;
        this.A0O = EnumC35661iT.NONE;
        this.A11 = -1;
        this.A12 = -1;
        this.A0W = null;
        this.A0h = null;
        this.A1A = false;
        this.A0D = 0;
        this.A00 = 0;
        this.A06 = 0;
        this.A10 = new Object();
        this.A0t = false;
        AnonymousClass009.A05(r5);
        this.A0z = r5;
        this.A0I = j;
        this.A0y = b;
        long uptimeMillis = SystemClock.uptimeMillis();
        this.A1E = uptimeMillis;
        this.A16 = uptimeMillis;
    }

    public static C16150oX A00(AbstractC16130oV r0) {
        C16150oX r02 = r0.A02;
        AnonymousClass009.A05(r02);
        return r02;
    }

    public static String A01(AbstractC16130oV r3) {
        String A00 = AnonymousClass14P.A00(r3.A06);
        Locale locale = Locale.US;
        String upperCase = A00.toUpperCase(locale);
        return (!TextUtils.isEmpty(upperCase) || TextUtils.isEmpty(r3.A16())) ? upperCase : C14350lI.A07(r3.A16()).toUpperCase(locale);
    }

    public static void A02(AbstractC15340mz r2, AbstractC15340mz r3, boolean z) {
        AbstractC15340mz r1;
        if (!(r2.A0G() == null || r3.A0G() == null || !r2.A0G().A05() || r2.A0G().A07() == null)) {
            r3.A0G().A03(r2.A0G().A07(), z);
        }
        AbstractC15340mz r22 = r2.A0P;
        if (r22 != null && (r1 = r3.A0P) != null) {
            A02(r22, r1, true);
        }
    }

    public int A03() {
        int i;
        synchronized (this.A10) {
            i = this.A02;
        }
        return i;
    }

    public int A04() {
        if (this instanceof C28861Ph) {
            return ((C28861Ph) this).A00;
        }
        if (this instanceof C30551Xw) {
            return ((C30551Xw) this).A00;
        }
        if (this instanceof AbstractC30571Xy) {
            return ((AbstractC30571Xy) this).A01 ? 1 : 0;
        }
        if (this instanceof C30521Xt) {
            return ((C30521Xt) this).A01;
        }
        if (this instanceof C30461Xm) {
            C30461Xm r1 = (C30461Xm) this;
            if (!(r1 instanceof AnonymousClass1Y3)) {
                return r1.A00;
            }
            return ((AnonymousClass1Y3) r1).A00;
        } else if (this instanceof AnonymousClass1Y1) {
            return ((AnonymousClass1Y1) this).A00;
        } else {
            if (this instanceof C30341Xa) {
                return ((C30341Xa) this).A00;
            }
            if (this instanceof C30361Xc) {
                return ((C30361Xc) this).A01;
            }
            if (this instanceof AbstractC16130oV) {
                AbstractC16130oV r12 = (AbstractC16130oV) this;
                if (!(r12 instanceof C16440p1)) {
                    return r12.A00;
                }
                return ((C16440p1) r12).A00;
            } else if (!(this instanceof C30401Xg)) {
                return 0;
            } else {
                return ((C30401Xg) this).A00;
            }
        }
    }

    public int A05() {
        if (this instanceof C28861Ph) {
            return ((C28861Ph) this).A01;
        }
        if (this instanceof AnonymousClass1Y5) {
            return ((AnonymousClass1Y5) this).A00;
        }
        if (this instanceof AbstractC30571Xy) {
            return ((AbstractC30571Xy) this).A00;
        }
        if (this instanceof C30531Xu) {
            return ((C30531Xu) this).A00;
        }
        if (this instanceof C30521Xt) {
            return ((C30521Xt) this).A00;
        }
        if (!(this instanceof C28581Od)) {
            return 0;
        }
        return ((C28581Od) this).A00;
    }

    public int A06() {
        if (!(this instanceof C30311Wx)) {
            return this.A06;
        }
        return ((C30311Wx) this).A01;
    }

    public int A07() {
        String str;
        if (!(this instanceof AbstractC16130oV)) {
            str = null;
        } else {
            str = ((AbstractC16130oV) this).A09;
        }
        if (TextUtils.isEmpty(str)) {
            A0U(64);
        }
        return this.A09;
    }

    public int A08() {
        if (!(this instanceof AnonymousClass1Iv)) {
            return this.A0D;
        }
        return 4;
    }

    public long A09() {
        if (this instanceof AnonymousClass1XB) {
            return (long) ((AnonymousClass1XB) this).A00;
        }
        if (this instanceof C30331Wz) {
            return ((C30331Wz) this).A00;
        }
        if (this instanceof AbstractC16130oV) {
            return ((AbstractC16130oV) this).A01;
        }
        if (this instanceof C30341Xa) {
            return ((C30341Xa) this).A01;
        }
        if (!(this instanceof C30401Xg)) {
            return 0;
        }
        return (long) ((C30401Xg) this).A01;
    }

    public long A0A() {
        long j;
        if (A0z()) {
            j = 1;
        } else {
            j = 0;
        }
        if (this.A0P != null || this.A0F > 0) {
            j |= 2;
        }
        if (this.A0m != null) {
            j |= 4;
        }
        return A0y() ? j | 8 : j;
    }

    public AbstractC14640lm A0B() {
        if (!(this instanceof C30511Xs)) {
            return this.A0M;
        }
        return null;
    }

    public UserJid A0C() {
        if (!(this instanceof AnonymousClass1XB)) {
            AbstractC14640lm r1 = this.A0z.A00;
            if (C15380n4.A0J(r1) || C15380n4.A0F(r1)) {
                r1 = this.A0M;
            }
            return (UserJid) r1;
        }
        AnonymousClass1XB r12 = (AnonymousClass1XB) this;
        if (!(r12 instanceof C30541Xv)) {
            return null;
        }
        return ((C30541Xv) r12).A01;
    }

    public AnonymousClass1G3 A0D(C39971qq r11) {
        if (this instanceof C28861Ph) {
            AnonymousClass1G3 A00 = C27081Fy.A00();
            C57712nV r0 = ((C27081Fy) A00.A00).A0D;
            if (r0 == null) {
                r0 = C57712nV.A0O;
            }
            AnonymousClass1G4 A0T = r0.A0T();
            if (A0I() != null) {
                String A0I = A0I();
                A0T.A03();
                C57712nV r1 = (C57712nV) A0T.A00;
                r1.A01 |= 1;
                r1.A0K = A0I;
            }
            AnonymousClass1PG r6 = r11.A04;
            byte[] bArr = r11.A09;
            if (C32411c7.A0U(r6, this, bArr)) {
                C43261wh A0P = C32411c7.A0P(r11.A00, r11.A02, r6, this, bArr, r11.A06);
                A0T.A03();
                C57712nV r12 = (C57712nV) A0T.A00;
                r12.A0F = A0P;
                r12.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
            }
            A00.A03();
            C27081Fy r13 = (C27081Fy) A00.A00;
            r13.A0D = (C57712nV) A0T.A02();
            r13.A00 |= 32;
            return A00;
        } else if (!(this instanceof C30061Vy)) {
            return null;
        } else {
            AnonymousClass1G3 A002 = C27081Fy.A00();
            C82343vT A1B = ((C30061Vy) this).A1B(r11);
            if (A1B != null) {
                A002.A03();
                C27081Fy r2 = (C27081Fy) A002.A00;
                r2.A0c = (C57702nU) A1B.A02();
                r2.A00 |= 2097152;
            }
            return A002;
        }
    }

    public AbstractC15340mz A0E() {
        if ((this instanceof AnonymousClass1XB) || (this instanceof AnonymousClass1XK) || (this instanceof AnonymousClass1Iv)) {
            return null;
        }
        return this.A0P;
    }

    public C30431Xj A0F() {
        C30431Xj r0 = this.A0Q;
        if (r0 != null) {
            return r0;
        }
        C30431Xj r02 = new C30431Xj();
        this.A0Q = r02;
        return r02;
    }

    public C16460p3 A0G() {
        C16460p3 r1;
        synchronized (this.A10) {
            r1 = this.A0S;
            if (r1 == null && C16460p3.A00(this.A0y)) {
                r1 = new C16460p3(this);
                this.A0S = r1;
            }
        }
        return r1;
    }

    public Object A0H() {
        String str;
        UserJid userJid;
        if (this instanceof C28861Ph) {
            C28861Ph r0 = (C28861Ph) this;
            TextData textData = r0.A02;
            if (textData == null) {
                return r0.A07;
            }
            return textData;
        } else if (this instanceof AnonymousClass1Y5) {
            String[][] strArr = (String[][]) Array.newInstance(String.class, 3, 2);
            Iterator it = ((AnonymousClass1Y5) this).A03.iterator();
            int i = 0;
            while (it.hasNext() && i < strArr.length) {
                AnonymousClass1OU r3 = (AnonymousClass1OU) it.next();
                strArr[i][0] = r3.A02.getRawString();
                strArr[i][1] = r3.A03;
                i++;
            }
            return strArr;
        } else if (this instanceof C30501Xq) {
            return ((C30501Xq) this).A00;
        } else {
            if (this instanceof C30541Xv) {
                AnonymousClass1IS r32 = ((C30541Xv) this).A02;
                if (r32 == null) {
                    return null;
                }
                String[] strArr2 = new String[3];
                AbstractC14640lm r02 = r32.A00;
                if (r02 != null) {
                    str = r02.getRawString();
                } else {
                    str = "null";
                }
                strArr2[0] = str;
                strArr2[1] = String.valueOf(r32.A02);
                strArr2[2] = r32.A01;
                return TextUtils.join(";", Arrays.asList(strArr2));
            } else if (this instanceof C30511Xs) {
                C30511Xs r2 = (C30511Xs) this;
                if (((AnonymousClass1XB) r2).A00 != 10 || (userJid = r2.A00) == null) {
                    return null;
                }
                return userJid.getRawString();
            } else if (this instanceof C30461Xm) {
                C30461Xm r1 = (C30461Xm) this;
                if (r1 instanceof C30451Xl) {
                    return ((C30451Xl) r1).A01;
                }
                if (r1.A01.size() > 0) {
                    return C15380n4.A06(r1.A01);
                }
                return null;
            } else if (this instanceof AbstractC16130oV) {
                C16150oX r03 = ((AbstractC16130oV) this).A02;
                if (r03 != null) {
                    return r03.A01();
                }
                return null;
            } else if (!(this instanceof AnonymousClass1XP)) {
                return null;
            } else {
                int i2 = ((AnonymousClass1XP) this).A02;
                if (i2 == 1) {
                    i2 = 0;
                }
                return Integer.valueOf(i2);
            }
        }
    }

    public String A0I() {
        String valueOf;
        String valueOf2;
        String str;
        String str2;
        String str3;
        byte[] bArr;
        if (this instanceof C30501Xq) {
            return ((C30501Xq) this).A15();
        }
        if (this instanceof AnonymousClass1Y2) {
            return ((AnonymousClass1Y2) this).A00;
        }
        if (this instanceof C30451Xl) {
            return ((C30451Xl) this).A15();
        }
        if (this instanceof AnonymousClass1Y0) {
            AnonymousClass1Y0 r0 = (AnonymousClass1Y0) this;
            synchronized (this) {
                valueOf = String.valueOf(r0.A00);
            }
            return valueOf;
        } else if (this instanceof C30581Xz) {
            C30581Xz r02 = (C30581Xz) this;
            synchronized (this) {
                valueOf2 = String.valueOf(r02.A00);
            }
            return valueOf2;
        } else if (this instanceof AnonymousClass1XE) {
            AnonymousClass1XE r1 = (AnonymousClass1XE) this;
            synchronized (r1.A10) {
                AnonymousClass1ZL r03 = r1.A00;
                if (r03 == null) {
                    str = null;
                } else if (TextUtils.isEmpty(r03.A01)) {
                    str = r1.A00.A03;
                } else {
                    StringBuilder sb = new StringBuilder();
                    AnonymousClass1ZL r12 = r1.A00;
                    sb.append(r12.A03);
                    sb.append("\n");
                    sb.append(r12.A01);
                    str = sb.toString();
                }
            }
            return str;
        } else if (!(this instanceof C30411Xh)) {
            synchronized (this.A10) {
                if (this.A02 == 1) {
                    str3 = null;
                } else {
                    str3 = this.A0f;
                    if (str3 == null && (bArr = this.A0x) != null) {
                        try {
                            str3 = new String(bArr, AnonymousClass01V.A08);
                        } catch (UnsupportedEncodingException unused) {
                            str3 = null;
                        }
                        this.A0f = str3;
                    }
                }
            }
            return str3;
        } else {
            C30411Xh r04 = (C30411Xh) this;
            synchronized (r04.A10) {
                str2 = r04.A01;
            }
            return str2;
        }
    }

    public String A0J() {
        AbstractC14640lm r0;
        UserJid userJid;
        if (!(this instanceof AnonymousClass1XB)) {
            List list = this.A0p;
            if (list != null) {
                return TextUtils.join(",", C15380n4.A0Q(list));
            }
            r0 = this.A0M;
        } else {
            AnonymousClass1XB r2 = (AnonymousClass1XB) this;
            if (r2 instanceof C30561Xx) {
                return ((C30561Xx) r2).A01;
            }
            if (r2 instanceof C30551Xw) {
                return ((C30541Xv) r2).A03;
            }
            if (!(r2 instanceof C30511Xs)) {
                r0 = r2.A0M;
            } else {
                C30511Xs r22 = (C30511Xs) r2;
                if (((AnonymousClass1XB) r22).A00 != 10 || (userJid = r22.A01) == null) {
                    return null;
                }
                return userJid.getRawString();
            }
        }
        return C15380n4.A03(r0);
    }

    public String A0K() {
        boolean z;
        if (this instanceof C28861Ph) {
            return ((C28861Ph) this).A05;
        }
        if (this instanceof AnonymousClass1Y5) {
            C15580nU r0 = ((AnonymousClass1Y5) this).A01;
            if (r0 != null) {
                return r0.getRawString();
            }
            return null;
        } else if (this instanceof C30541Xv) {
            C30541Xv r1 = (C30541Xv) this;
            if (r1 instanceof C30561Xx) {
                return ((C30561Xx) r1).A03;
            }
            if (!(r1 instanceof C30551Xw)) {
                return r1.A03;
            }
            return ((C30551Xw) r1).A02;
        } else if (!(this instanceof C30511Xs)) {
            if (this instanceof AnonymousClass1Y2) {
                z = ((AnonymousClass1Y2) this).A01;
            } else if (this instanceof AnonymousClass1Y4) {
                return ((AnonymousClass1Y4) this).A00;
            } else {
                if (this instanceof C32231bp) {
                    return ((C32231bp) this).A00;
                }
                if (this instanceof C30481Xo) {
                    return ((C30481Xo) this).A00;
                }
                if (this instanceof AnonymousClass1Y3) {
                    GroupJid groupJid = ((AnonymousClass1Y3) this).A01;
                    if (groupJid != null) {
                        return groupJid.getRawString();
                    }
                    return null;
                } else if (this instanceof AnonymousClass1XX) {
                    return ((AnonymousClass1XX) this).A00;
                } else {
                    if (this instanceof AbstractC16130oV) {
                        return ((AbstractC16130oV) this).A03;
                    }
                    if (this instanceof C30341Xa) {
                        return ((C30341Xa) this).A03;
                    }
                    if (this instanceof C28581Od) {
                        return ((C28581Od) this).A04;
                    }
                    if (this instanceof C30241Wq) {
                        return ((C30241Wq) this).A01;
                    }
                    if (this instanceof C30321Wy) {
                        return C15380n4.A03(((C30321Wy) this).A00);
                    }
                    if (!(this instanceof AbstractC30391Xf)) {
                        return null;
                    }
                    AbstractC30391Xf r12 = (AbstractC30391Xf) this;
                    if (!r12.A00) {
                        return null;
                    }
                    z = r12.A01;
                }
            }
            if (z) {
                return "video";
            }
            return "audio";
        } else {
            C30511Xs r4 = (C30511Xs) this;
            int i = ((AnonymousClass1XB) r4).A00;
            if (i == 28) {
                if (r4.A01 == null) {
                    r4.A03.AaV("sys-msg/number-change/old-jid-persist-null", r4.A15(), true);
                }
            } else if (i != 28) {
                return null;
            }
            UserJid userJid = r4.A01;
            if (userJid != null) {
                return userJid.getRawString();
            }
            return null;
        }
    }

    public String A0L() {
        if (this instanceof C30541Xv) {
            C30541Xv r1 = (C30541Xv) this;
            if (r1 instanceof C30561Xx) {
                return ((C30561Xx) r1).A04;
            }
            if (!(r1 instanceof C30551Xw)) {
                return C15380n4.A03(r1.A01);
            }
            return ((C30551Xw) r1).A03;
        } else if (!(this instanceof AbstractC16130oV)) {
            return null;
        } else {
            return ((AbstractC16130oV) this).A04;
        }
    }

    public String A0M() {
        if (this instanceof C30541Xv) {
            C30541Xv r1 = (C30541Xv) this;
            if (r1 instanceof C30561Xx) {
                return ((C30561Xx) r1).A00;
            }
            if (!(r1 instanceof C30551Xw)) {
                return C15380n4.A03(r1.A00);
            }
            return ((C30551Xw) r1).A01;
        } else if (!(this instanceof AbstractC16130oV)) {
            return null;
        } else {
            return ((AbstractC16130oV) this).A05;
        }
    }

    public String A0N() {
        if (this instanceof C28861Ph) {
            return ((C28861Ph) this).A04;
        }
        if (!(this instanceof AbstractC16130oV)) {
            return null;
        }
        return ((AbstractC16130oV) this).A06;
    }

    public String A0O() {
        if (this instanceof C28861Ph) {
            return ((C28861Ph) this).A03;
        }
        if (this instanceof C30511Xs) {
            C30511Xs r4 = (C30511Xs) this;
            int i = ((AnonymousClass1XB) r4).A00;
            if (i == 28) {
                if (r4.A00 == null) {
                    r4.A03.AaV("sys-msg/number-change/new-jid-persist-null", r4.A15(), true);
                }
            } else if (i != 28) {
                return null;
            }
            UserJid userJid = r4.A00;
            if (userJid != null) {
                return userJid.getRawString();
            }
            return null;
        } else if (this instanceof C30491Xp) {
            return ((C30491Xp) this).A00;
        } else {
            if (this instanceof C30331Wz) {
                return ((C30331Wz) this).A01;
            }
            if (this instanceof AnonymousClass1XO) {
                return ((AnonymousClass1XO) this).A17();
            }
            if (this instanceof C30341Xa) {
                C30751Yr r42 = ((C30341Xa) this).A02;
                if (r42 == null) {
                    return null;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(r42.A06.getRawString());
                sb.append(",");
                sb.append(Double.toString(r42.A00));
                sb.append(",");
                sb.append(Double.toString(r42.A01));
                sb.append(",");
                sb.append(Long.toString(r42.A05));
                return sb.toString();
            } else if (this instanceof AbstractC16130oV) {
                AbstractC16130oV r1 = (AbstractC16130oV) this;
                if (!(r1 instanceof C16440p1)) {
                    return r1.A07;
                }
                return r1.A16();
            } else if (this instanceof C30351Xb) {
                return ((C30351Xb) this).A00;
            } else {
                if (this instanceof C30411Xh) {
                    return ((C30411Xh) this).A00;
                }
                if (this instanceof C30401Xg) {
                    C30401Xg r12 = (C30401Xg) this;
                    if (((AbstractC30391Xf) r12).A00) {
                        return Long.toString(r12.A02);
                    }
                    return null;
                } else if (this instanceof C30241Wq) {
                    return ((C30241Wq) this).A00;
                } else {
                    return null;
                }
            }
        }
    }

    public String A0P() {
        if (this instanceof C28861Ph) {
            return ((C28861Ph) this).A06;
        }
        if (this instanceof C30561Xx) {
            return ((C30561Xx) this).A02;
        }
        if (this instanceof C30551Xw) {
            return ((C30551Xw) this).A04;
        }
        if (this instanceof AnonymousClass1Y3) {
            Integer num = ((AnonymousClass1Y3) this).A02;
            if (num != null) {
                return Integer.toString(num.intValue());
            }
            return null;
        } else if (this instanceof AnonymousClass1Y1) {
            return ((AnonymousClass1Y1) this).A01;
        } else {
            if (this instanceof AnonymousClass1XO) {
                return ((AnonymousClass1XO) this).A02;
            }
            if (this instanceof AbstractC16130oV) {
                return ((AbstractC16130oV) this).A08;
            }
            return null;
        }
    }

    public String A0Q() {
        String str;
        if (this instanceof C28861Ph) {
            return A0I();
        }
        if (this instanceof AnonymousClass1XB) {
            AnonymousClass1XB r2 = (AnonymousClass1XB) this;
            if (!(r2 instanceof C30451Xl)) {
                return r2.A0I();
            }
            C30451Xl r22 = (C30451Xl) r2;
            synchronized (r22.A10) {
                str = r22.A00;
            }
            return str;
        } else if (this instanceof C30331Wz) {
            return null;
        } else {
            if (this instanceof C27671Iq) {
                return ((C27671Iq) this).A02;
            }
            if ((this instanceof AbstractC30251Wr) || (this instanceof AbstractC30271Wt)) {
                return null;
            }
            if (this instanceof AnonymousClass1XX) {
                return ((AnonymousClass1XX) this).A00;
            }
            if ((this instanceof AnonymousClass1XC) || (this instanceof C30291Wv)) {
                return null;
            }
            if (this instanceof AbstractC16130oV) {
                return ((AbstractC16130oV) this).A03;
            }
            if (this instanceof AnonymousClass1XO) {
                return null;
            }
            if (this instanceof C30341Xa) {
                return ((C30341Xa) this).A03;
            }
            if ((this instanceof AnonymousClass1X5) || (this instanceof C16380ov)) {
                return null;
            }
            if (this instanceof C28581Od) {
                return ((C28581Od) this).A04;
            }
            if ((this instanceof C30361Xc) || (this instanceof AnonymousClass1XK)) {
                return null;
            }
            if (this instanceof C30351Xb) {
                return ((C30351Xb) this).A00;
            }
            if (this instanceof C30411Xh) {
                return ((C30411Xh) this).A00;
            }
            return null;
        }
    }

    public List A0R() {
        boolean z;
        String str;
        if (!(this instanceof AnonymousClass1XB)) {
            return this.A0p;
        }
        AnonymousClass1XB r1 = (AnonymousClass1XB) this;
        if (r1 instanceof C30541Xv) {
            z = false;
            str = "should not be called for FMessageSystemPayment";
        } else if (r1 instanceof C30461Xm) {
            return ((C30461Xm) r1).A01;
        } else {
            z = false;
            str = "should not be called for FMessageSystem";
        }
        AnonymousClass009.A0A(str, z);
        return null;
    }

    public void A0S() {
        String str;
        if (this instanceof AnonymousClass1XB) {
            str = "Cannot change status for FMessageSystem";
        } else if (!(this instanceof AbstractC30391Xf)) {
            this.A0C = 0;
            return;
        } else {
            str = "Cannot change status for calls message type";
        }
        AnonymousClass009.A07(str);
    }

    public void A0T(int i) {
        this.A09 = i | this.A09;
    }

    public void A0U(int i) {
        this.A09 = (i ^ -1) & this.A09;
    }

    public void A0V(int i) {
        if (this instanceof AnonymousClass1XK) {
            return;
        }
        if (i <= 0) {
            this.A04 = 0;
            A0U(256);
            return;
        }
        this.A04 = i;
        A0T(256);
    }

    public void A0W(int i) {
        if (this instanceof C28861Ph) {
            ((C28861Ph) this).A00 = i;
        } else if (this instanceof C30551Xw) {
            ((C30551Xw) this).A00 = i;
        } else if (this instanceof AbstractC30571Xy) {
            AbstractC30571Xy r1 = (AbstractC30571Xy) this;
            boolean z = true;
            if (i != 1) {
                z = false;
            }
            r1.A01 = z;
        } else if (this instanceof C30521Xt) {
            ((C30521Xt) this).A01 = i;
        } else if (this instanceof C30461Xm) {
            C30461Xm r12 = (C30461Xm) this;
            if (!(r12 instanceof AnonymousClass1Y3)) {
                r12.A00 = i;
            } else {
                ((AnonymousClass1Y3) r12).A00 = i;
            }
        } else if (this instanceof AnonymousClass1Y1) {
            ((AnonymousClass1Y1) this).A00 = i;
        } else if (this instanceof C30341Xa) {
            ((C30341Xa) this).A00 = i;
        } else if (this instanceof C30361Xc) {
            ((C30361Xc) this).A01 = i;
        } else if (this instanceof AbstractC16130oV) {
            AbstractC16130oV r13 = (AbstractC16130oV) this;
            if (!(r13 instanceof C16440p1)) {
                r13.A00 = i;
            } else {
                ((C16440p1) r13).A00 = i;
            }
        } else if (this instanceof C30401Xg) {
            C30401Xg r14 = (C30401Xg) this;
            if (i > 0) {
                ((AbstractC30391Xf) r14).A00 = true;
                r14.A00 = i;
            }
        }
    }

    public void A0X(int i) {
        if (this instanceof C28861Ph) {
            ((C28861Ph) this).A01 = i;
        } else if (this instanceof AnonymousClass1Y5) {
            ((AnonymousClass1Y5) this).A00 = i;
        } else if (this instanceof AbstractC30571Xy) {
            ((AbstractC30571Xy) this).A00 = i;
        } else if (this instanceof C30531Xu) {
            C30531Xu r1 = (C30531Xu) this;
            if (!r1.A01) {
                switch (i) {
                    case 1:
                        i = 8;
                        break;
                    case 2:
                        i = 6;
                        break;
                    case 3:
                        i = 10;
                        break;
                    case 4:
                        i = 7;
                        break;
                    case 5:
                        i = 5;
                        break;
                    case 6:
                        i = 9;
                        break;
                    default:
                        i = 0;
                        break;
                }
            }
            r1.A00 = i;
        } else if (this instanceof C30521Xt) {
            ((C30521Xt) this).A00 = i;
        } else if (this instanceof C28581Od) {
            ((C28581Od) this).A00 = i;
        }
    }

    public void A0Y(int i) {
        if (C37381mH.A00(this.A0C, i) > 0) {
            StringBuilder sb = new StringBuilder("FMessage/setStatus/statusDowngrade/key=");
            sb.append(this.A0z);
            sb.append("; type=");
            sb.append((int) this.A0y);
            sb.append("; current=");
            sb.append(this.A0C);
            sb.append("; new=");
            sb.append(i);
            Log.e(sb.toString());
        }
        this.A0C = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0005, code lost:
        if (r3 == r5) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Z(int r5) {
        /*
            r4 = this;
            int r3 = r4.A0D
            if (r3 == 0) goto L_0x0007
            r2 = 0
            if (r3 != r5) goto L_0x0008
        L_0x0007:
            r2 = 1
        L_0x0008:
            java.lang.String r0 = "FMessage/setStorageType/should only update storage type when it is undefined; current="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r3)
            java.lang.String r0 = "; new="
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
            X.AnonymousClass009.A0A(r0, r2)
            r4.A0D = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC15340mz.A0Z(int):void");
    }

    public void A0a(long j) {
        if (this instanceof C30331Wz) {
            ((C30331Wz) this).A00 = j;
        } else if (this instanceof AbstractC16130oV) {
            ((AbstractC16130oV) this).A01 = j;
        } else if (this instanceof C30341Xa) {
            ((C30341Xa) this).A01 = j;
        } else if (this instanceof C30401Xg) {
            C30401Xg r3 = (C30401Xg) this;
            if (j > 0) {
                ((AbstractC30391Xf) r3).A00 = true;
                r3.A01 = (int) j;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x012e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0b(android.database.Cursor r8) {
        /*
        // Method dump skipped, instructions count: 309
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC15340mz.A0b(android.database.Cursor):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:77:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0201  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0214  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x026d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0c(android.database.Cursor r8, X.C18460sU r9) {
        /*
        // Method dump skipped, instructions count: 1030
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC15340mz.A0c(android.database.Cursor, X.0sU):void");
    }

    public void A0d(AnonymousClass1IR r14, C39971qq r15) {
        C27081Fy r3;
        int i;
        int i2;
        AnonymousClass20C r9;
        long A07;
        int i3;
        if (r14.A0F()) {
            C30821Yy r0 = r14.A08;
            AnonymousClass009.A05(r0);
            long longValue = r0.A00.scaleByPowerOfTen(3).longValue();
            String str = r14.A0I;
            AbstractC30891Zf r02 = r14.A0A;
            if (r02 == null) {
                r9 = null;
                A07 = 0;
            } else {
                r9 = r02.A01;
                A07 = r02.A07();
            }
            UserJid userJid = r14.A0E;
            C30921Zi A01 = r14.A01();
            AnonymousClass1G3 r5 = r15.A03;
            C57572nH r4 = ((C27081Fy) r5.A00).A0Y;
            if (r4 == null) {
                r4 = C57572nH.A08;
            }
            AnonymousClass1G4 A0T = r4.A0T();
            AnonymousClass1G3 A0D = A0D(r15);
            if (A0D != null) {
                A0T.A03();
                C57572nH r10 = (C57572nH) A0T.A00;
                r10.A03 = (C27081Fy) A0D.A02();
                r10.A00 |= 1;
            }
            A0T.A03();
            C57572nH r102 = (C57572nH) A0T.A00;
            r102.A00 |= 4;
            r102.A01 = longValue;
            A0T.A03();
            C57572nH r103 = (C57572nH) A0T.A00;
            r103.A00 |= 2;
            r103.A06 = str;
            AnonymousClass2n0 r8 = r103.A04;
            if (r8 == null) {
                r8 = AnonymousClass2n0.A04;
            }
            AnonymousClass1G4 A0T2 = r8.A0T();
            if (r9 != null) {
                longValue = (long) r9.A01();
            }
            A0T2.A03();
            AnonymousClass2n0 r11 = (AnonymousClass2n0) A0T2.A00;
            r11.A00 |= 1;
            r11.A02 = longValue;
            if (r9 != null) {
                i3 = r9.A00;
            } else {
                i3 = 1000;
            }
            A0T2.A03();
            AnonymousClass2n0 r32 = (AnonymousClass2n0) A0T2.A00;
            r32.A00 |= 2;
            r32.A01 = i3;
            if (r9 != null) {
                str = ((AbstractC30781Yu) r9.A01).A04;
            }
            A0T2.A03();
            AnonymousClass2n0 r33 = (AnonymousClass2n0) A0T2.A00;
            r33.A00 |= 4;
            r33.A03 = str;
            A0T.A03();
            C57572nH r34 = (C57572nH) A0T.A00;
            r34.A04 = (AnonymousClass2n0) A0T2.A02();
            r34.A00 |= 32;
            A0T.A03();
            C57572nH r35 = (C57572nH) A0T.A00;
            r35.A00 |= 16;
            r35.A02 = A07 / 1000;
            if (userJid != null) {
                String rawString = userJid.getRawString();
                A0T.A03();
                C57572nH r1 = (C57572nH) A0T.A00;
                r1.A00 |= 8;
                r1.A07 = rawString;
            }
            if (A01 != null) {
                C57622nM A012 = A01.A01();
                A0T.A03();
                C57572nH r12 = (C57572nH) A0T.A00;
                r12.A05 = A012;
                r12.A00 |= 64;
            }
            r5.A03();
            r3 = (C27081Fy) r5.A00;
            r3.A0Y = (C57572nH) A0T.A02();
            i = r3.A00;
            i2 = C25981Bo.A0F;
        } else {
            String str2 = r14.A0M;
            UserJid userJid2 = r14.A0D;
            C30921Zi A013 = r14.A01();
            AnonymousClass1G3 r36 = r15.A03;
            C57412mz r03 = ((C27081Fy) r36.A00).A0Z;
            if (r03 == null) {
                r03 = C57412mz.A04;
            }
            AnonymousClass1G4 A0T3 = r03.A0T();
            if (str2 != null) {
                AnonymousClass1G9 r42 = (AnonymousClass1G9) AnonymousClass1G8.A05.A0T();
                r42.A05(str2);
                r42.A08(false);
                AbstractC14640lm r13 = this.A0z.A00;
                if (C15380n4.A0J(r13) && userJid2 != null) {
                    r42.A06(userJid2.getRawString());
                }
                r42.A07(C15380n4.A03(r13));
                A0T3.A03();
                C57412mz r16 = (C57412mz) A0T3.A00;
                r16.A03 = (AnonymousClass1G8) r42.A02();
                r16.A00 |= 2;
            }
            if (A013 != null) {
                C57622nM A014 = A013.A01();
                A0T3.A03();
                C57412mz r17 = (C57412mz) A0T3.A00;
                r17.A02 = A014;
                r17.A00 |= 4;
            }
            AnonymousClass1G3 A0D2 = A0D(r15);
            if (A0D2 != null) {
                A0T3.A03();
                C57412mz r18 = (C57412mz) A0T3.A00;
                r18.A01 = (C27081Fy) A0D2.A02();
                r18.A00 |= 1;
            }
            r36.A03();
            r3 = (C27081Fy) r36.A00;
            r3.A0Z = (C57412mz) A0T3.A02();
            i = r3.A00;
            i2 = 32768;
        }
        r3.A00 = i | i2;
    }

    public void A0e(AbstractC14640lm r5) {
        if (!(this instanceof AnonymousClass1XB)) {
            this.A0M = r5;
            this.A0p = null;
            return;
        }
        AnonymousClass1XB r2 = (AnonymousClass1XB) this;
        if (!(r2 instanceof C30541Xv)) {
            if (!(r2 instanceof C30511Xs) && r5 != null) {
                if (!r2.A14()) {
                    StringBuilder sb = new StringBuilder("FMessageSystem/setRemoteResourceJid/should not be called for FMessageSystem, key = ");
                    sb.append(r2.A0z.toString());
                    sb.append(" action = ");
                    sb.append(r2.A00);
                    Log.e(sb.toString());
                    return;
                }
            } else {
                return;
            }
        } else if (r5 == null) {
            return;
        } else {
            if (!r2.A14()) {
                StringBuilder sb2 = new StringBuilder("should not be called for FMessageSystem, key = ");
                sb2.append(r2.A0z.toString());
                sb2.append(" action = ");
                sb2.append(r2.A00);
                AnonymousClass009.A0A(sb2.toString(), false);
            }
        }
        r2.A0M = r5;
    }

    public void A0f(AbstractC15340mz r4) {
        ArrayList arrayList;
        int i;
        this.A11 = r4.A11;
        this.A12 = r4.A12;
        this.A0I = r4.A0I;
        this.A0G = r4.A0G;
        this.A0M = r4.A0B();
        this.A17 = r4.A17;
        if (r4.A0R() != null) {
            arrayList = new ArrayList(r4.A0R());
        } else {
            arrayList = null;
        }
        this.A0p = arrayList;
        this.A0H = r4.A0H;
        this.A0s = r4.A0s;
        this.A13 = r4.A13;
        AnonymousClass1IR r2 = r4.A0L;
        if (r2 != null && (this.A0L == null || !((i = r2.A03) == 4 || i == 20))) {
            this.A0m = r4.A0m;
            this.A0L = r2;
        }
        int i2 = r4.A04;
        if (i2 > 0) {
            this.A04 = i2;
            this.A0Y = r4.A0Y;
            this.A06 = r4.A06;
        }
    }

    public void A0g(AbstractC15340mz r4) {
        boolean z;
        String str;
        if (r4 != null) {
            if ((this instanceof AnonymousClass1XB) || (this instanceof AnonymousClass1XK) || (this instanceof AnonymousClass1Iv)) {
                z = false;
            } else {
                z = true;
            }
            boolean z2 = false;
            if (!z) {
                str = "message type is not allowed to have a quoted message";
            } else if ((r4 instanceof AnonymousClass1XB) || (r4 instanceof AnonymousClass1XK) || (r4 instanceof AnonymousClass1Iv)) {
                str = "message type can not be a quoted message";
            } else {
                if (r4.A08() == 2) {
                    z2 = true;
                }
                AnonymousClass009.A0A("quoted message should be marked StorageType.QUOTED", z2);
                r4.A0g(null);
            }
            AnonymousClass009.A0A(str, false);
            return;
        }
        this.A0P = r4;
    }

    public void A0h(C30441Xk r5) {
        A0F().A00 = r5;
        this.A15 |= 8;
    }

    public void A0i(C14360lJ r2) {
        this.A0T = r2;
        if (r2 == null) {
            A0U(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
        } else {
            A0T(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
        }
    }

    public void A0j(Long l) {
        if (this instanceof AnonymousClass1XK) {
            AnonymousClass009.A0A("For FMessageEphemeralSettingChange the setting timestamp is the timestamp", false);
        }
        this.A0X = l;
    }

    public void A0k(Object obj) {
        Object obj2;
        C16150oX r12;
        C16150oX r122;
        int i;
        if (this instanceof C28861Ph) {
            C28861Ph r1 = (C28861Ph) this;
            if (obj instanceof TextData) {
                r1.A15((TextData) obj);
            } else if ((obj instanceof byte[]) || obj == null) {
                r1.A07 = (byte[]) obj;
            } else {
                StringBuilder sb = new StringBuilder("FMessageText/setObjectForDatabaseFieldThumbImage/setting wrong object; object.class=");
                sb.append(obj.getClass());
                AnonymousClass009.A07(sb.toString());
            }
        } else if (this instanceof AnonymousClass1Y5) {
            AnonymousClass1Y5 r13 = (AnonymousClass1Y5) this;
            if (obj instanceof String[][]) {
                try {
                    String[][] strArr = (String[][]) obj;
                    Set set = r13.A03;
                    set.clear();
                    for (int i2 = 0; i2 < strArr.length; i2++) {
                        C15580nU A04 = C15580nU.A04(strArr[i2][0]);
                        String str = strArr[i2][1];
                        if (!(A04 == null || str == null)) {
                            set.add(new AnonymousClass1OU(A04, str, 2, 0));
                        }
                    }
                } catch (Exception e) {
                    Log.e(e);
                }
            }
        } else if (this instanceof C30501Xq) {
            C30501Xq r14 = (C30501Xq) this;
            if (obj instanceof ProfilePhotoChange) {
                r14.A00 = (ProfilePhotoChange) obj;
            }
        } else if (this instanceof C30541Xv) {
            C30541Xv r5 = (C30541Xv) this;
            if (obj instanceof String) {
                String[] split = ((String) obj).split(";");
                boolean z = false;
                if (split.length == 3) {
                    z = true;
                }
                AnonymousClass009.A0A("Wrong format of expired reference key.", z);
                r5.A02 = new AnonymousClass1IS(UserJid.getNullable(split[0]), split[2], Boolean.valueOf(split[1]).booleanValue());
            }
        } else if (this instanceof C30511Xs) {
            C30511Xs r2 = (C30511Xs) this;
            if (((AnonymousClass1XB) r2).A00 == 10 && (obj instanceof String)) {
                r2.A00 = UserJid.getNullable((String) obj);
            }
        } else if (this instanceof C30461Xm) {
            C30461Xm r15 = (C30461Xm) this;
            if (r15 instanceof C30451Xl) {
                C30451Xl r16 = (C30451Xl) r15;
                if (obj instanceof String) {
                    r16.A01 = (String) obj;
                }
            } else if (obj instanceof List) {
                List list = (List) obj;
                if (list.size() > 0 && (list.get(0) instanceof String)) {
                    r15.A01 = C15380n4.A07(UserJid.class, list);
                }
            }
        } else if (this instanceof AbstractC16130oV) {
            AbstractC16130oV r22 = (AbstractC16130oV) this;
            if (obj instanceof C16150oX) {
                r12 = (C16150oX) obj;
            } else if (obj instanceof MediaData) {
                r12 = C16150oX.A00((MediaData) obj);
            } else {
                StringBuilder sb2 = new StringBuilder("FMessageMedia/setObjectForDatabaseFieldThumbImage/setting wrong object; object.class=");
                if (obj == null) {
                    obj2 = "null";
                } else {
                    obj2 = obj.getClass();
                }
                sb2.append(obj2);
                Log.e(sb2.toString());
                r12 = new C16150oX();
            }
            r22.A02 = r12;
        } else if (this instanceof AnonymousClass1XP) {
            AnonymousClass1XP r23 = (AnonymousClass1XP) this;
            if (obj instanceof C16150oX) {
                r122 = (C16150oX) obj;
            } else if (obj instanceof MediaData) {
                r122 = C16150oX.A00((MediaData) obj);
            } else if (obj instanceof Integer) {
                i = ((Number) obj).intValue();
                r23.A02 = i;
            } else {
                if (obj != null) {
                    StringBuilder sb3 = new StringBuilder("FMessageLocation/setObjectForDatabaseFieldThumbImage/setting wrong object; object.class=");
                    sb3.append(obj.getClass());
                    AnonymousClass009.A07(sb3.toString());
                }
                i = 2;
                r23.A02 = i;
            }
            if (!r122.A0P) {
                i = 0;
                if (r122.A0a) {
                    i = 1;
                }
                r23.A02 = i;
            }
            i = 2;
            r23.A02 = i;
        }
    }

    public void A0l(String str) {
        synchronized (this.A10) {
            this.A0f = str;
            this.A0x = null;
        }
    }

    public void A0m(String str) {
        if (!(this instanceof AbstractC16130oV)) {
            this.A09 &= -65;
            return;
        }
        AbstractC16130oV r2 = (AbstractC16130oV) this;
        r2.A09 = str;
        if (!TextUtils.isEmpty(str)) {
            r2.A0T(64);
        } else {
            r2.A0U(64);
        }
    }

    public void A0n(String str) {
        AbstractC14640lm A01;
        if (!(this instanceof AnonymousClass1XB)) {
            AbstractC14640lm r1 = this.A0z.A00;
            if (!C15380n4.A0F(r1) || C15380n4.A0N(r1) || str == null) {
                if (C15380n4.A0N(r1)) {
                    A01 = C15380n4.A02(str);
                } else if (TextUtils.isEmpty(str)) {
                    A01 = null;
                } else {
                    A01 = AbstractC14640lm.A01(str);
                }
                A0e(A01);
                return;
            }
            A0u(C15380n4.A07(UserJid.class, Arrays.asList(str.split(","))));
            return;
        }
        AnonymousClass1XB r3 = (AnonymousClass1XB) this;
        if (r3 instanceof C30561Xx) {
            ((C30561Xx) r3).A01 = str;
        } else if (r3 instanceof C30551Xw) {
            ((C30541Xv) r3).A03 = str;
        } else if (!(r3 instanceof C30511Xs)) {
            r3.A0M = null;
            if (r3.A14() && !TextUtils.isEmpty(str)) {
                AbstractC14640lm A012 = AbstractC14640lm.A01(str);
                r3.A0M = A012;
                if (A012 == null) {
                    StringBuilder sb = new StringBuilder("Something went wrong with this message, key = ");
                    sb.append(r3.A0z.toString());
                    sb.append(" action = ");
                    sb.append(r3.A00);
                    AnonymousClass009.A0A(sb.toString(), false);
                }
            }
        } else {
            C30511Xs r32 = (C30511Xs) r3;
            if (((AnonymousClass1XB) r32).A00 == 10) {
                r32.A01 = UserJid.getNullable(str);
            }
        }
    }

    public void A0o(String str) {
        AbstractC15710nm r2;
        String A15;
        String str2;
        boolean z;
        if (this instanceof C28861Ph) {
            ((C28861Ph) this).A05 = str;
        } else if (this instanceof AnonymousClass1Y5) {
            ((AnonymousClass1Y5) this).A01 = C15580nU.A04(str);
        } else if (this instanceof C30541Xv) {
            C30541Xv r1 = (C30541Xv) this;
            if (r1 instanceof C30561Xx) {
                ((C30561Xx) r1).A03 = str;
            } else if (!(r1 instanceof C30551Xw)) {
                r1.A03 = str;
            } else {
                ((C30551Xw) r1).A02 = str;
            }
        } else if (this instanceof C30511Xs) {
            C30511Xs r4 = (C30511Xs) this;
            if (((AnonymousClass1XB) r4).A00 == 28) {
                if (r4.A01 != null) {
                    if (TextUtils.isEmpty(str)) {
                        r2 = r4.A03;
                        A15 = r4.A15();
                        str2 = "sys-msg/number-change/old-jid-null-override";
                    } else if (UserJid.getNullable(str) == null) {
                        r2 = r4.A03;
                        A15 = r4.A15();
                        str2 = "sys-msg/number-change/old-jid-invalid-override";
                    }
                    r2.AaV(str2, A15, false);
                }
                r4.A01 = UserJid.getNullable(str);
            }
        } else if (this instanceof AnonymousClass1Y2) {
            ((AnonymousClass1Y2) this).A01 = "video".equals(str);
        } else if (this instanceof AnonymousClass1Y4) {
            ((AnonymousClass1Y4) this).A00 = str;
        } else if (this instanceof C32231bp) {
            ((C32231bp) this).A00 = str;
        } else if (this instanceof C30481Xo) {
            ((C30481Xo) this).A00 = str;
        } else if (this instanceof AnonymousClass1Y3) {
            ((AnonymousClass1Y3) this).A01 = C15580nU.A04(str);
        } else if (this instanceof AnonymousClass1XX) {
            ((AnonymousClass1XX) this).A00 = str;
        } else if (this instanceof AbstractC16130oV) {
            ((AbstractC16130oV) this).A03 = str;
        } else if (this instanceof C30341Xa) {
            ((C30341Xa) this).A03 = str;
        } else if (this instanceof C28581Od) {
            ((C28581Od) this).A04 = str;
        } else if (this instanceof C30241Wq) {
            ((C30241Wq) this).A01 = str;
        } else if (this instanceof C30321Wy) {
            ((C30321Wy) this).A00 = UserJid.getNullable(str);
        } else if (this instanceof AbstractC30391Xf) {
            AbstractC30391Xf r12 = (AbstractC30391Xf) this;
            if (!TextUtils.isEmpty(str)) {
                r12.A01 = "video".equals(str);
                z = true;
            } else {
                z = false;
            }
            r12.A00 = z;
        }
    }

    public void A0p(String str) {
        if (this instanceof C30541Xv) {
            C30541Xv r1 = (C30541Xv) this;
            if (r1 instanceof C30561Xx) {
                ((C30561Xx) r1).A04 = str;
            } else if (!(r1 instanceof C30551Xw)) {
                r1.A01 = UserJid.getNullable(str);
            } else {
                ((C30551Xw) r1).A03 = str;
            }
        } else if (this instanceof AbstractC16130oV) {
            ((AbstractC16130oV) this).A04 = str;
        }
    }

    public void A0q(String str) {
        if (this instanceof C30541Xv) {
            C30541Xv r1 = (C30541Xv) this;
            if (r1 instanceof C30561Xx) {
                ((C30561Xx) r1).A00 = str;
            } else if (!(r1 instanceof C30551Xw)) {
                r1.A00 = UserJid.getNullable(str);
            } else {
                ((C30551Xw) r1).A01 = str;
            }
        } else if (this instanceof AbstractC16130oV) {
            ((AbstractC16130oV) this).A05 = str;
        }
    }

    public void A0r(String str) {
        AbstractC15710nm r2;
        String A15;
        String str2;
        if (this instanceof C28861Ph) {
            ((C28861Ph) this).A03 = str;
        } else if (this instanceof C30511Xs) {
            C30511Xs r4 = (C30511Xs) this;
            if (((AnonymousClass1XB) r4).A00 == 28) {
                if (r4.A00 != null) {
                    if (TextUtils.isEmpty(str)) {
                        r2 = r4.A03;
                        A15 = r4.A15();
                        str2 = "sys-msg/number-change/new-jid-null-override";
                    } else if (UserJid.getNullable(str) == null) {
                        r2 = r4.A03;
                        A15 = r4.A15();
                        str2 = "sys-msg/number-change/new-jid-invalid-override";
                    }
                    r2.AaV(str2, A15, false);
                }
                r4.A00 = UserJid.getNullable(str);
            }
        } else if (this instanceof C30491Xp) {
            ((C30491Xp) this).A00 = str;
        } else if (this instanceof C30331Wz) {
            ((C30331Wz) this).A01 = str;
        } else if (this instanceof AbstractC16130oV) {
            ((AbstractC16130oV) this).A07 = str;
        } else if (this instanceof AnonymousClass1XO) {
            AnonymousClass1XO r22 = (AnonymousClass1XO) this;
            if (!TextUtils.isEmpty(str)) {
                int indexOf = str.indexOf(10);
                if (indexOf == -1) {
                    r22.A01 = str;
                    return;
                }
                r22.A01 = str.substring(0, indexOf);
                if (str.length() > indexOf) {
                    r22.A00 = str.substring(indexOf + 1);
                }
            }
        } else if (this instanceof C30341Xa) {
            C30341Xa r42 = (C30341Xa) this;
            if (str != null) {
                String[] split = str.split(",");
                if (split.length == 4) {
                    UserJid nullable = UserJid.getNullable(split[0]);
                    AnonymousClass009.A05(nullable);
                    C30751Yr r23 = new C30751Yr(nullable);
                    r42.A02 = r23;
                    r23.A00 = Double.parseDouble(split[1]);
                    r23.A01 = Double.parseDouble(split[2]);
                    r23.A05 = Long.parseLong(split[3]);
                }
            }
        } else if (this instanceof C30351Xb) {
            ((C30351Xb) this).A00 = str;
        } else if (this instanceof C30411Xh) {
            ((C30411Xh) this).A00 = str;
        } else if (this instanceof C30401Xg) {
            C30401Xg r7 = (C30401Xg) this;
            long A01 = C28421Nd.A01(str, 0);
            if (A01 < 0 || A01 > 2147483648L) {
                A01 = 0;
            }
            r7.A02 = A01;
            if (A01 > 0) {
                ((AbstractC30391Xf) r7).A00 = true;
            }
        } else if (this instanceof C30241Wq) {
            ((C30241Wq) this).A00 = str;
        }
    }

    public void A0s(String str) {
        if (this instanceof C28861Ph) {
            ((C28861Ph) this).A06 = str;
        } else if (this instanceof C30561Xx) {
            ((C30561Xx) this).A02 = str;
        } else if (this instanceof C30551Xw) {
            ((C30551Xw) this).A04 = str;
        } else if (this instanceof AnonymousClass1Y3) {
            AnonymousClass1Y3 r3 = (AnonymousClass1Y3) this;
            Integer num = null;
            if (str != null) {
                try {
                    num = Integer.valueOf(Integer.parseInt(str));
                } catch (NumberFormatException e) {
                    Log.e("FMessageSystemCommunityLinkChanged/could not format number. Error = ", e);
                }
            }
            r3.A02 = num;
        } else if (this instanceof AnonymousClass1Y1) {
            ((AnonymousClass1Y1) this).A01 = str;
        } else if (this instanceof AnonymousClass1XO) {
            ((AnonymousClass1XO) this).A02 = str;
        } else if (this instanceof AbstractC16130oV) {
            ((AbstractC16130oV) this).A08 = str;
        }
    }

    public void A0t(String str) {
        if (this instanceof C28861Ph) {
            A0l(str);
        } else if (this instanceof AnonymousClass1XB) {
            AnonymousClass1XB r2 = (AnonymousClass1XB) this;
            if (!(r2 instanceof C30451Xl)) {
                r2.A0l(str);
                return;
            }
            C30451Xl r22 = (C30451Xl) r2;
            synchronized (r22.A10) {
                r22.A00 = str;
            }
        } else if (this instanceof C30331Wz) {
        } else {
            if (this instanceof C27671Iq) {
                ((C27671Iq) this).A02 = str;
            } else if (!(this instanceof AbstractC30251Wr) && !(this instanceof AbstractC30271Wt)) {
                if (this instanceof AnonymousClass1XX) {
                    ((AnonymousClass1XX) this).A00 = str;
                } else if (!(this instanceof AnonymousClass1XC) && !(this instanceof C30291Wv)) {
                    if (this instanceof AbstractC16130oV) {
                        ((AbstractC16130oV) this).A03 = str;
                    } else if (this instanceof AnonymousClass1XO) {
                    } else {
                        if (this instanceof C30341Xa) {
                            ((C30341Xa) this).A03 = str;
                        } else if (!(this instanceof AnonymousClass1X5) && !(this instanceof C16380ov)) {
                            if (this instanceof C28581Od) {
                                ((C28581Od) this).A04 = str;
                            } else if (!(this instanceof C30361Xc) && !(this instanceof AnonymousClass1XK)) {
                                if (this instanceof C30351Xb) {
                                    ((C30351Xb) this).A00 = str;
                                } else if (this instanceof C30411Xh) {
                                    ((C30411Xh) this).A00 = str;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void A0u(List list) {
        boolean z;
        String str;
        if (!(this instanceof AnonymousClass1XB)) {
            this.A0M = null;
            this.A0p = list;
            return;
        }
        AnonymousClass1XB r1 = (AnonymousClass1XB) this;
        if (r1 instanceof C30541Xv) {
            z = false;
            str = "should not be called for FMessageSystemPayment";
        } else if (!(r1 instanceof C30461Xm)) {
            z = false;
            str = "should not be called for FMessageSystem";
        } else {
            C30461Xm r12 = (C30461Xm) r1;
            if (list == null) {
                list = new ArrayList();
            }
            r12.A01 = list;
            return;
        }
        AnonymousClass009.A0A(str, z);
    }

    public final void A0v(List list) {
        if (list == null || list.isEmpty()) {
            this.A0o = null;
            return;
        }
        this.A0o = new ArrayList(list);
        this.A15 |= 1;
    }

    public void A0w(byte[] bArr) {
        synchronized (this.A10) {
            this.A0x = bArr;
            this.A0f = null;
            this.A02 = 1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        if (r3.A0x != null) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0x() {
        /*
            r3 = this;
            java.lang.Object r2 = r3.A10
            monitor-enter(r2)
            java.lang.String r0 = r3.A0f     // Catch: all -> 0x000f
            if (r0 != 0) goto L_0x000c
            byte[] r1 = r3.A0x     // Catch: all -> 0x000f
            r0 = 0
            if (r1 == 0) goto L_0x000d
        L_0x000c:
            r0 = 1
        L_0x000d:
            monitor-exit(r2)     // Catch: all -> 0x000f
            return r0
        L_0x000f:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x000f
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC15340mz.A0x():boolean");
    }

    public boolean A0y() {
        long j = (long) 8;
        return (this.A15 & j) == j;
    }

    public boolean A0z() {
        List list = this.A0o;
        return list != null && !list.isEmpty();
    }

    public boolean A10() {
        if ((this instanceof C27671Iq) || (this instanceof C30361Xc)) {
            return true;
        }
        return this.A0t;
    }

    public boolean A11() {
        return !(this instanceof AnonymousClass1XB) && !(this instanceof AbstractC30251Wr) && !(this instanceof C30361Xc) && !(this instanceof AbstractC30391Xf);
    }

    public boolean A12(int i) {
        return (this.A09 & i) == i;
    }

    public byte[] A13() {
        byte[] bArr;
        String str;
        synchronized (this.A10) {
            bArr = this.A0x;
            if (bArr == null && (str = this.A0f) != null) {
                try {
                    bArr = str.getBytes(AnonymousClass01V.A08);
                } catch (UnsupportedEncodingException unused) {
                    bArr = null;
                }
                this.A0x = bArr;
            }
        }
        return bArr;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" key=");
        sb.append(this.A0z);
        sb.append(" media_wa_type=");
        sb.append((int) this.A0y);
        return sb.toString();
    }
}
