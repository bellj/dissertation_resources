package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56972mG extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C56972mG A02;
    public static volatile AnonymousClass255 A03;
    public int A00;
    public long A01;

    static {
        C56972mG r0 = new C56972mG();
        A02 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r12, Object obj, Object obj2) {
        switch (r12.ordinal()) {
            case 0:
                return A02;
            case 1:
                AbstractC462925h r4 = (AbstractC462925h) obj;
                C56972mG r14 = (C56972mG) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                long j = this.A01;
                int i2 = r14.A00;
                this.A01 = r4.Afs(j, r14.A01, A1R, C12960it.A1R(i2));
                if (r4 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r42 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r42.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 8) {
                            this.A00 |= 1;
                            this.A01 = r42.A06();
                        } else if (!A0a(r42, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C56972mG();
            case 5:
                return new C81943up();
            case 6:
                break;
            case 7:
                if (A03 == null) {
                    synchronized (C56972mG.class) {
                        if (A03 == null) {
                            A03 = AbstractC27091Fz.A09(A02);
                        }
                    }
                }
                return A03;
            default:
                throw C12970iu.A0z();
        }
        return A02;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A05(1, this.A01);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0H(1, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
