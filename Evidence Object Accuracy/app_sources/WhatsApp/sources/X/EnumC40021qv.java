package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.1qv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC40021qv {
    A2B(0),
    A28(1),
    A13(2),
    A1O(3),
    A1t(4),
    A2C(5),
    A2I(6),
    A2H(7),
    A2D(8),
    A2G(9),
    A2F(10),
    A2E(11),
    A2K(12),
    A2J(13),
    A2L(14),
    A2M(15),
    A2Q(16),
    A2N(17),
    A2O(18),
    A2P(19),
    A1Z(20),
    A1Y(21),
    A1U(22),
    A1V(23),
    A1T(24),
    A1X(25),
    A1S(26),
    A1h(27),
    A1p(28),
    A1o(29),
    A1k(30),
    A1l(31),
    A1m(32),
    A1j(33),
    A0w(34),
    A0v(35),
    A0x(36),
    A1P(37),
    A1K(38),
    A1I(39),
    A11(40),
    A10(41),
    A1r(42),
    A1c(43),
    A1Q(44),
    A0z(45),
    A0y(46),
    A21(47),
    A22(48),
    A25(49),
    A26(50),
    A27(51),
    A23(52),
    A24(53),
    A1v(54),
    A20(55),
    A1z(56),
    A1x(57),
    A1y(58),
    A1w(59),
    A0D(60),
    A0C(61),
    A03(62),
    A02(63),
    A05(64),
    A04(65),
    A0B(66),
    A0A(67),
    A1u(68),
    A1W(69),
    A1q(70),
    A1i(71),
    A12(72),
    A1G(73),
    A2R(74),
    A1J(75),
    A0F(76),
    A0G(77),
    A0H(78),
    A0I(79),
    A0L(80),
    A0M(81),
    A0P(82),
    A0Q(83),
    A0R(84),
    A0S(85),
    A0T(86),
    A0U(87),
    A0V(88),
    A0W(89),
    A0X(90),
    A0Y(91),
    A0Z(92),
    A0a(93),
    A0b(94),
    A0d(95),
    A0e(96),
    A0g(97),
    A0h(98),
    A0i(99),
    A0j(100),
    A0k(101),
    A0l(102),
    A0m(103),
    A0n(104),
    A0o(105),
    A0p(106),
    A0q(107),
    A0r(C43951xu.A03),
    A0s(109),
    A0t(110),
    A0u(111),
    A0J(112),
    A0K(113),
    A0N(114),
    A0O(115),
    A0c(116),
    A0f(117),
    A1L(118),
    A1b(119),
    A1a(120),
    A1R(121),
    A0E(122),
    A1N(123),
    A2A(124),
    A29(125),
    A07(126),
    A06(127),
    A09(128),
    A08(129),
    A1F(130),
    A1H(131),
    A01(132),
    A1d(133),
    A15(134),
    A17(135),
    A18(136),
    A1C(137),
    A1D(138),
    A1E(139),
    A1g(140),
    A1n(141),
    A14(142),
    A1M(143),
    A1f(MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT),
    A1e(145),
    A1s(146),
    A1B(147),
    A1A(148),
    A19(149),
    A16(150);
    
    public final int value;

    EnumC40021qv(int i) {
        this.value = i;
    }

    public static EnumC40021qv A00(int i) {
        switch (i) {
            case 0:
                return A2B;
            case 1:
                return A28;
            case 2:
                return A13;
            case 3:
                return A1O;
            case 4:
                return A1t;
            case 5:
                return A2C;
            case 6:
                return A2I;
            case 7:
                return A2H;
            case 8:
                return A2D;
            case 9:
                return A2G;
            case 10:
                return A2F;
            case 11:
                return A2E;
            case 12:
                return A2K;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return A2J;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return A2L;
            case 15:
                return A2M;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return A2Q;
            case 17:
                return A2N;
            case 18:
                return A2O;
            case 19:
                return A2P;
            case C43951xu.A01 /* 20 */:
                return A1Z;
            case 21:
                return A1Y;
            case 22:
                return A1U;
            case 23:
                return A1V;
            case 24:
                return A1T;
            case 25:
                return A1X;
            case 26:
                return A1S;
            case 27:
                return A1h;
            case 28:
                return A1p;
            case 29:
                return A1o;
            case C25991Bp.A0S:
                return A1k;
            case 31:
                return A1l;
            case 32:
                return A1m;
            case 33:
                return A1j;
            case 34:
                return A0w;
            case 35:
                return A0v;
            case 36:
                return A0x;
            case 37:
                return A1P;
            case 38:
                return A1K;
            case 39:
                return A1I;
            case 40:
                return A11;
            case 41:
                return A10;
            case 42:
                return A1r;
            case 43:
                return A1c;
            case 44:
                return A1Q;
            case 45:
                return A0z;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                return A0y;
            case 47:
                return A21;
            case 48:
                return A22;
            case 49:
                return A25;
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                return A26;
            case 51:
                return A27;
            case 52:
                return A23;
            case 53:
                return A24;
            case 54:
                return A1v;
            case 55:
                return A20;
            case 56:
                return A1z;
            case 57:
                return A1x;
            case 58:
                return A1y;
            case 59:
                return A1w;
            case 60:
                return A0D;
            case 61:
                return A0C;
            case 62:
                return A03;
            case 63:
                return A02;
            case 64:
                return A05;
            case 65:
                return A04;
            case 66:
                return A0B;
            case 67:
                return A0A;
            case 68:
                return A1u;
            case 69:
                return A1W;
            case 70:
                return A1q;
            case 71:
                return A1i;
            case C43951xu.A02 /* 72 */:
                return A12;
            case 73:
                return A1G;
            case 74:
                return A2R;
            case 75:
                return A1J;
            case 76:
                return A0F;
            case 77:
                return A0G;
            case 78:
                return A0H;
            case 79:
                return A0I;
            case 80:
                return A0L;
            case 81:
                return A0M;
            case 82:
                return A0P;
            case 83:
                return A0Q;
            case 84:
                return A0R;
            case 85:
                return A0S;
            case 86:
                return A0T;
            case 87:
                return A0U;
            case 88:
                return A0V;
            case 89:
                return A0W;
            case 90:
                return A0X;
            case 91:
                return A0Y;
            case 92:
                return A0Z;
            case 93:
                return A0a;
            case 94:
                return A0b;
            case 95:
                return A0d;
            case 96:
                return A0e;
            case 97:
                return A0g;
            case 98:
                return A0h;
            case 99:
                return A0i;
            case 100:
                return A0j;
            case 101:
                return A0k;
            case 102:
                return A0l;
            case 103:
                return A0m;
            case 104:
                return A0n;
            case 105:
                return A0o;
            case 106:
                return A0p;
            case 107:
                return A0q;
            case C43951xu.A03 /* 108 */:
                return A0r;
            case 109:
                return A0s;
            case 110:
                return A0t;
            case 111:
                return A0u;
            case 112:
                return A0J;
            case 113:
                return A0K;
            case 114:
                return A0N;
            case 115:
                return A0O;
            case 116:
                return A0c;
            case 117:
                return A0f;
            case 118:
                return A1L;
            case 119:
                return A1b;
            case 120:
                return A1a;
            case 121:
                return A1R;
            case 122:
                return A0E;
            case 123:
                return A1N;
            case 124:
                return A2A;
            case 125:
                return A29;
            case 126:
                return A07;
            case 127:
                return A06;
            case 128:
                return A09;
            case 129:
                return A08;
            case 130:
                return A1F;
            case 131:
                return A1H;
            case 132:
                return A01;
            case 133:
                return A1d;
            case 134:
                return A15;
            case 135:
                return A17;
            case 136:
                return A18;
            case 137:
                return A1C;
            case 138:
                return A1D;
            case 139:
                return A1E;
            case 140:
                return A1g;
            case 141:
                return A1n;
            case 142:
                return A14;
            case 143:
                return A1M;
            case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                return A1f;
            case 145:
                return A1e;
            case 146:
                return A1s;
            case 147:
                return A1B;
            case 148:
                return A1A;
            case 149:
                return A19;
            case 150:
                return A16;
            default:
                return null;
        }
    }
}
