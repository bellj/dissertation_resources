package X;

import org.json.JSONObject;

/* renamed from: X.44L  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass44L extends AnonymousClass3XT {
    public final C17560r0 A00;

    public abstract void A00(AnonymousClass3H5 v, JSONObject jSONObject, int i);

    public AnonymousClass44L(C17560r0 r1) {
        this.A00 = r1;
    }
}
