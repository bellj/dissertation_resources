package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.jid.DeviceJid;

/* renamed from: X.13j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238813j {
    public final C14620lj A00 = new C14620lj();
    public final C21910yB A01;
    public final ExecutorC27271Gr A02;

    public C238813j(C21910yB r3, AbstractC14440lR r4) {
        this.A01 = r3;
        this.A02 = new ExecutorC27271Gr(r4, false);
    }

    public C40951sc A00() {
        DeviceJid nullable;
        AnonymousClass009.A00();
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id, device_id, sync_type, last_processed_msg_row_id, oldest_msg_row_id, oldest_message_to_sync_row_id, sent_msgs_count, chunk_order, sent_bytes, last_chunk_timestamp, status, peer_msg_row_id, session_id, md_reg_attempt_id FROM msg_history_sync WHERE status=1 ORDER BY sync_type ASC, last_chunk_timestamp ASC LIMIT 1", null);
            if (A09 != null) {
                if (!A09.moveToNext() || (nullable = DeviceJid.getNullable(A09.getString(A09.getColumnIndexOrThrow("device_id")))) == null) {
                    A09.close();
                } else {
                    long j = A09.getLong(A09.getColumnIndexOrThrow("_id"));
                    int i = A09.getInt(A09.getColumnIndexOrThrow("sync_type"));
                    long j2 = A09.getLong(A09.getColumnIndexOrThrow("last_processed_msg_row_id"));
                    long j3 = A09.getLong(A09.getColumnIndexOrThrow("oldest_msg_row_id"));
                    long j4 = A09.getLong(A09.getColumnIndexOrThrow("oldest_message_to_sync_row_id"));
                    long j5 = A09.getLong(A09.getColumnIndexOrThrow("sent_msgs_count"));
                    int i2 = A09.getInt(A09.getColumnIndexOrThrow("chunk_order"));
                    long j6 = (long) A09.getInt(A09.getColumnIndexOrThrow("sent_bytes"));
                    long j7 = (long) A09.getInt(A09.getColumnIndexOrThrow("last_chunk_timestamp"));
                    int i3 = A09.getInt(A09.getColumnIndexOrThrow("status"));
                    long j8 = A09.getLong(A09.getColumnIndexOrThrow("peer_msg_row_id"));
                    String string = A09.getString(A09.getColumnIndexOrThrow("session_id"));
                    String string2 = A09.getString(A09.getColumnIndexOrThrow("md_reg_attempt_id"));
                    C40951sc r8 = new C40951sc((string == null || string2 == null) ? null : new AnonymousClass1JT(string, string2), nullable, i, i2, i3, j, j2, j3, j4, j5, j6, j7, j8);
                    A09.close();
                    A01.close();
                    return r8;
                }
            }
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A01(C16310on r3, DeviceJid deviceJid) {
        if (r3.A03.A00.inTransaction()) {
            r3.A03(new RunnableBRunnable0Shape4S0200000_I0_4(this, 45, deviceJid));
        } else {
            A03(deviceJid);
        }
    }

    public void A02(C40951sc r9) {
        AnonymousClass009.A00();
        C16310on A02 = this.A01.A02();
        try {
            AnonymousClass1YE A0A = A02.A03.A0A("INSERT INTO msg_history_sync (device_id, sync_type, last_processed_msg_row_id, oldest_msg_row_id, sent_msgs_count, chunk_order, sent_bytes, last_chunk_timestamp, status, peer_msg_row_id, oldest_message_to_sync_row_id, session_id, md_reg_attempt_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            SQLiteStatement sQLiteStatement = A0A.A00;
            sQLiteStatement.clearBindings();
            DeviceJid deviceJid = r9.A0C;
            A0A.A02(1, deviceJid.getRawString());
            A0A.A01(2, (long) r9.A02);
            A0A.A01(3, r9.A04);
            A0A.A01(4, r9.A09);
            A0A.A01(5, r9.A08);
            A0A.A01(6, (long) r9.A00);
            A0A.A01(7, r9.A07);
            A0A.A01(8, r9.A03);
            A0A.A01(9, (long) r9.A01);
            A0A.A01(10, r9.A05);
            A0A.A01(11, r9.A0A);
            AnonymousClass1JT r7 = r9.A0B;
            if (r7 != null) {
                A0A.A02(12, r7.A01);
                A0A.A02(13, r7.A00);
            } else {
                sQLiteStatement.bindNull(12);
                sQLiteStatement.bindNull(13);
            }
            sQLiteStatement.executeInsert();
            A01(A02, deviceJid);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A03(DeviceJid deviceJid) {
        this.A02.execute(new RunnableBRunnable0Shape4S0200000_I0_4(this, 46, deviceJid));
    }

    public void A04(DeviceJid deviceJid, int i) {
        AnonymousClass009.A00();
        C16310on A02 = this.A01.A02();
        try {
            A02.A03.A0C("DELETE FROM msg_history_sync WHERE device_id=? AND sync_type=?", new String[]{deviceJid.getRawString(), String.valueOf(i)});
            A02.close();
            A03(deviceJid);
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
