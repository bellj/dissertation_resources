package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import java.util.List;

/* renamed from: X.687  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass687 implements AnonymousClass5W2 {
    public final C14900mE A00;
    public final C248217a A01;

    public AnonymousClass687(C14900mE r1, C248217a r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5W2
    public void A5s(List list) {
        this.A00.A0I(new RunnableBRunnable0Shape8S0100000_I0_8(this.A01, 25));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        if (r1 == false) goto L_0x0019;
     */
    @Override // X.AnonymousClass5W2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC28901Pl A6M(X.AbstractC28901Pl r3) {
        /*
            r2 = this;
            boolean r0 = r3 instanceof X.C30881Ze
            if (r0 == 0) goto L_0x001d
            X.1ZY r1 = r3.A08
            boolean r0 = r1 instanceof X.C119765f4
            if (r0 == 0) goto L_0x001d
            X.5f4 r1 = (X.C119765f4) r1
            X.5wD r0 = r1.A01
            java.lang.Boolean r0 = r0.A00
            if (r0 == 0) goto L_0x0019
            boolean r1 = r0.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x001a
        L_0x0019:
            r0 = 4
        L_0x001a:
            r3.A06(r0)
        L_0x001d:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass687.A6M(X.1Pl):X.1Pl");
    }
}
