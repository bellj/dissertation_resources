package X;

import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;

/* renamed from: X.3UA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UA implements AbstractC13860kS {
    public final AbstractActivityC28171Kz A00;

    public AnonymousClass3UA(AbstractActivityC28171Kz r1) {
        this.A00 = r1;
    }

    public void A00() {
        UserJid nullable;
        C36811kg r0;
        if (!(this instanceof AnonymousClass2xD)) {
            this.A00.finish();
            return;
        }
        AnonymousClass2xD r02 = (AnonymousClass2xD) this;
        VoipActivityV2 voipActivityV2 = r02.A01;
        voipActivityV2.A2q();
        Intent intent = r02.A00;
        if (intent != null && (nullable = UserJid.getNullable(intent.getStringExtra("contact"))) != null) {
            VoipActivityV2.A0A(nullable, voipActivityV2);
            VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = voipActivityV2.A1N;
            if (voipCallControlBottomSheetV2 != null && (r0 = voipCallControlBottomSheetV2.A0G) != null) {
                r0.A0I(nullable);
            }
        }
    }

    public void A01(Intent intent) {
        if (!(this instanceof AnonymousClass2xD)) {
            this.A00.setResult(-1, intent);
        } else {
            ((AnonymousClass2xD) this).A00 = intent;
        }
    }

    @Override // X.AbstractC13860kS
    public boolean AJN() {
        return this.A00.AJN();
    }

    @Override // X.AbstractC13860kS
    public void AaN() {
        this.A00.AaN();
    }

    @Override // X.AbstractC13860kS
    public void Adl(DialogFragment dialogFragment, String str) {
        this.A00.Adl(dialogFragment, null);
    }

    @Override // X.AbstractC13860kS
    public void Adm(DialogFragment dialogFragment) {
        this.A00.Adm(dialogFragment);
    }

    @Override // X.AbstractC13860kS
    public void Ado(int i) {
        this.A00.Ado(i);
    }

    @Override // X.AbstractC13860kS
    public void Adp(String str) {
        this.A00.Adp(str);
    }

    @Override // X.AbstractC13860kS
    public void Adq(AnonymousClass2GV r7, Object[] objArr, int i, int i2, int i3) {
        this.A00.Adq(r7, objArr, i, i2, R.string.manage_storage_button_text);
    }

    @Override // X.AbstractC13860kS
    public void Adr(Object[] objArr, int i, int i2) {
        this.A00.Adr(objArr, i, i2);
    }

    @Override // X.AbstractC13860kS
    public void Ady(int i, int i2) {
        this.A00.Ady(i, i2);
    }

    @Override // X.AbstractC13860kS
    public void AfX(String str) {
        this.A00.AfX(str);
    }
}
