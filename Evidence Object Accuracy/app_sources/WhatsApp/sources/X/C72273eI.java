package X;

import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3eI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72273eI extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $onError;
    public final /* synthetic */ AnonymousClass1J7 $onSuccess;
    public final /* synthetic */ AnonymousClass3BT this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72273eI(AnonymousClass3BT r2, AnonymousClass1J7 r3, AnonymousClass1J7 r4) {
        super(1);
        this.this$0 = r2;
        this.$onError = r3;
        this.$onSuccess = r4;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        C100494m0 r5 = (C100494m0) obj;
        C16700pc.A0E(r5, 0);
        String str = r5.A00;
        AnonymousClass3BT r0 = this.this$0;
        if (str == null) {
            r0.A00.A0H(new RunnableBRunnable0Shape16S0100000_I1_2(this.$onError, 2));
        } else {
            AnonymousClass3ED r1 = r0.A03;
            List singletonList = Collections.singletonList(str);
            C16700pc.A0B(singletonList);
            AnonymousClass1WI.A00(this.this$0.A00, this.$onSuccess, r1.A01(singletonList, false), 38);
        }
        return AnonymousClass1WZ.A00;
    }
}
