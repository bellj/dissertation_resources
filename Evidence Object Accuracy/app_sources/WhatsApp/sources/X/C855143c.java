package X;

/* renamed from: X.43c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C855143c extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public String A03;

    public C855143c() {
        super(3180, new AnonymousClass00E(1, 1, 100), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(4, this.A01);
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMdAppStateSyncMutationStats {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "applied", C12960it.A0Y(this.A00));
        String str = null;
        Integer num = this.A01;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "orphan", str);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "syncdAction", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "unsupported", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
