package X;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.concurrent.Executor;

/* renamed from: X.0mE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14900mE {
    public AbstractC13860kS A00;
    public C29651Uc A01;
    public final Handler A02 = new Handler(Looper.getMainLooper());
    public final C18640sm A03;
    public final C16590pI A04;
    public final C17170qN A05;
    public final Executor A06 = new Executor() { // from class: X.1Ub
        @Override // java.util.concurrent.Executor
        public final void execute(Runnable runnable) {
            C14900mE.this.A0H(runnable);
        }
    };

    public C14900mE(C18640sm r3, C16590pI r4, C17170qN r5) {
        this.A04 = r4;
        this.A03 = r3;
        this.A05 = r5;
    }

    public static void A00(C14900mE r1, Object obj, int i) {
        r1.A0H(new RunnableBRunnable0Shape14S0100000_I1(obj, i));
    }

    public static void A01(C14900mE r1, Object obj, int i) {
        r1.A0H(new RunnableBRunnable0Shape15S0100000_I1_1(obj, i));
    }

    @Deprecated
    public Toast A02(CharSequence charSequence) {
        Context context = this.A04.A00;
        View inflate = LayoutInflater.from(context).inflate(R.layout.toast, (ViewGroup) null);
        ((TextView) AnonymousClass028.A0D(inflate, 16908299)).setText(charSequence);
        Toast toast = new Toast(context);
        toast.setView(inflate);
        toast.setDuration(0);
        return toast;
    }

    public void A03() {
        StringBuilder sb = new StringBuilder("app/progress-spinner/remove dt=");
        sb.append(this.A00);
        Log.i(sb.toString());
        this.A01 = null;
        AbstractC13860kS r0 = this.A00;
        if (r0 != null) {
            r0.AaN();
        } else {
            C29661Ud.A02 = false;
        }
        Log.i("app/progress-spinner/remove done");
    }

    public final void A04(int i) {
        A0H(new RunnableBRunnable0Shape0S0101000_I0(this, i, 1));
    }

    public void A05(int i, int i2) {
        AbstractC13860kS r0 = this.A00;
        if (r0 != null) {
            r0.Ado(i);
        } else {
            A07(i, i2);
        }
    }

    public void A06(int i, int i2) {
        StringBuilder sb = new StringBuilder("app/progress-spinner/show dt=");
        sb.append(this.A00);
        Log.i(sb.toString());
        this.A01 = new C29651Uc(i, i2);
        AbstractC13860kS r0 = this.A00;
        if (r0 != null) {
            r0.Ady(i, i2);
        }
        Log.i("app/progress-spinner/show done");
    }

    public void A07(int i, int i2) {
        A0E(this.A04.A00.getString(i), i2);
    }

    public void A08(int i, int i2) {
        A0F(this.A04.A00.getString(i), i2);
    }

    public void A09(AbstractC13860kS r3) {
        StringBuilder sb = new StringBuilder("app/dt/clear dt=");
        sb.append(r3);
        sb.append(" dialog_toast=");
        sb.append(this.A00);
        Log.i(sb.toString());
        AbstractC13860kS r1 = this.A00;
        if (r1 == r3) {
            if (this.A01 != null) {
                r1.AaN();
            }
            this.A00 = null;
        }
        Log.i("app/dt/clear done");
    }

    public void A0A(AbstractC13860kS r2) {
        if (r2 == null && (r2 = this.A00) == null) {
            AnonymousClass009.A07("dialogToast == null");
            Log.w("app/removeProgressSpinner/ignore dialogToast == null");
            return;
        }
        r2.AaN();
    }

    public void A0B(AbstractC13860kS r4) {
        StringBuilder sb = new StringBuilder("app/dt/set ");
        sb.append(r4);
        Log.i(sb.toString());
        this.A00 = r4;
        C29651Uc r2 = this.A01;
        if (r2 != null) {
            StringBuilder sb2 = new StringBuilder("app/dt/set show_progress_data=");
            sb2.append(r2);
            sb2.append(" dialog_toast=");
            sb2.append(this.A00);
            Log.i(sb2.toString());
            AbstractC13860kS r22 = this.A00;
            C29651Uc r0 = this.A01;
            r22.Ady(r0.A02, r0.A01);
            if (this.A01.A00 != null) {
                Log.i("app/dt/set/update");
                this.A00.AfX(this.A01.A00);
            }
        }
        Log.i("app/dt/set done");
    }

    public void A0C(AbstractC13860kS r4) {
        if (r4 == null && (r4 = this.A00) == null) {
            AnonymousClass009.A07("dialogToast == null");
            A07(R.string.register_wait_message, 0);
            return;
        }
        r4.Ady(0, R.string.register_wait_message);
    }

    public void A0D(AbstractC13860kS r2, String str) {
        if (r2 != null) {
            r2.Adp(str);
        } else {
            A0L(str, 0);
        }
    }

    public void A0E(CharSequence charSequence, int i) {
        View view;
        Context context = this.A04.A00;
        Toast makeText = Toast.makeText(context, charSequence, i);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 26 && i2 <= 28) {
            String str = Build.MANUFACTURER;
            if (("google".equalsIgnoreCase(str) || "oneplus".equalsIgnoreCase(str)) && (view = makeText.getView()) != null) {
                Drawable background = view.getBackground();
                TextView textView = (TextView) view.findViewById(16908299);
                if (!(background == null || textView == null)) {
                    background.setColorFilter(AnonymousClass00T.A00(context, R.color.toast_background), PorterDuff.Mode.SRC_IN);
                    textView.setTextColor(AnonymousClass00T.A00(context, R.color.toast_text));
                }
            }
        }
        makeText.show();
    }

    public void A0F(CharSequence charSequence, int i) {
        if (AnonymousClass01I.A01()) {
            A0E(charSequence, i);
        } else {
            A0H(new RunnableBRunnable0Shape0S0201000_I0(this, charSequence, i, 3));
        }
    }

    public void A0G(Runnable runnable) {
        this.A02.removeCallbacks(runnable);
    }

    public void A0H(Runnable runnable) {
        this.A02.post(runnable);
    }

    public void A0I(Runnable runnable) {
        if (AnonymousClass01I.A01()) {
            runnable.run();
        } else {
            A0H(runnable);
        }
    }

    public void A0J(Runnable runnable, long j) {
        this.A02.postDelayed(runnable, j);
    }

    public void A0K(String str) {
        StringBuilder sb = new StringBuilder("app/progress-spinner/update-message dt=");
        sb.append(this.A00);
        Log.i(sb.toString());
        C29651Uc r0 = this.A01;
        if (r0 != null) {
            r0.A00 = str;
        } else {
            Log.w("app/progress-spinner/update-message no progress data");
        }
        AbstractC13860kS r02 = this.A00;
        if (r02 != null) {
            r02.AfX(str);
        }
        Log.i("app/progress-spinner/update-message done");
    }

    public void A0L(String str, int i) {
        AbstractC13860kS r0 = this.A00;
        if (r0 != null) {
            r0.Adp(str);
        } else {
            A0E(str, i);
        }
    }

    public boolean A0M() {
        if (this.A03.A0B()) {
            return true;
        }
        boolean A03 = C18640sm.A03(this.A04.A00);
        int i = R.string.network_required;
        if (A03) {
            i = R.string.network_required_airplane_on;
        }
        A07(i, 0);
        return false;
    }
}
