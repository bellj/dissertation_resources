package X;

import android.animation.ValueAnimator;

/* renamed from: X.4el  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C96044el implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ int A03;
    public final /* synthetic */ AnonymousClass2cj A04;

    public C96044el(AnonymousClass2cj r1, int i, int i2, int i3, int i4) {
        this.A04 = r1;
        this.A02 = i;
        this.A00 = i2;
        this.A03 = i3;
        this.A01 = i4;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float animatedFraction = valueAnimator.getAnimatedFraction();
        AnonymousClass2cj r3 = this.A04;
        int i = this.A02;
        int round = i + Math.round(((float) (this.A00 - i)) * animatedFraction);
        int i2 = this.A03;
        int round2 = i2 + Math.round(animatedFraction * ((float) (this.A01 - i2)));
        if (round != r3.A01 || round2 != r3.A02) {
            r3.A01 = round;
            r3.A02 = round2;
            r3.postInvalidateOnAnimation();
        }
    }
}
