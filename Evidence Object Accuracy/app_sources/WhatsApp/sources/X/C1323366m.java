package X;

import java.security.Signature;

/* renamed from: X.66m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1323366m implements AnonymousClass21K {
    public final /* synthetic */ long A00;
    public final /* synthetic */ C129155xG A01;
    public final /* synthetic */ C1323566o A02;

    @Override // X.AnonymousClass21K
    public /* synthetic */ void AMg(Signature signature) {
    }

    public C1323366m(C129155xG r1, C1323566o r2, long j) {
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = j;
    }

    @Override // X.AnonymousClass21K
    public void AMb(int i, CharSequence charSequence) {
        this.A01.A03.A05(C12960it.A0d(charSequence.toString(), C12960it.A0k("sendWithBiometric/onAuthenticationError/error: ")));
        this.A02.AMb(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMc() {
        this.A01.A03.A05("sendWithBiometric/onAuthenticationFailed");
        this.A02.AMc();
    }

    @Override // X.AnonymousClass21K
    public void AMe(int i, CharSequence charSequence) {
        this.A01.A03.A05(C12960it.A0d(charSequence.toString(), C12960it.A0k("sendWithBiometric/onAuthenticationHelp/help: ")));
        this.A02.AMe(i, charSequence);
    }

    @Override // X.AnonymousClass21K
    public void AMf(byte[] bArr) {
        if (bArr != null) {
            C129155xG r2 = this.A01;
            r2.A03.A06("sendWithBiometric/onAuthenticationSucceeded/success");
            this.A02.AMf(C130775zx.A00(Boolean.FALSE, bArr, r2.A05, null, null, new Object[0], this.A00));
            return;
        }
        this.A01.A03.A05("sendWithBiometric/onAuthenticationSucceeded/null signature");
        this.A02.AMc();
    }
}
