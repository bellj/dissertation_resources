package X;

import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0vD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20100vD {
    public final C15570nT A00;
    public final C19880up A01;
    public final C19870uo A02;

    public C20100vD(C15570nT r1, C19880up r2, C19870uo r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public final boolean A00(AnonymousClass32K r4) {
        if (this.A02.A00.A07(1319)) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("code", 2498048);
                jSONObject.put("description", "Commerce features are not available.");
                r4.A00(new AnonymousClass3H5(jSONObject), jSONObject, 451);
                return true;
            } catch (JSONException e) {
                Log.e(e.getMessage());
                r4.APp(e);
                return true;
            }
        } else if (r4 instanceof C59252uF) {
            C59252uF r42 = (C59252uF) r4;
            if (!r42.A04.A0B()) {
                r42.A00.A00(-1);
                return true;
            }
            C91864Tn r2 = r42.A02;
            if (r2.A04 == null) {
                r42.A05.A03("view_collection_details_tag");
            }
            r42.A02();
            StringBuilder A0k = C12960it.A0k("GetSingleCollectionGraphQLService/sendRequest jid=");
            A0k.append(r2.A03);
            Log.i(C12960it.A0d(" success", A0k));
            return true;
        } else if (r4 instanceof C59272uH) {
            C59272uH r43 = (C59272uH) r4;
            if (!r43.A05.A0B()) {
                return true;
            }
            AnonymousClass32K.A01(r43, r43.A04.A00);
            return true;
        } else if (r4 instanceof C59262uG) {
            C59262uG r44 = (C59262uG) r4;
            if (!r44.A05.A0B()) {
                r44.A02.A00(r44.A07, -1);
                return true;
            }
            AnonymousClass32K.A01(r44, r44.A07.A00);
            return true;
        } else if (r4 instanceof C59282uI) {
            C59282uI r45 = (C59282uI) r4;
            if (!r45.A04.A0B()) {
                Log.i("GetProductCatalogGraphQLService/sendRequest failed : no network access");
                return true;
            }
            AnonymousClass32K.A01(r45, r45.A05.A05);
            return true;
        } else if (!(r4 instanceof C59242uE)) {
            C59232uD r46 = (C59232uD) r4;
            if (!r46.A04.A0B()) {
                r46.A04(-1);
                return true;
            }
            AnonymousClass32K.A01(r46, r46.A02.A02);
            return true;
        } else {
            C59242uE r47 = (C59242uE) r4;
            if (!r47.A04.A0B()) {
                r47.A00.A00(-1);
                return true;
            }
            AnonymousClass32K.A01(r47, r47.A02.A05);
            return true;
        }
    }

    public boolean A01(AbstractC116505Vs r23, AnonymousClass28H r24) {
        C19880up r0 = this.A01;
        AbstractC15710nm r7 = r0.A00;
        C19840ul r4 = r0.A0D;
        C14650lo r8 = r0.A02;
        C17560r0 r10 = r0.A05;
        C18640sm r15 = r0.A09;
        C19830uk r13 = r0.A08;
        C15680nj r3 = r0.A0A;
        C17220qS r2 = r0.A0C;
        AbstractC14440lR r1 = r0.A0F;
        C19810ui r9 = r0.A04;
        return A00(new C59282uI(r7, r8, r9, r10, new AnonymousClass4RQ(r9, r10), r0.A07, r13, r23, r15, r24, r3, r0.A0B, r2, r4, r1));
    }
}
