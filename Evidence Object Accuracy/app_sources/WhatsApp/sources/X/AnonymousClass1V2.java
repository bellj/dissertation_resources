package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1V2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1V2 {
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public AbstractC15340mz A08;
    public final C14830m7 A09;
    public final UserJid A0A;

    public AnonymousClass1V2(C14830m7 r34, AnonymousClass1V2 r35) {
        this(r34, r35.A0A, r35.A01, r35.A00, r35.A04, r35.A06, r35.A07, r35.A03, r35.A02, r35.A05);
        this.A08 = r35.A08;
    }

    public AnonymousClass1V2(C14830m7 r3, UserJid userJid, int i, int i2, long j, long j2, long j3, long j4, long j5, long j6) {
        this.A09 = r3;
        this.A0A = userJid;
        this.A04 = j;
        this.A06 = j2;
        this.A07 = j3;
        this.A03 = j4;
        this.A02 = Math.max(j5, j4);
        this.A05 = j6;
        this.A01 = i;
        this.A00 = i2;
    }

    public static void A00(AbstractC15340mz r3) {
        StringBuilder sb = new StringBuilder("[id=");
        AnonymousClass1IS r1 = r3.A0z;
        sb.append(r1.A01);
        sb.append(", from_me=");
        sb.append(r1.A02);
        sb.append(", remote_resource=");
        sb.append(r3.A0B());
        sb.append("]");
        sb.toString();
    }

    public synchronized int A01() {
        return this.A00;
    }

    public synchronized int A02() {
        return this.A01;
    }

    public synchronized long A03() {
        return this.A04;
    }

    public synchronized long A04() {
        return this.A05;
    }

    public synchronized AnonymousClass1V2 A05() {
        return new AnonymousClass1V2(this.A09, this);
    }

    public synchronized AnonymousClass1V2 A06(AbstractC15340mz r6) {
        this.A08 = r6;
        long j = r6.A12;
        this.A04 = j;
        this.A05 = r6.A0I;
        this.A00++;
        if (r6.A0z.A02) {
            this.A01 = 0;
        } else {
            int i = this.A01 + 1;
            this.A01 = i;
            if (i == 1) {
                this.A03 = j;
            } else if (i <= 2) {
            }
            this.A02 = j;
        }
        A00(r6);
        return A05();
    }

    public UserJid A07() {
        return this.A0A;
    }

    public synchronized AbstractC15340mz A08() {
        return this.A08;
    }

    public synchronized void A09(int i) {
        this.A00 = i;
    }

    public synchronized void A0A(long j) {
        this.A02 = j;
    }

    public synchronized void A0B(long j) {
        this.A05 = j;
    }

    public boolean A0C() {
        return this.A0A == C29831Uv.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if ((r6.A09.A00() - r6.A05) <= 86400000) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0D() {
        /*
            r6 = this;
            monitor-enter(r6)
            com.whatsapp.jid.UserJid r0 = r6.A0A     // Catch: all -> 0x001d
            boolean r0 = X.C15380n4.A0M(r0)     // Catch: all -> 0x001d
            if (r0 != 0) goto L_0x001a
            X.0m7 r0 = r6.A09     // Catch: all -> 0x001d
            long r4 = r0.A00()     // Catch: all -> 0x001d
            long r0 = r6.A05     // Catch: all -> 0x001d
            long r4 = r4 - r0
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            r0 = 1
            if (r1 > 0) goto L_0x001b
        L_0x001a:
            r0 = 0
        L_0x001b:
            monitor-exit(r6)
            return r0
        L_0x001d:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1V2.A0D():boolean");
    }

    public synchronized boolean A0E(AbstractC15340mz r7) {
        boolean z;
        z = false;
        if (r7.A12 > this.A06) {
            z = true;
        }
        return z;
    }

    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append("StatusInfo[jid=");
        sb.append(this.A0A);
        sb.append(", msgId=");
        sb.append(this.A04);
        sb.append(", lastRead=");
        sb.append(this.A06);
        sb.append(", lastSent=");
        sb.append(this.A07);
        sb.append(", firstUnread=");
        sb.append(this.A03);
        sb.append(", autoDownloadLimit=");
        sb.append(this.A02);
        sb.append(", ts=");
        sb.append(this.A05);
        sb.append(", unreadCount=");
        sb.append(this.A01);
        sb.append(", total=");
        sb.append(this.A00);
        sb.append(" ]");
        return sb.toString();
    }
}
