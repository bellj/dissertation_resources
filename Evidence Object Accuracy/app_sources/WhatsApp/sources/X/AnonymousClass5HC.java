package X;

/* renamed from: X.5HC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HC extends Thread {
    public final /* synthetic */ AbstractC106564vu A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5HC(AbstractC106564vu r2) {
        super("ExoPlayer:SimpleDecoder");
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x009a, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00a4, code lost:
        r4.A03 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00a6, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a7, code lost:
        return;
     */
    @Override // java.lang.Thread, java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            X.4vu r4 = r9.A00
        L_0x0002:
            java.lang.Object r2 = r4.A07     // Catch: InterruptedException -> 0x00ae
            monitor-enter(r2)     // Catch: InterruptedException -> 0x00ae
        L_0x0005:
            boolean r0 = r4.A06     // Catch: all -> 0x00ab
            if (r0 != 0) goto L_0x0015
            java.util.ArrayDeque r0 = r4.A09     // Catch: all -> 0x00ab
            boolean r0 = r0.isEmpty()     // Catch: all -> 0x00ab
            if (r0 != 0) goto L_0x001c
            int r0 = r4.A01     // Catch: all -> 0x00ab
            if (r0 <= 0) goto L_0x001c
        L_0x0015:
            boolean r0 = r4.A06     // Catch: all -> 0x00ab
            r7 = 0
            if (r0 == 0) goto L_0x0020
            monitor-exit(r2)     // Catch: all -> 0x00ab
            goto L_0x008a
        L_0x001c:
            r2.wait()     // Catch: all -> 0x00ab
            goto L_0x0005
        L_0x0020:
            java.util.ArrayDeque r0 = r4.A09     // Catch: all -> 0x00ab
            java.lang.Object r5 = r0.removeFirst()     // Catch: all -> 0x00ab
            X.3mA r5 = (X.C76763mA) r5     // Catch: all -> 0x00ab
            X.3m3[] r1 = r4.A0C     // Catch: all -> 0x00ab
            int r0 = r4.A01     // Catch: all -> 0x00ab
            r8 = 1
            int r0 = r0 - r8
            r4.A01 = r0     // Catch: all -> 0x00ab
            r6 = r1[r0]     // Catch: all -> 0x00ab
            boolean r3 = r4.A05     // Catch: all -> 0x00ab
            r4.A05 = r7     // Catch: all -> 0x00ab
            monitor-exit(r2)     // Catch: all -> 0x00ab
            boolean r0 = X.AnonymousClass4YO.A00(r5)     // Catch: InterruptedException -> 0x00ae
            if (r0 == 0) goto L_0x0043
            r0 = 4
            r6.addFlag(r0)     // Catch: InterruptedException -> 0x00ae
        L_0x0041:
            monitor-enter(r2)     // Catch: InterruptedException -> 0x00ae
            goto L_0x0058
        L_0x0043:
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            int r0 = r5.flags     // Catch: InterruptedException -> 0x00ae
            r0 = r0 & r1
            boolean r0 = X.C12960it.A1V(r0, r1)     // Catch: InterruptedException -> 0x00ae
            if (r0 == 0) goto L_0x0051
            r6.addFlag(r1)     // Catch: InterruptedException -> 0x00ae
        L_0x0051:
            X.4CJ r3 = r4.A05(r5, r6, r3)     // Catch: RuntimeException | OutOfMemoryError -> 0x008e, InterruptedException -> 0x00ae
            if (r3 == 0) goto L_0x0041
            goto L_0x009a
        L_0x0058:
            boolean r0 = r4.A05     // Catch: all -> 0x008b
            if (r0 != 0) goto L_0x006c
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            int r0 = r6.flags     // Catch: all -> 0x008b
            r0 = r0 & r1
            boolean r0 = X.C12960it.A1V(r0, r1)     // Catch: all -> 0x008b
            if (r0 == 0) goto L_0x007e
            int r0 = r4.A02     // Catch: all -> 0x008b
            int r0 = r0 + r8
            r4.A02 = r0     // Catch: all -> 0x008b
        L_0x006c:
            r6.release()     // Catch: all -> 0x008b
        L_0x006f:
            r5.clear()     // Catch: all -> 0x008b
            X.3mA[] r3 = r4.A0B     // Catch: all -> 0x008b
            int r1 = r4.A00     // Catch: all -> 0x008b
            int r0 = r1 + 1
            r4.A00 = r0     // Catch: all -> 0x008b
            r3[r1] = r5     // Catch: all -> 0x008b
            monitor-exit(r2)     // Catch: all -> 0x008b
            goto L_0x0002
        L_0x007e:
            int r0 = r4.A02     // Catch: all -> 0x008b
            r6.skippedOutputBufferCount = r0     // Catch: all -> 0x008b
            r4.A02 = r7     // Catch: all -> 0x008b
            java.util.ArrayDeque r0 = r4.A0A     // Catch: all -> 0x008b
            r0.addLast(r6)     // Catch: all -> 0x008b
            goto L_0x006f
        L_0x008a:
            return
        L_0x008b:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x008b
            throw r0     // Catch: InterruptedException -> 0x00ae
        L_0x008e:
            r1 = move-exception
            boolean r0 = r4 instanceof X.AbstractC76783mE     // Catch: InterruptedException -> 0x00ae
            if (r0 != 0) goto L_0x009c
            java.lang.String r0 = "Unexpected decode error"
            X.3m4 r3 = new X.3m4     // Catch: InterruptedException -> 0x00ae
            r3.<init>(r0, r1)     // Catch: InterruptedException -> 0x00ae
        L_0x009a:
            monitor-enter(r2)     // Catch: InterruptedException -> 0x00ae
            goto L_0x00a4
        L_0x009c:
            java.lang.String r0 = "Unexpected decode error"
            X.3m6 r3 = new X.3m6     // Catch: InterruptedException -> 0x00ae
            r3.<init>(r0, r1)     // Catch: InterruptedException -> 0x00ae
            goto L_0x009a
        L_0x00a4:
            r4.A03 = r3     // Catch: all -> 0x00a8
            monitor-exit(r2)     // Catch: all -> 0x00a8
            return
        L_0x00a8:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x00a8
            throw r0     // Catch: InterruptedException -> 0x00ae
        L_0x00ab:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x00ab
            throw r0     // Catch: InterruptedException -> 0x00ae
        L_0x00ae:
            r1 = move-exception
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5HC.run():void");
    }
}
