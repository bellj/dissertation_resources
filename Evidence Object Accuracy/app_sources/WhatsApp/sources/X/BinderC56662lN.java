package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;

/* renamed from: X.2lN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56662lN extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115755Su A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC56662lN(AbstractC115755Su r2) {
        super("com.google.android.gms.maps.internal.IOnMapReadyCallback");
        this.A00 = r2;
    }

    @Override // X.AbstractBinderC73293fz
    public final boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        IGoogleMapDelegate r1;
        if (i != 1) {
            return false;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder == null) {
            r1 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IGoogleMapDelegate");
            if (queryLocalInterface instanceof IGoogleMapDelegate) {
                r1 = (IGoogleMapDelegate) queryLocalInterface;
            } else {
                r1 = new C79803rE(readStrongBinder);
            }
        }
        this.A00.ASP(new C35961j4(r1));
        parcel2.writeNoException();
        return true;
    }
}
