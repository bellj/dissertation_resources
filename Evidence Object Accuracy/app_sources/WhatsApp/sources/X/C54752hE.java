package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.search.IteratingPlayer;
import java.util.Iterator;

/* renamed from: X.2hE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54752hE extends AbstractC05270Ox {
    public final /* synthetic */ C36911kq A00;

    public C54752hE(C36911kq r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        C36911kq r7 = this.A00;
        boolean z = r7.A04;
        boolean A1S = C12960it.A1S(i);
        r7.A04 = A1S;
        if (z != A1S) {
            Iterator it = r7.A02.iterator();
            while (it.hasNext()) {
                AbstractC51682Vy r3 = (AbstractC51682Vy) it.next();
                if (r7.A04) {
                    r3.A0C(false);
                    r3.A0B(true);
                } else {
                    IteratingPlayer iteratingPlayer = r7.A0o;
                    int i2 = r3.A06;
                    if (i2 == -1) {
                        i2 = r3.A05;
                    }
                    r3.A0C(C12960it.A1V(i2, iteratingPlayer.A00));
                    r3.A0B(false);
                }
            }
        }
    }
}
