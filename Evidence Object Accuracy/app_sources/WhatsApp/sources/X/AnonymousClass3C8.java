package X;

import android.animation.ValueAnimator;
import android.view.animation.OvershootInterpolator;

/* renamed from: X.3C8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C8 {
    public void A00(AbstractC92674Wx r4) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.setDuration(250L);
        ofFloat.setInterpolator(new OvershootInterpolator(1.2f));
        ofFloat.addListener(new C72783fA(r4, this));
        C12980iv.A11(ofFloat, r4, 7);
        ofFloat.start();
    }
}
