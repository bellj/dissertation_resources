package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30811Yx implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30771Yt(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30771Yt[i];
    }
}
