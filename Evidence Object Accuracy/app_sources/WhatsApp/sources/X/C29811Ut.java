package X;

import android.database.sqlite.SQLiteTransactionListener;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1Ut  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29811Ut implements SQLiteTransactionListener {
    public final /* synthetic */ C29561To A00;
    public final /* synthetic */ AtomicBoolean A01;

    public C29811Ut(C29561To r1, AtomicBoolean atomicBoolean) {
        this.A00 = r1;
        this.A01 = atomicBoolean;
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onBegin() {
        this.A01.set(false);
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onCommit() {
        this.A01.set(true);
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onRollback() {
        this.A01.set(false);
    }
}
