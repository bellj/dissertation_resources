package X;

/* renamed from: X.5MU  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MU extends AnonymousClass1TM {
    public int A00;
    public AnonymousClass5MA A01;
    public C114725Mv A02;
    public AnonymousClass5MN A03;
    public boolean A04 = false;

    @Override // X.AnonymousClass1TM
    public int hashCode() {
        if (!this.A04) {
            this.A00 = super.hashCode();
            this.A04 = true;
        }
        return this.A00;
    }

    public AnonymousClass5MU(AbstractC114775Na r4) {
        AnonymousClass5MN r1;
        if (r4.A0B() == 3) {
            AnonymousClass1TN A0D = r4.A0D(0);
            if (A0D instanceof AnonymousClass5MN) {
                r1 = (AnonymousClass5MN) A0D;
            } else {
                r1 = A0D != null ? new AnonymousClass5MN(AbstractC114775Na.A04(A0D)) : null;
            }
            this.A03 = r1;
            this.A02 = C114725Mv.A00(AbstractC114775Na.A01(r4));
            this.A01 = AnonymousClass5MA.A00(r4.A0D(2));
            return;
        }
        throw C12970iu.A0f("sequence wrong size for CertificateList");
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r1 = new C94954co(3);
        r1.A06(this.A03);
        r1.A06(this.A02);
        return C94954co.A01(this.A01, r1);
    }
}
