package X;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.whatsapp.GifHelper;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.2JJ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2JJ implements AbstractC35581iK {
    public Cursor A00;
    public boolean A01 = false;
    public final int A02;
    public final ContentResolver A03;
    public final Uri A04;
    public final C006202y A05;
    public final C19390u2 A06;
    public final String A07;

    public AnonymousClass2JJ(Uri uri, C16590pI r13, C19390u2 r14, String str, int i) {
        String str2;
        Cursor query;
        String str3;
        String[] strArr;
        String[] strArr2;
        String[] strArr3;
        String str4;
        String[] strArr4;
        C006202y r1 = new C006202y(512);
        this.A05 = r1;
        this.A06 = r14;
        ContentResolver contentResolver = r13.A00.getContentResolver();
        this.A03 = contentResolver;
        this.A02 = i;
        this.A04 = uri;
        this.A07 = str;
        if (!(this instanceof AnonymousClass2JO)) {
            if (this instanceof AnonymousClass2JK) {
                strArr = AnonymousClass2JK.A01;
                if (str == null) {
                    str3 = "(mime_type in (?, ?))";
                } else {
                    str3 = "(mime_type in (?, ?)) AND bucket_id = ?";
                }
                strArr2 = AnonymousClass2JK.A00;
            } else if (this instanceof AnonymousClass2JL) {
                strArr = AnonymousClass2JL.A01;
                if (str == null) {
                    str3 = "(mime_type in (?))";
                } else {
                    str3 = "(mime_type in (?)) AND bucket_id = ?";
                }
                strArr2 = AnonymousClass2JL.A00;
            } else if (!(this instanceof AnonymousClass2JI)) {
                query = contentResolver.query(uri, C16520pA.A05, null, null, A02());
            } else {
                String[] strArr5 = AnonymousClass2JI.A00;
                if (str == null) {
                    str4 = "media_type in (1, 3)";
                    strArr4 = null;
                } else {
                    str4 = "media_type in (1, 3) and bucket_id=?";
                    strArr4 = new String[]{str};
                }
                query = contentResolver.query(uri, strArr5, str4, strArr4, A02());
            }
            if (str != null) {
                int length = strArr2.length;
                strArr3 = new String[length + 1];
                System.arraycopy(strArr2, 0, strArr3, 0, length);
                strArr3[length] = str;
            } else {
                strArr3 = strArr2;
            }
            query = MediaStore.Images.Media.query(contentResolver, uri, strArr, str3, strArr3, A02());
        } else {
            String[] strArr6 = AnonymousClass2JO.A00;
            if (str != null) {
                StringBuilder sb = new StringBuilder("bucket_id = '");
                sb.append(str);
                sb.append("'");
                str2 = sb.toString();
            } else {
                str2 = null;
            }
            query = MediaStore.Images.Media.query(contentResolver, uri, strArr6, str2, null, A02());
        }
        this.A00 = query;
        if (query == null) {
            Log.w("medialist/createCursor returns null");
        }
        r1.A06(0);
    }

    private Cursor A00() {
        Cursor cursor;
        synchronized (this) {
            Cursor cursor2 = this.A00;
            if (cursor2 == null) {
                cursor = null;
            } else {
                if (this.A01) {
                    cursor2.requery();
                    this.A01 = false;
                }
                cursor = this.A00;
            }
        }
        return cursor;
    }

    public Uri A01(long j) {
        try {
            Uri uri = this.A04;
            if (ContentUris.parseId(uri) != j) {
                Log.e("medialist/id mismatch");
            }
            return uri;
        } catch (NumberFormatException unused) {
            return ContentUris.withAppendedId(this.A04, j);
        }
    }

    public String A02() {
        String str;
        if (this.A02 == 1) {
            str = " ASC";
        } else {
            str = " DESC";
        }
        StringBuilder sb = new StringBuilder("case ifnull(datetaken,0) when 0 then date_modified*1000 else datetaken end");
        sb.append(str);
        sb.append(", _id");
        sb.append(str);
        return sb.toString();
    }

    @Override // X.AbstractC35581iK
    public AbstractC35611iN AEC(int i) {
        C006202y r6 = this.A05;
        Integer valueOf = Integer.valueOf(i);
        AbstractC35611iN r12 = (AbstractC35611iN) r6.A04(valueOf);
        if (r12 == null) {
            Cursor A00 = A00();
            r12 = null;
            if (A00 != null) {
                synchronized (this) {
                    if (A00.moveToPosition(i)) {
                        if (this instanceof AnonymousClass2JO) {
                            long j = A00.getLong(0);
                            String string = A00.getString(1);
                            long j2 = A00.getLong(2);
                            if (j2 == 0) {
                                j2 = A00.getLong(6) * 1000;
                            }
                            String string2 = A00.getString(5);
                            long j3 = A00.getLong(7);
                            if (string == null || !GifHelper.A01(new File(string))) {
                                r12 = new C616831q(this.A03, A01(j), string, string2, j, j2, j3);
                            } else {
                                r12 = new AnonymousClass441(this.A03, A01(j), string, string2, j, j2, j3);
                            }
                        } else if ((this instanceof AnonymousClass2JK) || (this instanceof AnonymousClass2JL)) {
                            long j4 = A00.getLong(0);
                            String string3 = A00.getString(1);
                            long j5 = A00.getLong(2);
                            if (j5 == 0) {
                                j5 = A00.getLong(7) * 1000;
                            }
                            r12 = new C616931r(this.A03, A01(j4), string3, A00.getString(6), A00.getInt(4), j4, j5, A00.getLong(8));
                        } else if (!(this instanceof AnonymousClass2JI)) {
                            String string4 = A00.getString(1);
                            AbstractC35611iN r9 = null;
                            if (string4 != null) {
                                long j6 = A00.getLong(2);
                                short s = A00.getShort(5);
                                File file = new File(string4);
                                if (s == 1) {
                                    r9 = new C616331e(null, file, j6);
                                } else if (s == 3) {
                                    r9 = new C857443z(null, file, j6, A00.getLong(6));
                                } else if (s == 13) {
                                    r9 = new C857343y(null, file, j6, A00.getLong(6));
                                }
                            }
                            r12 = r9;
                            if (r9 != null) {
                            }
                        } else {
                            long j7 = A00.getLong(0);
                            String string5 = A00.getString(1);
                            long j8 = A00.getLong(5);
                            if (j8 == 0) {
                                j8 = A00.getLong(4) * 1000;
                            }
                            String string6 = A00.getString(2);
                            int i2 = A00.getInt(3);
                            long j9 = A00.getLong(7);
                            File file2 = string5 != null ? new File(string5) : null;
                            if (i2 == 3) {
                                if (!GifHelper.A01(file2)) {
                                    r12 = new C616831q(this.A03, A01(j7), string5, string6, j7, j8, j9);
                                }
                                r12 = new AnonymousClass441(this.A03, A01(j7), string5, string6, j7, j8, j9);
                            } else {
                                if ("image/gif".equals(string6) && file2 != null) {
                                    try {
                                        C38911ou.A03(file2);
                                        try {
                                        } catch (IOException e) {
                                            Log.e("LoadMediaFromCursor/Image/Gif/IsSingleFrameGif/IOException", e);
                                        } catch (OutOfMemoryError e2) {
                                            Log.e("LoadMediaFromCursor/Image/Gif/IsSingleFrameGif/OutOfMemory", e2);
                                        }
                                        if (!(!C38911ou.A03(file2).A02)) {
                                            r12 = new AnonymousClass441(this.A03, A01(j7), string5, string6, j7, j8, j9);
                                        }
                                    } catch (IOException unused) {
                                    }
                                }
                                r12 = new C616931r(this.A03, A01(j7), string5, string6, A00.getInt(6), j7, j8, j9);
                            }
                        }
                        r6.A08(valueOf, r12);
                    }
                }
                return r12;
            }
        }
        return r12;
    }

    @Override // X.AbstractC35581iK
    public void AaX() {
        Cursor cursor;
        if ((this instanceof C617031s) && (cursor = this.A00) != null) {
            cursor.deactivate();
            this.A01 = true;
        }
    }

    @Override // X.AbstractC35581iK
    public void close() {
        try {
            Cursor cursor = this.A00;
            if (cursor != null) {
                cursor.deactivate();
                this.A01 = true;
            }
        } catch (IllegalStateException e) {
            Log.e("medialist/exception while deactivating cursor", e);
        }
        Cursor cursor2 = this.A00;
        if (cursor2 != null) {
            cursor2.close();
            this.A00 = null;
        }
    }

    @Override // X.AbstractC35581iK
    public int getCount() {
        int count;
        Cursor A00 = A00();
        if (A00 == null) {
            return 0;
        }
        synchronized (this) {
            count = A00.getCount();
        }
        return count;
    }

    @Override // X.AbstractC35581iK
    public boolean isEmpty() {
        return getCount() == 0;
    }

    @Override // X.AbstractC35581iK
    public void registerContentObserver(ContentObserver contentObserver) {
        Cursor cursor;
        if ((this instanceof C617031s) && (cursor = this.A00) != null) {
            cursor.registerContentObserver(contentObserver);
        }
    }

    @Override // X.AbstractC35581iK
    public void unregisterContentObserver(ContentObserver contentObserver) {
        Cursor cursor;
        if ((this instanceof C617031s) && (cursor = this.A00) != null) {
            cursor.unregisterContentObserver(contentObserver);
        }
    }
}
