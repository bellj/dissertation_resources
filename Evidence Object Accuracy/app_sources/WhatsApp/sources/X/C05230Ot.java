package X;

import android.database.sqlite.SQLiteException;
import android.util.Log;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0Ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05230Ot {
    public AnonymousClass0N2 A00;
    public Runnable A01 = new RunnableC10130e5(this);
    public Map A02;
    public AtomicBoolean A03 = new AtomicBoolean(false);
    public final AnonymousClass03E A04 = new AnonymousClass03E();
    public final AnonymousClass0N1 A05;
    public final AnonymousClass0QN A06;
    public final HashMap A07;
    public final String[] A08;
    public volatile AbstractC12830ic A09;
    public volatile boolean A0A = false;

    public C05230Ot(AnonymousClass0QN r7, Map map, Map map2, String... strArr) {
        this.A06 = r7;
        int length = strArr.length;
        this.A00 = new AnonymousClass0N2(length);
        this.A07 = new HashMap();
        this.A02 = map2;
        this.A05 = new AnonymousClass0N1(r7);
        this.A08 = new String[length];
        for (int i = 0; i < length; i++) {
            String str = strArr[i];
            Locale locale = Locale.US;
            String lowerCase = str.toLowerCase(locale);
            this.A07.put(lowerCase, Integer.valueOf(i));
            String str2 = (String) map.get(strArr[i]);
            if (str2 != null) {
                this.A08[i] = str2.toLowerCase(locale);
            } else {
                this.A08[i] = lowerCase;
            }
        }
        for (Map.Entry entry : map.entrySet()) {
            Locale locale2 = Locale.US;
            String lowerCase2 = ((String) entry.getValue()).toLowerCase(locale2);
            if (this.A07.containsKey(lowerCase2)) {
                String lowerCase3 = ((String) entry.getKey()).toLowerCase(locale2);
                HashMap hashMap = this.A07;
                hashMap.put(lowerCase3, hashMap.get(lowerCase2));
            }
        }
    }

    public void A00(AbstractC12920im r4) {
        if (!((AnonymousClass0ZE) r4).A00.inTransaction()) {
            try {
                ReentrantReadWriteLock.ReadLock readLock = this.A06.A09.readLock();
                readLock.lock();
                try {
                    synchronized (this.A00) {
                    }
                } finally {
                    readLock.unlock();
                }
            } catch (SQLiteException | IllegalStateException e) {
                Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e);
            }
        }
    }
}
