package X;

import android.content.ContentResolver;
import android.util.Log;
import java.util.HashMap;

/* renamed from: X.4zC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C108504zC implements AbstractC115585Sd {
    public final String A00;

    public C108504zC(String str) {
        this.A00 = str;
    }

    @Override // X.AbstractC115585Sd
    public final Object AhR() {
        Object obj;
        String str = this.A00;
        ContentResolver contentResolver = AbstractC95414df.A06.getContentResolver();
        boolean z = false;
        synchronized (C95244dN.class) {
            C95244dN.A01(contentResolver);
            obj = C95244dN.A00;
        }
        HashMap hashMap = C95244dN.A06;
        boolean z2 = false;
        synchronized (C95244dN.class) {
            if (hashMap.containsKey(str)) {
                Object obj2 = hashMap.get(str);
                if (obj2 != null) {
                    z2 = obj2;
                }
            } else {
                z2 = null;
            }
        }
        Boolean bool = (Boolean) z2;
        if (bool != null) {
            z = bool.booleanValue();
        } else {
            String A00 = C95244dN.A00(contentResolver, str);
            if (A00 != null && !A00.equals("")) {
                if (C95244dN.A0B.matcher(A00).matches()) {
                    z = true;
                    bool = Boolean.TRUE;
                } else if (C95244dN.A0C.matcher(A00).matches()) {
                    bool = Boolean.FALSE;
                } else {
                    StringBuilder A0k = C12960it.A0k("attempt to read gservices key ");
                    A0k.append(str);
                    A0k.append(" (value \"");
                    A0k.append(A00);
                    Log.w("Gservices", C12960it.A0d("\") as boolean", A0k));
                }
            }
            synchronized (C95244dN.class) {
                if (obj == C95244dN.A00) {
                    hashMap.put(str, bool);
                    C95244dN.A01.remove(str);
                }
            }
        }
        return Boolean.valueOf(z);
    }
}
