package X;

import android.media.AudioManager;

/* renamed from: X.3LV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LV implements AudioManager.OnAudioFocusChangeListener {
    public final AnonymousClass11P A00;

    public AnonymousClass3LV(AnonymousClass11P r1) {
        this.A00 = r1;
    }

    @Override // android.media.AudioManager.OnAudioFocusChangeListener
    public void onAudioFocusChange(int i) {
        C35191hP A00 = this.A00.A00();
        StringBuilder A0k = C12960it.A0k("messageaudioplayer/onaudiofocuschanged ");
        A0k.append(i);
        A0k.append(" current player:");
        A0k.append(C12960it.A1W(A00));
        C12960it.A1F(A0k);
        if (AnonymousClass01I.A01() && A00 != null) {
            if (i == -2 || i == -1) {
                if (A00.A0I()) {
                    A00.A0E(false);
                }
            } else if (i == 1 && A00.A0U) {
                A00.A0A(0, true, false);
            }
        }
    }
}
