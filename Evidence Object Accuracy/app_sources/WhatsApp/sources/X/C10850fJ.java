package X;

/* renamed from: X.0fJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10850fJ extends Throwable {
    public C10850fJ() {
        super("Failure occurred while trying to finish a future.");
    }

    @Override // java.lang.Throwable
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
