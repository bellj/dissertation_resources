package X;

/* renamed from: X.5MF  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MF extends AnonymousClass1TM {
    public C114525Mb A00;

    public AnonymousClass5MF(C114525Mb r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return C94954co.A01(this.A00, C94954co.A00());
    }
}
