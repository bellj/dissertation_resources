package X;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2wQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC60272wQ extends AbstractActivityC83723xn {
    public C48882Ih A00;
    public C21770xx A01;
    public AnonymousClass19Q A02;
    public UserJid A03;
    public String A04;
    public final AbstractC16710pd A05 = AnonymousClass4Yq.A00(new C71893dg(this));
    public final AbstractC16710pd A06 = AnonymousClass4Yq.A00(new C71903dh(this));

    public final UserJid A2e() {
        UserJid userJid = this.A03;
        if (userJid != null) {
            return userJid;
        }
        throw C16700pc.A06("bizJid");
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Parcelable parcelable;
        super.onCreate(bundle);
        if (bundle == null) {
            parcelable = getIntent().getParcelableExtra("business_owner_jid");
        } else {
            parcelable = bundle.getParcelable("business_owner_jid");
        }
        AnonymousClass009.A05(parcelable);
        C16700pc.A0B(parcelable);
        UserJid userJid = (UserJid) parcelable;
        C16700pc.A0E(userJid, 0);
        this.A03 = userJid;
        AbstractC16710pd r2 = this.A06;
        C12960it.A18(this, ((C53872fS) r2.getValue()).A00, 49);
        C12960it.A18(this, ((C53872fS) r2.getValue()).A01, 48);
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        C16700pc.A0E(menu, 0);
        MenuItem A0P = ActivityC13790kL.A0P(menu);
        C12960it.A0y(A0P.getActionView(), this, 0);
        TextView A0J = C12960it.A0J(A0P.getActionView(), R.id.cart_total_quantity);
        String str = this.A04;
        if (str != null) {
            A0J.setText(str);
        }
        AbstractC16710pd r2 = this.A05;
        ((C53852fO) r2.getValue()).A00.A05(this, new AnonymousClass02B(A0P, this) { // from class: X.4u4
            public final /* synthetic */ MenuItem A00;
            public final /* synthetic */ AbstractActivityC60272wQ A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
                if (r3.A04 == null) goto L_0x0013;
             */
            @Override // X.AnonymousClass02B
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void ANq(java.lang.Object r5) {
                /*
                    r4 = this;
                    X.2wQ r3 = r4.A01
                    android.view.MenuItem r2 = r4.A00
                    boolean r1 = X.C12970iu.A1Y(r5)
                    r0 = 0
                    X.C16700pc.A0E(r3, r0)
                    if (r1 == 0) goto L_0x0013
                    java.lang.String r1 = r3.A04
                    r0 = 1
                    if (r1 != 0) goto L_0x0014
                L_0x0013:
                    r0 = 0
                L_0x0014:
                    r2.setVisible(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C105444u4.ANq(java.lang.Object):void");
            }
        });
        ((C53852fO) r2.getValue()).A05();
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        ((C53872fS) this.A06.getValue()).A03.A00();
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        C16700pc.A0E(bundle, 0);
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("business_owner_jid", A2e());
    }
}
