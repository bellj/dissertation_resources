package X;

/* renamed from: X.5NY  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NY extends AbstractC114775Na {
    public int A00 = -1;

    public AnonymousClass5NY() {
    }

    public AnonymousClass5NY(AnonymousClass1TN r2) {
        super(r2);
    }

    public AnonymousClass5NY(C94954co r2) {
        super(r2);
    }

    public AnonymousClass5NY(AnonymousClass1TN[] r2) {
        super(r2, false);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int i = this.A00;
        if (i < 0) {
            int length = super.A00.length;
            i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                i = C72453ed.A0S(super.A00, i2, i);
            }
            this.A00 = i;
        }
        return AnonymousClass1TQ.A00(i) + 1 + i;
    }
}
