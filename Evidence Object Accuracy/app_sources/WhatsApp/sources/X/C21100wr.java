package X;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0wr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21100wr {
    public final C14850m9 A00;
    public final Map A01 = new HashMap();

    public C21100wr(C14850m9 r2) {
        this.A00 = r2;
    }

    public AnonymousClass1RV A00(C15950oC r3) {
        AnonymousClass1RV r0;
        Map map = this.A01;
        synchronized (map) {
            r0 = (AnonymousClass1RV) map.get(r3);
        }
        return r0;
    }

    public Set A01(List list) {
        HashSet hashSet = new HashSet(list.size());
        Map map = this.A01;
        synchronized (map) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C15950oC r1 = (C15950oC) it.next();
                AnonymousClass1RV r0 = (AnonymousClass1RV) map.get(r1);
                if (r0 != null && r0.A00) {
                    hashSet.add(r1);
                }
            }
        }
        return hashSet;
    }

    public Set A02(List list) {
        HashSet hashSet = new HashSet(list.size());
        Map map = this.A01;
        synchronized (map) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C15950oC r1 = (C15950oC) it.next();
                if (!map.containsKey(r1)) {
                    hashSet.add(r1);
                }
            }
        }
        return hashSet;
    }

    public void A03(AnonymousClass1RV r3, C15950oC r4) {
        if (this.A00.A07(1056)) {
            Map map = this.A01;
            synchronized (map) {
                map.put(r4, r3);
            }
        }
    }

    public void A04(C15950oC r3) {
        Map map = this.A01;
        synchronized (map) {
            map.remove(r3);
        }
    }

    public void A05(Collection collection) {
        if (this.A00.A07(1056)) {
            Map map = this.A01;
            synchronized (map) {
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    C15950oC r1 = (C15950oC) it.next();
                    if (!map.containsKey(r1)) {
                        map.put(r1, new AnonymousClass1RV());
                    }
                }
            }
        }
    }
}
