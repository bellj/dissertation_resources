package X;

/* renamed from: X.4Xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92824Xo {
    public int A00;
    public String A01;
    public final int A02;
    public final int A03;
    public final String A04;

    public C92824Xo(int i, int i2, int i3) {
        String str;
        if (i != Integer.MIN_VALUE) {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(i);
            str = C12960it.A0d("/", A0h);
        } else {
            str = "";
        }
        this.A04 = str;
        this.A02 = i2;
        this.A03 = i3;
        this.A00 = Integer.MIN_VALUE;
        this.A01 = "";
    }

    public static AnonymousClass5X6 A00(AbstractC14070ko r2, C92824Xo r3) {
        return r2.Af4(r3.A01(), 1);
    }

    public int A01() {
        int i = this.A00;
        if (i != Integer.MIN_VALUE) {
            return i;
        }
        throw C12960it.A0U("generateNewId() must be called before retrieving ids.");
    }

    public String A02() {
        if (this.A00 != Integer.MIN_VALUE) {
            return this.A01;
        }
        throw C12960it.A0U("generateNewId() must be called before retrieving ids.");
    }

    public void A03() {
        int i;
        int i2 = this.A00;
        if (i2 == Integer.MIN_VALUE) {
            i = this.A02;
        } else {
            i = i2 + this.A03;
        }
        this.A00 = i;
        this.A01 = C12960it.A0e(this.A04, C12960it.A0h(), i);
    }
}
