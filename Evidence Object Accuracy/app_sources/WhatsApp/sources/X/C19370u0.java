package X;

import android.content.SharedPreferences;
import java.util.UUID;

/* renamed from: X.0u0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19370u0 {
    public final C14830m7 A00;
    public final C14820m6 A01;

    public C19370u0(C14830m7 r1, C14820m6 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public synchronized C27371Hb A00() {
        C27371Hb r3;
        SharedPreferences sharedPreferences = this.A01.A00;
        String string = sharedPreferences.getString("phoneid_id", null);
        long j = sharedPreferences.getLong("phoneid_timestamp", -1);
        if (string == null || j == -1) {
            r3 = new C27371Hb(UUID.randomUUID().toString(), this.A00.A00());
            A01(r3);
        } else {
            r3 = new C27371Hb(string, j);
        }
        return r3;
    }

    public synchronized void A01(C27371Hb r7) {
        C14820m6 r5 = this.A01;
        String str = r7.A01;
        long j = r7.A00;
        r5.A00.edit().putString("phoneid_id", str).apply();
        r5.A0q("phoneid_timestamp", j);
    }
}
