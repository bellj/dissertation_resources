package X;

import com.facebook.msys.mci.Execution;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2Ct  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC47812Ct extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ long A02;
    public final /* synthetic */ AbstractRunnableC47782Cq A03;

    public RunnableC47812Ct(AbstractRunnableC47782Cq r1, int i, int i2, long j) {
        this.A03 = r1;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = j;
    }

    @Override // java.lang.Runnable
    public void run() {
        Execution.executeAfterWithPriorityInternal(this.A03, this.A00, this.A01, this.A02);
    }
}
