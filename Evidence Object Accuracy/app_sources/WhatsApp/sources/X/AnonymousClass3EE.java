package X;

import com.whatsapp.status.playback.StatusPlaybackActivity;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.3EE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3EE {
    public List A00 = C12960it.A0l();
    public final HashMap A01 = C12970iu.A11();
    public final /* synthetic */ StatusPlaybackActivity A02;

    public /* synthetic */ AnonymousClass3EE(StatusPlaybackActivity statusPlaybackActivity) {
        this.A02 = statusPlaybackActivity;
    }

    public int A00(String str) {
        int i = 0;
        for (AnonymousClass4LD r0 : this.A00) {
            if (r0.A00.A0A.getRawString().equals(str)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public void A01(AnonymousClass4LD r4) {
        this.A00.add(r4);
        String rawString = r4.A00.A0A.getRawString();
        HashMap hashMap = this.A01;
        if (!hashMap.containsKey(rawString)) {
            hashMap.put(rawString, C12980iv.A0l(hashMap.size()));
        }
    }
}
