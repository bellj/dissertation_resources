package X;

import android.content.Context;
import android.util.TypedValue;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.2Ul  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ul extends LinearLayout implements AnonymousClass004 {
    public WaImageView A00;
    public WaTextView A01;
    public AnonymousClass2P7 A02;
    public boolean A03;

    public AnonymousClass2Ul(Context context) {
        super(context);
        if (!this.A03) {
            this.A03 = true;
            generatedComponent();
        }
        LinearLayout.inflate(getContext(), R.layout.storage_usage_gallery_sort_bottom_sheet_row, this);
        setOrientation(0);
        setGravity(16);
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(16843534, typedValue, true);
        setBackgroundResource(typedValue.resourceId);
        this.A01 = (WaTextView) AnonymousClass028.A0D(this, R.id.storage_usage_sort_row_text);
        this.A00 = (WaImageView) AnonymousClass028.A0D(this, R.id.storage_usage_sort_row_checkmark);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A02;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A02 = r0;
        }
        return r0.generatedComponent();
    }

    public void setChecked(boolean z) {
        WaImageView waImageView = this.A00;
        int i = 8;
        if (z) {
            i = 0;
        }
        waImageView.setVisibility(i);
    }

    public void setText(String str) {
        this.A01.setText(str);
    }
}
