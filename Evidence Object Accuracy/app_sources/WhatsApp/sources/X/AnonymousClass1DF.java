package X;

import android.content.SharedPreferences;

/* renamed from: X.1DF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DF implements AbstractC16990q5 {
    public final AnonymousClass1AT A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1DF(AnonymousClass1AT r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        AnonymousClass1AT r4 = this.A00;
        for (Number number : AnonymousClass1AT.A02) {
            int intValue = number.intValue();
            AnonymousClass1AS r3 = r4.A01;
            SharedPreferences sharedPreferences = r3.A00;
            if (sharedPreferences == null) {
                sharedPreferences = r3.A01.A01("privacy_highlight");
                r3.A00 = sharedPreferences;
            }
            String str = "0,0,0";
            String string = sharedPreferences.getString(AnonymousClass1AS.A00(intValue), str);
            if (string != null) {
                str = string;
            }
            String[] split = str.split(",");
            if (split.length != 3) {
                break;
            }
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            int parseInt3 = Integer.parseInt(split[2]);
            if (parseInt > 0 || parseInt2 > 0 || parseInt3 > 0) {
                C614530k r2 = new C614530k();
                r2.A00 = 0;
                r2.A04 = Long.valueOf((long) parseInt);
                r2.A02 = Long.valueOf((long) parseInt2);
                r2.A03 = Long.valueOf((long) parseInt3);
                r2.A01 = Integer.valueOf(intValue);
                r4.A00.A07(r2);
            }
        }
        AnonymousClass1AS r22 = r4.A01;
        SharedPreferences sharedPreferences2 = r22.A00;
        if (sharedPreferences2 == null) {
            sharedPreferences2 = r22.A01.A01("privacy_highlight");
            r22.A00 = sharedPreferences2;
        }
        sharedPreferences2.edit().clear().apply();
    }
}
