package X;

import android.content.res.ColorStateList;
import android.content.res.Configuration;

/* renamed from: X.07e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C015107e {
    public final ColorStateList A00;
    public final Configuration A01;

    public C015107e(ColorStateList colorStateList, Configuration configuration) {
        this.A00 = colorStateList;
        this.A01 = configuration;
    }
}
