package X;

/* renamed from: X.5K2  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5K2 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public AnonymousClass5K2() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public Object AJ4(Object obj) {
        if (!(obj instanceof AbstractC10990fX)) {
            return null;
        }
        return obj;
    }
}
