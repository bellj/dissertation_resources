package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.shapes.OvalShape;

/* renamed from: X.0AC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AC extends OvalShape {
    public Paint A00 = new Paint();
    public RadialGradient A01;
    public final /* synthetic */ C02350Bi A02;

    public AnonymousClass0AC(C02350Bi r2, int i) {
        this.A02 = r2;
        r2.A00 = i;
        A00((int) rect().width());
    }

    public final void A00(int i) {
        float f = (float) (i / 2);
        RadialGradient radialGradient = new RadialGradient(f, f, (float) this.A02.A00, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
        this.A01 = radialGradient;
        this.A00.setShader(radialGradient);
    }

    @Override // android.graphics.drawable.shapes.OvalShape, android.graphics.drawable.shapes.Shape, android.graphics.drawable.shapes.RectShape
    public void draw(Canvas canvas, Paint paint) {
        C02350Bi r4 = this.A02;
        int width = r4.getWidth() >> 1;
        float f = (float) width;
        float height = (float) (r4.getHeight() >> 1);
        canvas.drawCircle(f, height, f, this.A00);
        canvas.drawCircle(f, height, (float) (width - r4.A00), paint);
    }

    @Override // android.graphics.drawable.shapes.Shape, android.graphics.drawable.shapes.RectShape
    public void onResize(float f, float f2) {
        super.onResize(f, f2);
        A00((int) f);
    }
}
