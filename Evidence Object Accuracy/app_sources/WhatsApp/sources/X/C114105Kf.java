package X;

/* renamed from: X.5Kf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114105Kf extends AbstractC114115Kg {
    public final AnonymousClass5LD A00;
    public final /* synthetic */ AbstractC11150fp A01;

    public C114105Kf(AbstractC11150fp r1, AnonymousClass5LD r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        this.A00.A06();
        return AnonymousClass1WZ.A00;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("RemoveReceiveOnCancel[");
        A0k.append(this.A00);
        return C72453ed.A0t(A0k);
    }
}
