package X;

/* renamed from: X.3E1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3E1 {
    public final AnonymousClass1V8 A00;
    public final C64023Dx A01;

    public AnonymousClass3E1(AbstractC15710nm r2, AnonymousClass1V8 r3) {
        AnonymousClass1V8.A01(r3, "next_screens");
        this.A01 = (C64023Dx) AnonymousClass3JT.A01(r2, r3, 17);
        this.A00 = r3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass3E1.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass3E1) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
