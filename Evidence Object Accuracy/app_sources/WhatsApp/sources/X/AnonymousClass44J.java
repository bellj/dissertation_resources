package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44J  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44J extends AbstractC51682Vy {
    public final C15550nR A00;
    public final C63563Cb A01;
    public final SearchViewModel A02;
    public final C621434s A03;

    public AnonymousClass44J(C15550nR r1, C63563Cb r2, SearchViewModel searchViewModel, C621434s r4) {
        super(r4);
        this.A03 = r4;
        this.A02 = searchViewModel;
        this.A00 = r1;
        this.A01 = r2;
    }
}
