package X;

/* renamed from: X.3Wt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68793Wt implements AbstractC116695Wl {
    public final /* synthetic */ C68883Xc A00;
    public final /* synthetic */ AbstractC44401yr A01;

    public C68793Wt(C68883Xc r1, AbstractC44401yr r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC116695Wl
    public void AOy() {
        this.A01.AOz(C12990iw.A0i("network error while creating user"));
    }

    @Override // X.AbstractC116695Wl
    public void APp(Exception exc) {
        this.A01.APp(exc);
    }

    @Override // X.AbstractC116695Wl
    public void AWx(C64063Ec r5) {
        C68883Xc r3 = this.A00;
        AbstractC44401yr r2 = this.A01;
        AnonymousClass5UI r1 = r3.A03;
        Object obj = r5.A02.A00;
        AnonymousClass009.A05(obj);
        r1.A7x((String) obj).AZO(new C68873Xb(r5, r3, r2));
    }
}
