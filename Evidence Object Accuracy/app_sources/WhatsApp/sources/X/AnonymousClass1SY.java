package X;

import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.MultiBufferLogger;
import com.facebook.soloader.SoLoader;

/* renamed from: X.1SY  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1SY {
    public int A00;
    public C29441Sr A01;
    public MultiBufferLogger A02;
    public String A03;
    public boolean A04;
    public volatile boolean A05;

    public abstract void disable();

    public abstract void enable();

    public abstract int getSupportedProviders();

    public abstract int getTracingProviders();

    public void onTraceEnded(C29441Sr r1, AnonymousClass1Se r2) {
    }

    public void onTraceStarted(C29441Sr r1, AnonymousClass1Se r2) {
    }

    public AnonymousClass1SY(String str) {
        this.A03 = str;
        this.A04 = str == null;
    }

    public final MultiBufferLogger A00() {
        if (!this.A05) {
            synchronized (this) {
                if (!this.A05) {
                    this.A02 = new MultiBufferLogger();
                    this.A05 = true;
                }
            }
        }
        return this.A02;
    }

    public final void A01() {
        if (!this.A04) {
            synchronized (this) {
                if (!this.A04) {
                    MultiBufferLogger A00 = A00();
                    int writeStandardEntry = A00.writeStandardEntry(6, 21, 0, 0, 0, 0, 0);
                    StringBuilder sb = new StringBuilder();
                    sb.append("ensureSoLibLoaded: ");
                    String str = this.A03;
                    sb.append(str);
                    A00.writeBytesEntry(0, 83, writeStandardEntry, sb.toString());
                    SoLoader.A04(str);
                    this.A04 = true;
                    A00.writeStandardEntry(6, 22, 0, 0, 0, 0, 0);
                }
            }
        }
    }

    public final void A02(C29441Sr r4) {
        int supportedProviders = getSupportedProviders() & TraceEvents.sProviders;
        int i = this.A00;
        if (i != 0) {
            int i2 = i & TraceEvents.sProviders;
            int i3 = this.A00;
            if (i2 == i3) {
                return;
            }
            if (i3 != 0) {
                disable();
                this.A01 = null;
            }
        }
        if (supportedProviders != 0) {
            this.A01 = r4;
            enable();
        }
        this.A00 = supportedProviders;
    }

    public final void A03(C29441Sr r3, AnonymousClass1Se r4) {
        if (this.A00 != 0 && (r3.A02 & getSupportedProviders()) != 0) {
            A01();
            onTraceEnded(r3, r4);
            A02(r3);
            A00().removeBuffer(r3.A09);
        }
    }
}
