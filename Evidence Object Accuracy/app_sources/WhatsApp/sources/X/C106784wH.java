package X;

import java.io.EOFException;

/* renamed from: X.4wH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106784wH implements AbstractC116785Ww {
    public int A00;
    public int A01;
    public long A02 = -9223372036854775807L;
    public long A03;
    public long A04;
    public AbstractC14070ko A05;
    public AnonymousClass5X6 A06;
    public AnonymousClass5X6 A07;
    public AbstractC117075Yd A08;
    public C100624mD A09;
    public boolean A0A;
    public final long A0B = -9223372036854775807L;
    public final AnonymousClass4W1 A0C = new AnonymousClass4W1();
    public final C93974b3 A0D = new C93974b3();
    public final C92084Ul A0E = new C92084Ul();
    public final AnonymousClass5X6 A0F;
    public final C95304dT A0G = C95304dT.A05(10);

    public C106784wH() {
        C106984wb r0 = new C106984wb();
        this.A0F = r0;
        this.A06 = r0;
    }

    public final AbstractC117075Yd A00(AnonymousClass5Yf r7) {
        C95304dT r3 = this.A0G;
        r7.AZ4(r3.A02, 0, 4);
        r3.A0S(0);
        AnonymousClass4W1 r1 = this.A0C;
        r1.A00(r3.A07());
        return new C76823mI(r1, r7.getLength(), r7.AFo());
    }

    public final boolean A01(AnonymousClass5Yf r8) {
        AbstractC117075Yd r0 = this.A08;
        if (r0 != null) {
            long ACO = r0.ACO();
            if (ACO != -1 && r8.AFf() > ACO - 4) {
                return true;
            }
        }
        try {
            return !r8.AZ5(this.A0G.A02, 0, 4, true);
        } catch (EOFException unused) {
            return true;
        }
    }

    public final boolean A02(AnonymousClass5Yf r18, boolean z) {
        int i;
        int i2;
        int A00;
        int i3 = C25981Bo.A0F;
        if (z) {
            i3 = 32768;
        }
        r18.Aaj();
        if (r18.AFo() == 0) {
            C100624mD A002 = this.A0E.A00(r18, null);
            this.A09 = A002;
            if (A002 != null) {
                this.A0D.A00(A002);
            }
            i2 = (int) r18.AFf();
            if (!z) {
                r18.Ae3(i2);
            }
            i = 0;
        } else {
            i = 0;
            i2 = 0;
        }
        int i4 = 0;
        int i5 = 0;
        while (true) {
            if (!A01(r18)) {
                int A03 = C95304dT.A03(this.A0G, 0);
                if ((i == 0 || ((long) (-128000 & A03)) == (((long) i) & -128000)) && (A00 = C94714cQ.A00(A03)) != -1) {
                    i4++;
                    if (i4 != 1) {
                        if (i4 == 4) {
                            break;
                        }
                    } else {
                        this.A0C.A00(A03);
                        i = A03;
                    }
                    r18.A5r(A00 - 4);
                } else {
                    int i6 = i5 + 1;
                    if (i5 != i3) {
                        if (z) {
                            r18.Aaj();
                            r18.A5r(i2 + i6);
                        } else {
                            r18.Ae3(1);
                        }
                        i5 = i6;
                        i = 0;
                        i4 = 0;
                    } else if (z) {
                        return false;
                    } else {
                        throw AnonymousClass496.A00("Searched too many bytes.");
                    }
                }
            } else if (i4 <= 0) {
                throw new EOFException();
            }
        }
        if (z) {
            r18.Ae3(i2 + i5);
        } else {
            r18.Aaj();
        }
        this.A01 = i;
        return true;
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r3) {
        this.A05 = r3;
        AnonymousClass5X6 Af4 = r3.Af4(0, 1);
        this.A07 = Af4;
        this.A06 = Af4;
        this.A05.A9V();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x021f, code lost:
        r4 = X.C12960it.A0k("XING data size mismatch: ");
        r4.append(r1);
        r4.append(", ");
        android.util.Log.w("XingSeeker", X.C12970iu.A0w(r4, r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0236, code lost:
        r4 = new X.C107034wg(r15, r3.A02, r18, r20, r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0249, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0250, code lost:
        if (r12.A00 < 40) goto L_0x025f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x025b, code lost:
        if (X.C95304dT.A03(r12, 36) != 1447187017) goto L_0x025f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x025f, code lost:
        r4 = null;
        r32.Aaj();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0267, code lost:
        if (r1 != 1) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x026b, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0283, code lost:
        r5 = new X.C107044wh(r10, r9, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0288, code lost:
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x028a, code lost:
        if (r31.A0A == false) goto L_0x035d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x028c, code lost:
        r4 = new X.C76833mJ();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0291, code lost:
        r31.A08 = r4;
        r31.A05.AbR(r4);
        r5 = r31.A06;
        r4 = X.C93844ap.A00();
        r4.A0R = r3.A06;
        r4.A08 = 4096;
        r4.A04 = r3.A01;
        r4.A0D = r3.A03;
        r2 = r31.A0D;
        r4.A05 = r2.A00;
        r4.A06 = r2.A01;
        r4.A0J = r31.A09;
        X.C72453ed.A17(r4, r5);
        r31.A03 = r32.AFo();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        if (r1 == 1) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        r9 = 21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x035d, code lost:
        if (r5 == null) goto L_0x0362;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x035f, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0362, code lost:
        if (r4 != null) goto L_0x0291;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0364, code lost:
        r4 = A00(r32);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        if (r12.A00 < (r9 + 4)) goto L_0x024c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
        r5 = X.C95304dT.A03(r12, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0041, code lost:
        if (r5 == 1483304551) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0046, code lost:
        if (r5 != 1231971951) goto L_0x024c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004e, code lost:
        if (r5 == 1483304551) goto L_0x017a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
        if (r5 == 1231971951) goto L_0x017a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0055, code lost:
        if (r5 != 1447187017) goto L_0x025f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        r6 = r32.getLength();
        r1 = r32.AFo();
        r12.A0T(10);
        r5 = r12.A07();
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0069, code lost:
        if (r5 <= 0) goto L_0x00bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006b, code lost:
        r9 = r3.A03;
        r13 = (long) r5;
        r4 = 576;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0075, code lost:
        if (r9 < 32000) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0077, code lost:
        r4 = 1152;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0079, code lost:
        r20 = X.AnonymousClass3JZ.A07(r13, com.google.android.search.verification.client.SearchActionVerificationClientService.MS_TO_NS * ((long) r4), (long) r9);
        r10 = r12.A0F();
        r19 = r12.A0F();
        r9 = r12.A0F();
        r12.A0T(2);
        r17 = r1 + ((long) r3.A02);
        r4 = new long[r10];
        r15 = new long[r10];
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009e, code lost:
        if (r11 >= r10) goto L_0x014a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a0, code lost:
        r4[r11] = (((long) r11) * r20) / ((long) r10);
        r15[r11] = java.lang.Math.max(r1, r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b0, code lost:
        if (r9 == 1) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b3, code lost:
        if (r9 == 2) goto L_0x0134;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b6, code lost:
        if (r9 == 3) goto L_0x013e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b9, code lost:
        if (r9 == 4) goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00bb, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bc, code lost:
        r32.Ae3(r3.A02);
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c1, code lost:
        r1 = r31.A09;
        r17 = r32.AFo();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c7, code lost:
        if (r1 == null) goto L_0x026b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c9, code lost:
        r9 = r1.A00;
        r7 = r9.length;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00cd, code lost:
        if (r2 >= r7) goto L_0x026b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00cf, code lost:
        r13 = r9[r2];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d3, code lost:
        if ((r13 instanceof X.C77093mj) == false) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d5, code lost:
        r13 = (X.C77093mj) r13;
        r6 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00d8, code lost:
        if (r6 >= r7) goto L_0x0126;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00da, code lost:
        r5 = r9[r6];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00de, code lost:
        if ((r5 instanceof X.C77033md) == false) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e0, code lost:
        r5 = (X.C77033md) r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ea, code lost:
        if (((X.AbstractC107404xH) r5).A00.equals("TLEN") == false) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ec, code lost:
        r1 = X.C95214dK.A01(java.lang.Long.parseLong(r5.A01));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f6, code lost:
        r12 = r13.A03;
        r11 = r12.length;
        r5 = r11 + 1;
        r10 = new long[r5];
        r9 = new long[r5];
        r10[0] = r17;
        r15 = 0;
        r9[0] = 0;
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0107, code lost:
        if (r7 > r11) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0109, code lost:
        r14 = r7 - 1;
        r17 = r17 + ((long) (r13.A00 + r12[r14]));
        r15 = r15 + ((long) (r13.A01 + r13.A04[r14]));
        r10[r7] = r17;
        r9[r7] = r15;
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0123, code lost:
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0126, code lost:
        r1 = -9223372036854775807L;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x012c, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x012f, code lost:
        r4 = r12.A0E();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0134, code lost:
        r4 = r12.A0F();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0139, code lost:
        r4 = r12.A0C();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x013e, code lost:
        r4 = r12.A0D();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0142, code lost:
        r1 = r1 + ((long) (r4 * r19));
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x014e, code lost:
        if (r6 == -1) goto L_0x016b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0152, code lost:
        if (r6 == r1) goto L_0x016b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0154, code lost:
        r5 = X.C12960it.A0k("VBRI data size mismatch: ");
        r5.append(r6);
        r5.append(", ");
        android.util.Log.w("VbriSeeker", X.C12970iu.A0w(r5, r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x016b, code lost:
        r4 = new X.C107024wf(r4, r15, r20, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x017a, code lost:
        r1 = r32.getLength();
        r18 = r32.AFo();
        r6 = r3.A04;
        r4 = r3.A03;
        r17 = r12.A07();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x018d, code lost:
        if ((r17 & 1) != 1) goto L_0x0249;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018f, code lost:
        r7 = r12.A0E();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0193, code lost:
        if (r7 == 0) goto L_0x0249;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0195, code lost:
        r20 = X.AnonymousClass3JZ.A07((long) r7, ((long) r6) * com.google.android.search.verification.client.SearchActionVerificationClientService.MS_TO_NS, (long) r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01a9, code lost:
        if ((r17 & 6) == 6) goto L_0x01ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01ab, code lost:
        r4 = new X.C107034wg(null, r3.A02, r18, r20, -1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01b9, code lost:
        r7 = r31.A0D;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01be, code lost:
        if (r7.A00 == -1) goto L_0x01db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01c0, code lost:
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01c2, code lost:
        if (r7.A01 == -1) goto L_0x01db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01c4, code lost:
        r32.Ae3(r3.A02);
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01c9, code lost:
        if (r4 == null) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01cb, code lost:
        r1 = X.C12960it.A1W(r4.A05);
        r4 = r4;
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01d1, code lost:
        if (r1 != false) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01d3, code lost:
        if (r5 != 1231971951) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01d5, code lost:
        r4 = A00(r32);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01db, code lost:
        r32.Aaj();
        r32.A5r(r9 + 141);
        r6 = r31.A0G;
        r32.AZ4(r6.A02, 0, 3);
        r6.A0S(0);
        r1 = r6.A0D();
        r2 = r1 >> 12;
        r1 = r1 & 4095;
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01f6, code lost:
        if (r2 > 0) goto L_0x01fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01f8, code lost:
        if (r1 <= 0) goto L_0x01c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01fa, code lost:
        r7.A00 = r2;
        r7.A01 = r1;
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01ff, code lost:
        r29 = r12.A0I();
        r15 = new long[100];
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0208, code lost:
        r15[r4] = (long) r12.A0C();
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0211, code lost:
        if (r4 < 100) goto L_0x0208;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0217, code lost:
        if (r1 == -1) goto L_0x0236;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0219, code lost:
        r6 = r18 + r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x021d, code lost:
        if (r1 == r6) goto L_0x0236;
     */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r32, X.AnonymousClass4IG r33) {
        /*
        // Method dump skipped, instructions count: 876
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106784wH.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        this.A01 = 0;
        this.A02 = -9223372036854775807L;
        this.A04 = 0;
        this.A00 = 0;
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r2) {
        return A02(r2, true);
    }
}
