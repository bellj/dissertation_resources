package X;

import android.os.Handler;

/* renamed from: X.3Rd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67343Rd implements AbstractC009404s {
    public final /* synthetic */ Handler A00;
    public final /* synthetic */ AnonymousClass2IU A01;
    public final /* synthetic */ AbstractC13940ka A02;

    public C67343Rd(Handler handler, AnonymousClass2IU r2, AbstractC13940ka r3) {
        this.A01 = r2;
        this.A00 = handler;
        this.A02 = r3;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AnonymousClass2IU r0 = this.A01;
        Handler handler = this.A00;
        AbstractC13940ka r12 = this.A02;
        C48302Fl r1 = r0.A00;
        AnonymousClass01J r02 = r1.A03;
        C14900mE A0R = C12970iu.A0R(r02);
        AbstractC14440lR A0T = C12960it.A0T(r02);
        C17220qS A0c = C12990iw.A0c(r02);
        AnonymousClass19T r14 = (AnonymousClass19T) r02.A2y.get();
        AnonymousClass018 A0R2 = C12960it.A0R(r02);
        C16240og r10 = (C16240og) r02.ANq.get();
        C19850um r13 = (C19850um) r02.A2v.get();
        C22700zV A0a = C12980iv.A0a(r02);
        C14650lo A0Y = C12980iv.A0Y(r02);
        AnonymousClass2IA r3 = new AnonymousClass2IA(C12960it.A0S(r1.A01.A1E));
        return new C14310lE(AbstractC250617y.A00(r02.AO3), handler, A0R, C12990iw.A0U(r02), (C20670w8) r02.AMw.get(), r10, A0Y, r12, r13, r14, (AnonymousClass19X) r02.AIe.get(), A0a, r3, A0R2, (C22180yf) r02.A5U.get(), A0c, A0T);
    }
}
