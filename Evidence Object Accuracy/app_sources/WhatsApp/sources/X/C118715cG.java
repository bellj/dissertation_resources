package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5cG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118715cG extends AnonymousClass03U {
    public TextView A00;

    public C118715cG(View view) {
        super(view);
        this.A00 = C12960it.A0J(view, R.id.transaction_history_section);
    }
}
