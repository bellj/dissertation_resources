package X;

import android.os.PowerManager;
import java.util.Set;

/* renamed from: X.11P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11P implements AnonymousClass11Q {
    public C35191hP A00;
    public C30421Xi A01;
    public C30421Xi A02;
    public boolean A03;

    public AnonymousClass11P(AnonymousClass11S r2, AnonymousClass11R r3, C14850m9 r4) {
        Set set;
        if (r4.A07(1617)) {
            set = r2.A01;
        } else {
            set = r3.A01;
        }
        set.add(this);
    }

    public synchronized C35191hP A00() {
        return this.A00;
    }

    public C30421Xi A01() {
        C30421Xi r0;
        synchronized (this) {
            r0 = this.A02;
        }
        if (r0 != null) {
            return r0;
        }
        C35191hP A00 = A00();
        if (A00 != null) {
            return A00.A0O;
        }
        C30421Xi A02 = A02();
        if (A02 == null) {
            return null;
        }
        return A02;
    }

    public synchronized C30421Xi A02() {
        return this.A01;
    }

    public void A03() {
        C35191hP r2 = this.A00;
        if (r2 != null) {
            boolean z = true;
            r2.A0Q = true;
            PowerManager.WakeLock wakeLock = r2.A0b;
            if (wakeLock == null || !wakeLock.isHeld()) {
                z = false;
            }
            r2.A0Z = z;
            r2.A08();
        }
    }

    public void A04() {
        C35191hP r1 = this.A00;
        if (r1 != null) {
            r1.A0E(false);
        }
    }

    public void A05() {
        C35191hP r1 = this.A00;
        if (r1 != null) {
            r1.A0Q = false;
            if (r1.A0Z) {
                r1.A07();
            }
        }
    }

    public void A06() {
        C35191hP r2 = this.A00;
        if (r2 != null) {
            r2.A0H(true, false);
        }
    }

    public synchronized void A07() {
        this.A01 = null;
    }

    public synchronized void A08(C35191hP r2) {
        A09(r2, false);
    }

    public synchronized void A09(C35191hP r2, boolean z) {
        this.A03 = z;
        this.A00 = r2;
        if (r2 != null && r2.A0u) {
            this.A01 = r2.A0O;
        }
    }

    public synchronized void A0A(boolean z) {
        this.A03 = z;
    }

    public boolean A0B() {
        C35191hP r0 = this.A00;
        return r0 != null && r0.A0I();
    }

    public synchronized boolean A0C() {
        return this.A03;
    }

    public boolean A0D(AbstractC15340mz r3) {
        C35191hP r0 = this.A00;
        return r0 != null && r3.A0z.equals(r0.A0O.A0z);
    }

    @Override // X.AnonymousClass11Q
    public void ARE(boolean z) {
        if (!z) {
            A04();
        }
    }
}
