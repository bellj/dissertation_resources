package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1PS  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1PS extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1PS A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01;
    public long A02;
    public String A03 = "";

    static {
        AnonymousClass1PS r0 = new AnonymousClass1PS();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        AnonymousClass1PU r0;
        switch (r16.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                AnonymousClass1PS r4 = (AnonymousClass1PS) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A03;
                int i2 = r4.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A03 = r8.Afy(str, r4.A03, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                int i3 = this.A01;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A01 = r8.Afp(i3, r4.A01, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                long j = this.A02;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A02 = r8.Afs(j, r4.A02, z5, z6);
                if (r8 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                String A0A = r82.A0A();
                                this.A00 = 1 | this.A00;
                                this.A03 = A0A;
                            } else if (A03 == 16) {
                                int A02 = r82.A02();
                                if (A02 == 0) {
                                    r0 = AnonymousClass1PU.A01;
                                } else if (A02 != 1) {
                                    r0 = null;
                                } else {
                                    r0 = AnonymousClass1PU.A02;
                                }
                                if (r0 == null) {
                                    super.A0X(2, A02);
                                } else {
                                    this.A00 |= 2;
                                    this.A01 = A02;
                                }
                            } else if (A03 == 24) {
                                this.A00 |= 4;
                                this.A02 = r82.A06();
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass1PS();
            case 5:
                return new AnonymousClass1PT();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (AnonymousClass1PS.class) {
                        if (A05 == null) {
                            A05 = new AnonymousClass255(A04);
                        }
                    }
                }
                return A05;
            default:
                throw new UnsupportedOperationException();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A03);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A02(2, this.A01);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A06(3, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0H(3, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
