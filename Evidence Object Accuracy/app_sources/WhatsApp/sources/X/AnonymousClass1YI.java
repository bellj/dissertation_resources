package X;

import org.chromium.net.UrlRequest;

/* renamed from: X.1YI  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass1YI {
    A0E(0),
    A04(1),
    A07(2),
    A08(3),
    A0B(4),
    A0C(5),
    A06(6),
    A05(7),
    A09(8),
    A02(9),
    A0A(10),
    A01(11),
    A03(12),
    A0D(13);
    
    public final int value;

    AnonymousClass1YI(int i) {
        this.value = i;
    }

    public static AnonymousClass1YI A00(int i) {
        switch (i) {
            case 0:
                return A0E;
            case 1:
                return A04;
            case 2:
                return A07;
            case 3:
                return A08;
            case 4:
                return A0B;
            case 5:
                return A0C;
            case 6:
                return A06;
            case 7:
                return A05;
            case 8:
                return A09;
            case 9:
                return A02;
            case 10:
                return A0A;
            case 11:
                return A01;
            case 12:
                return A03;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return A0D;
            default:
                return null;
        }
    }
}
