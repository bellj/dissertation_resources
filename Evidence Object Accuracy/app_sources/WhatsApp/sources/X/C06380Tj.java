package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/* renamed from: X.0Tj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06380Tj {
    public static C06380Tj A05;
    public static final Object A06 = new Object();
    public final Context A00;
    public final Handler A01;
    public final ArrayList A02 = new ArrayList();
    public final HashMap A03 = new HashMap();
    public final HashMap A04 = new HashMap();

    public C06380Tj(Context context) {
        this.A00 = context;
        this.A01 = new AnonymousClass0AR(context.getMainLooper(), this);
    }

    public static C06380Tj A00(Context context) {
        C06380Tj r1;
        synchronized (A06) {
            r1 = A05;
            if (r1 == null) {
                r1 = new C06380Tj(context.getApplicationContext());
                A05 = r1;
            }
        }
        return r1;
    }

    public void A01(BroadcastReceiver broadcastReceiver) {
        HashMap hashMap = this.A04;
        synchronized (hashMap) {
            ArrayList arrayList = (ArrayList) hashMap.remove(broadcastReceiver);
            if (arrayList != null) {
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    AnonymousClass0OZ r7 = (AnonymousClass0OZ) arrayList.get(size);
                    r7.A01 = true;
                    for (int i = 0; i < r7.A03.countActions(); i++) {
                        String action = r7.A03.getAction(i);
                        HashMap hashMap2 = this.A03;
                        ArrayList arrayList2 = (ArrayList) hashMap2.get(action);
                        if (arrayList2 != null) {
                            for (int size2 = arrayList2.size() - 1; size2 >= 0; size2--) {
                                AnonymousClass0OZ r1 = (AnonymousClass0OZ) arrayList2.get(size2);
                                if (r1.A02 == broadcastReceiver) {
                                    r1.A01 = true;
                                    arrayList2.remove(size2);
                                }
                            }
                            if (arrayList2.size() <= 0) {
                                hashMap2.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }

    public void A02(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        HashMap hashMap = this.A04;
        synchronized (hashMap) {
            AnonymousClass0OZ r5 = new AnonymousClass0OZ(broadcastReceiver, intentFilter);
            ArrayList arrayList = (ArrayList) hashMap.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList(1);
                hashMap.put(broadcastReceiver, arrayList);
            }
            arrayList.add(r5);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                HashMap hashMap2 = this.A03;
                ArrayList arrayList2 = (ArrayList) hashMap2.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(1);
                    hashMap2.put(action, arrayList2);
                }
                arrayList2.add(r5);
            }
        }
    }

    public void A03(Intent intent) {
        synchronized (this.A04) {
            String action = intent.getAction();
            String resolveTypeIfNeeded = intent.resolveTypeIfNeeded(this.A00.getContentResolver());
            Uri data = intent.getData();
            String scheme = intent.getScheme();
            Set<String> categories = intent.getCategories();
            boolean z = false;
            if ((intent.getFlags() & 8) != 0) {
                z = true;
                StringBuilder sb = new StringBuilder();
                sb.append("Resolving type ");
                sb.append(resolveTypeIfNeeded);
                sb.append(" scheme ");
                sb.append(scheme);
                sb.append(" of intent ");
                sb.append(intent);
                Log.v("LocalBroadcastManager", sb.toString());
            }
            ArrayList arrayList = (ArrayList) this.A03.get(intent.getAction());
            if (arrayList != null) {
                if (z) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Action list: ");
                    sb2.append(arrayList);
                    Log.v("LocalBroadcastManager", sb2.toString());
                }
                ArrayList arrayList2 = null;
                for (int i = 0; i < arrayList.size(); i++) {
                    AnonymousClass0OZ r4 = (AnonymousClass0OZ) arrayList.get(i);
                    if (z) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Matching against filter ");
                        sb3.append(r4.A03);
                        Log.v("LocalBroadcastManager", sb3.toString());
                    }
                    if (!r4.A00) {
                        int match = r4.A03.match(action, resolveTypeIfNeeded, scheme, data, categories, "LocalBroadcastManager");
                        if (match >= 0) {
                            if (z) {
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("  Filter matched!  match=0x");
                                sb4.append(Integer.toHexString(match));
                                Log.v("LocalBroadcastManager", sb4.toString());
                            }
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                            }
                            arrayList2.add(r4);
                            r4.A00 = true;
                        } else if (z) {
                            String str = match != -4 ? match != -3 ? match != -2 ? match != -1 ? "unknown reason" : "type" : "data" : "action" : "category";
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append("  Filter did not match: ");
                            sb5.append(str);
                            Log.v("LocalBroadcastManager", sb5.toString());
                        }
                    } else if (z) {
                        Log.v("LocalBroadcastManager", "  Filter's target already added");
                    }
                }
                if (arrayList2 != null) {
                    for (int i2 = 0; i2 < arrayList2.size(); i2++) {
                        ((AnonymousClass0OZ) arrayList2.get(i2)).A00 = false;
                    }
                    this.A02.add(new C04760My(intent, arrayList2));
                    Handler handler = this.A01;
                    if (!handler.hasMessages(1)) {
                        handler.sendEmptyMessage(1);
                    }
                }
            }
        }
    }
}
