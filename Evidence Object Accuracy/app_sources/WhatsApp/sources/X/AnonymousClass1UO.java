package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/* renamed from: X.1UO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UO extends Handler {
    public final /* synthetic */ C14860mA A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1UO(Looper looper, C14860mA r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            removeMessages(1);
            if (!hasMessages(2)) {
                try {
                    C14860mA r4 = this.A00;
                    synchronized (r4.A06(true)) {
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(r4.A0I.A00.getCacheDir(), "WebActionIdCache"));
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                        try {
                            objectOutputStream.writeObject(r4.A06(true));
                            objectOutputStream.close();
                            fileOutputStream.close();
                        } catch (Throwable th) {
                            try {
                                objectOutputStream.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    }
                    AnonymousClass1UP A00 = r4.A0N.A00();
                    A00.A08.edit().putString("epoch", A00.A01).commit();
                } catch (Exception e) {
                    Log.e("qrsession/persistActionCache/fail", e);
                }
            }
        } else if (i == 2) {
            removeMessages(1);
            removeMessages(2);
            C14860mA r42 = this.A00;
            synchronized (r42.A06(true)) {
                new File(r42.A0I.A00.getCacheDir(), "WebActionIdCache").delete();
            }
            r42.A0N.A00().A00("epoch");
        }
    }
}
