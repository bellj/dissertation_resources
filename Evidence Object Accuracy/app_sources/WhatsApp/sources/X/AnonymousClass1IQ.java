package X;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1IQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IQ extends AbstractC16280ok {
    public final C231410n A00;
    public final AnonymousClass1I2 A01;

    public AnonymousClass1IQ(Context context, AbstractC15710nm r11, C231410n r12, Set set) {
        super(context, r11, "payments.db", null, 3, true);
        this.A00 = r12;
        this.A01 = new AnonymousClass1I2(new C002601e(set, null));
    }

    public static String A00(SQLiteDatabase sQLiteDatabase, String str) {
        String str2 = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select sql from sqlite_master where type='table' and name='");
            sb.append(str);
            sb.append("';");
            Cursor rawQuery = sQLiteDatabase.rawQuery(sb.toString(), null);
            if (rawQuery != null && rawQuery.moveToNext()) {
                str2 = rawQuery.getString(0);
            }
            if (rawQuery != null) {
                rawQuery.close();
                return str2;
            }
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("payments-store/schema ");
            sb2.append(str);
            Log.e(sb2.toString(), e);
        }
        return str2;
    }

    public static void A01(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder();
        sb.append(str3);
        sb.append(" ");
        sb.append(str4);
        if (!str.contains(sb.toString())) {
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("ALTER TABLE ");
                sb2.append(str2);
                sb2.append(" ADD ");
                sb2.append(str3);
                sb2.append(" ");
                sb2.append(str4);
                sQLiteDatabase.execSQL(sb2.toString());
            } catch (SQLiteException e) {
                StringBuilder sb3 = new StringBuilder("payments-store/add-column ");
                sb3.append(str3);
                Log.e(sb3.toString(), e);
            }
        }
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A00);
        } catch (SQLiteException e) {
            Log.e("failed to open payment store", e);
            new SQLiteCantOpenDatabaseException();
            Iterator it = this.A01.iterator();
            while (it.hasNext()) {
                ((C38061nS) it.next()).A00.A0F();
            }
            return AnonymousClass1Tx.A01(super.A00(), this.A00);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Log.i("PAY: creating payments database version 3");
        sQLiteDatabase.execSQL("CREATE TABLE methods (credential_id TEXT NOT NULL PRIMARY KEY, country TEXT, readable_name TEXT, issuer_name TEXT, type INTEGER NOT NULL, subtype INTEGER, creation_ts INTEGER, updated_ts INTEGER, debit_mode INTEGER NOT NULL, credit_mode INTEGER NOT NULL, balance_1000 INTEGER, balance_ts INTEGER, country_data TEXT, icon BLOB, p2m_debit_mode INTEGER NOT NULL, p2m_credit_mode INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS payment_methods_index ON methods (credential_id)");
        sQLiteDatabase.execSQL("CREATE TABLE tmp_transactions (tmp_id TEXT NOT NULL, tmp_metadata TEXT, tmp_ts INTEGER)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS message_payment_transactions_index ON tmp_transactions (tmp_id)");
        sQLiteDatabase.execSQL("CREATE TABLE contacts (jid NOT NULL, country_data TEXT, merchant INTEGER, consumer_status INTEGER, default_payment_type INTEGER)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS payment_constacts_index ON contacts (jid)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("PaymentDbHelper/onDowngrade/oldVersion:");
        sb.append(i);
        sb.append(", newVersion:");
        sb.append(i2);
        Log.i(sb.toString());
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS methods");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS tmp_transactions");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(sQLiteDatabase);
    }

    @Override // X.AbstractC16280ok, android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        String A00 = A00(sQLiteDatabase, "methods");
        if (A00 != null) {
            A01(sQLiteDatabase, A00, "methods", "icon", "BLOB");
        }
        String A002 = A00(sQLiteDatabase, "contacts");
        if (A002 != null) {
            A01(sQLiteDatabase, A002, "contacts", "merchant", "INTEGER");
            A01(sQLiteDatabase, A002, "contacts", "default_payment_type", "INTEGER");
            A01(sQLiteDatabase, A002, "contacts", "consumer_status", "INTEGER");
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("PaymentDbHelper/onUpgrade/old version: ");
        sb.append(i);
        sb.append(", new version: ");
        sb.append(i2);
        Log.i(sb.toString());
        if (i == 1 || i == 2) {
            sQLiteDatabase.execSQL("ALTER TABLE methods ADD p2m_debit_mode  INTEGER NOT NULL DEFAULT 0");
            sQLiteDatabase.execSQL("ALTER TABLE methods ADD p2m_credit_mode  INTEGER NOT NULL DEFAULT 0");
            return;
        }
        StringBuilder sb2 = new StringBuilder("PaymentDbHelper/onUpgrade/Unknown upgrade from ");
        sb2.append(i);
        sb2.append(" to ");
        sb2.append(i2);
        throw new SQLiteException(sb2.toString());
    }
}
