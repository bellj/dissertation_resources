package X;

/* renamed from: X.3t7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80883t7 extends AnonymousClass5DH {
    public final /* synthetic */ AbstractC80943tD this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80883t7(AbstractC80943tD r1) {
        super(r1);
        this.this$0 = r1;
    }

    @Override // X.AnonymousClass5DH
    public AnonymousClass4Y5 result(int i) {
        return this.this$0.backingMap.getEntry(i);
    }
}
