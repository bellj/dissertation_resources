package X;

import android.os.Bundle;
import android.view.View;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.04J  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04J {
    public final AnonymousClass01F A00;
    public final CopyOnWriteArrayList A01 = new CopyOnWriteArrayList();

    public AnonymousClass04J(AnonymousClass01F r2) {
        this.A00 = r2;
    }

    public void A00(Bundle bundle, View view, AnonymousClass01E r5) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A00(bundle, view, r5);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A01(Bundle bundle, AnonymousClass01E r4) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A01(bundle, r4);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A02(Bundle bundle, AnonymousClass01E r4) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A02(bundle, r4);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A03(Bundle bundle, AnonymousClass01E r4) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A03(bundle, r4);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A04(Bundle bundle, AnonymousClass01E r4) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A04(bundle, r4);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A05(AnonymousClass01E r3) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A05(r3);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A06(AnonymousClass01E r3) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A06(r3);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A07(AnonymousClass01E r3) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A07(r3);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A08(AnonymousClass01E r3) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A08(r3);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A09(AnonymousClass01E r3) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A09(r3);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A0A(AnonymousClass01E r3) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A0A(r3);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A0B(AnonymousClass01E r3) {
        AnonymousClass01E r0 = this.A00.A05;
        if (r0 != null) {
            r0.A0F().A0T.A0B(r3);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A0C(AnonymousClass01E r5, boolean z) {
        AnonymousClass01F r3 = this.A00;
        AnonymousClass01E r0 = r3.A05;
        if (r0 != null) {
            r0.A0F().A0T.A0C(r5, true);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            AnonymousClass04K r1 = (AnonymousClass04K) it.next();
            if (!z || r1.A01) {
                r1.A00.A00(r5, r3);
            }
        }
    }

    public void A0D(AnonymousClass01E r5, boolean z) {
        AnonymousClass01F r3 = this.A00;
        AnonymousClass01E r0 = r3.A05;
        if (r0 != null) {
            r0.A0F().A0T.A0D(r5, true);
        }
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            AnonymousClass04K r1 = (AnonymousClass04K) it.next();
            if (!z || r1.A01) {
                r1.A00.A01(r5, r3);
            }
        }
    }
}
