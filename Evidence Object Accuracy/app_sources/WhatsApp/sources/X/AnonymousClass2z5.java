package X;

import android.content.Intent;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;

/* renamed from: X.2z5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2z5 extends AnonymousClass1MD {
    public final /* synthetic */ MediaAlbumActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2z5(AnonymousClass12P r44, AbstractC15710nm r45, C14900mE r46, C15570nT r47, C15450nH r48, C16170oZ r49, ActivityC13790kL r50, AnonymousClass12T r51, C18850tA r52, C15550nR r53, C22700zV r54, C15610nY r55, MediaAlbumActivity mediaAlbumActivity, AnonymousClass1A6 r57, C255419u r58, AnonymousClass01d r59, C14830m7 r60, C14820m6 r61, AnonymousClass018 r62, C15600nX r63, C253218y r64, C242114q r65, C22100yW r66, C22180yf r67, AnonymousClass19M r68, C231510o r69, AnonymousClass193 r70, C14850m9 r71, C16120oU r72, C20710wC r73, AnonymousClass109 r74, C22370yy r75, AnonymousClass13H r76, C16630pM r77, C88054Ec r78, AnonymousClass12F r79, C253018w r80, AnonymousClass12U r81, C23000zz r82, C252718t r83, AbstractC14440lR r84, AnonymousClass01H r85) {
        super(r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72, r73, r74, r75, r76, r77, r78, r79, r80, r81, r82, r83, r84, r85);
        this.A00 = mediaAlbumActivity;
    }

    @Override // X.AnonymousClass1MD, X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r10) {
        MediaAlbumActivity mediaAlbumActivity = this.A00;
        C35451ht r0 = ((AbstractActivityC13750kH) mediaAlbumActivity).A0J;
        if (!(r0 == null || r0.A04.size() == 0)) {
            if (menuItem.getItemId() == R.id.menuitem_reply) {
                AbstractC15340mz A01 = A01();
                AnonymousClass1IS r5 = A01.A0z;
                AbstractC14640lm r4 = r5.A00;
                AnonymousClass009.A05(r4);
                C15370n3 A0B = ((AbstractActivityC13750kH) mediaAlbumActivity).A07.A0B(r4);
                if ((!A0B.A0K() || ((AbstractActivityC13750kH) mediaAlbumActivity).A0N.A0C((GroupJid) r4)) && !mediaAlbumActivity.A0V.A0Z(A0B, r4)) {
                    Intent A0A = C12970iu.A0A();
                    A0A.setClassName(mediaAlbumActivity.getPackageName(), "com.whatsapp.status.playback.MessageReplyActivity");
                    mediaAlbumActivity.startActivity(C38211ni.A00(A0A, r5));
                } else {
                    mediaAlbumActivity.A2p(A01);
                }
            } else if (menuItem.getItemId() != R.id.menuitem_reply_privately) {
                return super.ALr(menuItem, r10);
            } else {
                mediaAlbumActivity.A2p(A01());
            }
            A03();
        }
        return true;
    }

    @Override // X.AnonymousClass1MD, X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        Log.i("starred/selectionended");
        super.AP3(r4);
        MediaAlbumActivity mediaAlbumActivity = this.A00;
        C35451ht r0 = ((AbstractActivityC13750kH) mediaAlbumActivity).A0J;
        if (r0 != null) {
            r0.A00();
            ((AbstractActivityC13750kH) mediaAlbumActivity).A0J = null;
        }
        mediaAlbumActivity.A08.notifyDataSetChanged();
        ((AbstractActivityC13750kH) mediaAlbumActivity).A01 = null;
        mediaAlbumActivity.A2j();
    }
}
