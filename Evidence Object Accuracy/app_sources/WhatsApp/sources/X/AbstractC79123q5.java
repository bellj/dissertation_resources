package X;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import org.chromium.net.UrlRequest;

/* renamed from: X.3q5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC79123q5 extends AbstractC108624zO {
    public static Map zzjr = new ConcurrentHashMap();
    public C94984cr zzjp = C94984cr.A05;
    public int zzjq = -1;

    public static Object A00(Object obj, Method method, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw cause;
            } else if (cause instanceof Error) {
                throw cause;
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @Override // X.AbstractC115625Sh
    public final /* synthetic */ AbstractC117125Yn AhB() {
        return (AbstractC108624zO) A05(6);
    }

    public static String A01(AbstractC111915Bh r5) {
        String str;
        AbstractC111915Bh r3 = new AnonymousClass4IU(r5).A00;
        StringBuilder A0t = C12980iv.A0t(r3.A02());
        for (int i = 0; i < r3.A02(); i++) {
            int A01 = r3.A01(i);
            if (A01 == 34) {
                str = "\\\"";
            } else if (A01 == 39) {
                str = "\\'";
            } else if (A01 != 92) {
                switch (A01) {
                    case 7:
                        str = "\\a";
                        break;
                    case 8:
                        str = "\\b";
                        break;
                    case 9:
                        str = "\\t";
                        break;
                    case 10:
                        str = "\\n";
                        break;
                    case 11:
                        str = "\\v";
                        break;
                    case 12:
                        str = "\\f";
                        break;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        str = "\\r";
                        break;
                    default:
                        if (A01 < 32 || A01 > 126) {
                            A0t.append('\\');
                            A0t.append((char) (((A01 >>> 6) & 3) + 48));
                            A0t.append((char) (((A01 >>> 3) & 7) + 48));
                            A01 = (A01 & 7) + 48;
                        }
                        A0t.append((char) A01);
                        continue;
                        break;
                }
            } else {
                str = "\\\\";
            }
            A0t.append(str);
        }
        return A0t.toString();
    }

    public static final String A02(String str) {
        StringBuilder A0h = C12960it.A0h();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                A0h.append("_");
            }
            A0h.append(Character.toLowerCase(charAt));
        }
        return A0h.toString();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:96:0x0163 */
    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r0v23, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r0v26, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r0v28, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r7v2, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r7v3, types: [X.5Bh] */
    public static void A03(AbstractC117125Yn r13, StringBuilder sb, int i) {
        int i2;
        String A0r;
        Method method;
        HashMap A11 = C12970iu.A11();
        HashMap A112 = C12970iu.A11();
        TreeSet treeSet = new TreeSet();
        for (Method method2 : r13.getClass().getDeclaredMethods()) {
            C72453ed.A1P(method2, treeSet, A112, A11);
        }
        Iterator it = treeSet.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            Object obj = "";
            String replaceFirst = A0x.replaceFirst("get", obj);
            if (replaceFirst.endsWith("List") && !replaceFirst.endsWith("OrBuilderList") && !replaceFirst.equals("List")) {
                A0r = C72453ed.A0r(C72453ed.A0q(replaceFirst), replaceFirst.substring(1, replaceFirst.length() - 4));
                method = (Method) A11.get(A0x);
                if (method != null && method.getReturnType().equals(List.class)) {
                    A04(A00(r13, method, new Object[0]), A02(A0r), sb, i);
                }
            }
            if (replaceFirst.endsWith("Map") && !replaceFirst.equals("Map")) {
                A0r = C72453ed.A0r(C72453ed.A0q(replaceFirst), replaceFirst.substring(1, replaceFirst.length() - 3));
                method = (Method) A11.get(A0x);
                if (method != null && method.getReturnType().equals(Map.class) && !method.isAnnotationPresent(Deprecated.class) && Modifier.isPublic(method.getModifiers())) {
                    A04(A00(r13, method, new Object[0]), A02(A0r), sb, i);
                }
            }
            int length = replaceFirst.length();
            if (A112.get(C72453ed.A0s("set", replaceFirst, length)) != null && (!replaceFirst.endsWith("Bytes") || !A11.containsKey(C72453ed.A0r("get", replaceFirst.substring(0, length - 5))))) {
                String A0r2 = C72453ed.A0r(C72453ed.A0q(replaceFirst), replaceFirst.substring(1));
                Method method3 = (Method) A11.get(C72453ed.A0s("get", replaceFirst, length));
                Method method4 = (Method) A11.get(C72453ed.A0s("has", replaceFirst, length));
                if (method3 != null) {
                    Object A00 = A00(r13, method3, new Object[0]);
                    if (method4 == null) {
                        if (A00 instanceof Boolean) {
                            i2 = C12970iu.A1Y(A00);
                        } else if (A00 instanceof Integer) {
                            i2 = C12960it.A05(A00);
                        } else if (A00 instanceof Float) {
                            i2 = (C72453ed.A02(A00) > 0.0f ? 1 : (C72453ed.A02(A00) == 0.0f ? 0 : -1));
                        } else if (A00 instanceof Double) {
                            i2 = (C72453ed.A00(A00) > 0.0d ? 1 : (C72453ed.A00(A00) == 0.0d ? 0 : -1));
                        } else {
                            if (!(A00 instanceof String)) {
                                if (A00 instanceof AbstractC111915Bh) {
                                    obj = AbstractC111915Bh.A00;
                                } else {
                                    if (A00 instanceof AbstractC117125Yn) {
                                        if (A00 == ((AbstractC115625Sh) A00).AhB()) {
                                        }
                                    } else if (A00 instanceof Enum) {
                                        i2 = ((Enum) A00).ordinal();
                                    }
                                    A04(A00, A02(A0r2), sb, i);
                                }
                            }
                            if (!A00.equals(obj)) {
                                A04(A00, A02(A0r2), sb, i);
                            }
                        }
                        if (i2) {
                            A04(A00, A02(A0r2), sb, i);
                        }
                    } else if (C12970iu.A1Y(A00(r13, method4, new Object[0]))) {
                        A04(A00, A02(A0r2), sb, i);
                    }
                }
            }
        }
        C94984cr r3 = ((AbstractC79123q5) r13).zzjp;
        if (r3 != null) {
            for (int i3 = 0; i3 < r3.A00; i3++) {
                A04(r3.A04[i3], String.valueOf(r3.A03[i3] >>> 3), sb, i);
            }
        }
    }

    public static final void A04(Object obj, String str, StringBuilder sb, int i) {
        String A01;
        if (obj instanceof List) {
            for (Object obj2 : (List) obj) {
                A04(obj2, str, sb, i);
            }
        } else if (obj instanceof Map) {
            Iterator A0n = C12960it.A0n((Map) obj);
            while (A0n.hasNext()) {
                A04(A0n.next(), str, sb, i);
            }
        } else {
            sb.append('\n');
            int i2 = 0;
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append(str);
            if (obj instanceof String) {
                sb.append(": \"");
                A01 = A01(new C79243qH(((String) obj).getBytes(C93094Zb.A03)));
            } else if (obj instanceof AbstractC111915Bh) {
                sb.append(": \"");
                A01 = A01((AbstractC111915Bh) obj);
            } else {
                if (obj instanceof AbstractC79123q5) {
                    sb.append(" {");
                    A03((AbstractC108624zO) obj, sb, i + 2);
                    sb.append("\n");
                    while (i2 < i) {
                        sb.append(' ');
                        i2++;
                    }
                } else if (obj instanceof Map.Entry) {
                    sb.append(" {");
                    Map.Entry entry = (Map.Entry) obj;
                    int i4 = i + 2;
                    A04(entry.getKey(), "key", sb, i4);
                    A04(entry.getValue(), "value", sb, i4);
                    sb.append("\n");
                    while (i2 < i) {
                        sb.append(' ');
                        i2++;
                    }
                } else {
                    sb.append(": ");
                    C12970iu.A1V(obj, sb);
                    return;
                }
                sb.append("}");
                return;
            }
            sb.append(A01);
            sb.append('\"');
        }
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [X.5Ql, java.lang.Object, X.4Zm] */
    /* JADX WARN: Type inference failed for: r3v8, types: [X.5Ql, java.lang.Object, X.4Zm] */
    /* JADX WARN: Type inference failed for: r3v18, types: [X.5Ql, java.lang.Object, X.4Zm] */
    /* JADX WARN: Type inference failed for: r3v21, types: [X.5Ql, java.lang.Object, X.4Zm] */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A05(int r6) {
        /*
        // Method dump skipped, instructions count: 400
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC79123q5.A05(int):java.lang.Object");
    }

    @Override // X.AbstractC117125Yn
    public final int Ah0() {
        int i = this.zzjq;
        if (i != -1) {
            return i;
        }
        int AhO = C72453ed.A0d(this).AhO(this);
        this.zzjq = AhO;
        return AhO;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!A05(6).getClass().isInstance(obj)) {
            return false;
        }
        return C72453ed.A0d(this).A9Z(this, obj);
    }

    public int hashCode() {
        int i = this.zzex;
        if (i != 0) {
            return i;
        }
        int AIO = C72453ed.A0d(this).AIO(this);
        this.zzex = AIO;
        return AIO;
    }

    public String toString() {
        String obj = super.toString();
        StringBuilder A0k = C12960it.A0k("# ");
        A0k.append(obj);
        A03(this, A0k, 0);
        return A0k.toString();
    }
}
