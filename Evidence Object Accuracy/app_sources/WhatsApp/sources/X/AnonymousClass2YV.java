package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Handler;

/* renamed from: X.2YV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YV extends AnimatorListenerAdapter {
    public final /* synthetic */ long A00;
    public final /* synthetic */ AnonymousClass1IO A01;
    public final /* synthetic */ AnonymousClass3BU A02;

    public AnonymousClass2YV(AnonymousClass1IO r1, AnonymousClass3BU r2, long j) {
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = j;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.A02.A0D.remove(Long.valueOf(this.A00));
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        AnonymousClass1IO r0 = this.A01;
        Handler handler = r0.A00;
        if (handler == null) {
            handler = C12970iu.A0E();
            r0.A00 = handler;
        }
        handler.post(r0.A01);
    }
}
