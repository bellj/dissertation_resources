package X;

import android.net.Uri;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

/* renamed from: X.3Ua  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68083Ua implements AbstractC33221de {
    public final Uri A00;
    public final C16590pI A01;

    @Override // X.AbstractC33221de
    public boolean A9l() {
        return true;
    }

    public C68083Ua(Uri uri, C16590pI r2) {
        this.A01 = r2;
        this.A00 = uri;
    }

    @Override // X.AbstractC33221de
    public boolean A8j() {
        throw C12980iv.A0u("BackupStreamResource/does not support delete");
    }

    @Override // X.AbstractC33221de
    public C32871cs ACp(C17050qB r2) {
        throw C12980iv.A0u("BackupStreamResource/does not support getExternalAtomicFileStream");
    }

    @Override // X.AbstractC33221de
    public FileInputStream AD0() {
        throw C12980iv.A0u("BackupStreamResource/does not support getFileInputStream");
    }

    @Override // X.AbstractC33221de
    public String ADG(MessageDigest messageDigest, long j) {
        InputStream ADW = ADW();
        try {
            messageDigest.reset();
            byte[] bArr = new byte[4096];
            BufferedInputStream bufferedInputStream = new BufferedInputStream(ADW);
            long j2 = 0;
            int i = 0;
            while (true) {
                if (i == -1) {
                    break;
                }
                i = bufferedInputStream.read(bArr, 0, 4096);
                if (i > 0) {
                    long j3 = ((long) i) + j2;
                    if (j3 >= j) {
                        messageDigest.update(bArr, 0, (int) (j - j2));
                        break;
                    }
                    messageDigest.update(bArr, 0, i);
                    j2 = j3;
                }
            }
            String A04 = C003501n.A04(messageDigest.digest());
            bufferedInputStream.close();
            ADW.close();
            return A04;
        } catch (Throwable th) {
            try {
                ADW.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC33221de
    public InputStream ADW() {
        InputStream openInputStream = this.A01.A00.getContentResolver().openInputStream(this.A00);
        if (openInputStream != null) {
            return openInputStream;
        }
        throw C12990iw.A0i("BackupStreamResource/cannot get input stream");
    }

    @Override // X.AbstractC33221de
    public OutputStream AEq() {
        OutputStream openOutputStream = this.A01.A00.getContentResolver().openOutputStream(this.A00);
        if (openOutputStream != null) {
            return openOutputStream;
        }
        throw C12990iw.A0i("BackupStreamResource/cannot get output stream");
    }

    @Override // X.AbstractC33221de
    public long AKN() {
        throw C12970iu.A0z();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        if (r2 != null) goto L_0x002e;
     */
    @Override // X.AbstractC33221de
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AKR() {
        /*
            r6 = this;
            X.0pI r0 = r6.A01
            android.content.Context r0 = r0.A00
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r1 = r6.A00
            r2 = 0
            r4 = r2
            r5 = r2
            r3 = r2
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)
            if (r2 == 0) goto L_0x0025
            java.lang.String r0 = "_size"
            int r1 = r2.getColumnIndex(r0)     // Catch: all -> 0x0032
            boolean r0 = r2.moveToFirst()     // Catch: all -> 0x0032
            if (r0 == 0) goto L_0x002a
            long r0 = r2.getLong(r1)     // Catch: all -> 0x0032
            goto L_0x002e
        L_0x0025:
            java.lang.String r0 = "BackupStreamResource/uri-length/fail"
            com.whatsapp.util.Log.e(r0)     // Catch: all -> 0x0032
        L_0x002a:
            r0 = -1
            if (r2 == 0) goto L_0x0031
        L_0x002e:
            r2.close()
        L_0x0031:
            return r0
        L_0x0032:
            r0 = move-exception
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch: all -> 0x0038
        L_0x0038:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C68083Ua.AKR():long");
    }
}
