package X;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow;

/* renamed from: X.5nf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123685nf extends PeerPaymentTransactionRow {
    public WaTextView A00;
    public C14830m7 A01;
    public AbstractC130195yx A02;
    public boolean A03;

    public C123685nf(Context context) {
        super(context);
        A01();
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public void A02() {
        super.A02();
        this.A00 = C12960it.A0N(this, R.id.fiat_transaction_amount);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        if ((r2 instanceof X.C123755no) == false) goto L_0x0012;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001b  */
    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
            r3 = this;
            X.5yx r2 = r3.A02
            android.content.Context r1 = r3.getContext()
            boolean r0 = r2 instanceof X.C123765np
            if (r0 != 0) goto L_0x0012
            boolean r0 = r2 instanceof X.C123745nn
            if (r0 != 0) goto L_0x0021
            boolean r0 = r2 instanceof X.C123755no
            if (r0 != 0) goto L_0x0025
        L_0x0012:
            r0 = 2131232185(0x7f0805b9, float:1.8080472E38)
        L_0x0015:
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r1, r0)
            if (r1 == 0) goto L_0x0025
            android.widget.ImageView r0 = r3.A06
            r0.setImageDrawable(r1)
            return
        L_0x0021:
            r0 = 2131232515(0x7f080703, float:1.8081141E38)
            goto L_0x0015
        L_0x0025:
            super.A03()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123685nf.A03():void");
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public void A04(AnonymousClass1IR r8) {
        super.A6W(r8);
        AbstractC130195yx r6 = this.A02;
        AnonymousClass009.A05(r6);
        boolean A07 = r6.A07();
        WaTextView waTextView = this.A00;
        if (A07) {
            AnonymousClass6F2 A02 = r6.A02();
            AnonymousClass009.A05(A02);
            waTextView.setText(A02.A06(r6.A06));
            if (this.A00.getVisibility() == 8) {
                this.A00.setVisibility(0);
                ((RelativeLayout.LayoutParams) findViewById(R.id.requested_from_note_container).getLayoutParams()).addRule(0, this.A00.getId());
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.A0A.getLayoutParams();
                layoutParams.addRule(6, this.A0C.getId());
                layoutParams.addRule(8, this.A0C.getId());
                layoutParams.addRule(3, this.A00.getId());
                ((RelativeLayout.LayoutParams) this.A0C.getLayoutParams()).addRule(0, this.A0A.getId());
            }
            boolean z = this.A02.A03;
            WaTextView waTextView2 = this.A00;
            if (z) {
                waTextView2.setPaintFlags(waTextView2.getPaintFlags() | 16);
            } else {
                C92984Yl.A01(waTextView2);
            }
        } else {
            waTextView.setVisibility(8);
            View findViewById = findViewById(R.id.requested_from_note_container);
            ((RelativeLayout.LayoutParams) findViewById.getLayoutParams()).addRule(0, this.A0A.getId());
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.A0A.getLayoutParams();
            layoutParams2.addRule(6, findViewById.getId());
            layoutParams2.addRule(8, findViewById.getId());
            ((RelativeLayout.LayoutParams) this.A0C.getLayoutParams()).addRule(0, 0);
        }
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public CharSequence getAmountText() {
        return this.A02.A03();
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public int getLayoutResourceId() {
        return R.layout.novi_payment_row_settings;
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public int getStatusColor() {
        return AnonymousClass00T.A00(getContext(), this.A02.A01());
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public String getStatusLabel() {
        return this.A02.A04();
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public String getTransactionTitle() {
        return this.A02.A05();
    }

    @Override // com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow
    public void setupRowButtons(AbstractC15340mz r4, AbstractC38231nk r5) {
        int i = 8;
        ((PeerPaymentTransactionRow) this).A01.setVisibility(8);
        if (this.A0Q.A03 == 8) {
            boolean A0A = ((C123765np) this.A02).A0A();
            View view = ((PeerPaymentTransactionRow) this).A01;
            if (A0A) {
                C117295Zj.A0n(AnonymousClass028.A0D(view, R.id.view_code_button), this, 186);
                view = ((PeerPaymentTransactionRow) this).A01;
                i = 0;
            }
            view.setVisibility(i);
        }
    }
}
