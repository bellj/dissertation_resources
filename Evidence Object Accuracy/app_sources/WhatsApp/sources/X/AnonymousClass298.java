package X;

/* renamed from: X.298  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass298<E> extends AnonymousClass1Mr<E> {
    public static final AnonymousClass1Mr EMPTY = new AnonymousClass298(new Object[0], 0);
    public final transient Object[] array;
    public final transient int size;

    @Override // X.AbstractC17950rf
    public int internalArrayStart() {
        return 0;
    }

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return false;
    }

    public AnonymousClass298(Object[] objArr, int i) {
        this.array = objArr;
        this.size = i;
    }

    @Override // X.AnonymousClass1Mr, X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        System.arraycopy(this.array, 0, objArr, i, this.size);
        return i + this.size;
    }

    @Override // java.util.List
    public Object get(int i) {
        C28291Mn.A01(i, this.size);
        return this.array[i];
    }

    @Override // X.AbstractC17950rf
    public Object[] internalArray() {
        return this.array;
    }

    @Override // X.AbstractC17950rf
    public int internalArrayEnd() {
        return this.size;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.size;
    }
}
