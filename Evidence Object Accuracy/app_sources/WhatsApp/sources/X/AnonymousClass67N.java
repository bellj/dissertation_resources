package X;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.ui.BkActionBottomSheet;
import com.whatsapp.wabloks.ui.WaBloksActivity;
import com.whatsapp.wabloks.ui.bottomsheet.BkBottomSheetContainerFragment;
import com.whatsapp.wabloks.ui.bottomsheet.BkBottomSheetContentFragment;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.67N  reason: invalid class name */
/* loaded from: classes4.dex */
public abstract class AnonymousClass67N implements AnonymousClass1AH {
    public static final Set A0O;
    public final AnonymousClass01H A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;
    public final AnonymousClass01H A03;
    public final AnonymousClass01H A04;
    public final AnonymousClass01H A05;
    public final AnonymousClass01H A06;
    public final AnonymousClass01H A07;
    public final AnonymousClass01H A08;
    public final AnonymousClass01H A09;
    public final AnonymousClass01H A0A;
    public final AnonymousClass01H A0B;
    public final AnonymousClass01H A0C;
    public final AnonymousClass01H A0D;
    public final AnonymousClass01H A0E;
    public final AnonymousClass01H A0F;
    public final AnonymousClass01H A0G;
    public final AnonymousClass01H A0H;
    public final AnonymousClass01H A0I;
    public final AnonymousClass01H A0J;
    public final AnonymousClass01H A0K;
    public final AnonymousClass01H A0L;
    public final AnonymousClass01H A0M;
    public final AnonymousClass01H A0N;

    static {
        HashSet A12 = C12970iu.A12();
        A0O = A12;
        A12.add("android.permission.CAMERA");
        A12.add("android.permission.WRITE_EXTERNAL_STORAGE");
        A12.add("android.permission.READ_EXTERNAL_STORAGE");
    }

    public AnonymousClass67N(AnonymousClass01H r2, AnonymousClass01H r3, AnonymousClass01H r4, AnonymousClass01H r5, AnonymousClass01H r6, AnonymousClass01H r7, AnonymousClass01H r8, AnonymousClass01H r9, AnonymousClass01H r10, AnonymousClass01H r11, AnonymousClass01H r12, AnonymousClass01H r13, AnonymousClass01H r14, AnonymousClass01H r15, AnonymousClass01H r16, AnonymousClass01H r17, AnonymousClass01H r18, AnonymousClass01H r19, AnonymousClass01H r20, AnonymousClass01H r21, AnonymousClass01H r22, AnonymousClass01H r23, AnonymousClass01H r24, AnonymousClass01H r25) {
        this.A0I = r2;
        this.A00 = r3;
        this.A05 = r4;
        this.A0D = r5;
        this.A0B = r6;
        this.A0E = r7;
        this.A0M = r8;
        this.A01 = r9;
        this.A0N = r10;
        this.A0G = r11;
        this.A0K = r12;
        this.A08 = r13;
        this.A0H = r14;
        this.A0F = r15;
        this.A02 = r16;
        this.A09 = r17;
        this.A0A = r18;
        this.A03 = r19;
        this.A07 = r20;
        this.A0J = r21;
        this.A06 = r22;
        this.A04 = r23;
        this.A0L = r24;
        this.A0C = r25;
    }

    public static final String A00(HashMap hashMap) {
        ArrayList A0x = C12980iv.A0x(hashMap.keySet());
        try {
            JSONObject A0a = C117295Zj.A0a();
            Collections.sort(A0x);
            for (int i = 0; i < A0x.size(); i++) {
                A0a.put((String) A0x.get(i), hashMap.get(A0x.get(i)));
            }
            return A0a.toString();
        } catch (JSONException e) {
            Log.e("Failed to Convert Map to JSON object.", e);
            return null;
        }
    }

    public final UserJid A01(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return UserJid.get(str);
            } catch (AnonymousClass1MW e) {
                C27631Ih A02 = C27631Ih.A02(str);
                ((AbstractC15710nm) this.A0B.get()).AaV("bloks/openchat - Jid missing suffix", e.getMessage(), true);
                return A02;
            }
        } else {
            throw new AnonymousClass1MW("Jid is Empty");
        }
    }

    @Override // X.AnonymousClass1AH
    public void A6B(AbstractC28681Oo r9, AbstractC28681Oo r10, Object obj, String str, HashMap hashMap) {
        AnonymousClass1AK r1 = (AnonymousClass1AK) this.A03.get();
        r1.A01.A02(null, new C71003cE(r10, r9, r1, obj), null, str, (String) hashMap.get("params"), null, true);
    }

    @Override // X.AnonymousClass1AH
    public void A6C(AbstractC28681Oo r3, AbstractC28681Oo r4, Object obj, String str, HashMap hashMap) {
        hashMap.put("nest_data_manifest", "true");
        A6B(r3, r4, obj, str, hashMap);
    }

    @Override // X.AnonymousClass1AH
    public void A8q(HashMap hashMap) {
        String A00 = A00(hashMap);
        if (!TextUtils.isEmpty(A00)) {
            ((C19560uJ) this.A06.get()).A01.remove(A00);
        }
    }

    @Override // X.AnonymousClass1AH
    public void A90(Activity activity, AbstractC28681Oo r4) {
        ((C130735zt) this.A04.get()).A00(activity);
        Stack stack = C130435zP.A01;
        if (!stack.isEmpty()) {
            BkBottomSheetContainerFragment bkBottomSheetContainerFragment = (BkBottomSheetContainerFragment) stack.peek();
            bkBottomSheetContainerFragment.A02 = r4;
            bkBottomSheetContainerFragment.A1B();
        }
    }

    @Override // X.AnonymousClass1AH
    public String AAU(Activity activity, String str, int i) {
        switch (str.hashCode()) {
            case -1808118735:
                if (str.equals("String")) {
                    return ((C14850m9) this.A00.get()).A03(i);
                }
                return "null";
            case -672261858:
                if (str.equals("Integer")) {
                    return Integer.toString(((C14850m9) this.A00.get()).A02(i));
                }
                return "null";
            case 1729365000:
                if (str.equals("Boolean")) {
                    return Boolean.toString(((C14850m9) this.A00.get()).A07(i));
                }
                return "null";
            default:
                return "null";
        }
    }

    @Override // X.AnonymousClass1AH
    public Map AAx() {
        return ((C128695wW) this.A07.get()).A00(((C15570nT) this.A0E.get()).A03());
    }

    @Override // X.AnonymousClass1AH
    public ClipboardManager ABR() {
        return ((AnonymousClass01d) this.A0H.get()).A0B();
    }

    @Override // X.AnonymousClass1AH
    public long ACC() {
        return ((C14830m7) this.A0I.get()).A00();
    }

    @Override // X.AnonymousClass1AH
    public File ACq(String str) {
        return ((C14330lG) this.A0C.get()).A0M(str);
    }

    @Override // X.AnonymousClass1AH
    public void AYc(Activity activity, AbstractC115815Ta r7) {
        C125555rR r4 = new C125555rR(r7.AAL());
        try {
            C130435zP A00 = ((C130735zt) this.A04.get()).A00(activity);
            ActivityC000900k r6 = (ActivityC000900k) activity;
            if (r4.A00.A02.get(35) != null) {
                String A0d = C12960it.A0d(C12990iw.A0n(), C12960it.A0k("bottom_sheet_fragment_tag"));
                AnonymousClass009.A05(r4);
                BkBottomSheetContentFragment A002 = BkBottomSheetContentFragment.A00(r4, (C18840t8) A00.A00.get(), A0d, false);
                BkBottomSheetContainerFragment bkBottomSheetContainerFragment = new BkBottomSheetContainerFragment();
                bkBottomSheetContainerFragment.A01 = C117315Zl.A05(A002, A0d);
                if (!bkBottomSheetContainerFragment.A0e()) {
                    AnonymousClass01F A0V = r6.A0V();
                    AnonymousClass009.A05(A0V);
                    bkBottomSheetContainerFragment.A1F(A0V, C12960it.A0d(C12990iw.A0n(), C12960it.A0k("bottom_sheet_container_tag")));
                    C130435zP.A01.push(bkBottomSheetContainerFragment);
                    return;
                }
                return;
            }
            throw new AnonymousClass5q5();
        } catch (AnonymousClass5q5 e) {
            Log.e(e);
            e.getMessage();
        }
    }

    @Override // X.AnonymousClass1AH
    public void AYd(Activity activity, String str) {
        Class ABc = ((C17070qD) this.A0F.get()).A02().ABc();
        if (ABc != null) {
            Intent A0D = C12990iw.A0D(activity, ABc);
            A0D.putExtra("extra_transaction_id", str);
            activity.startActivity(A0D);
        }
    }

    @Override // X.AnonymousClass1AH
    public void AYe(Activity activity, String str) {
        Class AG9 = ((C17070qD) this.A0F.get()).A02().AG9();
        if (AG9 != null) {
            Intent A0D = C12990iw.A0D(activity, AG9);
            A0D.putExtra("extra_transaction_id", str);
            activity.startActivity(A0D);
        }
    }

    @Override // X.AnonymousClass1AH
    public void AYf(Activity activity, String str, String str2) {
        try {
            UserJid A01 = A01(str);
            String A03 = C248917h.A03(A01);
            if (A03 != null) {
                Object[] A1a = C12980iv.A1a();
                A1a[0] = A03;
                if (str2 == null) {
                    str2 = "";
                }
                A1a[1] = str2;
                ((AnonymousClass12Q) this.A02.get()).Ab9(activity, Uri.parse(String.format("http://api.whatsapp.com/send?phone=%s&text=%s", A1a)));
                return;
            }
            throw new AnonymousClass1MW(C12960it.A0Z(A01, "invalid jid ", C12960it.A0h()));
        } catch (AnonymousClass1MW e) {
            ((AbstractC15710nm) this.A0B.get()).AaV("bloks/openchat", e.getMessage(), true);
            ((C14900mE) this.A0D.get()).A07(R.string.no_internet_message, 0);
        }
    }

    @Override // X.AnonymousClass1AH
    public void AYg(Activity activity, String str) {
        try {
            UserJid A01 = A01(str);
            String A03 = C248917h.A03(A01);
            if (A03 != null) {
                C15370n3 A0A = ((C15550nR) this.A08.get()).A0A(A01);
                if (A0A == null || !((C15570nT) this.A0E.get()).A0F(A01)) {
                    String replaceAll = A03.replaceAll("\\D", "");
                    String str2 = null;
                    if (replaceAll.length() < 5) {
                        Log.w("bkextentionsimpl/converttointlformat/too-short-no-cc");
                    } else {
                        Matcher matcher = Pattern.compile("^([17]|2[07]|3[0123469]|4[013456789]|5[12345678]|6[0123456]|8[1246]|9[0123458]|\\d{3})\\d*?(\\d{4,6})$").matcher(replaceAll);
                        if (matcher.find()) {
                            String group = matcher.group(1);
                            AnonymousClass009.A05(group);
                            String substring = replaceAll.substring(group.length());
                            C22680zT r1 = (C22680zT) this.A0A.get();
                            if (AbstractActivityC452520u.A09(r1, group, substring) == 1) {
                                int parseInt = Integer.parseInt(group);
                                try {
                                    substring = r1.A02(parseInt, substring.replaceAll("\\D", ""));
                                } catch (Exception e) {
                                    Log.w(C12960it.A0W(parseInt, "bkextentionsimpl/converttointlformat/trim/error "), e);
                                }
                                StringBuilder A0k = C12960it.A0k("+");
                                A0k.append(group);
                                str2 = C12960it.A0d(substring, A0k);
                            }
                        }
                    }
                    if (A0A != null || str2 == null) {
                        activity.startActivity(C14960mK.A0R(activity, A01, null, true));
                    } else {
                        ((AbstractC14440lR) this.A0M.get()).Ab2(new Runnable(activity, A01, this, str2) { // from class: X.6K2
                            public final /* synthetic */ Activity A00;
                            public final /* synthetic */ UserJid A01;
                            public final /* synthetic */ AnonymousClass67N A02;
                            public final /* synthetic */ String A03;

                            {
                                this.A02 = r3;
                                this.A03 = r4;
                                this.A00 = r1;
                                this.A01 = r2;
                            }

                            @Override // java.lang.Runnable
                            public final void run() {
                                AnonymousClass67N r0 = this.A02;
                                String str3 = this.A03;
                                Activity activity2 = this.A00;
                                UserJid userJid = this.A01;
                                ((C253318z) r0.A09.get()).A00(AnonymousClass1JA.A0C, str3);
                                AnonymousClass61K.A00(
                                /*  JADX ERROR: Method code generation error
                                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: INVOKE  
                                      (wrap: X.6Iy : 0x0017: CONSTRUCTOR  (r0v3 X.6Iy A[REMOVE]) = (r3v0 'activity2' android.app.Activity), (r2v0 'userJid' com.whatsapp.jid.UserJid) call: X.6Iy.<init>(android.app.Activity, com.whatsapp.jid.UserJid):void type: CONSTRUCTOR)
                                     type: STATIC call: X.61K.A00(java.lang.Runnable):void in method: X.6K2.run():void, file: classes4.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0017: CONSTRUCTOR  (r0v3 X.6Iy A[REMOVE]) = (r3v0 'activity2' android.app.Activity), (r2v0 'userJid' com.whatsapp.jid.UserJid) call: X.6Iy.<init>(android.app.Activity, com.whatsapp.jid.UserJid):void type: CONSTRUCTOR in method: X.6K2.run():void, file: classes4.dex
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                    	... 15 more
                                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Iy, state: NOT_LOADED
                                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                    	... 21 more
                                    */
                                /*
                                    this = this;
                                    X.67N r0 = r5.A02
                                    java.lang.String r4 = r5.A03
                                    android.app.Activity r3 = r5.A00
                                    com.whatsapp.jid.UserJid r2 = r5.A01
                                    X.01H r0 = r0.A09
                                    java.lang.Object r1 = r0.get()
                                    X.18z r1 = (X.C253318z) r1
                                    X.1JA r0 = X.AnonymousClass1JA.A0C
                                    r1.A00(r0, r4)
                                    X.6Iy r0 = new X.6Iy
                                    r0.<init>(r3, r2)
                                    X.AnonymousClass61K.A00(r0)
                                    return
                                */
                                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6K2.run():void");
                            }
                        });
                    }
                } else {
                    activity.startActivity(((AnonymousClass17S) this.A01.get()).A00(activity));
                }
            } else {
                throw new AnonymousClass1MW(C12960it.A0Z(A01, "invalid jid ", C12960it.A0h()));
            }
        } catch (AnonymousClass1MW e2) {
            ((AbstractC15710nm) this.A0B.get()).AaV("bloks/openContactInfo - ", e2.getMessage(), true);
            ((C14900mE) this.A0D.get()).A07(R.string.no_internet_message, 0);
        }
    }

    @Override // X.AnonymousClass1AH
    public void AYi(Activity activity, String str, String str2, List list) {
        BkActionBottomSheet.A00((C18840t8) this.A05.get(), str, str2, list).A1F(((ActivityC000900k) activity).A0V(), "bloks_action_sheet_tag");
    }

    @Override // X.AnonymousClass1AH
    public void AYj(Activity activity, String str, String str2) {
        activity.startActivity(WaBloksActivity.A09(activity, str, str2));
    }

    @Override // X.AnonymousClass1AH
    public void AYm(Activity activity, AbstractC115815Ta r5, String str, String str2) {
        activity.startActivity(C12990iw.A0D(activity, WaBloksActivity.class).putExtra("screen_name", str).putExtra("screen_params", str2).putExtra("screen_cache_config", (Parcelable) null));
    }

    @Override // X.AnonymousClass1AH
    public void AZL(Activity activity) {
        ((C130735zt) this.A04.get()).A00(activity);
        Stack stack = C130435zP.A01;
        if (!stack.isEmpty()) {
            ((AnonymousClass01E) stack.peek()).A0E().A0H();
        }
    }

    @Override // X.AnonymousClass1AH
    public void AZf(Activity activity, AbstractC115815Ta r10, boolean z) {
        C125555rR r3 = new C125555rR(r10.AAL());
        C130435zP A00 = ((C130735zt) this.A04.get()).A00(activity);
        String A0d = C12960it.A0d(C12990iw.A0n(), C12960it.A0k("bottom_sheet_fragment_tag"));
        BkBottomSheetContentFragment A002 = BkBottomSheetContentFragment.A00(r3, (C18840t8) A00.A00.get(), A0d, z);
        BkBottomSheetContainerFragment bkBottomSheetContainerFragment = (BkBottomSheetContainerFragment) C130435zP.A01.peek();
        C004902f r4 = new C004902f(bkBottomSheetContainerFragment.A0E());
        if (z) {
            r4.A0F(A0d);
        }
        r4.A02 = R.anim.enter_from_right;
        r4.A03 = R.anim.exit_to_left;
        r4.A05 = R.anim.enter_from_left;
        r4.A06 = R.anim.exit_to_right;
        r4.A0B(A002, A0d, bkBottomSheetContainerFragment.A00.getId());
        r4.A01();
    }

    @Override // X.AnonymousClass1AH
    public void Aa0(C90184Mx r7, HashMap hashMap) {
        String A00 = A00(hashMap);
        if (!TextUtils.isEmpty(A00)) {
            C19560uJ r4 = (C19560uJ) this.A06.get();
            r4.A01.put(A00, new AnonymousClass3FE(r7.A00, r7.A01, r4.A00));
        }
    }

    @Override // X.AnonymousClass1AH
    public void Aaa(Activity activity, AbstractC115825Tb r8, String[] strArr) {
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i < length) {
                String str = strArr[i];
                Set set = A0O;
                if (!set.contains(str)) {
                    StringBuilder A0k = C12960it.A0k("Unauthorized permission request ");
                    A0k.append(str);
                    A0k.append(", Bloks allowed to request only whitelisted permissions ");
                    Log.e(C12960it.A0d(set.toString(), A0k));
                    break;
                }
                i++;
            } else if (activity instanceof ActivityC13790kL) {
                ActivityC13790kL r3 = (ActivityC13790kL) activity;
                Intent A09 = RequestPermissionActivity.A09(activity, (C15890o4) this.A0L.get(), 30);
                if (A09 == null) {
                    r8.AVM(true);
                    return;
                }
                r3.A2Y(new C1323166k(r3, r8, this));
                r3.startActivityForResult(A09, 30);
                return;
            }
        }
        r8.AVM(false);
    }

    @Override // X.AnonymousClass1AH
    public void Abc(String str, ArrayList arrayList, HashMap hashMap, int i, int i2) {
        int i3 = 2;
        if (i2 == 1) {
            i3 = 1;
        } else if (i2 != 2) {
            i3 = 0;
        }
        ((C16120oU) this.A0N.get()).A07(C124905qJ.A00(str, arrayList, hashMap, i, i3));
    }

    @Override // X.AnonymousClass1AH
    public void AdI(Activity activity, String str) {
        AnonymousClass19X r1 = (AnonymousClass19X) this.A0G.get();
        if (!(activity instanceof WaBloksActivity) || !r1.A02() || !r1.A07.A01(str)) {
            C117295Zj.A0j(activity, str);
            return;
        }
        WaBloksActivity waBloksActivity = (WaBloksActivity) activity;
        waBloksActivity.A2Y(new AbstractC42541vN(waBloksActivity, str) { // from class: X.66h
            public final /* synthetic */ WaBloksActivity A01;
            public final /* synthetic */ String A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // X.AbstractC42541vN
            public final boolean ALt(Intent intent, int i, int i2) {
                AnonymousClass67N r5 = AnonymousClass67N.this;
                String str2 = this.A02;
                WaBloksActivity waBloksActivity2 = this.A01;
                if (i2 != -1 || i != 1) {
                    return false;
                }
                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                if (A07.isEmpty()) {
                    return true;
                }
                ((AnonymousClass19X) r5.A0G.get()).A01(new AnonymousClass6DZ(r5, waBloksActivity2, str2, A07), str2);
                return true;
            }
        });
        HashSet A12 = C12970iu.A12();
        C12980iv.A1R(A12, 55);
        C42641vY r2 = new C42641vY(waBloksActivity);
        r2.A0D = true;
        r2.A0F = true;
        r2.A0R = C12980iv.A0x(A12);
        r2.A0O = str;
        waBloksActivity.startActivityForResult(r2.A00(), 1);
    }

    @Override // X.AnonymousClass1AH
    public void Adx(Activity activity, ProgressDialog progressDialog, String str, boolean z) {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog.setMessage(str);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            if (z) {
                progressDialog.setCancelable(true);
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener(activity, this) { // from class: X.62E
                    public final /* synthetic */ Activity A00;
                    public final /* synthetic */ AnonymousClass67N A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnCancelListener
                    public final void onCancel(DialogInterface dialogInterface) {
                        this.A00.onBackPressed();
                    }
                });
            } else {
                progressDialog.setCancelable(false);
            }
            progressDialog.show();
        }
    }

    @Override // X.AnonymousClass1AH
    public void Ae8(Activity activity, Intent intent, AnonymousClass3CY r5, int i) {
        if (!(activity instanceof ActivityC13790kL)) {
            r5.A00(EnumC868449c.ERROR);
            return;
        }
        ActivityC13790kL r3 = (ActivityC13790kL) activity;
        r3.A2Y(new C1323266l(r3, r5, this));
        r3.startActivityForResult(intent, 1);
    }

    @Override // X.AnonymousClass1AH
    public void Aey(String str) {
        ((C14900mE) this.A0D.get()).A0E(str, 0);
    }
}
