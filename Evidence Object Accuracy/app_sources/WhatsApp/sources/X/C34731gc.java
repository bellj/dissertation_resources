package X;

/* renamed from: X.1gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34731gc extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public String A05;
    public String A06;

    public C34731gc() {
        super(2290, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(2, this.A00);
        r3.Abe(7, this.A01);
        r3.Abe(8, this.A05);
        r3.Abe(1, this.A06);
        r3.Abe(3, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamMdBootstrapAppStateDataUploaded {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapContactsCount", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapPayloadSize", this.A03);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapPayloadType", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapStepResult", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdRegAttemptId", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdSessionId", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdTimestamp", this.A04);
        sb.append("}");
        return sb.toString();
    }
}
