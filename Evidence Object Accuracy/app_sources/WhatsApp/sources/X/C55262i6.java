package X;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2i6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C55262i6 extends AbstractC008704k {
    public static final TimeInterpolator A0B = new ValueAnimator().getInterpolator();
    public List A00 = C12960it.A0l();
    public List A01 = C12960it.A0l();
    public List A02 = C12960it.A0l();
    public List A03 = C12960it.A0l();
    public List A04 = C12960it.A0l();
    public List A05 = C12960it.A0l();
    public List A06 = C12960it.A0l();
    public List A07 = C12960it.A0l();
    public List A08 = C12960it.A0l();
    public List A09 = C12960it.A0l();
    public List A0A = C12960it.A0l();

    public C55262i6() {
        ((AnonymousClass04Y) this).A00 = 240;
        super.A01 = 240;
        super.A02 = 240;
        super.A03 = 240;
        ((AbstractC008704k) this).A00 = false;
    }

    public static final void A01(List list) {
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                AnonymousClass028.A0F(((AnonymousClass03U) list.get(size)).A0H).A00();
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public void A08() {
        List list = this.A08;
        int size = list.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            AnonymousClass03U r1 = ((C91554Se) list.get(size)).A04;
            View view = r1.A0H;
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            A03(r1);
            list.remove(size);
        }
        List list2 = this.A09;
        int size2 = list2.size();
        while (true) {
            size2--;
            if (size2 < 0) {
                break;
            }
            A03((AnonymousClass03U) list2.get(size2));
            list2.remove(size2);
        }
        List list3 = this.A06;
        int size3 = list3.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            AnonymousClass03U r12 = (AnonymousClass03U) list3.get(size3);
            View view2 = r12.A0H;
            view2.setScaleX(1.0f);
            view2.setScaleY(1.0f);
            A03(r12);
            list3.remove(size3);
        }
        List list4 = this.A07;
        int size4 = list4.size();
        while (true) {
            size4--;
            if (size4 < 0) {
                break;
            }
            AnonymousClass3DA r13 = (AnonymousClass3DA) list4.get(size4);
            AnonymousClass03U r0 = r13.A05;
            if (r0 != null) {
                A0J(r0, r13);
            }
            AnonymousClass03U r02 = r13.A04;
            if (r02 != null) {
                A0J(r02, r13);
            }
        }
        list4.clear();
        if (A0B()) {
            List list5 = this.A05;
            int size5 = list5.size();
            while (true) {
                size5--;
                if (size5 < 0) {
                    break;
                }
                List A10 = C12980iv.A10(list5, size5);
                int size6 = A10.size();
                while (true) {
                    size6--;
                    if (size6 >= 0) {
                        AnonymousClass03U r14 = ((C91554Se) A10.get(size6)).A04;
                        View view3 = r14.A0H;
                        view3.setTranslationY(0.0f);
                        view3.setTranslationX(0.0f);
                        A03(r14);
                        A10.remove(size6);
                        if (A10.isEmpty()) {
                            list5.remove(A10);
                        }
                    }
                }
            }
            List list6 = this.A01;
            int size7 = list6.size();
            while (true) {
                size7--;
                if (size7 < 0) {
                    break;
                }
                List A102 = C12980iv.A10(list6, size7);
                int size8 = A102.size();
                while (true) {
                    size8--;
                    if (size8 >= 0) {
                        AnonymousClass03U r15 = (AnonymousClass03U) A102.get(size8);
                        View view4 = r15.A0H;
                        view4.setScaleX(1.0f);
                        view4.setScaleY(1.0f);
                        A03(r15);
                        A102.remove(size8);
                        if (A102.isEmpty()) {
                            list6.remove(A102);
                        }
                    }
                }
            }
            List list7 = this.A03;
            int size9 = list7.size();
            while (true) {
                size9--;
                if (size9 >= 0) {
                    List A103 = C12980iv.A10(list7, size9);
                    int size10 = A103.size();
                    while (true) {
                        size10--;
                        if (size10 >= 0) {
                            AnonymousClass3DA r16 = (AnonymousClass3DA) A103.get(size10);
                            AnonymousClass03U r03 = r16.A05;
                            if (r03 != null) {
                                A0J(r03, r16);
                            }
                            AnonymousClass03U r04 = r16.A04;
                            if (r04 != null) {
                                A0J(r04, r16);
                            }
                            if (A103.isEmpty()) {
                                list7.remove(A103);
                            }
                        }
                    }
                } else {
                    A01(this.A0A);
                    A01(this.A04);
                    A01(this.A00);
                    A01(this.A02);
                    A02();
                    return;
                }
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public void A09() {
        long j;
        long j2;
        List<AnonymousClass03U> list = this.A09;
        boolean z = !list.isEmpty();
        List list2 = this.A08;
        boolean z2 = !list2.isEmpty();
        List list3 = this.A07;
        boolean z3 = !list3.isEmpty();
        List list4 = this.A06;
        boolean z4 = !list4.isEmpty();
        if (z || z2 || z4 || z3) {
            for (AnonymousClass03U r3 : list) {
                AnonymousClass0QQ A0F = AnonymousClass028.A0F(r3.A0H);
                this.A0A.add(r3);
                A0F.A07(A07());
                A0F.A03(0.0f);
                A0F.A04(0.0f);
                A0F.A09(new AnonymousClass2xN(A0F, r3, this));
                A0F.A01();
            }
            list.clear();
            if (z2) {
                ArrayList A0l = C12960it.A0l();
                A0l.addAll(list2);
                this.A05.add(A0l);
                list2.clear();
                RunnableBRunnable0Shape11S0200000_I1_1 runnableBRunnable0Shape11S0200000_I1_1 = new RunnableBRunnable0Shape11S0200000_I1_1(this, 18, A0l);
                if (z) {
                    ((C91554Se) A0l.get(0)).A04.A0H.postOnAnimationDelayed(runnableBRunnable0Shape11S0200000_I1_1, A07());
                } else {
                    runnableBRunnable0Shape11S0200000_I1_1.run();
                }
            }
            if (z3) {
                ArrayList A0l2 = C12960it.A0l();
                A0l2.addAll(list3);
                this.A03.add(A0l2);
                list3.clear();
                RunnableBRunnable0Shape11S0200000_I1_1 runnableBRunnable0Shape11S0200000_I1_12 = new RunnableBRunnable0Shape11S0200000_I1_1(this, 16, A0l2);
                if (z) {
                    ((AnonymousClass3DA) A0l2.get(0)).A05.A0H.postOnAnimationDelayed(runnableBRunnable0Shape11S0200000_I1_12, A07());
                } else {
                    runnableBRunnable0Shape11S0200000_I1_12.run();
                }
            }
            if (z4) {
                ArrayList A0l3 = C12960it.A0l();
                A0l3.addAll(list4);
                this.A01.add(A0l3);
                list4.clear();
                RunnableBRunnable0Shape11S0200000_I1_1 runnableBRunnable0Shape11S0200000_I1_13 = new RunnableBRunnable0Shape11S0200000_I1_1(this, 17, A0l3);
                if (z || z2 || z3) {
                    long j3 = 0;
                    if (z) {
                        j = A07();
                    } else {
                        j = 0;
                    }
                    if (z2) {
                        j2 = A06();
                    } else {
                        j2 = 0;
                    }
                    if (z3) {
                        j3 = A05();
                    }
                    ((AnonymousClass03U) A0l3.get(0)).A0H.postOnAnimationDelayed(runnableBRunnable0Shape11S0200000_I1_13, j + Math.max(j2, j3));
                    return;
                }
                runnableBRunnable0Shape11S0200000_I1_13.run();
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public void A0A(AnonymousClass03U r9) {
        View view = r9.A0H;
        AnonymousClass028.A0F(view).A00();
        List list = this.A08;
        int size = list.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (((C91554Se) list.get(size)).A04 == r9) {
                view.setTranslationY(0.0f);
                view.setTranslationX(0.0f);
                A03(r9);
                list.remove(size);
            }
        }
        A0I(r9, this.A07);
        if (this.A09.remove(r9)) {
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            A03(r9);
        }
        if (this.A06.remove(r9)) {
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            A03(r9);
        }
        List list2 = this.A03;
        int size2 = list2.size();
        while (true) {
            size2--;
            if (size2 < 0) {
                break;
            }
            List A10 = C12980iv.A10(list2, size2);
            A0I(r9, A10);
            if (A10.isEmpty()) {
                list2.remove(size2);
            }
        }
        List list3 = this.A05;
        int size3 = list3.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            List A102 = C12980iv.A10(list3, size3);
            int size4 = A102.size();
            while (true) {
                size4--;
                if (size4 < 0) {
                    break;
                } else if (((C91554Se) A102.get(size4)).A04 == r9) {
                    view.setTranslationY(0.0f);
                    view.setTranslationX(0.0f);
                    A03(r9);
                    A102.remove(size4);
                    if (A102.isEmpty()) {
                        list3.remove(size3);
                    }
                }
            }
        }
        List list4 = this.A01;
        int size5 = list4.size();
        while (true) {
            size5--;
            if (size5 >= 0) {
                List A103 = C12980iv.A10(list4, size5);
                if (A103.remove(r9)) {
                    view.setScaleX(1.0f);
                    view.setScaleY(1.0f);
                    A03(r9);
                    if (A103.isEmpty()) {
                        list4.remove(size5);
                    }
                }
            } else {
                this.A0A.remove(r9);
                this.A00.remove(r9);
                this.A02.remove(r9);
                C12980iv.A1K(this, r9, this.A04);
                return;
            }
        }
    }

    @Override // X.AnonymousClass04Y
    public boolean A0B() {
        return !this.A06.isEmpty() || !this.A07.isEmpty() || !this.A08.isEmpty() || !this.A09.isEmpty() || !this.A04.isEmpty() || !this.A0A.isEmpty() || !this.A00.isEmpty() || !this.A02.isEmpty() || !this.A05.isEmpty() || !this.A01.isEmpty() || !this.A03.isEmpty();
    }

    @Override // X.AnonymousClass04Y
    public boolean A0C(AnonymousClass03U r3, List list) {
        return !list.isEmpty() || super.A0C(r3, list);
    }

    @Override // X.AbstractC008704k
    public boolean A0D(AnonymousClass03U r3) {
        A0H(r3);
        View view = r3.A0H;
        view.setScaleX(0.0f);
        view.setScaleY(0.0f);
        this.A06.add(r3);
        return true;
    }

    @Override // X.AbstractC008704k
    public boolean A0E(AnonymousClass03U r2) {
        A0H(r2);
        this.A09.add(r2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0028, code lost:
        if (r1 != 0) goto L_0x002a;
     */
    @Override // X.AbstractC008704k
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0F(X.AnonymousClass03U r9, int r10, int r11, int r12, int r13) {
        /*
            r8 = this;
            r3 = r9
            android.view.View r2 = r9.A0H
            float r1 = (float) r10
            float r0 = r2.getTranslationX()
            float r1 = r1 + r0
            int r4 = (int) r1
            float r1 = (float) r11
            float r0 = r2.getTranslationY()
            float r1 = r1 + r0
            int r5 = (int) r1
            r8.A0H(r9)
            r6 = r12
            int r0 = r12 - r4
            r7 = r13
            int r1 = r13 - r5
            if (r0 != 0) goto L_0x0023
            if (r1 != 0) goto L_0x002a
            r8.A03(r9)
            r0 = 0
            return r0
        L_0x0023:
            int r0 = -r0
            float r0 = (float) r0
            r2.setTranslationX(r0)
            if (r1 == 0) goto L_0x002f
        L_0x002a:
            int r0 = -r1
            float r0 = (float) r0
            r2.setTranslationY(r0)
        L_0x002f:
            java.util.List r0 = r8.A08
            X.4Se r2 = new X.4Se
            r2.<init>(r3, r4, r5, r6, r7)
            r0.add(r2)
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C55262i6.A0F(X.03U, int, int, int, int):boolean");
    }

    @Override // X.AbstractC008704k
    public boolean A0G(AnonymousClass03U r14, AnonymousClass03U r15, int i, int i2, int i3, int i4) {
        if (r14 == r15) {
            return A0F(r14, i, i2, i3, i4);
        }
        View view = r14.A0H;
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        float alpha = view.getAlpha();
        A0H(r14);
        view.setTranslationX(translationX);
        view.setTranslationY(translationY);
        view.setAlpha(alpha);
        A0H(r15);
        View view2 = r15.A0H;
        view2.setTranslationX((float) (-((int) (((float) (i3 - i)) - translationX))));
        view2.setTranslationY((float) (-((int) (((float) (i4 - i2)) - translationY))));
        view2.setAlpha(0.0f);
        this.A07.add(new AnonymousClass3DA(r14, r15, i, i2, i3, i4));
        return true;
    }

    public final void A0H(AnonymousClass03U r3) {
        r3.A0H.animate().setInterpolator(A0B);
        A0A(r3);
    }

    public final void A0I(AnonymousClass03U r4, List list) {
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                AnonymousClass3DA r1 = (AnonymousClass3DA) list.get(size);
                if (A0J(r4, r1) && r1.A05 == null && r1.A04 == null) {
                    list.remove(r1);
                }
            } else {
                return;
            }
        }
    }

    public final boolean A0J(AnonymousClass03U r5, AnonymousClass3DA r6) {
        if (r6.A04 == r5) {
            r6.A04 = null;
        } else if (r6.A05 != r5) {
            return false;
        } else {
            r6.A05 = null;
        }
        View view = r5.A0H;
        view.setAlpha(1.0f);
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        A03(r5);
        return true;
    }
}
