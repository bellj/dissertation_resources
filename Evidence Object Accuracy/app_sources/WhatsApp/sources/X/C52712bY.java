package X;

import android.content.Context;
import android.widget.ArrayAdapter;
import com.whatsapp.R;
import com.whatsapp.phonematching.CountryPicker;
import java.util.List;

/* renamed from: X.2bY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C52712bY extends ArrayAdapter {
    public final String A00;
    public final String A01;
    public final /* synthetic */ CountryPicker A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52712bY(Context context, CountryPicker countryPicker, String str, String str2, List list) {
        super(context, (int) R.layout.country_picker_row, list);
        this.A02 = countryPicker;
        this.A00 = str;
        this.A01 = str2;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        return Math.max(1, super.getCount());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0044, code lost:
        if (r0 == null) goto L_0x0046;
     */
    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r8, android.view.View r9, android.view.ViewGroup r10) {
        /*
            r7 = this;
            int r0 = super.getCount()
            r2 = 1
            if (r0 != 0) goto L_0x0032
            if (r9 == 0) goto L_0x0012
            r0 = 2131366511(0x7f0a126f, float:1.8352918E38)
            android.view.View r0 = r9.findViewById(r0)
            if (r0 != 0) goto L_0x0031
        L_0x0012:
            android.content.Context r0 = r7.getContext()
            android.widget.LinearLayout r9 = new android.widget.LinearLayout
            r9.<init>(r0)
            com.whatsapp.phonematching.CountryPicker r0 = r7.A02
            android.view.LayoutInflater r1 = r0.getLayoutInflater()
            r0 = 2131559307(0x7f0d038b, float:1.8743954E38)
            r1.inflate(r0, r9, r2)
            r0 = 2131363298(0x7f0a05e2, float:1.83464E38)
            android.view.View r0 = r9.findViewById(r0)
            r0.setClickable(r2)
        L_0x0031:
            return r9
        L_0x0032:
            java.lang.Object r6 = r7.getItem(r8)
            X.AnonymousClass009.A05(r6)
            X.3D6 r6 = (X.AnonymousClass3D6) r6
            if (r9 == 0) goto L_0x0046
            r0 = 2131362922(0x7f0a046a, float:1.8345638E38)
            android.view.View r0 = r9.findViewById(r0)
            if (r0 != 0) goto L_0x005b
        L_0x0046:
            android.content.Context r0 = r7.getContext()
            android.widget.LinearLayout r9 = new android.widget.LinearLayout
            r9.<init>(r0)
            com.whatsapp.phonematching.CountryPicker r0 = r7.A02
            android.view.LayoutInflater r1 = r0.getLayoutInflater()
            r0 = 2131558855(0x7f0d01c7, float:1.8743038E38)
            r1.inflate(r0, r9, r2)
        L_0x005b:
            r0 = 2131362923(0x7f0a046b, float:1.834564E38)
            com.whatsapp.TextEmojiLabel r1 = X.C12970iu.A0T(r9, r0)
            java.lang.String r0 = r6.A02
            r4 = 0
            r1.A0G(r4, r0)
            r0 = 2
            X.AnonymousClass028.A0a(r1, r0)
            r0 = 2131362922(0x7f0a046a, float:1.8345638E38)
            android.widget.TextView r3 = X.C12960it.A0I(r9, r0)
            java.lang.String r5 = r6.A01
            r3.setText(r5)
            r0 = 2131362925(0x7f0a046d, float:1.8345644E38)
            android.widget.TextView r1 = X.C12960it.A0J(r9, r0)
            java.lang.String r0 = r6.A04
            if (r0 == 0) goto L_0x00ce
            r1.setText(r0)
            r0 = 0
            r1.setVisibility(r0)
        L_0x008a:
            X.C27531Hw.A06(r3)
            r0 = 2131362921(0x7f0a0469, float:1.8345636E38)
            android.widget.TextView r2 = X.C12960it.A0I(r9, r0)
            java.lang.String r0 = "+"
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = r6.A00
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r2.setText(r0)
            r0 = 2131362927(0x7f0a046f, float:1.8345648E38)
            android.widget.ImageView r2 = X.C12970iu.A0L(r9, r0)
            java.lang.String r1 = r6.A03
            java.lang.String r0 = r7.A00
            boolean r0 = android.text.TextUtils.equals(r1, r0)
            if (r0 != 0) goto L_0x00d7
            java.lang.String r0 = r7.A01
            boolean r0 = android.text.TextUtils.equals(r5, r0)
            if (r0 != 0) goto L_0x00d7
            android.content.Context r0 = r7.getContext()
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131100374(0x7f0602d6, float:1.7813128E38)
            X.C12980iv.A14(r1, r3, r0)
            r2.setImageDrawable(r4)
            return r9
        L_0x00ce:
            r0 = 8
            r1.setVisibility(r0)
            r1.setText(r4)
            goto L_0x008a
        L_0x00d7:
            android.content.Context r1 = r7.getContext()
            r0 = 2131100819(0x7f060493, float:1.781403E38)
            X.C12960it.A0s(r1, r3, r0)
            r0 = 2131231340(0x7f08026c, float:1.8078758E38)
            r2.setImageResource(r0)
            android.content.Context r1 = r7.getContext()
            r0 = 2131100317(0x7f06029d, float:1.7813012E38)
            X.AnonymousClass2GE.A05(r1, r2, r0)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52712bY.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
