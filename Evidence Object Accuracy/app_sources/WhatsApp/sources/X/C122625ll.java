package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5ll  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122625ll extends AbstractC118835cS {
    public TextView A00;
    public TextView A01;

    public C122625ll(View view) {
        super(view);
        this.A01 = C12960it.A0I(view, R.id.title);
        this.A00 = C12960it.A0I(view, R.id.description);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r4, int i) {
        C123275mt r42 = (C123275mt) r4;
        this.A01.setText(r42.A04);
        TextView textView = this.A00;
        textView.setText(r42.A03);
        Drawable drawable = r42.A00;
        if (drawable != null) {
            textView.setCompoundDrawables(drawable, null, null, null);
        }
        View.OnClickListener onClickListener = r42.A01;
        if (onClickListener != null) {
            this.A0H.setOnClickListener(onClickListener);
        }
        View.OnLongClickListener onLongClickListener = r42.A02;
        if (onLongClickListener != null) {
            this.A0H.setOnLongClickListener(onLongClickListener);
        }
    }
}
