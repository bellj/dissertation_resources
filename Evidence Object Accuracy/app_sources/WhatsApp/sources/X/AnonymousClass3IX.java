package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import java.util.Arrays;

/* renamed from: X.3IX  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3IX {
    public static final AnonymousClass3IX A02 = new AnonymousClass3IX(new int[]{2}, 8);
    public static final AnonymousClass3IX A03 = new AnonymousClass3IX(new int[]{2, 5, 6}, 8);
    public final int A00;
    public final int[] A01;

    public AnonymousClass3IX(int[] iArr, int i) {
        if (iArr != null) {
            int[] copyOf = Arrays.copyOf(iArr, iArr.length);
            this.A01 = copyOf;
            Arrays.sort(copyOf);
        } else {
            this.A01 = new int[0];
        }
        this.A00 = i;
    }

    public static AnonymousClass3IX A00(Context context) {
        boolean z;
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.media.action.HDMI_AUDIO_PLUG"));
        if (AnonymousClass3JZ.A01 >= 17) {
            String str = AnonymousClass3JZ.A04;
            if ("Amazon".equals(str) || "Xiaomi".equals(str)) {
                z = true;
                if (!z && Settings.Global.getInt(context.getContentResolver(), "external_surround_sound_enabled", 0) == 1) {
                    return A03;
                }
                if (registerReceiver != null || registerReceiver.getIntExtra("android.media.extra.AUDIO_PLUG_STATE", 0) == 0) {
                    return A02;
                }
                return new AnonymousClass3IX(registerReceiver.getIntArrayExtra("android.media.extra.ENCODINGS"), registerReceiver.getIntExtra("android.media.extra.MAX_CHANNEL_COUNT", 8));
            }
        }
        z = false;
        if (!z) {
        }
        if (registerReceiver != null) {
        }
        return A02;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass3IX)) {
            return false;
        }
        AnonymousClass3IX r4 = (AnonymousClass3IX) obj;
        if (!Arrays.equals(this.A01, r4.A01) || this.A00 != r4.A00) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A00 + (Arrays.hashCode(this.A01) * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AudioCapabilities[maxChannelCount=");
        A0k.append(this.A00);
        A0k.append(", supportedEncodings=");
        A0k.append(Arrays.toString(this.A01));
        return C12960it.A0d("]", A0k);
    }
}
