package X;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* renamed from: X.02a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C004502a {
    public static String A00(File file, String str, Date date) {
        String A03 = A03(file.getName(), date);
        String A02 = A02(file.getName(), str);
        File[] listFiles = file.getParentFile().listFiles();
        File file2 = null;
        if (listFiles != null) {
            int i = 0;
            for (File file3 : listFiles) {
                String name = file3.getName();
                if (name.startsWith(A03) && name.endsWith(A02)) {
                    int length = A03.length() + 1;
                    int length2 = name.length();
                    int length3 = length2 - A02.length();
                    if (length3 < length2 && length < length3) {
                        try {
                            int parseInt = Integer.parseInt(name.substring(length, length3));
                            if (parseInt > i) {
                                file2 = file3;
                                i = parseInt;
                            }
                        } catch (NumberFormatException unused) {
                        }
                    }
                }
            }
        }
        if (file2 != null) {
            return file2.getName();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(A03);
        sb.append(".1");
        sb.append(A02);
        return sb.toString();
    }

    public static String A01(String str) {
        return A03(str, new Date()).substring(0, str.indexOf(46) + 1);
    }

    public static String A02(String str, String str2) {
        String substring = str.substring(str.indexOf(46));
        StringBuilder sb = new StringBuilder();
        sb.append(substring);
        sb.append(str2);
        return sb.toString();
    }

    public static String A03(String str, Date date) {
        String substring = str.substring(0, str.indexOf(46));
        StringBuilder sb = new StringBuilder();
        sb.append(substring);
        sb.append("-");
        sb.append(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date));
        return sb.toString();
    }

    public static void A04(File file, String str, int i, boolean z) {
        String str2;
        int length;
        int length2;
        file.getAbsolutePath();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date date = new Date();
        String pattern = simpleDateFormat.toPattern();
        String A02 = A02(file.getName(), str);
        String A01 = A01(file.getName());
        if (i < 0) {
            int indexOf = file.getName().indexOf(46);
            if (indexOf != -1) {
                str2 = file.getName().substring(0, indexOf);
            } else {
                return;
            }
        } else {
            str2 = null;
        }
        File[] listFiles = file.getParentFile().listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                String name = file2.getName();
                if (i < 0) {
                    if (name.startsWith(str2)) {
                        try {
                            file2.delete();
                        } catch (SecurityException | ParseException unused) {
                        }
                    }
                } else if (name.startsWith(A01) && name.endsWith(A02) && (length2 = (length = A01.length()) + pattern.length()) <= name.length()) {
                    long time = (date.getTime() - simpleDateFormat.parse(name.substring(length, length2)).getTime()) / TimeUnit.DAYS.toMillis(1);
                    if (z) {
                        time = Math.abs(time);
                    }
                    if (time <= ((long) i)) {
                        file2.getAbsolutePath();
                    } else {
                        file2.getAbsolutePath();
                        file2.delete();
                    }
                }
            }
        }
    }
}
