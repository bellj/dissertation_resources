package X;

import android.content.Context;
import android.net.wifi.WifiManager;

/* renamed from: X.4ID  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4ID {
    public final WifiManager A00;

    public AnonymousClass4ID(Context context) {
        this.A00 = (WifiManager) context.getApplicationContext().getSystemService("wifi");
    }
}
