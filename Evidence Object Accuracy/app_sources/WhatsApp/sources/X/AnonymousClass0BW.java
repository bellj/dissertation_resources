package X;

import android.database.Cursor;
import android.widget.Filter;

/* renamed from: X.0BW  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BW extends Filter {
    public AbstractC12540i5 A00;

    public AnonymousClass0BW(AbstractC12540i5 r1) {
        this.A00 = r1;
    }

    @Override // android.widget.Filter
    public CharSequence convertResultToString(Object obj) {
        return this.A00.A7k((Cursor) obj);
    }

    @Override // android.widget.Filter
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Cursor Ab7 = this.A00.Ab7(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (Ab7 != null) {
            filterResults.count = Ab7.getCount();
        } else {
            filterResults.count = 0;
            Ab7 = null;
        }
        filterResults.values = Ab7;
        return filterResults;
    }

    @Override // android.widget.Filter
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        AbstractC12540i5 r2 = this.A00;
        Cursor cursor = ((AnonymousClass0BR) r2).A02;
        Object obj = filterResults.values;
        if (obj != null && obj != cursor) {
            r2.A77((Cursor) obj);
        }
    }
}
