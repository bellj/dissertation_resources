package X;

import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.5pS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124405pS extends AbstractC119405dv {
    public C124405pS(AnonymousClass018 r1, WaBloksActivity waBloksActivity) {
        super(r1, waBloksActivity);
    }

    @Override // X.AbstractC119405dv
    public void A03(Intent intent, Bundle bundle) {
        A00().A0I(this.A01);
    }

    @Override // X.AbstractC119405dv
    public void A04(AbstractC115815Ta r3) {
        try {
            this.A01 = r3.AAL().A0I(48);
            A00().A0I(this.A01);
        } catch (ClassCastException e) {
            Log.e(C12960it.A0b("Bloks: Invalid navigation bar type", e));
        }
    }
}
