package X;

import android.os.HandlerThread;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.60I  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60I {
    public static final long[] A0J = {3, 2, 15};
    public int A00;
    public HandlerThread A01;
    public AnonymousClass6MR A02;
    public HandlerC117455Zz A03;
    public String A04;
    public String A05;
    public final C14900mE A06;
    public final C16590pI A07;
    public final AnonymousClass102 A08;
    public final C17220qS A09;
    public final C1308460e A0A;
    public final C1329668y A0B;
    public final C18650sn A0C;
    public final C64513Fv A0D;
    public final C18610sj A0E;
    public final C17070qD A0F;
    public final AnonymousClass60W A0G;
    public final C18590sh A0H;
    public final AbstractC14440lR A0I;

    public AnonymousClass60I(C14900mE r9, C16590pI r10, AnonymousClass102 r11, C17220qS r12, C119755f3 r13, C1308460e r14, C1329668y r15, C18650sn r16, C18610sj r17, C17070qD r18, AnonymousClass6MR r19, AnonymousClass60W r20, C18590sh r21, AbstractC14440lR r22) {
        this.A06 = r9;
        this.A07 = r10;
        this.A0I = r22;
        this.A09 = r12;
        this.A0H = r21;
        this.A0F = r18;
        this.A0A = r14;
        this.A0E = r17;
        this.A08 = r11;
        this.A0C = r16;
        this.A0B = r15;
        this.A0G = r20;
        this.A0D = r14.A04;
        this.A02 = r19;
        this.A04 = r14.A05(r13);
        this.A05 = r14.A06(r13);
        HandlerThread handlerThread = new HandlerThread("PAY: device binding iq sender");
        this.A01 = handlerThread;
        handlerThread.start();
        this.A03 = new HandlerC117455Zz(this.A01.getLooper(), r13, r14, r15, this, r21, this.A04);
    }

    public void A00() {
        long j;
        Log.i("PAY: IndiaUpiGetBankAccountsAction: delayedDeviceVerifIqHandlerMessage");
        this.A00++;
        HandlerC117455Zz r5 = this.A03;
        r5.removeMessages(0);
        int i = this.A00 - 1;
        long[] jArr = A0J;
        if (i < jArr.length) {
            j = jArr[i];
        } else {
            j = ((long) i) * 5;
        }
        r5.sendEmptyMessageDelayed(0, j * 1000);
    }

    public void A01(C119755f3 r13) {
        String A05;
        Log.i("PAY: sendGetBankAccounts called");
        C64513Fv r10 = this.A0D;
        r10.A04("upi-get-accounts");
        C17220qS r2 = this.A09;
        String A01 = r2.A01();
        C1329668y r3 = this.A0B;
        if (!TextUtils.isEmpty(r3.A07())) {
            A05 = r3.A07();
        } else {
            A05 = this.A0A.A05(r13);
        }
        String A012 = this.A0H.A01();
        long parseLong = Long.parseLong(r13.A0C);
        C117325Zm.A06(r2, new C120615gT(this.A07.A00, this.A06, this.A0C, r10, this), new C126505sz(new AnonymousClass3CS(A01), A012, A05, r13.A0A, parseLong).A00, A01);
    }
}
