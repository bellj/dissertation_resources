package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.5og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124145og extends AbstractC16350or {
    public final C006202y A00;
    public final AnonymousClass6MK A01;
    public final C16590pI A02;
    public final String A03;

    public C124145og(C006202y r1, AnonymousClass6MK r2, C16590pI r3, String str) {
        this.A02 = r3;
        this.A03 = str;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        File file = new File(this.A02.A00.getFilesDir(), C130795zz.A03);
        if (file.exists() || file.mkdirs()) {
            return BitmapFactory.decodeFile(new File(file, this.A03).getAbsolutePath());
        }
        Log.e("BloksImageManager/getBitmap/unable to get images directory");
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap != null) {
            this.A00.A08(this.A03, C12970iu.A10(bitmap));
            this.A01.AWv(bitmap);
            return;
        }
        this.A01.APk();
    }
}
