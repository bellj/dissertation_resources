package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;

/* renamed from: X.6B8  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B8 implements AbstractC136446Mo {
    public final AbstractC30791Yv A00;
    public final AbstractC30791Yv A01;
    public final AnonymousClass63Y A02;
    public final AnonymousClass63Y A03;

    public AnonymousClass6B8(AnonymousClass63Y r2, AnonymousClass63Y r3) {
        this.A03 = r2;
        this.A02 = r3;
        this.A01 = r2.A00;
        this.A00 = r3.A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0085, code lost:
        if (r9 == 5) goto L_0x0087;
     */
    @Override // X.AbstractC136446Mo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass6F2 A9k(X.AnonymousClass6F2 r7, X.C1315863i r8, int r9) {
        /*
            r6 = this;
            X.1Yv r0 = r7.A00
            X.1Yu r0 = (X.AbstractC30781Yu) r0
            java.lang.String r1 = r0.A04
            X.1Yv r0 = r6.A00
            boolean r0 = X.C117305Zk.A1V(r0, r1)
            r2 = 1
            if (r0 == 0) goto L_0x006d
            if (r9 != r2) goto L_0x0069
            r9 = 4
        L_0x0012:
            r5 = 0
            r0 = 4
            if (r9 != r0) goto L_0x0034
            X.63p r0 = r8.A02
            java.math.BigDecimal r1 = r0.A05
            X.63Y r0 = r6.A02
            X.1Yv r0 = r0.A02
            X.6F2 r4 = X.C127695uu.A00(r0, r7, r1, r5)
            X.63p r0 = r8.A05
            java.math.BigDecimal r1 = r0.A05
            X.63Y r0 = r6.A03
            X.1Yv r0 = r0.A02
            X.5uu r3 = new X.5uu
            r3.<init>(r0, r1, r5, r2)
        L_0x002f:
            X.6F2 r7 = r4.A05(r3)
        L_0x0033:
            return r7
        L_0x0034:
            r0 = 2
            if (r9 != r0) goto L_0x005b
            X.63p r0 = r8.A02
            java.math.BigDecimal r1 = r0.A05
            X.63Y r0 = r6.A02
            X.1Yv r0 = r0.A02
            X.6F2 r4 = X.C127695uu.A00(r0, r7, r1, r5)
            X.63p r0 = r8.A05
            java.math.BigDecimal r1 = r0.A05
            X.63Y r3 = r6.A03
            X.1Yv r0 = r3.A02
            X.6F2 r4 = X.C127695uu.A00(r0, r4, r1, r5)
            X.63p r0 = r8.A03
            java.math.BigDecimal r1 = r0.A05
            X.1Yv r0 = r3.A01
            X.5uu r3 = new X.5uu
            r3.<init>(r0, r1, r2, r2)
            goto L_0x002f
        L_0x005b:
            r0 = 5
            if (r9 != r0) goto L_0x0033
            X.63p r0 = r8.A02
            java.math.BigDecimal r1 = r0.A05
            X.63Y r0 = r6.A02
            X.6F2 r7 = X.C127695uu.A01(r7, r0, r1, r5, r2)
            return r7
        L_0x0069:
            if (r9 != 0) goto L_0x0012
            r9 = 5
            goto L_0x0012
        L_0x006d:
            if (r9 == r2) goto L_0x0090
            if (r9 == 0) goto L_0x0087
            r0 = 4
            if (r9 == r0) goto L_0x0033
            r0 = 2
            if (r9 != r0) goto L_0x0084
            X.63p r0 = r8.A03
            java.math.BigDecimal r1 = r0.A05
            X.63Y r0 = r6.A03
            X.1Yv r0 = r0.A01
        L_0x007f:
            X.6F2 r7 = X.C127695uu.A00(r0, r7, r1, r2)
            return r7
        L_0x0084:
            r0 = 5
            if (r9 != r0) goto L_0x0090
        L_0x0087:
            X.63p r0 = r8.A05
            java.math.BigDecimal r1 = r0.A05
            X.63Y r0 = r6.A02
            X.1Yv r0 = r0.A02
            goto L_0x007f
        L_0x0090:
            X.63p r0 = r8.A05
            java.math.BigDecimal r1 = r0.A05
            r0 = 0
            X.63Y r4 = r6.A02
            X.6F2 r3 = X.C127695uu.A01(r7, r4, r1, r2, r0)
            X.63p r0 = r8.A02
            java.math.BigDecimal r1 = r0.A05
            X.1Yv r0 = r4.A01
            X.6F2 r7 = X.C127695uu.A00(r0, r3, r1, r2)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6B8.A9k(X.6F2, X.63i, int):X.6F2");
    }

    @Override // X.AbstractC136446Mo
    public CharSequence AHI(Context context, AnonymousClass018 r9, C1315863i r10) {
        int i = 4;
        BigDecimal A01 = C130275z5.A01(r10.A05.A05, r10.A02.A05);
        AbstractC30791Yv r3 = this.A03.A02;
        Object[] objArr = new Object[2];
        objArr[0] = r3.AAB(r9, BigDecimal.ONE, 2);
        AbstractC30791Yv r1 = this.A02.A01;
        if (BigDecimal.ONE.equals(A01)) {
            i = 0;
        }
        return r3.AA7(context, C12960it.A0X(context, C117305Zk.A0k(r9, r1, A01, i), objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate));
    }

    @Override // X.AbstractC136446Mo
    public boolean AKE(C1315863i r3) {
        return ((AbstractC30781Yu) this.A01).A04.equals(r3.A05.A03) && ((AbstractC30781Yu) this.A00).A04.equals(r3.A02.A04);
    }
}
