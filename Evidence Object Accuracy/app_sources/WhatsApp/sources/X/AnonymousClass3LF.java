package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.whatsapp.util.Log;

/* renamed from: X.3LF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LF implements ServiceConnection {
    public final /* synthetic */ AnonymousClass29U A00;

    public AnonymousClass3LF(AnonymousClass29U r1) {
        this.A00 = r1;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        AnonymousClass29U r2 = this.A00;
        r2.A0Z.set(true);
        AnonymousClass10K r1 = r2.A0I;
        if (!r1.A0B) {
            r1.A03();
        }
        r2.A0X.open();
        Log.i("settings-gdrive/service-connected");
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        AnonymousClass29U r2 = this.A00;
        r2.A0Z.set(false);
        r2.A0X.close();
        Log.i("settings-gdrive/service-disconnected");
    }
}
