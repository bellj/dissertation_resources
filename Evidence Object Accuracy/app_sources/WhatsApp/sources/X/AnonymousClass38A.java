package X;

import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.38A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38A extends AbstractC16350or {
    public int A00;
    public C63373Bi A01;
    public final C15570nT A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final C20710wC A05;
    public final C17220qS A06;
    public final C20660w7 A07;
    public final String A08;
    public final WeakReference A09;

    public AnonymousClass38A(C15570nT r2, AcceptInviteLinkActivity acceptInviteLinkActivity, C15550nR r4, C15610nY r5, C20710wC r6, C17220qS r7, C20660w7 r8, String str) {
        super(acceptInviteLinkActivity);
        this.A02 = r2;
        this.A07 = r8;
        this.A06 = r7;
        this.A03 = r4;
        this.A04 = r5;
        this.A05 = r6;
        this.A09 = C12970iu.A10(acceptInviteLinkActivity);
        this.A08 = str;
    }
}
