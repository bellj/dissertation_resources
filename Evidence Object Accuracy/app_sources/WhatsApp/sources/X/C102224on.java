package X;

import android.widget.AbsListView;

/* renamed from: X.4on  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102224on implements AbsListView.OnScrollListener {
    public final /* synthetic */ RunnableC55452iU A00;

    public C102224on(RunnableC55452iU r1) {
        this.A00 = r1;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.A00.A00.A08();
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.A00.A00.A08();
    }
}
