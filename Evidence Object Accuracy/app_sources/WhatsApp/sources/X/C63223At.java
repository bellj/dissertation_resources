package X;

import android.view.View;

/* renamed from: X.3At  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63223At {
    public static void A00(View view, boolean z, boolean z2) {
        int visibility = view.getVisibility();
        if (z) {
            if (visibility != 0) {
                view.setVisibility(0);
                if (z2) {
                    view.startAnimation(C12960it.A0G(0.0f, 1.0f));
                }
            }
        } else if (visibility == 0) {
            if (z2) {
                view.startAnimation(C12960it.A0G(1.0f, 0.0f));
            }
            view.setVisibility(4);
        }
    }
}
