package X;

import com.whatsapp.util.Log;
import java.security.cert.CertificateExpiredException;

/* renamed from: X.3Wv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68813Wv implements AbstractC116705Wm {
    public final /* synthetic */ C68823Ww A00;

    public C68813Wv(C68823Ww r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116705Wm
    public void AP0(Exception exc) {
        AbstractC63833Dc r1 = this.A00.A00;
        Log.e("FBUserEntityManagement : On error response while sending the payload");
        r1.A00.APp(exc);
    }

    @Override // X.AbstractC116705Wm
    public void APp(Exception exc) {
        AbstractC63833Dc r1 = this.A00.A00;
        Log.e("FBUserEntityManagement : On error response while sending the payload");
        r1.A00.APp(exc);
    }

    @Override // X.AbstractC116705Wm
    public void AXB(Integer num, String str, String str2, String str3, String str4, String str5) {
        try {
            C68823Ww r0 = this.A00;
            AnonymousClass17H r2 = r0.A03;
            AnonymousClass17H.A00(r0.A00, r0.A02, r2, num, str2, str3, str4, str5);
        } catch (CertificateExpiredException e) {
            AbstractC63833Dc r1 = this.A00.A00;
            Log.e("FBUserEntityManagement : On error response while sending the payload");
            r1.A00.APp(e);
        }
    }
}
