package X;

import android.content.Context;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.R;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.gifsearch.GifSearchContainer;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.stickers.StickerView;

/* renamed from: X.6Dm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134146Dm implements AnonymousClass5Wu {
    public View.OnFocusChangeListener A00;
    public View A01;
    public View A02;
    public ImageButton A03;
    public ImageButton A04;
    public LinearLayout A05;
    public LinearLayout A06;
    public EmojiSearchContainer A07;
    public GifSearchContainer A08;
    public MentionableEntry A09;
    public C134066De A0A;
    public AnonymousClass1KS A0B;
    public StickerView A0C;
    public Integer A0D;
    public final Context A0E;
    public final TextWatcher A0F = new C123815nz(this);
    public final AnonymousClass01d A0G;
    public final AnonymousClass018 A0H;
    public final AnonymousClass19M A0I;
    public final C14850m9 A0J;
    public final C16630pM A0K;
    public final AnonymousClass1AB A0L;

    public C134146Dm(Context context, AnonymousClass01d r3, AnonymousClass018 r4, AnonymousClass19M r5, C14850m9 r6, C134066De r7, C16630pM r8, AnonymousClass1AB r9) {
        this.A0E = context;
        this.A0J = r6;
        this.A0I = r5;
        this.A0G = r3;
        this.A0H = r4;
        this.A0L = r9;
        this.A0K = r8;
        this.A0A = r7;
    }

    public void A00(AnonymousClass1KS r13, Integer num) {
        this.A06.setVisibility(0);
        AnonymousClass1AB r3 = this.A0L;
        StickerView stickerView = this.A0C;
        Context context = this.A0E;
        r3.A04(stickerView, r13, new AbstractC39661qJ(r13, num) { // from class: X.6Da
            public final /* synthetic */ AnonymousClass1KS A01;
            public final /* synthetic */ Integer A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AbstractC39661qJ
            public final void AWe(boolean z) {
                C134146Dm r32 = C134146Dm.this;
                AnonymousClass1KS r4 = this.A01;
                Integer num2 = this.A02;
                if (z) {
                    r32.A03.setOnClickListener(new C123935oB(r32));
                    r32.A09.setVisibility(8);
                    r32.A05.setVisibility(8);
                    r32.A0B = r4;
                    r32.A0D = num2;
                    r32.A0C.setContentDescription(C28111Kr.A01(r32.A0E, r4));
                    StickerView stickerView2 = r32.A0C;
                    stickerView2.A03 = true;
                    stickerView2.A02();
                    return;
                }
                r32.A06.setVisibility(8);
                r32.A09.setVisibility(0);
                r32.A05.setVisibility(0);
            }
        }, 1, context.getResources().getDimensionPixelSize(R.dimen.payment_compose_sticker_preview_width), context.getResources().getDimensionPixelSize(R.dimen.payment_compose_sticker_preview_height), true, false);
    }

    @Override // X.AnonymousClass5Wu
    public /* bridge */ /* synthetic */ void A6Q(Object obj) {
        this.A09.setText((String) obj);
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.shared_payment_entry;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A05 = C117305Zk.A07(view, R.id.input_layout_content);
        this.A04 = (ImageButton) AnonymousClass028.A0D(view, R.id.emoji_picker_btn);
        this.A09 = (MentionableEntry) AnonymousClass028.A0D(view, R.id.send_payment_note);
        this.A02 = AnonymousClass028.A0D(view, R.id.text_entry_layout);
        this.A08 = (GifSearchContainer) AnonymousClass028.A0D(view, R.id.gif_search_container);
        this.A07 = (EmojiSearchContainer) AnonymousClass028.A0D(view, R.id.emoji_search_container);
        if (this.A0J.A07(811)) {
            LinearLayout A07 = C117305Zk.A07(view, R.id.sticker_preview_layout);
            this.A06 = A07;
            this.A0C = (StickerView) AnonymousClass028.A0D(A07, R.id.sticker_preview);
            this.A03 = (ImageButton) AnonymousClass028.A0D(this.A06, R.id.sticker_remove_cta);
        }
        ViewStub viewStub = (ViewStub) view.findViewById(R.id.payment_entry_action_stub);
        if (viewStub != null) {
            C88094Eg.A00(viewStub, this.A0A);
        } else {
            this.A0A.AYL(AnonymousClass028.A0D(view, R.id.payment_entry_action_inflated));
        }
        this.A01 = AnonymousClass028.A0D(view, R.id.payment_entry_action_inflated);
        this.A09.addTextChangedListener(this.A0F);
        this.A09.setHint(view.getContext().getString(R.string.send_payment_note));
        this.A09.setFilters(new InputFilter[]{new InputFilter.LengthFilter(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)});
        this.A09.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: X.64k
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view2, boolean z) {
                View.OnFocusChangeListener onFocusChangeListener = C134146Dm.this.A00;
                if (onFocusChangeListener != null) {
                    onFocusChangeListener.onFocusChange(view2, z);
                }
            }
        });
        this.A09.addTextChangedListener(new AnonymousClass367(this.A09, C12960it.A0I(view, R.id.counter), this.A0G, this.A0H, this.A0I, this.A0K, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 30, true));
    }
}
