package X;

import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1EV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EV {
    public final C14830m7 A00;
    public final C16120oU A01;
    public final AnonymousClass1Q5 A02;
    public final ConcurrentHashMap A03 = new ConcurrentHashMap();

    public AnonymousClass1EV(C18420sQ r3, C14830m7 r4, C16120oU r5) {
        this.A00 = r4;
        this.A01 = r5;
        AnonymousClass1Q6 r1 = new AnonymousClass1Q6(154475307);
        r1.A03 = false;
        r1.A02 = false;
        this.A02 = r3.A00(r1, "IqPerfTracker");
    }
}
