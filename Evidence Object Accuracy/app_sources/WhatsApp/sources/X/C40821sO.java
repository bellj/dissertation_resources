package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.1sO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40821sO extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40821sO A0R;
    public static volatile AnonymousClass255 A0S;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public long A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;
    public AbstractC27881Jp A09;
    public AbstractC27881Jp A0A;
    public AbstractC27881Jp A0B;
    public AbstractC27881Jp A0C;
    public AbstractC27881Jp A0D;
    public AbstractC27881Jp A0E;
    public AbstractC27881Jp A0F;
    public AbstractC27881Jp A0G;
    public AbstractC41941uP A0H;
    public AnonymousClass1K6 A0I;
    public C43261wh A0J;
    public String A0K = "";
    public String A0L;
    public String A0M = "";
    public String A0N;
    public String A0O;
    public String A0P = "";
    public boolean A0Q;

    static {
        C40821sO r0 = new C40821sO();
        A0R = r0;
        r0.A0W();
    }

    public C40821sO() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A08 = r1;
        this.A0B = r1;
        this.A07 = r1;
        this.A0I = AnonymousClass277.A01;
        this.A0L = "";
        this.A0A = r1;
        this.A09 = r1;
        this.A0E = r1;
        this.A0H = C56822m0.A02;
        this.A0D = r1;
        this.A0C = r1;
        this.A0O = "";
        this.A0G = r1;
        this.A0F = r1;
        this.A0N = "";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        C81603uH r2;
        switch (r16.ordinal()) {
            case 0:
                return A0R;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C40821sO r1 = (C40821sO) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A0P;
                int i2 = r1.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A0P = r8.Afy(str, r1.A0P, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A0M;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A0M = r8.Afy(str2, r1.A0M, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                String str3 = this.A0K;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A0K = r8.Afy(str3, r1.A0K, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                AbstractC27881Jp r3 = this.A08;
                boolean z8 = false;
                if ((i2 & 8) == 8) {
                    z8 = true;
                }
                this.A08 = r8.Afm(r3, r1.A08, z7, z8);
                int i3 = this.A00;
                boolean z9 = false;
                if ((i3 & 16) == 16) {
                    z9 = true;
                }
                long j = this.A05;
                int i4 = r1.A00;
                boolean z10 = false;
                if ((i4 & 16) == 16) {
                    z10 = true;
                }
                this.A05 = r8.Afs(j, r1.A05, z9, z10);
                boolean z11 = false;
                if ((i3 & 32) == 32) {
                    z11 = true;
                }
                int i5 = this.A03;
                boolean z12 = false;
                if ((i4 & 32) == 32) {
                    z12 = true;
                }
                this.A03 = r8.Afp(i5, r1.A03, z11, z12);
                boolean z13 = false;
                if ((i3 & 64) == 64) {
                    z13 = true;
                }
                int i6 = this.A04;
                boolean z14 = false;
                if ((i4 & 64) == 64) {
                    z14 = true;
                }
                this.A04 = r8.Afp(i6, r1.A04, z13, z14);
                boolean z15 = false;
                if ((i3 & 128) == 128) {
                    z15 = true;
                }
                AbstractC27881Jp r32 = this.A0B;
                boolean z16 = false;
                if ((i4 & 128) == 128) {
                    z16 = true;
                }
                this.A0B = r8.Afm(r32, r1.A0B, z15, z16);
                boolean z17 = false;
                if ((this.A00 & 256) == 256) {
                    z17 = true;
                }
                AbstractC27881Jp r4 = this.A07;
                boolean z18 = false;
                if ((r1.A00 & 256) == 256) {
                    z18 = true;
                }
                this.A07 = r8.Afm(r4, r1.A07, z17, z18);
                this.A0I = r8.Afr(this.A0I, r1.A0I);
                int i7 = this.A00;
                boolean z19 = false;
                if ((i7 & 512) == 512) {
                    z19 = true;
                }
                String str4 = this.A0L;
                int i8 = r1.A00;
                boolean z20 = false;
                if ((i8 & 512) == 512) {
                    z20 = true;
                }
                this.A0L = r8.Afy(str4, r1.A0L, z19, z20);
                boolean z21 = false;
                if ((i7 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z21 = true;
                }
                long j2 = this.A06;
                boolean z22 = false;
                if ((i8 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z22 = true;
                }
                this.A06 = r8.Afs(j2, r1.A06, z21, z22);
                boolean z23 = false;
                if ((i7 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z23 = true;
                }
                AbstractC27881Jp r33 = this.A0A;
                boolean z24 = false;
                if ((i8 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z24 = true;
                }
                this.A0A = r8.Afm(r33, r1.A0A, z23, z24);
                this.A0J = (C43261wh) r8.Aft(this.A0J, r1.A0J);
                boolean z25 = false;
                if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z25 = true;
                }
                AbstractC27881Jp r42 = this.A09;
                boolean z26 = false;
                if ((r1.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z26 = true;
                }
                this.A09 = r8.Afm(r42, r1.A09, z25, z26);
                int i9 = this.A00;
                boolean z27 = false;
                if ((i9 & 16384) == 16384) {
                    z27 = true;
                }
                int i10 = this.A02;
                int i11 = r1.A00;
                boolean z28 = false;
                if ((i11 & 16384) == 16384) {
                    z28 = true;
                }
                this.A02 = r8.Afp(i10, r1.A02, z27, z28);
                boolean z29 = false;
                if ((i9 & 32768) == 32768) {
                    z29 = true;
                }
                int i12 = this.A01;
                boolean z30 = false;
                if ((i11 & 32768) == 32768) {
                    z30 = true;
                }
                this.A01 = r8.Afp(i12, r1.A01, z29, z30);
                boolean z31 = false;
                if ((i9 & 65536) == 65536) {
                    z31 = true;
                }
                AbstractC27881Jp r34 = this.A0E;
                boolean z32 = false;
                if ((i11 & 65536) == 65536) {
                    z32 = true;
                }
                this.A0E = r8.Afm(r34, r1.A0E, z31, z32);
                this.A0H = r8.Afq(this.A0H, r1.A0H);
                boolean z33 = false;
                if ((this.A00 & C25981Bo.A0F) == 131072) {
                    z33 = true;
                }
                AbstractC27881Jp r43 = this.A0D;
                boolean z34 = false;
                if ((r1.A00 & C25981Bo.A0F) == 131072) {
                    z34 = true;
                }
                this.A0D = r8.Afm(r43, r1.A0D, z33, z34);
                boolean z35 = false;
                if ((this.A00 & 262144) == 262144) {
                    z35 = true;
                }
                AbstractC27881Jp r44 = this.A0C;
                boolean z36 = false;
                if ((r1.A00 & 262144) == 262144) {
                    z36 = true;
                }
                this.A0C = r8.Afm(r44, r1.A0C, z35, z36);
                int i13 = this.A00;
                boolean z37 = false;
                if ((i13 & 524288) == 524288) {
                    z37 = true;
                }
                boolean z38 = this.A0Q;
                int i14 = r1.A00;
                boolean z39 = false;
                if ((i14 & 524288) == 524288) {
                    z39 = true;
                }
                this.A0Q = r8.Afl(z37, z38, z39, r1.A0Q);
                boolean z40 = false;
                if ((i13 & 1048576) == 1048576) {
                    z40 = true;
                }
                String str5 = this.A0O;
                boolean z41 = false;
                if ((i14 & 1048576) == 1048576) {
                    z41 = true;
                }
                this.A0O = r8.Afy(str5, r1.A0O, z40, z41);
                boolean z42 = false;
                if ((i13 & 2097152) == 2097152) {
                    z42 = true;
                }
                AbstractC27881Jp r35 = this.A0G;
                boolean z43 = false;
                if ((i14 & 2097152) == 2097152) {
                    z43 = true;
                }
                this.A0G = r8.Afm(r35, r1.A0G, z42, z43);
                boolean z44 = false;
                if ((this.A00 & 4194304) == 4194304) {
                    z44 = true;
                }
                AbstractC27881Jp r45 = this.A0F;
                boolean z45 = false;
                if ((r1.A00 & 4194304) == 4194304) {
                    z45 = true;
                }
                this.A0F = r8.Afm(r45, r1.A0F, z44, z45);
                int i15 = this.A00;
                boolean z46 = false;
                if ((i15 & 8388608) == 8388608) {
                    z46 = true;
                }
                String str6 = this.A0N;
                int i16 = r1.A00;
                boolean z47 = false;
                if ((i16 & 8388608) == 8388608) {
                    z47 = true;
                }
                this.A0N = r8.Afy(str6, r1.A0N, z46, z47);
                if (r8 == C463025i.A00) {
                    this.A00 = i15 | i16;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r82.A0A();
                                    this.A00 = 1 | this.A00;
                                    this.A0P = A0A;
                                    break;
                                case 18:
                                    String A0A2 = r82.A0A();
                                    this.A00 |= 2;
                                    this.A0M = A0A2;
                                    break;
                                case 26:
                                    String A0A3 = r82.A0A();
                                    this.A00 |= 4;
                                    this.A0K = A0A3;
                                    break;
                                case 34:
                                    this.A00 |= 8;
                                    this.A08 = r82.A08();
                                    break;
                                case 40:
                                    this.A00 |= 16;
                                    this.A05 = r82.A06();
                                    break;
                                case 48:
                                    this.A00 |= 32;
                                    this.A03 = r82.A02();
                                    break;
                                case 56:
                                    this.A00 |= 64;
                                    this.A04 = r82.A02();
                                    break;
                                case 66:
                                    this.A00 |= 128;
                                    this.A0B = r82.A08();
                                    break;
                                case 74:
                                    this.A00 |= 256;
                                    this.A07 = r82.A08();
                                    break;
                                case 82:
                                    AnonymousClass1K6 r22 = this.A0I;
                                    if (!((AnonymousClass1K7) r22).A00) {
                                        r22 = AbstractC27091Fz.A0G(r22);
                                        this.A0I = r22;
                                    }
                                    r22.add((C57262mk) r82.A09(r12, C57262mk.A04.A0U()));
                                    break;
                                case 90:
                                    String A0A4 = r82.A0A();
                                    this.A00 |= 512;
                                    this.A0L = A0A4;
                                    break;
                                case 96:
                                    this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A06 = r82.A06();
                                    break;
                                case 130:
                                    this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A0A = r82.A08();
                                    break;
                                case 138:
                                    if ((this.A00 & 4096) == 4096) {
                                        r2 = (C81603uH) this.A0J.A0T();
                                    } else {
                                        r2 = null;
                                    }
                                    C43261wh r0 = (C43261wh) r82.A09(r12, C43261wh.A0O.A0U());
                                    this.A0J = r0;
                                    if (r2 != null) {
                                        r2.A04(r0);
                                        this.A0J = (C43261wh) r2.A01();
                                    }
                                    this.A00 |= 4096;
                                    break;
                                case 146:
                                    this.A00 |= DefaultCrypto.BUFFER_SIZE;
                                    this.A09 = r82.A08();
                                    break;
                                case 152:
                                    this.A00 |= 16384;
                                    this.A02 = r82.A02();
                                    break;
                                case 160:
                                    this.A00 |= 32768;
                                    this.A01 = r82.A02();
                                    break;
                                case 170:
                                    this.A00 |= 65536;
                                    this.A0E = r82.A08();
                                    break;
                                case MediaCodecVideoEncoder.MIN_ENCODER_WIDTH /* 176 */:
                                    AbstractC41941uP r36 = this.A0H;
                                    if (!((AnonymousClass1K7) r36).A00) {
                                        r36 = AbstractC27091Fz.A0F(r36);
                                        this.A0H = r36;
                                    }
                                    C56822m0 r37 = (C56822m0) r36;
                                    r37.A02(r37.A00, r82.A02());
                                    break;
                                case 178:
                                    int A04 = r82.A04(r82.A02());
                                    AbstractC41941uP r23 = this.A0H;
                                    if (!((AnonymousClass1K7) r23).A00 && r82.A00() > 0) {
                                        this.A0H = AbstractC27091Fz.A0F(r23);
                                    }
                                    while (r82.A00() > 0) {
                                        C56822m0 r38 = (C56822m0) this.A0H;
                                        r38.A02(r38.A00, r82.A02());
                                    }
                                    r82.A03 = A04;
                                    r82.A0B();
                                    break;
                                case 186:
                                    this.A00 |= C25981Bo.A0F;
                                    this.A0D = r82.A08();
                                    break;
                                case 194:
                                    this.A00 |= 262144;
                                    this.A0C = r82.A08();
                                    break;
                                case 200:
                                    this.A00 |= 524288;
                                    this.A0Q = r82.A0F();
                                    break;
                                case 210:
                                    String A0A5 = r82.A0A();
                                    this.A00 |= 1048576;
                                    this.A0O = A0A5;
                                    break;
                                case 218:
                                    this.A00 |= 2097152;
                                    this.A0G = r82.A08();
                                    break;
                                case 226:
                                    this.A00 |= 4194304;
                                    this.A0F = r82.A08();
                                    break;
                                case 234:
                                    String A0A6 = r82.A0A();
                                    this.A00 |= 8388608;
                                    this.A0N = A0A6;
                                    break;
                                default:
                                    if (A0a(r82, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r13 = new C28971Pt(e2.getMessage());
                        r13.unfinishedMessage = this;
                        throw new RuntimeException(r13);
                    }
                }
                break;
            case 3:
                ((AnonymousClass1K7) this.A0I).A00 = false;
                ((AnonymousClass1K7) this.A0H).A00 = false;
                return null;
            case 4:
                return new C40821sO();
            case 5:
                return new C81963ur();
            case 6:
                break;
            case 7:
                if (A0S == null) {
                    synchronized (C40821sO.class) {
                        if (A0S == null) {
                            A0S = new AnonymousClass255(A0R);
                        }
                    }
                }
                return A0S;
            default:
                throw new UnsupportedOperationException();
        }
        return A0R;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A0P) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 2) == 2) {
            i += CodedOutputStream.A07(2, this.A0M);
        }
        if ((this.A00 & 4) == 4) {
            i += CodedOutputStream.A07(3, this.A0K);
        }
        int i3 = this.A00;
        if ((i3 & 8) == 8) {
            i += CodedOutputStream.A09(this.A08, 4);
        }
        if ((i3 & 16) == 16) {
            i += CodedOutputStream.A06(5, this.A05);
        }
        if ((i3 & 32) == 32) {
            i += CodedOutputStream.A04(6, this.A03);
        }
        if ((i3 & 64) == 64) {
            i += CodedOutputStream.A04(7, this.A04);
        }
        if ((i3 & 128) == 128) {
            i += CodedOutputStream.A09(this.A0B, 8);
        }
        if ((i3 & 256) == 256) {
            i += CodedOutputStream.A09(this.A07, 9);
        }
        for (int i4 = 0; i4 < this.A0I.size(); i4++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A0I.get(i4), 10);
        }
        if ((this.A00 & 512) == 512) {
            i += CodedOutputStream.A07(11, this.A0L);
        }
        int i5 = this.A00;
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i += CodedOutputStream.A05(12, this.A06);
        }
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i += CodedOutputStream.A09(this.A0A, 16);
        }
        if ((i5 & 4096) == 4096) {
            C43261wh r0 = this.A0J;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i += CodedOutputStream.A0A(r0, 17);
        }
        int i6 = this.A00;
        if ((i6 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i += CodedOutputStream.A09(this.A09, 18);
        }
        if ((i6 & 16384) == 16384) {
            i += CodedOutputStream.A04(19, this.A02);
        }
        if ((i6 & 32768) == 32768) {
            i += CodedOutputStream.A04(20, this.A01);
        }
        if ((i6 & 65536) == 65536) {
            i += CodedOutputStream.A09(this.A0E, 21);
        }
        int i7 = 0;
        for (int i8 = 0; i8 < this.A0H.size(); i8++) {
            C56822m0 r02 = (C56822m0) this.A0H;
            r02.A01(i8);
            i7 += CodedOutputStream.A01(r02.A01[i8]);
        }
        int size = i + i7 + (this.A0H.size() << 1);
        int i9 = this.A00;
        if ((i9 & C25981Bo.A0F) == 131072) {
            size += CodedOutputStream.A09(this.A0D, 23);
        }
        if ((i9 & 262144) == 262144) {
            size += CodedOutputStream.A09(this.A0C, 24);
        }
        if ((i9 & 524288) == 524288) {
            size += CodedOutputStream.A00(25);
        }
        if ((i9 & 1048576) == 1048576) {
            size += CodedOutputStream.A07(26, this.A0O);
        }
        int i10 = this.A00;
        if ((i10 & 2097152) == 2097152) {
            size += CodedOutputStream.A09(this.A0G, 27);
        }
        if ((i10 & 4194304) == 4194304) {
            size += CodedOutputStream.A09(this.A0F, 28);
        }
        if ((i10 & 8388608) == 8388608) {
            size += CodedOutputStream.A07(29, this.A0N);
        }
        int A00 = size + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0P);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A0M);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A0K);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A08, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0H(5, this.A05);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0F(6, this.A03);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0F(7, this.A04);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0K(this.A0B, 8);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0K(this.A07, 9);
        }
        for (int i = 0; i < this.A0I.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A0I.get(i), 10);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0I(11, this.A0L);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0H(12, this.A06);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0K(this.A0A, 16);
        }
        if ((this.A00 & 4096) == 4096) {
            C43261wh r0 = this.A0J;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0K(this.A09, 18);
        }
        if ((this.A00 & 16384) == 16384) {
            codedOutputStream.A0F(19, this.A02);
        }
        if ((this.A00 & 32768) == 32768) {
            codedOutputStream.A0F(20, this.A01);
        }
        if ((this.A00 & 65536) == 65536) {
            codedOutputStream.A0K(this.A0E, 21);
        }
        for (int i2 = 0; i2 < this.A0H.size(); i2++) {
            C56822m0 r02 = (C56822m0) this.A0H;
            r02.A01(i2);
            codedOutputStream.A0F(22, r02.A01[i2]);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0K(this.A0D, 23);
        }
        if ((this.A00 & 262144) == 262144) {
            codedOutputStream.A0K(this.A0C, 24);
        }
        if ((this.A00 & 524288) == 524288) {
            codedOutputStream.A0J(25, this.A0Q);
        }
        if ((this.A00 & 1048576) == 1048576) {
            codedOutputStream.A0I(26, this.A0O);
        }
        if ((this.A00 & 2097152) == 2097152) {
            codedOutputStream.A0K(this.A0G, 27);
        }
        if ((this.A00 & 4194304) == 4194304) {
            codedOutputStream.A0K(this.A0F, 28);
        }
        if ((this.A00 & 8388608) == 8388608) {
            codedOutputStream.A0I(29, this.A0N);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
