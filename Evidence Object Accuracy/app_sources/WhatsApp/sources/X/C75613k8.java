package X;

import android.view.View;

/* renamed from: X.3k8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C75613k8 extends AnonymousClass03U {
    public final View A00;
    public final AbstractC16710pd A01 = new AnonymousClass1WL(new C113865Jh(this));
    public final AbstractC16710pd A02 = new AnonymousClass1WL(new C113875Ji(this));
    public final AbstractC16710pd A03 = new AnonymousClass1WL(new C113885Jj(this));
    public final AbstractC16710pd A04 = new AnonymousClass1WL(new C113895Jk(this));
    public final AbstractC16710pd A05 = new AnonymousClass1WL(new C113905Jl(this));

    public C75613k8(View view) {
        super(view);
        this.A00 = view;
    }
}
