package X;

import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1YY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YY {
    public final int A00;
    public final AbstractC15340mz A01;
    public final AnonymousClass1Iv A02;
    public final Set A03 = new HashSet();

    public AnonymousClass1YY(AbstractC15340mz r2, AnonymousClass1Iv r3, int i) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = i;
    }
}
