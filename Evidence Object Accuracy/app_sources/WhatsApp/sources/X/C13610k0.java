package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import java.util.concurrent.CountDownLatch;

/* renamed from: X.0k0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13610k0 implements AbstractC13620k1, AbstractC13630k2, AbstractC13640k3 {
    public final CountDownLatch A00 = new CountDownLatch(1);

    public /* synthetic */ C13610k0(RunnableBRunnable0Shape0S0200000_I0 runnableBRunnable0Shape0S0200000_I0) {
    }

    public C13610k0() {
    }

    @Override // X.AbstractC13620k1
    public final void ANf() {
        this.A00.countDown();
    }

    @Override // X.AbstractC13630k2
    public final void AQA(Exception exc) {
        this.A00.countDown();
    }

    @Override // X.AbstractC13640k3
    public final void AX4(Object obj) {
        this.A00.countDown();
    }
}
