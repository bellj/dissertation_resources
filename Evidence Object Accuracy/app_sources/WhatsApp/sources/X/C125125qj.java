package X;

/* renamed from: X.5qj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125125qj {
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003d, code lost:
        if (r0 != false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C126115sM A00(java.lang.String r5) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 != 0) goto L_0x0015
            int r4 = r5.hashCode()
            java.lang.String r3 = "HDFC"
            java.lang.String r2 = "AXIS"
            java.lang.String r1 = "SBI"
            java.lang.String r0 = "JIO"
            switch(r4) {
                case 73456: goto L_0x0036;
                case 81882: goto L_0x002e;
                case 2023329: goto L_0x0026;
                case 2212537: goto L_0x001e;
                default: goto L_0x0015;
            }
        L_0x0015:
            r1 = 2131232256(0x7f080600, float:1.8080616E38)
        L_0x0018:
            X.5sM r0 = new X.5sM
            r0.<init>(r1)
            return r0
        L_0x001e:
            boolean r0 = r5.equals(r3)
            r1 = 2131231592(0x7f080368, float:1.807927E38)
            goto L_0x003d
        L_0x0026:
            boolean r0 = r5.equals(r2)
            r1 = 2131231155(0x7f0801b3, float:1.8078383E38)
            goto L_0x003d
        L_0x002e:
            boolean r0 = r5.equals(r1)
            r1 = 2131232649(0x7f080789, float:1.8081413E38)
            goto L_0x003d
        L_0x0036:
            boolean r0 = r5.equals(r0)
            r1 = 2131232333(0x7f08064d, float:1.8080772E38)
        L_0x003d:
            if (r0 != 0) goto L_0x0018
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C125125qj.A00(java.lang.String):X.5sM");
    }
}
