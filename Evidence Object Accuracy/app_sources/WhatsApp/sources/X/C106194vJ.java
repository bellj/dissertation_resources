package X;

/* renamed from: X.4vJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106194vJ implements AbstractC72393eW {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final AbstractC65073Ia A04;
    public final Object A05;

    @Override // X.AbstractC72393eW
    public final int ABP() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEr() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEs() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEt() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEu() {
        return 0;
    }

    public C106194vJ(AbstractC65073Ia r1, Object obj, int i, int i2, int i3, int i4) {
        this.A04 = r1;
        this.A02 = i3;
        this.A01 = i4;
        this.A03 = i;
        this.A00 = i2;
        this.A05 = obj;
    }

    @Override // X.AbstractC72393eW
    public final AbstractC72393eW ABK(int i) {
        throw C12970iu.A0f("A MountableLayoutResult has no children");
    }

    @Override // X.AbstractC72393eW
    public int ADK() {
        return this.A00;
    }

    @Override // X.AbstractC72393eW
    public final Object ADo() {
        return this.A05;
    }

    @Override // X.AbstractC72393eW
    public final AbstractC65073Ia AG8() {
        return this.A04;
    }

    @Override // X.AbstractC72393eW
    public int AHm() {
        return this.A03;
    }

    @Override // X.AbstractC72393eW
    public final int AHs(int i) {
        throw C12970iu.A0f("A MountableLayoutResult has no children");
    }

    @Override // X.AbstractC72393eW
    public final int AHt(int i) {
        throw C12970iu.A0f("A MountableLayoutResult has no children");
    }

    @Override // X.AbstractC72393eW
    public final int getHeight() {
        return this.A01;
    }

    @Override // X.AbstractC72393eW
    public final int getWidth() {
        return this.A02;
    }
}
