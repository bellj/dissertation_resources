package X;

import java.util.List;

/* renamed from: X.3wj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83123wj extends AnonymousClass4YR {
    public final String A00;
    public final List A01;

    public C83123wj(List list, char c) {
        if (!list.isEmpty()) {
            this.A01 = list;
            this.A00 = Character.toString(c);
            return;
        }
        throw C82843wH.A00("Empty properties");
    }
}
