package X;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.transition.Fade;
import android.transition.TransitionValues;
import android.view.ViewGroup;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.2ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52322ac extends Fade {
    public final /* synthetic */ float A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ ViewProfilePhoto A03;

    public C52322ac(ViewProfilePhoto viewProfilePhoto, float f, int i, int i2) {
        this.A03 = viewProfilePhoto;
        this.A00 = f;
        this.A02 = i;
        this.A01 = i2;
    }

    public static /* synthetic */ void A00(ObjectAnimator objectAnimator, C52322ac r5, float f, int i, int i2) {
        float A00 = (C12960it.A00(objectAnimator) - f) / (1.0f - f);
        ViewProfilePhoto viewProfilePhoto = r5.A03;
        viewProfilePhoto.getWindow().setStatusBarColor(C016907y.A03(A00, i, -16777216));
        viewProfilePhoto.getWindow().setNavigationBarColor(C016907y.A03(A00, i2, -16777216));
    }

    @Override // android.transition.Fade, android.transition.Visibility, android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        super.captureStartValues(transitionValues);
        float f = this.A00;
        if (f != 0.0f) {
            transitionValues.values.put("android:fade:transitionAlpha", Float.valueOf(f));
        }
    }

    @Override // android.transition.Visibility, android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        int i;
        ObjectAnimator objectAnimator = (ObjectAnimator) super.createAnimator(viewGroup, transitionValues, transitionValues2);
        if (!(objectAnimator == null || (i = this.A02) == 0)) {
            objectAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(objectAnimator, this, this.A00, i, this.A01) { // from class: X.4em
                public final /* synthetic */ float A00;
                public final /* synthetic */ int A01;
                public final /* synthetic */ int A02;
                public final /* synthetic */ ObjectAnimator A03;
                public final /* synthetic */ C52322ac A04;

                {
                    this.A04 = r2;
                    this.A03 = r1;
                    this.A00 = r3;
                    this.A01 = r4;
                    this.A02 = r5;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    C52322ac.A00(this.A03, this.A04, this.A00, this.A01, this.A02);
                }
            });
        }
        return objectAnimator;
    }
}
