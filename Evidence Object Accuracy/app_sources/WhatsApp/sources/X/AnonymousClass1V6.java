package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.1V6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1V6 extends Handler {
    public final /* synthetic */ C230710g A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1V6(Looper looper, C230710g r2) {
        super(looper);
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        if (r3 >= r4.A0C.size()) goto L_0x001e;
     */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r10) {
        /*
        // Method dump skipped, instructions count: 668
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1V6.handleMessage(android.os.Message):void");
    }
}
