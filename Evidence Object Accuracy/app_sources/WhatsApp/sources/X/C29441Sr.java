package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.profilo.mmapbuf.core.Buffer;
import java.io.File;

/* renamed from: X.1Sr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C29441Sr implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C98514io();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public long A06;
    public AnonymousClass1Sg A07;
    public C29461Su A08;
    public Buffer A09;
    public File A0A;
    public Object A0B;
    public Object A0C;
    public String A0D;
    public String A0E;
    public Buffer[] A0F;
    public volatile AnonymousClass1T1 A0G;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C29441Sr() {
    }

    public C29441Sr(AnonymousClass1Sg r5, C29461Su r6, Buffer buffer, File file, Object obj, String str, String str2, Buffer[] bufferArr, int i, int i2, long j, long j2) {
        this.A06 = j;
        this.A0D = str;
        this.A07 = r5;
        this.A01 = i;
        this.A0C = obj;
        this.A0B = null;
        this.A05 = j2;
        this.A02 = i2;
        this.A03 = 0;
        this.A00 = 0;
        this.A04 = -1;
        this.A08 = r6;
        this.A09 = buffer;
        this.A0F = bufferArr;
        this.A0A = file;
        this.A0E = str2;
    }

    public C29441Sr(C29441Sr r19, int i) {
        long j = r19.A06;
        String str = r19.A0D;
        AnonymousClass1Sg r0 = r19.A07;
        int i2 = r19.A01;
        Object obj = r19.A0C;
        Object obj2 = r19.A0B;
        long j2 = r19.A05;
        int i3 = r19.A02;
        int i4 = r19.A03;
        int i5 = r19.A04;
        C29461Su r9 = r19.A08;
        Buffer buffer = r19.A09;
        Buffer[] bufferArr = r19.A0F;
        File file = r19.A0A;
        String str2 = r19.A0E;
        this.A06 = j;
        this.A0D = str;
        this.A07 = r0;
        this.A01 = i2;
        this.A0C = obj;
        this.A0B = obj2;
        this.A05 = j2;
        this.A02 = i3;
        this.A03 = i4;
        this.A00 = i;
        this.A04 = i5;
        this.A08 = r9;
        this.A09 = buffer;
        this.A0F = bufferArr;
        this.A0A = file;
        this.A0E = str2;
    }

    public C29441Sr(Parcel parcel) {
        this.A06 = parcel.readLong();
        this.A0D = parcel.readString();
        this.A01 = parcel.readInt();
        this.A0C = null;
        this.A0B = null;
        this.A05 = parcel.readLong();
        this.A02 = parcel.readInt();
        this.A03 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A04 = parcel.readInt();
        this.A08 = (C29461Su) C29461Su.CREATOR.createFromParcel(parcel);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A06);
        parcel.writeString(this.A0D);
        parcel.writeInt(this.A01);
        parcel.writeLong(this.A05);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A04);
        this.A08.writeToParcel(parcel, i);
    }
}
