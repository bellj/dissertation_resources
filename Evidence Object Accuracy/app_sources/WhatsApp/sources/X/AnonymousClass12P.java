package X;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.12P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12P implements AnonymousClass12Q {
    public final C14900mE A00;
    public final C14830m7 A01;

    @Override // X.AnonymousClass12Q
    public void AbA(Context context, Uri uri, int i) {
    }

    @Override // X.AnonymousClass12Q
    public void AbB(Context context, Uri uri, int i, int i2) {
    }

    public AnonymousClass12P(C14900mE r1, C14830m7 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static Activity A00(Context context) {
        Activity A00 = AbstractC35731ia.A00(context);
        if (A00 != null) {
            return A00;
        }
        throw new IllegalStateException("The Context is not an Activity.");
    }

    public static Activity A01(Context context, Class cls) {
        Activity A00 = A00(context);
        if (cls.isAssignableFrom(A00.getClass())) {
            return A00;
        }
        StringBuilder sb = new StringBuilder("The Context is not assignable from class ");
        sb.append(cls.getSimpleName());
        throw new IllegalStateException(sb.toString());
    }

    public static Activity A02(View view) {
        return A00(view.getContext());
    }

    public static void A03(Context context) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    public static void A04(View view, WindowManager windowManager) {
        int identifier;
        if (Build.VERSION.SDK_INT >= 19 && view != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            int i = 0;
            Resources resources = view.getContext().getResources();
            if (Build.MANUFACTURER.equalsIgnoreCase("Essential Products") && Build.MODEL.equalsIgnoreCase("PH-1") && resources.getConfiguration().orientation == 2 && (identifier = resources.getIdentifier("navigation_bar_height", "dimen", "android")) > 0) {
                i = resources.getDimensionPixelSize(identifier);
            }
            view.getLayoutParams().width = point.x - i;
        }
    }

    public static void A05(Window window) {
        if (Build.VERSION.SDK_INT >= 28) {
            window.getAttributes().layoutInDisplayCutoutMode = 1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if ((r6.getFlags() & 268435456) != 0) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(android.content.Context r5, android.content.Intent r6) {
        /*
            r4 = this;
            android.app.Activity r0 = X.AbstractC35731ia.A00(r5)
            r3 = 0
            if (r0 != 0) goto L_0x0011
            int r2 = r6.getFlags()
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r2 = r2 & r0
            r1 = 0
            if (r2 == 0) goto L_0x0012
        L_0x0011:
            r1 = 1
        L_0x0012:
            java.lang.String r0 = "Need to use activity context or FLAG_ACTIVITY_NEW_TASK flag"
            X.AnonymousClass009.A0B(r0, r1)
            r5.startActivity(r6)     // Catch: ActivityNotFoundException | SecurityException -> 0x001b
            goto L_0x002a
        L_0x001b:
            r1 = move-exception
            java.lang.String r0 = "app/start-activity "
            com.whatsapp.util.Log.e(r0, r1)
            X.0mE r1 = r4.A00
            r0 = 2131886211(0x7f120083, float:1.9406994E38)
            r1.A07(r0, r3)
            return
        L_0x002a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass12P.A06(android.content.Context, android.content.Intent):void");
    }

    public void A07(Context context, Intent intent) {
        A08(context, intent, context.getClass().getSimpleName());
    }

    public void A08(Context context, Intent intent, String str) {
        boolean z = true;
        boolean z2 = false;
        if (context != null) {
            z2 = true;
        }
        AnonymousClass009.A0B("Context must not be null", z2);
        boolean z3 = false;
        if (intent != null) {
            z3 = true;
        }
        AnonymousClass009.A0B("Intent cannot be null to launch the activity", z3);
        if (str == null) {
            z = false;
        }
        AnonymousClass009.A0B("Origin cannot be null", z);
        C35741ib.A00(intent, str);
        A06(context, intent);
    }

    public boolean A09(Activity activity, Intent intent, int i) {
        try {
            activity.startActivityForResult(intent, i);
            return true;
        } catch (ActivityNotFoundException | SecurityException e) {
            Log.e("app/start-activity-for-result ", e);
            this.A00.A07(R.string.activity_not_found, 0);
            return false;
        }
    }

    @Override // X.AnonymousClass12Q
    public void Ab9(Context context, Uri uri) {
        if (uri == null) {
            Log.e("activity-utils/start-activity/uri-is-null");
            return;
        }
        Intent intent = new Intent("android.intent.action.VIEW", uri);
        intent.putExtra("com.android.browser.application_id", context.getPackageName());
        intent.putExtra("create_new_tab", true);
        A06(context, intent);
    }
}
