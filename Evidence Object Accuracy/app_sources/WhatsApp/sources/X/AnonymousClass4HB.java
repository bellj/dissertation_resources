package X;

import android.text.TextUtils;

/* renamed from: X.4HB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4HB {
    public static final String A00;
    public static final String[] A01;

    static {
        String[] strArr = {"reaction", "sender_timestamp"};
        A01 = strArr;
        A00 = TextUtils.join(",", strArr);
    }
}
