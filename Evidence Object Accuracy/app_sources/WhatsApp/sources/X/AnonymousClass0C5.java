package X;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.PowerManager;

/* renamed from: X.0C5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C5 extends AnonymousClass0Q2 {
    public final PowerManager A00;
    public final /* synthetic */ LayoutInflater$Factory2C011505o A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0C5(Context context, LayoutInflater$Factory2C011505o r4) {
        super(r4);
        this.A01 = r4;
        this.A00 = (PowerManager) context.getApplicationContext().getSystemService("power");
    }

    @Override // X.AnonymousClass0Q2
    public int A00() {
        if (Build.VERSION.SDK_INT < 21 || !AnonymousClass0K8.A00(this.A00)) {
            return 1;
        }
        return 2;
    }

    @Override // X.AnonymousClass0Q2
    public IntentFilter A01() {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
        return intentFilter;
    }

    @Override // X.AnonymousClass0Q2
    public void A04() {
        this.A01.A0V(true);
    }
}
