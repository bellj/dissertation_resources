package X;

import android.content.Context;
import android.content.res.Resources;
import android.util.SparseIntArray;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1Gq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27261Gq {
    public static final SparseIntArray A02 = new SparseIntArray();
    public static volatile boolean A03;
    public final AnonymousClass1Tg A00;
    public final AnonymousClass1Te A01;

    public C27261Gq(Context context, Resources resources, AnonymousClass1Te r7, Locale locale) {
        AnonymousClass1Tg r0;
        if ("en".equals(locale.getLanguage())) {
            String str = "en-US";
            try {
                Object A01 = AnonymousClass1Tf.A00.A01(locale.getCountry());
                if (A01 != null) {
                    str = A01;
                }
            } catch (IllegalArgumentException unused) {
            }
            if (str.equals(str)) {
                r0 = null;
                this.A00 = r0;
                this.A01 = r7;
            }
        }
        String A06 = AbstractC27291Gt.A06(locale);
        StringBuilder sb = new StringBuilder("strings_");
        sb.append(A06);
        r0 = A00(context, resources, sb.toString(), locale, false);
        this.A00 = r0;
        this.A01 = r7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x019f, code lost:
        if (r4 == false) goto L_0x01a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
        if (X.C27261Gq.A03 != false) goto L_0x0020;
     */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x017b A[Catch: IOException -> 0x0212, IndexOutOfBoundsException -> 0x0210, all -> 0x029a, TryCatch #6 {all -> 0x029a, blocks: (B:28:0x010d, B:31:0x0128, B:32:0x0137, B:34:0x013e, B:36:0x0145, B:39:0x014f, B:42:0x015c, B:44:0x0164, B:47:0x0172, B:48:0x0175, B:50:0x017b, B:52:0x0195, B:54:0x01a1, B:56:0x01af, B:57:0x01b2, B:59:0x01b8, B:60:0x01cc, B:61:0x01d2, B:63:0x01db, B:66:0x01e6, B:67:0x01ea, B:70:0x01f6, B:72:0x0204, B:91:0x026b, B:94:0x0281), top: B:106:0x0019 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01b8 A[Catch: IOException -> 0x0212, IndexOutOfBoundsException -> 0x0210, all -> 0x029a, TryCatch #6 {all -> 0x029a, blocks: (B:28:0x010d, B:31:0x0128, B:32:0x0137, B:34:0x013e, B:36:0x0145, B:39:0x014f, B:42:0x015c, B:44:0x0164, B:47:0x0172, B:48:0x0175, B:50:0x017b, B:52:0x0195, B:54:0x01a1, B:56:0x01af, B:57:0x01b2, B:59:0x01b8, B:60:0x01cc, B:61:0x01d2, B:63:0x01db, B:66:0x01e6, B:67:0x01ea, B:70:0x01f6, B:72:0x0204, B:91:0x026b, B:94:0x0281), top: B:106:0x0019 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1Tg A00(android.content.Context r17, android.content.res.Resources r18, java.lang.String r19, java.util.Locale r20, boolean r21) {
        /*
        // Method dump skipped, instructions count: 678
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C27261Gq.A00(android.content.Context, android.content.res.Resources, java.lang.String, java.util.Locale, boolean):X.1Tg");
    }

    public static void A01(boolean z) {
        A03 = z;
    }

    public static void A02(int[] iArr) {
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            A02.put(iArr[i], i);
        }
    }

    public String A03(int i) {
        AnonymousClass1Tg r0;
        int i2 = A02.get(i, Integer.MIN_VALUE);
        String str = null;
        if (!(i2 == Integer.MIN_VALUE || (r0 = this.A00) == null)) {
            ConcurrentHashMap concurrentHashMap = r0.A02;
            Integer valueOf = Integer.valueOf(i2);
            str = (String) concurrentHashMap.get(valueOf);
            if (str == null) {
                str = null;
                C29511Tj r02 = r0.A00;
                if (!(r02 == null || (str = r02.A02(i2)) == null)) {
                    concurrentHashMap.put(valueOf, str);
                }
            }
        }
        return str;
    }

    public String A04(long j, int i) {
        AnonymousClass1Tg r1;
        Long valueOf = Long.valueOf(j);
        int i2 = A02.get(i, Integer.MIN_VALUE);
        if (i2 == Integer.MIN_VALUE || (r1 = this.A00) == null) {
            return null;
        }
        return r1.A00(this.A01, valueOf, i2);
    }
}
