package X;

import java.lang.ref.WeakReference;

/* renamed from: X.1Ka  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27991Ka implements Comparable {
    public final int A00;
    public final long A01;
    public final C64253Ev A02;
    public final WeakReference A03;

    public C27991Ka(C64253Ev r2, C28001Kb r3, int i, long j) {
        this.A02 = r2;
        this.A01 = j;
        this.A00 = i;
        this.A03 = new WeakReference(r3);
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C27991Ka r6 = (C27991Ka) obj;
        if (r6 == null) {
            return -1;
        }
        long j = this.A01;
        long j2 = r6.A01;
        if (j >= j2) {
            return j == j2 ? 0 : 1;
        }
        return -1;
    }
}
