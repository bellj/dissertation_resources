package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100134lQ implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30961Zm(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30961Zm[i];
    }
}
