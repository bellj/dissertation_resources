package X;

import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperLinkActivity;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperLinkViewModel;

/* renamed from: X.3du  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72033du extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ IndiaUpiMapperLinkActivity this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72033du(IndiaUpiMapperLinkActivity indiaUpiMapperLinkActivity) {
        super(0);
        this.this$0 = indiaUpiMapperLinkActivity;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return C13000ix.A02(this.this$0).A00(IndiaUpiMapperLinkViewModel.class);
    }
}
