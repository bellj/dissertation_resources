package X;

import android.content.Context;

/* renamed from: X.3il  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74773il extends AnonymousClass0FE {
    public int A00 = 0;

    public C74773il(Context context) {
        super(context);
    }

    @Override // X.AnonymousClass0FE
    public int A05() {
        return this.A00;
    }

    @Override // X.AnonymousClass0FE
    public int A06() {
        return this.A00;
    }

    @Override // X.AnonymousClass0FE
    public int A08(int i, int i2, int i3, int i4, int i5) {
        if (this.A00 == Integer.MIN_VALUE) {
            return (i3 + ((i4 - i3) >> 1)) - (i + ((i2 - i) >> 1));
        }
        return super.A08(i, i2, i3, i4, i5);
    }

    public void A09(String str) {
        int i = 0;
        if (str != null) {
            switch (str.hashCode()) {
                case -1364013995:
                    if (str.equals("center")) {
                        i = Integer.MIN_VALUE;
                        break;
                    } else {
                        return;
                    }
                case 100571:
                    if (str.equals("end")) {
                        this.A00 = 1;
                        return;
                    }
                    return;
                case 109757538:
                    if (str.equals("start")) {
                        this.A00 = -1;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
        this.A00 = i;
    }
}
