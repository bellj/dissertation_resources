package X;

/* renamed from: X.4yd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108184yd implements AbstractC116855Xe {
    public static final C108184yd A00 = new C108184yd();

    public final boolean equals(Object obj) {
        return obj == this || (obj instanceof C108184yd);
    }

    public final int hashCode() {
        Object[] objArr = new Object[9];
        Boolean bool = Boolean.FALSE;
        objArr[0] = bool;
        objArr[1] = bool;
        objArr[2] = null;
        objArr[3] = bool;
        objArr[4] = bool;
        objArr[5] = null;
        objArr[6] = null;
        objArr[7] = null;
        return C12980iv.A0B(null, objArr, 8);
    }
}
