package X;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.Button;

/* renamed from: X.2zV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61162zV extends AbstractC53282dd implements AnonymousClass5X0 {
    public View A00;
    public LinearLayout A01 = ((LinearLayout) AnonymousClass028.A0D(this.A00, R.id.action_view_container));
    public TextEmojiLabel A02;
    public Button A03 = ((Button) AnonymousClass028.A0D(this.A00, R.id.action_view_1));
    public Button A04 = ((Button) AnonymousClass028.A0D(this.A00, R.id.action_view_2));
    public C64433Fn A05;
    public AnonymousClass01d A06;

    @Override // X.AnonymousClass5X0
    public int getType() {
        return 1;
    }

    public C61162zV(Context context) {
        super(context);
        RelativeLayout.inflate(context, R.layout.conversation_block_add_footer_floating, this);
        View findViewById = findViewById(R.id.block_add_footer_floating_content);
        this.A00 = findViewById;
        this.A02 = C12970iu.A0T(findViewById, R.id.header);
    }

    public final void A00(Button button, int i, int i2, int i3, int i4) {
        button.setVisibility(0);
        if (i != 0) {
            C12970iu.A19(getContext(), button, i);
        }
        if (i2 != 0) {
            C12960it.A0r(getContext(), button, i2);
        }
        if (i3 != 0) {
            AnonymousClass23N.A02(button, i3);
        }
        button.setCompoundDrawablesWithIntrinsicBounds(AnonymousClass2GE.A01(getContext(), i4, R.color.neutralButtonTextColor), (Drawable) null, (Drawable) null, (Drawable) null);
    }

    @Override // X.AnonymousClass5X0
    public void AIS() {
        this.A00.setVisibility(8);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    @Override // X.AnonymousClass5X0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AaU(X.AnonymousClass4UC r14) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C61162zV.AaU(X.4UC):void");
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        C12980iv.A1H(this.A01.getViewTreeObserver(), this, 5);
    }

    @Override // X.AnonymousClass5X0
    public void setup(C64433Fn r1) {
        this.A05 = r1;
    }
}
