package X;

import android.content.SharedPreferences;
import java.util.Random;
import java.util.Set;

/* renamed from: X.125  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass125 {
    public SharedPreferences A00;
    public Set A01;
    public Set A02;
    public final C14830m7 A03;
    public final C14850m9 A04;
    public final C16630pM A05;
    public final Random A06;

    public AnonymousClass125(C14830m7 r2, C14850m9 r3, C16630pM r4) {
        Random random = new Random();
        this.A03 = r2;
        this.A04 = r3;
        this.A05 = r4;
        this.A06 = random;
    }
}
