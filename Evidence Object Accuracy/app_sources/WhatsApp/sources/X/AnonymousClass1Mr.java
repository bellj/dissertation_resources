package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.1Mr  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1Mr<E> extends AbstractC17950rf<E> implements List<E>, RandomAccess {
    public static final AnonymousClass28W EMPTY_ITR = new AnonymousClass299(AnonymousClass298.EMPTY, 0);

    @Override // X.AbstractC17950rf
    @Deprecated
    public final AnonymousClass1Mr asList() {
        return this;
    }

    @Override // java.util.List
    @Deprecated
    public final void add(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.List
    @Deprecated
    public final boolean addAll(int i, Collection collection) {
        throw new UnsupportedOperationException();
    }

    public static AnonymousClass1Mr asImmutableList(Object[] objArr) {
        return asImmutableList(objArr, objArr.length);
    }

    public static AnonymousClass1Mr asImmutableList(Object[] objArr, int i) {
        if (i == 0) {
            return of();
        }
        return new AnonymousClass298(objArr, i);
    }

    public static AnonymousClass29A builder() {
        return new AnonymousClass29A();
    }

    public static AnonymousClass1Mr construct(Object... objArr) {
        C28331Mt.checkElementsNotNull(objArr);
        return asImmutableList(objArr);
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @Override // X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            objArr[i + i2] = get(i2);
        }
        return i + size;
    }

    public static AnonymousClass1Mr copyOf(Collection collection) {
        if (!(collection instanceof AbstractC17950rf)) {
            return construct(collection.toArray());
        }
        AnonymousClass1Mr asList = ((AbstractC17950rf) collection).asList();
        return asList.isPartialView() ? asImmutableList(asList.toArray()) : asList;
    }

    public static AnonymousClass1Mr copyOf(Object[] objArr) {
        if (objArr.length == 0) {
            return of();
        }
        return construct((Object[]) objArr.clone());
    }

    @Override // java.util.Collection, java.lang.Object, java.util.List
    public boolean equals(Object obj) {
        return AnonymousClass29B.equalsImpl(this, obj);
    }

    @Override // java.util.Collection, java.lang.Object, java.util.List
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (((i * 31) + get(i2).hashCode()) ^ -1) ^ -1;
        }
        return i;
    }

    @Override // java.util.List
    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return AnonymousClass29B.indexOfImpl(this, obj);
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public AnonymousClass1I5 iterator() {
        return listIterator();
    }

    @Override // java.util.List
    public int lastIndexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        return AnonymousClass29B.lastIndexOfImpl(this, obj);
    }

    @Override // java.util.List
    public AnonymousClass28W listIterator() {
        return listIterator(0);
    }

    @Override // java.util.List
    public AnonymousClass28W listIterator(int i) {
        C28291Mn.A02(i, size());
        if (isEmpty()) {
            return EMPTY_ITR;
        }
        return new AnonymousClass299(this, i);
    }

    public static AnonymousClass1Mr of() {
        return AnonymousClass298.EMPTY;
    }

    public static AnonymousClass1Mr of(Object obj) {
        return construct(obj);
    }

    public static AnonymousClass1Mr of(Object obj, Object obj2) {
        return construct(obj, obj2);
    }

    public static AnonymousClass1Mr of(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        return construct(obj, obj2, obj3, obj4, obj5);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @Override // java.util.List
    @Deprecated
    public final Object remove(int i) {
        throw new UnsupportedOperationException();
    }

    public AnonymousClass1Mr reverse() {
        return size() <= 1 ? this : new AnonymousClass29C(this);
    }

    @Override // java.util.List
    @Deprecated
    public final Object set(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.List
    public AnonymousClass1Mr subList(int i, int i2) {
        C28291Mn.A03(i, i2, size());
        int i3 = i2 - i;
        if (i3 == size()) {
            return this;
        }
        if (i3 == 0) {
            return of();
        }
        return subListUnchecked(i, i2);
    }

    public AnonymousClass1Mr subListUnchecked(int i, int i2) {
        return new AnonymousClass29D(this, i, i2 - i);
    }

    @Override // X.AbstractC17950rf
    public Object writeReplace() {
        return new C28381My(toArray());
    }
}
