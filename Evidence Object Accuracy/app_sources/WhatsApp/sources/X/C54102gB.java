package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2gB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54102gB extends AnonymousClass02L {
    public final AnonymousClass1J7 A00;

    public C54102gB(AnonymousClass1J7 r2) {
        super(AnonymousClass4G5.A00);
        this.A00 = r2;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r3, int i) {
        AbstractC75643kB r32 = (AbstractC75643kB) r3;
        C16700pc.A0E(r32, 0);
        AbstractC87964Ds r1 = (AbstractC87964Ds) A0E(i);
        C16700pc.A0B(r1);
        r32.A08(r1, new AnonymousClass5K5(this));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        int i2 = R.layout.item_avatar_color_selection;
        if (i == 0) {
            i2 = R.layout.item_avatar_pose_selection;
        }
        View inflate = C12960it.A0E(viewGroup).inflate(i2, viewGroup, false);
        if (i == 0) {
            C16700pc.A0B(inflate);
            return new C58692rE(inflate);
        }
        C16700pc.A0B(inflate);
        return new C58682rD(inflate);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        Object A0E = A0E(i);
        if (A0E instanceof AbstractC83923y7) {
            return 0;
        }
        if (A0E instanceof C58722rH) {
            return 1;
        }
        throw C12990iw.A0v();
    }
}
