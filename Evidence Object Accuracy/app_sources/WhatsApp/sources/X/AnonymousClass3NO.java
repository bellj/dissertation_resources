package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;

/* renamed from: X.3NO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NO implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ C36271jb A00;

    public AnonymousClass3NO(C36271jb r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C36271jb r2 = this.A00;
        boolean A00 = C252718t.A00(r2.A01);
        boolean isShowing = r2.A05.isShowing();
        if (A00) {
            if (!isShowing) {
                View view = r2.A00;
                if (view.getVisibility() == 8) {
                    TranslateAnimation translateAnimation = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                    translateAnimation.setDuration(100);
                    view.startAnimation(translateAnimation);
                    view.setVisibility(0);
                }
            }
        } else if (!isShowing) {
            View view2 = r2.A00;
            if (view2.getVisibility() == 0) {
                TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2.setDuration(100);
                view2.startAnimation(translateAnimation2);
                view2.setVisibility(8);
            }
        }
    }
}
