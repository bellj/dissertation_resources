package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.4Xt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92874Xt {
    public boolean A00;
    public final AnonymousClass5Xd A01;
    public final AnonymousClass5QQ A02;
    public final AnonymousClass5SV A03;
    public final AbstractC115805Sz A04;
    public final ArrayDeque A05 = new ArrayDeque();
    public final ArrayDeque A06 = new ArrayDeque();
    public final CopyOnWriteArraySet A07;

    public C92874Xt(Looper looper, AnonymousClass5Xd r4, AnonymousClass5SV r5, AbstractC115805Sz r6, CopyOnWriteArraySet copyOnWriteArraySet) {
        this.A01 = r4;
        this.A07 = copyOnWriteArraySet;
        this.A04 = r6;
        this.A03 = r5;
        this.A02 = new C107914yA(new Handler(looper, new Handler.Callback() { // from class: X.4iI
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                C92874Xt r3 = C92874Xt.this;
                int i = message.what;
                if (i == 0) {
                    Iterator it = r3.A07.iterator();
                    while (it.hasNext()) {
                        AnonymousClass0PK r62 = (AnonymousClass0PK) it.next();
                        AbstractC115805Sz r52 = r3.A04;
                        AnonymousClass5SV r42 = r3.A03;
                        if (!r62.A02 && r62.A01) {
                            AnonymousClass4WQ r1 = r62.A00;
                            r62.A00 = (AnonymousClass4WQ) r52.get();
                            r62.A01 = false;
                            r42.AJ8(r1, r62.A03);
                        }
                        if (((C107914yA) r3.A02).A00.hasMessages(0)) {
                            break;
                        }
                    }
                } else if (i == 1) {
                    r3.A02((AnonymousClass5SU) message.obj, message.arg1);
                    r3.A00();
                    r3.A01();
                    return true;
                }
                return true;
            }
        }));
    }

    public void A00() {
        ArrayDeque arrayDeque = this.A06;
        if (!arrayDeque.isEmpty()) {
            Handler handler = ((C107914yA) this.A02).A00;
            if (!handler.hasMessages(0)) {
                handler.obtainMessage(0).sendToTarget();
            }
            ArrayDeque arrayDeque2 = this.A05;
            boolean z = !arrayDeque2.isEmpty();
            arrayDeque2.addAll(arrayDeque);
            arrayDeque.clear();
            if (!z) {
                while (!arrayDeque2.isEmpty()) {
                    ((Runnable) arrayDeque2.peekFirst()).run();
                    arrayDeque2.removeFirst();
                }
            }
        }
    }

    public void A01() {
        CopyOnWriteArraySet copyOnWriteArraySet = this.A07;
        Iterator it = copyOnWriteArraySet.iterator();
        while (it.hasNext()) {
            AnonymousClass0PK r3 = (AnonymousClass0PK) it.next();
            AnonymousClass5SV r2 = this.A03;
            r3.A02 = true;
            if (r3.A01) {
                r2.AJ8(r3.A00, r3.A03);
            }
        }
        copyOnWriteArraySet.clear();
        this.A00 = true;
    }

    public void A02(AnonymousClass5SU r5, int i) {
        this.A06.add(new RunnableBRunnable0Shape1S0201000_I1(r5, new CopyOnWriteArraySet(this.A07), i, 0));
    }
}
