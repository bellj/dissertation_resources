package X;

import com.whatsapp.R;
import com.whatsapp.qrcode.contactqr.QrScanCodeFragment;

/* renamed from: X.2iJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55382iJ extends AnonymousClass077 {
    public final /* synthetic */ AnonymousClass34P A00;

    public C55382iJ(AnonymousClass34P r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass077, X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
        AnonymousClass34P r2 = this.A00;
        boolean z = true;
        if (i != C28141Kv.A01(r2.A0G) && f == 0.0f) {
            z = false;
        }
        if (r2.A0W != z) {
            r2.A0W = z;
            if (z) {
                AnonymousClass34P.A02(r2);
                return;
            }
            QrScanCodeFragment qrScanCodeFragment = r2.A0S;
            qrScanCodeFragment.A02.A0J(qrScanCodeFragment.A0E, 200);
            qrScanCodeFragment.A02.A0G(qrScanCodeFragment.A0D);
        }
    }

    @Override // X.AnonymousClass077, X.AnonymousClass070
    public void ATQ(int i) {
        AnonymousClass34P r3 = this.A00;
        r3.A0b();
        C53742f9 r4 = r3.A0Q;
        int i2 = 0;
        do {
            r4.A00[i2].A00.setSelected(C12960it.A1V(i2, i));
            i2++;
        } while (i2 < 2);
        boolean A01 = C28141Kv.A01(r3.A0G);
        if (i == 0) {
            A01 = !A01;
        } else if (i != 1) {
            return;
        }
        if (!A01) {
            C41691tw.A04(r3, R.color.lightNavigationBarBackgroundColor, 1);
        } else if (A01) {
            C41691tw.A04(r3, R.color.black, 2);
            if (!r3.A0W) {
                r3.A0W = true;
                AnonymousClass34P.A02(r3);
            }
            if (!((ActivityC13810kN) r3).A07.A0B()) {
                ((ActivityC13810kN) r3).A05.A07(R.string.no_internet_message, 1);
            }
        }
    }
}
