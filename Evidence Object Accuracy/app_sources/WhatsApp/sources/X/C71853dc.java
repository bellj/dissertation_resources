package X;

import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoActivity;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel;

/* renamed from: X.3dc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71853dc extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AvatarProfilePhotoActivity this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71853dc(AvatarProfilePhotoActivity avatarProfilePhotoActivity) {
        super(0);
        this.this$0 = avatarProfilePhotoActivity;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        AnonymousClass015 A00 = C13000ix.A02(this.this$0).A00(AvatarProfilePhotoViewModel.class);
        C16700pc.A0B(A00);
        return A00;
    }
}
