package X;

/* renamed from: X.43W  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43W extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public String A02;

    public AnonymousClass43W() {
        super(3560, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A02);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStatusReplyReceived {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusMediaType", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusPosterJid", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusReplyMessageType", C12960it.A0Y(this.A01));
        return C12960it.A0d("}", A0k);
    }
}
