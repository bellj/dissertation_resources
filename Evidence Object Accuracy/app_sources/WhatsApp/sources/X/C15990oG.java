package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.facebook.simplejni.NativeHolder;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.locks.Lock;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.0oG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15990oG {
    public C31201aA A00;
    public C31211aB A01;
    public final C22820zh A02;
    public final C15570nT A03;
    public final C15450nH A04;
    public final AnonymousClass13T A05;
    public final C29251Rl A06;
    public final C16040oL A07;
    public final C31161a6 A08;
    public final AnonymousClass1RM A09;
    public final C29291Rp A0A;
    public final AnonymousClass1RX A0B;
    public final AnonymousClass1RP A0C;
    public final C14830m7 A0D;
    public final C14820m6 A0E;
    public final C21100wr A0F;
    public final C31181a8 A0G;
    public final C31171a7 A0H = new C31171a7();
    public final C18240s8 A0I;
    public final AnonymousClass16Z A0J;
    public final AnonymousClass1RO A0K;
    public final C14850m9 A0L;
    public final JniBridge A0M;

    public C15990oG(AbstractC15710nm r27, C22820zh r28, C15570nT r29, C15450nH r30, AnonymousClass13T r31, C14830m7 r32, C16590pI r33, C14820m6 r34, C21100wr r35, C18240s8 r36, AnonymousClass16Z r37, C231410n r38, C14850m9 r39, JniBridge jniBridge) {
        AnonymousClass1RO r2 = new AnonymousClass1RO(r27, r32, r33, r38);
        AnonymousClass1RM r8 = new AnonymousClass1RM(r30, r32, r2);
        AnonymousClass1RX r10 = new AnonymousClass1RX(r32, r2);
        C29291Rp r9 = new C29291Rp(r32, r2);
        C16040oL r6 = new C16040oL(r32, r2);
        AnonymousClass1RP r11 = new AnonymousClass1RP(r32, r2);
        C31161a6 r7 = new C31161a6(r32, r2);
        C29251Rl r5 = new C29251Rl(r32, r2);
        C31181a8 r1 = new C31181a8(this);
        this.A0G = r1;
        this.A0D = r32;
        this.A0L = r39;
        this.A0M = jniBridge;
        this.A03 = r29;
        this.A0K = r2;
        this.A04 = r30;
        this.A0J = r37;
        this.A0F = r35;
        this.A0I = r36;
        this.A02 = r28;
        this.A09 = r8;
        this.A0B = r10;
        this.A05 = r31;
        this.A0E = r34;
        this.A0A = r9;
        this.A07 = r6;
        this.A0C = r11;
        this.A08 = r7;
        this.A06 = r5;
        this.A00 = new C31201aA(new C31191a9(r30, r5, r6, r8, r9, r10, r11, this, r36), r32, r36);
        this.A01 = new C31211aB(r30, r5, r6, r7, r8, r9, r10, r11, r32, this, r36, jniBridge);
        r2.A00 = r1;
    }

    public static C29211Rh A00(C29181Re r6) {
        C29191Rf r5 = r6.A00;
        int i = r5.A01;
        return new C29211Rh(new byte[]{(byte) (i >> 16), (byte) (i >> 8), (byte) i}, r6.A00().A01.A01, r5.A05.A04());
    }

    public static C29211Rh A01(byte[] bArr, int i) {
        byte[] A04 = ((C31281aI) AbstractC27091Fz.A0E(C31281aI.A04, bArr)).A03.A04();
        int length = A04.length - 1;
        byte[] bArr2 = new byte[length];
        System.arraycopy(A04, 1, bArr2, 0, length);
        return new C29211Rh(new byte[]{(byte) (i >> 16), (byte) (i >> 8), (byte) i}, bArr2, null);
    }

    public static String A02(String str, List list) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-512");
            byte[] A06 = A06(list);
            byte b = (byte) 0;
            byte[] A05 = C16050oM.A05(new byte[]{b, b}, A06, str.getBytes());
            for (int i = 0; i < 5200; i++) {
                instance.update(A05);
                A05 = instance.digest(A06);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(A03(A05, 0));
            sb.append(A03(A05, 5));
            sb.append(A03(A05, 10));
            sb.append(A03(A05, 15));
            sb.append(A03(A05, 20));
            sb.append(A03(A05, 25));
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static String A03(byte[] bArr, int i) {
        return String.format("%05d", Long.valueOf(((((long) bArr[i + 4]) & 255) | (((((((long) bArr[i]) & 255) << 32) | ((((long) bArr[i + 1]) & 255) << 24)) | ((((long) bArr[i + 2]) & 255) << 16)) | ((((long) bArr[i + 3]) & 255) << 8))) % 100000));
    }

    public static C31271aH A04(byte[] bArr) {
        C31221aC r4 = new C31221aC();
        byte[][] A01 = C31241aE.A01(r4.A02(bArr, new byte[32], "WhisperText".getBytes(), 64), 32, 32);
        return new C31271aH(new C31261aG(r4, A01[1], 0), new C31251aF(r4, A01[0]));
    }

    public static void A05(AnonymousClass1RV r1) {
        if (r1.A01.A00.A05.A04().length == 0) {
            throw new IOException("Alice base key missing from session");
        }
    }

    public static byte[] A06(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((AnonymousClass1JS) it.next()).A00.A00());
        }
        Collections.sort(arrayList, new C31301aK());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            byte[] bArr = (byte[]) it2.next();
            byteArrayOutputStream.write(bArr, 0, bArr.length);
        }
        return byteArrayOutputStream.toByteArray();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:22|(2:98|23)|(4:(11:93|29|96|33|36|(1:38)(1:39)|40|100|41|188|70)|100|41|188)|32|96|33|36|(0)(0)|40) */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0139, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x013a, code lost:
        r0 = new java.lang.StringBuilder("invalid signed prekey returned from server during prekey fetch; address=");
        r0.append(r37);
        com.whatsapp.util.Log.e(r0.toString(), r4);
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0155 A[Catch: all -> 0x0359, TryCatch #3 {all -> 0x0359, blocks: (B:4:0x000d, B:5:0x0011, B:6:0x0016, B:8:0x0024, B:10:0x0035, B:12:0x0041, B:14:0x0047, B:15:0x004f, B:17:0x005d, B:19:0x0061, B:20:0x0069, B:22:0x00d5, B:23:0x00d8, B:25:0x00ea, B:27:0x00fe, B:29:0x0102, B:31:0x0114, B:33:0x0126, B:35:0x013a, B:36:0x014b, B:39:0x0155, B:40:0x015b, B:41:0x0186, B:42:0x0188, B:80:0x034d), top: B:92:0x000a, inners: #4, #6, #7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0189 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A07(X.C15950oC r37, X.C29211Rh r38, X.C29211Rh r39, byte[] r40, byte[] r41, byte r42) {
        /*
        // Method dump skipped, instructions count: 864
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15990oG.A07(X.0oC, X.1Rh, X.1Rh, byte[], byte[], byte):int");
    }

    public C16020oJ A08(AbstractC16010oI r18, C15950oC r19, byte[] bArr) {
        C16020oJ r0;
        AbstractC31431aX r6;
        byte[] A01;
        Lock A00 = this.A0J.A00(r19);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            if (this.A0L.A07(182)) {
                JniBridge jniBridge = this.A01.A0B;
                C31391aT r02 = new C31391aT((NativeHolder) JniBridge.jvidispatchOIOOOO(2, (long) r19.A00, C29201Rg.A01(r19), r18, (NativeHolder) jniBridge.wajContext.get(), bArr));
                JniBridge.getInstance();
                NativeHolder nativeHolder = r02.A00;
                JniBridge.getInstance();
                r0 = new C16020oJ((byte[]) JniBridge.jvidispatchOIO(0, (long) 46, nativeHolder), (int) JniBridge.jvidispatchIIO(1, (long) 47, nativeHolder));
            } else {
                C31581am A05 = this.A00.A05(r19);
                try {
                    try {
                        try {
                            C31591an r7 = new C31591an(bArr);
                            if (r18 != null) {
                                r6 = new C31421aW(r18);
                            } else {
                                r6 = new C31621aq();
                            }
                            synchronized (C31581am.A04) {
                                C31191a9 r4 = A05.A01;
                                C31601ao r3 = A05.A03;
                                if (r4.A07.A0g(C29201Rg.A00(r3))) {
                                    C31611ap A02 = r4.A02(r3);
                                    A01 = A05.A01(r7, A02);
                                    r6.AI3(A01);
                                    r4.A04(r3, A02);
                                } else {
                                    StringBuilder sb = new StringBuilder("No session for: ");
                                    sb.append(r3);
                                    throw new C31551aj(sb.toString());
                                }
                            }
                            r0 = C31201aA.A00(null, A01, 0);
                        } catch (C31571al e) {
                            r0 = C31201aA.A00(e, null, -1007);
                        }
                    } catch (C31521ag e2) {
                        r0 = C31201aA.A00(e2, null, -1005);
                    }
                } catch (C31531ah e3) {
                    r0 = C31201aA.A00(e3, null, -1001);
                } catch (C31551aj e4) {
                    r0 = C31201aA.A00(e4, null, -1008);
                }
            }
            return r0;
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:74:0x02ba A[Catch: all -> 0x0354, TryCatch #7 {, blocks: (B:25:0x00b3, B:30:0x00f8, B:31:0x00fe, B:33:0x0104, B:37:0x0113, B:40:0x0121, B:42:0x0132, B:43:0x014b, B:44:0x0167, B:46:0x0171, B:47:0x0175, B:49:0x0179, B:51:0x018d, B:52:0x0190, B:53:0x01a0, B:55:0x01bb, B:56:0x01d4, B:57:0x01de, B:59:0x0222, B:60:0x0231, B:61:0x0245, B:63:0x0264, B:66:0x0274, B:68:0x0280, B:70:0x0288, B:71:0x02a1, B:72:0x02a3, B:74:0x02ba, B:75:0x02d0, B:78:0x02d9, B:79:0x02de, B:81:0x02e0, B:82:0x02fe, B:83:0x02ff, B:84:0x0312, B:86:0x0314, B:87:0x0319, B:89:0x031b, B:90:0x033f, B:91:0x0340, B:92:0x0353), top: B:133:0x00b3, inners: #0, #1, #11, #12 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C16020oJ A09(X.AbstractC16010oI r28, X.C15950oC r29, byte[] r30) {
        /*
        // Method dump skipped, instructions count: 939
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15990oG.A09(X.0oI, X.0oC, byte[]):X.0oJ");
    }

    public C16020oJ A0A(AbstractC16010oI r15, C15980oF r16, byte[] bArr) {
        C16020oJ r0;
        byte[] doFinal;
        Lock A01 = this.A0J.A01(r16);
        try {
            if (A01 == null) {
                this.A0I.A00();
            } else {
                A01.lock();
            }
            if (this.A0L.A07(188)) {
                JniBridge jniBridge = this.A01.A0B;
                String str = r16.A01;
                C15950oC r02 = r16.A00;
                C31391aT r03 = new C31391aT((NativeHolder) JniBridge.jvidispatchOIOOOOO(1, (long) r02.A00, str, C29201Rg.A01(r02), r15, (NativeHolder) jniBridge.wajContext.get(), bArr));
                JniBridge.getInstance();
                NativeHolder nativeHolder = r03.A00;
                JniBridge.getInstance();
                r0 = new C16020oJ((byte[]) JniBridge.jvidispatchOIO(0, (long) 46, nativeHolder), (int) JniBridge.jvidispatchIIO(1, (long) 47, nativeHolder));
            } else {
                C31411aV r04 = new C31411aV(this.A00.A00.A01, C29201Rg.A02(r16));
                try {
                    try {
                        try {
                            C31421aW r11 = new C31421aW(r15);
                            synchronized (C31411aV.A02) {
                                try {
                                    C31401aU r10 = r04.A00;
                                    C31351aP r9 = r04.A01;
                                    C29271Rn A00 = r10.A00(r9);
                                    LinkedList linkedList = A00.A00;
                                    if (!linkedList.isEmpty()) {
                                        C31441aY r5 = new C31441aY(bArr);
                                        int i = r5.A01;
                                        Iterator it = linkedList.iterator();
                                        while (it.hasNext()) {
                                            C31451aZ r6 = (C31451aZ) it.next();
                                            C31461aa r1 = r6.A00;
                                            if (r1.A01 == i) {
                                                C31471ab r05 = r1.A03;
                                                if (r05 == null) {
                                                    r05 = C31471ab.A03;
                                                }
                                                r5.A00(C31481ac.A00(r05.A02.A04()));
                                                int i2 = r5.A00;
                                                C31501ae r12 = new C31501ae(r6.A00.A02);
                                                int i3 = r12.A00;
                                                if (i3 <= i2) {
                                                    if (i3 < i2) {
                                                        r12 = r12.A02(i2 - i3);
                                                    }
                                                    r6.A00(r12.A02(1));
                                                    byte[][] A03 = r12.A03();
                                                    C31511af r13 = new C31511af(r12.A00, C31501ae.A01(A03[A03.length - 1], (byte) 1));
                                                    byte[] bArr2 = r13.A02;
                                                    byte[] bArr3 = r13.A01;
                                                    byte[] bArr4 = r5.A02;
                                                    try {
                                                        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr2);
                                                        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
                                                        instance.init(2, new SecretKeySpec(bArr3, "AES"), ivParameterSpec);
                                                        doFinal = instance.doFinal(bArr4);
                                                        r11.AI3(doFinal);
                                                        r10.A01(r9, A00);
                                                    } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
                                                        throw new AssertionError(e);
                                                    } catch (BadPaddingException | IllegalBlockSizeException e2) {
                                                        throw new C31521ag(e2);
                                                    }
                                                } else {
                                                    StringBuilder sb = new StringBuilder("Received message with old counter: ");
                                                    sb.append(i3);
                                                    sb.append(" , ");
                                                    sb.append(i2);
                                                    throw new C31531ah(sb.toString());
                                                }
                                            }
                                        }
                                        StringBuilder sb2 = new StringBuilder("No keys for: ");
                                        sb2.append(i);
                                        throw new C31541ai(sb2.toString());
                                    }
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("No sender key for: ");
                                    sb3.append(r9);
                                    throw new C31551aj(sb3.toString());
                                } catch (C31541ai | C31561ak e3) {
                                    throw new C31521ag(e3);
                                }
                            }
                            r0 = C31201aA.A00(null, doFinal, 0);
                        } catch (C31571al e4) {
                            r0 = C31201aA.A00(e4, null, -1007);
                        }
                    } catch (C31531ah e5) {
                        r0 = C31201aA.A00(e5, null, -1001);
                    }
                } catch (C31521ag e6) {
                    r0 = C31201aA.A00(e6, null, -1005);
                } catch (C31551aj e7) {
                    r0 = C31201aA.A00(e7, null, -1008);
                }
            }
            return r0;
        } finally {
            if (A01 != null) {
                A01.unlock();
            }
        }
    }

    public C31801b8 A0B(C15950oC r28, byte[] bArr) {
        C31801b8 r2;
        Cipher cipher;
        AbstractC31681aw r12;
        AbstractC31371aR r18;
        int i;
        Lock A00 = this.A0J.A00(r28);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            if (this.A0L.A07(187)) {
                C31791b7 r0 = new C31791b7((NativeHolder) JniBridge.jvidispatchOIOOO(0, (long) r28.A00, C29201Rg.A01(r28), (NativeHolder) this.A01.A0B.wajContext.get(), bArr));
                JniBridge.getInstance();
                NativeHolder nativeHolder = r0.A00;
                JniBridge.getInstance();
                JniBridge.getInstance();
                r2 = new C31801b8((byte[]) JniBridge.jvidispatchOIO(0, (long) 43, nativeHolder), (int) JniBridge.jvidispatchIIO(1, (long) 44, nativeHolder), (int) JniBridge.jvidispatchIIO(1, (long) 45, nativeHolder));
            } else {
                C31201aA r9 = this.A00;
                C31581am A05 = r9.A05(r28);
                byte[] A03 = C31201aA.A03(bArr);
                try {
                    synchronized (C31581am.A04) {
                        C31191a9 r6 = A05.A01;
                        C31601ao r5 = A05.A03;
                        C31611ap A02 = r6.A02(r5);
                        C31661au r3 = A02.A01;
                        C31261aG A04 = r3.A04();
                        C31831bB A002 = A04.A00();
                        C31491ad A032 = r3.A03();
                        C31321aM r02 = r3.A00;
                        int i2 = r02.A02;
                        int i3 = r02.A04;
                        if (i3 == 0) {
                            i3 = 2;
                        }
                        try {
                            if (i3 >= 3) {
                                SecretKeySpec secretKeySpec = A002.A02;
                                IvParameterSpec ivParameterSpec = A002.A01;
                                try {
                                    cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                                    cipher.init(1, secretKeySpec, ivParameterSpec);
                                } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
                                    throw new AssertionError(e);
                                }
                            } else {
                                cipher = C31581am.A00(A002.A02, 1, A002.A00);
                            }
                            byte[] doFinal = cipher.doFinal(A03);
                            SecretKeySpec secretKeySpec2 = A002.A03;
                            int i4 = A04.A00;
                            C31591an r182 = new C31591an(secretKeySpec2, r3.A01(), r3.A02(), A032, doFinal, i3, i4, i2);
                            C31321aM r122 = r3.A00;
                            r18 = r182;
                            if ((r122.A00 & 128) == 128) {
                                try {
                                    C31841bC r13 = r122.A0C;
                                    if (r13 == null) {
                                        r13 = C31841bC.A04;
                                    }
                                    if ((r13.A00 & 1) == 1) {
                                        r12 = new C31691ax(Integer.valueOf(r13.A01));
                                    } else {
                                        r12 = C31671av.A00;
                                    }
                                    C31851bD r14 = new C31851bD(C31481ac.A00(r13.A03.A04()), r12, r13.A02);
                                    int i5 = r3.A00.A01;
                                    r18 = new C31631ar(r3.A01(), r14.A01, r182, r14.A02, i3, i5, r14.A00);
                                } catch (C31561ak e2) {
                                    throw new AssertionError(e2);
                                }
                            }
                            C31261aG r10 = new C31261aG(A04.A01, A04.A01(C31261aG.A03), i4 + 1);
                            C31871bF r22 = (C31871bF) C31861bE.A03.A0T();
                            byte[] bArr2 = r10.A02;
                            r22.A06(AbstractC27881Jp.A01(bArr2, 0, bArr2.length));
                            r22.A05(r10.A00);
                            C31861bE r1 = (C31861bE) r22.A02();
                            C31881bG r03 = r3.A00.A0A;
                            if (r03 == null) {
                                r03 = C31881bG.A05;
                            }
                            C31891bH r04 = (C31891bH) r03.A0T();
                            r04.A05(r1);
                            C31901bI r23 = (C31901bI) r3.A00.A0T();
                            r23.A03();
                            C31321aM r15 = (C31321aM) r23.A00;
                            r15.A0A = (C31881bG) r04.A02();
                            r15.A00 |= 32;
                            r3.A00 = (C31321aM) r23.A02();
                            r6.A04(r5, A02);
                        } catch (BadPaddingException | IllegalBlockSizeException e3) {
                            throw new AssertionError(e3);
                        }
                    }
                    int type = r18.getType();
                    if (type == 2) {
                        i = 1;
                    } else if (type != 3) {
                        throw new IllegalStateException("SignalMessageType is neither message nor pre_key_message");
                    } else {
                        i = 2;
                    }
                    r2 = new C31801b8(r18.Abf(), i, 0);
                } catch (IllegalArgumentException e4) {
                    StringBuilder sb = new StringBuilder("SignalCoordinatorDefault/encryptForIndividual/error encrypting for ");
                    sb.append(r28);
                    Log.e(sb.toString(), e4);
                    if (!r9.A00.A07.A0g(C29201Rg.A00(new C31601ao(C29201Rg.A01(r28), r28.A00)))) {
                        r2 = new C31801b8(null, 0, -1008);
                    } else {
                        throw e4;
                    }
                }
            }
            return r2;
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public C31801b8 A0C(C15980oF r16, byte[] bArr) {
        C31801b8 r3;
        byte[] bArr2;
        Lock A01 = this.A0J.A01(r16);
        try {
            if (A01 == null) {
                this.A0I.A00();
            } else {
                A01.lock();
            }
            if (this.A0L.A07(189)) {
                JniBridge jniBridge = this.A01.A0B;
                String str = r16.A01;
                C15950oC r0 = r16.A00;
                C31791b7 r02 = new C31791b7((NativeHolder) JniBridge.jvidispatchOIOOOO(1, (long) r0.A00, str, C29201Rg.A01(r0), (NativeHolder) jniBridge.wajContext.get(), bArr));
                JniBridge.getInstance();
                NativeHolder nativeHolder = r02.A00;
                JniBridge.getInstance();
                JniBridge.getInstance();
                r3 = new C31801b8((byte[]) JniBridge.jvidispatchOIO(0, (long) 43, nativeHolder), (int) JniBridge.jvidispatchIIO(1, (long) 44, nativeHolder), (int) JniBridge.jvidispatchIIO(1, (long) 45, nativeHolder));
            } else {
                C31401aU r2 = this.A00.A00.A01;
                new C31811b9(r2).A00(C29201Rg.A02(r16));
                C31411aV r03 = new C31411aV(r2, C29201Rg.A02(r16));
                byte[] A03 = C31201aA.A03(bArr);
                try {
                    synchronized (C31411aV.A02) {
                        try {
                            C31401aU r7 = r03.A00;
                            C31351aP r6 = r03.A01;
                            C29271Rn A00 = r7.A00(r6);
                            C31451aZ A002 = A00.A00();
                            C31501ae r04 = new C31501ae(A002.A00.A02);
                            byte[][] A032 = r04.A03();
                            C31511af r11 = new C31511af(r04.A00, C31501ae.A01(A032[A032.length - 1], (byte) 1));
                            byte[] bArr3 = r11.A02;
                            byte[] bArr4 = r11.A01;
                            try {
                                IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr3);
                                Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
                                instance.init(1, new SecretKeySpec(bArr4, "AES"), ivParameterSpec);
                                byte[] doFinal = instance.doFinal(A03);
                                C31461aa r05 = A002.A00;
                                int i = r05.A01;
                                int i2 = r11.A00;
                                C31471ab r06 = r05.A03;
                                if (r06 == null) {
                                    r06 = C31471ab.A03;
                                }
                                C31441aY r32 = new C31441aY(new C31721b0(r06.A01.A04()), doFinal, i, i2);
                                A002.A00(new C31501ae(A002.A00.A02).A02(1));
                                r7.A01(r6, A00);
                                bArr2 = r32.A03;
                            } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
                                throw new AssertionError(e);
                            }
                        } catch (C31541ai e2) {
                            throw new C31551aj(e2);
                        }
                    }
                    r3 = new C31801b8(bArr2, 4, 0);
                } catch (C31551aj unused) {
                    r3 = new C31801b8(null, 0, -1008);
                }
            }
            return r3;
        } finally {
            if (A01 != null) {
                A01.unlock();
            }
        }
    }

    public C31911bJ A0D(String str, String str2, List list, List list2) {
        String str3;
        if (this.A04.A05(AbstractC15460nI.A1C)) {
            C31211aB r0 = this.A01;
            List A00 = r0.A00(list);
            if (A00 == null) {
                str3 = "wamsys/generateFingerprint/local-identity-key-conversion-failed";
            } else {
                List A002 = r0.A00(list2);
                if (A002 == null) {
                    str3 = "wamsys/generateFingerprint/remote-identity-key-conversion-failed";
                } else {
                    NativeHolder nativeHolder = (NativeHolder) JniBridge.jvidispatchOOOOOO(0, str, str2, r0.A0B.wajContext.get(), A00, A002);
                    if (nativeHolder != null) {
                        C31921bK r02 = new C31921bK(nativeHolder);
                        try {
                            JniBridge.getInstance();
                            NativeHolder nativeHolder2 = r02.A00;
                            JniBridge.getInstance();
                            C31931bL r3 = new C31931bL((String) JniBridge.jvidispatchOIO(2, (long) 52, nativeHolder2), (String) JniBridge.jvidispatchOIO(2, (long) 53, nativeHolder2));
                            JniBridge.getInstance();
                            return new C31911bJ(r3, (C31941bM) AbstractC27091Fz.A0E(C31941bM.A04, (byte[]) JniBridge.jvidispatchOIO(0, (long) 51, nativeHolder2)));
                        } catch (C28971Pt unused) {
                            Log.e("wamsys/generateFingerprint/protobuf-parsing-failed");
                            return null;
                        }
                    } else {
                        str3 = "wamsys/generateFingerprint/processing-fingerprints-failed";
                    }
                }
            }
            Log.e(str3);
            return null;
        }
        C31201aA r32 = this.A00;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new AnonymousClass1JS((AnonymousClass1PA) it.next()));
        }
        arrayList.add(r32.A04().A01);
        ArrayList arrayList2 = new ArrayList();
        Iterator it2 = list2.iterator();
        while (it2.hasNext()) {
            arrayList2.add(new AnonymousClass1JS((AnonymousClass1PA) it2.next()));
        }
        C31931bL r33 = new C31931bL(A02(str, arrayList), A02(str2, arrayList2));
        byte[] A06 = A06(arrayList);
        byte[] A062 = A06(arrayList2);
        AnonymousClass1G4 A0T = C31941bM.A04.A0T();
        A0T.A03();
        C31941bM r1 = (C31941bM) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = 0;
        C31961bO r9 = C31961bO.A03;
        AnonymousClass1G4 A0T2 = r9.A0T();
        byte[] bytes = str.getBytes();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bytes, 0, bytes.length);
        A0T2.A03();
        C31961bO r12 = (C31961bO) A0T2.A00;
        r12.A00 |= 2;
        r12.A02 = A01;
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(A06, 0, A06.length);
        A0T2.A03();
        C31961bO r13 = (C31961bO) A0T2.A00;
        r13.A00 |= 1;
        r13.A01 = A012;
        A0T.A03();
        C31941bM r14 = (C31941bM) A0T.A00;
        r14.A02 = (C31961bO) A0T2.A02();
        r14.A00 |= 2;
        AnonymousClass1G4 A0T3 = r9.A0T();
        byte[] bytes2 = str2.getBytes();
        AbstractC27881Jp A013 = AbstractC27881Jp.A01(bytes2, 0, bytes2.length);
        A0T3.A03();
        C31961bO r15 = (C31961bO) A0T3.A00;
        r15.A00 |= 2;
        r15.A02 = A013;
        AbstractC27881Jp A014 = AbstractC27881Jp.A01(A062, 0, A062.length);
        A0T3.A03();
        C31961bO r16 = (C31961bO) A0T3.A00;
        r16.A00 |= 1;
        r16.A01 = A014;
        A0T.A03();
        C31941bM r17 = (C31941bM) A0T.A00;
        r17.A03 = (C31961bO) A0T3.A02();
        r17.A00 |= 4;
        return new C31911bJ(r33, (C31941bM) A0T.A02());
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041 A[DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1JS A0E(X.C15950oC r7) {
        /*
            r6 = this;
            X.0oC r5 = r6.A0J(r7)
            X.16Z r0 = r6.A0J
            java.util.concurrent.locks.Lock r4 = r0.A00(r5)
            if (r4 == 0) goto L_0x000f
            r4.lock()     // Catch: all -> 0x0045
        L_0x000f:
            X.0oL r3 = r6.A07     // Catch: all -> 0x0045
            byte[] r0 = r3.A04(r5)     // Catch: all -> 0x0045
            if (r0 == 0) goto L_0x003e
            X.1PA r1 = X.C15940oB.A01(r0)     // Catch: 1RY -> 0x0021, all -> 0x0045
            X.1JS r0 = new X.1JS     // Catch: 1RY -> 0x0021, all -> 0x0045
            r0.<init>(r1)     // Catch: 1RY -> 0x0021, all -> 0x0045
            goto L_0x003f
        L_0x0021:
            r2 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0045
            r1.<init>()     // Catch: all -> 0x0045
            java.lang.String r0 = "axolotl identity key for "
            r1.append(r0)     // Catch: all -> 0x0045
            r1.append(r5)     // Catch: all -> 0x0045
            java.lang.String r0 = " decoded as invalid"
            r1.append(r0)     // Catch: all -> 0x0045
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0045
            com.whatsapp.util.Log.e(r0, r2)     // Catch: all -> 0x0045
            r3.A03(r5)     // Catch: all -> 0x0045
        L_0x003e:
            r0 = 0
        L_0x003f:
            if (r4 == 0) goto L_0x0044
            r4.unlock()
        L_0x0044:
            return r0
        L_0x0045:
            r0 = move-exception
            if (r4 == 0) goto L_0x004b
            r4.unlock()
        L_0x004b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15990oG.A0E(X.0oC):X.1JS");
    }

    public C31381aS A0F(C15980oF r5) {
        Lock A01 = this.A0J.A01(r5);
        try {
            if (A01 == null) {
                this.A0I.A00();
            } else {
                A01.lock();
            }
            return new C31381aS(new C31341aO(this.A00.A00.A02).A00(C29201Rg.A02(r5)).A04, 0);
        } finally {
            if (A01 != null) {
                A01.unlock();
            }
        }
    }

    public AnonymousClass1RV A0G(C15950oC r4) {
        AnonymousClass1RV A00;
        C21100wr r2 = this.A0F;
        synchronized (r2) {
            A00 = r2.A00(r4);
            if (A00 == null) {
                byte[] A03 = this.A0B.A03(r4);
                if (A03 == null) {
                    A00 = new AnonymousClass1RV();
                    r2.A03(A00, r4);
                } else {
                    try {
                        A00 = new AnonymousClass1RV(A03);
                        A05(A00);
                        r2.A03(A00, r4);
                    } catch (IOException unused) {
                        return A0H(r4);
                    }
                }
            }
        }
        return A00;
    }

    public final AnonymousClass1RV A0H(C15950oC r5) {
        AnonymousClass1RV r0;
        C16310on A02 = this.A0K.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C21100wr r1 = this.A0F;
            synchronized (r1) {
                this.A0B.A01(r5);
                r0 = new AnonymousClass1RV();
                r1.A03(r0, r5);
                A00.A00();
            }
            A00.close();
            A02.close();
            return r0;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public C15950oC A0I() {
        C15570nT r0 = this.A03;
        r0.A08();
        C27481Hq r02 = r0.A04;
        if (r02 != null) {
            return C15940oB.A02(r02);
        }
        return new C15950oC(0, "", 0);
    }

    public final C15950oC A0J(C15950oC r5) {
        C15570nT r1 = this.A03;
        AnonymousClass1MU A02 = r1.A02();
        r1.A08();
        C27631Ih r2 = r1.A05;
        return (A02 == null || r2 == null || !r5.A02.equals(A02.user)) ? r5 : new C15950oC(0, r2.user, r5.A00);
    }

    public C29181Re A0K() {
        C18240s8 r2 = this.A0I;
        WeakReference weakReference = r2.A01;
        if (weakReference != null && Thread.currentThread() == weakReference.get()) {
            return A0L();
        }
        try {
            return (C29181Re) r2.A00.submit(new Callable() { // from class: X.1bQ
                @Override // java.util.concurrent.Callable
                public final Object call() {
                    return C15990oG.this.A0L();
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.w("SignedPreKeyStore/getLatestSignedPrekeyRecord", e);
            throw new RuntimeException(e);
        }
    }

    public final C29181Re A0L() {
        Lock A00 = this.A0J.A00(A0I());
        if (A00 != null) {
            try {
                A00.lock();
            } finally {
                if (A00 != null) {
                    A00.unlock();
                }
            }
        }
        C16310on A01 = this.A0C.A01.get();
        C16330op r11 = A01.A03;
        Cursor A08 = r11.A08("signed_prekeys", null, "prekey_id DESC", "1", new String[]{"prekey_id", "record"}, null);
        if (!A08.moveToNext()) {
            Log.e("no signed prekey record found");
            A08.close();
            A01.close();
        } else {
            int i = A08.getInt(0);
            byte[] blob = A08.getBlob(1);
            A08.close();
            if (i == 16777215) {
                Cursor A082 = r11.A08("signed_prekeys", "prekey_id < ?", "prekey_id DESC", "1", new String[]{"prekey_id", "record"}, new String[]{String.valueOf(8388607)});
                if (A082.moveToNext()) {
                    i = A082.getInt(0);
                    blob = A082.getBlob(1);
                }
                A082.close();
            }
            A01.close();
            StringBuilder sb = new StringBuilder("axolotl retrieved latest signed prekey record successfully; id=");
            sb.append(i);
            Log.i(sb.toString());
            if (blob != null) {
                try {
                    return new C29181Re(blob);
                } catch (IOException e) {
                    Log.e("failed to parse the latest signed prekey record", e);
                    throw new AssertionError(e);
                }
            }
        }
        throw new AssertionError("no signed prekey record found");
    }

    public C29211Rh A0M() {
        Lock A00 = this.A0J.A00(A0I());
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            C29181Re A0K = A0K();
            Log.i("axolotl loaded the latest signed pre key for sending");
            return A00(A0K);
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public C29211Rh A0N() {
        AnonymousClass1RW r4;
        Lock A00 = this.A0J.A00(A0I());
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            C31201aA r1 = this.A00;
            if (!r1.A07()) {
                r1.A06();
            }
            AnonymousClass1RM r3 = r1.A00.A04;
            AnonymousClass1RO r5 = r3.A02;
            C16310on A01 = r5.get();
            C29211Rh r14 = null;
            Cursor A08 = A01.A03.A08("prekeys", "sent_to_server = 0 AND direct_distribution = 0", null, String.valueOf(1), new String[]{"prekey_id", "record"}, null);
            try {
                if (A08.moveToNext()) {
                    r4 = new AnonymousClass1RW(A08.getInt(0), A08.getBlob(1));
                    A08.close();
                    A01.close();
                } else {
                    A08.close();
                    A01.close();
                    r4 = null;
                }
                C29211Rh r9 = null;
                if (r4 != null) {
                    try {
                        int i = r4.A00;
                        r14 = C31191a9.A00(new C31761b4(r4.A01), i);
                        C16310on A02 = r5.A02();
                        ContentValues contentValues = new ContentValues(2);
                        contentValues.put("direct_distribution", Boolean.TRUE);
                        contentValues.put("upload_timestamp", Long.valueOf(r3.A01.A00() / 1000));
                        boolean z = true;
                        if (A02.A03.A00("prekeys", contentValues, "prekey_id=?", new String[]{String.valueOf(i)}) != 1) {
                            z = false;
                        }
                        A02.close();
                        if (!z) {
                            Log.e("Failed to mark key as direct distribution, not sending pre-key with retry receipt");
                        }
                    } catch (IOException e) {
                        StringBuilder sb = new StringBuilder("error reading prekey ");
                        int i2 = r4.A00;
                        sb.append(i2);
                        Log.e(sb.toString(), e);
                        r3.A02(i2);
                    }
                    r9 = r14;
                }
                return r9;
            } catch (Throwable th) {
                if (A08 != null) {
                    try {
                        A08.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00f9 A[DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C29211Rh A0O() {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15990oG.A0O():X.1Rh");
    }

    public Map A0P(Set set) {
        HashSet hashSet;
        HashMap hashMap = new HashMap();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            C15950oC r1 = (C15950oC) it.next();
            hashMap.put(A0J(r1), r1);
        }
        HashMap hashMap2 = new HashMap();
        AnonymousClass16Z r2 = this.A0J;
        Set<C15950oC> keySet = hashMap.keySet();
        if (!r2.A02 || keySet.isEmpty()) {
            hashSet = new HashSet();
        } else {
            hashSet = new HashSet(keySet.size());
            for (C15950oC r0 : keySet) {
                hashSet.add(r2.A00(r0));
            }
        }
        try {
            Iterator it2 = hashSet.iterator();
            while (it2.hasNext()) {
                ((Lock) it2.next()).lock();
            }
            C16040oL r6 = this.A07;
            Set keySet2 = hashMap.keySet();
            HashMap hashMap3 = new HashMap();
            if (!keySet2.isEmpty()) {
                C29841Uw r12 = new C29841Uw(keySet2.toArray(new C15950oC[0]), 100);
                C16310on A01 = r6.A03.get();
                Iterator it3 = r12.iterator();
                while (it3.hasNext()) {
                    C15950oC[] r10 = (C15950oC[]) it3.next();
                    C16330op r9 = A01.A03;
                    int length = r10.length;
                    StringBuilder sb = new StringBuilder("SELECT public_key, timestamp, recipient_id, recipient_type, device_id FROM identities INNER JOIN (SELECT ? AS r, ? AS t, ? AS d");
                    for (int i = 1; i < length; i++) {
                        sb.append(" UNION ALL SELECT ? AS r, ? AS t, ? AS d");
                    }
                    sb.append(") AS joined ON joined.r = identities.recipient_id AND joined.t = identities.recipient_type AND joined.d = identities.device_id");
                    Cursor A09 = r9.A09(sb.toString(), C31971bP.A00(Arrays.asList(r10)));
                    try {
                        int columnIndex = A09.getColumnIndex("public_key");
                        int columnIndex2 = A09.getColumnIndex("timestamp");
                        int columnIndex3 = A09.getColumnIndex("recipient_id");
                        int columnIndex4 = A09.getColumnIndex("recipient_type");
                        int columnIndex5 = A09.getColumnIndex("device_id");
                        while (A09.moveToNext()) {
                            C15950oC r13 = new C15950oC(A09.getInt(columnIndex4), A09.getString(columnIndex3), A09.getInt(columnIndex5));
                            byte[] blob = A09.getBlob(columnIndex);
                            A09.getLong(columnIndex2);
                            hashMap3.put(r13, blob);
                        }
                        A09.close();
                    } catch (Throwable th) {
                        if (A09 != null) {
                            try {
                                A09.close();
                            } catch (Throwable unused) {
                            }
                        }
                        throw th;
                    }
                }
                A01.close();
                for (Object obj : keySet2) {
                    if (!hashMap3.containsKey(obj)) {
                        hashMap3.put(obj, null);
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (Map.Entry entry : hashMap3.entrySet()) {
                C15950oC r7 = (C15950oC) hashMap.get(entry.getKey());
                byte[] bArr = (byte[]) entry.getValue();
                if (bArr != null) {
                    try {
                        hashMap2.put(r7, new AnonymousClass1JS(C15940oB.A01(bArr)));
                    } catch (AnonymousClass1RY e) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("axolotl identity key for ");
                        sb2.append(r7);
                        sb2.append(" decoded as invalid");
                        Log.e(sb2.toString(), e);
                        arrayList.add(r7);
                    }
                }
                hashMap2.put(r7, null);
            }
            if (!arrayList.isEmpty()) {
                C16310on A02 = r6.A03.A02();
                AnonymousClass1Lx A00 = A02.A00();
                try {
                    Iterator it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        r6.A03((C15950oC) it4.next());
                    }
                    A00.A00();
                    A00.close();
                    A02.close();
                } catch (Throwable th2) {
                    try {
                        A00.close();
                    } catch (Throwable unused2) {
                    }
                    throw th2;
                }
            }
            return hashMap2;
        } finally {
            Iterator it5 = hashSet.iterator();
            while (it5.hasNext()) {
                ((Lock) it5.next()).unlock();
            }
        }
    }

    public Set A0Q(List list) {
        HashSet hashSet = new HashSet();
        C21100wr r10 = this.A0F;
        synchronized (r10) {
            Set A01 = r10.A01(list);
            Set A02 = r10.A02(list);
            if (A02.isEmpty()) {
                return A01;
            }
            A01.addAll(A02);
            Cursor A00 = this.A0B.A00(A02);
            int columnIndex = A00.getColumnIndex("record");
            int columnIndex2 = A00.getColumnIndex("recipient_id");
            int columnIndex3 = A00.getColumnIndex("recipient_type");
            int columnIndex4 = A00.getColumnIndex("device_id");
            while (A00.moveToNext()) {
                byte[] blob = A00.getBlob(columnIndex);
                C15950oC r1 = new C15950oC(A00.getInt(columnIndex3), String.valueOf(A00.getLong(columnIndex2)), A00.getInt(columnIndex4));
                try {
                    AnonymousClass1RV r0 = new AnonymousClass1RV(blob);
                    A05(r0);
                    r10.A03(r0, r1);
                    A01.remove(r1);
                } catch (IOException unused) {
                    hashSet.add(r1);
                }
            }
            A00.close();
            r10.A05(A02);
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                A0H((C15950oC) it.next());
            }
            return A01;
        }
    }

    public void A0R() {
        NativeHolder nativeHolder;
        AnonymousClass1RO r3 = this.A0K;
        synchronized (r3) {
            r3.close();
            File databasePath = r3.A02.A00.getDatabasePath(r3.A04);
            AnonymousClass1Tx.A04(databasePath, "axolotl");
            if (databasePath.delete()) {
                StringBuilder sb = new StringBuilder();
                sb.append("deleted ");
                sb.append(databasePath);
                Log.i(sb.toString());
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("failed to delete ");
                sb2.append(databasePath);
                Log.e(sb2.toString());
            }
        }
        if (this.A04.A05(AbstractC15460nI.A1B) && (nativeHolder = (NativeHolder) this.A0M.wajContext.getAndSet(JniBridge.jvidispatchO(0))) != null) {
            nativeHolder.release();
        }
        Map map = this.A0F.A01;
        synchronized (map) {
            map.clear();
        }
    }

    public void A0S() {
        Lock A00 = this.A0J.A00(A0I());
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            if (this.A04.A05(AbstractC15460nI.A1B)) {
                C31211aB r6 = this.A01;
                int A002 = r6.A04.A00();
                int A02 = r6.A00.A02(AbstractC15460nI.A1K);
                if (A002 < A02) {
                    int i = A02 - A002;
                    while (true) {
                        if (i <= 0) {
                            break;
                        }
                        int min = Math.min(i, 50);
                        StringBuilder sb = new StringBuilder("wa-msys generating ");
                        sb.append(min);
                        sb.append(" new prekeys and recording them in the db");
                        Log.i(sb.toString());
                        int jvidispatchIIO = (int) JniBridge.jvidispatchIIO(4, (long) min, (NativeHolder) r6.A0B.wajContext.get());
                        if (jvidispatchIIO <= 0) {
                            StringBuilder sb2 = new StringBuilder("wa-msys generated 0 keys when expected to generate ");
                            sb2.append(min);
                            sb2.append(" keys");
                            Log.e(sb2.toString());
                            break;
                        }
                        i -= jvidispatchIIO;
                    }
                } else {
                    StringBuilder sb3 = new StringBuilder("skipping key generation because already more than ");
                    sb3.append(A02);
                    sb3.append(" are unsent");
                    Log.i(sb3.toString());
                }
            } else {
                this.A00.A06();
            }
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public void A0T() {
        Lock A00 = this.A0J.A00(A0I());
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            C31191a9 r1 = this.A00.A00;
            r1.A08.A00();
            AnonymousClass1RO r7 = r1.A04.A02;
            C16310on A02 = r7.A02();
            AnonymousClass1Lx A002 = A02.A00();
            ContentValues contentValues = new ContentValues();
            contentValues.put("sent_to_server", Boolean.FALSE);
            contentValues.put("upload_timestamp", (Long) 0L);
            A02.A03.A00("prekeys", contentValues, "sent_to_server != 0", null);
            Log.i("axolotl recorded no prekeys as received by server");
            C16310on A022 = r7.A02();
            try {
                int A01 = A022.A03.A01("prekey_uploads", null, null);
                StringBuilder sb = new StringBuilder();
                sb.append("axolotl deleted prekey upload timestamps:");
                sb.append(A01);
                Log.i(sb.toString());
                A022.close();
                A002.A00();
                A002.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A022.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public void A0U(AnonymousClass1JS r3, C15950oC r4) {
        DeviceJid A03 = C15940oB.A03(r4);
        C15570nT r1 = this.A03;
        if (!r1.A0E(A03)) {
            A0V(r3, r4);
            return;
        }
        r1.A08();
        Log.e("SignalCoordinator/saveIdentity - Not allowed to save my companion identity as primary");
    }

    public final void A0V(AnonymousClass1JS r10, C15950oC r11) {
        byte[] bArr;
        Lock A00 = this.A0J.A00(r11);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            AnonymousClass1JS A0E = A0E(r11);
            C16040oL r2 = this.A07;
            if (r10 != null) {
                bArr = r10.A00.A00();
            } else {
                bArr = null;
            }
            C16310on A02 = r2.A03.A02();
            ContentValues contentValues = new ContentValues();
            contentValues.put("recipient_id", r11.A02);
            contentValues.put("recipient_type", Integer.valueOf(r11.A01));
            contentValues.put("device_id", Integer.valueOf(r11.A00));
            if (bArr != null) {
                contentValues.put("public_key", bArr);
            } else {
                contentValues.putNull("public_key");
            }
            contentValues.put("timestamp", Long.valueOf(r2.A02.A00() / 1000));
            long A05 = A02.A03.A05(contentValues, "identities");
            StringBuilder sb = new StringBuilder();
            sb.append("axolotl saved identity for ");
            sb.append(r11);
            sb.append(" with resultant row id ");
            sb.append(A05);
            Log.i(sb.toString());
            A02.close();
            DeviceJid A03 = C15940oB.A03(r11);
            if (A03 != null) {
                if (r10 == null) {
                    if (A0E != null) {
                        C22820zh r1 = this.A02;
                        r1.A00.A00();
                        for (AbstractC27101Ga r0 : r1.A01()) {
                            r0.ARI(A03);
                        }
                    }
                } else if (A0E == null) {
                    C22820zh r12 = this.A02;
                    r12.A00.A00();
                    for (AbstractC27101Ga r02 : r12.A01()) {
                        r02.ARG(A03);
                    }
                } else if (!r10.equals(A0E)) {
                    C22820zh r13 = this.A02;
                    r13.A00.A00();
                    for (AbstractC27101Ga r03 : r13.A01()) {
                        r03.ARH(A03);
                    }
                }
            }
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public void A0W(C15950oC r5) {
        DeviceJid A03;
        Lock A00 = this.A0J.A00(r5);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            C16040oL r0 = this.A07;
            byte[] A04 = r0.A04(r5);
            boolean A032 = r0.A03(r5);
            if (!(A04 == null || !A032 || (A03 = C15940oB.A03(r5)) == null)) {
                C22820zh r1 = this.A02;
                r1.A00.A00();
                for (AbstractC27101Ga r02 : r1.A01()) {
                    r02.ARI(A03);
                }
            }
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public void A0X(C15950oC r5, AbstractC14640lm r6, byte[] bArr) {
        Lock A00 = this.A0J.A00(r5);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            A0Z(r5, new AnonymousClass1IS(r6, "location_msg_id", true), bArr);
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public void A0Y(C15950oC r9, AnonymousClass1IS r10) {
        Lock A00 = this.A0J.A00(r9);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            C31161a6 r2 = this.A08;
            String A002 = C31161a6.A00(r10);
            AbstractC14640lm r0 = r10.A00;
            AnonymousClass009.A05(r0);
            String rawString = r0.getRawString();
            C16310on A02 = r2.A01.A02();
            long A01 = (long) A02.A03.A01("message_base_key", A002, new String[]{rawString, r9.A02, String.valueOf(r9.A01), String.valueOf(r9.A00), r10.A01});
            int i = 5;
            if (A01 > 0) {
                i = 3;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("axolotl deleted ");
            sb.append(A01);
            sb.append(" message base key rows for ");
            sb.append(r10);
            Log.log(i, sb.toString());
            A02.close();
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public void A0Z(C15950oC r9, AnonymousClass1IS r10, byte[] bArr) {
        Lock A00 = this.A0J.A00(r9);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            C31161a6 r2 = this.A08;
            AbstractC14640lm r0 = r10.A00;
            AnonymousClass009.A05(r0);
            String rawString = r0.getRawString();
            C16310on A02 = r2.A01.A02();
            ContentValues contentValues = new ContentValues();
            contentValues.put("msg_key_remote_jid", rawString);
            contentValues.put("recipient_id", r9.A02);
            contentValues.put("recipient_type", Integer.valueOf(r9.A01));
            contentValues.put("device_id", Integer.valueOf(r9.A00));
            contentValues.put("msg_key_from_me", Boolean.valueOf(r10.A02));
            contentValues.put("msg_key_id", r10.A01);
            contentValues.put("last_alice_base_key", bArr);
            contentValues.put("timestamp", Long.valueOf(r2.A00.A00() / 1000));
            long A05 = A02.A03.A05(contentValues, "message_base_key");
            StringBuilder sb = new StringBuilder();
            sb.append("axolotl saved a message base key for ");
            sb.append(r10);
            sb.append(" with row id ");
            sb.append(A05);
            Log.i(sb.toString());
            A02.close();
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public void A0a(C15950oC r5, byte[] bArr) {
        C16310on A02 = this.A0K.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C21100wr r1 = this.A0F;
            synchronized (r1) {
                r1.A04(r5);
                this.A0B.A02(r5, bArr);
                A00.A00();
            }
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0b(C15980oF r7) {
        Lock A01 = this.A0J.A01(r7);
        try {
            if (A01 == null) {
                this.A0I.A00();
            } else {
                A01.lock();
            }
            String str = r7.A01;
            if (!C15960oD.A00.getRawString().equals(str)) {
                StringBuilder sb = new StringBuilder();
                sb.append("signalCoordinator/removefastratchetsenderkey/invalidgroupid ");
                sb.append(str);
                Log.w(sb.toString());
            } else {
                C16310on A02 = this.A06.A01.A02();
                A02.A03.A01("fast_ratchet_sender_keys", "group_id = ? AND sender_id = ? AND sender_type = ? AND device_id = ?", r7.A00());
                A02.close();
                AnonymousClass13T r0 = this.A05;
                r0.A00.A01(new C32041bW());
            }
        } finally {
            if (A01 != null) {
                A01.unlock();
            }
        }
    }

    public void A0c(C15980oF r7, byte[] bArr) {
        Lock A01 = this.A0J.A01(r7);
        try {
            if (A01 == null) {
                this.A0I.A00();
            } else {
                A01.lock();
            }
            String str = r7.A01;
            if (!C15960oD.A00.getRawString().equals(str)) {
                StringBuilder sb = new StringBuilder();
                sb.append("signalCoordinator/savefastratchetsenderkey/invalidgroupid ");
                sb.append(str);
                Log.w(sb.toString());
            } else {
                C16310on A02 = this.A06.A01.A02();
                ContentValues contentValues = new ContentValues();
                contentValues.put("group_id", str);
                C15950oC r2 = r7.A00;
                contentValues.put("sender_id", r2.A02);
                contentValues.put("sender_type", Integer.valueOf(r2.A01));
                contentValues.put("device_id", Integer.valueOf(r2.A00));
                contentValues.put("record", bArr);
                A02.A03.A05(contentValues, "fast_ratchet_sender_keys");
                A02.close();
                AnonymousClass13T r0 = this.A05;
                r0.A00.A01(new C32041bW());
            }
        } finally {
            if (A01 != null) {
                A01.unlock();
            }
        }
    }

    public void A0d(C15980oF r19, byte[] bArr) {
        Jid jid;
        Lock A01 = this.A0J.A01(r19);
        try {
            if (A01 == null) {
                this.A0I.A00();
            } else {
                A01.lock();
            }
            String str = r19.A01;
            try {
                jid = Jid.get(str);
            } catch (AnonymousClass1MW unused) {
                StringBuilder sb = new StringBuilder();
                sb.append("senderkeystore/storesenderkey/invalidgroupid ");
                sb.append(str);
                Log.w(sb.toString());
            }
            if (jid instanceof AbstractC15590nW) {
                AbstractC15590nW r3 = (AbstractC15590nW) jid;
                C29291Rp r4 = this.A0A;
                StringBuilder sb2 = new StringBuilder("SenderKeyStore/saveSenderKey/");
                sb2.append(r19);
                Log.i(sb2.toString());
                C16310on A02 = r4.A01.A02();
                AnonymousClass1Lx A00 = A02.A00();
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("record", bArr);
                    long A002 = r4.A00.A00() / 1000;
                    C16330op r8 = A02.A03;
                    long A003 = (long) r8.A00("sender_keys", contentValues, "group_id = ? AND sender_id = ? AND sender_type = ? AND device_id = ?", r19.A00());
                    if (A003 == 0) {
                        contentValues.put("group_id", str);
                        C15950oC r42 = r19.A00;
                        contentValues.put("sender_id", r42.A02);
                        contentValues.put("sender_type", Integer.valueOf(r42.A01));
                        contentValues.put("device_id", Integer.valueOf(r42.A00));
                        contentValues.put("timestamp", Long.valueOf(A002));
                        A003 = r8.A03(contentValues, "sender_keys");
                    }
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("SenderKeyStore/saveSenderKey/result/");
                    sb3.append(A003);
                    Log.i(sb3.toString());
                    A00.A00();
                    A00.close();
                    A02.close();
                    AnonymousClass13T r0 = this.A05;
                    r0.A01.A01(new C30641Yf(r3));
                } catch (Throwable th) {
                    try {
                        A00.close();
                    } catch (Throwable unused2) {
                    }
                    throw th;
                }
            } else {
                throw new AnonymousClass1MW(str);
            }
        } finally {
            if (A01 != null) {
                A01.unlock();
            }
        }
    }

    public void A0e(List list, int i) {
        Lock A00 = this.A0J.A00(A0I());
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            if (list.size() != 0) {
                C16310on A02 = this.A0K.A02();
                AnonymousClass1Lx A002 = A02.A00();
                try {
                    C16310on A022 = this.A09.A02.A02();
                    AnonymousClass1Lx A003 = A022.A00();
                    ContentValues contentValues = new ContentValues();
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        AnonymousClass1RW r2 = (AnonymousClass1RW) it.next();
                        contentValues.clear();
                        contentValues.put("prekey_id", Integer.valueOf(r2.A00));
                        contentValues.put("record", r2.A01);
                        Boolean bool = Boolean.FALSE;
                        contentValues.put("sent_to_server", bool);
                        contentValues.put("direct_distribution", bool);
                        A022.A03.A03(contentValues, "prekeys");
                    }
                    A003.A00();
                    A003.close();
                    A022.close();
                    C16310on A023 = this.A07.A03.A02();
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("next_prekey_id", Integer.valueOf(i));
                    C16330op r5 = A023.A03;
                    String valueOf = String.valueOf(0);
                    r5.A00("identities", contentValues2, "recipient_id=? AND recipient_type = ? AND device_id=?", new String[]{String.valueOf(-1), valueOf, valueOf});
                    A023.close();
                    A002.A00();
                    A002.close();
                    A02.close();
                } catch (Throwable th) {
                    try {
                        A002.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public boolean A0f() {
        Lock A00 = this.A0J.A00(A0I());
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            return this.A00.A07();
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public boolean A0g(C15950oC r6) {
        C21100wr r4 = this.A0F;
        synchronized (r4) {
            AnonymousClass1RV A00 = r4.A00(r6);
            boolean z = true;
            if (A00 == null) {
                byte[] A03 = this.A0B.A03(r6);
                if (A03 == null) {
                    r4.A03(new AnonymousClass1RV(), r6);
                    return false;
                }
                try {
                    AnonymousClass1RV r0 = new AnonymousClass1RV(A03);
                    A05(r0);
                    r4.A03(r0, r6);
                } catch (IOException unused) {
                    A0H(r6);
                    return false;
                }
            } else if (A00.A00) {
                z = false;
            }
            return z;
        }
    }

    public boolean A0h(C15950oC r5, AbstractC14640lm r6) {
        Lock A00 = this.A0J.A00(r5);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            return A0i(r5, new AnonymousClass1IS(r6, "location_msg_id", true));
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public boolean A0i(C15950oC r14, AnonymousClass1IS r15) {
        AnonymousClass16Z r1 = this.A0J;
        Lock A00 = r1.A00(r14);
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            Lock A002 = r1.A00(r14);
            if (A002 == null) {
                this.A0I.A00();
            } else {
                A002.lock();
            }
            C31161a6 r2 = this.A08;
            String A003 = C31161a6.A00(r15);
            AbstractC14640lm r0 = r15.A00;
            AnonymousClass009.A05(r0);
            String rawString = r0.getRawString();
            C16310on A01 = r2.A01.get();
            boolean z = false;
            byte[] bArr = null;
            Cursor A08 = A01.A03.A08("message_base_key", A003, null, null, new String[]{"last_alice_base_key"}, new String[]{rawString, r14.A02, String.valueOf(r14.A01), String.valueOf(r14.A00), r15.A01});
            try {
                if (A08.moveToNext()) {
                    bArr = A08.getBlob(0);
                }
                A08.close();
                A01.close();
                if (A002 != null) {
                    A002.unlock();
                }
                if (bArr == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("axolotl has no saved base key for ");
                    sb.append(r15);
                    Log.i(sb.toString());
                } else {
                    z = Arrays.equals(bArr, A0G(r14).A01.A00.A05.A04());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("axolotl has ");
                    sb2.append(z ? "matching" : "different");
                    sb2.append(" saved base key and session for ");
                    sb2.append(r15);
                    sb2.append(" and ");
                    sb2.append(r14);
                    Log.i(sb2.toString());
                }
                return z;
            } catch (Throwable th) {
                if (A08 != null) {
                    try {
                        A08.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }

    public boolean A0j(C15980oF r8) {
        boolean z;
        Jid jid;
        Lock A01 = this.A0J.A01(r8);
        try {
            if (A01 == null) {
                this.A0I.A00();
            } else {
                A01.lock();
            }
            String str = r8.A01;
            try {
                jid = Jid.get(str);
            } catch (AnonymousClass1MW unused) {
                StringBuilder sb = new StringBuilder();
                sb.append("senderkeystore/removesenderkey/invalidgroupid ");
                sb.append(str);
                Log.w(sb.toString());
                z = false;
            }
            if (jid instanceof AbstractC15590nW) {
                AbstractC15590nW r5 = (AbstractC15590nW) jid;
                C29291Rp r2 = this.A0A;
                StringBuilder sb2 = new StringBuilder("SenderKeyStore/removeSenderKey/");
                sb2.append(r8);
                Log.i(sb2.toString());
                C16310on A02 = r2.A01.A02();
                z = false;
                if (A02.A03.A01("sender_keys", "group_id = ? AND sender_id = ? AND sender_type = ? AND device_id = ?", r8.A00()) > 0) {
                    z = true;
                }
                A02.close();
                AnonymousClass13T r0 = this.A05;
                r0.A01.A01(new C30641Yf(r5));
                return z;
            }
            throw new AnonymousClass1MW(str);
        } finally {
            if (A01 != null) {
                A01.unlock();
            }
        }
    }

    public byte[] A0k() {
        Lock A00 = this.A0J.A00(A0I());
        try {
            if (A00 == null) {
                this.A0I.A00();
            } else {
                A00.lock();
            }
            byte[] bArr = this.A00.A04().A01.A00.A01;
            Log.i("axolotl fetched identity key for sending");
            return bArr;
        } finally {
            if (A00 != null) {
                A00.unlock();
            }
        }
    }
}
