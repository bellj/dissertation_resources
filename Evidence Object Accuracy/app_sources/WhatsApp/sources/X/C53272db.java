package X;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53272db extends RelativeLayout {
    public WaTextView A00 = C12960it.A0N(this, R.id.category_thumbnail_text);

    public C53272db(Context context) {
        super(context);
        C12960it.A0E(this).inflate(R.layout.category_media_card_thumbnail, (ViewGroup) this, true);
    }

    public void setText(String str) {
        this.A00.setText(str);
    }
}
