package X;

import android.text.TextUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Deprecated
/* renamed from: X.5ou  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124285ou extends AbstractC16350or {
    public final int A00;
    public final C20380vf A01;
    public final C20390vg A02;
    public final C17070qD A03;
    public final C30941Zk A04;
    public final AbstractC14440lR A05;
    public final WeakReference A06;
    public final WeakReference A07;
    public final WeakReference A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;

    public C124285ou(ActivityC13790kL r2, C20380vf r3, C20390vg r4, C17070qD r5, C30941Zk r6, AbstractC14440lR r7, WeakReference weakReference, WeakReference weakReference2, int i, boolean z, boolean z2, boolean z3) {
        this.A05 = r7;
        this.A04 = r6;
        this.A03 = r5;
        this.A01 = r3;
        this.A02 = r4;
        this.A06 = C12970iu.A10(r2);
        this.A0A = z;
        this.A0B = z2;
        this.A09 = z3;
        this.A00 = i;
        this.A07 = weakReference;
        this.A08 = weakReference2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        List A0l = C12960it.A0l();
        List A0l2 = C12960it.A0l();
        List A0l3 = C12960it.A0l();
        if (this.A0B) {
            C30941Zk r2 = this.A04;
            r2.A04 = true;
            C17070qD r0 = this.A03;
            r0.A03();
            C20370ve r1 = r0.A08;
            A0l2 = r1.A0V(r2);
            r0.A03();
            A0l3 = r1.A0S(this.A00);
        }
        if (this.A0A) {
            C17070qD r02 = this.A03;
            r02.A03();
            A0l = r02.A09.A0A();
        }
        return new C127435uU(A0l, A0l2, A0l3);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C127435uU r8 = (C127435uU) obj;
        synchronized (this) {
            ActivityC13790kL r3 = (ActivityC13790kL) this.A06.get();
            AnonymousClass6M5 r2 = (AnonymousClass6M5) this.A07.get();
            AbstractC136346Me r4 = (AbstractC136346Me) this.A08.get();
            if (r3 != null) {
                r3.AaN();
                if (r2 != null) {
                    List list = r8.A00;
                    r2.AfR(list);
                    if (this.A09) {
                        ArrayList A0l = C12960it.A0l();
                        Iterator it = list.iterator();
                        while (it.hasNext()) {
                            AbstractC28901Pl A0H = C117305Zk.A0H(it);
                            if (!TextUtils.isEmpty(A0H.A0A)) {
                                A0l.add(A0H.A0A);
                            }
                        }
                        this.A05.Ab2(new Runnable() { // from class: X.6H1
                            @Override // java.lang.Runnable
                            public final void run() {
                                C20390vg.this.A00();
                            }
                        });
                    }
                }
                if (this.A0B && r4 != null) {
                    List list2 = r8.A02;
                    r4.Afa(list2);
                    List list3 = r8.A01;
                    r4.AfY(list3);
                    if (this.A09) {
                        A08(list2);
                        A08(list3);
                    }
                }
                r3.invalidateOptionsMenu();
            }
        }
    }

    public final void A08(List list) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1IR A09 = C117315Zl.A09(it);
            if (!TextUtils.isEmpty(A09.A0K)) {
                A0l.add(A09.A0K);
            }
        }
        this.A05.Ab2(new Runnable(A0l) { // from class: X.6IZ
            public final /* synthetic */ List A01;

            {
                this.A01 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C124285ou r0 = C124285ou.this;
                r0.A01.A04(this.A01);
            }
        });
    }
}
