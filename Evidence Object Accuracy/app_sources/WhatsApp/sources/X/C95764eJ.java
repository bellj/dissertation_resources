package X;

import android.animation.ValueAnimator;
import com.google.android.material.tabs.TabLayout;

/* renamed from: X.4eJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95764eJ implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ TabLayout A00;

    public C95764eJ(TabLayout tabLayout) {
        this.A00 = tabLayout;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.A00.scrollTo(C12960it.A05(valueAnimator.getAnimatedValue()), 0);
    }
}
