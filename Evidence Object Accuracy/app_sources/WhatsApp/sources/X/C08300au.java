package X;

/* renamed from: X.0au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08300au implements AbstractC12050hI {
    public static final C08300au A00 = new C08300au();

    @Override // X.AbstractC12050hI
    public Object AYt(AbstractC08850bx r2, float f) {
        return Integer.valueOf(Math.round(AnonymousClass0UD.A00(r2) * f));
    }
}
