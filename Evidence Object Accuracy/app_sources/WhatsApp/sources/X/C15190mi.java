package X;

import android.content.Context;
import android.widget.EditText;

/* renamed from: X.0mi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15190mi extends EditText {
    public AnonymousClass5RC A00;

    public C15190mi(Context context) {
        super(context);
    }

    @Override // android.widget.TextView
    public void onSelectionChanged(int i, int i2) {
        super.onSelectionChanged(i, i2);
        AnonymousClass5RC r0 = this.A00;
        if (r0 != null) {
            int selectionStart = getSelectionStart();
            int selectionEnd = getSelectionEnd();
            AnonymousClass3C4 r02 = ((AnonymousClass51Z) r0).A00;
            r02.A07 = selectionStart;
            r02.A06 = selectionEnd;
        }
    }

    public void setOnSelectionChangedListener(AnonymousClass5RC r1) {
        this.A00 = r1;
    }
}
