package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0Vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06780Vb implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VL();
    public final Bundle A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C06780Vb(Bundle bundle) {
        this.A00 = bundle;
    }

    public C06780Vb(Parcel parcel, ClassLoader classLoader) {
        Bundle readBundle = parcel.readBundle();
        this.A00 = readBundle;
        if (classLoader != null && readBundle != null) {
            readBundle.setClassLoader(classLoader);
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.A00);
    }
}
