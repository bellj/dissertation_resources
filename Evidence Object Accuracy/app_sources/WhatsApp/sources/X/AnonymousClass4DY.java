package X;

import android.util.Log;

/* renamed from: X.4DY  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4DY {
    public static void A00(String str, Throwable th, Object... objArr) {
        if (Log.isLoggable("Vision", 6)) {
            boolean isLoggable = Log.isLoggable("Vision", 3);
            String format = String.format(str, objArr);
            if (isLoggable) {
                Log.e("Vision", format, th);
                return;
            }
            String valueOf = String.valueOf(th);
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(format) + 2 + valueOf.length());
            C12990iw.A1T(A0t, format);
            Log.e("Vision", C12960it.A0d(valueOf, A0t));
        }
    }
}
