package X;

import android.text.TextUtils;
import android.widget.TextView;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.notification.PopupNotification;

/* renamed from: X.2Dn  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2Dn {
    public void A01(AbstractC14640lm r1) {
    }

    public void A00(AbstractC14640lm r4) {
        if (this instanceof C60332wf) {
            PopupNotification.A03(((C60332wf) this).A00);
        } else if (!(this instanceof C60322we)) {
            CallsHistoryFragment callsHistoryFragment = ((C60312wd) this).A00;
            C36961kx.A00((C36961kx) callsHistoryFragment.A07.getFilter());
            callsHistoryFragment.A07.getFilter().filter(callsHistoryFragment.A0d);
        } else {
            ContactInfoActivity contactInfoActivity = ((C60322we) this).A00;
            contactInfoActivity.A31();
            contactInfoActivity.A0b();
        }
    }

    public void A02(AbstractC14640lm r5) {
        AnonymousClass3DR r1;
        if (this instanceof C60332wf) {
            PopupNotification popupNotification = ((C60332wf) this).A00;
            C15370n3 r0 = popupNotification.A0p;
            if (r0 != null && r5 != null && r5.equals(r0.A0D) && !C15380n4.A0J(r5)) {
                String A00 = popupNotification.A0H.A00(popupNotification.A0p);
                boolean isEmpty = TextUtils.isEmpty(A00);
                TextView textView = popupNotification.A0F;
                if (isEmpty) {
                    textView.setVisibility(8);
                    return;
                }
                textView.setVisibility(0);
                popupNotification.A0F.setText(A00);
            }
        } else if (this instanceof C60322we) {
            ContactInfoActivity contactInfoActivity = ((C60322we) this).A00;
            if (r5.equals(UserJid.getNullable(contactInfoActivity.getIntent().getStringExtra("jid")))) {
                if (!contactInfoActivity.A3B() && (r1 = contactInfoActivity.A0a) != null) {
                    r1.A00(contactInfoActivity.A0s);
                }
                contactInfoActivity.A33();
            }
        }
    }
}
