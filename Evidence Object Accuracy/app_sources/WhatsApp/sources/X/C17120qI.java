package X;

import android.content.Context;
import android.content.ContextWrapper;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.0qI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17120qI {
    public static final Map A01 = new WeakHashMap();
    public static final Map A02 = new WeakHashMap();
    public C50132Og A00;

    public synchronized C50132Og A00() {
        C50132Og r0;
        r0 = this.A00;
        if (r0 == null) {
            r0 = new C50132Og();
            this.A00 = r0;
        }
        return r0;
    }

    public synchronized C50132Og A01(Context context) {
        C50132Og r0;
        while (context instanceof ContextWrapper) {
            ContextWrapper contextWrapper = (ContextWrapper) context;
            if (contextWrapper.getBaseContext() == null) {
                break;
            }
            context = contextWrapper.getBaseContext();
        }
        Map map = A01;
        r0 = (C50132Og) map.get(context);
        if (r0 == null) {
            r0 = new C50132Og();
            map.put(context, r0);
        }
        return r0;
    }

    public synchronized C50132Og A02(String str) {
        C50132Og r0;
        Map map = A02;
        r0 = (C50132Og) map.get(str);
        if (r0 == null) {
            r0 = new C50132Og();
            map.put(str, r0);
        }
        return r0;
    }
}
