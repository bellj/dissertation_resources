package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.1tV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41421tV {
    public static final HashSet A05 = new HashSet(Arrays.asList("CELL", "AOL", "APPLELINK", "ATTMAIL", "CIS", "EWORLD", "INTERNET", "IBMMAIL", "MCIMAIL", "POWERSHARE", "PRODIGY", "TLX", "X400"));
    public static final HashSet A06 = new HashSet(Arrays.asList("PREF", "WORK", "HOME", "VOICE", "FAX", "MSG", "CELL", "PAGER", "BBS", "MODEM", "CAR", "ISDN", "VIDEO"));
    public static final HashSet A07 = new HashSet(Arrays.asList("BLOG", "FTP", "HOMEPAGE", "PROFILE", "OTHER", "HOME", "WORK"));
    public int A00 = 0;
    public String A01;
    public StringBuilder A02;
    public final C22680zT A03;
    public final AnonymousClass018 A04;

    public C41421tV(C22680zT r2, AnonymousClass018 r3) {
        this.A04 = r3;
        this.A03 = r2;
    }

    public static void A00(C15550nR r5, C30721Yo r6) {
        List<C30741Yq> list = r6.A05;
        if (list != null) {
            for (C30741Yq r3 : list) {
                C15370n3 A0C = r5.A0C(r3.A02, true);
                if (A0C != null) {
                    if (A0C.A0J()) {
                        AnonymousClass3DQ r1 = r6.A08;
                        r1.A08 = r1.A01;
                        String A01 = r6.A0A.A01((UserJid) A0C.A0B(UserJid.class));
                        if (!TextUtils.isEmpty(A01)) {
                            r6.A01 = A01;
                        }
                    }
                    UserJid userJid = (UserJid) A0C.A0B(UserJid.class);
                    if (A0C.A0f && userJid != null) {
                        r3.A01 = userJid;
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0206, code lost:
        if (r9.trim().equals("") != false) goto L_0x0208;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x02c0, code lost:
        if (r8.trim().equals("") != false) goto L_0x02c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x036d, code lost:
        if (r8.trim().equals("") != false) goto L_0x036f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x011e, code lost:
        if (r1 != false) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01c5, code lost:
        if (r6.A00 == -1) goto L_0x018b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x01b1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x0163 A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01a7  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01cd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A01(X.C30721Yo r13) {
        /*
        // Method dump skipped, instructions count: 1505
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41421tV.A01(X.1Yo):java.lang.String");
    }
}
