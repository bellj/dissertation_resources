package X;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipErrorDialogFragment;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

/* renamed from: X.19Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19Z {
    public long A00;
    public final Handler A01;
    public final C16210od A02;
    public final AbstractC15710nm A03;
    public final C14900mE A04;
    public final C15570nT A05;
    public final C15450nH A06;
    public final C238013b A07;
    public final C20970wc A08;
    public final C237412v A09;
    public final C15550nR A0A;
    public final C15610nY A0B;
    public final C18640sm A0C;
    public final AnonymousClass01d A0D;
    public final C14830m7 A0E;
    public final C16590pI A0F;
    public final C15890o4 A0G;
    public final AnonymousClass018 A0H;
    public final C15600nX A0I;
    public final C236812p A0J;
    public final C245716a A0K;
    public final C20710wC A0L;
    public final AnonymousClass17R A0M;
    public final AbstractC14440lR A0N;
    public final AnonymousClass0t1 A0O;
    public final C237612x A0P;
    public final C236312k A0Q;
    public final AnonymousClass01H A0R;
    public volatile AnonymousClass1MP A0S;

    public AnonymousClass19Z(C16210od r5, AbstractC15710nm r6, C14900mE r7, C15570nT r8, C15450nH r9, C238013b r10, C20970wc r11, C237412v r12, C15550nR r13, C15610nY r14, C18640sm r15, AnonymousClass01d r16, C14830m7 r17, C16590pI r18, C15890o4 r19, AnonymousClass018 r20, C15600nX r21, C236812p r22, C245716a r23, C20710wC r24, AnonymousClass17R r25, AbstractC14440lR r26, AnonymousClass0t1 r27, C237612x r28, C236312k r29, AnonymousClass01H r30) {
        this.A0F = r18;
        this.A0E = r17;
        this.A04 = r7;
        this.A03 = r6;
        this.A05 = r8;
        this.A0N = r26;
        this.A0Q = r29;
        this.A06 = r9;
        this.A0M = r25;
        this.A0A = r13;
        this.A0D = r16;
        this.A0B = r14;
        this.A0H = r20;
        this.A07 = r10;
        this.A0L = r24;
        this.A0K = r23;
        this.A08 = r11;
        this.A0G = r19;
        this.A0J = r22;
        this.A0O = r27;
        this.A0I = r21;
        this.A0C = r15;
        this.A02 = r5;
        this.A09 = r12;
        this.A0P = r28;
        this.A0R = r30;
        if (Build.VERSION.SDK_INT >= 28) {
            r28.A08(new AnonymousClass2OU(this, r28, r29));
        }
        this.A01 = new AnonymousClass2OV(Looper.getMainLooper(), this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0055, code lost:
        if (r0 != false) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0175, code lost:
        if (android.text.TextUtils.equals(r13, r6.callId) == false) goto L_0x0177;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        if (android.text.TextUtils.equals(r13, r6.callId) == false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A00(android.content.Context r10, X.AnonymousClass1YW r11, com.whatsapp.jid.GroupJid r12, java.lang.String r13, java.util.ArrayList r14, java.util.ArrayList r15, java.util.List r16, boolean r17) {
        /*
        // Method dump skipped, instructions count: 650
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19Z.A00(android.content.Context, X.1YW, com.whatsapp.jid.GroupJid, java.lang.String, java.util.ArrayList, java.util.ArrayList, java.util.List, boolean):int");
    }

    public int A01(Context context, C15370n3 r12, int i, boolean z) {
        if (r12 == null) {
            return 1;
        }
        return A02(context, null, AnonymousClass15O.A00(this.A05, this.A0E, true), Collections.singletonList(r12), i, z);
    }

    public final int A02(Context context, GroupJid groupJid, String str, List list, int i, boolean z) {
        int i2;
        AnonymousClass009.A01();
        this.A09.A00.A08();
        Context context2 = this.A0F.A00;
        StringBuilder sb = new StringBuilder("app/startOutgoingCall/from ");
        sb.append(i);
        sb.append(", video call:");
        sb.append(z);
        sb.append(", groupJid:");
        sb.append(groupJid);
        Log.i(sb.toString());
        C236312k r5 = this.A0Q;
        int size = list.size();
        if (r5.A04(str, 726210773)) {
            r5.A01(str, size, z, false);
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator it = list.iterator();
        if (groupJid != null) {
            while (it.hasNext()) {
                arrayList.add(((C15370n3) it.next()).A0B(UserJid.class));
            }
        } else {
            while (it.hasNext()) {
                UserJid userJid = (UserJid) ((C15370n3) it.next()).A0B(UserJid.class);
                if (this.A07.A0I(userJid)) {
                    arrayList2.add(userJid);
                    Log.w("app/startOutgoingCall/failed_contact_blocked");
                } else {
                    arrayList.add(userJid);
                }
            }
        }
        int A00 = A00(context, null, groupJid, null, arrayList, arrayList2, list, z);
        if (A00 != 0) {
            if (A00 != 7) {
                if (A00 == 11) {
                    i2 = 1;
                }
                r5.A03(str, 7952);
                return A00;
            }
            i2 = 0;
            Intent intent = new Intent();
            intent.setClassName(context.getPackageName(), "com.whatsapp.calling.VoipPermissionsActivity");
            intent.putStringArrayListExtra("jids", C15380n4.A06(arrayList));
            intent.putExtra("call_from", i);
            intent.putExtra("video_call", z);
            intent.putExtra("permission_type", i2);
            if (groupJid != null) {
                intent.putExtra("group_jid", groupJid.getRawString());
            }
            context.startActivity(intent);
            r5.A03(str, 7952);
            return A00;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            linkedHashMap.put(it2.next(), null);
        }
        if (Build.VERSION.SDK_INT >= 28 && this.A0P.A0D()) {
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            Iterator it3 = arrayList.iterator();
            while (it3.hasNext()) {
                linkedHashMap2.put(it3.next(), null);
            }
            int A04 = A04(context2, new AnonymousClass1MP(groupJid, str, linkedHashMap2, i, z), str, arrayList, z, false);
            if (A04 == 0) {
                return A04;
            }
        }
        this.A0N.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(this, 43, new AnonymousClass1MP(groupJid, str, linkedHashMap, i, z)));
        return 0;
    }

    public int A03(Context context, GroupJid groupJid, List list, int i, boolean z) {
        return A02(context, groupJid, AnonymousClass15O.A00(this.A05, this.A0E, true), list, i, z);
    }

    public final int A04(Context context, AnonymousClass1MP r13, String str, List list, boolean z, boolean z2) {
        UserJid userJid = (UserJid) list.get(0);
        C15370n3 A0B = this.A0A.A0B(userJid);
        if (Build.VERSION.SDK_INT < 28) {
            return 10;
        }
        C237612x r5 = this.A0P;
        if (!r5.A0D()) {
            return 10;
        }
        this.A00 = SystemClock.elapsedRealtime();
        C15570nT r0 = this.A05;
        r0.A08();
        C27631Ih r02 = r0.A05;
        AnonymousClass009.A05(r02);
        if (!r5.A0E(context, r02)) {
            return 10;
        }
        this.A0S = r13;
        if (r5.A0G(userJid, str, this.A0B.A04(A0B), z, z2)) {
            Handler handler = this.A01;
            handler.removeMessages(1);
            Message message = new Message();
            message.what = 1;
            message.obj = Boolean.valueOf(z2);
            handler.sendMessageDelayed(message, 2000);
            return 0;
        }
        this.A0S = null;
        return 10;
    }

    public final void A05(int i) {
        AbstractC13860kS r2 = this.A04.A00;
        if (r2 != null) {
            ((C18980tN) this.A0R.get()).A00(AnonymousClass115.class);
            r2.Adl(VoipErrorDialogFragment.A01(new C50102Ob(), i), null);
        }
    }

    public void A06(Context context, AnonymousClass1YT r24, int i) {
        AnonymousClass009.A01();
        this.A09.A00.A08();
        StringBuilder sb = new StringBuilder("app/startFromCallLog/from ");
        sb.append(r24.A03());
        Log.i(sb.toString());
        if (this.A0S != null) {
            Log.e("app/startFromCallLog user tapped the call button twice before the telecom framework responds");
            return;
        }
        this.A00 = 0;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        for (AnonymousClass1YV r0 : r24.A04()) {
            UserJid userJid = r0.A02;
            arrayList.add(userJid);
            arrayList3.add(this.A0A.A0B(userJid));
        }
        AnonymousClass1YU r02 = r24.A0B;
        String str = r02.A02;
        String A0A = AnonymousClass1SF.A0A(str);
        C236312k r2 = this.A0Q;
        boolean z = r24.A0H;
        int size = arrayList3.size();
        if (r2.A04(A0A, 726210773)) {
            r2.A01(A0A, size, z, true);
        }
        boolean z2 = r24.A0H;
        int A00 = A00(context, r24.A0F, null, AnonymousClass1SF.A0A(str), arrayList, arrayList2, arrayList3, z2);
        if (A00 != 0) {
            if (A00 == 7 || A00 == 11) {
                int i2 = r02.A00;
                boolean z3 = r02.A03;
                UserJid userJid2 = r02.A01;
                boolean z4 = r24.A0H;
                int i3 = 1;
                if (A00 == 7) {
                    i3 = 0;
                }
                Intent intent = new Intent();
                intent.setClassName(context.getPackageName(), "com.whatsapp.calling.VoipPermissionsActivity");
                intent.putExtra("join_call_log", true);
                intent.putExtra("call_log_call_id", str);
                intent.putExtra("call_log_transaction_id", i2);
                intent.putExtra("call_log_from_me", z3);
                intent.putExtra("call_log_user_jid", userJid2.getRawString());
                intent.putExtra("video_call", z4);
                intent.putExtra("lobby_entry_point", i);
                intent.putExtra("permission_type", i3);
                context.startActivity(intent);
            }
            r2.A03(A0A, 7952);
            return;
        }
        if (A04(this.A0F.A00, new AnonymousClass1MP(r24, i), A0A, arrayList, r24.A0H, true) != 0) {
            AnonymousClass1MP r3 = new AnonymousClass1MP(r24, i);
            if (r3.A04 != null) {
                this.A0N.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(this, 42, r3));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006a, code lost:
        if (r4.A0J.A04(r1) == null) goto L_0x006c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(android.content.Context r5, java.lang.String r6, boolean r7) {
        /*
            r4 = this;
            X.AnonymousClass009.A01()
            X.0o4 r1 = r4.A0G
            boolean r0 = r1.A0B(r7)
            if (r0 != 0) goto L_0x002e
            r3 = 0
        L_0x000c:
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r1 = r5.getPackageName()
            java.lang.String r0 = "com.whatsapp.calling.VoipPermissionsActivity"
            r2.setClassName(r1, r0)
            java.lang.String r0 = "video_call"
            r2.putExtra(r0, r7)
            java.lang.String r0 = "permission_type"
            r2.putExtra(r0, r3)
            java.lang.String r0 = "call_link_lobby_token"
            r2.putExtra(r0, r6)
            r5.startActivity(r2)
            return
        L_0x002e:
            boolean r0 = r1.A09()
            if (r0 == 0) goto L_0x0036
            r3 = 1
            goto L_0x000c
        L_0x0036:
            java.lang.String r0 = "app/previewCallLink token:"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r6)
            java.lang.String r0 = " isVideoEnabled: "
            r1.append(r0)
            r1.append(r7)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            int r1 = r6.length()
            r0 = 22
            if (r1 == r0) goto L_0x005d
            java.lang.String r0 = "app/previewCallLink token with wrong length!"
            com.whatsapp.util.Log.e(r0)
            return
        L_0x005d:
            java.lang.String r1 = com.whatsapp.voipcalling.Voip.getCurrentCallId()
            if (r1 == 0) goto L_0x006c
            X.12p r0 = r4.A0J
            X.1YS r0 = r0.A04(r1)
            r3 = 1
            if (r0 != 0) goto L_0x006d
        L_0x006c:
            r3 = 0
        L_0x006d:
            X.01d r0 = r4.A0D
            android.telephony.TelephonyManager r0 = r0.A0N()
            r2 = 1
            if (r0 == 0) goto L_0x008a
            int r0 = r0.getCallState()
            if (r0 == 0) goto L_0x008a
            r1 = 2131886921(0x7f120349, float:1.9408434E38)
            if (r3 == 0) goto L_0x0084
            r1 = 2131892943(0x7f121acf, float:1.9420649E38)
        L_0x0084:
            X.0mE r0 = r4.A04
            r0.A05(r1, r2)
            return
        L_0x008a:
            X.0lR r2 = r4.A0N
            r1 = 5
            com.facebook.redex.RunnableBRunnable0Shape0S1110000_I0 r0 = new com.facebook.redex.RunnableBRunnable0Shape0S1110000_I0
            r0.<init>(r4, r6, r1, r7)
            r2.Ab2(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19Z.A07(android.content.Context, java.lang.String, boolean):void");
    }
}
