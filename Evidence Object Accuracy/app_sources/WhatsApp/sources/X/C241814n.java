package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.14n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241814n {
    public final C15570nT A00;
    public final C15450nH A01;
    public final C22700zV A02;
    public final C17170qN A03;
    public final C19990v2 A04;
    public final C20140vH A05;
    public final AnonymousClass134 A06;
    public final AnonymousClass150 A07;
    public final C14850m9 A08;

    public C241814n(C15570nT r1, C15450nH r2, C22700zV r3, C17170qN r4, C19990v2 r5, C20140vH r6, AnonymousClass134 r7, AnonymousClass150 r8, C14850m9 r9) {
        this.A08 = r9;
        this.A00 = r1;
        this.A04 = r5;
        this.A05 = r6;
        this.A06 = r7;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A07 = r8;
    }

    public void A00(AnonymousClass1PE r3) {
        int i = r3.A06;
        if (i > 0) {
            int i2 = i - 1;
            synchronized (r3) {
                r3.A06 = i2;
            }
            if (!this.A08.A07(1336)) {
                int i3 = r3.A08 - 1;
                synchronized (r3) {
                    r3.A08 = i3;
                }
            }
        }
    }

    public boolean A01(int i) {
        String A03;
        ArrayList arrayList;
        if (i >= 0) {
            if (i == 0) {
                return true;
            }
            C15450nH r1 = this.A01;
            synchronized (AbstractC15460nI.class) {
                A03 = r1.A03(AbstractC15460nI.A2J);
            }
            try {
                arrayList = new ArrayList();
                String[] split = A03.split(",");
                for (String str : split) {
                    arrayList.add(Integer.valueOf(str));
                }
            } catch (Exception unused) {
                arrayList = new ArrayList();
                for (String str2 : "604800".split(",")) {
                    arrayList.add(Integer.valueOf(str2));
                }
            }
            if (arrayList.contains(Integer.valueOf(i))) {
                return true;
            }
        }
        return false;
    }

    public boolean A02(AbstractC14640lm r7) {
        AnonymousClass1PE r0;
        C19990v2 r1 = this.A04;
        if ((r1.A0F(r7) || (r0 = (AnonymousClass1PE) r1.A0B().get(r7)) == null || r0.A0M == 1) && this.A05.A02(r7) == 1) {
            return false;
        }
        return true;
    }

    public final boolean A03(AbstractC14640lm r7, AnonymousClass1PG r8, Long l, int i, long j) {
        if (!(r7 instanceof UserJid)) {
            return false;
        }
        if (r8 == null) {
            return true;
        }
        if (i == r8.expiration || l == null) {
            return false;
        }
        long longValue = l.longValue();
        if (longValue > j) {
            return false;
        }
        long j2 = r8.ephemeralSettingTimestamp;
        if (longValue == j2) {
            UserJid of = UserJid.of(r7);
            AnonymousClass009.A05(of);
            C15570nT r0 = this.A00;
            r0.A08();
            C27631Ih r02 = r0.A05;
            return r02 != null && of.getRawString().compareTo(r02.getRawString()) < 0;
        } else if (longValue > j2) {
            return true;
        } else {
            return false;
        }
    }

    public boolean A04(AbstractC15340mz r7) {
        AnonymousClass1PE A06 = this.A04.A06(r7.A0z.A00);
        return A06 != null && r7.A12 <= A06.A0P;
    }
}
