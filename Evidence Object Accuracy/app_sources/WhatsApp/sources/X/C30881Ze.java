package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Ze  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C30881Ze extends AbstractC28901Pl {
    public static final Parcelable.Creator CREATOR = new C99774kq();
    public int A00;
    public int A01;

    public static String A07(int i) {
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? i != 5 ? "unknown" : "elo" : "discover" : "amex" : "mastercard" : "visa";
    }

    public static String A08(int i) {
        if (i == 1) {
            return "Visa";
        }
        if (i == 2) {
            return "MasterCard";
        }
        if (i == 3) {
            return "American Express";
        }
        if (i == 4) {
            return "Discover";
        }
        if (i != 5) {
            return null;
        }
        return "Elo";
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30881Ze(C17930rd r4, String str, String str2, String str3, int i, int i2, int i3, int i4, int i5, int i6) {
        int i7;
        int i8;
        this.A00 = i;
        this.A01 = i6;
        this.A0B = str2;
        AnonymousClass009.A05(r4);
        this.A07 = r4;
        A08(i2);
        A07(i3);
        if (i4 != 1 || this.A07.A00 == (i8 = this.A00)) {
            this.A03 = i4;
            if (i5 != 1 || this.A07.A01 == (i7 = this.A00)) {
                this.A02 = i5;
                this.A0A = str;
                if (str3 != null) {
                    A0A(str3);
                    return;
                }
                return;
            }
            StringBuilder sb = new StringBuilder("PAY: ");
            sb.append(i7);
            sb.append(" in country cannot be primary account type");
            throw new IllegalArgumentException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder("PAY: ");
        sb2.append(i8);
        sb2.append(" in country cannot be primary account type");
        throw new IllegalArgumentException(sb2.toString());
    }

    public /* synthetic */ C30881Ze(Parcel parcel) {
        A09(parcel);
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
    }

    public static int A04(String str) {
        if ("debit".equals(str)) {
            return 1;
        }
        if ("credit".equals(str)) {
            return 4;
        }
        if ("combo".equals(str)) {
            return 6;
        }
        if ("prepaid".equals(str)) {
            return 8;
        }
        return "UNKNOWN".equals(str) ? 7 : 0;
    }

    public static int A05(String str) {
        if ("visa".equalsIgnoreCase(str)) {
            return 1;
        }
        if ("mastercard".equalsIgnoreCase(str)) {
            return 2;
        }
        if ("amex".equalsIgnoreCase(str)) {
            return 3;
        }
        if ("discover".equalsIgnoreCase(str)) {
            return 4;
        }
        return "elo".equalsIgnoreCase(str) ? 5 : 0;
    }

    public static C30881Ze A06(C17930rd r12, AnonymousClass1ZY r13, String str, String str2, int i, int i2, int i3, int i4, int i5, int i6, long j) {
        String str3;
        Object obj;
        if (r13 != null) {
            AnonymousClass1ZR A07 = r13.A07();
            if (A07 == null) {
                obj = null;
            } else {
                obj = A07.A00;
            }
            str3 = (String) obj;
        } else {
            str3 = null;
        }
        C30881Ze r2 = new C30881Ze(r12, str, str3, str2, i, i2, i3, i4, i5, i6);
        r2.A05 = j;
        r2.A08 = r13;
        return r2;
    }

    @Override // X.AbstractC28901Pl, java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("[ CARD: ");
        sb.append(super.toString());
        sb.append(" ]");
        return sb.toString();
    }

    @Override // X.AbstractC28901Pl, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
    }
}
