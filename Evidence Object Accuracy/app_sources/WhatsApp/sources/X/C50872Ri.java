package X;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewParent;
import com.whatsapp.util.Log;

/* renamed from: X.2Ri  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50872Ri {
    public static void A00(View view, String str) {
        String str2;
        Resources resources = view.getResources();
        do {
            if (resources != null) {
                str2 = view.getId() != -1 ? resources.getResourceName(view.getId()) : "no_id";
            } else {
                str2 = "no_resources";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(view.getClass().getSimpleName());
            sb.append("/");
            sb.append(str2);
            Log.i(sb.toString());
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                view = (View) parent;
            } else {
                return;
            }
        } while (view != null);
    }
}
