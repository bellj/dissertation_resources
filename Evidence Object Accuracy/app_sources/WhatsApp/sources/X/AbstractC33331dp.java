package X;

import com.whatsapp.Conversation;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import java.util.Set;

/* renamed from: X.1dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC33331dp {
    public void A00(Set set) {
        if (this instanceof C33321do) {
            ((C33321do) this).A00.A0D();
        } else if (this instanceof AnonymousClass32P) {
            C36911kq r3 = ((AnonymousClass32P) this).A00.A1F;
            if (r3 != null) {
                int i = 0;
                while (true) {
                    C44091yC r1 = r3.A0q;
                    if (i < r1.size()) {
                        if (set.contains(r1.get(i).A01)) {
                            r3.A03(i);
                        }
                        i++;
                    } else {
                        return;
                    }
                }
            }
        } else if (this instanceof AnonymousClass32O) {
            CallsHistoryFragment callsHistoryFragment = ((AnonymousClass32O) this).A00;
            C36961kx.A00((C36961kx) callsHistoryFragment.A07.getFilter());
            callsHistoryFragment.A07.getFilter().filter(callsHistoryFragment.A0d);
        } else if (this instanceof AnonymousClass32Q) {
            AnonymousClass32Q r32 = (AnonymousClass32Q) this;
            Conversation conversation = r32.A00;
            if (!((AbstractActivityC13750kH) conversation).A0N.A0C(r32.A02)) {
                C15350n0 r12 = r32.A01;
                r12.A0F.clear();
                r12.A0Q.clear();
                conversation.A1g.A02();
            }
        }
    }
}
