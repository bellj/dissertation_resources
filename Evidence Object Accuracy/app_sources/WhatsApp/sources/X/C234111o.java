package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;

/* renamed from: X.11o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C234111o {
    public final C15450nH A00;
    public final C14850m9 A01;

    public C234111o(C15450nH r1, C14850m9 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public AnonymousClass1JQ A00(C27811Jh r33, String str, boolean z) {
        String obj;
        StringBuilder sb;
        String str2;
        StringBuilder sb2;
        String str3;
        StringBuilder sb3;
        String str4;
        String obj2;
        String[] strArr = r33.A06;
        String str5 = strArr[0];
        if (A02(str5)) {
            switch (str5.hashCode()) {
                case -1751563479:
                    if (str5.equals("setting_locale")) {
                        C27791Jf r5 = r33.A01;
                        C27831Jk r4 = r33.A03;
                        if (strArr.length != 1 || !"setting_locale".equals(strArr[0]) || !C27791Jf.A03.equals(r5) || r4 == null || !r4.A0b() || (r4.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) != 2048) {
                            return null;
                        }
                        C34701gZ r3 = r4.A0C;
                        if (r3 == null) {
                            r3 = C34701gZ.A02;
                        }
                        if ((r3.A00 & 1) != 1) {
                            return null;
                        }
                        return new AnonymousClass1JP(r33.A02, str, r3.A01, r4.A01);
                    }
                    break;
                case -1727394375:
                    if (!str5.equals("primary_feature") || strArr.length != 1 || !"primary_feature".equals(strArr[0])) {
                        return null;
                    }
                    C27831Jk r42 = r33.A03;
                    if (!C27791Jf.A03.equals(r33.A01) || r42 == null || !r42.A0b() || (r42.A00 & 524288) != 524288) {
                        return null;
                    }
                    C34681gX r1 = r42.A0G;
                    if (r1 == null) {
                        r1 = C34681gX.A01;
                    }
                    return new C34691gY(r33.A02, str, r1.A00, r42.A01);
                case -1271410875:
                    if (str5.equals("clearChat")) {
                        C27791Jf r10 = r33.A01;
                        C27831Jk r43 = r33.A03;
                        if (strArr.length == 4 && "clearChat".equals(strArr[0])) {
                            AbstractC14640lm A01 = AbstractC14640lm.A01(strArr[1]);
                            if (A01 == null) {
                                obj = "clear-chat-mutation/from-key-value unable to create chat jid";
                            } else {
                                int i = 2;
                                String str6 = strArr[2];
                                Boolean A00 = AnonymousClass1K2.A00(str6);
                                if (A00 != null) {
                                    i = 3;
                                    str6 = strArr[3];
                                    Boolean A002 = AnonymousClass1K2.A00(str6);
                                    if (A002 != null) {
                                        if (C27791Jf.A03.equals(r10) && r43 != null && r43.A0b() && (r43.A00 & 65536) == 65536) {
                                            C34661gV r32 = r43.A05;
                                            if (r32 == null) {
                                                r32 = C34661gV.A02;
                                            }
                                            boolean z2 = true;
                                            if ((r32.A00 & 1) != 1) {
                                                z2 = false;
                                            }
                                            C34351g0 r12 = r32.A01;
                                            if (r12 == null) {
                                                r12 = C34351g0.A04;
                                            }
                                            C34361g1 A02 = C34361g1.A02(r12, z2);
                                            boolean booleanValue = A00.booleanValue();
                                            boolean booleanValue2 = A002.booleanValue();
                                            return new C34671gW(r33.A02, A02, A01, str, r43.A01, booleanValue, booleanValue2, z);
                                        }
                                    }
                                }
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("clear-chat-mutation/from-key-value value=");
                                sb4.append(str6);
                                sb4.append(" at index=");
                                sb4.append(i);
                                sb4.append(" is not one of the valid strings");
                                obj = sb4.toString();
                            }
                            Log.e(obj);
                        }
                        return null;
                    }
                    break;
                case -988160667:
                    if (str5.equals("pin_v1")) {
                        C27791Jf r6 = r33.A01;
                        C27831Jk r52 = r33.A03;
                        if (strArr.length == 2 && "pin_v1".equals(strArr[0])) {
                            AbstractC14640lm A012 = AbstractC14640lm.A01(strArr[1]);
                            if (A012 == null) {
                                StringBuilder sb5 = new StringBuilder("pin-chat-mutation/from-key-value unable to create chat jid from ");
                                sb5.append(strArr[1]);
                                Log.e(sb5.toString());
                            } else if (C27791Jf.A03.equals(r6) && r52 != null && r52.A0b() && (r52.A00 & 16) == 16) {
                                C34641gT r2 = r52.A0F;
                                if (r2 == null) {
                                    r2 = C34641gT.A02;
                                }
                                if ((r2.A00 & 1) == 1) {
                                    boolean z3 = r2.A01;
                                    return new C34651gU(r33.A02, A012, str, r52.A01, z3, z);
                                }
                            }
                        }
                        return null;
                    }
                    break;
                case -748101438:
                    if (str5.equals("archive")) {
                        C27791Jf r44 = r33.A01;
                        C27831Jk r53 = r33.A03;
                        if (strArr.length == 2 && "archive".equals(strArr[0])) {
                            AbstractC14640lm A013 = AbstractC14640lm.A01(strArr[1]);
                            if (A013 == null) {
                                StringBuilder sb6 = new StringBuilder("archive-chat-mutation/from-key-value unable to create chat jid from ");
                                sb6.append(strArr[1]);
                                Log.e(sb6.toString());
                            } else if (C27791Jf.A03.equals(r44) && r53 != null && r53.A0b() && (r53.A00 & 4096) == 4096) {
                                C34621gR r45 = r53.A04;
                                if (r45 == null) {
                                    r45 = C34621gR.A03;
                                }
                                int i2 = r45.A00;
                                if ((i2 & 1) == 1) {
                                    boolean z4 = false;
                                    if ((i2 & 2) == 2) {
                                        z4 = true;
                                    }
                                    C34351g0 r13 = r45.A01;
                                    if (r13 == null) {
                                        r13 = C34351g0.A04;
                                    }
                                    C34361g1 A022 = C34361g1.A02(r13, z4);
                                    boolean z5 = r45.A02;
                                    return new C34631gS(r33.A02, A022, A013, str, r53.A01, z5, z);
                                }
                            }
                        }
                        return null;
                    }
                    break;
                case 3363353:
                    if (str5.equals("mute")) {
                        C27791Jf r54 = r33.A01;
                        C27831Jk r7 = r33.A03;
                        if (strArr.length == 2 && "mute".equals(strArr[0])) {
                            AbstractC14640lm A014 = AbstractC14640lm.A01(strArr[1]);
                            if (A014 == null) {
                                StringBuilder sb7 = new StringBuilder("mute-chat-mutation/from-key-value unable to create chat jid from ");
                                sb7.append(strArr[1]);
                                Log.e(sb7.toString());
                            } else if (C27791Jf.A03.equals(r54) && r7 != null && r7.A0b() && (r7.A00 & 8) == 8) {
                                C34601gP r34 = r7.A0E;
                                if (r34 == null) {
                                    r34 = C34601gP.A03;
                                }
                                int i3 = r34.A00;
                                if ((i3 & 1) == 1) {
                                    long j = 0;
                                    boolean z6 = r34.A02;
                                    if (z6 && (i3 & 2) == 2) {
                                        long j2 = r34.A01;
                                        if (j2 >= 0 || j2 == -1) {
                                            j = j2;
                                        }
                                    }
                                    return new C34611gQ(r33.A02, A014, str, j, r7.A01, z6, z);
                                }
                            }
                        }
                        return null;
                    }
                    break;
                case 3540562:
                    if (str5.equals("star")) {
                        C27791Jf r72 = r33.A01;
                        C27831Jk r55 = r33.A03;
                        if (strArr.length == 5 && "star".equals(strArr[0])) {
                            AbstractC14640lm A015 = AbstractC14640lm.A01(strArr[1]);
                            if (A015 == null) {
                                sb = new StringBuilder("star-message-mutation/from-key-value unable to create chat jid from ");
                                str2 = strArr[1];
                            } else {
                                String str7 = strArr[3];
                                Boolean A003 = AnonymousClass1K2.A00(str7);
                                if (A003 == null) {
                                    sb = new StringBuilder("star-message-mutation/from-key-value value=");
                                    sb.append(str7);
                                    sb.append(" at index=");
                                    sb.append(3);
                                    str2 = " is not one of the valid strings";
                                } else if (C27791Jf.A03.equals(r72) && r55 != null && r55.A0b() && (r55.A00 & 2) == 2) {
                                    C34581gN r14 = r55.A0L;
                                    if (r14 == null) {
                                        r14 = C34581gN.A02;
                                    }
                                    if ((r14.A00 & 1) == 1) {
                                        AnonymousClass1IS r46 = new AnonymousClass1IS(A015, strArr[2], A003.booleanValue());
                                        AbstractC14640lm A016 = AbstractC14640lm.A01(strArr[4]);
                                        C34581gN r15 = r55.A0L;
                                        if (r15 == null) {
                                            r15 = C34581gN.A02;
                                        }
                                        boolean z7 = r15.A01;
                                        return new C34591gO(r33.A02, A016, r46, str, r55.A01, z7, z);
                                    }
                                }
                            }
                            sb.append(str2);
                            Log.e(sb.toString());
                        }
                        return null;
                    }
                    break;
                case 249675220:
                    if (str5.equals("setting_pushName")) {
                        C27791Jf r56 = r33.A01;
                        C27831Jk r47 = r33.A03;
                        if (strArr.length != 1 || !"setting_pushName".equals(strArr[0]) || !C27791Jf.A03.equals(r56) || r47 == null || !r47.A0b() || (r47.A00 & 64) != 64) {
                            return null;
                        }
                        C34561gL r35 = r47.A0H;
                        if (r35 == null) {
                            r35 = C34561gL.A02;
                        }
                        if ((r35.A00 & 1) != 1) {
                            return null;
                        }
                        return new C34571gM(r33.A02, str, r35.A01, r47.A01);
                    }
                    break;
                case 478826921:
                    if (str5.equals("time_format")) {
                        C27791Jf r57 = r33.A01;
                        C27831Jk r48 = r33.A03;
                        if (strArr.length != 1 || !"time_format".equals(strArr[0]) || !C27791Jf.A03.equals(r57) || r48 == null || !r48.A0b() || (r48.A00 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) != 16777216) {
                            return null;
                        }
                        C34541gJ r36 = r48.A0O;
                        if (r36 == null) {
                            r36 = C34541gJ.A02;
                        }
                        if ((r36.A00 & 1) != 1) {
                            return null;
                        }
                        boolean z8 = r36.A01;
                        return new C34551gK(r33.A02, str, r48.A01, z8);
                    }
                    break;
                case 685217037:
                    if (str5.equals("markChatAsRead")) {
                        C27791Jf r49 = r33.A01;
                        C27831Jk r58 = r33.A03;
                        if (strArr.length == 2 && "markChatAsRead".equals(strArr[0])) {
                            AbstractC14640lm A017 = AbstractC14640lm.A01(strArr[1]);
                            if (A017 == null) {
                                Log.e("mark-chat-as-read-mutation/from-key-value unable to create chat jid");
                            } else if (C27791Jf.A03.equals(r49) && r58 != null && r58.A0b() && (r58.A00 & 32768) == 32768) {
                                C34511gG r410 = r58.A0D;
                                if (r410 == null) {
                                    r410 = C34511gG.A03;
                                }
                                int i4 = r410.A00;
                                if ((i4 & 1) == 1) {
                                    boolean z9 = false;
                                    if ((i4 & 2) == 2) {
                                        z9 = true;
                                    }
                                    C34351g0 r16 = r410.A01;
                                    if (r16 == null) {
                                        r16 = C34351g0.A04;
                                    }
                                    C34361g1 A023 = C34361g1.A02(r16, z9);
                                    boolean z10 = r410.A02;
                                    return new C34521gH(r33.A02, A023, A017, str, r58.A01, z10, z);
                                }
                            }
                        }
                        return null;
                    }
                    break;
                case 951526432:
                    if (str5.equals("contact")) {
                        C27791Jf r62 = r33.A01;
                        C27831Jk r59 = r33.A03;
                        String str8 = null;
                        if (strArr.length == 2 && "contact".equals(strArr[0])) {
                            UserJid nullable = UserJid.getNullable(strArr[1]);
                            if (nullable == null) {
                                sb2 = new StringBuilder("contact-mutation/from-key-value unable to create user jid from ");
                                sb2.append(strArr[1]);
                            } else {
                                C27791Jf r8 = C27791Jf.A03;
                                if (r8.equals(r62)) {
                                    if (r59 == null || !r59.A0b() || (r59.A00 & 4) != 4) {
                                        str3 = "contact-mutation/from-key-value syncActionValue is null, missing timestamp, or is missing contactAction";
                                    } else {
                                        long j3 = r59.A01;
                                        C34491gE r63 = r59.A06;
                                        if (r63 == null) {
                                            r63 = C34491gE.A03;
                                        }
                                        int i5 = r63.A00;
                                        if ((i5 & 1) == 1) {
                                            String str9 = r63.A02;
                                            if ((i5 & 2) == 2) {
                                                str8 = r63.A01;
                                            }
                                            return new C34501gF(r8, r33.A02, nullable, str, str8, str9, j3);
                                        }
                                        str3 = "contact-mutation/from-key-value fullName was not in contactAction protobuf";
                                    }
                                    Log.e(str3);
                                } else {
                                    C27791Jf r22 = C27791Jf.A02;
                                    if (r22.equals(r62)) {
                                        return new C34501gF(r22, r33.A02, nullable, str, null, null, 0);
                                    }
                                    sb2 = new StringBuilder("contact-mutation/from-key-value unknown operation: ");
                                    sb2.append(r62);
                                }
                            }
                            str3 = sb2.toString();
                            Log.e(str3);
                        }
                        return null;
                    }
                    break;
                case 1084168033:
                    if (str5.equals("setting_unarchiveChats")) {
                        C27791Jf r510 = r33.A01;
                        C27831Jk r411 = r33.A03;
                        if (strArr.length != 1 || !"setting_unarchiveChats".equals(strArr[0]) || !C27791Jf.A03.equals(r510) || r411 == null || !r411.A0b() || (r411.A00 & 262144) != 262144) {
                            return null;
                        }
                        long j4 = r411.A01;
                        AnonymousClass1JR r17 = r33.A02;
                        C34471gC r0 = r411.A0P;
                        if (r0 == null) {
                            r0 = C34471gC.A02;
                        }
                        return new C34481gD(r17, str, j4, r0.A01);
                    }
                    break;
                case 1178537985:
                    if (str5.equals("favoriteSticker")) {
                        return null;
                    }
                    break;
                case 1262856228:
                    if (str5.equals("sentinel")) {
                        C27791Jf r412 = r33.A01;
                        C27831Jk r73 = r33.A03;
                        if (strArr.length == 2 && "sentinel".equals(strArr[0])) {
                            String str10 = strArr[1];
                            if (!AnonymousClass1JQ.A08.contains(str10)) {
                                StringBuilder sb8 = new StringBuilder("sentinel-mutation/from-key-value unknown collectionName: ");
                                sb8.append(str10);
                                Log.e(sb8.toString());
                            } else if (C27791Jf.A03.equals(r412) && r73 != null && r73.A0b() && (r73.A00 & 16384) == 16384) {
                                C34451gA r413 = r73.A09;
                                if (r413 == null) {
                                    r413 = C34451gA.A02;
                                }
                                if ((r413.A00 & 1) == 1) {
                                    long j5 = r73.A01;
                                    return new C34461gB(r33.A02, str, str10, r413.A01, j5, z);
                                }
                            }
                        }
                        return null;
                    }
                    break;
                case 1693532963:
                    if (str5.equals("android_unsupported_actions")) {
                        C27791Jf r511 = r33.A01;
                        C27831Jk r414 = r33.A03;
                        if (strArr.length != 1 || !"android_unsupported_actions".equals(strArr[0]) || !C27791Jf.A03.equals(r511) || r414 == null || !r414.A0b() || (r414.A00 & 1048576) != 1048576) {
                            return null;
                        }
                        C34431g8 r18 = r414.A03;
                        if (r18 == null) {
                            r18 = C34431g8.A02;
                        }
                        if ((r18.A00 & 1) != 1) {
                            return null;
                        }
                        return new C34441g9(r33.A02, str, r414.A01, z);
                    }
                    break;
                case 1728918981:
                    if (str5.equals("deleteMessageForMe")) {
                        C27791Jf r415 = r33.A01;
                        C27831Jk r512 = r33.A03;
                        if (strArr.length == 5 && "deleteMessageForMe".equals(strArr[0])) {
                            AbstractC14640lm A018 = AbstractC14640lm.A01(strArr[1]);
                            if (A018 == null) {
                                sb3 = new StringBuilder("delete-message-for-me-mutation/from-key-value unable to create chat jid from ");
                                str4 = strArr[1];
                            } else {
                                String str11 = strArr[3];
                                Boolean A004 = AnonymousClass1K2.A00(str11);
                                if (A004 == null) {
                                    sb3 = new StringBuilder("delete-message-for-me-mutation/from-key-value value=");
                                    sb3.append(str11);
                                    sb3.append(" at index=");
                                    sb3.append(3);
                                    str4 = " is not one of the valid strings";
                                } else if (C27791Jf.A03.equals(r415) && r512 != null && r512.A0b() && (r512.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                                    C34401g5 r19 = r512.A08;
                                    if (r19 == null) {
                                        r19 = C34401g5.A03;
                                    }
                                    int i6 = r19.A00;
                                    if ((i6 & 1) == 1 && (i6 & 2) == 2) {
                                        AnonymousClass1IS r74 = new AnonymousClass1IS(A018, strArr[2], A004.booleanValue());
                                        AbstractC14640lm A019 = AbstractC14640lm.A01(strArr[4]);
                                        C34401g5 r37 = r512.A08;
                                        if (r37 == null) {
                                            r37 = C34401g5.A03;
                                        }
                                        boolean z11 = r37.A02;
                                        return new C34411g6(r33.A02, A019, r74, str, r512.A01, TimeUnit.SECONDS.toMillis(r37.A01), z11, z);
                                    }
                                }
                            }
                            sb3.append(str4);
                            Log.e(sb3.toString());
                        }
                        return null;
                    }
                    break;
                case 1764081571:
                    if (str5.equals("deleteChat")) {
                        C27791Jf r75 = r33.A01;
                        C27831Jk r416 = r33.A03;
                        if (strArr.length == 3 && "deleteChat".equals(strArr[0])) {
                            AbstractC14640lm A0110 = AbstractC14640lm.A01(strArr[1]);
                            if (A0110 == null) {
                                obj2 = "delete-chat-mutation/from-key-value unable to create chat jid";
                            } else {
                                String str12 = strArr[2];
                                Boolean A005 = AnonymousClass1K2.A00(str12);
                                if (A005 == null) {
                                    StringBuilder sb9 = new StringBuilder("delete-chat-mutation/from-key-value value=");
                                    sb9.append(str12);
                                    sb9.append(" at index=");
                                    sb9.append(2);
                                    sb9.append(" is not one of the valid strings");
                                    obj2 = sb9.toString();
                                } else if (C27791Jf.A03.equals(r75) && r416 != null && r416.A0b() && (r416.A00 & C25981Bo.A0F) == 131072) {
                                    C34341fz r38 = r416.A07;
                                    if (r38 == null) {
                                        r38 = C34341fz.A02;
                                    }
                                    boolean z12 = true;
                                    if ((r38.A00 & 1) != 1) {
                                        z12 = false;
                                    }
                                    C34351g0 r110 = r38.A01;
                                    if (r110 == null) {
                                        r110 = C34351g0.A04;
                                    }
                                    C34361g1 A024 = C34361g1.A02(r110, z12);
                                    boolean booleanValue3 = A005.booleanValue();
                                    return new C34371g2(r33.A02, A024, A0110, str, r416.A01, booleanValue3, z);
                                }
                            }
                            Log.e(obj2);
                        }
                        return null;
                    }
                    break;
            }
        }
        return null;
    }

    public final AnonymousClass1JQ A01(AnonymousClass1JR r8, String str, String str2, byte[] bArr, byte[] bArr2, byte[] bArr3, int i, boolean z) {
        try {
            C27791Jf r1 = C27791Jf.A03;
            if (!Arrays.equals(r1.A01, bArr2)) {
                r1 = C27791Jf.A02;
                if (!Arrays.equals(r1.A01, bArr2)) {
                    StringBuilder sb = new StringBuilder("Incorrect operation bytes: ");
                    sb.append(new String(bArr2));
                    throw new IllegalStateException(sb.toString());
                }
            }
            C27811Jh r0 = new C27811Jh(r1, r8, str2, bArr, bArr3, i);
            AnonymousClass1JQ A00 = A00(r0, str, z);
            if (A00 == null) {
                return A00;
            }
            A00.A02 = r0.A05;
            return A00;
        } catch (C28971Pt | C34331fy | IllegalArgumentException | JSONException e) {
            Log.e("sync-mutation/from-key-value couldn't create sync action data", e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x009d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x0009 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(java.lang.String r4) {
        /*
            r3 = this;
            int r0 = r4.hashCode()
            r2 = 1
            r1 = 0
            switch(r0) {
                case -1751563479: goto L_0x0094;
                case -1727394375: goto L_0x0091;
                case -1271410875: goto L_0x0080;
                case -988160667: goto L_0x006f;
                case -748101438: goto L_0x006c;
                case 3363353: goto L_0x0069;
                case 3540562: goto L_0x0065;
                case 249675220: goto L_0x0061;
                case 478826921: goto L_0x004f;
                case 685217037: goto L_0x004c;
                case 951526432: goto L_0x0049;
                case 1084168033: goto L_0x0037;
                case 1178537985: goto L_0x0026;
                case 1262856228: goto L_0x0022;
                case 1693532963: goto L_0x0011;
                case 1728918981: goto L_0x000d;
                case 1764081571: goto L_0x000a;
                default: goto L_0x0009;
            }
        L_0x0009:
            return r1
        L_0x000a:
            java.lang.String r0 = "deleteChat"
            goto L_0x0082
        L_0x000d:
            java.lang.String r0 = "deleteMessageForMe"
            goto L_0x0097
        L_0x0011:
            java.lang.String r0 = "android_unsupported_actions"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            X.0nH r1 = r3.A00
            X.0nJ r0 = X.AbstractC15460nI.A0e
            boolean r0 = r1.A05(r0)
            return r0
        L_0x0022:
            java.lang.String r0 = "sentinel"
            goto L_0x0097
        L_0x0026:
            java.lang.String r0 = "favoriteSticker"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            X.0m9 r1 = r3.A01
            r0 = 864(0x360, float:1.211E-42)
            boolean r0 = r1.A07(r0)
            return r0
        L_0x0037:
            java.lang.String r0 = "setting_unarchiveChats"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            X.0m9 r1 = r3.A01
            r0 = 736(0x2e0, float:1.031E-42)
            boolean r0 = r1.A07(r0)
            return r0
        L_0x0049:
            java.lang.String r0 = "contact"
            goto L_0x0097
        L_0x004c:
            java.lang.String r0 = "markChatAsRead"
            goto L_0x0097
        L_0x004f:
            java.lang.String r0 = "time_format"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            X.0m9 r1 = r3.A01
            r0 = 1612(0x64c, float:2.259E-42)
            boolean r0 = r1.A07(r0)
            return r0
        L_0x0061:
            java.lang.String r0 = "setting_pushName"
            goto L_0x0097
        L_0x0065:
            java.lang.String r0 = "star"
            goto L_0x0097
        L_0x0069:
            java.lang.String r0 = "mute"
            goto L_0x0097
        L_0x006c:
            java.lang.String r0 = "archive"
            goto L_0x0097
        L_0x006f:
            java.lang.String r0 = "pin_v1"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            X.0nH r1 = r3.A00
            X.0nJ r0 = X.AbstractC15460nI.A0i
            boolean r0 = r1.A05(r0)
            return r0
        L_0x0080:
            java.lang.String r0 = "clearChat"
        L_0x0082:
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            X.0nH r1 = r3.A00
            X.0nJ r0 = X.AbstractC15460nI.A0f
            boolean r0 = r1.A05(r0)
            return r0
        L_0x0091:
            java.lang.String r0 = "primary_feature"
            goto L_0x0097
        L_0x0094:
            java.lang.String r0 = "setting_locale"
        L_0x0097:
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0009
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C234111o.A02(java.lang.String):boolean");
    }
}
