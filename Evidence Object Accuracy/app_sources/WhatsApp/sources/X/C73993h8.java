package X;

import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;

/* renamed from: X.3h8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73993h8 extends TranslateAnimation {
    public final /* synthetic */ C92254Vd A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C73993h8(C92254Vd r10) {
        super(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        this.A00 = r10;
    }

    @Override // android.view.animation.TranslateAnimation, android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        C92254Vd r2 = this.A00;
        r2.A02.ATC(((float) r2.A01.getHeight()) * (1.0f - f));
    }
}
