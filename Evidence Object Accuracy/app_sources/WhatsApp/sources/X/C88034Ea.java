package X;

/* renamed from: X.4Ea  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88034Ea {
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.String r1) {
        /*
            if (r1 == 0) goto L_0x000d
            java.lang.String r1 = r1.toLowerCase()
            int r0 = r1.hashCode()
            switch(r0) {
                case -2139208497: goto L_0x00cb;
                case -1771213723: goto L_0x00c1;
                case -1560609346: goto L_0x00b7;
                case -1338910485: goto L_0x00ad;
                case -1152426539: goto L_0x00a3;
                case -919668978: goto L_0x0099;
                case -856935945: goto L_0x008f;
                case -850113115: goto L_0x0085;
                case -596951334: goto L_0x007b;
                case -371061680: goto L_0x0071;
                case 3536713: goto L_0x0067;
                case 92676538: goto L_0x005d;
                case 306174265: goto L_0x0053;
                case 329032921: goto L_0x0048;
                case 908259181: goto L_0x003d;
                case 1155840218: goto L_0x0032;
                case 1223328215: goto L_0x0027;
                case 1659800405: goto L_0x001c;
                case 1945443043: goto L_0x0011;
                default: goto L_0x000d;
            }
        L_0x000d:
            r1 = 2131891235(0x7f121423, float:1.9417184E38)
        L_0x0010:
            return r1
        L_0x0011:
            java.lang.String r0 = "third_party_infringement"
            boolean r0 = r1.equals(r0)
            r1 = 2131891244(0x7f12142c, float:1.9417203E38)
            goto L_0x00d4
        L_0x001c:
            java.lang.String r0 = "violent_content"
            boolean r0 = r1.equals(r0)
            r1 = 2131891248(0x7f121430, float:1.941721E38)
            goto L_0x00d4
        L_0x0027:
            java.lang.String r0 = "weapons"
            boolean r0 = r1.equals(r0)
            r1 = 2131891249(0x7f121431, float:1.9417213E38)
            goto L_0x00d4
        L_0x0032:
            java.lang.String r0 = "real_fake_currency"
            boolean r0 = r1.equals(r0)
            r1 = 2131891241(0x7f121429, float:1.9417196E38)
            goto L_0x00d4
        L_0x003d:
            java.lang.String r0 = "healthcare"
            boolean r0 = r1.equals(r0)
            r1 = 2131891238(0x7f121426, float:1.941719E38)
            goto L_0x00d4
        L_0x0048:
            java.lang.String r0 = "unauthorized_media"
            boolean r0 = r1.equals(r0)
            r1 = 2131891246(0x7f12142e, float:1.9417207E38)
            goto L_0x00d4
        L_0x0053:
            java.lang.String r0 = "violation_drugs"
            boolean r0 = r1.equals(r0)
            r1 = 2131891247(0x7f12142f, float:1.9417209E38)
            goto L_0x00d4
        L_0x005d:
            java.lang.String r0 = "adult"
            boolean r0 = r1.equals(r0)
            r1 = 2131891230(0x7f12141e, float:1.9417174E38)
            goto L_0x00d4
        L_0x0067:
            java.lang.String r0 = "spam"
            boolean r0 = r1.equals(r0)
            r1 = 2131891242(0x7f12142a, float:1.9417199E38)
            goto L_0x00d4
        L_0x0071:
            java.lang.String r0 = "illegal_products_services"
            boolean r0 = r1.equals(r0)
            r1 = 2131891239(0x7f121427, float:1.9417192E38)
            goto L_0x00d4
        L_0x007b:
            java.lang.String r0 = "supplements"
            boolean r0 = r1.equals(r0)
            r1 = 2131891243(0x7f12142b, float:1.94172E38)
            goto L_0x00d4
        L_0x0085:
            java.lang.String r0 = "body_parts_fluids"
            boolean r0 = r1.equals(r0)
            r1 = 2131891233(0x7f121421, float:1.941718E38)
            goto L_0x00d4
        L_0x008f:
            java.lang.String r0 = "animals"
            boolean r0 = r1.equals(r0)
            r1 = 2131891232(0x7f121420, float:1.9417178E38)
            goto L_0x00d4
        L_0x0099:
            java.lang.String r0 = "alcohol"
            boolean r0 = r1.equals(r0)
            r1 = 2131891231(0x7f12141f, float:1.9417176E38)
            goto L_0x00d4
        L_0x00a3:
            java.lang.String r0 = "tobacco"
            boolean r0 = r1.equals(r0)
            r1 = 2131891245(0x7f12142d, float:1.9417205E38)
            goto L_0x00d4
        L_0x00ad:
            java.lang.String r0 = "dating"
            boolean r0 = r1.equals(r0)
            r1 = 2131891234(0x7f121422, float:1.9417182E38)
            goto L_0x00d4
        L_0x00b7:
            java.lang.String r0 = "digital_services_products"
            boolean r0 = r1.equals(r0)
            r1 = 2131891236(0x7f121424, float:1.9417186E38)
            goto L_0x00d4
        L_0x00c1:
            java.lang.String r0 = "gambling"
            boolean r0 = r1.equals(r0)
            r1 = 2131891237(0x7f121425, float:1.9417188E38)
            goto L_0x00d4
        L_0x00cb:
            java.lang.String r0 = "misleading"
            boolean r0 = r1.equals(r0)
            r1 = 2131891240(0x7f121428, float:1.9417194E38)
        L_0x00d4:
            if (r0 != 0) goto L_0x0010
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C88034Ea.A00(java.lang.String):int");
    }
}
