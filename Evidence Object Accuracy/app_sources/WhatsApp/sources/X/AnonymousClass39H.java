package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.YoutubePlayerTouchOverlay;
import java.io.InputStream;
import java.util.Locale;
import org.json.JSONObject;

/* renamed from: X.39H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39H extends AnonymousClass21T {
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = 1;
    public int A03 = -1;
    public long A04 = -9223372036854775807L;
    public JSONObject A05;
    public boolean A06 = false;
    public boolean A07 = false;
    public Bitmap[] A08;
    public final Context A09;
    public final ViewGroup A0A;
    public final WebView A0B;
    public final C14900mE A0C;
    public final AnonymousClass39B A0D;
    public final YoutubePlayerTouchOverlay A0E;
    public final String A0F;

    @Override // X.AnonymousClass21T
    public Bitmap A03() {
        return null;
    }

    @Override // X.AnonymousClass21T
    public void A0A(boolean z) {
    }

    @Override // X.AnonymousClass21T
    public boolean A0C() {
        return false;
    }

    @Override // X.AnonymousClass21T
    public boolean A0D() {
        return false;
    }

    public AnonymousClass39H(Context context, Bitmap bitmap, AnonymousClass12P r14, C14900mE r15, AnonymousClass39B r16, String str, int i) {
        String str2;
        JSONObject jSONObject;
        int i2;
        if (Build.VERSION.SDK_INT >= 19) {
            this.A09 = context;
            this.A0C = r15;
            this.A0D = r16;
            try {
                InputStream openRawResource = context.getResources().openRawResource(R.raw.youtube_player_iframe);
                str2 = new String(AnonymousClass1P1.A04(openRawResource));
                if (openRawResource != null) {
                    openRawResource.close();
                }
            } catch (Exception e) {
                Log.e(e);
                A0E("Unable to load youtube html frame.", false);
                str2 = null;
            }
            this.A0F = str2;
            ViewGroup viewGroup = (ViewGroup) C12980iv.A0O(LayoutInflater.from(context), R.layout.inline_video_youtube_player);
            this.A0A = viewGroup;
            WebView webView = (WebView) AnonymousClass028.A0D(viewGroup, R.id.youtubeWebView);
            this.A0B = webView;
            YoutubePlayerTouchOverlay youtubePlayerTouchOverlay = (YoutubePlayerTouchOverlay) AnonymousClass028.A0D(viewGroup, R.id.youtubePlayerTouchOverlay);
            this.A0E = youtubePlayerTouchOverlay;
            youtubePlayerTouchOverlay.A01 = r16;
            youtubePlayerTouchOverlay.A00 = i;
            ViewGroup.LayoutParams layoutParams = youtubePlayerTouchOverlay.getLayoutParams();
            layoutParams.height = i;
            youtubePlayerTouchOverlay.setLayoutParams(layoutParams);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webView.getSettings().setUserAgentString(WebSettings.getDefaultUserAgent(context));
            webView.getSettings().setAllowFileAccess(false);
            webView.getSettings().setAllowUniversalAccessFromFileURLs(false);
            webView.addJavascriptInterface(new C64743Gs(this), "YoutubeJsInterface");
            String A02 = AnonymousClass3JH.A02(Uri.parse(C33771f3.A03(str, C33771f3.A03)));
            int i3 = 0;
            try {
                String queryParameter = Uri.parse(str).getQueryParameter("t");
                if (queryParameter != null) {
                    if (queryParameter.contains("h")) {
                        String[] split = queryParameter.split("h");
                        i2 = (Integer.parseInt(split[0]) * 3600) + 0;
                        queryParameter = split[1];
                    } else {
                        i2 = 0;
                    }
                    if (queryParameter.contains("m")) {
                        String[] split2 = queryParameter.split("m");
                        i2 += Integer.parseInt(split2[0]) * 60;
                        queryParameter = split2[1];
                    }
                    i3 = i2 + Integer.parseInt(queryParameter.contains("s") ? queryParameter.split("s")[0] : queryParameter);
                }
            } catch (Exception unused) {
            }
            if (A02 == null) {
                A0E("Unable to parse youtube id.", false);
                return;
            }
            try {
                JSONObject jSONObject2 = new JSONObject();
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("onReady", "onPlayerReady").put("onError", "onPlayerError").put("onStateChange", "onPlayerStateChange");
                jSONObject2.put("start", i3).put("rel", 0).put("modestbranding", 0).put("iv_load_policy", 3).put("autohide", 1).put("autoplay", 1).put("cc_load_policy", 1).put("playsinline", 1).put("controls", 0);
                jSONObject = new JSONObject().put("videoId", A02).put("events", jSONObject3).put("height", "100%").put("width", "100%").put("playerVars", jSONObject2);
            } catch (Exception e2) {
                Log.e(e2);
                jSONObject = null;
            }
            this.A05 = jSONObject;
            if (jSONObject == null) {
                A0E("Invalid player params.", true);
                return;
            }
            webView.setWebViewClient(new C52652bS(r14, this));
            this.A08 = new Bitmap[]{bitmap};
            webView.setWebChromeClient(new C74053hE(this));
            return;
        }
        Log.e("InlineYoutubeVideoPlayer/constructor instance created on version <19");
        throw C12970iu.A0f("InlineVideoPlayback is not supported on version <19");
    }

    @Override // X.AnonymousClass21T
    public int A01() {
        return this.A01;
    }

    @Override // X.AnonymousClass21T
    public int A02() {
        long j = this.A04;
        if (j == -9223372036854775807L) {
            return 0;
        }
        return (int) j;
    }

    @Override // X.AnonymousClass21T
    public View A04() {
        return this.A0A;
    }

    @Override // X.AnonymousClass21T
    public void A05() {
        if (this.A07) {
            Log.i("InlineYoutubeVideoPlayer/pause");
            this.A0B.loadUrl("javascript:(function() { player.pauseVideo(); })()");
            this.A02 = 2;
            this.A00 = 2;
            AnonymousClass39B r1 = this.A0D;
            r1.A00();
            r1.A08 = true;
        }
    }

    @Override // X.AnonymousClass21T
    public void A07() {
        if (!this.A06) {
            Log.i("InlineYoutubeVideoPlayer/start");
            this.A06 = true;
            WebView webView = this.A0B;
            Locale locale = Locale.US;
            String str = this.A0F;
            AnonymousClass009.A04(str);
            JSONObject jSONObject = this.A05;
            AnonymousClass009.A05(jSONObject);
            webView.loadDataWithBaseURL("https://whatsapp.com", String.format(locale, str, jSONObject.toString()), "text/html", AnonymousClass01V.A08, "https://youtube.com");
            return;
        }
        this.A0B.loadUrl("javascript:(function() { player.playVideo(); })()");
        this.A02 = 1;
        this.A00 = 1;
        AnonymousClass39B r0 = this.A0D;
        r0.A0E();
        r0.A08 = true;
    }

    @Override // X.AnonymousClass21T
    public void A08() {
        Log.i("InlineYoutubeVideoPlayer/stop");
        YoutubePlayerTouchOverlay youtubePlayerTouchOverlay = this.A0E;
        if (youtubePlayerTouchOverlay.getChildCount() > 0) {
            youtubePlayerTouchOverlay.removeAllViews();
        }
        WebView webView = this.A0B;
        webView.removeJavascriptInterface("YoutubeJsInterface");
        webView.stopLoading();
        webView.destroy();
        this.A01 = 0;
        this.A03 = -1;
        this.A00 = 0;
        this.A02 = 1;
        this.A07 = false;
        this.A06 = false;
        this.A04 = -9223372036854775807L;
    }

    @Override // X.AnonymousClass21T
    public void A09(int i) {
        if (this.A07) {
            StringBuilder A0k = C12960it.A0k("InlineYoutubeVideoPlayer/seekTo: ");
            int i2 = i / 1000;
            A0k.append(i2);
            C12960it.A1F(A0k);
            WebView webView = this.A0B;
            StringBuilder A0k2 = C12960it.A0k("javascript:(function() { player.seekTo(");
            A0k2.append(i2);
            webView.loadUrl(C12960it.A0d(", true); })()", A0k2));
            this.A01 = i;
        }
    }

    @Override // X.AnonymousClass21T
    public boolean A0B() {
        return C12970iu.A1W(this.A02);
    }

    public final void A0E(String str, boolean z) {
        String A0d = C12960it.A0d(str, C12960it.A0k("InlineYoutubeVideoPlayer: "));
        AnonymousClass5V3 r0 = super.A02;
        if (r0 != null) {
            r0.APt(A0d, z);
        }
    }
}
