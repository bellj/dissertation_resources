package X;

import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.5v8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127835v8 {
    public final C15570nT A00;
    public final C14830m7 A01;
    public final C130705zq A02;
    public final C18590sh A03;
    public final JniBridge A04;

    public C127835v8(C15570nT r1, C14830m7 r2, C130705zq r3, C18590sh r4, JniBridge jniBridge) {
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = jniBridge;
        this.A02 = r3;
        this.A03 = r4;
    }
}
