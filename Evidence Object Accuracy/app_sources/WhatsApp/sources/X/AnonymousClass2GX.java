package X;

import android.content.res.Configuration;

/* renamed from: X.2GX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GX extends AnonymousClass015 {
    public int A00;
    public final AnonymousClass016 A01 = new AnonymousClass016();

    public void A04(Configuration configuration) {
        int i;
        int i2 = AnonymousClass025.A00;
        if ((i2 != 3 && i2 != -1) || this.A00 == (i = configuration.uiMode & 48)) {
            this.A01.A0B(0);
        } else if (i == 16 || i == 32) {
            this.A01.A0B(1);
        }
    }
}
