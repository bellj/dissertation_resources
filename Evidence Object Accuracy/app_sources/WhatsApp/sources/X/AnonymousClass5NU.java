package X;

import java.io.IOException;

/* renamed from: X.5NU  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5NU extends AnonymousClass1TL implements AbstractC117255Zd {
    public final int A00;
    public final AnonymousClass1TN A01;
    public final boolean A02;

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        boolean z = this.A02;
        return new C114835Ng(this.A01, this.A00, z);
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000c, code lost:
        if ((r3 instanceof X.AbstractC115545Ry) != false) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass5NU(X.AnonymousClass1TN r3, int r4, boolean r5) {
        /*
            r2 = this;
            r2.<init>()
            if (r3 == 0) goto L_0x0014
            r2.A00 = r4
            if (r5 != 0) goto L_0x000e
            boolean r1 = r3 instanceof X.AbstractC115545Ry
            r0 = 0
            if (r1 == 0) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            r2.A02 = r0
            r2.A01 = r3
            return
        L_0x0014:
            java.lang.String r0 = "'obj' cannot be null"
            java.lang.NullPointerException r0 = X.C12980iv.A0n(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5NU.<init>(X.1TN, int, boolean):void");
    }

    public static AnonymousClass1TL A00(AnonymousClass5NU r0) {
        return r0.A01.Aer();
    }

    public static AnonymousClass5NU A01(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NU)) {
            return (AnonymousClass5NU) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return A01(AnonymousClass1TL.A03((byte[]) obj));
            } catch (IOException e) {
                throw C12970iu.A0f(C12960it.A0d(e.getMessage(), C12960it.A0k("failed to construct tagged object from byte[]: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("unknown object in getInstance: ")));
        }
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A07() {
        if ((this instanceof C114825Nf) || (this instanceof C114835Ng)) {
            return this;
        }
        boolean z = this.A02;
        return new C114825Nf(this.A01, this.A00, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009a, code lost:
        if (r3.A09() != false) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c6, code lost:
        if (r3.A09() != false) goto L_0x00c8;
     */
    @Override // X.AnonymousClass1TL
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.AnonymousClass1TP r6, boolean r7) {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C114825Nf
            if (r0 != 0) goto L_0x00b4
            boolean r0 = r5 instanceof X.C114835Ng
            if (r0 != 0) goto L_0x0088
            int r1 = r5.A00
            r0 = 160(0xa0, float:2.24E-43)
            r6.A03(r0, r1, r7)
            r0 = 128(0x80, float:1.794E-43)
            java.io.OutputStream r3 = r6.A00
            r3.write(r0)
            boolean r0 = r5.A02
            X.1TN r4 = r5.A01
            if (r0 != 0) goto L_0x0078
            boolean r0 = r4 instanceof X.AnonymousClass5NH
            if (r0 == 0) goto L_0x004d
            boolean r0 = r4 instanceof X.AnonymousClass5N6
            if (r0 == 0) goto L_0x003f
            X.5N6 r4 = (X.AnonymousClass5N6) r4
            java.util.Enumeration r2 = r4.A0B()
        L_0x002a:
            boolean r0 = r2.hasMoreElements()
            if (r0 == 0) goto L_0x0080
            java.lang.Object r0 = r2.nextElement()
            X.1TN r0 = (X.AnonymousClass1TN) r0
            X.1TL r1 = r0.Aer()
            r0 = 1
            r6.A04(r1, r0)
            goto L_0x002a
        L_0x003f:
            X.5NH r4 = (X.AnonymousClass5NH) r4
            byte[] r1 = r4.A00
            X.5N6 r0 = new X.5N6
            r0.<init>(r1)
            java.util.Enumeration r2 = r0.A0B()
            goto L_0x002a
        L_0x004d:
            boolean r0 = r4 instanceof X.AbstractC114775Na
            if (r0 == 0) goto L_0x0058
            X.5Na r4 = (X.AbstractC114775Na) r4
            java.util.Enumeration r2 = r4.A0C()
            goto L_0x002a
        L_0x0058:
            boolean r0 = r4 instanceof X.AnonymousClass5NV
            if (r0 == 0) goto L_0x0064
            X.5NV r4 = (X.AnonymousClass5NV) r4
            X.5D0 r2 = new X.5D0
            r2.<init>(r4)
            goto L_0x002a
        L_0x0064:
            java.lang.String r0 = "not implemented: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12980iv.A0s(r4)
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            X.492 r0 = new X.492
            r0.<init>(r1)
            throw r0
        L_0x0078:
            X.1TL r1 = r4.Aer()
            r0 = 1
            r6.A04(r1, r0)
        L_0x0080:
            r0 = 0
            r3.write(r0)
            r3.write(r0)
            return
        L_0x0088:
            X.1TL r0 = A00(r5)
            X.1TL r3 = r0.A06()
            boolean r2 = r5.A02
            if (r2 != 0) goto L_0x009c
            boolean r0 = r3.A09()
            r1 = 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x009e
        L_0x009c:
            r1 = 160(0xa0, float:2.24E-43)
        L_0x009e:
            int r0 = r5.A00
            r6.A03(r1, r0, r7)
            if (r2 == 0) goto L_0x00ac
            int r0 = r3.A05()
            r6.A02(r0)
        L_0x00ac:
            X.5N8 r0 = r6.A01()
            r3.A08(r0, r2)
            return
        L_0x00b4:
            X.1TL r0 = A00(r5)
            X.1TL r3 = r0.A07()
            boolean r2 = r5.A02
            if (r2 != 0) goto L_0x00c8
            boolean r0 = r3.A09()
            r1 = 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x00ca
        L_0x00c8:
            r1 = 160(0xa0, float:2.24E-43)
        L_0x00ca:
            int r0 = r5.A00
            r6.A03(r1, r0, r7)
            if (r2 == 0) goto L_0x00d8
            int r0 = r3.A05()
            r6.A02(r0)
        L_0x00d8:
            X.1TP r0 = r6.A00()
            r0.A04(r3, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5NU.A08(X.1TP, boolean):void");
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r4) {
        if (!(r4 instanceof AnonymousClass5NU)) {
            return false;
        }
        AnonymousClass5NU r42 = (AnonymousClass5NU) r4;
        if (this.A00 != r42.A00 || this.A02 != r42.A02) {
            return false;
        }
        AnonymousClass1TL A00 = A00(this);
        AnonymousClass1TL A002 = A00(r42);
        if (A00 == A002 || A00.A0A(A002)) {
            return true;
        }
        return false;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        int i = this.A00;
        int i2 = 240;
        if (this.A02) {
            i2 = 15;
        }
        return (i ^ i2) ^ A00(this).hashCode();
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[");
        A0k.append(this.A00);
        A0k.append("]");
        return C12970iu.A0s(this.A01, A0k);
    }
}
