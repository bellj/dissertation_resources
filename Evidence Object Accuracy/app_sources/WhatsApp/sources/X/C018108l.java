package X;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;

/* renamed from: X.08l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C018108l {
    public Bundle A03() {
        return null;
    }

    public static C018108l A00() {
        if (Build.VERSION.SDK_INT >= 23) {
            return new AnonymousClass0D5(AnonymousClass0KP.A00());
        }
        return new C018108l();
    }

    public static C018108l A01(Activity activity, View view, String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new AnonymousClass0D5(C05590Qd.A00(activity, view, str));
        }
        return new C018108l();
    }

    public static C018108l A02(Activity activity, AnonymousClass01T... r6) {
        if (Build.VERSION.SDK_INT < 21) {
            return new C018108l();
        }
        Pair[] pairArr = null;
        if (r6 != null) {
            int length = r6.length;
            pairArr = new Pair[length];
            for (int i = 0; i < length; i++) {
                AnonymousClass01T r0 = r6[i];
                pairArr[i] = Pair.create(r0.A00, r0.A01);
            }
        }
        return new AnonymousClass0D5(C05590Qd.A01(activity, pairArr));
    }
}
