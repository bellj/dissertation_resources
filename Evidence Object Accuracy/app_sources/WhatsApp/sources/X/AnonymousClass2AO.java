package X;

import android.os.SystemClock;
import android.view.Window;
import android.view.WindowManager;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.2AO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AO implements AnonymousClass2AQ {
    public final /* synthetic */ AnonymousClass1s8 A00;

    public AnonymousClass2AO(AnonymousClass1s8 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2AQ
    public void ATk(byte[] bArr, boolean z) {
        int i;
        AnonymousClass1s8 r3 = this.A00;
        if (r3.A08 != null) {
            Log.i("cameraui/picturetaken");
            boolean z2 = false;
            if (bArr == null) {
                z2 = true;
            }
            AnonymousClass1AX r11 = r3.A0e;
            AnonymousClass1s9 r1 = r3.A0A;
            Integer valueOf = Integer.valueOf(r1.getCameraApi());
            int cameraType = r1.getCameraType();
            int i2 = !r1.AJS();
            String flashMode = r1.getFlashMode();
            String l = Long.toString(r1.getPictureResolution());
            if (!flashMode.equals("on")) {
                i = 2;
                if (!flashMode.equals("auto")) {
                    i = 0;
                }
            } else {
                i = 1;
            }
            long elapsedRealtime = SystemClock.elapsedRealtime() - r11.A00;
            C614830n r12 = new C614830n();
            r12.A02 = Integer.valueOf(cameraType);
            r12.A00 = valueOf;
            r12.A01 = Integer.valueOf(i2);
            r12.A03 = Integer.valueOf(i);
            r12.A05 = l;
            r12.A04 = Long.valueOf(elapsedRealtime);
            if (r11.A09) {
                r11.A07.A07(r12);
            }
            if (r11.A0A) {
                r11.A03(valueOf, 554240366, cameraType);
                r11.A00(554240366, i2);
                AbstractC21180x0 r13 = r11.A08;
                r13.AKw(554240366, "flash_mode", flashMode);
                r13.AKw(554240366, "requested_photo_resolution", l);
                short s = 2;
                if (z2) {
                    s = 87;
                }
                r13.AL7(554240366, s);
            }
            r3.A0D.A0B.setVisibility(0);
            AnonymousClass2U3 r14 = r3.A0D;
            Window window = r3.A02().getWindow();
            WindowManager.LayoutParams attributes = window.getAttributes();
            r14.A04.setVisibility(4);
            attributes.screenBrightness = -1.0f;
            window.setAttributes(attributes);
            if (z2) {
                r3.A0b.A07(R.string.camera_failed, 1);
                r3.A0x.ANa();
                return;
            }
            if (r3.A08 != null) {
                if (AnonymousClass2BK.A01(r3.A0f, r3.A0n) < 2013) {
                    r3.A02().findViewById(R.id.save_progress).setVisibility(0);
                }
            }
            C14330lG r4 = r3.A0a;
            C16630pM r5 = r3.A0n;
            C14370lK r6 = C14370lK.A0B;
            int i3 = 1;
            if (r3.A0l.A07(401)) {
                i3 = 4;
            }
            r3.A0K = C22200yh.A0G(r4, r5, r6, ".jpeg", 0, i3);
            r3.A0p.Ab5(new C624637i(r3.A02(), new C89434Jy(r3), r3.A0K, bArr, r3.A01(), z), new Void[0]);
        }
    }

    @Override // X.AnonymousClass2AQ
    public void onShutter() {
        AnonymousClass1s8 r3 = this.A00;
        AnonymousClass1AX r1 = r3.A0e;
        if (r1.A0A) {
            r1.A08.ALB(554240366, "on_shutter");
        }
        r3.A0b.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(this, 29));
    }
}
