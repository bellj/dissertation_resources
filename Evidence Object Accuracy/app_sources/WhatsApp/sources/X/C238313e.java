package X;

import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;

/* renamed from: X.13e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238313e implements AbstractC18870tC {
    public C42981wD A00;
    public C42991wE A01;
    public boolean A02;
    public boolean A03;
    public final C16240og A04;
    public final C14830m7 A05;
    public final AbstractC14440lR A06;

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARC() {
    }

    public C238313e(C16240og r1, C14830m7 r2, AbstractC14440lR r3) {
        this.A05 = r2;
        this.A06 = r3;
        this.A04 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003c, code lost:
        if (r13 == false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A00(boolean r13) {
        /*
            r12 = this;
            r5 = r12
            monitor-enter(r5)
            r0 = 1
            r12.A02 = r0     // Catch: all -> 0x0048
            boolean r0 = r12.A03     // Catch: all -> 0x0048
            r12.A03 = r13     // Catch: all -> 0x0048
            if (r0 == 0) goto L_0x003c
            if (r13 != 0) goto L_0x0046
            X.1wD r0 = r12.A00     // Catch: all -> 0x0048
            if (r0 == 0) goto L_0x003e
            X.1wE r0 = r12.A01     // Catch: all -> 0x0048
            if (r0 == 0) goto L_0x003e
            long r3 = r0.A00     // Catch: all -> 0x0048
            r0 = 20000(0x4e20, double:9.8813E-320)
            long r3 = r3 + r0
            X.0m7 r0 = r12.A05     // Catch: all -> 0x0048
            long r1 = r0.A00()     // Catch: all -> 0x0048
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x003e
            X.1wD r7 = r12.A00     // Catch: all -> 0x0048
            X.1wE r0 = r12.A01     // Catch: all -> 0x0048
            X.1IS r8 = r0.A01     // Catch: all -> 0x0048
            boolean r10 = r0.A03     // Catch: all -> 0x0048
            boolean r11 = r0.A02     // Catch: all -> 0x0048
            X.0vP r0 = r7.A00     // Catch: all -> 0x0048
            X.0lR r0 = r0.A0S     // Catch: all -> 0x0048
            r9 = 2
            com.facebook.redex.RunnableBRunnable0Shape0S0220000_I0 r6 = new com.facebook.redex.RunnableBRunnable0Shape0S0220000_I0     // Catch: all -> 0x0048
            r6.<init>(r7, r8, r9, r10, r11)     // Catch: all -> 0x0048
            r0.Ab2(r6)     // Catch: all -> 0x0048
            goto L_0x003e
        L_0x003c:
            if (r13 != 0) goto L_0x0046
        L_0x003e:
            r0 = 0
            r12.A01 = r0     // Catch: all -> 0x0044
            r12.A00 = r0     // Catch: all -> 0x0044
            goto L_0x0046
        L_0x0044:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0048
        L_0x0046:
            monitor-exit(r5)
            return
        L_0x0048:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C238313e.A00(boolean):void");
    }

    @Override // X.AbstractC18870tC
    public void AR9() {
        boolean z;
        synchronized (this) {
            this.A02 = false;
            z = this.A03;
        }
        if (z) {
            this.A06.AbK(new RunnableBRunnable0Shape9S0100000_I0_9(this, 5), "PeerPresenceManager/onHandlerConnected", 10000);
        }
    }
}
