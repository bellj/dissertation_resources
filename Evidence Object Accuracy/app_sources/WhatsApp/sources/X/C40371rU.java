package X;

import com.whatsapp.jid.GroupJid;

/* renamed from: X.1rU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40371rU extends AnonymousClass1JW {
    public final GroupJid A00;
    public final AnonymousClass1XB A01;

    public C40371rU(AbstractC15710nm r1, C15450nH r2, GroupJid groupJid, AnonymousClass1XB r4, boolean z) {
        super(r1, r2, z);
        this.A01 = r4;
        this.A00 = groupJid;
    }
}
