package X;

import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.12k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C236312k {
    public final C14850m9 A00;
    public final C21230x5 A01;
    public final ConcurrentHashMap A02 = new ConcurrentHashMap();

    public C236312k(C14850m9 r2, C21230x5 r3) {
        this.A01 = r3;
        this.A00 = r2;
    }

    public void A00(String str) {
        Number number = (Number) this.A02.remove(str);
        if (number != null) {
            this.A01.AL8("start_foreground_service", number.intValue(), str.hashCode(), 213);
        }
    }

    public void A01(String str, int i, boolean z, boolean z2) {
        Number number = (Number) this.A02.get(str);
        if (number != null) {
            C21230x5 r3 = this.A01;
            int intValue = number.intValue();
            int hashCode = str.hashCode();
            r3.AL1("is_video_call", intValue, hashCode, z);
            r3.AKy("peer_participants_count", intValue, hashCode, i);
            r3.AL1("is_rejoin", intValue, hashCode, z2);
        }
    }

    public void A02(String str, String str2) {
        Number number = (Number) this.A02.get(str);
        if (number != null) {
            this.A01.ALC(number.intValue(), str2, str.hashCode());
        }
    }

    public void A03(String str, short s) {
        Number number = (Number) this.A02.remove(str);
        if (number != null) {
            this.A01.AL6(number.intValue(), str.hashCode(), s);
        }
    }

    public final boolean A04(String str, int i) {
        ConcurrentHashMap concurrentHashMap = this.A02;
        Number number = (Number) concurrentHashMap.get(str);
        if (!this.A00.A07(620)) {
            return false;
        }
        if (number != null && number.intValue() == i) {
            return false;
        }
        this.A01.ALF(i, str.hashCode());
        concurrentHashMap.put(str, Integer.valueOf(i));
        return true;
    }
}
