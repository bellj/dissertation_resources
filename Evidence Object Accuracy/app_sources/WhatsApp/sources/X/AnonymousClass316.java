package X;

/* renamed from: X.316  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass316 extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;

    public AnonymousClass316() {
        super(3502, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(5, this.A04);
        r3.Abe(6, this.A05);
        r3.Abe(7, this.A06);
        r3.Abe(8, this.A07);
        r3.Abe(9, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamBizIntegrityBannerEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizIntegrityBannerBannerType", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizIntegrityBannerEventSource", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bizIntegrityBannerEventType", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeSpent", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "violationPriority", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "violationSeverity", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "violationSource", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "violationStatus", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "violationType", this.A08);
        return C12960it.A0d("}", A0k);
    }
}
