package X;

import android.content.Context;
import android.graphics.Matrix;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.04Q  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass04Q implements AnonymousClass04R {
    public float A00 = 21.0f;
    public float A01 = 2.0f;
    public float A02;
    public float A03;
    public int A04;
    public int A05;
    public int A06;
    public AbstractC12500i1 A07;
    public AbstractC12150hS A08;
    public AbstractC12160hT A09;
    public AbstractC12170hU A0A;
    public AbstractC12180hV A0B;
    public AbstractC12190hW A0C;
    public AnonymousClass03S A0D;
    public AnonymousClass04L A0E;
    public AnonymousClass0I9 A0F;
    public AnonymousClass0IA A0G;
    public AnonymousClass0IB A0H;
    public C06470Tt A0I;
    public C06470Tt A0J;
    public C06470Tt A0K;
    public C06470Tt A0L;
    public boolean A0M = false;
    public boolean A0N;
    public final int A0O;
    public final Context A0P;
    public final Matrix A0Q = new Matrix();
    public final AnonymousClass0U9 A0R;
    public final C05110Oh A0S;
    public final AnonymousClass0IR A0T;
    public final AnonymousClass0VE A0U;
    public final ArrayList A0V = new ArrayList();
    public final List A0W = new ArrayList();
    public final float[] A0X = new float[2];

    public AnonymousClass04Q(AnonymousClass0O0 r6, AnonymousClass04L r7) {
        String str;
        this.A0E = r7;
        Context applicationContext = r7.getContext().getApplicationContext();
        this.A0P = applicationContext;
        this.A0R = new AnonymousClass0U9(this);
        C05110Oh r4 = new C05110Oh(this);
        this.A0S = r4;
        Context applicationContext2 = applicationContext.getApplicationContext();
        AnonymousClass03Q.A02 = applicationContext2;
        AnonymousClass03Q.A00 = applicationContext2.getResources().getDisplayMetrics().density;
        int i = applicationContext.getResources().getDisplayMetrics().densityDpi >= 320 ? 512 : 256;
        this.A0O = i;
        AnonymousClass0IR r3 = new AnonymousClass0IR(this, new AnonymousClass0IC(applicationContext, i));
        A0C(r3);
        this.A0T = r3;
        AnonymousClass0VE r2 = new AnonymousClass0VE(this.A0E.getContext());
        this.A0U = r2;
        C04630Ml r1 = new C04630Ml(this);
        r2.A01 = r1;
        if (r2.A00 != null && r2.A03) {
            r1.A00.A0E.invalidate();
        }
        if (r6 != null) {
            boolean z = r6.A02;
            AnonymousClass04Q r12 = r4.A00;
            AnonymousClass0I9 r0 = r12.A0F;
            if (z) {
                if (r0 == null) {
                    AnonymousClass0I9 r02 = new AnonymousClass0I9(r12);
                    r12.A0F = r02;
                    r12.A0C(r02);
                }
            } else if (r0 != null) {
                r12.A0D(r0);
                r12.A0F = null;
            }
            r4.A02 = r6.A03;
            r4.A03 = r6.A04;
            r4.A04 = r6.A05;
            this.A00 = Math.min(Math.max(21.0f, 2.0f), 21.0f);
            this.A01 = Math.min(Math.max(2.0f, 2.0f), 21.0f);
            int i2 = r6.A00;
            if (i2 != r3.A00) {
                r3.A00 = i2;
                if (i2 == 0) {
                    r3.A09(false);
                    return;
                }
                if (!((AnonymousClass03S) r3).A04) {
                    r3.A09(true);
                }
                AnonymousClass0IC r13 = r3.A03;
                if (i2 == 5) {
                    str = "live_maps";
                } else if (i2 == 6) {
                    str = "crowdsourcing_osm";
                } else if (i2 != 7) {
                    str = null;
                } else {
                    str = "indoor_osm";
                }
                r13.A00 = str;
                AnonymousClass04Q r03 = ((AnonymousClass03S) r3).A09;
                r03.A04();
                r03.A0E.invalidate();
            }
        }
    }

    public final float A00() {
        return ((float) 0) + (((float) ((this.A0E.A0F - 0) - this.A05)) / 2.0f);
    }

    public final float A01() {
        int i = this.A06;
        return ((float) i) + (((float) ((this.A0E.A0D - i) - this.A04)) / 2.0f);
    }

    public final C06860Vj A02() {
        float[] fArr = this.A0X;
        AnonymousClass04L r7 = this.A0E;
        fArr[0] = r7.A04 - A00();
        fArr[1] = r7.A05 - A01();
        r7.A0l.mapVectors(fArr);
        double d = r7.A02;
        float f = fArr[0];
        float f2 = (float) r7.A0J;
        return new C06860Vj(new AnonymousClass03T(AnonymousClass0U9.A00(r7.A03 - ((double) (fArr[1] / f2))), ((d - ((double) (f / f2))) * 360.0d) - 180.0d), r7.getZoom(), 0.0f, r7.A0B);
    }

    public final AnonymousClass03R A03(C06050Rz r2) {
        AnonymousClass03R r0 = new AnonymousClass03R(this, r2);
        A0C(r0);
        r0.A0I = this;
        return r0;
    }

    public void A04() {
        List list = this.A0W;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass03S r7 = (AnonymousClass03S) list.get(i);
            if (r7 instanceof AnonymousClass0IT) {
                AnonymousClass0IT r72 = (AnonymousClass0IT) r7;
                r72.A00 = 0;
                ArrayList arrayList = AnonymousClass0IR.A06;
                arrayList.remove(r72);
                if (arrayList.isEmpty()) {
                    AnonymousClass0Q8 r1 = r72.A0A;
                    if (r1.A03 != -1) {
                        r1.A03 = -1;
                    }
                }
                int[] iArr = r72.A0B;
                AnonymousClass0IR.A00(iArr);
                AnonymousClass0Q8 r12 = r72.A0A;
                r12.A01 = iArr[0];
                r12.A02 = iArr[1];
                r12.A01();
            }
        }
    }

    public void A05() {
        if (this.A09 != null || !this.A0V.isEmpty()) {
            C06860Vj A02 = A02();
            AbstractC12160hT r0 = this.A09;
            if (r0 != null) {
                r0.ANZ(A02);
            }
            ArrayList arrayList = this.A0V;
            if (!arrayList.isEmpty()) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((AbstractC12160hT) it.next()).ANZ(A02);
                }
            }
        }
    }

    public final void A06() {
        Iterator it = this.A0W.iterator();
        while (it.hasNext()) {
            int i = ((AnonymousClass03S) it.next()).A03;
            if (i == 1 || i == 2 || i == 4) {
                it.remove();
            }
        }
        this.A0E.invalidate();
    }

    public final void A07() {
        C06470Tt r0 = this.A0J;
        if (r0 != null) {
            r0.A01();
        }
        C06470Tt r02 = this.A0K;
        if (r02 != null) {
            r02.A01();
        }
        C06470Tt r03 = this.A0L;
        if (r03 != null) {
            r03.A01();
        }
        C06470Tt r04 = this.A0I;
        if (r04 != null) {
            r04.A01();
        }
    }

    public final void A08(int i, int i2, int i3) {
        AnonymousClass04L r6 = this.A0E;
        double d = r6.A02;
        long j = r6.A0J << 1;
        r6.A0A(d + ((double) (((long) ((0 - this.A05) - (0 - i2))) / j)), r6.A03 + ((double) (((long) ((this.A06 - this.A04) - (i - i3))) / j)));
        this.A06 = i;
        this.A05 = i2;
        this.A04 = i3;
        r6.requestLayout();
        r6.invalidate();
    }

    public final void A09(C05200Oq r3) {
        A0B(r3, null, 1500);
    }

    public final void A0A(C05200Oq r3) {
        A0B(r3, null, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0112  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B(X.C05200Oq r19, X.AbstractC12500i1 r20, int r21) {
        /*
        // Method dump skipped, instructions count: 580
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04Q.A0B(X.0Oq, X.0i1, int):void");
    }

    public final void A0C(AnonymousClass03S r3) {
        List list = this.A0W;
        int binarySearch = Collections.binarySearch(list, r3, AnonymousClass03S.A0E);
        if (binarySearch <= 0) {
            list.add(-1 - binarySearch, r3);
            r3.A07();
            this.A0E.invalidate();
        }
    }

    public final void A0D(AnonymousClass03S r2) {
        this.A0W.remove(r2);
        this.A0E.invalidate();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (X.AnonymousClass00T.A01(r1, "android.permission.ACCESS_FINE_LOCATION") == 0) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0E(boolean r3) {
        /*
            r2 = this;
            android.content.Context r1 = r2.A0P
            java.lang.String r0 = "android.permission.ACCESS_COARSE_LOCATION"
            int r0 = X.AnonymousClass00T.A01(r1, r0)
            if (r0 == 0) goto L_0x0013
            java.lang.String r0 = "android.permission.ACCESS_FINE_LOCATION"
            int r1 = X.AnonymousClass00T.A01(r1, r0)
            r0 = 0
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            r0 = 1
        L_0x0014:
            r2.A0N = r0
            r3 = r3 & r0
            X.0VE r0 = r2.A0U
            r0.A01(r3)
            if (r3 == 0) goto L_0x003d
            X.0IB r0 = r2.A0H
            if (r0 != 0) goto L_0x0037
            X.0IB r0 = new X.0IB
            r0.<init>(r2)
            r2.A0H = r0
            r2.A0C(r0)
            X.0IB r0 = r2.A0H
            X.0Tt r1 = r0.A0E
            boolean r0 = r1.A0I
            if (r0 != 0) goto L_0x0037
            r1.A03()
        L_0x0037:
            X.0Oh r0 = r2.A0S
            r0.A00()
            return
        L_0x003d:
            X.0IB r1 = r2.A0H
            if (r1 == 0) goto L_0x0037
            X.0Tt r0 = r1.A0E
            r0.A01()
            r1.A01()
            X.0IB r0 = r2.A0H
            r2.A0D(r0)
            r0 = 0
            r2.A0H = r0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04Q.A0E(boolean):void");
    }

    @Override // X.AnonymousClass04R
    public void AME(C06470Tt r7) {
        AnonymousClass04L r3;
        double d;
        double d2;
        C06470Tt r0 = this.A0J;
        if (r7 == r0) {
            r3 = this.A0E;
            d = (double) r0.A00;
            d2 = r3.A03;
        } else {
            C06470Tt r02 = this.A0K;
            if (r7 == r02) {
                r3 = this.A0E;
                d = r3.A02;
                d2 = (double) r02.A00;
            } else {
                if (r7 == this.A0L) {
                    r3 = this.A0E;
                    if (r3.A0H(r7.A00, this.A02, this.A03)) {
                        r3.A0M.A05();
                    }
                } else if (r7 == this.A0I) {
                    r3 = this.A0E;
                    r3.A0B(r7.A00, A00(), A01());
                } else {
                    return;
                }
                r3.invalidate();
            }
        }
        r3.A0A(d, d2);
        r3.invalidate();
    }
}
