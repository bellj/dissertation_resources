package X;

import android.database.sqlite.SQLiteDatabase;

/* renamed from: X.0oj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16270oj extends AbstractC16280ok {
    public final C16590pI A00;
    public final C231410n A01;

    public C16270oj(AbstractC15710nm r8, C16590pI r9, C231410n r10) {
        super(r9.A00, r8, "media.db", null, 24, true);
        this.A00 = r9;
        this.A01 = r10;
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        return AnonymousClass1Tx.A01(super.A00(), this.A01);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS media_job");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS media_experiments");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS web_upload_media_data_store");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS draft_voice_note_metadata");
        sQLiteDatabase.execSQL("CREATE TABLE media_job (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,uuid TEXT NOT NULL, job_type INTEGER, create_time TIMESTAMP, transfer_start_time TIMESTAMP, last_update_time TIMESTAMP, user_initiated_attempt_count INTEGER, overall_cumulative_time TIMESTAMP, overall_cumulative_user_visible_time TIMESTAMP, streaming_playback_count INTEGER, media_key_reuse_type INTEGER, doodle_id TEXT, transferred_bytes INTEGER,reupload_attempt_count INTEGER,last_reupload_attempt_timestamp TIMESTAMP,last_reupload_success_timestamp TIMESTAMP)");
        sQLiteDatabase.execSQL("CREATE TABLE web_upload_media_data_store (media_id TEXT PRIMARY KEY NOT NULL, file_hash TEXT, media_key BLOB, mime_type TEXT, upload_url TEXT, direct_path TEXT, enc_file_hash TEXT, file_size INTEGER, width INTEGER, height INTEGER)");
        sQLiteDatabase.execSQL("CREATE TABLE shared_media_ids (item_uuid TEXT PRIMARY KEY NOT NULL, file_name TEXT NOT NULL, mime_type TEXT NOT NULL, display_name TEXT, expiration_timestamp INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("CREATE TABLE draft_voice_note_metadata (chat_jid TEXT PRIMARY KEY NOT NULL, page_number INTEGER)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS media_job");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS media_experiments");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS web_upload_media_key_store");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS web_upload_media_data_store");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS shared_media_ids");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS draft_voice_note_metadata");
        onCreate(sQLiteDatabase);
    }

    @Override // X.AbstractC16280ok, android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        sQLiteDatabase.execSQL("PRAGMA synchronous=NORMAL;");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS media_job");
        sQLiteDatabase.execSQL("CREATE TABLE media_job (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,uuid TEXT NOT NULL, job_type INTEGER, create_time TIMESTAMP, transfer_start_time TIMESTAMP, last_update_time TIMESTAMP, user_initiated_attempt_count INTEGER, overall_cumulative_time TIMESTAMP, overall_cumulative_user_visible_time TIMESTAMP, streaming_playback_count INTEGER, media_key_reuse_type INTEGER, doodle_id TEXT, transferred_bytes INTEGER,reupload_attempt_count INTEGER,last_reupload_attempt_timestamp TIMESTAMP,last_reupload_success_timestamp TIMESTAMP)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS media_experiments");
        if (i < 16) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS web_upload_media_key_store");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS web_upload_media_data_store");
            sQLiteDatabase.execSQL("CREATE TABLE web_upload_media_data_store (media_id TEXT PRIMARY KEY NOT NULL, file_hash TEXT, media_key BLOB, mime_type TEXT, upload_url TEXT, direct_path TEXT, enc_file_hash TEXT, file_size INTEGER, width INTEGER, height INTEGER)");
        }
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS shared_media_ids");
        sQLiteDatabase.execSQL("CREATE TABLE shared_media_ids (item_uuid TEXT PRIMARY KEY NOT NULL, file_name TEXT NOT NULL, mime_type TEXT NOT NULL, display_name TEXT, expiration_timestamp INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS draft_voice_note_metadata");
        sQLiteDatabase.execSQL("CREATE TABLE draft_voice_note_metadata (chat_jid TEXT PRIMARY KEY NOT NULL, page_number INTEGER)");
    }
}
