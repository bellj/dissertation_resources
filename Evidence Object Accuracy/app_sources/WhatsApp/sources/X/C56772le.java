package X;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.languageselector.LanguageSelectorBottomSheet;

/* renamed from: X.2le  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56772le extends AnonymousClass2UH {
    public final /* synthetic */ BottomSheetBehavior A00;
    public final /* synthetic */ LanguageSelectorBottomSheet A01;

    public C56772le(BottomSheetBehavior bottomSheetBehavior, LanguageSelectorBottomSheet languageSelectorBottomSheet) {
        this.A01 = languageSelectorBottomSheet;
        this.A00 = bottomSheetBehavior;
    }
}
