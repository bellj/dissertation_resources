package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.03T  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass03T implements Parcelable {
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VP();
    public final double A00;
    public final double A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass03T(double d, double d2) {
        this.A00 = d;
        this.A01 = d2;
    }

    public /* synthetic */ AnonymousClass03T(Parcel parcel) {
        this.A00 = parcel.readDouble();
        this.A01 = parcel.readDouble();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass03T)) {
            return false;
        }
        AnonymousClass03T r7 = (AnonymousClass03T) obj;
        if (Math.abs(this.A00 - r7.A00) >= 0.002d || Math.abs(this.A01 - r7.A01) >= 2.0E-4d) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (int) (((527.0d + this.A00) * 31.0d) + this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("LatLng");
        sb.append("{latitude=");
        sb.append(this.A00);
        sb.append(", longitude=");
        sb.append(this.A01);
        sb.append("}");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.A00);
        parcel.writeDouble(this.A01);
    }
}
