package X;

import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.3Xu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69063Xu implements AnonymousClass5Z1 {
    public final int A00;
    public final C253719d A01;
    public final AbstractC14470lU A02;
    public final AnonymousClass3DC A03;
    public final String A04;

    public C69063Xu(C253719d r1, AbstractC14470lU r2, AnonymousClass3DC r3, String str, int i) {
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = str;
        this.A00 = i;
        this.A03 = r3;
    }

    @Override // java.lang.Runnable
    public void run() {
        File file;
        File file2;
        C253719d r1 = this.A01;
        String str = this.A04;
        int i = this.A00;
        AnonymousClass009.A00();
        AnonymousClass1O6 A05 = r1.A07.A05();
        AnonymousClass31y r8 = new AnonymousClass31y(r1.A02, r1.A03, r1.A05, r1.A06, r1.A08, r1.A09, A05, C253719d.A0D, str);
        C92384Vr A08 = r8.A08();
        AbstractC39381po r6 = r8.A06;
        String str2 = r8.A07;
        byte[] bArr = null;
        if (A08 == null) {
            file = null;
        } else {
            file = A08.A02;
            bArr = A08.A03;
        }
        r6.AQX(file, str2, bArr);
        C39391pp A00 = A05.A00(str);
        if (A00 != null) {
            file2 = new File(A00.A00);
        } else {
            file2 = null;
            StringBuilder A0k = C12960it.A0k("MediaLoadGifJob/failed to load, name: ");
            A0k.append(str);
            Log.e(C12960it.A0e(", attribution:", A0k, i));
        }
        this.A03.A00(file2, true);
    }
}
