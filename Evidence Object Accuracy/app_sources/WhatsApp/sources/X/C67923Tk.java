package X;

import java.util.Map;

/* renamed from: X.3Tk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67923Tk implements AbstractC17390qj {
    public final C63903Dj A00;

    public C67923Tk(C63903Dj r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC17390qj
    public C89994Me Ad6(C14260l7 r8, C14270l8 r9, AnonymousClass4ZD r10, C93484aF r11, C14240l5 r12) {
        C89994Me A00;
        Map map = r11.A02;
        String A0t = C12970iu.A0t("key", map);
        if (A0t != null) {
            Object A002 = AnonymousClass3AE.A00(r12, map);
            Object obj = map.get("mode");
            String str = r11.A00;
            boolean equals = "p".equals(obj);
            C63903Dj r2 = this.A00;
            synchronized (r2) {
                if (equals) {
                    r2.A01(A0t, A002);
                    A00 = r2.A00(r9, A002, A0t, str);
                } else {
                    Map map2 = r2.A01;
                    Object obj2 = map2.get(A0t);
                    if (obj2 == null) {
                        map2.put(A0t, A002);
                    } else {
                        A002 = obj2;
                    }
                    A00 = r2.A00(r9, A002, A0t, str);
                }
            }
            map.get("debug_metadata");
            return A00;
        }
        throw C12960it.A0U("Key not defined in data manifest");
    }
}
