package X;

/* renamed from: X.1V9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1V9 extends Exception {
    public static final long serialVersionUID = 1;
    public String bufString;

    public AnonymousClass1V9() {
    }

    public AnonymousClass1V9(String str) {
        super(str);
    }

    public AnonymousClass1V9(String str, String str2) {
        super(str);
        this.bufString = str2;
    }

    public AnonymousClass1V9(Throwable th) {
        super(th);
    }
}
