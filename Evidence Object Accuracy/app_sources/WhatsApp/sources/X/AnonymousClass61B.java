package X;

import android.media.Image;
import android.media.ImageReader;
import android.util.Pair;
import android.view.Surface;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: X.61B */
/* loaded from: classes4.dex */
public class AnonymousClass61B {
    public Image A00;
    public ImageReader A01;
    public AnonymousClass66I A02;
    public C119085cr A03;
    public C1308560f A04;
    public boolean A05;
    public final ImageReader.OnImageAvailableListener A06 = new AnonymousClass63K(this);
    public final C128455w8 A07 = new C128455w8(this);
    public final C129775yH A08 = new C129775yH();
    public final C129975yb A09 = new C129975yb();
    public final Callable A0A = new IDxCallableShape15S0100000_3_I1(this, 14);

    public static /* synthetic */ void A02(AnonymousClass61B r14) {
        C119085cr r1;
        AnonymousClass60H A00;
        C1308560f r0 = r14.A04;
        if (r0 == null) {
            return;
        }
        if (!r0.A09()) {
            throw new AnonymousClass6L0("Method tryToNotifyCpuFrame() must run on the Optic Background Thread.");
        } else if (r14.A00 != null && r14.A03 != null) {
            C129775yH r4 = r14.A08;
            if (!r4.A00.isEmpty()) {
                AnonymousClass66I r3 = r14.A02;
                try {
                    if (r3 == null || (r1 = r14.A03) == null || !C12970iu.A1Y(r1.A03(AbstractC130685zo.A0R))) {
                        C129975yb r5 = r14.A09;
                        r5.A02(r14.A00, r14.A05, false);
                        List list = r4.A00;
                        int size = list.size();
                        for (int i = 0; i < size; i++) {
                            ((AbstractC136166Lg) list.get(i)).AUE(r5);
                        }
                    } else {
                        long timestamp = r14.A00.getTimestamp();
                        C129475xm r02 = r3.A05;
                        if (r02 != null && (A00 = r02.A00(timestamp)) != null) {
                            C129975yb r52 = r14.A09;
                            Image image = r14.A00;
                            boolean z = r14.A05;
                            r52.A01(image, (Pair) A00.A00(AnonymousClass60H.A0N), (Float) A00.A00(AnonymousClass60H.A0M), (Long) A00.A00(AnonymousClass60H.A0O), (Long) A00.A00(AnonymousClass60H.A0K), (float[]) A00.A00(AnonymousClass60H.A0Q), z, false);
                            List list2 = r4.A00;
                            int size2 = list2.size();
                            for (int i2 = 0; i2 < size2; i2++) {
                                ((AbstractC136166Lg) list2.get(i2)).AUE(r52);
                            }
                        } else {
                            return;
                        }
                    }
                } catch (RuntimeException unused) {
                }
                C129975yb r42 = r14.A09;
                if (r42.A0C != null) {
                    int i3 = 0;
                    while (true) {
                        C128845wl[] r12 = r42.A0C;
                        if (i3 >= r12.length) {
                            break;
                        }
                        C128845wl r13 = r12[i3];
                        ByteBuffer byteBuffer = r13.A02;
                        if (byteBuffer != null) {
                            byteBuffer.clear();
                            r13.A02 = null;
                        }
                        i3++;
                    }
                    r42.A0C = null;
                }
                r42.A0A = null;
                r42.A0B = null;
                r42.A05 = null;
                r42.A08 = null;
                r42.A06 = null;
                r42.A07 = null;
                r14.A00.close();
                r14.A00 = null;
            }
        }
    }

    public Surface A03() {
        ImageReader imageReader = this.A01;
        if (imageReader != null) {
            return imageReader.getSurface();
        }
        throw C12960it.A0U("Getting image reader surface without initialize.");
    }

    public void A04() {
        ImageReader imageReader = this.A01;
        if (imageReader != null) {
            imageReader.setOnImageAvailableListener(null, null);
            this.A01.close();
            this.A01 = null;
        }
        Image image = this.A00;
        if (image != null) {
            image.close();
            this.A00 = null;
        }
        this.A04 = null;
        this.A03 = null;
        this.A02 = null;
    }

    public void A05(AbstractC1311561m r15, C119085cr r16, AbstractC130695zp r17, C129845yO r18, C1308560f r19) {
        int i;
        C129845yO r8 = r18;
        this.A04 = r19;
        this.A05 = C117295Zj.A1S(AbstractC130695zp.A0N, r17);
        this.A03 = r16;
        int A05 = C12960it.A05(r16.A03(AbstractC130685zo.A0i));
        if (C12970iu.A1Y(r15.AAQ(AbstractC1311561m.A0B))) {
            List A0Y = C117295Zj.A0Y(AbstractC130695zp.A0h, r17);
            int i2 = r8.A02;
            int i3 = r8.A01;
            int i4 = i2 * i3;
            int size = A0Y.size();
            for (int i5 = 0; i5 < size; i5++) {
                C129845yO r2 = (C129845yO) A0Y.get(i5);
                int i6 = r2.A02;
                int i7 = r2.A01;
                if (Math.abs((((float) Math.max(i6, i7)) / ((float) Math.min(i6, i7))) - (((float) Math.max(i2, i3)) / ((float) Math.min(i2, i3)))) <= 1.0E-4f && (i = r2.A02 * r2.A01) < i4 && i >= 180000) {
                    r8 = r2;
                    i4 = i;
                }
            }
        }
        ImageReader newInstance = ImageReader.newInstance(r8.A02, r8.A01, A05, 1);
        this.A01 = newInstance;
        newInstance.setOnImageAvailableListener(this.A06, null);
    }
}
