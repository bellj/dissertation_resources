package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;

/* renamed from: X.12j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C236212j {
    public final C246416h A00;
    public final C18460sU A01;
    public final C16490p7 A02;

    public C236212j(C246416h r1, C18460sU r2, C16490p7 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public synchronized AnonymousClass1YW A00(Cursor cursor) {
        AnonymousClass1YW r5;
        int i;
        int columnIndex = cursor.getColumnIndex("call_link_id");
        r5 = null;
        if (!(columnIndex == -1 || (i = cursor.getInt(columnIndex)) == 0)) {
            r5 = new AnonymousClass1YW(UserJid.of(this.A01.A03((long) cursor.getInt(cursor.getColumnIndexOrThrow("creator_jid_row_id")))), cursor.getString(cursor.getColumnIndexOrThrow("token")), (long) i);
        }
        return r5;
    }
}
