package X;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.core.graphics.drawable.IconCompat;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.03m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C006903m {
    public int A00 = -1;
    public int A01;
    public int A02 = 8388613;
    public int A03;
    public int A04 = 0;
    public int A05 = 1;
    public int A06 = 80;
    public int A07;
    public PendingIntent A08;
    public Bitmap A09;
    public String A0A;
    public String A0B;
    public ArrayList A0C = new ArrayList();
    public ArrayList A0D = new ArrayList();

    public static Notification.Action A00(AnonymousClass03l r6) {
        int i;
        Notification.Action.Builder builder;
        Bundle bundle;
        Icon A07;
        int i2 = Build.VERSION.SDK_INT;
        IconCompat A00 = r6.A00();
        if (i2 >= 23) {
            if (A00 == null) {
                A07 = null;
            } else {
                A07 = A00.A07();
            }
            builder = new Notification.Action.Builder(A07, r6.A03, r6.A01);
        } else {
            if (A00 == null || A00.A04() != 2) {
                i = 0;
            } else {
                i = A00.A03();
            }
            builder = new Notification.Action.Builder(i, r6.A03, r6.A01);
        }
        Bundle bundle2 = r6.A07;
        if (bundle2 != null) {
            bundle = new Bundle(bundle2);
        } else {
            bundle = new Bundle();
        }
        boolean z = r6.A04;
        bundle.putBoolean("android.support.allowGeneratedReplies", z);
        if (i2 >= 24) {
            builder.setAllowGeneratedReplies(z);
        }
        if (i2 >= 31) {
            builder.setAuthenticationRequired(false);
        }
        builder.addExtras(bundle);
        C007103o[] r0 = r6.A09;
        if (r0 != null) {
            for (RemoteInput remoteInput : C007103o.A01(r0)) {
                builder.addRemoteInput(remoteInput);
            }
        }
        return builder.build();
    }

    public void A01(C005602s r8) {
        Bundle bundle = new Bundle();
        ArrayList arrayList = this.A0C;
        if (!arrayList.isEmpty()) {
            ArrayList<? extends Parcelable> arrayList2 = new ArrayList<>(arrayList.size());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                AnonymousClass03l r3 = (AnonymousClass03l) it.next();
                if (Build.VERSION.SDK_INT >= 20) {
                    arrayList2.add(A00(r3));
                } else {
                    arrayList2.add(AnonymousClass0U1.A01(r3));
                }
            }
            bundle.putParcelableArrayList("actions", arrayList2);
        }
        int i = this.A05;
        if (i != 1) {
            bundle.putInt("flags", i);
        }
        PendingIntent pendingIntent = this.A08;
        if (pendingIntent != null) {
            bundle.putParcelable("displayIntent", pendingIntent);
        }
        ArrayList arrayList3 = this.A0D;
        if (!arrayList3.isEmpty()) {
            bundle.putParcelableArray("pages", (Parcelable[]) arrayList3.toArray(new Notification[arrayList3.size()]));
        }
        Bitmap bitmap = this.A09;
        if (bitmap != null) {
            bundle.putParcelable("background", bitmap);
        }
        int i2 = this.A01;
        if (i2 != 0) {
            bundle.putInt("contentIcon", i2);
        }
        int i3 = this.A02;
        if (i3 != 8388613) {
            bundle.putInt("contentIconGravity", i3);
        }
        int i4 = this.A00;
        if (i4 != -1) {
            bundle.putInt("contentActionIndex", i4);
        }
        int i5 = this.A04;
        if (i5 != 0) {
            bundle.putInt("customSizePreset", i5);
        }
        int i6 = this.A03;
        if (i6 != 0) {
            bundle.putInt("customContentHeight", i6);
        }
        int i7 = this.A06;
        if (i7 != 80) {
            bundle.putInt("gravity", i7);
        }
        int i8 = this.A07;
        if (i8 != 0) {
            bundle.putInt("hintScreenTimeout", i8);
        }
        String str = this.A0B;
        if (str != null) {
            bundle.putString("dismissalId", str);
        }
        String str2 = this.A0A;
        if (str2 != null) {
            bundle.putString("bridgeTag", str2);
        }
        Bundle bundle2 = r8.A0D;
        if (bundle2 == null) {
            bundle2 = new Bundle();
            r8.A0D = bundle2;
        }
        bundle2.putBundle("android.wearable.EXTENSIONS", bundle);
    }

    public /* bridge */ /* synthetic */ Object clone() {
        C006903m r2 = new C006903m();
        r2.A0C = new ArrayList(this.A0C);
        r2.A05 = this.A05;
        r2.A08 = this.A08;
        r2.A0D = new ArrayList(this.A0D);
        r2.A09 = this.A09;
        r2.A01 = this.A01;
        r2.A02 = this.A02;
        r2.A00 = this.A00;
        r2.A04 = this.A04;
        r2.A03 = this.A03;
        r2.A06 = this.A06;
        r2.A07 = this.A07;
        r2.A0B = this.A0B;
        r2.A0A = this.A0A;
        return r2;
    }
}
