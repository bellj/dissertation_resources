package X;

import com.whatsapp.base.WaFragment;

/* renamed from: X.5bc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118315bc extends AnonymousClass0Yo {
    public final /* synthetic */ WaFragment A00;
    public final /* synthetic */ C128355vy A01;

    public C118315bc(WaFragment waFragment, C128355vy r2) {
        this.A01 = r2;
        this.A00 = waFragment;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118065bD.class)) {
            WaFragment waFragment = this.A00;
            C128355vy r0 = this.A01;
            C14830m7 r5 = r0.A09;
            C14900mE r2 = r0.A00;
            C16590pI r6 = r0.A0A;
            C15450nH r3 = r0.A02;
            C17220qS r8 = r0.A0J;
            C14820m6 r7 = r0.A0B;
            C18610sj r11 = r0.A0T;
            C17900ra r12 = r0.A0U;
            return new C118065bD(waFragment, r2, r3, r0.A07, r5, r6, r7, r8, r0.A0N, r0.A0Q, r11, r12);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
