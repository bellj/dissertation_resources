package X;

/* renamed from: X.4dS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95294dS {
    public int A00;
    public C94464br A01;
    public short A02;
    public short A03;
    public int[] A04;
    public int[] A05;
    public int[] A06;
    public int[] A07;
    public int[] A08;

    public C95294dS(C94464br r1) {
        this.A01 = r1;
    }

    public static int A00(Object obj, AnonymousClass4YW r5) {
        if (obj instanceof Integer) {
            return 4194304 | C12960it.A05(obj);
        }
        if (!(obj instanceof String)) {
            return r5.A04("", ((C94464br) obj).A00) | 12582912;
        }
        String str = (String) obj;
        int i = 12;
        if (str.charAt(0) == '[') {
            i = 9;
        }
        return A01(new C95574dz(str, i, 0, str.length()).A06(), r5, 0);
    }

    public static int A01(String str, AnonymousClass4YW r13, int i) {
        char charAt = str.charAt(i);
        int i2 = 4194306;
        if (charAt == 'F') {
            return 4194306;
        }
        if (charAt == 'L') {
            return r13.A03(str.substring(i + 1, str.length() - 1)) | 8388608;
        }
        if (charAt != 'S') {
            if (charAt == 'V') {
                return 0;
            }
            if (charAt != 'I') {
                if (charAt == 'J') {
                    return 4194308;
                }
                if (charAt != 'Z') {
                    if (charAt != '[') {
                        switch (charAt) {
                            case 'B':
                            case 'C':
                                break;
                            case 'D':
                                return 4194307;
                            default:
                                throw C72453ed.A0h();
                        }
                    } else {
                        int i3 = i + 1;
                        while (str.charAt(i3) == '[') {
                            i3++;
                        }
                        char charAt2 = str.charAt(i3);
                        if (charAt2 != 'F') {
                            if (charAt2 == 'L') {
                                i2 = r13.A03(str.substring(i3 + 1, str.length() - 1)) | 8388608;
                            } else if (charAt2 == 'S') {
                                i2 = 4194316;
                            } else if (charAt2 == 'Z') {
                                i2 = 4194313;
                            } else if (charAt2 == 'I') {
                                i2 = 4194305;
                            } else if (charAt2 != 'J') {
                                switch (charAt2) {
                                    case 'B':
                                        i2 = 4194314;
                                        break;
                                    case 'C':
                                        i2 = 4194315;
                                        break;
                                    case 'D':
                                        i2 = 4194307;
                                        break;
                                    default:
                                        throw C72453ed.A0h();
                                }
                            } else {
                                i2 = 4194308;
                            }
                        }
                        return ((i3 - i) << 26) | i2;
                    }
                }
            }
        }
        return 4194305;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0101, code lost:
        if (r9 == 8388608) goto L_0x0103;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A02(X.AnonymousClass4YW r21, int[] r22, int r23, int r24) {
        /*
        // Method dump skipped, instructions count: 298
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95294dS.A02(X.4YW, int[], int, int):boolean");
    }

    public final int A03() {
        short s = this.A03;
        if (s > 0) {
            int[] iArr = this.A08;
            short s2 = (short) (s - 1);
            this.A03 = s2;
            return iArr[s2];
        }
        short s3 = (short) (this.A02 - 1);
        this.A02 = s3;
        return 20971520 | (-s3);
    }

    public final int A04(int i, int i2) {
        int i3;
        int i4 = -67108864 & i;
        int i5 = 62914560 & i;
        if (i5 == 16777216) {
            i3 = this.A05[i & 1048575];
        } else if (i5 != 20971520) {
            return i;
        } else {
            i3 = this.A06[i2 - (1048575 & i)];
        }
        int i6 = i4 + i3;
        if ((i & 1048576) == 0 || (i6 != 4194308 && i6 != 4194307)) {
            return i6;
        }
        return 4194304;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003f A[LOOP:0: B:7:0x000d->B:20:0x003f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x002b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A05(X.AnonymousClass4YW r9, int r10) {
        /*
            r8 = this;
            r6 = 4194310(0x400006, float:5.87748E-39)
            if (r10 == r6) goto L_0x000c
            r1 = -4194304(0xffffffffffc00000, float:NaN)
            r1 = r1 & r10
            r0 = 12582912(0xc00000, float:1.7632415E-38)
            if (r1 != r0) goto L_0x004d
        L_0x000c:
            r7 = 0
        L_0x000d:
            int r0 = r8.A00
            if (r7 >= r0) goto L_0x004d
            int[] r0 = r8.A04
            r1 = r0[r7]
            r5 = -67108864(0xfffffffffc000000, float:-2.658456E36)
            r5 = r5 & r1
            r4 = 62914560(0x3c00000, float:1.1284746E-36)
            r4 = r4 & r1
            r3 = 1048575(0xfffff, float:1.469367E-39)
            r2 = r1 & r3
            r0 = 16777216(0x1000000, float:2.3509887E-38)
            if (r4 != r0) goto L_0x0042
            int[] r0 = r8.A05
            r1 = r0[r2]
        L_0x0028:
            int r1 = r1 + r5
        L_0x0029:
            if (r10 != r1) goto L_0x003f
            r1 = 8388608(0x800000, float:1.17549435E-38)
            if (r10 != r6) goto L_0x0037
            java.lang.String r0 = r9.A05
        L_0x0031:
            int r0 = r9.A03(r0)
            r0 = r0 | r1
            return r0
        L_0x0037:
            r10 = r10 & r3
            X.4de[] r0 = r9.A09
            r0 = r0[r10]
            java.lang.String r0 = r0.A08
            goto L_0x0031
        L_0x003f:
            int r7 = r7 + 1
            goto L_0x000d
        L_0x0042:
            r0 = 20971520(0x1400000, float:3.526483E-38)
            if (r4 != r0) goto L_0x0029
            int[] r1 = r8.A06
            int r0 = r1.length
            int r0 = r0 - r2
            r1 = r1[r0]
            goto L_0x0028
        L_0x004d:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95294dS.A05(X.4YW, int):int");
    }

    public final void A06(int i) {
        short s;
        short s2 = this.A03;
        if (s2 >= i) {
            s = (short) (s2 - i);
        } else {
            this.A02 = (short) (this.A02 - (i - s2));
            s = 0;
        }
        this.A03 = s;
    }

    public final void A07(int i) {
        int[] iArr = this.A08;
        int[] iArr2 = iArr;
        if (iArr == null) {
            iArr = new int[10];
            this.A08 = iArr;
            iArr2 = iArr;
        }
        int length = iArr.length;
        short s = this.A03;
        if (s >= length) {
            iArr2 = new int[Math.max(s + 1, length << 1)];
            System.arraycopy(iArr, 0, iArr2, 0, length);
            this.A08 = iArr2;
        }
        short s2 = this.A03;
        short s3 = (short) (s2 + 1);
        this.A03 = s3;
        iArr2[s2] = i;
        short s4 = (short) (this.A02 + s3);
        C94464br r1 = this.A01;
        if (s4 > r1.A08) {
            r1.A08 = s4;
        }
    }

    public final void A08(int i, int i2) {
        int[] iArr = this.A07;
        int[] iArr2 = iArr;
        if (iArr == null) {
            iArr = new int[10];
            this.A07 = iArr;
            iArr2 = iArr;
        }
        int length = iArr.length;
        if (i >= length) {
            iArr2 = new int[Math.max(i + 1, length << 1)];
            System.arraycopy(iArr, 0, iArr2, 0, length);
            this.A07 = iArr2;
        }
        iArr2[i] = i2;
    }

    public final void A09(String str) {
        char charAt = str.charAt(0);
        if (charAt == '(') {
            A06((C95574dz.A00(str) >> 2) - 1);
        } else if (charAt == 'J' || charAt == 'D') {
            A06(2);
        } else {
            A06(1);
        }
    }

    public final void A0A(String str, AnonymousClass4YW r7) {
        int i = 0;
        if (str.charAt(0) == '(') {
            int i2 = 1;
            while (str.charAt(i2) != ')') {
                while (str.charAt(i2) == '[') {
                    i2++;
                }
                int i3 = i2 + 1;
                i2 = str.charAt(i2) == 'L' ? Math.max(i3, str.indexOf(59, i3) + 1) : i3;
            }
            i = i2 + 1;
        }
        int A01 = A01(str, r7, i);
        if (A01 != 0) {
            A07(A01);
            if (A01 == 4194308 || A01 == 4194307) {
                A07(4194304);
            }
        }
    }

    public final void A0B(String str, AnonymousClass4YW r12, int i, int i2) {
        int[] iArr = new int[i2];
        this.A05 = iArr;
        this.A06 = new int[0];
        int i3 = 1;
        if ((i & 8) == 0) {
            int i4 = 4194310;
            if ((i & 262144) == 0) {
                i4 = 8388608 | r12.A03(r12.A05);
            }
            iArr[0] = i4;
        } else {
            i3 = 0;
        }
        for (C95574dz r0 : C95574dz.A05(str)) {
            int A01 = A01(r0.A06(), r12, 0);
            int[] iArr2 = this.A05;
            int i5 = i3 + 1;
            iArr2[i3] = A01;
            if (A01 != 4194308) {
                i3 = i5;
                if (A01 != 4194307) {
                }
            }
            i3 = i5 + 1;
            iArr2[i5] = 4194304;
        }
        while (i3 < i2) {
            i3++;
            this.A05[i3] = 4194304;
        }
    }

    public final void A0C(AnonymousClass5M6 r13) {
        int i;
        int i2;
        int i3;
        int i4;
        int[] iArr = this.A05;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        loop0: while (true) {
            int i8 = 0;
            do {
                int i9 = 2;
                if (i6 >= iArr.length) {
                    break loop0;
                }
                i4 = iArr[i6];
                if (!(i4 == 4194308 || i4 == 4194307)) {
                    i9 = 1;
                }
                i6 += i9;
                i8++;
            } while (i4 == 4194304);
            i7 += i8;
        }
        int[] iArr2 = this.A06;
        int i10 = 0;
        int i11 = 0;
        while (i10 < iArr2.length) {
            int i12 = iArr2[i10];
            if (i12 != 4194308) {
                i3 = 1;
                if (i12 != 4194307) {
                    i10 += i3;
                    i11++;
                }
            }
            i3 = 2;
            i10 += i3;
            i11++;
        }
        int A0C = r13.A0C(this.A01.A00, i7, i11);
        int i13 = 0;
        while (true) {
            int i14 = i7 - 1;
            if (i7 > 0) {
                int i15 = iArr[i13];
                if (i15 != 4194308) {
                    i2 = 1;
                    if (i15 != 4194307) {
                        i13 += i2;
                        r13.A0Y[A0C] = i15;
                        i7 = i14;
                        A0C++;
                    }
                }
                i2 = 2;
                i13 += i2;
                r13.A0Y[A0C] = i15;
                i7 = i14;
                A0C++;
            }
        }
        while (true) {
            int i16 = i11 - 1;
            if (i11 > 0) {
                int i17 = iArr2[i5];
                if (i17 != 4194308) {
                    i = 1;
                    if (i17 != 4194307) {
                        i5 += i;
                        r13.A0Y[A0C] = i17;
                        A0C++;
                        i11 = i16;
                    }
                }
                i = 2;
                i5 += i;
                r13.A0Y[A0C] = i17;
                A0C++;
                i11 = i16;
            } else {
                r13.A0D();
                return;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Failed to find switch 'out' block
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:819)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:858)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:858)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:858)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:858)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0217  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x021d  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0228  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:144:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01e4  */
    public void A0D(X.C95404de r14, X.AnonymousClass4YW r15, int r16, int r17) {
        /*
        // Method dump skipped, instructions count: 982
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95294dS.A0D(X.4de, X.4YW, int, int):void");
    }

    public final boolean A0E(C95294dS r9, AnonymousClass4YW r10, int i) {
        boolean z;
        int i2;
        int i3;
        int length = this.A05.length;
        int length2 = this.A06.length;
        boolean z2 = true;
        if (r9.A05 == null) {
            r9.A05 = new int[length];
            z = true;
        } else {
            z = false;
        }
        for (int i4 = 0; i4 < length; i4++) {
            int[] iArr = this.A07;
            if (iArr == null || i4 >= iArr.length || (i3 = iArr[i4]) == 0) {
                i2 = this.A05[i4];
            } else {
                i2 = A04(i3, length2);
            }
            if (this.A04 != null) {
                i2 = A05(r10, i2);
            }
            z |= A02(r10, r9.A05, i2, i4);
        }
        if (i > 0) {
            for (int i5 = 0; i5 < length; i5++) {
                z |= A02(r10, r9.A05, this.A05[i5], i5);
            }
            int[] iArr2 = r9.A06;
            if (iArr2 == null) {
                iArr2 = new int[1];
                r9.A06 = iArr2;
            } else {
                z2 = z;
            }
            return A02(r10, iArr2, i, 0) | z2;
        }
        int length3 = this.A06.length + this.A02;
        if (r9.A06 == null) {
            r9.A06 = new int[this.A03 + length3];
        } else {
            z2 = z;
        }
        for (int i6 = 0; i6 < length3; i6++) {
            int i7 = this.A06[i6];
            if (this.A04 != null) {
                i7 = A05(r10, i7);
            }
            z2 |= A02(r10, r9.A06, i7, i6);
        }
        for (int i8 = 0; i8 < this.A03; i8++) {
            int A04 = A04(this.A08[i8], length2);
            if (this.A04 != null) {
                A04 = A05(r10, A04);
            }
            z2 |= A02(r10, r9.A06, A04, length3 + i8);
        }
        return z2;
    }
}
