package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1lQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1lQ {
    public final C39931qm A00;
    public final UserJid A01;

    public AnonymousClass1lQ(C39931qm r1, UserJid userJid) {
        this.A01 = userJid;
        this.A00 = r1;
    }

    public int A00() {
        return this.A00.A00();
    }

    public long A01(int i) {
        return this.A00.A01(i);
    }

    public AnonymousClass49N A02() {
        return AnonymousClass49N.A00;
    }
}
