package X;

import android.database.Cursor;
import java.util.HashMap;

/* renamed from: X.1Iv  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1Iv extends AbstractC15340mz {
    public long A00;
    public C40711sC A01;
    public C40711sC A02;

    public AnonymousClass1Iv(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    @Override // X.AbstractC15340mz
    public void A0Z(int i) {
        AnonymousClass009.A0A("Cannot change storage type for add on messages", false);
    }

    public AnonymousClass1IS A14() {
        C40711sC r0 = this.A02;
        if (r0 == null) {
            return null;
        }
        return r0.A01;
    }

    public void A15(Cursor cursor, C18460sU r10, HashMap hashMap) {
        int A00 = AnonymousClass1Tx.A00("_id", hashMap);
        int A002 = AnonymousClass1Tx.A00("sender_jid_row_id", hashMap);
        int A003 = AnonymousClass1Tx.A00("parent_message_row_id", hashMap);
        int A004 = AnonymousClass1Tx.A00("status", hashMap);
        long j = cursor.getLong(A00);
        long j2 = cursor.getLong(A002);
        long j3 = cursor.getLong(A003);
        int i = cursor.getInt(A004);
        this.A11 = j;
        A0e((AbstractC14640lm) r10.A07(AbstractC14640lm.class, j2));
        this.A00 = j3;
        A0Y(i);
    }
}
