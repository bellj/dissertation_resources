package X;

import android.os.Looper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0QN  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0QN {
    public AbstractC12910il A00;
    @Deprecated
    public List A01;
    public Executor A02;
    public Executor A03;
    public boolean A04;
    public boolean A05;
    public final C05230Ot A06 = new C05230Ot(this, new HashMap(0), new HashMap(0), "Dependency", "WorkSpec", "WorkTag", "SystemIdInfo", "WorkName", "WorkProgress", "Preference");
    public final ThreadLocal A07 = new ThreadLocal();
    public final Map A08 = new ConcurrentHashMap();
    public final ReentrantReadWriteLock A09 = new ReentrantReadWriteLock();
    @Deprecated
    public volatile AbstractC12920im A0A;

    public AbstractC12830ic A00(String str) {
        A01();
        A02();
        return new C02980Fp(((AnonymousClass0ZE) ((AnonymousClass0ZH) this.A00).A00().A00()).A00.compileStatement(str));
    }

    public void A01() {
        if (!this.A04 && Looper.getMainLooper().getThread() == Thread.currentThread()) {
            throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
        }
    }

    public void A02() {
        if (!((AnonymousClass0ZE) ((AnonymousClass0ZH) this.A00).A00().A00()).A00.inTransaction() && this.A07.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }

    @Deprecated
    public void A03() {
        A01();
        AbstractC12920im A00 = ((AnonymousClass0ZH) this.A00).A00().A00();
        this.A06.A00(A00);
        ((AnonymousClass0ZE) A00).A00.beginTransaction();
    }

    @Deprecated
    public void A04() {
        ((AnonymousClass0ZE) ((AnonymousClass0ZH) this.A00).A00().A00()).A00.endTransaction();
        if (!((AnonymousClass0ZE) ((AnonymousClass0ZH) this.A00).A00().A00()).A00.inTransaction()) {
            C05230Ot r3 = this.A06;
            if (r3.A03.compareAndSet(false, true)) {
                r3.A06.A02.execute(r3.A01);
            }
        }
    }

    @Deprecated
    public void A05() {
        ((AnonymousClass0ZE) ((AnonymousClass0ZH) this.A00).A00().A00()).A00.setTransactionSuccessful();
    }
}
