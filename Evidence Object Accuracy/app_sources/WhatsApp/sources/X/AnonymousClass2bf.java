package X;

import android.content.Context;
import android.widget.BaseAdapter;

/* renamed from: X.2bf  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2bf extends BaseAdapter {
    public final int A00;
    public final Context A01;
    public final AnonymousClass018 A02;
    public final /* synthetic */ AnonymousClass3IZ A03;

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        return null;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return 0;
    }

    public AnonymousClass2bf(Context context, AnonymousClass3IZ r2, AnonymousClass018 r3, int i) {
        this.A03 = r2;
        this.A01 = context;
        this.A02 = r3;
        this.A00 = i;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        AnonymousClass3IZ r6 = this.A03;
        int i = 0;
        if (r6.A01 == 0) {
            return 0;
        }
        AnonymousClass3HU[] r4 = r6.A0T;
        int i2 = this.A00;
        int A00 = r4[i2].A00();
        int i3 = r6.A01;
        int i4 = ((A00 + i3) - 1) / i3;
        if (r6.A0R && r4[i2].A00() > 0) {
            i = 1;
        }
        return i4 + i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r1 != r4.A01) goto L_0x000f;
     */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r11, android.view.View r12, android.view.ViewGroup r13) {
        /*
            r10 = this;
            r5 = 0
            if (r12 == 0) goto L_0x000f
            android.view.ViewGroup r12 = (android.view.ViewGroup) r12
            int r1 = r12.getChildCount()
            X.3IZ r4 = r10.A03
            int r0 = r4.A01
            if (r1 == r0) goto L_0x003b
        L_0x000f:
            X.3IZ r4 = r10.A03
            android.content.Context r0 = r4.A0A
            X.2bA r12 = new X.2bA
            r12.<init>(r0, r10)
            r3 = 0
        L_0x0019:
            int r0 = r4.A01
            if (r3 >= r0) goto L_0x0034
            android.content.Context r0 = r10.A01
            X.2ax r2 = new X.2ax
            r2.<init>(r0, r4)
            int r1 = r4.A06
            android.view.ViewGroup$LayoutParams r0 = new android.view.ViewGroup$LayoutParams
            r0.<init>(r1, r1)
            r2.setLayoutParams(r0)
            r12.addView(r2)
            int r3 = r3 + 1
            goto L_0x0019
        L_0x0034:
            r12.setClickable(r5)
            r0 = 2
            X.AnonymousClass028.A0a(r12, r0)
        L_0x003b:
            r3 = 0
        L_0x003c:
            int r0 = r4.A01
            if (r3 >= r0) goto L_0x00b3
            android.view.View r2 = r12.getChildAt(r3)
            X.2ax r2 = (X.C52502ax) r2
            int r8 = r4.A01
            int r8 = r8 * r11
            int r8 = r8 + r3
            X.3HU[] r9 = r4.A0T
            int r7 = r10.A00
            r1 = r9[r7]
            int r0 = r1.A00()
            r6 = 0
            if (r8 >= r0) goto L_0x00a0
            r0 = r9[r7]
            X.0pM r1 = r4.A0Q
            int[] r0 = r0.A02(r1, r8)
            r2.setEmoji(r0)
            r0 = 2131232677(0x7f0807a5, float:1.808147E38)
            r2.setBackgroundResource(r0)
            r0 = 1
            r2.setClickable(r0)
            android.view.View$OnClickListener r0 = r4.A0D
            r2.setOnClickListener(r0)
            r0 = r9[r7]
            int[] r1 = r0.A02(r1, r8)
            if (r1 == 0) goto L_0x008a
            boolean r0 = X.AnonymousClass3JU.A02(r1)
            if (r0 == 0) goto L_0x008a
            X.3Mk r0 = new X.3Mk
            r0.<init>()
            r2.setOnLongClickListener(r0)
        L_0x0087:
            int r3 = r3 + 1
            goto L_0x003c
        L_0x008a:
            boolean r0 = X.AnonymousClass3JU.A03(r1)
            if (r0 == 0) goto L_0x0099
            X.3Ml r0 = new X.3Ml
            r0.<init>()
            r2.setOnLongClickListener(r0)
            goto L_0x0087
        L_0x0099:
            r2.setOnLongClickListener(r6)
            r2.setLongClickable(r5)
            goto L_0x0087
        L_0x00a0:
            r2.setEmoji(r6)
            r2.setBackground(r6)
            r2.setOnClickListener(r6)
            r2.setClickable(r5)
            r2.setOnLongClickListener(r6)
            r2.setLongClickable(r5)
            goto L_0x0087
        L_0x00b3:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2bf.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
