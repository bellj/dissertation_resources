package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.1B8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1B8 {
    public AnonymousClass12P A00;
    public AbstractC15710nm A01;
    public C14900mE A02;
    public AnonymousClass01d A03;
    public C252018m A04;

    public AnonymousClass1B8(AnonymousClass12P r1, AbstractC15710nm r2, C14900mE r3, AnonymousClass01d r4, C252018m r5) {
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r5;
        this.A00 = r1;
        this.A03 = r4;
    }

    public final void A00(Context context, TextEmojiLabel textEmojiLabel, String str, String str2, String str3) {
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3)) {
            AbstractC15710nm r3 = this.A01;
            StringBuilder sb = new StringBuilder("groupname=");
            sb.append(str2);
            sb.append(", articleName=");
            sb.append(str3);
            r3.AaV("BusinessDirectoryFaqLinkHelper/addDirectoryGeneralFaqLink/group name or article name are null or empty", sb.toString(), true);
            return;
        }
        C42971wC.A08(context, this.A04.A04(str2, str3), this.A00, this.A02, textEmojiLabel, this.A03, str, "learn-more");
    }
}
