package X;

/* renamed from: X.18r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252518r {
    public final C14850m9 A00;
    public final C22370yy A01;
    public final C252418q A02;

    public C252518r(C14850m9 r1, C22370yy r2, C252418q r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public void A00(AbstractC15340mz r9) {
        if ((r9 instanceof AbstractC16130oV) && this.A00.A07(249) && r9.A0G() != null && !r9.A0G().A04()) {
            AbstractC16130oV r2 = (AbstractC16130oV) r9;
            if (r9.A0T != null) {
                this.A02.A06(r2, 0);
            } else if (r2 instanceof AnonymousClass1X7) {
                C38101nW A14 = r2.A14();
                AnonymousClass009.A05(A14);
                C16150oX r0 = A14.A04.A02;
                AnonymousClass009.A05(r0);
                if (r0.A0M) {
                    this.A01.A08(null, r2, 2, 0, true, false);
                }
            }
        }
    }
}
