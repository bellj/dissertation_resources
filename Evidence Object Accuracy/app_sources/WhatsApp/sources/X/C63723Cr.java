package X;

import android.content.BroadcastReceiver;
import android.content.Intent;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* renamed from: X.3Cr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63723Cr {
    public boolean A00 = false;
    public final BroadcastReceiver.PendingResult A01;
    public final Intent A02;
    public final ScheduledFuture A03;

    public C63723Cr(BroadcastReceiver.PendingResult pendingResult, Intent intent, ScheduledExecutorService scheduledExecutorService) {
        this.A02 = intent;
        this.A01 = pendingResult;
        this.A03 = scheduledExecutorService.schedule(new RunnableBRunnable0Shape10S0200000_I1(this, 27, intent), 9000, TimeUnit.MILLISECONDS);
    }

    public final synchronized void A00() {
        if (!this.A00) {
            this.A01.finish();
            this.A03.cancel(false);
            this.A00 = true;
        }
    }
}
