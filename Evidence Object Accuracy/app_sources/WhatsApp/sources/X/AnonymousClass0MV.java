package X;

/* renamed from: X.0MV  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0MV {
    public final AbstractC11800gt A00;

    public AnonymousClass0MV() {
        AbstractC11800gt r0;
        Class<?> cls = Class.forName("dalvik.system.DexPathList$Element");
        try {
            try {
                r0 = new AnonymousClass0Ys(cls);
            } catch (NoSuchMethodException unused) {
                r0 = new AnonymousClass0Yt(cls);
            }
        } catch (NoSuchMethodException unused2) {
            r0 = new AnonymousClass0Yu(cls);
        }
        this.A00 = r0;
    }
}
