package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0W8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W8 implements View.OnAttachStateChangeListener {
    public final /* synthetic */ LayoutInflater$Factory2C010105a A00;
    public final /* synthetic */ C06460Ts A01;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
    }

    public AnonymousClass0W8(LayoutInflater$Factory2C010105a r1, C06460Ts r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
        C06460Ts r1 = this.A01;
        AnonymousClass01E r0 = r1.A02;
        r1.A04();
        AbstractC06180Sm.A01((ViewGroup) r0.A0A.getParent()).A03();
    }
}
