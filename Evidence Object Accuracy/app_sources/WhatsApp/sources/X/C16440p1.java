package X;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Base64;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;

/* renamed from: X.0p1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16440p1 extends AbstractC16130oV implements AbstractC16400ox, AbstractC16420oz {
    public int A00;
    public String A01;

    public C16440p1(C16150oX r2, AnonymousClass1IS r3, C16440p1 r4, byte b, long j, boolean z) {
        super(r2, r3, r4, b, j, z);
        this.A00 = r4.A00;
        this.A01 = r4.A01;
    }

    public C16440p1(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    public C16440p1(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 9, j);
    }

    public C16440p1(C40831sP r2, AnonymousClass1IS r3, long j, boolean z, boolean z2) {
        super(r3, (byte) 63, j);
        A1D(r2, z, z2);
    }

    @Override // X.AbstractC15340mz
    public C16460p3 A0G() {
        C16460p3 A0G = super.A0G();
        AnonymousClass009.A05(A0G);
        return A0G;
    }

    @Override // X.AbstractC16130oV
    public void A17(Cursor cursor, C16150oX r3) {
        super.A17(cursor, r3);
        this.A00 = cursor.getInt(cursor.getColumnIndexOrThrow("page_count"));
        this.A01 = cursor.getString(cursor.getColumnIndexOrThrow("media_caption"));
    }

    @Override // X.AbstractC16130oV
    public void A18(Cursor cursor, C16150oX r3) {
        super.A18(cursor, r3);
        this.A00 = cursor.getInt(cursor.getColumnIndexOrThrow("page_count"));
        this.A01 = cursor.getString(cursor.getColumnIndexOrThrow("media_caption"));
    }

    public String A1B() {
        if (TextUtils.isEmpty(this.A01)) {
            return A16();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01);
        sb.append(" ");
        sb.append(A16());
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0284, code lost:
        if (r13.A0U != null) goto L_0x012a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1C(X.C15570nT r12, X.C16150oX r13, X.C14850m9 r14, X.C81873ui r15, X.AnonymousClass1PG r16, X.C16460p3 r17, byte[] r18, boolean r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 648
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16440p1.A1C(X.0nT, X.0oX, X.0m9, X.3ui, X.1PG, X.0p3, byte[], boolean, boolean):void");
    }

    public void A1D(C40831sP r12, boolean z, boolean z2) {
        C16150oX r5 = new C16150oX();
        ((AbstractC16130oV) this).A02 = r5;
        if ((r12.A00 & 64) == 64) {
            AnonymousClass3AI.A00(r5, this, r12.A09.A04());
        } else if (!z) {
            StringBuilder sb = new StringBuilder("FMessageDocument/missing media key; message.key=");
            sb.append(this.A0z);
            Log.w(sb.toString());
            throw new C43271wi(16);
        }
        boolean z3 = false;
        if ((r12.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            z3 = true;
        }
        if (z3) {
            r5.A0B = r12.A05 * 1000;
        }
        byte[] A04 = r12.A08.A04();
        if (A04.length > 0) {
            ((AbstractC15340mz) this).A02 = 1;
            A0G().A03(A04, z2);
        }
        if (!z || (r12.A00 & 16) == 16) {
            long j = r12.A04;
            if (j >= 0) {
                ((AbstractC16130oV) this).A01 = j;
            } else {
                StringBuilder sb2 = new StringBuilder("FMessageDocument/bogus media size received; file_length=");
                sb2.append(j);
                sb2.append("; message.key=");
                sb2.append(this.A0z);
                Log.w(sb2.toString());
                throw new C43271wi(13);
            }
        }
        if (!z || (r12.A00 & 8) == 8) {
            byte[] A042 = r12.A07.A04();
            int length = A042.length;
            if (length == 32) {
                ((AbstractC16130oV) this).A05 = Base64.encodeToString(A042, 2);
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("FMessageDocument/bogus sha-256 hash received; length=");
                sb3.append(length);
                sb3.append("; message.key=");
                sb3.append(this.A0z);
                Log.w(sb3.toString());
                throw new C43271wi(14);
            }
        }
        if ((r12.A00 & 256) == 256) {
            byte[] A043 = r12.A06.A04();
            int length2 = A043.length;
            if (length2 == 32) {
                ((AbstractC16130oV) this).A04 = Base64.encodeToString(A043, 2);
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("FMessageDocument/bogus sha-256 hash received; length=");
                sb4.append(length2);
                sb4.append("; message.key=");
                sb4.append(this.A0z);
                Log.w(sb4.toString());
                throw new C43271wi(14);
            }
        }
        if (!z || (r12.A00 & 1) == 1) {
            A19(r12.A0J);
        }
        if (!TextUtils.isEmpty(r12.A0I)) {
            ((AbstractC16130oV) this).A03 = AnonymousClass1US.A03(65536, r12.A0I);
        }
        this.A00 = r12.A01;
        if (!TextUtils.isEmpty(r12.A0D)) {
            this.A01 = r12.A0D;
        }
        if (!TextUtils.isEmpty(r12.A0F)) {
            ((AbstractC16130oV) this).A07 = AnonymousClass1US.A03(65536, r12.A0F);
        }
        if (!z || (r12.A00 & 512) == 512) {
            r5.A0G = r12.A0E;
        } else {
            StringBuilder sb5 = new StringBuilder("FMessageAudio/message without direct path received; message.key=");
            sb5.append(this.A0z);
            sb5.append("; message.senderJid=");
            sb5.append(this.A0M);
            Log.w(sb5.toString());
        }
        int i = r12.A00;
        if ((i & 4096) == 4096 && (i & 16384) == 16384 && (i & DefaultCrypto.BUFFER_SIZE) == 8192 && (i & 64) == 64) {
            C14360lJ r3 = new C14360lJ();
            r3.A04 = r12.A0H;
            r3.A07 = Base64.encodeToString(r12.A0B.A04(), 2);
            r3.A05 = Base64.encodeToString(r12.A0A.A04(), 2);
            r3.A09 = r12.A09.A04();
            int i2 = r12.A00;
            if ((i2 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                r3.A02 = r12.A05 * 1000;
            }
            if ((i2 & 32768) == 32768) {
                r3.A0A = r12.A08.A04();
            }
            int i3 = r12.A00;
            if ((i3 & 262144) == 262144) {
                r3.A01 = r12.A03;
            }
            if ((i3 & C25981Bo.A0F) == 131072) {
                r3.A00 = r12.A02;
            }
            A0i(r3);
        }
        ((AbstractC16130oV) this).A06 = r12.A0G;
        if (r12.A0K) {
            ((AbstractC15340mz) this).A08 = 7;
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r17) {
        StringBuilder sb;
        byte b;
        String str;
        boolean z;
        AbstractC27091Fz r1;
        C28891Pk r0;
        boolean z2;
        boolean z3;
        if (this instanceof AnonymousClass1XT) {
            AnonymousClass1XT r6 = (AnonymousClass1XT) this;
            C16150oX r8 = ((AbstractC16130oV) r6).A02;
            C16460p3 A0G = r6.A0G();
            if (r8 == null || (!(z = r17.A08) && r8.A0U == null)) {
                sb = new StringBuilder("FMessageTemplateDocument/unable to send encrypted media message due to missing mediaKey; message.key=");
                sb.append(r6.A0z);
                str = "; media_wa_type=";
            } else {
                AnonymousClass1G3 r3 = r17.A03;
                AnonymousClass2Lw r2 = (AnonymousClass2Lw) r3.A05().A0T();
                C14850m9 r9 = r17.A02;
                C15570nT r7 = r17.A00;
                AnonymousClass2Ly r5 = r3.A05().A03;
                if (r5 == null) {
                    r5 = AnonymousClass2Ly.A07;
                }
                if (r5.A01 == 1) {
                    r1 = (AbstractC27091Fz) r5.A03;
                } else {
                    r1 = C40831sP.A0L;
                }
                C81873ui r10 = (C81873ui) r1.A0T();
                r6.A1C(r7, r8, r9, r10, r17.A04, A0G, r17.A09, z, r17.A06);
                if (r10 == null || (r0 = r6.A00) == null) {
                    sb = new StringBuilder();
                    str = "MessageTemplateDocument: cannot send encrypted document message, ";
                } else {
                    AnonymousClass2Lx A00 = C63173Ao.A00(r3, r0);
                    A00.A03();
                    AnonymousClass2Ly r12 = (AnonymousClass2Ly) A00.A00;
                    r12.A03 = r10.A02();
                    r12.A01 = 1;
                    r2.A06(A00);
                    r2.A05(A00);
                    r3.A0B(r2);
                    return;
                }
            }
            sb.append(str);
            b = r6.A0y;
        } else if (!(this instanceof AnonymousClass1X0)) {
            C16150oX r82 = ((AbstractC16130oV) this).A02;
            C16460p3 A0G2 = A0G();
            if (r82 == null || (!(z3 = r17.A08) && r82.A0U == null)) {
                sb = new StringBuilder("FMessageDocument/unable to send encrypted media message due to missing mediaKey; message.key=");
                sb.append(this.A0z);
                sb.append("; media_wa_type=");
                b = this.A0y;
            } else {
                C14850m9 r92 = r17.A02;
                C15570nT r72 = r17.A00;
                AnonymousClass1G3 r22 = r17.A03;
                C40831sP r13 = ((C27081Fy) r22.A00).A0C;
                if (r13 == null) {
                    r13 = C40831sP.A0L;
                }
                C81873ui r102 = (C81873ui) r13.A0T();
                A1C(r72, r82, r92, r102, r17.A04, A0G2, r17.A09, z3, r17.A06);
                if (C35011h5.A04(this)) {
                    C57552nF r02 = ((C27081Fy) r22.A00).A03;
                    if (r02 == null) {
                        r02 = C57552nF.A08;
                    }
                    C56852m3 r32 = (C56852m3) r02.A0T();
                    C35011h5.A03(r32, A0F().A00);
                    r32.A03();
                    C57552nF r14 = (C57552nF) r32.A00;
                    r14.A05 = r102.A02();
                    r14.A01 = 2;
                    r32.A05(AnonymousClass39x.A01);
                    r22.A06((C57552nF) r32.A02());
                    return;
                } else if (!TextUtils.isEmpty(this.A01)) {
                    C56962mF r03 = ((C27081Fy) r22.A00).A0E;
                    if (r03 == null) {
                        r03 = C56962mF.A02;
                    }
                    AnonymousClass1G4 A0T = r03.A0T();
                    C27081Fy r04 = ((C56962mF) A0T.A00).A01;
                    if (r04 == null) {
                        r04 = C27081Fy.A0i;
                    }
                    AnonymousClass1G4 A0T2 = r04.A0T();
                    A0T2.A03();
                    C27081Fy r15 = (C27081Fy) A0T2.A00;
                    r15.A0C = (C40831sP) r102.A02();
                    r15.A00 |= 64;
                    A0T.A03();
                    C56962mF r16 = (C56962mF) A0T.A00;
                    r16.A01 = (C27081Fy) A0T2.A02();
                    r16.A00 |= 1;
                    r22.A03();
                    C27081Fy r18 = (C27081Fy) r22.A00;
                    r18.A0E = (C56962mF) A0T.A02();
                    r18.A01 |= 512;
                    return;
                } else {
                    r22.A03();
                    C27081Fy r19 = (C27081Fy) r22.A00;
                    r19.A0C = (C40831sP) r102.A02();
                    r19.A00 |= 64;
                    return;
                }
            }
        } else {
            AnonymousClass1X0 r62 = (AnonymousClass1X0) this;
            if (r62.ACL() != null) {
                r62.ACL().A0A(r62, r17);
                AnonymousClass1G3 r23 = r17.A03;
                C57752nZ r110 = ((C27081Fy) r23.A00).A0K;
                if (r110 == null) {
                    r110 = C57752nZ.A07;
                }
                AnonymousClass1G4 A0T3 = r110.A0T();
                C57722nW r111 = ((C57752nZ) A0T3.A00).A05;
                if (r111 == null) {
                    r111 = C57722nW.A06;
                }
                C82513vk r112 = (C82513vk) r111.A0T();
                C16150oX r83 = ((AbstractC16130oV) r62).A02;
                C16460p3 A0G3 = r62.A0G();
                if (r83 != null && ((z2 = r17.A08) || r83.A0U != null)) {
                    C14850m9 r93 = r17.A02;
                    C15570nT r73 = r17.A00;
                    C40831sP r4 = ((C27081Fy) r23.A00).A0C;
                    if (r4 == null) {
                        r4 = C40831sP.A0L;
                    }
                    C81873ui r103 = (C81873ui) r4.A0T();
                    r62.A1C(r73, r83, r93, r103, r17.A04, A0G3, r17.A09, z2, r17.A06);
                    r112.A05();
                    r112.A03();
                    C57722nW r42 = (C57722nW) r112.A00;
                    r42.A02 = r103.A02();
                    r42.A01 = 3;
                }
                A0T3.A03();
                C57752nZ r113 = (C57752nZ) A0T3.A00;
                r113.A05 = (C57722nW) r112.A02();
                r113.A00 |= 1;
                r23.A07((C57752nZ) A0T3.A02());
                return;
            }
            return;
        }
        sb.append((int) b);
        Log.w(sb.toString());
    }

    @Override // X.AbstractC16400ox
    public AbstractC15340mz A7M(AnonymousClass1IS r9) {
        if (this instanceof AnonymousClass1XT) {
            AnonymousClass1XT r3 = (AnonymousClass1XT) this;
            return new AnonymousClass1XT(((AbstractC16130oV) r3).A02, r9, r3, r3.A0I);
        } else if (!(this instanceof AnonymousClass1X0)) {
            return new C16440p1(((AbstractC16130oV) this).A02, r9, this, this.A0y, this.A0I, true);
        } else {
            AnonymousClass1X0 r32 = (AnonymousClass1X0) this;
            return new AnonymousClass1X0(((AbstractC16130oV) r32).A02, r9, r32, r32.A0I, true);
        }
    }
}
