package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.0n5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15390n5 {
    public final TextView A00;
    public final AnonymousClass018 A01;

    public C15390n5(View view, AnonymousClass018 r3) {
        this.A01 = r3;
        this.A00 = (TextView) AnonymousClass028.A0D(view, R.id.product_share_tip);
    }
}
