package X;

import android.os.Parcel;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.internal.IMapViewDelegate;

/* renamed from: X.3T2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3T2 implements AbstractC115105Qf {
    public View A00;
    public final ViewGroup A01;
    public final IMapViewDelegate A02;

    public AnonymousClass3T2(ViewGroup viewGroup, IMapViewDelegate iMapViewDelegate) {
        this.A02 = iMapViewDelegate;
        C13020j0.A01(viewGroup);
        this.A01 = viewGroup;
    }

    public final void A00(AbstractC115755Su r4) {
        try {
            IMapViewDelegate iMapViewDelegate = this.A02;
            BinderC56662lN r0 = new BinderC56662lN(r4);
            C65873Li r2 = (C65873Li) iMapViewDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            r2.A03(9, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
