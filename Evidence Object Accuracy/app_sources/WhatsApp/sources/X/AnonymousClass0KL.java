package X;

import android.content.pm.PackageManager;

/* renamed from: X.0KL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KL {
    public static boolean A00(PackageManager packageManager) {
        return packageManager.hasSystemFeature("android.hardware.fingerprint");
    }
}
