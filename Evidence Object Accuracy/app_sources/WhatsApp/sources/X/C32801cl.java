package X;

import com.whatsapp.util.Log;

/* renamed from: X.1cl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32801cl implements AbstractC21730xt {
    public final /* synthetic */ C19500uD A00;
    public final /* synthetic */ Runnable A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ byte[] A03;
    public final /* synthetic */ byte[] A04;

    public C32801cl(C19500uD r1, Runnable runnable, String str, byte[] bArr, byte[] bArr2) {
        this.A00 = r1;
        this.A02 = str;
        this.A04 = bArr;
        this.A03 = bArr2;
        this.A01 = runnable;
    }

    public final void A00(Runnable runnable, String str, byte[] bArr, byte[] bArr2, byte[] bArr3, int i) {
        if (str == null || bArr2 == null) {
            Log.w("BackupSendMethods/sendGetCipherKey/get-ck/file is null");
        } else {
            StringBuilder sb = new StringBuilder("BackupSendMethods/sendGetCipherKey/success v=");
            sb.append(str);
            Log.i(sb.toString());
            this.A00.A03.A00(str, bArr, bArr2, bArr3, i);
        }
        runnable.run();
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("BackupSendMethods/sendGetCipherKey/failed to deliver id=");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r13, String str) {
        for (AnonymousClass1V8 r1 : r13.A0J("error")) {
            if (r1 != null) {
                String A0I = r1.A0I("code", null);
                String A0I2 = r1.A0I("text", null);
                StringBuilder sb = new StringBuilder("BackupSendMethods/sendGetCipherKey id=");
                sb.append(str);
                sb.append(" error=");
                sb.append(A0I);
                sb.append(" ");
                sb.append(A0I2);
                Log.w(sb.toString());
                if (A0I != null) {
                    int parseInt = Integer.parseInt(A0I);
                    A00(this.A01, this.A02, null, this.A04, this.A03, parseInt);
                }
            }
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r8, String str) {
        AnonymousClass1V8 A0D = r8.A0D(0);
        AnonymousClass009.A05(A0D);
        AnonymousClass1V8.A01(A0D, "crypto");
        byte[] bArr = A0D.A0F("password").A01;
        A00(this.A01, this.A02, bArr, this.A04, this.A03, 0);
    }
}
