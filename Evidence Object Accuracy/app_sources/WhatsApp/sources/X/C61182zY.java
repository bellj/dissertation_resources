package X;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.ConversationListRowHeaderView;
import com.whatsapp.conversationslist.ViewHolder;

/* renamed from: X.2zY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61182zY extends AbstractC65103Id {
    public C61232zd A00;
    public final Context A01;
    public final C15550nR A02;
    public final C63563Cb A03;
    public final AbstractC33091dK A04;
    public final AnonymousClass018 A05;

    public C61182zY(Activity activity, Context context, C15570nT r29, C15450nH r30, AnonymousClass11L r31, C14650lo r32, C238013b r33, C15550nR r34, C15610nY r35, C63563Cb r36, AbstractC33091dK r37, C63543Bz r38, ViewHolder viewHolder, C14830m7 r40, C16590pI r41, AnonymousClass018 r42, C19990v2 r43, C241714m r44, C14850m9 r45, C20710wC r46, AnonymousClass13H r47, C22710zW r48, C17070qD r49, AnonymousClass14X r50, AnonymousClass3J9 r51) {
        super(activity, context, r29, r30, r31, r32, r33, r34, r35, r37, r38, viewHolder, r40, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51);
        this.A01 = context;
        this.A05 = r42;
        this.A03 = r36;
        this.A04 = r37;
        this.A02 = r34;
    }

    public final void A07(AnonymousClass4Q9 r17) {
        AbstractC15340mz r14 = r17.A02;
        C15370n3 r12 = r17.A00;
        C15370n3 r13 = r17.A01;
        AnonymousClass018 r5 = this.A05;
        ViewHolder viewHolder = this.A0B;
        View view = viewHolder.A05;
        Context context = this.A01;
        C42941w9.A0A(view, r5, context.getResources().getDimensionPixelSize(R.dimen.list_row_padding), 0, 0, 0);
        View view2 = viewHolder.A04;
        view2.setVisibility(8);
        viewHolder.A0G.setVisibility(8);
        viewHolder.A0E.setVisibility(8);
        viewHolder.A0B.setVisibility(8);
        viewHolder.A0D.setVisibility(8);
        viewHolder.A0C.setVisibility(8);
        viewHolder.A0A.setVisibility(8);
        viewHolder.A03.setVisibility(8);
        viewHolder.A0J(false, 0);
        TextEmojiLabel textEmojiLabel = viewHolder.A0L;
        textEmojiLabel.setVisibility(0);
        C12960it.A0s(context, textEmojiLabel, R.color.list_item_sub_title);
        textEmojiLabel.setTypeface(null, 0);
        textEmojiLabel.setPlaceholder(0);
        C64303Fa r2 = viewHolder.A00;
        int A00 = AnonymousClass00T.A00(context, R.color.list_item_sub_title);
        ConversationListRowHeaderView conversationListRowHeaderView = r2.A00;
        conversationListRowHeaderView.A01.setTextColor(A00);
        conversationListRowHeaderView.A01.setVisibility(0);
        ImageView imageView = viewHolder.A08;
        imageView.setEnabled(false);
        imageView.setOnClickListener(null);
        imageView.setOnLongClickListener(null);
        view2.setOnClickListener(null);
        view2.setOnLongClickListener(null);
        C12960it.A14(((AnonymousClass03U) viewHolder).A0H, this, r14, 2);
        r2.A03(r12, this.A0N, this.A04.AFi());
        r2.A02(r12);
        A06(null, r12, r13, r14, null);
    }
}
