package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.whatsapp.MessageDialogFragment;
import com.whatsapp.R;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0ml  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15220ml {
    public C14850m9 A00;
    public final C18640sm A01;
    public final AnonymousClass018 A02;
    public final C254819o A03;
    public final C252018m A04;

    public C15220ml(C18640sm r1, AnonymousClass018 r2, C254819o r3, C252018m r4) {
        this.A04 = r4;
        this.A02 = r2;
        this.A03 = r3;
        this.A01 = r1;
    }

    public void A00(ActivityC000900k r8, String str) {
        Intent intent;
        if (!this.A01.A0B()) {
            boolean A03 = C18640sm.A03((Context) r8);
            int i = R.string.network_required;
            if (A03) {
                i = R.string.network_required_airplane_on;
            }
            AnonymousClass2AC A01 = MessageDialogFragment.A01(new Object[0], i);
            A01.A02(DialogInterface$OnClickListenerC97494hA.A00, R.string.ok);
            A01.A01().A1F(r8.A0V(), null);
            return;
        }
        boolean A07 = this.A00.A07(1836);
        Context baseContext = r8.getBaseContext();
        if (A07) {
            intent = new Intent();
            intent.setClassName(baseContext, "com.whatsapp.wabloks.ui.WaBloksActivity");
            try {
                intent.putExtra("screen_params", new JSONObject().put("params", new JSONObject().put("server_params", new JSONObject().put("entrypointid", str))).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra("screen_name", "com.bloks.www.cxthelp.whatsapp.cms2bloks");
        } else {
            Uri.Builder A012 = this.A04.A01();
            A012.appendPath("cxt");
            A012.appendQueryParameter("entrypointid", str);
            AnonymousClass018 r3 = this.A02;
            A012.appendQueryParameter("lg", r3.A06());
            A012.appendQueryParameter("lc", r3.A05());
            A012.appendQueryParameter("platform", "android");
            A012.appendQueryParameter("anid", (String) this.A03.A00().second);
            String obj = A012.toString();
            intent = new Intent();
            intent.setClassName(baseContext, "com.whatsapp.contextualhelp.ContextualHelpActivity");
            intent.putExtra("webview_url", obj);
            intent.putExtra("webview_hide_url", true);
            intent.putExtra("webview_javascript_enabled", true);
            intent.putExtra("webview_avoid_external", true);
        }
        r8.startActivity(intent);
    }
}
