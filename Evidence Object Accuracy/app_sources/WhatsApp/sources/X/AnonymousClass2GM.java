package X;

import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.2GM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GM {
    @Deprecated
    public static List A00(byte[] bArr) {
        try {
            return new ArrayList((Collection) new ObjectInputStream(new ByteArrayInputStream(bArr)).readObject());
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            Log.e("ContactUtil/getContactsFromBytes/error getting contacts from data", e);
            return new ArrayList();
        }
    }
}
