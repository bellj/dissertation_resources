package X;

import android.content.ContentResolver;
import android.net.Uri;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

/* renamed from: X.4dN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95244dN {
    public static Object A00;
    public static HashMap A01;
    public static boolean A02;
    public static String[] A03 = new String[0];
    public static final Uri A04 = Uri.parse("content://com.google.android.gsf.gservices");
    public static final Uri A05 = Uri.parse("content://com.google.android.gsf.gservices/prefix");
    public static final HashMap A06 = C12970iu.A11();
    public static final HashMap A07 = C12970iu.A11();
    public static final HashMap A08 = C12970iu.A11();
    public static final HashMap A09 = C12970iu.A11();
    public static final AtomicBoolean A0A = new AtomicBoolean();
    public static final Pattern A0B = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
    public static final Pattern A0C = Pattern.compile("^(0|false|f|off|no|n)$", 2);

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0079, code lost:
        if (r0 != null) goto L_0x007b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(android.content.ContentResolver r13, java.lang.String r14) {
        /*
        // Method dump skipped, instructions count: 215
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95244dN.A00(android.content.ContentResolver, java.lang.String):java.lang.String");
    }

    public static void A01(ContentResolver contentResolver) {
        if (A01 == null) {
            A0A.set(false);
            A01 = C12970iu.A11();
            A00 = C12970iu.A0l();
            A02 = false;
            contentResolver.registerContentObserver(A04, true, new C72953fR());
        } else if (A0A.getAndSet(false)) {
            A01.clear();
            A06.clear();
            A07.clear();
            A08.clear();
            A09.clear();
            A00 = C12970iu.A0l();
            A02 = false;
        }
    }
}
