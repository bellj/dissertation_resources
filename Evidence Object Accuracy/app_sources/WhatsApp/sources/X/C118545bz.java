package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.payments.ui.NoviServiceSelectionBottomSheet;
import java.util.List;

/* renamed from: X.5bz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118545bz extends AnonymousClass02M {
    public List A00;
    public final AnonymousClass018 A01;
    public final NoviServiceSelectionBottomSheet A02;

    public C118545bz(AnonymousClass018 r1, NoviServiceSelectionBottomSheet noviServiceSelectionBottomSheet) {
        this.A01 = r1;
        this.A02 = noviServiceSelectionBottomSheet;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        List list = this.A00;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r5, int i) {
        C118775cM r52 = (C118775cM) r5;
        C127705uv r3 = (C127705uv) this.A00.get(i);
        r52.A01.setText(r3.A02);
        r52.A00.setText(r3.A01);
        View view = r52.A0H;
        C117295Zj.A0n(view, this, 106);
        Context context = view.getContext();
        int i2 = r3.A00;
        WaImageView waImageView = r52.A02;
        if (i2 == 0) {
            i2 = R.drawable.black_alpha_10_circle;
        }
        C12990iw.A0x(context, waImageView, i2);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C118775cM(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.selection_row), this);
    }
}
