package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Pk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28891Pk {
    public final Long A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final List A04;

    public C28891Pk(Long l, String str, String str2, String str3, List list) {
        this.A01 = str;
        this.A02 = str2;
        this.A04 = list;
        this.A03 = str3;
        this.A00 = l;
    }

    public C28891Pk A00() {
        String str = this.A01;
        String str2 = this.A02;
        List list = this.A04;
        return new C28891Pk(this.A00, str, str2, this.A03, list != null ? new ArrayList(list) : null);
    }
}
