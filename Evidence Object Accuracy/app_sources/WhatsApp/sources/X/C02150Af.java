package X;

import android.util.AndroidRuntimeException;

/* renamed from: X.0Af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02150Af extends AndroidRuntimeException {
    public C02150Af(String str) {
        super(str);
    }
}
