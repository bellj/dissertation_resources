package X;

/* renamed from: X.1Y8  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Y8 extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass1Y8() {
        super(2428, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamMdDisabledReason {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "participantMigrationStatus", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
