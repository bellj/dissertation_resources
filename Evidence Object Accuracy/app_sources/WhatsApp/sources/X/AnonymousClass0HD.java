package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0HD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HD extends AbstractC08070aX {
    public final C08020aS A00;

    public AnonymousClass0HD(AnonymousClass0AA r5, AnonymousClass0PU r6) {
        super(r5, r6);
        C08020aS r2 = new C08020aS(r5, new C08220am("__container", r6.A0K, false), this);
        this.A00 = r2;
        r2.Aby(Collections.emptyList(), Collections.emptyList());
    }

    @Override // X.AbstractC08070aX
    public void A04(C06430To r2, C06430To r3, List list, int i) {
        this.A00.Aan(r2, r3, list, i);
    }

    @Override // X.AbstractC08070aX
    public void A06(Canvas canvas, Matrix matrix, int i) {
        this.A00.A9C(canvas, matrix, i);
    }

    @Override // X.AbstractC08070aX, X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        super.AAy(matrix, rectF, z);
        this.A00.AAy(this.A08, rectF, z);
    }
}
