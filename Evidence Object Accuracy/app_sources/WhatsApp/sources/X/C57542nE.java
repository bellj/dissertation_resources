package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57542nE extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57542nE A07;
    public static volatile AnonymousClass255 A08;
    public int A00;
    public C43261wh A01;
    public C57382mw A02;
    public C57652nP A03;
    public String A04 = "";
    public String A05 = "";
    public String A06 = "";

    static {
        C57542nE r0 = new C57542nE();
        A07 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C81603uH r1;
        C82263vL r12;
        C82273vM r13;
        switch (r7.ordinal()) {
            case 0:
                return A07;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57542nE r9 = (C57542nE) obj2;
                this.A03 = (C57652nP) r8.Aft(this.A03, r9.A03);
                this.A05 = r8.Afy(this.A05, r9.A05, C12960it.A1V(this.A00 & 2, 2), C12960it.A1V(r9.A00 & 2, 2));
                this.A02 = (C57382mw) r8.Aft(this.A02, r9.A02);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 8, 8);
                String str = this.A04;
                int i2 = r9.A00;
                this.A04 = r8.Afy(str, r9.A04, A1V, C12960it.A1V(i2 & 8, 8));
                this.A06 = r8.Afy(this.A06, r9.A06, C12960it.A1V(i & 16, 16), C12960it.A1V(i2 & 16, 16));
                this.A01 = (C43261wh) r8.Aft(this.A01, r9.A01);
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                if ((this.A00 & 1) == 1) {
                                    r13 = (C82273vM) this.A03.A0T();
                                } else {
                                    r13 = null;
                                }
                                C57652nP r0 = (C57652nP) AbstractC27091Fz.A0H(r82, r92, C57652nP.A0C);
                                this.A03 = r0;
                                if (r13 != null) {
                                    this.A03 = (C57652nP) AbstractC27091Fz.A0C(r13, r0);
                                }
                                this.A00 |= 1;
                            } else if (A03 == 18) {
                                String A0A = r82.A0A();
                                this.A00 |= 2;
                                this.A05 = A0A;
                            } else if (A03 == 34) {
                                if ((this.A00 & 4) == 4) {
                                    r12 = (C82263vL) this.A02.A0T();
                                } else {
                                    r12 = null;
                                }
                                C57382mw r02 = (C57382mw) AbstractC27091Fz.A0H(r82, r92, C57382mw.A04);
                                this.A02 = r02;
                                if (r12 != null) {
                                    this.A02 = (C57382mw) AbstractC27091Fz.A0C(r12, r02);
                                }
                                this.A00 |= 4;
                            } else if (A03 == 42) {
                                String A0A2 = r82.A0A();
                                this.A00 |= 8;
                                this.A04 = A0A2;
                            } else if (A03 == 50) {
                                String A0A3 = r82.A0A();
                                this.A00 |= 16;
                                this.A06 = A0A3;
                            } else if (A03 == 138) {
                                if ((this.A00 & 32) == 32) {
                                    r1 = (C81603uH) this.A01.A0T();
                                } else {
                                    r1 = null;
                                }
                                C43261wh r03 = (C43261wh) AbstractC27091Fz.A0H(r82, r92, C43261wh.A0O);
                                this.A01 = r03;
                                if (r1 != null) {
                                    this.A01 = (C43261wh) AbstractC27091Fz.A0C(r1, r03);
                                }
                                this.A00 |= 32;
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57542nE();
            case 5:
                return new C82253vK();
            case 6:
                break;
            case 7:
                if (A08 == null) {
                    synchronized (C57542nE.class) {
                        if (A08 == null) {
                            A08 = AbstractC27091Fz.A09(A07);
                        }
                    }
                }
                return A08;
            default:
                throw C12970iu.A0z();
        }
        return A07;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C57652nP r0 = this.A03;
            if (r0 == null) {
                r0 = C57652nP.A0C;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A05, i2);
        }
        if ((this.A00 & 4) == 4) {
            C57382mw r02 = this.A02;
            if (r02 == null) {
                r02 = C57382mw.A04;
            }
            i2 = AbstractC27091Fz.A08(r02, 4, i2);
        }
        if ((this.A00 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(5, this.A04, i2);
        }
        if ((this.A00 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(6, this.A06, i2);
        }
        if ((this.A00 & 32) == 32) {
            C43261wh r03 = this.A01;
            if (r03 == null) {
                r03 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r03, 17, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C57652nP r0 = this.A03;
            if (r0 == null) {
                r0 = C57652nP.A0C;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A05);
        }
        if ((this.A00 & 4) == 4) {
            C57382mw r02 = this.A02;
            if (r02 == null) {
                r02 = C57382mw.A04;
            }
            codedOutputStream.A0L(r02, 4);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(5, this.A04);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(6, this.A06);
        }
        if ((this.A00 & 32) == 32) {
            C43261wh r03 = this.A01;
            if (r03 == null) {
                r03 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r03, 17);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
