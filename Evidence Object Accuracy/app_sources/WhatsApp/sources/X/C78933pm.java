package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.3pm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78933pm extends AbstractC78753pU {
    public static final HashMap A06;
    public static final Parcelable.Creator CREATOR = new C98544ir();
    public C78943pn A00;
    public String A01;
    public String A02;
    public String A03;
    public final int A04;
    public final Set A05;

    public C78933pm() {
        this.A05 = new HashSet(3);
        this.A04 = 1;
    }

    public C78933pm(C78943pn r1, String str, String str2, String str3, Set set, int i) {
        this.A05 = set;
        this.A04 = i;
        this.A00 = r1;
        this.A02 = str;
        this.A01 = str2;
        this.A03 = str3;
    }

    static {
        HashMap A11 = C12970iu.A11();
        A06 = A11;
        A11.put("authenticatorInfo", new C78633pE(C78943pn.class, "authenticatorInfo", 11, 11, 2, false, false));
        A11.put("signature", new C78633pE(null, "signature", 7, 7, 3, false, false));
        A11.put("package", new C78633pE(null, "package", 7, 7, 4, false, false));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        Set set = this.A05;
        if (AbstractC78753pU.A00(set, 1)) {
            C95654e8.A07(parcel, 1, this.A04);
        }
        if (AbstractC78753pU.A00(set, 2)) {
            C95654e8.A0B(parcel, this.A00, 2, i, true);
        }
        if (AbstractC78753pU.A00(set, 3)) {
            C95654e8.A0D(parcel, this.A02, 3, true);
        }
        if (AbstractC78753pU.A00(set, 4)) {
            C95654e8.A0D(parcel, this.A01, 4, true);
        }
        if (AbstractC78753pU.A00(set, 5)) {
            C95654e8.A0D(parcel, this.A03, 5, true);
        }
        C95654e8.A06(parcel, A00);
    }
}
