package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC59082tw extends AnonymousClass2EI {
    public final UserJid A00;

    public AbstractC59082tw(C14650lo r1, UserJid userJid) {
        super(r1);
        this.A00 = userJid;
    }

    public void A02() {
        C14650lo r3 = this.A01;
        UserJid userJid = this.A00;
        C246816l r1 = r3.A07;
        if (r1.A01(userJid) == null || r1.A06(userJid)) {
            r3.A03(new AbstractC30111Wd() { // from class: X.53M
                @Override // X.AbstractC30111Wd
                public final void ANP(C30141Wg r6) {
                    AbstractC59082tw r4 = AbstractC59082tw.this;
                    if (r6 == null) {
                        r4.A01.A04(new AnonymousClass53W(r4), r4.A00, null);
                        return;
                    }
                    r4.A04(r6);
                }
            }, userJid);
        } else {
            A03();
        }
    }

    public void A03() {
        AnonymousClass1Q5 A00;
        if (this instanceof C59042ts) {
            C59042ts r6 = (C59042ts) this;
            String A01 = r6.A07.A01();
            r6.A08.A03("view_product_tag");
            C19870uo r4 = r6.A06;
            AnonymousClass4TA r9 = r6.A05;
            C14650lo r0 = ((AnonymousClass2EI) r6).A01;
            UserJid userJid = r9.A00;
            String A012 = r0.A07.A01(userJid);
            String str = r9.A03;
            AnonymousClass009.A0B("catalog productId cannot be null or empty", !TextUtils.isEmpty(str));
            ArrayList A0l = C12960it.A0l();
            AnonymousClass2EI.A00("product_id", str, A0l);
            Integer num = r9.A02;
            if (num != null) {
                AnonymousClass2EI.A00("width", num.toString(), A0l);
            }
            Integer num2 = r9.A01;
            if (num2 != null) {
                AnonymousClass2EI.A00("height", num2.toString(), A0l);
            }
            AnonymousClass2EI.A00("catalog_session_id", r9.A04, A0l);
            if (r9.A05) {
                AnonymousClass2EI.A00("fetch_compliance_info", "true", A0l);
            }
            if (!TextUtils.isEmpty(A012)) {
                AnonymousClass2EI.A00("direct_connection_encrypted_info", A012, A0l);
            }
            AnonymousClass1V8 r7 = new AnonymousClass1V8("product", new AnonymousClass1W9[]{new AnonymousClass1W9(userJid, "jid")}, (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[0]));
            AnonymousClass1W9[] r3 = new AnonymousClass1W9[4];
            C12960it.A1M("id", A01, r3, 0);
            C12960it.A1M("xmlns", "w:biz:catalog", r3, 1);
            C12960it.A1M("type", "get", r3, 2);
            r4.A02(r6, AnonymousClass1V8.A00(AnonymousClass1VY.A00, r7, r3, 3), A01, 196);
        } else if (this instanceof C59062tu) {
            C59062tu r42 = (C59062tu) this;
            String A013 = r42.A06.A01();
            AnonymousClass4Tm r2 = r42.A01;
            String str2 = r2.A06;
            if (str2 == null) {
                int i = r2.A02;
                C19840ul r1 = r42.A07;
                if (i == 0) {
                    r1.A03("collection_management_view_tag");
                } else {
                    AnonymousClass1Q5 A002 = C19840ul.A00(r1);
                    if (A002 != null) {
                        A002.A08("datasource_collections");
                    }
                }
            }
            C19870uo r62 = r42.A05;
            ArrayList A0l2 = C12960it.A0l();
            AnonymousClass2EI.A00("width", Integer.toString(r2.A03), A0l2);
            AnonymousClass2EI.A00("height", Integer.toString(r2.A01), A0l2);
            if (str2 != null) {
                AnonymousClass2EI.A00("after", str2, A0l2);
            }
            String str3 = r2.A07;
            if (str3 != null) {
                AnonymousClass2EI.A00("catalog_session_id", str3, A0l2);
            }
            AnonymousClass2EI.A00("collection_limit", Integer.toString(r2.A00), A0l2);
            AnonymousClass2EI.A00("item_limit", Integer.toString(r2.A02), A0l2);
            C14650lo r02 = ((AnonymousClass2EI) r42).A01;
            UserJid userJid2 = r2.A05;
            String A014 = r02.A07.A01(userJid2);
            if (A014 != null) {
                AnonymousClass2EI.A00("direct_connection_encrypted_info", A014, A0l2);
            }
            AnonymousClass1V8 r72 = new AnonymousClass1V8("collections", new AnonymousClass1W9[]{new AnonymousClass1W9(userJid2, "biz_jid")}, (AnonymousClass1V8[]) A0l2.toArray(new AnonymousClass1V8[0]));
            AnonymousClass1W9[] r5 = new AnonymousClass1W9[5];
            r5[0] = new AnonymousClass1W9(r2.A04, "to");
            C12960it.A1M("id", A013, r5, 1);
            C12960it.A1M("smax_id", "35", r5, 2);
            C12960it.A1M("xmlns", "w:biz:catalog", r5, 3);
            C12960it.A1M("type", "get", r5, 4);
            r62.A02(r42, new AnonymousClass1V8(r72, "iq", r5), A013, 271);
        } else if (this instanceof C59032tr) {
            C59032tr r63 = (C59032tr) this;
            AnonymousClass2EK.A00(r63.A02, 0);
            String A015 = r63.A06.A01();
            C14650lo r03 = r63.A01;
            UserJid userJid3 = ((AbstractC59082tw) r63).A00;
            String A016 = r03.A07.A01(userJid3);
            r63.A07.A03("plm_details_view_tag");
            C19870uo r32 = r63.A05;
            List list = r63.A0A;
            String str4 = r63.A09;
            String str5 = r63.A08;
            ArrayList A0l3 = C12960it.A0l();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                A0l3.add(new AnonymousClass1V8(new AnonymousClass1V8("id", C12970iu.A0x(it), (AnonymousClass1W9[]) null), "product", (AnonymousClass1W9[]) null));
            }
            AnonymousClass2EI.A00("width", str4, A0l3);
            AnonymousClass2EI.A00("height", str5, A0l3);
            if (A016 != null) {
                AnonymousClass2EI.A00("direct_connection_encrypted_info", A016, A0l3);
            }
            AnonymousClass1V8 r8 = new AnonymousClass1V8("product_list", new AnonymousClass1W9[]{new AnonymousClass1W9(userJid3, "jid")}, (AnonymousClass1V8[]) A0l3.toArray(new AnonymousClass1V8[0]));
            AnonymousClass1W9[] r73 = new AnonymousClass1W9[5];
            C12960it.A1M("id", A015, r73, 0);
            C12960it.A1M("xmlns", "w:biz:catalog", r73, 1);
            C12960it.A1M("type", "get", r73, 2);
            C12960it.A1M("smax_id", "21", r73, 3);
            r32.A02(r63, AnonymousClass1V8.A00(AnonymousClass1VY.A00, r8, r73, 4), A015, 164);
            Log.i(C12960it.A0b("RequestBizProductListProtocolHelper/doSendRequest/jid=", userJid3));
        } else if (!(this instanceof C59072tv)) {
            C59052tt r74 = (C59052tt) this;
            C17220qS r64 = r74.A04;
            String A017 = r64.A01();
            C14650lo r04 = r74.A00;
            UserJid userJid4 = ((AbstractC59082tw) r74).A00;
            String A018 = r04.A07.A01(userJid4);
            if (A018 != null) {
                AnonymousClass1V8 r43 = new AnonymousClass1V8(new AnonymousClass1V8("direct_connection_encrypted_info", A018, (AnonymousClass1W9[]) null), "verify_postcode", new AnonymousClass1W9[]{new AnonymousClass1W9(userJid4, "biz_jid")});
                AnonymousClass1W9[] r33 = new AnonymousClass1W9[5];
                C12960it.A1M("id", A017, r33, 0);
                C12960it.A1M("xmlns", "w:biz:catalog", r33, 1);
                r33[2] = new AnonymousClass1W9("type", "get");
                r33[3] = new AnonymousClass1W9("smax_id", "70");
                r33[4] = new AnonymousClass1W9(AnonymousClass1VY.A00, "to");
                r64.A0A(r74, new AnonymousClass1V8(r43, "iq", r33), A017, 317, 32000);
                return;
            }
            r74.A03.AU2("error");
        } else {
            C59072tv r52 = (C59072tv) this;
            String A019 = r52.A07.A01();
            AnonymousClass28H r22 = r52.A04;
            String str6 = r22.A06;
            if (str6 == null && (A00 = C19840ul.A00(r52.A08)) != null) {
                A00.A08("datasource_catalog");
            }
            C19870uo r65 = r52.A06;
            C14650lo r05 = ((AnonymousClass2EI) r52).A01;
            UserJid userJid5 = r22.A05;
            String A0110 = r05.A07.A01(userJid5);
            ArrayList A0l4 = C12960it.A0l();
            AnonymousClass2EI.A00("limit", Integer.toString(r22.A02), A0l4);
            AnonymousClass2EI.A00("width", Integer.toString(r22.A04), A0l4);
            AnonymousClass2EI.A00("height", Integer.toString(r22.A03), A0l4);
            if (str6 != null) {
                AnonymousClass2EI.A00("after", str6, A0l4);
            }
            String str7 = r22.A07;
            if (str7 != null) {
                AnonymousClass2EI.A00("catalog_session_id", str7, A0l4);
            }
            if (A0110 != null) {
                AnonymousClass2EI.A00("direct_connection_encrypted_info", A0110, A0l4);
            }
            ArrayList A0l5 = C12960it.A0l();
            A0l5.add(new AnonymousClass1W9(userJid5, "jid"));
            Boolean bool = Boolean.TRUE;
            if (bool == r22.A01) {
                A0l5.add(new AnonymousClass1W9("allow_shop_source", bool.toString()));
            }
            AnonymousClass1V8 r75 = new AnonymousClass1V8("product_catalog", (AnonymousClass1W9[]) A0l5.toArray(new AnonymousClass1W9[A0l5.size()]), (AnonymousClass1V8[]) A0l4.toArray(new AnonymousClass1V8[0]));
            AnonymousClass1W9[] r34 = new AnonymousClass1W9[4];
            C12960it.A1M("id", A019, r34, 0);
            C12960it.A1M("xmlns", "w:biz:catalog", r34, 1);
            C12960it.A1M("type", "get", r34, 2);
            r65.A02(r52, AnonymousClass1V8.A00(AnonymousClass1VY.A00, r75, r34, 3), A019, 164);
        }
    }

    public void A04(C30141Wg r4) {
        if (r4 == null || !r4.A0H) {
            A03();
            return;
        }
        String str = r4.A08;
        if (str != null) {
            this.A01.A06(this.A00, str);
        }
        C14650lo r0 = this.A01;
        r0.A07.A02(this, this.A00, false);
    }

    public void A05(UserJid userJid, int i) {
        AbstractC15710nm r3;
        String str;
        String str2;
        if (this instanceof C59042ts) {
            C59042ts r2 = (C59042ts) this;
            Log.e(C12960it.A0W(i, "ProductRequestProtocolHelper/onError/error - "));
            r2.A08.A02("view_product_tag");
            r2.A01.A00(r2.A05, i);
        } else if (!(this instanceof C59062tu)) {
            if (this instanceof C59032tr) {
                C59032tr r22 = (C59032tr) this;
                r22.A07.A02("plm_details_view_tag");
                Log.e(C12960it.A0d(userJid.getRawString(), C12960it.A0k("RequestBizProductListProtocolHelper/onError/response-error, jid = ")));
                AnonymousClass2EK.A00(r22.A02, 2);
                r3 = r22.A00;
                str = C12960it.A0W(i, "error_code=");
                str2 = "RequestBizProductListProtocolHelper/get product list error";
            } else if (!(this instanceof C59072tv)) {
                ((C59052tt) this).A07("error");
                return;
            } else {
                C59072tv r23 = (C59072tv) this;
                r23.A06();
                Log.e(C12960it.A0b("sendGetBizProductCatalog/response-error/jid=", userJid));
                r23.A01.AQ9(r23.A04, i);
                r3 = r23.A00;
                str = C12960it.A0W(i, "error_code=");
                str2 = "RequestBizProductCatalogProtocolHelper/get product catalog error";
            }
            r3.AaV(str2, str, true);
        } else {
            C59062tu r1 = (C59062tu) this;
            r1.A07();
            Log.e(C12960it.A0W(i, "GetCollectionsProtocol/onError/error - "));
            r1.A00.A00(i);
        }
    }

    @Override // X.AnonymousClass1W3
    public final void APB(UserJid userJid) {
        Log.e(C12960it.A0d(userJid.getRawString(), C12960it.A0k("DirectConnectionProtocolHelperEntryPoint/onDirectConnectionError, jid = ")));
        A05(userJid, 422);
    }

    @Override // X.AnonymousClass1W3
    public final void APC(UserJid userJid) {
        Log.i(C12960it.A0d(userJid.getRawString(), C12960it.A0k("DirectConnectionProtocolHelperEntryPoint/onDirectConnectionSucceeded, jid = ")));
        A03();
    }

    @Override // X.AbstractC21730xt
    public final void APv(AnonymousClass1V8 r4, String str) {
        StringBuilder sb;
        UserJid userJid;
        String A0d;
        int A00 = C41151sz.A00(r4);
        if (A00 != 421 || super.A00) {
            A05(this.A00, A00);
            return;
        }
        if (!(this instanceof C59042ts)) {
            if (!(this instanceof C59062tu)) {
                if (this instanceof C59032tr) {
                    C59032tr r2 = (C59032tr) this;
                    r2.A07.A02("plm_details_view_tag");
                    A0d = C12960it.A0d(((AbstractC59082tw) r2).A00.getRawString(), C12960it.A0k("RequestBizProductListProtocolHelper/onDirectConnectionRevokeKey, jid = "));
                } else if (!(this instanceof C59072tv)) {
                    A0d = "DCPostcodeVerificationProtocolHelper/onDirectConnectionRevokeKey";
                } else {
                    C59072tv r22 = (C59072tv) this;
                    r22.A06();
                    sb = C12960it.A0k("sendGetBizProductCatalog/onDirectConnectionRevokeKey/jid=");
                    userJid = ((AbstractC59082tw) r22).A00;
                }
                Log.i(A0d);
            } else {
                C59062tu r23 = (C59062tu) this;
                r23.A07();
                sb = C12960it.A0k("GetCollectionsProtocol/onDirectConnectionRevokeKey/jid - ");
                userJid = r23.A01.A05;
            }
            sb.append(userJid);
            C12960it.A1F(sb);
        } else {
            Log.i("ProductRequestProtocolHelper/onDirectConnectionRevokeKey");
            ((C59042ts) this).A08.A02("view_product_tag");
        }
        super.A00 = true;
        C14650lo r0 = this.A01;
        r0.A07.A02(this, this.A00, true);
    }
}
