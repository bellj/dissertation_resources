package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25I  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25I extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25I A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public int A02;
    public AbstractC27881Jp A03 = AbstractC27881Jp.A01;
    public AnonymousClass1K6 A04 = AnonymousClass277.A01;

    static {
        AnonymousClass25I r0 = new AnonymousClass25I();
        A05 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i = CodedOutputStream.A04(1, this.A01) + 0;
        } else {
            i = 0;
        }
        if ((i3 & 2) == 2) {
            i += CodedOutputStream.A04(2, this.A02);
        }
        int i4 = 0;
        for (int i5 = 0; i5 < this.A04.size(); i5++) {
            int A03 = ((AbstractC27881Jp) this.A04.get(i5)).A03();
            i4 += CodedOutputStream.A01(A03) + A03;
        }
        int size = i + i4 + this.A04.size();
        if ((this.A00 & 4) == 4) {
            size += CodedOutputStream.A09(this.A03, 4);
        }
        int A00 = size + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(2, this.A02);
        }
        for (int i = 0; i < this.A04.size(); i++) {
            codedOutputStream.A0K((AbstractC27881Jp) this.A04.get(i), 3);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A03, 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
