package X;

import com.whatsapp.jid.DeviceJid;

/* renamed from: X.1PM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PM {
    public final AnonymousClass1JT A00;
    public final DeviceJid A01;
    public final boolean A02;
    public final boolean A03;

    public AnonymousClass1PM(AnonymousClass1PL r2) {
        this.A01 = r2.A03;
        this.A00 = r2.A00;
        this.A03 = r2.A02;
        this.A02 = r2.A01;
    }
}
