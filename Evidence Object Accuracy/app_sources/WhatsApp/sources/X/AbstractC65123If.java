package X;

import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Pair;
import android.view.View;
import java.util.Locale;

/* renamed from: X.3If  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC65123If {
    public C16150oX A00;
    public final int A01;

    public AbstractC65123If(int i) {
        this.A01 = i;
    }

    public static float A01(int i, float f) {
        return View.MeasureSpec.getMode(i) != 0 ? Math.min((float) View.MeasureSpec.getSize(i), f) : f;
    }

    public static Pair A02(float f, float f2) {
        return Pair.create(Integer.valueOf(Math.round(f)), Integer.valueOf(Math.round(f2)));
    }

    public int A03() {
        int i = this.A01;
        int A04 = A04();
        AnonymousClass009.A0E(C12960it.A1S(A04));
        return Math.round((((float) i) * ((float) A04)) / 100.0f);
    }

    public int A04() {
        if (this instanceof C61092zO) {
            return 72;
        }
        if (!(this instanceof C61112zQ)) {
            return 100;
        }
        C61112zQ r1 = (C61112zQ) this;
        return (r1.A0A() ? r1.A01 : r1.A00).A02;
    }

    public RectF A05(int i, int i2) {
        C16150oX r1;
        if (this instanceof C61092zO) {
            return A06(C61092zO.A00, i, i2);
        }
        if (this instanceof C61112zQ) {
            C61112zQ r12 = (C61112zQ) this;
            if (!(r12 instanceof C61102zP)) {
                return r12.A06(r12.A00, i, i2);
            }
            throw C12970iu.A0z();
        } else if (!(this instanceof C61082zN) || (r1 = this.A00) == null) {
            return null;
        } else {
            int i3 = r1.A08;
            int i4 = r1.A06;
            int i5 = i3 * i2;
            float f = (float) i;
            float f2 = (float) i3;
            if (i5 > i4 * i) {
                f = (float) i2;
                f2 = (float) i4;
            }
            return new RectF(0.0f, 0.0f, f2, ((float) i2) / (f / f2));
        }
    }

    public final RectF A06(C63643Cj r13, int i, int i2) {
        float f;
        float f2;
        C16150oX r1 = this.A00;
        if (r1 == null || i2 <= 0 || i <= 0) {
            return null;
        }
        float f3 = (float) r1.A08;
        float f4 = (float) r1.A06;
        PointF pointF = new PointF(f3 / 2.0f, f4 / 2.0f);
        float f5 = (float) i;
        float f6 = (float) i2;
        PointF pointF2 = new PointF(f5 / 2.0f, f6 / 2.0f);
        if (A0A()) {
            int i3 = this.A00.A03;
            if (i3 > 0) {
                f2 = (float) i3;
            } else {
                f2 = f4 / 3.0f;
            }
            pointF.y = f2;
            pointF2.y = f6 / 3.0f;
        }
        float f7 = f3 / f5;
        float f8 = f6 * f7;
        float f9 = f3 / f4;
        if (f9 > r13.A01 / r13.A00) {
            f7 = f4 / f6;
            f = f5 * f7;
            f8 = f4;
        } else {
            f = f3;
        }
        PointF pointF3 = new PointF(pointF2.x * f7, pointF2.y * f7);
        RectF A0K = C12980iv.A0K();
        float f10 = pointF.x - pointF3.x;
        A0K.left = f10;
        float f11 = pointF.y - pointF3.y;
        A0K.top = f11;
        A0K.right = f10 + f;
        float f12 = f11 + f8;
        A0K.bottom = f12;
        if (f11 < 0.0f) {
            A0K.top = 0.0f;
            A0K.bottom = f8;
            f12 = f8;
        }
        if (f12 > f4) {
            A0K.bottom = f4;
            A0K.top = f4 - f8;
        }
        Locale locale = Locale.US;
        Object[] objArr = new Object[12];
        objArr[0] = Float.valueOf(f3);
        objArr[1] = Float.valueOf(f4);
        objArr[2] = Float.valueOf(f9);
        C16150oX r8 = this.A00;
        C12960it.A1P(objArr, r8.A02, 3);
        C12960it.A1P(objArr, r8.A03, 4);
        C12960it.A1P(objArr, i, 5);
        C12960it.A1P(objArr, i2, 6);
        objArr[7] = Float.valueOf(f5 / f6);
        objArr[8] = Float.valueOf(f);
        objArr[9] = Float.valueOf(f8);
        objArr[10] = Float.valueOf(f / f8);
        objArr[11] = A0K.toString();
        String.format(locale, "ConversationRowSingleImagePreviewCalculator/getSourceRect bitmap=%f,%f(%f) face=%d,%d preview=%d,%d(%f) scaled=%f,%f(%f) rect=%s", objArr);
        return A0K;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b3, code lost:
        if (r6 == null) goto L_0x00b5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.util.Pair A07(int r10, int r11) {
        /*
        // Method dump skipped, instructions count: 314
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC65123If.A07(int, int):android.util.Pair");
    }

    public final Pair A08(int i, int i2, int i3) {
        C16150oX r1 = this.A00;
        AnonymousClass009.A06(r1, "setMediaData() must be called prior.");
        float f = (float) r1.A08;
        float f2 = (float) r1.A06;
        float A01 = A01(i, (float) i3);
        return A02(A01, A01(i2, (f2 * A01) / f));
    }

    public void A09(int i, int i2) {
        C16150oX r0 = new C16150oX();
        r0.A08 = i;
        r0.A06 = i2;
        this.A00 = r0;
    }

    public boolean A0A() {
        boolean z = this instanceof C61102zP;
        C16150oX r3 = this.A00;
        AnonymousClass009.A0F(C12960it.A1W(r3));
        int i = r3.A06;
        int i2 = r3.A08;
        if (!z) {
            if (i > i2) {
                return true;
            }
            return false;
        } else if (i >= i2) {
            return true;
        } else {
            return false;
        }
    }
}
