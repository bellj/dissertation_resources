package X;

/* renamed from: X.0cC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09000cC implements Runnable {
    public final /* synthetic */ AnonymousClass0AB A00;

    public RunnableC09000cC(AnonymousClass0AB r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0AB r1 = this.A00;
        r1.A01(true);
        r1.invalidateSelf();
    }
}
