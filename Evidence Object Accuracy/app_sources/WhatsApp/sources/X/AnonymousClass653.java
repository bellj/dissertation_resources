package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.653  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass653 implements ViewTreeObserver.OnGlobalLayoutListener {
    public View A00;
    public C14260l7 A01;
    public AnonymousClass28D A02;
    public AbstractC14200l1 A03;
    public AbstractC14200l1 A04;
    public String A05;
    public boolean A06;

    public final void A00() {
        if (this.A01 != null) {
            this.A06 = false;
            AbstractC14200l1 r3 = this.A04;
            if (r3 != null) {
                AnonymousClass28D r2 = this.A02;
                C14210l2 r1 = new C14210l2();
                r1.A05(this.A05, 0);
                C28701Oq.A01(this.A01, r2, r1.A03(), r3);
            }
        }
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        int visibility;
        int visibility2;
        View view = this.A00;
        if (view != null && this.A01 != null) {
            if (this.A06 && ((visibility2 = view.getVisibility()) == 4 || visibility2 == 8)) {
                A00();
            } else if (!this.A06 && (visibility = this.A00.getVisibility()) != 4 && visibility != 8 && this.A01 != null) {
                this.A06 = true;
                AbstractC14200l1 r4 = this.A03;
                if (r4 != null) {
                    AnonymousClass28D r3 = this.A02;
                    C14210l2 r2 = new C14210l2();
                    r2.A05(this.A05, 0);
                    C28701Oq.A01(this.A01, r3, r2.A03(), r4);
                }
            }
        }
    }
}
