package X;

import android.content.Context;
import com.whatsapp.calling.VoipPermissionsActivity;

/* renamed from: X.3OY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OY implements AbstractC009204q {
    public final /* synthetic */ VoipPermissionsActivity A00;

    public AnonymousClass3OY(VoipPermissionsActivity voipPermissionsActivity) {
        this.A00 = voipPermissionsActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        VoipPermissionsActivity voipPermissionsActivity = this.A00;
        if (!voipPermissionsActivity.A0D) {
            voipPermissionsActivity.A0D = true;
            AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) voipPermissionsActivity.generatedComponent())).A1E;
            voipPermissionsActivity.A02 = C12970iu.A0R(r1);
            voipPermissionsActivity.A01 = C12970iu.A0Q(r1);
            voipPermissionsActivity.A07 = C12970iu.A0b(r1);
            voipPermissionsActivity.A0A = (AnonymousClass19Z) r1.A2o.get();
            voipPermissionsActivity.A03 = C12960it.A0O(r1);
            voipPermissionsActivity.A05 = (C18750sx) r1.A2p.get();
            voipPermissionsActivity.A04 = C12970iu.A0Y(r1);
            voipPermissionsActivity.A06 = C12960it.A0S(r1);
        }
    }
}
