package X;

import android.graphics.Paint;

/* renamed from: X.3fX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73013fX extends Paint {
    public final float A00;
    public final float A01;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
        if (r2 >= 27) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C73013fX(int r4, android.graphics.Paint r5) {
        /*
            r3 = this;
            r3.<init>(r5)
            float r0 = X.C95194dI.A00(r5)
            r3.A00 = r0
            float r0 = X.C95194dI.A01(r5)
            r3.A01 = r0
            r3.setColor(r4)
            r3.setStrokeWidth(r0)
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r2 <= r0) goto L_0x0020
            r1 = 27
            r0 = 0
            if (r2 < r1) goto L_0x0021
        L_0x0020:
            r0 = 1
        L_0x0021:
            r3.setAntiAlias(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C73013fX.<init>(int, android.graphics.Paint):void");
    }

    @Override // android.graphics.Paint
    public float getUnderlinePosition() {
        return this.A00;
    }

    @Override // android.graphics.Paint
    public float getUnderlineThickness() {
        return this.A01;
    }
}
