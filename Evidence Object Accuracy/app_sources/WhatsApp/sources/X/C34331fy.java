package X;

/* renamed from: X.1fy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34331fy extends Exception {
    public final int errorCode;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C34331fy(int r3) {
        /*
            r2 = this;
            java.lang.String r1 = "SyncD critical event with error code: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.errorCode = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34331fy.<init>(int):void");
    }
}
