package X;

import android.content.SharedPreferences;
import android.os.SystemClock;
import com.whatsapp.util.Log;

/* renamed from: X.0m7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14830m7 {
    public final C14820m6 A00;
    public volatile long A01;
    public volatile long A02;
    public volatile long A03;
    public volatile long A04;

    public C14830m7(C14820m6 r10) {
        this.A00 = r10;
        SharedPreferences sharedPreferences = r10.A00;
        this.A02 = sharedPreferences.getLong("client_server_time_diff", 0);
        long currentTimeMillis = System.currentTimeMillis();
        long j = sharedPreferences.getLong("last_ntp_client_time", 0);
        if (sharedPreferences.contains("client_ntp_time_diff") && j > 0 && Math.abs(currentTimeMillis - j) < 86400000) {
            A03(sharedPreferences.getLong("client_ntp_time_diff", 0));
        }
    }

    public long A00() {
        long j;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.A01 != 0) {
            j = this.A01;
        } else if (this.A03 == 0) {
            return System.currentTimeMillis() - this.A02;
        } else {
            j = this.A03;
        }
        return j + elapsedRealtime;
    }

    public long A01() {
        if (this.A03 != 0) {
            return this.A03 + SystemClock.elapsedRealtime();
        }
        return System.currentTimeMillis() - this.A02;
    }

    public long A02(long j) {
        long currentTimeMillis;
        long j2;
        long currentTimeMillis2 = j + System.currentTimeMillis();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.A01 != 0) {
            j2 = this.A01;
        } else if (this.A03 != 0) {
            j2 = this.A03;
        } else {
            currentTimeMillis = System.currentTimeMillis() - this.A02;
            return currentTimeMillis2 - currentTimeMillis;
        }
        currentTimeMillis = j2 + elapsedRealtime;
        return currentTimeMillis2 - currentTimeMillis;
    }

    public final void A03(long j) {
        System.currentTimeMillis();
        A00();
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = currentTimeMillis + j;
        this.A01 = j2 - SystemClock.elapsedRealtime();
        StringBuilder sb = new StringBuilder("app/time ntp update processed; diffClientNtp:");
        sb.append(j);
        sb.append(" device time: ");
        sb.append(currentTimeMillis);
        sb.append(" ntp time: ");
        sb.append(j2);
        Log.i(sb.toString());
        System.currentTimeMillis();
        A00();
    }

    public void A04(long j, long j2) {
        System.currentTimeMillis();
        A00();
        if (j > 0) {
            this.A03 = j - SystemClock.elapsedRealtime();
            this.A02 = j2 - j;
            StringBuilder sb = new StringBuilder("app/time server update processed; diffClientWaServer:");
            sb.append(this.A02);
            sb.append(" device time: ");
            sb.append(j2);
            sb.append(" server time: ");
            sb.append(j);
            Log.i(sb.toString());
            C14820m6 r0 = this.A00;
            r0.A00.edit().putLong("client_server_time_diff", this.A02).apply();
        }
        System.currentTimeMillis();
        A00();
    }
}
