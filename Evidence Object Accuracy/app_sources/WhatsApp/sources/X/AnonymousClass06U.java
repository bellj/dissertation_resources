package X;

import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.06U  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass06U extends AnonymousClass015 {
    public static final AbstractC009404s A06 = new AnonymousClass06V();
    public boolean A00 = false;
    public boolean A01 = false;
    public final HashMap A02 = new HashMap();
    public final HashMap A03 = new HashMap();
    public final HashMap A04 = new HashMap();
    public final boolean A05;

    public AnonymousClass06U(boolean z) {
        this.A05 = z;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        if (AnonymousClass01F.A01(3)) {
            StringBuilder sb = new StringBuilder("onCleared called for ");
            sb.append(this);
            Log.d("FragmentManager", sb.toString());
        }
        this.A00 = true;
    }

    public void A04(AnonymousClass01E r5) {
        String obj;
        if (this.A01) {
            if (AnonymousClass01F.A01(2)) {
                obj = "Ignoring removeRetainedFragment as the state is already saved";
            } else {
                return;
            }
        } else if (this.A03.remove(r5.A0T) != null && AnonymousClass01F.A01(2)) {
            StringBuilder sb = new StringBuilder("Updating retained Fragments: Removed ");
            sb.append(r5);
            obj = sb.toString();
        } else {
            return;
        }
        Log.v("FragmentManager", obj);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass06U.class != obj.getClass()) {
                return false;
            }
            AnonymousClass06U r5 = (AnonymousClass06U) obj;
            if (!this.A03.equals(r5.A03) || !this.A02.equals(r5.A02) || !this.A04.equals(r5.A04)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (((this.A03.hashCode() * 31) + this.A02.hashCode()) * 31) + this.A04.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator it = this.A03.values().iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator it2 = this.A02.keySet().iterator();
        while (it2.hasNext()) {
            sb.append((String) it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator it3 = this.A04.keySet().iterator();
        while (it3.hasNext()) {
            sb.append((String) it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }
}
