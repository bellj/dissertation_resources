package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.emoji.EmojiDescriptor;
import com.whatsapp.util.ViewOnClickCListenerShape0S0301000_I1;
import com.whatsapp.util.ViewOnClickCListenerShape3S0300000_I1;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.2da  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53262da extends PopupWindow {
    public int A00 = -1;
    public int A01 = -1;
    public final Context A02;
    public final Resources A03;
    public final Drawable A04;
    public final Drawable A05;
    public final Drawable A06;
    public final LinearLayout A07;
    public final AnonymousClass19M A08;
    public final AnonymousClass3HQ A09;
    public final AnonymousClass5UB A0A;
    public final int[] A0B;

    public C53262da(View view, AnonymousClass19M r22, AnonymousClass5UB r23, int[] iArr) {
        super(new LinearLayout(view.getContext()), -2, -2);
        Drawable A04;
        Drawable A042;
        int i;
        boolean z;
        Drawable[] drawableArr;
        BitmapDrawable bitmapDrawable;
        Drawable[] drawableArr2;
        BitmapDrawable bitmapDrawable2;
        int i2;
        int i3;
        LinearLayout linearLayout = (LinearLayout) getContentView();
        this.A07 = linearLayout;
        Context context = view.getContext();
        this.A02 = context;
        this.A08 = r22;
        this.A03 = context.getResources();
        int[] iArr2 = (int[]) iArr.clone();
        AnonymousClass3HQ r8 = new AnonymousClass3HQ(AnonymousClass3JU.A05(iArr));
        int i4 = 2;
        char c = 1;
        if (r8.A01().size() == 2) {
            List list = r8.A01;
            if (0 < list.size()) {
                i2 = C12960it.A05(((Pair) list.get(0)).second);
            } else {
                i2 = -1;
            }
            this.A00 = i2;
            if (1 < list.size()) {
                i3 = C12960it.A05(((Pair) list.get(1)).second);
            } else {
                i3 = -1;
            }
            this.A01 = i3;
        }
        int[] A07 = AnonymousClass3JU.A07(iArr2);
        this.A0B = A07;
        this.A09 = new AnonymousClass3HQ(AnonymousClass3JU.A05(A07));
        this.A0A = r23;
        LayoutInflater A01 = AnonymousClass01d.A01(context);
        AnonymousClass009.A05(A01);
        A01.inflate(R.layout.multi_skin_tone_popup, (ViewGroup) linearLayout, true);
        linearLayout.setOrientation(1);
        ViewGroup A0P = C12980iv.A0P(linearLayout, R.id.skin_tone_selector);
        AnonymousClass009.A03(A0P);
        int[] iArr3 = AnonymousClass3JU.A05;
        Drawable A02 = A02(iArr3[0]);
        if (A02 == null) {
            A04 = null;
        } else {
            A04 = AnonymousClass2GE.A04(A02, C12960it.A09(this.A07).getColor(R.color.emojiMultiSkinToneSilhouette));
        }
        this.A05 = A04;
        Drawable A03 = A03(iArr3[0]);
        if (A03 == null) {
            A042 = null;
        } else {
            A042 = AnonymousClass2GE.A04(A03, C12960it.A09(this.A07).getColor(R.color.emojiMultiSkinToneSilhouette));
        }
        this.A06 = A042;
        AnonymousClass009.A05(A04);
        AnonymousClass009.A05(A042);
        C39511q1 r12 = new C39511q1(AnonymousClass4Db.A00((Collection) this.A09.A00.get(0)));
        this.A04 = this.A08.A05(this.A03, r12, EmojiDescriptor.A00(r12, false));
        int length = iArr3.length;
        ArrayList A0w = C12980iv.A0w(length);
        int i5 = 0;
        while (true) {
            i = 3;
            if (i5 >= length) {
                break;
            }
            int i6 = iArr3[i5];
            WaImageButton waImageButton = (WaImageButton) A01.inflate(R.layout.multi_skin_tone_popup_selection_item, A0P, false);
            Drawable A022 = A02(i6);
            AnonymousClass009.A05(A022);
            Drawable drawable = this.A04;
            if (drawable == null) {
                drawableArr2 = new Drawable[2];
                drawableArr2[0] = A022;
                drawableArr2[c] = this.A06;
            } else {
                drawableArr2 = new Drawable[3];
                drawableArr2[0] = A022;
                drawableArr2[c] = this.A06;
                drawableArr2[2] = drawable;
            }
            String A012 = A01(this.A02, this.A0B, i6, -1);
            Resources resources = this.A03;
            Bitmap A00 = C39551q5.A00(drawableArr2);
            if (A00 != null) {
                bitmapDrawable2 = new BitmapDrawable(resources, A00);
            } else {
                bitmapDrawable2 = null;
            }
            waImageButton.setImageDrawable(bitmapDrawable2);
            waImageButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
            waImageButton.setContentDescription(A012);
            waImageButton.setSelected(C12960it.A1V(i6, this.A00));
            waImageButton.setOnClickListener(new ViewOnClickCListenerShape0S0301000_I1(this, waImageButton, A0w, i6, 0));
            A0P.addView(waImageButton);
            A0w.add(waImageButton);
            i5++;
            c = 1;
        }
        ArrayList A0w2 = C12980iv.A0w(length);
        int i7 = 0;
        while (i7 < length) {
            int i8 = iArr3[i7];
            WaImageButton waImageButton2 = (WaImageButton) A01.inflate(R.layout.multi_skin_tone_popup_selection_item, A0P, false);
            Drawable A032 = A03(i8);
            AnonymousClass009.A05(A032);
            Drawable drawable2 = this.A04;
            if (drawable2 == null) {
                drawableArr = new Drawable[i4];
                drawableArr[0] = this.A05;
                z = true;
                drawableArr[1] = A032;
            } else {
                z = true;
                drawableArr = new Drawable[i];
                drawableArr[0] = this.A05;
                drawableArr[1] = A032;
                drawableArr[2] = drawable2;
            }
            String A013 = A01(this.A02, this.A0B, -1, i8);
            Resources resources2 = this.A03;
            Bitmap A002 = C39551q5.A00(drawableArr);
            if (A002 != null) {
                bitmapDrawable = new BitmapDrawable(resources2, A002);
            } else {
                bitmapDrawable = null;
            }
            waImageButton2.setImageDrawable(bitmapDrawable);
            waImageButton2.setScaleType(ImageView.ScaleType.FIT_CENTER);
            waImageButton2.setContentDescription(A013);
            if (i8 != this.A01) {
                z = false;
            }
            waImageButton2.setSelected(z);
            waImageButton2.setOnClickListener(new ViewOnClickCListenerShape0S0301000_I1(this, waImageButton2, A0w2, i8, 1));
            A0P.addView(waImageButton2);
            A0w2.add(waImageButton2);
            i7++;
            i = 3;
            i4 = 2;
        }
        ImageView A0L = C12970iu.A0L(this.A07, R.id.default_emoji);
        AnonymousClass009.A03(A0L);
        A0L.setImageDrawable(r22.A04(view.getResources(), new C39511q1(this.A0B), 1.0f, -1));
        A0L.setContentDescription(C37471mS.A00(this.A0B));
        AbstractView$OnClickListenerC34281fs.A00(A0L, this, 0);
        A04();
        this.A07.setFocusableInTouchMode(true);
        this.A07.setFocusable(true);
        C12970iu.A1F(this.A07);
        setTouchable(true);
        setFocusable(true);
        setOutsideTouchable(true);
        setInputMethodMode(i4);
        setBackgroundDrawable(AnonymousClass2GE.A01(view.getContext(), R.drawable.panel, R.color.skinTonePopupBackground));
        this.A07.requestFocus();
        view.getParent().requestDisallowInterceptTouchEvent(true);
    }

    public static int A00(int i) {
        switch (i) {
            case 127995:
                return R.string.emoji_skin_tone_light;
            case 127996:
                return R.string.emoji_skin_tone_medium_light;
            case 127997:
                return R.string.emoji_skin_tone_medium;
            case 127998:
                return R.string.emoji_skin_tone_medium_dark;
            case 127999:
                return R.string.emoji_skin_tone_dark;
            default:
                throw C12970iu.A0f(C12960it.A0W(i, "Invalid skin tone: "));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0006, code lost:
        if (r9 == -1) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A01(android.content.Context r6, int[] r7, int r8, int r9) {
        /*
            r5 = 0
            r4 = 1
            r1 = -1
            if (r8 == r1) goto L_0x0008
            r0 = 0
            if (r9 != r1) goto L_0x0009
        L_0x0008:
            r0 = 1
        L_0x0009:
            X.AnonymousClass009.A0F(r0)
            java.lang.String r3 = X.C37471mS.A00(r7)
            r0 = 2
            if (r8 == r1) goto L_0x0027
            r2 = 2131887901(0x7f12071d, float:1.9410422E38)
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r1[r5] = r3
            int r0 = A00(r8)
        L_0x001e:
            java.lang.String r0 = r6.getString(r0)
            java.lang.String r0 = X.C12960it.A0X(r6, r0, r1, r4, r2)
            return r0
        L_0x0027:
            if (r9 == r1) goto L_0x0035
            r2 = 2131887902(0x7f12071e, float:1.9410424E38)
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r1[r5] = r3
            int r0 = A00(r9)
            goto L_0x001e
        L_0x0035:
            r0 = 2131887903(0x7f12071f, float:1.9410426E38)
            java.lang.String r0 = r6.getString(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53262da.A01(android.content.Context, int[], int, int):java.lang.String");
    }

    public final Drawable A02(int i) {
        C39511q1 r4 = new C39511q1(this.A09.A00(1, i).A02());
        return this.A08.A05(this.A03, r4, EmojiDescriptor.A00(r4, false));
    }

    public final Drawable A03(int i) {
        C39511q1 r4 = new C39511q1(this.A09.A00(2, i).A02());
        return this.A08.A05(this.A03, r4, EmojiDescriptor.A00(r4, false));
    }

    public final void A04() {
        Drawable A02;
        Drawable A03;
        BitmapDrawable bitmapDrawable;
        String A01;
        int i;
        ImageView A0L = C12970iu.A0L(this.A07, R.id.selected_emoji);
        AnonymousClass009.A03(A0L);
        Drawable drawable = this.A04;
        int i2 = 2;
        if (drawable != null) {
            i2 = 3;
        }
        Drawable[] drawableArr = new Drawable[i2];
        int i3 = this.A00;
        if (i3 == -1) {
            A02 = this.A05;
        } else {
            A02 = A02(i3);
        }
        drawableArr[0] = A02;
        int i4 = this.A01;
        if (i4 == -1) {
            A03 = this.A06;
        } else {
            A03 = A03(i4);
        }
        drawableArr[1] = A03;
        if (drawable != null) {
            drawableArr[2] = drawable;
        }
        Resources resources = this.A03;
        Bitmap A00 = C39551q5.A00(drawableArr);
        if (A00 != null) {
            bitmapDrawable = new BitmapDrawable(resources, A00);
        } else {
            bitmapDrawable = null;
        }
        A0L.setImageDrawable(bitmapDrawable);
        A0L.setBackgroundResource(R.drawable.multi_skin_tone_emoji_selector);
        int i5 = this.A00;
        if (i5 == -1 || (i = this.A01) == -1) {
            A0L.setOnClickListener(null);
            A01 = A01(this.A02, this.A0B, this.A00, this.A01);
        } else {
            int[] A022 = this.A09.A00(1, i5).A00(2, i).A02();
            A0L.setOnClickListener(new ViewOnClickCListenerShape3S0300000_I1(this, A022, A0L, 0));
            A01 = C37471mS.A00(A022);
        }
        A0L.setContentDescription(A01);
    }
}
