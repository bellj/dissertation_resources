package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.22A  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass22A extends BroadcastReceiver {
    public final C21830y3 A00;
    public final C18230s7 A01;
    public final AnonymousClass01d A02;
    public final C16590pI A03;
    public final C21840y4 A04;
    public final C14910mF A05;
    public final C21810y1 A06;
    public final Object A07 = new Object();
    public volatile boolean A08 = false;

    public AnonymousClass22A(C21830y3 r2, C18230s7 r3, AnonymousClass01d r4, C16590pI r5, C21840y4 r6, C14910mF r7, C21810y1 r8) {
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
        this.A05 = r7;
        this.A04 = r6;
        this.A06 = r8;
        this.A00 = r2;
    }

    public void A00() {
        PendingIntent A01 = AnonymousClass1UY.A01(this.A03.A00, 0, new Intent("com.whatsapp.alarm.AVAILABLE_TIMEOUT").setPackage("com.whatsapp"), 536870912);
        if (A01 != null) {
            AlarmManager A04 = this.A02.A04();
            if (A04 != null) {
                A04.cancel(A01);
            } else {
                Log.w("AvailabilityTimeoutAlarmBroadcastReceiver/cancelAvailableTimeoutAlarm AlarmManager is null");
            }
            A01.cancel();
        }
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A08) {
            synchronized (this.A07) {
                if (!this.A08) {
                    AnonymousClass22D.A00(context);
                    this.A08 = true;
                }
            }
        }
        C14910mF r3 = this.A05;
        if (r3.A00 != 1) {
            C21840y4 r2 = this.A04;
            r2.A04.A00();
            StringBuilder sb = new StringBuilder("presencestatemanager/setUnavailable previous-state: ");
            C14910mF r1 = r2.A05;
            sb.append(r1);
            Log.i(sb.toString());
            r1.A00 = 3;
            C21810y1 r12 = this.A06;
            r12.A00 = false;
            r12.A01();
            this.A00.A00.clear();
        }
        StringBuilder sb2 = new StringBuilder("app/presenceavailable/timeout/foreground ");
        sb2.append(r3);
        Log.i(sb2.toString());
    }
}
