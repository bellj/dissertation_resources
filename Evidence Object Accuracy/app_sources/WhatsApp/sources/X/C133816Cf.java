package X;

import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.6Cf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133816Cf implements AnonymousClass61g {
    public final /* synthetic */ AnonymousClass1ZO A00;
    public final /* synthetic */ AbstractC28901Pl A01;
    public final /* synthetic */ AnonymousClass6CN A02;
    public final /* synthetic */ PaymentBottomSheet A03;

    public C133816Cf(AnonymousClass1ZO r1, AbstractC28901Pl r2, AnonymousClass6CN r3, PaymentBottomSheet paymentBottomSheet) {
        this.A02 = r3;
        this.A03 = paymentBottomSheet;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass61g
    public void A91() {
        this.A02.A05.A1B();
    }

    @Override // X.AnonymousClass61g
    public void ASe(AnonymousClass1KC r5) {
        AnonymousClass6CN r3 = this.A02;
        PaymentBottomSheet paymentBottomSheet = this.A03;
        r3.A00(this.A00, this.A01, r5, paymentBottomSheet);
    }

    @Override // X.AbstractC136476Mr
    public void AaH() {
        PaymentView paymentView = this.A02.A03.A0V;
        if (paymentView != null) {
            paymentView.A04();
        }
    }

    @Override // X.AbstractC136476Mr
    public void AaN() {
        this.A02.A03.AaN();
    }

    @Override // X.AbstractC136476Mr
    public void AaQ() {
        PaymentView paymentView = this.A02.A03.A0V;
        if (paymentView != null) {
            paymentView.A05();
        }
    }
}
