package X;

import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: X.1Ew  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26801Ew {
    public C39101pI A00;
    public C39101pI A01 = new C39101pI(this.A03, new C002601e(null, new AnonymousClass01N() { // from class: X.1pu
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            return C39421ps.this.A00.A8a("TranscodeQueue", new LinkedBlockingQueue(), 1, 1, 1, 0);
        }
    }));
    public final C39421ps A02;
    public final C26821Ey A03;

    public C26801Ew(C26821Ey r5, AbstractC14440lR r6) {
        this.A03 = r5;
        C39421ps r1 = new C39421ps(r6);
        this.A02 = r1;
        this.A00 = new C39101pI(r5, new C002601e(null, new AnonymousClass01N() { // from class: X.1pt
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return C39421ps.this.A00.A8a("ProcessImageQueue", new LinkedBlockingQueue(), 4, 4, 1, 0);
            }
        }));
    }

    public final C39101pI A00(C14370lK r2) {
        if (r2 == C14370lK.A0B || r2 == C14370lK.A0Z || r2 == C14370lK.A0G || r2 == C14370lK.A0R || r2 == C14370lK.A06 || r2 == C14370lK.A0L || r2 == C14370lK.A07) {
            return this.A00;
        }
        return this.A01;
    }

    public void A01(AbstractC39111pJ r3, C14370lK r4) {
        A00(r4).A01(r3.A02, r3);
    }
}
