package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107724xq implements AbstractC116805Wy {
    public final List A00;

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return 1;
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        return j < 0 ? 0 : -1;
    }

    public C107724xq(List list) {
        this.A00 = Collections.unmodifiableList(list);
    }

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        return j >= 0 ? this.A00 : Collections.emptyList();
    }

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        C95314dV.A03(C12960it.A1T(i));
        return 0;
    }
}
