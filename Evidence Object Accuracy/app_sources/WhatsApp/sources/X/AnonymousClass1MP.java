package X;

import com.whatsapp.jid.GroupJid;
import java.util.LinkedHashMap;

/* renamed from: X.1MP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MP {
    public long A00 = -1;
    public final int A01;
    public final int A02;
    public final GroupJid A03;
    public final AnonymousClass1YT A04;
    public final String A05;
    public final LinkedHashMap A06;
    public final boolean A07;

    public AnonymousClass1MP(AnonymousClass1YT r3, int i) {
        this.A05 = AnonymousClass1SF.A0A(r3.A0B.A02);
        this.A06 = new LinkedHashMap();
        this.A07 = r3.A0H;
        this.A03 = r3.A04;
        this.A01 = 0;
        this.A04 = r3;
        this.A02 = i;
    }

    public AnonymousClass1MP(GroupJid groupJid, String str, LinkedHashMap linkedHashMap, int i, boolean z) {
        this.A05 = str;
        this.A06 = linkedHashMap;
        this.A07 = z;
        this.A03 = groupJid;
        this.A01 = i;
        this.A04 = null;
        this.A02 = 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("callId=");
        sb.append(this.A05);
        sb.append(" isVideoCall=");
        sb.append(this.A07);
        sb.append(" groupJid=");
        sb.append(this.A03);
        sb.append(" jids=[ ");
        for (Object obj : this.A06.keySet()) {
            sb.append(obj);
            sb.append(" ");
        }
        sb.append("]");
        sb.append(" callLog=");
        sb.append(this.A04);
        sb.append(" entryPoint=");
        sb.append(this.A02);
        return sb.toString();
    }
}
