package X;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.5L6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5L6 extends AbstractC11150fp {
    public int A00;
    public Object[] A01;
    public final int A02 = 10;
    public final ReentrantLock A03;
    public final AnonymousClass4A7 A04;
    public volatile /* synthetic */ int size;

    public AnonymousClass5L6(AnonymousClass4A7 r5) {
        this.A04 = r5;
        this.A03 = new ReentrantLock();
        int min = Math.min(10, 8);
        Object[] objArr = new Object[min];
        Arrays.fill(objArr, 0, min, AnonymousClass4HY.A00);
        this.A01 = objArr;
        this.size = 0;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AnonymousClass5F5
    public Object A00(Object obj) {
        AnonymousClass4VA r0;
        AnonymousClass5LD A03;
        ReentrantLock reentrantLock = this.A03;
        reentrantLock.lock();
        try {
            int i = this.size;
            super.A00.A04();
            int i2 = this.A02;
            if (i < i2) {
                this.size = 1 + i;
            } else {
                switch (this.A04.ordinal()) {
                    case 0:
                        r0 = AnonymousClass4HY.A01;
                        reentrantLock.unlock();
                        return r0;
                    case 1:
                        break;
                    case 2:
                        r0 = AnonymousClass4HY.A02;
                        reentrantLock.unlock();
                        return r0;
                    default:
                        throw new C113285Gx();
                }
            }
            if (i == 0) {
                do {
                    A03 = A03();
                    if (A03 != null) {
                    }
                } while (A03.A0A() == null);
                this.size = i;
                reentrantLock.unlock();
                AnonymousClass5L7 r1 = (AnonymousClass5L7) A03;
                r1.A01.A00 = obj;
                C114215Kq r12 = r1.A00;
                r12.A07(((AnonymousClass5LK) r12).A00);
                return AnonymousClass4HY.A02;
            }
            if (i < i2) {
                Object[] objArr = this.A01;
                int length = objArr.length;
                if (i >= length) {
                    int min = Math.min(length << 1, i2);
                    Object[] objArr2 = new Object[min];
                    for (int i3 = 0; i3 < i; i3++) {
                        objArr2[i3] = objArr[(this.A00 + i3) % length];
                    }
                    Arrays.fill(objArr2, i, min, AnonymousClass4HY.A00);
                    this.A01 = objArr2;
                    objArr = objArr2;
                    this.A00 = 0;
                }
                objArr[(this.A00 + i) % objArr.length] = obj;
            } else {
                Object[] objArr3 = this.A01;
                int i4 = this.A00;
                int length2 = objArr3.length;
                objArr3[i4 % length2] = null;
                objArr3[(i + i4) % length2] = obj;
                this.A00 = (i4 + 1) % length2;
            }
            r0 = AnonymousClass4HY.A02;
            reentrantLock.unlock();
            return r0;
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    @Override // X.AnonymousClass5F5
    public String A01() {
        StringBuilder A0k = C12960it.A0k("(buffer:capacity=");
        A0k.append(this.A02);
        A0k.append(",size=");
        A0k.append(this.size);
        return C12970iu.A0u(A0k);
    }

    @Override // X.AbstractC11150fp
    public Object A04() {
        ReentrantLock reentrantLock = this.A03;
        reentrantLock.lock();
        try {
            int i = this.size;
            if (i == 0) {
                super.A00.A04();
                return AnonymousClass4HY.A03;
            }
            Object[] objArr = this.A01;
            int i2 = this.A00;
            Object obj = objArr[i2];
            objArr[i2] = null;
            this.size = i - 1;
            AnonymousClass4VA r4 = AnonymousClass4HY.A03;
            if (i == this.A02) {
                super.A00.A01();
            }
            if (r4 != r4) {
                this.size = i;
                Object[] objArr2 = this.A01;
                objArr2[(this.A00 + i) % objArr2.length] = r4;
            }
            this.A00 = (this.A00 + 1) % this.A01.length;
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // X.AbstractC11150fp
    public boolean A06(AnonymousClass5LD r3) {
        ReentrantLock reentrantLock = this.A03;
        reentrantLock.lock();
        try {
            return super.A06(r3);
        } finally {
            reentrantLock.unlock();
        }
    }
}
