package X;

import androidx.work.impl.foreground.SystemForegroundService;

/* renamed from: X.0dI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09650dI implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ SystemForegroundService A01;

    public RunnableC09650dI(SystemForegroundService systemForegroundService, int i) {
        this.A01 = systemForegroundService;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A01.A00.cancel(this.A00);
    }
}
