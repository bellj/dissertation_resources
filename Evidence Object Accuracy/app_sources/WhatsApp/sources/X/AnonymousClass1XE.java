package X;

/* renamed from: X.1XE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XE extends C28861Ph {
    public AnonymousClass1ZL A00;

    public AnonymousClass1XE(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 46, j);
    }

    public AnonymousClass1XE(AnonymousClass1IS r8, AnonymousClass1XE r9, long j) {
        super(r8, r9, j, true);
        this.A00 = r9.A00;
    }

    public AbstractC33751f1 A18() {
        AnonymousClass1ZL r1 = this.A00;
        if (r1 == null) {
            return null;
        }
        int i = r1.A04;
        if (i == 1) {
            return new AnonymousClass3ZZ();
        }
        if (i == 2) {
            return new AnonymousClass34L(r1);
        }
        StringBuilder sb = new StringBuilder("Interactive response message does not support customizations: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }
}
