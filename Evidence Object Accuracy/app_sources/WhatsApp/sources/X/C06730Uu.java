package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Uu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06730Uu implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AnonymousClass0OB A00;
    public final /* synthetic */ AnonymousClass0A7 A01;

    public C06730Uu(AnonymousClass0OB r1, AnonymousClass0A7 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float floatValue = ((Number) valueAnimator.getAnimatedValue()).floatValue();
        AnonymousClass0A7 r2 = this.A01;
        AnonymousClass0OB r1 = this.A00;
        r2.A01(r1, floatValue);
        r2.A02(r1, floatValue, false);
        r2.invalidateSelf();
    }
}
