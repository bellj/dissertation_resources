package X;

import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.1Y5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Y5 extends AnonymousClass1XB implements AbstractC30471Xn {
    public int A00;
    public C15580nU A01;
    public AnonymousClass1OT A02;
    public final Set A03 = new LinkedHashSet();

    public AnonymousClass1Y5(AnonymousClass1IS r2, AnonymousClass1OT r3, int i, long j) {
        super(r2, i, j);
        this.A02 = r3;
    }

    @Override // X.AbstractC30471Xn
    public AnonymousClass1OT AGs() {
        return this.A02;
    }
}
