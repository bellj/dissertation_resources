package X;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5ze  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130585ze {
    public final C1316663q A00;
    public final C128005vP A01;

    public C130585ze(C1316663q r1, C128005vP r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static C130585ze A00(AnonymousClass1V8 r10) {
        C1316663q r2;
        r10.A0H("tkyc_tier");
        AnonymousClass1V8 A0E = r10.A0E("step_up");
        AnonymousClass1V8 A0E2 = r10.A0E("identity_content");
        C128005vP r4 = null;
        if (A0E != null) {
            r2 = C125045qa.A00(A0E);
        } else {
            r2 = null;
        }
        if (A0E2 != null) {
            AnonymousClass619 A01 = AnonymousClass619.A01(A0E2, "title");
            AnonymousClass619 A012 = AnonymousClass619.A01(A0E2, "description");
            AnonymousClass619 A013 = AnonymousClass619.A01(A0E2, "subtitle");
            AnonymousClass619 A014 = AnonymousClass619.A01(A0E2, "disclaimer");
            String A0H = A0E2.A0F("action").A0H("action_text");
            List A0J = A0E2.A0J("steps");
            ArrayList A0l = C12960it.A0l();
            Iterator it = A0J.iterator();
            while (it.hasNext()) {
                A0l.add(AnonymousClass619.A00(C117305Zk.A0d(it)));
            }
            r4 = new C128005vP(A01, A012, A013, A014, A0H, A0l);
        }
        return new C130585ze(r2, r4);
    }
}
