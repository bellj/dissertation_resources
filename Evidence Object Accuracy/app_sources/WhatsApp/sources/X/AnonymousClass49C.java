package X;

import java.io.InputStream;

/* renamed from: X.49C  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49C extends InputStream {
    public InputStream A00;
    public boolean A01 = true;
    public final C92754Xh A02;

    public AnonymousClass49C(C92754Xh r2) {
        this.A02 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001f  */
    @Override // java.io.InputStream
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read() {
        /*
            r3 = this;
            java.io.InputStream r0 = r3.A00
            r2 = -1
            if (r0 != 0) goto L_0x0018
            boolean r0 = r3.A01
            if (r0 == 0) goto L_0x0028
            X.5Ze r1 = r3.A00()
            if (r1 == 0) goto L_0x0028
            r0 = 0
            r3.A01 = r0
        L_0x0012:
            java.io.InputStream r0 = r1.AEi()
            r3.A00 = r0
        L_0x0018:
            int r0 = r0.read()
            if (r0 < 0) goto L_0x001f
            return r0
        L_0x001f:
            X.5Ze r1 = r3.A00()
            if (r1 != 0) goto L_0x0012
            r0 = 0
            r3.A00 = r0
        L_0x0028:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass49C.read():int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0028 A[SYNTHETIC] */
    @Override // java.io.InputStream
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read(byte[] r6, int r7, int r8) {
        /*
            r5 = this;
            java.io.InputStream r0 = r5.A00
            r4 = 0
            r3 = -1
            if (r0 != 0) goto L_0x0018
            boolean r0 = r5.A01
            if (r0 == 0) goto L_0x0035
            X.5Ze r0 = r5.A00()
            if (r0 == 0) goto L_0x0035
            r5.A01 = r4
        L_0x0012:
            java.io.InputStream r0 = r0.AEi()
            r5.A00 = r0
        L_0x0018:
            java.io.InputStream r2 = r5.A00
            int r1 = r7 + r4
            int r0 = r8 - r4
            int r0 = r2.read(r6, r1, r0)
            if (r0 < 0) goto L_0x0028
            int r4 = r4 + r0
            if (r4 != r8) goto L_0x0018
            return r4
        L_0x0028:
            X.5Ze r0 = r5.A00()
            if (r0 != 0) goto L_0x0012
            r0 = 0
            r5.A00 = r0
            r0 = 1
            if (r4 < r0) goto L_0x0035
            return r4
        L_0x0035:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass49C.read(byte[], int, int):int");
    }

    public final AbstractC117265Ze A00() {
        AnonymousClass1TN A00 = this.A02.A00();
        if (A00 == null) {
            return null;
        }
        if (A00 instanceof AbstractC117265Ze) {
            return (AbstractC117265Ze) A00;
        }
        throw C12990iw.A0i(C12970iu.A0s(A00.getClass(), C12960it.A0k("unknown object encountered: ")));
    }
}
