package X;

import com.whatsapp.Conversation;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.viewmodels.StatusesViewModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.12H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12H extends AbstractC16220oe {
    public long A00;

    public AnonymousClass12H() {
        super(new C002601e(Collections.emptySet(), null));
    }

    public AnonymousClass12H(AnonymousClass01H r1) {
        super(r1);
    }

    public void A05(AbstractC14640lm r7) {
        for (AbstractC18860tB r4 : A01()) {
            this.A00++;
            if (r4 instanceof C35121hH) {
                C35121hH r42 = (C35121hH) r4;
                if (C15380n4.A0N(r7)) {
                    StatusesViewModel.A01(UserJid.of(r7), r42.A00);
                }
            } else if (r4 instanceof C35131hI) {
                C35131hI r43 = (C35131hI) r4;
                if (C15380n4.A0N(r7)) {
                    r43.A00.A1B();
                }
            } else if (r4 instanceof C35141hJ) {
                C35141hJ r44 = (C35141hJ) r4;
                if (r7 != null) {
                    Conversation conversation = r44.A00;
                    if (r7.equals(conversation.A2s)) {
                        conversation.A1g.getConversationCursorAdapter().A01++;
                        conversation.A20.A07(conversation.A2o(), conversation.A03);
                    }
                }
            }
        }
    }

    public void A06(AbstractC14640lm r8) {
        C30421Xi r0;
        AbstractC14640lm r02;
        for (AbstractC18860tB r2 : A01()) {
            this.A00++;
            if (r2 instanceof C35121hH) {
                C35121hH r22 = (C35121hH) r2;
                if (C15380n4.A0N(r8)) {
                    StatusesViewModel.A01(UserJid.of(r8), r22.A00);
                }
            } else if (r2 instanceof C35131hI) {
                C35131hI r23 = (C35131hI) r2;
                if (C15380n4.A0N(r8)) {
                    r23.A00.A1B();
                }
            } else if (r2 instanceof C35171hM) {
                C35231hU r24 = ((C35171hM) r2).A00;
                boolean z = false;
                if (r24.A0G.A02 == 0) {
                    z = true;
                }
                r24.A0A(z);
            } else if (r2 instanceof AnonymousClass1hN) {
                C16030oK r5 = ((AnonymousClass1hN) r2).A00;
                synchronized (r5.A0X) {
                    C35211hS r25 = (C35211hS) r5.A0B().get(r8);
                    if (r25 != null && ((C15650ng) r5.A0i.get()).A0x(r25.A02)) {
                        r5.A0P(r8);
                    }
                }
                synchronized (r5.A0W) {
                    Map A0A = r5.A0A();
                    if (A0A.containsKey(r8)) {
                        Map map = (Map) A0A.get(r8);
                        AnonymousClass009.A05(map);
                        Iterator it = new ArrayList(map.values()).iterator();
                        while (it.hasNext()) {
                            C35221hT r26 = (C35221hT) it.next();
                            if (((C15650ng) r5.A0i.get()).A0x(r26.A02)) {
                                r5.A0Q(r8, r26.A01);
                            }
                        }
                    }
                }
            } else if (r2 instanceof C35181hO) {
                MessageDetailsActivity messageDetailsActivity = ((C35181hO) r2).A00;
                if (r8.equals(messageDetailsActivity.A0P.A0z.A00)) {
                    if (messageDetailsActivity.A0I.A0K.A03(messageDetailsActivity.A0P.A0z) == null) {
                        messageDetailsActivity.finish();
                    }
                }
            } else if (r2 instanceof C27161Gg) {
                C35191hP r27 = ((C27161Gg) r2).A00.A00;
                if (!(r27 == null || (r0 = r27.A0O) == null || (r02 = r0.A0z.A00) == null || !r02.equals(r8))) {
                    r27.A0H(true, true);
                }
            } else if (r2 instanceof C22520zD) {
                ((C22520zD) r2).A08.A00.remove(r8);
            } else if (r2 instanceof C35141hJ) {
                Conversation conversation = ((C35141hJ) r2).A00;
                if (r8.equals(conversation.A2s)) {
                    C15360n1 r1 = conversation.A20;
                    r1.A03 = 0;
                    r1.A01 = 0;
                    r1.A02 = 0;
                    r1.A05();
                    conversation.A20.A07(conversation.A2o(), conversation.A03);
                }
            }
        }
    }

    public void A07(AbstractC15340mz r7, int i) {
        for (AbstractC18860tB r4 : A01()) {
            this.A00++;
            r4.A01(r7, i);
        }
    }

    public void A08(AbstractC15340mz r7, int i) {
        for (AbstractC18860tB r4 : A01()) {
            this.A00++;
            r4.A02(r7, i);
        }
    }

    public void A09(Collection collection, int i) {
        for (AbstractC18860tB r3 : A01()) {
            this.A00++;
            if (!(r3 instanceof C35151hK)) {
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    r3.A02((AbstractC15340mz) it.next(), i);
                }
            } else {
                C35151hK r32 = (C35151hK) r3;
                for (Object obj : collection) {
                    if (obj instanceof C30421Xi) {
                        r32.A00.A08.A0B(obj);
                    }
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 688
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:66)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public void A0A(java.util.Collection r15, java.util.Map r16, java.util.Map r17) {
        /*
        // Method dump skipped, instructions count: 1990
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass12H.A0A(java.util.Collection, java.util.Map, java.util.Map):void");
    }
}
