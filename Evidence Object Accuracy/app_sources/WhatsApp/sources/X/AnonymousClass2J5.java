package X;

/* renamed from: X.2J5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2J5 implements AnonymousClass2J6 {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2J5(C48302Fl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2J6
    public C59452uk A85(AnonymousClass2K4 r26, AnonymousClass2K3 r27, C48122Ek r28, AnonymousClass2K1 r29, AnonymousClass1B4 r30, C30211Wn r31, String str, String str2, String str3) {
        AnonymousClass01J r6 = this.A00.A03;
        C14900mE r8 = (C14900mE) r6.A8X.get();
        C16590pI r4 = (C16590pI) r6.AMg.get();
        AbstractC15710nm r7 = (AbstractC15710nm) r6.A4o.get();
        AnonymousClass018 r2 = (AnonymousClass018) r6.ANb.get();
        C16340oq r15 = (C16340oq) r6.A5z.get();
        C17170qN r1 = (C17170qN) r6.AMt.get();
        return new C59452uk(r7, r8, (C16430p0) r6.A5y.get(), r26, r27, r28, r29, r30, r15, r31, r4, r1, r2, (C14850m9) r6.A04.get(), (AbstractC14440lR) r6.ANe.get(), str3, str, str2);
    }
}
