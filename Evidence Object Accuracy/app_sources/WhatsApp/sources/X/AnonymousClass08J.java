package X;

import android.graphics.Typeface;
import android.os.Build;
import android.widget.TextView;
import java.lang.ref.WeakReference;

/* renamed from: X.08J  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08J extends AnonymousClass08K {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass086 A02;
    public final /* synthetic */ WeakReference A03;

    @Override // X.AnonymousClass08K
    public void A01(int i) {
    }

    public AnonymousClass08J(AnonymousClass086 r1, WeakReference weakReference, int i, int i2) {
        this.A02 = r1;
        this.A00 = i;
        this.A01 = i2;
        this.A03 = weakReference;
    }

    @Override // X.AnonymousClass08K
    public void A02(Typeface typeface) {
        int i;
        if (Build.VERSION.SDK_INT >= 28 && (i = this.A00) != -1) {
            boolean z = false;
            if ((this.A01 & 2) != 0) {
                z = true;
            }
            typeface = Typeface.create(typeface, i, z);
        }
        AnonymousClass086 r3 = this.A02;
        WeakReference weakReference = this.A03;
        if (r3.A0A) {
            r3.A02 = typeface;
            TextView textView = (TextView) weakReference.get();
            if (textView != null) {
                boolean A0q = AnonymousClass028.A0q(textView);
                int i2 = r3.A01;
                if (A0q) {
                    textView.post(new RunnableC10020dt(typeface, textView, r3, i2));
                } else {
                    textView.setTypeface(typeface, i2);
                }
            }
        }
    }
}
