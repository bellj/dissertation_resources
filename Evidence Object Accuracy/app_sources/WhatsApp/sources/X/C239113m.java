package X;

/* renamed from: X.13m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239113m {
    public final AbstractC15710nm A00;
    public final C20870wS A01;
    public final C18850tA A02;
    public final C21370xJ A03;
    public final C14830m7 A04;
    public final C22320yt A05;
    public final C16510p9 A06;
    public final C15650ng A07;
    public final C21380xK A08;
    public final C242914y A09;
    public final C238513g A0A;
    public final C22830zi A0B;
    public final C21400xM A0C;
    public final C14850m9 A0D;
    public final C22170ye A0E;
    public final C20740wF A0F;
    public final C17230qT A0G;
    public final C20220vP A0H;

    public C239113m(AbstractC15710nm r2, C20870wS r3, C18850tA r4, C21370xJ r5, C14830m7 r6, C22320yt r7, C16510p9 r8, C15650ng r9, C21380xK r10, C242914y r11, C238513g r12, C22830zi r13, C21400xM r14, C14850m9 r15, C22170ye r16, C20740wF r17, C17230qT r18, C20220vP r19) {
        this.A04 = r6;
        this.A0D = r15;
        this.A06 = r8;
        this.A00 = r2;
        this.A02 = r4;
        this.A03 = r5;
        this.A0E = r16;
        this.A08 = r10;
        this.A01 = r3;
        this.A07 = r9;
        this.A05 = r7;
        this.A0F = r17;
        this.A0G = r18;
        this.A0C = r14;
        this.A0H = r19;
        this.A0A = r12;
        this.A0B = r13;
        this.A09 = r11;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a1, code lost:
        if (r16 != 2) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a6, code lost:
        if (r16 != 3) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a8, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a9, code lost:
        if (r10 == false) goto L_0x00e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ab, code lost:
        if (r0 != false) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ad, code lost:
        r13 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ae, code lost:
        if (r2 != false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b0, code lost:
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b1, code lost:
        r2 = r15.A11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b7, code lost:
        if (r9.contains(r8) == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b9, code lost:
        r10 = r5.A07;
        r9 = r10.A01.A06(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c1, code lost:
        if (r9 == null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c7, code lost:
        if (r2 < 1) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00cd, code lost:
        if (r9.A0L >= r2) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00cf, code lost:
        r9.A0L = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d1, code lost:
        r0 = r9.A05 + r13;
        r9.A05 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d6, code lost:
        if (r0 >= 0) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d8, code lost:
        r9.A05 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00db, code lost:
        r10.A0A(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e0, code lost:
        if (r3 != false) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e2, code lost:
        if (r0 == false) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e4, code lost:
        r13 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r0.A02 != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.AnonymousClass1Iv r15, int r16) {
        /*
        // Method dump skipped, instructions count: 245
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C239113m.A00(X.1Iv, int):void");
    }
}
