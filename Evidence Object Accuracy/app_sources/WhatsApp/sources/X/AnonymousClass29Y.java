package X;

import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;

/* renamed from: X.29Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29Y implements AnonymousClass29Z {
    public final /* synthetic */ SettingsGoogleDriveViewModel A00;

    public AnonymousClass29Y(SettingsGoogleDriveViewModel settingsGoogleDriveViewModel) {
        this.A00 = settingsGoogleDriveViewModel;
    }

    @Override // X.AnonymousClass29Z
    public void AM6(int i) {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A00;
        settingsGoogleDriveViewModel.A0R.A03.A04(this);
        int i2 = 1;
        if (i != 1) {
            i2 = 2;
            if (i != 2) {
                if (i == 0) {
                    i2 = 3;
                } else {
                    return;
                }
            }
        }
        settingsGoogleDriveViewModel.A0d.A0A(new C84233yd(i2));
    }

    @Override // X.AnonymousClass29Z
    public void ANE() {
        this.A00.A0d.A0A(new C84233yd(0));
    }
}
