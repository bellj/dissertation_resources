package X;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;

/* renamed from: X.0QI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QI {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = 0;
    public AnonymousClass0FL A04;
    public AnonymousClass0Z4 A05;
    public Object[] A06;
    public Object[] A07;
    public final Class A08;

    public AnonymousClass0QI(AnonymousClass0Z4 r2, Class cls) {
        this.A08 = cls;
        this.A06 = (Object[]) Array.newInstance(cls, 10);
        this.A05 = r2;
    }

    public final int A00(Object obj, Object[] objArr, int i, int i2, int i3) {
        Object obj2;
        while (i < i2) {
            int i4 = (i + i2) >> 1;
            Object obj3 = objArr[i4];
            int compare = this.A05.compare(obj3, obj);
            if (compare < 0) {
                i = i4 + 1;
            } else if (compare == 0) {
                if (!this.A05.A02(obj3, obj)) {
                    int i5 = i4;
                    for (int i6 = i4 - 1; i6 >= i; i6--) {
                        Object obj4 = this.A06[i6];
                        if (this.A05.compare(obj4, obj) != 0) {
                            break;
                        } else if (this.A05.A02(obj4, obj)) {
                            i5 = i6;
                            break;
                        }
                    }
                    do {
                        i5++;
                        if (i5 < i2) {
                            obj2 = this.A06[i5];
                            if (this.A05.compare(obj2, obj) == 0) {
                            }
                        }
                        i5 = -1;
                        break;
                    } while (!this.A05.A02(obj2, obj));
                    if (i3 != 1) {
                        return i5;
                    }
                    if (i5 != -1) {
                        return i5;
                    }
                }
                return i4;
            } else {
                i2 = i4;
            }
        }
        if (i3 != 1) {
            return -1;
        }
        return i;
    }

    public final int A01(Object[] objArr) {
        int length = objArr.length;
        int i = 0;
        if (length != 0) {
            Arrays.sort(objArr, this.A05);
            i = 1;
            int i2 = 0;
            for (int i3 = 1; i3 < length; i3++) {
                Object obj = objArr[i3];
                if (this.A05.compare(objArr[i2], obj) == 0) {
                    int i4 = i2;
                    while (true) {
                        if (i4 >= i) {
                            break;
                        } else if (!this.A05.A02(objArr[i4], obj)) {
                            i4++;
                        } else if (i4 != -1) {
                            objArr[i4] = obj;
                        }
                    }
                    if (i != i3) {
                        objArr[i] = obj;
                    }
                    i++;
                } else {
                    if (i != i3) {
                        objArr[i] = obj;
                    }
                    i++;
                    i2 = i;
                }
            }
        }
        return i;
    }

    public Object A02(int i) {
        int i2;
        int i3 = this.A03;
        if (i >= i3 || i < 0) {
            StringBuilder sb = new StringBuilder("Asked to get item at ");
            sb.append(i);
            sb.append(" but size is ");
            sb.append(i3);
            throw new IndexOutOfBoundsException(sb.toString());
        }
        Object[] objArr = this.A07;
        if (objArr == null || i < (i2 = this.A00)) {
            objArr = this.A06;
        } else {
            i = (i - i2) + this.A02;
        }
        return objArr[i];
    }

    public void A03() {
        A04();
        AnonymousClass0Z4 r1 = this.A05;
        if (r1 instanceof AnonymousClass0FL) {
            ((AnonymousClass0FL) r1).A00.A00();
        }
        AnonymousClass0Z4 r12 = this.A05;
        AnonymousClass0FL r0 = this.A04;
        if (r12 == r0) {
            this.A05 = r0.A01;
        }
    }

    public final void A04() {
        if (this.A07 != null) {
            throw new IllegalStateException("Data cannot be mutated in the middle of a batch update operation such as addAll or replaceAll.");
        }
    }

    public void A05(Collection collection) {
        Class cls = this.A08;
        Object[] array = collection.toArray((Object[]) Array.newInstance(cls, collection.size()));
        A04();
        AnonymousClass0Z4 r1 = this.A05;
        boolean z = r1 instanceof AnonymousClass0FL;
        boolean z2 = !z;
        if (z2) {
            A04();
            if (!z) {
                AnonymousClass0FL r0 = this.A04;
                if (r0 == null) {
                    r0 = new AnonymousClass0FL(r1);
                    this.A04 = r0;
                }
                this.A05 = r0;
            }
        }
        this.A02 = 0;
        this.A01 = this.A03;
        this.A07 = this.A06;
        this.A00 = 0;
        int A01 = A01(array);
        this.A06 = (Object[]) Array.newInstance(cls, A01);
        while (true) {
            int i = this.A00;
            if (i >= A01 && this.A02 >= this.A01) {
                break;
            }
            int i2 = this.A02;
            int i3 = this.A01;
            if (i2 >= i3) {
                int i4 = A01 - i;
                System.arraycopy(array, i, this.A06, i, i4);
                this.A00 += i4;
                this.A03 += i4;
                this.A05.ARP(i, i4);
                break;
            } else if (i >= A01) {
                int i5 = i3 - i2;
                this.A03 -= i5;
                this.A05.AUs(i, i5);
                break;
            } else {
                Object obj = this.A07[i2];
                Object obj2 = array[i];
                int compare = this.A05.compare(obj, obj2);
                if (compare < 0) {
                    this.A03--;
                    this.A02++;
                    this.A05.AUs(this.A00, 1);
                } else {
                    if (compare <= 0) {
                        if (!this.A05.A02(obj, obj2)) {
                            this.A03--;
                            this.A02++;
                            this.A05.AUs(this.A00, 1);
                        } else {
                            Object[] objArr = this.A06;
                            int i6 = this.A00;
                            objArr[i6] = obj2;
                            this.A02++;
                            this.A00 = i6 + 1;
                            if (!this.A05.A01(obj, obj2)) {
                                AnonymousClass0Z4 r2 = this.A05;
                                r2.ANr(r2.A00(obj, obj2), this.A00 - 1, 1);
                            }
                        }
                    }
                    Object[] objArr2 = this.A06;
                    int i7 = this.A00;
                    objArr2[i7] = obj2;
                    int i8 = i7 + 1;
                    this.A00 = i8;
                    this.A03++;
                    this.A05.ARP(i8 - 1, 1);
                }
            }
        }
        this.A07 = null;
        if (z2) {
            A03();
        }
    }
}
