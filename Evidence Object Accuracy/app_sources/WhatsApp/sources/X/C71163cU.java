package X;

import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.TextUtils;

/* renamed from: X.3cU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71163cU implements Cloneable {
    public float A00 = 0.0f;
    public float A01 = 0.0f;
    public float A02 = 0.0f;
    public float A03 = 0.0f;
    public float A04 = 0.0f;
    public float A05 = 0.0f;
    public float A06 = 1.0f;
    public int A07 = -1;
    public int A08 = 0;
    public int A09 = 0;
    public int A0A = 0;
    public int A0B = 0;
    public int A0C = -16776961;
    public int A0D = Integer.MIN_VALUE;
    public int A0E = Integer.MIN_VALUE;
    public int A0F = -1;
    public int A0G = Integer.MAX_VALUE;
    public int A0H = Integer.MAX_VALUE;
    public int A0I = -1;
    public int A0J = Integer.MIN_VALUE;
    public int A0K = 0;
    public int A0L = -7829368;
    public int A0M = -16777216;
    public int A0N = -1;
    public int A0O = Typeface.DEFAULT.getStyle();
    public ColorStateList A0P;
    public Typeface A0Q;
    public TextUtils.TruncateAt A0R;
    public AnonymousClass02T A0S;
    public EnumC630039m A0T = EnumC630039m.TEXT_START;
    public AnonymousClass4A9 A0U = AnonymousClass4A9.TOP;
    public CharSequence A0V;
    public boolean A0W = true;
    public boolean A0X = false;
    public boolean A0Y;
    public boolean A0Z = false;

    public C71163cU A00() {
        try {
            return (C71163cU) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
