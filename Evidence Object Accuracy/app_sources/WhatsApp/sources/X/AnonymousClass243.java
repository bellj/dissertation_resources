package X;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.math.BigDecimal;

/* renamed from: X.243  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass243 extends AnonymousClass1OY {
    public RunnableBRunnable0Shape3S0200000_I0_3 A00;
    public C26191Cj A01;
    public boolean A02;
    public final TextEmojiLabel A03;
    public final TextEmojiLabel A04;
    public final WaTextView A05;
    public final WaTextView A06;
    public final ThumbnailButton A07 = ((ThumbnailButton) findViewById(R.id.thumb));

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return 0;
    }

    public AnonymousClass243(Context context, AbstractC13890kV r6, AnonymousClass1XF r7) {
        super(context, r6, r7);
        A0Z();
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.message_text);
        this.A03 = textEmojiLabel;
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setFocusable(false);
        textEmojiLabel.setClickable(false);
        textEmojiLabel.setLongClickable(false);
        TextEmojiLabel textEmojiLabel2 = (TextEmojiLabel) findViewById(R.id.order_message_btn);
        this.A04 = textEmojiLabel2;
        this.A06 = (WaTextView) findViewById(R.id.order_title);
        this.A05 = (WaTextView) findViewById(R.id.order_subtitle);
        Activity A00 = AbstractC35731ia.A00(context);
        if (A00 instanceof AbstractC001200n) {
            RunnableBRunnable0Shape3S0200000_I0_3 runnableBRunnable0Shape3S0200000_I0_3 = new RunnableBRunnable0Shape3S0200000_I0_3();
            this.A00 = runnableBRunnable0Shape3S0200000_I0_3;
            ((AnonymousClass017) runnableBRunnable0Shape3S0200000_I0_3.A00).A05((AbstractC001200n) A00, new AnonymousClass02B() { // from class: X.4t2
                @Override // X.AnonymousClass02B
                public final void ANq(Object obj) {
                    int i;
                    Bitmap bitmap = (Bitmap) obj;
                    ThumbnailButton thumbnailButton = AnonymousClass243.this.A07;
                    if (bitmap != null) {
                        thumbnailButton.setImageBitmap(bitmap);
                        i = 0;
                    } else {
                        thumbnailButton.setImageDrawable(null);
                        i = 8;
                    }
                    thumbnailButton.setVisibility(i);
                }
            });
        }
        ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I0 = new ViewOnClickCListenerShape4S0200000_I0(context, 18, this);
        textEmojiLabel2.setOnClickListener(viewOnClickCListenerShape4S0200000_I0);
        findViewById(R.id.order_message_preview).setOnClickListener(viewOnClickCListenerShape4S0200000_I0);
        A1M();
    }

    public static String A0X(Context context, AnonymousClass018 r8, AnonymousClass1XF r9) {
        BigDecimal bigDecimal;
        String str = r9.A04;
        if (str == null || (bigDecimal = r9.A09) == null) {
            return null;
        }
        return context.getString(R.string.cart_estimated_total, new C30711Yn(str).A03(r8, bigDecimal, true));
    }

    public static String A0Y(AnonymousClass018 r6, AnonymousClass1XF r7) {
        int i = r7.A00;
        return r6.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.total_items, (long) i);
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2P6 r2 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r1 = r2.A06;
            ((AbstractC28551Oa) this).A0L = (C14850m9) r1.A04.get();
            ((AbstractC28551Oa) this).A0P = (AnonymousClass1CY) r1.ABO.get();
            ((AbstractC28551Oa) this).A0F = (AbstractC15710nm) r1.A4o.get();
            ((AbstractC28551Oa) this).A0N = (C244415n) r1.AAg.get();
            ((AbstractC28551Oa) this).A0J = (AnonymousClass01d) r1.ALI.get();
            ((AbstractC28551Oa) this).A0K = (AnonymousClass018) r1.ANb.get();
            ((AbstractC28551Oa) this).A0M = (C22050yP) r1.A7v.get();
            ((AbstractC28551Oa) this).A0G = (AnonymousClass19I) r1.A4a.get();
            this.A0k = (C14830m7) r1.ALb.get();
            ((AnonymousClass1OY) this).A0J = (C14900mE) r1.A8X.get();
            this.A13 = (AnonymousClass13H) r1.ABY.get();
            this.A1P = (AbstractC14440lR) r1.ANe.get();
            ((AnonymousClass1OY) this).A0L = (C15570nT) r1.AAr.get();
            this.A0h = (AnonymousClass19P) r1.ACQ.get();
            ((AnonymousClass1OY) this).A0M = (C239613r) r1.AI9.get();
            ((AnonymousClass1OY) this).A0O = (C18790t3) r1.AJw.get();
            this.A0n = (C19990v2) r1.A3M.get();
            this.A10 = (AnonymousClass19M) r1.A6R.get();
            ((AnonymousClass1OY) this).A0N = (C15450nH) r1.AII.get();
            this.A0v = (C21250x7) r1.AJh.get();
            this.A0w = (C18470sV) r1.AK8.get();
            ((AnonymousClass1OY) this).A0R = (C16170oZ) r1.AM4.get();
            this.A1Q = (AnonymousClass19Z) r1.A2o.get();
            ((AnonymousClass1OY) this).A0K = (AnonymousClass18U) r1.AAU.get();
            this.A12 = (C14410lO) r1.AB3.get();
            ((AnonymousClass1OY) this).A0I = (AnonymousClass12P) r1.A0H.get();
            ((AnonymousClass1OY) this).A0a = (C21270x9) r1.A4A.get();
            this.A0s = (C20040v7) r1.AAK.get();
            this.A15 = (C17220qS) r1.ABt.get();
            ((AnonymousClass1OY) this).A0X = (C15550nR) r1.A45.get();
            ((AnonymousClass1OY) this).A0U = (C253619c) r1.AId.get();
            ((AnonymousClass1OY) this).A0Z = (C15610nY) r1.AMe.get();
            this.A1M = (C252018m) r1.A7g.get();
            this.A1A = (C17070qD) r1.AFC.get();
            this.A0t = (AnonymousClass1BK) r1.AFZ.get();
            ((AnonymousClass1OY) this).A0b = (C253318z) r1.A4B.get();
            this.A0p = (C15650ng) r1.A4m.get();
            ((AnonymousClass1OY) this).A0V = (C238013b) r1.A1Z.get();
            this.A11 = (C20710wC) r1.A8m.get();
            this.A14 = (C22910zq) r1.A9O.get();
            this.A1J = (AnonymousClass12F) r1.AJM.get();
            this.A1F = r1.A41();
            this.A1E = (AnonymousClass12V) r1.A0p.get();
            this.A1I = (C240514a) r1.AJL.get();
            this.A1O = (AnonymousClass19O) r1.ACO.get();
            this.A17 = (C26151Cf) r1.ADi.get();
            this.A1H = (C26701Em) r1.ABc.get();
            this.A0x = (AnonymousClass132) r1.ALx.get();
            ((AnonymousClass1OY) this).A0S = (C19850um) r1.A2v.get();
            this.A0y = (C21400xM) r1.ABd.get();
            this.A0z = (C15670ni) r1.AIb.get();
            this.A1N = (C23000zz) r1.ALg.get();
            ((AnonymousClass1OY) this).A0Y = (C22700zV) r1.AMN.get();
            this.A0m = (C14820m6) r1.AN3.get();
            ((AnonymousClass1OY) this).A0W = (C22640zP) r1.A3Z.get();
            this.A19 = (C22710zW) r1.AF7.get();
            ((AnonymousClass1OY) this).A0T = (AnonymousClass19Q) r1.A2u.get();
            this.A1K = (AnonymousClass1AB) r1.AKI.get();
            this.A18 = (AnonymousClass18T) r1.AE9.get();
            this.A0r = (C15600nX) r1.A8x.get();
            this.A0u = (C22440z5) r1.AG6.get();
            this.A1D = (C16630pM) r1.AIc.get();
            this.A0j = (C18640sm) r1.A3u.get();
            this.A1L = (C26671Ej) r1.AKR.get();
            this.A1G = r1.A42();
            this.A0o = (C20830wO) r1.A4W.get();
            this.A0q = (C242814x) r1.A71.get();
            this.A0d = (AnonymousClass19K) r1.AFh.get();
            this.A16 = (AnonymousClass19J) r1.ACA.get();
            this.A1R = (C237512w) r1.AAD.get();
            this.A0c = (AnonymousClass1AO) r1.AFg.get();
            this.A0l = (C17170qN) r1.AMt.get();
            this.A0i = (C17000q6) r1.ACv.get();
            this.A1B = (C21190x1) r1.A3G.get();
            this.A0f = r2.A02();
            this.A01 = (C26191Cj) r1.ADs.get();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != ((AbstractC28551Oa) this).A0O) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A1M();
        }
    }

    public final void A1M() {
        int i;
        AnonymousClass1XF r4 = (AnonymousClass1XF) ((AbstractC28551Oa) this).A0O;
        setThumbnail(r4);
        this.A06.setText(A0Y(((AbstractC28551Oa) this).A0K, r4), TextView.BufferType.SPANNABLE);
        CharSequence A0X = A0X(getContext(), ((AbstractC28551Oa) this).A0K, r4);
        boolean isEmpty = TextUtils.isEmpty(A0X);
        WaTextView waTextView = this.A05;
        if (isEmpty) {
            i = 8;
        } else {
            waTextView.setText(A0q(A0X));
            i = 0;
        }
        waTextView.setVisibility(i);
        TextEmojiLabel textEmojiLabel = this.A04;
        boolean z = r4.A0z.A02;
        Context context = getContext();
        int i2 = R.string.message_order_cta_business;
        if (z) {
            i2 = R.string.message_order_cta_consumer;
        }
        textEmojiLabel.setText(context.getString(i2));
        String str = r4.A05;
        if (str != null) {
            setMessageText(str, this.A03, r4);
        }
    }

    @Override // X.AbstractC28551Oa
    public AnonymousClass1XF getFMessage() {
        return (AnonymousClass1XF) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_order_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_order_right;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1XF);
        ((AbstractC28551Oa) this).A0O = r2;
    }

    private void setThumbnail(AnonymousClass1XF r3) {
        RunnableBRunnable0Shape3S0200000_I0_3 runnableBRunnable0Shape3S0200000_I0_3;
        C16460p3 A0G = r3.A0G();
        if (A0G != null && A0G.A04() && (runnableBRunnable0Shape3S0200000_I0_3 = this.A00) != null) {
            synchronized (runnableBRunnable0Shape3S0200000_I0_3) {
                runnableBRunnable0Shape3S0200000_I0_3.A01 = r3;
            }
            this.A1P.Ab2(runnableBRunnable0Shape3S0200000_I0_3);
        }
    }
}
