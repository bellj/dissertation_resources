package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.1wF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43001wF extends C42951wA implements AbstractC43011wG {
    public AnonymousClass1YY A00;
    public final C16590pI A01;

    public C43001wF(AbstractC15710nm r14, C15570nT r15, C15450nH r16, C15550nR r17, C15610nY r18, AnonymousClass01d r19, C16590pI r20, AnonymousClass018 r21, C15670ni r22, C22630zO r23, AnonymousClass1YY r24) {
        super(r14, r15, r16, r17, r18, r19, r21, r22, r23, r24.A02);
        this.A01 = r20;
        this.A00 = r24;
    }

    public final CharSequence A08(String str) {
        int i = this.A00.A00 - 1;
        return i != 0 ? this.A01.A00.getResources().getQuantityString(R.plurals.reactions_summary_notification_title, i, str, Integer.valueOf(i)) : str;
    }

    public final String A09() {
        C16590pI r0 = this.A01;
        C22630zO r2 = this.A09;
        AnonymousClass1YY r1 = this.A00;
        int i = r1.A00 - 1;
        Context context = r0.A00;
        CharSequence A0F = r2.A0F(r1.A01);
        String str = ((AnonymousClass1X9) r1.A02).A01;
        if (!TextUtils.isEmpty(str)) {
            AnonymousClass009.A05(str);
            if (!C38491oB.A02(str)) {
                str = "□";
            }
        }
        if (i != 0) {
            return context.getString(R.string.reactions_notification_text_multiple, str, String.valueOf(i), A0F);
        }
        return context.getString(R.string.reactions_notification_text_single, str, A0F);
    }

    @Override // X.AbstractC43011wG
    public AnonymousClass1IS AEx() {
        return this.A00.A01.A0z;
    }
}
