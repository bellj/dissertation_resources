package X;

import android.view.View;
import android.widget.CompoundButton;

/* renamed from: X.0X3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X3 implements CompoundButton.OnCheckedChangeListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C03540Ie A01;
    public final /* synthetic */ C14260l7 A02;
    public final /* synthetic */ AnonymousClass28D A03;
    public final /* synthetic */ AbstractC14200l1 A04;
    public final /* synthetic */ boolean A05;
    public final /* synthetic */ boolean A06;

    public AnonymousClass0X3(View view, C03540Ie r2, C14260l7 r3, AnonymousClass28D r4, AbstractC14200l1 r5, boolean z, boolean z2) {
        this.A01 = r2;
        this.A06 = z;
        this.A00 = view;
        this.A05 = z2;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
    }

    @Override // android.widget.CompoundButton.OnCheckedChangeListener
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (this.A06) {
            AbstractC12680iK r1 = (AbstractC12680iK) this.A00;
            r1.setOnCheckedChangeListener(null);
            r1.setChecked(this.A05);
            r1.setOnCheckedChangeListener(this);
        }
        AbstractC14200l1 r5 = this.A04;
        if (r5 != null) {
            C14260l7 r4 = this.A02;
            C65093Ic.A01(r4.A01().A01());
            AnonymousClass28D r3 = this.A03;
            C14210l2 r2 = new C14210l2();
            r2.A05(r3, 0);
            r2.A05(Boolean.valueOf(z), 1);
            r2.A05(r4, 2);
            C28701Oq.A01(r4, r3, r2.A03(), r5);
        }
    }
}
