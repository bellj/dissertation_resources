package X;

import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1Wc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30101Wc implements AbstractC21730xt {
    public AbstractC13940ka A00;
    public AnonymousClass1VC A01;
    public final int A02;
    public final Handler A03 = new Handler(Looper.getMainLooper());
    public final AbstractC15710nm A04;
    public final C14650lo A05;
    public final AnonymousClass10A A06;
    public final UserJid A07;
    public final C17220qS A08;
    public final C19840ul A09;
    public final String A0A;

    public C30101Wc(AbstractC15710nm r3, C14650lo r4, AnonymousClass10A r5, UserJid userJid, C17220qS r7, C19840ul r8, String str, int i) {
        this.A02 = i;
        this.A07 = userJid;
        this.A0A = str;
        this.A04 = r3;
        this.A09 = r8;
        this.A08 = r7;
        this.A05 = r4;
        this.A06 = r5;
    }

    public AnonymousClass1V8 A00(String str) {
        String str2 = this.A0A;
        return new AnonymousClass1V8(new AnonymousClass1V8(new AnonymousClass1V8("profile", str2 != null ? new AnonymousClass1W9[]{new AnonymousClass1W9(this.A07, "jid"), new AnonymousClass1W9("tag", str2)} : new AnonymousClass1W9[]{new AnonymousClass1W9(this.A07, "jid")}), "business_profile", new AnonymousClass1W9[]{new AnonymousClass1W9("v", this.A02)}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", str), new AnonymousClass1W9("xmlns", "w:biz"), new AnonymousClass1W9("type", "get")});
    }

    public void A01(AbstractC13940ka r10) {
        this.A00 = r10;
        C17220qS r2 = this.A08;
        String A01 = r2.A01();
        this.A09.A03("profile_view_tag");
        r2.A09(this, A00(A01), A01, 132, 32000);
        StringBuilder sb = new StringBuilder("sendGetBusinessProfile jid=");
        sb.append(this.A07);
        Log.i(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A09.A02("profile_view_tag");
        Log.i("sendGetBusinessProfile/delivery-error");
        this.A03.post(new RunnableBRunnable0Shape0S1100000_I0(12, str, this));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        this.A09.A02("profile_view_tag");
        Log.i("sendGetBusinessProfile/response-error");
        this.A03.post(new RunnableBRunnable0Shape0S1200000_I0(this, r4, str, 8));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        AbstractC15710nm r1;
        String str2;
        this.A09.A02("profile_view_tag");
        AnonymousClass1V8 A0E = r5.A0E("business_profile");
        if (A0E == null) {
            r1 = this.A04;
            str2 = "payload businessProfileNode doesn't match server";
        } else {
            AnonymousClass1V8 A0E2 = A0E.A0E("profile");
            if (A0E2 == null) {
                r1 = this.A04;
                str2 = "payload profileNode doesn't match server";
            } else {
                UserJid userJid = this.A07;
                C30141Wg A00 = C42161ul.A00(userJid, A0E2);
                this.A05.A05(A00, userJid);
                this.A03.post(new RunnableBRunnable0Shape1S0200000_I0_1(this, 5, A00));
                return;
            }
        }
        r1.AaV("smb-reg-business-profile-fetch-failed", str2, false);
        APv(r5, str);
    }
}
