package X;

import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;

/* renamed from: X.1UT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UT extends TriggerEventListener {
    public final /* synthetic */ C14860mA A00;

    public AnonymousClass1UT(C14860mA r1) {
        this.A00 = r1;
    }

    @Override // android.hardware.TriggerEventListener
    public void onTrigger(TriggerEvent triggerEvent) {
        C14860mA r1 = this.A00;
        if (r1.A09) {
            r1.A0F();
        } else {
            r1.A0A = false;
        }
    }
}
