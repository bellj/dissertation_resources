package X;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import androidx.appcompat.widget.SearchView;

/* renamed from: X.0WO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WO implements View.OnKeyListener {
    public final /* synthetic */ SearchView A00;

    public AnonymousClass0WO(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // android.view.View.OnKeyListener
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        SearchView searchView = this.A00;
        if (searchView.A02 != null) {
            SearchView.SearchAutoComplete searchAutoComplete = searchView.A0k;
            if (!searchAutoComplete.isPopupShowing() || searchAutoComplete.getListSelection() == -1) {
                if (TextUtils.getTrimmedLength(searchAutoComplete.getText()) != 0 && keyEvent.hasNoModifiers() && keyEvent.getAction() == 1 && i == 66) {
                    view.cancelLongPress();
                    searchView.A0G(searchAutoComplete.getText().toString());
                    return true;
                }
            } else if (searchView.A02 == null || searchView.A0E == null || keyEvent.getAction() != 0 || !keyEvent.hasNoModifiers()) {
                return false;
            } else {
                if (i == 66 || i == 84 || i == 61) {
                    return searchView.A0K(searchAutoComplete.getListSelection());
                }
                int i2 = 0;
                if (i != 21) {
                    if (i == 22) {
                        i2 = searchAutoComplete.length();
                    } else if (i != 19) {
                        return false;
                    } else {
                        searchAutoComplete.getListSelection();
                        return false;
                    }
                }
                searchAutoComplete.setSelection(i2);
                searchAutoComplete.setListSelection(0);
                searchAutoComplete.clearListSelection();
                searchAutoComplete.A00();
                return true;
            }
        }
        return false;
    }
}
