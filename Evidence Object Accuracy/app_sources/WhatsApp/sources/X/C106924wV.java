package X;

/* renamed from: X.4wV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106924wV implements AnonymousClass5WY {
    public final long A00;
    public final boolean A01;
    public final long[] A02;
    public final long[] A03;

    public C106924wV(long[] jArr, long[] jArr2, long j) {
        int length = jArr.length;
        int length2 = jArr2.length;
        C95314dV.A03(C12960it.A1V(length, length2));
        boolean A1U = C12960it.A1U(length2);
        this.A01 = A1U;
        if (!A1U || jArr2[0] <= 0) {
            this.A02 = jArr;
            this.A03 = jArr2;
        } else {
            int i = length2 + 1;
            long[] jArr3 = new long[i];
            this.A02 = jArr3;
            long[] jArr4 = new long[i];
            this.A03 = jArr4;
            System.arraycopy(jArr, 0, jArr3, 1, length2);
            System.arraycopy(jArr2, 0, jArr4, 1, length2);
        }
        this.A00 = j;
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A00;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        if (!this.A01) {
            C94324bc r1 = C94324bc.A02;
            return new C92684Xa(r1, r1);
        }
        long[] jArr = this.A03;
        int A06 = AnonymousClass3JZ.A06(jArr, j, true);
        long j2 = jArr[A06];
        long[] jArr2 = this.A02;
        C94324bc r4 = new C94324bc(j2, jArr2[A06]);
        if (r4.A01 == j || A06 == jArr.length - 1) {
            return new C92684Xa(r4, r4);
        }
        int i = A06 + 1;
        return C92684Xa.A00(r4, jArr[i], jArr2[i]);
    }

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return this.A01;
    }
}
