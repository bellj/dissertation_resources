package X;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.3CF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CF {
    public final CopyOnWriteArrayList A00 = new CopyOnWriteArrayList();

    public void A00(AnonymousClass5QM r5) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.A00;
        Iterator it = copyOnWriteArrayList.iterator();
        while (it.hasNext()) {
            AnonymousClass4PD r1 = (AnonymousClass4PD) it.next();
            if (r1.A02 == r5) {
                r1.A00 = true;
                copyOnWriteArrayList.remove(r1);
            }
        }
    }
}
