package X;

import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.6Ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133396Ap implements AnonymousClass6MV {
    public final /* synthetic */ AnonymousClass6CF A00;
    public final /* synthetic */ String A01;

    public C133396Ap(AnonymousClass6CF r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r3) {
        AnonymousClass6CF r1 = this.A00;
        r1.A00.A1C();
        r1.A01.A01();
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r9) {
        AnonymousClass6CF r0 = this.A00;
        AbstractC1308360d r7 = r0.A01;
        C128545wH r6 = new C128545wH(r9);
        String str = this.A01;
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = r0.A00;
        r7.A0F.A00(new C1331069m(r6, new C130775zx(r7.A04, r7.A07, r7.A0D), pinBottomSheetDialogFragment, r7), r6, str);
    }
}
