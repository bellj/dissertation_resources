package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService;
import com.whatsapp.util.Log;

/* renamed from: X.3LK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LK implements ServiceConnection {
    public IInterface A00;
    public AnonymousClass4AX A01;
    public AnonymousClass4L8 A02;
    public final Context A03;
    public final AbstractC15710nm A04;
    public final AnonymousClass4L7 A05;
    public final Object A06 = C12970iu.A0l();
    public final String A07;

    public AnonymousClass3LK(Context context, AbstractC15710nm r3, AnonymousClass4L7 r4, AnonymousClass4L8 r5, String str) {
        this.A03 = context;
        this.A04 = r3;
        this.A07 = str;
        this.A05 = r4;
        this.A02 = r5;
        this.A01 = AnonymousClass4AX.NEW;
    }

    public void A00(String str) {
        String A0d = C12960it.A0d(this.A07, C12960it.A0k("svc-connection/detach-binder; service="));
        StringBuilder A0j = C12960it.A0j(A0d);
        A0j.append(", reason=");
        Log.i(C12960it.A0d(str, A0j));
        synchronized (this.A06) {
            AnonymousClass4AX r2 = this.A01;
            if (!(r2 == AnonymousClass4AX.CONNECTING || r2 == AnonymousClass4AX.CONNECTED)) {
                StringBuilder A0j2 = C12960it.A0j(A0d);
                A0j2.append(", reason=");
                A0j2.append(str);
                Log.e(C12960it.A0Z(r2, ", detached while in wrong state=", A0j2));
                AbstractC15710nm r3 = this.A04;
                StringBuilder A0h = C12960it.A0h();
                A0h.append("reason=");
                A0h.append(str);
                A0h.append(", unexpected state=");
                r3.AaV("svc-connection-detach-binder-failure", C12970iu.A0s(this.A01, A0h), false);
            }
        }
        A01(true);
    }

    public void A01(boolean z) {
        String A0d = C12960it.A0d(this.A07, C12960it.A0k("svc-connection/close; service="));
        Log.i(A0d);
        Object obj = this.A06;
        synchronized (obj) {
            AnonymousClass4AX r1 = this.A01;
            AnonymousClass4AX r0 = AnonymousClass4AX.CLOSED;
            if (r1 != r0) {
                AnonymousClass4L8 r3 = this.A02;
                this.A02 = null;
                this.A01 = r0;
                obj.notifyAll();
                StringBuilder A0j = C12960it.A0j(A0d);
                A0j.append(" -> state=");
                A0j.append(this.A01);
                C12960it.A1F(A0j);
                this.A03.unbindService(this);
                if (z && r3 != null) {
                    C26011Br r5 = r3.A00;
                    String str = r5.A08;
                    synchronized (r5) {
                        if (r5.A01 != this) {
                            AbstractC15710nm r32 = r5.A05;
                            StringBuilder A0h = C12960it.A0h();
                            A0h.append("name=");
                            r32.AaV("svc-client-close-unexpected-connection", C12960it.A0d(str, A0h), false);
                        } else {
                            r5.A01 = null;
                        }
                    }
                }
            }
        }
    }

    @Override // android.content.ServiceConnection
    public void onBindingDied(ComponentName componentName) {
        A00("binder-died");
    }

    @Override // android.content.ServiceConnection
    public void onNullBinding(ComponentName componentName) {
        A00("binder-null");
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        boolean z;
        IInterface r1;
        if (iBinder != null) {
            String A0d = C12960it.A0d(this.A07, C12960it.A0k("svc-connection/attach-binder; service="));
            Log.i(A0d);
            Object obj = this.A06;
            synchronized (obj) {
                AnonymousClass4AX r2 = this.A01;
                z = false;
                if (r2 == AnonymousClass4AX.CONNECTING) {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof IAppDataReaderService)) {
                        r1 = new C67603Sd(iBinder);
                    } else {
                        r1 = (IAppDataReaderService) queryLocalInterface;
                    }
                    this.A00 = r1;
                    this.A01 = AnonymousClass4AX.CONNECTED;
                    obj.notifyAll();
                    StringBuilder A0j = C12960it.A0j(A0d);
                    A0j.append(" -> state=");
                    A0j.append(this.A01);
                    C12960it.A1F(A0j);
                } else {
                    Log.e(C12960it.A0Z(r2, ", attached while in a wrong state=", C12960it.A0j(A0d)));
                    AbstractC15710nm r3 = this.A04;
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("unexpected state=");
                    r3.AaV("svc-connection-attach-binder-failure", C12970iu.A0s(this.A01, A0h), false);
                    z = true;
                }
            }
            if (z) {
                A01(true);
                return;
            }
            return;
        }
        A00("binder-null-on-connect");
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        A00("disconnected");
    }
}
