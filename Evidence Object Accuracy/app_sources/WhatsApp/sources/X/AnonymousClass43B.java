package X;

/* renamed from: X.43B  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43B extends AbstractC16110oT {
    public Integer A00;

    public AnonymousClass43B() {
        super(3200, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStatusPrivacySettings {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "setting", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
