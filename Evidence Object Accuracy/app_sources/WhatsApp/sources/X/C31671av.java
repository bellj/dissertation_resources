package X;

/* renamed from: X.1av  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31671av extends AbstractC31681aw {
    public static final C31671av A00 = new C31671av();
    public static final long serialVersionUID = 0;

    @Override // X.AbstractC31681aw, java.lang.Object
    public boolean equals(Object obj) {
        return obj == this;
    }

    @Override // X.AbstractC31681aw, java.lang.Object
    public int hashCode() {
        return 1502476572;
    }

    @Override // X.AbstractC31681aw, java.lang.Object
    public String toString() {
        return "Optional.absent()";
    }

    private Object readResolve() {
        return A00;
    }
}
