package X;

/* renamed from: X.6J4  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6J4 implements Runnable {
    public final /* synthetic */ C129455xk A00;
    public final /* synthetic */ AnonymousClass60X A01;
    public final /* synthetic */ Exception A02;

    public AnonymousClass6J4(C129455xk r1, AnonymousClass60X r2, Exception exc) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = exc;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.A00(this.A02);
    }
}
