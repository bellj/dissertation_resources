package X;

import java.util.List;

/* renamed from: X.5eX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119495eX extends AnonymousClass66W {
    public final C125795rp A00;

    public C119495eX(AbstractC17450qp r2, C124895qI r3, C125795rp r4) {
        super(r2, r3, null);
        this.A00 = r4;
    }

    @Override // X.AnonymousClass66W
    public Object A00(C14230l4 r7, C14220l3 r8, C1093651k r9) {
        String str;
        C93954b1 r3;
        String str2;
        String str3;
        boolean A0N = C16700pc.A0N(r9, r8);
        C16700pc.A0E(r7, 2);
        if (!C16700pc.A0O(r9.A00, "wa.action.perf.TrackPerfBlock")) {
            return super.A9j(r7, r8, r9);
        }
        List list = r8.A00;
        Object obj = list.get(0);
        C16700pc.A0B(obj);
        String str4 = (String) obj;
        Object obj2 = list.get(A0N ? 1 : 0);
        C16700pc.A0B(obj2);
        String str5 = (String) obj2;
        C125795rp r2 = this.A00;
        if (str4 == null) {
            return null;
        }
        int hashCode = str4.hashCode();
        if (hashCode == -1564272644) {
            str = "shadow_bind";
        } else if (hashCode != 283926139) {
            if (hashCode != 646679040) {
                return null;
            }
            str = "ui_rendered";
        } else if (!str4.equals("tracking_success")) {
            return null;
        } else {
            r3 = r2.A00;
            C16700pc.A0E(str5, 0);
            r3.A01("annotate", str5, "ending_surface_name", str5);
            str2 = C93954b1.A00(str5);
            str3 = "end_trace_successful";
            r3.A01(str3, str2, "", "");
            return null;
        }
        if (!str4.equals(str)) {
            return null;
        }
        r3 = r2.A00;
        C16700pc.A0E(str5, 0);
        str2 = C93954b1.A00(str5);
        str3 = "point";
        r3.A01(str3, str2, "", "");
        return null;
    }
}
