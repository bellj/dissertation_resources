package X;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.1vB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42421vB extends AbstractC42431vC {
    public C21860y6 A00;
    public final TextView A01 = ((TextView) findViewById(R.id.setup_payment_account_button));

    @Override // X.C42441vD
    public int getBackgroundResource() {
        return 0;
    }

    public C42421vB(Context context, AbstractC13890kV r3, AnonymousClass1XB r4) {
        super(context, r3, r4);
        A0X();
    }

    private void A0X() {
        TextView textView;
        int i;
        boolean z = true;
        int i2 = 8;
        if (!this.A19.A07()) {
            Log.i("PAY: Cannot render payment invite system messages because payment is not enabled");
            findViewById(R.id.divider).setVisibility(8);
            this.A01.setVisibility(8);
            ((AbstractC28551Oa) this).A0F.AaV("ConversationRowPaymentInviteSystemMessage/fillView", "Cannot render payment invite message because payment is disabled", true);
            return;
        }
        AnonymousClass1XB r3 = (AnonymousClass1XB) ((AbstractC28551Oa) this).A0O;
        int i3 = r3.A00;
        if (i3 != 40) {
            if (i3 != 41) {
                if (i3 == 64) {
                    if (!(r3 instanceof AbstractC30571Xy) || !((AbstractC30571Xy) r3).A01) {
                        z = false;
                    }
                } else if (i3 == 42 || i3 == 65 || i3 == 66) {
                    findViewById(R.id.divider).setVisibility(8);
                    this.A01.setVisibility(8);
                    return;
                } else {
                    return;
                }
            }
            View findViewById = findViewById(R.id.divider);
            int i4 = 8;
            if (z) {
                i4 = 0;
            }
            findViewById.setVisibility(i4);
            textView = this.A01;
            if (z) {
                i2 = 0;
            }
            textView.setVisibility(i2);
            textView.setText(R.string.payments_send_money_text);
            i = 28;
            if (!this.A00.A0C()) {
                i = 26;
            }
        } else if (this.A00.A0C()) {
            this.A01.setVisibility(8);
            findViewById(R.id.divider).setVisibility(8);
            return;
        } else {
            findViewById(R.id.divider).setVisibility(0);
            textView = this.A01;
            textView.setVisibility(0);
            textView.setText(R.string.payments_setup_account_reminder_button_text);
            i = 27;
        }
        textView.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, i, r3));
    }

    @Override // X.C42441vD, X.AnonymousClass1OY
    public void A0s() {
        A0X();
        super.A0s();
    }

    @Override // X.C42441vD, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != ((AbstractC28551Oa) this).A0O) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A0X();
        }
    }

    @Override // X.C42441vD, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_payment_invite_system_message;
    }

    @Override // X.C42441vD, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_payment_invite_system_message;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        return ((int) getResources().getDimension(R.dimen.payment_bubble_amount_width)) + (((int) getResources().getDimension(R.dimen.payment_bubble_margin_width)) << 1);
    }

    @Override // X.C42441vD, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_payment_invite_system_message;
    }
}
