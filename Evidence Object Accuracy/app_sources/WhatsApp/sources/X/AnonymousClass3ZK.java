package X;

import android.util.Pair;
import androidx.work.ListenableWorker;
import com.whatsapp.usernotice.UserNoticeStageUpdateWorker;
import com.whatsapp.util.Log;
import java.util.TreeMap;

/* renamed from: X.3ZK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZK implements AbstractC21730xt {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ C05380Pi A03;
    public final /* synthetic */ UserNoticeStageUpdateWorker A04;

    public AnonymousClass3ZK(C05380Pi r1, UserNoticeStageUpdateWorker userNoticeStageUpdateWorker, int i, int i2, int i3) {
        this.A04 = userNoticeStageUpdateWorker;
        this.A00 = i;
        this.A02 = i2;
        this.A01 = i3;
        this.A03 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Object r0;
        Log.e("UserNoticeStageUpdateWorker/onDeliveryFailure");
        int i = ((ListenableWorker) this.A04).A01.A00;
        C05380Pi r1 = this.A03;
        if (i > 4) {
            r0 = new AnonymousClass0GK();
        } else {
            r0 = new AnonymousClass042();
        }
        r1.A01(r0);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        Object r0;
        Pair A01 = C41151sz.A01(r4);
        Log.e(C12960it.A0b("UserNoticeStageUpdateWorker/onError ", A01));
        if (A01 != null && C12960it.A05(A01.first) == 400) {
            this.A04.A01.A02(C12960it.A0V());
        }
        int i = ((ListenableWorker) this.A04).A01.A00;
        C05380Pi r1 = this.A03;
        if (i > 4) {
            r0 = new AnonymousClass0GK();
        } else {
            r0 = new AnonymousClass042();
        }
        r1.A01(r0);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r10, String str) {
        Log.i("UserNoticeStageUpdateWorker/success");
        AnonymousClass1V8 A0E = r10.A0E("notice");
        if (A0E != null) {
            AnonymousClass12O r2 = this.A04.A02;
            int i = this.A00;
            int i2 = this.A02;
            Log.i(C12960it.A0W(i, "UserNoticeManager/handleStaleClientStage/notice id: "));
            r2.A08.A03(new C43831xf(i, A0E.A06(A0E.A0H("stage"), "stage"), i2, 1000 * A0E.A09(A0E.A0H("t"), "t")));
        }
        if (this.A01 == 5) {
            AnonymousClass12O r3 = this.A04.A02;
            int i3 = this.A00;
            Log.i(C12960it.A0W(i3, "UserNoticeManager/handleCleanup/notice id: "));
            Log.i(C12960it.A0W(i3, "UserNoticeManager/deleteUserNotice/notice id: "));
            r3.A07.A04(i3);
            AnonymousClass12N r22 = r3.A08;
            TreeMap treeMap = r22.A02;
            treeMap.remove(Integer.valueOf(i3));
            C43831xf A01 = r22.A01();
            if (A01 != null && A01.A00 == i3) {
                C12990iw.A11(r22.A00().edit().remove("current_user_notice_id").remove("current_user_notice_stage").remove("current_user_notice_stage_timestamp").remove("current_user_notice_version").remove("current_user_notice_duration_repeat_index").remove("current_user_notice_duration_repeat_timestamp"), "current_user_notice_banner_dismiss_timestamp");
            }
            r22.A04(C12980iv.A0x(treeMap.values()));
            r3.A05();
        }
        this.A03.A01(new AnonymousClass0GL(C006503b.A01));
    }
}
