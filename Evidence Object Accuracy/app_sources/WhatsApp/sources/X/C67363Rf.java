package X;

import android.os.Bundle;
import java.util.Set;

/* renamed from: X.3Rf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67363Rf implements AbstractC009404s {
    public final AnonymousClass07A A00;
    public final AbstractC009404s A01;
    public final Set A02;

    public C67363Rf(Bundle bundle, AbstractC009404s r3, AbstractC001500q r4, AnonymousClass2IF r5, Set set) {
        this.A02 = set;
        this.A01 = r3;
        this.A00 = new C53772fC(bundle, r4, r5, this);
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (this.A02.contains(cls.getName())) {
            return this.A00.A7r(cls);
        }
        return this.A01.A7r(cls);
    }
}
