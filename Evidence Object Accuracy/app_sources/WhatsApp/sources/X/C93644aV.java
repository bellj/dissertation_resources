package X;

import java.io.File;

/* renamed from: X.4aV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93644aV {
    public final C43161wW A00;
    public final C14370lK A01;
    public final AbstractC37701mr A02;
    public final File A03;

    public C93644aV(C14370lK r2, AbstractC37701mr r3, File file) {
        this.A02 = r3;
        this.A03 = file;
        this.A01 = r2;
        this.A00 = null;
    }

    public C93644aV(C43161wW r1, C14370lK r2, AbstractC37701mr r3, File file) {
        this.A02 = r3;
        this.A03 = file;
        this.A01 = r2;
        this.A00 = r1;
    }
}
