package X;

import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import java.lang.reflect.Field;

/* renamed from: X.0RU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0RU {
    public static Field A00;
    public static Field A01;
    public static Field A02;
    public static boolean A03 = true;

    static {
        try {
            Field declaredField = View.class.getDeclaredField("mAttachInfo");
            A02 = declaredField;
            declaredField.setAccessible(true);
            Class<?> cls = Class.forName("android.view.View$AttachInfo");
            Field declaredField2 = cls.getDeclaredField("mStableInsets");
            A01 = declaredField2;
            declaredField2.setAccessible(true);
            Field declaredField3 = cls.getDeclaredField("mContentInsets");
            A00 = declaredField3;
            declaredField3.setAccessible(true);
        } catch (ReflectiveOperationException e) {
            StringBuilder sb = new StringBuilder("Failed to get visible insets from AttachInfo ");
            sb.append(e.getMessage());
            Log.w("WindowInsetsCompat", sb.toString(), e);
        }
    }

    public static C018408o A00(View view) {
        if (A03 && view.isAttachedToWindow()) {
            try {
                Object obj = A02.get(view.getRootView());
                if (obj != null) {
                    Rect rect = (Rect) A01.get(obj);
                    Rect rect2 = (Rect) A00.get(obj);
                    if (!(rect == null || rect2 == null)) {
                        AnonymousClass0RW r4 = new AnonymousClass0RW();
                        AnonymousClass0U7 A002 = AnonymousClass0U7.A00(rect.left, rect.top, rect.right, rect.bottom);
                        AnonymousClass0PY r42 = r4.A00;
                        r42.A01(A002);
                        r42.A02(AnonymousClass0U7.A00(rect2.left, rect2.top, rect2.right, rect2.bottom));
                        C018408o A003 = r42.A00();
                        C06250St r1 = A003.A00;
                        r1.A0D(A003);
                        r1.A0B(view.getRootView());
                        return A003;
                    }
                }
            } catch (IllegalAccessException e) {
                StringBuilder sb = new StringBuilder("Failed to get insets from AttachInfo. ");
                sb.append(e.getMessage());
                Log.w("WindowInsetsCompat", sb.toString(), e);
            }
        }
        return null;
    }
}
