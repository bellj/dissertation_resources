package X;

import android.util.Base64;
import java.security.PublicKey;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5us  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127675us {
    public final long A00;
    public final AnonymousClass61O A01;
    public final AnonymousClass2ST A02;
    public final byte[] A03;

    public C127675us(String str, PublicKey publicKey, Date date) {
        try {
            AnonymousClass61O r1 = new AnonymousClass61O(publicKey, str.replaceAll("-----BEGIN CUSTOM CERTIFICATE-----", "").replaceAll("-----END CUSTOM CERTIFICATE-----", "").replaceAll("[\\r\\n]", ""));
            this.A01 = r1;
            if (r1.A04) {
                try {
                    JSONObject A05 = C13000ix.A05(new String(Base64.decode(r1.A03, 8)));
                    if (A05.getInt("version") == 0) {
                        byte[] decode = Base64.decode(A05.getString("keyId"), 2);
                        this.A03 = decode;
                        if (decode.length == 2) {
                            A05.getString("serviceName");
                            long j = A05.getLong("expiration");
                            this.A00 = j;
                            if (date.getTime() < j) {
                                byte[] decode2 = Base64.decode(A05.getString("publicKey"), 2);
                                if (decode2.length == 32) {
                                    this.A02 = new AnonymousClass2ST(decode2);
                                    return;
                                }
                                throw new C124725q0("Invalid custom certificate");
                            }
                            throw new C124725q0("Expired custom certificate");
                        }
                        throw new C124725q0("Invalid custom certificate");
                    }
                    throw new C124725q0("Invalid custom certificate version");
                } catch (JSONException unused) {
                    throw new C124725q0("Invalid JWT Payload");
                }
            } else {
                throw new C124725q0("Invalid custom certificate");
            }
        } catch (C124685pw unused2) {
            throw new C124725q0("Can't parse the JWT");
        }
    }
}
