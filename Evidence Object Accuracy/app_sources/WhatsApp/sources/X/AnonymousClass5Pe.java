package X;

import android.graphics.SurfaceTexture;
import android.view.SurfaceHolder;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

/* renamed from: X.5Pe  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5Pe extends AbstractC94504bw {
    public EGLConfig A00;
    public EGLContext A01;
    public EGLDisplay A02;
    public EGLSurface A03 = EGL10.EGL_NO_SURFACE;
    public final EGL10 A04;

    public AnonymousClass5Pe(int[] iArr) {
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        this.A04 = egl10;
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        if (eglGetDisplay == EGL10.EGL_NO_DISPLAY) {
            throw C12990iw.A0m("Unable to get EGL10 display");
        } else if (egl10.eglInitialize(eglGetDisplay, new int[2])) {
            this.A02 = eglGetDisplay;
            EGLConfig[] eGLConfigArr = new EGLConfig[1];
            if (this.A04.eglChooseConfig(eglGetDisplay, iArr, eGLConfigArr, 1, new int[1])) {
                EGLConfig eGLConfig = eGLConfigArr[0];
                this.A00 = eGLConfig;
                EGLContext eGLContext = EGL10.EGL_NO_CONTEXT;
                EGLContext eglCreateContext = this.A04.eglCreateContext(this.A02, eGLConfig, eGLContext, new int[]{12440, 2, 12344});
                if (eglCreateContext != EGL10.EGL_NO_CONTEXT) {
                    this.A01 = eglCreateContext;
                } else {
                    C93054Yu.A00("eglCreateContext");
                    throw C12990iw.A0m("Failed to create EGL context");
                }
            } else {
                C93054Yu.A00("eglChooseConfig");
                throw C12990iw.A0m("Unable to find any matching EGL config");
            }
        } else {
            C93054Yu.A00("eglInitialize");
            throw C12990iw.A0m("Unable to initialize EGL10");
        }
    }

    public final void A0B() {
        if (this.A02 == EGL10.EGL_NO_DISPLAY || this.A01 == EGL10.EGL_NO_CONTEXT || this.A00 == null) {
            throw C12990iw.A0m("This object has been released");
        }
    }

    public final void A0C(Object obj) {
        if ((obj instanceof SurfaceHolder) || (obj instanceof SurfaceTexture)) {
            A0B();
            if (this.A03 == EGL10.EGL_NO_SURFACE) {
                EGLSurface eglCreateWindowSurface = this.A04.eglCreateWindowSurface(this.A02, this.A00, obj, new int[]{12344});
                this.A03 = eglCreateWindowSurface;
                if (eglCreateWindowSurface == EGL10.EGL_NO_SURFACE) {
                    C93054Yu.A00("eglCreateWindowSurface");
                    throw C12990iw.A0m("Failed to create window surface");
                }
                return;
            }
            throw C12990iw.A0m("Already has an EGLSurface");
        }
        throw C12960it.A0U("Input must be either a SurfaceHolder or SurfaceTexture");
    }
}
