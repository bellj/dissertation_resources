package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.3Na  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC66293Na implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ ViewTreeObserver A00;
    public final /* synthetic */ ScaleGestureDetector$OnScaleGestureListenerC52942c4 A01;
    public final /* synthetic */ boolean A02;

    public ViewTreeObserver$OnGlobalLayoutListenerC66293Na(ViewTreeObserver viewTreeObserver, ScaleGestureDetector$OnScaleGestureListenerC52942c4 r2, boolean z) {
        this.A01 = r2;
        this.A00 = viewTreeObserver;
        this.A02 = z;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        int A04;
        int A05;
        this.A00.removeOnGlobalLayoutListener(this);
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r4 = this.A01;
        View view = r4.A0C;
        if (view == null) {
            return;
        }
        if (r4.A0M) {
            view.setPivotX((float) (view.getMeasuredWidth() >> 1));
            View view2 = r4.A0C;
            view2.setPivotY((float) (view2.getMeasuredHeight() >> 1));
            if (this.A02) {
                A04 = r4.A09;
                A05 = r4.A0A;
            } else {
                A04 = r4.A04(r4.A0C.getWidth());
                A05 = r4.A05(r4.A0C.getHeight());
            }
            r4.A08();
            r4.A0U.A0H(r4.A0C, A04, A05);
            r4.A04 = A04;
            r4.A05 = A05;
            r4.A0M = false;
        } else if (!r4.A0N) {
            int A042 = r4.A04(view.getWidth());
            int A052 = r4.A05(r4.A0C.getHeight());
            r4.A08();
            r4.A0U.A0H(r4.A0C, A042, A052);
            r4.A04 = A042;
            r4.A05 = A052;
        }
    }
}
