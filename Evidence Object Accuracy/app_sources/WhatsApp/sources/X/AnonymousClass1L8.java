package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.whispersystems.jobqueue.Job;

/* renamed from: X.1L8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1L8 extends Thread {
    public final ThreadPoolExecutor A00;
    public final AtomicInteger A01;
    public final AnonymousClass1Lv A02;
    public final AnonymousClass1LD A03;

    public AnonymousClass1L8(String str, AtomicInteger atomicInteger, AnonymousClass1Lv r12, AnonymousClass1LD r13, int i, int i2, boolean z) {
        super(str);
        this.A02 = r12;
        this.A03 = r13;
        this.A01 = atomicInteger;
        ThreadPoolExecutor threadPoolExecutor = z ? new ThreadPoolExecutor(i, i2, 60, TimeUnit.SECONDS, new SynchronousQueue(), new ThreadFactoryC47702Ca()) : null;
        this.A00 = threadPoolExecutor;
        if (threadPoolExecutor != null) {
            threadPoolExecutor.setRejectedExecutionHandler(new RejectedExecutionHandler(r12) { // from class: X.2Cb
                public final /* synthetic */ AnonymousClass1Lv A01;

                {
                    this.A01 = r2;
                }

                @Override // java.util.concurrent.RejectedExecutionHandler
                public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor2) {
                    AnonymousClass1L8 r3 = AnonymousClass1L8.this;
                    AnonymousClass1Lv r2 = this.A01;
                    if (runnable instanceof RunnableBRunnable0Shape8S0200000_I0_8) {
                        RunnableBRunnable0Shape8S0200000_I0_8 runnableBRunnable0Shape8S0200000_I0_8 = (RunnableBRunnable0Shape8S0200000_I0_8) runnable;
                        if (runnableBRunnable0Shape8S0200000_I0_8.A02 == 49) {
                            Job job = (Job) runnableBRunnable0Shape8S0200000_I0_8.A00;
                            r2.A02(job);
                            String str2 = job.parameters.groupId;
                            if (str2 != null) {
                                synchronized (r2) {
                                    r2.A03.remove(str2);
                                    r2.A05.A00.open();
                                }
                            }
                        }
                    }
                    try {
                        r3.A00.getQueue().put(new RunnableBRunnable0Shape0S0000000_I0(11));
                    } catch (InterruptedException unused) {
                        Thread.currentThread().interrupt();
                    }
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0372, code lost:
        if (r1 >= 500) goto L_0x0374;
     */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x0127 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(org.whispersystems.jobqueue.Job r18) {
        /*
        // Method dump skipped, instructions count: 1263
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1L8.A00(org.whispersystems.jobqueue.Job):void");
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        while (true) {
            AnonymousClass1Lv r1 = this.A02;
            Job job = null;
            do {
                try {
                    job = (Job) r1.A04.take();
                    continue;
                } catch (InterruptedException unused) {
                }
            } while (job == null);
            ThreadPoolExecutor threadPoolExecutor = this.A00;
            if (threadPoolExecutor == null) {
                A00(job);
            } else {
                threadPoolExecutor.execute(new RunnableBRunnable0Shape8S0200000_I0_8(job, 49, this));
            }
        }
    }
}
