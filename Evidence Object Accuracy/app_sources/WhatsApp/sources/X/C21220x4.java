package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;

/* renamed from: X.0x4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21220x4 {
    public final C18850tA A00;
    public final C14820m6 A01;
    public final C22100yW A02;
    public final C22230yk A03;
    public final C14860mA A04;

    public C21220x4(C18850tA r1, C14820m6 r2, C22100yW r3, C22230yk r4, C14860mA r5) {
        this.A04 = r5;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    public void A00() {
        SharedPreferences sharedPreferences = this.A01.A00;
        if (!sharedPreferences.getBoolean("companion_reg_opt_in_enabled", false)) {
            if (!this.A04.A04().isEmpty()) {
                A01();
            }
            sharedPreferences.edit().putBoolean("companion_reg_opt_in_enabled", true).apply();
        }
    }

    public void A01() {
        Log.i("MDOptInManager/Logging_Out_Legacy_Devices");
        try {
            this.A03.A0I(true, true);
            this.A04.A07();
        } catch (Exception e) {
            Log.w("MDOptInManager/Logging_Out_Legacy_Devices/Failed", e);
        }
    }

    public void A02(AnonymousClass02O r6) {
        SharedPreferences sharedPreferences = this.A01.A00;
        if (sharedPreferences.getBoolean("companion_reg_opt_in_enabled", false)) {
            C22100yW r2 = this.A02;
            if (!r2.A09().isEmpty()) {
                Log.i("MDOptInManager/Logging_Out_Companion_Devices");
                r2.A03(new C41041sn(r6, this));
                r2.A0E("md_opt_out", false);
                return;
            }
            Log.i("MDOptInManager/Deleting_Sync_Data");
            this.A00.A0J(2);
            sharedPreferences.edit().remove("delete_chat_clear_chat_nux_accepted").apply();
            sharedPreferences.edit().putBoolean("companion_reg_opt_in_enabled", false).apply();
            if (r6 != null) {
                r6.apply(Boolean.TRUE);
            }
        }
    }
}
