package X;

/* renamed from: X.2WP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2WP extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;

    public AnonymousClass2WP() {
        super(1094, new AnonymousClass00E(1, 200, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A02);
        r3.Abe(7, this.A00);
        r3.Abe(1, this.A03);
        r3.Abe(5, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamAppLaunch {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "appLaunchCpuT", this.A02);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "appLaunchDestination", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "appLaunchT", this.A03);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "appLaunchTypeT", obj2);
        sb.append("}");
        return sb.toString();
    }
}
