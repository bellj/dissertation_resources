package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import java.util.regex.Matcher;

/* renamed from: X.3mE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC76783mE extends AbstractC106564vu implements AbstractC117065Yc {
    public final String A00;

    @Override // X.AbstractC117065Yc
    public void Aca(long j) {
    }

    public AbstractC76783mE(String str) {
        super(new C76743m8[2], new AbstractC76773mC[2]);
        this.A00 = str;
        int i = super.A00;
        C76763mA[] r3 = this.A0B;
        int length = r3.length;
        C95314dV.A04(C12960it.A1V(i, length));
        for (C76763mA r0 : r3) {
            r0.A01(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        }
    }

    public static long A00(Matcher matcher, int i) {
        return Long.parseLong(matcher.group(i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:71:0x016a, code lost:
        if (r0 != false) goto L_0x0112;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x018c, code lost:
        if (r0 != false) goto L_0x010c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC116805Wy A07(byte[] r24, int r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 1056
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC76783mE.A07(byte[], int, boolean):X.5Wy");
    }

    @Override // X.AnonymousClass5XF
    public final String getName() {
        return this.A00;
    }
}
