package X;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/* renamed from: X.3mM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76863mM extends AnonymousClass4IJ {
    public long A00 = -9223372036854775807L;
    public long[] A01 = new long[0];
    public long[] A02 = new long[0];

    public C76863mM() {
        super(new C106984wb());
    }

    public static Object A00(C95304dT r4, int i) {
        if (i == 0) {
            return Double.valueOf(Double.longBitsToDouble(r4.A0H()));
        }
        if (i == 1) {
            boolean z = true;
            if (r4.A0C() != 1) {
                z = false;
            }
            return Boolean.valueOf(z);
        } else if (i == 2) {
            return A01(r4);
        } else {
            if (i == 3) {
                HashMap A11 = C12970iu.A11();
                while (true) {
                    String A01 = A01(r4);
                    int A0C = r4.A0C();
                    if (A0C == 9) {
                        return A11;
                    }
                    Object A00 = A00(r4, A0C);
                    if (A00 != null) {
                        A11.put(A01, A00);
                    }
                }
            } else if (i == 8) {
                return A02(r4);
            } else {
                if (i == 10) {
                    int A0E = r4.A0E();
                    ArrayList A0w = C12980iv.A0w(A0E);
                    for (int i2 = 0; i2 < A0E; i2++) {
                        Object A002 = A00(r4, r4.A0C());
                        if (A002 != null) {
                            A0w.add(A002);
                        }
                    }
                    return A0w;
                } else if (i != 11) {
                    return null;
                } else {
                    Date date = new Date((long) Double.valueOf(Double.longBitsToDouble(r4.A0H())).doubleValue());
                    r4.A0T(2);
                    return date;
                }
            }
        }
    }

    public static String A01(C95304dT r4) {
        int A0F = r4.A0F();
        int i = r4.A01;
        r4.A0T(A0F);
        return new String(r4.A02, i, A0F);
    }

    public static HashMap A02(C95304dT r5) {
        int A0E = r5.A0E();
        HashMap hashMap = new HashMap(A0E);
        for (int i = 0; i < A0E; i++) {
            String A01 = A01(r5);
            Object A00 = A00(r5, r5.A0C());
            if (A00 != null) {
                hashMap.put(A01, A00);
            }
        }
        return hashMap;
    }
}
