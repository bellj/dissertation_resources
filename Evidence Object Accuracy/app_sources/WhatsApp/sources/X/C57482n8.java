package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57482n8 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57482n8 A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public AbstractC27881Jp A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public AbstractC27881Jp A04;
    public String A05 = "";

    static {
        C57482n8 r0 = new C57482n8();
        A06 = r0;
        r0.A0W();
    }

    public C57482n8() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A01 = r1;
        this.A04 = r1;
        this.A03 = r1;
        this.A02 = r1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57482n8 r9 = (C57482n8) obj2;
                this.A01 = r8.Afm(this.A01, r9.A01, C12960it.A1R(this.A00), C12960it.A1R(r9.A00));
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                String str = this.A05;
                int i2 = r9.A00;
                this.A05 = r8.Afy(str, r9.A05, A1V, C12960it.A1V(i2 & 2, 2));
                this.A04 = r8.Afm(this.A04, r9.A04, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A03 = r8.Afm(this.A03, r9.A03, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r9.A00 & 8, 8));
                this.A02 = r8.Afm(this.A02, r9.A02, C12960it.A1V(this.A00 & 16, 16), C12960it.A1V(r9.A00 & 16, 16));
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                this.A00 |= 1;
                                this.A01 = r82.A08();
                            } else if (A03 == 18) {
                                String A0A = r82.A0A();
                                this.A00 |= 2;
                                this.A05 = A0A;
                            } else if (A03 == 26) {
                                this.A00 |= 4;
                                this.A04 = r82.A08();
                            } else if (A03 == 34) {
                                this.A00 |= 8;
                                this.A03 = r82.A08();
                            } else if (A03 == 42) {
                                this.A00 |= 16;
                                this.A02 = r82.A08();
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57482n8();
            case 5:
                return new C81473u4();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C57482n8.class) {
                        if (A07 == null) {
                            A07 = AbstractC27091Fz.A09(A06);
                        }
                    }
                }
                return A07;
            default:
                throw C12970iu.A0z();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A05(this.A01, 1, 0);
        }
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A05, i2);
        }
        int i4 = this.A00;
        if ((i4 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A04, 3, i2);
        }
        if ((i4 & 8) == 8) {
            i2 = AbstractC27091Fz.A05(this.A03, 4, i2);
        }
        if ((i4 & 16) == 16) {
            i2 = AbstractC27091Fz.A05(this.A02, 5, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A01, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A05);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A04, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A03, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0K(this.A02, 5);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
