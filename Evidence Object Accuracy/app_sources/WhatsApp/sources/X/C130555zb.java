package X;

import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.5zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130555zb {
    public static final ArrayList A01;
    public static final ArrayList A02;
    public static final ArrayList A03;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "p2m";
        A02 = C12960it.A0m("p2p", strArr, 1);
        String[] strArr2 = new String[3];
        strArr2[0] = "1";
        strArr2[1] = "2";
        A03 = C12960it.A0m("3", strArr2, 2);
        String[] strArr3 = new String[2];
        strArr3[0] = "0";
        A01 = C12960it.A0m("1", strArr3, 1);
    }

    public C130555zb(AnonymousClass3CT r22, C128675wU r23, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, String str18, String str19, String str20, String str21, String str22, String str23, String str24) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-send-to-vpa");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 255, false)) {
            C41141sy.A01(A0N, "device-id", str2);
        }
        if (C117295Zj.A1W(str3, 0, false)) {
            C41141sy.A01(A0N, "mpin", str3);
        }
        if (AnonymousClass3JT.A0E(str4, 0, 100, false)) {
            C41141sy.A01(A0N, "sender-vpa", str4);
        }
        if (AnonymousClass3JT.A0E(str5, 0, 100, false)) {
            C41141sy.A01(A0N, "receiver-vpa", str5);
        }
        if (AnonymousClass3JT.A0E(str6, 0, 35, false)) {
            C41141sy.A01(A0N, "seq-no", str6);
        }
        if (AnonymousClass3JT.A0E(str7, 0, 9007199254740991L, false)) {
            C41141sy.A01(A0N, "message-id", str7);
        }
        if (str8 != null && AnonymousClass3JT.A0E(str8, 0, 4, true)) {
            C41141sy.A01(A0N, "mcc", str8);
        }
        if (str9 != null && C117295Zj.A1V(str9, 0, true)) {
            C41141sy.A01(A0N, "ref-id", str9);
        }
        if (str10 != null && AnonymousClass3JT.A0E(str10, 0, 2048, true)) {
            C41141sy.A01(A0N, "ref-url", str10);
        }
        if (str11 != null && AnonymousClass3JT.A0E(str11, 0, 9007199254740991L, true)) {
            C41141sy.A01(A0N, "note", str11);
        }
        if (str12 != null && C117295Zj.A1W(str12, 1, true)) {
            C41141sy.A01(A0N, "payee-name", str12);
        }
        if (str13 != null && AnonymousClass3JT.A0E(str13, 0, 4, true)) {
            C41141sy.A01(A0N, "mode", str13);
        }
        if (str14 != null && AnonymousClass3JT.A0E(str14, 1, 4, true)) {
            C41141sy.A01(A0N, "purpose-code", str14);
        }
        if (str15 != null && AnonymousClass3JT.A0E(str15, 0, 9007199254740991L, true)) {
            C41141sy.A01(A0N, "upi-bank-info", str15);
        }
        if (str16 != null && C117295Zj.A1U(str16)) {
            C41141sy.A01(A0N, "sender-vpa-id", str16);
        }
        if (str17 != null && C117295Zj.A1U(str17)) {
            C41141sy.A01(A0N, "receiver-vpa-id", str17);
        }
        if (str18 != null && AnonymousClass3JT.A0E(str18, 8, 15, true)) {
            C41141sy.A01(A0N, "receiver-upi-number", str18);
        }
        if (str19 != null && C117295Zj.A1U(str19)) {
            C41141sy.A01(A0N, "amount", str19);
        }
        if (str20 != null && C117295Zj.A1U(str20)) {
            C41141sy.A01(A0N, "currency", str20);
        }
        if (str21 != null && C117295Zj.A1W(str21, 1, true)) {
            C41141sy.A01(A0N, "token", str21);
        }
        if (str22 != null && C117295Zj.A1W(str22, 1, true)) {
            C41141sy.A01(A0N, "id", str22);
        }
        ArrayList arrayList = A02;
        if (str23 != null) {
            A0N.A0A(str23, "transaction-type", arrayList);
        }
        A0N.A0A("2", "version", A03);
        ArrayList arrayList2 = A01;
        if (str24 != null) {
            A0N.A0A(str24, "is_first_send", arrayList2);
        }
        C41141sy r5 = new C41141sy("amount");
        AnonymousClass1V8 r4 = r23.A00;
        r5.A07(r4, C12960it.A0l());
        r5.A09(r4, Arrays.asList(new String[0]), C12960it.A0l());
        C117295Zj.A1H(r5, A0N);
        this.A00 = C117295Zj.A0J(A0N, A0M, r22);
    }
}
