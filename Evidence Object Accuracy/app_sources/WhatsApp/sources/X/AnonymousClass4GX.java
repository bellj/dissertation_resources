package X;

/* renamed from: X.4GX  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4GX {
    public static final AnonymousClass4WL A00;

    static {
        AnonymousClass4WL r0;
        String property = System.getProperty("java.specification.version");
        int i = 65542;
        if (property != null) {
            int A01 = AnonymousClass03B.A01(property, '.', 0, 6);
            try {
                if (A01 < 0) {
                    i = Integer.parseInt(property) * 65536;
                } else {
                    int i2 = A01 + 1;
                    int A012 = AnonymousClass03B.A01(property, '.', i2, 4);
                    if (A012 < 0) {
                        A012 = property.length();
                    }
                    String substring = property.substring(0, A01);
                    C16700pc.A0B(substring);
                    String substring2 = property.substring(i2, A012);
                    C16700pc.A0B(substring2);
                    i = (Integer.parseInt(substring) * 65536) + Integer.parseInt(substring2);
                }
            } catch (NumberFormatException unused) {
            }
        }
        if (i >= 65544) {
            try {
                Object newInstance = C113705Iq.class.newInstance();
                C16700pc.A0B(newInstance);
                try {
                    try {
                        r0 = (AnonymousClass4WL) newInstance;
                    } catch (ClassCastException e) {
                        throw A00(newInstance, e);
                    }
                } catch (ClassNotFoundException unused2) {
                }
            } catch (ClassNotFoundException unused3) {
                Object newInstance2 = Class.forName("kotlin.internal.JRE8PlatformImplementations").newInstance();
                C16700pc.A0B(newInstance2);
                try {
                    r0 = (AnonymousClass4WL) newInstance2;
                } catch (ClassCastException e2) {
                    throw A00(newInstance2, e2);
                }
            }
            A00 = r0;
        }
        if (i >= 65543) {
            try {
                Object newInstance3 = C113715Ir.class.newInstance();
                C16700pc.A0B(newInstance3);
                try {
                    try {
                        r0 = (AnonymousClass4WL) newInstance3;
                    } catch (ClassCastException e3) {
                        throw A00(newInstance3, e3);
                    }
                } catch (ClassNotFoundException unused4) {
                }
            } catch (ClassNotFoundException unused5) {
                Object newInstance4 = Class.forName("kotlin.internal.JRE7PlatformImplementations").newInstance();
                C16700pc.A0B(newInstance4);
                try {
                    r0 = (AnonymousClass4WL) newInstance4;
                } catch (ClassCastException e4) {
                    throw A00(newInstance4, e4);
                }
            }
            A00 = r0;
        }
        r0 = new AnonymousClass4WL();
        A00 = r0;
    }

    public static Throwable A00(Object obj, Throwable th) {
        ClassLoader classLoader = obj.getClass().getClassLoader();
        ClassLoader classLoader2 = AnonymousClass4WL.class.getClassLoader();
        StringBuilder sb = new StringBuilder();
        sb.append("Instance classloader: ");
        sb.append(classLoader);
        sb.append(", base type classloader: ");
        sb.append(classLoader2);
        Throwable initCause = new ClassCastException(sb.toString()).initCause(th);
        C16700pc.A0B(initCause);
        return initCause;
    }
}
