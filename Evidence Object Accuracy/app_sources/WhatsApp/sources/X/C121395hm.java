package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121395hm extends AbstractC130285z6 {
    public C125755rl A00;
    public List A01 = C12960it.A0l();
    public final C128715wY A02;

    public C121395hm(C128715wY r7, AnonymousClass1V8 r8) {
        this.A02 = r7;
        try {
            AnonymousClass1V8 A0E = r8.A0E("account_question_set");
            if (A0E != null) {
                this.A00 = new C125755rl(A00(A0E));
            }
            List A0J = r8.A0J("transactions_question_set");
            if (A0J.size() > 0) {
                Iterator it = A0J.iterator();
                while (it.hasNext()) {
                    AnonymousClass1V8 A0d = C117305Zk.A0d(it);
                    this.A01.add(new C121405hn(this.A02.A00(A0d.A0F("transaction")), A00(A0d)));
                }
            }
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: TextInputChallenge/parseTextInput failed.");
        }
    }

    public static final List A00(AnonymousClass1V8 r13) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = r13.A0J("required_questions").iterator();
        while (it.hasNext()) {
            AnonymousClass1V8 A0d = C117305Zk.A0d(it);
            String A0H = A0d.A0H("type");
            String A0H2 = A0d.A0H("step_up_type");
            String A0H3 = A0d.A0H("id");
            String A0H4 = A0d.A0F("text").A0H("text");
            String A0H5 = A0d.A0F("label").A0H("text");
            List A0J = A0d.A0J("option");
            ArrayList A0l2 = C12960it.A0l();
            Iterator it2 = A0J.iterator();
            while (it2.hasNext()) {
                AnonymousClass1V8 A0d2 = C117305Zk.A0d(it2);
                String A0H6 = A0d2.A0H("text");
                A0d2.A0H("value");
                A0l2.add(new C125765rm(A0H6));
            }
            A0l.add(new C127995vO(A0H3, A0H, A0H2, A0H4, A0H5, A0l2));
        }
        return A0l;
    }
}
