package X;

import android.graphics.PointF;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0Gt  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Gt extends AnonymousClass0QR {
    public AnonymousClass0SF A00;
    public AnonymousClass0SF A01;
    public final PointF A02 = new PointF();
    public final PointF A03 = new PointF();
    public final AnonymousClass0QR A04;
    public final AnonymousClass0QR A05;

    public AnonymousClass0Gt(AnonymousClass0QR r2, AnonymousClass0QR r3) {
        super(Collections.emptyList());
        this.A04 = r2;
        this.A05 = r3;
        A07(super.A02);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r2, float f) {
        return A03();
    }

    @Override // X.AnonymousClass0QR
    public void A07(float f) {
        AnonymousClass0QR r0 = this.A04;
        r0.A07(f);
        AnonymousClass0QR r3 = this.A05;
        r3.A07(f);
        this.A02.set(((Number) r0.A03()).floatValue(), ((Number) r3.A03()).floatValue());
        int i = 0;
        while (true) {
            List list = this.A07;
            if (i < list.size()) {
                ((AbstractC12030hG) list.get(i)).AYB();
                i++;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0074  */
    /* renamed from: A09 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.PointF A03() {
        /*
            r5 = this;
            X.0SF r0 = r5.A00
            r3 = 0
            if (r0 == 0) goto L_0x0079
            X.0QR r2 = r5.A04
            X.0iI r0 = r2.A06
            X.0U8 r1 = r0.AC6()
            X.AnonymousClass0MI.A00()
            if (r1 == 0) goto L_0x0079
            r2.A01()
            java.lang.Float r0 = r1.A08
            X.0SF r4 = r5.A00
            if (r0 == 0) goto L_0x001e
            r0.floatValue()
        L_0x001e:
            java.lang.Object r2 = r1.A0F
            java.lang.Object r1 = r1.A09
            X.0NB r0 = r4.A02
            r0.A01 = r2
            r0.A00 = r1
            java.lang.Object r4 = r4.A01
            java.lang.Number r4 = (java.lang.Number) r4
        L_0x002c:
            X.0SF r0 = r5.A01
            if (r0 == 0) goto L_0x0057
            X.0QR r2 = r5.A05
            X.0iI r0 = r2.A06
            X.0U8 r1 = r0.AC6()
            X.AnonymousClass0MI.A00()
            if (r1 == 0) goto L_0x0057
            r2.A01()
            java.lang.Float r0 = r1.A08
            X.0SF r3 = r5.A01
            if (r0 == 0) goto L_0x0049
            r0.floatValue()
        L_0x0049:
            java.lang.Object r2 = r1.A0F
            java.lang.Object r1 = r1.A09
            X.0NB r0 = r3.A02
            r0.A01 = r2
            r0.A00 = r1
            java.lang.Object r3 = r3.A01
            java.lang.Number r3 = (java.lang.Number) r3
        L_0x0057:
            r1 = 0
            android.graphics.PointF r2 = r5.A03
            if (r4 != 0) goto L_0x0074
            android.graphics.PointF r0 = r5.A02
            float r0 = r0.x
        L_0x0060:
            r2.set(r0, r1)
            float r1 = r2.x
            if (r3 != 0) goto L_0x006f
            android.graphics.PointF r0 = r5.A02
            float r0 = r0.y
        L_0x006b:
            r2.set(r1, r0)
            return r2
        L_0x006f:
            float r0 = r3.floatValue()
            goto L_0x006b
        L_0x0074:
            float r0 = r4.floatValue()
            goto L_0x0060
        L_0x0079:
            r4 = r3
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Gt.A03():android.graphics.PointF");
    }
}
