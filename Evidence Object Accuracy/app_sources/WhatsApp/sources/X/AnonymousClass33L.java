package X;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import org.json.JSONObject;

/* renamed from: X.33L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33L extends AbstractC454821u {
    public static Typeface A0B;
    public static Typeface A0C;
    public static Typeface A0D;
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public StaticLayout A04;
    public String A05;
    public String A06;
    public final Context A07;
    public final TextPaint A08;
    public final AnonymousClass018 A09;
    public final AnonymousClass19M A0A;

    public AnonymousClass33L(Context context, AnonymousClass018 r4, AnonymousClass19M r5) {
        this.A08 = new TextPaint(1);
        this.A03 = 0;
        this.A07 = context;
        this.A0A = r5;
        this.A09 = r4;
        super.A01.setStyle(Paint.Style.FILL);
    }

    public AnonymousClass33L(Context context, AnonymousClass018 r10, AnonymousClass19M r11, JSONObject jSONObject) {
        this(context, r10, r11);
        String str;
        this.A02 = ((float) jSONObject.getInt("orig-w")) / 100.0f;
        this.A01 = ((float) jSONObject.getInt("orig-h")) / 100.0f;
        this.A00 = 0.0f;
        A0T(jSONObject.getString("text"), jSONObject.getInt("style"));
        if (this.A03 == 3) {
            str = this.A06;
        } else {
            str = this.A05;
        }
        this.A08.setTextSize(((float) jSONObject.getInt("text-size")) / 100.0f);
        this.A04 = new StaticLayout(AnonymousClass1US.A01(AbstractC36671kL.A03(context, this.A08, r11, str)), this.A08, ((int) this.A02) + 1, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
        this.A00 = 0.0f;
        int i = 0;
        while (true) {
            int lineCount = this.A04.getLineCount();
            StaticLayout staticLayout = this.A04;
            if (i < lineCount) {
                float lineWidth = staticLayout.getLineWidth(i);
                if (lineWidth > this.A00) {
                    this.A00 = lineWidth;
                }
                i++;
            } else {
                staticLayout.getHeight();
                super.A0A(jSONObject);
                return;
            }
        }
    }

    public static Typeface A02(Context context) {
        Typeface typeface = A0C;
        if (typeface != null) {
            return typeface;
        }
        Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Oswald-Heavy.ttf");
        A0C = createFromAsset;
        return createFromAsset;
    }

    public static Typeface A03(Context context) {
        Typeface typeface = A0D;
        if (typeface != null) {
            return typeface;
        }
        Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), "fonts/Norican-Regular.ttf");
        A0D = createFromAsset;
        return createFromAsset;
    }

    public static void A04(AnonymousClass33L r8, int i) {
        if (i != 0) {
            RectF rectF = ((AbstractC454821u) r8).A02;
            float width = rectF.width() / r8.A00;
            rectF.set(rectF.centerX() - (r8.A02 / 2.0f), rectF.centerY() - (r8.A01 / 2.0f), rectF.centerX() + (r8.A02 / 2.0f), rectF.centerY() + (r8.A01 / 2.0f));
            r8.A0R();
            rectF.set(rectF.centerX() - ((rectF.width() * width) / 2.0f), rectF.centerY() - ((rectF.height() * width) / 2.0f), rectF.centerX() + ((rectF.width() * width) / 2.0f), rectF.centerY() + ((width * rectF.height()) / 2.0f));
        }
    }

    @Override // X.AbstractC454821u
    public void A0M(C91484Rx r3) {
        super.A0M(r3);
        AnonymousClass45I r32 = (AnonymousClass45I) r3;
        A0T(r32.A01, r32.A00);
    }

    @Override // X.AbstractC454821u
    public void A0N(JSONObject jSONObject) {
        super.A0N(jSONObject);
        jSONObject.put("orig-w", (int) (this.A02 * 100.0f));
        jSONObject.put("orig-h", (int) (this.A01 * 100.0f));
        jSONObject.put("text", this.A05);
        jSONObject.put("text-size", (int) (this.A08.getTextSize() * 100.0f));
        jSONObject.put("style", this.A03);
    }

    @Override // X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        this.A02 = Math.abs(f3 - f);
        this.A01 = Math.abs(f4 - f2);
        RectF rectF2 = super.A02;
        rectF2.set(f, f2, f3, f4);
        rectF2.sort();
        A0R();
    }

    public final void A0R() {
        String str;
        if (!TextUtils.isEmpty(this.A05)) {
            if (this.A03 == 3) {
                str = this.A06;
            } else {
                str = this.A05;
            }
            float f = AbstractC454821u.A05 + 1.0f;
            TextPaint textPaint = this.A08;
            while (true) {
                textPaint.setTextSize(f);
                float desiredWidth = Layout.getDesiredWidth(str, textPaint);
                if (f >= AbstractC454821u.A08 || desiredWidth >= super.A02.width()) {
                    break;
                }
                f += 1.0f;
            }
            textPaint.setTextSize(f - 1.0f);
            textPaint.setColor(super.A01.getColor());
            CharSequence A01 = AnonymousClass1US.A01(AbstractC36671kL.A03(this.A07, textPaint, this.A0A, str));
            RectF rectF = super.A02;
            this.A04 = new StaticLayout(A01, textPaint, ((int) rectF.width()) + 1, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
            this.A00 = 0.0f;
            int i = 0;
            while (true) {
                int lineCount = this.A04.getLineCount();
                StaticLayout staticLayout = this.A04;
                if (i < lineCount) {
                    float lineWidth = staticLayout.getLineWidth(i);
                    if (lineWidth > this.A00) {
                        this.A00 = lineWidth;
                    }
                    i++;
                } else {
                    float height = (float) staticLayout.getHeight();
                    float f2 = rectF.left;
                    float f3 = rectF.top;
                    float f4 = rectF.right;
                    float f5 = rectF.bottom;
                    float f6 = f2 + f4;
                    float f7 = this.A00;
                    float f8 = f3 + f5;
                    rectF.set((f6 - f7) / 2.0f, (f8 - height) / 2.0f, (f6 + f7) / 2.0f, (f8 + height) / 2.0f);
                    rectF.sort();
                    return;
                }
            }
        }
    }

    public void A0S(int i) {
        TextPaint textPaint;
        Typeface typeface;
        if (this.A03 != i) {
            this.A03 = i;
            if (i == 3) {
                textPaint = this.A08;
                typeface = A02(this.A07);
            } else {
                textPaint = this.A08;
                if (i == 2) {
                    typeface = A03(this.A07);
                } else {
                    typeface = Typeface.DEFAULT;
                }
            }
            textPaint.setTypeface(typeface);
            textPaint.setFakeBoldText(C12970iu.A1W(i));
            A04(this, (this.A00 > 0.0f ? 1 : (this.A00 == 0.0f ? 0 : -1)));
        }
    }

    public void A0T(String str, int i) {
        TextPaint textPaint;
        Typeface typeface;
        if (!str.equals(this.A05) || this.A03 != i) {
            this.A05 = str;
            this.A06 = str.toUpperCase(C12970iu.A14(this.A09));
            this.A03 = i;
            if (i == 3) {
                textPaint = this.A08;
                typeface = A02(this.A07);
            } else {
                textPaint = this.A08;
                if (i == 2) {
                    typeface = A03(this.A07);
                } else {
                    typeface = Typeface.DEFAULT;
                }
            }
            textPaint.setTypeface(typeface);
            textPaint.setFakeBoldText(C12970iu.A1W(i));
            A04(this, (this.A00 > 0.0f ? 1 : (this.A00 == 0.0f ? 0 : -1)));
        }
    }
}
