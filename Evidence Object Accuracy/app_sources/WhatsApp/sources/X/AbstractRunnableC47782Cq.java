package X;

import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2Cq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractRunnableC47782Cq extends EmptyBaseRunnable0 implements Runnable {
    public final String A00;

    public AbstractRunnableC47782Cq(String str) {
        this.A00 = str;
    }

    @Override // java.lang.Object
    public String toString() {
        return this.A00;
    }
}
