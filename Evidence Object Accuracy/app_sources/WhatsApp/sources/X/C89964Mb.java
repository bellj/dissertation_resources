package X;

import android.graphics.Typeface;

/* renamed from: X.4Mb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C89964Mb {
    public final int A00;
    public final Typeface A01;

    public C89964Mb(Typeface typeface, int i) {
        this.A00 = i;
        this.A01 = typeface;
    }
}
