package X;

import java.io.Serializable;
import java.util.Iterator;

/* renamed from: X.1Jp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC27881Jp implements Iterable, Serializable {
    public static final AnonymousClass27D A00;
    public static final AbstractC27881Jp A01 = new C27861Jn(C27851Jm.A04);
    public int hash = 0;

    @Override // java.lang.Object
    public abstract boolean equals(Object obj);

    static {
        AnonymousClass27D r0;
        try {
            Class.forName("android.content.Context");
            r0 = new AnonymousClass27C();
        } catch (ClassNotFoundException unused) {
            r0 = new AnonymousClass51T();
        }
        A00 = r0;
    }

    public static int A00(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    public static AbstractC27881Jp A01(byte[] bArr, int i, int i2) {
        return new C27861Jn(A00.A7n(bArr, i, i2));
    }

    public byte A02(int i) {
        C27861Jn r3 = (C27861Jn) this;
        if (!(r3 instanceof C56832m1)) {
            return r3.bytes[i];
        }
        C56832m1 r32 = (C56832m1) r3;
        int i2 = r32.bytesLength;
        if (((i2 - (i + 1)) | i) >= 0) {
            return r32.bytes[r32.bytesOffset + i];
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder("Index < 0: ");
            sb.append(i);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder("Index > length: ");
        sb2.append(i);
        sb2.append(", ");
        sb2.append(i2);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    public int A03() {
        C27861Jn r1 = (C27861Jn) this;
        if (!(r1 instanceof C56832m1)) {
            return r1.bytes.length;
        }
        return ((C56832m1) r1).bytesLength;
    }

    public final byte[] A04() {
        int A03 = A03();
        if (A03 == 0) {
            return C27851Jm.A04;
        }
        byte[] bArr = new byte[A03];
        C27861Jn r3 = (C27861Jn) this;
        if (!(r3 instanceof C56832m1)) {
            System.arraycopy(r3.bytes, 0, bArr, 0, A03);
            return bArr;
        }
        C56832m1 r32 = (C56832m1) r3;
        System.arraycopy(r32.bytes, r32.bytesOffset + 0, bArr, 0, A03);
        return bArr;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = this.hash;
        if (i == 0) {
            int A03 = A03();
            i = A03;
            C27861Jn r2 = (C27861Jn) this;
            byte[] bArr = r2.bytes;
            int A05 = r2.A05() + 0;
            for (int i2 = A05; i2 < A05 + A03; i2++) {
                i = (i * 31) + bArr[i2];
            }
            if (i == 0) {
                i = 1;
            }
            this.hash = i;
        }
        return i;
    }

    @Override // java.lang.Iterable
    public /* bridge */ /* synthetic */ Iterator iterator() {
        return new C71343cm(this);
    }

    @Override // java.lang.Object
    public final String toString() {
        return String.format("<ByteString@%s size=%d>", Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(A03()));
    }
}
