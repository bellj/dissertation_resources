package X;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.3CA  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CA {
    public final Map A00(Map map, Map map2) {
        Map map3;
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        Iterator A0n = C12960it.A0n(map2);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            Object key = A15.getKey();
            Object value = A15.getValue();
            Object obj = linkedHashMap.get(key);
            Map map4 = null;
            if (obj instanceof Map) {
                map3 = (Map) obj;
            } else {
                map3 = null;
            }
            if (value instanceof Map) {
                map4 = (Map) value;
            }
            if (!(map3 == null || map4 == null)) {
                value = A00(map3, map4);
            }
            linkedHashMap.put(key, value);
        }
        return linkedHashMap;
    }
}
