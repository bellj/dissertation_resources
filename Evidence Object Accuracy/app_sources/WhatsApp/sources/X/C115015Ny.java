package X;

/* renamed from: X.5Ny  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C115015Ny extends AbstractC94944cn {
    public AnonymousClass20J A00;
    public byte[] A01;

    public C115015Ny() {
        this(new AnonymousClass5OB());
    }

    public C115015Ny(AnonymousClass5XI r2) {
        AnonymousClass5G4 r0 = new AnonymousClass5G4(r2);
        this.A00 = r0;
        this.A01 = new byte[r0.A01];
    }

    public final byte[] A05(int i) {
        AnonymousClass20J r13 = this.A00;
        int AE2 = r13.AE2();
        int i2 = ((i + AE2) - 1) / AE2;
        byte[] bArr = new byte[4];
        byte[] bArr2 = new byte[i2 * AE2];
        r13.AIc(new AnonymousClass20K(super.A01));
        int i3 = 0;
        for (int i4 = 1; i4 <= i2; i4++) {
            int i5 = 3;
            while (true) {
                byte b = (byte) (bArr[i5] + 1);
                bArr[i5] = b;
                if (b != 0) {
                    break;
                }
                i5--;
            }
            byte[] bArr3 = this.A02;
            int i6 = super.A00;
            if (i6 != 0) {
                if (bArr3 != null) {
                    r13.update(bArr3, 0, bArr3.length);
                }
                r13.update(bArr, 0, 4);
                byte[] bArr4 = this.A01;
                r13.A97(bArr4, 0);
                int length = bArr4.length;
                System.arraycopy(bArr4, 0, bArr2, i3, length);
                for (int i7 = 1; i7 < i6; i7++) {
                    r13.update(bArr4, 0, length);
                    r13.A97(bArr4, 0);
                    for (int i8 = 0; i8 != length; i8++) {
                        int i9 = i3 + i8;
                        C72463ee.A0P(bArr4[i8], bArr2, bArr2[i9], i9);
                    }
                }
                i3 += AE2;
            } else {
                throw C12970iu.A0f("iteration count must be at least 1.");
            }
        }
        return bArr2;
    }
}
