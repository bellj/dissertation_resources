package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1lv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37271lv extends AnonymousClass015 implements AbstractC37281lw, AbstractC37291lx, AbstractC37301ly {
    public int A00;
    public C15370n3 A01;
    public boolean A02 = false;
    public boolean A03 = false;
    public final AnonymousClass016 A04 = new AnonymousClass016();
    public final AnonymousClass016 A05;
    public final AnonymousClass016 A06 = new AnonymousClass016();
    public final AnonymousClass016 A07 = new AnonymousClass016();
    public final AbstractC15710nm A08;
    public final C22640zP A09;
    public final C112245Cq A0A;
    public final C15550nR A0B;
    public final C27131Gd A0C;
    public final AnonymousClass10S A0D;
    public final C15610nY A0E;
    public final C14820m6 A0F;
    public final C27151Gf A0G;
    public final C21320xE A0H;
    public final C15600nX A0I;
    public final AnonymousClass5UJ A0J;
    public final AnonymousClass11A A0K;
    public final AbstractC469528i A0L;
    public final AnonymousClass1E8 A0M;
    public final AnonymousClass1E5 A0N;
    public final C15580nU A0O;
    public final C20660w7 A0P;
    public final C20750wG A0Q;
    public final C36161jQ A0R = new C36161jQ(0);
    public final C36161jQ A0S = new C36161jQ(new ArrayList());
    public final C36161jQ A0T = new C36161jQ(new ArrayList());
    public final ExecutorC27271Gr A0U;
    public final AbstractC14440lR A0V;
    public final List A0W = new CopyOnWriteArrayList();
    public final List A0X = new CopyOnWriteArrayList();
    public final List A0Y = new ArrayList();
    public final List A0Z = new ArrayList();

    public C37271lv(AbstractC15710nm r11, C22640zP r12, C15550nR r13, AnonymousClass10S r14, C15610nY r15, C14820m6 r16, C19990v2 r17, C21320xE r18, C15600nX r19, AnonymousClass11A r20, AnonymousClass1E8 r21, AnonymousClass1E5 r22, C15580nU r23, C20660w7 r24, C20750wG r25, AbstractC14440lR r26) {
        AnonymousClass016 r0;
        C37141lY r5 = new C37141lY(this);
        this.A0C = r5;
        AnonymousClass57A r4 = new AnonymousClass57A(this);
        this.A0J = r4;
        AnonymousClass32Y r3 = new AnonymousClass32Y(this);
        this.A0L = r3;
        C468227u r2 = new C468227u(this);
        this.A0G = r2;
        this.A08 = r11;
        this.A0V = r26;
        this.A0P = r24;
        this.A0B = r13;
        this.A0E = r15;
        this.A0D = r14;
        this.A0N = r22;
        this.A0Q = r25;
        this.A0F = r16;
        this.A09 = r12;
        this.A0M = r21;
        this.A0H = r18;
        this.A0K = r20;
        this.A0I = r19;
        this.A0O = r23;
        C15370n3 A0A = r13.A0A(r23);
        this.A01 = A0A;
        if (A0A == null) {
            r0 = new AnonymousClass016();
        } else {
            r0 = new AnonymousClass016(A0A);
        }
        this.A05 = r0;
        r21.A03(r3);
        r14.A03(r5);
        r18.A03(r2);
        r20.A00.add(r4);
        this.A0U = new ExecutorC27271Gr(r26, false);
        this.A0A = new C112245Cq(r17);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A0M.A04(this.A0L);
        this.A0D.A04(this.A0C);
        this.A0H.A04(this.A0G);
        AnonymousClass11A r0 = this.A0K;
        r0.A00.remove(this.A0J);
    }

    public final void A04() {
        int i;
        int min;
        List list = this.A0X;
        list.clear();
        List list2 = this.A0Z;
        C112245Cq r0 = this.A0A;
        Collections.sort(list2, r0);
        List list3 = this.A0Y;
        Collections.sort(list3, r0);
        C15580nU r6 = this.A0O;
        list.add(new AnonymousClass4Ww(1, r6));
        if (!this.A0F.A00.getBoolean("dismissed_invite_member_row", false) && this.A0I.A0G(r6) && !this.A0N.A00(this.A01)) {
            list.add(new AnonymousClass4Ww(2, r6));
        }
        if (!list2.isEmpty()) {
            list.add(new AnonymousClass4Ww(3, Integer.valueOf((int) R.string.participating_subgroup_title)));
            if (this.A03) {
                min = list2.size();
            } else {
                min = Math.min(this.A00, list2.size());
            }
            for (int i2 = 0; i2 < min; i2++) {
                list.add(new AnonymousClass4Ww(4, new AnonymousClass2WJ(((AnonymousClass1OU) list2.get(i2)).A02)));
            }
            if (list2.size() > min) {
                list.add(new AnonymousClass4Ww(6, new C90264Nf(0, this.A03)));
            }
        }
        boolean A0D = this.A0I.A0D(r6);
        if (A0D || !list3.isEmpty()) {
            list.add(new AnonymousClass4Ww(3, Integer.valueOf((int) R.string.non_participating_subgroup_title)));
            if (A0D) {
                list.add(new AnonymousClass4Ww(12, Integer.valueOf((int) R.string.new_group)));
            }
            if (!list3.isEmpty()) {
                if (this.A02) {
                    i = list3.size();
                } else {
                    i = Math.min(this.A00, list3.size());
                }
                for (int i3 = 0; i3 < i; i3++) {
                    list.add(new AnonymousClass4Ww(5, list3.get(i3)));
                }
                if (list3.size() > i) {
                    list.add(new AnonymousClass4Ww(6, new C90264Nf(1, this.A02)));
                }
            }
        }
        list.add(new AnonymousClass4Ww(11, r6));
        this.A0R.A0A(Integer.valueOf(list2.size() + list3.size()));
        this.A0T.A0A(list);
    }

    public final void A05() {
        List list = this.A0W;
        list.clear();
        list.addAll(this.A0Z);
        list.addAll(this.A0Y);
        this.A0S.A0A(list);
    }

    @Override // X.AbstractC37291lx
    public void AQ3(C90264Nf r4) {
        if (r4.A00 == 0) {
            this.A03 = !this.A03;
        } else {
            this.A02 = !this.A02;
        }
        this.A0U.execute(new RunnableBRunnable0Shape3S0100000_I0_3(this, 49));
    }

    @Override // X.AbstractC37281lw
    public void ARV() {
        this.A0U.execute(new RunnableBRunnable0Shape3S0100000_I0_3(this, 45));
    }
}
