package X;

import android.content.Intent;
import android.net.Uri;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.payments.ui.NoviTransactionMethodDetailsFragment;

/* renamed from: X.5a3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117495a3 extends ClickableSpan {
    public final /* synthetic */ NoviTransactionMethodDetailsFragment A00;

    public C117495a3(NoviTransactionMethodDetailsFragment noviTransactionMethodDetailsFragment) {
        this.A00 = noviTransactionMethodDetailsFragment;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        NoviTransactionMethodDetailsFragment noviTransactionMethodDetailsFragment = this.A00;
        AnonymousClass608 r2 = new AnonymousClass608(noviTransactionMethodDetailsFragment.A00);
        r2.A00.append("WA");
        Uri A01 = r2.A01();
        AnonymousClass60Y r5 = noviTransactionMethodDetailsFragment.A06;
        C128365vz r1 = new AnonymousClass610("HELP_LINK_CLICK", "SEND_MONEY", "REVIEW_TRANSACTION", "LINK").A00;
        r1.A0i = "PAYMENT_METHODS";
        r1.A0L = A01.toString();
        r5.A06(r1);
        noviTransactionMethodDetailsFragment.A0v(new Intent("android.intent.action.VIEW", A01));
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        C117295Zj.A0k(this.A00.A02(), textPaint);
    }
}
