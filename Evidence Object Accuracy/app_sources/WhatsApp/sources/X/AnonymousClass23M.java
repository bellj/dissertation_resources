package X;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.23M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23M {
    public static String A00 = "";

    public static int A00(EditText editText) {
        if (!editText.isFocused()) {
            return -1;
        }
        String obj = editText.getText().toString();
        int i = 0;
        int i2 = 0;
        while (i < editText.getSelectionEnd() && i <= obj.length()) {
            if (obj.charAt(i) <= '9' && obj.charAt(i) >= '0') {
                i2++;
            }
            i++;
        }
        return i2;
    }

    public static int A01(String str, String str2) {
        int length;
        int length2;
        if (str == null || str2 == null || (length = str.length()) < 6 || (length2 = str2.length()) < 6) {
            return -1;
        }
        int i = length - 6;
        int i2 = length2 - 6;
        int i3 = 0;
        int i4 = 0;
        do {
            if (str.charAt(i + i3) != str2.charAt(i2 + i3)) {
                i4++;
            }
            i3++;
        } while (i3 < 6);
        return i4;
    }

    public static long A02(String str, long j) {
        if (str != null) {
            try {
                return Long.parseLong(str);
            } catch (NumberFormatException e) {
                Log.w(e);
            }
        }
        return j;
    }

    public static Dialog A03(ActivityC13810kN r4, AnonymousClass19Y r5, C18640sm r6, AnonymousClass01d r7, C15890o4 r8, AnonymousClass11G r9, C20800wL r10, AbstractC14440lR r11) {
        Log.i("registrationutils/dialog/cant-connect");
        C004802e r2 = new C004802e(r4);
        r2.A0A(r4.getString(R.string.register_try_again_later));
        r2.A03(new DialogInterface.OnClickListener(r5, r6, r7, r8, r9, r10, r11) { // from class: X.3L2
            public final /* synthetic */ AnonymousClass19Y A01;
            public final /* synthetic */ C18640sm A02;
            public final /* synthetic */ AnonymousClass01d A03;
            public final /* synthetic */ C15890o4 A04;
            public final /* synthetic */ AnonymousClass11G A05;
            public final /* synthetic */ C20800wL A06;
            public final /* synthetic */ AbstractC14440lR A07;

            {
                this.A07 = r8;
                this.A01 = r2;
                this.A03 = r4;
                this.A05 = r6;
                this.A04 = r5;
                this.A06 = r7;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ActivityC13810kN r42 = ActivityC13810kN.this;
                AbstractC14440lR r1 = this.A07;
                AnonymousClass19Y r52 = this.A01;
                AnonymousClass01d r72 = this.A03;
                AnonymousClass11G r102 = this.A05;
                C15890o4 r82 = this.A04;
                C20800wL r112 = this.A06;
                C18640sm r62 = this.A02;
                Log.i("verifynumber/dialog/cant-connect/button/checkstatus");
                C36021jC.A00(r42, 109);
                r1.Aaz(new C627238i(null, r42, r52, r62, r72, r82, null, r102, r112, "reg/cant-connect", true, true, false), new String[0]);
            }
        }, r4.getString(R.string.check_system_status));
        r2.A01(new DialogInterface.OnClickListener() { // from class: X.4g3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ActivityC13810kN r1 = ActivityC13810kN.this;
                Log.i("registername/dialog/cant-connect/button/cancel");
                C36021jC.A00(r1, 109);
            }
        }, r4.getString(R.string.cancel));
        r2.A08(new DialogInterface.OnCancelListener() { // from class: X.4ep
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                Log.i("registername/dialog/cant-connect/cancel");
            }
        });
        return r2.create();
    }

    public static Dialog A04(ActivityC13810kN r14, AnonymousClass19Y r15, AnonymousClass018 r16, AnonymousClass11G r17, Runnable runnable, String str, String str2) {
        boolean z = false;
        if (runnable != null) {
            z = true;
        }
        StringBuilder sb = new StringBuilder("registrationutils/dialog/ban cancelable=");
        sb.append(z);
        Log.w(sb.toString());
        String A0G = r16.A0G(A0E(str, str2));
        StringBuilder sb2 = new StringBuilder();
        sb2.append(A0G);
        sb2.append("\n\n");
        sb2.append(r14.getString(R.string.register_user_is_banned_bottom));
        SpannableString spannableString = new SpannableString(sb2.toString());
        spannableString.setSpan(new StyleSpan(1), 0, A0G.length() + 2, 33);
        C004802e r2 = new C004802e(r14);
        r2.A0A(spannableString);
        r2.A0B(z);
        r2.A01(new DialogInterface.OnClickListener(runnable) { // from class: X.4gi
            public final /* synthetic */ Runnable A01;

            {
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ActivityC13810kN r22 = ActivityC13810kN.this;
                Runnable runnable2 = this.A01;
                C36021jC.A00(r22, 124);
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
        }, r14.getString(R.string.cancel));
        r2.A03(new DialogInterface.OnClickListener(r15, r17, runnable, str, str2) { // from class: X.4h9
            public final /* synthetic */ AnonymousClass19Y A01;
            public final /* synthetic */ AnonymousClass11G A02;
            public final /* synthetic */ Runnable A03;
            public final /* synthetic */ String A04;
            public final /* synthetic */ String A05;

            {
                this.A03 = r4;
                this.A01 = r2;
                this.A02 = r3;
                this.A04 = r5;
                this.A05 = r6;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ActivityC13810kN r6 = ActivityC13810kN.this;
                Runnable runnable2 = this.A03;
                AnonymousClass19Y r5 = this.A01;
                AnonymousClass11G r3 = this.A02;
                String str3 = this.A04;
                String str4 = this.A05;
                C36021jC.A00(r6, 124);
                if (runnable2 != null) {
                    runnable2.run();
                }
                boolean A002 = r3.A00();
                StringBuilder A0k = C12960it.A0k("blocked +");
                A0k.append(str3);
                r6.startActivity(r5.A00(r6, null, null, null, C12960it.A0d(str4, A0k), null, null, null, A002));
            }
        }, r14.getString(R.string.register_user_support_button));
        return r2.create();
    }

    public static Dialog A05(ActivityC13810kN r4, AnonymousClass19Y r5, AnonymousClass11G r6, String str, String str2) {
        Log.w("registrationutils/dialog/underage-ban cancelable=");
        C004802e r2 = new C004802e(r4);
        r2.setTitle(r4.getString(R.string.underage_account_ban_title));
        r2.A0A(r4.getString(R.string.underage_account_ban_description));
        r2.A0B(false);
        r2.A03(new DialogInterface.OnClickListener() { // from class: X.4g5
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C36021jC.A00(ActivityC13810kN.this, 125);
            }
        }, r4.getString(R.string.cancel));
        r2.A02(new DialogInterface.OnClickListener(r5, r6, str, str2) { // from class: X.4h8
            public final /* synthetic */ AnonymousClass19Y A01;
            public final /* synthetic */ AnonymousClass11G A02;
            public final /* synthetic */ String A03;
            public final /* synthetic */ String A04;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
                this.A04 = r5;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ActivityC13810kN r52 = ActivityC13810kN.this;
                AnonymousClass19Y r42 = this.A01;
                AnonymousClass11G r3 = this.A02;
                String str3 = this.A03;
                String str4 = this.A04;
                C36021jC.A00(r52, 125);
                boolean A002 = r3.A00();
                StringBuilder A0k = C12960it.A0k("blocked +");
                A0k.append(str3);
                r52.startActivity(r42.A00(r52, null, null, null, C12960it.A0d(str4, A0k), null, null, null, A002));
            }
        }, r4.getString(R.string.register_contact_support));
        return r2.create();
    }

    public static Dialog A06(ActivityC13810kN r7, Runnable runnable, Runnable runnable2, Runnable runnable3) {
        View inflate = LayoutInflater.from(r7).inflate(R.layout.mtrl_alert_dialog_actions_vertical, (ViewGroup) null);
        C004802e r3 = new C004802e(r7);
        r3.A06(R.string.register_user_is_banned_export_report);
        r3.setView(inflate);
        r3.A0B(false);
        TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.button3);
        TextView textView2 = (TextView) AnonymousClass028.A0D(inflate, R.id.button1);
        TextView textView3 = (TextView) AnonymousClass028.A0D(inflate, R.id.button2);
        textView.setVisibility(0);
        textView.setText(R.string.cancel);
        textView.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(r7, 42, runnable));
        textView3.setVisibility(0);
        textView3.setText(R.string.delete);
        textView3.setTextColor(AnonymousClass00T.A00(r7, R.color.red_button_text));
        textView3.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(r7, 44, runnable3));
        textView2.setVisibility(0);
        textView2.setText(R.string.export);
        textView2.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(r7, 45, runnable2));
        return r3.create();
    }

    public static SpannableStringBuilder A07(TextAppearanceSpan textAppearanceSpan, String str, Map map, int i) {
        Spanned fromHtml = Html.fromHtml(str);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fromHtml);
        URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                if (map.containsKey(uRLSpan.getURL())) {
                    int spanStart = spannableStringBuilder.getSpanStart(uRLSpan);
                    int spanEnd = spannableStringBuilder.getSpanEnd(uRLSpan);
                    int spanFlags = spannableStringBuilder.getSpanFlags(uRLSpan);
                    spannableStringBuilder.removeSpan(uRLSpan);
                    spannableStringBuilder.setSpan(new C58262oP(uRLSpan, map, i), spanStart, spanEnd, spanFlags);
                    if (textAppearanceSpan != null) {
                        spannableStringBuilder.setSpan(textAppearanceSpan, spanStart, spanEnd, spanFlags);
                    }
                }
            }
        }
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder A08(Runnable runnable, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put(str2, runnable);
        return A07(null, str, hashMap, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0012, code lost:
        if (r24.A03 == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass04S A09(X.AnonymousClass12P r18, X.ActivityC13810kN r19, X.C14900mE r20, X.AnonymousClass19Y r21, X.AnonymousClass018 r22, X.AnonymousClass11G r23, X.AnonymousClass1R1 r24, java.lang.Runnable r25, java.lang.String r26, java.lang.String r27) {
        /*
            r9 = 0
            r12 = r24
            if (r24 == 0) goto L_0x00ea
            java.lang.String r0 = r12.A02
        L_0x0007:
            int r7 = X.C88034Ea.A00(r0)
            r10 = 1
            r1 = 0
            if (r24 == 0) goto L_0x0014
            boolean r0 = r12.A03
            r11 = 1
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            r11 = 0
        L_0x0015:
            r8 = 0
            r6 = r25
            if (r25 == 0) goto L_0x001b
            r8 = 1
        L_0x001b:
            java.lang.String r2 = "registrationutils/dialog/ban cancelable="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r2)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.w(r0)
            r5 = r26
            r4 = r27
            java.lang.String r0 = A0E(r5, r4)
            r13 = r22
            java.lang.String r2 = r13.A0G(r0)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r2)
            java.lang.String r0 = "\n\n"
            r3.append(r0)
            r15 = r19
            java.lang.String r0 = r15.getString(r7)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            android.text.SpannableString r7 = new android.text.SpannableString
            r7.<init>(r0)
            android.text.style.StyleSpan r3 = new android.text.style.StyleSpan
            r3.<init>(r10)
            int r0 = r2.length()
            int r2 = r0 + 2
            r0 = 33
            r7.setSpan(r3, r1, r2, r0)
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r15)
            r0 = 2131559300(0x7f0d0384, float:1.874394E38)
            android.view.View r2 = r2.inflate(r0, r9)
            X.02e r3 = new X.02e
            r3.<init>(r15)
            r3.A0A(r7)
            r3.setView(r2)
            r3.A0B(r8)
            r0 = 2131362395(0x7f0a025b, float:1.834457E38)
            android.view.View r8 = X.AnonymousClass028.A0D(r2, r0)
            android.widget.TextView r8 = (android.widget.TextView) r8
            r0 = 2131362393(0x7f0a0259, float:1.8344565E38)
            android.view.View r7 = X.AnonymousClass028.A0D(r2, r0)
            android.widget.TextView r7 = (android.widget.TextView) r7
            r0 = 2131362394(0x7f0a025a, float:1.8344567E38)
            android.view.View r2 = X.AnonymousClass028.A0D(r2, r0)
            android.widget.TextView r2 = (android.widget.TextView) r2
            r8.setVisibility(r1)
            r0 = 2131886925(0x7f12034d, float:1.9408443E38)
            r8.setText(r0)
            r7.setVisibility(r1)
            r0 = 2131891252(0x7f121434, float:1.9417219E38)
            r7.setText(r0)
            r2.setVisibility(r1)
            r0 = 2131891253(0x7f121435, float:1.941722E38)
            if (r11 == 0) goto L_0x00ba
            r0 = 2131887349(0x7f1204f5, float:1.9409303E38)
        L_0x00ba:
            r2.setText(r0)
            r1 = 46
            com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0 r0 = new com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0
            r0.<init>(r15, r1, r6)
            r8.setOnClickListener(r0)
            r17 = 1
            r16 = r20
            r14 = r18
            com.facebook.redex.ViewOnClickCListenerShape0S0500000_I0 r11 = new com.facebook.redex.ViewOnClickCListenerShape0S0500000_I0
            r11.<init>(r12, r13, r14, r15, r16, r17)
            r7.setOnClickListener(r11)
            r9 = r21
            r10 = r23
            r8 = r15
            r11 = r6
            r12 = r5
            r13 = r4
            X.3lg r7 = new X.3lg
            r7.<init>(r8, r9, r10, r11, r12, r13)
            r2.setOnClickListener(r7)
            X.04S r0 = r3.create()
            return r0
        L_0x00ea:
            r0 = r9
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass23M.A09(X.12P, X.0kN, X.0mE, X.19Y, X.018, X.11G, X.1R1, java.lang.Runnable, java.lang.String, java.lang.String):X.04S");
    }

    public static C65993Lw A0A(C20920wX r6, String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str)) {
            String replaceAll = str.replaceAll("\\D", "");
            if (!TextUtils.isEmpty(str3) && !str3.equals("ZZ")) {
                try {
                    C71133cR A0E = r6.A0E(replaceAll, str3.toUpperCase(Locale.US));
                    return new C65993Lw(String.valueOf(A0E.countryCode_), String.valueOf(A0E.nationalNumber_), str2);
                } catch (AnonymousClass4C8 e) {
                    Log.w("parsePhoneNumber/exception", e);
                }
            }
            String A01 = AnonymousClass1ZT.A01(replaceAll);
            if (A01 != null) {
                return new C65993Lw(A01, replaceAll.substring(A01.length()), str2);
            }
        }
        return null;
    }

    public static String A0B(C22680zT r3, String str, String str2) {
        String str3;
        String replaceAll = str2.replaceAll("\\D", "");
        try {
            str3 = r3.A02(Integer.parseInt(str), replaceAll);
        } catch (IOException e) {
            Log.e("verify/number/trim/error", e);
            str3 = null;
        }
        return str3 != null ? A0E(str, replaceAll.substring(str.length())) : replaceAll;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0063, code lost:
        if (r5.equals(r4) != false) goto L_0x0065;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A0C(X.C22680zT r9, java.lang.String r10, java.lang.String r11, java.lang.String r12) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r12)
            r8 = 0
            if (r0 != 0) goto L_0x0059
            int r1 = r12.length()
            int r0 = r11.length()
            if (r1 < r0) goto L_0x0059
            java.lang.String r1 = "\\D"
            java.lang.String r0 = ""
            java.lang.String r5 = r12.replaceAll(r1, r0)
            java.lang.String r1 = r10.replaceAll(r1, r0)
            java.lang.String r6 = X.AnonymousClass1ZT.A00(r9, r11, r1)
            java.lang.String r4 = X.AnonymousClass1ZT.A00(r9, r11, r5)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r11)
            r0.append(r6)
            java.lang.String r7 = r0.toString()
            int r0 = A01(r6, r4)
            if (r0 != 0) goto L_0x005a
            boolean r0 = r4.equals(r6)
            if (r0 != 0) goto L_0x0059
            boolean r0 = r4.equals(r7)
            if (r0 != 0) goto L_0x0059
            boolean r0 = X.AnonymousClass1ZT.A02(r5, r6, r4, r11)
            if (r0 != 0) goto L_0x0059
            boolean r0 = r1.endsWith(r4)
            if (r0 == 0) goto L_0x005a
            int r1 = X.AbstractActivityC452520u.A03(r9, r11, r4)
            r0 = 5
            if (r1 != r0) goto L_0x005a
        L_0x0059:
            return r8
        L_0x005a:
            r3 = 1
            r2 = 0
            if (r4 == 0) goto L_0x0065
            boolean r0 = r5.equals(r4)
            r1 = 1
            if (r0 == 0) goto L_0x0066
        L_0x0065:
            r1 = 0
        L_0x0066:
            boolean r0 = A0K(r9, r5, r11, r7, r3)
            if (r0 == 0) goto L_0x006d
            return r5
        L_0x006d:
            if (r1 == 0) goto L_0x0076
            boolean r0 = A0K(r9, r4, r11, r7, r3)
            if (r0 == 0) goto L_0x0076
            return r4
        L_0x0076:
            boolean r0 = A0K(r9, r5, r11, r6, r2)
            if (r0 == 0) goto L_0x008c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r11)
            r0.append(r5)
        L_0x0087:
            java.lang.String r0 = r0.toString()
            return r0
        L_0x008c:
            if (r1 == 0) goto L_0x0059
            boolean r0 = A0K(r9, r4, r11, r6, r2)
            if (r0 == 0) goto L_0x0059
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r11)
            r0.append(r4)
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass23M.A0C(X.0zT, java.lang.String, java.lang.String, java.lang.String):java.lang.String");
    }

    public static String A0D(AnonymousClass01d r3, C16590pI r4, C15890o4 r5) {
        List<SubscriptionInfo> activeSubscriptionInfoList;
        if (!r5.A08()) {
            Log.i("verifynumber/getphonennumber/permission denied");
        } else {
            if (Build.VERSION.SDK_INT >= 22 && (activeSubscriptionInfoList = SubscriptionManager.from(r4.A00).getActiveSubscriptionInfoList()) != null) {
                for (SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                    String number = subscriptionInfo.getNumber();
                    if (number != null) {
                        return number;
                    }
                }
            }
            try {
                TelephonyManager A0N = r3.A0N();
                if (A0N != null) {
                    return A0N.getLine1Number();
                }
            } catch (Exception e) {
                Log.w("verifynumber/getphonennumber/error ", e);
                return null;
            }
        }
        return null;
    }

    public static String A0E(String str, String str2) {
        if (str == null || str2 == null) {
            Log.e("verifynumber/prettyprint/cc-or-phnum-is-null");
            return null;
        }
        StringBuilder sb = new StringBuilder("+");
        sb.append(str);
        sb.append(" ");
        sb.append(str2);
        String obj = sb.toString();
        C20920wX A002 = C20920wX.A00();
        try {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("+");
            sb2.append(str);
            sb2.append(str2);
            return A002.A0G(EnumC868649g.INTERNATIONAL, A002.A0E(sb2.toString(), "ZZ"));
        } catch (Exception e) {
            Log.e("verifynumber/formatter-exception", e);
            return obj;
        } catch (ExceptionInInitializerError e2) {
            Log.e("verifynumber/formatter-init-exception", e2);
            return obj;
        }
    }

    public static List A0F(C20920wX r6, AnonymousClass01d r7, C15890o4 r8) {
        C65993Lw A0A;
        List<SubscriptionInfo> activeSubscriptionInfoList;
        ArrayList arrayList = new ArrayList();
        if (!r8.A08()) {
            Log.i("verifynumber/getphonennumbers/permission denied");
        } else if (Build.VERSION.SDK_INT >= 22) {
            SubscriptionManager A0M = r7.A0M();
            if (!(A0M == null || (activeSubscriptionInfoList = A0M.getActiveSubscriptionInfoList()) == null)) {
                for (SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                    C65993Lw A0A2 = A0A(r6, subscriptionInfo.getNumber(), subscriptionInfo.getCarrierName().toString(), subscriptionInfo.getCountryIso());
                    if (A0A2 != null) {
                        arrayList.add(A0A2);
                    }
                }
            }
        } else {
            try {
                TelephonyManager A0N = r7.A0N();
                if (!(A0N == null || (A0A = A0A(r6, A0N.getLine1Number(), A0N.getNetworkOperatorName(), A0N.getSimCountryIso())) == null)) {
                    arrayList.add(A0A);
                    return arrayList;
                }
            } catch (Exception e) {
                Log.w("verifynumber/getphonennumbers/error ", e);
                return arrayList;
            }
        }
        return arrayList;
    }

    public static void A0G(Context context, C18360sK r10, int i) {
        Log.i("registrationutils/notify/unverified");
        long currentTimeMillis = System.currentTimeMillis();
        String string = context.getString(R.string.sms_notification_headline_unverified_app_name, context.getString(R.string.localized_app_name));
        String string2 = context.getString(R.string.sms_notification_title_unverified);
        String string3 = context.getString(R.string.sms_notification_message_unverified);
        Intent intent = new Intent(context, context.getClass());
        if (i != -1) {
            intent.putExtra("com.whatsapp.verifynumber.dialog", i);
        }
        intent.addFlags(536870912);
        PendingIntent A002 = AnonymousClass1UY.A00(context, 0, intent, 134217728);
        C005602s A003 = C22630zO.A00(context);
        A003.A0J = "critical_app_alerts@1";
        A003.A0B(string);
        A003.A05(currentTimeMillis);
        A003.A02(3);
        A003.A0D(true);
        A003.A0A(string2);
        A003.A09(string3);
        A003.A09 = A002;
        C18360sK.A01(A003, R.drawable.notifybar);
        r10.A03(1, A003.A01());
    }

    public static void A0H(Context context, C18360sK r11, C18350sJ r12, boolean z) {
        Intent intent;
        Log.i("registrationutils/notify/verified");
        long currentTimeMillis = System.currentTimeMillis();
        String string = context.getString(R.string.sms_notification_headline_verified_app_name, context.getString(R.string.localized_app_name));
        String string2 = context.getString(R.string.sms_notification_title_verified);
        String string3 = context.getString(R.string.sms_notification_message_verified);
        if (z) {
            intent = C14960mK.A04(context);
        } else {
            intent = new Intent();
            intent.setClassName(context.getPackageName(), "com.whatsapp.registration.RegisterName");
            r12.A0A(2);
        }
        PendingIntent A002 = AnonymousClass1UY.A00(context, 1, intent, 0);
        C005602s A003 = C22630zO.A00(context);
        A003.A0J = "other_notifications@1";
        A003.A0B(string);
        A003.A05(currentTimeMillis);
        A003.A02(3);
        A003.A0D(true);
        A003.A0A(string2);
        A003.A09(string3);
        A003.A09 = A002;
        C18360sK.A01(A003, R.drawable.notifybar);
        r11.A03(1, A003.A01());
    }

    public static void A0I(EditText editText, int i) {
        int length = editText.getText().length();
        if (i <= -1 || i > length) {
            if (i > length) {
                editText.requestFocus();
            }
            editText.setSelection(length);
            return;
        }
        editText.requestFocus();
        String obj = editText.getText().toString();
        int i2 = 0;
        for (int i3 = 0; i3 < obj.length() && i > 0; i3++) {
            if (obj.charAt(i3) <= '9' && obj.charAt(i3) >= '0') {
                i--;
            }
            i2++;
        }
        editText.setSelection(i2);
    }

    public static void A0J(C14820m6 r1, String str) {
        A00 = str;
        r1.A00.edit().putString("registration_failure_reason", str).apply();
    }

    public static boolean A0K(C22680zT r8, String str, String str2, String str3, boolean z) {
        int length;
        int length2;
        boolean z2;
        String substring;
        String substring2;
        if (TextUtils.isEmpty(str) || (length = str.length()) < (length2 = str2.length())) {
            return false;
        }
        int length3 = str3.length();
        int abs = Math.abs(length3 - length);
        if (abs == 1) {
            String str4 = str;
            if (length3 < length) {
                str4 = str3;
            }
            if (str4.equals(str3)) {
                str3 = str;
            }
            for (int i = 0; i < str4.length(); i++) {
                if (str4.charAt(i) != str3.charAt(i)) {
                    substring = str3.substring(i + 1);
                    substring2 = str4.substring(i);
                    z2 = substring.equals(substring2);
                    break;
                }
            }
            z2 = true;
        } else {
            if (abs == 0) {
                for (int i2 = 0; i2 < length3; i2++) {
                    if (str3.charAt(i2) != str.charAt(i2)) {
                        if (i2 != length3 - 1) {
                            int i3 = i2 + 1;
                            substring = str3.substring(i3);
                            substring2 = str.substring(i3);
                            z2 = substring.equals(substring2);
                            break;
                        }
                        z2 = true;
                    }
                }
            }
            z2 = false;
        }
        if (z) {
            if (!z2) {
                return false;
            }
            str = str.substring(length2);
        } else if (!z2) {
            return false;
        }
        if (AbstractActivityC452520u.A03(r8, str2, str) == 1) {
            return true;
        }
        return false;
    }
}
