package X;

import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;

/* renamed from: X.1vO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC42551vO implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ C42561vP A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC42551vO(C42561vP r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        int[] iArr = new int[2];
        C42561vP r3 = this.A00;
        View view = r3.A06;
        view.getLocationOnScreen(iArr);
        if (!r3.isShowing()) {
            r3.A05();
        } else if (r3.A01 != iArr[0]) {
            r3.A05();
            view.post(new RunnableBRunnable0Shape4S0100000_I0_4(this, 40));
        }
    }
}
