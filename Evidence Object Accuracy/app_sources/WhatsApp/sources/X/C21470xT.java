package X;

import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;

/* renamed from: X.0xT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21470xT {
    public boolean A00 = false;
    public boolean A01 = true;
    public final C14900mE A02;
    public final C14820m6 A03;
    public final C17260qW A04;
    public final C14850m9 A05;

    public C21470xT(C14900mE r2, C14820m6 r3, C17260qW r4, C14850m9 r5) {
        this.A05 = r5;
        this.A02 = r2;
        this.A04 = r4;
        this.A03 = r3;
    }

    public boolean A00() {
        this.A02.A0I(new RunnableBRunnable0Shape6S0100000_I0_6(this, 1));
        if (!this.A05.A07(1624)) {
            return true;
        }
        SharedPreferences sharedPreferences = this.A03.A00;
        return sharedPreferences.contains("tos_2016_opt_out_state") && !sharedPreferences.getBoolean("tos_2016_opt_out_state", false);
    }
}
