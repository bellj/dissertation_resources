package X;

/* renamed from: X.0jB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13120jB {
    public static C13080j7 A00(String str, String str2) {
        AnonymousClass00S r3 = new AnonymousClass00S(str, str2);
        C13090j8 r1 = new C13090j8(AnonymousClass00P.class, new Class[0]);
        r1.A01 = 1;
        r1.A02 = new AbstractC13110jA(r3) { // from class: X.0jV
            public final Object A00;

            {
                this.A00 = r1;
            }

            @Override // X.AbstractC13110jA
            public Object A80(AbstractC13170jG r2) {
                return this.A00;
            }
        };
        return r1.A00();
    }
}
