package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.1aG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31261aG {
    public static final byte[] A03 = {2};
    public static final byte[] A04 = {1};
    public final int A00;
    public final AbstractC31231aD A01;
    public final byte[] A02;

    public C31261aG(AbstractC31231aD r1, byte[] bArr, int i) {
        this.A01 = r1;
        this.A02 = bArr;
        this.A00 = i;
    }

    public C31831bB A00() {
        C91174Qs r0 = new C91174Qs(this.A01.A02(A01(A04), new byte[32], "WhisperMessageKeys".getBytes(), 80));
        return new C31831bB(r0.A00, r0.A01, r0.A02, this.A00);
    }

    public final byte[] A01(byte[] bArr) {
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            instance.init(new SecretKeySpec(this.A02, DefaultCrypto.HMAC_SHA256));
            return instance.doFinal(bArr);
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
