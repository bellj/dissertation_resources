package X;

/* renamed from: X.1TU  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1TU {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A;
    public static final AnonymousClass1TK A0B;
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;

    static {
        AnonymousClass1TK r2 = new AnonymousClass1TK("1.3.6.1");
        A08 = r2;
        A02 = new AnonymousClass1TK("1", r2);
        A0C = new AnonymousClass1TK("2", r2);
        A03 = new AnonymousClass1TK("3", r2);
        A01 = new AnonymousClass1TK("4", r2);
        AnonymousClass1TK r4 = new AnonymousClass1TK("5", r2);
        A0E = r4;
        A00 = new AnonymousClass1TK("6", r2);
        A0B = new AnonymousClass1TK("7", r2);
        AnonymousClass1TK r22 = new AnonymousClass1TK("5", r4);
        A0F = r22;
        A0G = new AnonymousClass1TK("6", r4);
        A0D = new AnonymousClass1TK("6", r22);
        AnonymousClass1TK r0 = new AnonymousClass1TK("8", r22);
        A09 = r0;
        AnonymousClass1TK r1 = new AnonymousClass1TK("1", r0);
        A0A = r1;
        A04 = new AnonymousClass1TK("1", r1);
        A06 = new AnonymousClass1TK("2", r1);
        A07 = new AnonymousClass1TK("3", r1);
        A05 = new AnonymousClass1TK("4", r1);
    }
}
