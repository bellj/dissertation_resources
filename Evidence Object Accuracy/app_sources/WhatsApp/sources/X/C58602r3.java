package X;

import android.transition.Transition;
import com.whatsapp.profile.ProfileInfoActivity;

/* renamed from: X.2r3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58602r3 extends AbstractC100714mM {
    public final /* synthetic */ ProfileInfoActivity A00;

    public C58602r3(ProfileInfoActivity profileInfoActivity) {
        this.A00 = profileInfoActivity;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
        ProfileInfoActivity profileInfoActivity = this.A00;
        profileInfoActivity.A01.setScaleX(1.0f);
        profileInfoActivity.A01.setScaleY(1.0f);
        profileInfoActivity.A01.animate().scaleX(0.0f).scaleY(0.0f).setDuration(125);
    }
}
