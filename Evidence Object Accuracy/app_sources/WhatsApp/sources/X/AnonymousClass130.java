package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.SparseArray;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.130  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass130 {
    public final AnonymousClass169 A00;
    public final C16590pI A01;
    public final C19990v2 A02;
    public final C14850m9 A03;

    public AnonymousClass130(AnonymousClass169 r1, C16590pI r2, C19990v2 r3, C14850m9 r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public static Bitmap A00(Context context, float f, int i, int i2) {
        Drawable drawable = context.getResources().getDrawable(i);
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, Bitmap.Config.ARGB_8888);
        int A00 = AnonymousClass00T.A00(context, R.color.primary_surface);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, i2, i2);
        if (drawable instanceof BitmapDrawable) {
            Paint paint = new Paint();
            float f2 = (float) i2;
            RectF rectF = new RectF(0.0f, 0.0f, f2, f2);
            paint.setAntiAlias(true);
            paint.setDither(true);
            paint.setFilterBitmap(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(A00);
            if (f >= 0.0f) {
                canvas.drawRoundRect(rectF, f, f, paint);
            } else {
                canvas.drawArc(rectF, 0.0f, 360.0f, true, paint);
            }
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
            canvas.drawBitmap(((BitmapDrawable) drawable).getBitmap(), (Rect) null, rectF, paint);
            return createBitmap;
        }
        drawable.draw(canvas);
        return createBitmap;
    }

    public int A01(C15370n3 r2) {
        return A02((AbstractC14640lm) r2.A0B(AbstractC14640lm.class));
    }

    public int A02(AbstractC14640lm r6) {
        if (C15380n4.A0M(r6)) {
            return R.drawable.avatar_server_psa;
        }
        if (C15380n4.A0N(r6)) {
            return R.drawable.avatar_status;
        }
        if (C15380n4.A0F(r6)) {
            return R.drawable.avatar_broadcast;
        }
        if (!C15380n4.A0J(r6)) {
            return R.drawable.avatar_contact;
        }
        C14850m9 r4 = this.A03;
        if (r4.A07(982) && this.A02.A02(C15580nU.A02(r6)) == 1) {
            return R.drawable.avatar_parent_large;
        }
        if (!r4.A07(982) || this.A02.A02(C15580nU.A02(r6)) != 3) {
            return R.drawable.avatar_group;
        }
        return R.drawable.avatar_announcement;
    }

    public Bitmap A03(Context context, int i) {
        Bitmap bitmap;
        AnonymousClass169 r4 = this.A00;
        synchronized (r4) {
            if (C41691tw.A08(context) != r4.A00) {
                r4.A01.clear();
                boolean z = false;
                if (!r4.A00) {
                    z = true;
                }
                r4.A00 = z;
            }
            SparseArray sparseArray = r4.A01;
            bitmap = (Bitmap) sparseArray.get(i);
            if (bitmap == null) {
                bitmap = A00(context, context.getResources().getDimension(R.dimen.small_avatar_radius), i, context.getResources().getDimensionPixelSize(R.dimen.small_avatar_size));
                sparseArray.put(i, bitmap);
            }
        }
        return bitmap;
    }

    public Bitmap A04(C15370n3 r4, float f, int i) {
        Bitmap decodeResource;
        if (Build.VERSION.SDK_INT >= 21) {
            int i2 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
            int A01 = A01(r4);
            Context context = this.A01.A00;
            if (i2 >= 0) {
                decodeResource = A00(context, f, A01, i);
            } else {
                decodeResource = A03(context, A01);
            }
        } else {
            decodeResource = BitmapFactory.decodeResource(this.A01.A00.getResources(), A01(r4));
        }
        return i != 0 ? Bitmap.createScaledBitmap(decodeResource, i, i, true) : decodeResource;
    }

    public void A05(ImageView imageView, int i) {
        imageView.setImageBitmap(A03(imageView.getContext(), i));
    }

    public void A06(ImageView imageView, C15370n3 r3) {
        A05(imageView, A02((AbstractC14640lm) r3.A0B(AbstractC14640lm.class)));
    }
}
