package X;

import android.app.Activity;
import android.content.Context;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.5oh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124155oh extends AbstractC16350or {
    public final Activity A00;
    public final AnonymousClass1IR A01;
    public final AnonymousClass5UT A02;
    public final /* synthetic */ AnonymousClass697 A03;

    public /* synthetic */ C124155oh(Activity activity, AnonymousClass1IR r2, AnonymousClass697 r3, AnonymousClass5UT r4) {
        this.A03 = r3;
        this.A01 = r2;
        this.A02 = r4;
        this.A00 = activity;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        List A03 = AbstractC28901Pl.A03(C17930rd.A0E, C117295Zj.A0Z(this.A03.A0A));
        if (A03.size() > 0) {
            return C12980iv.A0o(A03);
        }
        return null;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C126455su r8;
        AnonymousClass5UT r1;
        AbstractC28901Pl r3 = (AbstractC28901Pl) obj;
        if (r3 == null && (r1 = this.A02) != null) {
            r1.AUq(C117305Zk.A0L());
        }
        AnonymousClass697 r12 = this.A03;
        Context context = r12.A01.A00;
        C14850m9 r82 = r12.A04;
        C120555gN r5 = new C120555gN(context, r12.A00, r82, r12.A05, r12.A07, r12.A08, r12.A09, r12.A0C);
        C1329768z r15 = new AnonymousClass5UT() { // from class: X.68z
            @Override // X.AnonymousClass5UT
            public final void AUq(C452120p r52) {
                int i;
                C124155oh r32 = C124155oh.this;
                if (r52 == null || (i = r52.A00) != 11495) {
                    AnonymousClass5UT r0 = r32.A02;
                    if (r0 != null) {
                        r0.AUq(r52);
                        return;
                    }
                    return;
                }
                Log.i(C12960it.A0W(i, "PAY: reject collect; error code: "));
                AnonymousClass697 r2 = r32.A03;
                r2.A0D.Ab2(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001c: INVOKE  
                      (wrap: X.0lR : 0x0015: IGET  (r1v1 X.0lR A[REMOVE]) = (r2v0 'r2' X.697) X.697.A0D X.0lR)
                      (wrap: X.6FQ : 0x0019: CONSTRUCTOR  (r0v4 X.6FQ A[REMOVE]) = (r3v0 'r32' X.5oh) call: X.6FQ.<init>(X.5oh):void type: CONSTRUCTOR)
                     type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.68z.AUq(X.20p):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0019: CONSTRUCTOR  (r0v4 X.6FQ A[REMOVE]) = (r3v0 'r32' X.5oh) call: X.6FQ.<init>(X.5oh):void type: CONSTRUCTOR in method: X.68z.AUq(X.20p):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 19 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6FQ, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 25 more
                    */
                /*
                    this = this;
                    X.5oh r3 = X.C124155oh.this
                    if (r5 == 0) goto L_0x002c
                    int r1 = r5.A00
                    r0 = 11495(0x2ce7, float:1.6108E-41)
                    if (r1 != r0) goto L_0x002c
                    java.lang.String r0 = "PAY: reject collect; error code: "
                    java.lang.String r0 = X.C12960it.A0W(r1, r0)
                    com.whatsapp.util.Log.i(r0)
                    X.697 r2 = r3.A03
                    X.0lR r1 = r2.A0D
                    X.6FQ r0 = new X.6FQ
                    r0.<init>(r3)
                    r1.Ab2(r0)
                    X.0mE r0 = r2.A00
                    r0.A03()
                    android.app.Activity r1 = r3.A00
                    r0 = 100
                    X.C36021jC.A01(r1, r0)
                L_0x002b:
                    return
                L_0x002c:
                    X.5UT r0 = r3.A02
                    if (r0 == 0) goto L_0x002b
                    r0.AUq(r5)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.C1329768z.AUq(X.20p):void");
            }
        };
        AnonymousClass1IR r2 = this.A01;
        AbstractC30891Zf r13 = r2.A0A;
        AnonymousClass009.A05(r13);
        C119835fB r14 = (C119835fB) r13;
        AnonymousClass1ZY r0 = r3.A08;
        AnonymousClass009.A05(r0);
        C119755f3 r02 = (C119755f3) r0;
        String str = r2.A0K;
        C30821Yy r4 = r2.A08;
        if (r4 == null) {
            r4 = null;
        }
        String str2 = r14.A0L;
        String str3 = r14.A0M;
        String str4 = r14.A0J;
        AnonymousClass1ZR r32 = r02.A06;
        Log.i("PAY: rejectCollect called");
        C17220qS r16 = r5.A03;
        String A01 = r16.A01();
        String A012 = r5.A05.A01();
        String str5 = (String) C117295Zj.A0R(r32);
        if (r4 != null) {
            r8 = new C126455su(C120555gN.A00(((C126705tJ) r5).A01.A00(C30771Yt.A05, r4)));
        } else {
            r8 = null;
        }
        C126445st r6 = new C126445st(new AnonymousClass3CT(A01), r8, str, A012, str2, str3, str5, str4);
        C64513Fv r42 = ((C126705tJ) r5).A00;
        if (r42 != null) {
            r42.A04("upi-reject-collect");
        }
        r16.A0D(new C120675gZ(r5.A00, r5.A01, r15, r5.A04, r42, r5), r6.A00, A01, 204, 0);
    }
}
