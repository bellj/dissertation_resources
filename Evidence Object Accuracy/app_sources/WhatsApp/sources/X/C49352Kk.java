package X;

import android.content.Context;
import android.os.Parcel;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.2Kk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49352Kk {
    public static final Object A00 = new Object();
    public static final byte[] A01 = {0, 2};

    public static void A00(Context context, C14830m7 r12, C14820m6 r13, String str, byte[] bArr) {
        byte[] bArr2;
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass029.A0M);
        sb.append(str.substring(Math.max(str.length() - 4, 0)));
        String obj = sb.toString();
        byte[] bArr3 = A01;
        try {
            byte[] A0D = C003501n.A0D(4);
            byte[] A0D2 = C003501n.A0D(16);
            SecretKeySpec secretKeySpec = new SecretKeySpec(new SecretKeySpec(C003501n.A06(obj.getBytes(), A0D, 16, 128).getEncoded(), "AES").getEncoded(), "AES/OFB/NoPadding");
            Cipher instance = Cipher.getInstance("AES/OFB/NoPadding");
            instance.init(1, secretKeySpec, new IvParameterSpec(A0D2));
            byte[] doFinal = instance.doFinal(bArr);
            bArr2 = new byte[bArr3.length + A0D.length + A0D2.length + doFinal.length];
            byte[][] bArr4 = {bArr3, A0D, A0D2, doFinal};
            int i = 0;
            int i2 = 0;
            while (true) {
                byte[] bArr5 = bArr4[i];
                System.arraycopy(bArr5, 0, bArr2, i2, bArr5.length);
                i2 += bArr5.length;
                i++;
                if (i >= 4) {
                    break;
                }
            }
        } catch (Exception e) {
            Log.e("BackupTokenUtils/encrypt/unable to encrypt", e);
            bArr2 = null;
        }
        A02(context, bArr2);
        r13.A0q("backup_token_file_timestamp", r12.A00());
    }

    public static void A01(Context context, byte[] bArr) {
        C56292ke r5 = new C56292ke(context);
        AnonymousClass4MF r4 = new AnonymousClass4MF();
        AnonymousClass3H0 r3 = new AnonymousClass3H0(null);
        r3.A03 = new C78603pB[]{C88854Hj.A04};
        r3.A01 = new AnonymousClass5SY() { // from class: X.4ym
            @Override // X.AnonymousClass5SY
            public final void A5Y(Object obj, Object obj2) {
                BinderC79033pw r32 = new BinderC79033pw((C13690kA) obj2);
                Parcel obtain = Parcel.obtain();
                obtain.writeInterfaceToken("com.google.android.gms.auth.blockstore.internal.IBlockstoreService");
                obtain.writeStrongBinder(r32);
                ((C98354iY) ((AbstractC95064d1) obj).A03()).A00(11, obtain);
            }
        };
        r3.A02 = false;
        r3.A00 = 1651;
        C13600jz A012 = r5.A01(r3.A00(), 0);
        AnonymousClass3TT r0 = new AbstractC13640k3(r5, r4, bArr) { // from class: X.3TT
            public final /* synthetic */ AnonymousClass5Yi A00;
            public final /* synthetic */ AnonymousClass4MF A01;
            public final /* synthetic */ byte[] A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AbstractC13640k3
            public final void AX4(Object obj) {
                AnonymousClass4MF r2 = this.A01;
                byte[] bArr2 = this.A02;
                AnonymousClass5Yi r6 = this.A00;
                Boolean bool = (Boolean) obj;
                r2.A00 = bool.booleanValue();
                Log.i(C12960it.A0b("BackupTokenUtils/setBlockStoreBytes/isE2EEAvailable ", bool));
                r2.A01 = bArr2;
                C78043oH r52 = new C78043oH(bArr2, r2.A00);
                C56292ke r62 = (C56292ke) r6;
                AnonymousClass3H0 r42 = new AnonymousClass3H0(null);
                r42.A03 = new C78603pB[]{C88854Hj.A03, C88854Hj.A05};
                r42.A01 = 
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003c: IPUT  
                      (wrap: X.4yo : 0x0039: CONSTRUCTOR  (r0v8 X.4yo A[REMOVE]) = (r5v0 'r52' X.3oH), (r6v1 'r62' X.2ke) call: X.4yo.<init>(X.3oH, X.2ke):void type: CONSTRUCTOR)
                      (r4v0 'r42' X.3H0)
                     X.3H0.A01 X.5SY in method: X.3TT.AX4(java.lang.Object):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0039: CONSTRUCTOR  (r0v8 X.4yo A[REMOVE]) = (r5v0 'r52' X.3oH), (r6v1 'r62' X.2ke) call: X.4yo.<init>(X.3oH, X.2ke):void type: CONSTRUCTOR in method: X.3TT.AX4(java.lang.Object):void, file: classes2.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:459)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.4yo, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 19 more
                    */
                /*
                    this = this;
                    X.4MF r2 = r7.A01
                    byte[] r1 = r7.A02
                    X.5Yi r6 = r7.A00
                    java.lang.Boolean r8 = (java.lang.Boolean) r8
                    boolean r0 = r8.booleanValue()
                    r2.A00 = r0
                    java.lang.String r0 = "BackupTokenUtils/setBlockStoreBytes/isE2EEAvailable "
                    java.lang.String r0 = X.C12960it.A0b(r0, r8)
                    com.whatsapp.util.Log.i(r0)
                    r2.A01 = r1
                    boolean r0 = r2.A00
                    X.3oH r5 = new X.3oH
                    r5.<init>(r1, r0)
                    X.2ke r6 = (X.C56292ke) r6
                    r0 = 0
                    X.3H0 r4 = new X.3H0
                    r4.<init>(r0)
                    r0 = 2
                    X.3pB[] r3 = new X.C78603pB[r0]
                    X.3pB r0 = X.C88854Hj.A03
                    r2 = 0
                    r3[r2] = r0
                    X.3pB r0 = X.C88854Hj.A05
                    r1 = 1
                    r3[r1] = r0
                    r4.A03 = r3
                    X.4yo r0 = new X.4yo
                    r0.<init>(r5, r6)
                    r4.A01 = r0
                    r0 = 1645(0x66d, float:2.305E-42)
                    r4.A00 = r0
                    r4.A02 = r2
                    X.4b8 r0 = r4.A00()
                    X.0jz r2 = r6.A01(r0, r1)
                    X.50z r0 = new X.50z
                    r0.<init>()
                    java.util.concurrent.Executor r1 = X.C13650k5.A00
                    r2.A06(r0, r1)
                    X.50w r0 = new X.50w
                    r0.<init>()
                    r2.A05(r0, r1)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3TT.AX4(java.lang.Object):void");
            }
        };
        Executor executor = C13650k5.A00;
        A012.A06(r0, executor);
        A012.A05(new AbstractC13630k2() { // from class: X.50v
            @Override // X.AbstractC13630k2
            public final void AQA(Exception exc) {
                Log.e("BackupTokenUtils/setBlockStoreBytes/exception determining E2EE", exc);
            }
        }, executor);
    }

    public static void A02(Context context, byte[] bArr) {
        if (C28391Mz.A02()) {
            A01(context, bArr);
        }
        synchronized (A00) {
            try {
                C003501n.A08(new File(context.getFilesDir(), "backup_token"), bArr);
            } catch (IOException e) {
                Log.e("BackupTokenUtils/setEncodedBackupToken/unable to write to file", e);
            }
        }
    }

    public static byte[] A03(Context context) {
        byte[] A0G;
        File file = new File(context.getFilesDir(), "backup_token");
        if (!file.exists()) {
            return null;
        }
        synchronized (A00) {
            A0G = C003501n.A0G(file);
        }
        return A0G;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r6.length == 0) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] A04(android.content.Context r10, java.lang.String r11) {
        /*
            byte[] r6 = A03(r10)
            if (r6 == 0) goto L_0x000a
            int r1 = r6.length
            r0 = 1
            if (r1 != 0) goto L_0x000b
        L_0x000a:
            r0 = 0
        L_0x000b:
            if (r0 != 0) goto L_0x000f
            r11 = 0
        L_0x000e:
            return r11
        L_0x000f:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r0 = X.AnonymousClass029.A0M
            r2.append(r0)
            int r0 = r11.length()
            int r1 = r0 + -4
            r0 = 0
            int r0 = java.lang.Math.max(r1, r0)
            java.lang.String r0 = r11.substring(r0)
            r2.append(r0)
            java.lang.String r10 = r2.toString()
            byte[] r9 = X.C49352Kk.A01
            java.lang.String r8 = "AES/OFB/NoPadding"
            r11 = 0
            if (r6 == 0) goto L_0x000e
            int r7 = r9.length     // Catch: Exception -> 0x00b2
            r4 = 4
            int r0 = r7 + r4
            r3 = 16
            int r0 = r0 + r3
            int r2 = r0 + 20
            int r5 = r6.length     // Catch: Exception -> 0x00b2
            if (r5 < r2) goto L_0x0092
            byte[] r0 = new byte[r7]     // Catch: Exception -> 0x00b2
            r1 = 0
            java.lang.System.arraycopy(r6, r1, r0, r1, r7)     // Catch: Exception -> 0x00b2
            int r7 = r7 + r1
            boolean r0 = java.util.Arrays.equals(r0, r9)     // Catch: Exception -> 0x00b2
            if (r0 == 0) goto L_0x008c
            byte[] r2 = new byte[r4]     // Catch: Exception -> 0x00b2
            java.lang.System.arraycopy(r6, r7, r2, r1, r4)     // Catch: Exception -> 0x00b2
            int r7 = r7 + r4
            byte[] r4 = new byte[r3]     // Catch: Exception -> 0x00b2
            java.lang.System.arraycopy(r6, r7, r4, r1, r3)     // Catch: Exception -> 0x00b2
            int r7 = r7 + r3
            byte[] r1 = r10.getBytes()     // Catch: Exception -> 0x00b2
            r0 = 128(0x80, float:1.794E-43)
            javax.crypto.SecretKey r0 = X.C003501n.A06(r1, r2, r3, r0)     // Catch: Exception -> 0x00b2
            byte[] r2 = r0.getEncoded()     // Catch: Exception -> 0x00b2
            java.lang.String r1 = "AES"
            javax.crypto.spec.SecretKeySpec r0 = new javax.crypto.spec.SecretKeySpec     // Catch: Exception -> 0x00b2
            r0.<init>(r2, r1)     // Catch: Exception -> 0x00b2
            byte[] r0 = r0.getEncoded()     // Catch: Exception -> 0x00b2
            javax.crypto.spec.SecretKeySpec r3 = new javax.crypto.spec.SecretKeySpec     // Catch: Exception -> 0x00b2
            r3.<init>(r0, r8)     // Catch: Exception -> 0x00b2
            javax.crypto.Cipher r2 = javax.crypto.Cipher.getInstance(r8)     // Catch: Exception -> 0x00b2
            r1 = 2
            javax.crypto.spec.IvParameterSpec r0 = new javax.crypto.spec.IvParameterSpec     // Catch: Exception -> 0x00b2
            r0.<init>(r4)     // Catch: Exception -> 0x00b2
            r2.init(r1, r3, r0)     // Catch: Exception -> 0x00b2
            int r5 = r5 - r7
            byte[] r11 = r2.doFinal(r6, r7, r5)     // Catch: Exception -> 0x00b2
            return r11
        L_0x008c:
            X.03G r0 = new X.03G     // Catch: Exception -> 0x00b2
            r0.<init>()     // Catch: Exception -> 0x00b2
            throw r0     // Catch: Exception -> 0x00b2
        L_0x0092:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: Exception -> 0x00b2
            r1.<init>()     // Catch: Exception -> 0x00b2
            java.lang.String r0 = "size mismatch expected length:"
            r1.append(r0)     // Catch: Exception -> 0x00b2
            r1.append(r2)     // Catch: Exception -> 0x00b2
            java.lang.String r0 = ", actual length:"
            r1.append(r0)     // Catch: Exception -> 0x00b2
            r1.append(r5)     // Catch: Exception -> 0x00b2
            java.lang.String r1 = r1.toString()     // Catch: Exception -> 0x00b2
            java.security.InvalidParameterException r0 = new java.security.InvalidParameterException     // Catch: Exception -> 0x00b2
            r0.<init>(r1)     // Catch: Exception -> 0x00b2
            throw r0     // Catch: Exception -> 0x00b2
        L_0x00b2:
            r1 = move-exception
            java.lang.String r0 = "BackupTokenUtils/decrypt/unable to decrypt"
            com.whatsapp.util.Log.e(r0, r1)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49352Kk.A04(android.content.Context, java.lang.String):byte[]");
    }
}
