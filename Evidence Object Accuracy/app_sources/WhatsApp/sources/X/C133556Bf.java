package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.stepup.NoviPayStepUpBloksActivity;
import com.whatsapp.util.Log;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.6Bf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133556Bf implements AnonymousClass6MX {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ NoviPayStepUpBloksActivity A01;
    public final /* synthetic */ Map A02;

    @Override // X.AnonymousClass6MX
    public void AY5(int i) {
    }

    public C133556Bf(AnonymousClass3FE r1, NoviPayStepUpBloksActivity noviPayStepUpBloksActivity, Map map) {
        this.A01 = noviPayStepUpBloksActivity;
        this.A00 = r1;
        this.A02 = map;
    }

    @Override // X.AnonymousClass6MX
    public void AY6(C452120p r12, String str, String str2, boolean z) {
        if (!z || str == null) {
            NoviPayStepUpBloksActivity noviPayStepUpBloksActivity = this.A01;
            C004802e A0S = C12980iv.A0S(noviPayStepUpBloksActivity);
            C117305Zk.A17(A0S, C117295Zj.A0S(noviPayStepUpBloksActivity, A0S, R.string.novi_bloks_doc_upload_failed_message), this.A00, 77);
            return;
        }
        NoviPayStepUpBloksActivity noviPayStepUpBloksActivity2 = this.A01;
        AnonymousClass3FE r6 = this.A00;
        Map map = this.A02;
        AnonymousClass61S[] r3 = new AnonymousClass61S[2];
        C1316663q r2 = noviPayStepUpBloksActivity2.A06;
        AnonymousClass61S.A04("entry_flow", r2.A03, r3);
        C1310460z A0B = C117315Zl.A0B("step_up", C12960it.A0m(AnonymousClass61S.A00("metadata", r2.A04), r3, 1));
        C1310460z A0B2 = C117315Zl.A0B("document", AnonymousClass61S.A02("id", str));
        C1310460z A01 = AnonymousClass61S.A01("novi-answer-rekyc-step-up-challenge");
        ArrayList arrayList = A01.A02;
        arrayList.add(A0B);
        arrayList.add(A0B2);
        KeyPair A02 = ((AbstractActivityC123635nW) noviPayStepUpBloksActivity2).A06.A02();
        if (A02 != null && ((ActivityC13810kN) noviPayStepUpBloksActivity2).A0C.A07(822)) {
            long A022 = C117315Zl.A02(noviPayStepUpBloksActivity2);
            String A0n = C12990iw.A0n();
            AnonymousClass61C r22 = ((AbstractActivityC123635nW) noviPayStepUpBloksActivity2).A0C;
            JSONObject A04 = r22.A04(A022);
            AnonymousClass61C.A01(A0n, A04);
            try {
                A04.put("document_id", str);
            } catch (JSONException unused) {
                Log.e("PAY: IntentPayloadHelper/getUploadKycDocumentIntent/toJson can't construct json");
            }
            String A012 = new C129585xx(r22.A04, "UPLOAD_KYC_DOCUMENT", A04).A01(A02);
            AnonymousClass009.A05(A012);
            C117305Zk.A1K(A01, "answer_rekyc_stepup_signed_intent", AnonymousClass61S.A02("answer_rekyc_stepup_signed_intent", A012));
        }
        String A0t = C12970iu.A0t("challenge_id", map);
        if (!TextUtils.isEmpty(A0t)) {
            C117305Zk.A1R("step_up_challenge", arrayList, AnonymousClass61S.A02("challenge_id", A0t));
        }
        C130155yt.A04(C117305Zk.A09(r6, noviPayStepUpBloksActivity2, 41), ((AbstractActivityC123635nW) noviPayStepUpBloksActivity2).A04, A01, noviPayStepUpBloksActivity2.A00, 4);
    }
}
