package X;

import com.facebook.redex.EmptyBaseRunnable0;
import java.io.IOException;

/* renamed from: X.1OM  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass1OM extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ AnonymousClass1OI A00;
    public final /* synthetic */ C15820nx A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ boolean A03;
    public final /* synthetic */ byte[] A04;

    public /* synthetic */ AnonymousClass1OM(AnonymousClass1OI r1, C15820nx r2, String str, byte[] bArr, boolean z) {
        this.A01 = r2;
        this.A04 = bArr;
        this.A03 = z;
        this.A02 = str;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C15820nx r4 = this.A01;
        byte[] bArr = this.A04;
        boolean z = this.A03;
        String str = this.A02;
        AnonymousClass1OI r5 = this.A00;
        try {
            r4.A01.A02(bArr);
            C14820m6 r1 = r4.A03;
            r1.A14(true);
            r1.A15(z);
            if (!z && str != null) {
                r4.A03(str);
            }
            r5.AWu();
        } catch (IOException unused) {
            r5.APs("Failed to store root key", 6, 4, -1, 0);
        }
    }
}
