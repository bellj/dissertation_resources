package X;

import com.whatsapp.R;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;

/* renamed from: X.3Vx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68573Vx implements AbstractC116575Vz {
    public final /* synthetic */ DirectorySetLocationMapActivity A00;

    public C68573Vx(DirectorySetLocationMapActivity directorySetLocationMapActivity) {
        this.A00 = directorySetLocationMapActivity;
    }

    @Override // X.AbstractC116575Vz
    public void AQs(int i) {
        DirectorySetLocationMapActivity directorySetLocationMapActivity = this.A00;
        directorySetLocationMapActivity.A08.A06.setVisibility(8);
        AnonymousClass3LU r3 = directorySetLocationMapActivity.A08;
        r3.A0C = null;
        r3.A05.setText(R.string.biz_dir_pick_location_on_map_address_fallback);
        C12960it.A0s(r3.A07, r3.A05, R.color.hint_text);
    }

    @Override // X.AbstractC116575Vz
    public void AQt(String str) {
        DirectorySetLocationMapActivity directorySetLocationMapActivity = this.A00;
        directorySetLocationMapActivity.A08.A06.setVisibility(8);
        directorySetLocationMapActivity.A08.A02(str);
    }
}
