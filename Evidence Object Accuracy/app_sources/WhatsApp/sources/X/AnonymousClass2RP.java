package X;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: X.2RP  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2RP implements AnonymousClass2RQ {
    public final int A00;
    public final Context A01;
    public final Looper A02;
    public final AbstractC116855Xe A03;
    public final AnonymousClass1UE A04;
    public final AnonymousClass1U8 A05;
    public final AnonymousClass4XF A06;
    public final C65863Lh A07;
    public final AnonymousClass5QZ A08;
    public final String A09;

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v12, types: [X.01E, com.google.android.gms.common.api.internal.zzd] */
    /* JADX WARN: Type inference failed for: r4v14, types: [X.01E, com.google.android.gms.common.api.internal.zzd] */
    /* JADX WARN: Type inference failed for: r0v40, types: [X.02f] */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x008e, code lost:
        if (r4 == null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00be, code lost:
        if (r4 == null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00d0, code lost:
        if (r0 != false) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0102, code lost:
        if (r0 != false) goto L_0x0104;
     */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2RP(android.app.Activity r9, android.content.Context r10, X.AbstractC116855Xe r11, X.AnonymousClass1UE r12, X.C93404a7 r13) {
        /*
        // Method dump skipped, instructions count: 326
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2RP.<init>(android.app.Activity, android.content.Context, X.5Xe, X.1UE, X.4a7):void");
    }

    @Deprecated
    public AnonymousClass2RP(Context context, AbstractC116855Xe r8, AnonymousClass1UE r9, AnonymousClass5QZ r10) {
        this(null, context, r8, r9, new C93404a7(Looper.getMainLooper(), r10));
    }

    public AnonymousClass4RB A00() {
        AnonymousClass4RB r3 = new AnonymousClass4RB();
        Set emptySet = Collections.emptySet();
        AnonymousClass01b r1 = r3.A00;
        if (r1 == null) {
            r1 = new AnonymousClass01b(0);
            r3.A00 = r1;
        }
        r1.addAll(emptySet);
        Context context = this.A01;
        r3.A03 = context.getClass().getName();
        r3.A02 = context.getPackageName();
        return r3;
    }

    public final C13600jz A01(AbstractC94024b8 r15, int i) {
        long currentTimeMillis;
        long elapsedRealtime;
        C13690kA r4 = new C13690kA();
        C65863Lh r8 = this.A07;
        AnonymousClass5QZ r3 = this.A08;
        int i2 = r15.A00;
        if (i2 != 0) {
            AnonymousClass4XF r7 = this.A06;
            if (r8.A06()) {
                C56412kq r1 = C94784cX.A00().A00;
                if (r1 != null) {
                    if (r1.A03) {
                        boolean z = r1.A04;
                        C14970mL r2 = (C14970mL) r8.A09.get(r7);
                        if (r2 != null) {
                            AbstractC72443eb r12 = r2.A04;
                            if (r12 instanceof AbstractC95064d1) {
                                AbstractC95064d1 r13 = (AbstractC95064d1) r12;
                                if (r13.A0Q != null && !r13.AJG()) {
                                    C56422kr A00 = AnonymousClass3TQ.A00(r2, r13, i2);
                                    if (A00 != null) {
                                        r2.A00++;
                                        z = A00.A03;
                                    }
                                }
                            }
                        }
                        if (!z) {
                            currentTimeMillis = 0;
                            elapsedRealtime = 0;
                            AnonymousClass3TQ r6 = new AnonymousClass3TQ(r7, r8, i2, currentTimeMillis, elapsedRealtime);
                            C13600jz r5 = r4.A00;
                            r5.A03.A00(new AnonymousClass514(r6, new Executor(r8.A06) { // from class: X.5E2
                                public final /* synthetic */ Handler A00;

                                {
                                    this.A00 = r1;
                                }

                                @Override // java.util.concurrent.Executor
                                public final void execute(Runnable runnable) {
                                    this.A00.post(runnable);
                                }
                            }));
                            r5.A04();
                        }
                    }
                }
                currentTimeMillis = System.currentTimeMillis();
                elapsedRealtime = SystemClock.elapsedRealtime();
                AnonymousClass3TQ r6 = new AnonymousClass3TQ(r7, r8, i2, currentTimeMillis, elapsedRealtime);
                C13600jz r5 = r4.A00;
                r5.A03.A00(new AnonymousClass514(r6, new Executor(r8.A06) { // from class: X.5E2
                    public final /* synthetic */ Handler A00;

                    {
                        this.A00 = r1;
                    }

                    @Override // java.util.concurrent.Executor
                    public final void execute(Runnable runnable) {
                        this.A00.post(runnable);
                    }
                }));
                r5.A04();
            }
        }
        C56342kj r52 = new C56342kj(r3, r15, r4, i);
        Handler handler = r8.A06;
        handler.sendMessage(handler.obtainMessage(4, new AnonymousClass4PI(this, r52, r8.A0D.get())));
        return r4.A00;
    }

    public final void A02(AnonymousClass1UI r5, int i) {
        r5.A04();
        C65863Lh r0 = this.A07;
        C56352kk r3 = new C56352kk(r5, i);
        Handler handler = r0.A06;
        handler.sendMessage(handler.obtainMessage(4, new AnonymousClass4PI(this, r3, r0.A0D.get())));
    }
}
