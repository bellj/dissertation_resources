package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.15F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15F {
    public final AbstractC15710nm A00;
    public final C240413z A01;

    public AnonymousClass15F(AbstractC15710nm r1, C240413z r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public List A00(int i, int i2) {
        String str;
        String str2;
        ArrayList arrayList = new ArrayList();
        if (i2 == 0) {
            str = "SELECT plaintext_hash, hash_of_image_part, timestamp, url, enc_hash, direct_path, mimetype, media_key, file_size, width, height, emojis, is_first_party, is_avatar FROM starred_stickers ORDER BY timestamp DESC LIMIT ?";
        } else {
            str = "SELECT plaintext_hash, hash_of_image_part, timestamp, url, enc_hash, direct_path, mimetype, media_key, file_size, width, height, emojis, is_first_party, is_avatar FROM starred_stickers WHERE is_avatar = ? ORDER BY timestamp DESC LIMIT ?";
        }
        if (i2 == 2) {
            str2 = "1";
        } else {
            str2 = "0";
        }
        String[] strArr = i2 == 0 ? new String[]{Integer.toString(i)} : new String[]{str2, Integer.toString(i)};
        try {
            C16310on A01 = this.A01.get();
            Cursor A09 = A01.A03.A09(str, strArr);
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("plaintext_hash");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("hash_of_image_part");
            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("timestamp");
            int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("url");
            int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("enc_hash");
            int columnIndexOrThrow6 = A09.getColumnIndexOrThrow("direct_path");
            int columnIndexOrThrow7 = A09.getColumnIndexOrThrow("mimetype");
            int columnIndexOrThrow8 = A09.getColumnIndexOrThrow("media_key");
            int columnIndexOrThrow9 = A09.getColumnIndexOrThrow("file_size");
            int columnIndexOrThrow10 = A09.getColumnIndexOrThrow("width");
            int columnIndexOrThrow11 = A09.getColumnIndexOrThrow("height");
            int columnIndexOrThrow12 = A09.getColumnIndexOrThrow("emojis");
            int columnIndexOrThrow13 = A09.getColumnIndexOrThrow("is_first_party");
            int columnIndexOrThrow14 = A09.getColumnIndexOrThrow("is_avatar");
            while (A09.moveToNext()) {
                String string = A09.getString(columnIndexOrThrow);
                String string2 = A09.getString(columnIndexOrThrow6);
                if (string != null) {
                    String string3 = A09.getString(columnIndexOrThrow2);
                    long j = A09.getLong(columnIndexOrThrow3);
                    String string4 = A09.getString(columnIndexOrThrow4);
                    String string5 = A09.getString(columnIndexOrThrow5);
                    String string6 = A09.getString(columnIndexOrThrow7);
                    String string7 = A09.getString(columnIndexOrThrow8);
                    int i3 = A09.getInt(columnIndexOrThrow9);
                    int i4 = A09.getInt(columnIndexOrThrow10);
                    int i5 = A09.getInt(columnIndexOrThrow11);
                    String string8 = A09.getString(columnIndexOrThrow12);
                    boolean z = false;
                    if (A09.getInt(columnIndexOrThrow13) == 1) {
                        z = true;
                    }
                    boolean z2 = false;
                    if (A09.getInt(columnIndexOrThrow14) == 1) {
                        z2 = true;
                    }
                    arrayList.add(new C37421mM(string, string3, string4, string5, string2, string6, string7, string8, i3, i4, i5, j, z, z2));
                }
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e("StarredStickerDBTableHelper.getStarredStickersData", e);
            this.A00.AaV("StarredStickerDBTableHelper.getStarredStickersData", e.getMessage(), true);
            return arrayList;
        }
    }

    public void A01(C37421mM r6) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("plaintext_hash", r6.A0A);
        contentValues.put("hash_of_image_part", r6.A01);
        contentValues.put("timestamp", Long.valueOf(r6.A07));
        contentValues.put("url", r6.A0D);
        contentValues.put("enc_hash", r6.A09);
        contentValues.put("direct_path", r6.A08);
        contentValues.put("mimetype", r6.A0C);
        contentValues.put("media_key", r6.A0B);
        contentValues.put("file_size", Integer.valueOf(r6.A04));
        contentValues.put("width", Integer.valueOf(r6.A06));
        contentValues.put("height", Integer.valueOf(r6.A05));
        contentValues.put("emojis", r6.A00);
        contentValues.put("is_first_party", Boolean.valueOf(r6.A03));
        contentValues.put("is_avatar", Boolean.valueOf(r6.A02));
        C16310on A02 = this.A01.A02();
        try {
            A02.A03.A06(contentValues, "starred_stickers", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A02(String str) {
        C16310on A02 = this.A01.A02();
        try {
            A02.A03.A01("starred_stickers", "plaintext_hash = ?", new String[]{str});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
