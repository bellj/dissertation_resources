package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119735f1 extends AbstractC30851Zb {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(20);
    public long A00;
    public AnonymousClass1ZR A01;
    public String A02;
    public String A03;
    public String A04;
    public boolean A05;

    @Override // X.AnonymousClass1ZY
    public AnonymousClass1ZR A06() {
        return null;
    }

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return null;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r7, AnonymousClass1V8 r8, int i) {
        try {
            super.A05 = r8.A0I("country", null);
            this.A06 = r8.A0I("credential-id", null);
            long j = 0;
            super.A00 = C28421Nd.A01(r8.A0I("created", null), 0);
            this.A08 = C117295Zj.A1T(r8, "default-debit", null, "1");
            this.A07 = C117295Zj.A1T(r8, "default-credit", null, "1");
            this.A04 = r8.A0H("status");
            String A0H = r8.A0H("bank-name");
            this.A01 = C117305Zk.A0I(C117305Zk.A0J(), A0H.getClass(), A0H, "bankName");
            this.A03 = r8.A0H("account-number");
            this.A02 = r8.A0H("account-id");
            this.A05 = C117295Zj.A1T(r8, "is-top-up", null, "true");
            long A08 = r8.A08("last_updated_time_usec", 0);
            if (A08 > 0) {
                j = A08 / 1000;
            }
            this.A00 = j;
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: NoviPaymentMethodBankAccountCountryData fromNetwork threw exception");
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: NoviPaymentMethodBankAccountCountryData toNetwork unsupported");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        Object obj;
        try {
            JSONObject A0C = A0C();
            A0C.put("status", this.A04);
            AnonymousClass1ZR r0 = this.A01;
            if (r0 == null) {
                obj = null;
            } else {
                obj = r0.A00;
            }
            A0C.put("bank-name", obj);
            A0C.put("account-number", this.A03);
            A0C.put("account-id", this.A02);
            A0C.put("is-top-up", this.A05);
            A0C.put("last-update-ts", this.A00);
            return A0C.toString();
        } catch (JSONException unused) {
            Log.e("PAY: NoviPaymentMethodBankAccountCountryData toDBString threw exception");
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        try {
            JSONObject A05 = C13000ix.A05(str);
            A0D(A05);
            this.A04 = A05.getString("status");
            String string = A05.getString("bank-name");
            this.A01 = C117305Zk.A0I(C117305Zk.A0J(), string.getClass(), string, "bankName");
            this.A03 = A05.getString("account-number");
            this.A02 = A05.getString("account-id");
            this.A05 = A05.getBoolean("is-top-up");
            this.A00 = A05.getLong("last-update-ts");
        } catch (JSONException unused) {
            Log.e("PAY: NoviPaymentMethodBankAccountCountryData fromDBString threw exception");
        }
    }

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        C17930rd A00 = C17930rd.A00(super.A05);
        String str = this.A06;
        long j = super.A00;
        int A002 = C117305Zk.A00(this.A08 ? 1 : 0);
        int A003 = C117305Zk.A00(this.A07 ? 1 : 0);
        String str2 = this.A03;
        C30861Zc r4 = new C30861Zc(A00, A002, A003, j, -1);
        r4.A0A = str;
        r4.A0A(str2);
        r4.A0B = (String) C117295Zj.A0R(this.A01);
        r4.A0D = null;
        r4.A08 = this;
        return r4;
    }

    @Override // X.AnonymousClass1ZY
    public AnonymousClass1ZR A07() {
        return this.A01;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(super.A05);
        parcel.writeString(this.A06);
        parcel.writeLong(super.A00);
        parcel.writeInt(this.A08 ? 1 : 0);
        parcel.writeInt(this.A07 ? 1 : 0);
        parcel.writeString(this.A04);
        parcel.writeParcelable(this.A01, i);
        parcel.writeString(this.A03);
        parcel.writeString(this.A02);
        parcel.writeInt(this.A05 ? 1 : 0);
        parcel.writeLong(this.A00);
    }
}
