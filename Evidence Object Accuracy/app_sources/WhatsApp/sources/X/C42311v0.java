package X;

import android.content.SharedPreferences;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.1v0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42311v0 implements AbstractC42321v1 {
    public final /* synthetic */ C28231Mf A00;

    public /* synthetic */ C42311v0(C28231Mf r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC42321v1
    public void AI5(AnonymousClass1JA r7, String str, int i, int i2, long j) {
        SharedPreferences.Editor edit;
        String str2;
        C28231Mf r5 = this.A00;
        r5.A02 = Long.valueOf((long) i2);
        StringBuilder sb = new StringBuilder("contactsyncmanager/handleSyncContactError/error sid=");
        sb.append(str);
        sb.append(" index=");
        sb.append(0);
        sb.append(" code=");
        sb.append(i2);
        sb.append(" backoff=");
        sb.append(j);
        Log.e(sb.toString());
        if (j > 0) {
            long A00 = r5.A0D.A00() + j;
            C20840wP r4 = r5.A09;
            r4.A01().edit().putLong("contact_sync_backoff", A00).apply();
            if (i2 == 503 && r5.A0H.A07(1297)) {
                Log.e("contactsyncmanager/handleSyncContactError need global backoff");
                edit = r4.A01().edit();
                str2 = "global_backoff_time";
            } else if (r5.A0H.A07(949) && r7.mode == EnumC42301uz.DELTA && i2 == 429) {
                Log.e("contactsyncmanager/handleSyncContactError/deltaSync need backoff");
                edit = r4.A01().edit();
                str2 = "delta_sync_backoff";
            } else {
                return;
            }
            edit.putLong(str2, A00).apply();
        }
    }

    @Override // X.AbstractC42321v1
    public void AI6(C42061ub r18, String str, int i) {
        List<Object> list;
        C28231Mf r0 = this.A00;
        r0.A01 = r18;
        C42081ud r2 = r18.A00;
        C42111ug r13 = r2.A01;
        C42111ug r11 = r2.A07;
        C42111ug r7 = r2.A08;
        C42111ug r6 = r2.A06;
        C42111ug r8 = r2.A00;
        C42111ug r9 = r2.A02;
        C42111ug r5 = r2.A05;
        C42111ug r4 = r2.A03;
        C42111ug r3 = r2.A04;
        StringBuilder sb = new StringBuilder("sync/result sid=");
        sb.append(str);
        sb.append(" index=");
        sb.append(0);
        sb.append(" users_count=");
        C42051ua[] r12 = r18.A01;
        int length = r12.length;
        sb.append(length);
        sb.append(" version=");
        sb.append(r2.A09);
        StringBuilder sb2 = new StringBuilder(sb.toString());
        if (r13 != null) {
            sb2.append(" contact=");
            sb2.append(r13.toString());
            Long l = r13.A02;
            if (l != null) {
                r0.A09.A01().edit().putLong("contact_full_sync_wait", l.longValue()).apply();
            }
            Long l2 = r13.A01;
            if (l2 != null) {
                r0.A09.A01().edit().putLong("contact_sync_backoff", r0.A0D.A00() + l2.longValue()).apply();
            }
        }
        if (r11 != null) {
            sb2.append(" sidelist=");
            sb2.append(r11.toString());
            Long l3 = r11.A02;
            if (l3 != null) {
                r0.A09.A01().edit().putLong("sidelist_full_sync_wait", l3.longValue()).apply();
            }
            Long l4 = r11.A01;
            if (l4 != null) {
                r0.A09.A03(r0.A0D.A00() + l4.longValue());
            }
        }
        if (r7 != null) {
            sb2.append(" status=");
            sb2.append(r7.toString());
            Long l5 = r7.A02;
            if (l5 != null) {
                r0.A09.A01().edit().putLong("status_full_sync_wait", l5.longValue()).apply();
            }
            Long l6 = r7.A01;
            if (l6 != null) {
                r0.A09.A01().edit().putLong("status_sync_backoff", r0.A0D.A00() + l6.longValue()).apply();
            }
        }
        if (r6 != null) {
            sb2.append(" picture=");
            sb2.append(r6.toString());
            Long l7 = r6.A02;
            if (l7 != null) {
                r0.A09.A01().edit().putLong("picture_full_sync_wait", l7.longValue()).apply();
            }
            Long l8 = r6.A01;
            if (l8 != null) {
                r0.A09.A01().edit().putLong("picture_sync_backoff", r0.A0D.A00() + l8.longValue()).apply();
            }
        }
        if (r8 != null) {
            sb2.append(" business=");
            sb2.append(r8.toString());
            Long l9 = r8.A02;
            if (l9 != null) {
                r0.A09.A01().edit().putLong("business_full_sync_wait", l9.longValue()).apply();
            }
            Long l10 = r8.A01;
            if (l10 != null) {
                r0.A09.A01().edit().putLong("business_sync_backoff", r0.A0D.A00() + l10.longValue()).apply();
            }
        }
        if (r9 != null) {
            sb2.append(" devices=");
            sb2.append(r9.toString());
            Long l11 = r9.A02;
            if (l11 != null) {
                r0.A09.A01().edit().putLong("devices_full_sync_wait", l11.longValue()).apply();
            }
            Long l12 = r9.A01;
            if (l12 != null) {
                r0.A09.A01().edit().putLong("devices_sync_backoff", r0.A0D.A00() + l12.longValue()).apply();
            }
        }
        if (r5 != null) {
            sb2.append(" payment=");
            sb2.append(r5.toString());
            Long l13 = r5.A02;
            if (l13 != null) {
                r0.A09.A01().edit().putLong("payment_full_sync_wait", l13.longValue()).apply();
            }
            Long l14 = r5.A01;
            if (l14 != null) {
                r0.A09.A01().edit().putLong("payment_sync_backoff", r0.A0D.A00() + l14.longValue()).apply();
            }
        }
        if (r4 != null) {
            sb2.append(" disappearing_mode=");
            sb2.append(r4.toString());
            Long l15 = r4.A02;
            if (l15 != null) {
                r0.A09.A01().edit().putLong("disappearing_mode_full_sync_wait", l15.longValue()).apply();
            }
            Long l16 = r4.A01;
            if (l16 != null) {
                r0.A09.A01().edit().putLong("disappearing_mode_sync_backoff", r0.A0D.A00() + l16.longValue()).apply();
            }
        }
        if (r3 != null) {
            sb2.append(" lid=");
            sb2.append(r3);
            Long l17 = r3.A02;
            if (l17 != null) {
                r0.A09.A01().edit().putLong("contact_lid_sync_wait", l17.longValue()).apply();
            }
            Long l18 = r3.A01;
            if (l18 != null) {
                r0.A09.A01().edit().putLong("lid_sync_backoff", r0.A0D.A00() + l18.longValue()).apply();
            }
        }
        Log.i(sb2.toString());
        C42221ur r42 = r0.A0A;
        HashSet A00 = r42.A00();
        for (C42051ua r62 : r12) {
            int i2 = r62.A04;
            if (i2 == 3) {
                List list2 = r62.A0G;
                AnonymousClass009.A05(list2);
                A00.addAll(list2);
            } else {
                if ((i2 == 1 || i2 == 2) && (list = r62.A0G) != null) {
                    for (Object obj : list) {
                        r0.A0P.put(obj, r62);
                    }
                }
                UserJid userJid = r62.A0C;
                if (userJid != null) {
                    r0.A0N.put(userJid, r62);
                } else {
                    Log.w("sync/result/no-jid-found");
                }
            }
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(r42.A01.A00.getFilesDir(), "invalid_numbers"));
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            try {
                r42.A00 = A00;
                objectOutputStream.writeObject(A00);
                objectOutputStream.close();
                fileOutputStream.close();
            } catch (Throwable th) {
                try {
                    objectOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException e) {
            Log.e(e);
        }
    }

    @Override // X.AbstractC42321v1
    public void AI7(String str, int i, int i2, long j) {
        C28231Mf r3 = this.A00;
        r3.A02 = 1L;
        StringBuilder sb = new StringBuilder("contactsyncmanager/handleSyncSidelistError/error sid=");
        sb.append(str);
        sb.append(" index=");
        sb.append(0);
        sb.append(" code=");
        sb.append(i2);
        sb.append(" backoff=");
        sb.append(j);
        Log.e(sb.toString());
        if (j > 0) {
            r3.A09.A03(r3.A0D.A00() + j);
        }
    }
}
