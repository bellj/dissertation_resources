package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.registration.RegisterName;

/* renamed from: X.3g6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class HandlerC73353g6 extends Handler {
    public final /* synthetic */ RegisterName A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC73353g6(Looper looper, RegisterName registerName) {
        super(looper);
        this.A00 = registerName;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        this.A00.A2k();
    }
}
