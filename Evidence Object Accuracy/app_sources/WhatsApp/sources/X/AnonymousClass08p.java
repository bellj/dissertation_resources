package X;

import android.database.DataSetObserver;
import androidx.viewpager.widget.ViewPager;

/* renamed from: X.08p  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08p extends DataSetObserver {
    public final /* synthetic */ ViewPager A00;

    public AnonymousClass08p(ViewPager viewPager) {
        this.A00 = viewPager;
    }

    @Override // android.database.DataSetObserver
    public void onChanged() {
        this.A00.A06();
    }

    @Override // android.database.DataSetObserver
    public void onInvalidated() {
        this.A00.A06();
    }
}
