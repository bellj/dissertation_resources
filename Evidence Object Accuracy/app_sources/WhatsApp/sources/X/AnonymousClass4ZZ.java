package X;

/* renamed from: X.4ZZ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4ZZ {
    public static C94884ch A00 = C94884ch.A06;
    public static final C94904cj A01 = new C94904cj();
    public static final C92284Vg A02 = new C92284Vg();

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        if (r2 == null) goto L_0x004b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(java.lang.Appendable r5, java.lang.Object r6, X.C94884ch r7) {
        /*
            if (r6 != 0) goto L_0x0008
            java.lang.String r0 = "null"
            r5.append(r0)
            return
        L_0x0008:
            java.lang.Class r4 = r6.getClass()
            X.4cj r3 = X.AnonymousClass4ZZ.A01
            java.util.concurrent.ConcurrentHashMap r0 = r3.A01
            java.lang.Object r2 = r0.get(r4)
            X.5VN r2 = (X.AnonymousClass5VN) r2
            if (r2 != 0) goto L_0x0029
            boolean r0 = r4.isArray()
            if (r0 == 0) goto L_0x002d
            X.5VN r2 = X.C94904cj.A09
        L_0x0020:
            r0 = 1
            java.lang.Class[] r1 = new java.lang.Class[r0]
            r0 = 0
            r1[r0] = r4
            r3.A01(r2, r1)
        L_0x0029:
            r2.AgH(r5, r6, r7)
            return
        L_0x002d:
            java.util.LinkedList r0 = r3.A00
            java.util.Iterator r2 = r0.iterator()
        L_0x0033:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x004b
            java.lang.Object r1 = r2.next()
            X.4Oj r1 = (X.C90564Oj) r1
            java.lang.Class r0 = r1.A00
            boolean r0 = r0.isAssignableFrom(r4)
            if (r0 == 0) goto L_0x0033
            X.5VN r2 = r1.A01
            if (r2 != 0) goto L_0x0020
        L_0x004b:
            X.5VN r2 = X.C94904cj.A0A
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4ZZ.A00(java.lang.Appendable, java.lang.Object, X.4ch):void");
    }
}
