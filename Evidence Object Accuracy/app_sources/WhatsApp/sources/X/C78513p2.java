package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3p2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78513p2 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98774jE();
    public final int A00;
    public final C78693pN A01;

    public C78513p2(C78693pN r2) {
        this.A00 = 1;
        this.A01 = r2;
    }

    public C78513p2(C78693pN r1, int i) {
        this.A00 = i;
        this.A01 = r1;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0B(parcel, this.A01, 2, i, false);
        C95654e8.A06(parcel, A00);
    }
}
