package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/* renamed from: X.0aO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07980aO implements AbstractC12850if, AbstractC12030hG, AbstractC12860ig, AbstractC12020hF, AbstractC12870ih {
    public C08020aS A00;
    public final Matrix A01 = new Matrix();
    public final Path A02 = new Path();
    public final AnonymousClass0AA A03;
    public final AnonymousClass0QR A04;
    public final AnonymousClass0QR A05;
    public final AnonymousClass0QD A06;
    public final AbstractC08070aX A07;
    public final String A08;
    public final boolean A09;

    public C07980aO(AnonymousClass0AA r3, C08150af r4, AbstractC08070aX r5) {
        this.A03 = r3;
        this.A07 = r5;
        this.A08 = r4.A03;
        this.A09 = r4.A04;
        AnonymousClass0H1 r0 = new AnonymousClass0H1(r4.A00.A00);
        this.A04 = r0;
        r5.A03(r0);
        r0.A07.add(this);
        AnonymousClass0H1 r02 = new AnonymousClass0H1(r4.A01.A00);
        this.A05 = r02;
        r5.A03(r02);
        r02.A07.add(this);
        AnonymousClass0QD r03 = new AnonymousClass0QD(r4.A02);
        this.A06 = r03;
        r03.A03(r5);
        r03.A02(this);
    }

    @Override // X.AbstractC12020hF
    public void A5W(ListIterator listIterator) {
        if (this.A00 == null) {
            while (listIterator.hasPrevious() && listIterator.previous() != this) {
            }
            ArrayList arrayList = new ArrayList();
            while (listIterator.hasPrevious()) {
                arrayList.add(listIterator.previous());
                listIterator.remove();
            }
            Collections.reverse(arrayList);
            this.A00 = new C08020aS(this.A03, null, this.A07, "Repeater", arrayList, this.A09);
        }
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r2, Object obj) {
        AnonymousClass0QR r0;
        if (!this.A06.A04(r2, obj)) {
            if (obj == AbstractC12810iX.A0E) {
                r0 = this.A04;
            } else if (obj == AbstractC12810iX.A0F) {
                r0 = this.A05;
            } else {
                return;
            }
            r0.A08(r2);
        }
    }

    @Override // X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        float floatValue = ((Number) this.A04.A03()).floatValue();
        float floatValue2 = ((Number) this.A05.A03()).floatValue();
        AnonymousClass0QD r5 = this.A06;
        float floatValue3 = ((Number) r5.A06.A03()).floatValue() / 100.0f;
        float floatValue4 = ((Number) r5.A01.A03()).floatValue() / 100.0f;
        int i2 = (int) floatValue;
        while (true) {
            i2--;
            if (i2 >= 0) {
                Matrix matrix2 = this.A01;
                matrix2.set(matrix);
                float f = (float) i2;
                matrix2.preConcat(r5.A01(f + floatValue2));
                this.A00.A9C(canvas, matrix2, (int) (((float) i) * (floatValue3 + ((f / floatValue) * (floatValue4 - floatValue3)))));
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        this.A00.AAy(matrix, rectF, z);
    }

    @Override // X.AbstractC12850if
    public Path AF0() {
        Path AF0 = this.A00.AF0();
        Path path = this.A02;
        path.reset();
        float floatValue = ((Number) this.A04.A03()).floatValue();
        float floatValue2 = ((Number) this.A05.A03()).floatValue();
        int i = (int) floatValue;
        while (true) {
            i--;
            if (i < 0) {
                return path;
            }
            Matrix matrix = this.A01;
            matrix.set(this.A06.A01(((float) i) + floatValue2));
            path.addPath(AF0, matrix);
        }
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A03.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r1, C06430To r2, List list, int i) {
        AnonymousClass0U0.A01(this, r1, r2, list, i);
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        this.A00.Aby(list, list2);
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A08;
    }
}
