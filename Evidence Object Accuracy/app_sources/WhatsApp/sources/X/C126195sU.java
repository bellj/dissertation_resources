package X;

import android.util.SparseArray;

/* renamed from: X.5sU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C126195sU {
    public final AnonymousClass28D A00;

    public C126195sU(AnonymousClass28D r5) {
        AnonymousClass28D r2 = new AnonymousClass28D(13642);
        this.A00 = r2;
        String A0I = r5.A0I(36);
        SparseArray sparseArray = r2.A02;
        sparseArray.put(36, A0I);
        sparseArray.put(44, Boolean.valueOf(r5.A0O(44, false)));
        sparseArray.put(43, Boolean.valueOf(r5.A0O(43, false)));
        sparseArray.put(48, r5.A0F(48));
        sparseArray.put(41, r5.A0F(41));
        sparseArray.put(45, r5.A0I(45));
    }
}
