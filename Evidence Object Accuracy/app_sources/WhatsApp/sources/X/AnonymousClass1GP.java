package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.1GP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1GP extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass11S A00;

    public AnonymousClass1GP(AnonymousClass11S r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("android.media.AUDIO_BECOMING_NOISY".equals(intent.getAction())) {
            for (AnonymousClass11P r0 : this.A00.A01) {
                r0.A04();
            }
        }
    }
}
