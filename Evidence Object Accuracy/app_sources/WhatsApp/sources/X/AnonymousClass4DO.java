package X;

import com.google.android.gms.common.api.Status;

/* renamed from: X.4DO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4DO {
    public static void A00(Status status, C13690kA r3, Object obj) {
        if (status.A01 <= 0) {
            r3.A01(obj);
            return;
        }
        r3.A00.A07(new C630439z(status));
    }
}
