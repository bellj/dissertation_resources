package X;

/* renamed from: X.1zv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C45011zv extends AbstractC16350or {
    public final long A00;
    public final C22490zA A01;
    public final C19380u1 A02;
    public final AnonymousClass01d A03;
    public final C18260sA A04;
    public final C44951zp A05;
    public final C15880o3 A06;
    public final AnonymousClass01H A07;

    public C45011zv(C22490zA r1, C19380u1 r2, AnonymousClass01d r3, C18260sA r4, C44951zp r5, C15880o3 r6, AnonymousClass01H r7, long j) {
        this.A00 = j;
        this.A07 = r7;
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r6;
        this.A05 = r5;
        this.A01 = r1;
    }
}
