package X;

import android.view.View;

/* renamed from: X.4rn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C104084rn implements AnonymousClass07F {
    public final /* synthetic */ AbstractC15160mf A00;

    public C104084rn(AbstractC15160mf r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass07F
    public C018408o AMH(View view, C018408o r6) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), r6.A03());
        return r6;
    }
}
