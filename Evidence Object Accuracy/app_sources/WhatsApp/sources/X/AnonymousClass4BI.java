package X;

/* renamed from: X.4BI  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BI {
    A03(0),
    A01(1),
    A02(2),
    A05(3),
    A04(4),
    A06(5);
    
    public final int mIntValue;

    AnonymousClass4BI(int i) {
        this.mIntValue = i;
    }
}
