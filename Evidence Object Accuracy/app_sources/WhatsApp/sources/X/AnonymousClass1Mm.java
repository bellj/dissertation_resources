package X;

import java.io.Serializable;
import java.util.Map;

/* renamed from: X.1Mm  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Mm implements Serializable {
    public static final long serialVersionUID = 0;
    public final Object keys;
    public final Object values;

    public AnonymousClass1Mm(AbstractC17190qP r7) {
        Object[] objArr = new Object[r7.size()];
        Object[] objArr2 = new Object[r7.size()];
        AnonymousClass1I5 it = r7.entrySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            objArr[i] = entry.getKey();
            objArr2[i] = entry.getValue();
            i++;
        }
        this.keys = objArr;
        this.values = objArr2;
    }

    public final Object legacyReadResolve() {
        Object[] objArr = (Object[]) this.keys;
        Object[] objArr2 = (Object[]) this.values;
        C28241Mh makeBuilder = makeBuilder(objArr.length);
        for (int i = 0; i < objArr.length; i++) {
            makeBuilder.put(objArr[i], objArr2[i]);
        }
        return makeBuilder.build();
    }

    public C28241Mh makeBuilder(int i) {
        return new C28241Mh(i);
    }

    public final Object readResolve() {
        Object obj = this.keys;
        if (!(obj instanceof AbstractC17940re)) {
            return legacyReadResolve();
        }
        AbstractC17950rf r2 = (AbstractC17950rf) obj;
        C28241Mh makeBuilder = makeBuilder(r2.size());
        AnonymousClass1I5 it = r2.iterator();
        AnonymousClass1I5 it2 = ((AbstractC17950rf) this.values).iterator();
        while (it.hasNext()) {
            makeBuilder.put(it.next(), it2.next());
        }
        return makeBuilder.build();
    }
}
