package X;

import java.util.List;

/* renamed from: X.0D0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0D0 extends AbstractC07280Xj {
    @Override // X.AbstractC07280Xj
    public boolean A0B() {
        return false;
    }

    public AnonymousClass0D0(AnonymousClass0QV r1) {
        super(r1);
    }

    @Override // X.AbstractC07280Xj
    public void A06() {
        C07270Xi r0;
        AnonymousClass0QV r6 = this.A03;
        if (r6 instanceof AnonymousClass0Cu) {
            C07270Xi r2 = this.A05;
            r2.A09 = true;
            AnonymousClass0Cu r62 = (AnonymousClass0Cu) r6;
            int i = r62.A00;
            boolean z = r62.A02;
            int i2 = 0;
            if (i == 0) {
                r2.A04 = EnumC03760Ja.LEFT;
                while (i2 < ((C02550Cv) r62).A00) {
                    AnonymousClass0QV r1 = ((C02550Cv) r62).A01[i2];
                    if (z || r1.A0N != 8) {
                        C07270Xi r12 = r1.A0c.A05;
                        r12.A07.add(r2);
                        r2.A08.add(r12);
                    }
                    i2++;
                }
            } else if (i != 1) {
                if (i == 2) {
                    r2.A04 = EnumC03760Ja.TOP;
                    while (i2 < ((C02550Cv) r62).A00) {
                        AnonymousClass0QV r13 = ((C02550Cv) r62).A01[i2];
                        if (z || r13.A0N != 8) {
                            C07270Xi r14 = r13.A0d.A05;
                            r14.A07.add(r2);
                            r2.A08.add(r14);
                        }
                        i2++;
                    }
                } else if (i == 3) {
                    r2.A04 = EnumC03760Ja.BOTTOM;
                    while (i2 < ((C02550Cv) r62).A00) {
                        AnonymousClass0QV r15 = ((C02550Cv) r62).A01[i2];
                        if (z || r15.A0N != 8) {
                            C07270Xi r16 = r15.A0d.A04;
                            r16.A07.add(r2);
                            r2.A08.add(r16);
                        }
                        i2++;
                    }
                } else {
                    return;
                }
                C07270Xi r02 = this.A03.A0d.A05;
                List list = r2.A07;
                list.add(r02);
                r02.A08.add(r2);
                r0 = this.A03.A0d.A04;
                list.add(r0);
                r0.A08.add(r2);
            } else {
                r2.A04 = EnumC03760Ja.RIGHT;
                while (i2 < ((C02550Cv) r62).A00) {
                    AnonymousClass0QV r17 = ((C02550Cv) r62).A01[i2];
                    if (z || r17.A0N != 8) {
                        C07270Xi r18 = r17.A0c.A04;
                        r18.A07.add(r2);
                        r2.A08.add(r18);
                    }
                    i2++;
                }
            }
            C07270Xi r03 = this.A03.A0c.A05;
            List list2 = r2.A07;
            list2.add(r03);
            r03.A08.add(r2);
            r0 = this.A03.A0c.A04;
            list2.add(r0);
            r0.A08.add(r2);
        }
    }

    @Override // X.AbstractC07280Xj
    public void A07() {
        AnonymousClass0QV r2 = this.A03;
        if (r2 instanceof AnonymousClass0Cu) {
            int i = ((AnonymousClass0Cu) r2).A00;
            if (i == 0 || i == 1) {
                r2.A0P = this.A05.A02;
            } else {
                r2.A0Q = this.A05.A02;
            }
        }
    }

    @Override // X.AbstractC07280Xj
    public void A08() {
        this.A07 = null;
        this.A05.A00();
    }

    @Override // X.AbstractC07280Xj, X.AbstractC11720gk
    public void AfH(AbstractC11720gk r9) {
        AnonymousClass0Cu r7 = (AnonymousClass0Cu) this.A03;
        int i = r7.A00;
        C07270Xi r5 = this.A05;
        int i2 = 0;
        int i3 = -1;
        for (C07270Xi r0 : r5.A08) {
            int i4 = r0.A02;
            if (i3 == -1 || i4 < i3) {
                i3 = i4;
            }
            if (i2 < i4) {
                i2 = i4;
            }
        }
        if (i == 0 || i == 2) {
            r5.A01(i3 + r7.A01);
        } else {
            r5.A01(i2 + r7.A01);
        }
    }
}
