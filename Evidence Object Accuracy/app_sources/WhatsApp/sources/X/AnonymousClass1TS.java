package X;

import java.util.Arrays;

/* renamed from: X.1TS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1TS {
    public final int A00;
    public final byte[] A01;

    public AnonymousClass1TS(byte[] bArr) {
        this.A00 = AnonymousClass1TT.A00(bArr);
        this.A01 = bArr;
    }

    public int hashCode() {
        return this.A00;
    }

    public boolean equals(Object obj) {
        if (obj instanceof AnonymousClass1TS) {
            return Arrays.equals(this.A01, ((AnonymousClass1TS) obj).A01);
        }
        return false;
    }
}
