package X;

import android.graphics.Path;
import android.os.Build;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/* renamed from: X.0aM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07960aM implements AbstractC12850if, AbstractC12020hF {
    public final Path A00 = new Path();
    public final Path A01 = new Path();
    public final Path A02 = new Path();
    public final C08210al A03;
    public final String A04;
    public final List A05 = new ArrayList();

    public C07960aM(C08210al r3) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.A04 = r3.A01;
            this.A03 = r3;
            return;
        }
        throw new IllegalStateException("Merge paths are not supported pre-KitKat.");
    }

    @Override // X.AbstractC12020hF
    public void A5W(ListIterator listIterator) {
        while (listIterator.hasPrevious() && listIterator.previous() != this) {
        }
        while (listIterator.hasPrevious()) {
            Object previous = listIterator.previous();
            if (previous instanceof AbstractC12850if) {
                this.A05.add(previous);
                listIterator.remove();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c7  */
    @Override // X.AbstractC12850if
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Path AF0() {
        /*
            r12 = this;
            android.graphics.Path r4 = r12.A01
            r4.reset()
            X.0al r1 = r12.A03
            boolean r0 = r1.A02
            if (r0 != 0) goto L_0x0014
            X.0JU r0 = r1.A00
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x0015;
                case 1: goto L_0x0031;
                case 2: goto L_0x0034;
                case 3: goto L_0x0037;
                case 4: goto L_0x002e;
                default: goto L_0x0014;
            }
        L_0x0014:
            return r4
        L_0x0015:
            r2 = 0
        L_0x0016:
            java.util.List r1 = r12.A05
            int r0 = r1.size()
            if (r2 >= r0) goto L_0x0014
            java.lang.Object r0 = r1.get(r2)
            X.0if r0 = (X.AbstractC12850if) r0
            android.graphics.Path r0 = r0.AF0()
            r4.addPath(r0)
            int r2 = r2 + 1
            goto L_0x0016
        L_0x002e:
            android.graphics.Path$Op r8 = android.graphics.Path.Op.XOR
            goto L_0x0039
        L_0x0031:
            android.graphics.Path$Op r8 = android.graphics.Path.Op.UNION
            goto L_0x0039
        L_0x0034:
            android.graphics.Path$Op r8 = android.graphics.Path.Op.REVERSE_DIFFERENCE
            goto L_0x0039
        L_0x0037:
            android.graphics.Path$Op r8 = android.graphics.Path.Op.INTERSECT
        L_0x0039:
            android.graphics.Path r7 = r12.A02
            r7.reset()
            android.graphics.Path r6 = r12.A00
            r6.reset()
            java.util.List r9 = r12.A05
            int r10 = r9.size()
            r5 = 1
            int r10 = r10 - r5
        L_0x004b:
            if (r10 < r5) goto L_0x008f
            java.lang.Object r11 = r9.get(r10)
            X.0if r11 = (X.AbstractC12850if) r11
            boolean r0 = r11 instanceof X.C08020aS
            if (r0 == 0) goto L_0x0085
            X.0aS r11 = (X.C08020aS) r11
            java.util.List r3 = r11.A00()
            int r2 = r3.size()
            int r2 = r2 - r5
        L_0x0062:
            if (r2 < 0) goto L_0x008c
            java.lang.Object r0 = r3.get(r2)
            X.0if r0 = (X.AbstractC12850if) r0
            android.graphics.Path r1 = r0.AF0()
            X.0QD r0 = r11.A02
            if (r0 == 0) goto L_0x007f
            android.graphics.Matrix r0 = r0.A00()
        L_0x0076:
            r1.transform(r0)
            r7.addPath(r1)
            int r2 = r2 + -1
            goto L_0x0062
        L_0x007f:
            android.graphics.Matrix r0 = r11.A04
            r0.reset()
            goto L_0x0076
        L_0x0085:
            android.graphics.Path r0 = r11.AF0()
            r7.addPath(r0)
        L_0x008c:
            int r10 = r10 + -1
            goto L_0x004b
        L_0x008f:
            r5 = 0
            java.lang.Object r3 = r9.get(r5)
            X.0if r3 = (X.AbstractC12850if) r3
            boolean r0 = r3 instanceof X.C08020aS
            if (r0 == 0) goto L_0x00c7
            X.0aS r3 = (X.C08020aS) r3
            java.util.List r2 = r3.A00()
        L_0x00a0:
            int r0 = r2.size()
            if (r5 >= r0) goto L_0x00ce
            java.lang.Object r0 = r2.get(r5)
            X.0if r0 = (X.AbstractC12850if) r0
            android.graphics.Path r1 = r0.AF0()
            X.0QD r0 = r3.A02
            if (r0 == 0) goto L_0x00c1
            android.graphics.Matrix r0 = r0.A00()
        L_0x00b8:
            r1.transform(r0)
            r6.addPath(r1)
            int r5 = r5 + 1
            goto L_0x00a0
        L_0x00c1:
            android.graphics.Matrix r0 = r3.A04
            r0.reset()
            goto L_0x00b8
        L_0x00c7:
            android.graphics.Path r0 = r3.AF0()
            r6.set(r0)
        L_0x00ce:
            r4.op(r6, r7, r8)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07960aM.AF0():android.graphics.Path");
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        int i = 0;
        while (true) {
            List list3 = this.A05;
            if (i < list3.size()) {
                ((AbstractC12470hy) list3.get(i)).Aby(list, list2);
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A04;
    }
}
