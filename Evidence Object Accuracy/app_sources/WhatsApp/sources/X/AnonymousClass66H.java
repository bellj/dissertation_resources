package X;

import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.params.ColorSpaceTransform;
import android.hardware.camera2.params.RggbChannelVector;
import android.os.SystemClock;

/* renamed from: X.66H  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66H implements AbstractC136416Ml, AnonymousClass6MJ {
    public static final float[] A09 = new float[4];
    public static final int[] A0A = new int[18];
    public Long A00;
    public final C129475xm A01;
    public final AnonymousClass6Lj A02;
    public final C129875yR A03;
    public final C128475wA A04 = new C128475wA(this);
    public final boolean A05;
    public volatile AnonymousClass6L0 A06;
    public volatile C130635zj A07;
    public volatile Boolean A08;

    @Override // X.AbstractC136416Ml
    public void ANj(C129415xg r1, C1310360y r2) {
    }

    public AnonymousClass66H(boolean z) {
        AnonymousClass66G r0 = new AnonymousClass66G(this);
        this.A02 = r0;
        this.A05 = z;
        C129875yR r2 = new C129875yR();
        this.A03 = r2;
        r2.A01 = r0;
        r2.A02(10000);
        this.A01 = new C129475xm();
    }

    @Override // X.AnonymousClass6MJ
    public void A6e() {
        this.A03.A00();
    }

    @Override // X.AnonymousClass6MJ
    public /* bridge */ /* synthetic */ Object AGH() {
        if (this.A08 == null) {
            throw C12960it.A0U("Photo capture operation hasn't completed yet.");
        } else if (this.A08.booleanValue()) {
            C130635zj r1 = this.A07;
            if (r1 != null && (r1.A04 != null || r1.A01 != null)) {
                return r1;
            }
            throw C12960it.A0U("Photo capture data is null.");
        } else {
            throw this.A06;
        }
    }

    @Override // X.AbstractC136416Ml
    public void ANi(C1310360y r5, C129425xh r6) {
        AnonymousClass60C A00 = AnonymousClass60C.A00();
        A00.A02(6, A00.A02);
        C129475xm r2 = this.A01;
        r2.A01(r6);
        Number number = (Number) r6.A00(CaptureResult.SENSOR_TIMESTAMP);
        if (number != null) {
            AnonymousClass60H A002 = r2.A00(number.longValue());
            if (A002 == null) {
                AnonymousClass616.A01("StillImageCaptureCallback", "Failed to retrieve current frame metadata object, after setting it!");
            } else {
                try {
                    RggbChannelVector rggbChannelVector = (RggbChannelVector) r6.A00(CaptureResult.COLOR_CORRECTION_GAINS);
                    if (rggbChannelVector != null) {
                        float[] fArr = A09;
                        rggbChannelVector.copyTo(fArr, 0);
                        A002.A01(AnonymousClass60H.A0H, fArr);
                    }
                } catch (IllegalArgumentException unused) {
                }
                try {
                    ColorSpaceTransform colorSpaceTransform = (ColorSpaceTransform) r6.A00(CaptureResult.COLOR_CORRECTION_TRANSFORM);
                    if (colorSpaceTransform != null) {
                        int[] iArr = A0A;
                        colorSpaceTransform.copyElements(iArr, 0);
                        A002.A01(AnonymousClass60H.A0I, iArr);
                    }
                } catch (IllegalArgumentException unused2) {
                }
            }
        }
        this.A00 = (Long) r6.A00(CaptureResult.SENSOR_EXPOSURE_TIME);
        if (this.A05 && Boolean.TRUE.equals(this.A08)) {
            this.A03.A01();
        }
    }

    @Override // X.AbstractC136416Ml
    public void ANk(CaptureRequest captureRequest, C1310360y r5, long j, long j2) {
        AnonymousClass60C.A00().A02 = SystemClock.elapsedRealtime();
    }
}
