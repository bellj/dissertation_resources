package X;

import android.database.Cursor;
import java.io.Closeable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.2FP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2FP implements Iterator, Closeable {
    public final Cursor A00;
    public final AnonymousClass2FN A01;

    public AnonymousClass2FP(Cursor cursor, AnonymousClass2FN r2) {
        this.A00 = cursor;
        this.A01 = r2;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00.close();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r1.isAfterLast() != false) goto L_0x0010;
     */
    @Override // java.util.Iterator
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasNext() {
        /*
            r3 = this;
            android.database.Cursor r1 = r3.A00
            boolean r0 = r1.isLast()
            r2 = 1
            if (r0 != 0) goto L_0x0010
            boolean r1 = r1.isAfterLast()
            r0 = 0
            if (r1 == 0) goto L_0x0011
        L_0x0010:
            r0 = 1
        L_0x0011:
            r0 = r0 ^ r2
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2FP.hasNext():boolean");
    }

    @Override // java.util.Iterator
    public Object next() {
        Cursor cursor = this.A00;
        if (cursor.moveToNext()) {
            return this.A01.AYs(cursor);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
