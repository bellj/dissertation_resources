package X;

/* renamed from: X.0mC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14880mC {
    public double A00;
    public double A01;
    public double A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public boolean A0C;
    public boolean A0D;
    public final C15450nH A0E;
    public final String A0F;

    public C14880mC(C15450nH r2, String str, String str2, String str3, String str4, String str5, boolean z) {
        this.A0E = r2;
        this.A0F = str;
        this.A0A = str2;
        this.A0B = str3;
        this.A08 = str4;
        this.A07 = str5;
        this.A0D = z;
        this.A0C = "Portal".equals(str4);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("WebSessionInfo{browserId='");
        sb.append(this.A0F);
        sb.append('\'');
        sb.append(", secret='");
        sb.append(this.A0A);
        sb.append('\'');
        sb.append(", token='");
        sb.append(this.A0B);
        sb.append('\'');
        sb.append(", os='");
        sb.append(this.A08);
        sb.append('\'');
        sb.append(", browserType='");
        sb.append(this.A07);
        sb.append('\'');
        sb.append(", lat=");
        sb.append(this.A01);
        sb.append(", lon=");
        sb.append(this.A02);
        sb.append(", accuracy=");
        sb.append(this.A00);
        sb.append(", placeName='");
        sb.append(this.A09);
        sb.append('\'');
        sb.append(", lastActive=");
        sb.append(this.A04);
        sb.append(", lastPushSent=");
        sb.append(this.A05);
        sb.append(", timeout=");
        sb.append(this.A0D);
        sb.append(", expiration=");
        sb.append(this.A03);
        sb.append('}');
        return sb.toString();
    }
}
