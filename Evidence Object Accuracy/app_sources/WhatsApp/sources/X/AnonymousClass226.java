package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.StrictMode;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape2S0200000_I1;
import com.google.android.gms.maps.GoogleMapOptions;
import com.whatsapp.R;

/* renamed from: X.226  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass226 extends FrameLayout {
    public final C64163Em A00;

    public AnonymousClass226(Context context, GoogleMapOptions googleMapOptions) {
        super(context);
        this.A00 = new C64163Em(context, this, googleMapOptions);
        setClickable(true);
    }

    public void A00() {
        C64163Em r1 = this.A00;
        AbstractC115105Qf r0 = r1.A01;
        if (r0 != null) {
            try {
                C65873Li r2 = (C65873Li) ((AnonymousClass3T2) r0).A02;
                r2.A03(5, r2.A01());
            } catch (RemoteException e) {
                throw new C113245Gt(e);
            }
        } else {
            r1.A00(1);
        }
    }

    public void A01() {
        AbstractC115105Qf r0 = this.A00.A01;
        if (r0 != null) {
            try {
                C65873Li r2 = (C65873Li) ((AnonymousClass3T2) r0).A02;
                r2.A03(6, r2.A01());
            } catch (RemoteException e) {
                throw new C113245Gt(e);
            }
        }
    }

    public void A02() {
        C64163Em r1 = this.A00;
        AbstractC115105Qf r0 = r1.A01;
        if (r0 != null) {
            try {
                C65873Li r2 = (C65873Li) ((AnonymousClass3T2) r0).A02;
                r2.A03(4, r2.A01());
            } catch (RemoteException e) {
                throw new C113245Gt(e);
            }
        } else {
            r1.A00(5);
        }
    }

    public void A03() {
        C64163Em r2 = this.A00;
        r2.A01(null, new AnonymousClass3T3(r2));
    }

    public void A04(Bundle bundle) {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitAll().build());
        try {
            C64163Em r1 = this.A00;
            r1.A01(bundle, new AnonymousClass3T4(bundle, r1));
            if (r1.A01 == null) {
                C471729i r3 = C471729i.A00;
                Context context = getContext();
                int A00 = r3.A00(context, 12451000);
                String A01 = C472129m.A01(context, A00);
                Resources resources = context.getResources();
                int i = R.string.common_google_play_services_install_button;
                if (A00 != 1) {
                    i = R.string.common_google_play_services_update_button;
                    if (A00 != 2) {
                        i = R.string.common_google_play_services_enable_button;
                        if (A00 != 3) {
                            i = 17039370;
                        }
                    }
                }
                String string = resources.getString(i);
                LinearLayout linearLayout = new LinearLayout(getContext());
                linearLayout.setOrientation(1);
                linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                addView(linearLayout);
                TextView textView = new TextView(getContext());
                textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                textView.setText(A01);
                linearLayout.addView(textView);
                Intent A012 = r3.A01(context, null, A00);
                if (A012 != null) {
                    Button button = new Button(context);
                    button.setId(16908313);
                    button.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                    button.setText(string);
                    linearLayout.addView(button);
                    button.setOnClickListener(new ViewOnClickCListenerShape2S0200000_I1(context, 0, A012));
                }
            }
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    public void A05(Bundle bundle) {
        C64163Em r1 = this.A00;
        AbstractC115105Qf r0 = r1.A01;
        if (r0 != null) {
            AnonymousClass3T2 r02 = (AnonymousClass3T2) r0;
            try {
                Bundle bundle2 = new Bundle();
                C65153Ii.A01(bundle, bundle2);
                C65873Li r2 = (C65873Li) r02.A02;
                Parcel A01 = r2.A01();
                C65183In.A01(A01, bundle2);
                Parcel A02 = r2.A02(7, A01);
                if (A02.readInt() != 0) {
                    bundle2.readFromParcel(A02);
                }
                A02.recycle();
                C65153Ii.A01(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C113245Gt(e);
            }
        } else {
            Bundle bundle3 = r1.A00;
            if (bundle3 != null) {
                bundle.putAll(bundle3);
            }
        }
    }

    public void A06(AbstractC115755Su r4) {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            C13020j0.A02(r4, "callback must not be null.");
            C64163Em r1 = this.A00;
            AbstractC115105Qf r0 = r1.A01;
            if (r0 != null) {
                ((AnonymousClass3T2) r0).A00(r4);
            } else {
                r1.A08.add(r4);
            }
        } else {
            throw new IllegalStateException("getMapAsync() must be called on the main thread");
        }
    }
}
