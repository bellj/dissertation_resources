package X;

/* renamed from: X.312  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass312 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public Long A06;
    public String A07;

    public AnonymousClass312() {
        super(2130, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A05);
        r3.Abe(5, this.A06);
        r3.Abe(3, this.A07);
        r3.Abe(6, this.A00);
        r3.Abe(8, this.A01);
        r3.Abe(7, this.A02);
        r3.Abe(1, this.A03);
        r3.Abe(2, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidInviteEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteAddressbookSize", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteAddressbookWhatsappSize", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteAppName", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteContactPermissionDisabled", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteContactWithMultiplePhoneNumbersClicked", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteContactWithMultiplePhoneNumbersExists", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteEventType", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviteSource", C12960it.A0Y(this.A04));
        return C12960it.A0d("}", A0k);
    }
}
