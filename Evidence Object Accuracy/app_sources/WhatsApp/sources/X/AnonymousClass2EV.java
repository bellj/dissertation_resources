package X;

import java.util.List;

/* renamed from: X.2EV  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2EV extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $handler;
    public final /* synthetic */ String $parentCategoryId;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2EV(String str, AnonymousClass1J7 r3) {
        super(1);
        this.$parentCategoryId = str;
        this.$handler = r3;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        AnonymousClass4K6 r5 = (AnonymousClass4K6) obj;
        C16700pc.A0E(r5, 0);
        if (r5.A00) {
            C60262wN r52 = (C60262wN) r5;
            this.$handler.AJ4(new C60252wM((List) C17530qx.A00(r52.A01, this.$parentCategoryId), r52.A00));
        } else {
            this.$handler.AJ4(r5);
        }
        return AnonymousClass1WZ.A00;
    }
}
