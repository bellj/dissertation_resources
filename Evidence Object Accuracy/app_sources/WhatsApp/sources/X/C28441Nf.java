package X;

import java.util.Map;

/* renamed from: X.1Nf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28441Nf {
    public final long A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final Map A04;
    public final boolean A05;
    public final boolean A06;

    public C28441Nf(String str, String str2, String str3, Map map, long j, boolean z, boolean z2) {
        this.A02 = str;
        this.A01 = str2;
        this.A06 = z;
        this.A05 = z2;
        this.A03 = str3;
        this.A00 = j;
        this.A04 = map;
    }
}
