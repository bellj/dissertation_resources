package X;

import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: X.38g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C627038g extends AbstractC16350or {
    public final C22680zT A00;
    public final C16170oZ A01;
    public final C14830m7 A02;
    public final AnonymousClass018 A03;
    public final C15650ng A04;
    public final AbstractC14640lm A05;
    public final C15580nU A06;
    public final AnonymousClass1IS A07;
    public final WeakReference A08;
    public final ArrayList A09;
    public final ArrayList A0A;
    public final boolean A0B;

    public C627038g(C22680zT r2, C16170oZ r3, C14830m7 r4, AnonymousClass018 r5, C15650ng r6, AbstractC14640lm r7, C15580nU r8, AnonymousClass1IS r9, ViewSharedContactArrayActivity viewSharedContactArrayActivity, ArrayList arrayList, ArrayList arrayList2, boolean z) {
        super(viewSharedContactArrayActivity);
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = r5;
        this.A04 = r6;
        this.A00 = r2;
        this.A08 = C12970iu.A10(viewSharedContactArrayActivity);
        this.A05 = r7;
        this.A09 = arrayList;
        this.A0A = arrayList2;
        this.A0B = z;
        this.A07 = r9;
        this.A06 = r8;
    }
}
