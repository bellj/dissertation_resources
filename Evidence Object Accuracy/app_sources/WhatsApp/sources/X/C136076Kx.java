package X;

/* renamed from: X.6Kx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C136076Kx extends RuntimeException {
    public C136076Kx(String str) {
        super(C12960it.A0d(str, C12960it.A0k("Cannot use CameraService in a disconnected state: ")));
    }
}
