package X;

import android.text.TextUtils;
import com.whatsapp.emoji.EmojiDescriptor;

/* renamed from: X.2VC  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2VC {
    public static int A00(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return 0;
        }
        return A01(charSequence, 0, charSequence.length());
    }

    public static int A01(CharSequence charSequence, int i, int i2) {
        int length = charSequence.length();
        if (i < 0 || i2 > length || i > i2) {
            throw new IndexOutOfBoundsException();
        }
        int i3 = 0;
        if (length != 0) {
            C32681cY r2 = new C32681cY(charSequence);
            while (i < i2) {
                r2.A00 = i;
                i += r2.A01(i, EmojiDescriptor.A00(r2, false));
                i3++;
            }
        }
        return i3;
    }
}
