package X;

/* renamed from: X.4bs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94474bs {
    public int A00;
    public AnonymousClass5XE A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public byte[] A05;

    public C94474bs() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0025, code lost:
        if (r4.startsWith("PGP", r3) == false) goto L_0x0027;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C94474bs(X.AnonymousClass5XE r7) {
        /*
            r6 = this;
            r6.<init>()
            r6.A01 = r7
            int r0 = r7.AAt()
            byte[] r0 = new byte[r0]
            r6.A05 = r0
            r5 = 0
            r6.A00 = r5
            java.lang.String r4 = r7.AAf()
            r0 = 47
            int r3 = r4.indexOf(r0)
            r2 = 1
            int r3 = r3 + r2
            if (r3 <= 0) goto L_0x0027
            java.lang.String r0 = "PGP"
            boolean r1 = r4.startsWith(r0, r3)
            r0 = 1
            if (r1 != 0) goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            r6.A04 = r0
            if (r0 != 0) goto L_0x003e
            boolean r0 = r7 instanceof X.AnonymousClass20H
            if (r0 != 0) goto L_0x003e
            if (r3 <= 0) goto L_0x003b
            java.lang.String r0 = "OpenPGP"
            boolean r0 = r4.startsWith(r0, r3)
            if (r0 == 0) goto L_0x003b
            r5 = 1
        L_0x003b:
            r6.A03 = r5
            return
        L_0x003e:
            r6.A03 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94474bs.<init>(X.5XE):void");
    }

    public void A01() {
        int i = 0;
        while (true) {
            byte[] bArr = this.A05;
            if (i < bArr.length) {
                bArr[i] = 0;
                i++;
            } else {
                this.A00 = 0;
                this.A01.reset();
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        if (r0 == 0) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(int r3) {
        /*
            r2 = this;
            boolean r0 = r2 instanceof X.C114945Nr
            if (r0 != 0) goto L_0x0031
            boolean r1 = r2 instanceof X.C114935Nq
            int r0 = r2.A00
            int r3 = r3 + r0
            if (r1 != 0) goto L_0x0029
            boolean r0 = r2.A04
            if (r0 == 0) goto L_0x0023
            boolean r1 = r2.A02
            byte[] r0 = r2.A05
            int r0 = r0.length
            if (r1 == 0) goto L_0x0026
            int r1 = r3 % r0
            X.5XE r0 = r2.A01
            int r0 = r0.AAt()
            int r0 = r0 + 2
            int r1 = r1 - r0
        L_0x0021:
            int r3 = r3 - r1
            return r3
        L_0x0023:
            byte[] r0 = r2.A05
            int r0 = r0.length
        L_0x0026:
            int r1 = r3 % r0
            goto L_0x0021
        L_0x0029:
            byte[] r0 = r2.A05
            int r1 = r0.length
            int r0 = r3 % r1
            if (r0 != 0) goto L_0x0042
            goto L_0x0021
        L_0x0031:
            int r0 = r2.A00
            int r3 = r3 + r0
            byte[] r0 = r2.A05
            int r1 = r0.length
            int r0 = r3 % r1
            if (r0 != 0) goto L_0x0042
            r0 = 0
            int r3 = r3 - r1
            int r3 = java.lang.Math.max(r0, r3)
            return r3
        L_0x0042:
            int r3 = r3 - r0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94474bs.A00(int):int");
    }
}
