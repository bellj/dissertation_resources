package X;

import android.transition.Transition;
import com.whatsapp.profile.ProfileInfoActivity;

/* renamed from: X.2r4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58612r4 extends AbstractC100714mM {
    public final /* synthetic */ ProfileInfoActivity A00;

    public C58612r4(ProfileInfoActivity profileInfoActivity) {
        this.A00 = profileInfoActivity;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
        ProfileInfoActivity profileInfoActivity = this.A00;
        profileInfoActivity.A01.setScaleX(0.0f);
        profileInfoActivity.A01.setScaleY(0.0f);
        profileInfoActivity.A01.animate().scaleX(1.0f).scaleY(1.0f).setDuration(125);
    }
}
