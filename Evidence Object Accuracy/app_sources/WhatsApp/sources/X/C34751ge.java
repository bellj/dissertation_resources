package X;

/* renamed from: X.1ge  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34751ge extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;
    public String A03;

    public C34751ge() {
        super(2286, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(4, this.A02);
        r3.Abe(1, this.A03);
        r3.Abe(3, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamMdBootstrapStarted {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapSource", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdRegAttemptId", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdSessionId", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdTimestamp", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
