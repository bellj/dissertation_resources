package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1YD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YD {
    public int A00;
    public final Map A01 = new HashMap();
    public final AnonymousClass01N A02;

    public AnonymousClass1YD(AnonymousClass01N r2, int i) {
        this.A00 = i;
        this.A02 = r2;
    }

    public void A00() {
        Map map = this.A01;
        for (AnonymousClass1YE r0 : map.values()) {
            if (r0 != null) {
                r0.A00.close();
            }
        }
        map.clear();
    }
}
