package X;

import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.audiopicker.AudioPickerActivity;

/* renamed from: X.3WP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WP implements AnonymousClass2MF {
    public final /* synthetic */ ActivityC13810kN A00;
    public final /* synthetic */ C64393Fj A01;
    public final /* synthetic */ C35191hP A02;
    public final /* synthetic */ C30421Xi A03;

    @Override // X.AnonymousClass2MF
    public void APa(boolean z) {
    }

    public AnonymousClass3WP(ActivityC13810kN r1, C64393Fj r2, C35191hP r3, C30421Xi r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
    }

    @Override // X.AnonymousClass2MF
    public C30421Xi ACr() {
        return this.A03;
    }

    @Override // X.AnonymousClass2MF
    public void ATS(int i) {
        this.A01.A01(this.A00, false);
    }

    @Override // X.AnonymousClass2MF
    public void AUL(int i) {
        C64393Fj r0 = this.A01;
        CircularProgressBar circularProgressBar = r0.A0A;
        circularProgressBar.setProgress(i);
        AudioPickerActivity audioPickerActivity = r0.A0C;
        circularProgressBar.setContentDescription(C12960it.A0X(audioPickerActivity, C38131nZ.A06(((ActivityC13830kP) audioPickerActivity).A01, (long) i), C12970iu.A1b(), 0, R.string.voice_message_time_elapsed));
    }

    @Override // X.AnonymousClass2MF
    public void AVR() {
        this.A01.A00(this.A00);
    }

    @Override // X.AnonymousClass2MF
    public void AWG(int i) {
        this.A02.A09(0);
        C64393Fj r1 = this.A01;
        r1.A00(this.A00);
        r1.A0A.setMax(i);
    }

    @Override // X.AnonymousClass2MF
    public void AWo(int i, boolean z) {
        C64393Fj r2 = this.A01;
        r2.A01(this.A00, true);
        if (z) {
            r2.A0A.setProgress(0);
        }
        r2.A0C.A0G.A08(null);
    }
}
