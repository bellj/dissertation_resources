package X;

import java.util.List;

/* renamed from: X.5Xu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC117015Xu {
    public static final AbstractC117015Xu A00 = new AbstractC117015Xu() { // from class: X.4x9
        @Override // X.AbstractC117015Xu
        public final List ACR(String str, boolean z, boolean z2) {
            return C95604e3.A03(str, z, z2);
        }
    };

    List ACR(String str, boolean z, boolean z2);
}
