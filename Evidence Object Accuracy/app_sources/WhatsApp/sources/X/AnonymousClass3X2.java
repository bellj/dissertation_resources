package X;

import android.graphics.Bitmap;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3X2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X2 implements AnonymousClass23D {
    public final /* synthetic */ View$OnClickListenerC55232i0 A00;
    public final /* synthetic */ AbstractC35611iN A01;

    public AnonymousClass3X2(View$OnClickListenerC55232i0 r1, AbstractC35611iN r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        Uri AAE = this.A01.AAE();
        StringBuilder A0h = C12960it.A0h();
        A0h.append(AAE);
        return C12960it.A0d("-gallery_thumb", A0h);
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        AnonymousClass2T1 r1 = this.A00.A03;
        if (r1.getTag() != this) {
            return null;
        }
        Bitmap Aem = this.A01.Aem(r1.getResources().getDimensionPixelSize(R.dimen.camera_thumb_size));
        if (Aem == null) {
            return MediaGalleryFragmentBase.A0U;
        }
        return Aem;
    }
}
