package X;

/* renamed from: X.4SC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SC {
    public final AbstractC455722e A00;
    public final String A01;
    public final byte[] A02;
    public final byte[] A03;

    public AnonymousClass4SC(AbstractC455722e r1, String str, byte[] bArr, byte[] bArr2) {
        this.A02 = bArr;
        this.A03 = bArr2;
        this.A01 = str;
        this.A00 = r1;
    }
}
