package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2m8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56902m8 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C56902m8 A00;
    public static volatile AnonymousClass255 A01;

    static {
        C56902m8 r0 = new C56902m8();
        A00 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int A002 = this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A002;
        return A002;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r3, Object obj, Object obj2) {
        int A03;
        switch (r3.ordinal()) {
            case 0:
                return A00;
            case 1:
                return this;
            case 2:
                AnonymousClass253 r4 = (AnonymousClass253) obj;
                do {
                    try {
                        A03 = r4.A03();
                        if (A03 == 0) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                } while (A0a(r4, A03));
                break;
            case 3:
                return null;
            case 4:
                return new C56902m8();
            case 5:
                return new C82243vJ();
            case 6:
                break;
            case 7:
                if (A01 == null) {
                    synchronized (C56902m8.class) {
                        if (A01 == null) {
                            A01 = AbstractC27091Fz.A09(A00);
                        }
                    }
                }
                return A01;
            default:
                throw C12970iu.A0z();
        }
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
