package X;

import android.net.TrafficStats;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.2rk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58812rk extends AbstractC84003yF {
    public final /* synthetic */ C44791zY A00;
    public final /* synthetic */ String A01;

    public C58812rk(C44791zY r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AbstractC84003yF, X.AbstractC84013yG, X.AbstractC84023yH, X.AnonymousClass4UU
    public /* bridge */ /* synthetic */ Object A00(int i) {
        Throwable th;
        IOException e;
        HttpsURLConnection A07;
        C44791zY r5 = this.A00;
        String str = this.A01;
        HttpsURLConnection httpsURLConnection = null;
        r8 = null;
        C65113Ie r8 = null;
        if (r5.A0B()) {
            Log.i("gdrive-api-v2/create-backup/api disabled");
            return null;
        }
        try {
            TrafficStats.setThreadStatsTag(13);
            try {
                HashMap A11 = C12970iu.A11();
                A11.put("backupId", str);
                A07 = r5.A07("POST", "clients/wa/backups", null, A11, false);
            } catch (IOException e2) {
                e = e2;
            }
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            int responseCode = A07.getResponseCode();
            if (responseCode == 200) {
                r8 = C65113Ie.A00(r5.A04, r5.A06, r5, r5.A0C, A07.getInputStream(), str);
            } else if (responseCode == 403) {
                throw new C84093yP();
            } else if (responseCode == 409) {
                try {
                    r8 = r5.A04(str);
                } catch (C84053yL e3) {
                    Log.e("gdrive-api-v2/create-backup/failed to get one", e3);
                    throw new C84183yY(e3);
                }
            } else if (responseCode == 400) {
                String A01 = AnonymousClass1P1.A01(A07);
                r5.A04.AaV("gdrive-api-v2/create-backup", A01, true);
                throw new C84063yM(A01);
            } else if (responseCode != 401) {
                Log.e(C12960it.A0f(C12960it.A0j("gdrive-api-v2/create-backup/failed "), A07.getResponseCode()));
                Log.e(C12960it.A0d(AnonymousClass1P1.A01(A07), C12960it.A0j("gdrive-api-v2/create-backup/failed ")));
            } else {
                r5.A0A();
            }
            A07.disconnect();
            TrafficStats.clearThreadStatsTag();
            return r8;
        } catch (IOException e4) {
            e = e4;
            throw AnonymousClass2AR.A00(e);
        } catch (Throwable th3) {
            th = th3;
            httpsURLConnection = A07;
            if (httpsURLConnection != null) {
                httpsURLConnection.disconnect();
            }
            TrafficStats.clearThreadStatsTag();
            throw th;
        }
    }
}
