package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.2tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58992tm extends AbstractC59092tx {
    public final AnonymousClass5TV A00;
    public final AbstractC116525Vu A01;

    public C58992tm(AnonymousClass12P r16, C14900mE r17, C15570nT r18, AnonymousClass19Q r19, AnonymousClass19T r20, C37071lG r21, AnonymousClass5TV r22, AbstractC116525Vu r23, C15550nR r24, C22700zV r25, C15610nY r26, AnonymousClass018 r27, UserJid userJid, String str) {
        super(r16, r17, r18, r19, r20, r21, r24, r25, r26, r27, userJid, str);
        this.A01 = r23;
        this.A00 = r22;
        List list = ((AnonymousClass1lL) this).A00;
        list.add(new C84663zg());
        A04(C12980iv.A0C(list));
    }

    @Override // X.AbstractC59092tx, X.AnonymousClass1lK
    public AbstractC75723kJ A0F(ViewGroup viewGroup, int i) {
        if (i != 5) {
            return super.A0F(viewGroup, i);
        }
        Context context = viewGroup.getContext();
        UserJid userJid = ((AnonymousClass1lK) this).A04;
        C15570nT r2 = ((AnonymousClass1lK) this).A01;
        AnonymousClass018 r9 = ((AbstractC59092tx) this).A05;
        C37071lG r4 = ((AnonymousClass1lK) this).A03;
        AnonymousClass19Q r3 = ((AbstractC59092tx) this).A01;
        AbstractC116525Vu r8 = this.A01;
        AnonymousClass5TV r7 = this.A00;
        View A0F = C12960it.A0F(LayoutInflater.from(context), viewGroup, R.layout.business_product_catalog_list_product);
        AnonymousClass23N.A01(A0F);
        return new C59302uQ(A0F, r2, r3, r4, this, this, r7, r8, r9, userJid);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return A0F(viewGroup, i);
    }
}
