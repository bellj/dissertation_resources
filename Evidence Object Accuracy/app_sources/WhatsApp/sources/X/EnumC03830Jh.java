package X;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;

/* renamed from: X.0Jh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03830Jh {
    AUTOMATIC,
    TRUNCATE,
    WRITE_AHEAD_LOGGING;

    public EnumC03830Jh A00(Context context) {
        if (this != AUTOMATIC) {
            return this;
        }
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager == null || (Build.VERSION.SDK_INT >= 19 && activityManager.isLowRamDevice())) {
            return TRUNCATE;
        }
        return WRITE_AHEAD_LOGGING;
    }
}
