package X;

import java.util.ArrayList;

/* renamed from: X.0Xv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07400Xv implements AnonymousClass024 {
    public final /* synthetic */ String A00;

    public C07400Xv(String str) {
        this.A00 = str;
    }

    @Override // X.AnonymousClass024
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        synchronized (AnonymousClass0RT.A02) {
            AnonymousClass00O r1 = AnonymousClass0RT.A01;
            String str = this.A00;
            ArrayList arrayList = (ArrayList) r1.get(str);
            if (arrayList != null) {
                r1.remove(str);
                for (int i = 0; i < arrayList.size(); i++) {
                    ((AnonymousClass024) arrayList.get(i)).accept(obj);
                }
            }
        }
    }
}
