package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.TrafficStats;
import android.net.Uri;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/* renamed from: X.38N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38N extends AbstractC16350or {
    public final Context A00;
    public final Uri A01;
    public final C21990yJ A02;
    public final C63673Cm A03;

    public /* synthetic */ AnonymousClass38N(Context context, Uri uri, C21990yJ r3, C63673Cm r4) {
        this.A02 = r3;
        this.A00 = context;
        this.A01 = uri;
        this.A03 = r4;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AnonymousClass4OI r3;
        String str;
        BufferedInputStream bufferedInputStream;
        File A02;
        long j;
        String path = this.A01.getPath();
        AnonymousClass009.A05(path);
        File file = new File(path);
        C21990yJ r4 = this.A02;
        Context context = this.A00;
        Bitmap A00 = C21990yJ.A00(context, file);
        if (A00 != null) {
            super.A02.A01(new AnonymousClass4OI(1, A00));
        }
        String str2 = file.getName().split("\\.")[0];
        C21980yI r8 = r4.A02;
        File A01 = r8.A01(str2);
        if (A01 != null) {
            Bitmap A002 = C21990yJ.A00(context, A01);
            AnonymousClass009.A05(A002);
            return new AnonymousClass4OI(2, A002);
        }
        try {
            if (!r4.A01.A0B()) {
                AnonymousClass009.A0E(true);
                return new AnonymousClass4OI(3, null);
            }
            try {
                C21970yH r42 = r4.A03;
                if (C12960it.A01(r42.A00.A00) >= 2.0f) {
                    str = "xxhdpi";
                } else {
                    str = "hdpi";
                }
                HashMap A11 = C12970iu.A11();
                A11.put("category", "wallpaper");
                StringBuilder A0j = C12960it.A0j(str2);
                A0j.append("_");
                A11.put("id", C12960it.A0d(str, A0j));
                AbstractC37631mk A003 = r42.A00(r42.A01.A00(), C39461pw.A01(A11));
                try {
                    if (A003 == null) {
                        AnonymousClass009.A0E(true);
                        r3 = new AnonymousClass4OI(4, null);
                    } else {
                        String A0d = C12960it.A0d(".jpg", C12960it.A0j(str2));
                        try {
                            bufferedInputStream = new BufferedInputStream(A003.AAZ(r8.A00, 0, 17));
                            try {
                                A02 = r8.A02(true);
                            } catch (Throwable th) {
                                try {
                                    bufferedInputStream.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        } catch (IOException e) {
                            Log.e("DownloadableWallpaperStorage/storeFullResolutionWallpaper/Failed!", e);
                        }
                        if (A02.exists() || C14350lI.A0O(A02)) {
                            long j2 = 0;
                            File file2 = new File(A02, A0d);
                            int i = DefaultCrypto.BUFFER_SIZE;
                            byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
                            try {
                                FileOutputStream fileOutputStream = new FileOutputStream(file2);
                                while (true) {
                                    j = ((long) i) + j2;
                                    if (j > 2097152) {
                                        break;
                                    }
                                    try {
                                        int read = bufferedInputStream.read(bArr);
                                        if (read == -1) {
                                            break;
                                        }
                                        fileOutputStream.write(bArr, 0, read);
                                        j2 += (long) read;
                                        i = DefaultCrypto.BUFFER_SIZE;
                                    } catch (Throwable th2) {
                                        try {
                                            fileOutputStream.close();
                                        } catch (Throwable unused2) {
                                        }
                                        throw th2;
                                    }
                                }
                                fileOutputStream.close();
                            } catch (FileNotFoundException e2) {
                                Log.e("DownloadableWallpaperStorage/storeFullResolutionWallpaper/", e2);
                            }
                            if (j > 2097152) {
                                Log.e("DownloadableWallpaperStorage: File being saved is too large.");
                            } else {
                                File file3 = new File(r8.A02(false), A0d);
                                C12980iv.A1N(file3);
                                try {
                                    C14350lI.A0B(r8.A02, file2, file3);
                                    bufferedInputStream.close();
                                    File A012 = r8.A01(str2);
                                    AnonymousClass009.A05(A012);
                                    Bitmap A004 = C21990yJ.A00(context, A012);
                                    AnonymousClass009.A05(A004);
                                    r3 = new AnonymousClass4OI(2, A004);
                                } catch (IOException unused3) {
                                    StringBuilder A0h = C12960it.A0h();
                                    A0h.append("DownloadableWallpaperStorage/storeFullResolutionWallpaper : rename failed, from ");
                                    C12970iu.A1V(file2, A0h);
                                    A0h.append(" to ");
                                    Log.e(C12960it.A0d(file3.toString(), A0h));
                                }
                                A003.close();
                            }
                        } else {
                            Log.e("DownloadableWallpaperStorage/store/Could not prepare temporary cache subdirectory");
                        }
                        bufferedInputStream.close();
                        AnonymousClass009.A0E(true);
                        r3 = new AnonymousClass4OI(5, null);
                        A003.close();
                    }
                } catch (Throwable th3) {
                    if (A003 != null) {
                        try {
                            A003.close();
                        } catch (Throwable unused4) {
                        }
                    }
                    throw th3;
                }
            } catch (IOException unused5) {
                AnonymousClass009.A0E(true);
                r3 = new AnonymousClass4OI(4, null);
            }
            return r3;
        } finally {
            TrafficStats.clearThreadStatsTag();
        }
    }
}
