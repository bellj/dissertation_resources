package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.whatsapp.StickyHeadersRecyclerView;

/* renamed from: X.3gS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73573gS extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ StickyHeadersRecyclerView A00;

    public C73573gS(StickyHeadersRecyclerView stickyHeadersRecyclerView) {
        this.A00 = stickyHeadersRecyclerView;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        StickyHeadersRecyclerView stickyHeadersRecyclerView = this.A00;
        return ((AnonymousClass2TX) ((C54412gg) stickyHeadersRecyclerView.A0N).A00).AWm(motionEvent, stickyHeadersRecyclerView.A08, stickyHeadersRecyclerView.A02);
    }
}
