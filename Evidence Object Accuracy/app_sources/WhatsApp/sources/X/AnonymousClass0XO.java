package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import androidx.appcompat.view.menu.ExpandedMenuView;
import com.whatsapp.R;

/* renamed from: X.0XO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XO implements AbstractC12690iL, AdapterView.OnItemClickListener {
    public int A00 = R.layout.abc_list_menu_item_layout;
    public Context A01;
    public LayoutInflater A02;
    public ExpandedMenuView A03;
    public AnonymousClass0BP A04;
    public AnonymousClass07H A05;
    public AbstractC12280hf A06;

    @Override // X.AbstractC12690iL
    public boolean A7P(AnonymousClass07H r2, C07340Xp r3) {
        return false;
    }

    @Override // X.AbstractC12690iL
    public boolean A9n(AnonymousClass07H r2, C07340Xp r3) {
        return false;
    }

    @Override // X.AbstractC12690iL
    public boolean AA1() {
        return false;
    }

    public AnonymousClass0XO(Context context) {
        this.A01 = context;
        this.A02 = LayoutInflater.from(context);
    }

    @Override // X.AbstractC12690iL
    public void AIn(Context context, AnonymousClass07H r3) {
        if (this.A01 != null) {
            this.A01 = context;
            if (this.A02 == null) {
                this.A02 = LayoutInflater.from(context);
            }
        }
        this.A05 = r3;
        AnonymousClass0BP r0 = this.A04;
        if (r0 != null) {
            r0.notifyDataSetChanged();
        }
    }

    @Override // X.AbstractC12690iL
    public void AOF(AnonymousClass07H r2, boolean z) {
        AbstractC12280hf r0 = this.A06;
        if (r0 != null) {
            r0.AOF(r2, z);
        }
    }

    @Override // X.AbstractC12690iL
    public boolean AWs(AnonymousClass0CK r8) {
        if (!r8.hasVisibleItems()) {
            return false;
        }
        AnonymousClass0XM r3 = new AnonymousClass0XM(r8);
        AnonymousClass07H r6 = r3.A02;
        Context context = r6.A0N;
        C004802e r4 = new C004802e(context);
        AnonymousClass0OC r2 = r4.A01;
        AnonymousClass0XO r0 = new AnonymousClass0XO(r2.A0O);
        r3.A01 = r0;
        r0.A06 = r3;
        r6.A08(context, r0);
        AnonymousClass0XO r1 = r3.A01;
        AnonymousClass0BP r02 = r1.A04;
        if (r02 == null) {
            r02 = new AnonymousClass0BP(r1);
            r1.A04 = r02;
        }
        r2.A0D = r02;
        r2.A05 = r3;
        View view = r6.A02;
        if (view != null) {
            r2.A0B = view;
        } else {
            r2.A0A = r6.A01;
            r4.setTitle(r6.A05);
        }
        r2.A08 = r3;
        AnonymousClass04S create = r4.create();
        r3.A00 = create;
        create.setOnDismissListener(r3);
        WindowManager.LayoutParams attributes = r3.A00.getWindow().getAttributes();
        attributes.type = 1003;
        attributes.flags |= C25981Bo.A0F;
        r3.A00.show();
        AbstractC12280hf r03 = this.A06;
        if (r03 == null) {
            return true;
        }
        r03.ATF(r8);
        return true;
    }

    @Override // X.AbstractC12690iL
    public void Abr(AbstractC12280hf r1) {
        this.A06 = r1;
    }

    @Override // X.AbstractC12690iL
    public void AfQ(boolean z) {
        AnonymousClass0BP r0 = this.A04;
        if (r0 != null) {
            r0.notifyDataSetChanged();
        }
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.A05.A0K(this.A04.getItem(i), this, 0);
    }
}
