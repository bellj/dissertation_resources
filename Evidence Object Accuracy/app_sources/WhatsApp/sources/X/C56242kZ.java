package X;

import java.util.HashMap;

/* renamed from: X.2kZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56242kZ extends AbstractC64703Go {
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;

    public final String toString() {
        HashMap A11 = C12970iu.A11();
        A11.put("name", this.A00);
        A11.put("source", this.A01);
        A11.put("medium", this.A02);
        A11.put("keyword", this.A03);
        A11.put("content", this.A04);
        A11.put("id", this.A05);
        A11.put("adNetworkId", this.A06);
        A11.put("gclid", this.A07);
        A11.put("dclid", this.A08);
        return AbstractC64703Go.A01("aclid", this.A09, A11);
    }
}
