package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.22i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C456122i extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C456122i A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public String A04 = "";

    static {
        C456122i r0 = new C456122i();
        A05 = r0;
        r0.A0W();
    }

    public C456122i() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A02 = r1;
        this.A03 = r1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        switch (r8.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                C456122i r10 = (C456122i) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                AbstractC27881Jp r2 = this.A02;
                boolean z2 = true;
                if ((r10.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A02 = r9.Afm(r2, r10.A02, z, z2);
                int i = this.A00;
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str = this.A04;
                int i2 = r10.A00;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A04 = r9.Afy(str, r10.A04, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                AbstractC27881Jp r22 = this.A03;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A03 = r9.Afm(r22, r10.A03, z5, z6);
                int i3 = this.A00;
                boolean z7 = false;
                if ((i3 & 8) == 8) {
                    z7 = true;
                }
                int i4 = this.A01;
                int i5 = r10.A00;
                boolean z8 = false;
                if ((i5 & 8) == 8) {
                    z8 = true;
                }
                this.A01 = r9.Afp(i4, r10.A01, z7, z8);
                if (r9 == C463025i.A00) {
                    this.A00 = i3 | i5;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                this.A00 |= 1;
                                this.A02 = r92.A08();
                            } else if (A03 == 18) {
                                String A0A = r92.A0A();
                                this.A00 |= 2;
                                this.A04 = A0A;
                            } else if (A03 == 26) {
                                this.A00 |= 4;
                                this.A03 = r92.A08();
                            } else if (A03 == 32) {
                                this.A00 |= 8;
                                this.A01 = r92.A02();
                            } else if (!A0a(r92, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C456122i();
            case 5:
                return new C455822f();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C456122i.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A02, 1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A04);
        }
        int i4 = this.A00;
        if ((i4 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A03, 3);
        }
        if ((i4 & 8) == 8) {
            i2 += CodedOutputStream.A04(4, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A02, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
