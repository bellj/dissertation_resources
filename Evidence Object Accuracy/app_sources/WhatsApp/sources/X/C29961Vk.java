package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1Vk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C29961Vk {
    public static volatile C29961Vk A01;
    public final C006202y A00 = new C006202y(1000);

    public static C29961Vk A00() {
        if (A01 == null) {
            synchronized (C29961Vk.class) {
                if (A01 == null) {
                    A01 = new C29961Vk();
                }
            }
        }
        return A01;
    }

    public static final UserJid A01(String str, String str2) {
        switch (str2.hashCode()) {
            case -2070199035:
                if (str2.equals("status_me")) {
                    return C29831Uv.A00;
                }
                break;
            case -1673355044:
                if (str2.equals("s.whatsapp.net")) {
                    if (str.equals("Server")) {
                        return C29891Vd.A00;
                    }
                    if (!str.equals("0")) {
                        return new C27631Ih(str);
                    }
                    return AnonymousClass1VZ.A00;
                }
                break;
            case 107143:
                if (str2.equals("lid")) {
                    return new AnonymousClass1MU(str);
                }
                break;
        }
        throw new AnonymousClass1MW(Jid.buildRawString(str, str2));
    }

    public UserJid A02(String str, String str2) {
        String buildRawString = Jid.buildRawString(str, str2);
        C006202y r2 = this.A00;
        Jid jid = (Jid) r2.A04(buildRawString);
        if (jid instanceof UserJid) {
            return (UserJid) jid;
        }
        UserJid A012 = A01(str, str2);
        r2.A08(buildRawString, A012);
        return A012;
    }
}
