package X;

import android.graphics.Bitmap;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3X5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X5 implements AnonymousClass23D {
    public final /* synthetic */ AbstractC35611iN A00;
    public final /* synthetic */ AnonymousClass4TC A01;
    public final /* synthetic */ View$OnClickListenerC55212hy A02;

    public AnonymousClass3X5(AbstractC35611iN r1, AnonymousClass4TC r2, View$OnClickListenerC55212hy r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        AnonymousClass4TC r3 = this.A01;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(r3.A02);
        A0h.append("-");
        A0h.append(r3.A04);
        A0h.append("-");
        return C12960it.A0f(A0h, r3.A01);
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        View$OnClickListenerC55212hy r2 = this.A02;
        if (r2.A02.getTag() != this) {
            return null;
        }
        Bitmap Aem = this.A00.Aem(r2.A05.A02);
        if (Aem == null) {
            return MediaGalleryFragmentBase.A0U;
        }
        return Aem;
    }
}
