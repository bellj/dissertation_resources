package X;

/* renamed from: X.1fL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33951fL implements Comparable {
    public int A00;
    public long A01;

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C33951fL r8 = (C33951fL) obj;
        if (r8 == this) {
            return 0;
        }
        long j = (long) (this.A00 - r8.A00);
        if (j == 0) {
            j = this.A01 - r8.A01;
        }
        if (j < 0) {
            return -1;
        }
        if (j > 0) {
            return 1;
        }
        return 0;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00);
        sb.append(" ");
        sb.append(this.A01);
        return sb.toString();
    }
}
