package X;

import com.whatsapp.util.Log;

/* renamed from: X.19v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255519v {
    public boolean A00 = false;
    public final C14820m6 A01;

    public C255519v(C14820m6 r2) {
        this.A01 = r2;
    }

    public void A00() {
        boolean z = this.A01.A00.getBoolean("sticker_contextual_suggestion_eligibility", false);
        this.A00 = z;
        Log.i(String.format("StickerContextualSuggestionGatingUtils/initializeIsContextualSuggestionEnabled contextualSuggestionEnabled: %s", Boolean.valueOf(z)));
    }
}
