package X;

import android.content.Context;
import com.whatsapp.storage.StorageUsageGalleryActivity;

/* renamed from: X.4r1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103604r1 implements AbstractC009204q {
    public final /* synthetic */ StorageUsageGalleryActivity A00;

    public C103604r1(StorageUsageGalleryActivity storageUsageGalleryActivity) {
        this.A00 = storageUsageGalleryActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
