package X;

import android.graphics.PointF;
import android.view.View;

/* renamed from: X.0FI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FI extends AnonymousClass0FB {
    public AbstractC06220Sq A00;
    public AbstractC06220Sq A01;

    @Override // X.AnonymousClass0FB
    public int A00(AnonymousClass02H r11, int i, int i2) {
        int A07;
        View A01;
        int A02;
        int i3;
        PointF A7U;
        int i4;
        int i5;
        if (!(!(r11 instanceof AnonymousClass02I) || (A07 = r11.A07()) == 0 || (A01 = A01(r11)) == null || (A02 = AnonymousClass02H.A02(A01)) == -1 || (A7U = ((AnonymousClass02I) r11).A7U(A07 - 1)) == null)) {
            int i6 = 0;
            if (r11.A14()) {
                AbstractC06220Sq r1 = this.A00;
                if (r1 == null || r1.A02 != r11) {
                    r1 = new AnonymousClass0Ez(r11);
                    this.A00 = r1;
                }
                i4 = A05(r1, r11, i, 0);
                if (A7U.x < 0.0f) {
                    i4 = -i4;
                }
            } else {
                i4 = 0;
            }
            if (r11.A15()) {
                AbstractC06220Sq r12 = this.A01;
                if (r12 == null || r12.A02 != r11) {
                    r12 = new AnonymousClass0F0(r11);
                    this.A01 = r12;
                }
                i5 = A05(r12, r11, 0, i2);
                if (A7U.y < 0.0f) {
                    i5 = -i5;
                }
            } else {
                i5 = 0;
            }
            if (r11.A15()) {
                i4 = i5;
            }
            if (i4 != 0) {
                int i7 = A02 + i4;
                if (i7 >= 0) {
                    i6 = i7;
                }
                if (i6 < A07) {
                    return i6;
                }
                return i3;
            }
        }
        return -1;
    }

    @Override // X.AnonymousClass0FB
    public View A01(AnonymousClass02H r10) {
        AbstractC06220Sq r6;
        int A01;
        if (r10.A15()) {
            r6 = this.A01;
            if (r6 == null || r6.A02 != r10) {
                r6 = new AnonymousClass0F0(r10);
                this.A01 = r6;
            }
        } else if (!r10.A14()) {
            return null;
        } else {
            r6 = this.A00;
            if (r6 == null || r6.A02 != r10) {
                r6 = new AnonymousClass0Ez(r10);
                this.A00 = r6;
            }
        }
        int A06 = r10.A06();
        View view = null;
        if (A06 == 0) {
            return null;
        }
        if (r10.A0S()) {
            A01 = r6.A06() + (r6.A07() >> 1);
        } else {
            A01 = r6.A01() >> 1;
        }
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < A06; i2++) {
            View A0D = r10.A0D(i2);
            int abs = Math.abs((r6.A0B(A0D) + (r6.A09(A0D) >> 1)) - A01);
            if (abs < i) {
                view = A0D;
                i = abs;
            }
        }
        return view;
    }

    @Override // X.AnonymousClass0FB
    public int[] A04(View view, AnonymousClass02H r9) {
        int A01;
        int A012;
        int[] iArr = new int[2];
        if (r9.A14()) {
            AbstractC06220Sq r3 = this.A00;
            if (r3 == null || r3.A02 != r9) {
                r3 = new AnonymousClass0Ez(r9);
                this.A00 = r3;
            }
            int A0B = r3.A0B(view) + (r3.A09(view) >> 1);
            if (r9.A0S()) {
                A012 = r3.A06() + (r3.A07() >> 1);
            } else {
                A012 = r3.A01() >> 1;
            }
            iArr[0] = A0B - A012;
        } else {
            iArr[0] = 0;
        }
        if (r9.A15()) {
            AbstractC06220Sq r32 = this.A01;
            if (r32 == null || r32.A02 != r9) {
                r32 = new AnonymousClass0F0(r9);
                this.A01 = r32;
            }
            int A0B2 = r32.A0B(view) + (r32.A09(view) >> 1);
            if (r9.A0S()) {
                A01 = r32.A06() + (r32.A07() >> 1);
            } else {
                A01 = r32.A01() >> 1;
            }
            iArr[1] = A0B2 - A01;
            return iArr;
        }
        iArr[1] = 0;
        return iArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x007e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A05(X.AbstractC06220Sq r12, X.AnonymousClass02H r13, int r14, int r15) {
        /*
            r11 = this;
            r0 = 2
            int[] r1 = new int[r0]
            android.widget.Scroller r2 = r11.A00
            r3 = 0
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 2147483647(0x7fffffff, float:NaN)
            r4 = 0
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
            r10 = 2147483647(0x7fffffff, float:NaN)
            r5 = r14
            r6 = r15
            r2.fling(r3, r4, r5, r6, r7, r8, r9, r10)
            android.widget.Scroller r0 = r11.A00
            int r0 = r0.getFinalX()
            r1[r3] = r0
            android.widget.Scroller r0 = r11.A00
            int r2 = r0.getFinalY()
            r0 = 1
            r1[r0] = r2
            int r10 = r13.A06()
            r9 = 1065353216(0x3f800000, float:1.0)
            if (r10 == 0) goto L_0x004f
            r8 = 0
            r7 = 0
            r4 = r7
            r6 = 2147483647(0x7fffffff, float:NaN)
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
        L_0x0037:
            if (r8 >= r10) goto L_0x0052
            android.view.View r3 = r13.A0D(r8)
            int r2 = X.AnonymousClass02H.A02(r3)
            r0 = -1
            if (r2 == r0) goto L_0x004c
            if (r2 >= r6) goto L_0x0048
            r7 = r3
            r6 = r2
        L_0x0048:
            if (r2 <= r5) goto L_0x004c
            r4 = r3
            r5 = r2
        L_0x004c:
            int r8 = r8 + 1
            goto L_0x0037
        L_0x004f:
            r4 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0078
        L_0x0052:
            if (r7 == 0) goto L_0x004f
            if (r4 == 0) goto L_0x004f
            int r2 = r12.A0B(r7)
            int r0 = r12.A0B(r4)
            int r3 = java.lang.Math.min(r2, r0)
            int r2 = r12.A08(r7)
            int r0 = r12.A08(r4)
            int r0 = java.lang.Math.max(r2, r0)
            int r0 = r0 - r3
            if (r0 == 0) goto L_0x004f
            float r4 = (float) r0
            float r4 = r4 * r9
            int r5 = r5 - r6
            int r0 = r5 + 1
            float r0 = (float) r0
            float r4 = r4 / r0
        L_0x0078:
            r2 = 0
            r0 = 0
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x007f
            return r2
        L_0x007f:
            r3 = r1[r2]
            int r2 = java.lang.Math.abs(r3)
            r0 = 1
            r1 = r1[r0]
            int r0 = java.lang.Math.abs(r1)
            if (r2 > r0) goto L_0x008f
            r3 = r1
        L_0x008f:
            float r0 = (float) r3
            float r0 = r0 / r4
            int r0 = java.lang.Math.round(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0FI.A05(X.0Sq, X.02H, int, int):int");
    }
}
