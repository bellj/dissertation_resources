package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.StatusesFragment;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33681eu extends BaseAdapter implements Filterable {
    public long A00 = 4;
    public Filter A01;
    public final Map A02 = new HashMap();
    public final /* synthetic */ StatusesFragment A03;

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 3;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    public /* synthetic */ C33681eu(StatusesFragment statusesFragment) {
        this.A03 = statusesFragment;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A03.A0v.size();
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        Filter filter = this.A01;
        if (filter != null) {
            return filter;
        }
        C52892br r1 = new C52892br(this.A03);
        this.A01 = r1;
        return r1;
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A03.A0v.get(i);
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        AbstractC116185Ul r1 = (AbstractC116185Ul) this.A03.A0v.get(i);
        if (r1 instanceof C69643a0) {
            UserJid userJid = ((C69643a0) r1).A01.A0A;
            Map map = this.A02;
            Number number = (Number) map.get(userJid);
            if (number == null) {
                long j = this.A00;
                this.A00 = 1 + j;
                number = Long.valueOf(j);
                map.put(userJid, number);
            }
            return number.longValue();
        } else if (r1 instanceof C69633Zz) {
            return ((C69633Zz) r1).A00;
        } else {
            if (r1 instanceof C69653a1) {
                return 3;
            }
            throw new UnsupportedOperationException("Each list item must have an id");
        }
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getItemViewType(int i) {
        Object obj = this.A03.A0v.get(i);
        if (obj instanceof C69643a0) {
            return 0;
        }
        if (obj instanceof C69633Zz) {
            return 1;
        }
        if (obj instanceof C69653a1) {
            return 2;
        }
        throw new UnsupportedOperationException("Each list item type must have a itemType");
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        StatusesFragment statusesFragment = this.A03;
        return ((AbstractC116185Ul) statusesFragment.A0v.get(i)).AHa(statusesFragment.A0p(), view, viewGroup, statusesFragment.A0G, statusesFragment.A1B, statusesFragment.A1C, statusesFragment.A1A, statusesFragment.A0u, statusesFragment.A10);
    }
}
