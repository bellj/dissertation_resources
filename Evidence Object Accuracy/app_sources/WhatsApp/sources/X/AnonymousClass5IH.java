package X;

import java.io.IOException;
import java.util.HashMap;

/* renamed from: X.5IH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IH extends HashMap<String, Object> implements AnonymousClass5ZZ, AbstractC117225Za {
    public static final long serialVersionUID = -503443796854799292L;

    @Override // X.AnonymousClass5VJ
    public String Aeu() {
        C94884ch r2 = AnonymousClass4ZZ.A00;
        StringBuilder A0h = C12960it.A0h();
        try {
            C94904cj.A06.AgH(A0h, this, r2);
        } catch (IOException unused) {
        }
        return A0h.toString();
    }

    @Override // X.AnonymousClass5ZZ
    public String Aev(C94884ch r3) {
        StringBuilder A0h = C12960it.A0h();
        try {
            C94904cj.A06.AgH(A0h, this, r3);
        } catch (IOException unused) {
        }
        return A0h.toString();
    }

    @Override // X.AnonymousClass5VK
    public void AgF(Appendable appendable) {
        C94904cj.A06.AgH(appendable, this, AnonymousClass4ZZ.A00);
    }

    @Override // X.AbstractC117225Za
    public void AgG(Appendable appendable, C94884ch r3) {
        C94904cj.A06.AgH(appendable, this, r3);
    }

    @Override // java.util.AbstractMap, java.lang.Object
    public String toString() {
        C94884ch r2 = AnonymousClass4ZZ.A00;
        StringBuilder A0h = C12960it.A0h();
        try {
            C94904cj.A06.AgH(A0h, this, r2);
        } catch (IOException unused) {
        }
        return A0h.toString();
    }
}
