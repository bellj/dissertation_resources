package X;

import android.app.Notification;

/* renamed from: X.0Qe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05600Qe {
    public static void A00(Notification.BigPictureStyle bigPictureStyle) {
        bigPictureStyle.setContentDescription(null);
    }

    public static void A01(Notification.BigPictureStyle bigPictureStyle) {
        bigPictureStyle.showBigPictureWhenCollapsed(false);
    }
}
