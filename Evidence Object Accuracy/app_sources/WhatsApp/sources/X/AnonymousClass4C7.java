package X;

/* renamed from: X.4C7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4C7 extends Exception {
    public Throwable cause;

    public AnonymousClass4C7(String str, Throwable th) {
        super(str);
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
