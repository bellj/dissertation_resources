package X;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.widget.EditText;
import com.whatsapp.R;
import com.whatsapp.emoji.EmojiDescriptor;

/* renamed from: X.1kL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC36671kL {
    public static boolean A00;

    public static SpannableStringBuilder A00(Context context, Paint paint, AnonymousClass19M r5, AbstractC32661cW r6, CharSequence charSequence, float f) {
        C64583Gc r0;
        if (paint == null) {
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.dialog_emoji_size);
            Paint paint2 = new Paint();
            paint2.setTextSize((float) dimensionPixelSize);
            r0 = new C64583Gc(paint2, 1.0f);
        } else {
            r0 = new C64583Gc(paint, f);
        }
        return A01(context, r5, r6, r0, charSequence);
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00cb A[EDGE_INSN: B:40:0x00cb->B:35:0x00cb ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e8 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.text.SpannableStringBuilder A01(android.content.Context r20, X.AnonymousClass19M r21, X.AbstractC32661cW r22, X.C64583Gc r23, java.lang.CharSequence r24) {
        /*
        // Method dump skipped, instructions count: 236
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC36671kL.A01(android.content.Context, X.19M, X.1cW, X.3Gc, java.lang.CharSequence):android.text.SpannableStringBuilder");
    }

    public static CharSequence A02(Context context, Paint paint, AnonymousClass19M r8, AbstractC32661cW r9, CharSequence charSequence) {
        SpannableStringBuilder A002 = A00(context, paint, r8, r9, charSequence, 1.0f);
        int ADt = r9.ADt();
        if (ADt > 0) {
            if (A002 == null) {
                A002 = new SpannableStringBuilder(charSequence);
            }
            SpannableStringBuilder delete = A002.delete(ADt + (Character.charCount(Character.codePointAt(charSequence, ADt - 1)) - 1), A002.length());
            delete.append((CharSequence) "…");
            return delete;
        } else if (A002 != null) {
            return A002;
        } else {
            return charSequence;
        }
    }

    public static CharSequence A03(Context context, Paint paint, AnonymousClass19M r8, CharSequence charSequence) {
        if (charSequence == null) {
            return null;
        }
        SpannableStringBuilder A002 = A00(context, paint, r8, null, charSequence, 1.0f);
        if (A002 != null) {
            return A002;
        }
        return charSequence;
    }

    public static CharSequence A04(Context context, Paint paint, AnonymousClass19M r7, CharSequence charSequence, float f) {
        if (charSequence == null) {
            return null;
        }
        SpannableStringBuilder A002 = A00(context, paint, r7, null, charSequence, f);
        return A002 != null ? A002 : charSequence;
    }

    public static CharSequence A05(Context context, AnonymousClass19M r4, CharSequence charSequence) {
        if (charSequence == null) {
            return null;
        }
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.dialog_emoji_size);
        Paint paint = new Paint();
        paint.setTextSize((float) dimensionPixelSize);
        SpannableStringBuilder A01 = A01(context, r4, null, new C64583Gc(paint, 1.0f), charSequence);
        if (A01 != null) {
            return A01;
        }
        return charSequence;
    }

    public static String A06(int[] iArr) {
        int length = iArr.length;
        StringBuilder sb = new StringBuilder(length);
        for (int i : iArr) {
            if (AbstractC32691cZ.A00 && i >= 126980 && i <= 129782) {
                i &= 65535;
            }
            sb.append(Character.toChars(i));
        }
        return sb.toString();
    }

    public static void A07(Context context, Paint paint, Editable editable, AnonymousClass19M r14, float f) {
        Drawable A05;
        Object imageSpan;
        Paint.FontMetricsInt A002;
        if (!A00) {
            A00 = true;
            String obj = editable.toString();
            int length = obj.length();
            for (ImageSpan imageSpan2 : (ImageSpan[]) editable.getSpans(0, length, ImageSpan.class)) {
                editable.removeSpan(imageSpan2);
            }
            if (AbstractC32691cZ.A00) {
                int i = 0;
                int i2 = 0;
                while (i < length) {
                    int codePointAt = obj.codePointAt(i);
                    int charCount = Character.charCount(codePointAt);
                    if (charCount > 1 && codePointAt >= 126980 && codePointAt <= 129782) {
                        int i3 = i2 + charCount;
                        if (codePointAt >= 126980 && codePointAt <= 129782) {
                            codePointAt &= 65535;
                        }
                        editable.replace(i2, i3, new String(Character.toChars(codePointAt)));
                        i2 -= charCount - 1;
                    }
                    i += charCount;
                    i2 += charCount;
                }
                obj = editable.toString();
            }
            C32681cY r4 = new C32681cY(obj);
            int length2 = obj.length();
            int i4 = 0;
            while (i4 < length2) {
                r4.A00 = i4;
                long A003 = EmojiDescriptor.A00(r4, false);
                int A01 = r4.A01(i4, A003);
                if (!(A003 == -1 || (A05 = r14.A05(context.getResources(), r4, A003)) == null)) {
                    if (paint == null || (A002 = AnonymousClass3I9.A00(paint)) == null) {
                        A05.setBounds(0, 0, (int) (context.getResources().getDisplayMetrics().scaledDensity * 22.0f), (int) (context.getResources().getDisplayMetrics().scaledDensity * 22.0f));
                        imageSpan = new ImageSpan(A05, 0);
                    } else {
                        int textSize = (int) ((paint.getTextSize() * f) + 0.5f);
                        A05.setBounds(0, 0, textSize, textSize);
                        imageSpan = new AnonymousClass36k(context, A002, A05, obj.substring(i4, i4 + A01));
                    }
                    editable.setSpan(imageSpan, i4, i4 + A01, 33);
                }
                i4 += A01;
            }
            A00 = false;
        }
    }

    public static void A08(EditText editText, int[] iArr, int i) {
        int charCount;
        int max = Math.max(0, editText.getSelectionStart());
        int max2 = Math.max(0, editText.getSelectionEnd());
        if (max > max2) {
            max2 = max;
            max = max2;
        }
        if (i > 0) {
            StringBuilder sb = new StringBuilder(editText.getText().toString());
            sb.replace(max, max2, A06(iArr));
            if (AnonymousClass2VC.A00(sb) > i) {
                return;
            }
        }
        editText.getText().replace(max, max2, A06(iArr));
        int i2 = 0;
        for (int i3 : iArr) {
            if (AbstractC32691cZ.A00) {
                charCount = 1;
            } else {
                charCount = Character.charCount(i3);
            }
            i2 += charCount;
        }
        if (max <= editText.length() - i2) {
            editText.setSelection(max + i2);
        }
    }
}
