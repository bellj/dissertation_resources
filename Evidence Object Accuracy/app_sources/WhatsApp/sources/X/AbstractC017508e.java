package X;

import android.graphics.drawable.Drawable;
import android.view.View;

/* renamed from: X.08e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC017508e {
    void A93();

    void Ac3(View view);

    void Ac5(int i);

    void AcM(Drawable drawable);

    void Acw(CharSequence charSequence);

    void Ad1(CharSequence charSequence);

    void setWindowTitle(CharSequence charSequence);
}
