package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.payments.ui.widget.NoviPayHubBalanceView;

/* renamed from: X.5cR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118825cR extends AnonymousClass03U {
    public AbstractC118825cR(View view) {
        super(view);
    }

    public void A08(AbstractC125975s7 r10, int i) {
        TextView textView;
        CharSequence charSequence;
        Button button;
        View.OnClickListener onClickListener;
        int i2;
        String str;
        IDxCListenerShape2S0200000_3_I1 iDxCListenerShape2S0200000_3_I1;
        AnonymousClass0B6 r0;
        int i3;
        String A0X;
        if (this instanceof C122325lH) {
            textView = ((C122325lH) this).A00;
            charSequence = ((C123015mT) r10).A00;
        } else if (this instanceof C122405lP) {
            C122405lP r4 = (C122405lP) this;
            TextView textView2 = r4.A01;
            textView2.setText(((C123005mS) r10).A00);
            Resources resources = r4.A0H.getResources();
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(textView2);
            r4.A00.setVisibility(8);
            A0H.bottomMargin = resources.getDimensionPixelSize(R.dimen.payment_common_centered_title_bottom_margin_no_subtitle);
            return;
        } else if (!(this instanceof C122445lT)) {
            if (this instanceof C122435lS) {
                C122435lS r2 = (C122435lS) this;
                C122865mE r102 = (C122865mE) r10;
                r2.A01.setText(r102.A01);
                String str2 = r102.A02;
                TextView textView3 = r2.A02;
                if (str2 != null) {
                    textView3.setText(str2);
                    i2 = 0;
                } else {
                    i2 = 8;
                }
                textView3.setVisibility(i2);
                View view = r2.A00;
                onClickListener = r102.A00;
                button = view;
            } else if (this instanceof C122395lO) {
                C122395lO r22 = (C122395lO) this;
                C122855mD r103 = (C122855mD) r10;
                r22.A00.setText(r103.A01);
                TextView textView4 = r22.A01;
                textView4.setText(r103.A02);
                if (r103.A00 == 2) {
                    C42941w9.A03(textView4);
                    return;
                } else {
                    C42941w9.A04(textView4);
                    return;
                }
            } else if (this instanceof C122385lN) {
                C122845mC r104 = (C122845mC) r10;
                LinearLayout linearLayout = ((C122385lN) this).A00;
                linearLayout.removeAllViews();
                for (C127025tp r42 : r104.A01) {
                    AnonymousClass6M7 r3 = r104.A00;
                    C123645na r23 = new C123645na(linearLayout.getContext());
                    if (r42 != null) {
                        AbstractC28901Pl r6 = r42.A01;
                        C1311161i.A0A(r6, r23);
                        if (r6 instanceof C30861Zc) {
                            r23.A05.setText(C1311161i.A03(r23.getContext(), (C30861Zc) r6));
                        } else if (r6 instanceof C30881Ze) {
                            C30881Ze r62 = (C30881Ze) r6;
                            r23.A05.setText(C1311161i.A05(r23.getContext(), r62));
                            r23.A02(C1311161i.A04(r23.getContext(), r62));
                        }
                        C117305Zk.A15(r23, R.id.account_number_divider);
                        C117295Zj.A0o(r23, r3, r42, 33);
                    }
                    linearLayout.addView(r23);
                }
                return;
            } else if (this instanceof C122375lM) {
                C122845mC r105 = (C122845mC) r10;
                LinearLayout linearLayout2 = ((C122375lM) this).A00;
                linearLayout2.removeAllViews();
                for (C127025tp r43 : r105.A01) {
                    AnonymousClass6M7 r32 = r105.A00;
                    C123655nb r24 = new C123655nb(linearLayout2.getContext());
                    if (r43 != null) {
                        boolean z = true;
                        AbstractC28901Pl r7 = r43.A01;
                        C1311161i.A0A(r7, r24);
                        if (r7 instanceof C30861Zc) {
                            r24.A05.setText(C1311161i.A03(r24.getContext(), (C30861Zc) r7));
                        } else if (r7 instanceof C30881Ze) {
                            C30881Ze r72 = (C30881Ze) r7;
                            r24.A05.setText(C1311161i.A05(r24.getContext(), r72));
                            r24.A02(C1311161i.A04(r24.getContext(), r72));
                            AbstractC30871Zd r02 = (AbstractC30871Zd) r72.A08;
                            if (!(r02 == null || (str = r02.A0I) == null)) {
                                z = "ACTIVE".equals(str);
                                r24.A03(z);
                            }
                        }
                        r24.A00.setChecked(r43.A00);
                        C117305Zk.A15(r24, R.id.account_number_divider);
                        if (z) {
                            iDxCListenerShape2S0200000_3_I1 = new IDxCListenerShape2S0200000_3_I1(r32, 32, r43);
                        } else {
                            iDxCListenerShape2S0200000_3_I1 = null;
                        }
                        r24.setOnClickListener(iDxCListenerShape2S0200000_3_I1);
                    }
                    linearLayout2.addView(r24);
                }
                return;
            } else if (this instanceof C122365lL) {
                C122365lL r5 = (C122365lL) this;
                C122935mL r106 = (C122935mL) r10;
                WaImageView waImageView = r5.A01;
                View view2 = r5.A0H;
                waImageView.setImageDrawable(AnonymousClass2GE.A01(view2.getContext(), r106.A00, r106.A01));
                waImageView.A01 = r106.A06;
                waImageView.invalidate();
                r5.A00.setText(r106.A04);
                if (r106.A05) {
                    view2.setVisibility(0);
                    view2.setOnClickListener(r106.A02);
                    r0 = new AnonymousClass0B6(-1, -2);
                } else {
                    view2.setVisibility(8);
                    r0 = new AnonymousClass0B6(0, 0);
                }
                view2.setLayoutParams(r0);
                if (r106.A03 != null) {
                    ViewGroup.MarginLayoutParams A0H2 = C12970iu.A0H(view2);
                    ViewGroup.MarginLayoutParams marginLayoutParams = r106.A03;
                    A0H2.setMargins(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
                    return;
                }
                return;
            } else if (this instanceof C122315lG) {
                textView = ((C122315lG) this).A00;
                charSequence = ((C122865mE) r10).A01;
            } else if (this instanceof C122305lF) {
                C122885mG r107 = (C122885mG) r10;
                NoviPayHubBalanceView noviPayHubBalanceView = ((C122305lF) this).A00;
                int i4 = r107.A00;
                if (i4 == 1) {
                    noviPayHubBalanceView.A00.setVisibility(0);
                    noviPayHubBalanceView.A02.setText(r107.A02);
                    noviPayHubBalanceView.A04.A01();
                    noviPayHubBalanceView.A05.A01();
                    noviPayHubBalanceView.A03.setText(r107.A03);
                    noviPayHubBalanceView.A01.setVisibility(8);
                    return;
                } else if (i4 == 2) {
                    noviPayHubBalanceView.A00.setVisibility(8);
                    noviPayHubBalanceView.A01.setVisibility(0);
                    LinearLayout linearLayout3 = noviPayHubBalanceView.A01;
                    onClickListener = r107.A01;
                    button = linearLayout3;
                } else if (i4 == 3) {
                    noviPayHubBalanceView.A04.A00();
                    noviPayHubBalanceView.A05.A00();
                    noviPayHubBalanceView.A00.setVisibility(0);
                    noviPayHubBalanceView.A01.setVisibility(8);
                    noviPayHubBalanceView.A02.setText(r107.A02);
                    textView = noviPayHubBalanceView.A03;
                    charSequence = r107.A03;
                } else {
                    return;
                }
            } else if (this instanceof C122465lV) {
                C122465lV r25 = (C122465lV) this;
                C122915mJ r108 = (C122915mJ) r10;
                r25.A01.setImageResource(r108.A00);
                r25.A04.setText(r108.A03);
                r25.A03.setText(r108.A02);
                r25.A02.setOnClickListener(r108.A01);
                boolean z2 = r108.A04;
                ImageView imageView = r25.A00;
                if (z2) {
                    View view3 = r25.A0H;
                    imageView.setImageDrawable(AnonymousClass2GE.A04(C12960it.A09(view3).getDrawable(R.drawable.ic_add_payment_method_plus_sign), C12960it.A09(view3).getColor(R.color.fb_pay_hub_icon_tint)));
                    i3 = 0;
                } else {
                    i3 = 8;
                }
                imageView.setVisibility(i3);
                return;
            } else if (this instanceof C122425lR) {
                C122425lR r44 = (C122425lR) this;
                C123255mr r109 = (C123255mr) r10;
                r44.A01.setImageDrawable(AnonymousClass2GE.A01(r44.A0H.getContext(), r109.A00, r109.A01));
                r44.A02.setText(r109.A04);
                Button button2 = r44.A00;
                button2.setText(r109.A03);
                onClickListener = r109.A02;
                button = button2;
            } else if (this instanceof C122415lQ) {
                C122415lQ r26 = (C122415lQ) this;
                C123175mj r1010 = (C123175mj) r10;
                r26.A01.setText(r1010.A00);
                r26.A02.setText(r1010.A01);
                r26.A00.setVisibility(0);
                return;
            } else if (this instanceof C122355lK) {
                C122355lK r33 = (C122355lK) this;
                r33.A01.setText(((C123035mV) r10).A00);
                AnonymousClass2GE.A05(r33.A0H.getContext(), r33.A00, R.color.fb_pay_hub_icon_tint);
                return;
            } else if (this instanceof C122455lU) {
                C122455lU r73 = (C122455lU) this;
                C123305mw r1011 = (C123305mw) r10;
                byte[] bArr = r1011.A09;
                if (bArr != null) {
                    r73.A01.setImageBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
                }
                C117315Zl.A0N(r73.A04, C117295Zj.A0R(r1011.A03));
                String str3 = r1011.A04;
                if (str3 != null) {
                    r73.A03.setText(str3);
                }
                TextView textView5 = r73.A03;
                int i5 = 0;
                if (r1011.A04 == null) {
                    i5 = 8;
                }
                textView5.setVisibility(i5);
                if (r1011.A08) {
                    LinearLayout linearLayout4 = r73.A02;
                    linearLayout4.setVisibility(0);
                    ImageView A0K = C12970iu.A0K(linearLayout4, R.id.warning_icon);
                    TextView A0I = C12960it.A0I(linearLayout4, R.id.warning_message);
                    A0K.setImageDrawable(AnonymousClass2GE.A01(r73.A0H.getContext(), r1011.A00, r1011.A01));
                    A0I.setText(r1011.A06);
                    if (r1011.A07) {
                        Button button3 = r73.A00;
                        button3.setVisibility(0);
                        button3.setText(r1011.A05);
                        onClickListener = r1011.A02;
                        button = button3;
                    } else {
                        r73.A00.setVisibility(8);
                        return;
                    }
                } else {
                    r73.A02.setVisibility(8);
                    return;
                }
            } else if (!(this instanceof C122285lD)) {
                if (this instanceof C122345lJ) {
                    C122345lJ r27 = (C122345lJ) this;
                    C122825mA r1012 = (C122825mA) r10;
                    r27.A01.setText(r1012.A02);
                    r27.A00.A0E(Html.fromHtml(r1012.A01));
                    View view4 = r27.A0H;
                    onClickListener = r1012.A00;
                    button = view4;
                } else if (!(this instanceof C122295lE)) {
                    C122335lI r52 = (C122335lI) this;
                    C123215mn r1013 = (C123215mn) r10;
                    ImageView imageView2 = r52.A00;
                    View view5 = r52.A0H;
                    imageView2.setImageDrawable(AnonymousClass2GE.A01(view5.getContext(), r1013.A00, r1013.A01));
                    r52.A01.setText(r1013.A03);
                    view5.setOnClickListener(r1013.A02);
                    return;
                } else {
                    C122295lE r28 = (C122295lE) this;
                    C122815m9 r1014 = (C122815m9) r10;
                    r28.A0H.setOnClickListener(r1014.A00);
                    boolean isEmpty = TextUtils.isEmpty(r1014.A02);
                    TextView textView6 = r28.A00;
                    if (isEmpty) {
                        A0X = r1014.A01;
                    } else {
                        Context context = textView6.getContext();
                        Object[] A1a = C12980iv.A1a();
                        A1a[0] = r1014.A01;
                        A0X = C12960it.A0X(context, r1014.A02, A1a, 1, R.string.upi_mandate_transaction_detail_recurring_instance_transaction_info);
                    }
                    textView6.setText(A0X);
                    return;
                }
            } else {
                return;
            }
            button.setOnClickListener(onClickListener);
            return;
        } else {
            C122445lT r34 = (C122445lT) this;
            C122895mH r1015 = (C122895mH) r10;
            r34.A02.setText(r1015.A02);
            TextView textView7 = r34.A01;
            textView7.setText(r1015.A01);
            textView7.setLinksClickable(true);
            C12990iw.A1F(textView7);
            View view6 = r34.A00;
            view6.setOnClickListener(r1015.A00);
            r34.A03.setChecked(r1015.A03);
            view6.setTag(Integer.valueOf(i));
            return;
        }
        textView.setText(charSequence);
    }
}
