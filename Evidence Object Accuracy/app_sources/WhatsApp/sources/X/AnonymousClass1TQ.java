package X;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;

/* renamed from: X.1TQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1TQ {
    public static final long A00 = Runtime.getRuntime().maxMemory();

    public static int A00(int i) {
        int i2 = 1;
        if (i > 127) {
            int i3 = 1;
            while (true) {
                i >>>= 8;
                if (i == 0) {
                    break;
                }
                i3++;
            }
            for (int i4 = (i3 - 1) << 3; i4 >= 0; i4 -= 8) {
                i2++;
            }
        }
        return i2;
    }

    public static int A01(int i) {
        if (i < 31) {
            return 1;
        }
        if (i < 128) {
            return 2;
        }
        int i2 = 4;
        do {
            i >>= 7;
            i2--;
        } while (i > 127);
        return 1 + (5 - i2);
    }

    public static int A02(InputStream inputStream) {
        FileChannel channel;
        long size;
        if (inputStream instanceof AnonymousClass49A) {
            return ((AnonymousClass49A) inputStream).A00;
        }
        if (inputStream instanceof AnonymousClass1TO) {
            return ((AnonymousClass1TO) inputStream).A00;
        }
        if (inputStream instanceof ByteArrayInputStream) {
            return inputStream.available();
        }
        if (inputStream instanceof FileInputStream) {
            try {
                channel = ((FileInputStream) inputStream).getChannel();
            } catch (IOException unused) {
            }
            if (channel != null) {
                size = channel.size();
                if (size < 2147483647L) {
                    return (int) size;
                }
            }
        }
        size = A00;
        if (size > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) size;
    }
}
