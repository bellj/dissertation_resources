package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;
import com.whatsapp.stickers.StickerStoreMyTabFragment;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;
import com.whatsapp.stickers.StickerStoreTabFragment;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.12c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C235512c {
    public C38721ob A00;
    public byte[] A01;
    public final AbstractC15710nm A02;
    public final C14330lG A03;
    public final C14900mE A04;
    public final C002701f A05;
    public final C18790t3 A06;
    public final C18720su A07;
    public final C14830m7 A08;
    public final C16590pI A09;
    public final C14820m6 A0A;
    public final AnonymousClass11Z A0B;
    public final AnonymousClass0o6 A0C;
    public final AnonymousClass140 A0D;
    public final C14850m9 A0E;
    public final C16120oU A0F;
    public final C21780xy A0G;
    public final C18810t5 A0H;
    public final AnonymousClass148 A0I;
    public final AnonymousClass12J A0J;
    public final C22230yk A0K;
    public final C18190s3 A0L;
    public final C235812f A0M;
    public final AnonymousClass149 A0N;
    public final AnonymousClass15H A0O;
    public final AnonymousClass146 A0P;
    public final AnonymousClass15I A0Q;
    public final C232711a A0R;
    public final C232911c A0S;
    public final AnonymousClass147 A0T;
    public final AnonymousClass15J A0U;
    public final AnonymousClass14B A0V;
    public final C235412b A0W;
    public final C15830ny A0X;
    public final AbstractC14440lR A0Y;

    public C235512c(AbstractC15710nm r2, C14330lG r3, C14900mE r4, C002701f r5, C18790t3 r6, C18720su r7, C14830m7 r8, C16590pI r9, C14820m6 r10, AnonymousClass11Z r11, AnonymousClass0o6 r12, AnonymousClass140 r13, C14850m9 r14, C16120oU r15, C21780xy r16, C18810t5 r17, AnonymousClass148 r18, AnonymousClass12J r19, C22230yk r20, C18190s3 r21, C235812f r22, AnonymousClass149 r23, AnonymousClass15H r24, AnonymousClass146 r25, AnonymousClass15I r26, C232711a r27, C232911c r28, AnonymousClass147 r29, AnonymousClass15J r30, AnonymousClass14B r31, C235412b r32, C15830ny r33, AbstractC14440lR r34) {
        this.A09 = r9;
        this.A08 = r8;
        this.A07 = r7;
        this.A0E = r14;
        this.A04 = r4;
        this.A02 = r2;
        this.A0Y = r34;
        this.A03 = r3;
        this.A06 = r6;
        this.A0F = r15;
        this.A0M = r22;
        this.A0K = r20;
        this.A0W = r32;
        this.A0P = r25;
        this.A0J = r19;
        this.A0H = r17;
        this.A0A = r10;
        this.A0V = r31;
        this.A0B = r11;
        this.A0O = r24;
        this.A0I = r18;
        this.A0T = r29;
        this.A0Q = r26;
        this.A0D = r13;
        this.A0L = r21;
        this.A05 = r5;
        this.A0S = r28;
        this.A0G = r16;
        this.A0X = r33;
        this.A0R = r27;
        this.A0U = r30;
        this.A0N = r23;
        this.A0C = r12;
    }

    public static final void A00(AnonymousClass148 r2, AnonymousClass1KZ r3) {
        for (AnonymousClass1KS r0 : r3.A04) {
            r2.A01(r0.A0C);
        }
        r2.A01(r3.A0D);
    }

    public Pair A01(AnonymousClass1KS r6) {
        IOException e;
        String str;
        AnonymousClass1KB r0;
        String str2;
        AnonymousClass009.A05(r6.A0C);
        String str3 = null;
        try {
            File A00 = this.A0G.A00.A00(AnonymousClass1US.A0A(Base64.encodeToString(C003501n.A0D(32), 2)));
            if (!(r6.A01 == 3 || (str2 = r6.A08) == null)) {
                try {
                    C14350lI.A0A(this.A03.A04, new File(str2), A00);
                    if (A00.exists()) {
                        return new Pair(A00, r6.A0C);
                    }
                } catch (IOException e2) {
                    e = e2;
                    str = "StickerRepository/moveThirdPartyStickerToTempStorage failed to copy cached file";
                    Log.e(str, e);
                    return null;
                }
            }
            boolean z = false;
            try {
                InputStream openInputStream = this.A09.A00.getContentResolver().openInputStream(Uri.parse(r6.A08));
                if (openInputStream != null) {
                    z = C14350lI.A0P(A00, openInputStream);
                }
                if (openInputStream != null) {
                    openInputStream.close();
                }
            } catch (IOException e3) {
                Log.e("StickerRepository/moveThirdPartyStickerToTempStorage failed to copy external file", e3);
            }
            if (z) {
                try {
                    C38521oE.A00(A00, null, null);
                    r0 = r6.A04;
                } catch (C38501oC e4) {
                    Log.e("StickerRepository/moveThirdPartyStickerToTempStorage sticker file failed validation", e4);
                }
                if (r0 == null || WebpUtils.A01(A00, r0.A01())) {
                    try {
                        str3 = C38531oF.A00(A00);
                    } catch (IOException e5) {
                        StringBuilder sb = new StringBuilder("StickerRepository/moveThirdPartyStickerToTempStorage could not get file hash; file=");
                        sb.append(A00);
                        Log.e(sb.toString(), e5);
                    }
                } else {
                    Log.e("StickerRepository/moveThirdPartyStickerToTempStorage failed to insert metadata");
                    C14350lI.A0M(A00);
                    return null;
                }
            }
            return new Pair(A00, str3);
        } catch (IOException e6) {
            e = e6;
            str = "StickerRepository/moveThirdPartyStickerToTempStorage failed to generate internal temp file";
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r4 == null) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1KZ A02(X.AbstractC33611ef r7, java.lang.String r8, boolean r9) {
        /*
            r6 = this;
            X.0ny r3 = r6.A0X
            X.1KZ r4 = r3.A01(r8)
            if (r4 != 0) goto L_0x000c
            X.1KZ r4 = r3.A00(r8)
        L_0x000c:
            if (r9 == 0) goto L_0x002a
            if (r4 != 0) goto L_0x006d
            java.lang.String r0 = " "
            boolean r0 = r8.contains(r0)
            if (r0 != 0) goto L_0x002c
            X.11c r0 = r6.A0S
            r0.A00()
            X.1KZ r4 = r3.A00(r8)
            if (r4 != 0) goto L_0x006d
            X.11a r0 = r6.A0R
            X.1KZ r0 = r0.A00(r7, r8)
            return r0
        L_0x002a:
            if (r4 != 0) goto L_0x006d
        L_0x002c:
            java.lang.String r0 = " "
            boolean r0 = r8.contains(r0)
            if (r0 == 0) goto L_0x0095
            android.util.Pair r5 = X.AnonymousClass144.A00(r8)     // Catch: Exception -> 0x0059
            if (r5 == 0) goto L_0x0095
            X.0o6 r2 = r6.A0C     // Catch: Exception -> 0x0059
            java.lang.Object r1 = r5.first     // Catch: Exception -> 0x0059
            java.lang.String r1 = (java.lang.String) r1     // Catch: Exception -> 0x0059
            java.lang.Object r0 = r5.second     // Catch: Exception -> 0x0059
            java.lang.String r0 = (java.lang.String) r0     // Catch: Exception -> 0x0059
            boolean r0 = r2.A02(r1, r0)     // Catch: Exception -> 0x0059
            if (r0 == 0) goto L_0x0095
            X.147 r2 = r6.A0T     // Catch: Exception -> 0x0059
            java.lang.Object r1 = r5.first     // Catch: Exception -> 0x0059
            java.lang.String r1 = (java.lang.String) r1     // Catch: Exception -> 0x0059
            java.lang.Object r0 = r5.second     // Catch: Exception -> 0x0059
            java.lang.String r0 = (java.lang.String) r0     // Catch: Exception -> 0x0059
            X.1KZ r4 = r2.A00(r1, r0)     // Catch: Exception -> 0x0059
            goto L_0x008a
        L_0x0059:
            r2 = move-exception
            java.lang.String r1 = "StickerRepository/getStickerPackByIdSync/error fetching sticker pack: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0, r2)
            r0 = 0
            return r0
        L_0x006d:
            java.lang.String r0 = r4.A02
            if (r0 == 0) goto L_0x0077
            boolean r0 = r4.A01()
            if (r0 == 0) goto L_0x008a
        L_0x0077:
            X.11a r2 = r6.A0R
            java.lang.String r1 = r4.A0D
            r0 = 0
            X.1KZ r1 = r2.A00(r0, r1)
            if (r1 == 0) goto L_0x008a
            java.util.List r0 = r1.A03
            r4.A03 = r0
            java.util.List r0 = r1.A04
            r4.A04 = r0
        L_0x008a:
            X.AnonymousClass009.A00()
            X.15A r0 = r3.A0B
            int r0 = r0.A00(r8)
            r4.A00 = r0
        L_0x0095:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C235512c.A02(X.1ef, java.lang.String, boolean):X.1KZ");
    }

    public AnonymousClass1KZ A03(String str) {
        C15830ny r4 = this.A0X;
        AnonymousClass1KZ A01 = r4.A01(str);
        if (A01 == null) {
            if (str.contains(" ")) {
                A01 = null;
                try {
                    Pair A00 = AnonymousClass144.A00(str);
                    if (A00 != null) {
                        A01 = this.A0T.A00((String) A00.first, (String) A00.second);
                    }
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder("StickerRepository/getInstalledStickerPackByIdSync/error fetching sticker pack: ");
                    sb.append(str);
                    Log.e(sb.toString(), e);
                    return A01;
                }
            }
            return A01;
        }
        AnonymousClass009.A00();
        A01.A00 = r4.A0B.A00(str);
        return A01;
    }

    public C38721ob A04() {
        C38721ob r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        Context context = this.A09.A00;
        File file = new File(context.getCacheDir(), "stickers_preview_images");
        if (!file.exists() && !file.mkdirs()) {
            StringBuilder sb = new StringBuilder("StickerRepository/getPreviewImageLoader/could not create diskcache directory:");
            sb.append(file.getAbsolutePath());
            Log.w(sb.toString());
        }
        long min = Math.min(4194304L, file.getFreeSpace() / 16);
        C14900mE r7 = this.A04;
        C38751oe r6 = new C38751oe(r7, new C38731oc(AnonymousClass00T.A04(context, R.drawable.sticker_store_error), AnonymousClass00T.A04(context, R.drawable.sticker_store_error)), this.A0R, file, min);
        C38771og r13 = new C38771og(r7, this.A06, this.A0H, file, "sticker-repository");
        r13.A04 = r6;
        r13.A00 = Integer.MAX_VALUE;
        r13.A01 = min;
        r13.A03 = AnonymousClass00T.A04(context, R.drawable.sticker_store_error);
        r13.A02 = AnonymousClass00T.A04(context, R.drawable.sticker_store_error);
        r13.A05 = true;
        C38721ob A00 = r13.A00();
        this.A00 = A00;
        return A00;
    }

    public File A05(AnonymousClass1KS r7) {
        Pair A01;
        Object obj;
        Object obj2;
        String str = r7.A0C;
        AnonymousClass009.A05(str);
        C002701f r5 = this.A05;
        File A03 = r5.A03(str);
        if (!(A03 != null || (A01 = A01(r7)) == null || (obj = A01.first) == null || (obj2 = A01.second) == null)) {
            try {
                File file = (File) obj;
                File A04 = r5.A04((String) obj2);
                AnonymousClass009.A05(A04);
                try {
                    C14350lI.A0B(this.A03.A04, file, A04);
                    r5.A03((String) A01.second);
                    return A04;
                } catch (IOException e) {
                    Log.e("StickerRepository/moveTempStickerFileToInternalStorage failed to copy resulting file");
                    C14350lI.A0M(file);
                    throw e;
                }
            } catch (IOException unused) {
                StringBuilder sb = new StringBuilder("StickerRepository/incrementReferenceCountOnThirdPartySticker unable to move ");
                sb.append(((File) A01.first).getAbsolutePath());
                sb.append(" to internal storage");
                Log.e(sb.toString());
            }
        }
        return A03;
    }

    public File A06(AnonymousClass1KS r4, File file) {
        File A00 = this.A0U.A00(r4, file);
        if (A00 != null) {
            r4.A08 = A00.getAbsolutePath();
            r4.A01 = 1;
            r4.A04 = AnonymousClass1KB.A00(WebpUtils.fetchWebpMetadata(A00.getAbsolutePath()));
            StringBuilder sb = new StringBuilder("StickerRepository/downloadSticker/downloaded sticker, file_hash:");
            sb.append(r4.A0C);
            sb.append(",media_key:");
            sb.append(r4.A0A);
            sb.append(",file:");
            sb.append(A00.getAbsolutePath());
            sb.append(", direct_path:");
            sb.append(r4.A05);
            Log.i(sb.toString());
            return A00;
        }
        StringBuilder sb2 = new StringBuilder("StickerRepository/downloadSticker/sticker file is null for: ");
        sb2.append(r4.A0C);
        Log.e(sb2.toString());
        return A00;
    }

    public final File A07(String str) {
        File A02 = this.A05.A02();
        if (!A02.exists() && !A02.mkdirs()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".png");
        return new File(A02, sb.toString());
    }

    public String A08(AnonymousClass1KZ r4) {
        String str = r4.A0D;
        File A07 = A07(str);
        if (A07 == null || !A07.exists()) {
            A07 = this.A0T.A01(str);
        }
        if (A07 == null || !A07.exists()) {
            throw new IOException("StickerRepository/calculateThirdPartyTrayFileHash/cannot fetch third party tray");
        }
        FileInputStream fileInputStream = new FileInputStream(A07);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            C38531oF.A02(fileInputStream, instance);
            String encodeToString = Base64.encodeToString(instance.digest(), 2);
            fileInputStream.close();
            return encodeToString;
        } catch (Throwable th) {
            try {
                fileInputStream.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public String A09(List list) {
        String str;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1KZ r6 = (AnonymousClass1KZ) it.next();
                String str2 = r6.A02;
                if (str2 != null) {
                    instance.update(str2.getBytes());
                } else if (r6.A0O) {
                    StringBuilder sb = new StringBuilder("StickerRepository/calculateImageDataHashForThirdParty, id: ");
                    String str3 = r6.A0D;
                    sb.append(str3);
                    Log.i(sb.toString());
                    String str4 = r6.A0E;
                    if (str4 == null) {
                        MessageDigest instance2 = MessageDigest.getInstance("MD5");
                        Iterator it2 = r6.A04.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            String str5 = ((AnonymousClass1KS) it2.next()).A0C;
                            if (str5 == null) {
                                this.A02.AaV("third party sticker plaintext hash is null", null, true);
                                instance2.update(str3.getBytes());
                                break;
                            }
                            instance2.update(str5.getBytes());
                        }
                        String A08 = A08(r6);
                        if (A08 != null) {
                            instance2.update(A08.getBytes());
                        }
                        str4 = AnonymousClass1US.A0A(Base64.encodeToString(instance2.digest(), 2));
                    }
                    instance.update(str4.getBytes());
                }
            }
            return AnonymousClass1US.A0A(Base64.encodeToString(instance.digest(), 2));
        } catch (IOException | NoSuchAlgorithmException e) {
            Log.e("app/xmpp/recv/handle_sticker_pack_query/could not get MD5 message digest", e);
            String[] strArr = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                String str6 = ((AnonymousClass1KZ) list.get(i)).A02;
                AnonymousClass1KZ r0 = (AnonymousClass1KZ) list.get(i);
                if (str6 != null) {
                    str = r0.A02;
                } else {
                    str = r0.A0D;
                }
                strArr[i] = str;
            }
            return String.valueOf(Arrays.hashCode(strArr));
        }
    }

    public List A0A() {
        AnonymousClass0o6 r0 = this.A0C;
        ArrayList arrayList = new ArrayList();
        C16310on A01 = r0.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT authority, sticker_pack_id, sticker_pack_name, sticker_pack_publisher, sticker_pack_image_data_hash, avoid_cache, is_animated_pack FROM third_party_whitelist_packs", null);
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("authority");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("sticker_pack_id");
            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("sticker_pack_name");
            int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("sticker_pack_publisher");
            int columnIndex = A09.getColumnIndex("sticker_pack_image_data_hash");
            int columnIndex2 = A09.getColumnIndex("avoid_cache");
            int columnIndex3 = A09.getColumnIndex("is_animated_pack");
            while (A09.moveToNext()) {
                arrayList.add(AnonymousClass0o6.A00(A09, columnIndexOrThrow, columnIndexOrThrow2, columnIndexOrThrow3, columnIndexOrThrow4, columnIndex, columnIndex2, columnIndex3));
            }
            A09.close();
            A01.close();
            Set A00 = this.A0D.A00();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                AnonymousClass1KZ r3 = (AnonymousClass1KZ) it.next();
                C15830ny r02 = this.A0X;
                String str = r3.A0D;
                AnonymousClass009.A00();
                r3.A00 = r02.A0B.A00(str);
                r3.A07 = A00.contains(str);
            }
            StringBuilder sb = new StringBuilder("StickerRepository/getCachedWhiteListedStickerPacksSync/found total cached sticker pack count: ");
            sb.append(arrayList.size());
            Log.i(sb.toString());
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A0B() {
        List A07 = this.A07.A07(0);
        if (A07 != null) {
            return A07;
        }
        C15830ny r0 = this.A0X;
        AnonymousClass009.A00();
        List A00 = r0.A0C.A00("SELECT * FROM installed_sticker_packs LEFT JOIN downloadable_sticker_packs ON (installed_id = id)", null);
        A0M(A00, 0);
        return A00;
    }

    public List A0C() {
        C15830ny r5 = this.A0X;
        AnonymousClass009.A00();
        List<AnonymousClass1KZ> A00 = r5.A0C.A00("SELECT * FROM installed_sticker_packs LEFT JOIN downloadable_sticker_packs ON (installed_id = id)", null);
        A00.addAll(A0A());
        for (AnonymousClass1KZ r2 : A00) {
            String str = r2.A0D;
            AnonymousClass009.A00();
            r2.A00 = r5.A0B.A00(str);
        }
        Collections.sort(A00, new C38681oX());
        return A00;
    }

    public List A0D(int i) {
        C235412b r0 = this.A0W;
        AnonymousClass009.A00();
        List<C37421mM> A00 = r0.A01.A00(Integer.MAX_VALUE, i);
        ArrayList arrayList = new ArrayList();
        for (C37421mM r3 : A00) {
            C002701f r02 = this.A05;
            String str = r3.A0A;
            File A04 = r02.A04(str);
            if (A04.exists()) {
                AnonymousClass1KS r1 = new AnonymousClass1KS();
                r1.A08 = A04.getAbsolutePath();
                r1.A01 = 1;
                r1.A0C = str;
                r1.A0F = r3.A0D;
                r1.A07 = r3.A09;
                r1.A05 = r3.A08;
                String str2 = r3.A0C;
                if (str2 == null) {
                    str2 = "image/webp";
                }
                r1.A0B = str2;
                r1.A0A = r3.A0B;
                r1.A00 = r3.A04;
                r1.A03 = r3.A06;
                r1.A02 = r3.A05;
                r1.A04 = AnonymousClass1KB.A00(WebpUtils.fetchWebpMetadata(A04.getAbsolutePath()));
                C37431mO.A00(r1);
                r1.A09 = r3.A01;
                A0F(r1);
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    public List A0E(C38691oY r15) {
        Throwable e;
        String str;
        AnonymousClass0o6 r4 = this.A0C;
        ArrayList arrayList = new ArrayList();
        String[] strArr = {"authority", "sticker_pack_id"};
        C16310on A01 = r4.A00.get();
        try {
            Cursor A08 = A01.A03.A08("third_party_whitelist_packs", null, null, null, strArr, null);
            int columnIndexOrThrow = A08.getColumnIndexOrThrow("authority");
            int columnIndexOrThrow2 = A08.getColumnIndexOrThrow("sticker_pack_id");
            while (A08.moveToNext()) {
                arrayList.add(new Pair(A08.getString(columnIndexOrThrow), A08.getString(columnIndexOrThrow2)));
            }
            A08.close();
            A01.close();
            HashMap hashMap = new HashMap();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                Pair pair = (Pair) it.next();
                String A012 = AnonymousClass144.A01((String) pair.first, (String) pair.second);
                C15830ny r0 = this.A0X;
                AnonymousClass009.A00();
                hashMap.put(A012, Integer.valueOf(r0.A0B.A00(A012)));
            }
            Collections.sort(arrayList, new C38701oZ(this, hashMap));
            ArrayList arrayList2 = new ArrayList();
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                Pair pair2 = (Pair) it2.next();
                AnonymousClass1KZ r5 = null;
                try {
                    r5 = this.A0T.A00((String) pair2.first, (String) pair2.second);
                } catch (C38541oG e2) {
                    StringBuilder sb = new StringBuilder("StickerRepository/getInstalledStickerPacksSync/third party sticker pack is either invalid or cannot be found, so removing from the whitelist, authority:");
                    sb.append((String) pair2.first);
                    sb.append(", identifier:");
                    sb.append((String) pair2.second);
                    Log.e(sb.toString(), e2);
                    A0N((String) pair2.first, (String) pair2.second);
                    if (r15 != null) {
                        ((AbstractC16350or) r15.A00).A02.A01(AnonymousClass144.A01((String) pair2.first, (String) pair2.second));
                    }
                } catch (C38551oH e3) {
                    e = e3;
                    str = "StickerRepository/getInstalledThirdPartyStickerPacksSync/fetch of sticker pack restricted";
                    Log.e(str, e);
                } catch (Exception e4) {
                    e = e4;
                    str = "StickerRepository/getInstalledThirdPartyStickerPacksSync/failed to fetch sticker pack";
                    Log.e(str, e);
                }
                if (r5 != null) {
                    arrayList2.add(r5);
                    Set A00 = this.A0D.A00();
                    String str2 = r5.A0D;
                    r5.A07 = A00.contains(str2);
                    for (AnonymousClass1KS r02 : r5.A04) {
                        A0F(r02);
                    }
                    Object obj = hashMap.get(str2);
                    AnonymousClass009.A05(obj);
                    r5.A00 = ((Number) obj).intValue();
                    if (r15 != null) {
                        ((AbstractC16350or) r15.A00).A02.A01(r5);
                    }
                }
            }
            StringBuilder sb2 = new StringBuilder("StickerRepository/getInstalledThirdPartyStickerPacksSync/found total 3rd party sticker pack count: ");
            sb2.append(arrayList2.size());
            Log.i(sb2.toString());
            return arrayList2;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0F(AnonymousClass1KS r8) {
        String str;
        byte[] bArr;
        String str2 = r8.A0C;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            synchronized (this) {
                if (this.A01 == null) {
                    SharedPreferences sharedPreferences = this.A0A.A00;
                    String string = sharedPreferences.getString("sticker_hash_salt", null);
                    if (string == null) {
                        byte[] A0D = C003501n.A0D(32);
                        this.A01 = A0D;
                        sharedPreferences.edit().putString("sticker_hash_salt", Base64.encodeToString(A0D, 2)).apply();
                    } else {
                        this.A01 = Base64.decode(string, 0);
                    }
                }
                bArr = this.A01;
            }
            instance.update(bArr);
            instance.update(str2.getBytes());
            str = Base64.encodeToString(instance.digest(), 2);
        } catch (NoSuchAlgorithmException unused) {
            str = null;
        }
        r8.A0D = str;
    }

    public final void A0G(AnonymousClass1KZ r6) {
        C16310on A02 = this.A0D.A00.A02();
        try {
            A02.A03.A01("unseen_sticker_packs", "pack_id = ?", new String[]{r6.A0D});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0H(AnonymousClass1KZ r17, AbstractC38651oS r18, int i, boolean z) {
        String str = r17.A0D;
        AnonymousClass15I r11 = this.A0Q;
        Map map = r11.A01;
        if (map.containsKey(str)) {
            Log.e("StickerRepository/downloadStickersOfAStickerPackAsync attempting to download same pack twice");
            return;
        }
        C235812f r8 = this.A0M;
        AnonymousClass146 r9 = this.A0P;
        C38661oT r6 = new C38661oT(this.A0F, r8, r9, r18, r11, this, str, i, z);
        map.put(str, 0);
        r11.A00.put(str, r6);
        for (AnonymousClass1KM r1 : r9.A01()) {
            if (r1 instanceof AnonymousClass1KN) {
                AnonymousClass1KT r12 = ((AnonymousClass1KN) r1).A00;
                r12.A0F.put(str, r17);
                List list = r12.A05;
                if (list != null) {
                    r12.A05(list);
                }
            } else if (r1 instanceof C33501eB) {
                StickerStoreTabFragment stickerStoreTabFragment = ((C33501eB) r1).A00;
                if (!(stickerStoreTabFragment instanceof StickerStoreMyTabFragment)) {
                    if ((stickerStoreTabFragment instanceof StickerStoreFeaturedTabFragment) && stickerStoreTabFragment.A0E != null) {
                        for (int i2 = 0; i2 < stickerStoreTabFragment.A0E.size(); i2++) {
                            AnonymousClass1KZ r13 = (AnonymousClass1KZ) stickerStoreTabFragment.A0E.get(i2);
                            if (r13.A0D.equals(str)) {
                                r13.A05 = true;
                                C38671oW r0 = stickerStoreTabFragment.A0D;
                                if (r0 != null) {
                                    r0.A03(i2);
                                }
                            }
                        }
                    }
                } else if (stickerStoreTabFragment.A0E != null) {
                    int i3 = 0;
                    while (true) {
                        if (i3 < stickerStoreTabFragment.A0E.size()) {
                            AnonymousClass1KZ r14 = (AnonymousClass1KZ) stickerStoreTabFragment.A0E.get(i3);
                            if (r14.A0D.equals(str)) {
                                r14.A05 = true;
                                C38671oW r02 = stickerStoreTabFragment.A0D;
                                if (r02 != null) {
                                    r02.A03(i3);
                                }
                            } else {
                                i3++;
                            }
                        }
                    }
                }
            } else if (r1 instanceof AnonymousClass1KO) {
                AnonymousClass1KO r15 = (AnonymousClass1KO) r1;
                if (r17.A0N) {
                    StickerStorePackPreviewActivity stickerStorePackPreviewActivity = r15.A00;
                    stickerStorePackPreviewActivity.A01.setVisibility(8);
                    stickerStorePackPreviewActivity.A04.setVisibility(0);
                }
            }
        }
        this.A0Y.Aaz(r6, r17);
    }

    public void A0I(AnonymousClass1KZ r5, AbstractC38791oi r6) {
        if (this.A0E.A07(575)) {
            AnonymousClass14B r0 = this.A0V;
            String str = r5.A0D;
            AnonymousClass1O4 A04 = r0.A00.A04();
            Bitmap bitmap = (Bitmap) A04.A00(str);
            if (bitmap != null) {
                if (bitmap.isRecycled()) {
                    A04.A00.A07(str);
                } else {
                    r6.AS5(bitmap);
                    return;
                }
            }
        }
        this.A0Y.Aaz(new C38801oj(this, r6), r5);
    }

    public void A0J(AbstractC33611ef r5, String str, boolean z) {
        this.A0Y.Aaz(new C38781oh(this.A0Q, r5, this), new Pair(str, Boolean.valueOf(z)));
    }

    public void A0K(Collection collection) {
        this.A0Y.Ab2(new RunnableBRunnable0Shape8S0200000_I0_8(this, 8, collection));
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01be A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0L(java.util.Collection r36, boolean r37) {
        /*
        // Method dump skipped, instructions count: 479
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C235512c.A0L(java.util.Collection, boolean):void");
    }

    public final void A0M(List list, int i) {
        HashSet hashSet = new HashSet();
        Set A00 = this.A0D.A00();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1KZ r7 = (AnonymousClass1KZ) it.next();
            String str = r7.A0D;
            if (hashSet.contains(str)) {
                Log.e("StickerRepository/getInstalledFirstPartyStickerPacksSync duplicate sticker pack");
            } else {
                hashSet.add(str);
                C15830ny r0 = this.A0X;
                AnonymousClass009.A00();
                List<AnonymousClass1KS> A002 = r0.A08.A00(str);
                for (AnonymousClass1KS r1 : A002) {
                    if (!TextUtils.isEmpty(r1.A08)) {
                        C37431mO.A00(r1);
                    }
                }
                r7.A04 = A002;
                r7.A07 = A00.contains(str);
                for (AnonymousClass1KS r02 : r7.A04) {
                    A0F(r02);
                }
            }
        }
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            AnonymousClass1KZ r2 = (AnonymousClass1KZ) it2.next();
            C15830ny r03 = this.A0X;
            String str2 = r2.A0D;
            AnonymousClass009.A00();
            r2.A00 = r03.A0B.A00(str2);
        }
        Collections.sort(list, new C38681oX());
        StringBuilder sb = new StringBuilder("StickerRepository/getInstalledFirstPartyStickerPacksSync/found total sticker pack count: ");
        sb.append(list.size());
        Log.i(sb.toString());
        C18720su r04 = this.A07;
        synchronized (r04.A0E) {
            r04.A07.put(Integer.valueOf(i), new WeakReference(list));
        }
    }

    public final boolean A0N(String str, String str2) {
        try {
            A00(this.A0I, this.A0T.A00(str, str2));
        } catch (Exception e) {
            Log.e("StickerRepository/uninstallThirdPartyPack/fetch pack failed", e);
        }
        AnonymousClass147 r4 = this.A0T;
        File A01 = r4.A07.A01(AnonymousClass144.A01(str, str2));
        if (A01 != null && C14350lI.A0M(A01)) {
            A01.toString();
        }
        AnonymousClass145 r3 = r4.A06;
        synchronized (r3) {
            File A00 = r3.A00(str, str2);
            if (A00.exists()) {
                File parentFile = A00.getParentFile();
                C14350lI.A0C(A00);
                if (parentFile != null && parentFile.isDirectory() && parentFile.listFiles().length == 0) {
                    C14350lI.A0M(parentFile);
                }
            }
        }
        String[] strArr = {str, str2};
        C16310on A02 = r4.A02.A00.A02();
        try {
            A02.A03.A01("third_party_sticker_emoji_mapping", "authority = ? AND sticker_pack_id = ?", strArr);
            A02.close();
            A02 = r4.A03.A00.A02();
            try {
                boolean z = false;
                if (A02.A03.A01("third_party_whitelist_packs", "authority = ? AND sticker_pack_id = ?", new String[]{str, str2}) > 0) {
                    z = true;
                }
                A02.close();
                if (z) {
                    this.A0O.A00();
                }
                this.A0K.A0G(A09(A0C()), AnonymousClass144.A01(str, str2));
                return z;
            } finally {
            }
        } finally {
        }
    }
}
