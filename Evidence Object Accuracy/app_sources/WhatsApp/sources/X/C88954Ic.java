package X;

import com.google.android.material.behavior.SwipeDismissBehavior;

/* renamed from: X.4Ic  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88954Ic {
    public AnonymousClass5R8 A00;

    public C88954Ic(SwipeDismissBehavior swipeDismissBehavior) {
        swipeDismissBehavior.A01 = Math.min(Math.max(0.0f, 0.1f), 1.0f);
        swipeDismissBehavior.A00 = Math.min(Math.max(0.0f, 0.6f), 1.0f);
        swipeDismissBehavior.A02 = 0;
    }
}
