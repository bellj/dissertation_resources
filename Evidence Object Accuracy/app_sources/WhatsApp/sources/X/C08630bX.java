package X;

import android.util.Log;

/* renamed from: X.0bX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08630bX implements AbstractC12710iN {
    public static final C08630bX A01 = new C08630bX();
    public int A00;

    @Override // X.AbstractC12710iN
    public void A9G(String str, String str2) {
        Log.e(str, str2);
    }

    @Override // X.AbstractC12710iN
    public void A9H(String str, String str2, Throwable th) {
        Log.e(str, str2, th);
    }

    @Override // X.AbstractC12710iN
    public void AIU(String str, String str2) {
        Log.i("OpticE2EConfig", str2);
    }

    @Override // X.AbstractC12710iN
    public boolean AJi(int i) {
        return this.A00 <= i;
    }

    @Override // X.AbstractC12710iN
    public void Aff(String str, String str2) {
        Log.v(str, str2);
    }

    @Override // X.AbstractC12710iN
    public void Ag0(String str, String str2) {
        Log.w(str, str2);
    }

    @Override // X.AbstractC12710iN
    public void Ag1(String str, String str2, Throwable th) {
        Log.w(str, str2, th);
    }

    @Override // X.AbstractC12710iN
    public void AgJ(String str, String str2) {
        Log.e(str, str2);
    }

    @Override // X.AbstractC12710iN
    public void AgK(String str, String str2, Throwable th) {
        Log.e("FixedOrientationCompat", str2, th);
    }
}
