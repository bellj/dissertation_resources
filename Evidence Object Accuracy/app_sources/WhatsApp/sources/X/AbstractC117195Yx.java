package X;

import java.io.Closeable;

/* renamed from: X.5Yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC117195Yx extends Closeable {
    long AZp(C10730f6 v, long j);

    @Override // java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    void close();
}
