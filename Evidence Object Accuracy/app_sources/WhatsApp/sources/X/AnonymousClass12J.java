package X;

/* renamed from: X.12J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12J {
    public final C15570nT A00;
    public final C15450nH A01;
    public final C15550nR A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final C14820m6 A05;
    public final C14950mJ A06;
    public final C19990v2 A07;
    public final C20830wO A08;
    public final C15600nX A09;
    public final C21250x7 A0A;
    public final C18470sV A0B;
    public final C14850m9 A0C;
    public final C14410lO A0D;

    public AnonymousClass12J(C15570nT r1, C15450nH r2, C15550nR r3, C18640sm r4, C14830m7 r5, C14820m6 r6, C14950mJ r7, C19990v2 r8, C20830wO r9, C15600nX r10, C21250x7 r11, C18470sV r12, C14850m9 r13, C14410lO r14) {
        this.A04 = r5;
        this.A0C = r13;
        this.A00 = r1;
        this.A07 = r8;
        this.A01 = r2;
        this.A0A = r11;
        this.A0B = r12;
        this.A06 = r7;
        this.A0D = r14;
        this.A02 = r3;
        this.A05 = r6;
        this.A09 = r10;
        this.A03 = r4;
        this.A08 = r9;
    }

    public final boolean A00(AbstractC14640lm r9) {
        if (r9 == null) {
            return false;
        }
        C19990v2 r4 = this.A07;
        if (r4.A00(r9) < 5) {
            return false;
        }
        long j = this.A05.A00.getLong("last_read_conversation_time", 0);
        AnonymousClass1PE r0 = (AnonymousClass1PE) r4.A0B().get(r9);
        if (r0 == null) {
            return false;
        }
        long j2 = r0.A0X;
        if (j2 == 0 || j2 + 86400000 >= j) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0041 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A01(X.AbstractC14640lm r7, X.C14370lK r8, int r9, int r10, long r11, boolean r13) {
        /*
            r6 = this;
            X.0lK r0 = X.C14370lK.A0S
            r3 = 3
            r5 = 1
            r4 = 0
            if (r8 != r0) goto L_0x001e
            if (r10 < 0) goto L_0x0042
            long r1 = (long) r10
            int r0 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0042
            boolean r0 = r6.A00(r7)
            if (r0 != 0) goto L_0x0042
            if (r9 == r3) goto L_0x0038
            r3 = 102400(0x19000, double:5.05923E-319)
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x0058
            return r5
        L_0x001e:
            X.0lK r0 = X.C14370lK.A0B
            if (r8 == r0) goto L_0x0043
            X.0lK r0 = X.C14370lK.A0G
            if (r8 == r0) goto L_0x0043
            X.0lK r0 = X.C14370lK.A0Z
            if (r8 == r0) goto L_0x0043
            X.0lK r0 = X.C14370lK.A0X
            if (r8 != r0) goto L_0x0042
            if (r13 == 0) goto L_0x0042
            r1 = 262144(0x40000, double:1.295163E-318)
            int r0 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0038
            return r4
        L_0x0038:
            X.0m6 r0 = r6.A05
            int r0 = X.C38441o6.A00(r0, r9)
            r0 = r0 & r5
            if (r0 == 0) goto L_0x0058
            return r5
        L_0x0042:
            return r4
        L_0x0043:
            if (r10 <= 0) goto L_0x0058
            long r0 = (long) r10
            int r2 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r2 >= 0) goto L_0x0058
            r0 = 35000(0x88b8, float:4.9045E-41)
            if (r10 > r0) goto L_0x0058
            if (r9 == r3) goto L_0x0058
            boolean r0 = r6.A00(r7)
            if (r0 != 0) goto L_0x0058
            return r5
        L_0x0058:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass12J.A01(X.0lm, X.0lK, int, int, long, boolean):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00bc, code lost:
        if (r13 <= 524288) goto L_0x00be;
     */
    /* JADX WARNING: Removed duplicated region for block: B:76:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(X.C14370lK r11, int r12, long r13, boolean r15, boolean r16, boolean r17, boolean r18) {
        /*
        // Method dump skipped, instructions count: 200
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass12J.A02(X.0lK, int, long, boolean, boolean, boolean, boolean):boolean");
    }

    public boolean A03(AbstractC16130oV r10) {
        boolean z;
        C16150oX r0;
        if ((!(r10 instanceof C30061Vy) || !((C30061Vy) r10).A00) && ((r0 = r10.A02) == null || r0.A04 <= 0)) {
            z = false;
        } else {
            z = true;
        }
        return A02(C14370lK.A01(r10.A0y, ((AbstractC15340mz) r10).A08), ((AbstractC15340mz) r10).A08, r10.A01, false, z, C15380n4.A0N(r10.A0z.A00), C15380n4.A0M(r10.A0B()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007c, code lost:
        if (r1.A00((X.AbstractC14640lm) r0) == 1) goto L_0x0061;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A04(X.AbstractC16130oV r8) {
        /*
            r7 = this;
            X.0wO r1 = r7.A08
            X.1IS r0 = r8.A0z
            X.0lm r0 = r0.A00
            X.AnonymousClass009.A05(r0)
            X.0n3 r4 = r1.A01(r0)
            boolean r0 = r4.A0K()
            r6 = 0
            if (r0 == 0) goto L_0x0065
            X.0nR r1 = r7.A02
            java.lang.Class<com.whatsapp.jid.GroupJid> r0 = com.whatsapp.jid.GroupJid.class
            com.whatsapp.jid.Jid r0 = r4.A0B(r0)
            com.whatsapp.jid.GroupJid r0 = (com.whatsapp.jid.GroupJid) r0
            com.whatsapp.jid.UserJid r0 = r1.A0D(r0)
            r5 = 0
            if (r0 == 0) goto L_0x0063
            X.0n3 r3 = r1.A0B(r0)
        L_0x0029:
            X.0nT r0 = r7.A00
            r0.A08()
            X.1Ih r2 = r0.A05
            X.0lm r0 = r8.A0B()
            if (r0 == 0) goto L_0x003a
            X.0n3 r5 = r1.A0B(r0)
        L_0x003a:
            X.0nX r1 = r7.A09
            java.lang.Class<X.0nU> r0 = X.C15580nU.class
            com.whatsapp.jid.Jid r0 = r4.A0B(r0)
            X.AnonymousClass009.A05(r0)
            X.0nU r0 = (X.C15580nU) r0
            boolean r0 = r1.A0F(r0)
            if (r0 != 0) goto L_0x0061
            if (r3 == 0) goto L_0x005b
            X.1Pc r0 = r3.A0C
            if (r0 != 0) goto L_0x0061
            com.whatsapp.jid.Jid r0 = r3.A0D
            boolean r0 = X.C29941Vi.A00(r2, r0)
            if (r0 != 0) goto L_0x0061
        L_0x005b:
            if (r5 == 0) goto L_0x0062
            X.1Pc r0 = r5.A0C
        L_0x005f:
            if (r0 == 0) goto L_0x0062
        L_0x0061:
            r6 = 1
        L_0x0062:
            return r6
        L_0x0063:
            r3 = r5
            goto L_0x0029
        L_0x0065:
            int r1 = r4.A06
            r0 = 3
            if (r1 != r0) goto L_0x007f
            X.0x7 r1 = r7.A0A
            java.lang.Class<X.0lm> r0 = X.AbstractC14640lm.class
            com.whatsapp.jid.Jid r0 = r4.A0B(r0)
            X.AnonymousClass009.A05(r0)
            X.0lm r0 = (X.AbstractC14640lm) r0
            int r1 = r1.A00(r0)
            r0 = 1
            if (r1 != r0) goto L_0x007f
            goto L_0x0061
        L_0x007f:
            X.1Pc r0 = r4.A0C
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass12J.A04(X.0oV):boolean");
    }

    public final boolean A05(AbstractC16130oV r4) {
        if (C38451o7.A00(this.A04, this.A05, this.A0C, r4) && (r4 instanceof AnonymousClass1X7)) {
            C38101nW A14 = r4.A14();
            AnonymousClass009.A05(A14);
            C16150oX r0 = A14.A04.A02;
            AnonymousClass009.A05(r0);
            if (r0.A0M) {
                C16460p3 A0G = r4.A0G();
                AnonymousClass009.A05(A0G);
                if (!A0G.A04()) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        if (X.C30041Vv.A0o(r13) != false) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A06(X.AbstractC16130oV r13, int r14) {
        /*
            r12 = this;
            X.0oX r2 = r13.A02
            X.AnonymousClass009.A05(r2)
            r4 = r12
            X.0lO r0 = r12.A0D
            java.lang.String r3 = r2.A0I
            if (r3 == 0) goto L_0x002a
            X.0lP r1 = r0.A0K
            r0 = 1
            X.0lQ r0 = r1.A02(r3, r0)
            if (r0 == 0) goto L_0x002a
            long r9 = r0.A0A
        L_0x0017:
            boolean r0 = r13 instanceof X.AnonymousClass1X7
            if (r0 == 0) goto L_0x002d
            X.1IS r0 = r13.A0z
            X.0lm r0 = r0.A00
            boolean r0 = X.C15380n4.A0N(r0)
            if (r0 == 0) goto L_0x002d
            boolean r0 = r12.A05(r13)
            return r0
        L_0x002a:
            r9 = 0
            goto L_0x0017
        L_0x002d:
            byte r1 = r13.A0y
            int r0 = r13.A08
            X.0lK r6 = X.C14370lK.A01(r1, r0)
            boolean r0 = X.C30041Vv.A0n(r13)
            if (r0 != 0) goto L_0x0042
            boolean r0 = X.C30041Vv.A0o(r13)
            r11 = 0
            if (r0 == 0) goto L_0x0043
        L_0x0042:
            r11 = 1
        L_0x0043:
            int r8 = r2.A04
            X.1IS r0 = r13.A0z
            X.0lm r5 = r0.A00
            r7 = r14
            boolean r0 = r4.A01(r5, r6, r7, r8, r9, r11)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass12J.A06(X.0oV, int):boolean");
    }
}
