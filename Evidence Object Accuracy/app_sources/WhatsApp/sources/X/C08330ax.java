package X;

/* renamed from: X.0ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08330ax implements AbstractC12050hI {
    public static final C08330ax A00 = new C08330ax();

    @Override // X.AbstractC12050hI
    public Object AYt(AbstractC08850bx r7, float f) {
        boolean z = false;
        if (r7.A05() == EnumC03770Jb.BEGIN_ARRAY) {
            z = true;
            r7.A09();
        }
        float A02 = (float) r7.A02();
        float A022 = (float) r7.A02();
        while (r7.A0H()) {
            r7.A0E();
        }
        if (z) {
            r7.A0B();
        }
        return new AnonymousClass0OM((A02 / 100.0f) * f, (A022 / 100.0f) * f);
    }
}
