package X;

import android.content.Context;

/* renamed from: X.2Hp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48742Hp implements AbstractC009404s {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C48702Hh A01;

    public C48742Hp(Context context, C48702Hh r2) {
        this.A01 = r2;
        this.A00 = context;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        return new C48752Hq(new C48722Hj(new C48762Hr(((AnonymousClass01J) AnonymousClass01M.A00(this.A00, AnonymousClass01J.class)).ANr).A00));
    }
}
