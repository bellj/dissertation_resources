package X;

import android.content.Context;
import android.content.res.Configuration;

/* renamed from: X.063  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass063 {
    public static Context A00(Context context, Configuration configuration) {
        return context.createConfigurationContext(configuration);
    }

    public static void A01(Configuration configuration, Configuration configuration2, Configuration configuration3) {
        int i = configuration.densityDpi;
        int i2 = configuration2.densityDpi;
        if (i != i2) {
            configuration3.densityDpi = i2;
        }
    }
}
