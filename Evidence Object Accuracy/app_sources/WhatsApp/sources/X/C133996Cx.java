package X;

/* renamed from: X.6Cx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133996Cx implements AbstractC136366Mg {
    public final /* synthetic */ AnonymousClass6CM A00;

    public C133996Cx(AnonymousClass6CM r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136366Mg
    public boolean A6x() {
        return C12960it.A1W(((AbstractActivityC121525iS) this.A00.A00).A0Q);
    }

    @Override // X.AbstractC136366Mg
    public void ATU(String str) {
        this.A00.AXn(str);
    }
}
