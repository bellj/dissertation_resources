package X;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;

/* renamed from: X.4xC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107354xC implements AnonymousClass5X8 {
    public MediaCodecInfo[] A00;
    public final int A01;

    @Override // X.AnonymousClass5X8
    public boolean AbO() {
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0006, code lost:
        if (r3 != false) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C107354xC(boolean r2, boolean r3) {
        /*
            r1 = this;
            r1.<init>()
            if (r2 != 0) goto L_0x0008
            r0 = 0
            if (r3 == 0) goto L_0x0009
        L_0x0008:
            r0 = 1
        L_0x0009:
            r1.A01 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107354xC.<init>(boolean, boolean):void");
    }

    @Override // X.AnonymousClass5X8
    public int ABS() {
        MediaCodecInfo[] mediaCodecInfoArr = this.A00;
        if (mediaCodecInfoArr == null) {
            mediaCodecInfoArr = new MediaCodecList(this.A01).getCodecInfos();
            this.A00 = mediaCodecInfoArr;
        }
        return mediaCodecInfoArr.length;
    }

    @Override // X.AnonymousClass5X8
    public MediaCodecInfo ABT(int i) {
        MediaCodecInfo[] mediaCodecInfoArr = this.A00;
        if (mediaCodecInfoArr == null) {
            mediaCodecInfoArr = new MediaCodecList(this.A01).getCodecInfos();
            this.A00 = mediaCodecInfoArr;
        }
        return mediaCodecInfoArr[i];
    }

    @Override // X.AnonymousClass5X8
    public boolean AJO(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, String str2) {
        return codecCapabilities.isFeatureRequired(str);
    }

    @Override // X.AnonymousClass5X8
    public boolean AJP(MediaCodecInfo.CodecCapabilities codecCapabilities, String str, String str2) {
        return codecCapabilities.isFeatureSupported(str);
    }
}
