package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.2bA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52522bA extends ViewGroup {
    public final /* synthetic */ AnonymousClass2bf A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52522bA(Context context, AnonymousClass2bf r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        float f;
        int i5;
        float f2 = (float) (i3 - i);
        int i6 = i4 - i2;
        AnonymousClass2bf r9 = this.A00;
        float f3 = f2 / ((float) r9.A03.A01);
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            float f4 = ((float) i7) * f3;
            View childAt = getChildAt(i7);
            if (C28141Kv.A01(r9.A02)) {
                i5 = (int) f4;
                f = f4 + f3;
            } else {
                f = f2 - f4;
                i5 = (int) (f - f3);
            }
            childAt.layout(i5, 0, (int) f, i6);
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i);
        int childCount = getChildCount();
        AnonymousClass3IZ r7 = this.A00.A03;
        float f = (float) (size / r7.A01);
        for (int i3 = 0; i3 < childCount; i3++) {
            float f2 = ((float) i3) * f;
            getChildAt(i3).measure(C12980iv.A04(((int) (f2 + f)) - ((int) f2)), View.MeasureSpec.makeMeasureSpec(0, 0));
        }
        setMeasuredDimension(View.MeasureSpec.getSize(i), r7.A06);
    }
}
