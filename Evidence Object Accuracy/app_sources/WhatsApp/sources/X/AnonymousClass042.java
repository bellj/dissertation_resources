package X;

/* renamed from: X.042  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass042 extends AnonymousClass043 {
    public String toString() {
        return "Retry";
    }

    public boolean equals(Object obj) {
        return this == obj || (obj != null && AnonymousClass042.class == obj.getClass());
    }

    public int hashCode() {
        return AnonymousClass042.class.getName().hashCode();
    }
}
