package X;

import android.content.SharedPreferences;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1IM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1IM implements SharedPreferences.Editor {
    public boolean A00 = false;
    public final Object A01 = new Object();
    public final Map A02 = new HashMap();
    public final /* synthetic */ AnonymousClass1IL A03;

    public AnonymousClass1IM(AnonymousClass1IL r2) {
        this.A03 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0089 A[Catch: all -> 0x00a4, TryCatch #1 {, blocks: (B:4:0x0005, B:6:0x0009, B:7:0x0012, B:9:0x0024, B:10:0x0029, B:11:0x002b, B:46:0x009d, B:14:0x002f, B:16:0x0034, B:18:0x003a, B:20:0x0040, B:21:0x0043, B:22:0x004d, B:24:0x0053, B:27:0x0067, B:29:0x006d, B:31:0x0073, B:34:0x007a, B:35:0x007e, B:37:0x0084, B:39:0x0089, B:41:0x008e, B:43:0x0093, B:44:0x009a, B:45:0x009c), top: B:52:0x0005 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass280 A00() {
        /*
            r12 = this;
            X.1IL r8 = r12.A03
            java.lang.Object r6 = r8.A0A
            monitor-enter(r6)
            int r0 = r8.A00     // Catch: all -> 0x00a7
            if (r0 <= 0) goto L_0x0012
            java.util.Map r1 = r8.A04     // Catch: all -> 0x00a7
            java.util.HashMap r0 = new java.util.HashMap     // Catch: all -> 0x00a7
            r0.<init>(r1)     // Catch: all -> 0x00a7
            r8.A04 = r0     // Catch: all -> 0x00a7
        L_0x0012:
            java.util.Map r7 = r8.A04     // Catch: all -> 0x00a7
            int r0 = r8.A00     // Catch: all -> 0x00a7
            int r0 = r0 + 1
            r8.A00 = r0     // Catch: all -> 0x00a7
            java.util.Map r0 = r8.A0D     // Catch: all -> 0x00a7
            boolean r11 = r0.isEmpty()     // Catch: all -> 0x00a7
            r0 = 1
            r11 = r11 ^ r0
            if (r11 == 0) goto L_0x002d
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch: all -> 0x00a7
            r5.<init>()     // Catch: all -> 0x00a7
        L_0x0029:
            java.lang.Object r4 = r12.A01     // Catch: all -> 0x00a7
            monitor-enter(r4)     // Catch: all -> 0x00a7
            goto L_0x002f
        L_0x002d:
            r5 = 0
            goto L_0x0029
        L_0x002f:
            boolean r0 = r12.A00     // Catch: all -> 0x00a4
            r9 = 0
            if (r0 == 0) goto L_0x0043
            boolean r0 = r7.isEmpty()     // Catch: all -> 0x00a4
            if (r0 != 0) goto L_0x003f
            r7.clear()     // Catch: all -> 0x00a4
            r0 = 1
            goto L_0x0040
        L_0x003f:
            r0 = 0
        L_0x0040:
            r12.A00 = r9     // Catch: all -> 0x00a4
            r9 = r0
        L_0x0043:
            java.util.Map r3 = r12.A02     // Catch: all -> 0x00a4
            java.util.Set r0 = r3.entrySet()     // Catch: all -> 0x00a4
            java.util.Iterator r10 = r0.iterator()     // Catch: all -> 0x00a4
        L_0x004d:
            boolean r0 = r10.hasNext()     // Catch: all -> 0x00a4
            if (r0 == 0) goto L_0x008e
            java.lang.Object r0 = r10.next()     // Catch: all -> 0x00a4
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch: all -> 0x00a4
            java.lang.Object r2 = r0.getKey()     // Catch: all -> 0x00a4
            java.lang.String r2 = (java.lang.String) r2     // Catch: all -> 0x00a4
            java.lang.Object r1 = r0.getValue()     // Catch: all -> 0x00a4
            if (r1 == r12) goto L_0x007e
            if (r1 == 0) goto L_0x007e
            boolean r0 = r7.containsKey(r2)     // Catch: all -> 0x00a4
            if (r0 == 0) goto L_0x007a
            java.lang.Object r0 = r7.get(r2)     // Catch: all -> 0x00a4
            if (r0 == 0) goto L_0x007a
            boolean r0 = r0.equals(r1)     // Catch: all -> 0x00a4
            if (r0 == 0) goto L_0x007a
            goto L_0x004d
        L_0x007a:
            r7.put(r2, r1)     // Catch: all -> 0x00a4
            goto L_0x0087
        L_0x007e:
            boolean r0 = r7.containsKey(r2)     // Catch: all -> 0x00a4
            if (r0 == 0) goto L_0x004d
            r7.remove(r2)     // Catch: all -> 0x00a4
        L_0x0087:
            if (r11 == 0) goto L_0x008c
            r5.add(r2)     // Catch: all -> 0x00a4
        L_0x008c:
            r9 = 1
            goto L_0x004d
        L_0x008e:
            r3.clear()     // Catch: all -> 0x00a4
            if (r9 == 0) goto L_0x009a
            long r2 = r8.A01     // Catch: all -> 0x00a4
            r0 = 1
            long r0 = r0 + r2
            r8.A01 = r0     // Catch: all -> 0x00a4
        L_0x009a:
            long r1 = r8.A01     // Catch: all -> 0x00a4
            monitor-exit(r4)     // Catch: all -> 0x00a4
            monitor-exit(r6)     // Catch: all -> 0x00a7
            X.280 r0 = new X.280
            r0.<init>(r5, r7, r1)
            return r0
        L_0x00a4:
            r0 = move-exception
            monitor-exit(r4)     // Catch: all -> 0x00a4
            throw r0     // Catch: all -> 0x00a7
        L_0x00a7:
            r0 = move-exception
            monitor-exit(r6)     // Catch: all -> 0x00a7
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IM.A00():X.280");
    }

    public final void A01(AnonymousClass280 r8) {
        List list = r8.A01;
        if (list != null && list.size() != 0) {
            AnonymousClass1IL r5 = this.A03;
            synchronized (r5.A0A) {
                Map map = r5.A0D;
                if (!map.isEmpty()) {
                    int size = list.size();
                    while (true) {
                        size--;
                        if (size < 0) {
                            break;
                        }
                        String str = (String) list.get(size);
                        Map map2 = (Map) map.get(str);
                        if (map2 != null) {
                            A02(str, map2);
                        }
                        Map map3 = (Map) map.get(r5.A0C);
                        if (map3 != null) {
                            A02(str, map3);
                        }
                    }
                }
            }
        }
    }

    public final void A02(String str, Map map) {
        for (Map.Entry entry : map.entrySet()) {
            ((Handler) entry.getValue()).post(new RunnableBRunnable0Shape0S1200000_I0(this, entry.getKey(), str, 26));
        }
    }

    @Override // android.content.SharedPreferences.Editor
    public void apply() {
        AnonymousClass280 A00 = A00();
        AnonymousClass1IL r4 = this.A03;
        r4.A09.A00(new RunnableBRunnable0Shape0S0210000_I0(r4, A00, 18, false), r4.A06, true);
        A01(A00);
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor clear() {
        synchronized (this.A01) {
            this.A00 = true;
        }
        return this;
    }

    @Override // android.content.SharedPreferences.Editor
    public boolean commit() {
        boolean z;
        AnonymousClass280 A00 = A00();
        AnonymousClass1IL r5 = this.A03;
        RunnableBRunnable0Shape0S0210000_I0 runnableBRunnable0Shape0S0210000_I0 = new RunnableBRunnable0Shape0S0210000_I0(r5, A00, 18, true);
        synchronized (r5.A0A) {
            z = false;
            if (r5.A00 == 1) {
                z = true;
            }
        }
        if (z) {
            runnableBRunnable0Shape0S0210000_I0.run();
        } else {
            r5.A09.A00(runnableBRunnable0Shape0S0210000_I0, r5.A06, false);
        }
        try {
            A00.A03.await();
            A01(A00);
            return A00.A04;
        } catch (InterruptedException e) {
            Log.e("LightSharedPreferencesImpl/Commit: Got exception:", e);
            return false;
        }
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor putBoolean(String str, boolean z) {
        synchronized (this.A01) {
            this.A02.put(str, Boolean.valueOf(z));
        }
        return this;
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor putFloat(String str, float f) {
        synchronized (this.A01) {
            this.A02.put(str, Float.valueOf(f));
        }
        return this;
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor putInt(String str, int i) {
        synchronized (this.A01) {
            this.A02.put(str, Integer.valueOf(i));
        }
        return this;
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor putLong(String str, long j) {
        synchronized (this.A01) {
            this.A02.put(str, Long.valueOf(j));
        }
        return this;
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor putString(String str, String str2) {
        synchronized (this.A01) {
            this.A02.put(str, str2);
        }
        return this;
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor putStringSet(String str, Set set) {
        synchronized (this.A01) {
            this.A02.put(str, set == null ? null : new HashSet(set));
        }
        return this;
    }

    @Override // android.content.SharedPreferences.Editor
    public /* bridge */ /* synthetic */ SharedPreferences.Editor remove(String str) {
        synchronized (this.A01) {
            this.A02.put(str, this);
        }
        return this;
    }
}
