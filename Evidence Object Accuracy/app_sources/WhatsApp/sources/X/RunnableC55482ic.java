package X;

import com.facebook.redex.EmptyBaseRunnable0;
import java.util.List;

/* renamed from: X.2ic  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class RunnableC55482ic extends EmptyBaseRunnable0 implements Runnable {
    public final int A00;
    public final List A01;
    public volatile boolean A02;

    public /* synthetic */ RunnableC55482ic(List list, int i) {
        this.A00 = i;
        this.A01 = list;
    }

    @Override // java.lang.Runnable
    public void run() {
        int i = 0;
        while (true) {
            List list = this.A01;
            if (i < list.size() && !this.A02) {
                AnonymousClass3D8 r2 = ((C89954Ma) list.get(i)).A00;
                if (!r2.A03.isCancelled() && !r2.A03.isDone() && r2.A04.get() == -1) {
                    r2.A00();
                }
                i++;
            } else {
                return;
            }
        }
    }
}
