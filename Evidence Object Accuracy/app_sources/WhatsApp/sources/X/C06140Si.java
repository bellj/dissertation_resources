package X;

import android.content.LocusId;
import android.os.Build;
import android.text.TextUtils;

/* renamed from: X.0Si  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06140Si {
    public final LocusId A00;
    public final String A01;

    public C06140Si(String str) {
        LocusId locusId;
        if (!TextUtils.isEmpty(str)) {
            this.A01 = str;
            if (Build.VERSION.SDK_INT >= 29) {
                locusId = C05620Qh.A00(str);
            } else {
                locusId = null;
            }
            this.A00 = locusId;
            return;
        }
        throw new IllegalArgumentException(String.valueOf("id cannot be empty"));
    }

    public static C06140Si A00(LocusId locusId) {
        C04160Kp.A00(locusId, "locusId cannot be null");
        String A01 = C05620Qh.A01(locusId);
        if (!TextUtils.isEmpty(A01)) {
            return new C06140Si(A01);
        }
        throw new IllegalArgumentException(String.valueOf("id cannot be empty"));
    }

    public LocusId A01() {
        return this.A00;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C06140Si.class != obj.getClass()) {
            return false;
        }
        String str = this.A01;
        String str2 = ((C06140Si) obj).A01;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.A01;
        return 31 + (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("LocusIdCompat[");
        int length = this.A01.length();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(length);
        sb2.append("_chars");
        sb.append(sb2.toString());
        sb.append("]");
        return sb.toString();
    }
}
