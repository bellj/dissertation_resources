package X;

/* renamed from: X.5sd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126285sd {
    public final AnonymousClass1V8 A00;

    public C126285sd(AnonymousClass3CS r7, String str, String str2) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-get-verification-methods");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (C117295Zj.A1V(str2, 1, false)) {
            C41141sy.A01(A0N, "device-id", str2);
        }
        this.A00 = C117295Zj.A0I(A0N, A0M, r7);
    }
}
