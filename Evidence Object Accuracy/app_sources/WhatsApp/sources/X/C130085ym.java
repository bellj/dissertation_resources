package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.5ym  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130085ym {
    public final int A00;
    public final Context A01;
    public final AnonymousClass018 A02;
    public final AnonymousClass102 A03;
    public final AnonymousClass1IR A04;
    public final C120995h5 A05;
    public final C130205yy A06;
    public final C129835yN A07;
    public final C123755no A08;
    public final String A09;

    public C130085ym(Context context, AnonymousClass018 r3, AnonymousClass102 r4, C130205yy r5, C129835yN r6, C123755no r7, String str) {
        this.A01 = context;
        this.A02 = r3;
        this.A09 = str;
        this.A00 = r7.A00;
        this.A08 = r7;
        this.A03 = r4;
        C120995h5 r0 = r7.A01;
        AnonymousClass009.A05(r0);
        this.A05 = r0;
        C127915vG r02 = r5.A09;
        this.A06 = r5;
        AnonymousClass1IR r03 = r02.A01;
        AnonymousClass009.A05(r03);
        this.A04 = r03;
        this.A07 = r6;
    }

    public void A00(CharSequence charSequence, List list, boolean z, boolean z2) {
        String A0X;
        Context context = this.A01;
        AnonymousClass018 r7 = this.A02;
        CharSequence A00 = AnonymousClass613.A00(context, r7, this.A04);
        int i = this.A00;
        C1316363n r0 = this.A05.A02;
        if (r0 == null) {
            A0X = null;
        } else {
            AnonymousClass6F2 r02 = r0.A01;
            C30821Yy r5 = r02.A01;
            AbstractC30791Yv r4 = r02.A00;
            int i2 = R.string.novi_payment_transaction_details_unilateral_receiver_receiver_currency_mismatch_after_onboarding;
            if (i == 14) {
                i2 = R.string.novi_payment_transaction_details_unilateral_sender_receiver_currency_mismatch_after_onboarding;
            }
            Object[] A1a = C12980iv.A1a();
            A1a[0] = ((AbstractC30781Yu) r4).A04;
            A0X = C12960it.A0X(context, AnonymousClass613.A01(context, r7, new AnonymousClass6F2(r4, r5)), A1a, 1, i2);
        }
        C123755no r03 = this.A08;
        list.add(new C123285mu(A00, r03.A07.A0D(((AbstractC130195yx) r03).A01, A00), charSequence, null, A0X, z, z2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005b, code lost:
        if (r0 == null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0068, code lost:
        if (r14 == false) goto L_0x006a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(java.util.List r16) {
        /*
        // Method dump skipped, instructions count: 268
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C130085ym.A01(java.util.List):void");
    }

    public void A02(List list) {
        String str;
        Context context;
        int i;
        String A0X;
        Context context2;
        int i2;
        C130205yy r3;
        int i3;
        int i4;
        C123755no r2 = this.A08;
        String A04 = r2.A04();
        switch (r2.A00) {
            case 1:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                str = r2.A02;
                context = r2.A03;
                i = R.string.novi_payment_transaction_details_sender_status_desc_success;
                A0X = C12960it.A0X(context, str, new Object[1], 0, i);
                break;
            case 2:
                context2 = r2.A03;
                i2 = R.string.payments_transaction_system_pending_processing;
                A0X = context2.getString(i2);
                break;
            case 3:
            case 11:
                context2 = r2.A03;
                i2 = R.string.payments_transaction_status_description_failed;
                A0X = context2.getString(i2);
                break;
            case 4:
                context2 = r2.A03;
                i2 = R.string.transaction_status_sender_canceled;
                A0X = context2.getString(i2);
                break;
            case 5:
                context2 = r2.A03;
                i2 = R.string.payments_transaction_in_review;
                A0X = context2.getString(i2);
                break;
            case 6:
                context2 = r2.A03;
                i2 = R.string.novi_payment_transaction_details_sender_status_desc_in_review;
                A0X = context2.getString(i2);
                break;
            case 7:
                context2 = r2.A03;
                i2 = R.string.novi_payment_transaction_details_sender_status_desc_claim_approved;
                A0X = context2.getString(i2);
                break;
            case 8:
                context2 = r2.A03;
                i2 = R.string.novi_payment_transaction_details_sender_status_desc_claim_declined;
                A0X = context2.getString(i2);
                break;
            case 9:
                context2 = r2.A03;
                i2 = R.string.novi_payment_transaction_details_sender_status_desc_novi_declined_transaction;
                A0X = context2.getString(i2);
                break;
            case 10:
                context2 = r2.A03;
                i2 = R.string.novi_payment_transaction_details_receiver_status_desc_success;
                A0X = context2.getString(i2);
                break;
            case 12:
                str = r2.A02;
                context = r2.A03;
                i = R.string.novi_payment_transacton_details_receiver_status_sender_cancelled;
                A0X = C12960it.A0X(context, str, new Object[1], 0, i);
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                Context context3 = r2.A03;
                A0X = C12960it.A0X(context3, C12960it.A0X(context3, AnonymousClass1MY.A02(((AbstractC130195yx) r2).A06, ((AbstractC130195yx) r2).A01.A05 + 1209600000), new Object[1], 0, R.string.date), new Object[1], 0, R.string.novi_payment_transaction_details_receiver_status_desc_unilateral_pending);
                break;
            default:
                A0X = null;
                break;
        }
        switch (this.A00) {
            case 1:
            case 9:
            case 10:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                r3 = this.A06;
                i3 = R.string.payments_transaction_status_complete;
                i4 = R.color.payments_status_green;
                break;
            case 2:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A06.A04(A04, A0X, list, R.color.payments_status_gray);
                return;
            case 3:
            case 11:
                r3 = this.A06;
                i3 = R.string.payments_transaction_status_failed;
                i4 = R.color.payments_status_red;
                break;
            default:
                return;
        }
        r3.A04(r3.A01.getString(i3), A0X, list, i4);
    }

    public final void A03(List list, int i) {
        C120995h5 r4 = this.A05;
        C121035h9 r1 = r4.A00;
        boolean z = false;
        if (r1 != null) {
            z = true;
            Context context = this.A01;
            String string = context.getString(R.string.novi_payment_transaction_details_breakdown_sender_abtu_fee_add_label);
            AnonymousClass6F2 r12 = r1.A00.A01.A02;
            AnonymousClass009.A05(r12);
            list.add(C123335mz.A00(string, AnonymousClass613.A01(context, this.A02, r12), 2));
        }
        Context context2 = this.A01;
        int i2 = R.string.novi_payment_transaction_details_breakdown_sender_generic_fees_label;
        if (z) {
            i2 = R.string.novi_payment_transaction_details_breakdown_sender_fees_label;
        }
        String string2 = context2.getString(i2);
        AnonymousClass6F2 r13 = r4.A01.A02.A02;
        AnonymousClass009.A05(r13);
        list.add(C123335mz.A00(string2, AnonymousClass613.A01(context2, this.A02, r13), i));
    }
}
