package X;

import com.whatsapp.util.Log;
import java.util.concurrent.Callable;

/* renamed from: X.5Dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class CallableC112555Dv implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass5B1 A02;

    public /* synthetic */ CallableC112555Dv(AnonymousClass5B1 r1, int i, int i2) {
        this.A02 = r1;
        this.A00 = i;
        this.A01 = i2;
    }

    @Override // java.util.concurrent.Callable
    public final Object call() {
        int i;
        AnonymousClass5B1 r6 = this.A02;
        int i2 = this.A00;
        int i3 = this.A01;
        AbstractC94504bw r0 = r6.A03;
        if (r0 == null || !r0.A09()) {
            i = -6;
        } else {
            r6.A01 = i2;
            r6.A00 = i3;
            boolean z = r6.A0A;
            if (!z) {
                int i4 = 0;
                while (true) {
                    if (r6.A03.A02() == i2 && r6.A03.A01() == i3) {
                        break;
                    }
                    i4++;
                    if (i4 > 3) {
                        Log.w("failed to flush buffer to update window size, drop frame");
                        i = -4;
                        break;
                    }
                    r6.A01();
                }
            }
            r6.A08.setWindow(0, 0, i2, i3);
            if (!z) {
                r6.A01();
            }
            return 0;
        }
        return Integer.valueOf(i);
    }
}
