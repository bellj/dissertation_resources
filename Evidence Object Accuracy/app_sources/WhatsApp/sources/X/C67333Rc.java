package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Rc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67333Rc implements AbstractC009404s {
    public final C48892Ii A00;
    public final AnonymousClass3EX A01;
    public final UserJid A02;

    public C67333Rc(C48892Ii r1, AnonymousClass3EX r2, UserJid userJid) {
        this.A02 = userJid;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C48892Ii r0 = this.A00;
        UserJid userJid = this.A02;
        AnonymousClass3EX r13 = this.A01;
        C48302Fl r3 = r0.A00;
        AnonymousClass01J r5 = r3.A03;
        C14830m7 A0b = C12980iv.A0b(r5);
        C14850m9 A0S = C12960it.A0S(r5);
        C15570nT A0S2 = C12970iu.A0S(r5);
        AnonymousClass19T r12 = (AnonymousClass19T) r5.A2y.get();
        C19850um r8 = (C19850um) r5.A2v.get();
        C25801Aw r14 = (C25801Aw) r5.A33.get();
        AnonymousClass19Q A0Z = C12980iv.A0Z(r5);
        C25811Ax r10 = (C25811Ax) r5.A2w.get();
        C14820m6 A0Z2 = C12970iu.A0Z(r5);
        AnonymousClass01J r02 = r3.A01.A1E;
        return new C53842fM(AbstractC250617y.A00(r5.AO3), A0S2, r8, new C90094Mo(C12980iv.A0Y(r02), C12960it.A0S(r02)), r10, A0Z, r12, r13, r14, A0b, A0Z2, A0S, userJid, (C19840ul) r5.A1Q.get(), (C25681Ai) r5.AJE.get());
    }
}
