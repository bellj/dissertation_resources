package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1MJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MJ implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99954l8();
    public final String A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1MJ(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
    }

    public AnonymousClass1MJ(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }
}
