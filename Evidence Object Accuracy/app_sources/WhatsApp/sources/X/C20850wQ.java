package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0wQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20850wQ {
    public final C16590pI A00;
    public final C19350ty A01;
    public final C16490p7 A02;

    public C20850wQ(C16590pI r1, C19350ty r2, C16490p7 r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public C29851Uy A00() {
        C29851Uy r1;
        C16490p7 r3 = this.A02;
        r3.A04();
        ReentrantReadWriteLock.WriteLock writeLock = r3.A08;
        writeLock.lock();
        try {
            Log.i("msgstore-manager/initialize");
            synchronized (r3) {
                if (!r3.A01) {
                    r3.A05();
                    r3.A06();
                    r1 = new C29851Uy(2);
                } else {
                    r1 = new C29851Uy(0);
                }
            }
            return r1;
        } finally {
            writeLock.unlock();
        }
    }

    public void A01() {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("msgstore-manager/finish/db-is-ready ");
            C16490p7 r1 = this.A02;
            r1.A04();
            sb.append(r1.A01);
            Log.i(sb.toString());
            r1.A04();
            if (!r1.A01) {
                Log.w("msgstore-manager/finish/db is not ready yet", new Throwable());
            } else {
                r1.A00 = true;
            }
        }
    }

    public void A02() {
        C16490p7 r2 = this.A02;
        r2.A04();
        r2.A05.A02 = true;
        r2.A04();
        r2.A05();
        try {
            Context context = this.A00.A00;
            Intent intent = new Intent(context, Class.forName("com.whatsapp.Main"));
            intent.setFlags(268468224);
            context.startActivity(intent);
            this.A01.A04("MessageStoreLifecycleManager");
        } catch (ClassNotFoundException e) {
            Log.e(e);
        }
    }
}
