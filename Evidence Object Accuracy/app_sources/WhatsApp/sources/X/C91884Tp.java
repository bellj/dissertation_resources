package X;

import android.graphics.Paint;

/* renamed from: X.4Tp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91884Tp {
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public Paint.Cap A06;
    public Paint.Cap A07;

    public C91884Tp(C91874To r2) {
        this.A05 = r2.A05;
        this.A04 = r2.A04;
        this.A03 = r2.A03;
        this.A01 = r2.A01;
        this.A02 = r2.A02;
        this.A00 = r2.A00;
        this.A06 = r2.A06;
        this.A07 = r2.A07;
    }
}
