package X;

/* renamed from: X.0GK  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0GK extends AnonymousClass043 {
    public final C006503b A00 = C006503b.A01;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass0GK.class != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((AnonymousClass0GK) obj).A00);
    }

    public int hashCode() {
        return (AnonymousClass0GK.class.getName().hashCode() * 31) + this.A00.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Failure {mOutputData=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
