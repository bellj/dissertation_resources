package X;

import android.content.Context;
import android.os.ConditionVariable;

/* renamed from: X.216  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass216 extends AbstractC16350or {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ ConditionVariable A01;
    public final /* synthetic */ ConditionVariable A02;
    public final /* synthetic */ C22880zn A03;
    public final /* synthetic */ AnonymousClass10L A04;

    public AnonymousClass216(Context context, ConditionVariable conditionVariable, ConditionVariable conditionVariable2, C22880zn r4, AnonymousClass10L r5) {
        this.A03 = r4;
        this.A02 = conditionVariable;
        this.A01 = conditionVariable2;
        this.A04 = r5;
        this.A00 = context;
    }
}
