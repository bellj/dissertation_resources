package X;

import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import java.io.File;

/* renamed from: X.300  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass300 extends C67773Sv implements AbstractC14590lg {
    public final AnonymousClass237 A00;
    public final AnonymousClass1KC A01;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass300(android.content.Context r2, X.AnonymousClass237 r3, X.AnonymousClass1KC r4) {
        /*
            r1 = this;
            X.0lj r0 = r4.A08
            java.lang.Object r0 = r0.A00()
            X.1qg r0 = (X.C39871qg) r0
            if (r0 == 0) goto L_0x0018
            java.io.File r0 = r0.A01
            android.net.Uri r0 = android.net.Uri.fromFile(r0)
        L_0x0010:
            r1.<init>(r2, r0)
            r1.A01 = r4
            r1.A00 = r3
            return
        L_0x0018:
            r0 = 0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass300.<init>(android.content.Context, X.237, X.1KC):void");
    }

    @Override // X.C67773Sv, X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r3) {
        this.A01.A08.A03(this, null);
        return super.AYZ(r3);
    }

    @Override // X.AbstractC14590lg
    public /* bridge */ /* synthetic */ void accept(Object obj) {
        File file = ((C39871qg) obj).A01;
        Uri fromFile = Uri.fromFile(file);
        synchronized (this.A04) {
            if (!fromFile.equals(super.A01)) {
                super.A01 = fromFile;
                this.A02 = true;
            }
        }
        AnonymousClass237 r5 = this.A00;
        if (r5 != null) {
            if (!(r5.A01 == null || r5.A00 == file.length())) {
                r5.A02.A0H(new RunnableBRunnable0Shape6S0100000_I0_6(r5, 23));
            }
            r5.A00 = file.length();
        }
    }

    @Override // X.C67773Sv, X.AnonymousClass2BW
    public void close() {
        this.A01.A08.A02(this);
        super.close();
    }
}
