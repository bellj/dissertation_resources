package X;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.VideoSurfaceView;
import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.676  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass676 implements AbstractC21000wf {
    public final AnonymousClass12Q A00;
    public final C14900mE A01;
    public final C129055x6 A02;
    public final C130795zz A03;
    public final C128485wB A04;
    public final AnonymousClass01d A05;
    public final AnonymousClass018 A06;
    public final C252718t A07;
    public final AnonymousClass01H A08;

    public AnonymousClass676(AnonymousClass12Q r1, C14900mE r2, C129055x6 r3, C130795zz r4, C128485wB r5, AnonymousClass01d r6, AnonymousClass018 r7, C252718t r8, AnonymousClass01H r9) {
        this.A01 = r2;
        this.A07 = r8;
        this.A08 = r9;
        this.A05 = r6;
        this.A06 = r7;
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static void A00(SpannableStringBuilder spannableStringBuilder, C130745zu r4, Object obj) {
        int i = r4.A01;
        spannableStringBuilder.setSpan(obj, i, r4.A00 + i, 0);
    }

    public static /* synthetic */ void A01(C90894Pq r7, float f, float f2, float f3) {
        if (r7 != null) {
            double d = (double) f3;
            BigDecimal bigDecimal = new BigDecimal(((double) Math.round(((double) f) / d)) * d);
            BigDecimal bigDecimal2 = new BigDecimal(((double) Math.round(((double) f2) / d)) * d);
            float floatValue = bigDecimal.floatValue();
            float floatValue2 = bigDecimal2.floatValue();
            AnonymousClass28D r5 = r7.A01;
            AbstractC14200l1 r4 = r7.A02;
            C14260l7 r3 = r7.A00;
            C14210l2 r2 = new C14210l2();
            r2.A05(C64983Hr.A00((double) floatValue), 0);
            r2.A05(C64983Hr.A00((double) floatValue2), 1);
            C28701Oq.A01(r3, r5, r2.A03(), r4);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0024, code lost:
        if (r1 == false) goto L_0x0026;
     */
    @Override // X.AbstractC21000wf
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6X(android.content.Context r5, android.view.View r6, X.AnonymousClass024 r7, X.AnonymousClass024 r8, X.AnonymousClass024 r9, java.lang.Integer r10, java.lang.Integer r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, int r15, boolean r16) {
        /*
            r4 = this;
            android.app.Activity r0 = X.AnonymousClass12P.A00(r5)
            android.view.Window r1 = r0.getWindow()
            r0 = 8192(0x2000, float:1.14794E-41)
            r1.setFlags(r0, r0)
            r0 = 2131362706(0x7f0a0392, float:1.83452E38)
            android.view.View r3 = r6.findViewById(r0)
            com.whatsapp.CodeInputField r3 = (com.whatsapp.CodeInputField) r3
            if (r3 != 0) goto L_0x004a
            r2 = 0
            if (r12 == 0) goto L_0x0026
            java.lang.String r0 = "fb_pay"
            boolean r1 = r12.equals(r0)
            r0 = 2131558635(0x7f0d00eb, float:1.8742591E38)
            if (r1 != 0) goto L_0x0029
        L_0x0026:
            r0 = 2131558634(0x7f0d00ea, float:1.874259E38)
        L_0x0029:
            android.view.View r3 = android.view.View.inflate(r5, r0, r2)
            com.whatsapp.CodeInputField r3 = (com.whatsapp.CodeInputField) r3
            X.641 r0 = new X.641
            r0.<init>(r4)
            r3.setCustomSelectionActionModeCallback(r0)
            android.view.ViewGroup r6 = (android.view.ViewGroup) r6
            r6.addView(r3)
            X.66Z r1 = new X.66Z
            r1.<init>(r8, r9)
            if (r11 == 0) goto L_0x00b7
            int r0 = r11.intValue()
            r3.A07(r1, r15, r0)
        L_0x004a:
            if (r10 == 0) goto L_0x0053
            int r0 = r10.intValue()
            r3.setGravity(r0)
        L_0x0053:
            r2 = 0
            if (r14 == 0) goto L_0x00b3
            java.lang.String r0 = "error"
            boolean r0 = r14.equals(r0)
            if (r0 == 0) goto L_0x00b3
            r0 = 1
            r3.setErrorState(r0)
            r3.A05()
            java.lang.String r0 = "no_error"
            r7.accept(r0)
            X.3MH r0 = r3.A04
            r3.removeTextChangedListener(r0)
            X.66Z r1 = new X.66Z
            r1.<init>(r8, r9)
            if (r11 == 0) goto L_0x00af
            int r0 = r11.intValue()
            r3.A07(r1, r15, r0)
        L_0x007d:
            r1 = r16
            r3.setEnabled(r1)
            r3.setCursorVisible(r2)
            if (r13 == 0) goto L_0x0094
            java.lang.String r0 = r3.getCode()
            boolean r0 = r13.equals(r0)
            if (r0 != 0) goto L_0x0094
            r3.setCode(r13)
        L_0x0094:
            if (r16 == 0) goto L_0x00bb
            r3.requestFocus()
            java.lang.Object r2 = r3.getTag()
            java.lang.Runnable r2 = (java.lang.Runnable) r2
            if (r2 != 0) goto L_0x00a9
            X.6Hl r2 = new X.6Hl
            r2.<init>(r3, r4)
            r3.setTag(r2)
        L_0x00a9:
            r0 = 0
            r3.postDelayed(r2, r0)
            return
        L_0x00af:
            r3.A06(r1, r15)
            goto L_0x007d
        L_0x00b3:
            r3.setErrorState(r2)
            goto L_0x007d
        L_0x00b7:
            r3.A06(r1, r15)
            goto L_0x004a
        L_0x00bb:
            r3.A05()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass676.A6X(android.content.Context, android.view.View, X.024, X.024, X.024, java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, int, boolean):void");
    }

    @Override // X.AbstractC21000wf
    public void A6Z(Context context, View view, C90884Pp r16, String str, String str2, String str3, int i, boolean z) {
        Long l;
        TextView A0I = C12960it.A0I(view, R.id.hintOrDate);
        TextInputLayout textInputLayout = (TextInputLayout) AnonymousClass028.A0D(view, R.id.inputView);
        if (str != null) {
            textInputLayout.setHint(str);
        }
        Calendar instance = Calendar.getInstance();
        if (!TextUtils.isEmpty(str2)) {
            Date A01 = C1308860i.A01(str2);
            if (A01 != null) {
                AnonymousClass009.A05(A01);
                A0I.setText(DateFormat.getDateInstance(1).format(A01));
                instance.setTime(A01);
            }
        } else if (i != 0) {
            instance.set(1, instance.get(1) + i);
        }
        AnonymousClass61v r8 = new DatePickerDialog.OnDateSetListener() { // from class: X.61v
            @Override // android.app.DatePickerDialog.OnDateSetListener
            public final void onDateSet(DatePicker datePicker, int i2, int i3, int i4) {
                C90884Pp r4 = C90884Pp.this;
                Calendar instance2 = Calendar.getInstance();
                instance2.set(1, i2);
                instance2.set(2, i3);
                instance2.set(5, i4);
                String format = new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(instance2.getTime());
                C57812nf r7 = r4.A02;
                C14260l7 r3 = r4.A00;
                AnonymousClass28D r2 = r4.A01;
                AnonymousClass3JV.A03(r3).A07(new C82793wC(r7, format), (long) r2.A00);
                AbstractC14200l1 A0G = r2.A0G(44);
                if (A0G != null) {
                    C14210l2 r42 = new C14210l2();
                    r42.A05(format, 0);
                    r42.A05(Integer.valueOf(i4), 1);
                    r42.A05(Integer.valueOf(i3), 2);
                    r42.A05(Integer.valueOf(i2), 3);
                    C28701Oq.A01(r3, r2, r42.A03(), A0G);
                }
            }
        };
        A0I.setEnabled(z);
        A0I.setClickable(z);
        try {
            l = Long.valueOf(Long.parseLong(str3));
        } catch (NumberFormatException unused) {
            Log.e(C12960it.A0d(str3, C12960it.A0k("WaBkComponentConfiguratorImpl/WaDatePicker/bind Max date is not a valid date format")));
            l = null;
        }
        if (z) {
            DialogInterface$OnClickListenerC117805aj r7 = new DialogInterface$OnClickListenerC117805aj(r8, context, instance.get(1), instance.get(2), instance.get(5));
            if (l != null) {
                r7.A01.setMaxDate(l.longValue());
            }
            A0I.setOnClickListener(new View.OnClickListener() { // from class: X.644
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    DialogInterface$OnClickListenerC117805aj.this.show();
                }
            });
            return;
        }
        A0I.setOnClickListener(null);
    }

    @Override // X.AbstractC21000wf
    public void A6a(ImageView imageView, AbstractC11740gm r19, AbstractC11740gm r20, Object obj, String str, String str2, String str3, String str4) {
        File A00;
        String str5 = str2;
        if (!TextUtils.isEmpty(str)) {
            C130795zz r6 = this.A03;
            C129055x6 r2 = this.A02;
            if (!TextUtils.isEmpty(str5)) {
                String str6 = str5;
                if (str3 != null && C12970iu.A1Y(r19.get())) {
                    str6 = str3;
                }
                r6.A00(new C1324466x(imageView, r20), str6);
            }
            r2.A00().A00(null, null, imageView, new C134046Dc(imageView, r20, r19, r6, str5, str3, C12970iu.A10(imageView), C12970iu.A10(obj)), str);
        } else if (!TextUtils.isEmpty(str5)) {
            C130795zz r1 = this.A03;
            if (str3 != null && C12970iu.A1Y(r19.get())) {
                str5 = str3;
            }
            r1.A00(new C1324466x(imageView, r20), str5);
        } else if (!TextUtils.isEmpty(str4) && (A00 = this.A04.A00(str4)) != null) {
            imageView.setImageBitmap(BitmapFactory.decodeFile(A00.getAbsolutePath()));
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:7:0x0014 */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0, types: [android.view.View] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.lang.String] */
    @Override // X.AbstractC21000wf
    public void A6b(Context context, View view, AnonymousClass024 r20, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, boolean z2) {
        String str9 = view;
        RecyclerView recyclerView = (RecyclerView) AnonymousClass028.A0D(str9, R.id.recycler_view);
        str9.getContext();
        C12990iw.A1K(recyclerView);
        recyclerView.A0h = true;
        try {
            str9 = str;
            recyclerView.setAdapter(new C118565c1(LayoutInflater.from(context), r20, this.A02.A00(), str2, str3, str4, str5, str6, str7, str8, new JSONArray((String) str9), z, z2));
        } catch (JSONException unused) {
            Log.e(C12960it.A0d(str9, C12960it.A0k("WaBkComponentConfiguratorImpl/bindView data source is not a valid JSON: ")));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006a, code lost:
        if (r2 != false) goto L_0x0040;
     */
    @Override // X.AbstractC21000wf
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6c(android.view.View r5, X.C90894Pq r6, float r7, int r8, int r9, int r10, int r11) {
        /*
            r4 = this;
            r0 = 2131362272(0x7f0a01e0, float:1.834432E38)
            android.view.View r3 = X.AnonymousClass028.A0D(r5, r0)
            com.whatsapp.bloks.ui.widgets.rangeslider.WaRangeSeekBar r3 = (com.whatsapp.bloks.ui.widgets.rangeslider.WaRangeSeekBar) r3
            float r2 = (float) r8
            float r1 = (float) r9
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0046
            r3.A01 = r2
            r3.A00 = r1
            float r0 = r3.A03
            boolean r0 = java.lang.Float.isNaN(r0)
            if (r0 == 0) goto L_0x002a
            float r0 = r3.A02
            boolean r0 = java.lang.Float.isNaN(r0)
            if (r0 == 0) goto L_0x002a
            r3.A03 = r2
            r3.A02 = r1
            r3.A02()
        L_0x002a:
            r2 = 0
            float r0 = r3.A03
            float r1 = r3.A01
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0036
            r3.A03 = r1
            r2 = 1
        L_0x0036:
            float r0 = r3.A02
            float r1 = r3.A00
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x006a
            r3.A02 = r1
        L_0x0040:
            r3.invalidate()
            r3.A02()
        L_0x0046:
            float r2 = (float) r10
            float r1 = (float) r11
            float r0 = r3.A01
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x0062
            float r0 = r3.A00
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x0062
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0062
            r3.A03 = r2
            r3.A02 = r1
            r3.invalidate()
            r3.A02()
        L_0x0062:
            X.67T r0 = new X.67T
            r0.<init>(r6, r7)
            r3.A0D = r0
            return
        L_0x006a:
            if (r2 == 0) goto L_0x0046
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass676.A6c(android.view.View, X.4Pq, float, int, int, int, int):void");
    }

    @Override // X.AbstractC21000wf
    public void A6d(View view, String str, boolean z) {
        int i;
        File A00;
        VideoSurfaceView videoSurfaceView = (VideoSurfaceView) AnonymousClass028.A0D(view, R.id.video_view);
        View A0D = AnonymousClass028.A0D(view, R.id.loading_progress);
        View A0D2 = AnonymousClass028.A0D(view, R.id.play_button);
        String str2 = null;
        if (!TextUtils.isEmpty(str) && (A00 = this.A04.A00(str)) != null) {
            str2 = A00.getAbsolutePath();
        }
        if (!TextUtils.isEmpty(str2)) {
            videoSurfaceView.setVideoPath(str2);
        }
        videoSurfaceView.A0B = new MediaPlayer.OnPreparedListener(A0D) { // from class: X.63O
            public final /* synthetic */ View A00;

            {
                this.A00 = r1;
            }

            @Override // android.media.MediaPlayer.OnPreparedListener
            public final void onPrepared(MediaPlayer mediaPlayer) {
                this.A00.setVisibility(8);
            }
        };
        videoSurfaceView.A09 = new MediaPlayer.OnCompletionListener(A0D2) { // from class: X.63M
            public final /* synthetic */ View A00;

            {
                this.A00 = r1;
            }

            @Override // android.media.MediaPlayer.OnCompletionListener
            public final void onCompletion(MediaPlayer mediaPlayer) {
                this.A00.setVisibility(0);
            }
        };
        A0D2.setOnClickListener(new View.OnClickListener(A0D2, videoSurfaceView) { // from class: X.645
            public final /* synthetic */ View A00;
            public final /* synthetic */ VideoSurfaceView A01;

            {
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                View view3 = this.A00;
                VideoSurfaceView videoSurfaceView2 = this.A01;
                view3.setVisibility(8);
                if (!videoSurfaceView2.isPlaying()) {
                    videoSurfaceView2.start();
                }
            }
        });
        if (z) {
            videoSurfaceView.start();
            i = 8;
        } else {
            i = 0;
        }
        A0D2.setVisibility(i);
    }

    @Override // X.AbstractC21000wf
    public void A94(ImageView imageView, String str, String str2) {
        ((C129125xD) this.A08.get()).A00(imageView, str, str2);
    }

    @Override // X.AbstractC21000wf
    public Spannable AD8(Context context, Context context2, AnonymousClass024 r15, String str, List list, List list2, List list3, List list4) {
        SpannableStringBuilder spannableStringBuilder;
        if (str == null) {
            return null;
        }
        Iterator it = AnonymousClass1Z4.A01.keySet().iterator();
        while (true) {
            if (it.hasNext()) {
                if (str.contains(C12970iu.A0x(it))) {
                    spannableStringBuilder = new SpannableStringBuilder(AnonymousClass1Z4.A00(context, str));
                    break;
                }
            } else {
                spannableStringBuilder = new SpannableStringBuilder(str);
                break;
            }
        }
        if (list != null) {
            Iterator it2 = list.iterator();
            while (it2.hasNext()) {
                C119545ec r1 = new C119545ec(C13000ix.A05(C12970iu.A0x(it2)));
                String str2 = r1.A00;
                switch (str2.hashCode()) {
                    case -2125451728:
                        if (!str2.equals("ITALIC")) {
                            break;
                        } else {
                            A00(spannableStringBuilder, r1, new StyleSpan(2));
                            break;
                        }
                    case 2044549:
                        if (!str2.equals("BOLD")) {
                            break;
                        } else {
                            A00(spannableStringBuilder, r1, new StyleSpan(1));
                            break;
                        }
                    case 1759631020:
                        if (!str2.equals("UNDERLINE")) {
                            break;
                        } else {
                            A00(spannableStringBuilder, r1, new UnderlineSpan());
                            break;
                        }
                    case 2143721139:
                        if (!str2.equals("STRIKETHROUGH")) {
                            break;
                        } else {
                            A00(spannableStringBuilder, r1, new StrikethroughSpan());
                            break;
                        }
                }
            }
        }
        if (list2 != null) {
            Iterator it3 = list2.iterator();
            while (it3.hasNext()) {
                C119515eZ r2 = new C119515eZ(C13000ix.A05(C12970iu.A0x(it3)));
                A00(spannableStringBuilder, r2, new ForegroundColorSpan(r2.A00));
            }
        }
        if (list3 != null) {
            Iterator it4 = list3.iterator();
            while (it4.hasNext()) {
                C119525ea r12 = new C119525ea(C13000ix.A05(C12970iu.A0x(it4)));
                C58272oQ r6 = new C58272oQ(context2, this.A00, this.A01, this.A05, r12.A00);
                r6.A02 = new AnonymousClass5TE(r12) { // from class: X.66f
                    public final /* synthetic */ C119525ea A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AnonymousClass5TE
                    public final void A7H() {
                        AnonymousClass024.this.accept(this.A01.A00);
                    }
                };
                A00(spannableStringBuilder, r12, r6);
            }
        }
        if (list4 == null) {
            return spannableStringBuilder;
        }
        Iterator it5 = list4.iterator();
        while (it5.hasNext()) {
            C119535eb r4 = new C119535eb(C13000ix.A05(C12970iu.A0x(it5)));
            A00(spannableStringBuilder, r4, new RelativeSizeSpan((float) r4.A00));
        }
        return spannableStringBuilder;
    }

    @Override // X.AbstractC21000wf
    public TextWatcher AE6(TextInputEditText textInputEditText, Integer num, String str, String str2) {
        if ((num.intValue() & 2) != 2) {
            return new AnonymousClass640(textInputEditText, str);
        }
        if (str == null) {
            str = str2;
        }
        return new C119505eY(textInputEditText, str);
    }

    @Override // X.AbstractC21000wf
    public InputFilter AEg() {
        return new C1317063u();
    }

    @Override // X.AbstractC21000wf
    public CharacterStyle AHg(Runnable runnable, int i, int i2, int i3) {
        return new C119175d2(runnable, i);
    }

    @Override // X.AbstractC21000wf
    public Locale AHl() {
        return C12970iu.A14(this.A06);
    }

    @Override // X.AbstractC21000wf
    public void Ad9(View view, long j) {
        C12960it.A0I(view, R.id.timer_text).setText(C38131nZ.A04(this.A06, j));
    }

    @Override // X.AbstractC21000wf
    public void AeY() {
        C51282Tp.A01(this.A05);
    }

    @Override // X.AbstractC21000wf
    public void AfA(View view) {
        C12960it.A0I(view, R.id.start_message).setText("");
        C12960it.A0I(view, R.id.timer_text).setText("");
    }
}
