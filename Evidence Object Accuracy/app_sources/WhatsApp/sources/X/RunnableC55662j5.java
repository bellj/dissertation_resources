package X;

import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.2j5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class RunnableC55662j5 extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AbstractC14640lm A02;
    public final /* synthetic */ C15580nU A03;
    public final /* synthetic */ C20750wG A04;
    public final /* synthetic */ String A05 = "url";

    public /* synthetic */ RunnableC55662j5(AbstractC14640lm r2, C15580nU r3, C20750wG r4, int i, int i2) {
        this.A04 = r4;
        this.A00 = i;
        this.A02 = r2;
        this.A01 = i2;
        this.A03 = r3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        String str;
        C20750wG r15 = this.A04;
        int i = this.A00;
        String str2 = this.A05;
        AbstractC14640lm r3 = this.A02;
        int i2 = this.A01;
        C15580nU r8 = this.A03;
        StringBuilder A0k = C12960it.A0k("ProfilePhotoManager/sendGetSubProfilePhoto photoId:");
        A0k.append(i);
        A0k.append(" query type:");
        A0k.append(str2);
        A0k.append(" jid:");
        A0k.append(r3);
        A0k.append(" image type:");
        A0k.append(i2);
        C12960it.A1F(A0k);
        if (i2 == 1) {
            str = "image";
        } else {
            str = "preview";
        }
        C17220qS r2 = r15.A0D;
        C16590pI r12 = r15.A09;
        C19930uu r7 = r15.A0G;
        AbstractC14440lR r6 = r15.A0H;
        String str3 = null;
        C63973Dr r1 = new C63973Dr(r2, new C69423Ze(r15.A04, r15.A07, r12, r15.A0B, r15.A0C, r15, null, r15.A0E, r15.A0F, r7, r6));
        GroupJid groupJid = (GroupJid) r3;
        if (i > 0) {
            str3 = Integer.toString(i);
        }
        r1.A00(groupJid, r8, r1.A00.A01(), null, str3, str, str2);
    }
}
