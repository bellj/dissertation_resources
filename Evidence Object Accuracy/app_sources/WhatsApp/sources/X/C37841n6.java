package X;

/* renamed from: X.1n6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37841n6 {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = -1;
    public int A04 = -1;
    public int A05 = 0;
    public boolean A06;

    public C37841n6(int i, int i2, int i3, boolean z) {
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
        this.A06 = z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("container: ");
        sb.append(this.A01);
        sb.append(", video: ");
        sb.append(this.A02);
        sb.append(", audio: ");
        sb.append(this.A00);
        sb.append(", problems: ");
        sb.append(this.A06);
        return sb.toString();
    }
}
