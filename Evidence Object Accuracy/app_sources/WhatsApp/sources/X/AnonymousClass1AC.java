package X;

/* renamed from: X.1AC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AC {
    public AbstractC15710nm A00;
    public C14900mE A01;
    public C15450nH A02;
    public AnonymousClass01d A03;
    public C14820m6 A04;
    public AnonymousClass018 A05;
    public C14850m9 A06;
    public C16120oU A07;
    public C16630pM A08;
    public C18120rw A09;
    public C22210yi A0A;
    public AnonymousClass1KT A0B;
    public C252718t A0C;
    public AbstractC14440lR A0D;

    public AnonymousClass1AC(AbstractC15710nm r7, C14900mE r8, C15450nH r9, AnonymousClass01d r10, C14820m6 r11, AnonymousClass018 r12, C14850m9 r13, C16120oU r14, C16630pM r15, C18120rw r16, C22210yi r17, AnonymousClass146 r18, C235512c r19, C252718t r20, AbstractC14440lR r21) {
        this.A06 = r13;
        this.A0C = r20;
        this.A00 = r7;
        this.A01 = r8;
        this.A0D = r21;
        this.A07 = r14;
        this.A02 = r9;
        this.A0B = new AnonymousClass1KT(r11, r17, r18, r19, r21);
        this.A03 = r10;
        this.A05 = r12;
        this.A04 = r11;
        this.A0A = r17;
        this.A08 = r15;
        this.A09 = r16;
    }
}
