package X;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.0bn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC08750bn implements AbstractFutureC44231yX {
    public static final AbstractC05550Pz A00;
    public static final Object A01 = new Object();
    public static final Logger A02 = Logger.getLogger(AbstractC08750bn.class.getName());
    public static final boolean A03 = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    public volatile C05910Rl listeners;
    public volatile Object value;
    public volatile C06320Td waiters;

    static {
        AbstractC05550Pz r4;
        Throwable th;
        try {
            r4 = new C02490Cm(AtomicReferenceFieldUpdater.newUpdater(C06320Td.class, Thread.class, "thread"), AtomicReferenceFieldUpdater.newUpdater(C06320Td.class, C06320Td.class, "next"), AtomicReferenceFieldUpdater.newUpdater(AbstractC08750bn.class, C06320Td.class, "waiters"), AtomicReferenceFieldUpdater.newUpdater(AbstractC08750bn.class, C05910Rl.class, "listeners"), AtomicReferenceFieldUpdater.newUpdater(AbstractC08750bn.class, Object.class, "value"));
            th = null;
        } catch (Throwable th2) {
            th = th2;
            r4 = new C02480Cl();
        }
        A00 = r4;
        if (th != null) {
            A02.log(Level.SEVERE, "SafeAtomicHelper is broken!", th);
        }
    }

    public static final Object A01(Object obj) {
        if (obj instanceof C05810Rb) {
            Throwable th = ((C05810Rb) obj).A00;
            CancellationException cancellationException = new CancellationException("Task was cancelled.");
            cancellationException.initCause(th);
            throw cancellationException;
        } else if (obj instanceof AnonymousClass0RY) {
            throw new ExecutionException(((AnonymousClass0RY) obj).A00);
        } else if (obj == A01) {
            return null;
        } else {
            return obj;
        }
    }

    public static void A02(AbstractC08750bn r4) {
        C06320Td r1;
        AbstractC05550Pz r2;
        C05910Rl r12;
        C05910Rl r3 = null;
        do {
            r1 = r4.waiters;
            r2 = A00;
        } while (!r2.A03(r1, C06320Td.A00, r4));
        while (r1 != null) {
            Thread thread = r1.thread;
            if (thread != null) {
                r1.thread = null;
                LockSupport.unpark(thread);
            }
            r1 = r1.next;
        }
        do {
            r12 = r4.listeners;
        } while (!r2.A02(r12, C05910Rl.A03, r4));
        while (r12 != null) {
            C05910Rl r0 = r12.A00;
            r12.A00 = r3;
            r3 = r12;
            r12 = r0;
        }
        while (r3 != null) {
            C05910Rl r22 = r3.A00;
            A03(r3.A01, r3.A02);
            r3 = r22;
        }
    }

    public static void A03(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e) {
            Logger logger = A02;
            Level level = Level.SEVERE;
            StringBuilder sb = new StringBuilder("RuntimeException while executing runnable ");
            sb.append(runnable);
            sb.append(" with executor ");
            sb.append(executor);
            logger.log(level, sb.toString(), (Throwable) e);
        }
    }

    public String A04() {
        if (!(this instanceof ScheduledFuture)) {
            return null;
        }
        StringBuilder sb = new StringBuilder("remaining delay=[");
        sb.append(((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS));
        sb.append(" ms]");
        return sb.toString();
    }

    public final void A05(C06320Td r6) {
        r6.thread = null;
        while (true) {
            C06320Td r3 = this.waiters;
            if (r3 != C06320Td.A00) {
                C06320Td r2 = null;
                while (r3 != null) {
                    C06320Td r1 = r3.next;
                    if (r3.thread != null) {
                        r2 = r3;
                    } else if (r2 != null) {
                        r2.next = r1;
                        if (r2.thread == null) {
                            break;
                        }
                    } else if (!A00.A03(r3, r1, this)) {
                        break;
                    }
                    r3 = r1;
                }
                return;
            }
            return;
        }
    }

    public void A06(Throwable th) {
        if (A00.A04(this, null, new AnonymousClass0RY(th))) {
            A02(this);
        }
    }

    public boolean A07(Object obj) {
        if (obj == null) {
            obj = A01;
        }
        if (!A00.A04(this, null, obj)) {
            return false;
        }
        A02(this);
        return true;
    }

    @Override // X.AbstractFutureC44231yX
    public final void A5i(Runnable runnable, Executor executor) {
        C05910Rl r3 = this.listeners;
        C05910Rl r2 = C05910Rl.A03;
        if (r3 != r2) {
            C05910Rl r1 = new C05910Rl(runnable, executor);
            do {
                r1.A00 = r3;
                if (!A00.A02(r3, r1, this)) {
                    r3 = this.listeners;
                } else {
                    return;
                }
            } while (r3 != r2);
            A03(runnable, executor);
        }
        A03(runnable, executor);
    }

    @Override // java.util.concurrent.Future
    public final boolean cancel(boolean z) {
        C05810Rb r1;
        Object obj = this.value;
        if (obj != null) {
            return false;
        }
        if (A03) {
            r1 = new C05810Rb(new CancellationException("Future.cancel() was called."));
        } else {
            r1 = z ? C05810Rb.A02 : C05810Rb.A01;
        }
        if (!A00.A04(this, obj, r1)) {
            return false;
        }
        A02(this);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002b, code lost:
        java.util.concurrent.locks.LockSupport.park(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0032, code lost:
        if (java.lang.Thread.interrupted() != false) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0034, code lost:
        r0 = r4.value;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        if (r0 == null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0039, code lost:
        A05(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        throw new java.lang.InterruptedException();
     */
    @Override // java.util.concurrent.Future
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object get() {
        /*
            r4 = this;
            boolean r0 = java.lang.Thread.interrupted()
            if (r0 != 0) goto L_0x0042
            java.lang.Object r0 = r4.value
            if (r0 != 0) goto L_0x0026
            X.0Td r3 = r4.waiters
            X.0Td r2 = X.C06320Td.A00
            if (r3 == r2) goto L_0x0024
            X.0Td r1 = new X.0Td
            r1.<init>()
        L_0x0015:
            r1.A00(r3)
            X.0Pz r0 = X.AbstractC08750bn.A00
            boolean r0 = r0.A03(r3, r1, r4)
            if (r0 != 0) goto L_0x002b
            X.0Td r3 = r4.waiters
            if (r3 != r2) goto L_0x0015
        L_0x0024:
            java.lang.Object r0 = r4.value
        L_0x0026:
            java.lang.Object r0 = A01(r0)
            return r0
        L_0x002b:
            java.util.concurrent.locks.LockSupport.park(r4)
            boolean r0 = java.lang.Thread.interrupted()
            if (r0 != 0) goto L_0x0039
            java.lang.Object r0 = r4.value
            if (r0 == 0) goto L_0x002b
            goto L_0x0026
        L_0x0039:
            r4.A05(r1)
            java.lang.InterruptedException r0 = new java.lang.InterruptedException
            r0.<init>()
            throw r0
        L_0x0042:
            java.lang.InterruptedException r0 = new java.lang.InterruptedException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC08750bn.get():java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        java.util.concurrent.locks.LockSupport.parkNanos(r15, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0053, code lost:
        if (java.lang.Thread.interrupted() != false) goto L_0x016b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0055, code lost:
        r0 = r15.value;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0057, code lost:
        if (r0 != null) goto L_0x0166;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        r0 = r9 - java.lang.System.nanoTime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        if (r0 >= 1000) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        A05(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00cf, code lost:
        if (r2 > 1000) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x016b, code lost:
        A05(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0173, code lost:
        throw new java.lang.InterruptedException();
     */
    @Override // java.util.concurrent.Future
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object get(long r16, java.util.concurrent.TimeUnit r18) {
        /*
        // Method dump skipped, instructions count: 378
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC08750bn.get(long, java.util.concurrent.TimeUnit):java.lang.Object");
    }

    @Override // java.util.concurrent.Future
    public final boolean isCancelled() {
        return this.value instanceof C05810Rb;
    }

    @Override // java.util.concurrent.Future
    public final boolean isDone() {
        boolean z = false;
        if (this.value != null) {
            z = true;
        }
        return true & z;
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        String str2;
        Object obj;
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (isCancelled()) {
            str2 = "CANCELLED";
        } else {
            if (!isDone()) {
                try {
                    str = A04();
                } catch (RuntimeException e) {
                    StringBuilder sb2 = new StringBuilder("Exception thrown from implementation: ");
                    sb2.append(e.getClass());
                    str = sb2.toString();
                }
                if (str != null && !str.isEmpty()) {
                    sb.append("PENDING, info=[");
                    sb.append(str);
                    sb.append("]");
                    sb.append("]");
                    return sb.toString();
                } else if (!isDone()) {
                    str2 = "PENDING";
                }
            }
            boolean z = false;
            while (true) {
                try {
                    try {
                        obj = get();
                        break;
                    } catch (InterruptedException unused) {
                        z = true;
                    } catch (Throwable th) {
                        if (z) {
                            Thread.currentThread().interrupt();
                        }
                        throw th;
                    }
                } catch (CancellationException unused2) {
                    str2 = "CANCELLED";
                } catch (RuntimeException e2) {
                    sb.append("UNKNOWN, cause=[");
                    sb.append(e2.getClass());
                    str2 = " thrown from get()]";
                } catch (ExecutionException e3) {
                    sb.append("FAILURE, cause=[");
                    sb.append(e3.getCause());
                    sb.append("]");
                }
            }
            if (z) {
                Thread.currentThread().interrupt();
            }
            sb.append("SUCCESS, result=[");
            if (obj == this) {
                str3 = "this future";
            } else {
                str3 = String.valueOf(obj);
            }
            sb.append(str3);
            sb.append("]");
            sb.append("]");
            return sb.toString();
        }
        sb.append(str2);
        sb.append("]");
        return sb.toString();
    }
}
