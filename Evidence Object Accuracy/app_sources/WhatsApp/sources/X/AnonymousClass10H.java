package X;

import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;

/* renamed from: X.10H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10H {
    public final List A00;

    public AnonymousClass10H() {
        try {
            List asList = Arrays.asList(new AnonymousClass218[0]);
            List asList2 = Arrays.asList(new AnonymousClass218[0]);
            List asList3 = Arrays.asList(new AnonymousClass218[0]);
            Boolean bool = Boolean.TRUE;
            AnonymousClass21D[] r2 = {new AnonymousClass21D(bool, 1780)};
            Boolean bool2 = Boolean.FALSE;
            this.A00 = Collections.unmodifiableList(Arrays.asList(new AnonymousClass217(new AnonymousClass219(4, "platform", "android"), "offline_aa", asList), new AnonymousClass217(new AnonymousClass21B(new AnonymousClass21B(new AnonymousClass219(4, "platform", "smba"), new AnonymousClass219(4, "platform", "android"), 1), new AnonymousClass219(4, "release_channel", "beta"), 0), "registration_offline_universe_beta", asList2), new AnonymousClass217(new AnonymousClass21B(new AnonymousClass21B(new AnonymousClass219(4, "platform", "smba"), new AnonymousClass219(4, "platform", "android"), 1), new AnonymousClass219(4, "release_channel", "release"), 0), "registration_offline_universe_release", asList3), new AnonymousClass217(new AnonymousClass21B(new AnonymousClass219(Arrays.asList("android", "smba")), new AnonymousClass219(4, "release_channel", "beta"), 0), "code_input_box_user_rid_android_beta_universe", Arrays.asList(new AnonymousClass218(new AnonymousClass219(9, "app_version", "2.22.16.12"), "code_input_box_user_rid_android_beta_experiment", Arrays.asList(new AnonymousClass21C(Arrays.asList(r2), 5000, "test"), new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool2, 1780)), 5000, "control")), 1659510000, 1660719600))), new AnonymousClass217(new AnonymousClass21B(new AnonymousClass219(Arrays.asList("android", "smba")), new AnonymousClass219(4, "release_channel", "release"), 0), "code_input_box_new_ui_android_prod_universe", Arrays.asList(new AnonymousClass218(new AnonymousClass219(9, "app_version", "2.22.16.12"), "code_input_box_new_ui_android_prod_experiment", Arrays.asList(new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool, 1780)), 500, "test"), new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool2, 1780)), 500, "control")), 1659337200, 1660546800), new AnonymousClass218(new AnonymousClass219(9, "app_version", "2.22.16.12"), "code_input_box_new_ui_android_prod_10_percent_experiment", Arrays.asList(new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool, 1780)), 1000, "test"), new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool2, 1780)), 1000, "control")), 1660546800, 1661756400))), new AnonymousClass217(new AnonymousClass21B(new AnonymousClass219(4, "platform", "android"), new AnonymousClass219(4, "release_channel", "release"), 0), "language_selector_universe_phase2", Arrays.asList(new AnonymousClass218(null, "language_selector_experiment_phase2", Arrays.asList(new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool2, 1778)), 2500, "control"), new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool, 1778)), 2500, "test")), 1655708400, 1660978800))), new AnonymousClass217(new AnonymousClass21B(new AnonymousClass219(4, "platform", "android"), new AnonymousClass219(4, "release_channel", "release"), 0), "language_selector_universe_az", Arrays.asList(new AnonymousClass218(null, "language_selector_experiment_az", Arrays.asList(new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool2, 1779)), 5000, "control"), new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool, 1779)), 5000, "test")), 1655708400, 1660978800))), new AnonymousClass217(new AnonymousClass219(4, "platform", "android"), "dummy_aa_offline_rid_universe", Arrays.asList(new AnonymousClass218(null, "dummy_aa_offline_rid_experiment", Arrays.asList(new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool2, 1957)), 5000, "control"), new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool, 1957)), 5000, "test")), 1658386800, 1664521200))), new AnonymousClass217(new AnonymousClass21B(new AnonymousClass219(Arrays.asList("android", "smba")), new AnonymousClass219(4, "release_channel", "beta"), 0), "skip_backup_flow_for_new_reg_users_universe", Arrays.asList(new AnonymousClass218(null, "skip_backup_flow_for_new_reg_users_experiment", Arrays.asList(new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool2, 1942)), 5000, "control"), new AnonymousClass21C(Arrays.asList(new AnonymousClass21D(bool, 1942)), 5000, "test")), 1658991600, 1664348400)))));
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder("ABConfig/invalid json format for ab property from code gen:");
            sb.append(e.toString());
            Log.w(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ABConfig/invalid json format for ab property from code gen:");
            sb2.append(e.toString());
            throw new RuntimeException(sb2.toString());
        }
    }
}
