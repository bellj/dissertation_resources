package X;

import android.os.Handler;
import android.os.Looper;
import com.google.android.exoplayer2.Timeline;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.3Fq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64463Fq {
    public AnonymousClass5XM A00 = new C107634xh(new Random(), new int[0]);
    public AnonymousClass5QP A01;
    public boolean A02;
    public final AnonymousClass5Pw A03;
    public final AnonymousClass4P0 A04;
    public final AnonymousClass1Or A05;
    public final HashMap A06;
    public final IdentityHashMap A07 = new IdentityHashMap();
    public final List A08 = C12960it.A0l();
    public final Map A09 = C12970iu.A11();
    public final Set A0A;

    public C64463Fq(Handler handler, AnonymousClass5Pw r7, C106424vg r8) {
        this.A03 = r7;
        AnonymousClass1Or r1 = new AnonymousClass1Or(null, new CopyOnWriteArrayList(), 0);
        this.A05 = r1;
        AnonymousClass4P0 r2 = new AnonymousClass4P0(null, new CopyOnWriteArrayList(), 0);
        this.A04 = r2;
        this.A06 = C12970iu.A11();
        this.A0A = C12970iu.A12();
        r1.A02.add(new AnonymousClass4M2(handler, r8));
        r2.A02.add(new C89874Ls(handler, r8));
    }

    public Timeline A00() {
        List list = this.A08;
        if (list.isEmpty()) {
            return Timeline.A00;
        }
        int i = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            C67613Se r0 = (C67613Se) list.get(i2);
            r0.A00 = i;
            i += r0.A02.A01.A01();
        }
        return new C76503lk(this.A00, list);
    }

    public Timeline A01(AnonymousClass5XM r7, List list, int i) {
        int i2;
        List list2;
        if (!list.isEmpty()) {
            this.A00 = r7;
            for (int i3 = i; i3 < list.size() + i; i3++) {
                C67613Se r3 = (C67613Se) list.get(i3 - i);
                if (i3 > 0) {
                    C67613Se r1 = (C67613Se) this.A08.get(i3 - 1);
                    i2 = r1.A00 + r1.A02.A01.A01();
                } else {
                    i2 = 0;
                }
                r3.A00 = i2;
                r3.A01 = false;
                r3.A04.clear();
                int A01 = r3.A02.A01.A01();
                int i4 = i3;
                while (true) {
                    list2 = this.A08;
                    if (i4 >= list2.size()) {
                        break;
                    }
                    ((C67613Se) list2.get(i4)).A00 += A01;
                    i4++;
                }
                list2.add(i3, r3);
                this.A09.put(r3.A03, r3);
                if (this.A02) {
                    A05(r3);
                    if (this.A07.isEmpty()) {
                        this.A0A.add(r3);
                    } else {
                        C90694Ow r0 = (C90694Ow) this.A06.get(r3);
                        if (r0 != null) {
                            r0.A01.A8u(r0.A00);
                        }
                    }
                }
            }
        }
        return A00();
    }

    public final void A02() {
        Iterator it = this.A0A.iterator();
        while (it.hasNext()) {
            C67613Se r1 = (C67613Se) it.next();
            if (r1.A04.isEmpty()) {
                C90694Ow r0 = (C90694Ow) this.A06.get(r1);
                if (r0 != null) {
                    r0.A01.A8u(r0.A00);
                }
                it.remove();
            }
        }
    }

    public final void A03(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            List list = this.A08;
            C67613Se r4 = (C67613Se) list.remove(i3);
            this.A09.remove(r4.A03);
            int i4 = -r4.A02.A01.A01();
            for (int i5 = i3; i5 < list.size(); i5++) {
                ((C67613Se) list.get(i5)).A00 += i4;
            }
            r4.A01 = true;
            if (this.A02) {
                A04(r4);
            }
        }
    }

    public final void A04(C67613Se r4) {
        if (r4.A01 && r4.A04.isEmpty()) {
            C90694Ow r2 = (C90694Ow) this.A06.remove(r4);
            AnonymousClass2CD r1 = r2.A01;
            r1.AaA(r2.A00);
            r1.AaI(r2.A02);
            this.A0A.remove(r4);
        }
    }

    public final void A05(C67613Se r7) {
        C56032kD r4 = r7.A02;
        C67693Sm r3 = new AnonymousClass5SQ() { // from class: X.3Sm
            @Override // X.AnonymousClass5SQ
            public final void AWC(Timeline timeline, AnonymousClass2CD r42) {
                ((C107914yA) ((C107544xV) C64463Fq.this.A03).A0Z).A00.sendEmptyMessage(22);
            }
        };
        C67713So r5 = new C67713So(r7, this);
        this.A06.put(r7, new C90694Ow(r3, r4, r5));
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        ((AbstractC67703Sn) r4).A03.A02.add(new AnonymousClass4M2(new Handler(myLooper, null), r5));
        Looper myLooper2 = Looper.myLooper();
        if (myLooper2 == null) {
            myLooper2 = Looper.getMainLooper();
        }
        ((AbstractC67703Sn) r4).A02.A02.add(new C89874Ls(new Handler(myLooper2, null), r5));
        r4.AZV(r3, this.A01);
    }

    public void A06(AbstractC14080kp r5) {
        IdentityHashMap identityHashMap = this.A07;
        C67613Se r2 = (C67613Se) identityHashMap.remove(r5);
        r2.A02.Aa9(r5);
        r2.A04.remove(((C67683Sl) r5).A05);
        if (!identityHashMap.isEmpty()) {
            A02();
        }
        A04(r2);
    }
}
