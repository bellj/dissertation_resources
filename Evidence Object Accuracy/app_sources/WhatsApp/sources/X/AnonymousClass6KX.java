package X;

import android.hardware.camera2.CameraManager;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/* renamed from: X.6KX  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KX implements Callable {
    public final /* synthetic */ AnonymousClass662 A00;
    public final /* synthetic */ C128385w1 A01;

    public AnonymousClass6KX(AnonymousClass662 r1, C128385w1 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass66I r3;
        AnonymousClass662 r2 = this.A00;
        if (r2.A0C == null || r2.A06 == null || r2.A0h == null || r2.A0E == null) {
            throw C12960it.A0U("Cannot modify settings, camera was closed.");
        }
        C119085cr r0 = r2.A0C;
        C125475rJ r4 = AbstractC130685zo.A0J;
        boolean A1Y = C12970iu.A1Y(r0.A03(r4));
        C119085cr r02 = r2.A0C;
        C125475rJ r32 = AbstractC130685zo.A02;
        HashMap hashMap = new HashMap((Map) r02.A03(r32));
        if (Boolean.valueOf(r2.A0C.A05(this.A01)).booleanValue()) {
            AnonymousClass61Q r1 = r2.A0Y;
            if (r1.A0Q) {
                if (r2.A0A != null) {
                    boolean A1Y2 = C12970iu.A1Y(r2.A0C.A03(r4));
                    HashMap hashMap2 = new HashMap((Map) r2.A0C.A03(r32));
                    if (A1Y == A1Y2) {
                        if (A1Y && A1Y2 && !hashMap2.equals(hashMap)) {
                            r2.A0E(true);
                            AnonymousClass662.A08(r2, r2.A0h.getId());
                        }
                    }
                }
                r2.A0m = C12970iu.A1Y(r2.A0C.A03(AbstractC130685zo.A0R));
                if (C12970iu.A1Y(r2.A0C.A03(AbstractC130685zo.A0N)) && r2.A0j != null) {
                    r2.A0V.A0A(r2.A0j);
                }
                r1.A07();
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 0);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 1);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 2);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 3);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 4);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 5);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 6);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 7);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 8);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 9);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 10);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 11);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 12);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 13);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 14);
                C1308660g.A02(r2.A06, r2.A0C, r2.A0E, 15);
                CameraManager cameraManager = r2.A0N;
                C1308660g.A00(cameraManager, r2.A06, r2.A0C, r2.A0E, r2.A0h.getId(), 0);
                C1308660g.A00(cameraManager, r2.A06, r2.A0C, r2.A0E, r2.A0h.getId(), 1);
                if (C117295Zj.A1S(AbstractC130695zp.A0B, r2.A0E)) {
                    r2.A0C.A03(AbstractC130685zo.A0h);
                }
                C119085cr r42 = r1.A0C;
                if (!(r42 == null || (r3 = r1.A08) == null)) {
                    r3.A0F = C12970iu.A1Y(r42.A03(AbstractC130685zo.A0P));
                }
                r1.A05();
            }
        }
        return r2.A0C;
    }
}
