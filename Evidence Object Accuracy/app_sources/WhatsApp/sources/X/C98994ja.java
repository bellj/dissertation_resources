package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.4ja  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98994ja implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        double d = 0.0d;
        double d2 = 0.0d;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                C95664e9.A0F(parcel, readInt, 8);
                d = parcel.readDouble();
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                C95664e9.A0F(parcel, readInt, 8);
                d2 = parcel.readDouble();
            }
        }
        C95664e9.A0C(parcel, A01);
        return new LatLng(d, d2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LatLng[i];
    }
}
