package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import com.whatsapp.migration.export.ui.ExportMigrationActivity;

/* renamed from: X.11z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C235211z {
    public final ComponentName A00;
    public final PackageManager A01;

    public C235211z(Context context) {
        this.A01 = context.getPackageManager();
        this.A00 = new ComponentName(context, ExportMigrationActivity.class);
    }
}
