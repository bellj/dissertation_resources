package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2EK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EK {
    public int A00;
    public int A01 = 4;
    public AnonymousClass016 A02;
    public final C14900mE A03;
    public final C19850um A04;
    public final C90844Pl A05;
    public final UserJid A06;
    public final List A07;

    public AnonymousClass2EK(C14900mE r2, C19850um r3, C90844Pl r4, UserJid userJid, List list) {
        this.A03 = r2;
        this.A06 = userJid;
        this.A07 = list;
        this.A04 = r3;
        this.A05 = r4;
        this.A00 = 0;
    }

    public static void A00(AnonymousClass2EK r1, int i) {
        r1.A02(new C90834Pk(i));
    }

    public final List A01() {
        C44731zS r0;
        ArrayList arrayList = new ArrayList();
        for (String str : this.A07) {
            C44691zO A05 = this.A04.A05(null, str);
            if (A05 != null && (r0 = A05.A01) != null && r0.A00 == 0 && !A05.A07) {
                arrayList.add(A05);
            }
        }
        return arrayList;
    }

    public void A02(C90834Pk r5) {
        this.A01 = r5.A00;
        List<C44691zO> list = r5.A01;
        if (list != null) {
            for (C44691zO r2 : list) {
                this.A04.A0C(r2, this.A06);
            }
        }
        r5.A01 = A01();
        this.A03.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 12, r5));
    }
}
