package X;

import com.whatsapp.gallery.GalleryFragmentBase;
import java.lang.ref.WeakReference;

/* renamed from: X.38b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C626538b extends AbstractC16350or {
    public int A00;
    public AnonymousClass02N A01;
    public final AnonymousClass018 A02;
    public final C15650ng A03;
    public final C15250mo A04;
    public final AnonymousClass12I A05;
    public final AnonymousClass2WM A06;
    public final AbstractC14640lm A07;
    public final WeakReference A08;

    public C626538b(AnonymousClass018 r3, C15650ng r4, C15250mo r5, AnonymousClass12I r6, GalleryFragmentBase galleryFragmentBase, AbstractC14640lm r8) {
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A08 = C12970iu.A10(galleryFragmentBase);
        this.A07 = r8;
        this.A04 = r5;
        this.A06 = new AnonymousClass2WM(galleryFragmentBase.A01(), r3);
    }

    public void A08() {
        A03(true);
        synchronized (this) {
            AnonymousClass02N r0 = this.A01;
            if (r0 != null) {
                r0.A01();
            }
        }
    }
}
