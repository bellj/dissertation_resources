package X;

import java.util.List;

/* renamed from: X.0Pe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05340Pe {
    public final String A00;
    public final List A01;
    public final boolean A02;

    public C05340Pe(String str, List list, boolean z) {
        this.A00 = str;
        this.A02 = z;
        this.A01 = list;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C05340Pe r5 = (C05340Pe) obj;
            if (this.A02 == r5.A02 && this.A01.equals(r5.A01)) {
                String str = this.A00;
                boolean startsWith = str.startsWith("index_");
                String str2 = r5.A00;
                if (startsWith) {
                    return str2.startsWith("index_");
                }
                return str.equals(str2);
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode;
        String str = this.A00;
        if (str.startsWith("index_")) {
            hashCode = -1184239155;
        } else {
            hashCode = str.hashCode();
        }
        return (((hashCode * 31) + (this.A02 ? 1 : 0)) * 31) + this.A01.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Index{name='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append(", unique=");
        sb.append(this.A02);
        sb.append(", columns=");
        sb.append(this.A01);
        sb.append('}');
        return sb.toString();
    }
}
