package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.InteractiveMessageButton;
import com.whatsapp.conversation.conversationrow.InteractiveMessageView;

/* renamed from: X.2yB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60532yB extends C60782yd {
    public C50512Pv A00;
    public boolean A01;
    public final InteractiveMessageButton A02 = ((InteractiveMessageButton) AnonymousClass028.A0D(this, R.id.button));
    public final InteractiveMessageView A03;

    public C60532yB(Context context, AbstractC13890kV r3, AnonymousClass1X6 r4) {
        super(context, r3, r4);
        A0Z();
        InteractiveMessageView interactiveMessageView = (InteractiveMessageView) AnonymousClass028.A0D(this, R.id.interactive_view);
        this.A03 = interactiveMessageView;
        AnonymousClass1OY.A0R(this, interactiveMessageView, r4);
        A1P();
    }

    @Override // X.AbstractC60592yH, X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            AnonymousClass1OY.A0O(A08, this);
            this.A00 = A07.A03();
        }
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public void A0s() {
        super.A0s();
        A1P();
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, (AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1P();
        }
    }

    public final void A1P() {
        AbstractC16130oV r2 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
        this.A03.A00(this, r2);
        this.A02.A00(this, ((AbstractC28551Oa) this).A0a, r2);
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_image_interactive_left;
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_image_interactive_left;
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_image_interactive_right;
    }
}
