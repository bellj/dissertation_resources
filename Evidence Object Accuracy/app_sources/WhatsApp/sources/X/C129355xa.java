package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;
import java.util.ArrayList;

/* renamed from: X.5xa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129355xa {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final AnonymousClass102 A05;
    public final C17220qS A06;
    public final AnonymousClass60Z A07;
    public final C18650sn A08;
    public final C18610sj A09;
    public final C17070qD A0A;
    public final C129095xA A0B;
    public final C18590sh A0C;
    public final String A0D;

    public C129355xa(Context context, C14900mE r2, C15570nT r3, C18640sm r4, C14830m7 r5, AnonymousClass102 r6, C17220qS r7, AnonymousClass60Z r8, C18650sn r9, C18610sj r10, C17070qD r11, C129095xA r12, C18590sh r13, String str) {
        this.A04 = r5;
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A0C = r13;
        this.A0A = r11;
        this.A09 = r10;
        this.A0B = r12;
        this.A05 = r6;
        this.A07 = r8;
        this.A03 = r4;
        this.A08 = r9;
        this.A0D = str;
    }

    public void A00(AnonymousClass6MO r17) {
        C17070qD r13 = this.A0A;
        r13.A03();
        C241414j r1 = r13.A09;
        String str = this.A0D;
        AbstractC28901Pl A08 = r1.A08(str);
        AnonymousClass009.A05(A08);
        C30881Ze r12 = (C30881Ze) A08;
        C119775f5 r4 = (C119775f5) r12.A08;
        if (r4 != null) {
            if (r4.A07) {
                C14830m7 r7 = this.A04;
                Context context = this.A00;
                C14900mE r42 = this.A01;
                C15570nT r5 = this.A02;
                C17220qS r9 = this.A06;
                C18610sj r122 = this.A09;
                C129095xA r15 = this.A0B;
                new C129725yC(context, r42, r5, this.A03, r7, this.A05, r9, this.A07, this.A08, r122, r13, new AnonymousClass6Lp(r17, this) { // from class: X.6AD
                    public final /* synthetic */ AnonymousClass6MO A00;
                    public final /* synthetic */ C129355xa A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass6Lp
                    public final void AP5(C30881Ze r43, C452120p r52, ArrayList arrayList) {
                        C129355xa r0 = this.A01;
                        AnonymousClass6MO r2 = this.A00;
                        if (r52 == null) {
                            r0.A0A.A00().A03(
                            /*  JADX ERROR: Method code generation error
                                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0011: INVOKE  
                                  (wrap: X.1nR : 0x0008: INVOKE  (r1v0 X.1nR A[REMOVE]) = (wrap: X.0qD : 0x0006: IGET  (r0v2 X.0qD A[REMOVE]) = (r0v0 'r0' X.5xa) X.5xa.A0A X.0qD) type: VIRTUAL call: X.0qD.A00():X.1nR)
                                  (wrap: X.67j : 0x000e: CONSTRUCTOR  (r0v3 X.67j A[REMOVE]) = (r2v0 'r2' X.6MO), (r6v0 'arrayList' java.util.ArrayList) call: X.67j.<init>(X.6MO, java.util.ArrayList):void type: CONSTRUCTOR)
                                  (r4v0 'r43' X.1Ze)
                                 type: VIRTUAL call: X.1nR.A03(X.20l, X.1Pl):void in method: X.6AD.AP5(X.1Ze, X.20p, java.util.ArrayList):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000e: CONSTRUCTOR  (r0v3 X.67j A[REMOVE]) = (r2v0 'r2' X.6MO), (r6v0 'arrayList' java.util.ArrayList) call: X.67j.<init>(X.6MO, java.util.ArrayList):void type: CONSTRUCTOR in method: X.6AD.AP5(X.1Ze, X.20p, java.util.ArrayList):void, file: classes4.dex
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                                	... 21 more
                                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.67j, state: NOT_LOADED
                                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                                	... 27 more
                                */
                            /*
                                this = this;
                                X.5xa r0 = r3.A01
                                X.6MO r2 = r3.A00
                                if (r5 != 0) goto L_0x0015
                                X.0qD r0 = r0.A0A
                                X.1nR r1 = r0.A00()
                                X.67j r0 = new X.67j
                                r0.<init>(r2, r6)
                                r1.A03(r0, r4)
                                return
                            L_0x0015:
                                r0 = 0
                                r2.AVP(r5, r0)
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6AD.AP5(X.1Ze, X.20p, java.util.ArrayList):void");
                        }
                    }, r15).A00(r12);
                    return;
                } else if (r4.A0A()) {
                    r17.ANl(r12);
                    return;
                }
            }
            C17220qS r6 = this.A06;
            String A01 = r6.A01();
            C117325Zm.A06(r6, new IDxRCallbackShape0S0200000_3_I1(this.A00, this.A01, this.A08, r17, this, 4), new C126285sd(new AnonymousClass3CS(A01), str, this.A0C.A01()).A00, A01);
        }
    }
