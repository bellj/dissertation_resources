package X;

import android.graphics.Bitmap;
import android.os.Handler;
import java.util.Map;

/* renamed from: X.1O4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1O4 {
    public final C006202y A00;

    public AnonymousClass1O4(int i) {
        this.A00 = new C41621tp(this, i);
    }

    public Object A00(Object obj) {
        C41631tq r2 = (C41631tq) this.A00.A04(obj);
        if (r2 == null) {
            return null;
        }
        A01();
        r2.A00 = System.currentTimeMillis();
        return r2.A01;
    }

    public void A01() {
        C18720su r0;
        if (this instanceof AnonymousClass1O7) {
            r0 = ((AnonymousClass1O7) this).A00;
        } else if (this instanceof C41641tr) {
            r0 = ((C41641tr) this).A00;
        } else if (this instanceof AnonymousClass1O5) {
            r0 = ((AnonymousClass1O5) this).A00;
        } else if (this instanceof AnonymousClass1O8) {
            r0 = ((AnonymousClass1O8) this).A00;
        } else {
            return;
        }
        Handler A00 = C18720su.A00(r0);
        if (!A00.hasMessages(0)) {
            A00.sendEmptyMessageDelayed(0, 60000);
        }
    }

    public void A02(int i) {
        long currentTimeMillis = System.currentTimeMillis() - ((long) i);
        C006202y r4 = this.A00;
        for (Map.Entry entry : r4.A05().entrySet()) {
            synchronized (r4) {
                if (((C41631tq) entry.getValue()).A00 < currentTimeMillis) {
                    r4.A07(entry.getKey());
                    entry.getKey();
                    Object obj = ((C41631tq) entry.getValue()).A01;
                    if ((this instanceof AnonymousClass1O7) || (this instanceof C41641tr) || (this instanceof AnonymousClass1O5) || (this instanceof AnonymousClass1O8)) {
                        ((Bitmap) obj).getByteCount();
                    }
                } else {
                    return;
                }
            }
        }
    }

    public void A03(Object obj, Object obj2) {
        this.A00.A08(obj, new C41631tq(obj2, System.currentTimeMillis()));
        A01();
    }
}
