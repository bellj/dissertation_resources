package X;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.facebook.redex.ViewOnClickCListenerShape5S0100000_I0_5;
import com.whatsapp.R;

/* renamed from: X.19x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255719x {
    public final C14900mE A00;
    public final C15450nH A01;
    public final C14820m6 A02;
    public final C242114q A03;
    public final C22100yW A04;
    public final AnonymousClass19M A05;
    public final C14850m9 A06;
    public final C14840m8 A07;
    public final AbstractC14440lR A08;

    public C255719x(C14900mE r1, C15450nH r2, C14820m6 r3, C242114q r4, C22100yW r5, AnonymousClass19M r6, C14850m9 r7, C14840m8 r8, AbstractC14440lR r9) {
        this.A06 = r7;
        this.A00 = r1;
        this.A08 = r9;
        this.A05 = r6;
        this.A01 = r2;
        this.A07 = r8;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
    }

    public static final String A00(Context context, int i) {
        if (i == -1) {
            return context.getString(R.string.delete_all_media_phone_gallery);
        }
        return context.getResources().getQuantityString(R.plurals.delete_medias_phone_gallery, i, Integer.valueOf(i));
    }

    public C004802e A01(Context context, AnonymousClass5WL r12, String str, int i) {
        if (A08()) {
            return A04(context, new C1114959p(r12), 1, i, 0, false);
        }
        return A03(context, r12, str, R.string.delete, 1, false);
    }

    public C004802e A02(Context context, AnonymousClass5WL r12, String str, int i) {
        if (A08()) {
            return A04(context, new C1114959p(r12), 1, i, 2, false);
        }
        return A03(context, r12, str, R.string.delete, 1, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0033, code lost:
        if (r10 != false) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C004802e A03(android.content.Context r12, X.AnonymousClass5WL r13, java.lang.String r14, int r15, int r16, boolean r17) {
        /*
            r11 = this;
            r4 = 2131886925(0x7f12034d, float:1.9408443E38)
            r8 = r11
            X.0m6 r0 = r11.A02
            android.content.SharedPreferences r2 = r0.A00
            java.lang.String r1 = "pref_media_delete_per_conversation"
            r0 = 0
            boolean r10 = r2.getBoolean(r1, r0)
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r12)
            r1 = 2131558874(0x7f0d01da, float:1.8743076E38)
            r0 = 0
            android.view.View r3 = r2.inflate(r1, r0)
            r0 = 2131363033(0x7f0a04d9, float:1.8345863E38)
            android.view.View r6 = r3.findViewById(r0)
            android.widget.CheckBox r6 = (android.widget.CheckBox) r6
            r0 = r16
            java.lang.String r0 = A00(r12, r0)
            r6.setText(r0)
            r5 = 1
            r9 = r17
            if (r17 == 0) goto L_0x0035
            r0 = 0
            if (r10 == 0) goto L_0x0036
        L_0x0035:
            r0 = 1
        L_0x0036:
            r6.setChecked(r0)
            X.02e r2 = new X.02e
            r2.<init>(r12)
            android.content.Context r1 = r12.getApplicationContext()
            X.19M r0 = r11.A05
            java.lang.CharSequence r0 = X.AbstractC36671kL.A05(r1, r0, r14)
            r2.A0A(r0)
            r2.A0B(r5)
            r7 = r13
            X.4gH r0 = new X.4gH
            r0.<init>()
            r2.setNegativeButton(r4, r0)
            X.3Kw r5 = new X.3Kw
            r5.<init>(r6, r7, r8, r9, r10)
            r2.setPositiveButton(r15, r5)
            r2.setView(r3)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C255719x.A03(android.content.Context, X.5WL, java.lang.String, int, int, boolean):X.02e");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e3, code lost:
        if (r19 > 1) goto L_0x00e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011f, code lost:
        if (r19 > 1) goto L_0x0121;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x011a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C004802e A04(android.content.Context r17, X.AnonymousClass5WM r18, int r19, int r20, int r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 404
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C255719x.A04(android.content.Context, X.5WM, int, int, int, boolean):X.02e");
    }

    public final C004802e A05(Context context, AnonymousClass5WM r9, String str, int i, boolean z) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.dialog_clear_messages, (ViewGroup) null);
        inflate.findViewById(R.id.delete_media_container);
        CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.delete_media_checkbox);
        CheckBox checkBox2 = (CheckBox) inflate.findViewById(R.id.delete_starred_checkbox);
        checkBox.setChecked(z);
        ((TextView) inflate.findViewById(R.id.delete_media_checkbox_text)).setText(A00(context, i));
        inflate.findViewById(R.id.delete_media_container).setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(checkBox, 15));
        inflate.findViewById(R.id.delete_starred_container).setOnClickListener(new ViewOnClickCListenerShape5S0100000_I0_5(checkBox2, 18));
        C004802e r2 = new C004802e(context);
        r2.setView(inflate);
        r2.setPositiveButton(R.string.clear_all_chats_dialog_positive_button, new DialogInterface.OnClickListener(checkBox2, checkBox, r9) { // from class: X.4h0
            public final /* synthetic */ CheckBox A00;
            public final /* synthetic */ CheckBox A01;
            public final /* synthetic */ AnonymousClass5WM A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                this.A02.ATz(!this.A00.isChecked(), this.A01.isChecked());
            }
        });
        r2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() { // from class: X.4gI
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                AnonymousClass5WM.this.ASv();
            }
        });
        if (!TextUtils.isEmpty(null)) {
            r2.setTitle(null);
        } else {
            r2.A07(R.string.clear_all_chats_dialog_prompt);
        }
        if (!TextUtils.isEmpty(str)) {
            ((TextView) inflate.findViewById(R.id.clear_messages_dialog_message)).setText(str);
        }
        return r2;
    }

    public C14580lf A06(ActivityC000900k r6) {
        C14580lf r4 = new C14580lf();
        if (this.A02.A00.getBoolean("delete_chat_clear_chat_nux_accepted", false) || !this.A07.A03()) {
            r4.A02(Boolean.FALSE);
            return r4;
        }
        this.A08.Aaz(new AnonymousClass37P(r6, r4, this), new Void[0]);
        return r4;
    }

    public void A07(AbstractC14640lm r5, AnonymousClass1Kp r6) {
        AnonymousClass390 r2 = new AnonymousClass390(this.A03, r5, r6);
        this.A08.Aaz(r2, new Void[0]);
        this.A00.A0J(new RunnableBRunnable0Shape8S0200000_I0_8(r2, 30, r6), 500);
    }

    public final boolean A08() {
        return this.A06.A07(498) || this.A07.A03();
    }
}
