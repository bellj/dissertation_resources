package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewPropertyAnimator;
import android.view.animation.Interpolator;
import com.facebook.redex.IDxLAdapterShape1S0100000_2_I1;
import com.whatsapp.settings.chat.wallpaper.WallpaperPreview;

/* renamed from: X.2YT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YT extends AnimatorListenerAdapter {
    public final /* synthetic */ Interpolator A00;
    public final /* synthetic */ WallpaperPreview A01;

    public AnonymousClass2YT(Interpolator interpolator, WallpaperPreview wallpaperPreview) {
        this.A01 = wallpaperPreview;
        this.A00 = interpolator;
    }

    public static ViewPropertyAnimator A00(AnonymousClass2YT r6) {
        WallpaperPreview wallpaperPreview = r6.A01;
        ViewPropertyAnimator alpha = wallpaperPreview.A05.animate().setDuration(250).alpha(0.0f);
        Interpolator interpolator = r6.A00;
        alpha.setInterpolator(interpolator);
        return wallpaperPreview.A09.animate().setDuration(250).alpha(0.0f).scaleX(wallpaperPreview.A00).scaleY(wallpaperPreview.A01).translationX((float) wallpaperPreview.A02).translationY((float) wallpaperPreview.A03).setInterpolator(interpolator);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        super.onAnimationCancel(animator);
        A00(this).setListener(new IDxLAdapterShape1S0100000_2_I1(this, 16));
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        A00(this).setListener(new IDxLAdapterShape1S0100000_2_I1(this, 15));
    }
}
