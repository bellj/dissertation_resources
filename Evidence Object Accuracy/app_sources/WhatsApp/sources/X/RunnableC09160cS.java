package X;

import androidx.biometric.BiometricFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.0cS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09160cS implements Runnable {
    public final WeakReference A00;

    public RunnableC09160cS(BiometricFragment biometricFragment) {
        this.A00 = new WeakReference(biometricFragment);
    }

    @Override // java.lang.Runnable
    public void run() {
        WeakReference weakReference = this.A00;
        if (weakReference.get() != null) {
            ((BiometricFragment) weakReference.get()).A1A();
        }
    }
}
