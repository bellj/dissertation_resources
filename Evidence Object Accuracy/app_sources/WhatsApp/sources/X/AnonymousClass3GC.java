package X;

import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3GC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GC {
    public static int A00(String str) {
        if (!(str == null || str.length() == 0)) {
            try {
                return Integer.parseInt(str);
            } catch (NumberFormatException e) {
                Log.d("Whatsapp", C12960it.A0d(str, C12960it.A0k("NumberUtil/Invalid int value:")), e);
            }
        }
        return -1;
    }

    public static Map A01(Map map) {
        HashMap A11 = C12970iu.A11();
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            C12990iw.A1Q(A15.getKey(), A11, A15);
        }
        return A11;
    }
}
