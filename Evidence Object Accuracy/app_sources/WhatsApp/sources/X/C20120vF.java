package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.whatsapp.InteractiveAnnotation;
import com.whatsapp.MediaData;
import com.whatsapp.SerializablePoint;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;

/* renamed from: X.0vF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20120vF {
    public final C15810nw A00;
    public final C16510p9 A01;
    public final C16490p7 A02;
    public final C21390xL A03;

    public C20120vF(C15810nw r1, C16510p9 r2, C16490p7 r3, C21390xL r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static C16150oX A00(String str, byte[] bArr) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bArr));
            try {
                Object readObject = objectInputStream.readObject();
                if (!(readObject instanceof MediaData)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected type of media data (");
                    sb.append(readObject);
                    sb.append(" )");
                    Log.e(sb.toString());
                    objectInputStream.close();
                    return null;
                }
                C16150oX A00 = C16150oX.A00((MediaData) readObject);
                objectInputStream.close();
                return A00;
            } catch (Throwable th) {
                try {
                    objectInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException | ClassNotFoundException e) {
            StringBuilder sb2 = new StringBuilder("failure fetching media data by hash; hash=");
            sb2.append(str);
            Log.w(sb2.toString(), e);
            return null;
        }
    }

    public static final void A01(ContentValues contentValues, Integer num, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, int i, long j, long j2, long j3, boolean z) {
        contentValues.put("message_row_id", Long.valueOf(j));
        contentValues.put("chat_row_id", Long.valueOf(j2));
        C30021Vq.A04(contentValues, "multicast_id", str);
        C30021Vq.A04(contentValues, "message_url", str2);
        C30021Vq.A04(contentValues, "mime_type", str3);
        contentValues.put("file_length", Long.valueOf(j3));
        C30021Vq.A04(contentValues, "media_name", str4);
        C30021Vq.A04(contentValues, "file_hash", str5);
        if (num != null) {
            contentValues.put("page_count", num);
            contentValues.put("media_duration", (Integer) 0);
        } else {
            contentValues.put("page_count", (Integer) 0);
            contentValues.put("media_duration", Integer.valueOf(i));
        }
        C30021Vq.A04(contentValues, "media_caption", str8);
        C30021Vq.A04(contentValues, "enc_file_hash", str6);
        C30021Vq.A05(contentValues, "is_animated_sticker", z);
        C30021Vq.A04(contentValues, "original_file_hash", str7);
    }

    public C16150oX A02(Cursor cursor) {
        File file;
        C16150oX r4 = new C16150oX();
        boolean z = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("autotransfer_retry_enabled")) == 1) {
            z = true;
        }
        r4.A0L = z;
        r4.A0I = cursor.getString(cursor.getColumnIndexOrThrow("media_job_uuid"));
        boolean z2 = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("transferred")) == 1) {
            z2 = true;
        }
        r4.A0P = z2;
        boolean z3 = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("transcoded")) == 1) {
            z3 = true;
        }
        r4.A0O = z3;
        r4.A0A = cursor.getLong(cursor.getColumnIndexOrThrow("file_size"));
        r4.A07 = cursor.getInt(cursor.getColumnIndexOrThrow("suspicious_content"));
        r4.A0D = cursor.getLong(cursor.getColumnIndexOrThrow("trim_from"));
        r4.A0E = cursor.getLong(cursor.getColumnIndexOrThrow("trim_to"));
        r4.A02 = cursor.getInt(cursor.getColumnIndexOrThrow("face_x"));
        r4.A03 = cursor.getInt(cursor.getColumnIndexOrThrow("face_y"));
        r4.A0U = cursor.getBlob(cursor.getColumnIndexOrThrow("media_key"));
        r4.A0B = cursor.getLong(cursor.getColumnIndexOrThrow("media_key_timestamp"));
        r4.A08 = cursor.getInt(cursor.getColumnIndexOrThrow("width"));
        r4.A06 = cursor.getInt(cursor.getColumnIndexOrThrow("height"));
        boolean z4 = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("has_streaming_sidecar")) == 1) {
            z4 = true;
        }
        r4.A0M = z4;
        r4.A05 = cursor.getInt(cursor.getColumnIndexOrThrow("gif_attribution"));
        r4.A00 = cursor.getFloat(cursor.getColumnIndexOrThrow("thumbnail_height_width_ratio"));
        r4.A0G = cursor.getString(cursor.getColumnIndexOrThrow("direct_path"));
        r4.A0R = cursor.getBlob(cursor.getColumnIndexOrThrow("first_scan_sidecar"));
        r4.A04 = cursor.getInt(cursor.getColumnIndexOrThrow("first_scan_length"));
        String string = cursor.getString(cursor.getColumnIndexOrThrow("file_path"));
        r4.A0K = cursor.getString(cursor.getColumnIndexOrThrow("partial_media_hash"));
        r4.A0J = cursor.getString(cursor.getColumnIndexOrThrow("partial_media_enc_hash"));
        boolean z5 = false;
        if (cursor.getLong(cursor.getColumnIndexOrThrow("mute_video")) == 1) {
            z5 = true;
        }
        r4.A0N = z5;
        if (string == null) {
            file = null;
        } else {
            file = new File(string);
        }
        r4.A0F = this.A00.A06(file);
        return r4;
    }

    public C16150oX A03(byte[] bArr) {
        C16150oX A00;
        File file;
        if (bArr != null) {
            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
                try {
                    ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                    Object readObject = objectInputStream.readObject();
                    objectInputStream.close();
                    byteArrayInputStream.close();
                    if (readObject instanceof C16150oX) {
                        AnonymousClass009.A05(readObject);
                        A00 = (C16150oX) readObject;
                    } else if (readObject instanceof MediaData) {
                        AnonymousClass009.A05(readObject);
                        A00 = C16150oX.A00((MediaData) readObject);
                    }
                    File file2 = A00.A0F;
                    if (file2 != null) {
                        String path = file2.getPath();
                        if (path == null) {
                            file = null;
                        } else {
                            file = new File(path);
                        }
                        A00.A0F = this.A00.A06(file);
                    }
                    return A00;
                } catch (Throwable th) {
                    try {
                        byteArrayInputStream.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (IOException | ClassCastException | ClassNotFoundException | IllegalArgumentException | IndexOutOfBoundsException | NegativeArraySizeException | NullPointerException e) {
                Log.e("CachedMessageStore/getMessageMediaData", e);
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0019, code lost:
        if (X.C15380n4.A0J(r14) != false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C28011Kc A04(X.AbstractC14640lm r14) {
        /*
        // Method dump skipped, instructions count: 468
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20120vF.A04(X.0lm):X.1Kc");
    }

    public void A05(ContentValues contentValues, C16150oX r5) {
        AnonymousClass009.A05(r5);
        C30021Vq.A05(contentValues, "autotransfer_retry_enabled", r5.A0L);
        C30021Vq.A04(contentValues, "media_job_uuid", r5.A0I);
        C30021Vq.A05(contentValues, "transferred", r5.A0P);
        C30021Vq.A05(contentValues, "transcoded", r5.A0O);
        contentValues.put("file_size", Long.valueOf(r5.A0A));
        contentValues.put("suspicious_content", Integer.valueOf(r5.A07));
        contentValues.put("trim_from", Long.valueOf(r5.A0D));
        contentValues.put("trim_to", Long.valueOf(r5.A0E));
        contentValues.put("face_x", Integer.valueOf(r5.A02));
        contentValues.put("face_y", Integer.valueOf(r5.A03));
        C30021Vq.A06(contentValues, "media_key", r5.A0U);
        contentValues.put("media_key_timestamp", Long.valueOf(r5.A0B));
        contentValues.put("width", Integer.valueOf(r5.A08));
        contentValues.put("height", Integer.valueOf(r5.A06));
        C30021Vq.A05(contentValues, "has_streaming_sidecar", r5.A0M);
        contentValues.put("gif_attribution", Integer.valueOf(r5.A05));
        contentValues.put("thumbnail_height_width_ratio", Float.valueOf(r5.A00));
        C30021Vq.A04(contentValues, "direct_path", r5.A0G);
        C30021Vq.A06(contentValues, "first_scan_sidecar", r5.A0R);
        contentValues.put("first_scan_length", Integer.valueOf(r5.A04));
        File file = r5.A0F;
        if (file != null) {
            contentValues.put("file_path", this.A00.A08(file));
        } else {
            contentValues.putNull("file_path");
        }
        C30021Vq.A04(contentValues, "partial_media_hash", r5.A0K);
        C30021Vq.A04(contentValues, "partial_media_enc_hash", r5.A0J);
        C30021Vq.A05(contentValues, "mute_video", r5.A0N);
    }

    public final void A06(C16150oX r20, long j) {
        if (r20 != null && r20.A0V != null) {
            C16310on A02 = this.A02.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                InteractiveAnnotation[] interactiveAnnotationArr = r20.A0V;
                int i = 0;
                for (InteractiveAnnotation interactiveAnnotation : interactiveAnnotationArr) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("message_row_id", Long.valueOf(j));
                    contentValues.put("location_latitude", Double.valueOf(interactiveAnnotation.serializableLocation.latitude));
                    contentValues.put("location_longitude", Double.valueOf(interactiveAnnotation.serializableLocation.longitude));
                    contentValues.put("location_name", interactiveAnnotation.serializableLocation.name);
                    contentValues.put("sort_order", Integer.valueOf(i));
                    C16330op r6 = A02.A03;
                    long A022 = r6.A02(contentValues, "message_media_interactive_annotation");
                    i++;
                    SerializablePoint[] serializablePointArr = interactiveAnnotation.polygonVertices;
                    if (serializablePointArr != null) {
                        int i2 = 0;
                        for (SerializablePoint serializablePoint : serializablePointArr) {
                            ContentValues contentValues2 = new ContentValues();
                            contentValues2.put("message_media_interactive_annotation_row_id", Long.valueOf(A022));
                            contentValues2.put("x", Double.valueOf(serializablePoint.x));
                            contentValues2.put("y", Double.valueOf(serializablePoint.y));
                            contentValues2.put("sort_order", Integer.valueOf(i2));
                            r6.A02(contentValues2, "message_media_interactive_annotation_vertex");
                            i2++;
                        }
                    }
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ef A[Catch: all -> 0x0138, TryCatch #3 {all -> 0x013d, blocks: (B:21:0x006f, B:46:0x0134, B:22:0x0073, B:24:0x007c, B:25:0x007f, B:27:0x00aa, B:28:0x00b7, B:30:0x00bd, B:33:0x00c7, B:35:0x00d0, B:37:0x00ef, B:40:0x00f7, B:42:0x010e, B:43:0x0124, B:44:0x0125, B:45:0x0131), top: B:55:0x006f }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f7 A[Catch: all -> 0x0138, TryCatch #3 {all -> 0x013d, blocks: (B:21:0x006f, B:46:0x0134, B:22:0x0073, B:24:0x007c, B:25:0x007f, B:27:0x00aa, B:28:0x00b7, B:30:0x00bd, B:33:0x00c7, B:35:0x00d0, B:37:0x00ef, B:40:0x00f7, B:42:0x010e, B:43:0x0124, B:44:0x0125, B:45:0x0131), top: B:55:0x006f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.AbstractC15340mz r37) {
        /*
        // Method dump skipped, instructions count: 326
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20120vF.A07(X.0mz):void");
    }

    public void A08(AbstractC16130oV r28) {
        File file;
        if (!A0A()) {
            Log.w("MediaCoreMessageStore/fillMediaInfo/mediaCoreMessageStore not ready");
            C16150oX r2 = r28.A02;
            if (r2 != null && (file = r2.A0F) != null) {
                r2.A0F = this.A00.A06(file);
                return;
            }
            return;
        }
        boolean z = false;
        if (r28.A11 > 0) {
            z = true;
        }
        StringBuilder sb = new StringBuilder("MediaCoreMessageStore/fillMediaInfo/message must have row_id set; key=");
        sb.append(r28.A0z);
        AnonymousClass009.A0B(sb.toString(), z);
        String[] strArr = {Long.toString(r28.A11)};
        C16490p7 r5 = this.A02;
        C16310on A01 = r5.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT message_row_id, chat_row_id, autotransfer_retry_enabled, multicast_id, media_job_uuid, transferred, transcoded, file_path, file_size, suspicious_content, trim_from, trim_to, face_x, face_y, media_key, media_key_timestamp, width, height, has_streaming_sidecar, gif_attribution, thumbnail_height_width_ratio, direct_path, first_scan_sidecar, first_scan_length, message_url, mime_type, file_length, media_name, file_hash, media_duration, page_count, enc_file_hash, partial_media_hash, partial_media_enc_hash, is_animated_sticker, original_file_hash, mute_video, media_caption FROM message_media WHERE message_row_id = ?", strArr);
            if (A09.moveToNext()) {
                C16150oX A02 = A02(A09);
                long j = r28.A11;
                boolean z2 = false;
                if (j > 0) {
                    z2 = true;
                }
                AnonymousClass009.A0B("MediaCoreMessageStore/loadInteractiveAnnotations/invalid row_id", z2);
                A01 = r5.get();
                try {
                    C16330op r7 = A01.A03;
                    Cursor A092 = r7.A09("SELECT _id, message_row_id, location_latitude, location_longitude, location_name, sort_order FROM message_media_interactive_annotation WHERE message_row_id = ? ORDER BY sort_order", new String[]{Long.toString(j)});
                    int count = A092.getCount();
                    InteractiveAnnotation[] interactiveAnnotationArr = new InteractiveAnnotation[count];
                    int i = 0;
                    while (A092.moveToNext()) {
                        Cursor A093 = r7.A09("SELECT message_media_interactive_annotation_row_id, x, y, sort_order FROM message_media_interactive_annotation_vertex WHERE message_media_interactive_annotation_row_id = ? ORDER BY sort_order", new String[]{Long.toString(A092.getLong(A092.getColumnIndexOrThrow("_id")))});
                        try {
                            SerializablePoint[] serializablePointArr = new SerializablePoint[A093.getCount()];
                            int i2 = 0;
                            while (A093.moveToNext()) {
                                serializablePointArr[i2] = new SerializablePoint(A093.getDouble(A093.getColumnIndexOrThrow("x")), A093.getDouble(A093.getColumnIndexOrThrow("y")));
                                i2++;
                            }
                            A093.close();
                            interactiveAnnotationArr[i] = new InteractiveAnnotation(A092.getString(A092.getColumnIndexOrThrow("location_name")), serializablePointArr, A092.getDouble(A092.getColumnIndexOrThrow("location_latitude")), A092.getDouble(A092.getColumnIndexOrThrow("location_longitude")));
                            i++;
                        } catch (Throwable th) {
                            if (A093 != null) {
                                try {
                                    A093.close();
                                } catch (Throwable unused) {
                                }
                            }
                            throw th;
                        }
                    }
                    A092.close();
                    A01.close();
                    if (count == 0) {
                        interactiveAnnotationArr = null;
                    }
                    A02.A0V = interactiveAnnotationArr;
                    r28.A17(A09, A02);
                } finally {
                }
            }
            A09.close();
            A01.close();
            if (r28.A02 == null) {
                StringBuilder sb2 = new StringBuilder("MediaCoreMessageStore/fillMediaInfo; media was not found for message: id=");
                sb2.append(r28.A11);
                sb2.append(", type=");
                sb2.append((int) r28.A0y);
                Log.e(sb2.toString());
                r28.A02 = new C16150oX();
            }
        } finally {
        }
    }

    public void A09(AbstractC16130oV r22, long j) {
        int i;
        Integer num;
        String str;
        boolean z = false;
        boolean z2 = false;
        if (r22.A08() == 2) {
            z2 = true;
        }
        StringBuilder sb = new StringBuilder("MediaCoreMessageStore/insertOrUpdateQuotedMediaMessage/message in main storage; key=");
        AnonymousClass1IS r5 = r22.A0z;
        sb.append(r5);
        AnonymousClass009.A0B(sb.toString(), z2);
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            ContentValues contentValues = new ContentValues();
            byte[] bArr = null;
            if (r22 instanceof C16440p1) {
                C16440p1 r1 = (C16440p1) r22;
                num = Integer.valueOf(r1.A00);
                str = r1.A01;
                i = 0;
            } else {
                i = r22.A00;
                num = null;
                str = null;
            }
            C16460p3 A0G = r22.A0G();
            if (A0G != null) {
                bArr = A0G.A07();
            }
            String str2 = r22.A08;
            String str3 = r22.A06;
            long j2 = r22.A01;
            String A16 = r22.A16();
            String str4 = r22.A05;
            String str5 = r22.A04;
            contentValues.put("message_row_id", Long.valueOf(j));
            C30021Vq.A04(contentValues, "message_url", str2);
            C30021Vq.A04(contentValues, "mime_type", str3);
            contentValues.put("file_length", Long.valueOf(j2));
            C30021Vq.A04(contentValues, "media_name", A16);
            C30021Vq.A04(contentValues, "file_hash", str4);
            int i2 = 0;
            if (num != null) {
                contentValues.put("page_count", num);
            } else {
                contentValues.put("page_count", (Integer) 0);
                i2 = Integer.valueOf(i);
            }
            contentValues.put("media_duration", i2);
            C30021Vq.A04(contentValues, "enc_file_hash", str5);
            C30021Vq.A06(contentValues, "thumbnail", bArr);
            C30021Vq.A04(contentValues, "media_caption", str);
            C16150oX r3 = r22.A02;
            if (r3 != null) {
                AnonymousClass009.A05(r3);
                C30021Vq.A04(contentValues, "media_job_uuid", r3.A0I);
                C30021Vq.A05(contentValues, "transferred", r3.A0P);
                contentValues.put("file_size", Long.valueOf(r3.A0A));
                C30021Vq.A06(contentValues, "media_key", r3.A0U);
                contentValues.put("media_key_timestamp", Long.valueOf(r3.A0B));
                contentValues.put("width", Integer.valueOf(r3.A08));
                contentValues.put("height", Integer.valueOf(r3.A06));
                C30021Vq.A04(contentValues, "direct_path", r3.A0G);
                File file = r3.A0F;
                if (file != null) {
                    contentValues.put("file_path", this.A00.A08(file));
                } else {
                    contentValues.putNull("file_path");
                }
            }
            C16330op r10 = A02.A03;
            long A022 = r10.A02(contentValues, "message_quoted_media");
            if (A022 >= 0) {
                if (j == A022) {
                    z = true;
                }
                AnonymousClass009.A0C("MediaCoreMessageStore/insertOrUpdateQuotedMediaMessage/inserted row should have same row_id", z);
            } else {
                contentValues.remove("message_row_id");
                if (r10.A00("message_quoted_media", contentValues, "message_row_id = ?", new String[]{String.valueOf(j)}) != 1) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("MediaCoreMessageStore/insertOrUpdateQuotedMediaMessage/Failed to insert / update quoted media data; key=");
                    sb2.append(r5);
                    throw new SQLiteException(sb2.toString());
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A0A() {
        C16490p7 r0 = this.A02;
        C16310on A01 = r0.get();
        try {
            r0.A04();
            boolean z = true;
            if (!r0.A05.A06(A01.A03).booleanValue()) {
                if (this.A01.A0G()) {
                    if (this.A03.A00("media_message_ready", 0) == 2) {
                    }
                }
                z = false;
            }
            A01.close();
            return z;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
