package X;

import android.content.Context;
import android.view.ActionProvider;

/* renamed from: X.0CL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CL extends AnonymousClass0DX implements ActionProvider.VisibilityListener {
    public AbstractC11260g0 A00;
    public final /* synthetic */ AnonymousClass0CJ A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CL(Context context, ActionProvider actionProvider, AnonymousClass0CJ r3) {
        super(context, actionProvider, r3);
        this.A01 = r3;
    }

    @Override // android.view.ActionProvider.VisibilityListener
    public void onActionProviderVisibilityChanged(boolean z) {
        AbstractC11260g0 r0 = this.A00;
        if (r0 != null) {
            AnonymousClass07H r1 = ((C07430Xy) r0).A00.A0E;
            r1.A0F = true;
            r1.A0E(true);
        }
    }
}
