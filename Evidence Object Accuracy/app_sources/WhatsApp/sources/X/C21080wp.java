package X;

/* renamed from: X.0wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21080wp {
    public final C21070wo A00;
    public final C21060wn A01;
    public final C21050wm A02;
    public final C14830m7 A03;
    public final C16120oU A04;
    public final AnonymousClass01H A05;
    public final AnonymousClass01H A06;
    public final AnonymousClass01H A07;
    public final AnonymousClass01H A08;
    public final AnonymousClass01H A09;
    public final AnonymousClass01H A0A;
    public final AnonymousClass01H A0B;
    public final AnonymousClass01H A0C;
    public final AnonymousClass01H A0D;

    public C21080wp(C21070wo r1, C21060wn r2, C21050wm r3, C14830m7 r4, C16120oU r5, AnonymousClass01H r6, AnonymousClass01H r7, AnonymousClass01H r8, AnonymousClass01H r9, AnonymousClass01H r10, AnonymousClass01H r11, AnonymousClass01H r12, AnonymousClass01H r13, AnonymousClass01H r14) {
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
        this.A08 = r6;
        this.A00 = r1;
        this.A02 = r3;
        this.A0B = r7;
        this.A09 = r8;
        this.A0C = r9;
        this.A0A = r10;
        this.A07 = r11;
        this.A0D = r12;
        this.A06 = r13;
        this.A05 = r14;
    }
}
