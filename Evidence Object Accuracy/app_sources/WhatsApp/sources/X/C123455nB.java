package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5nB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123455nB extends AbstractC118075bE {
    public final C15550nR A00;
    public final AnonymousClass018 A01;
    public final C20370ve A02;
    public final AnonymousClass14X A03;

    public C123455nB(C15450nH r7, C15550nR r8, C14830m7 r9, AnonymousClass018 r10, C15650ng r11, C20370ve r12, AbstractC16870pt r13, AnonymousClass14X r14, C20320vZ r15) {
        super(r7, r9, r11, r13, r15);
        this.A02 = r12;
        this.A03 = r14;
        this.A00 = r8;
        this.A01 = r10;
    }

    @Override // X.AbstractC118075bE
    public void A08(String str) {
        String str2;
        super.A08(str);
        AnonymousClass1IR A0N = this.A02.A0N(null, str);
        if (A0N != null) {
            C127065tt r7 = new C127065tt(5);
            UserJid userJid = A0N.A0D;
            if (userJid == null || (str2 = this.A00.A0B(userJid).A0D()) == null) {
                str2 = "";
            }
            r7.A00 = new C127735uy(str2, AnonymousClass14X.A05(this.A01, A0N.A00(), A0N.A08, 0, true), this.A03.A0J(A0N), AnonymousClass14X.A01(A0N));
            ((AbstractC118075bE) this).A01.A0A(r7);
            C127735uy r3 = r7.A00;
            AnonymousClass3FW r2 = this.A05;
            r2.A01("transaction_status", C31001Zq.A05(A0N.A03, A0N.A02));
            r2.A01("transaction_status_name", r3.A03);
            r2.A01("merchant_name", r3.A01);
        }
    }
}
