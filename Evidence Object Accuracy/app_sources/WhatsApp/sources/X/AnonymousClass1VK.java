package X;

/* renamed from: X.1VK  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass1VK implements AbstractC14590lg {
    public final /* synthetic */ int A00;
    public final /* synthetic */ String A01;

    public /* synthetic */ AnonymousClass1VK(String str, int i) {
        this.A01 = str;
        this.A00 = i;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        String str = this.A01;
        int i = this.A00;
        AnonymousClass1EV r11 = (AnonymousClass1EV) obj;
        C91774Tb r5 = (C91774Tb) r11.A03.get(str);
        if (r5 != null && r5.A04 == 0) {
            r5.A00 = i;
            r5.A04 = System.currentTimeMillis();
            AnonymousClass1Q5 r2 = r11.A02;
            int hashCode = str.hashCode();
            r2.A04(hashCode, "IqMessagePerfLoggerInterceptor", true);
            r2.A07.AKz("iq_type", r2.A06.A05, hashCode, (long) i);
            r2.A02(hashCode, "iq_queue");
        }
    }
}
