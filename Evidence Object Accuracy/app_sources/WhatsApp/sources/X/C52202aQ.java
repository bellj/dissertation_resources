package X;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.backup.google.SettingsGoogleDrive;

/* renamed from: X.2aQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52202aQ extends ClickableSpan {
    public final /* synthetic */ SettingsGoogleDrive A00;

    public C52202aQ(SettingsGoogleDrive settingsGoogleDrive) {
        this.A00 = settingsGoogleDrive;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        AnonymousClass2KB r1 = new AnonymousClass2KB();
        r1.A00 = Boolean.TRUE;
        SettingsGoogleDrive settingsGoogleDrive = this.A00;
        settingsGoogleDrive.A0l.A07(r1);
        ((ActivityC13790kL) settingsGoogleDrive).A00.A06(view.getContext(), C12970iu.A0B(((ActivityC13790kL) settingsGoogleDrive).A02.A00("https://faq.whatsapp.com/android/chats/about-whatsapp-backup-capacity-on-android")));
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setColor(this.A00.getResources().getColor(R.color.link_color));
        textPaint.setUnderlineText(false);
    }
}
