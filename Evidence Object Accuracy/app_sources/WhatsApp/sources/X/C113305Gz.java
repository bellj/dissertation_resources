package X;

import java.util.List;

/* renamed from: X.5Gz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113305Gz extends RuntimeException {
    public final List zzoy = null;

    public C113305Gz() {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
