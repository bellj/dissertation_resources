package X;

import java.util.Map;

/* renamed from: X.52K  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass52K implements AnonymousClass5TA {
    public C83123wj A00;
    public final C94394bk A01;

    public /* synthetic */ AnonymousClass52K(C94394bk r1, AnonymousClass4YR r2) {
        this.A01 = r1;
        this.A00 = (C83123wj) r2;
    }

    @Override // X.AnonymousClass5TA
    public boolean ALL(Object obj) {
        C92364Vp r1 = this.A01.A01;
        AbstractC117035Xw r4 = r1.A00;
        if (!(obj instanceof Map)) {
            return false;
        }
        C83123wj r3 = this.A00;
        if (!r3.A06() || (((AnonymousClass4YR) r3).A01 == null && r1.A03.contains(EnumC87084Ad.DEFAULT_PATH_LEAF_TO_NULL))) {
            return true;
        }
        return r4.AFy(obj).containsAll(r3.A01);
    }
}
