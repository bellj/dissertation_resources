package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiQuickBuyActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.5wg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128795wg {
    public final /* synthetic */ C30821Yy A00;
    public final /* synthetic */ IndiaUpiCheckOrderDetailsActivity A01;

    public /* synthetic */ C128795wg(C30821Yy r1, IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity) {
        this.A01 = indiaUpiCheckOrderDetailsActivity;
        this.A00 = r1;
    }

    public final void A00(C452120p r10, String str, String str2, String str3, String str4) {
        String str5;
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A01;
        C30821Yy r3 = this.A00;
        indiaUpiCheckOrderDetailsActivity.AaN();
        if (r10 == null) {
            ((AbstractActivityC121665jA) indiaUpiCheckOrderDetailsActivity).A0H = str;
            ((AbstractActivityC121665jA) indiaUpiCheckOrderDetailsActivity).A0I = str4;
            ((AbstractActivityC121665jA) indiaUpiCheckOrderDetailsActivity).A08 = C117305Zk.A0I(C117305Zk.A0J(), String.class, str2, "upiHandle");
            if (!TextUtils.isEmpty(str3)) {
                ((AbstractActivityC121665jA) indiaUpiCheckOrderDetailsActivity).A06 = C117305Zk.A0I(C117305Zk.A0J(), String.class, str3, "accountHolderName");
            }
            if (((AbstractActivityC121525iS) indiaUpiCheckOrderDetailsActivity).A0C == null || !((ActivityC13810kN) indiaUpiCheckOrderDetailsActivity).A0C.A07(1916)) {
                PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
                paymentBottomSheet.A01 = indiaUpiCheckOrderDetailsActivity.A3J(r3, paymentBottomSheet);
                indiaUpiCheckOrderDetailsActivity.Adm(paymentBottomSheet);
                indiaUpiCheckOrderDetailsActivity.A3R(paymentBottomSheet);
                return;
            }
            HashMap A11 = C12970iu.A11();
            A11.put("action", "start");
            HashMap A112 = C12970iu.A11();
            A112.put("receiver_jid", ((AbstractActivityC121525iS) indiaUpiCheckOrderDetailsActivity).A0C.getRawString());
            A112.put("receiver_vpa", str2);
            A112.put("order_message_id", indiaUpiCheckOrderDetailsActivity.A0D.A01);
            AnonymousClass3FB r6 = new AnonymousClass3FB("upi_p2m_order_payment", null, A11);
            if (indiaUpiCheckOrderDetailsActivity instanceof IndiaUpiQuickBuyActivity) {
                str5 = "chat";
            } else {
                str5 = "order_details";
            }
            indiaUpiCheckOrderDetailsActivity.A05.A06(null, new AnonymousClass5UZ() { // from class: X.6Bu
                @Override // X.AnonymousClass5UZ
                public final void AQd(Map map) {
                    IndiaUpiCheckOrderDetailsActivity.this.finish();
                }
            }, r6, str5, A112);
            return;
        }
        indiaUpiCheckOrderDetailsActivity.Ado(R.string.payments_generic_error);
    }
}
