package X;

import android.os.Build;

/* renamed from: X.1Mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28391Mz {
    public static boolean A00() {
        return Build.VERSION.SDK_INT >= 17;
    }

    public static boolean A01() {
        return Build.VERSION.SDK_INT >= 19;
    }

    public static boolean A02() {
        return Build.VERSION.SDK_INT >= 21;
    }

    public static boolean A03() {
        return Build.VERSION.SDK_INT >= 23;
    }

    public static boolean A04() {
        return Build.VERSION.SDK_INT >= 27;
    }

    public static boolean A05() {
        return Build.VERSION.SDK_INT >= 31;
    }
}
