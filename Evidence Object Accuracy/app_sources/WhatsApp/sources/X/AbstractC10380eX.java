package X;

import java.util.Iterator;

/* renamed from: X.0eX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC10380eX implements Iterator, AnonymousClass076 {
    public AnonymousClass05U A00;
    public AnonymousClass05U A01;

    public abstract AnonymousClass05U A00(AnonymousClass05U v);

    public abstract AnonymousClass05U A01(AnonymousClass05U v);

    public AbstractC10380eX(AnonymousClass05U r1, AnonymousClass05U r2) {
        this.A00 = r2;
        this.A01 = r1;
    }

    @Override // X.AnonymousClass076
    public void Aea(AnonymousClass05U r3) {
        AnonymousClass05U r0;
        AnonymousClass05U r1 = this.A00;
        if (r1 == r3 && r3 == this.A01) {
            r1 = null;
            this.A01 = null;
            this.A00 = null;
        }
        if (r1 == r3) {
            r1 = A00(r1);
            this.A00 = r1;
        }
        AnonymousClass05U r02 = this.A01;
        if (r02 == r3) {
            if (r02 == r1 || r1 == null) {
                r0 = null;
            } else {
                r0 = A01(r02);
            }
            this.A01 = r0;
        }
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.A01 != null;
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        AnonymousClass05U r0;
        AnonymousClass05U r1 = this.A01;
        AnonymousClass05U r02 = this.A00;
        if (r1 == r02 || r02 == null) {
            r0 = null;
        } else {
            r0 = A01(r1);
        }
        this.A01 = r0;
        return r1;
    }
}
