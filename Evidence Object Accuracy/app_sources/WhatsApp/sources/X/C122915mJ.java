package X;

import android.view.View;

/* renamed from: X.5mJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122915mJ extends C122985mQ {
    public final int A00;
    public final View.OnClickListener A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public C122915mJ(View.OnClickListener onClickListener, String str, String str2, int i, boolean z) {
        super(1008);
        this.A00 = i;
        this.A03 = str;
        this.A02 = str2;
        this.A04 = z;
        this.A01 = onClickListener;
    }
}
