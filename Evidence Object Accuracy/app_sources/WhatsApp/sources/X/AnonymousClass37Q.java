package X;

import java.lang.ref.WeakReference;

/* renamed from: X.37Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37Q extends AbstractC16350or {
    public final C18750sx A00;
    public final C237512w A01;
    public final WeakReference A02;

    public AnonymousClass37Q(C18750sx r2, AnonymousClass5V8 r3, C237512w r4) {
        this.A02 = C12970iu.A10(r3);
        this.A00 = r2;
        this.A01 = r4;
    }
}
