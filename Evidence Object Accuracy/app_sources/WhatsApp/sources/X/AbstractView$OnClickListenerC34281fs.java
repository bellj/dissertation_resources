package X;

import android.os.SystemClock;
import android.view.View;
import com.facebook.redex.EmptyBaseViewOnClick0CListener;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import com.whatsapp.util.ViewOnClickCListenerShape3S0300000_I1;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;

/* renamed from: X.1fs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractView$OnClickListenerC34281fs extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public long A00;

    public abstract void A04(View view);

    public static void A00(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape17S0100000_I1(obj, i));
    }

    public static void A01(View view, Object obj, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape18S0100000_I1_1(obj, i));
    }

    public static void A02(View view, Object obj, Object obj2, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(obj, i, obj2));
    }

    public static void A03(View view, Object obj, Object obj2, Object obj3, int i) {
        view.setOnClickListener(new ViewOnClickCListenerShape3S0300000_I1(obj, obj2, obj3, i));
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (elapsedRealtime - this.A00 > 1000) {
            this.A00 = elapsedRealtime;
            A04(view);
        }
    }
}
