package X;

/* renamed from: X.1e7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33461e7 {
    public final C14830m7 A00;
    public final C21560xc A01;

    public C33461e7(C14830m7 r1, C21560xc r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public C33601ee A00() {
        C91814Tf r2 = new C91814Tf();
        if (A02("STORAGE_USAGE_MEDIA_SIZE_CACHE_TIME")) {
            C21560xc r1 = this.A01;
            r2.A04 = r1.A01("STORAGE_USAGE_MEDIA_SIZE");
            r1.A01("STORAGE_USAGE_PHOTOS_SIZE");
            r1.A01("STORAGE_USAGE_AUDIO_SIZE");
            r1.A01("STORAGE_USAGE_VIDEOS_SIZE");
            r1.A01("STORAGE_USAGE_DOCUMENTS_SIZE");
        }
        if (A02("STORAGE_USAGE_LOCAL_BACKUP_RELATED_FILES_SIZE_CACHE_TIME_KEY")) {
            C21560xc r12 = this.A01;
            r12.A01("STORAGE_USAGE_CHAT_DB_SIZE");
            r12.A01("STORAGE_USAGE_OTHER_BACKUPS_SIZE");
        }
        if (A02("STORAGE_USAGE_LARGE_FILES_CACHE_TIME")) {
            C21560xc r13 = this.A01;
            r2.A03 = r13.A01("STORAGE_USAGE_LARGE_FILES_MEDIA_SIZE");
            r2.A01 = r13.A00("STORAGE_USAGE_LARGE_FILES_COUNT");
            r2.A06 = r13.A03("STORAGE_USAGE_LARGE_FILES_ROW_IDS");
        }
        if (A02("STORAGE_USAGE_FORWARDED_FILES_CACHE_TIME")) {
            C21560xc r14 = this.A01;
            r2.A02 = r14.A01("STORAGE_USAGE_FORWARDED_FILES_MEDIA_SIZE");
            r2.A00 = r14.A00("STORAGE_USAGE_FORWARDED_FILES_COUNT");
            r2.A05 = r14.A03("STORAGE_USAGE_FORWARDED_FILES_ROW_IDS");
        }
        return new C33601ee(r2);
    }

    public final void A01(String str) {
        this.A01.A05(str, String.valueOf(this.A00.A00()));
    }

    public final boolean A02(String str) {
        Long A01 = this.A01.A01(str);
        return A01 != null && this.A00.A00() - A01.longValue() <= 2592000000L;
    }
}
