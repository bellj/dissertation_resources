package X;

/* renamed from: X.30x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615830x extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public String A06;

    public C615830x() {
        super(3488, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A03);
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A04);
        r3.Abe(4, this.A01);
        r3.Abe(5, this.A05);
        r3.Abe(6, this.A02);
        r3.Abe(7, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamKeepInChatPerf {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatEphemeralityDuration", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "kicErrorCode", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "kicMessageEphemeralityDuration", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "kicRequestType", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "requestSendTime", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "response", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "threadId", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
