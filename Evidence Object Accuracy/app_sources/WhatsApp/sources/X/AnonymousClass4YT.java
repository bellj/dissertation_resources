package X;

/* renamed from: X.4YT  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4YT {
    public AnonymousClass4YT A00 = null;

    public abstract void A03(int i, int i2);

    public abstract void A0B(Object[] objArr, int i, Object[] objArr2, int i2, int i3);

    public void A00(int i) {
        AnonymousClass5M6 r3 = (AnonymousClass5M6) this;
        C95044cz r1 = r3.A0k;
        r3.A02 = r1.A00;
        r1.A02(i);
        C94464br r2 = r3.A0T;
        if (r2 != null) {
            int i2 = r3.A0d;
            if (i2 == 4 || i2 == 3) {
                r2.A02.A0D(null, null, i, 0);
            } else {
                int i3 = r3.A0B + AnonymousClass5M6.A0n[i];
                if (i3 > r3.A07) {
                    r3.A07 = i3;
                }
                r3.A0B = i3;
            }
            if ((i >= 172 && i <= 177) || i == 191) {
                r3.A0E();
            }
        }
    }

    public void A01(int i, int i2) {
        AnonymousClass5M6 r3 = (AnonymousClass5M6) this;
        C95044cz r1 = r3.A0k;
        r3.A02 = r1.A00;
        if (i == 17) {
            r1.A07(i, i2);
        } else {
            r1.A06(i, i2);
        }
        C94464br r2 = r3.A0T;
        if (r2 != null) {
            int i3 = r3.A0d;
            if (i3 == 4 || i3 == 3) {
                r2.A02.A0D(null, null, i, i2);
            } else if (i != 188) {
                int i4 = r3.A0B + 1;
                if (i4 > r3.A07) {
                    r3.A07 = i4;
                }
                r3.A0B = i4;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        if (r8 == 57) goto L_0x0056;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(int r8, int r9) {
        /*
            r7 = this;
            r4 = r7
            X.5M6 r4 = (X.AnonymousClass5M6) r4
            X.4cz r1 = r4.A0k
            int r0 = r1.A00
            r4.A02 = r0
            r6 = 169(0xa9, float:2.37E-43)
            r3 = 54
            r2 = 4
            if (r9 >= r2) goto L_0x0086
            if (r8 == r6) goto L_0x0086
            int r0 = r8 + -54
            int r0 = r0 << 2
            int r0 = r0 + 59
            if (r8 >= r3) goto L_0x0020
            int r0 = r8 + -21
            int r0 = r0 << 2
            int r0 = r0 + 26
        L_0x0020:
            int r0 = r0 + r9
            r1.A02(r0)
        L_0x0024:
            X.4br r5 = r4.A0T
            if (r5 == 0) goto L_0x0040
            int r1 = r4.A0d
            if (r1 == r2) goto L_0x007f
            r0 = 3
            if (r1 == r0) goto L_0x007f
            if (r8 != r6) goto L_0x006f
            short r0 = r5.A05
            r0 = r0 | 64
            short r0 = (short) r0
            r5.A05 = r0
            int r0 = r4.A0B
            short r0 = (short) r0
            r5.A09 = r0
            r4.A0E()
        L_0x0040:
            int r5 = r4.A0d
            if (r5 == 0) goto L_0x005e
            r0 = 22
            if (r8 == r0) goto L_0x0056
            r0 = 24
            if (r8 == r0) goto L_0x0056
            r0 = 55
            if (r8 == r0) goto L_0x0056
            r0 = 57
            int r1 = r9 + 1
            if (r8 != r0) goto L_0x0058
        L_0x0056:
            int r1 = r9 + 2
        L_0x0058:
            int r0 = r4.A06
            if (r1 <= r0) goto L_0x005e
            r4.A06 = r1
        L_0x005e:
            if (r8 < r3) goto L_0x006e
            if (r5 != r2) goto L_0x006e
            X.4ce r0 = r4.A0R
            if (r0 == 0) goto L_0x006e
            X.4br r0 = new X.4br
            r0.<init>()
            r4.A08(r0)
        L_0x006e:
            return
        L_0x006f:
            int r1 = r4.A0B
            int[] r0 = X.AnonymousClass5M6.A0n
            r0 = r0[r8]
            int r1 = r1 + r0
            int r0 = r4.A07
            if (r1 <= r0) goto L_0x007c
            r4.A07 = r1
        L_0x007c:
            r4.A0B = r1
            goto L_0x0040
        L_0x007f:
            X.4dS r1 = r5.A02
            r0 = 0
            r1.A0D(r0, r0, r8, r9)
            goto L_0x0040
        L_0x0086:
            r0 = 256(0x100, float:3.59E-43)
            if (r9 < r0) goto L_0x0093
            r0 = 196(0xc4, float:2.75E-43)
            r1.A02(r0)
            r1.A07(r8, r9)
            goto L_0x0024
        L_0x0093:
            r1.A06(r8, r9)
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YT.A02(int, int):void");
    }

    public void A04(int i, String str) {
        AnonymousClass5M6 r4 = (AnonymousClass5M6) this;
        C95044cz r1 = r4.A0k;
        r4.A02 = r1.A00;
        AnonymousClass4YW r5 = r4.A0l;
        C95404de A0A = r5.A0A(str, 7);
        r1.A07(i, A0A.A03);
        C94464br r2 = r4.A0T;
        if (r2 != null) {
            int i2 = r4.A0d;
            if (i2 == 4 || i2 == 3) {
                r2.A02.A0D(A0A, r5, i, r4.A02);
            } else if (i == 187) {
                int i3 = r4.A0B + 1;
                if (i3 > r4.A07) {
                    r4.A07 = i3;
                }
                r4.A0B = i3;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(java.lang.Object r12) {
        /*
            r11 = this;
            r6 = r11
            X.5M6 r6 = (X.AnonymousClass5M6) r6
            X.4cz r2 = r6.A0k
            int r0 = r2.A00
            r6.A02 = r0
            X.4YW r7 = r6.A0l
            X.4de r8 = r7.A09(r12)
            int r9 = r8.A03
            int r1 = r8.A04
            r10 = 1
            r5 = 0
            r0 = 5
            if (r1 == r0) goto L_0x002d
            r0 = 6
            if (r1 == r0) goto L_0x002d
            r0 = 17
            if (r1 != r0) goto L_0x005c
            java.lang.String r0 = r8.A08
            char r1 = r0.charAt(r5)
            r0 = 74
            if (r1 == r0) goto L_0x002d
            r0 = 68
            if (r1 != r0) goto L_0x005c
        L_0x002d:
            r4 = 1
        L_0x002e:
            r3 = 18
            r0 = 20
            if (r4 != 0) goto L_0x003a
            r0 = 256(0x100, float:3.59E-43)
            if (r9 < r0) goto L_0x0058
            r0 = 19
        L_0x003a:
            r2.A07(r0, r9)
        L_0x003d:
            X.4br r2 = r6.A0T
            if (r2 == 0) goto L_0x0057
            int r1 = r6.A0d
            r0 = 4
            if (r1 == r0) goto L_0x005e
            r0 = 3
            if (r1 == r0) goto L_0x005e
            int r1 = r6.A0B
            if (r4 == 0) goto L_0x004e
            r10 = 2
        L_0x004e:
            int r1 = r1 + r10
            int r0 = r6.A07
            if (r1 <= r0) goto L_0x0055
            r6.A07 = r1
        L_0x0055:
            r6.A0B = r1
        L_0x0057:
            return
        L_0x0058:
            r2.A06(r3, r9)
            goto L_0x003d
        L_0x005c:
            r4 = 0
            goto L_0x002e
        L_0x005e:
            X.4dS r0 = r2.A02
            r0.A0D(r8, r7, r3, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YT.A05(java.lang.Object):void");
    }

    public void A06(String str, String str2, String str3, int i) {
        int i2;
        AnonymousClass5M6 r7 = (AnonymousClass5M6) this;
        C95044cz r1 = r7.A0k;
        r7.A02 = r1.A00;
        AnonymousClass4YW r4 = r7.A0l;
        C95404de A0C = r4.A0C(str, str2, str3, 9);
        r1.A07(i, A0C.A03);
        C94464br r2 = r7.A0T;
        if (r2 != null) {
            int i3 = r7.A0d;
            int i4 = 0;
            if (i3 == 4 || i3 == 3) {
                r2.A02.A0D(A0C, r4, i, 0);
                return;
            }
            char charAt = str3.charAt(0);
            int i5 = 1;
            int i6 = -2;
            int i7 = r7.A0B;
            switch (i) {
                case 178:
                    if (charAt == 'D' || charAt == 'J') {
                        i5 = 2;
                    }
                    i2 = i7 + i5;
                    break;
                case 179:
                    if (!(charAt == 'D' || charAt == 'J')) {
                        i6 = -1;
                    }
                    i2 = i7 + i6;
                    break;
                case 180:
                    if (charAt == 'D' || charAt == 'J') {
                        i4 = 1;
                    }
                    i2 = i7 + i4;
                    break;
                default:
                    if (charAt == 'D' || charAt == 'J') {
                        i6 = -3;
                    }
                    i2 = i7 + i6;
                    break;
            }
            if (i2 > r7.A07) {
                r7.A07 = i2;
            }
            r7.A0B = i2;
        }
    }

    public void A07(String str, String str2, String str3, int i, boolean z) {
        AnonymousClass5M6 r3 = (AnonymousClass5M6) this;
        C95044cz r2 = r3.A0k;
        r3.A02 = r2.A00;
        AnonymousClass4YW r4 = r3.A0l;
        int i2 = 10;
        if (z) {
            i2 = 11;
        }
        C95404de A0C = r4.A0C(str, str2, str3, i2);
        int i3 = A0C.A03;
        if (i == 185) {
            r2.A07(185, i3);
            int i4 = A0C.A00;
            if (i4 == 0) {
                i4 = C95574dz.A00(A0C.A08);
                A0C.A00 = i4;
            }
            r2.A06(i4 >> 2, 0);
        } else {
            r2.A07(i, i3);
        }
        C94464br r22 = r3.A0T;
        if (r22 != null) {
            int i5 = r3.A0d;
            if (i5 == 4 || i5 == 3) {
                r22.A02.A0D(A0C, r4, i, 0);
                return;
            }
            int i6 = A0C.A00;
            if (i6 == 0) {
                i6 = C95574dz.A00(A0C.A08);
                A0C.A00 = i6;
            }
            int i7 = r3.A0B + ((i6 & 3) - (i6 >> 2));
            if (i == 184) {
                i7++;
            }
            if (i7 > r3.A07) {
                r3.A07 = i7;
            }
            r3.A0B = i7;
        }
    }

    public void A08(C94464br r8) {
        C95294dS r0;
        AnonymousClass5M6 r2 = (AnonymousClass5M6) this;
        boolean z = r2.A0W;
        C95044cz r02 = r2.A0k;
        r2.A0W = z | r8.A03(r02.A01, r02.A00);
        short s = r8.A05;
        if ((s & 1) == 0) {
            int i = r2.A0d;
            if (i == 4) {
                C94464br r3 = r2.A0T;
                if (r3 != null) {
                    if (r8.A00 == r3.A00) {
                        r3.A05 = (short) ((s & 2) | r3.A05);
                        r0 = r3.A02;
                        r8.A02 = r0;
                        return;
                    }
                    r2.A0H(r8, 0);
                }
                C94464br r32 = r2.A0V;
                if (r32 != null) {
                    if (r8.A00 == r32.A00) {
                        r32.A05 = (short) (r32.A05 | (s & 2));
                        r8.A02 = r32.A02;
                        r2.A0T = r32;
                        return;
                    }
                    r32.A03 = r8;
                }
                r2.A0V = r8;
                r2.A0T = r8;
                r0 = new C95294dS(r8);
                r8.A02 = r0;
                return;
            }
            if (i == 3) {
                C94464br r03 = r2.A0T;
                if (r03 != null) {
                    r03.A02.A01 = r8;
                    return;
                }
            } else if (i == 1) {
                C94464br r1 = r2.A0T;
                if (r1 != null) {
                    r1.A08 = (short) r2.A07;
                    r2.A0H(r8, r2.A0B);
                }
                r2.A0T = r8;
                r2.A0B = 0;
                r2.A07 = 0;
                C94464br r04 = r2.A0V;
                if (r04 != null) {
                    r04.A03 = r8;
                }
                r2.A0V = r8;
                return;
            } else if (!(i == 2 && r2.A0T == null)) {
                return;
            }
            r2.A0T = r8;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004c, code lost:
        if (r6 != 167) goto L_0x004e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(X.C94464br r14, int r15) {
        /*
        // Method dump skipped, instructions count: 227
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YT.A09(X.4br, int):void");
    }

    public void A0A(C94464br r8, C94464br[] r9, int i, int i2) {
        AnonymousClass5M6 r6 = (AnonymousClass5M6) this;
        C95044cz r5 = r6.A0k;
        r6.A02 = r5.A00;
        r5.A02(170);
        r5.A0A(null, 0, (4 - (r5.A00 % 4)) % 4);
        r8.A01(r5, r6.A02, true);
        r5.A03(i);
        r5.A03(i2);
        for (C94464br r1 : r9) {
            r1.A01(r5, r6.A02, true);
        }
        r6.A0I(r8, r9);
    }
}
