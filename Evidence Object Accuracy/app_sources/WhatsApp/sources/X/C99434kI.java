package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99434kI implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        boolean z = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c == 3) {
                str2 = C95664e9.A08(parcel, readInt);
            } else if (c != 4) {
                z = C95664e9.A0H(parcel, c, 5, readInt, z);
            } else {
                C95664e9.A0F(parcel, readInt, 4);
                i = parcel.readInt();
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78503p1(str, str2, i, z);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78503p1[i];
    }
}
