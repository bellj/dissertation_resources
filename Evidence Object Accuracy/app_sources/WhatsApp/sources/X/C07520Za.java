package X;

import android.app.Activity;
import androidx.window.extensions.layout.WindowLayoutComponent;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.0Za  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07520Za implements AbstractC12440hv {
    public final WindowLayoutComponent A00;
    public final Map A01 = new LinkedHashMap();
    public final Map A02 = new LinkedHashMap();
    public final ReentrantLock A03 = new ReentrantLock();

    public C07520Za(WindowLayoutComponent windowLayoutComponent) {
        this.A00 = windowLayoutComponent;
    }

    @Override // X.AbstractC12440hv
    public void Aa1(Activity activity, AnonymousClass024 r6, Executor executor) {
        C16700pc.A0E(activity, 0);
        ReentrantLock reentrantLock = this.A03;
        reentrantLock.lock();
        try {
            Map map = this.A01;
            C10680f1 r0 = (C10680f1) map.get(activity);
            if (r0 == null) {
                C10680f1 r1 = new C10680f1(activity);
                map.put(activity, r1);
                this.A02.put(r6, activity);
                r1.A00(r6);
                this.A00.addWindowLayoutInfoListener(activity, r1);
            } else {
                r0.A00(r6);
                this.A02.put(r6, activity);
            }
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // X.AbstractC12440hv
    public void AfD(AnonymousClass024 r4) {
        C10680f1 r1;
        C16700pc.A0E(r4, 0);
        ReentrantLock reentrantLock = this.A03;
        reentrantLock.lock();
        try {
            Activity activity = (Activity) this.A02.get(r4);
            if (!(activity == null || (r1 = (C10680f1) this.A01.get(activity)) == null)) {
                r1.A01(r4);
                if (r1.A03()) {
                    this.A00.removeWindowLayoutInfoListener(r1);
                }
            }
        } finally {
            reentrantLock.unlock();
        }
    }
}
