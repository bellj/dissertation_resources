package X;

import android.os.Bundle;
import java.util.List;

/* renamed from: X.2fD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53782fD extends AnonymousClass07A {
    public final AnonymousClass4JJ A00;
    public final C30211Wn A01;
    public final List A02;
    public final List A03;

    public C53782fD(Bundle bundle, AbstractC001500q r2, AnonymousClass4JJ r3, C30211Wn r4, List list, List list2) {
        super(bundle, r2);
        this.A00 = r3;
        this.A02 = list;
        this.A01 = r4;
        this.A03 = list2;
    }

    @Override // X.AnonymousClass07A
    public AnonymousClass015 A02(AnonymousClass07E r9, Class cls, String str) {
        AnonymousClass4JJ r0 = this.A00;
        C30211Wn r5 = this.A01;
        List list = this.A02;
        List list2 = this.A03;
        AnonymousClass01J r1 = r0.A00.A04;
        return new C53822fI(AbstractC250617y.A00(r1.AO3), r9, C12970iu.A0V(r1), r5, list, list2);
    }
}
