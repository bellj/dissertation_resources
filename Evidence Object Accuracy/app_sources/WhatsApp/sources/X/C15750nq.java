package X;

import android.content.SharedPreferences;
import com.whatsapp.jid.UserJid;
import java.util.UUID;

/* renamed from: X.0nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15750nq {
    public final AbstractC15710nm A00;
    public final C16630pM A01;
    public final AnonymousClass01H A02 = new C002601e(null, new AnonymousClass01N() { // from class: X.5EO
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            return C15750nq.this.A01.A01("migration_export");
        }
    });

    public C15750nq(AbstractC15710nm r4, C16630pM r5) {
        this.A00 = r4;
        this.A01 = r5;
    }

    public AnonymousClass2F8 A00() {
        SharedPreferences sharedPreferences = (SharedPreferences) this.A02.get();
        String string = sharedPreferences.getString("/export/enc/active/owner", null);
        String string2 = sharedPreferences.getString("/export/enc/active/version", null);
        String string3 = sharedPreferences.getString("/export/enc/active/account_hash", null);
        String string4 = sharedPreferences.getString("/export/enc/active/server_salt", null);
        long j = sharedPreferences.getLong("/export/enc/active/last_fetch_time", 0);
        String string5 = sharedPreferences.getString("/export/enc/active/seed", null);
        if (!(string == null || string2 == null || string3 == null || string4 == null || string5 == null)) {
            try {
                return new AnonymousClass2F8(UserJid.get(string), string2, string3, string4, string5, j);
            } catch (AnonymousClass1MW e) {
                AbstractC15710nm r3 = this.A00;
                StringBuilder sb = new StringBuilder("invalid jid: ");
                sb.append(string);
                r3.A02("xpm-export-preferences-active", sb.toString(), e);
            }
        }
        return null;
    }

    public AnonymousClass2F8 A01() {
        SharedPreferences sharedPreferences = (SharedPreferences) this.A02.get();
        String string = sharedPreferences.getString("/export/enc/prefetched/owner", null);
        String string2 = sharedPreferences.getString("/export/enc/prefetched/version", null);
        String string3 = sharedPreferences.getString("/export/enc/prefetched/account_hash", null);
        String string4 = sharedPreferences.getString("/export/enc/prefetched/server_salt", null);
        long j = sharedPreferences.getLong("/export/enc/prefetched/last_fetch_time", 0);
        String string5 = sharedPreferences.getString("/export/enc/prefetched/seed", null);
        if (!(string == null || string2 == null || string3 == null || string4 == null || string5 == null)) {
            try {
                return new AnonymousClass2F8(UserJid.get(string), string2, string3, string4, string5, j);
            } catch (AnonymousClass1MW e) {
                AbstractC15710nm r3 = this.A00;
                StringBuilder sb = new StringBuilder("invalid jid: ");
                sb.append(string);
                r3.A02("xpm-export-preferences-prefetched", sb.toString(), e);
            }
        }
        return null;
    }

    public String A02() {
        AnonymousClass01H r3 = this.A02;
        String string = ((SharedPreferences) r3.get()).getString("/export/logging/funnelId", null);
        if (string != null) {
            return string;
        }
        String obj = UUID.randomUUID().toString();
        ((SharedPreferences) r3.get()).edit().putString("/export/logging/funnelId", obj).apply();
        return obj;
    }

    public void A03() {
        ((SharedPreferences) this.A02.get()).edit().remove("/export/logging/funnelId").apply();
    }

    public void A04() {
        ((SharedPreferences) this.A02.get()).edit().remove("/export/enc/prefetched/owner").remove("/export/enc/prefetched/version").remove("/export/enc/prefetched/account_hash").remove("/export/enc/prefetched/server_salt").remove("/export/enc/prefetched/last_fetch_time").remove("/export/enc/prefetched/seed").apply();
    }
}
