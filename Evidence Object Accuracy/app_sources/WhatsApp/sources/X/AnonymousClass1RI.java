package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.1RI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1RI extends AbstractC29111Qx {
    public final /* synthetic */ C251818k A00;
    public final /* synthetic */ C251918l A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ List A06;
    public final /* synthetic */ Map A07;
    public final /* synthetic */ byte[] A08;
    public final /* synthetic */ byte[] A09;

    public AnonymousClass1RI(C251818k r1, C251918l r2, String str, String str2, String str3, String str4, List list, Map map, byte[] bArr, byte[] bArr2) {
        this.A01 = r2;
        this.A00 = r1;
        this.A05 = str;
        this.A03 = str2;
        this.A04 = str3;
        this.A09 = bArr;
        this.A08 = bArr2;
        this.A02 = str4;
        this.A07 = map;
        this.A06 = list;
    }
}
