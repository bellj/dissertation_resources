package X;

/* renamed from: X.2Ut  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51442Ut extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Boolean A0D;

    public C51442Ut() {
        super(3124, new AnonymousClass00E(1, 1, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(1, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(7, this.A05);
        r3.Abe(11, this.A06);
        r3.Abe(12, this.A07);
        r3.Abe(13, this.A08);
        r3.Abe(14, this.A09);
        r3.Abe(15, this.A0A);
        r3.Abe(16, this.A0B);
        r3.Abe(17, this.A0C);
        r3.Abe(18, this.A0D);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamContactInfo {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "avatar", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "blockContact", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactDetails", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactInfoVisit", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "disappearingMessages", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "encryption", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mute", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "payments", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "reportContact", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "search", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "shareContact", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "starredMessages", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "videoCall", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "voiceCall", this.A0D);
        sb.append("}");
        return sb.toString();
    }
}
