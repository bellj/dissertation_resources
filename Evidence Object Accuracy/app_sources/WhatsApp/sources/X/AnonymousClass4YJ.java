package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.4YJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4YJ {
    public int A00;
    public long A01;
    public long A02;
    public C94374bh A03 = C94374bh.A04;
    public Object A04;
    public Object A05;

    public static int A00(AnonymousClass4YJ r0, Timeline timeline, Object obj) {
        return timeline.A0A(r0, obj).A00;
    }

    public int A01(int i) {
        AnonymousClass4XE r4 = this.A03.A03[i];
        int i2 = 0;
        while (true) {
            int[] iArr = r4.A01;
            if (i2 >= iArr.length || iArr[i2] == 0 || iArr[i2] == 1) {
                break;
            }
            i2++;
        }
        return i2;
    }

    public int A02(long j) {
        int length;
        C94374bh r9 = this.A03;
        long j2 = this.A01;
        if (j != Long.MIN_VALUE && (j2 == -9223372036854775807L || j < j2)) {
            int i = 0;
            while (true) {
                long[] jArr = r9.A02;
                length = jArr.length;
                if (i >= length || jArr[i] == Long.MIN_VALUE) {
                    break;
                }
                if (j < jArr[i]) {
                    AnonymousClass4XE r5 = r9.A03[i];
                    int i2 = r5.A00;
                    if (i2 == -1) {
                        break;
                    }
                    int i3 = 0;
                    while (true) {
                        int[] iArr = r5.A01;
                        if (i3 >= iArr.length || iArr[i3] == 0 || iArr[i3] == 1) {
                            break;
                        }
                        i3++;
                    }
                }
                i++;
            }
            if (i < length) {
                return i;
            }
        }
        return -1;
    }

    public int A03(long j) {
        int i;
        C94374bh r7 = this.A03;
        long j2 = this.A01;
        long[] jArr = r7.A02;
        int length = jArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                if (j == Long.MIN_VALUE) {
                    break;
                }
                long j3 = jArr[length];
                if (j3 != Long.MIN_VALUE) {
                    i = (j > j3 ? 1 : (j == j3 ? 0 : -1));
                } else if (j2 != -9223372036854775807L) {
                    i = (j > j2 ? 1 : (j == j2 ? 0 : -1));
                } else {
                    continue;
                }
                if (i >= 0) {
                    break;
                }
            } else {
                return -1;
            }
        }
        AnonymousClass4XE r6 = r7.A03[length];
        int i2 = r6.A00;
        if (i2 != -1) {
            int i3 = 0;
            while (true) {
                int[] iArr = r6.A01;
                if (i3 >= iArr.length || iArr[i3] == 0 || iArr[i3] == 1) {
                    break;
                }
                i3++;
            }
            return -1;
        }
        return length;
    }

    public long A04(int i, int i2) {
        AnonymousClass4XE r2 = this.A03.A03[i];
        if (r2.A00 != -1) {
            return r2.A02[i2];
        }
        return -9223372036854775807L;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || !AnonymousClass4YJ.class.equals(obj.getClass())) {
                return false;
            }
            AnonymousClass4YJ r7 = (AnonymousClass4YJ) obj;
            if (!AnonymousClass3JZ.A0H(this.A04, r7.A04) || !AnonymousClass3JZ.A0H(this.A05, r7.A05) || this.A00 != r7.A00 || this.A01 != r7.A01 || this.A02 != r7.A02 || !AnonymousClass3JZ.A0H(this.A03, r7.A03)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int A0D = (217 + C72453ed.A0D(this.A04)) * 31;
        Object obj = this.A05;
        if (obj != null) {
            i = obj.hashCode();
        }
        return C12990iw.A08(this.A03, C72453ed.A0A(C72453ed.A0A((((A0D + i) * 31) + this.A00) * 31, this.A01), this.A02));
    }
}
