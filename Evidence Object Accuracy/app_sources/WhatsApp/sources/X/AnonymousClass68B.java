package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilOrderDetailsActivity;

/* renamed from: X.68B  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68B implements AnonymousClass1QA {
    public final /* synthetic */ BrazilOrderDetailsActivity A00;
    public final /* synthetic */ EnumC124545pi A01;
    public final /* synthetic */ C128335vw A02;

    @Override // X.AnonymousClass1QA
    public void AWZ() {
    }

    public AnonymousClass68B(BrazilOrderDetailsActivity brazilOrderDetailsActivity, EnumC124545pi r2, C128335vw r3) {
        this.A00 = brazilOrderDetailsActivity;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass1QA
    public void AWX() {
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = this.A00;
        AbstractC005102i A1U = brazilOrderDetailsActivity.A1U();
        if (A1U != null) {
            int i = this.A02.A00;
            int i2 = R.string.order_summary_bar_text;
            if (i == 1) {
                i2 = R.string.review_order_action_bar_text;
            }
            A1U.A0I(brazilOrderDetailsActivity.getResources().getString(i2));
        }
        brazilOrderDetailsActivity.A08.A00(brazilOrderDetailsActivity, ((ActivityC13790kL) brazilOrderDetailsActivity).A01, this.A01, this.A02, "WhatsappPay", 3);
    }
}
