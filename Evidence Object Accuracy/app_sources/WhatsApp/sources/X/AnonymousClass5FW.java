package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* renamed from: X.5FW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5FW implements AbstractC02750Dv {
    public boolean A00;
    public final C10730f6 A01 = new C10730f6();
    public final AbstractC117195Yx A02;

    public AnonymousClass5FW(AbstractC117195Yx r2) {
        this.A02 = r2;
    }

    public static long A00(C10730f6 r3, AnonymousClass5FW r4) {
        return r4.A02.AZp(r3, (long) DefaultCrypto.BUFFER_SIZE);
    }

    @Override // X.AbstractC02750Dv
    public C10730f6 A6i() {
        return this.A01;
    }

    @Override // X.AbstractC02750Dv
    public C10730f6 AB0() {
        return this.A01;
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x015a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x014e  */
    @Override // X.AbstractC02750Dv
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AIW(X.C08960c8 r19) {
        /*
        // Method dump skipped, instructions count: 368
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5FW.AIW(X.0c8):long");
    }

    @Override // X.AbstractC02750Dv
    public InputStream AIz() {
        return new AnonymousClass49D(this);
    }

    @Override // X.AbstractC02750Dv
    public AbstractC02750Dv AZ1() {
        return new AnonymousClass5FW(new AnonymousClass5FX(this));
    }

    @Override // X.AbstractC117195Yx
    public long AZp(C10730f6 r7, long j) {
        if (j < 0) {
            throw C12970iu.A0f(C12970iu.A0w(C12960it.A0k("byteCount < 0: "), j));
        } else if (true ^ this.A00) {
            C10730f6 r5 = this.A01;
            if (r5.A00 == 0 && A00(r5, this) == -1) {
                return -1;
            }
            return r5.AZp(r7, Math.min(j, r5.A00));
        } else {
            throw C12960it.A0U("closed");
        }
    }

    @Override // X.AbstractC02750Dv
    public boolean AaY(long j) {
        C10730f6 r3;
        if (j < 0) {
            throw C12970iu.A0f(C12970iu.A0w(C12960it.A0k("byteCount < 0: "), j));
        } else if (!(!this.A00)) {
            throw C12960it.A0U("closed");
        } else {
            do {
                r3 = this.A01;
                if (r3.A00 >= j) {
                    return true;
                }
            } while (A00(r3, this) != -1);
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0067, code lost:
        r0 = r7[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x008a, code lost:
        if (r5 == -2) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008c, code lost:
        if (r5 == -1) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009c, code lost:
        r12.A0G((long) r18.A01[r5].A01());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a8, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00b7, code lost:
        return -1;
     */
    @Override // X.AbstractC02750Dv
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AbU(X.AnonymousClass5I3 r18) {
        /*
            r17 = this;
            r0 = 0
            r14 = r18
            X.C16700pc.A0D(r14, r0)
            r13 = r17
            boolean r1 = r13.A00
            r0 = 1
            r1 = r1 ^ r0
            if (r1 != 0) goto L_0x0015
            java.lang.String r0 = "closed"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        L_0x0015:
            X.0f6 r12 = r13.A01
            X.4bt r11 = r12.A01
            r10 = -1
            if (r11 == 0) goto L_0x00ad
            byte[] r9 = r11.A06
            int r0 = r11.A01
            int r8 = r11.A00
            int[] r7 = r14.A00
            r6 = r11
            r2 = 0
            r5 = -1
        L_0x0027:
            int r1 = r2 + 1
            r15 = r7[r2]
            int r4 = r1 + 1
            r1 = r7[r1]
            if (r1 == r10) goto L_0x0032
            r5 = r1
        L_0x0032:
            if (r6 == 0) goto L_0x00ad
            r16 = 0
            if (r15 >= 0) goto L_0x006a
            int r1 = -r15
            int r2 = r4 + r1
        L_0x003b:
            int r3 = r0 + 1
            byte r0 = r9[r0]
            r15 = r0 & 255(0xff, float:3.57E-43)
            int r1 = r4 + 1
            r0 = r7[r4]
            if (r15 != r0) goto L_0x0089
            boolean r0 = X.C12960it.A1V(r1, r2)
            if (r3 != r8) goto L_0x0056
            X.4bt r6 = r6.A02
            if (r6 != 0) goto L_0x005b
            java.lang.RuntimeException r0 = X.C72453ed.A0m()
            throw r0
        L_0x0056:
            if (r0 != 0) goto L_0x0067
            r0 = r3
            r4 = r1
            goto L_0x003b
        L_0x005b:
            int r3 = r6.A01
            byte[] r9 = r6.A06
            int r8 = r6.A00
            if (r6 != r11) goto L_0x0056
            if (r0 == 0) goto L_0x00ad
            r6 = r16
        L_0x0067:
            r0 = r7[r1]
            goto L_0x0099
        L_0x006a:
            int r3 = r0 + 1
            byte r0 = r9[r0]
            r2 = r0 & 255(0xff, float:3.57E-43)
            int r1 = r4 + r15
        L_0x0072:
            if (r4 == r1) goto L_0x0089
            r0 = r7[r4]
            if (r2 != r0) goto L_0x0086
            int r4 = r4 + r15
            r0 = r7[r4]
            if (r3 != r8) goto L_0x0099
            X.4bt r6 = r6.A02
            if (r6 != 0) goto L_0x008f
            java.lang.RuntimeException r0 = X.C72453ed.A0m()
            throw r0
        L_0x0086:
            int r4 = r4 + 1
            goto L_0x0072
        L_0x0089:
            r0 = -2
            if (r5 == r0) goto L_0x00ad
            if (r5 == r10) goto L_0x00b7
            goto L_0x009c
        L_0x008f:
            int r3 = r6.A01
            byte[] r9 = r6.A06
            int r8 = r6.A00
            if (r6 != r11) goto L_0x0099
            r6 = r16
        L_0x0099:
            if (r0 < 0) goto L_0x00a9
            r5 = r0
        L_0x009c:
            X.0c8[] r0 = r14.A01
            r0 = r0[r5]
            int r0 = r0.A01()
            long r0 = (long) r0
            r12.A0G(r0)
            return r5
        L_0x00a9:
            int r2 = -r0
            r0 = r3
            goto L_0x0027
        L_0x00ad:
            long r3 = A00(r12, r13)
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0015
        L_0x00b7:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5FW.AbU(X.5I3):int");
    }

    @Override // X.AbstractC117195Yx, java.io.Closeable, java.lang.AutoCloseable, java.nio.channels.Channel
    public void close() {
        if (!this.A00) {
            this.A00 = true;
            this.A02.close();
            this.A01.A0A();
        }
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return !this.A00;
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) {
        C16700pc.A0D(byteBuffer, 0);
        C10730f6 r5 = this.A01;
        if (r5.A00 == 0 && A00(r5, this) == -1) {
            return -1;
        }
        return r5.read(byteBuffer);
    }

    @Override // X.AbstractC02750Dv
    public byte readByte() {
        if (AaY(1)) {
            return this.A01.readByte();
        }
        throw new EOFException();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("buffer(");
        A0k.append(this.A02);
        return C12970iu.A0u(A0k);
    }
}
