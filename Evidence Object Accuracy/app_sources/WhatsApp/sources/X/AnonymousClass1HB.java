package X;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1HB  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1HB {
    public final AnonymousClass1HE A00 = new AnonymousClass1HE();
    public final List A01 = new ArrayList(2);
    public volatile AbstractC15710nm A02;

    public synchronized void A00(AnonymousClass1IA r6) {
        AnonymousClass1IG r3 = AnonymousClass1IG.A01;
        AnonymousClass1HE r2 = this.A00;
        synchronized (r2) {
            HashMap hashMap = r2.A02;
            AnonymousClass1IH r0 = (AnonymousClass1IH) hashMap.get(r6);
            if (r0 == null) {
                hashMap.put(r6, new AnonymousClass1IH(r3));
            } else {
                r0.A00 = r3;
            }
        }
    }

    public synchronized void A01(AnonymousClass1IA r5, Runnable runnable) {
        AnonymousClass1HE r2 = this.A00;
        synchronized (r2) {
            AnonymousClass1IH A03 = r2.A03(r5);
            A03.A03.remove(runnable);
            A03.A04.remove(runnable);
            String A00 = AnonymousClass1HE.A00(runnable);
            r2.A01.remove(A00);
            r2.A00.remove(A00);
        }
        String A002 = AnonymousClass1HE.A00(runnable);
        for (AnonymousClass1HG r1 : this.A01) {
            if (r1 instanceof AnonymousClass1HH) {
                Set set = ((AnonymousClass1HH) r1).A02;
                synchronized (set) {
                    set.remove(A002);
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:96:0x0219
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:86)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public synchronized void A02(X.AnonymousClass1IA r19, java.lang.Runnable r20) {
        /*
        // Method dump skipped, instructions count: 559
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1HB.A02(X.1IA, java.lang.Runnable):void");
    }

    public synchronized void A03(AnonymousClass1IA r11, Runnable runnable, Thread thread) {
        AnonymousClass1HE r4 = this.A00;
        synchronized (r4) {
            AnonymousClass1IH A03 = r4.A03(r11);
            A03.A02.remove(runnable);
            A03.A03.put(runnable, Long.valueOf(SystemClock.uptimeMillis()));
            A03.A04.put(runnable, thread.getName());
        }
        String A00 = AnonymousClass1HE.A00(runnable);
        for (AnonymousClass1HG r5 : this.A01) {
            if (r5 instanceof AnonymousClass1HH) {
                AnonymousClass1HH r52 = (AnonymousClass1HH) r5;
                if (r52.A01()) {
                    AnonymousClass1II r3 = r52.A00;
                    synchronized (r3) {
                        if (r52.A01()) {
                            r3.A00(r52.A01, 300000);
                        }
                    }
                }
            } else if (r5 instanceof AnonymousClass1HI) {
                AnonymousClass1HI r53 = (AnonymousClass1HI) r5;
                AnonymousClass1IA A01 = ((AnonymousClass1HG) r53).A01.A01(A00);
                if (A01 != null) {
                    String str = A01.A00;
                    if ("WhatsApp Worker".equals(str)) {
                        Number number = (Number) r53.A00.remove(A00);
                        long uptimeMillis = SystemClock.uptimeMillis();
                        AtomicBoolean atomicBoolean = r53.A01;
                        if (!atomicBoolean.get() && number != null && uptimeMillis - number.longValue() > 10000) {
                            r53.A00(str, null);
                            atomicBoolean.set(true);
                        }
                    }
                }
            }
        }
    }
}
