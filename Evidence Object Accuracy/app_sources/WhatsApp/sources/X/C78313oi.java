package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78313oi extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99064jh();
    public final int A00;
    public final String A01;
    public final byte[] A02;

    public C78313oi(String str, byte[] bArr, int i) {
        this.A01 = str;
        this.A02 = bArr;
        this.A00 = i;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0G(parcel, this.A02, 3, C95654e8.A0K(parcel, this.A01));
        C95654e8.A07(parcel, 4, this.A00);
        C95654e8.A06(parcel, A00);
    }
}
