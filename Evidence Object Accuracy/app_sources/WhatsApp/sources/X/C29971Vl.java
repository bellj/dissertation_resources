package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Vl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29971Vl extends AbstractC14640lm {
    public static final C29971Vl A00 = new C29971Vl();
    public static final Parcelable.Creator CREATOR = new C99974lA();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "s.whatsapp.net";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 9;
    }

    public C29971Vl() {
        super("gdpr");
    }

    public C29971Vl(Parcel parcel) {
        super(parcel);
    }
}
