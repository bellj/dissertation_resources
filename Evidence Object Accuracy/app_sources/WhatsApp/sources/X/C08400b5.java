package X;

/* renamed from: X.0b5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08400b5 implements AbstractC12090hM {
    public final /* synthetic */ AnonymousClass0UA A00;
    public final /* synthetic */ AbstractC12090hM A01;
    public final /* synthetic */ AbstractC11560gU A02;

    public C08400b5(AnonymousClass0UA r1, AbstractC12090hM r2, AbstractC11560gU r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0042, code lost:
        if (r1.A00 == null) goto L_0x0044;
     */
    @Override // X.AbstractC12090hM
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AUv(int r5) {
        /*
            r4 = this;
            java.lang.String r3 = "Fetch summary is missing."
            X.0hM r0 = r4.A01     // Catch: all -> 0x0031
            if (r0 == 0) goto L_0x0009
            r0.AUv(r5)     // Catch: all -> 0x0031
        L_0x0009:
            X.0UA r1 = r4.A00
            java.util.concurrent.atomic.AtomicReference r0 = r1.A08
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x002b
            switch(r5) {
                case 3: goto L_0x0017;
                case 4: goto L_0x0016;
                case 5: goto L_0x0021;
                case 6: goto L_0x0016;
                case 7: goto L_0x0016;
                case 8: goto L_0x0016;
                case 9: goto L_0x001b;
                default: goto L_0x0016;
            }
        L_0x0016:
            return
        L_0x0017:
            X.0Tn r0 = r1.A00
            if (r0 != 0) goto L_0x0016
        L_0x001b:
            com.facebook.common.time.AwakeTimeSinceBootClock r0 = com.facebook.common.time.AwakeTimeSinceBootClock.INSTANCE
            r0.now()
            return
        L_0x0021:
            java.lang.Throwable r0 = r1.A04()
            if (r0 == 0) goto L_0x0016
            r0.getMessage()
            return
        L_0x002b:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r3)
            throw r0
        L_0x0031:
            r2 = move-exception
            X.0UA r1 = r4.A00
            java.util.concurrent.atomic.AtomicReference r0 = r1.A08
            java.lang.Object r0 = r0.get()
            if (r0 == 0) goto L_0x0054
            switch(r5) {
                case 3: goto L_0x0040;
                case 4: goto L_0x003f;
                case 5: goto L_0x004a;
                case 6: goto L_0x003f;
                case 7: goto L_0x003f;
                case 8: goto L_0x003f;
                case 9: goto L_0x0044;
                default: goto L_0x003f;
            }
        L_0x003f:
            throw r2
        L_0x0040:
            X.0Tn r0 = r1.A00
            if (r0 != 0) goto L_0x003f
        L_0x0044:
            com.facebook.common.time.AwakeTimeSinceBootClock r0 = com.facebook.common.time.AwakeTimeSinceBootClock.INSTANCE
            r0.now()
            throw r2
        L_0x004a:
            java.lang.Throwable r0 = r1.A04()
            if (r0 == 0) goto L_0x003f
            r0.getMessage()
            throw r2
        L_0x0054:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08400b5.AUv(int):void");
    }
}
