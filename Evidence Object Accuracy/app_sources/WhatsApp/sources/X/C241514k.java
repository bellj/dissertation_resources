package X;

import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.14k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241514k {
    public AbstractC248317b A00;
    public Map A01;
    public final C241414j A02;
    public final C22710zW A03;

    public C241514k(C241414j r1, C22710zW r2) {
        this.A02 = r1;
        this.A03 = r2;
    }

    public C47822Cw A00(UserJid userJid, String str) {
        AbstractC16830pp AGf = this.A00.AGf();
        AnonymousClass1ZO r0 = null;
        if (AGf == null) {
            return null;
        }
        Map map = this.A01;
        if (map != null) {
            r0 = (AnonymousClass1ZO) map.get(userJid);
        }
        return AGf.AHU(r0, userJid, str);
    }
}
