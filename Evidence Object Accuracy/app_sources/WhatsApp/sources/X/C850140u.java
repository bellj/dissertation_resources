package X;

import android.view.View;

/* renamed from: X.40u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C850140u extends AbstractC75663kD {
    public final AbstractC16710pd A00;
    public final AbstractC16710pd A01;
    public final AnonymousClass1J7 A02;

    public C850140u(View view, AnonymousClass1J7 r4) {
        super(view);
        this.A02 = r4;
        this.A01 = new AnonymousClass1WL(new AnonymousClass5JU(view));
        this.A00 = new AnonymousClass1WL(new AnonymousClass5JT(view));
    }
}
