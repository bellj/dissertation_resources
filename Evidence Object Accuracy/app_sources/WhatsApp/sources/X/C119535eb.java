package X;

import org.json.JSONObject;

/* renamed from: X.5eb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119535eb extends C130745zu {
    public final double A00;

    public C119535eb(AnonymousClass1V8 r4) {
        super(r4);
        String A0H = r4.A0H("scale");
        try {
            this.A00 = Double.parseDouble(A0H);
        } catch (NumberFormatException unused) {
            throw new AnonymousClass1V9(C12960it.A0d(A0H, C12960it.A0k("TextWithEntities/ScaleRange/invalid scale=")));
        }
    }

    public C119535eb(JSONObject jSONObject) {
        super(jSONObject);
        this.A00 = jSONObject.getDouble("scale");
    }

    @Override // X.C130745zu
    public JSONObject A01() {
        return super.A01().put("scale", this.A00);
    }
}
