package X;

import android.os.ConditionVariable;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.23R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23R implements AbstractC28461Nh {
    public final /* synthetic */ ConditionVariable A00;
    public final /* synthetic */ C22760zb A01;
    public final /* synthetic */ StringBuffer A02;

    public AnonymousClass23R(ConditionVariable conditionVariable, C22760zb r2, StringBuffer stringBuffer) {
        this.A01 = r2;
        this.A02 = stringBuffer;
        this.A00 = conditionVariable;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void AOt(long j) {
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        StringBuilder sb = new StringBuilder("debug-builder/uploadLogsInternal Error received: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            it.next();
        }
        List list = (List) map.get("X-Uploaded-File-Id");
        if (list != null && !list.isEmpty()) {
            this.A02.append((String) list.get(0));
        }
        this.A00.open();
    }
}
