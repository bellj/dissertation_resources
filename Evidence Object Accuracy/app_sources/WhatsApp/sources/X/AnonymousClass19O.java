package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape0S0700000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.19O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19O extends AbstractC18440sS {
    public static final BitmapFactory.Options A07;
    public int A00;
    public final Handler A01 = new Handler(Looper.getMainLooper());
    public final AnonymousClass1O4 A02;
    public final AnonymousClass160 A03;
    public final C41491tc A04;
    public final HashMap A05 = new HashMap();
    public final List A06 = new ArrayList();

    static {
        BitmapFactory.Options options = new BitmapFactory.Options();
        A07 = options;
        options.inInputShareable = true;
        options.inPurgeable = true;
        options.inDither = true;
    }

    public AnonymousClass19O(C14330lG r10, C18720su r11, C16590pI r12, AnonymousClass018 r13, AnonymousClass19M r14, C14410lO r15, AnonymousClass14A r16, AnonymousClass1EK r17, AnonymousClass160 r18, AnonymousClass1AB r19, C22590zK r20) {
        super(r16);
        this.A03 = r18;
        this.A04 = new C41491tc(r10, r12, r13, r14, r15, r17, r19, r20);
        StringBuilder sb = new StringBuilder("messagethumbcache/construct ");
        sb.append((int) ((AnonymousClass01V.A00 / 1024) / 8));
        Log.i(sb.toString());
        this.A02 = r11.A02();
        r11.A0L.add(new C41501td(this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00ef, code lost:
        if (r1 > 0.0f) goto L_0x00bb;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00f5 A[ORIG_RETURN, RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(X.AbstractC16130oV r4, int r5) {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19O.A00(X.0oV, int):int");
    }

    public static Bitmap A01(AbstractC15340mz r2, int i, boolean z, boolean z2) {
        byte[] bArr = null;
        if (!(r2 instanceof AnonymousClass1XH)) {
            if (r2 instanceof C28861Ph) {
                bArr = ((C28861Ph) r2).A17();
            } else if (r2.A0G() != null && r2.A0G().A05()) {
                bArr = r2.A0G().A07();
            }
        }
        return A02(r2, bArr, i, z, z2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
        if (r0 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
        if (r3.getWidth() < 100) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap A02(X.AbstractC15340mz r2, byte[] r3, int r4, boolean r5, boolean r6) {
        /*
            boolean r0 = r2 instanceof X.AnonymousClass1XH
            r1 = 0
            if (r0 != 0) goto L_0x0045
            if (r3 == 0) goto L_0x0045
            android.graphics.BitmapFactory$Options r0 = X.AnonymousClass19O.A07
            android.graphics.Bitmap r3 = X.C41511te.A00(r0, r3, r4)
            if (r3 == 0) goto L_0x0045
            if (r5 == 0) goto L_0x0022
            boolean r0 = r2 instanceof X.C28861Ph
            if (r0 == 0) goto L_0x0023
            r0 = 4
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x001a:
            int r1 = r0.intValue()
            r0 = 2
            com.whatsapp.filter.FilterUtils.blurNative(r3, r1, r0)
        L_0x0022:
            return r3
        L_0x0023:
            boolean r0 = r2 instanceof X.AbstractC16130oV
            if (r0 == 0) goto L_0x0022
            boolean r0 = r2 instanceof X.C16440p1
            r2 = 1
            if (r0 != 0) goto L_0x0036
            if (r6 == 0) goto L_0x002f
            r2 = 2
        L_0x002f:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r2)
            if (r0 == 0) goto L_0x0022
            goto L_0x001a
        L_0x0036:
            int r0 = r3.getHeight()
            r1 = 100
            if (r0 >= r1) goto L_0x0022
            int r0 = r3.getWidth()
            if (r0 >= r1) goto L_0x0022
            goto L_0x002f
        L_0x0045:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19O.A02(X.0mz, byte[], int, boolean, boolean):android.graphics.Bitmap");
    }

    public int A03(Context context) {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.message_thumb_cache_default_thumb_density_size);
        this.A00 = dimensionPixelSize;
        return dimensionPixelSize;
    }

    public final synchronized Bitmap A04(String str) {
        Bitmap bitmap;
        bitmap = (Bitmap) this.A02.A00(str);
        if (bitmap == null || bitmap.isRecycled()) {
            if (bitmap != null && bitmap.isRecycled()) {
                Log.e("!! recycled message in hard cache");
            }
            HashMap hashMap = this.A05;
            SoftReference softReference = (SoftReference) hashMap.get(str);
            bitmap = softReference != null ? (Bitmap) softReference.get() : null;
            if (bitmap == null || bitmap.isRecycled()) {
                if (softReference != null) {
                    hashMap.remove(str);
                }
                bitmap = null;
            }
        }
        return bitmap;
    }

    public void A05() {
        AnonymousClass009.A01();
        List<Runnable> list = this.A06;
        synchronized (list) {
            for (Runnable runnable : list) {
                runnable.run();
            }
            list.clear();
        }
    }

    public final void A06(View view, AbstractC15340mz r13, AbstractC41531tg r14, AbstractC41521tf r15, Object obj) {
        String str;
        Bitmap A04;
        synchronized (this) {
            AnonymousClass1IS r0 = r13.A0z;
            if (r0 == null || (str = r0.A01) == null) {
                str = "null";
            }
            A04 = A04(str);
        }
        if (A04 != null) {
            if (r14 instanceof C41541th) {
                C41541th r5 = (C41541th) r14;
                int AGm = (int) (((float) r5.A03.AGm()) / r5.A01.getResources().getDisplayMetrics().density);
                if (AGm > A04.getWidth() || AGm == 0) {
                    r5.A01();
                }
            }
            r15.Adg(A04, view, r13);
            return;
        }
        Bitmap A00 = r14.A00();
        C16460p3 A0G = r13.A0G();
        if (A00 != null || A0G == null || A0G.A05()) {
            r15.Adg(A00, view, r13);
            r14.A01();
            return;
        }
        this.A03.A02(A0G, new RunnableBRunnable0Shape0S0700000_I0(this, A0G, r14, obj, view, r15, r13, 1));
        r15.Adu(view);
    }

    public void A07(View view, AbstractC15340mz r10, AbstractC41521tf r11) {
        A0A(view, r10, r11, r10.A0z, 100, false, false);
    }

    public void A08(View view, AbstractC15340mz r3, AbstractC41521tf r4) {
        A09(view, r3, r4, r3.A0z);
    }

    public void A09(View view, AbstractC15340mz r9, AbstractC41521tf r10, Object obj) {
        view.setTag(obj);
        if (AnonymousClass01I.A01()) {
            A05();
        }
        if ((r9 instanceof AnonymousClass1XH) || (!(r9 instanceof C28861Ph) ? r9.A0G() == null || !r9.A0G().A04() : ((C28861Ph) r9).A17() == null)) {
            r10.Adg(null, view, r9);
        } else {
            A06(view, r9, new C41551ti(r9, this), r10, obj);
        }
    }

    public final synchronized void A0A(View view, AbstractC15340mz r14, AbstractC41521tf r15, Object obj, int i, boolean z, boolean z2) {
        view.setTag(obj);
        if (AnonymousClass01I.A01()) {
            A05();
        }
        A06(view, r14, new C41541th(view, r14, r15, this, obj, i, z2, z), r15, obj);
    }

    public void A0B(View view, AbstractC15340mz r12, AbstractC41521tf r13, Object obj, boolean z) {
        view.setTag(obj);
        this.A04.A01(view, r12, new C41561tj(view, r12, r13, this, obj), r13, obj, z);
    }

    public void A0C(View view, AbstractC15340mz r8, AbstractC41521tf r9, boolean z) {
        C32361c2 r3;
        String str;
        view.setTag(r8.A0N.A07);
        synchronized (this) {
            if (AnonymousClass01I.A01()) {
                A05();
            }
            C32361c2 r0 = r8.A0N;
            if (r0 != null) {
                byte[] bArr = r0.A00;
                if (!TextUtils.isEmpty(r0.A03)) {
                    r3 = r8.A0N;
                    str = r3.A03;
                } else {
                    r3 = r8.A0N;
                    str = r3.A07;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("_");
                sb.append(z);
                String obj = sb.toString();
                if (bArr == null) {
                    bArr = r3.A0C;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(obj);
                    sb2.append("_micro");
                    obj = sb2.toString();
                }
                Bitmap A04 = A04(obj);
                if (A04 == null) {
                    A04 = A02(r8, bArr, 2000, z, false);
                    this.A05.remove(obj);
                    if (A04 != null) {
                        this.A02.A03(obj, A04);
                    }
                }
                r9.Adg(A04, view, r8);
            }
        }
    }

    public void A0D(AbstractC15340mz r4) {
        String str;
        AnonymousClass1O4 r2 = this.A02;
        AnonymousClass1IS r0 = r4.A0z;
        if (r0 == null || (str = r0.A01) == null) {
            str = "null";
        }
        r2.A00.A07(str);
        this.A05.remove(str);
    }
}
