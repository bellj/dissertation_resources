package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.22G  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass22G extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass22G A08;
    public static volatile AnonymousClass255 A09;
    public int A00;
    public int A01 = 0;
    public int A02;
    public AnonymousClass1K6 A03 = AnonymousClass277.A01;
    public Object A04;
    public String A05 = "";
    public String A06 = "";
    public String A07 = "";

    static {
        AnonymousClass22G r0 = new AnonymousClass22G();
        A08 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ba, code lost:
        if (r10.A01 == 1) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00bf, code lost:
        if (r10.A01 == 6) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00c1, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00c2, code lost:
        r10.A04 = r12.Afv(r10.A04, r13.A04, r5);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r11, java.lang.Object r12, java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 534
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass22G.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if (this.A01 == 1) {
            i = CodedOutputStream.A0A((AnonymousClass1G0) this.A04, 1) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 1) == 1) {
            i += CodedOutputStream.A07(2, this.A07);
        }
        if ((this.A00 & 2) == 2) {
            i += CodedOutputStream.A04(3, this.A02);
        }
        for (int i3 = 0; i3 < this.A03.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A03.get(i3), 4);
        }
        if ((this.A00 & 4) == 4) {
            i += CodedOutputStream.A07(5, this.A05);
        }
        if (this.A01 == 6) {
            i += CodedOutputStream.A0A((AnonymousClass1G0) this.A04, 6);
        }
        if ((this.A00 & 32) == 32) {
            i += CodedOutputStream.A07(7, this.A06);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A04, 1);
        }
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(2, this.A07);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(3, this.A02);
        }
        for (int i = 0; i < this.A03.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A03.get(i), 4);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(5, this.A05);
        }
        if (this.A01 == 6) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A04, 6);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(7, this.A06);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
