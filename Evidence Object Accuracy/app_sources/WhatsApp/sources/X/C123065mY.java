package X;

import com.whatsapp.R;
import java.util.AbstractCollection;

/* renamed from: X.5mY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123065mY extends AbstractC125975s7 {
    public final String A00;

    public C123065mY(String str) {
        super(1006);
        this.A00 = str;
    }

    public static void A00(C129835yN r2, AbstractCollection abstractCollection) {
        abstractCollection.add(new C123065mY(r2.A00.getString(R.string.novi_transaction_breakdown_title)));
    }
}
