package X;

/* renamed from: X.4CJ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CJ extends Exception {
    public AnonymousClass4CJ(String str) {
        super(str);
    }

    public AnonymousClass4CJ(String str, Throwable th) {
        super(str, th);
    }

    public AnonymousClass4CJ(Throwable th) {
        super(th);
    }
}
