package X;

import android.app.Application;

/* renamed from: X.4JX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4JX {
    public final int A00;

    public AnonymousClass4JX(Application application, int i) {
        int i2;
        if (application.getResources() != null) {
            i2 = application.getResources().getDimensionPixelSize(i);
        } else {
            i2 = 0;
        }
        this.A00 = i2;
    }
}
