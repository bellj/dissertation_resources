package X;

import java.util.Arrays;

/* renamed from: X.2V7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2V7 {
    public int A00;
    public final int A01;
    public final C15370n3 A02;
    public final C15370n3 A03;
    public final C15580nU A04;
    public final boolean A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;

    public AnonymousClass2V7(C15370n3 r1, C15370n3 r2, C15580nU r3, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9) {
        this.A02 = r1;
        this.A04 = r3;
        this.A03 = r2;
        this.A05 = z;
        this.A06 = z2;
        this.A0C = z3;
        this.A0D = z4;
        this.A0B = z5;
        this.A08 = z6;
        this.A09 = z7;
        this.A0A = z8;
        this.A01 = i;
        this.A07 = z9;
        this.A00 = i2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass2V7 r5 = (AnonymousClass2V7) obj;
            if (!(this.A05 == r5.A05 && this.A06 == r5.A06 && this.A0C == r5.A0C && this.A0D == r5.A0D && this.A0B == r5.A0B && this.A08 == r5.A08 && this.A09 == r5.A09 && this.A0A == r5.A0A && this.A01 == r5.A01 && this.A02.equals(r5.A02) && C29941Vi.A00(this.A04, r5.A04) && C29941Vi.A00(this.A03, r5.A03) && this.A07 == r5.A07)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A02, this.A04, this.A03, Boolean.valueOf(this.A05), Boolean.valueOf(this.A06), Boolean.valueOf(this.A0C), Boolean.valueOf(this.A0D), Boolean.valueOf(this.A0B), Boolean.valueOf(this.A08), false, Boolean.valueOf(this.A09), Boolean.valueOf(this.A0A), Integer.valueOf(this.A01), Boolean.valueOf(this.A07)});
    }
}
