package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4ky  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99854ky implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C38171nd(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C38171nd[i];
    }
}
