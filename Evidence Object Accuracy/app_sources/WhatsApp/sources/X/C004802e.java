package X;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.appcompat.app.AlertController$RecycleListView;

/* renamed from: X.02e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C004802e {
    public final int A00;
    public final AnonymousClass0OC A01;

    public C004802e(Context context) {
        this(context, AnonymousClass04S.A00(context, 0));
    }

    public C004802e(Context context, int i) {
        this.A01 = new AnonymousClass0OC(new ContextThemeWrapper(context, AnonymousClass04S.A00(context, i)));
        this.A00 = i;
    }

    public C004802e A00(int i, DialogInterface.OnClickListener onClickListener) {
        AnonymousClass0OC r1 = this.A01;
        r1.A0G = r1.A0O.getText(i);
        r1.A04 = onClickListener;
        return this;
    }

    public C004802e A01(DialogInterface.OnClickListener onClickListener, CharSequence charSequence) {
        AnonymousClass0OC r0 = this.A01;
        r0.A0F = charSequence;
        r0.A03 = onClickListener;
        return this;
    }

    public C004802e A02(DialogInterface.OnClickListener onClickListener, CharSequence charSequence) {
        AnonymousClass0OC r0 = this.A01;
        r0.A0G = charSequence;
        r0.A04 = onClickListener;
        return this;
    }

    public C004802e A03(DialogInterface.OnClickListener onClickListener, CharSequence charSequence) {
        AnonymousClass0OC r0 = this.A01;
        r0.A0H = charSequence;
        r0.A06 = onClickListener;
        return this;
    }

    public C004802e A04(DialogInterface.OnDismissListener onDismissListener) {
        this.A01.A07 = onDismissListener;
        return this;
    }

    public AnonymousClass04S A05() {
        AnonymousClass04S create = create();
        create.show();
        return create;
    }

    public void A06(int i) {
        AnonymousClass0OC r1 = this.A01;
        r1.A0E = r1.A0O.getText(i);
    }

    public void A07(int i) {
        AnonymousClass0OC r1 = this.A01;
        r1.A0I = r1.A0O.getText(i);
    }

    public void A08(DialogInterface.OnCancelListener onCancelListener) {
        this.A01.A02 = onCancelListener;
    }

    public void A09(DialogInterface.OnClickListener onClickListener, CharSequence[] charSequenceArr, int i) {
        AnonymousClass0OC r1 = this.A01;
        r1.A0M = charSequenceArr;
        r1.A05 = onClickListener;
        r1.A00 = i;
        r1.A0L = true;
    }

    public void A0A(CharSequence charSequence) {
        this.A01.A0E = charSequence;
    }

    public void A0B(boolean z) {
        this.A01.A0J = z;
    }

    public AnonymousClass04S create() {
        int i;
        ListAdapter listAdapter;
        AnonymousClass0OC r7 = this.A01;
        Context context = r7.A0O;
        AnonymousClass04S r4 = new AnonymousClass04S(context, this.A00);
        AnonymousClass0U5 r3 = r4.A00;
        View view = r7.A0B;
        if (view != null) {
            r3.A0C = view;
        } else {
            CharSequence charSequence = r7.A0I;
            if (charSequence != null) {
                r3.A0R = charSequence;
                TextView textView = r3.A0L;
                if (textView != null) {
                    textView.setText(charSequence);
                }
            }
            Drawable drawable = r7.A0A;
            if (drawable != null) {
                r3.A07 = drawable;
                ImageView imageView = r3.A0H;
                if (imageView != null) {
                    imageView.setVisibility(0);
                    r3.A0H.setImageDrawable(drawable);
                }
            }
        }
        CharSequence charSequence2 = r7.A0E;
        if (charSequence2 != null) {
            r3.A0Q = charSequence2;
            TextView textView2 = r3.A0K;
            if (textView2 != null) {
                textView2.setText(charSequence2);
            }
        }
        CharSequence charSequence3 = r7.A0H;
        if (charSequence3 != null) {
            DialogInterface.OnClickListener onClickListener = r7.A06;
            Message message = null;
            if (onClickListener != null) {
                message = r3.A08.obtainMessage(-1, onClickListener);
            }
            r3.A0P = charSequence3;
            r3.A0B = message;
        }
        CharSequence charSequence4 = r7.A0F;
        if (charSequence4 != null) {
            DialogInterface.OnClickListener onClickListener2 = r7.A03;
            Message message2 = null;
            if (onClickListener2 != null) {
                message2 = r3.A08.obtainMessage(-2, onClickListener2);
            }
            r3.A0N = charSequence4;
            r3.A09 = message2;
        }
        CharSequence charSequence5 = r7.A0G;
        if (charSequence5 != null) {
            DialogInterface.OnClickListener onClickListener3 = r7.A04;
            Message message3 = null;
            if (onClickListener3 != null) {
                message3 = r3.A08.obtainMessage(-3, onClickListener3);
            }
            r3.A0O = charSequence5;
            r3.A0A = message3;
        }
        if (!(r7.A0M == null && r7.A0D == null)) {
            AlertController$RecycleListView alertController$RecycleListView = (AlertController$RecycleListView) r7.A0P.inflate(r3.A03, (ViewGroup) null);
            if (r7.A0K) {
                listAdapter = new AnonymousClass0BN(context, r7, alertController$RecycleListView, r7.A0M, r3.A04);
            } else {
                if (r7.A0L) {
                    i = r3.A05;
                } else {
                    i = r3.A02;
                }
                listAdapter = r7.A0D;
                if (listAdapter == null) {
                    listAdapter = new AnonymousClass0BO(context, r7.A0M, i);
                }
            }
            r3.A0I = listAdapter;
            r3.A01 = r7.A00;
            if (r7.A05 != null) {
                alertController$RecycleListView.setOnItemClickListener(new C07130Wu(r7, r3));
            } else if (r7.A09 != null) {
                alertController$RecycleListView.setOnItemClickListener(new C07150Ww(r7, alertController$RecycleListView, r3));
            }
            if (r7.A0L) {
                alertController$RecycleListView.setChoiceMode(1);
            } else if (r7.A0K) {
                alertController$RecycleListView.setChoiceMode(2);
            }
            r3.A0J = alertController$RecycleListView;
        }
        View view2 = r7.A0C;
        if (view2 != null) {
            r3.A0D = view2;
            r3.A06 = 0;
        } else {
            int i2 = r7.A01;
            if (i2 != 0) {
                r3.A0D = null;
                r3.A06 = i2;
            }
        }
        r4.setCancelable(r7.A0J);
        if (r7.A0J) {
            r4.setCanceledOnTouchOutside(true);
        }
        r4.setOnCancelListener(r7.A02);
        r4.setOnDismissListener(r7.A07);
        DialogInterface.OnKeyListener onKeyListener = r7.A08;
        if (onKeyListener != null) {
            r4.setOnKeyListener(onKeyListener);
        }
        return r4;
    }

    public Context getContext() {
        return this.A01.A0O;
    }

    public C004802e setNegativeButton(int i, DialogInterface.OnClickListener onClickListener) {
        AnonymousClass0OC r1 = this.A01;
        r1.A0F = r1.A0O.getText(i);
        r1.A03 = onClickListener;
        return this;
    }

    public C004802e setPositiveButton(int i, DialogInterface.OnClickListener onClickListener) {
        AnonymousClass0OC r1 = this.A01;
        r1.A0H = r1.A0O.getText(i);
        r1.A06 = onClickListener;
        return this;
    }

    public C004802e setTitle(CharSequence charSequence) {
        this.A01.A0I = charSequence;
        return this;
    }

    public C004802e setView(View view) {
        AnonymousClass0OC r1 = this.A01;
        r1.A0C = view;
        r1.A01 = 0;
        return this;
    }
}
