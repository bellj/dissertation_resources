package X;

import android.os.IBinder;

/* renamed from: X.3rC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79783rC extends C65873Li implements AnonymousClass5YC {
    public C79783rC(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IMarkerDelegate");
    }
}
