package X;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Handler;
import android.view.View;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3Tp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67973Tp implements AbstractC17450qp {
    public final C67963To A00 = new C67963To();

    public static Activity A00(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return A00(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    public static View A01(C14220l3 r2) {
        C89054Im r0 = ((AnonymousClass28D) r2.A00.get(0)).A03;
        if (r0 == null) {
            return null;
        }
        return r0.A00;
    }

    @Deprecated
    public static C14260l7 A02(C14230l4 r2, C14220l3 r3, int i) {
        List list = r3.A00;
        if (i < list.size()) {
            Object obj = list.get(i);
            if (obj instanceof C14260l7) {
                return (C14260l7) obj;
            }
        }
        return r2.A00;
    }

    public static AnonymousClass3JI A03(C14230l4 r10, AnonymousClass28D r11, String str, List list, List list2, List list3, List list4, List list5, boolean z) {
        List list6 = list5;
        AnonymousClass28D r4 = r11;
        List list7 = list4;
        ArrayList A0l = C12960it.A0l();
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                String A0x = C12970iu.A0x(it);
                Object obj = A06(r10).A02.get(A0x);
                if (obj != null) {
                    A0l.add(obj);
                } else {
                    StringBuilder A0k = C12960it.A0k("Data Manifest for referenced internal variable id ");
                    A0k.append(A0x);
                    throw C12970iu.A0f(C12960it.A0d("\n\nYou are running parseEmbedded without the parent Bloks Context needed to process the data manifests. If you are implementing a new feature, check to makesure you're not calling evaluateWithoutTreeDANGEROUSLY on your signature. Otherwise, this is an infra error that you should post in the Bloks Q&A group about.", C12960it.A0j(C12960it.A0d(" not found! ", A0k))));
                }
            }
        }
        if (list2 != null) {
            Iterator it2 = list2.iterator();
            while (it2.hasNext()) {
                String A0x2 = C12970iu.A0x(it2);
                A0l.add(new C93484aF(A0x2, Collections.singletonMap("initial", A0A(r10, A0x2))));
            }
        }
        HashMap A11 = C12970iu.A11();
        if (list3 != null) {
            Iterator it3 = list3.iterator();
            while (it3.hasNext()) {
                String A0x3 = C12970iu.A0x(it3);
                A11.put(A0x3, A07(r10, A0x3));
            }
        }
        if (z && r11 != null) {
            r4 = AnonymousClass3AD.A00(new C1092651a(r11), r11);
        }
        if (list5 == null) {
            list6 = Collections.EMPTY_LIST;
        }
        if (list4 == null) {
            list7 = Collections.EMPTY_LIST;
        }
        return new AnonymousClass3JI(r4, str, A0l, list6, list7, A11);
    }

    public static AnonymousClass3JI A04(C14230l4 r8, C63433Bo r9, boolean z) {
        String str;
        AnonymousClass28D r1 = r9.A00;
        List list = r9.A09;
        List list2 = r9.A08;
        List list3 = r9.A07;
        List list4 = r9.A06;
        List list5 = r9.A03;
        C89064In r0 = r9.A01;
        if (r0 != null) {
            str = r0.A00;
        } else {
            str = null;
        }
        return A03(r8, r1, str, list, list2, list3, list4, list5, z);
    }

    public static C14270l8 A05(C14230l4 r0, C14220l3 r1, int i) {
        return AnonymousClass3JV.A03(A02(r0, r1, i));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        if (r4 != null) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C94244bU A06(X.C14230l4 r5) {
        /*
            X.4bU r4 = r5.A01
            if (r4 != 0) goto L_0x0019
            X.0l7 r0 = r5.A00
            if (r0 == 0) goto L_0x0026
            X.0l8 r1 = X.AnonymousClass3JV.A03(r0)
            java.lang.String r0 = "Tree resources can only be read from the UI Thread"
            X.AnonymousClass3J3.A02(r0)
            X.4bU r4 = r1.A04
            java.util.Map r3 = r1.A08
            if (r3 != 0) goto L_0x001a
            if (r4 == 0) goto L_0x0026
        L_0x0019:
            return r4
        L_0x001a:
            java.util.Map r2 = r4.A02
            java.util.Map r1 = r4.A01
            java.util.Map r0 = r4.A00
            X.4bU r4 = new X.4bU
            r4.<init>(r2, r1, r3, r0)
            return r4
        L_0x0026:
            java.lang.String r0 = "No tree resources available in the Scripting Environment. This is an infra error that you should post in the Bloks Q&A group about."
            java.lang.NullPointerException r0 = X.C12980iv.A0n(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67973Tp.A06(X.0l4):X.4bU");
    }

    public static C64573Gb A07(C14230l4 r1, String str) {
        C64573Gb r0 = (C64573Gb) A06(r1).A01.get(str);
        if (r0 != null) {
            return r0;
        }
        StringBuilder A0k = C12960it.A0k("Payload for referenced embedded payload id ");
        A0k.append(str);
        throw C12970iu.A0f(C12960it.A0d("\n\nYou are running parseEmbedded without the parent Bloks Context needed to process the data manifests. If you are implementing a new feature, check to makesure you're not calling evaluateWithoutTreeDANGEROUSLY on your signature. Otherwise, this is an infra error that you should post in the Bloks Q&A group about.", C12960it.A0j(C12960it.A0d(" not found!", A0k))));
    }

    public static Number A08(String str, float f) {
        if (!"px".equalsIgnoreCase(str)) {
            if ("dp".equalsIgnoreCase(str)) {
                f /= C12960it.A01(C65093Ic.A00().A00);
            } else {
                C28691Op.A00("BloksCoreInterpreterExtensions", C12960it.A0d(str, C12960it.A0k("Unrecognised unit string ")));
                return null;
            }
        }
        return C64983Hr.A00((double) f);
    }

    public static Object A09(C14230l4 r2, C14220l3 r3, List list, int i) {
        return AnonymousClass3JV.A04(A02(r2, r3, i), (AnonymousClass28D) list.get(0));
    }

    public static Object A0A(C14230l4 r2, Object obj) {
        Object obj2 = A06(r2).A03.get(obj);
        Set set = r2.A04;
        if (set != null) {
            set.add(obj);
        }
        return obj2;
    }

    public static Object A0B(C14230l4 r2, String str) {
        Map map = r2.A03;
        if (map == null) {
            C14270l8 A03 = AnonymousClass3JV.A03(r2.A00);
            if (AnonymousClass3J3.A03()) {
                AnonymousClass4TT r0 = A03.A02;
                if (r0 == null) {
                    map = Collections.emptyMap();
                } else {
                    map = r0.A05;
                }
            } else {
                throw C12990iw.A0m("Expanded Variables can only be read from the UI Thread");
            }
        }
        Object obj = map.get(str);
        Set set = r2.A04;
        if (set != null) {
            set.add(str);
        }
        return obj;
    }

    public static String A0C(AnonymousClass49M r4, String str, List list, int i) {
        List subList = list.subList(0, i);
        char c = '|';
        if (r4 == AnonymousClass49M.A01) {
            c = '/';
        }
        StringBuilder A0h = C12960it.A0h();
        Iterator it = subList.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            A0h.append(c);
            A0h.append(A0x);
        }
        String obj = A0h.toString();
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append("#");
        return C12960it.A0d(obj, A0j);
    }

    public static List A0D(C14270l8 r5, List list) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass3JI r2 = (AnonymousClass3JI) it.next();
            C93634aU r1 = r2.A00;
            if (r5.A09) {
                C28691Op.A00("BloksTreeManager", "Trying to enqueue mutations on a destroyed BloksTreeManager");
            } else {
                AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
                r5.A0J.add(r1);
                Handler handler = C14270l8.A0L;
                Runnable runnable = r5.A0F;
                handler.removeCallbacks(runnable);
                handler.post(runnable);
            }
            A0l.add(r2.A01);
        }
        return A0l;
    }

    public static void A0E(C14270l8 r2, String str, List list, int i) {
        r2.A05(new C67933Tl(str), new C57972nv(list, i, str));
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:567:0x08f8 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v0 */
    /* JADX WARN: Type inference failed for: r5v2, types: [X.51l] */
    /* JADX WARN: Type inference failed for: r5v9 */
    /* JADX WARN: Type inference failed for: r1v32, types: [java.lang.Throwable, java.io.IOException] */
    /* JADX WARN: Type inference failed for: r5v15, types: [X.0l1] */
    /* JADX WARN: Type inference failed for: r1v54, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v544, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v30 */
    /* JADX WARN: Type inference failed for: r5v31 */
    /* JADX WARN: Type inference failed for: r5v32 */
    /* JADX WARN: Type inference failed for: r5v33 */
    /* JADX WARN: Type inference failed for: r5v34 */
    /* JADX WARN: Type inference failed for: r5v35 */
    /* JADX WARN: Type inference failed for: r5v36 */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0044, code lost:
        if (r9.equals(r0) == false) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x06f4, code lost:
        if (r0 == false) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:374:0x0991, code lost:
        if (r0.requestFocus() != false) goto L_0x0993;
     */
    /* JADX WARNING: Unknown variable types count: 3 */
    @Override // X.AbstractC17450qp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A9j(X.C14220l3 r18, X.C1093651k r19, X.C14240l5 r20) {
        /*
        // Method dump skipped, instructions count: 4216
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67973Tp.A9j(X.0l3, X.51k, X.0l5):java.lang.Object");
    }
}
