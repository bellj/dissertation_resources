package X;

import java.util.Enumeration;

/* renamed from: X.5N3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N3 extends AnonymousClass1TM implements AnonymousClass1TJ {
    public AnonymousClass1TN A00;
    public AnonymousClass1TK A01;
    public boolean A02 = true;

    public AnonymousClass5N3(AnonymousClass1TN r2, AnonymousClass1TK r3) {
        this.A01 = r3;
        this.A00 = r2;
    }

    public AnonymousClass5N3(AbstractC114775Na r3) {
        Enumeration A0C = r3.A0C();
        this.A01 = (AnonymousClass1TK) A0C.nextElement();
        if (A0C.hasMoreElements()) {
            this.A00 = AnonymousClass5NU.A00((AnonymousClass5NU) A0C.nextElement());
        }
        this.A02 = r3 instanceof AnonymousClass5NX;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A01);
        AnonymousClass1TN r3 = this.A00;
        if (r3 != null) {
            A00.A06(new C114815Ne(r3, 0, true));
        }
        return this.A02 ? new AnonymousClass5NX(A00) : new AnonymousClass5NY(A00);
    }
}
