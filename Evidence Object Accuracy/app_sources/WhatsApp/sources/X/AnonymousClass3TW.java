package X;

import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.whatsapp.biz.catalog.view.activity.CatalogCategoryTabsActivity;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryTabsViewModel;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.3TW  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3TW implements AbstractC467727m {
    public final /* synthetic */ TabLayout A00;
    public final /* synthetic */ CatalogCategoryTabsActivity A01;
    public final /* synthetic */ List A02;

    @Override // X.AbstractC467727m
    public void AXN(AnonymousClass3FN r1) {
    }

    public AnonymousClass3TW(TabLayout tabLayout, CatalogCategoryTabsActivity catalogCategoryTabsActivity, List list) {
        this.A02 = list;
        this.A00 = tabLayout;
        this.A01 = catalogCategoryTabsActivity;
    }

    @Override // X.AbstractC467727m
    public void AXO(AnonymousClass3FN r9) {
        List list = this.A02;
        TabLayout tabLayout = this.A00;
        CatalogCategoryTabsActivity catalogCategoryTabsActivity = this.A01;
        AnonymousClass3F7 r2 = (AnonymousClass3F7) list.get(tabLayout.getSelectedTabPosition());
        ViewPager viewPager = catalogCategoryTabsActivity.A00;
        if (viewPager == null) {
            throw C16700pc.A06("viewPager");
        }
        viewPager.A0F(tabLayout.getSelectedTabPosition(), false);
        String str = r2.A01;
        UserJid userJid = r2.A00;
        ((CatalogCategoryTabsViewModel) catalogCategoryTabsActivity.A03.getValue()).A01.A01(userJid, str, C16700pc.A0N(str, userJid) ? 1 : 0, 3, tabLayout.getSelectedTabPosition(), false);
    }
}
