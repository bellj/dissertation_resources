package X;

/* renamed from: X.1uy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC42291uy {
    REGISTRATION(0),
    INTERACTIVE(1),
    BACKGROUND(2),
    NOTIFICATION(3),
    MESSAGE(4),
    ADD(5),
    VOIP(6),
    CONTACT_DISCOVERY(7);
    
    public final String contextString;

    EnumC42291uy(int i) {
        this.contextString = r2;
    }
}
