package X;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;

/* renamed from: X.3BM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BM {
    public final Intent A00;
    public final Drawable A01;
    public final CharSequence A02;

    public AnonymousClass3BM(Intent intent, ActivityInfo activityInfo, Drawable drawable, CharSequence charSequence) {
        C16700pc.A0E(intent, 1);
        Intent intent2 = new Intent(intent);
        intent2.setComponent(new ComponentName(activityInfo.packageName, activityInfo.name));
        this.A00 = intent2;
        this.A01 = drawable;
        this.A02 = charSequence;
    }
}
