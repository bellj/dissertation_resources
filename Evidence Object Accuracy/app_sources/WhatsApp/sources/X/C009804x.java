package X;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.04x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C009804x extends AbstractC009904y {
    public int A00 = 0;
    public AnonymousClass05N A01 = new AnonymousClass05N();
    public AnonymousClass05I A02;
    public ArrayList A03 = new ArrayList();
    public boolean A04 = false;
    public boolean A05 = false;
    public final WeakReference A06;
    public final boolean A07;

    public C009804x(AbstractC001200n r3) {
        this.A06 = new WeakReference(r3);
        this.A02 = AnonymousClass05I.INITIALIZED;
        this.A07 = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        if (r8.A04 != false) goto L_0x002e;
     */
    @Override // X.AbstractC009904y
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass03H r9) {
        /*
            r8 = this;
            java.lang.String r0 = "addObserver"
            r8.A06(r0)
            X.05I r1 = r8.A02
            X.05I r0 = X.AnonymousClass05I.DESTROYED
            if (r1 == r0) goto L_0x000d
            X.05I r0 = X.AnonymousClass05I.INITIALIZED
        L_0x000d:
            X.05T r5 = new X.05T
            r5.<init>(r0, r9)
            X.05N r4 = r8.A01
            java.lang.Object r0 = r4.A02(r9, r5)
            if (r0 != 0) goto L_0x0084
            java.lang.ref.WeakReference r0 = r8.A06
            java.lang.Object r3 = r0.get()
            X.00n r3 = (X.AbstractC001200n) r3
            if (r3 == 0) goto L_0x0084
            int r0 = r8.A00
            r7 = 1
            if (r0 != 0) goto L_0x002e
            boolean r0 = r8.A04
            r6 = 0
            if (r0 == 0) goto L_0x002f
        L_0x002e:
            r6 = 1
        L_0x002f:
            X.05I r1 = r8.A02(r9)
            int r0 = r8.A00
            int r0 = r0 + r7
            r8.A00 = r0
        L_0x0038:
            X.05I r0 = r5.A00
            int r0 = r0.compareTo(r1)
            if (r0 >= 0) goto L_0x007a
            java.util.HashMap r0 = r4.A00
            boolean r0 = r0.containsKey(r9)
            if (r0 == 0) goto L_0x007a
            X.05I r2 = r5.A00
            java.util.ArrayList r1 = r8.A03
            r1.add(r2)
            X.074 r0 = X.AnonymousClass074.A00(r2)
            if (r0 == 0) goto L_0x0066
            r5.A00(r0, r3)
            int r0 = r1.size()
            int r0 = r0 + -1
            r1.remove(r0)
            X.05I r1 = r8.A02(r9)
            goto L_0x0038
        L_0x0066:
            java.lang.String r1 = "no event up from "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x007a:
            if (r6 != 0) goto L_0x007f
            r8.A03()
        L_0x007f:
            int r0 = r8.A00
            int r0 = r0 - r7
            r8.A00 = r0
        L_0x0084:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C009804x.A00(X.03H):void");
    }

    @Override // X.AbstractC009904y
    public void A01(AnonymousClass03H r2) {
        A06("removeObserver");
        this.A01.A01(r2);
    }

    public final AnonymousClass05I A02(AnonymousClass03H r5) {
        AnonymousClass05U r0;
        AnonymousClass05I r3;
        HashMap hashMap = this.A01.A00;
        if (hashMap.containsKey(r5)) {
            r0 = ((AnonymousClass05U) hashMap.get(r5)).A01;
        } else {
            r0 = null;
        }
        AnonymousClass05I r2 = null;
        if (r0 != null) {
            r3 = ((AnonymousClass05T) r0.getValue()).A00;
        } else {
            r3 = null;
        }
        ArrayList arrayList = this.A03;
        if (!arrayList.isEmpty()) {
            r2 = (AnonymousClass05I) arrayList.get(arrayList.size() - 1);
        }
        AnonymousClass05I r1 = this.A02;
        if (r3 != null && r3.compareTo(r1) < 0) {
            r1 = r3;
        }
        if (r2 == null || r2.compareTo(r1) >= 0) {
            return r1;
        }
        return r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:77:0x005a, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00e9, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03() {
        /*
        // Method dump skipped, instructions count: 342
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C009804x.A03():void");
    }

    public void A04(AnonymousClass074 r2) {
        A06("handleLifecycleEvent");
        A05(r2.A01());
    }

    public final void A05(AnonymousClass05I r3) {
        if (this.A02 != r3) {
            this.A02 = r3;
            if (this.A04 || this.A00 != 0) {
                this.A05 = true;
                return;
            }
            this.A04 = true;
            A03();
            this.A04 = false;
        }
    }

    public final void A06(String str) {
        if (this.A07 && !AnonymousClass05O.A00().A03()) {
            StringBuilder sb = new StringBuilder("Method ");
            sb.append(str);
            sb.append(" must be called on the main thread");
            throw new IllegalStateException(sb.toString());
        }
    }
}
