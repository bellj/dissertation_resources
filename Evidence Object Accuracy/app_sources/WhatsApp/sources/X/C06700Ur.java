package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06700Ur implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AnonymousClass0A9 A00;

    public C06700Ur(AnonymousClass0A9 r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        AnonymousClass0A9 r3 = this.A00;
        r3.A05.setColor(AnonymousClass0LP.A00(r3.A02, ((Number) valueAnimator.getAnimatedValue()).floatValue()));
        r3.invalidateSelf();
    }
}
