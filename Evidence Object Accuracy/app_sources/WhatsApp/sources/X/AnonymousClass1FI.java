package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1FI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FI {
    public final C22700zV A00;
    public final C14850m9 A01;
    public final C16120oU A02;
    public final C21860y6 A03;

    public AnonymousClass1FI(C22700zV r1, C14850m9 r2, C16120oU r3, C21860y6 r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    public void A00(AbstractC16390ow r5, String str, int i) {
        int i2;
        if (!A01(r5)) {
            AbstractC14640lm r2 = ((AbstractC15340mz) r5).A0z.A00;
            C615130q r3 = new C615130q();
            r3.A01 = 4;
            if (r5 instanceof AnonymousClass1X0) {
                i2 = 8;
            } else if (r5 instanceof AnonymousClass1X6) {
                i2 = 2;
            } else {
                i2 = 1;
                if (r5 instanceof AnonymousClass1X1) {
                    i2 = 3;
                }
            }
            r3.A03 = Integer.valueOf(i2);
            r3.A02 = Integer.valueOf(i);
            r3.A00 = Integer.valueOf(C65003Ht.A00(this.A00.A00(UserJid.of(r2))));
            if (r2 != null) {
                r3.A04 = r2.getRawString();
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("cta", "order_details");
                jSONObject.put("wa_pay_registered", this.A03.A0B());
                jSONObject.put("order_info", new JSONObject());
                jSONObject.put("payment_method_choice", str);
                r3.A05 = jSONObject.toString();
                this.A02.A05(r3);
            } catch (JSONException unused) {
                Log.e("OrderDetailsMessageLogging/logOrderDetailsAction failed to construct message class attributes");
            }
        }
    }

    public final boolean A01(AbstractC16390ow r3) {
        C16470p4 ABf;
        return !this.A01.A07(1345) || (ABf = r3.ABf()) == null || ABf.A01 == null || !(r3 instanceof AbstractC15340mz);
    }
}
