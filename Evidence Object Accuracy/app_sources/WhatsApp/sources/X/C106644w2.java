package X;

import com.google.android.exoplayer2.drm.ExoMediaCrypto;

/* renamed from: X.4w2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106644w2 implements ExoMediaCrypto {
    public static final boolean A00;

    static {
        boolean z;
        if ("Amazon".equals(AnonymousClass3JZ.A04)) {
            String str = AnonymousClass3JZ.A05;
            if ("AFTM".equals(str) || "AFTB".equals(str)) {
                z = true;
                A00 = z;
            }
        }
        z = false;
        A00 = z;
    }
}
