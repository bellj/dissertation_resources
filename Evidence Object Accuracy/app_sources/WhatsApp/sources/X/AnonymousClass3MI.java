package X;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.3MI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MI implements TextWatcher {
    public Runnable A00;
    public final long A01;
    public final Handler A02 = C12970iu.A0E();
    public final C14260l7 A03;
    public final AnonymousClass28D A04;
    public final C67823Ta A05;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AnonymousClass3MI(C14260l7 r2, AnonymousClass28D r3, C67823Ta r4, long j) {
        this.A04 = r3;
        this.A03 = r2;
        this.A05 = r4;
        this.A01 = j;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.A05.A05 = charSequence.toString();
        AnonymousClass28D r7 = this.A04;
        AbstractC14200l1 A0G = r7.A0G(56);
        if (A0G != null) {
            long j = this.A01;
            if (j <= 0) {
                C28701Oq.A01(this.A03, r7, C14210l2.A01(new C14210l2(), charSequence.toString(), 0), A0G);
                return;
            }
            Runnable runnable = this.A00;
            if (runnable != null) {
                this.A02.removeCallbacks(runnable);
            }
            RunnableBRunnable0Shape3S0300000_I1 runnableBRunnable0Shape3S0300000_I1 = new RunnableBRunnable0Shape3S0300000_I1(this, A0G, charSequence, 10);
            this.A00 = runnableBRunnable0Shape3S0300000_I1;
            this.A02.postDelayed(runnableBRunnable0Shape3S0300000_I1, j);
        }
    }
}
