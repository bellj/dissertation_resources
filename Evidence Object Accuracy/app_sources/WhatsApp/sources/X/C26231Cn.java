package X;

import android.database.Cursor;

/* renamed from: X.1Cn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26231Cn {
    public final C16490p7 A00;

    public C26231Cn(C16490p7 r1) {
        this.A00 = r1;
    }

    public Integer A00(long j) {
        String[] strArr = {Long.toString(j)};
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT rating FROM message_rating WHERE message_row_id = ?", strArr);
            if (A09 == null || !A09.moveToFirst()) {
                if (A09 != null) {
                    A09.close();
                }
                A01.close();
                return null;
            }
            Integer valueOf = Integer.valueOf(A09.getInt(A09.getColumnIndexOrThrow("rating")));
            A09.close();
            A01.close();
            return valueOf;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
