package X;

/* renamed from: X.4AP  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4AP {
    FROM_NUMBER_WITH_PLUS_SIGN,
    FROM_NUMBER_WITH_IDD,
    FROM_NUMBER_WITHOUT_PLUS_SIGN,
    FROM_DEFAULT_COUNTRY
}
