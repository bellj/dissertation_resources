package X;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/* renamed from: X.0fU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10960fU extends FutureTask {
    public final /* synthetic */ C06120Sg A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C10960fU(C06120Sg r1, Callable callable) {
        super(callable);
        this.A00 = r1;
    }

    @Override // java.util.concurrent.FutureTask
    public void done() {
        if (!isCancelled()) {
            try {
                this.A00.A02((AnonymousClass0ST) get());
            } catch (InterruptedException | ExecutionException e) {
                this.A00.A02(new AnonymousClass0ST(e));
            }
        }
    }
}
