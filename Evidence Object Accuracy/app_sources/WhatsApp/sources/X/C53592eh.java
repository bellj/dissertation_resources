package X;

import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;

/* renamed from: X.2eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53592eh extends AnonymousClass04v {
    public final TextInputLayout A00;

    public C53592eh(TextInputLayout textInputLayout) {
        this.A00 = textInputLayout;
    }

    @Override // X.AnonymousClass04v
    public void A02(View view, AccessibilityEvent accessibilityEvent) {
        CharSequence charSequence;
        super.A02(view, accessibilityEvent);
        TextInputLayout textInputLayout = this.A00;
        EditText editText = textInputLayout.A0L;
        if (editText != null) {
            charSequence = editText.getText();
        } else {
            charSequence = null;
        }
        if (TextUtils.isEmpty(charSequence)) {
            charSequence = textInputLayout.getHint();
        }
        if (!TextUtils.isEmpty(charSequence)) {
            accessibilityEvent.getText().add(charSequence);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (r4 != false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0031, code lost:
        if (android.text.TextUtils.isEmpty(r6) == false) goto L_0x0033;
     */
    @Override // X.AnonymousClass04v
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(android.view.View r11, X.AnonymousClass04Z r12) {
        /*
            r10 = this;
            super.A06(r11, r12)
            com.google.android.material.textfield.TextInputLayout r1 = r10.A00
            android.widget.EditText r0 = r1.A0L
            if (r0 == 0) goto L_0x005a
            android.text.Editable r9 = r0.getText()
        L_0x000d:
            java.lang.CharSequence r8 = r1.getHint()
            java.lang.CharSequence r7 = r1.getError()
            java.lang.CharSequence r6 = r1.getCounterOverflowDescription()
            boolean r5 = android.text.TextUtils.isEmpty(r9)
            r0 = 1
            r5 = r5 ^ r0
            boolean r4 = android.text.TextUtils.isEmpty(r8)
            r4 = r4 ^ r0
            boolean r3 = android.text.TextUtils.isEmpty(r7)
            r3 = r3 ^ r0
            r2 = 0
            if (r3 != 0) goto L_0x0033
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            r1 = 0
            if (r0 != 0) goto L_0x0034
        L_0x0033:
            r1 = 1
        L_0x0034:
            if (r5 == 0) goto L_0x0052
            android.view.accessibility.AccessibilityNodeInfo r0 = r12.A02
            r0.setText(r9)
            if (r4 == 0) goto L_0x0046
        L_0x003d:
            r12.A0D(r8)
            if (r5 != 0) goto L_0x0043
            r2 = 1
        L_0x0043:
            r12.A0N(r2)
        L_0x0046:
            if (r1 == 0) goto L_0x0051
            if (r3 != 0) goto L_0x004b
            r7 = r6
        L_0x004b:
            r12.A0C(r7)
            r12.A05()
        L_0x0051:
            return
        L_0x0052:
            if (r4 == 0) goto L_0x0046
            android.view.accessibility.AccessibilityNodeInfo r0 = r12.A02
            r0.setText(r8)
            goto L_0x003d
        L_0x005a:
            r9 = 0
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C53592eh.A06(android.view.View, X.04Z):void");
    }
}
