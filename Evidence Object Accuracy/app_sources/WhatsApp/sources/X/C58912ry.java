package X;

import com.whatsapp.backup.google.workers.GoogleBackupWorker;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.2ry  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58912ry extends C64473Fr {
    public final /* synthetic */ GoogleBackupWorker A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58912ry(AbstractC15710nm r32, C14330lG r33, C33241dg r34, C15820nx r35, C22730zY r36, AnonymousClass10N r37, AnonymousClass28Q r38, C27051Fv r39, AbstractC44761zV r40, AnonymousClass1D6 r41, C44791zY r42, AnonymousClass5TK r43, GoogleBackupWorker googleBackupWorker, C15810nw r45, C17050qB r46, C14830m7 r47, C16590pI r48, C15890o4 r49, C14820m6 r50, C15880o3 r51, C16490p7 r52, C14850m9 r53, C16120oU r54, C44931zn r55, C17220qS r56, String str, List list, AtomicLong atomicLong, AtomicLong atomicLong2) {
        super(r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, r42, r43, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, str, list, atomicLong, atomicLong2, false);
        this.A00 = googleBackupWorker;
    }
}
