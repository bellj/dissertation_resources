package X;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;

/* renamed from: X.3p8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78573p8 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99104jl();
    public long A00;
    public ParcelFileDescriptor A01;
    public DataHolder A02;
    public String A03;
    public byte[] A04;

    public C78573p8() {
        this(null, null, null, null, 0);
    }

    public C78573p8(ParcelFileDescriptor parcelFileDescriptor, DataHolder dataHolder, String str, byte[] bArr, long j) {
        this.A03 = str;
        this.A02 = dataHolder;
        this.A01 = parcelFileDescriptor;
        this.A00 = j;
        this.A04 = bArr;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        boolean A0K = C95654e8.A0K(parcel, this.A03);
        C95654e8.A0B(parcel, this.A02, 3, i, A0K);
        C95654e8.A0B(parcel, this.A01, 4, i, A0K);
        C95654e8.A08(parcel, 5, this.A00);
        C95654e8.A0G(parcel, this.A04, 6, A0K);
        C95654e8.A06(parcel, A00);
        this.A01 = null;
    }
}
