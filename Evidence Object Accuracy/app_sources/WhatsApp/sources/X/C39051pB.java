package X;

import java.io.Closeable;
import java.io.InputStream;

/* renamed from: X.1pB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39051pB implements Closeable {
    public final InputStream[] A00;
    public final /* synthetic */ C39041pA A01;

    public /* synthetic */ C39051pB(C39041pA r1, InputStream[] inputStreamArr) {
        this.A01 = r1;
        this.A00 = inputStreamArr;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        for (InputStream inputStream : this.A00) {
            C39041pA.A03(inputStream);
        }
    }
}
