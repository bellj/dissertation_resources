package X;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.textclassifier.TextClassifier;
import android.widget.EditText;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.core.view.inputmethod.InputConnectionCompat;
import com.whatsapp.R;

/* renamed from: X.011  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass011 extends EditText implements AnonymousClass012, AnonymousClass013 {
    public final AnonymousClass085 A00;
    public final AnonymousClass087 A01;
    public final AnonymousClass086 A02;
    public final AnonymousClass0Y8 A03;

    public AnonymousClass011(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.editTextStyle);
    }

    public AnonymousClass011(Context context, AttributeSet attributeSet, int i) {
        super(AnonymousClass083.A00(context), attributeSet, i);
        AnonymousClass084.A03(getContext(), this);
        AnonymousClass085 r0 = new AnonymousClass085(this);
        this.A00 = r0;
        r0.A05(attributeSet, i);
        AnonymousClass086 r02 = new AnonymousClass086(this);
        this.A02 = r02;
        r02.A0A(attributeSet, i);
        r02.A02();
        this.A01 = new AnonymousClass087(this);
        this.A03 = new AnonymousClass0Y8();
    }

    @Override // X.AnonymousClass013
    public AnonymousClass0SR AUf(AnonymousClass0SR r2) {
        return this.A03.AUe(this, r2);
    }

    @Override // android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass086 r02 = this.A02;
        if (r02 != null) {
            r02.A02();
        }
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A01;
    }

    @Override // android.widget.EditText, android.widget.TextView
    public Editable getText() {
        if (Build.VERSION.SDK_INT >= 28) {
            return super.getText();
        }
        return super.getEditableText();
    }

    @Override // android.widget.TextView
    public TextClassifier getTextClassifier() {
        AnonymousClass087 r0;
        if (Build.VERSION.SDK_INT >= 28 || (r0 = this.A01) == null) {
            return super.getTextClassifier();
        }
        return r0.A00();
    }

    @Override // android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        AnonymousClass086.A01(editorInfo, onCreateInputConnection, this);
        AnonymousClass08D.A00(this, editorInfo, onCreateInputConnection);
        String[] A0v = AnonymousClass028.A0v(this);
        if (onCreateInputConnection == null || A0v == null) {
            return onCreateInputConnection;
        }
        EditorInfoCompat.setContentMimeTypes(editorInfo, A0v);
        return InputConnectionCompat.createWrapper(onCreateInputConnection, editorInfo, new AnonymousClass0YC(this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004c, code lost:
        r0 = new java.lang.StringBuilder("Can't handle drop: no activity: view=");
        r0.append(r3);
        android.util.Log.i("ReceiveContent", r0.toString());
     */
    @Override // android.widget.TextView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onDragEvent(android.view.DragEvent r4) {
        /*
            r3 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 24
            if (r1 < r0) goto L_0x005f
            java.lang.Object r0 = r4.getLocalState()
            if (r0 != 0) goto L_0x005f
            java.lang.String[] r0 = X.AnonymousClass028.A0v(r3)
            if (r0 == 0) goto L_0x005f
            android.content.Context r2 = r3.getContext()
        L_0x0016:
            boolean r0 = r2 instanceof android.content.ContextWrapper
            if (r0 == 0) goto L_0x004c
            boolean r0 = r2 instanceof android.app.Activity
            if (r0 == 0) goto L_0x0045
            android.app.Activity r2 = (android.app.Activity) r2
            if (r2 == 0) goto L_0x004c
            int r0 = r4.getAction()
            r1 = 1
            if (r0 != r1) goto L_0x0030
            boolean r0 = r3 instanceof android.widget.TextView
            r0 = r0 ^ r1
        L_0x002c:
            if (r0 == 0) goto L_0x005f
            r0 = 1
            return r0
        L_0x0030:
            int r1 = r4.getAction()
            r0 = 3
            if (r1 != r0) goto L_0x005f
            boolean r0 = r3 instanceof android.widget.TextView
            if (r0 == 0) goto L_0x0040
            boolean r0 = X.AnonymousClass0QW.A01(r2, r4, r3)
            goto L_0x002c
        L_0x0040:
            boolean r0 = X.AnonymousClass0QW.A00(r2, r4, r3)
            goto L_0x002c
        L_0x0045:
            android.content.ContextWrapper r2 = (android.content.ContextWrapper) r2
            android.content.Context r2 = r2.getBaseContext()
            goto L_0x0016
        L_0x004c:
            java.lang.String r1 = "Can't handle drop: no activity: view="
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = "ReceiveContent"
            android.util.Log.i(r0, r1)
        L_0x005f:
            boolean r0 = super.onDragEvent(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass011.onDragEvent(android.view.DragEvent):boolean");
    }

    @Override // android.widget.TextView
    public boolean onTextContextMenuItem(int i) {
        ClipData primaryClip;
        int i2 = 0;
        if ((i != 16908322 && i != 16908337) || AnonymousClass028.A0v(this) == null) {
            return super.onTextContextMenuItem(i);
        }
        ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService("clipboard");
        if (clipboardManager == null || (primaryClip = clipboardManager.getPrimaryClip()) == null || primaryClip.getItemCount() <= 0) {
            return true;
        }
        AnonymousClass0MT r0 = new AnonymousClass0MT(primaryClip, 1);
        if (i != 16908322) {
            i2 = 1;
        }
        AbstractC12620iD r02 = r0.A00;
        r02.Ac8(i2);
        AnonymousClass028.A0E(this, r02.A6j());
        return true;
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A02(i);
        }
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(AnonymousClass04D.A02(callback, this));
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A03(colorStateList);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A04(mode);
        }
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        AnonymousClass086 r0 = this.A02;
        if (r0 != null) {
            r0.A05(context, i);
        }
    }

    @Override // android.widget.TextView
    public void setTextClassifier(TextClassifier textClassifier) {
        AnonymousClass087 r0;
        if (Build.VERSION.SDK_INT >= 28 || (r0 = this.A01) == null) {
            super.setTextClassifier(textClassifier);
        } else {
            r0.A01(textClassifier);
        }
    }
}
