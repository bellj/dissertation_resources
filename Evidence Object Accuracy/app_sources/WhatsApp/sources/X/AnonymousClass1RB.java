package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.1RB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1RB extends AbstractC29111Qx {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ C251818k A03;
    public final /* synthetic */ C251918l A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ String A07;
    public final /* synthetic */ String A08;
    public final /* synthetic */ String A09;
    public final /* synthetic */ String A0A;
    public final /* synthetic */ List A0B;
    public final /* synthetic */ Map A0C;
    public final /* synthetic */ byte[] A0D;
    public final /* synthetic */ byte[] A0E;
    public final /* synthetic */ byte[] A0F;

    public AnonymousClass1RB(C251818k r2, C251918l r3, String str, String str2, String str3, String str4, String str5, String str6, List list, Map map, byte[] bArr, byte[] bArr2, byte[] bArr3, int i, int i2, int i3) {
        this.A04 = r3;
        this.A03 = r2;
        this.A08 = str;
        this.A09 = str2;
        this.A0F = bArr;
        this.A0D = bArr2;
        this.A0A = str3;
        this.A07 = str4;
        this.A06 = str5;
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
        this.A0E = bArr3;
        this.A05 = str6;
        this.A0C = map;
        this.A0B = list;
    }
}
