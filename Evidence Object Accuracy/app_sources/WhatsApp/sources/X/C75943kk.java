package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;

/* renamed from: X.3kk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75943kk extends AbstractC94334bd {
    public C75943kk(AnonymousClass0DQ r1, AnonymousClass5YZ r2, int i) {
        super(r1, r2, i);
    }

    @Override // X.AbstractC94334bd
    public int A00(BitmapFactory.Options options, int i, int i2) {
        ColorSpace colorSpace = options.outColorSpace;
        if (colorSpace == null || !colorSpace.isWideGamut() || options.inPreferredConfig == Bitmap.Config.RGBA_F16) {
            return i * i2 * C94594c9.A00(options.inPreferredConfig);
        }
        return (i * i2) << 3;
    }
}
