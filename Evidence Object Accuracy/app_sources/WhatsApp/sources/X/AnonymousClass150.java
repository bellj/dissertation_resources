package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.150  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass150 {
    public final AnonymousClass016 A00 = new AnonymousClass016();
    public final C15570nT A01;
    public final C15550nR A02;
    public final C22700zV A03;
    public final C14830m7 A04;
    public final AnonymousClass15P A05;
    public final C22140ya A06;

    public AnonymousClass150(C15570nT r2, C15550nR r3, C22700zV r4, C14830m7 r5, AnonymousClass15P r6, C22140ya r7) {
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A06 = r7;
        this.A05 = r6;
    }

    public int A00(UserJid userJid) {
        C15370n3 A0A;
        if (this.A01.A0F(userJid)) {
            return A04().intValue();
        }
        if (this.A03.A02(userJid) || (A0A = this.A02.A0A(userJid)) == null) {
            return 0;
        }
        return A0A.A00;
    }

    public long A01(UserJid userJid) {
        if (this.A01.A0F(userJid)) {
            return this.A05.A00().getLong("disappearing_mode_timestamp", 0);
        }
        C15370n3 A0A = this.A02.A0A(userJid);
        if (A0A == null) {
            return 0;
        }
        return A0A.A07;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        if (r1 <= r0) goto L_0x003b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.whatsapp.jid.UserJid A02(X.AbstractC14640lm r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.whatsapp.jid.UserJid
            r5 = 0
            if (r0 == 0) goto L_0x005f
            X.0zV r0 = r6.A03
            com.whatsapp.jid.UserJid r7 = (com.whatsapp.jid.UserJid) r7
            boolean r0 = r0.A02(r7)
            if (r0 != 0) goto L_0x005f
            X.15P r4 = r6.A05
            android.content.SharedPreferences r1 = r4.A00()
            java.lang.String r3 = "disappearing_mode_duration_int"
            r0 = 0
            int r0 = r1.getInt(r3, r0)
            if (r0 == 0) goto L_0x0046
            X.0nR r2 = r6.A02
            X.0n3 r0 = r2.A0A(r7)
            if (r0 == 0) goto L_0x003b
            int r0 = r0.A00
            if (r0 == 0) goto L_0x003b
            java.lang.Integer r0 = r6.A04()
            int r1 = r0.intValue()
            X.0n3 r0 = r2.A0A(r7)
            if (r0 != 0) goto L_0x0043
            r0 = 0
        L_0x0039:
            if (r1 > r0) goto L_0x0046
        L_0x003b:
            X.0nT r0 = r6.A01
            r0.A08()
            X.1Ih r7 = r0.A05
            return r7
        L_0x0043:
            int r0 = r0.A00
            goto L_0x0039
        L_0x0046:
            X.0nT r0 = r6.A01
            boolean r0 = r0.A0F(r7)
            if (r0 == 0) goto L_0x005a
            android.content.SharedPreferences r1 = r4.A00()
            r0 = 0
            int r0 = r1.getInt(r3, r0)
        L_0x0057:
            if (r0 == 0) goto L_0x005f
            return r7
        L_0x005a:
            int r0 = r6.A00(r7)
            goto L_0x0057
        L_0x005f:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass150.A02(X.0lm):com.whatsapp.jid.UserJid");
    }

    public AnonymousClass1XA A03(AbstractC14640lm r14) {
        UserJid A02 = A02(r14);
        if (A02 == null) {
            return null;
        }
        int i = 2;
        if (this.A01.A0F(A02)) {
            i = 1;
        }
        C22140ya r7 = this.A06;
        UserJid of = UserJid.of(r14);
        AnonymousClass009.A05(of);
        long A00 = this.A04.A00();
        int A002 = A00(A02);
        long A01 = A01(A02);
        AnonymousClass1XA r1 = (AnonymousClass1XA) C22140ya.A00(r7.A00, r7.A03.A02(of, true), null, 68, A00);
        r1.A00 = A002;
        r1.A0j(Long.valueOf(A01));
        ((AbstractC15340mz) r1).A00 = i;
        return r1;
    }

    public Integer A04() {
        Integer num = (Integer) this.A00.A01();
        return num == null ? Integer.valueOf(this.A05.A00().getInt("disappearing_mode_duration_int", 0)) : num;
    }

    public void A05(int i, long j) {
        AnonymousClass15P r2 = this.A05;
        r2.A00().edit().putInt("disappearing_mode_duration_int", i).apply();
        r2.A00().edit().putLong("disappearing_mode_timestamp", j).apply();
        this.A00.A0A(Integer.valueOf(i));
    }
}
