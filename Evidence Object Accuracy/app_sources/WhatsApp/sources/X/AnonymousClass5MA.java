package X;

/* renamed from: X.5MA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MA extends AnonymousClass5NS {
    public AnonymousClass5MA(byte[] bArr, int i) {
        super(bArr, i);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int length = this.A01.length;
        return AnonymousClass1TQ.A00(length + 1) + 1 + length + 1;
    }

    @Override // X.AnonymousClass5NS, X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return this;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    public static AnonymousClass5MA A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5MA)) {
            return (AnonymousClass5MA) obj;
        }
        if (obj instanceof AnonymousClass5M9) {
            AnonymousClass5NS r3 = (AnonymousClass5NS) obj;
            return new AnonymousClass5MA(r3.A01, r3.A00);
        } else if (obj instanceof byte[]) {
            try {
                return (AnonymousClass5MA) AnonymousClass1TL.A03((byte[]) obj);
            } catch (Exception e) {
                throw C12970iu.A0f(C12960it.A0d(e.toString(), C12960it.A0k("encoding error in getInstance: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("illegal object in getInstance: ")));
        }
    }

    public static AnonymousClass5MA A01(AnonymousClass5NU r5) {
        AnonymousClass1TL A00 = AnonymousClass5NU.A00(r5);
        if (A00 instanceof AnonymousClass5MA) {
            return A00(A00);
        }
        byte[] A05 = AnonymousClass5NH.A05(A00);
        int length = A05.length;
        if (length >= 1) {
            byte b = A05[0];
            int i = length - 1;
            byte[] bArr = new byte[i];
            if (i != 0) {
                System.arraycopy(A05, 1, bArr, 0, i);
            }
            return new AnonymousClass5MA(bArr, b);
        }
        throw C12970iu.A0f("truncated BIT STRING detected");
    }
}
