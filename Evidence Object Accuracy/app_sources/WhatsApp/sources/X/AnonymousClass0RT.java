package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0RT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0RT {
    public static final C006202y A00 = new C006202y(16);
    public static final AnonymousClass00O A01 = new AnonymousClass00O();
    public static final Object A02 = new Object();
    public static final ExecutorService A03;

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, (long) SearchActionVerificationClientService.NOTIFICATION_ID, TimeUnit.MILLISECONDS, new LinkedBlockingDeque(), new ThreadFactoryC10650ey());
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        A03 = threadPoolExecutor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004c, code lost:
        if (r2 != 1) goto L_0x004e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C05820Rc A00(android.content.Context r7, X.C05060Oc r8, java.lang.String r9, int r10) {
        /*
            X.02y r6 = X.AnonymousClass0RT.A00
            java.lang.Object r1 = r6.A04(r9)
            android.graphics.Typeface r1 = (android.graphics.Typeface) r1
            if (r1 != 0) goto L_0x002c
            r5 = 0
            X.0Mu r0 = X.AnonymousClass0RC.A00(r7, r8)     // Catch: NameNotFoundException -> 0x0016
            int r2 = r0.A00
            r4 = -3
            r1 = 1
            if (r2 == 0) goto L_0x0032
            goto L_0x004a
        L_0x0016:
            r1 = -1
            X.0Rc r0 = new X.0Rc
            r0.<init>(r1)
            return r0
        L_0x001d:
            int r1 = r1 + 1
            if (r1 < r2) goto L_0x003a
            X.0Tl r0 = X.AnonymousClass082.A01
            android.graphics.Typeface r1 = r0.A06(r7, r5, r3, r10)
            if (r1 == 0) goto L_0x0044
            r6.A08(r9, r1)
        L_0x002c:
            X.0Rc r0 = new X.0Rc
            r0.<init>(r1)
            return r0
        L_0x0032:
            X.0No[] r3 = r0.A01
            if (r3 == 0) goto L_0x004f
            int r2 = r3.length
            if (r2 == 0) goto L_0x004f
            r1 = 0
        L_0x003a:
            r0 = r3[r1]
            int r0 = r0.A00
            if (r0 == 0) goto L_0x001d
            if (r0 < 0) goto L_0x004e
            r4 = r0
            goto L_0x004e
        L_0x0044:
            X.0Rc r0 = new X.0Rc
            r0.<init>(r4)
            return r0
        L_0x004a:
            r0 = 1
            r1 = -2
            if (r2 == r0) goto L_0x004f
        L_0x004e:
            r1 = r4
        L_0x004f:
            X.0Rc r0 = new X.0Rc
            r0.<init>(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0RT.A00(android.content.Context, X.0Oc, java.lang.String, int):X.0Rc");
    }
}
