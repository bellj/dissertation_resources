package X;

/* renamed from: X.5oH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123995oH extends AbstractC16350or {
    public final /* synthetic */ C1327568c A00;

    public C123995oH(C1327568c r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AbstractActivityC121515iQ r2 = this.A00.A00;
        C17070qD r0 = ((AbstractActivityC121685jC) r2).A0P;
        r0.A03();
        return r0.A09.A08(r2.A00.A0A);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AbstractC28901Pl r4 = (AbstractC28901Pl) obj;
        C1327568c r0 = this.A00;
        if (r4 != null) {
            AbstractActivityC121515iQ r2 = r0.A00;
            C30861Zc r42 = (C30861Zc) r4;
            r2.A00 = r42;
            r2.A01.A01((C119755f3) r42.A08, r2);
            return;
        }
        r0.A00.A39();
    }
}
