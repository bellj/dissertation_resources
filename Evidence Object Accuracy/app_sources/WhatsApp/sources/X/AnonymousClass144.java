package X;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.144  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass144 {
    public final AbstractC15710nm A00;
    public final AnonymousClass143 A01;
    public final C16590pI A02;
    public final C21780xy A03;

    public AnonymousClass144(AbstractC15710nm r1, AnonymousClass143 r2, C16590pI r3, C21780xy r4) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
    }

    public static Pair A00(String str) {
        int indexOf = str.indexOf(" ");
        if (indexOf < 0) {
            return null;
        }
        return new Pair(URLDecoder.decode(str.substring(0, indexOf)), str.substring(indexOf + 1));
    }

    public static String A01(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(URLEncoder.encode(str));
        sb.append(" ");
        sb.append(str2);
        return sb.toString();
    }

    public static byte[] A02(Context context, AnonymousClass1KZ r10) {
        try {
            try {
                InputStream openInputStream = context.getContentResolver().openInputStream(Uri.parse(r10.A0I));
                try {
                    if (openInputStream != null) {
                        byte[] bArr = new byte[51201];
                        int read = openInputStream.read(bArr, 0, 51201);
                        if (read != 51201) {
                            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, read);
                            if (decodeByteArray.getWidth() > 512 || decodeByteArray.getHeight() < 24) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("tray icon width incorrect, it is currently ");
                                sb.append(decodeByteArray.getWidth());
                                sb.append(", should be between ");
                                sb.append(24);
                                sb.append(" and ");
                                sb.append(512);
                                sb.append(" pixels, sticker pack: ");
                                sb.append(r10.A0D);
                                throw new IllegalArgumentException(sb.toString());
                            } else if (decodeByteArray.getHeight() > 512 || decodeByteArray.getHeight() < 24) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("tray icon height incorrect, it is currently ");
                                sb2.append(decodeByteArray.getHeight());
                                sb2.append(", should be between ");
                                sb2.append(24);
                                sb2.append(" and ");
                                sb2.append(512);
                                sb2.append(" pixels, sticker pack: ");
                                sb2.append(r10.A0D);
                                throw new IllegalArgumentException(sb2.toString());
                            } else {
                                openInputStream.close();
                                return bArr;
                            }
                        } else {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("tray icon file size too big, limit is 50 KB, sticker pack: ");
                            sb3.append(r10.A0D);
                            throw new IllegalArgumentException(sb3.toString());
                        }
                    } else {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("failed to fetch sticker tray icon, inputstream is null: ");
                        sb4.append(r10.A0D);
                        throw new IOException(sb4.toString());
                    }
                } catch (Throwable th) {
                    if (openInputStream != null) {
                        try {
                            openInputStream.close();
                        } catch (Throwable unused) {
                        }
                    }
                    throw th;
                }
            } catch (Exception e) {
                StringBuilder sb5 = new StringBuilder("failed to fetch sticker tray icon, sticker pack:");
                sb5.append(r10.A0D);
                throw new IOException(sb5.toString(), e);
            }
        } catch (IOException | IllegalArgumentException e2) {
            throw e2;
        }
    }

    public AnonymousClass1KZ A03(String str, String str2) {
        String path;
        Boolean valueOf;
        C37471mS[] r0;
        AnonymousClass1KZ A04 = A04(str, str2);
        ArrayList arrayList = new ArrayList();
        String A01 = A01(str, str2);
        Context context = this.A02.A00;
        Cursor query = context.getContentResolver().query(new Uri.Builder().scheme("content").authority(str).appendPath("stickers").appendPath(str2).build(), new String[]{"sticker_file_name", "sticker_emoji"}, null, null, null);
        try {
            if (query == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("could not find stickers for sticker pack: ");
                sb.append(A01);
                throw new IllegalArgumentException(sb.toString());
            } else if (query.getCount() < 3 || query.getCount() > 30) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("sticker count should be between 3 to 30 inclusive, it currently has ");
                sb2.append(query.getCount());
                sb2.append(", sticker pack: ");
                sb2.append(str2);
                throw new IllegalArgumentException(sb2.toString());
            } else {
                query.moveToFirst();
                do {
                    String string = query.getString(query.getColumnIndexOrThrow("sticker_file_name"));
                    String string2 = query.getString(query.getColumnIndexOrThrow("sticker_emoji"));
                    if (!C14350lI.A0R(string)) {
                        C37471mS[] r1 = null;
                        if (!TextUtils.isEmpty(string2)) {
                            String[] split = string2.split(",");
                            int length = split.length;
                            r1 = new C37471mS[length];
                            for (int i = 0; i < length; i++) {
                                r1[i] = C38491oB.A00(split[i]);
                            }
                        }
                        Uri build = new Uri.Builder().scheme("content").authority(str).appendPath("stickers_asset").appendPath(str2).appendPath(string).build();
                        AnonymousClass1KB r11 = new AnonymousClass1KB(A01, A04.A0F, A04.A0H, A04.A0G, A04.A0C, r1, false, false, false);
                        try {
                            InputStream openInputStream = context.getContentResolver().openInputStream(build);
                            if (openInputStream != null) {
                                try {
                                    File A00 = this.A03.A00.A00(AnonymousClass1US.A0A(Base64.encodeToString(C003501n.A0D(32), 2)));
                                    if (C14350lI.A0P(A00, openInputStream)) {
                                        openInputStream.close();
                                        try {
                                            path = build.getPath();
                                            valueOf = Boolean.valueOf(A04.A0M);
                                            r0 = r11.A08;
                                        } catch (IOException e) {
                                            Log.e("ThirdPartyStickerFetcher/fetchStickersForStickerPack/exception when operating on sticker file", e);
                                        }
                                        if (r0 != null && r0.length > 3) {
                                            StringBuilder sb3 = new StringBuilder("emoji count exceed limit, sticker name:");
                                            sb3.append(path);
                                            throw new C38501oC(sb3.toString());
                                            break;
                                        }
                                        C38521oE.A00(A00, valueOf, path);
                                        if (WebpUtils.A01(A00, r11.A01())) {
                                            String A002 = C38531oF.A00(A00);
                                            if (A002 != null) {
                                                AnonymousClass1KS r13 = new AnonymousClass1KS();
                                                r13.A0E = A01;
                                                r13.A08 = build.toString();
                                                r13.A01 = 3;
                                                r13.A0C = A002;
                                                r13.A09 = WebpUtils.A00(A00);
                                                r13.A0B = "image/webp";
                                                r13.A00 = (int) A00.length();
                                                r13.A04 = r11;
                                                arrayList.add(r13);
                                            }
                                        } else {
                                            Log.e("ThirdPartyStickerFetcher/calculatePlainTextHash/failed to insert metadata");
                                        }
                                        C14350lI.A0M(A00);
                                    } else {
                                        openInputStream.close();
                                    }
                                } catch (Throwable th) {
                                    try {
                                        openInputStream.close();
                                    } catch (Throwable unused) {
                                    }
                                    throw th;
                                    break;
                                }
                            }
                        } catch (IOException e2) {
                            Log.e("ThirdPartyStickerFetcher/saveStickerFileToTempLocation/io exception when fetching sticker", e2);
                        }
                    } else {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("sticker file name: ");
                        sb4.append(string);
                        sb4.append(" is invalid, authority: ");
                        sb4.append(str);
                        sb4.append(",identifier: ");
                        sb4.append(str2);
                        throw new IllegalArgumentException(sb4.toString());
                    }
                } while (query.moveToNext());
                query.close();
                Iterator it = arrayList.iterator();
                long j = 0;
                while (it.hasNext()) {
                    j += (long) ((AnonymousClass1KS) it.next()).A00;
                }
                A04.A04 = arrayList;
                A04.A01 = j;
                return A04;
            }
        } catch (Throwable th2) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable unused2) {
                }
            }
            throw th2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01ff, code lost:
        if (r15.getShort(r3) <= 0) goto L_0x0201;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0066  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1KZ A04(java.lang.String r28, java.lang.String r29) {
        /*
        // Method dump skipped, instructions count: 991
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass144.A04(java.lang.String, java.lang.String):X.1KZ");
    }
}
