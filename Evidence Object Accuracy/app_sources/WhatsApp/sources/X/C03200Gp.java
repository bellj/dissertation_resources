package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.0Gp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03200Gp extends AnonymousClass0Q1 {
    public final AtomicReferenceFieldUpdater A00;
    public final AtomicReferenceFieldUpdater A01;
    public final AtomicReferenceFieldUpdater A02;
    public final AtomicReferenceFieldUpdater A03;
    public final AtomicReferenceFieldUpdater A04;

    public C03200Gp(AtomicReferenceFieldUpdater atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater atomicReferenceFieldUpdater5) {
        this.A03 = atomicReferenceFieldUpdater;
        this.A02 = atomicReferenceFieldUpdater2;
        this.A04 = atomicReferenceFieldUpdater3;
        this.A00 = atomicReferenceFieldUpdater4;
        this.A01 = atomicReferenceFieldUpdater5;
    }

    @Override // X.AnonymousClass0Q1
    public void A00(C06340Tf r2, C06340Tf r3) {
        this.A02.lazySet(r2, r3);
    }

    @Override // X.AnonymousClass0Q1
    public void A01(C06340Tf r2, Thread thread) {
        this.A03.lazySet(r2, thread);
    }

    @Override // X.AnonymousClass0Q1
    public boolean A02(C05940Ro r2, C05940Ro r3, AnonymousClass041 r4) {
        return AnonymousClass0KN.A00(r4, r2, r3, this.A00);
    }

    @Override // X.AnonymousClass0Q1
    public boolean A03(C06340Tf r2, C06340Tf r3, AnonymousClass041 r4) {
        return AnonymousClass0KN.A00(r4, r2, r3, this.A04);
    }

    @Override // X.AnonymousClass0Q1
    public boolean A04(AnonymousClass041 r2, Object obj, Object obj2) {
        return AnonymousClass0KN.A00(r2, obj, obj2, this.A01);
    }
}
