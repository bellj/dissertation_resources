package X;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* renamed from: X.025  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass025 {
    public static int A00 = -100;
    public static final AnonymousClass01b A01 = new AnonymousClass01b(0);
    public static final Object A02 = new Object();

    public abstract Context A03(Context context);

    public abstract View A04(View view, String str, Context context, AttributeSet attributeSet);

    public abstract AbstractC009504t A05(AnonymousClass02Q v);

    public abstract void A06();

    public abstract void A07();

    public abstract void A08();

    public abstract void A09();

    public abstract void A0A(int i);

    public abstract void A0B(int i);

    public abstract void A0C(Configuration configuration);

    public abstract void A0D(Bundle bundle);

    public abstract void A0E(View view);

    public abstract void A0F(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void A0G(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void A0H(CharSequence charSequence);

    public static void A00(int i) {
        if (i != -1 && i != 0 && i != 1 && i != 2 && i != 3) {
            Log.d("AppCompatDelegate", "setDefaultNightMode() called with an unknown mode");
        } else if (A00 != i) {
            A00 = i;
            synchronized (A02) {
                Iterator it = A01.iterator();
                while (it.hasNext()) {
                    AnonymousClass025 r1 = (AnonymousClass025) ((WeakReference) it.next()).get();
                    if (r1 != null) {
                        ((LayoutInflater$Factory2C011505o) r1).A0V(true);
                    }
                }
            }
        }
    }

    public static void A01(AnonymousClass025 r3) {
        synchronized (A02) {
            Iterator it = A01.iterator();
            while (it.hasNext()) {
                AnonymousClass025 r0 = (AnonymousClass025) ((WeakReference) it.next()).get();
                if (r0 == r3 || r0 == null) {
                    it.remove();
                }
            }
        }
    }
}
