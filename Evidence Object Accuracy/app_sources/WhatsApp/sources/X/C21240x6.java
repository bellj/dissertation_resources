package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;

/* renamed from: X.0x6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21240x6 {
    public final C15550nR A00;
    public final AnonymousClass01d A01;
    public final C15890o4 A02;
    public final C16120oU A03;
    public final AbstractC14440lR A04;

    public C21240x6(C15550nR r1, AnonymousClass01d r2, C15890o4 r3, C16120oU r4, AbstractC14440lR r5) {
        this.A04 = r5;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public void A00(Integer num) {
        AnonymousClass312 r3 = new AnonymousClass312();
        r3.A03 = 1;
        r3.A04 = num;
        this.A04.Ab2(new RunnableBRunnable0Shape2S0200000_I0_2(this, 40, r3));
    }

    public void A01(Integer num, String str, int i) {
        AnonymousClass312 r3 = new AnonymousClass312();
        r3.A03 = Integer.valueOf(i);
        r3.A04 = num;
        r3.A07 = str;
        this.A04.Ab2(new RunnableBRunnable0Shape2S0200000_I0_2(this, 40, r3));
    }
}
