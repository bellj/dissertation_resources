package X;

import android.content.Context;
import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;

/* renamed from: X.4r4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103634r4 implements AbstractC009204q {
    public final /* synthetic */ TextStatusComposerActivity A00;

    public C103634r4(TextStatusComposerActivity textStatusComposerActivity) {
        this.A00 = textStatusComposerActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
