package X;

import android.view.View;
import com.whatsapp.util.Log;

/* renamed from: X.2zQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61112zQ extends AbstractC65123If {
    public static final C63643Cj A02 = new C63643Cj(72, 4.0f, 1.0f);
    public static final C63643Cj A03 = new C63643Cj(72, 1.0f, 1.0f);
    public static final C63643Cj A04 = new C63643Cj(65, 5.0f, 7.0f);
    public final C63643Cj A00;
    public final C63643Cj A01;

    public C61112zQ(C63643Cj r1, C63643Cj r2, int i) {
        super(i);
        this.A01 = r1;
        this.A00 = r2;
    }

    public static final Float A00(float f, int i) {
        int mode = View.MeasureSpec.getMode(i);
        if (mode == Integer.MIN_VALUE) {
            f = Math.min(f, (float) View.MeasureSpec.getSize(i));
        } else if (mode != 0) {
            if (mode != 1073741824) {
                Log.w(C12960it.A0W(mode, "ConversationRowSingleImagePreviewCalculator/getSizeToSpec: Unhandled View.MeasureSpec "));
            } else if (f != ((float) View.MeasureSpec.getSize(i))) {
                return null;
            }
        }
        return Float.valueOf(f);
    }
}
