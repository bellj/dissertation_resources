package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1ZH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZH implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100294lg();
    public boolean A00;
    public final String A01;
    public final byte[] A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZH(Parcel parcel) {
        this.A00 = parcel.readInt() != 0;
        this.A02 = parcel.createByteArray();
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A01 = readString;
    }

    public AnonymousClass1ZH(String str, byte[] bArr, boolean z) {
        this.A02 = bArr;
        this.A01 = str;
        this.A00 = z;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00 ? 1 : 0);
        parcel.writeByteArray(this.A02);
        parcel.writeString(this.A01);
    }
}
