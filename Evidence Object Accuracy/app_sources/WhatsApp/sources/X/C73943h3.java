package X;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3h3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73943h3 extends Animation {
    public final int A00;
    public final int A01;
    public final /* synthetic */ C92254Vd A02;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return true;
    }

    public C73943h3(View view, C92254Vd r3, int i) {
        this.A02 = r3;
        this.A01 = i;
        this.A00 = view.getHeight();
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i = this.A00;
        int i2 = i + ((int) (((float) (this.A01 - i)) * f));
        C92254Vd r2 = this.A02;
        View view = r2.A01;
        view.getLayoutParams().height = i2;
        view.requestLayout();
        r2.A02.ATC((float) i2);
    }
}
