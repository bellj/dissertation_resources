package X;

/* renamed from: X.1X0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1X0 extends C16440p1 implements AbstractC16390ow, AbstractC16400ox, AbstractC16420oz {
    public C16470p4 A00;

    public AnonymousClass1X0(C16150oX r10, AnonymousClass1IS r11, AnonymousClass1X0 r12, long j, boolean z) {
        super(r10, r11, r12, r12.A0y, j, z);
        this.A00 = r12.A00;
    }

    public AnonymousClass1X0(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 63, j);
    }

    public AnonymousClass1X0(C40831sP r8, AnonymousClass1IS r9, C16470p4 r10, long j, boolean z, boolean z2) {
        super(r8, r9, j, z, z2);
        this.A00 = r10;
    }

    @Override // X.AbstractC16390ow
    public C16470p4 ABf() {
        return this.A00;
    }

    @Override // X.AbstractC16390ow
    public C33711ex ACL() {
        C16470p4 r1 = this.A00;
        if (r1 == null) {
            return null;
        }
        return AnonymousClass4EY.A00(r1, r1.A00);
    }

    @Override // X.AbstractC16390ow
    public void Abv(C16470p4 r1) {
        this.A00 = r1;
    }
}
