package X;

/* renamed from: X.319  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass319 extends AbstractC16110oT {
    public Boolean A00;
    public Double A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public Long A06;
    public String A07;
    public String A08;

    public AnonymousClass319() {
        super(834, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(6, this.A00);
        r3.Abe(4, this.A07);
        r3.Abe(8, this.A01);
        r3.Abe(7, this.A08);
        r3.Abe(5, this.A05);
        r3.Abe(3, this.A02);
        r3.Abe(9, this.A06);
        r3.Abe(1, this.A03);
        r3.Abe(2, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPlacesApiQuery {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiCached", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiFailureDescription", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiPlacesCount", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiQueryString", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiRequestIndex", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiResponse", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiResponseT", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiSource", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "placesApiSourceDefault", C12960it.A0Y(this.A04));
        return C12960it.A0d("}", A0k);
    }
}
