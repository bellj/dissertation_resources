package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Zq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31001Zq {
    public static String A04(int i) {
        return i != 6 ? i != 7 ? i != 8 ? (i == 100 || i == 200) ? "p2m" : "p2p" : "withdrawal" : "refund" : "deposit";
    }

    /* JADX WARNING: Removed duplicated region for block: B:146:0x0149 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:156:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A05(int r8, int r9) {
        /*
        // Method dump skipped, instructions count: 336
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31001Zq.A05(int, int):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044 A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:196:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(int r17, java.lang.String r18) {
        /*
        // Method dump skipped, instructions count: 690
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31001Zq.A00(int, java.lang.String):int");
    }

    public static AnonymousClass1IR A01(long j) {
        return new AnonymousClass1IR("UNSET", 4, 1, j);
    }

    public static AnonymousClass1IR A02(AbstractC30791Yv r24, C30821Yy r25, UserJid userJid, UserJid userJid2, String str, String str2, String str3, int i, int i2, int i3, int i4, int i5, long j) {
        ArrayList arrayList = new ArrayList();
        if (i5 == 1) {
            arrayList.add("feature_bip");
        }
        int i6 = 1;
        if (!TextUtils.isEmpty(str3) && AnonymousClass4HR.A02.containsKey(str3)) {
            if (arrayList.isEmpty()) {
                i6 = AnonymousClass20N.A00(str3);
            } else {
                Integer A01 = AnonymousClass20N.A01(str3, (String) Collections.max(arrayList, new Comparator(str3) { // from class: X.5Cd
                    public final /* synthetic */ String A00;

                    {
                        this.A00 = r1;
                    }

                    @Override // java.util.Comparator
                    public final int compare(Object obj, Object obj2) {
                        int i7;
                        String str4 = this.A00;
                        Integer A012 = AnonymousClass20N.A01(str4, (String) obj);
                        Integer A013 = AnonymousClass20N.A01(str4, (String) obj2);
                        int i8 = 1;
                        if (A012 != null) {
                            i7 = A012.intValue();
                        } else {
                            i7 = 1;
                        }
                        Integer valueOf = Integer.valueOf(i7);
                        if (A013 != null) {
                            i8 = A013.intValue();
                        }
                        Integer valueOf2 = Integer.valueOf(i8);
                        if (valueOf.intValue() <= valueOf2.intValue()) {
                            valueOf = valueOf2;
                        }
                        return valueOf.intValue();
                    }
                }));
                if (A01 != null) {
                    i6 = A01.intValue();
                }
            }
        }
        return new AnonymousClass1IR(r24, r25, userJid, userJid2, str, null, null, null, null, str2, str3, i, i2, Math.max(i6, i3), i4, i5, j, 0);
    }

    public static AnonymousClass1IR A03(AbstractC30791Yv r20, UserJid userJid, UserJid userJid2, String str, String str2, String str3, String str4, String str5, String str6, BigDecimal bigDecimal, byte[] bArr, int i, int i2, int i3, int i4, int i5, long j, long j2) {
        if (i == 4) {
            return A01(j);
        }
        AnonymousClass1IR r1 = new AnonymousClass1IR(r20, new C30821Yy(bigDecimal, ((AbstractC30781Yu) r20).A01), userJid, userJid2, str, str2, str3, str4, str5, null, str6, i, i2, i3, i4, i5, j, j2);
        r1.A0R = bArr;
        r1.A09(false);
        r1.A07 = r20;
        return r1;
    }

    public static String A06(List list) {
        if (list.size() <= 0) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            jSONArray.put(((AnonymousClass20Z) it.next()).A00(false));
        }
        return jSONArray.toString();
    }

    public static ArrayList A07(AbstractC30791Yv r14, String str) {
        String str2;
        try {
            JSONArray jSONArray = new JSONArray(str);
            ArrayList arrayList = new ArrayList(jSONArray.length());
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                int optInt = jSONObject.optInt("t", 0);
                String optString = jSONObject.optString("st", null);
                String optString2 = jSONObject.optString("cc", null);
                String optString3 = jSONObject.optString("c", null);
                String optString4 = jSONObject.optString("n", null);
                String optString5 = jSONObject.optString("a", null);
                int optInt2 = jSONObject.optInt("sd", 1);
                if (TextUtils.isEmpty(optString3) || TextUtils.isEmpty(optString5)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("PAY: PaymentTransaction:Source:fromJsonString could not parse string: ");
                    sb.append(str);
                    str2 = sb.toString();
                } else {
                    C30821Yy A00 = C30821Yy.A00(optString5, ((AbstractC30781Yu) r14).A01);
                    if (A00 == null || !A00.A03()) {
                        str2 = "PAY: PaymentTransaction:Source:fromJsonString could not parse string amount";
                    } else {
                        AbstractC28901Pl A01 = AbstractC28901Pl.A01(C17930rd.A00(optString2), optString, optString3, optString4, optInt);
                        if (A01 instanceof C30881Ze) {
                            ((C30881Ze) A01).A01 = jSONObject.optInt("ci", 0);
                        }
                        arrayList.add(new AnonymousClass20Z(A00, A01, optInt2));
                    }
                }
                Log.w(str2);
                return null;
            }
            arrayList.size();
            return arrayList;
        } catch (JSONException e) {
            Log.w("PAY: PaymentTransaction:Source:fromJsonString threw json exception in response: ", e);
            return null;
        }
    }

    public static boolean A08(AnonymousClass1IR r4) {
        C30821Yy r0;
        if (r4 == null) {
            return true;
        }
        int i = r4.A03;
        if (i == 1000) {
            return false;
        }
        if (i == 4 || (r0 = r4.A08) == null || !r0.A02()) {
            return true;
        }
        return false;
    }

    public static boolean A09(String str) {
        return !TextUtils.isEmpty(str) && !"UNSET".equals(str);
    }

    public static boolean A0A(String str, int i) {
        AbstractMap abstractMap;
        Number number;
        int i2 = 1;
        if (!TextUtils.isEmpty(str)) {
            HashMap hashMap = AnonymousClass4HR.A02;
            if (!(!hashMap.containsKey(str) || (abstractMap = (AbstractMap) hashMap.get(str)) == null || (number = (Number) Collections.max(abstractMap.values())) == null)) {
                i2 = number.intValue();
            }
        }
        return i2 >= i;
    }
}
