package X;

import com.whatsapp.util.Log;

/* renamed from: X.1iu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35871iu {
    public final /* synthetic */ AnonymousClass1BS A00;
    public final /* synthetic */ C35881iv A01;
    public final /* synthetic */ boolean A02;

    public C35871iu(AnonymousClass1BS r1, C35881iv r2, boolean z) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = z;
    }

    public void A00(int i) {
        StringBuilder sb = new StringBuilder("ChatSupportTicketManager/contactSupport/onError, errorCode=");
        sb.append(i);
        Log.e(sb.toString());
        C35891iw r2 = this.A01.A00;
        if (r2.A02 != null) {
            StringBuilder sb2 = new StringBuilder("ContactUsActivity/createTicketIq/onError/errorCode=");
            sb2.append(i);
            sb2.append(" falling back to email support.");
            Log.e(sb2.toString());
            r2.A02.AaN();
            r2.A01();
        }
    }
}
