package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.URLSpan;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.IDxAListenerShape7S0100000_3_I1;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.facebook.redex.IDxCListenerShape11S0100000_3_I1;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.RoundedBottomSheetDialogFragment;
import com.whatsapp.base.WaDialogFragment;
import com.whatsapp.base.WaFragment;
import com.whatsapp.contact.picker.ContactPicker;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.payments.ui.PaymentContactPickerFragment;
import com.whatsapp.payments.ui.PaymentSettingsFragment;
import com.whatsapp.payments.ui.ReTosFragment;
import com.whatsapp.payments.ui.invites.PaymentInviteFragment;
import com.whatsapp.payments.ui.widget.DebitCardInputText;
import com.whatsapp.payments.ui.widget.NoviSelfieFaceAnimationView;
import com.whatsapp.wabloks.base.BkFragment;
import com.whatsapp.wabloks.base.BkScreenFragment;
import com.whatsapp.wabloks.base.FdsContentFragmentManager;
import com.whatsapp.wabloks.base.Hilt_FdsContentFragmentManager;
import com.whatsapp.wabloks.ui.bottomsheet.BkBottomSheetContentFragment;
import com.whatsapp.wabloks.ui.bottomsheet.Hilt_BkBottomSheetContentFragment;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import org.json.JSONObject;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.5Zj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C117295Zj {
    public static float A00(Context context, View view) {
        if (view.canScrollVertically(-1)) {
            return context.getResources().getDimension(R.dimen.actionbar_elevation);
        }
        return 0.0f;
    }

    public static float A01(View view, float f) {
        return (float) ((int) (f * ((float) (view.getResources().getDisplayMetrics().densityDpi / 160))));
    }

    public static int A02(DebitCardInputText debitCardInputText) {
        debitCardInputText.A02();
        debitCardInputText.A01 = 1.0f;
        debitCardInputText.A02 = 2.0f;
        debitCardInputText.A03 = 24.0f;
        debitCardInputText.A04 = 8.0f;
        debitCardInputText.A06 = 4;
        return 4;
    }

    public static long A03(C14830m7 r3) {
        return r3.A00() / 1000;
    }

    public static Intent A04(String str) {
        return new Intent("android.intent.action.VIEW", Uri.parse(str));
    }

    public static SharedPreferences.Editor A05(C18600si r0) {
        return r0.A01().edit();
    }

    public static Uri A06(Object obj, String str) {
        String obj2 = obj.toString();
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter("locale", obj2);
        buildUpon.appendQueryParameter("risk_period_id", AnonymousClass600.A03);
        return Uri.parse(buildUpon.toString()).buildUpon().build();
    }

    public static ImageView A07(Context context, View view, TextView textView, int i) {
        textView.setText(context.getString(i));
        ImageView imageView = (ImageView) AnonymousClass028.A0D(view, R.id.icon);
        imageView.setImageResource(R.drawable.ic_close);
        return imageView;
    }

    public static C1310160w A08(String str, String str2) {
        return new C1310160w("", str, str2, -1);
    }

    public static AnonymousClass2FL A09(AbstractActivityC13850kR r0) {
        return (AnonymousClass2FL) ((AnonymousClass2FJ) r0.generatedComponent());
    }

    public static AnonymousClass01J A0A(C51112Sw r1, RoundedBottomSheetDialogFragment roundedBottomSheetDialogFragment) {
        AnonymousClass01J r12 = r1.A0Y;
        roundedBottomSheetDialogFragment.A00 = (AnonymousClass182) r12.A94.get();
        roundedBottomSheetDialogFragment.A01 = (AnonymousClass180) r12.ALt.get();
        return r12;
    }

    public static AnonymousClass01J A0B(C51112Sw r1, ContactPickerFragment contactPickerFragment) {
        AnonymousClass01J r12 = r1.A0Y;
        ((WaFragment) contactPickerFragment).A00 = (AnonymousClass182) r12.A94.get();
        ((WaFragment) contactPickerFragment).A01 = (AnonymousClass180) r12.ALt.get();
        contactPickerFragment.A13 = (C14830m7) r12.ALb.get();
        contactPickerFragment.A1O = (C14850m9) r12.A04.get();
        contactPickerFragment.A1e = (AnonymousClass1CY) r12.ABO.get();
        contactPickerFragment.A0K = (C14900mE) r12.A8X.get();
        contactPickerFragment.A0L = (C15570nT) r12.AAr.get();
        return r12;
    }

    public static AnonymousClass01J A0C(PaymentSettingsFragment paymentSettingsFragment, Object obj) {
        AnonymousClass01J r1 = ((C51112Sw) obj).A0Y;
        ((WaDialogFragment) paymentSettingsFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaDialogFragment) paymentSettingsFragment).A01 = (AnonymousClass180) r1.ALt.get();
        paymentSettingsFragment.A0P = (C14830m7) r1.ALb.get();
        paymentSettingsFragment.A0Q = (C16590pI) r1.AMg.get();
        return r1;
    }

    public static C30821Yy A0D(Object obj, BigDecimal bigDecimal) {
        return new C30821Yy(bigDecimal, ((AbstractC30781Yu) obj).A01);
    }

    public static AnonymousClass1ZR A0E(Object obj) {
        return new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, obj, "upiHandle");
    }

    public static C1310460z A0F(Object obj, Object[] objArr, int i) {
        objArr[i] = obj;
        return new C1310460z("account", new ArrayList(Arrays.asList(objArr)));
    }

    public static C30931Zj A0G(String str) {
        return C30931Zj.A00(str, "payment-settings", "IN");
    }

    public static AnonymousClass1V8 A0H(AnonymousClass1V8 r2, C41141sy r3) {
        r3.A07(r2, new ArrayList());
        r3.A09(r2, Arrays.asList(new String[0]), new ArrayList());
        return r3.A03();
    }

    public static AnonymousClass1V8 A0I(C41141sy r3, C41141sy r4, AnonymousClass3CS r5) {
        r4.A05(r3.A03());
        r4.A07(r5.A00, new ArrayList());
        r5.A00(r4, Arrays.asList(new String[0]));
        return r4.A03();
    }

    public static AnonymousClass1V8 A0J(C41141sy r3, C41141sy r4, AnonymousClass3CT r5) {
        r4.A05(r3.A03());
        r4.A07(r5.A00, new ArrayList());
        r5.A00(r4, Arrays.asList(new String[0]));
        return r4.A03();
    }

    public static AnonymousClass1V8 A0K(AbstractCollection abstractCollection) {
        return new AnonymousClass1V8("account", (AnonymousClass1W9[]) abstractCollection.toArray(new AnonymousClass1W9[0]));
    }

    public static AnonymousClass1V8 A0L(AbstractCollection abstractCollection, AnonymousClass1V8[] r3) {
        return new AnonymousClass1V8("account", (AnonymousClass1W9[]) abstractCollection.toArray(new AnonymousClass1W9[0]), r3);
    }

    public static C41141sy A0M() {
        return new C41141sy("iq");
    }

    public static C41141sy A0N(C41141sy r3) {
        r3.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
        r3.A04(new AnonymousClass1W9("xmlns", "w:pay"));
        return new C41141sy("account");
    }

    public static C41141sy A0O(C41141sy r3) {
        r3.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
        return new C41141sy("account");
    }

    public static ByteArrayOutputStream A0P(String str, KeyStore.PrivateKeyEntry privateKeyEntry, byte[] bArr) {
        Cipher instance = Cipher.getInstance(str);
        instance.init(1, privateKeyEntry.getCertificate().getPublicKey());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        CipherOutputStream cipherOutputStream = new CipherOutputStream(byteArrayOutputStream, instance);
        cipherOutputStream.write(bArr);
        cipherOutputStream.close();
        return byteArrayOutputStream;
    }

    public static Object A0Q(AnonymousClass01J r1, PaymentSettingsFragment paymentSettingsFragment) {
        paymentSettingsFragment.A0T = r1.A3M();
        paymentSettingsFragment.A0h = (C129135xE) r1.AFj.get();
        paymentSettingsFragment.A0O = (AnonymousClass01d) r1.ALI.get();
        paymentSettingsFragment.A0R = (AnonymousClass018) r1.ANb.get();
        paymentSettingsFragment.A0g = (C17070qD) r1.AFC.get();
        paymentSettingsFragment.A0b = (C18600si) r1.AEo.get();
        paymentSettingsFragment.A0K = (AnonymousClass116) r1.A3z.get();
        paymentSettingsFragment.A0U = (C18810t5) r1.AMu.get();
        paymentSettingsFragment.A0W = (C21860y6) r1.AE6.get();
        paymentSettingsFragment.A0d = (C18610sj) r1.AF0.get();
        paymentSettingsFragment.A0f = (C22710zW) r1.AF7.get();
        return r1.AEk.get();
    }

    public static Object A0R(AnonymousClass1ZR r0) {
        if (r0 == null) {
            return null;
        }
        return r0.A00;
    }

    public static String A0S(Context context, C004802e r2, int i) {
        r2.A0A(context.getString(i));
        return context.getString(R.string.ok);
    }

    public static String A0T(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static String A0U(C15570nT r1, C14830m7 r2) {
        byte[] A01 = AnonymousClass15O.A01(r1, r2, false);
        AnonymousClass009.A05(A01);
        return C003501n.A03(A01);
    }

    public static String A0V(AnonymousClass018 r3, String str, long j) {
        return new SimpleDateFormat(str, AnonymousClass018.A00(r3.A00)).format(new Date(j * 1000));
    }

    public static String A0W(AnonymousClass1V8 r1, String str) {
        return r1.A0I(str, null);
    }

    public static String A0X(Object obj, Map map) {
        Object obj2 = map.get(obj);
        AnonymousClass009.A05(obj2);
        return (String) obj2;
    }

    public static List A0Y(C125465rI r0, AbstractC130695zp r1) {
        return (List) r1.A01(r0);
    }

    public static List A0Z(C17070qD r0) {
        r0.A03();
        return r0.A09.A0B();
    }

    public static JSONObject A0a() {
        return new JSONObject();
    }

    public static JSONObject A0b(C18600si r1) {
        String A04 = r1.A04();
        if (TextUtils.isEmpty(A04)) {
            return new JSONObject();
        }
        return new JSONObject(A04);
    }

    public static JSONObject A0c(C18600si r1) {
        String A04 = r1.A04();
        if (TextUtils.isEmpty(A04)) {
            return new JSONObject();
        }
        return new JSONObject(A04);
    }

    public static void A0d(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.getWindow().addFlags(Integer.MIN_VALUE);
        }
    }

    public static void A0e(Activity activity) {
        Window window = activity.getWindow();
        if (window != null) {
            window.addFlags(DefaultCrypto.BUFFER_SIZE);
        }
    }

    public static void A0f(Context context, Resources resources, Object obj, Object[] objArr) {
        objArr[0] = obj;
        String string = resources.getString(R.string.order_details_order_expired_message, objArr);
        C004802e r2 = new C004802e(context);
        r2.A0B(false);
        r2.A07(R.string.order_details_order_expired_dialog_title);
        r2.A0A(string);
        r2.setPositiveButton(R.string.order_details_order_expired_dialog_button_text, null);
        r2.create().show();
    }

    public static void A0g(Context context, AbstractC005102i r1, int i) {
        r1.A0I(context.getString(i));
        r1.A0M(true);
    }

    public static void A0h(Context context, AbstractC005102i r2, int i, int i2) {
        r2.A0A(i);
        r2.A0M(true);
        r2.A0D(AnonymousClass2GE.A04(context.getResources().getDrawable(R.drawable.ic_close), i2));
    }

    public static void A0i(Context context, NoviSelfieFaceAnimationView noviSelfieFaceAnimationView) {
        noviSelfieFaceAnimationView.A01();
        noviSelfieFaceAnimationView.A00 = 0;
        noviSelfieFaceAnimationView.A0F = false;
        noviSelfieFaceAnimationView.A0E = true;
        noviSelfieFaceAnimationView.A03(context);
    }

    public static void A0j(Context context, String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", str);
        intent.setType("text/plain");
        context.startActivity(Intent.createChooser(intent, null));
    }

    public static void A0k(Resources resources, Paint paint) {
        paint.setColor(resources.getColor(R.color.hyper_link_color));
        paint.setUnderlineText(false);
    }

    public static void A0l(SpannableString spannableString, String str, String str2, String str3) {
        URLSpan uRLSpan = new URLSpan(str);
        int indexOf = str2.indexOf(str3);
        spannableString.setSpan(uRLSpan, indexOf, indexOf + str3.length(), 33);
    }

    public static void A0m(View view, int i, int i2) {
        AnonymousClass2GE.A07((ImageView) view.findViewById(i), i2);
    }

    public static void A0n(View view, Object obj, int i) {
        view.setOnClickListener(new IDxCListenerShape11S0100000_3_I1(obj, i));
    }

    public static void A0o(View view, Object obj, Object obj2, int i) {
        view.setOnClickListener(new IDxCListenerShape2S0200000_3_I1(obj, i, obj2));
    }

    public static void A0p(ActivityC001000l r1, int i) {
        r1.A0R(new IDxAListenerShape7S0100000_3_I1(r1, i));
    }

    public static void A0q(C004802e r1, Object obj, int i, int i2) {
        r1.setPositiveButton(i2, new IDxCListenerShape10S0100000_3_I1(obj, i));
    }

    public static void A0r(AbstractC001200n r1, AnonymousClass017 r2, int i) {
        r2.A05(r1, new IDxObserverShape5S0100000_3_I1(r1, i));
    }

    public static void A0s(AbstractC001200n r1, AnonymousClass017 r2, Object obj, int i) {
        r2.A05(r1, new IDxObserverShape5S0100000_3_I1(obj, i));
    }

    public static void A0t(IDxAListenerShape7S0100000_3_I1 iDxAListenerShape7S0100000_3_I1) {
        ((AbstractActivityC13850kR) iDxAListenerShape7S0100000_3_I1.A00).A1k();
    }

    public static void A0u(AnonymousClass2FL r1, AnonymousClass01J r2, ContactPicker contactPicker) {
        ((AbstractActivityC28171Kz) contactPicker).A04 = (C17050qB) r2.ABL.get();
        ((AbstractActivityC28171Kz) contactPicker).A0C = (C20740wF) r2.AIA.get();
        ((AbstractActivityC28171Kz) contactPicker).A0D = (C18350sJ) r2.AHX.get();
        ((AbstractActivityC28171Kz) contactPicker).A06 = (C20820wN) r2.ACG.get();
        ((AbstractActivityC28171Kz) contactPicker).A08 = (C26041Bu) r2.ACL.get();
        ((AbstractActivityC28171Kz) contactPicker).A00 = (AnonymousClass2FR) r1.A0P.get();
        ((AbstractActivityC28171Kz) contactPicker).A07 = (C20850wQ) r2.ACI.get();
        contactPicker.A01 = (C239613r) r2.AI9.get();
        contactPicker.A00 = (C20640w5) r2.AHk.get();
        contactPicker.A02 = (C16170oZ) r2.AM4.get();
        contactPicker.A06 = (WhatsAppLibLoader) r2.ANa.get();
    }

    public static void A0v(C51112Sw r1, AnonymousClass01J r2, ContactPickerFragment contactPickerFragment) {
        contactPickerFragment.A18 = new AnonymousClass3CP((C14830m7) r1.A0V.A1E.ALb.get());
        contactPickerFragment.A1Y = (C16630pM) r2.AIc.get();
        contactPickerFragment.A11 = (C18640sm) r2.A3u.get();
        contactPickerFragment.A0g = (C26311Cv) r2.AAQ.get();
    }

    public static void A0w(C51112Sw r1, AnonymousClass01J r2, BkScreenFragment bkScreenFragment) {
        ((BkFragment) bkScreenFragment).A06 = C18000rk.A00(r2.AGe);
        bkScreenFragment.A03 = r2.A2U();
        bkScreenFragment.A04 = (C17120qI) r2.ALs.get();
        bkScreenFragment.A02 = (C48912Ik) r1.A0V.A19.get();
        bkScreenFragment.A05 = r2.A4d();
    }

    public static void A0x(AnonymousClass01J r1, AbstractActivityC28171Kz r2, C249317l r3) {
        ((ActivityC13790kL) r2).A08 = r3;
        r2.A05 = (C20650w6) r1.A3A.get();
        r2.A09 = (C18470sV) r1.AK8.get();
        r2.A02 = (C20670w8) r1.AMw.get();
        r2.A03 = (C15550nR) r1.A45.get();
        r2.A0B = (C19890uq) r1.ABw.get();
        r2.A0A = (C20710wC) r1.A8m.get();
        r2.A0E = (AbstractC15850o0) r1.ANA.get();
    }

    public static void A0y(AnonymousClass01J r1, AnonymousClass27X r2) {
        r2.A0G = (AnonymousClass01d) r1.ALI.get();
        r2.A0I = (C21200x2) r1.A2q.get();
        r2.A0J = (C16630pM) r1.AIc.get();
        r2.A0H = (C15890o4) r1.AN1.get();
    }

    public static void A0z(AnonymousClass01J r1, ContactPickerFragment contactPickerFragment) {
        contactPickerFragment.A1Z = (C15860o1) r1.A3H.get();
        contactPickerFragment.A1P = (C22050yP) r1.A7v.get();
        contactPickerFragment.A0T = (C18330sH) r1.AN4.get();
        contactPickerFragment.A1U = (AnonymousClass1E5) r1.AKs.get();
        contactPickerFragment.A0a = (C22330yu) r1.A3I.get();
        contactPickerFragment.A0w = (C20730wE) r1.A4J.get();
        contactPickerFragment.A1B = (C255019q) r1.A50.get();
        contactPickerFragment.A0c = (AnonymousClass116) r1.A3z.get();
        contactPickerFragment.A1G = (C16490p7) r1.ACJ.get();
        contactPickerFragment.A0h = (C22700zV) r1.AMN.get();
        contactPickerFragment.A14 = (C15890o4) r1.AN1.get();
    }

    public static void A10(AnonymousClass01J r1, ContactPickerFragment contactPickerFragment) {
        contactPickerFragment.A0i = (C15610nY) r1.AMe.get();
        contactPickerFragment.A1X = (C17070qD) r1.AFC.get();
        contactPickerFragment.A16 = (AnonymousClass018) r1.ANb.get();
        contactPickerFragment.A1H = (AnonymousClass1BK) r1.AFZ.get();
        contactPickerFragment.A0V = (C238013b) r1.A1Z.get();
        contactPickerFragment.A0f = (AnonymousClass10S) r1.A46.get();
        contactPickerFragment.A0v = (C253318z) r1.A4B.get();
        contactPickerFragment.A0J = (C22680zT) r1.AGW.get();
        contactPickerFragment.A1b = (AnonymousClass12F) r1.AJM.get();
        contactPickerFragment.A0s = (C21240x6) r1.AA7.get();
        contactPickerFragment.A1f = (AnonymousClass198) r1.A0J.get();
    }

    public static void A11(AnonymousClass01J r1, ContactPickerFragment contactPickerFragment) {
        contactPickerFragment.A0N = (C239613r) r1.AI9.get();
        contactPickerFragment.A1j = (AbstractC14440lR) r1.ANe.get();
        contactPickerFragment.A1C = (C19990v2) r1.A3M.get();
        contactPickerFragment.A0H = (AnonymousClass17S) r1.A0F.get();
        contactPickerFragment.A1Q = (C16120oU) r1.ANE.get();
        contactPickerFragment.A0P = (C18790t3) r1.AJw.get();
        contactPickerFragment.A1N = (AnonymousClass19M) r1.A6R.get();
        contactPickerFragment.A0O = (C15450nH) r1.AII.get();
        contactPickerFragment.A1J = (C18470sV) r1.AK8.get();
        contactPickerFragment.A0Q = (AnonymousClass1AR) r1.ALM.get();
        contactPickerFragment.A0S = (C16170oZ) r1.AM4.get();
    }

    public static void A12(AnonymousClass01J r1, ContactPickerFragment contactPickerFragment) {
        contactPickerFragment.A15 = (C14820m6) r1.AN3.get();
        contactPickerFragment.A0b = (AnonymousClass1BF) r1.A3b.get();
        contactPickerFragment.A0y = (AnonymousClass16P) r1.A4K.get();
        contactPickerFragment.A1D = (C15680nj) r1.A4e.get();
        contactPickerFragment.A1E = (C19780uf) r1.A8E.get();
        contactPickerFragment.A10 = (C15220ml) r1.A4V.get();
        contactPickerFragment.A1h = (C15690nk) r1.A74.get();
        contactPickerFragment.A0W = (C251118d) r1.A2D.get();
        contactPickerFragment.A1M = (AnonymousClass1AN) r1.A5T.get();
        contactPickerFragment.A0X = (C16430p0) r1.A5y.get();
        contactPickerFragment.A1F = (C15600nX) r1.A8x.get();
    }

    public static void A13(AnonymousClass01J r1, ContactPickerFragment contactPickerFragment) {
        contactPickerFragment.A1k = (C26501Ds) r1.AML.get();
        contactPickerFragment.A1g = (C22650zQ) r1.A4n.get();
        contactPickerFragment.A0k = (C21270x9) r1.A4A.get();
        contactPickerFragment.A1W = (C17220qS) r1.ABt.get();
        contactPickerFragment.A1a = (AnonymousClass17R) r1.AIz.get();
        contactPickerFragment.A1R = (C20710wC) r1.A8m.get();
        contactPickerFragment.A0I = (AnonymousClass12P) r1.A0H.get();
        contactPickerFragment.A0d = (C15550nR) r1.A45.get();
        contactPickerFragment.A1c = (C252018m) r1.A7g.get();
        contactPickerFragment.A1i = (C22190yg) r1.AB6.get();
        contactPickerFragment.A12 = (AnonymousClass01d) r1.ALI.get();
    }

    public static void A14(AnonymousClass01J r1, PaymentContactPickerFragment paymentContactPickerFragment) {
        paymentContactPickerFragment.A1A = (C21550xb) r1.AAj.get();
        paymentContactPickerFragment.A0u = (C17010q7) r1.A43.get();
        paymentContactPickerFragment.A0z = (AnonymousClass1DP) r1.A4N.get();
        paymentContactPickerFragment.A1T = (C244215l) r1.A8y.get();
        paymentContactPickerFragment.A0t = (AnonymousClass118) r1.A42.get();
        ((ContactPickerFragment) paymentContactPickerFragment).A0Y = (AnonymousClass11I) r1.A2F.get();
        paymentContactPickerFragment.A1L = (C25951Bl) r1.A2O.get();
        paymentContactPickerFragment.A00 = (C21860y6) r1.AE6.get();
        paymentContactPickerFragment.A02 = (C22710zW) r1.AF7.get();
        paymentContactPickerFragment.A01 = (AnonymousClass18T) r1.AE9.get();
        paymentContactPickerFragment.A07 = (C129395xe) r1.AEy.get();
    }

    public static void A15(AnonymousClass01J r1, PaymentSettingsFragment paymentSettingsFragment) {
        paymentSettingsFragment.A0N = (C18640sm) r1.A3u.get();
        paymentSettingsFragment.A0Y = (C18650sn) r1.AEe.get();
        paymentSettingsFragment.A0i = (AnonymousClass1A7) r1.AEs.get();
        paymentSettingsFragment.A0n = (AnonymousClass61E) r1.AEY.get();
        paymentSettingsFragment.A0j = (AnonymousClass60T) r1.AFI.get();
        paymentSettingsFragment.A0Z = (C18660so) r1.AEf.get();
        paymentSettingsFragment.A0c = (C243515e) r1.AEt.get();
        paymentSettingsFragment.A0X = (C20400vh) r1.AE8.get();
        paymentSettingsFragment.A0V = (C20380vf) r1.ACR.get();
        paymentSettingsFragment.A0a = (C20390vg) r1.AEi.get();
        paymentSettingsFragment.A0u = (C129395xe) r1.AEy.get();
    }

    public static void A16(AnonymousClass01J r1, PaymentSettingsFragment paymentSettingsFragment) {
        paymentSettingsFragment.A0G = (C15570nT) r1.AAr.get();
        paymentSettingsFragment.A0J = (C18790t3) r1.AJw.get();
        paymentSettingsFragment.A0H = (AnonymousClass19Y) r1.AI6.get();
        paymentSettingsFragment.A11 = (AbstractC14440lR) r1.ANe.get();
        paymentSettingsFragment.A0z = (AnonymousClass14X) r1.AFM.get();
        paymentSettingsFragment.A0L = (C15550nR) r1.A45.get();
        paymentSettingsFragment.A0M = (C15610nY) r1.AMe.get();
        paymentSettingsFragment.A0S = (C14850m9) r1.A04.get();
        paymentSettingsFragment.A0F = (C14900mE) r1.A8X.get();
        paymentSettingsFragment.A10 = (C253118x) r1.AAW.get();
        paymentSettingsFragment.A0I = (C15450nH) r1.AII.get();
    }

    public static void A17(AnonymousClass01J r1, PaymentSettingsFragment paymentSettingsFragment, Object obj) {
        paymentSettingsFragment.A0o = (C130015yf) obj;
        paymentSettingsFragment.A0e = (C17900ra) r1.AF5.get();
        paymentSettingsFragment.A0l = (AnonymousClass17Z) r1.AEZ.get();
    }

    public static void A18(AnonymousClass01J r1, ReTosFragment reTosFragment) {
        reTosFragment.A02 = (AnonymousClass01d) r1.ALI.get();
        reTosFragment.A04 = (C18610sj) r1.AF0.get();
        reTosFragment.A06 = (C22710zW) r1.AF7.get();
        reTosFragment.A03 = (C129925yW) r1.AEW.get();
        reTosFragment.A05 = (AnonymousClass61M) r1.AF1.get();
    }

    public static void A19(AnonymousClass01J r1, PaymentInviteFragment paymentInviteFragment) {
        ((WaFragment) paymentInviteFragment).A00 = (AnonymousClass182) r1.A94.get();
        ((WaFragment) paymentInviteFragment).A01 = (AnonymousClass180) r1.ALt.get();
        paymentInviteFragment.A03 = (C16120oU) r1.ANE.get();
        paymentInviteFragment.A02 = (C21270x9) r1.A4A.get();
        paymentInviteFragment.A00 = (C15550nR) r1.A45.get();
        paymentInviteFragment.A01 = (C15610nY) r1.AMe.get();
        paymentInviteFragment.A05 = (AnonymousClass17V) r1.AEX.get();
        paymentInviteFragment.A04 = (C22480z9) r1.AEc.get();
        paymentInviteFragment.A09 = (C129395xe) r1.AEy.get();
    }

    public static void A1A(C252818u r0, String str, Object[] objArr, int i) {
        objArr[i] = r0.A00(str).toString();
    }

    public static void A1B(C17220qS r7, AbstractC21730xt r8, AnonymousClass1V8 r9, String str) {
        r7.A09(r8, r9, str, 204, 0);
    }

    public static void A1C(C452120p r5, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AnonymousClass6CE r7) {
        int i = r5.A00;
        if (i == 1440) {
            pinBottomSheetDialogFragment.A1O(r5.A01);
        } else if (i == 1441) {
            pinBottomSheetDialogFragment.A1P(r5.A02 * 1000, true);
        } else {
            C129285xT r1 = r7.A01;
            IDxDListenerShape14S0100000_3_I1 iDxDListenerShape14S0100000_3_I1 = new IDxDListenerShape14S0100000_3_I1(pinBottomSheetDialogFragment, 23);
            IDxDListenerShape14S0100000_3_I1 iDxDListenerShape14S0100000_3_I12 = new IDxDListenerShape14S0100000_3_I1(pinBottomSheetDialogFragment, 24);
            ActivityC13790kL r12 = r1.A01;
            AnonymousClass04S A00 = AnonymousClass61M.A00(r12, iDxDListenerShape14S0100000_3_I1, iDxDListenerShape14S0100000_3_I12, i);
            if (A00 == null && (A00 = AnonymousClass61M.A01(r12, iDxDListenerShape14S0100000_3_I1, iDxDListenerShape14S0100000_3_I12, null, i)) == null) {
                A00 = AnonymousClass61M.A02(r12, iDxDListenerShape14S0100000_3_I12, r12.getString(R.string.payments_generic_error));
            }
            A00.show();
        }
    }

    public static void A1D(C452120p r1, AbstractMap abstractMap) {
        abstractMap.put("error_code", String.valueOf(r1.A00));
    }

    public static void A1E(C18600si r1, Object obj) {
        r1.A0F(obj.toString());
    }

    public static void A1F(C30931Zj r1, Object obj) {
        r1.A06(obj.toString());
    }

    public static void A1G(C118035bA r4, Object obj) {
        AnonymousClass1ZX r5 = (AnonymousClass1ZX) obj;
        if (!TextUtils.isEmpty(r5.A0B)) {
            C128215vk r3 = new C128215vk(3);
            r3.A05 = r5.A0B.replaceAll("[^\\d]", "");
            r4.A03.A0B(r3);
        }
    }

    public static void A1H(C41141sy r0, C41141sy r1) {
        r1.A05(r0.A03());
    }

    public static void A1I(Hilt_FdsContentFragmentManager hilt_FdsContentFragmentManager) {
        hilt_FdsContentFragmentManager.A18();
        if (!hilt_FdsContentFragmentManager.A02) {
            hilt_FdsContentFragmentManager.A02 = true;
            ((FdsContentFragmentManager) hilt_FdsContentFragmentManager).A01 = (C17120qI) ((C51112Sw) ((AbstractC51092Su) hilt_FdsContentFragmentManager.generatedComponent())).A0Y.ALs.get();
        }
    }

    public static void A1J(Hilt_BkBottomSheetContentFragment hilt_BkBottomSheetContentFragment) {
        hilt_BkBottomSheetContentFragment.A1G();
        if (!hilt_BkBottomSheetContentFragment.A02) {
            hilt_BkBottomSheetContentFragment.A02 = true;
            BkBottomSheetContentFragment bkBottomSheetContentFragment = (BkBottomSheetContentFragment) hilt_BkBottomSheetContentFragment;
            AnonymousClass01J r1 = ((C51112Sw) ((AbstractC51092Su) hilt_BkBottomSheetContentFragment.generatedComponent())).A0Y;
            ((BkFragment) bkBottomSheetContentFragment).A06 = C18000rk.A00(r1.AGe);
            bkBottomSheetContentFragment.A04 = C18000rk.A00(r1.A1V);
        }
    }

    public static void A1K(Object obj, Object obj2, Object obj3, Object obj4) {
        C16700pc.A0E(obj, 2);
        C16700pc.A0E(obj2, 3);
        C16700pc.A0E(obj3, 4);
        C16700pc.A0E(obj4, 5);
    }

    public static void A1L(Object obj, Object obj2, JSONObject jSONObject, long j) {
        jSONObject.put("risk_period_uuid", obj);
        jSONObject.put("app_install_uuid", obj2);
        jSONObject.put("client_timestamp_ms", j);
    }

    public static void A1M(String str, String str2, AbstractCollection abstractCollection) {
        abstractCollection.add(new AnonymousClass1W9(str, str2));
    }

    public static void A1N(String str, String str2, AbstractCollection abstractCollection) {
        abstractCollection.add(new C1310160w("", str, str2, -1));
    }

    public static void A1O(String str, String str2, List list) {
        list.add(new AnonymousClass1W9(str, str2));
    }

    public static void A1P(String str, String str2, Object[] objArr) {
        objArr[2] = new AnonymousClass1W9(str, str2);
    }

    public static void A1Q(AbstractCollection abstractCollection, int i) {
        if (i != -1) {
            abstractCollection.add(Integer.valueOf(i));
        }
    }

    public static void A1R(AbstractMap abstractMap, Iterator it) {
        AnonymousClass5TM r1 = (AnonymousClass5TM) it.next();
        if (r1 instanceof C1097953b) {
            C44691zO r12 = ((C1097953b) r1).A00;
            List list = r12.A06;
            if (!list.isEmpty()) {
                abstractMap.put(r12.A0D, list.get(0));
            }
        }
    }

    public static boolean A1S(C125465rI r0, AbstractC130695zp r1) {
        return ((Boolean) r1.A01(r0)).booleanValue();
    }

    public static boolean A1T(AnonymousClass1V8 r0, String str, String str2, String str3) {
        return str3.equals(r0.A0I(str, str2));
    }

    public static boolean A1U(String str) {
        return AnonymousClass3JT.A0E(str, 1, 100, true);
    }

    public static boolean A1V(String str, long j, boolean z) {
        return AnonymousClass3JT.A0E(str, j, 255, z);
    }

    public static boolean A1W(String str, long j, boolean z) {
        return AnonymousClass3JT.A0E(str, j, 1000, z);
    }

    public static boolean A1X(String str, boolean z) {
        return AnonymousClass3JT.A0E(str, 0, 100, z);
    }

    public static boolean A1Y(String str, boolean z) {
        return AnonymousClass3JT.A0E(str, 0, 9007199254740991L, z);
    }

    public static boolean A1Z(FormItemEditText formItemEditText) {
        formItemEditText.A0E = null;
        formItemEditText.A0G = null;
        formItemEditText.A0F = null;
        formItemEditText.A06 = 0;
        formItemEditText.A04 = 24.0f;
        formItemEditText.A03 = 4.0f;
        formItemEditText.A05 = 8.0f;
        return false;
    }

    public static int[] A1a(int i) {
        int[] iArr = new int[i];
        iArr[0] = 1;
        iArr[1] = 2;
        iArr[2] = 3;
        iArr[3] = 4;
        iArr[4] = 5;
        iArr[5] = 6;
        iArr[6] = 7;
        return iArr;
    }

    public static int[][] A1b(FormItemEditText formItemEditText) {
        formItemEditText.A0J = false;
        formItemEditText.A01 = 1.0f;
        formItemEditText.A02 = 2.0f;
        formItemEditText.A0H = false;
        formItemEditText.A0I = false;
        return new int[][]{new int[]{16842913}, new int[]{16842914}, new int[]{16842908}, new int[]{-16842908}};
    }
}
