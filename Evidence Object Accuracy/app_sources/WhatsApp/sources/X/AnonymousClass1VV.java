package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1VV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1VV extends AnonymousClass1GS {
    public final /* synthetic */ C15570nT A00;
    public final /* synthetic */ C21580xe A01;

    public AnonymousClass1VV(C15570nT r1, C21580xe r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final int A00(Collection collection) {
        Iterator it = collection.iterator();
        int i = 0;
        while (it.hasNext()) {
            C15370n3 r2 = (C15370n3) it.next();
            if (r2 != null && r2.A0f && r2.A0C != null && !this.A00.A0F(r2.A0D)) {
                i++;
            }
        }
        return i;
    }
}
