package X;

import com.whatsapp.payments.ui.viewmodel.BusinessHubViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.1IY  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1IY implements AnonymousClass1FK {
    public final /* synthetic */ BusinessHubViewModel A00;

    public AnonymousClass1IY(BusinessHubViewModel businessHubViewModel) {
        this.A00 = businessHubViewModel;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        C16700pc.A0E(r4, 0);
        StringBuilder sb = new StringBuilder("PAY: BusinessHubViewModel unlinkMerchantAccount/onRequestError paymentNetworkError: [");
        sb.append(r4.A00);
        sb.append("] ");
        sb.append((Object) r4.A07);
        Log.e(sb.toString());
        ((AnonymousClass017) this.A00.A0A.getValue()).A0A(AnonymousClass617.A02(null, new RuntimeException(r4.A07)));
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        C16700pc.A0E(r4, 0);
        StringBuilder sb = new StringBuilder("PAY: BusinessHubViewModel unlinkMerchantAccount/onRequestError paymentNetworkError: [");
        sb.append(r4.A00);
        sb.append("] ");
        sb.append((Object) r4.A07);
        Log.e(sb.toString());
        ((AnonymousClass017) this.A00.A0A.getValue()).A0A(AnonymousClass617.A02(null, new RuntimeException(r4.A07)));
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r3) {
        Log.i("PAY: BusinessHubViewModel unlinkMerchantAccount/onResponseSuccess");
        ((AnonymousClass017) this.A00.A0A.getValue()).A0A(AnonymousClass617.A01(null));
    }
}
