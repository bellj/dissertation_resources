package X;

import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1JH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JH implements AnonymousClass1JI {
    public final /* synthetic */ C37241lo A00;
    public final /* synthetic */ C22950zu A01;

    public AnonymousClass1JH(C37241lo r1, C22950zu r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1JI
    public void AOv(UserJid userJid) {
        this.A00.A00(1);
    }

    @Override // X.AnonymousClass1JI
    public void APn(UserJid userJid, int i) {
        this.A01.A00.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(this, 44));
    }

    @Override // X.AnonymousClass1JI
    public void AT4(UserJid userJid) {
        this.A00.A00(1);
    }

    @Override // X.AnonymousClass1JI
    public void AWV(UserJid userJid, String str, long j) {
        this.A00.A00(1);
    }
}
