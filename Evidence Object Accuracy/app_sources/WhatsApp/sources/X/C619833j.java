package X;

import android.content.Context;

/* renamed from: X.33j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C619833j extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C18610sj A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C619833j(Context context, C14900mE r2, AnonymousClass1FK r3, C18650sn r4, C18610sj r5) {
        super(context, r2, r4);
        this.A01 = r5;
        this.A00 = r3;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r3) {
        this.A01.A0I.A06(C12960it.A0b("Tos onRequestError: ", r3));
        this.A00.AV3(r3);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r3) {
        this.A01.A0I.A06(C12960it.A0b("Tos onResponseError: ", r3));
        this.A00.AVA(r3);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r6) {
        AnonymousClass1V8 A0E = r6.A0E("accept_pay");
        AnonymousClass46N r3 = new AnonymousClass46N();
        if (A0E != null) {
            r3.A02 = "1".equals(A0E.A0I("accept", null));
            r3.A00 = "1".equals(A0E.A0I("outage", null));
            boolean equals = "1".equals(A0E.A0I("sandbox", null));
            r3.A01 = equals;
            this.A01.A0D.A0L(equals);
        } else {
            r3.A02 = false;
        }
        this.A00.AVB(r3);
    }
}
