package X;

import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.WeakHashMap;

/* renamed from: X.0Tg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06350Tg {
    public static final ArrayList A03 = new ArrayList();
    public SparseArray A00 = null;
    public WeakReference A01 = null;
    public WeakHashMap A02 = null;

    public static final void A00(View view) {
        int size;
        AbstractList abstractList = (AbstractList) view.getTag(R.id.tag_unhandled_key_listeners);
        if (abstractList != null && (size = abstractList.size() - 1) >= 0) {
            abstractList.get(size);
            throw new NullPointerException("onUnhandledKeyEvent");
        }
    }

    public final View A01(KeyEvent keyEvent, View view) {
        View A01;
        WeakHashMap weakHashMap = this.A02;
        if (weakHashMap != null && weakHashMap.containsKey(view)) {
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                do {
                    childCount--;
                    if (childCount >= 0) {
                        A01 = A01(keyEvent, viewGroup.getChildAt(childCount));
                    }
                } while (A01 == null);
                return A01;
            }
            A00(view);
        }
        return null;
    }
}
