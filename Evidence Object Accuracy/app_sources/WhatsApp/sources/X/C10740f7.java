package X;

import org.xml.sax.Attributes;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: X.0f7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10740f7 implements Attributes {
    public XmlPullParser A00;
    public final /* synthetic */ C06560Ud A01;

    @Override // org.xml.sax.Attributes
    public int getIndex(String str) {
        return -1;
    }

    @Override // org.xml.sax.Attributes
    public int getIndex(String str, String str2) {
        return -1;
    }

    @Override // org.xml.sax.Attributes
    public String getType(int i) {
        return null;
    }

    @Override // org.xml.sax.Attributes
    public String getType(String str) {
        return null;
    }

    @Override // org.xml.sax.Attributes
    public String getType(String str, String str2) {
        return null;
    }

    @Override // org.xml.sax.Attributes
    public String getValue(String str) {
        return null;
    }

    @Override // org.xml.sax.Attributes
    public String getValue(String str, String str2) {
        return null;
    }

    public C10740f7(C06560Ud r1, XmlPullParser xmlPullParser) {
        this.A01 = r1;
        this.A00 = xmlPullParser;
    }

    @Override // org.xml.sax.Attributes
    public int getLength() {
        return this.A00.getAttributeCount();
    }

    @Override // org.xml.sax.Attributes
    public String getLocalName(int i) {
        return this.A00.getAttributeName(i);
    }

    @Override // org.xml.sax.Attributes
    public String getQName(int i) {
        XmlPullParser xmlPullParser = this.A00;
        String attributeName = xmlPullParser.getAttributeName(i);
        if (xmlPullParser.getAttributePrefix(i) == null) {
            return attributeName;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(xmlPullParser.getAttributePrefix(i));
        sb.append(':');
        sb.append(attributeName);
        return sb.toString();
    }

    @Override // org.xml.sax.Attributes
    public String getURI(int i) {
        return this.A00.getAttributeNamespace(i);
    }

    @Override // org.xml.sax.Attributes
    public String getValue(int i) {
        return this.A00.getAttributeValue(i);
    }
}
