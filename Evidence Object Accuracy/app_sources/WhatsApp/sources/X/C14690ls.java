package X;

import android.media.AudioRecord;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.animation.Animation;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.audioRecording.AudioRecordFactory;
import com.whatsapp.audioRecording.OpusRecorderFactory;
import com.whatsapp.conversation.waveforms.VoiceVisualizer;
import com.whatsapp.util.Log;
import com.whatsapp.util.OpusRecorder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.0ls  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14690ls {
    public long A00;
    public long A01;
    public Animation A02;
    public FileOutputStream A03;
    public boolean A04;
    public final AudioRecord A05;
    public final View A06;
    public final VoiceVisualizer A07;
    public final C14830m7 A08;
    public final OpusRecorder A09;
    public final File A0A;
    public final File A0B;
    public final boolean A0C;
    public final short[] A0D;

    public C14690ls(View view, AudioRecordFactory audioRecordFactory, OpusRecorderFactory opusRecorderFactory, VoiceVisualizer voiceVisualizer, C14830m7 r9, C14850m9 r10, String str) {
        this.A08 = r9;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".opus");
        String obj = sb.toString();
        this.A0A = new File(obj);
        this.A07 = voiceVisualizer;
        this.A06 = view;
        this.A09 = opusRecorderFactory.createOpusRecorder(obj);
        this.A0C = r10.A07(1139);
        int minBufferSize = AudioRecord.getMinBufferSize(44100, 16, 2);
        this.A05 = audioRecordFactory.createAudioRecord(44100, minBufferSize);
        this.A0D = new short[((minBufferSize == -1 || minBufferSize == -2) ? 88200 : minBufferSize) / 2];
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("Visualization.data");
        String obj2 = sb2.toString();
        File file = new File(obj2);
        this.A0B = file;
        try {
            if (file.createNewFile()) {
                this.A03 = new FileOutputStream(file);
                return;
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append("voicerecorder/unable to create visualization file; visualizationPath=");
            sb3.append(obj2);
            Log.w(sb3.toString());
        } catch (IOException e) {
            Log.e("voicerecorder/error creating visualization file ", e);
        }
    }

    public float A00() {
        int read;
        int i;
        int i2 = Build.VERSION.SDK_INT;
        AudioRecord audioRecord = this.A05;
        short[] sArr = this.A0D;
        int length = sArr.length;
        if (i2 >= 23) {
            read = audioRecord.read(sArr, 0, length, 1);
        } else {
            read = audioRecord.read(sArr, 0, length);
        }
        if (read > 0) {
            this.A00 = 0;
            if (this.A04) {
                this.A04 = false;
                i = 31;
                new Handler(this.A06.getContext().getMainLooper()).post(new RunnableBRunnable0Shape13S0100000_I0_13(this, i));
            }
        } else {
            long j = this.A00;
            int i3 = (j > 0 ? 1 : (j == 0 ? 0 : -1));
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (i3 == 0) {
                this.A00 = elapsedRealtime;
            } else if (elapsedRealtime - j > 1000 && !this.A04) {
                this.A04 = true;
                i = 32;
                new Handler(this.A06.getContext().getMainLooper()).post(new RunnableBRunnable0Shape13S0100000_I0_13(this, i));
            }
        }
        short s = 0;
        for (int i4 = 0; i4 < read; i4++) {
            short s2 = sArr[i4];
            if (s2 > s) {
                s = s2;
            }
        }
        if (this.A0C && read == 0) {
            return -1.0f;
        }
        float max = Math.max(0.0f, Math.min((float) ((Math.log((double) s) * 0.25d) - 1.5807000398635864d), 1.0f));
        try {
            FileOutputStream fileOutputStream = this.A03;
            if (fileOutputStream == null) {
                return max;
            }
            fileOutputStream.write((int) (100.0f * max));
            return max;
        } catch (IOException e) {
            Log.e("voicerecorder/getandstorevisualizationvalue/ error writing visualization file data ", e);
            return max;
        }
    }

    public void A01() {
        this.A05.stop();
        this.A00 = 0;
        this.A04 = false;
        this.A07.setVisibility(0);
        this.A06.setVisibility(8);
        Animation animation = this.A02;
        if (animation != null) {
            animation.cancel();
            this.A02 = null;
        }
    }
}
