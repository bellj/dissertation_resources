package X;

import android.os.Message;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.2Kz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49502Kz extends AbstractC49422Kr {
    public final C50232Or A00;
    public final C17230qT A01;

    public C49502Kz(AbstractC15710nm r7, C50232Or r8, C14850m9 r9, C16120oU r10, C450720b r11, C17230qT r12, Map map) {
        super(r7, r9, r10, r11, map);
        this.A01 = r12;
        this.A00 = r8;
    }

    @Override // X.AbstractC49422Kr
    public void A01(AnonymousClass1V8 r25) {
        String str;
        if (AnonymousClass1V8.A02(r25, "stream:error")) {
            this.A00.A01.A08();
            AnonymousClass1V8 A0E = r25.A0E("ack");
            if (A0E != null) {
                String A0I = r25.A0I("id", null);
                String A0I2 = A0E.A0I("id", null);
                if (A0I == null) {
                    A0I = A0I2;
                }
                StringBuilder sb = new StringBuilder("Connection/received ack-kick id=");
                sb.append(A0I);
                Log.w(sb.toString());
                AnonymousClass31J r5 = new AnonymousClass31J();
                C17230qT r3 = this.A01;
                synchronized (r3) {
                    long j = Long.MAX_VALUE;
                    C619333d r6 = null;
                    long j2 = 0;
                    long j3 = 0;
                    long j4 = 0;
                    for (int i : r3.A06) {
                        for (Map.Entry entry : new HashMap(r3.A02(i)).entrySet()) {
                            Long l = (Long) entry.getKey();
                            AnonymousClass1V4 r1 = (AnonymousClass1V4) entry.getValue();
                            if (r1 != null) {
                                j2++;
                                Integer num = r1.A0A;
                                if (num != null && num.intValue() > 0) {
                                    j3++;
                                }
                                if (A0I != null && A0I.equals(r1.A0B)) {
                                    j4++;
                                    long longValue = l.longValue();
                                    if (j > longValue) {
                                        j = longValue;
                                        r6 = r1;
                                    }
                                }
                            }
                        }
                    }
                    r5.A05 = Long.valueOf(j2);
                    r5.A04 = Long.valueOf(j3);
                    r5.A07 = Long.valueOf(j4);
                    if (r6 != null) {
                        r5.A06 = Long.valueOf((long) r3.A02(r6.A02).size());
                        r5.A09 = Long.valueOf(SystemClock.uptimeMillis() - r6.A04);
                        Integer num2 = r6.A0A;
                        if (num2 != null) {
                            r5.A08 = Long.valueOf((long) num2.intValue());
                        }
                        if (r6 instanceof AnonymousClass2R7) {
                            AnonymousClass2R7 r62 = (AnonymousClass2R7) r6;
                            r5.A03 = 2;
                            r5.A02 = Integer.valueOf(r62.A03);
                            r5.A0B = r62.A05;
                        } else if (r6 instanceof AnonymousClass2QR) {
                            AnonymousClass2QR r63 = (AnonymousClass2QR) r6;
                            r5.A03 = 4;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(r63.A01);
                            sb2.append("/");
                            sb2.append(r63.A00);
                            r5.A0A = sb2.toString();
                        } else if (!(r6 instanceof AnonymousClass2LM)) {
                            r5.A03 = 3;
                            r5.A00 = Integer.valueOf(r6.A00);
                        } else {
                            AnonymousClass2LM r64 = (AnonymousClass2LM) r6;
                            r5.A03 = 1;
                            r5.A01 = Integer.valueOf(r64.A00);
                            r5.A02 = Integer.valueOf(r64.A03);
                        }
                    }
                }
                this.A03.A06(r5);
            }
            C450720b r32 = this.A04;
            AnonymousClass1V8[] r12 = r25.A03;
            if (r12 == null || r12.length <= 0) {
                str = "xmpp/reader/read/stream/error";
            } else {
                StringBuilder sb3 = new StringBuilder("xmpp/reader/read/stream/error ");
                AnonymousClass1V8 r13 = r12[0];
                sb3.append(r13.A00);
                sb3.append(" ");
                sb3.append(Arrays.toString(r13.A01));
                str = sb3.toString();
            }
            Log.i(str);
            try {
                int A05 = r25.A05("code", 0);
                AbstractC450820c r33 = r32.A00;
                Message obtain = Message.obtain(null, 0, 158, 0);
                obtain.getData().putInt("errorCode", A05);
                r33.AYY(obtain);
            } catch (AnonymousClass1V9 e) {
                Log.e(e);
            }
        } else if (AnonymousClass1V8.A02(r25, "error") && "479".equalsIgnoreCase(r25.A0I("code", null))) {
            C450720b r14 = this.A04;
            Log.i("xmpp/reader/read/smax/invalid");
            r14.A00.AYY(Message.obtain(null, 0, 237, 0));
        }
    }
}
