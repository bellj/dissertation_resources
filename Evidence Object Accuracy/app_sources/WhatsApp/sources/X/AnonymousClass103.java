package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import java.util.ArrayList;

/* renamed from: X.103  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass103 {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C20370ve A02;
    public final AnonymousClass102 A03;
    public final C22170ye A04;
    public final C17230qT A05;
    public final C20380vf A06;
    public final C20400vh A07;
    public final AnonymousClass1FX A08;
    public final C25861Bc A09;
    public final C20390vg A0A;
    public final C243515e A0B;
    public final C18610sj A0C;
    public final C22710zW A0D;
    public final C17070qD A0E;
    public final C30931Zj A0F = C30931Zj.A00("PaymentsMessageHandler", "infra", "COMMON");

    public AnonymousClass103(AbstractC15710nm r4, C14900mE r5, C20370ve r6, AnonymousClass102 r7, C22170ye r8, C17230qT r9, C20380vf r10, C20400vh r11, AnonymousClass1FX r12, C25861Bc r13, C20390vg r14, C243515e r15, C18610sj r16, C22710zW r17, C17070qD r18) {
        this.A01 = r5;
        this.A00 = r4;
        this.A04 = r8;
        this.A0E = r18;
        this.A05 = r9;
        this.A0C = r16;
        this.A03 = r7;
        this.A02 = r6;
        this.A0B = r15;
        this.A06 = r10;
        this.A0A = r14;
        this.A07 = r11;
        this.A09 = r13;
        this.A08 = r12;
        this.A0D = r17;
    }

    public final void A00(AnonymousClass1V8 r4, AnonymousClass1OT r5, ArrayList arrayList) {
        if (arrayList.size() > 0) {
            AbstractC28901Pl A05 = ((AnonymousClass1ZY) arrayList.get(0)).A05();
            if (A05 != null) {
                C17070qD r0 = this.A0E;
                r0.A03();
                C38051nR r1 = r0.A00;
                AnonymousClass009.A05(r1);
                C14580lf A01 = r1.A01(A05.A0A);
                A01.A00(new AbstractC14590lg(A05, this, r4) { // from class: X.24i
                    public final /* synthetic */ AbstractC28901Pl A00;
                    public final /* synthetic */ AnonymousClass103 A01;
                    public final /* synthetic */ AnonymousClass1V8 A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        AnonymousClass103 r42 = this.A01;
                        AnonymousClass1V8 r3 = this.A02;
                        AbstractC28901Pl r2 = this.A00;
                        C17070qD r02 = r42.A0E;
                        r02.A03();
                        C38051nR r12 = r02.A00;
                        AnonymousClass009.A05(r12);
                        r12.A03(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0015: INVOKE  
                              (r1v0 'r12' X.1nR)
                              (wrap: X.3Wj : 0x0012: CONSTRUCTOR  (r0v1 X.3Wj A[REMOVE]) = (r4v0 'r42' X.103), (r3v0 'r3' X.1V8) call: X.3Wj.<init>(X.103, X.1V8):void type: CONSTRUCTOR)
                              (r2v0 'r2' X.1Pl)
                             type: VIRTUAL call: X.1nR.A03(X.20l, X.1Pl):void in method: X.24i.accept(java.lang.Object):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0012: CONSTRUCTOR  (r0v1 X.3Wj A[REMOVE]) = (r4v0 'r42' X.103), (r3v0 'r3' X.1V8) call: X.3Wj.<init>(X.103, X.1V8):void type: CONSTRUCTOR in method: X.24i.accept(java.lang.Object):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 15 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3Wj, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 21 more
                            */
                        /*
                            this = this;
                            X.103 r4 = r5.A01
                            X.1V8 r3 = r5.A02
                            X.1Pl r2 = r5.A00
                            X.0qD r0 = r4.A0E
                            r0.A03()
                            X.1nR r1 = r0.A00
                            X.AnonymousClass009.A05(r1)
                            X.3Wj r0 = new X.3Wj
                            r0.<init>(r4, r3)
                            r1.A03(r0, r2)
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.C460724i.accept(java.lang.Object):void");
                    }
                });
                A01.A00.A03(new AbstractC14590lg(r4) { // from class: X.24j
                    public final /* synthetic */ AnonymousClass1V8 A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        AnonymousClass103 r3 = AnonymousClass103.this;
                        r3.A0C.A08(new AnonymousClass2DO(r3, this.A01), 2);
                    }
                }, null);
            } else {
                this.A01.A0H(new RunnableBRunnable0Shape6S0200000_I0_6(this, 37, r4));
            }
        }
        this.A04.A03.A0B(r5);
    }
}
