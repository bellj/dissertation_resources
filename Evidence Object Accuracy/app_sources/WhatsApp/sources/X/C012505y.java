package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: X.05y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C012505y implements AbstractC012305w {
    @Override // X.AbstractC012305w
    public Drawable A8J(Context context, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        try {
            return AnonymousClass0CE.A00(context, theme, context.getResources(), attributeSet, xmlPullParser);
        } catch (Exception e) {
            Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e);
            return null;
        }
    }
}
