package X;

import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2;
import com.whatsapp.ClearableEditText;
import com.whatsapp.R;

/* renamed from: X.28x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C470928x {
    public ValueAnimator A00;
    public ValueAnimator A01;
    public final Activity A02;
    public final View A03;
    public final View A04;
    public final View A05;
    public final View A06;
    public final ImageView A07;
    public final TextView A08;
    public final TextView A09;
    public final ClearableEditText A0A;
    public final AnonymousClass018 A0B;
    public final C470028n A0C;
    public final AnonymousClass1CV A0D;
    public final C252718t A0E;
    public final C36161jQ A0F;

    public C470928x(Activity activity, View view, AbstractC001200n r8, AbstractC001400p r9, AnonymousClass018 r10, AnonymousClass1CV r11, C252718t r12, C36161jQ r13) {
        this.A0E = r12;
        this.A0B = r10;
        this.A0D = r11;
        this.A03 = view;
        this.A02 = activity;
        this.A0F = r13;
        this.A0C = (C470028n) new AnonymousClass02A(new AnonymousClass3RW(), r9).A00(C470028n.class);
        this.A09 = (TextView) AnonymousClass028.A0D(view, R.id.sticker_tab_button);
        this.A08 = (TextView) AnonymousClass028.A0D(view, R.id.emoji_tab_button);
        this.A06 = AnonymousClass028.A0D(view, R.id.tab_buttons_container);
        this.A05 = AnonymousClass028.A0D(view, R.id.search_container);
        this.A0A = (ClearableEditText) AnonymousClass028.A0D(view, R.id.search_entry);
        this.A07 = (ImageView) AnonymousClass028.A0D(view, R.id.search_icon);
        this.A04 = AnonymousClass028.A0D(view, R.id.search_button);
        View view2 = this.A06;
        view2.setVisibility(0);
        TextView textView = this.A09;
        textView.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 42));
        TextView textView2 = this.A08;
        textView2.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 41));
        if (r10.A04().A06) {
            textView.setBackgroundResource(R.drawable.shape_picker_right_tab_background);
            textView2.setBackgroundResource(R.drawable.shape_picker_left_tab_background);
        }
        view2.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: X.3Mg
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view3, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                C470928x r5 = C470928x.this;
                TextView textView3 = r5.A09;
                int width = textView3.getWidth();
                TextView textView4 = r5.A08;
                int width2 = textView4.getWidth();
                if (width <= width2) {
                    textView4 = textView3;
                }
                int max = Math.max(width, width2);
                ViewGroup.LayoutParams layoutParams = textView4.getLayoutParams();
                if (max != layoutParams.width) {
                    layoutParams.width = max;
                    textView4.post(new RunnableBRunnable0Shape5S0200000_I0_5(textView4, 49, layoutParams));
                }
                if (C12970iu.A1Y(r5.A0F.A01())) {
                    View view4 = r5.A03;
                    ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view4);
                    int A07 = C12990iw.A07(view4, R.dimen.shape_picker_search_collapsed_width) + C12990iw.A07(view4, R.dimen.shape_picker_landscape_button_spacing) + (max << 1);
                    if (A0H.width != A07) {
                        A0H.width = A07;
                        view4.post(new RunnableBRunnable0Shape6S0200000_I0_6(r5, 0, A0H));
                    }
                }
            }
        });
        ClearableEditText clearableEditText = this.A0A;
        clearableEditText.setHint(this.A03.getContext().getString(R.string.search));
        clearableEditText.getBackground().setAlpha(clearableEditText.hasFocus() ? 230 : 204);
        clearableEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: X.3MY
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view3, boolean z) {
                Window window;
                int i;
                C470928x r2 = C470928x.this;
                Drawable background = r2.A0A.getBackground();
                int i2 = 204;
                if (z) {
                    i2 = 230;
                }
                background.setAlpha(i2);
                C252718t r0 = r2.A0E;
                if (z) {
                    r0.A02(view3);
                    window = r2.A02.getWindow();
                    i = 5;
                } else {
                    r0.A01(view3);
                    window = r2.A02.getWindow();
                    i = 3;
                }
                window.setSoftInputMode(i);
            }
        });
        clearableEditText.addTextChangedListener(new C469828l(clearableEditText, this.A05, this.A0C));
        clearableEditText.A01 = new ViewOnClickCListenerShape2S0100000_I0_2(this, 44);
        clearableEditText.setVisibility(4);
        clearableEditText.setAlwaysShowClearIcon(true);
        View view3 = this.A04;
        view3.setOnClickListener(new ViewOnClickCListenerShape2S0100000_I0_2(this, 43));
        view3.setVisibility(0);
        this.A07.setVisibility(0);
        A00(0, false);
        AnonymousClass1CV r2 = this.A0D;
        r2.A00 = new AnonymousClass5UG() { // from class: X.573
            @Override // X.AnonymousClass5UG
            public final void AVg(boolean z) {
                C470928x r3 = C470928x.this;
                r3.A05.post(new RunnableBRunnable0Shape0S0110000_I0(r3, 11, z));
            }
        };
        this.A0C.A01.A0B(Boolean.valueOf(r2.A02));
        r2.A01();
        this.A0F.A05(r8, new AnonymousClass02B() { // from class: X.3QV
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                View view4;
                FrameLayout.LayoutParams layoutParams;
                int i;
                C470928x r5 = C470928x.this;
                if (C12970iu.A1Y(obj)) {
                    r5.A03.addOnLayoutChangeListener(new View$OnLayoutChangeListenerC66103Mh(r5));
                    view4 = r5.A06;
                    layoutParams = (FrameLayout.LayoutParams) view4.getLayoutParams();
                    i = 5;
                    if (C28141Kv.A00(r5.A0B)) {
                        i = 3;
                    }
                } else {
                    View view5 = r5.A03;
                    ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view5);
                    int dimensionPixelSize = view5.getResources().getDimensionPixelSize(R.dimen.shape_picker_header_horizontal_margin);
                    A0H.setMargins(dimensionPixelSize, view5.getResources().getDimensionPixelSize(R.dimen.shape_picker_header_top_margin), dimensionPixelSize, 0);
                    A0H.width = -1;
                    view5.setLayoutParams(A0H);
                    view4 = r5.A06;
                    layoutParams = (FrameLayout.LayoutParams) view4.getLayoutParams();
                    i = 49;
                }
                layoutParams.gravity = i;
                view4.setLayoutParams(layoutParams);
            }
        });
        C470028n r22 = this.A0C;
        r22.A03.A05(r8, new AnonymousClass02B() { // from class: X.3QW
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                TextView textView3;
                TextView textView4;
                C470928x r3 = C470928x.this;
                if (((Number) r3.A0C.A03.A01()).intValue() == 0) {
                    textView3 = r3.A09;
                    textView4 = r3.A08;
                } else {
                    textView3 = r3.A08;
                    textView4 = r3.A09;
                }
                View view4 = r3.A03;
                C12960it.A0s(view4.getContext(), textView3, R.color.shape_picker_tab_selected_text_color);
                textView3.getBackground().setAlpha(230);
                C12960it.A0s(view4.getContext(), textView4, R.color.shape_picker_tab_deselected_text_color);
                textView4.getBackground().setAlpha(51);
            }
        });
        r22.A01.A05(r8, new AnonymousClass02B() { // from class: X.4tM
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C470928x.this.A05.setVisibility(C12960it.A02(C12970iu.A1Y(obj) ? 1 : 0));
            }
        });
        r22.A00.A05(r8, new AnonymousClass02B() { // from class: X.4tL
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C470928x.this.A00(200, C12970iu.A1Y(obj));
            }
        });
    }

    public final void A00(long j, boolean z) {
        if (z) {
            int width = this.A0A.getWidth();
            View view = this.A03;
            int dimensionPixelSize = view.getResources().getDimensionPixelSize(R.dimen.shape_picker_search_entry_height);
            AnonymousClass2YN r4 = new AnonymousClass2YN(this);
            A02(r4, width, dimensionPixelSize, j);
            if (!((Boolean) this.A0F.A01()).booleanValue()) {
                A01(r4, view.getResources().getDimensionPixelSize(R.dimen.search_container_top_margin), view.getResources().getDimensionPixelSize(R.dimen.search_container_expanded_top_margin), j);
                return;
            }
            return;
        }
        View view2 = this.A03;
        int dimensionPixelSize2 = view2.getResources().getDimensionPixelSize(R.dimen.shape_picker_search_collapsed_width);
        int dimensionPixelSize3 = view2.getResources().getDimensionPixelSize(R.dimen.shape_picker_collapsed_search_height);
        AnonymousClass2YO r42 = new AnonymousClass2YO(this);
        A02(r42, dimensionPixelSize2, dimensionPixelSize3, j);
        if (!((Boolean) this.A0F.A01()).booleanValue()) {
            A01(r42, view2.getResources().getDimensionPixelSize(R.dimen.search_container_expanded_top_margin), view2.getResources().getDimensionPixelSize(R.dimen.search_container_top_margin), j);
        }
    }

    public final void A01(AnimatorListenerAdapter animatorListenerAdapter, int i, int i2, long j) {
        ValueAnimator valueAnimator = this.A00;
        if (valueAnimator == null) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            this.A00 = ofFloat;
            ofFloat.setInterpolator(new AccelerateDecelerateInterpolator());
        } else {
            valueAnimator.removeAllUpdateListeners();
            this.A00.removeAllListeners();
        }
        this.A00.setDuration(j);
        this.A00.addUpdateListener(new ValueAnimator.AnimatorUpdateListener((ViewGroup.MarginLayoutParams) this.A05.getLayoutParams(), i2, i) { // from class: X.4ei
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;
            public final /* synthetic */ ViewGroup.MarginLayoutParams A02;

            {
                this.A00 = r2;
                this.A01 = r3;
                this.A02 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                int i3 = this.A00;
                int i4 = this.A01;
                ViewGroup.MarginLayoutParams marginLayoutParams = this.A02;
                float animatedFraction = valueAnimator2.getAnimatedFraction();
                marginLayoutParams.setMargins(0, (int) ((animatedFraction * ((float) i3)) + ((1.0f - animatedFraction) * ((float) i4))), 0, 0);
            }
        });
        this.A00.addListener(animatorListenerAdapter);
        this.A00.start();
    }

    public final void A02(AnimatorListenerAdapter animatorListenerAdapter, int i, int i2, long j) {
        ValueAnimator valueAnimator = this.A01;
        if (valueAnimator == null) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            this.A01 = ofFloat;
            ofFloat.setInterpolator(new AccelerateDecelerateInterpolator());
        } else {
            valueAnimator.removeAllUpdateListeners();
            this.A01.removeAllListeners();
        }
        this.A01.setDuration(j);
        View view = this.A04;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        view.setVisibility(0);
        this.A01.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(layoutParams, this, i, view.getWidth(), i2, view.getHeight()) { // from class: X.4en
            public final /* synthetic */ int A00;
            public final /* synthetic */ int A01;
            public final /* synthetic */ int A02;
            public final /* synthetic */ int A03;
            public final /* synthetic */ ViewGroup.LayoutParams A04;
            public final /* synthetic */ C470928x A05;

            {
                this.A05 = r2;
                this.A04 = r1;
                this.A00 = r3;
                this.A01 = r4;
                this.A02 = r5;
                this.A03 = r6;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                C470928x r7 = this.A05;
                ViewGroup.LayoutParams layoutParams2 = this.A04;
                int i3 = this.A00;
                int i4 = this.A01;
                int i5 = this.A02;
                int i6 = this.A03;
                float animatedFraction = valueAnimator2.getAnimatedFraction();
                float f = 1.0f - animatedFraction;
                layoutParams2.width = (int) ((((float) i3) * animatedFraction) + (((float) i4) * f));
                layoutParams2.height = (int) ((animatedFraction * ((float) i5)) + (f * ((float) i6)));
                r7.A04.setLayoutParams(layoutParams2);
            }
        });
        this.A01.addListener(animatorListenerAdapter);
        this.A01.start();
    }
}
