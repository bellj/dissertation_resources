package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.group.GroupChatInfo;

/* renamed from: X.32N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32N extends AnonymousClass4V1 {
    public final View A00;
    public final ImageView A01;
    public final TextView A02;
    public final TextEmojiLabel A03;
    public final TextEmojiLabel A04;
    public final TextEmojiLabel A05;
    public final C28801Pb A06;
    public final C89474Kc A07;
    public final /* synthetic */ GroupChatInfo A08;

    public AnonymousClass32N(View view, C89474Kc r7, GroupChatInfo groupChatInfo) {
        this.A08 = groupChatInfo;
        this.A06 = new C28801Pb(view, groupChatInfo.A0Y, groupChatInfo.A1M, (int) R.id.name);
        this.A03 = C12970iu.A0T(view, R.id.status);
        ImageView A0K = C12970iu.A0K(view, R.id.wdsProfilePicture);
        ImageView A0K2 = C12970iu.A0K(view, R.id.avatar);
        if (((ActivityC13810kN) this.A08).A0C.A07(1533)) {
            A0K.setVisibility(0);
            A0K2.setVisibility(8);
        } else {
            A0K.setVisibility(8);
            A0K2.setVisibility(0);
            A0K = A0K2;
        }
        this.A01 = A0K;
        this.A02 = C12960it.A0I(view, R.id.owner);
        this.A05 = C12970iu.A0T(view, R.id.push_name);
        this.A04 = C12970iu.A0T(view, R.id.push_name_alternative);
        this.A00 = AnonymousClass028.A0D(view, R.id.group_chat_info_layout);
        this.A07 = r7;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:72:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass4V1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AbstractC36121jM r20, X.C92434Vw r21, java.util.ArrayList r22) {
        /*
        // Method dump skipped, instructions count: 460
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass32N.A00(X.1jM, X.4Vw, java.util.ArrayList):void");
    }
}
