package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1JW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JW extends AnonymousClass1JX {
    public int A00;

    public AnonymousClass1JW(AbstractC15710nm r2, C15450nH r3) {
        this(r2, r3, false);
    }

    public AnonymousClass1JW(AbstractC15710nm r2, C15450nH r3, boolean z) {
        super(r2, r3, null, z);
        super.A00 = 0;
    }

    public static Jid A00(AbstractC15710nm r3, Jid jid, Class cls, String str) {
        if (jid == null || cls.isInstance(jid)) {
            return jid;
        }
        StringBuilder sb = new StringBuilder("web-query/failed to cast ");
        sb.append(jid.getClass().getName());
        sb.append(" to ");
        sb.append(cls.getName());
        sb.append("(");
        sb.append(jid);
        sb.append(")@");
        sb.append(str);
        String obj = sb.toString();
        Log.e(obj);
        StringBuilder sb2 = new StringBuilder("web-query/downcast-failure/");
        sb2.append(str);
        r3.AaV(sb2.toString(), obj, false);
        return null;
    }

    public static void A01(AbstractC15590nW r1, UserJid userJid, AnonymousClass1G9 r3, AnonymousClass1G7 r4) {
        if (r1 != null) {
            r3.A07(r1.getRawString());
        } else {
            r3.A03();
            AnonymousClass1G8 r12 = (AnonymousClass1G8) r3.A00;
            r12.A00 &= -2;
            r12.A03 = AnonymousClass1G8.A05.A03;
        }
        if (userJid != null) {
            r4.A0A(userJid.getRawString());
        } else {
            r4.A05();
        }
    }

    public static void A02(AnonymousClass1G7 r2, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (str != null) {
                r2.A09(str);
            }
        }
    }
}
