package X;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.33k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C619933k extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass5W8 A00;
    public final /* synthetic */ C18610sj A01;
    public final /* synthetic */ String A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C619933k(Context context, C14900mE r2, C18650sn r3, AnonymousClass5W8 r4, C18610sj r5, String str) {
        super(context, r2, r3);
        this.A01 = r5;
        this.A02 = str;
        this.A00 = r4;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r4) {
        C30931Zj r2 = this.A01.A0I;
        StringBuilder A0k = C12960it.A0k("get-method: credential-id=");
        A0k.append(this.A02);
        r2.A05(C12960it.A0Z(r4, " on-request-error=", A0k));
        this.A00.APo(r4);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r4) {
        C30931Zj r2 = this.A01.A0I;
        StringBuilder A0k = C12960it.A0k("get-method: credential-id=");
        A0k.append(this.A02);
        r2.A05(C12960it.A0Z(r4, " on-response-error=", A0k));
        this.A00.APo(r4);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r7) {
        AbstractC28901Pl r3;
        C18610sj r5 = this.A01;
        C30931Zj r32 = r5.A0I;
        StringBuilder A0k = C12960it.A0k("get-method: credential-id=");
        String str = this.A02;
        A0k.append(str);
        r32.A03(null, C12960it.A0d(" success", A0k));
        ArrayList A07 = r5.A0L.A07(r7.A0E("account"));
        if (A07 == null || A07.size() <= 0 || (r3 = (AbstractC28901Pl) A07.get(0)) == null || !str.equals(r3.A0A)) {
            this.A00.AQy(null);
            return;
        }
        C17070qD r0 = r5.A0G;
        r0.A03();
        C38051nR r2 = r0.A00;
        AnonymousClass009.A05(r2);
        r2.A03(new AbstractC451720l(r3, this.A00) { // from class: X.56I
            public final /* synthetic */ AbstractC28901Pl A00;
            public final /* synthetic */ AnonymousClass5W8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                this.A01.AQy(this.A00);
            }
        }, r3);
    }
}
