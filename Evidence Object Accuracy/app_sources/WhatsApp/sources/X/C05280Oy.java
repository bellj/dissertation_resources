package X;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.view.ViewConfiguration;

/* renamed from: X.0Oy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05280Oy {
    public Context A00;

    public C05280Oy(Context context) {
        this.A00 = context;
    }

    public int A00() {
        Configuration configuration = this.A00.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600 || ((i > 960 && i2 > 720) || (i > 720 && i2 > 960))) {
            return 5;
        }
        if (i >= 500) {
            return 4;
        }
        if (i > 640 && i2 > 480) {
            return 4;
        }
        if (i <= 480 || i2 <= 640) {
            return i >= 360 ? 3 : 2;
        }
        return 4;
    }

    public boolean A01() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return !ViewConfiguration.get(this.A00).hasPermanentMenuKey();
    }
}
