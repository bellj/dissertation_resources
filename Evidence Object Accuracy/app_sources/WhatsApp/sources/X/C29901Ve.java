package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.Jid;

/* renamed from: X.1Ve  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29901Ve extends AbstractC15970oE implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99964l9();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "broadcast";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 3;
    }

    public C29901Ve(Parcel parcel) {
        super(parcel);
    }

    public C29901Ve(String str) {
        super(str);
        char charAt;
        int length = str.length();
        if (length >= 1 && length <= 20 && (charAt = str.charAt(0)) >= '1' && charAt <= '9') {
            int i = 1;
            while (i < length) {
                char charAt2 = str.charAt(i);
                i = (charAt2 >= '0' && charAt2 <= '9') ? i + 1 : i;
            }
            return;
        }
        throw new AnonymousClass1MW(str);
    }

    public static C29901Ve A02(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            Jid jid = Jid.get(str);
            if (jid instanceof C29901Ve) {
                return (C29901Ve) jid;
            }
            throw new AnonymousClass1MW(str);
        } catch (AnonymousClass1MW unused) {
            return null;
        }
    }

    @Override // com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass1US.A02(4, this.user));
        sb.append('@');
        sb.append("broadcast");
        return sb.toString();
    }
}
