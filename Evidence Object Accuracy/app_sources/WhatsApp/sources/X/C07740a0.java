package X;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/* renamed from: X.0a0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07740a0 implements AbstractC12700iM {
    public final AbstractC02880Ff A00;
    public final AnonymousClass0QN A01;
    public final AbstractC05330Pd A02;
    public final AbstractC05330Pd A03;
    public final AbstractC05330Pd A04;
    public final AbstractC05330Pd A05;
    public final AbstractC05330Pd A06;
    public final AbstractC05330Pd A07;
    public final AbstractC05330Pd A08;
    public final AbstractC05330Pd A09;

    public C07740a0(AnonymousClass0QN r2) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0FS(r2, this);
        this.A02 = new AnonymousClass0FX(r2, this);
        this.A08 = new AnonymousClass0FY(r2, this);
        this.A09 = new AnonymousClass0FZ(r2, this);
        this.A03 = new C02830Fa(r2, this);
        this.A07 = new C02840Fb(r2, this);
        this.A04 = new C02850Fc(r2, this);
        this.A06 = new C02860Fd(r2, this);
        this.A05 = new C02870Fe(r2, this);
    }

    public final void A00(AnonymousClass00N r9) {
        ArrayList arrayList;
        int i;
        Set<String> keySet = r9.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (r9.size() > 999) {
            AnonymousClass00N r6 = new AnonymousClass00N(999);
            int size = r9.size();
            int i2 = 0;
            loop0: while (true) {
                i = 0;
                while (i2 < size) {
                    Object[] objArr = r9.A02;
                    int i3 = i2 << 1;
                    r6.put(objArr[i3], objArr[i3 + 1]);
                    i2++;
                    i++;
                    if (i == 999) {
                        break;
                    }
                }
                A00(r6);
                r6 = new AnonymousClass00N(999);
            }
            if (i > 0) {
                A00(r6);
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder("SELECT `progress`,`work_spec_id` FROM `WorkProgress` WHERE `work_spec_id` IN (");
        int size2 = keySet.size();
        AnonymousClass0LD.A00(sb, size2);
        sb.append(")");
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00(sb.toString(), size2 + 0);
        int i4 = 1;
        for (String str : keySet) {
            if (str == null) {
                A00.A6T(i4);
            } else {
                A00.A6U(i4, str);
            }
            i4++;
        }
        Cursor A002 = AnonymousClass0LC.A00(this.A01, A00, false);
        try {
            int columnIndex = A002.getColumnIndex("work_spec_id");
            if (columnIndex < 0) {
                StringBuilder sb2 = new StringBuilder("`");
                sb2.append("work_spec_id");
                sb2.append("`");
                columnIndex = A002.getColumnIndex(sb2.toString());
                if (columnIndex == -1) {
                }
            }
            while (A002.moveToNext()) {
                if (!A002.isNull(columnIndex) && (arrayList = (ArrayList) r9.get(A002.getString(columnIndex))) != null) {
                    arrayList.add(C006503b.A00(A002.getBlob(0)));
                }
            }
        } finally {
            A002.close();
        }
    }

    public final void A01(AnonymousClass00N r9) {
        ArrayList arrayList;
        int i;
        Set<String> keySet = r9.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (r9.size() > 999) {
            AnonymousClass00N r6 = new AnonymousClass00N(999);
            int size = r9.size();
            int i2 = 0;
            loop0: while (true) {
                i = 0;
                while (i2 < size) {
                    Object[] objArr = r9.A02;
                    int i3 = i2 << 1;
                    r6.put(objArr[i3], objArr[i3 + 1]);
                    i2++;
                    i++;
                    if (i == 999) {
                        break;
                    }
                }
                A01(r6);
                r6 = new AnonymousClass00N(999);
            }
            if (i > 0) {
                A01(r6);
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder("SELECT `tag`,`work_spec_id` FROM `WorkTag` WHERE `work_spec_id` IN (");
        int size2 = keySet.size();
        AnonymousClass0LD.A00(sb, size2);
        sb.append(")");
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00(sb.toString(), size2 + 0);
        int i4 = 1;
        for (String str : keySet) {
            if (str == null) {
                A00.A6T(i4);
            } else {
                A00.A6U(i4, str);
            }
            i4++;
        }
        Cursor A002 = AnonymousClass0LC.A00(this.A01, A00, false);
        try {
            int columnIndex = A002.getColumnIndex("work_spec_id");
            if (columnIndex < 0) {
                StringBuilder sb2 = new StringBuilder("`");
                sb2.append("work_spec_id");
                sb2.append("`");
                columnIndex = A002.getColumnIndex(sb2.toString());
                if (columnIndex == -1) {
                }
            }
            while (A002.moveToNext()) {
                if (!A002.isNull(columnIndex) && (arrayList = (ArrayList) r9.get(A002.getString(columnIndex))) != null) {
                    arrayList.add(A002.getString(0));
                }
            }
        } finally {
            A002.close();
        }
    }

    @Override // X.AbstractC12700iM
    public List AAg(int i) {
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 ORDER BY period_start_time LIMIT ?", 1);
        A00.A6S(1, (long) 200);
        AnonymousClass0QN r1 = this.A01;
        r1.A02();
        Cursor A002 = AnonymousClass0LC.A00(r1, A00, false);
        try {
            int A003 = AnonymousClass0LB.A00(A002, "required_network_type");
            int A004 = AnonymousClass0LB.A00(A002, "requires_charging");
            int A005 = AnonymousClass0LB.A00(A002, "requires_device_idle");
            int A006 = AnonymousClass0LB.A00(A002, "requires_battery_not_low");
            int A007 = AnonymousClass0LB.A00(A002, "requires_storage_not_low");
            int A008 = AnonymousClass0LB.A00(A002, "trigger_content_update_delay");
            int A009 = AnonymousClass0LB.A00(A002, "trigger_max_content_delay");
            int A0010 = AnonymousClass0LB.A00(A002, "content_uri_triggers");
            int A0011 = AnonymousClass0LB.A00(A002, "id");
            int A0012 = AnonymousClass0LB.A00(A002, "state");
            int A0013 = AnonymousClass0LB.A00(A002, "worker_class_name");
            int A0014 = AnonymousClass0LB.A00(A002, "input_merger_class_name");
            int A0015 = AnonymousClass0LB.A00(A002, "input");
            int A0016 = AnonymousClass0LB.A00(A002, "output");
            int A0017 = AnonymousClass0LB.A00(A002, "initial_delay");
            int A0018 = AnonymousClass0LB.A00(A002, "interval_duration");
            int A0019 = AnonymousClass0LB.A00(A002, "flex_duration");
            int A0020 = AnonymousClass0LB.A00(A002, "run_attempt_count");
            int A0021 = AnonymousClass0LB.A00(A002, "backoff_policy");
            int A0022 = AnonymousClass0LB.A00(A002, "backoff_delay_duration");
            int A0023 = AnonymousClass0LB.A00(A002, "period_start_time");
            int A0024 = AnonymousClass0LB.A00(A002, "minimum_retention_duration");
            int A0025 = AnonymousClass0LB.A00(A002, "schedule_requested_at");
            int A0026 = AnonymousClass0LB.A00(A002, "run_in_foreground");
            int A0027 = AnonymousClass0LB.A00(A002, "out_of_quota_policy");
            ArrayList arrayList = new ArrayList(A002.getCount());
            while (A002.moveToNext()) {
                String string = A002.getString(A0011);
                String string2 = A002.getString(A0013);
                C004101u r3 = new C004101u();
                r3.A03 = AnonymousClass0UK.A03(A002.getInt(A003));
                boolean z = false;
                if (A002.getInt(A004) != 0) {
                    z = true;
                }
                r3.A05 = z;
                boolean z2 = false;
                if (A002.getInt(A005) != 0) {
                    z2 = true;
                }
                r3.A02(z2);
                boolean z3 = false;
                if (A002.getInt(A006) != 0) {
                    z3 = true;
                }
                r3.A04 = z3;
                boolean z4 = false;
                if (A002.getInt(A007) != 0) {
                    z4 = true;
                }
                r3.A07 = z4;
                r3.A00 = A002.getLong(A008);
                r3.A01 = A002.getLong(A009);
                r3.A01(AnonymousClass0UK.A02(A002.getBlob(A0010)));
                C004401z r2 = new C004401z(string, string2);
                r2.A0D = AnonymousClass0UK.A05(A002.getInt(A0012));
                r2.A0F = A002.getString(A0014);
                r2.A0A = C006503b.A00(A002.getBlob(A0015));
                r2.A0B = C006503b.A00(A002.getBlob(A0016));
                r2.A03 = A002.getLong(A0017);
                r2.A04 = A002.getLong(A0018);
                r2.A02 = A002.getLong(A0019);
                r2.A00 = A002.getInt(A0020);
                r2.A08 = AnonymousClass0UK.A01(A002.getInt(A0021));
                r2.A01 = A002.getLong(A0022);
                r2.A06 = A002.getLong(A0023);
                r2.A05 = A002.getLong(A0024);
                r2.A07 = A002.getLong(A0025);
                boolean z5 = false;
                if (A002.getInt(A0026) != 0) {
                    z5 = true;
                }
                r2.A0H = z5;
                r2.A0C = AnonymousClass0UK.A04(A002.getInt(A0027));
                r2.A09 = r3;
                arrayList.add(r2);
            }
            return arrayList;
        } finally {
            A002.close();
            A00.A01();
        }
    }

    @Override // X.AbstractC12700iM
    public List AGL() {
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=1", 0);
        AnonymousClass0QN r1 = this.A01;
        r1.A02();
        Cursor A002 = AnonymousClass0LC.A00(r1, A00, false);
        try {
            int A003 = AnonymousClass0LB.A00(A002, "required_network_type");
            int A004 = AnonymousClass0LB.A00(A002, "requires_charging");
            int A005 = AnonymousClass0LB.A00(A002, "requires_device_idle");
            int A006 = AnonymousClass0LB.A00(A002, "requires_battery_not_low");
            int A007 = AnonymousClass0LB.A00(A002, "requires_storage_not_low");
            int A008 = AnonymousClass0LB.A00(A002, "trigger_content_update_delay");
            int A009 = AnonymousClass0LB.A00(A002, "trigger_max_content_delay");
            int A0010 = AnonymousClass0LB.A00(A002, "content_uri_triggers");
            int A0011 = AnonymousClass0LB.A00(A002, "id");
            int A0012 = AnonymousClass0LB.A00(A002, "state");
            int A0013 = AnonymousClass0LB.A00(A002, "worker_class_name");
            int A0014 = AnonymousClass0LB.A00(A002, "input_merger_class_name");
            int A0015 = AnonymousClass0LB.A00(A002, "input");
            int A0016 = AnonymousClass0LB.A00(A002, "output");
            int A0017 = AnonymousClass0LB.A00(A002, "initial_delay");
            int A0018 = AnonymousClass0LB.A00(A002, "interval_duration");
            int A0019 = AnonymousClass0LB.A00(A002, "flex_duration");
            int A0020 = AnonymousClass0LB.A00(A002, "run_attempt_count");
            int A0021 = AnonymousClass0LB.A00(A002, "backoff_policy");
            int A0022 = AnonymousClass0LB.A00(A002, "backoff_delay_duration");
            int A0023 = AnonymousClass0LB.A00(A002, "period_start_time");
            int A0024 = AnonymousClass0LB.A00(A002, "minimum_retention_duration");
            int A0025 = AnonymousClass0LB.A00(A002, "schedule_requested_at");
            int A0026 = AnonymousClass0LB.A00(A002, "run_in_foreground");
            int A0027 = AnonymousClass0LB.A00(A002, "out_of_quota_policy");
            ArrayList arrayList = new ArrayList(A002.getCount());
            while (A002.moveToNext()) {
                String string = A002.getString(A0011);
                String string2 = A002.getString(A0013);
                C004101u r3 = new C004101u();
                r3.A03 = AnonymousClass0UK.A03(A002.getInt(A003));
                boolean z = false;
                if (A002.getInt(A004) != 0) {
                    z = true;
                }
                r3.A05 = z;
                boolean z2 = false;
                if (A002.getInt(A005) != 0) {
                    z2 = true;
                }
                r3.A02(z2);
                boolean z3 = false;
                if (A002.getInt(A006) != 0) {
                    z3 = true;
                }
                r3.A04 = z3;
                boolean z4 = false;
                if (A002.getInt(A007) != 0) {
                    z4 = true;
                }
                r3.A07 = z4;
                r3.A00 = A002.getLong(A008);
                r3.A01 = A002.getLong(A009);
                r3.A01(AnonymousClass0UK.A02(A002.getBlob(A0010)));
                C004401z r2 = new C004401z(string, string2);
                r2.A0D = AnonymousClass0UK.A05(A002.getInt(A0012));
                r2.A0F = A002.getString(A0014);
                r2.A0A = C006503b.A00(A002.getBlob(A0015));
                r2.A0B = C006503b.A00(A002.getBlob(A0016));
                r2.A03 = A002.getLong(A0017);
                r2.A04 = A002.getLong(A0018);
                r2.A02 = A002.getLong(A0019);
                r2.A00 = A002.getInt(A0020);
                r2.A08 = AnonymousClass0UK.A01(A002.getInt(A0021));
                r2.A01 = A002.getLong(A0022);
                r2.A06 = A002.getLong(A0023);
                r2.A05 = A002.getLong(A0024);
                r2.A07 = A002.getLong(A0025);
                boolean z5 = false;
                if (A002.getInt(A0026) != 0) {
                    z5 = true;
                }
                r2.A0H = z5;
                r2.A0C = AnonymousClass0UK.A04(A002.getInt(A0027));
                r2.A09 = r3;
                arrayList.add(r2);
            }
            return arrayList;
        } finally {
            A002.close();
            A00.A01();
        }
    }

    @Override // X.AbstractC12700iM
    public List AGQ() {
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        AnonymousClass0QN r1 = this.A01;
        r1.A02();
        Cursor A002 = AnonymousClass0LC.A00(r1, A00, false);
        try {
            int A003 = AnonymousClass0LB.A00(A002, "required_network_type");
            int A004 = AnonymousClass0LB.A00(A002, "requires_charging");
            int A005 = AnonymousClass0LB.A00(A002, "requires_device_idle");
            int A006 = AnonymousClass0LB.A00(A002, "requires_battery_not_low");
            int A007 = AnonymousClass0LB.A00(A002, "requires_storage_not_low");
            int A008 = AnonymousClass0LB.A00(A002, "trigger_content_update_delay");
            int A009 = AnonymousClass0LB.A00(A002, "trigger_max_content_delay");
            int A0010 = AnonymousClass0LB.A00(A002, "content_uri_triggers");
            int A0011 = AnonymousClass0LB.A00(A002, "id");
            int A0012 = AnonymousClass0LB.A00(A002, "state");
            int A0013 = AnonymousClass0LB.A00(A002, "worker_class_name");
            int A0014 = AnonymousClass0LB.A00(A002, "input_merger_class_name");
            int A0015 = AnonymousClass0LB.A00(A002, "input");
            int A0016 = AnonymousClass0LB.A00(A002, "output");
            int A0017 = AnonymousClass0LB.A00(A002, "initial_delay");
            int A0018 = AnonymousClass0LB.A00(A002, "interval_duration");
            int A0019 = AnonymousClass0LB.A00(A002, "flex_duration");
            int A0020 = AnonymousClass0LB.A00(A002, "run_attempt_count");
            int A0021 = AnonymousClass0LB.A00(A002, "backoff_policy");
            int A0022 = AnonymousClass0LB.A00(A002, "backoff_delay_duration");
            int A0023 = AnonymousClass0LB.A00(A002, "period_start_time");
            int A0024 = AnonymousClass0LB.A00(A002, "minimum_retention_duration");
            int A0025 = AnonymousClass0LB.A00(A002, "schedule_requested_at");
            int A0026 = AnonymousClass0LB.A00(A002, "run_in_foreground");
            int A0027 = AnonymousClass0LB.A00(A002, "out_of_quota_policy");
            ArrayList arrayList = new ArrayList(A002.getCount());
            while (A002.moveToNext()) {
                String string = A002.getString(A0011);
                String string2 = A002.getString(A0013);
                C004101u r3 = new C004101u();
                r3.A03 = AnonymousClass0UK.A03(A002.getInt(A003));
                boolean z = false;
                if (A002.getInt(A004) != 0) {
                    z = true;
                }
                r3.A05 = z;
                boolean z2 = false;
                if (A002.getInt(A005) != 0) {
                    z2 = true;
                }
                r3.A02(z2);
                boolean z3 = false;
                if (A002.getInt(A006) != 0) {
                    z3 = true;
                }
                r3.A04 = z3;
                boolean z4 = false;
                if (A002.getInt(A007) != 0) {
                    z4 = true;
                }
                r3.A07 = z4;
                r3.A00 = A002.getLong(A008);
                r3.A01 = A002.getLong(A009);
                r3.A01(AnonymousClass0UK.A02(A002.getBlob(A0010)));
                C004401z r2 = new C004401z(string, string2);
                r2.A0D = AnonymousClass0UK.A05(A002.getInt(A0012));
                r2.A0F = A002.getString(A0014);
                r2.A0A = C006503b.A00(A002.getBlob(A0015));
                r2.A0B = C006503b.A00(A002.getBlob(A0016));
                r2.A03 = A002.getLong(A0017);
                r2.A04 = A002.getLong(A0018);
                r2.A02 = A002.getLong(A0019);
                r2.A00 = A002.getInt(A0020);
                r2.A08 = AnonymousClass0UK.A01(A002.getInt(A0021));
                r2.A01 = A002.getLong(A0022);
                r2.A06 = A002.getLong(A0023);
                r2.A05 = A002.getLong(A0024);
                r2.A07 = A002.getLong(A0025);
                boolean z5 = false;
                if (A002.getInt(A0026) != 0) {
                    z5 = true;
                }
                r2.A0H = z5;
                r2.A0C = AnonymousClass0UK.A04(A002.getInt(A0027));
                r2.A09 = r3;
                arrayList.add(r2);
            }
            return arrayList;
        } finally {
            A002.close();
            A00.A01();
        }
    }

    @Override // X.AbstractC12700iM
    public EnumC03840Ji AGv(String str) {
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            A00.A6T(1);
        } else {
            A00.A6U(1, str);
        }
        AnonymousClass0QN r0 = this.A01;
        r0.A02();
        EnumC03840Ji r2 = null;
        Cursor A002 = AnonymousClass0LC.A00(r0, A00, false);
        try {
            if (A002.moveToFirst()) {
                r2 = AnonymousClass0UK.A05(A002.getInt(0));
            }
            return r2;
        } finally {
            A002.close();
            A00.A01();
        }
    }

    @Override // X.AbstractC12700iM
    public C004401z AHn(String str) {
        C004401z r1;
        AnonymousClass0ZJ A00 = AnonymousClass0ZJ.A00("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE id=?", 1);
        if (str == null) {
            A00.A6T(1);
        } else {
            A00.A6U(1, str);
        }
        AnonymousClass0QN r12 = this.A01;
        r12.A02();
        Cursor A002 = AnonymousClass0LC.A00(r12, A00, false);
        try {
            int A003 = AnonymousClass0LB.A00(A002, "required_network_type");
            int A004 = AnonymousClass0LB.A00(A002, "requires_charging");
            int A005 = AnonymousClass0LB.A00(A002, "requires_device_idle");
            int A006 = AnonymousClass0LB.A00(A002, "requires_battery_not_low");
            int A007 = AnonymousClass0LB.A00(A002, "requires_storage_not_low");
            int A008 = AnonymousClass0LB.A00(A002, "trigger_content_update_delay");
            int A009 = AnonymousClass0LB.A00(A002, "trigger_max_content_delay");
            int A0010 = AnonymousClass0LB.A00(A002, "content_uri_triggers");
            int A0011 = AnonymousClass0LB.A00(A002, "id");
            int A0012 = AnonymousClass0LB.A00(A002, "state");
            int A0013 = AnonymousClass0LB.A00(A002, "worker_class_name");
            int A0014 = AnonymousClass0LB.A00(A002, "input_merger_class_name");
            int A0015 = AnonymousClass0LB.A00(A002, "input");
            int A0016 = AnonymousClass0LB.A00(A002, "output");
            int A0017 = AnonymousClass0LB.A00(A002, "initial_delay");
            int A0018 = AnonymousClass0LB.A00(A002, "interval_duration");
            int A0019 = AnonymousClass0LB.A00(A002, "flex_duration");
            int A0020 = AnonymousClass0LB.A00(A002, "run_attempt_count");
            int A0021 = AnonymousClass0LB.A00(A002, "backoff_policy");
            int A0022 = AnonymousClass0LB.A00(A002, "backoff_delay_duration");
            int A0023 = AnonymousClass0LB.A00(A002, "period_start_time");
            int A0024 = AnonymousClass0LB.A00(A002, "minimum_retention_duration");
            int A0025 = AnonymousClass0LB.A00(A002, "schedule_requested_at");
            int A0026 = AnonymousClass0LB.A00(A002, "run_in_foreground");
            int A0027 = AnonymousClass0LB.A00(A002, "out_of_quota_policy");
            if (A002.moveToFirst()) {
                String string = A002.getString(A0011);
                String string2 = A002.getString(A0013);
                C004101u r4 = new C004101u();
                r4.A03 = AnonymousClass0UK.A03(A002.getInt(A003));
                boolean z = false;
                if (A002.getInt(A004) != 0) {
                    z = true;
                }
                r4.A05 = z;
                boolean z2 = false;
                if (A002.getInt(A005) != 0) {
                    z2 = true;
                }
                r4.A02(z2);
                boolean z3 = false;
                if (A002.getInt(A006) != 0) {
                    z3 = true;
                }
                r4.A04 = z3;
                boolean z4 = false;
                if (A002.getInt(A007) != 0) {
                    z4 = true;
                }
                r4.A07 = z4;
                r4.A00 = A002.getLong(A008);
                r4.A01 = A002.getLong(A009);
                r4.A01(AnonymousClass0UK.A02(A002.getBlob(A0010)));
                r1 = new C004401z(string, string2);
                r1.A0D = AnonymousClass0UK.A05(A002.getInt(A0012));
                r1.A0F = A002.getString(A0014);
                r1.A0A = C006503b.A00(A002.getBlob(A0015));
                r1.A0B = C006503b.A00(A002.getBlob(A0016));
                r1.A03 = A002.getLong(A0017);
                r1.A04 = A002.getLong(A0018);
                r1.A02 = A002.getLong(A0019);
                r1.A00 = A002.getInt(A0020);
                r1.A08 = AnonymousClass0UK.A01(A002.getInt(A0021));
                r1.A01 = A002.getLong(A0022);
                r1.A06 = A002.getLong(A0023);
                r1.A05 = A002.getLong(A0024);
                r1.A07 = A002.getLong(A0025);
                boolean z5 = false;
                if (A002.getInt(A0026) != 0) {
                    z5 = true;
                }
                r1.A0H = z5;
                r1.A0C = AnonymousClass0UK.A04(A002.getInt(A0027));
                r1.A09 = r4;
            } else {
                r1 = null;
            }
            return r1;
        } finally {
            A002.close();
            A00.A01();
        }
    }

    @Override // X.AbstractC12700iM
    public int AKt(String str, long j) {
        AnonymousClass0QN r3 = this.A01;
        r3.A02();
        AbstractC05330Pd r2 = this.A04;
        AbstractC12830ic A00 = r2.A00();
        A00.A6S(1, j);
        if (str == null) {
            A00.A6T(2);
        } else {
            A00.A6U(2, str);
        }
        r3.A03();
        try {
            int executeUpdateDelete = ((C02980Fp) A00).A00.executeUpdateDelete();
            r3.A05();
            return executeUpdateDelete;
        } finally {
            r3.A04();
            r2.A02(A00);
        }
    }

    @Override // X.AbstractC12700iM
    public void AcQ(C006503b r6, String str) {
        AnonymousClass0QN r4 = this.A01;
        r4.A02();
        AbstractC05330Pd r3 = this.A08;
        AbstractC12830ic A00 = r3.A00();
        byte[] A01 = C006503b.A01(r6);
        if (A01 == null) {
            A00.A6T(1);
        } else {
            A00.A6P(1, A01);
        }
        if (str == null) {
            A00.A6T(2);
        } else {
            A00.A6U(2, str);
        }
        r4.A03();
        try {
            ((C02980Fp) A00).A00.executeUpdateDelete();
            r4.A05();
        } finally {
            r4.A04();
            r3.A02(A00);
        }
    }

    @Override // X.AbstractC12700iM
    public int Act(EnumC03840Ji r7, String... strArr) {
        AnonymousClass0QN r4 = this.A01;
        r4.A02();
        StringBuilder sb = new StringBuilder("UPDATE workspec SET state=");
        sb.append("?");
        sb.append(" WHERE id IN (");
        int length = strArr.length;
        AnonymousClass0LD.A00(sb, length);
        sb.append(")");
        AbstractC12830ic A00 = r4.A00(sb.toString());
        A00.A6S(1, (long) AnonymousClass0UK.A00(r7));
        int i = 2;
        for (String str : strArr) {
            if (str == null) {
                A00.A6T(i);
            } else {
                A00.A6U(i, str);
            }
            i++;
        }
        r4.A03();
        try {
            int executeUpdateDelete = ((C02980Fp) A00).A00.executeUpdateDelete();
            r4.A05();
            return executeUpdateDelete;
        } finally {
            r4.A04();
        }
    }
}
