package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.5nG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123505nG extends AbstractC118085bF {
    public static final long A0C = TimeUnit.MINUTES.toMillis(15);
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C20370ve A02;
    public final AnonymousClass102 A03;
    public final C14850m9 A04;
    public final C1329668y A05;
    public final C21860y6 A06;
    public final AnonymousClass1A7 A07;
    public final C30931Zj A08 = C30931Zj.A00("IndiaPaymentSettingsViewModel", "payment", "IN");
    public final C126885tb A09;
    public final AnonymousClass18O A0A;
    public final AbstractC14440lR A0B;

    public C123505nG(C14830m7 r10, AnonymousClass018 r11, C20370ve r12, AnonymousClass102 r13, C14850m9 r14, C1329668y r15, C21860y6 r16, C18600si r17, C17070qD r18, AnonymousClass1A7 r19, AbstractC16870pt r20, C126885tb r21, AnonymousClass18O r22, AbstractC14440lR r23) {
        super(r10, r11, r17, r18, r20);
        this.A04 = r14;
        this.A0B = r23;
        this.A06 = r16;
        this.A03 = r13;
        this.A09 = r21;
        this.A02 = r12;
        this.A07 = r19;
        this.A05 = r15;
        this.A0A = r22;
    }

    public int A0C() {
        C14850m9 r2 = this.A04;
        if (r2.A07(1644)) {
            C1329668y r1 = this.A05;
            if (!r1.A0N(r1.A07()) && C12980iv.A1W(super.A06.A01(), "payment_account_recovered")) {
                return 5;
            }
        }
        C21860y6 r4 = this.A06;
        if (r4.A0A() && C130395zL.A00(r2, this.A05.A07()) && AnonymousClass01Y.A08(this.A0A.A01).size() == 0 && !C12980iv.A1W(super.A06.A01(), "payments_home_add_upi_number_banner_dismissed")) {
            return 4;
        }
        if (!((AbstractC118085bF) this).A01.isEmpty() && super.A03.isEmpty() && super.A02.isEmpty() && !C12980iv.A1W(this.A09.A01.A01(), "settingsQuickTipDismissedState")) {
            return 2;
        }
        C18600si r3 = super.A06;
        if (!r3.A01().getBoolean("payments_home_onboarding_banner_dismissed", false) && r3.A01().getBoolean("payments_resume_onboarding_banner_started", false) && !r4.A0A()) {
            return 1;
        }
        AnonymousClass016 r22 = ((AbstractC118085bF) this).A00;
        if (r22.A01() == null || ((AnonymousClass60P) r22.A01()).A01 != 1) {
            return 0;
        }
        return 3;
    }

    public boolean A0D() {
        C14850m9 r1 = this.A04;
        return (r1.A07(1231) || r1.A07(1433)) && C12980iv.A1W(super.A06.A01(), "payment_has_received_upi_mandate_request");
    }
}
