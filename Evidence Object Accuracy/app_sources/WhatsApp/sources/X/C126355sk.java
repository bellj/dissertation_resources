package X;

/* renamed from: X.5sk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126355sk {
    public final AnonymousClass1V8 A00;

    public C126355sk(C126385sn r6, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy r3 = new C41141sy("account");
        C41141sy.A01(r3, "action", "delete");
        if (str != null && C117295Zj.A1V(str, 1, true)) {
            C41141sy.A01(r3, "device-id", str);
        }
        C117295Zj.A1H(r3, A0M);
        this.A00 = C117295Zj.A0H(r6.A00, A0M);
    }
}
