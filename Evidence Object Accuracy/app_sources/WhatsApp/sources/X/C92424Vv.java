package X;

import android.graphics.PointF;

/* renamed from: X.4Vv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92424Vv {
    public C73023fY A00;
    public C73023fY A01;
    public final long A02;
    public final C94124bI A03;
    public final C73023fY A04;

    public C92424Vv(PointF pointF, long j) {
        C94124bI r2 = new C94124bI();
        this.A03 = r2;
        C73023fY r1 = new C73023fY();
        this.A04 = r1;
        C73023fY r0 = new C73023fY(pointF);
        this.A00 = r0;
        this.A01 = r0;
        this.A02 = j;
        r2.A00(r0, r1);
    }

    public void A00(PointF pointF, long j) {
        if (j >= this.A02) {
            C73023fY r5 = this.A00;
            C73023fY r4 = this.A01;
            if (r5 == r4) {
                this.A00 = new C73023fY(pointF);
                return;
            }
            C73023fY r3 = this.A04;
            float f = pointF.x - ((PointF) r4).x;
            ((PointF) r3).x = f;
            float f2 = pointF.y - ((PointF) r4).y;
            ((PointF) r3).y = f2;
            ((PointF) r3).x = f * 0.5f;
            ((PointF) r3).y = f2 * 0.5f;
            this.A03.A00(r5, r3);
            this.A01.set(this.A00);
            this.A00.set(pointF);
            return;
        }
        throw C12960it.A0U("events must deliver in order");
    }
}
