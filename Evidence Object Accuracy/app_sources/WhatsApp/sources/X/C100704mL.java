package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.polls.PollCreatorViewModel;

/* renamed from: X.4mL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100704mL implements TextWatcher {
    public final /* synthetic */ AnonymousClass348 A00;
    public final /* synthetic */ PollCreatorViewModel A01;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C100704mL(AnonymousClass348 r1, PollCreatorViewModel pollCreatorViewModel) {
        this.A00 = r1;
        this.A01 = pollCreatorViewModel;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        this.A01.A07.A00 = editable.toString();
    }
}
