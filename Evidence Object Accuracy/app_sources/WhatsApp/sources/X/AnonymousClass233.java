package X;

import android.app.Application;

/* renamed from: X.233  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass233 extends AnonymousClass014 {
    public Application A00;
    public C30141Wg A01;
    public AnonymousClass232 A02;
    public AbstractC14440lR A03;
    public final AnonymousClass016 A04;

    public AnonymousClass233(Application application, AnonymousClass232 r5, AbstractC14440lR r6) {
        super(application);
        this.A03 = r6;
        this.A02 = r5;
        AnonymousClass016 r2 = new AnonymousClass016();
        this.A04 = r2;
        r2.A0B(new AnonymousClass4KW(1));
    }

    public static boolean A00(C30141Wg r1) {
        if (r1 == null) {
            return false;
        }
        if (r1.A0I) {
            return true;
        }
        C30201Wm r0 = r1.A00;
        return r0 != null && !r0.A02.isEmpty();
    }

    public void A04(C30141Wg r6) {
        AnonymousClass4KW r0;
        this.A01 = r6;
        AnonymousClass016 r4 = this.A04;
        AnonymousClass009.A05(r4.A01());
        if (((AnonymousClass4KW) r4.A01()).A00 != 3 && ((AnonymousClass4KW) r4.A01()).A00 != 4 && !A00(r6)) {
            r0 = new AnonymousClass4KW(2);
        } else if (((AnonymousClass4KW) r4.A01()).A00 == 1 || ((AnonymousClass4KW) r4.A01()).A00 == 3) {
            r0 = new AnonymousClass4KW(4);
        } else {
            return;
        }
        r4.A0B(r0);
    }
}
