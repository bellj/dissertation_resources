package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54222gN extends AnonymousClass02M {
    public AnonymousClass5UD A00;
    public List A01 = C12960it.A0l();

    public C54222gN(AnonymousClass5UD r2, List list) {
        this.A00 = r2;
        this.A01 = list;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r9, int i) {
        ImageView imageView;
        Context context;
        int i2;
        C75613k8 r92 = (C75613k8) r9;
        C16700pc.A0E(r92, 0);
        C460124c r6 = (C460124c) this.A01.get(i);
        AnonymousClass5UD r5 = this.A00;
        boolean A0N = C16700pc.A0N(r6, r5);
        AbstractC16710pd r4 = r92.A03;
        ((View) C16700pc.A05(r4)).setVisibility(0);
        int i3 = r6.A01;
        if (i3 != A0N) {
            if (i3 == 2) {
                AbstractC16710pd r7 = r92.A04;
                View view = r92.A0H;
                C12990iw.A0x(view.getContext(), (ImageView) C16700pc.A05(r7), R.drawable.ic_alert);
                imageView = (ImageView) C16700pc.A05(r7);
                context = view.getContext();
                i2 = R.color.alert_icon_critical;
            } else if (i3 == 3) {
                AbstractC16710pd r72 = r92.A04;
                View view2 = r92.A0H;
                C12990iw.A0x(view2.getContext(), (ImageView) C16700pc.A05(r72), R.drawable.ic_card);
                imageView = (ImageView) C16700pc.A05(r72);
                context = view2.getContext();
                i2 = R.color.alert_icon_info;
            }
            C016307r.A00(ColorStateList.valueOf(AnonymousClass00T.A00(context, i2)), imageView);
        } else {
            AbstractC16710pd r73 = r92.A04;
            View view3 = r92.A0H;
            C12990iw.A0x(view3.getContext(), (ImageView) C16700pc.A05(r73), R.drawable.ic_alert_round);
            C016307r.A00(ColorStateList.valueOf(AnonymousClass00T.A00(view3.getContext(), R.color.alert_icon_blocker)), (ImageView) C16700pc.A05(r73));
            ((View) C16700pc.A05(r4)).setVisibility(8);
        }
        ((TextView) C16700pc.A05(r92.A05)).setText(r6.A08);
        ((TextView) C16700pc.A05(r92.A02)).setText(r6.A05);
        TextView textView = (TextView) C16700pc.A05(r92.A01);
        textView.setText(r6.A04);
        C12960it.A14(textView, r5, r6, 8);
        if (((View) C16700pc.A05(r4)).getVisibility() == 0) {
            TextView textView2 = (TextView) C16700pc.A05(r4);
            textView2.setText(textView2.getResources().getString(R.string.alert_card_dismiss_cta_text));
            C12960it.A14(textView2, r5, r6, 7);
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        return new C75613k8(C16700pc.A01(C12960it.A0E(viewGroup), viewGroup, R.layout.alert_card_list_item));
    }
}
