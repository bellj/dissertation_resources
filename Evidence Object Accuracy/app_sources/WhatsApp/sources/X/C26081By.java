package X;

import android.database.Cursor;

/* renamed from: X.1By  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26081By {
    public final AnonymousClass1C0 A00;

    public C26081By(AnonymousClass1C0 r1) {
        this.A00 = r1;
    }

    public int A00() {
        C16310on A00 = this.A00.A00();
        try {
            Cursor A09 = A00.A03.A09("SELECT COUNT(*) FROM prefetched_files WHERE prefetched = 0", null);
            int i = 0;
            if (A09 != null) {
                if (A09.moveToNext()) {
                    i = A09.getInt(0);
                }
                A09.close();
            }
            A00.close();
            return i;
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
