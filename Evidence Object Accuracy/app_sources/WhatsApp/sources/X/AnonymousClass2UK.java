package X;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

/* renamed from: X.2UK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UK extends View {
    public final int[] A00 = new int[2];
    public final int[] A01 = new int[2];
    public final /* synthetic */ AnonymousClass1s8 A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2UK(Context context, AnonymousClass1s8 r4) {
        super(context);
        this.A02 = r4;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        AnonymousClass1s8 r5 = this.A02;
        View view = r5.A06;
        if (view == null) {
            return super.onTouchEvent(motionEvent);
        }
        int[] iArr = this.A00;
        view.getLocationOnScreen(iArr);
        int[] iArr2 = this.A01;
        getLocationOnScreen(iArr2);
        motionEvent.offsetLocation((float) (iArr2[0] - iArr[0]), (float) (iArr2[1] - iArr[1]));
        return r5.A06.dispatchTouchEvent(motionEvent);
    }
}
