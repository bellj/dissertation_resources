package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.5aD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117575aD extends ArrayAdapter {
    public C117575aD(Context context, List list) {
        super(context, (int) R.layout.support_simple_spinner_dropdown_item, list);
        setDropDownViewResource(R.layout.novi_text_input_spinner_dropdown_item);
    }

    @Override // android.widget.ArrayAdapter, android.widget.SpinnerAdapter, android.widget.BaseAdapter
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) super.getDropDownView(i, view, viewGroup);
        Object item = getItem(i);
        AnonymousClass009.A05(item);
        textView.setText(((C125765rm) item).A00);
        return textView;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) super.getView(i, view, viewGroup);
        Object item = getItem(i);
        AnonymousClass009.A05(item);
        textView.setText(((C125765rm) item).A00);
        return textView;
    }
}
