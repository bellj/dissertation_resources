package X;

import android.os.Build;
import android.view.View;
import android.widget.ScrollView;

/* renamed from: X.4Yo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93004Yo {
    public static void A00(View view, ScrollView scrollView) {
        if (Build.VERSION.SDK_INT >= 21) {
            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101744o1(view, scrollView));
        }
    }

    public static boolean A01(ScrollView scrollView) {
        View childAt = scrollView.getChildAt(0);
        if (childAt == null) {
            return false;
        }
        if (scrollView.getHeight() < childAt.getHeight() + scrollView.getPaddingTop() + scrollView.getPaddingBottom()) {
            return true;
        }
        return false;
    }
}
