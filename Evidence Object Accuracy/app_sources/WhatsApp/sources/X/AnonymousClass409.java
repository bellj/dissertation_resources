package X;

/* renamed from: X.409  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass409 extends AnonymousClass4UW {
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof AnonymousClass409);
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("FilterChip(isSelected=");
        A0k.append(false);
        return C12970iu.A0u(A0k);
    }
}
