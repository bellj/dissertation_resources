package X;

import android.text.TextUtils;
import com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity;
import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchQueryFragment;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;

/* renamed from: X.3Ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66743Ot implements AnonymousClass07L {
    public final /* synthetic */ BusinessDirectoryActivity A00;

    public C66743Ot(BusinessDirectoryActivity businessDirectoryActivity) {
        this.A00 = businessDirectoryActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment = this.A00.A07;
        if (businessDirectorySearchQueryFragment == null) {
            return true;
        }
        businessDirectorySearchQueryFragment.A09.A0P(str);
        return true;
    }

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment = this.A00.A07;
        if (businessDirectorySearchQueryFragment == null) {
            return false;
        }
        BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = businessDirectorySearchQueryFragment.A09;
        String trim = str.trim();
        if (TextUtils.isEmpty(trim)) {
            return false;
        }
        businessDirectorySearchQueryViewModel.A0Q.A00(new C48152En(trim, System.currentTimeMillis()));
        if (!businessDirectorySearchQueryViewModel.A0J.A04()) {
            return false;
        }
        businessDirectorySearchQueryViewModel.A0R(trim);
        return false;
    }
}
