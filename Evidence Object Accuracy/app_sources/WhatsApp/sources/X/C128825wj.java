package X;

import android.util.Base64;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

/* renamed from: X.5wj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128825wj {
    public final String A00;
    public final C128045vT A01;

    public C128825wj(C128045vT r8) {
        String str;
        this.A01 = r8;
        StringBuilder A0h = C12960it.A0h();
        String[] split = new String(Base64.decode("MS03LTItNA==", -1)).split("-");
        for (String str2 : split) {
            Properties properties = this.A01.A03;
            if (properties != null) {
                str = properties.getProperty(str2);
            } else {
                str = null;
            }
            A0h.append(str);
        }
        this.A00 = new String(Base64.decode(A0h.toString(), -1));
    }

    public String A00(JSONObject jSONObject) {
        String str = this.A00;
        if (str.isEmpty()) {
            return null;
        }
        Matcher matcher = Pattern.compile("\\[([^\\]]*)\\]").matcher(str);
        StringBuffer stringBuffer = new StringBuffer(1000);
        while (matcher.find()) {
            String group = matcher.group();
            matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(jSONObject.optString(group.substring(1, group.length() - 1))));
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }
}
