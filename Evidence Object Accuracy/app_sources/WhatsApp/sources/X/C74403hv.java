package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.google.android.material.internal.CheckableImageButton;

/* renamed from: X.3hv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74403hv extends AnonymousClass04v {
    public final /* synthetic */ CheckableImageButton A00;

    public C74403hv(CheckableImageButton checkableImageButton) {
        this.A00 = checkableImageButton;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        super.A01(view, accessibilityEvent);
        accessibilityEvent.setChecked(this.A00.isChecked());
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        AccessibilityNodeInfo accessibilityNodeInfo = r4.A02;
        accessibilityNodeInfo.setCheckable(true);
        accessibilityNodeInfo.setChecked(this.A00.isChecked());
    }
}
