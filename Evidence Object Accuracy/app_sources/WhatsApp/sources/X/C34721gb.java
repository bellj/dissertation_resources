package X;

/* renamed from: X.1gb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34721gb extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public Long A05;
    public String A06;
    public String A07;

    public C34721gb() {
        super(2288, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(8, this.A04);
        r3.Abe(7, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A06);
        r3.Abe(1, this.A07);
        r3.Abe(4, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        StringBuilder sb = new StringBuilder("WamMdBootstrapDataGenerated {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "historySyncStageProgress", this.A04);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapHistoryPayloadType", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapPayloadType", obj2);
        Integer num3 = this.A02;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapSource", obj3);
        Integer num4 = this.A03;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapStepResult", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdRegAttemptId", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdSessionId", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdTimestamp", this.A05);
        sb.append("}");
        return sb.toString();
    }
}
