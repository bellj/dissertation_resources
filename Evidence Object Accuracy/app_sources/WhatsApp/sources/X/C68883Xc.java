package X;

/* renamed from: X.3Xc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68883Xc implements AnonymousClass19W {
    public final C14830m7 A00;
    public final C19550uI A01;
    public final C16820po A02;
    public final AnonymousClass5UI A03;

    public C68883Xc(C14830m7 r1, C19550uI r2, C16820po r3, AnonymousClass5UI r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass19W
    public void AZO(AbstractC44401yr r11) {
        C19550uI r6 = this.A01;
        C16820po r3 = this.A02;
        C64063Ec A00 = r6.A00(r3);
        if (A00 != null) {
            AnonymousClass5UI r8 = this.A03;
            C14830m7 r0 = this.A00;
            Long l = A00.A05;
            if (l != null) {
                long A0D = C12980iv.A0D(r0.A00());
                long j = A00.A00;
                AnonymousClass009.A05(l);
                if (A0D > j + l.longValue()) {
                    r6.A01(A00, new C68803Wu(this, r11), null);
                    return;
                }
            }
            Object obj = A00.A02.A00;
            AnonymousClass009.A05(obj);
            r8.A7x((String) obj).AZO(new C68873Xb(A00, this, r11));
            return;
        }
        C68793Wt r2 = new C68793Wt(this, r11);
        ((AnonymousClass17H) r6.A02.get()).A01(new AnonymousClass301(r2, r2, r6, r3), r3);
    }
}
