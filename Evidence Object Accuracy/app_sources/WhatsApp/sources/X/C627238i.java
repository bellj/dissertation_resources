package X;

import android.os.Bundle;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* renamed from: X.38i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C627238i extends AbstractC16350or {
    public WeakReference A00;
    public HashMap A01;
    public final Bundle A02;
    public final AnonymousClass19Y A03;
    public final C18640sm A04;
    public final AnonymousClass01d A05;
    public final C15890o4 A06;
    public final AnonymousClass1MJ A07;
    public final AnonymousClass11G A08;
    public final C20800wL A09;
    public final String A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;

    public C627238i(Bundle bundle, ActivityC13810kN r3, AnonymousClass19Y r4, C18640sm r5, AnonymousClass01d r6, C15890o4 r7, AnonymousClass1MJ r8, AnonymousClass11G r9, C20800wL r10, String str, boolean z, boolean z2, boolean z3) {
        super(r3);
        this.A00 = C12970iu.A10(r3);
        this.A03 = r4;
        this.A05 = r6;
        this.A08 = r9;
        this.A09 = r10;
        this.A04 = r5;
        this.A06 = r7;
        this.A0C = z;
        this.A0B = z2;
        this.A0D = z3;
        this.A0A = str;
        this.A02 = bundle;
        this.A07 = r8;
    }
}
