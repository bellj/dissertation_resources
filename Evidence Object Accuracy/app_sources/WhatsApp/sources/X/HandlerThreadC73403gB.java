package X;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

/* renamed from: X.3gB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class HandlerThreadC73403gB extends HandlerThread implements Handler.Callback {
    public Handler A00;
    public RunnableC111955Bm A01;
    public C73663gb A02;
    public Error A03;
    public RuntimeException A04;

    public HandlerThreadC73403gB() {
        super("ExoPlayer:DummySurface");
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        int i = message.what;
        try {
            if (i == 1) {
                try {
                    try {
                        this.A01.A01(message.arg1);
                        this.A02 = new C73663gb(this.A01.A00, this);
                        synchronized (this) {
                            notify();
                        }
                        return true;
                    } catch (Error e) {
                        C64923Hl.A01("DummySurface", "Failed to initialize dummy surface", e);
                        this.A03 = e;
                        synchronized (this) {
                            notify();
                            return true;
                        }
                    }
                } catch (RuntimeException e2) {
                    C64923Hl.A01("DummySurface", "Failed to initialize dummy surface", e2);
                    this.A04 = e2;
                    synchronized (this) {
                        notify();
                    }
                }
            } else if (i == 2) {
                try {
                    this.A01.A00();
                } finally {
                    try {
                        return true;
                    } finally {
                    }
                }
                return true;
            }
            return true;
        } catch (Throwable th) {
            synchronized (this) {
                notify();
                throw th;
            }
        }
    }
}
