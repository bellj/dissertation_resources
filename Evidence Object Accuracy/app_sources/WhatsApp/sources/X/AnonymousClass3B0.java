package X;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

/* renamed from: X.3B0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3B0 {
    public static final String A00 = "RippleDrawableUtils";

    public static Drawable A00(C14260l7 r2, AnonymousClass28D r3, AnonymousClass28D r4) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass3AC.A00(r2, r3, r4);
        }
        AnonymousClass28D A0F = r3.A0F(36);
        if (A0F != null) {
            return C65093Ic.A00().A06.A01(r2, A0F, r4);
        }
        C28691Op.A01(A00, "Client received a RippleDrawable with null fallback", null);
        return new ColorDrawable();
    }
}
