package X;

/* renamed from: X.56Z  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass56Z implements AnonymousClass5UB {
    public final /* synthetic */ C52502ax A00;
    public final /* synthetic */ AnonymousClass3IZ A01;

    public /* synthetic */ AnonymousClass56Z(C52502ax r1, AnonymousClass3IZ r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5UB
    public final void AW8(int[] iArr) {
        AnonymousClass3IZ r0 = this.A01;
        C52502ax r1 = this.A00;
        r0.A02(iArr);
        r1.setEmoji(iArr);
        AnonymousClass3JF.A02(r0.A0Q, iArr);
        r1.invalidate();
    }
}
