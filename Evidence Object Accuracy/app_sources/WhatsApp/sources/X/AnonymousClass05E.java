package X;

import android.app.Application;
import android.os.Bundle;
import androidx.lifecycle.SavedStateHandleController;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/* renamed from: X.05E  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass05E extends AnonymousClass05F {
    public static final Class[] A05 = {Application.class, AnonymousClass07E.class};
    public static final Class[] A06 = {AnonymousClass07E.class};
    public final Application A00;
    public final Bundle A01;
    public final AbstractC009904y A02;
    public final AbstractC009404s A03;
    public final AnonymousClass058 A04;

    public AnonymousClass05E(Application application, Bundle bundle, AbstractC001500q r4) {
        AnonymousClass0EQ r0;
        this.A04 = r4.AGO();
        this.A02 = r4.ADr();
        this.A01 = bundle;
        this.A00 = application;
        if (application != null) {
            AnonymousClass0EQ r02 = AnonymousClass0EQ.A01;
            r0 = r02;
            if (r02 == null) {
                AnonymousClass0EQ r03 = new AnonymousClass0EQ(application);
                AnonymousClass0EQ.A01 = r03;
                r0 = r03;
            }
        } else {
            AnonymousClass0Yo r04 = AnonymousClass0Yo.A00;
            r0 = r04;
            if (r04 == null) {
                AnonymousClass0Yo r05 = new AnonymousClass0Yo();
                AnonymousClass0Yo.A00 = r05;
                r0 = r05;
            }
        }
        this.A03 = r0;
    }

    @Override // X.AnonymousClass05G
    public void A00(AnonymousClass015 r3) {
        SavedStateHandleController.A00(this.A02, r3, this.A04);
    }

    @Override // X.AnonymousClass05F
    public AnonymousClass015 A01(Class cls, String str) {
        Class[] clsArr;
        AnonymousClass015 r0;
        boolean isAssignableFrom = AnonymousClass014.class.isAssignableFrom(cls);
        if (!isAssignableFrom || this.A00 == null) {
            clsArr = A06;
        } else {
            clsArr = A05;
        }
        Constructor<?>[] constructors = cls.getConstructors();
        for (Constructor<?> constructor : constructors) {
            if (Arrays.equals(clsArr, constructor.getParameterTypes())) {
                AnonymousClass058 r3 = this.A04;
                AbstractC009904y r2 = this.A02;
                SavedStateHandleController savedStateHandleController = new SavedStateHandleController(AnonymousClass07E.A00(r3.A00(str), this.A01), str);
                savedStateHandleController.A02(r2, r3);
                SavedStateHandleController.A01(r2, r3);
                if (isAssignableFrom) {
                    try {
                        Application application = this.A00;
                        if (application != null) {
                            r0 = (AnonymousClass015) constructor.newInstance(application, savedStateHandleController.A01);
                            r0.A00(savedStateHandleController);
                            return r0;
                        }
                    } catch (IllegalAccessException e) {
                        StringBuilder sb = new StringBuilder("Failed to access ");
                        sb.append(cls);
                        throw new RuntimeException(sb.toString(), e);
                    } catch (InstantiationException e2) {
                        StringBuilder sb2 = new StringBuilder("A ");
                        sb2.append(cls);
                        sb2.append(" cannot be instantiated.");
                        throw new RuntimeException(sb2.toString(), e2);
                    } catch (InvocationTargetException e3) {
                        StringBuilder sb3 = new StringBuilder("An exception happened in constructor of ");
                        sb3.append(cls);
                        throw new RuntimeException(sb3.toString(), e3.getCause());
                    }
                }
                r0 = (AnonymousClass015) constructor.newInstance(savedStateHandleController.A01);
                r0.A00(savedStateHandleController);
                return r0;
            }
        }
        return this.A03.A7r(cls);
    }

    @Override // X.AnonymousClass05F, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return A01(cls, canonicalName);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
}
