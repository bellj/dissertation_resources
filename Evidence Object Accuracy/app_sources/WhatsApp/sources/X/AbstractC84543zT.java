package X;

import android.content.Context;
import com.whatsapp.biz.catalog.view.AspectRatioFrameLayout;

/* renamed from: X.3zT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC84543zT extends AspectRatioFrameLayout {
    public int A00;
    public C14850m9 A01;
    public AbstractC15340mz A02;

    public void A03() {
    }

    public boolean A04() {
        return false;
    }

    public abstract void setMessage(AbstractC16130oV v);

    public void setScrolling(boolean z) {
    }

    public void setShouldPlay(boolean z) {
    }

    public AbstractC84543zT(Context context) {
        super(context);
    }

    public void setRadius(int i) {
        this.A00 = i;
    }
}
