package X;

import com.whatsapp.Conversation;
import com.whatsapp.reactions.ReactionsTrayViewModel;

/* renamed from: X.3UB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UB implements AbstractC116455Vm {
    public final /* synthetic */ Conversation A00;

    public AnonymousClass3UB(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        Conversation conversation = this.A00;
        if (!conversation.A2n()) {
            C12960it.A0v(conversation.A2w);
        }
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        Conversation conversation = this.A00;
        if (conversation.A2n()) {
            ReactionsTrayViewModel reactionsTrayViewModel = ((AbstractActivityC13750kH) conversation).A0g;
            AnonymousClass009.A05(reactionsTrayViewModel);
            reactionsTrayViewModel.A05(AbstractC36671kL.A06(iArr));
            AbstractC15340mz r0 = ((AbstractActivityC13750kH) conversation).A0g.A02;
            AnonymousClass009.A05(r0);
            conversation.Af1(r0);
            return;
        }
        AbstractC36671kL.A08(conversation.A2w, iArr, 0);
        conversation.A2w.callOnClick();
    }
}
