package X;

/* renamed from: X.5Xm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116935Xm {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A;
    public static final AnonymousClass1TK A0B;
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;

    static {
        AnonymousClass1TK A12 = C72453ed.A12("1.2.643.7");
        A0J = A12;
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("1", A12);
        A00 = A022;
        A0D = AnonymousClass1TL.A02("1.2.2", A022);
        A0E = AnonymousClass1TL.A02("1.2.3", A022);
        A0F = AnonymousClass1TL.A02("1.4.1", A022);
        A0G = AnonymousClass1TL.A02("1.4.2", A022);
        A05 = AnonymousClass1TL.A02("1.1.1", A022);
        A08 = AnonymousClass1TL.A02("1.1.2", A022);
        A0H = AnonymousClass1TL.A02("1.3.2", A022);
        A0I = AnonymousClass1TL.A02("1.3.3", A022);
        AnonymousClass1TK A023 = AnonymousClass1TL.A02("1.6", A022);
        A01 = A023;
        A02 = AnonymousClass1TL.A02("1", A023);
        A03 = AnonymousClass1TL.A02("2", A023);
        AnonymousClass1TK A024 = AnonymousClass1TL.A02("2.1.1", A022);
        A06 = A024;
        A07 = AnonymousClass1TL.A02("1", A024);
        AnonymousClass1TK A025 = AnonymousClass1TL.A02("2.1.2", A022);
        A09 = A025;
        A0A = AnonymousClass1TL.A02("1", A025);
        A0B = AnonymousClass1TL.A02("2", A025);
        A0C = AnonymousClass1TL.A02("3", A025);
        A04 = AnonymousClass1TL.A02("2.5.1.1", A022);
    }
}
