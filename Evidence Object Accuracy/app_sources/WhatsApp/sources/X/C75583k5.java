package X;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.3k5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75583k5 extends AnonymousClass03U {
    public final Button A00;
    public final ImageView A01;
    public final TextView A02;
    public final TextEmojiLabel A03;

    public /* synthetic */ C75583k5(View view) {
        super(view);
        AnonymousClass028.A0l(view, true);
        this.A03 = (TextEmojiLabel) view.findViewById(R.id.name);
        this.A02 = C12960it.A0J(view, R.id.description);
        this.A01 = C12970iu.A0L(view, R.id.image);
        this.A00 = (Button) view.findViewById(R.id.add_contact_btn);
    }
}
