package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.4vY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106344vY implements AnonymousClass5WW {
    public final C14260l7 A00;
    public final AnonymousClass2k7 A01;

    public /* synthetic */ C106344vY(C14260l7 r1, AnonymousClass2k7 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return false;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
        this.A01.A05((View) obj, this.A00);
    }
}
