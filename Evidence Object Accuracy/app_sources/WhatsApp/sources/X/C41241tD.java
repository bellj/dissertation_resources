package X;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/* renamed from: X.1tD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41241tD {
    public final AnonymousClass00O A00 = new AnonymousClass00O();

    public final void A00(Object obj, List list, Set set) {
        if (list.contains(obj)) {
            return;
        }
        if (!set.contains(obj)) {
            set.add(obj);
            List<Object> list2 = (List) this.A00.get(obj);
            if (list2 != null) {
                for (Object obj2 : list2) {
                    A00(obj2, list, set);
                }
            }
            set.remove(obj);
            list.add(obj);
            return;
        }
        throw new C41271tG();
    }

    public boolean A01(Object obj, Object obj2) {
        AnonymousClass00O r1 = this.A00;
        if (!r1.containsKey(obj) || !r1.containsKey(obj2)) {
            throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
        }
        List list = (List) r1.get(obj);
        if (list == null) {
            list = new ArrayList();
            r1.put(obj, list);
        }
        return list.add(obj2);
    }
}
