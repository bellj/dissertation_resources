package X;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import com.whatsapp.gallerypicker.GalleryPickerFragment;
import com.whatsapp.util.Log;

/* renamed from: X.2ZJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZJ extends ContentObserver {
    public final /* synthetic */ GalleryPickerFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2ZJ(Handler handler, GalleryPickerFragment galleryPickerFragment) {
        super(handler);
        this.A00 = galleryPickerFragment;
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z) {
        ContentResolver contentResolver;
        GalleryPickerFragment galleryPickerFragment = this.A00;
        ActivityC000900k A0B = galleryPickerFragment.A0B();
        if (A0B == null || A0B.getContentResolver() == null) {
            StringBuilder A0k = C12960it.A0k("gallerypicker/");
            A0k.append(galleryPickerFragment.A00);
            Log.i(C12960it.A0d(" no content resolver", A0k));
            return;
        }
        ActivityC000900k A0B2 = galleryPickerFragment.A0B();
        if (A0B2 == null) {
            contentResolver = null;
        } else {
            contentResolver = A0B2.getContentResolver();
        }
        boolean z2 = false;
        String[] strArr = {"volume"};
        Uri mediaScannerUri = MediaStore.getMediaScannerUri();
        if (contentResolver != null) {
            try {
                Cursor query = contentResolver.query(mediaScannerUri, strArr, null, null, null);
                if (query != null) {
                    try {
                        if (query.getCount() == 1) {
                            query.moveToFirst();
                            z2 = "external".equals(query.getString(0));
                        }
                        query.close();
                    } catch (Throwable th) {
                        try {
                            query.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            } catch (UnsupportedOperationException unused2) {
            }
        }
        galleryPickerFragment.A1C(false, z2);
    }
}
