package X;

/* renamed from: X.5sx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126485sx {
    public final AnonymousClass1V8 A00;

    public C126485sx(AnonymousClass3CT r19, C126495sy r20, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-collect-from-vpa");
        if (AnonymousClass3JT.A0E(str, 1, 100, false)) {
            C41141sy.A01(A0N, "sender-vpa", str);
        }
        if (str2 != null && AnonymousClass3JT.A0E(str2, 1, 100, true)) {
            C41141sy.A01(A0N, "sender-vpa-id", str2);
        }
        if (str3 != null && AnonymousClass3JT.A0E(str3, 8, 15, true)) {
            C41141sy.A01(A0N, "sender-upi-number", str3);
        }
        if (AnonymousClass3JT.A0E(str4, 1, 100, false)) {
            C41141sy.A01(A0N, "receiver-vpa", str4);
        }
        if (str5 != null && AnonymousClass3JT.A0E(str5, 1, 100, true)) {
            C41141sy.A01(A0N, "receiver-vpa-id", str5);
        }
        if (C117305Zk.A1X(str6, 1, false)) {
            C41141sy.A01(A0N, "upi-bank-info", str6);
        }
        if (AnonymousClass3JT.A0E(str7, 0, 35, false)) {
            C41141sy.A01(A0N, "seq-no", str7);
        }
        if (AnonymousClass3JT.A0E(str8, 1, 100, false)) {
            C41141sy.A01(A0N, "credential-id", str8);
        }
        if (str9 != null && C117305Zk.A1X(str9, 0, true)) {
            C41141sy.A01(A0N, "note", str9);
        }
        if (AnonymousClass3JT.A0E(str10, 1, 100, false)) {
            C41141sy.A01(A0N, "message-id", str10);
        }
        if (C117295Zj.A1V(str11, 1, false)) {
            C41141sy.A01(A0N, "device-id", str11);
        }
        A0N.A05(r20.A00);
        this.A00 = C117295Zj.A0J(A0N, A0M, r19);
    }
}
