package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4l4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99914l4 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass2UQ(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass2UQ[i];
    }
}
