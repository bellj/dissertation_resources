package X;

import androidx.appcompat.widget.SearchView;

/* renamed from: X.0cL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09090cL implements Runnable {
    public final /* synthetic */ SearchView A00;

    public RunnableC09090cL(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00.A09();
    }
}
