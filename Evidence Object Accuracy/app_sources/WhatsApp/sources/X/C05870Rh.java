package X;

/* renamed from: X.0Rh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05870Rh {
    public static final C05870Rh A02 = new C05870Rh(null, false);
    public static final C05870Rh A03 = new C05870Rh(null, true);
    public final Throwable A00;
    public final boolean A01;

    static {
        if (!AnonymousClass041.A03) {
        }
    }

    public C05870Rh(Throwable th, boolean z) {
        this.A01 = z;
        this.A00 = th;
    }
}
