package X;

/* renamed from: X.4IS  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4IS {
    public final String A00;

    public AnonymousClass4IS(String str, String str2) {
        boolean z = str.length() <= 23;
        Object[] A1a = C12980iv.A1a();
        A1a[0] = str;
        C12960it.A1P(A1a, 23, 1);
        if (!z) {
            throw C12970iu.A0f(String.format("tag \"%s\" is longer than the %d character maximum", A1a));
        } else if (str2 == null || str2.length() <= 0) {
            this.A00 = null;
        } else {
            this.A00 = str2;
        }
    }
}
