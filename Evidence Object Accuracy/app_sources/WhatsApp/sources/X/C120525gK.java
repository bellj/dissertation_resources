package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.HashMap;

/* renamed from: X.5gK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120525gK extends C126705tJ {
    public AnonymousClass6MS A00;
    public final Context A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C18640sm A04;
    public final AnonymousClass102 A05;
    public final C14850m9 A06;
    public final C17220qS A07;
    public final C1329668y A08;
    public final C21860y6 A09;
    public final C18650sn A0A;
    public final C17900ra A0B;
    public final C17070qD A0C;
    public final AnonymousClass6BE A0D;
    public final C121265hX A0E;
    public final C18590sh A0F;

    public C120525gK(Context context, C14900mE r3, C15570nT r4, C18640sm r5, AnonymousClass102 r6, C14850m9 r7, C17220qS r8, C1308460e r9, C1329668y r10, C21860y6 r11, C18650sn r12, C18610sj r13, C17900ra r14, C17070qD r15, AnonymousClass6MS r16, AnonymousClass6BE r17, C121265hX r18, C18590sh r19) {
        super(r9.A04, r13);
        this.A01 = context;
        this.A06 = r7;
        this.A02 = r3;
        this.A03 = r4;
        this.A07 = r8;
        this.A0F = r19;
        this.A0C = r15;
        this.A09 = r11;
        this.A05 = r6;
        this.A0B = r14;
        this.A0D = r17;
        this.A04 = r5;
        this.A0A = r12;
        this.A08 = r10;
        this.A0E = r18;
        this.A00 = r16;
    }

    public void A00() {
        this.A0D.AeG();
        Log.i("PAY: IndiaUpiPaymentSetup sendGetListKeys called");
        String A0A = this.A08.A0A();
        if (!TextUtils.isEmpty(A0A)) {
            StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiPaymentSetup got cached listkeys; callback: ");
            A0k.append(this.A00);
            C12960it.A1F(A0k);
            AnonymousClass6MS r1 = this.A00;
            if (r1 != null) {
                r1.ARr(null, A0A);
                return;
            }
            return;
        }
        C64513Fv r8 = super.A00;
        r8.A04("upi-list-keys");
        Log.i("PAY: IndiaUPIPaymentBankSetup sendGetListKeys");
        this.A0E.A02(185475201, "in_upi_list_keys_tag");
        C17220qS r3 = this.A07;
        String A01 = r3.A01();
        C117325Zm.A06(r3, new C120625gU(this.A01, this.A02, this.A0A, r8, this), new C126535t2(new AnonymousClass3CS(A01)).A00, A01);
    }

    public final void A01(AnonymousClass1ZR r20, AnonymousClass1ZR r21, String str, String str2, String str3, String str4, String str5, String str6, HashMap hashMap) {
        String str7;
        String str8;
        this.A0D.AeG();
        Log.i("PAY: IndiaUpiPaymentSetup sendSetPin called");
        C64513Fv r3 = super.A00;
        r3.A04("upi-set-mpin");
        String str9 = null;
        if (hashMap != null) {
            str9 = C1308460e.A00("SMS", hashMap);
            str7 = C1308460e.A00("MPIN", hashMap);
            str8 = C1308460e.A00("ATMPIN", hashMap);
        } else {
            str7 = null;
            str8 = null;
        }
        C17220qS r2 = this.A07;
        String A01 = r2.A01();
        C117325Zm.A05(r2, new C120635gV(this.A01, this.A02, this.A0A, r3, this), new C130535zZ(new AnonymousClass3CT(A01), C117315Zl.A0K(r20), str, (String) C117295Zj.A0R(r21), str2, str6, this.A0F.A01(), str9, str7, str8, str3, str4, str5).A00, A01);
    }

    public final void A02(AnonymousClass1ZR r16, AnonymousClass1ZR r17, String str, String str2, String str3, HashMap hashMap) {
        String str4;
        this.A0D.AeG();
        Log.i("PAY: IndiaUpiPaymentSetup sendChangePin called");
        C64513Fv r13 = super.A00;
        r13.A04("upi-change-mpin");
        String str5 = null;
        if (hashMap != null) {
            str5 = C1308460e.A00("MPIN", hashMap);
            str4 = C1308460e.A00("NMPIN", hashMap);
        } else {
            str4 = null;
        }
        C17220qS r2 = this.A07;
        String A01 = r2.A01();
        C117325Zm.A06(r2, new C120645gW(this.A01, this.A02, this.A0A, r13, this), new C126425sr(new AnonymousClass3CT(A01), str2, str3, str5, str4, this.A0F.A01(), C117315Zl.A0K(r16), str, (String) C117295Zj.A0R(r17)).A00, A01);
    }
}
