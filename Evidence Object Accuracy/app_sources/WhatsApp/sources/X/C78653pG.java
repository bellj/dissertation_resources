package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3pG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78653pG extends AnonymousClass1U5 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99204jv();
    public final String A00;
    public final String A01;
    public final String A02;

    public C78653pG(String str, String str2, String str3) {
        C13020j0.A01(str);
        this.A00 = str;
        C13020j0.A01(str2);
        this.A01 = str2;
        C13020j0.A01(str3);
        this.A02 = str3;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof C78653pG) {
                C78653pG r5 = (C78653pG) obj;
                if (!this.A00.equals(r5.A00) || !C13300jT.A00(r5.A01, this.A01) || !C13300jT.A00(r5.A02, this.A02)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.A00.hashCode();
    }

    @Override // java.lang.Object
    public final String toString() {
        String str = this.A00;
        int i = 0;
        for (char c : str.toCharArray()) {
            i += c;
        }
        String trim = str.trim();
        int length = trim.length();
        if (length > 25) {
            String substring = trim.substring(0, 10);
            String substring2 = trim.substring(length - 10, length);
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(substring) + 16 + C12970iu.A07(substring2));
            A0t.append(substring);
            A0t.append("...");
            A0t.append(substring2);
            trim = C12960it.A0e("::", A0t, i);
        }
        String str2 = this.A01;
        String str3 = this.A02;
        StringBuilder A0t2 = C12980iv.A0t(C12970iu.A07(trim) + 31 + C12970iu.A07(str2) + C12970iu.A07(str3));
        A0t2.append("Channel{token=");
        A0t2.append(trim);
        A0t2.append(", nodeId=");
        A0t2.append(str2);
        A0t2.append(", path=");
        A0t2.append(str3);
        return C12960it.A0d("}", A0t2);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        boolean A0K = C95654e8.A0K(parcel, this.A00);
        C95654e8.A0D(parcel, this.A01, 3, A0K);
        C95654e8.A0D(parcel, this.A02, 4, A0K);
        C95654e8.A06(parcel, A00);
    }
}
