package X;

import android.text.TextUtils;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2tz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59112tz extends AbstractC75653kC {
    public final WaTextView A00;
    public final WaTextView A01;
    public final WaTextView A02;
    public final WaTextView A03;

    public C59112tz(View view) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.save_label);
        this.A00 = C12960it.A0N(view, R.id.save_amount);
        this.A03 = C12960it.A0N(view, R.id.subtotal_label);
        this.A02 = C12960it.A0N(view, R.id.subtotal_amount);
    }

    @Override // X.AbstractC75653kC
    public void A08(AnonymousClass4JV r5) {
        if (r5 instanceof C84503zP) {
            C84503zP r52 = (C84503zP) r5;
            boolean isEmpty = TextUtils.isEmpty(r52.A00);
            WaTextView waTextView = this.A01;
            if (isEmpty) {
                waTextView.setVisibility(8);
                this.A00.setVisibility(8);
            } else {
                waTextView.setVisibility(0);
                WaTextView waTextView2 = this.A00;
                waTextView2.setVisibility(0);
                waTextView2.setText(r52.A00);
            }
            boolean isEmpty2 = TextUtils.isEmpty(r52.A01);
            WaTextView waTextView3 = this.A03;
            if (isEmpty2) {
                waTextView3.setVisibility(8);
                this.A02.setVisibility(8);
                return;
            }
            waTextView3.setVisibility(0);
            WaTextView waTextView4 = this.A02;
            waTextView4.setVisibility(0);
            waTextView4.setText(r52.A01);
        }
    }
}
