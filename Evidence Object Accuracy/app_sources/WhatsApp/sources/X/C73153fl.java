package X;

import android.media.AudioDeviceCallback;
import android.media.AudioDeviceInfo;

/* renamed from: X.3fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73153fl extends AudioDeviceCallback {
    public final /* synthetic */ C236912q A00;

    public C73153fl(C236912q r1) {
        this.A00 = r1;
    }

    @Override // android.media.AudioDeviceCallback
    public void onAudioDevicesAdded(AudioDeviceInfo[] audioDeviceInfoArr) {
        for (AudioDeviceInfo audioDeviceInfo : audioDeviceInfoArr) {
            if (C236912q.A03(audioDeviceInfo)) {
                this.A00.A05(2);
                return;
            }
        }
    }

    @Override // android.media.AudioDeviceCallback
    public void onAudioDevicesRemoved(AudioDeviceInfo[] audioDeviceInfoArr) {
        for (AudioDeviceInfo audioDeviceInfo : audioDeviceInfoArr) {
            if (C236912q.A03(audioDeviceInfo)) {
                this.A00.A05(0);
                return;
            }
        }
    }
}
