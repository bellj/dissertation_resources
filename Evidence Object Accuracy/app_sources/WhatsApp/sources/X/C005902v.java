package X;

import android.app.Notification;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import java.lang.reflect.Field;

/* renamed from: X.02v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C005902v {
    public static Bundle A00(Notification notification) {
        if (Build.VERSION.SDK_INT >= 19) {
            return notification.extras;
        }
        synchronized (AnonymousClass0U1.A02) {
            if (!AnonymousClass0U1.A01) {
                try {
                    Field field = AnonymousClass0U1.A00;
                    if (field == null) {
                        field = Notification.class.getDeclaredField("extras");
                        if (!Bundle.class.isAssignableFrom(field.getType())) {
                            Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                            AnonymousClass0U1.A01 = true;
                        } else {
                            field.setAccessible(true);
                            AnonymousClass0U1.A00 = field;
                        }
                    }
                    Bundle bundle = (Bundle) field.get(notification);
                    if (bundle == null) {
                        bundle = new Bundle();
                        AnonymousClass0U1.A00.set(notification, bundle);
                    }
                    return bundle;
                } catch (IllegalAccessException e) {
                    Log.e("NotificationCompat", "Unable to access notification extras", e);
                    AnonymousClass0U1.A01 = true;
                    return null;
                } catch (NoSuchFieldException e2) {
                    Log.e("NotificationCompat", "Unable to access notification extras", e2);
                    AnonymousClass0U1.A01 = true;
                    return null;
                }
            }
            return null;
        }
    }
}
