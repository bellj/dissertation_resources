package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.2Az  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC47512Az {
    void A5h(AnonymousClass5XZ v);

    long AB1();

    long ABh();

    int AC2();

    int AC3();

    int AC8();

    long AC9();

    Timeline ACE();

    int ACF();

    long ACb();

    boolean AFj();

    int AFl();

    long AHF();

    boolean AJt();

    void AaK(AnonymousClass5XZ v);

    void AbS(int i, long j);

    void AcW(boolean z);
}
