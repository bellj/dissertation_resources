package X;

import android.content.Context;
import java.util.concurrent.Callable;

/* renamed from: X.0ek  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10510ek implements Callable {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;

    public CallableC10510ek(Context context, String str, String str2) {
        this.A00 = context;
        this.A02 = str;
        this.A01 = str2;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:112:0x0075 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:117:0x0043 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:100:0x0075 */
    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: X.0bt */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r7v1, resolved type: X.0bt */
    /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r2v3, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r7v2, resolved type: X.0bt */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r7v6, types: [X.0Jk] */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6, types: [java.lang.Object, java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r7v7, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r2v7, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r2v10, types: [X.0SS] */
    /* JADX WARN: Type inference failed for: r2v16 */
    /* JADX WARN: Type inference failed for: r2v17 */
    /* JADX WARN: Type inference failed for: r7v10 */
    /* JADX WARN: Type inference failed for: r7v11 */
    /* JADX WARNING: Can't wrap try/catch for region: R(19:(11:25|117|26|(1:28)|30|79|(1:81)|82|(1:84)(1:92)|85|(4:87|88|(1:90)|91))|112|31|111|32|107|33|(8:35|(1:37)|38|(2:46|(3:48|123|75)(1:49))(2:42|(3:44|123|75)(1:45))|50|(2:52|(1:54))|123|75)|115|55|(4:57|71|123|75)|58|(3:119|59|(1:61)(1:105))|62|63|71|123|75|(4:(0)|(1:125)|(0)|(0))) */
    /* JADX WARNING: Can't wrap try/catch for region: R(22:2|(3:4|8|19)|23|(11:25|117|26|(1:28)|30|79|(1:81)|82|(1:84)(1:92)|85|(4:87|88|(1:90)|91))|112|31|111|32|107|33|(8:35|(1:37)|38|(2:46|(3:48|123|75)(1:49))(2:42|(3:44|123|75)(1:45))|50|(2:52|(1:54))|123|75)|115|55|(4:57|71|123|75)|58|(3:119|59|(1:61)(1:105))|62|63|71|123|75|(4:(0)|(1:125)|(0)|(0))) */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006c, code lost:
        if (r8.exists() != false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01c1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01c2, code lost:
        X.AnonymousClass0R5.A01("get error failed ", r1);
        r1 = r1.getMessage();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01d6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01d7, code lost:
        r4 = new X.AnonymousClass0ST((java.lang.Throwable) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01dc, code lost:
        if (0 == 0) goto L_0x01e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01e2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01e3, code lost:
        X.AnonymousClass0R5.A01(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01e6, code lost:
        if (r3 == null) goto L_0x0226;
     */
    @Override // java.util.concurrent.Callable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object call() {
        /*
        // Method dump skipped, instructions count: 568
        */
        throw new UnsupportedOperationException("Method not decompiled: X.CallableC10510ek.call():java.lang.Object");
    }
}
