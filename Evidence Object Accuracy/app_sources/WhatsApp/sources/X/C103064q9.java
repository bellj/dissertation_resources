package X;

import android.content.Context;
import com.whatsapp.deeplink.DeepLinkActivity;

/* renamed from: X.4q9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103064q9 implements AbstractC009204q {
    public final /* synthetic */ DeepLinkActivity A00;

    public C103064q9(DeepLinkActivity deepLinkActivity) {
        this.A00 = deepLinkActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
