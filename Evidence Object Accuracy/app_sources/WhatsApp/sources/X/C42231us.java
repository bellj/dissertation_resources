package X;

import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;

/* renamed from: X.1us  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42231us {
    public C42251uu A00 = null;
    public Map A01;
    public Set A02;
    public final C20840wP A03;

    public C42231us(C20840wP r2) {
        this.A03 = r2;
        A01();
    }

    public final void A00() {
        C20840wP r4;
        SharedPreferences.Editor editor;
        Set<C42251uu> set;
        try {
            C42251uu r0 = this.A00;
            if (r0 == null || !r0.A02()) {
                r4 = this.A03;
                r4.A01().edit().remove("current_running_sync").apply();
            } else {
                r4 = this.A03;
                r4.A01().edit().putString("current_running_sync", this.A00.A01()).apply();
            }
        } catch (JSONException unused) {
            r4 = this.A03;
            r4.A01().edit().remove("current_running_sync").apply();
        }
        if (!this.A02.isEmpty()) {
            HashSet hashSet = new HashSet();
            synchronized (this) {
                set = this.A02;
            }
            for (C42251uu r1 : set) {
                try {
                    if (r1.A02()) {
                        hashSet.add(r1.A01());
                    }
                } catch (JSONException unused2) {
                }
            }
            if (!hashSet.isEmpty()) {
                editor = r4.A01().edit().putStringSet("queued_running_sync_set", hashSet);
                editor.apply();
            }
        }
        editor = r4.A01().edit().remove("queued_running_sync_set");
        editor.apply();
    }

    public synchronized void A01() {
        this.A02 = new HashSet();
        this.A01 = new HashMap();
    }
}
