package X;

import com.whatsapp.jid.DeviceJid;
import java.util.concurrent.Callable;

/* renamed from: X.1uo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CallableC42191uo implements Callable {
    public final C15990oG A00;
    public final DeviceJid A01;

    public CallableC42191uo(C15990oG r1, DeviceJid deviceJid) {
        this.A01 = deviceJid;
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        return this.A00.A0E(C15940oB.A02(this.A01));
    }
}
