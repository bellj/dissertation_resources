package X;

/* renamed from: X.0OM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OM {
    public float A00;
    public float A01;

    public AnonymousClass0OM(float f, float f2) {
        this.A00 = f;
        this.A01 = f2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00);
        sb.append("x");
        sb.append(this.A01);
        return sb.toString();
    }
}
