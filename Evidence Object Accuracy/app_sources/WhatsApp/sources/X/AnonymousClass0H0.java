package X;

import java.util.List;

/* renamed from: X.0H0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0H0 extends AnonymousClass0H3 {
    public AnonymousClass0H0(List list) {
        super(list);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r2, float f) {
        return Integer.valueOf(A09(r2, f));
    }

    public int A09(AnonymousClass0U8 r5, float f) {
        Object obj;
        Object obj2 = r5.A0F;
        if (obj2 == null || (obj = r5.A09) == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        AnonymousClass0SF r1 = this.A03;
        if (r1 != null) {
            r5.A08.floatValue();
            A02();
            AnonymousClass0NB r0 = r1.A02;
            r0.A01 = obj2;
            r0.A00 = obj;
            Number number = (Number) r1.A01;
            if (number != null) {
                return number.intValue();
            }
        }
        int i = r5.A05;
        if (i == 784923401) {
            i = ((Number) obj2).intValue();
            r5.A05 = i;
        }
        int i2 = r5.A04;
        if (i2 == 784923401) {
            i2 = ((Number) r5.A09).intValue();
            r5.A04 = i2;
        }
        return (int) (((float) i) + (f * ((float) (i2 - i))));
    }
}
