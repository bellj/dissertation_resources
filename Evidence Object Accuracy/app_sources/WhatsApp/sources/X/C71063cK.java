package X;

import java.util.ArrayList;

/* renamed from: X.3cK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71063cK implements AbstractC115485Rs {
    public static final ArrayList A06;
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3HF A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    static {
        String[] strArr = new String[3];
        strArr[0] = "FAILED";
        strArr[1] = "PENDING";
        A06 = C12960it.A0m("VERIFIED", strArr, 2);
    }

    public C71063cK(AbstractC15710nm r14, AnonymousClass1V8 r15) {
        AnonymousClass1V8.A01(r15, "payout");
        AnonymousClass3JT.A04(null, r15, String.class, C12970iu.A0j(), C12970iu.A0k(), "bank", new String[]{"type"}, false);
        this.A02 = (String) AnonymousClass3JT.A03(null, r15, String.class, 1L, 100L, null, new String[]{"account-number"}, false);
        this.A03 = (String) AnonymousClass3JT.A04(null, r15, String.class, 1L, 100L, null, new String[]{"bank-name"}, false);
        this.A04 = (String) AnonymousClass3JT.A03(null, r15, String.class, 1L, 100L, null, new String[]{"code"}, false);
        this.A05 = AnonymousClass3JT.A09(r15, A06, new String[]{"verification-status"});
        this.A01 = (AnonymousClass3HF) AnonymousClass3JT.A02(r14, r15, 32);
        this.A00 = r15;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C71063cK.class != obj.getClass()) {
                return false;
            }
            C71063cK r5 = (C71063cK) obj;
            if (!this.A05.equals(r5.A05) || !C29941Vi.A00(this.A02, r5.A02) || !this.A03.equals(r5.A03) || !C29941Vi.A00(this.A04, r5.A04) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[5];
        objArr[0] = this.A05;
        objArr[1] = this.A02;
        objArr[2] = this.A03;
        objArr[3] = this.A04;
        return C12980iv.A0B(this.A01, objArr, 4);
    }
}
