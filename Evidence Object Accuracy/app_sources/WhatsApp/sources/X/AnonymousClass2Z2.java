package X;

import android.content.BroadcastReceiver;

/* renamed from: X.2Z2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2Z2 extends BroadcastReceiver {
    public static final String A03 = AnonymousClass2Z2.class.getName();
    public boolean A00;
    public boolean A01;
    public final C14160kx A02;

    public AnonymousClass2Z2(C14160kx r1) {
        this.A02 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0035, code lost:
        if (r0.isConnected() == false) goto L_0x0037;
     */
    @Override // android.content.BroadcastReceiver
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onReceive(android.content.Context r6, android.content.Intent r7) {
        /*
            r5 = this;
            X.0kx r4 = r5.A02
            X.2lF r3 = r4.A0C
            X.C14160kx.A01(r3)
            X.2lE r2 = r4.A06
            X.C14160kx.A01(r2)
            java.lang.String r1 = r7.getAction()
            X.C14160kx.A01(r3)
            java.lang.String r0 = "NetworkBroadcastReceiver received action"
            r3.A0D(r0, r1)
            java.lang.String r0 = "android.net.conn.CONNECTIVITY_CHANGE"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x005c
            android.content.Context r1 = r4.A00
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r1.getSystemService(r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch: SecurityException -> 0x0037
            if (r0 == 0) goto L_0x0037
            boolean r0 = r0.isConnected()     // Catch: SecurityException -> 0x0037
            r1 = 1
            if (r0 != 0) goto L_0x0038
        L_0x0037:
            r1 = 0
        L_0x0038:
            boolean r0 = r5.A01
            if (r0 == r1) goto L_0x009a
            r5.A01 = r1
            X.C14160kx.A01(r2)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            java.lang.String r0 = "Network connectivity status changed"
            r2.A0D(r0, r1)
            X.0kx r0 = r2.A00
            X.0ky r0 = r0.A03
            X.C13020j0.A01(r0)
            com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1 r1 = new com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1
            r1.<init>(r2)
            X.0kz r0 = r0.A03
            r0.submit(r1)
            return
        L_0x005c:
            java.lang.String r0 = "com.google.analytics.RADIO_POWERED"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00a0
            java.lang.String r0 = X.AnonymousClass2Z2.A03
            boolean r0 = r7.hasExtra(r0)
            if (r0 != 0) goto L_0x009a
            X.C14160kx.A01(r2)
            java.lang.String r0 = "Radio powered up"
            r2.A09(r0)
            r2.A0G()
            X.0kx r0 = r2.A00
            android.content.Context r3 = r0.A00
            boolean r0 = X.C15060mU.A00(r3)
            if (r0 == 0) goto L_0x009b
            boolean r0 = X.C15110ma.A00(r3)
            if (r0 == 0) goto L_0x009b
            java.lang.String r0 = "com.google.android.gms.analytics.ANALYTICS_DISPATCH"
            android.content.Intent r2 = X.C12990iw.A0E(r0)
            java.lang.String r1 = "com.google.android.gms.analytics.AnalyticsService"
            android.content.ComponentName r0 = new android.content.ComponentName
            r0.<init>(r3, r1)
            r2.setComponent(r0)
            r3.startService(r2)
        L_0x009a:
            return
        L_0x009b:
            r0 = 0
            r2.A0H(r0)
            return
        L_0x00a0:
            X.C14160kx.A01(r3)
            java.lang.String r0 = "NetworkBroadcastReceiver received unknown action"
            r3.A0E(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Z2.onReceive(android.content.Context, android.content.Intent):void");
    }
}
