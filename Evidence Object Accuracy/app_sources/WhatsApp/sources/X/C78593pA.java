package X;

import android.accounts.Account;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.IAccountAccessor;

/* renamed from: X.3pA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78593pA extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C100484lz();
    public int A00;
    public int A01;
    public Account A02;
    public Bundle A03;
    public IBinder A04;
    public String A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public C78603pB[] A09;
    public C78603pB[] A0A;
    public Scope[] A0B;
    public final int A0C;
    public final int A0D;

    public C78593pA(int i, String str) {
        this.A0C = 6;
        this.A00 = 12451000;
        this.A0D = i;
        this.A07 = true;
        this.A06 = str;
    }

    public C78593pA(Account account, Bundle bundle, IBinder iBinder, String str, String str2, C78603pB[] r13, C78603pB[] r14, Scope[] scopeArr, int i, int i2, int i3, int i4, boolean z, boolean z2) {
        Account account2;
        IInterface r2;
        Parcelable parcelable;
        this.A0C = i;
        this.A0D = i2;
        this.A00 = i3;
        if ("com.google.android.gms".equals(str)) {
            this.A05 = "com.google.android.gms";
        } else {
            this.A05 = str;
        }
        if (i < 2) {
            if (iBinder != null) {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
                if (queryLocalInterface instanceof IAccountAccessor) {
                    r2 = (IAccountAccessor) queryLocalInterface;
                } else {
                    r2 = new C79543ql(iBinder);
                }
                account2 = null;
                if (r2 != null) {
                    long clearCallingIdentity = Binder.clearCallingIdentity();
                    try {
                        try {
                            C79543ql r22 = (C79543ql) r2;
                            Parcel obtain = Parcel.obtain();
                            obtain.writeInterfaceToken(r22.A01);
                            Parcel A00 = r22.A00(2, obtain);
                            Parcelable.Creator creator = Account.CREATOR;
                            if (A00.readInt() == 0) {
                                parcelable = null;
                            } else {
                                parcelable = (Parcelable) creator.createFromParcel(A00);
                            }
                            Account account3 = (Account) parcelable;
                            A00.recycle();
                            account2 = account3;
                        } catch (RemoteException unused) {
                            Log.w("AccountAccessor", "Remote account accessor probably died");
                        }
                    } finally {
                        Binder.restoreCallingIdentity(clearCallingIdentity);
                    }
                }
            } else {
                account2 = null;
            }
            this.A02 = account2;
        } else {
            this.A04 = iBinder;
            this.A02 = account;
        }
        this.A0B = scopeArr;
        this.A03 = bundle;
        this.A09 = r13;
        this.A0A = r14;
        this.A07 = z;
        this.A01 = i4;
        this.A08 = z2;
        this.A06 = str2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        C100484lz.A00(parcel, this, i);
    }
}
