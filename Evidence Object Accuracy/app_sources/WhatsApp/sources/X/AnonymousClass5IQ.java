package X;

import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.settings.SettingsDataUsageActivity;
import java.util.TimerTask;

/* renamed from: X.5IQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IQ extends TimerTask {
    public final /* synthetic */ SettingsDataUsageActivity A00;

    public AnonymousClass5IQ(SettingsDataUsageActivity settingsDataUsageActivity) {
        this.A00 = settingsDataUsageActivity;
    }

    @Override // java.util.TimerTask, java.lang.Runnable
    public void run() {
        SettingsDataUsageActivity settingsDataUsageActivity = this.A00;
        settingsDataUsageActivity.A04.post(new RunnableBRunnable0Shape16S0100000_I1_2(settingsDataUsageActivity, 11));
    }
}
