package X;

import com.whatsapp.util.Log;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3G0  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3G0 {
    public final int A00;
    public final long A01;
    public final int[] A02;

    public AnonymousClass3G0(int[] iArr, int i, long j) {
        this.A01 = j;
        this.A00 = i;
        if (iArr == null) {
            this.A02 = new int[i];
        } else {
            this.A02 = iArr;
        }
    }

    public int A00(int i) {
        if (this instanceof C622435t) {
            return C12960it.A05(((C622435t) this).A00.get(i));
        }
        C622535u r1 = (C622535u) this;
        int i2 = r1.A00 - 1;
        long j = r1.A01;
        if (i == i2) {
            j = Math.min(j, ((AnonymousClass3G0) r1).A01 % j);
        }
        return (int) j;
    }

    public int A01(long j) {
        boolean z = true;
        int i = 0;
        AnonymousClass009.A0F(C12990iw.A1W((j > 0 ? 1 : (j == 0 ? 0 : -1))));
        if (j >= this.A01) {
            z = false;
        }
        AnonymousClass009.A0F(z);
        int i2 = 0;
        while (i < this.A00) {
            i2 += A00(i);
            if (j < ((long) i2)) {
                break;
            }
            i++;
        }
        return i;
    }

    public long A02() {
        long j = 0;
        for (int i = 0; i < this.A00; i++) {
            j += A04(i);
        }
        return j;
    }

    public long A03(int i) {
        long j = 0;
        int i2 = 0;
        while (i2 < i && i2 < this.A00) {
            j += (long) A00(i2);
            i2++;
        }
        return j;
    }

    public final synchronized long A04(int i) {
        return (long) this.A02[i];
    }

    public long A05(long j) {
        int i;
        if (j >= this.A01) {
            return -1;
        }
        int A01 = A01(j);
        while (true) {
            i = this.A00;
            if (A01 >= i || !A08(A01)) {
                break;
            }
            A01++;
        }
        if (A01 != i) {
            return A03(A01) + A04(A01);
        }
        return -1;
    }

    public List A06(long j, long j2, boolean z) {
        ArrayList A0l = C12960it.A0l();
        while (j2 > 0) {
            int A01 = A01(j);
            long A03 = j - A03(A01);
            boolean z2 = true;
            AnonymousClass009.A0F(C12990iw.A1W((A03 > 0 ? 1 : (A03 == 0 ? 0 : -1))));
            if (A03 >= ((long) A00(A01))) {
                z2 = false;
            }
            AnonymousClass009.A0F(z2);
            int A012 = A01(j);
            long A04 = A04(A012);
            if (A03 <= A04) {
                long j3 = A03 + j2;
                if (j3 < A04) {
                    break;
                }
                int A00 = A00(A012);
                long j4 = (long) A00;
                if (j3 > j4) {
                    if (z) {
                        synchronized (this) {
                            this.A02[A012] = A00;
                        }
                    }
                    long j5 = j3 - j4;
                    int i = A012 + 1;
                    if (i < this.A00) {
                        C12980iv.A1R(A0l, A012);
                        j = A03(i);
                        j2 = j5;
                    } else {
                        throw C12960it.A0U("downloaded more bytes than chunks");
                    }
                } else {
                    if (z) {
                        int i2 = (int) j3;
                        synchronized (this) {
                            this.A02[A012] = i2;
                        }
                    }
                    if (j3 == ((long) A00(A012))) {
                        C12980iv.A1R(A0l, A012);
                    }
                    j2 = 0;
                }
            } else {
                throw C12960it.A0U("gap in downloaded chunk");
            }
        }
        return A0l;
    }

    public void A07(File file) {
        DataOutputStream dataOutputStream;
        try {
            if (!(this instanceof C622435t)) {
                C622535u r3 = (C622535u) this;
                dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
                dataOutputStream.writeLong(((AnonymousClass3G0) r3).A01);
                dataOutputStream.writeLong(r3.A01);
                dataOutputStream.writeInt(r3.A00);
                for (int i = 0; i < ((AnonymousClass3G0) r3).A00; i++) {
                    dataOutputStream.writeInt(r3.A02[i]);
                }
                dataOutputStream.flush();
            } else {
                dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
                dataOutputStream.writeLong(this.A01);
                for (int i2 = 0; i2 < this.A00; i2++) {
                    dataOutputStream.writeInt(this.A02[i2]);
                }
                dataOutputStream.flush();
            }
            dataOutputStream.close();
        } catch (IOException e) {
            Log.e("ChunkStore/chunk store write failed", e);
        }
    }

    public synchronized boolean A08(int i) {
        return C12960it.A1V(this.A02[i], A00(i));
    }

    public synchronized String toString() {
        StringBuilder A0h;
        String str;
        A0h = C12960it.A0h();
        StringBuilder A0h2 = C12960it.A0h();
        A0h2.append("totalBytes: ");
        A0h2.append(this.A01);
        A0h.append(C12960it.A0d(", ", A0h2));
        StringBuilder A0h3 = C12960it.A0h();
        A0h3.append("chunkCount: ");
        int i = this.A00;
        A0h3.append(i);
        A0h.append(C12960it.A0d(", ", A0h3));
        int i2 = -1;
        for (int i3 = 0; i3 < i; i3++) {
            if (!A08(i3)) {
                if (i2 >= 0) {
                    int i4 = i3 - 1;
                    if (i4 != i2) {
                        StringBuilder A0h4 = C12960it.A0h();
                        A0h4.append("-");
                        A0h4.append(i4);
                        str = C12960it.A0d("]", A0h4);
                    } else {
                        str = "]";
                    }
                    A0h.append(str);
                    i2 = -1;
                }
                int[] iArr = this.A02;
                if (iArr[i3] > 0) {
                    StringBuilder A0h5 = C12960it.A0h();
                    A0h5.append("[");
                    A0h5.append(i3);
                    A0h5.append("] ");
                    A0h5.append(iArr[i3]);
                    A0h.append(C12960it.A0d(", ", A0h5));
                }
            } else if (i2 < 0) {
                A0h.append(C12960it.A0e("[", C12960it.A0h(), i3));
                i2 = i3;
            }
        }
        if (i2 >= 0) {
            StringBuilder A0h6 = C12960it.A0h();
            A0h6.append("[");
            A0h6.append(i2);
            A0h6.append("-");
            A0h6.append(i - 1);
            A0h.append(C12960it.A0d("]", A0h6));
        }
        return A0h.toString();
    }
}
