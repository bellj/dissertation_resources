package X;

import java.util.ArrayList;

/* renamed from: X.3Fu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64503Fu {
    public final C15550nR A00;
    public final C15610nY A01;
    public final C14830m7 A02;
    public final ArrayList A03 = C12960it.A0l();

    public C64503Fu(C15550nR r2, C15610nY r3, C14830m7 r4) {
        this.A02 = r4;
        this.A00 = r2;
        this.A01 = r3;
    }

    public int A00() {
        ArrayList arrayList = this.A03;
        if (arrayList.isEmpty()) {
            return 3;
        }
        AnonymousClass1YT A0g = C12990iw.A0g(arrayList, 0);
        if (A0g.A0F != null) {
            if (A0g.A00 == 5) {
                return 4;
            }
            return 5;
        } else if (A0g.A0B.A03) {
            return 0;
        } else {
            int i = A0g.A00;
            return (i == 5 || i == 6) ? 1 : 2;
        }
    }

    public long A01() {
        ArrayList arrayList = this.A03;
        if (arrayList.isEmpty()) {
            return 0;
        }
        return this.A02.A02(C12990iw.A0g(arrayList, 0).A09);
    }

    public C15370n3 A02() {
        ArrayList arrayList = this.A03;
        if (!arrayList.isEmpty()) {
            return this.A00.A0B(C12990iw.A0g(arrayList, 0).A0B.A01);
        }
        return null;
    }

    public String A03() {
        StringBuilder A0k;
        ArrayList arrayList = this.A03;
        if (arrayList.isEmpty()) {
            return null;
        }
        AnonymousClass1YT A0g = C12990iw.A0g(arrayList, 0);
        if (A0g.A0B()) {
            A0k = C12960it.A0k("G:");
            AnonymousClass1YU r1 = A0g.A0B;
            A0k.append(C15380n4.A03(r1.A01));
            A0k.append(r1.A03);
            A0k.append(r1.A02);
            A0k.append(r1.A00);
        } else {
            A0k = C12960it.A0k("O:");
            A0k.append(C15380n4.A03(A0g.A0B.A01));
            A0k.append(A0g.A09);
        }
        return A0k.toString();
    }

    public boolean A04() {
        ArrayList arrayList = this.A03;
        if (arrayList.isEmpty() || !C12990iw.A0g(arrayList, 0).A0B()) {
            return false;
        }
        return true;
    }

    public boolean A05() {
        ArrayList arrayList = this.A03;
        if (arrayList.isEmpty() || !C12990iw.A0g(arrayList, 0).A0H) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004f, code lost:
        if (r1 == 6) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005f, code lost:
        if (r2 == 6) goto L_0x0061;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A06(X.AnonymousClass1YT r8, boolean r9) {
        /*
            r7 = this;
            java.util.ArrayList r4 = r7.A03
            boolean r0 = r4.isEmpty()
            r6 = 1
            if (r0 != 0) goto L_0x006a
            int r0 = r4.size()
            int r0 = r0 - r6
            X.1YT r5 = X.C12990iw.A0g(r4, r0)
            if (r9 != 0) goto L_0x0022
            boolean r0 = r8.A0B()
            if (r0 != 0) goto L_0x0020
            boolean r0 = r5.A0B()
            if (r0 == 0) goto L_0x0022
        L_0x0020:
            r6 = 0
            return r6
        L_0x0022:
            long r2 = r8.A09
            long r0 = r5.A09
            boolean r0 = X.C38121nY.A0A(r2, r0)
            if (r0 == 0) goto L_0x0020
            if (r9 == 0) goto L_0x006e
            com.whatsapp.jid.GroupJid r1 = r8.A04
            com.whatsapp.jid.GroupJid r0 = r5.A04
            boolean r0 = X.C29941Vi.A00(r1, r0)
        L_0x0036:
            if (r0 == 0) goto L_0x0020
            X.1YS r1 = r8.A06
            X.1YS r0 = r5.A06
            boolean r0 = X.C29941Vi.A00(r1, r0)
            if (r0 == 0) goto L_0x0020
            X.1YU r0 = r8.A0B
            boolean r0 = r0.A03
            if (r0 != 0) goto L_0x0051
            int r1 = r8.A00
            r0 = 5
            if (r1 == r0) goto L_0x0051
            r0 = 6
            r3 = 1
            if (r1 != r0) goto L_0x0052
        L_0x0051:
            r3 = 0
        L_0x0052:
            X.1YU r0 = r5.A0B
            boolean r0 = r0.A03
            if (r0 != 0) goto L_0x0061
            int r2 = r5.A00
            r0 = 5
            if (r2 == r0) goto L_0x0061
            r1 = 6
            r0 = 1
            if (r2 != r1) goto L_0x0062
        L_0x0061:
            r0 = 0
        L_0x0062:
            if (r3 != r0) goto L_0x0020
            boolean r1 = r8.A0H
            boolean r0 = r5.A0H
            if (r1 != r0) goto L_0x0020
        L_0x006a:
            r4.add(r8)
            return r6
        L_0x006e:
            X.1YU r0 = r8.A0B
            com.whatsapp.jid.UserJid r1 = r0.A01
            X.1YU r0 = r5.A0B
            com.whatsapp.jid.UserJid r0 = r0.A01
            boolean r0 = X.C29941Vi.A00(r1, r0)
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64503Fu.A06(X.1YT, boolean):boolean");
    }

    public String toString() {
        if (this.A03.isEmpty()) {
            return null;
        }
        return this.A01.A04(A02());
    }
}
