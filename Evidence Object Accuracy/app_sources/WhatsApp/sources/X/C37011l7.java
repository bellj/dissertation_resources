package X;

import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.KeyEvent;

/* renamed from: X.1l7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37011l7 extends AbstractC37021l8 {
    public static final Editable.Factory A01 = new C73433gE();
    public AbstractC116025Tv A00;

    public C37011l7(Context context) {
        super(context);
        setEditableFactory(A01);
        setCustomSelectionActionModeCallback(new AnonymousClass3MN(this));
    }

    public C37011l7(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setEditableFactory(A01);
        setCustomSelectionActionModeCallback(new AnonymousClass3MN(this));
    }

    public C37011l7(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setEditableFactory(A01);
        setCustomSelectionActionModeCallback(new AnonymousClass3MN(this));
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        AbstractC116025Tv r0 = this.A00;
        if (r0 != null) {
            r0.ARf(i, keyEvent);
        }
        return super.onKeyPreIme(i, keyEvent);
    }

    @Override // com.whatsapp.WaEditText, X.AnonymousClass011, android.widget.TextView
    public boolean onTextContextMenuItem(int i) {
        if (Build.VERSION.SDK_INT >= 23 && i == 16908322) {
            i = 16908337;
        }
        return super.onTextContextMenuItem(i);
    }

    public void setInputEnterDone(boolean z) {
        int i = 0;
        if (z) {
            i = 6;
        }
        setInputEnterAction(i);
    }

    public void setInputEnterSend(boolean z) {
        int i = 0;
        if (z) {
            i = 4;
        }
        setInputEnterAction(i);
    }

    public void setOnKeyPreImeListener(AbstractC116025Tv r1) {
        this.A00 = r1;
    }
}
