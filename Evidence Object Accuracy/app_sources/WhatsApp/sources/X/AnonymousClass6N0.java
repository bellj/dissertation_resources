package X;

import android.view.View;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.6N0  reason: invalid class name */
/* loaded from: classes4.dex */
public interface AnonymousClass6N0 {
    void AOW(View view, View view2, AnonymousClass1ZO v, AbstractC28901Pl v2, PaymentBottomSheet paymentBottomSheet);

    void ATW(PaymentBottomSheet paymentBottomSheet, int i);

    void ATZ(AbstractC28901Pl v, PaymentMethodRow paymentMethodRow);

    void ATc(PaymentBottomSheet paymentBottomSheet, int i);

    void ATg(PaymentBottomSheet paymentBottomSheet, int i);

    void AXm(PaymentBottomSheet paymentBottomSheet);

    void AXn(String str);

    void AXo(PaymentBottomSheet paymentBottomSheet);
}
