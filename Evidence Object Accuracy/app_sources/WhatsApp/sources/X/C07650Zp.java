package X;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.foreground.SystemForegroundService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.0Zp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07650Zp implements AbstractC11960h9, AbstractC11450gJ {
    public static final String A0B = C06390Tk.A01("Processor");
    public Context A00;
    public PowerManager.WakeLock A01;
    public C05180Oo A02;
    public WorkDatabase A03;
    public AbstractC11500gO A04;
    public List A05;
    public Map A06 = new HashMap();
    public Map A07 = new HashMap();
    public Set A08;
    public final Object A09;
    public final List A0A;

    public C07650Zp(Context context, C05180Oo r3, WorkDatabase workDatabase, AbstractC11500gO r5, List list) {
        this.A00 = context;
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = workDatabase;
        this.A05 = list;
        this.A08 = new HashSet();
        this.A0A = new ArrayList();
        this.A01 = null;
        this.A09 = new Object();
    }

    public static boolean A00(RunnableC10250eH r6, String str) {
        if (r6 != null) {
            r6.A01();
            C06390Tk.A00().A02(A0B, String.format("WorkerWrapper interrupted for %s", str), new Throwable[0]);
            return true;
        }
        C06390Tk.A00().A02(A0B, String.format("WorkerWrapper could not be found for %s", str), new Throwable[0]);
        return false;
    }

    public final void A01() {
        synchronized (this.A09) {
            if (!(!this.A07.isEmpty())) {
                Context context = this.A00;
                Intent intent = new Intent(context, SystemForegroundService.class);
                intent.setAction("ACTION_STOP_FOREGROUND");
                context.startService(intent);
                PowerManager.WakeLock wakeLock = this.A01;
                if (wakeLock != null) {
                    wakeLock.release();
                    this.A01 = null;
                }
            }
        }
    }

    public void A02(AbstractC11960h9 r3) {
        synchronized (this.A09) {
            this.A0A.add(r3);
        }
    }

    public void A03(AbstractC11960h9 r3) {
        synchronized (this.A09) {
            this.A0A.remove(r3);
        }
    }

    public boolean A04(AnonymousClass0NN r15, String str) {
        synchronized (this.A09) {
            if (A05(str)) {
                C06390Tk.A00().A02(A0B, String.format("Work %s is already enqueued for processing", str), new Throwable[0]);
                return false;
            }
            Context context = this.A00;
            C05180Oo r9 = this.A02;
            AbstractC11500gO r12 = this.A04;
            AnonymousClass0O4 r7 = new AnonymousClass0O4(context, r9, this.A03, this, r12, str);
            r7.A07 = this.A05;
            if (r15 != null) {
                r7.A02 = r15;
            }
            RunnableC10250eH r6 = new RunnableC10250eH(r7);
            AbstractFutureC44231yX A00 = r6.A00();
            C07760a2 r122 = (C07760a2) r12;
            A00.A5i(new RunnableC09920dj(this, A00, str), r122.A02);
            this.A06.put(str, r6);
            r122.A01.execute(r6);
            C06390Tk.A00().A02(A0B, String.format("%s: processing %s", getClass().getSimpleName(), str), new Throwable[0]);
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r3.A07.containsKey(r4) != false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A05(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.Object r2 = r3.A09
            monitor-enter(r2)
            java.util.Map r0 = r3.A06     // Catch: all -> 0x0017
            boolean r0 = r0.containsKey(r4)     // Catch: all -> 0x0017
            if (r0 != 0) goto L_0x0014
            java.util.Map r0 = r3.A07     // Catch: all -> 0x0017
            boolean r1 = r0.containsKey(r4)     // Catch: all -> 0x0017
            r0 = 0
            if (r1 == 0) goto L_0x0015
        L_0x0014:
            r0 = 1
        L_0x0015:
            monitor-exit(r2)     // Catch: all -> 0x0017
            return r0
        L_0x0017:
            r0 = move-exception
            monitor-exit(r2)     // Catch: all -> 0x0017
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07650Zp.A05(java.lang.String):boolean");
    }

    @Override // X.AbstractC11960h9
    public void AQ1(String str, boolean z) {
        synchronized (this.A09) {
            this.A06.remove(str);
            C06390Tk.A00().A02(A0B, String.format("%s %s executed; reschedule = %s", getClass().getSimpleName(), str, Boolean.valueOf(z)), new Throwable[0]);
            for (AbstractC11960h9 r0 : this.A0A) {
                r0.AQ1(str, z);
            }
        }
    }
}
