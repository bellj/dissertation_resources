package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.5aG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117605aG extends ArrayAdapter {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass1J1 A01;
    public final /* synthetic */ C133766Ca A02;
    public final /* synthetic */ List A03;

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public boolean hasStableIds() {
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C117605aG(Context context, Context context2, AnonymousClass1J1 r4, C133766Ca r5, List list, List list2) {
        super(context, (int) R.layout.selected_contact, list);
        this.A02 = r5;
        this.A03 = list2;
        this.A00 = context2;
        this.A01 = r4;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        return this.A03.size();
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A03.get(i);
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        C15370n3 r2 = (C15370n3) this.A03.get(i);
        AnonymousClass009.A05(r2);
        if (view == null) {
            view = C12960it.A0F(LayoutInflater.from(this.A00), viewGroup, R.layout.payment_invite_selected_contact);
        }
        C12960it.A0I(view, R.id.contact_name).setText(this.A02.A0C.A04(r2));
        ImageView A0K = C12970iu.A0K(view, R.id.contact_row_photo);
        this.A01.A06(A0K, r2);
        AnonymousClass028.A0a(A0K, 2);
        AnonymousClass23N.A03(view, R.string.payments_multi_invite_contact_content_description);
        return view;
    }
}
