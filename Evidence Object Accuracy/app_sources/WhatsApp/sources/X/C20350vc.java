package X;

import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape0S0800000_I0;
import com.whatsapp.jid.UserJid;
import java.io.File;

/* renamed from: X.0vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20350vc {
    public final C14330lG A00;
    public final C18640sm A01;
    public final C14830m7 A02;
    public final C20340vb A03;
    public final C14850m9 A04;
    public final C14410lO A05;
    public final C14300lD A06;
    public final C20330va A07;
    public final AbstractC14440lR A08;

    public C20350vc(C14330lG r1, C18640sm r2, C14830m7 r3, C20340vb r4, C14850m9 r5, C14410lO r6, C14300lD r7, C20330va r8, AbstractC14440lR r9) {
        this.A02 = r3;
        this.A04 = r5;
        this.A08 = r9;
        this.A00 = r1;
        this.A05 = r6;
        this.A06 = r7;
        this.A07 = r8;
        this.A03 = r4;
        this.A01 = r2;
    }

    public static AnonymousClass1K9 A00(AnonymousClass1KS r21, String str) {
        File file;
        Uri uri;
        String str2 = r21.A08;
        if (str2 != null) {
            if (r21.A01 == 3) {
                uri = Uri.parse(str2);
                file = null;
            } else {
                file = new File(str2);
                uri = null;
            }
            if (uri != null) {
                return AnonymousClass1K9.A00(uri, null, null, new C14480lV(true, false, true), C14370lK.A0S, r21.A04, str, 0, false, true, true, false);
            }
            if (file != null) {
                C14480lV r0 = new C14480lV(true, false, true);
                C14370lK r5 = C14370lK.A0S;
                return new AnonymousClass1K9(new C14380lL(null, null, r5, null, null, null, "optimistic", null, 0, 0, 0, true, true, false, false), new AnonymousClass1qQ(null, r5, r21.A04, file, null, file.getName(), r21.A0C, r21.A0B, 0, 0, 0, 0, false, true, true, false, false), r0, null);
            }
        }
        return null;
    }

    public C14580lf A01(C30921Zi r12, AbstractC14640lm r13, UserJid userJid, AbstractC15340mz r15, AnonymousClass1KS r16, Integer num) {
        C14580lf r7 = new C14580lf();
        if (!this.A01.A0B()) {
            r7.A02(new AnonymousClass22U());
            return r7;
        }
        this.A08.Ab2(new RunnableBRunnable0Shape0S0800000_I0(r16, num, r12, this, r15, r7, userJid, r13, 2));
        return r7;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C30061Vy A02(X.AbstractC14640lm r18, com.whatsapp.jid.UserJid r19, X.AbstractC15340mz r20, X.AnonymousClass1KS r21, java.lang.Integer r22) {
        /*
            r17 = this;
            X.0oX r5 = new X.0oX
            r5.<init>()
            r2 = r21
            java.lang.String r3 = r2.A08
            if (r3 == 0) goto L_0x005e
            int r1 = r2.A01
            r0 = 3
            if (r1 != r0) goto L_0x0057
            android.net.Uri r4 = android.net.Uri.parse(r3)
        L_0x0014:
            int r0 = r2.A03
            r5.A08 = r0
            int r0 = r2.A02
            r5.A06 = r0
            r0 = r17
            X.0lO r3 = r0.A05
            r14 = 0
            r13 = 20
            r6 = 0
            r10 = r6
            r11 = r6
            r12 = r6
            r15 = 0
            r16 = 0
            r7 = r18
            r8 = r20
            r9 = r6
            X.0oV r1 = r3.A03(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
            X.1Vy r1 = (X.C30061Vy) r1
            boolean r0 = X.C15380n4.A0J(r7)
            if (r0 == 0) goto L_0x0040
            r0 = r19
            r1.A0e(r0)
        L_0x0040:
            java.lang.String r0 = r2.A0C
            r1.A05 = r0
            java.lang.String r0 = r2.A0B
            r1.A06 = r0
            if (r0 != 0) goto L_0x004e
            java.lang.String r0 = "image/webp"
            r1.A06 = r0
        L_0x004e:
            X.1KB r0 = r2.A04
            r1.A01 = r0
            r0 = r22
            r1.A02 = r0
            return r1
        L_0x0057:
            java.io.File r0 = new java.io.File
            r0.<init>(r3)
            r5.A0F = r0
        L_0x005e:
            r4 = 0
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20350vc.A02(X.0lm, com.whatsapp.jid.UserJid, X.0mz, X.1KS, java.lang.Integer):X.1Vy");
    }

    public void A03(C30921Zi r27, C22530zE r28) {
        this.A07.A06(new AnonymousClass22T(r27, r28, this), C14370lK.A0P, null, null, r27.A0F, r27.A04, r27.A03, null, r27.A02, r27.A0G, r27.A08, 3, 1, 1, 0, r27.A0E);
    }
}
