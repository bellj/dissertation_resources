package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.util.Log;
import dalvik.system.BaseDexClassLoader;
import dalvik.system.DexFile;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

/* renamed from: X.00G  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass00G {
    public static final Set A00 = new HashSet();
    public static final boolean A01;

    static {
        String str;
        String str2;
        String property = System.getProperty("java.vm.version");
        boolean z = false;
        if (property != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(property, ".");
            String str3 = null;
            if (stringTokenizer.hasMoreTokens()) {
                str2 = stringTokenizer.nextToken();
            } else {
                str2 = null;
            }
            if (stringTokenizer.hasMoreTokens()) {
                str3 = stringTokenizer.nextToken();
            }
            if (!(str2 == null || str3 == null)) {
                try {
                    int parseInt = Integer.parseInt(str2);
                    int parseInt2 = Integer.parseInt(str3);
                    if (parseInt > 2 || (parseInt == 2 && parseInt2 >= 1)) {
                        z = true;
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        StringBuilder sb = new StringBuilder("VM with version ");
        sb.append(property);
        if (z) {
            str = " has multidex support";
        } else {
            str = " does not have multidex support";
        }
        sb.append(str);
        Log.i("MultiDex", sb.toString());
        A01 = z;
    }

    public static Field A00(Object obj, String str) {
        Class<?> cls = obj.getClass();
        while (cls != null) {
            try {
                Field declaredField = cls.getDeclaredField(str);
                if (declaredField.isAccessible()) {
                    return declaredField;
                }
                declaredField.setAccessible(true);
                return declaredField;
            } catch (NoSuchFieldException unused) {
                cls = cls.getSuperclass();
            }
        }
        StringBuilder sb = new StringBuilder("Field ");
        sb.append(str);
        sb.append(" not found in ");
        sb.append(cls);
        throw new NoSuchFieldException(sb.toString());
    }

    public static void A01(Context context) {
        String str;
        StringBuilder sb;
        Log.i("MultiDex", "Installing application");
        if (A01) {
            str = "VM has multidex support, MultiDex support library is disabled.";
        } else {
            try {
                try {
                    ApplicationInfo applicationInfo = context.getApplicationInfo();
                    if (applicationInfo != null) {
                        File file = new File(applicationInfo.sourceDir);
                        File file2 = new File(applicationInfo.dataDir);
                        Set set = A00;
                        synchronized (set) {
                            if (!set.contains(file)) {
                                set.add(file);
                                int i = Build.VERSION.SDK_INT;
                                if (i > 20) {
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("MultiDex is not guaranteed to work in SDK version ");
                                    sb2.append(i);
                                    sb2.append(": SDK version higher than ");
                                    sb2.append(20);
                                    sb2.append(" should be backed by ");
                                    sb2.append("runtime with built-in multidex capabilty but it's not the ");
                                    sb2.append("case here: java.vm.version=\"");
                                    sb2.append(System.getProperty("java.vm.version"));
                                    sb2.append("\"");
                                    Log.w("MultiDex", sb2.toString());
                                }
                                try {
                                    ClassLoader classLoader = context.getClassLoader();
                                    if (!(classLoader instanceof BaseDexClassLoader)) {
                                        Log.e("MultiDex", "Context class loader is null or not dex-capable. Must be running in test mode. Skip patching.");
                                    } else if (classLoader != null) {
                                        File file3 = new File(context.getFilesDir(), "secondary-dexes");
                                        if (file3.isDirectory()) {
                                            StringBuilder sb3 = new StringBuilder("Clearing old secondary dex dir (");
                                            sb3.append(file3.getPath());
                                            sb3.append(").");
                                            Log.i("MultiDex", sb3.toString());
                                            File[] listFiles = file3.listFiles();
                                            if (listFiles == null) {
                                                sb = new StringBuilder("Failed to list secondary dex dir content (");
                                                sb.append(file3.getPath());
                                                sb.append(").");
                                            } else {
                                                for (File file4 : listFiles) {
                                                    StringBuilder sb4 = new StringBuilder("Trying to delete old file ");
                                                    sb4.append(file4.getPath());
                                                    sb4.append(" of size ");
                                                    sb4.append(file4.length());
                                                    Log.i("MultiDex", sb4.toString());
                                                    if (!file4.delete()) {
                                                        StringBuilder sb5 = new StringBuilder("Failed to delete old file ");
                                                        sb5.append(file4.getPath());
                                                        Log.w("MultiDex", sb5.toString());
                                                    } else {
                                                        StringBuilder sb6 = new StringBuilder("Deleted old file ");
                                                        sb6.append(file4.getPath());
                                                        Log.i("MultiDex", sb6.toString());
                                                    }
                                                }
                                                if (!file3.delete()) {
                                                    sb = new StringBuilder("Failed to delete secondary dex dir ");
                                                    sb.append(file3.getPath());
                                                } else {
                                                    StringBuilder sb7 = new StringBuilder("Deleted old secondary dex dir ");
                                                    sb7.append(file3.getPath());
                                                    Log.i("MultiDex", sb7.toString());
                                                }
                                            }
                                            Log.w("MultiDex", sb.toString());
                                        }
                                        File file5 = new File(file2, "code_cache");
                                        try {
                                            A02(file5);
                                        } catch (IOException unused) {
                                            file5 = new File(context.getFilesDir(), "code_cache");
                                            A02(file5);
                                        }
                                        File file6 = new File(file5, "secondary-dexes");
                                        A02(file6);
                                        AnonymousClass00I r2 = new AnonymousClass00I(file, file6);
                                        try {
                                            A03(file6, classLoader, r2.A03(context, false));
                                        } catch (IOException e) {
                                            Log.w("MultiDex", "Failed to install extracted secondary dex files, retrying with forced extraction", e);
                                            A03(file6, classLoader, r2.A03(context, true));
                                        }
                                        try {
                                            r2.close();
                                        } catch (IOException e2) {
                                            throw e2;
                                        }
                                    }
                                } catch (RuntimeException e3) {
                                    Log.w("MultiDex", "Failure while trying to obtain Context class loader. Must be running in test mode. Skip patching.", e3);
                                }
                            }
                        }
                        str = "install done";
                    }
                } catch (RuntimeException e4) {
                    Log.w("MultiDex", "Failure while trying to obtain ApplicationInfo from Context. Must be running in test mode. Skip patching.", e4);
                }
                Log.i("MultiDex", "No ApplicationInfo available, i.e. running on a test Context: MultiDex support library is disabled.");
                return;
            } catch (Exception e5) {
                Log.e("MultiDex", "MultiDex installation failure", e5);
                StringBuilder sb8 = new StringBuilder("MultiDex installation failed (");
                sb8.append(e5.getMessage());
                sb8.append(").");
                throw new RuntimeException(sb8.toString());
            }
        }
        Log.i("MultiDex", str);
    }

    public static void A02(File file) {
        file.mkdir();
        if (!file.isDirectory()) {
            File parentFile = file.getParentFile();
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to create dir ");
            String path = file.getPath();
            if (parentFile == null) {
                sb.append(path);
                sb.append(". Parent file is null.");
            } else {
                sb.append(path);
                sb.append(". parent file is a dir ");
                sb.append(parentFile.isDirectory());
                sb.append(", a file ");
                sb.append(parentFile.isFile());
                sb.append(", exists ");
                sb.append(parentFile.exists());
                sb.append(", readable ");
                sb.append(parentFile.canRead());
                sb.append(", writable ");
                sb.append(parentFile.canWrite());
            }
            Log.e("MultiDex", sb.toString());
            StringBuilder sb2 = new StringBuilder("Failed to create directory ");
            sb2.append(file.getPath());
            throw new IOException(sb2.toString());
        }
    }

    public static void A03(File file, ClassLoader classLoader, List list) {
        Object obj;
        if (!list.isEmpty()) {
            int i = Build.VERSION.SDK_INT;
            Field A002 = A00(classLoader, "pathList");
            if (i >= 19) {
                Object obj2 = A002.get(classLoader);
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList(list);
                Class<?>[] clsArr = {ArrayList.class, File.class, ArrayList.class};
                Class<?> cls = obj2.getClass();
                while (cls != null) {
                    try {
                        Method declaredMethod = cls.getDeclaredMethod("makeDexElements", clsArr);
                        if (!declaredMethod.isAccessible()) {
                            declaredMethod.setAccessible(true);
                        }
                        A04(obj2, "dexElements", (Object[]) declaredMethod.invoke(obj2, arrayList2, file, arrayList));
                        if (arrayList.size() > 0) {
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                Log.w("MultiDex", "Exception in makeDexElement", (Throwable) it.next());
                            }
                            Field A003 = A00(obj2, "dexElementsSuppressedExceptions");
                            IOException[] iOExceptionArr = (IOException[]) A003.get(obj2);
                            int size = arrayList.size();
                            if (iOExceptionArr == null) {
                                obj = arrayList.toArray(new IOException[size]);
                            } else {
                                int length = iOExceptionArr.length;
                                obj = new IOException[size + length];
                                arrayList.toArray(obj);
                                System.arraycopy(iOExceptionArr, 0, obj, arrayList.size(), length);
                            }
                            A003.set(obj2, obj);
                            IOException iOException = new IOException("I/O exception during makeDexElement");
                            iOException.initCause((Throwable) arrayList.get(0));
                            throw iOException;
                        }
                        return;
                    } catch (NoSuchMethodException unused) {
                        cls = cls.getSuperclass();
                    }
                }
                StringBuilder sb = new StringBuilder("Method ");
                sb.append("makeDexElements");
                sb.append(" with parameters ");
                sb.append(Arrays.asList(clsArr));
                sb.append(" not found in ");
                sb.append(cls);
                throw new NoSuchMethodException(sb.toString());
            }
            Object obj3 = A002.get(classLoader);
            AnonymousClass0MV r8 = new AnonymousClass0MV();
            int size2 = list.size();
            Object[] objArr = new Object[size2];
            for (int i2 = 0; i2 < size2; i2++) {
                File file2 = (File) list.get(i2);
                AbstractC11800gt r2 = r8.A00;
                String path = file2.getPath();
                File parentFile = file2.getParentFile();
                String name = file2.getName();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(name.substring(0, name.length() - 4));
                sb2.append(".dex");
                objArr[i2] = r2.ALe(DexFile.loadDex(path, new File(parentFile, sb2.toString()).getPath(), 0), file2);
            }
            try {
                A04(obj3, "dexElements", objArr);
            } catch (NoSuchFieldException e) {
                Log.w("MultiDex", "Failed find field 'dexElements' attempting 'pathElements'", e);
                A04(obj3, "pathElements", objArr);
            }
        }
    }

    public static void A04(Object obj, String str, Object[] objArr) {
        Field A002 = A00(obj, str);
        Object[] objArr2 = (Object[]) A002.get(obj);
        Class<?> componentType = objArr2.getClass().getComponentType();
        int length = objArr2.length;
        int length2 = objArr.length;
        Object newInstance = Array.newInstance(componentType, length + length2);
        System.arraycopy(objArr2, 0, newInstance, 0, length);
        System.arraycopy(objArr, 0, newInstance, length, length2);
        A002.set(obj, newInstance);
    }
}
