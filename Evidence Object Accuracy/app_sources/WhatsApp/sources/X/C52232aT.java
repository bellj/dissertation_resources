package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.status.posting.FirstStatusConfirmationDialogFragment;

/* renamed from: X.2aT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52232aT extends ClickableSpan {
    public final /* synthetic */ FirstStatusConfirmationDialogFragment A00;

    public C52232aT(FirstStatusConfirmationDialogFragment firstStatusConfirmationDialogFragment) {
        this.A00 = firstStatusConfirmationDialogFragment;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        Context context = view.getContext();
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(context.getPackageName(), "com.whatsapp.status.StatusPrivacyActivity");
        this.A00.startActivityForResult(A0A, 0);
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        C12980iv.A12(this.A00.A0p(), textPaint, R.color.accent_light);
    }
}
