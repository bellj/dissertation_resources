package X;

import android.graphics.BitmapFactory;

/* renamed from: X.4Va  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92224Va {
    public int A00 = 1;
    public BitmapFactory.Options A01;
    public boolean A02;

    public String toString() {
        String str;
        int i = this.A00;
        if (i == 0) {
            str = "Cancel";
        } else {
            str = i == 1 ? "Allow" : "?";
        }
        StringBuilder A0k = C12960it.A0k("thread state = ");
        A0k.append(str);
        A0k.append(", options = ");
        return C12970iu.A0s(this.A01, A0k);
    }
}
