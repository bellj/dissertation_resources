package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.WaViewPager;

/* renamed from: X.2iG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55352iG extends AnonymousClass01A {
    public final AnonymousClass01A A00;
    public final AnonymousClass018 A01;

    public C55352iG(AnonymousClass01A r2, AnonymousClass018 r3) {
        this.A01 = r3;
        this.A00 = r2;
        r2.A07(new C72983fU(this));
    }

    @Override // X.AnonymousClass01A
    public float A00(int i) {
        AnonymousClass01A r2 = this.A00;
        return r2.A00(WaViewPager.A00(this.A01, i, r2.A01()));
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A00.A01();
    }

    @Override // X.AnonymousClass01A
    public int A02(Object obj) {
        AnonymousClass01A r3 = this.A00;
        int A02 = r3.A02(obj);
        return (A02 == -2 || A02 == -1) ? A02 : WaViewPager.A00(this.A01, A02, r3.A01());
    }

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        AnonymousClass01A r2 = this.A00;
        return r2.A04(WaViewPager.A00(this.A01, i, r2.A01()));
    }

    @Override // X.AnonymousClass01A
    public Object A05(ViewGroup viewGroup, int i) {
        AnonymousClass01A r2 = this.A00;
        return r2.A05(viewGroup, WaViewPager.A00(this.A01, i, r2.A01()));
    }

    @Override // X.AnonymousClass01A
    public void A0C(ViewGroup viewGroup, Object obj, int i) {
        AnonymousClass01A r2 = this.A00;
        r2.A0C(viewGroup, obj, WaViewPager.A00(this.A01, i, r2.A01()));
    }

    @Override // X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        AnonymousClass01A r2 = this.A00;
        int A01 = r2.A01();
        if (i == A01) {
            A01++;
        }
        r2.A0D(viewGroup, obj, WaViewPager.A00(this.A01, i, A01));
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return this.A00.A0E(view, obj);
    }
}
