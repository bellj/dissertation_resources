package X;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: X.3ws  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83213ws extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AnonymousClass2xV A00;

    public C83213ws(AnonymousClass2xV r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        View view = this.A00.A00;
        AnonymousClass009.A03(view);
        view.setVisibility(0);
    }
}
