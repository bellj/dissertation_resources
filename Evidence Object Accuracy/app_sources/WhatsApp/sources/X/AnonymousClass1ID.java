package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1ID  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ID extends AnonymousClass1IA {
    public final /* synthetic */ AnonymousClass168 A00;
    public final /* synthetic */ boolean A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1ID(AnonymousClass168 r11, String str, BlockingQueue blockingQueue, ThreadFactory threadFactory, TimeUnit timeUnit, int i, int i2, long j, boolean z) {
        super(str, blockingQueue, threadFactory, timeUnit, i, i2, j);
        this.A00 = r11;
        this.A01 = z;
    }

    @Override // java.util.concurrent.ThreadPoolExecutor
    public void afterExecute(Runnable runnable, Throwable th) {
        if (this.A01) {
            AnonymousClass168.A05.A01(this, runnable);
        }
    }

    @Override // java.util.concurrent.ThreadPoolExecutor
    public void beforeExecute(Thread thread, Runnable runnable) {
        if (this.A01) {
            AnonymousClass168.A05.A03(this, runnable, thread);
        }
    }

    @Override // java.util.concurrent.ThreadPoolExecutor, java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        if (this.A01) {
            AnonymousClass168.A05.A02(this, runnable);
        }
        super.execute(runnable);
    }
}
