package X;

import com.whatsapp.util.Log;

/* renamed from: X.1E6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1E6 {
    public final C15600nX A00;
    public final C14850m9 A01;
    public final C22140ya A02;

    public AnonymousClass1E6(C15600nX r1, C14850m9 r2, C22140ya r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public AnonymousClass1XB A00(C15580nU r10, AnonymousClass1V0 r11, AnonymousClass1V0 r12, long j) {
        C22140ya r2;
        int i;
        if (r12 != null && this.A01.A07(1204) && this.A00.A0D(r10)) {
            if (r11 == null) {
                r11 = new AnonymousClass1V0(0, 0);
            }
            int i2 = r11.A00;
            if (i2 == 0) {
                if (r12.A00 == 1) {
                    r2 = this.A02;
                    StringBuilder sb = new StringBuilder("SystemMessageFactory/newInviteViaLinkUnavailableSystemMessage/gjid=");
                    sb.append(r10);
                    Log.i(sb.toString());
                    i = 73;
                    return r2.A07(null, r10, null, i, j);
                }
            } else if (i2 == 1 && r12.A00 == 0) {
                r2 = this.A02;
                StringBuilder sb2 = new StringBuilder("SystemMessageFactory/newInviteViaLinkAvailableAgainSystemMessage/gjid=");
                sb2.append(r10);
                Log.i(sb2.toString());
                i = 74;
                return r2.A07(null, r10, null, i, j);
            }
        }
        return null;
    }
}
