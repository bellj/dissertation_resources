package X;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.URLSpan;
import android.widget.TextView;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingDeque;

/* renamed from: X.3DI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DI {
    public AnonymousClass398 A00;
    public final C006202y A01 = new C006202y(32);
    public final C14900mE A02;
    public final AnonymousClass4KM A03 = new AnonymousClass4KM();
    public final C14820m6 A04;
    public final C22710zW A05;
    public final C17070qD A06;

    public AnonymousClass3DI(C14900mE r3, C14820m6 r4, C22710zW r5, C17070qD r6) {
        this.A02 = r3;
        this.A06 = r6;
        this.A04 = r4;
        this.A05 = r5;
    }

    public void A00(TextView textView, AbstractC116035Tw r12, CharSequence charSequence, Object obj) {
        textView.setTag(obj);
        Spannable spannable = (Spannable) this.A01.A04(charSequence.toString());
        if (spannable != null) {
            URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, spannable.length(), URLSpan.class);
            SpannableString valueOf = SpannableString.valueOf(charSequence);
            if (uRLSpanArr != null) {
                for (URLSpan uRLSpan : uRLSpanArr) {
                    valueOf.setSpan(new URLSpan(uRLSpan.getURL()), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), spannable.getSpanFlags(uRLSpan));
                }
            }
            r12.AaT(valueOf);
            return;
        }
        AnonymousClass4KM r9 = this.A03;
        LinkedBlockingDeque linkedBlockingDeque = r9.A00;
        Iterator it = linkedBlockingDeque.iterator();
        while (it.hasNext()) {
            C91264Rb r1 = (C91264Rb) it.next();
            if (r1.A00 == textView) {
                linkedBlockingDeque.remove(r1);
            }
        }
        linkedBlockingDeque.add(new C91264Rb(textView, r12, charSequence, obj));
        if (this.A00 == null) {
            AnonymousClass398 r8 = new AnonymousClass398(r9, this, this.A05, this.A06, this.A04.A0B());
            this.A00 = r8;
            r8.start();
        }
    }
}
