package X;

import com.whatsapp.biz.BusinessProfileExtraFieldsActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2Cu  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2Cu {
    public void A00(UserJid userJid) {
    }

    public void A01(UserJid userJid) {
        if (this instanceof C58982tl) {
            AbstractActivityC37081lH r2 = ((C58982tl) this).A00;
            if (r2.A0J.equals(userJid) && !((ActivityC13790kL) r2).A01.A0F(r2.A0J)) {
                r2.A0E.A0M();
            }
        } else if (this instanceof C58972tk) {
            C58972tk r0 = (C58972tk) this;
            if (userJid != null) {
                BusinessProfileExtraFieldsActivity businessProfileExtraFieldsActivity = r0.A00;
                if (userJid.equals(businessProfileExtraFieldsActivity.A0C)) {
                    businessProfileExtraFieldsActivity.A01.A03(new AnonymousClass53L(businessProfileExtraFieldsActivity), businessProfileExtraFieldsActivity.A0C);
                }
            }
        }
    }
}
