package X;

/* renamed from: X.5FS  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FS implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FS(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r7) {
        float[] fArr = (float[]) obj;
        appendable.append('[');
        boolean z = false;
        for (float f : fArr) {
            z = C72453ed.A1U(appendable, z);
            appendable.append(Float.toString(f));
        }
        appendable.append(']');
    }
}
