package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import java.util.Map;

/* renamed from: X.3FE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FE {
    public boolean A00 = true;
    public final C14230l4 A01;
    public final AbstractC14200l1 A02;
    public final AnonymousClass3CX A03;

    public AnonymousClass3FE(C14230l4 r2, AbstractC14200l1 r3, AnonymousClass3CX r4) {
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    public void A00(String str) {
        A01(str, C12970iu.A11());
    }

    @Deprecated
    public void A01(String str, Map map) {
        if (this.A00) {
            this.A03.A00(new RunnableBRunnable0Shape1S1200000_I1(map, this, str, 9));
        }
    }

    public void A02(String str, Map map) {
        if (this.A00) {
            this.A03.A00(new RunnableBRunnable0Shape1S1200000_I1(map, this, str, 11));
        }
    }
}
