package X;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.LockSupport;

/* renamed from: X.5HE  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5HE extends Thread {
    public static final /* synthetic */ AtomicIntegerFieldUpdater A07 = AtomicIntegerFieldUpdater.newUpdater(AnonymousClass5HE.class, "workerCtl");
    public int A00 = AbstractC93854ar.A00.A00();
    public long A01;
    public long A02;
    public EnumC87094Ae A03 = EnumC87094Ae.A03;
    public boolean A04;
    public final C94384bj A05 = new C94384bj();
    public final /* synthetic */ AnonymousClass5E8 A06;
    public volatile int indexInArray;
    public volatile Object nextParkedWorker = AnonymousClass5E8.A07;
    public volatile /* synthetic */ int workerCtl = 0;

    public AnonymousClass5HE(AnonymousClass5E8 r2, int i) {
        this.A06 = r2;
        this.A06 = r2;
        setDaemon(true);
        A03(i);
    }

    public final AbstractRunnableC75993ks A00() {
        AnonymousClass5LG r0;
        int i = this.A00;
        int i2 = i ^ (i << 13);
        int i3 = i2 ^ (i2 >> 17);
        int i4 = i3 ^ (i3 << 5);
        this.A00 = i4;
        int i5 = i4 & 1;
        AnonymousClass5E8 r1 = this.A06;
        if (i5 == 0) {
            AbstractRunnableC75993ks r02 = (AbstractRunnableC75993ks) r1.A06.A01();
            if (r02 != null) {
                return r02;
            }
            r0 = r1.A05;
        } else {
            AbstractRunnableC75993ks r03 = (AbstractRunnableC75993ks) r1.A05.A01();
            if (r03 != null) {
                return r03;
            }
            r0 = r1.A06;
        }
        return (AbstractRunnableC75993ks) r0.A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0086, code lost:
        if (r3 == false) goto L_0x0088;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AbstractRunnableC75993ks A01(boolean r11) {
        /*
            r10 = this;
            X.4Ae r0 = r10.A03
            X.4Ae r3 = X.EnumC87094Ae.A02
            if (r0 == r3) goto L_0x004c
            X.5E8 r5 = r10.A06
        L_0x0008:
            long r6 = r5.controlState
            r1 = 9223367638808264704(0x7ffffc0000000000, double:NaN)
            long r1 = r1 & r6
            r0 = 42
            long r1 = r1 >> r0
            int r0 = (int) r1
            if (r0 != 0) goto L_0x003b
            if (r11 == 0) goto L_0x002b
            X.4bj r2 = r10.A05
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = X.C94384bj.A04
            r0 = 0
            java.lang.Object r0 = r1.getAndSet(r2, r0)
            X.3ks r0 = (X.AbstractRunnableC75993ks) r0
            if (r0 != 0) goto L_0x003a
            X.3ks r0 = r2.A01()
            if (r0 != 0) goto L_0x003a
        L_0x002b:
            X.5LG r0 = r5.A05
            java.lang.Object r0 = r0.A01()
            X.3ks r0 = (X.AbstractRunnableC75993ks) r0
            if (r0 != 0) goto L_0x003a
            r0 = 1
            X.3ks r0 = r10.A02(r0)
        L_0x003a:
            return r0
        L_0x003b:
            r0 = 4398046511104(0x40000000000, double:2.1729236899484E-311)
            long r8 = r6 - r0
            java.util.concurrent.atomic.AtomicLongFieldUpdater r4 = X.AnonymousClass5E8.A09
            boolean r0 = r4.compareAndSet(r5, r6, r8)
            if (r0 == 0) goto L_0x0008
            r10.A03 = r3
        L_0x004c:
            r4 = 0
            if (r11 == 0) goto L_0x0088
            X.5E8 r0 = r10.A06
            int r0 = r0.A00
            int r3 = r0 << 1
            int r2 = r10.A00
            int r0 = r2 << 13
            r2 = r2 ^ r0
            int r0 = r2 >> 17
            r2 = r2 ^ r0
            int r0 = r2 << 5
            r2 = r2 ^ r0
            r10.A00 = r2
            int r1 = r3 + -1
            r0 = r1 & r3
            if (r0 != 0) goto L_0x0093
            r2 = r2 & r1
        L_0x0069:
            r3 = 0
            if (r2 != 0) goto L_0x0073
            r3 = 1
            X.3ks r0 = r10.A00()
            if (r0 != 0) goto L_0x003a
        L_0x0073:
            X.4bj r2 = r10.A05
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = X.C94384bj.A04
            r0 = 0
            java.lang.Object r0 = r1.getAndSet(r2, r0)
            X.3ks r0 = (X.AbstractRunnableC75993ks) r0
            if (r0 != 0) goto L_0x003a
            X.3ks r0 = r2.A01()
            if (r0 != 0) goto L_0x003a
            if (r3 != 0) goto L_0x008e
        L_0x0088:
            X.3ks r0 = r10.A00()
            if (r0 != 0) goto L_0x003a
        L_0x008e:
            X.3ks r0 = r10.A02(r4)
            return r0
        L_0x0093:
            r0 = 2147483647(0x7fffffff, float:NaN)
            r2 = r2 & r0
            int r2 = r2 % r3
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5HE.A01(boolean):X.3ks");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0079 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AbstractRunnableC75993ks A02(boolean r22) {
        /*
            r21 = this;
            r12 = 1
            r11 = r21
            X.5E8 r10 = r11.A06
            long r0 = r10.controlState
            r2 = 2097151(0x1fffff, double:1.0361303E-317)
            long r0 = r0 & r2
            int r9 = (int) r0
            r0 = 2
            r8 = 0
            if (r9 < r0) goto L_0x00b9
            int r7 = r11.A00
            int r0 = r7 << 13
            r7 = r7 ^ r0
            int r0 = r7 >> 17
            r7 = r7 ^ r0
            int r0 = r7 << 5
            r7 = r7 ^ r0
            r11.A00 = r7
            int r1 = r9 + -1
            r0 = r1 & r9
            if (r0 != 0) goto L_0x00aa
            r7 = r7 & r1
        L_0x0024:
            r18 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6 = 0
        L_0x002f:
            r16 = 0
            if (r6 >= r9) goto L_0x00b1
            int r6 = r6 + 1
            int r7 = r7 + r12
            if (r7 <= r9) goto L_0x0039
            r7 = 1
        L_0x0039:
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r10.A04
            java.lang.Object r0 = r0.get(r7)
            X.5HE r0 = (X.AnonymousClass5HE) r0
            if (r0 == 0) goto L_0x002f
            if (r0 == r11) goto L_0x002f
            X.4bj r5 = r11.A05
            X.4bj r14 = r0.A05
            if (r22 == 0) goto L_0x0090
            int r13 = r14.consumerIndex
            int r0 = r14.producerIndex
            r20 = r0
            java.util.concurrent.atomic.AtomicReferenceArray r4 = r14.A00
        L_0x0053:
            r0 = r20
            if (r13 == r0) goto L_0x008b
            r1 = r13 & 127(0x7f, float:1.78E-43)
            int r0 = r14.blockingTasksInBuffer
            if (r0 == 0) goto L_0x008b
            java.lang.Object r0 = r4.get(r1)
            X.3ks r0 = (X.AbstractRunnableC75993ks) r0
            if (r0 == 0) goto L_0x0088
            X.4La r15 = r0.A01
            int r15 = r15.A00
            if (r15 != r12) goto L_0x0088
            boolean r1 = r4.compareAndSet(r1, r0, r8)
            if (r1 == 0) goto L_0x0088
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r1 = X.C94384bj.A01
            r1.decrementAndGet(r14)
        L_0x0076:
            r5.A02(r0)
        L_0x0079:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r0 = X.C94384bj.A04
            java.lang.Object r0 = r0.getAndSet(r5, r8)
            X.3ks r0 = (X.AbstractRunnableC75993ks) r0
            if (r0 != 0) goto L_0x0087
            X.3ks r0 = r5.A01()
        L_0x0087:
            return r0
        L_0x0088:
            int r13 = r13 + 1
            goto L_0x0053
        L_0x008b:
            long r0 = r5.A00(r14, r12)
            goto L_0x009b
        L_0x0090:
            r1 = 0
            X.3ks r0 = r14.A01()
            if (r0 != 0) goto L_0x0076
            long r0 = r5.A00(r14, r1)
        L_0x009b:
            r13 = -1
            int r4 = (r0 > r13 ? 1 : (r0 == r13 ? 0 : -1))
            if (r4 == 0) goto L_0x0079
            int r4 = (r0 > r16 ? 1 : (r0 == r16 ? 0 : -1))
            if (r4 <= 0) goto L_0x002f
            long r2 = java.lang.Math.min(r2, r0)
            goto L_0x002f
        L_0x00aa:
            r0 = 2147483647(0x7fffffff, float:NaN)
            r7 = r7 & r0
            int r7 = r7 % r9
            goto L_0x0024
        L_0x00b1:
            int r0 = (r2 > r18 ? 1 : (r2 == r18 ? 0 : -1))
            if (r0 != 0) goto L_0x00b7
            r2 = 0
        L_0x00b7:
            r11.A01 = r2
        L_0x00b9:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5HE.A02(boolean):X.3ks");
    }

    public final void A03(int i) {
        String valueOf;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A06.A03);
        A0h.append("-worker-");
        if (i == 0) {
            valueOf = "TERMINATED";
        } else {
            valueOf = String.valueOf(i);
        }
        setName(C12960it.A0d(valueOf, A0h));
        this.indexInArray = i;
    }

    public final boolean A04(EnumC87094Ae r7) {
        EnumC87094Ae r5 = this.A03;
        boolean z = false;
        if (r5 == EnumC87094Ae.A02) {
            z = true;
            AnonymousClass5E8.A09.addAndGet(this.A06, 4398046511104L);
        }
        if (r5 != r7) {
            this.A03 = r7;
        }
        return z;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        EnumC87094Ae r8;
        long j;
        int i;
        loop0: while (true) {
            boolean z = false;
            while (true) {
                AnonymousClass5E8 r15 = this.A06;
                if (r15._isTerminated != 0 || this.A03 == (r8 = EnumC87094Ae.A05)) {
                    break loop0;
                }
                AbstractRunnableC75993ks A01 = A01(this.A04);
                if (A01 != null) {
                    this.A01 = 0;
                    int i2 = A01.A01.A00;
                    this.A02 = 0;
                    if (this.A03 == EnumC87094Ae.A04) {
                        this.A03 = EnumC87094Ae.A01;
                    }
                    if (i2 != 0 && A04(EnumC87094Ae.A01) && !r15.A04() && !r15.A05(r15.controlState)) {
                        r15.A04();
                    }
                    AnonymousClass5E8.A00(A01);
                    if (i2 != 0) {
                        AnonymousClass5E8.A09.addAndGet(r15, -2097152);
                        if (this.A03 != r8) {
                            this.A03 = EnumC87094Ae.A03;
                        }
                    }
                } else {
                    this.A04 = false;
                    if (this.A01 != 0) {
                        if (z) {
                            A04(EnumC87094Ae.A04);
                            Thread.interrupted();
                            LockSupport.parkNanos(this.A01);
                            this.A01 = 0;
                            break;
                        }
                        z = true;
                    } else {
                        Object obj = this.nextParkedWorker;
                        AnonymousClass4VA r9 = AnonymousClass5E8.A07;
                        if (obj != r9) {
                            this.workerCtl = -1;
                            while (this.nextParkedWorker != r9 && this.workerCtl == -1 && r15._isTerminated == 0 && this.A03 != r8) {
                                A04(EnumC87094Ae.A04);
                                Thread.interrupted();
                                if (this.A02 == 0) {
                                    this.A02 = System.nanoTime() + r15.A02;
                                }
                                LockSupport.parkNanos(r15.A02);
                                if (System.nanoTime() - this.A02 >= 0) {
                                    this.A02 = 0;
                                    AtomicReferenceArray atomicReferenceArray = r15.A04;
                                    synchronized (atomicReferenceArray) {
                                        if (r15._isTerminated == 0 && ((int) (r15.controlState & 2097151)) > r15.A00 && A07.compareAndSet(this, -1, 1)) {
                                            int i3 = this.indexInArray;
                                            A03(0);
                                            r15.A03(this, i3, 0);
                                            int andDecrement = (int) (2097151 & AnonymousClass5E8.A09.getAndDecrement(r15));
                                            if (andDecrement != i3) {
                                                Object obj2 = atomicReferenceArray.get(andDecrement);
                                                C16700pc.A0C(obj2);
                                                AnonymousClass5HE r0 = (AnonymousClass5HE) obj2;
                                                atomicReferenceArray.set(i3, r0);
                                                r0.A03(i3);
                                                r15.A03(r0, andDecrement, i3);
                                            }
                                            atomicReferenceArray.set(andDecrement, null);
                                            this.A03 = r8;
                                        }
                                    }
                                }
                            }
                        } else if (this.nextParkedWorker == r9) {
                            do {
                                j = r15.parkedWorkersStack;
                                i = this.indexInArray;
                                this.nextParkedWorker = r15.A04.get((int) (2097151 & j));
                            } while (!AnonymousClass5E8.A0A.compareAndSet(r15, j, ((long) i) | ((2097152 + j) & -2097152)));
                        }
                    }
                }
            }
        }
        A04(EnumC87094Ae.A05);
    }
}
