package X;

import android.os.Handler;
import com.whatsapp.authentication.VerifyTwoFactorAuthCodeDialogFragment;

/* renamed from: X.3U0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3U0 implements AbstractC116435Vk {
    public final /* synthetic */ VerifyTwoFactorAuthCodeDialogFragment A00;

    public AnonymousClass3U0(VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment) {
        this.A00 = verifyTwoFactorAuthCodeDialogFragment;
    }

    @Override // X.AbstractC116435Vk
    public void AOH(String str) {
        VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment = this.A00;
        verifyTwoFactorAuthCodeDialogFragment.A03.setEnabled(false);
        verifyTwoFactorAuthCodeDialogFragment.A01.setProgress(0);
        Handler handler = verifyTwoFactorAuthCodeDialogFragment.A09;
        handler.removeMessages(0);
        handler.sendMessageDelayed(handler.obtainMessage(0, str), 400);
    }

    @Override // X.AbstractC116435Vk
    public void AT5(String str) {
        C12990iw.A1G(this.A00.A02);
    }
}
