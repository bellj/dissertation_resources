package X;

import java.math.BigInteger;
import java.security.AlgorithmParametersSpi;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import org.spongycastle.jcajce.provider.symmetric.AES;
import org.spongycastle.jcajce.provider.symmetric.PBEPBKDF2;

/* renamed from: X.5HV  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5HV extends AlgorithmParametersSpi {
    public static boolean A00(String str) {
        return str == null || str.equals("ASN.1");
    }

    @Override // java.security.AlgorithmParametersSpi
    public AlgorithmParameterSpec engineGetParameterSpec(Class cls) {
        byte[] bArr;
        if (cls == null) {
            throw C12980iv.A0n("argument to getParameterSpec must not be null");
        } else if (!(this instanceof PBEPBKDF2.AlgParams)) {
            if (this instanceof AES.AlgParamsGCM) {
                AES.AlgParamsGCM algParamsGCM = (AES.AlgParamsGCM) this;
                if (cls == AlgorithmParameterSpec.class || C94694cN.A00 == cls) {
                    if (C94694cN.A00 != null) {
                        return C94694cN.A00(algParamsGCM.A00.Aer());
                    }
                } else if (cls != AnonymousClass5IZ.class) {
                    if (cls == IvParameterSpec.class) {
                        bArr = algParamsGCM.A00.A01;
                    } else {
                        throw new InvalidParameterSpecException(C12960it.A0d(cls.getName(), C12960it.A0k("AlgorithmParameterSpec not recognized: ")));
                    }
                }
                return new AnonymousClass5IZ(AnonymousClass1TT.A02(algParamsGCM.A00.A01), algParamsGCM.A00.A00 << 3);
            } else if (!(this instanceof AES.AlgParamsCCM)) {
                AES.AlgParams algParams = (AES.AlgParams) this;
                if (cls == IvParameterSpec.class || cls == AlgorithmParameterSpec.class) {
                    return new IvParameterSpec(algParams.A00);
                }
                throw new InvalidParameterSpecException("unknown parameter spec passed to IV parameters object.");
            } else {
                AES.AlgParamsCCM algParamsCCM = (AES.AlgParamsCCM) this;
                if (cls == AlgorithmParameterSpec.class || C94694cN.A00 == cls) {
                    if (C94694cN.A00 != null) {
                        return C94694cN.A00(algParamsCCM.A00.Aer());
                    }
                } else if (cls != AnonymousClass5IZ.class) {
                    if (cls == IvParameterSpec.class) {
                        bArr = algParamsCCM.A00.A01;
                    } else {
                        throw new InvalidParameterSpecException(C12960it.A0d(cls.getName(), C12960it.A0k("AlgorithmParameterSpec not recognized: ")));
                    }
                }
                return new AnonymousClass5IZ(AnonymousClass1TT.A02(algParamsCCM.A00.A01), algParamsCCM.A00.A00 << 3);
            }
            return new IvParameterSpec(AnonymousClass1TT.A02(bArr));
        } else {
            PBEPBKDF2.AlgParams algParams2 = (PBEPBKDF2.AlgParams) this;
            if (cls == PBEParameterSpec.class || cls == AlgorithmParameterSpec.class) {
                C114645Mn r1 = algParams2.A00;
                return new PBEParameterSpec(r1.A02.A00, new BigInteger(r1.A00.A01).intValue());
            }
            throw new InvalidParameterSpecException("unknown parameter spec passed to PBKDF2 PBE parameters object.");
        }
    }
}
