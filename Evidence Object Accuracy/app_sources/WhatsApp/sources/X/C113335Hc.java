package X;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5Hc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113335Hc extends CertPath {
    public static final List A00;
    public List certificates;
    public final AnonymousClass5S2 helper;

    @Override // java.security.cert.CertPath
    public Iterator getEncodings() {
        return A00.iterator();
    }

    static {
        ArrayList A0l = C12960it.A0l();
        A0l.add("PkiPath");
        A0l.add("PEM");
        A0l.add("PKCS7");
        A00 = Collections.unmodifiableList(A0l);
    }

    public C113335Hc(InputStream inputStream, String str) {
        super("X.509");
        AnonymousClass5GT r4 = new AnonymousClass5GT();
        this.helper = r4;
        try {
            if (str.equalsIgnoreCase("PkiPath")) {
                AnonymousClass1TL A05 = new AnonymousClass1TO(inputStream).A05();
                if (A05 instanceof AbstractC114775Na) {
                    Enumeration A0C = ((AbstractC114775Na) A05).A0C();
                    this.certificates = C12960it.A0l();
                    CertificateFactory instance = CertificateFactory.getInstance("X.509", r4.A00);
                    while (A0C.hasMoreElements()) {
                        this.certificates.add(0, instance.generateCertificate(new ByteArrayInputStream(C72463ee.A0c((AnonymousClass1TN) A0C.nextElement()))));
                    }
                } else {
                    throw new CertificateException("input stream does not contain a ASN1 SEQUENCE while reading PkiPath encoded data to load CertPath");
                }
            } else if (str.equalsIgnoreCase("PKCS7") || str.equalsIgnoreCase("PEM")) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                this.certificates = C12960it.A0l();
                CertificateFactory instance2 = CertificateFactory.getInstance("X.509", r4.A00);
                while (true) {
                    Certificate generateCertificate = instance2.generateCertificate(bufferedInputStream);
                    if (generateCertificate == null) {
                        break;
                    }
                    this.certificates.add(generateCertificate);
                }
            } else {
                StringBuilder A0h = C12960it.A0h();
                A0h.append("unsupported encoding: ");
                throw new CertificateException(C12960it.A0d(str, A0h));
            }
            this.certificates = A00(this.certificates);
        } catch (IOException e) {
            throw new CertificateException(C12960it.A0d(e.toString(), C12960it.A0k("IOException throw while decoding CertPath:\n")));
        } catch (NoSuchProviderException e2) {
            throw new CertificateException(C12960it.A0d(e2.toString(), C12960it.A0k("SpongyCastle provider not found while trying to get a CertificateFactory:\n")));
        }
    }

    public C113335Hc(List list) {
        super("X.509");
        this.helper = new AnonymousClass5GT();
        this.certificates = A00(C12980iv.A0x(list));
    }

    public static final List A00(List list) {
        if (list.size() >= 2) {
            X500Principal issuerX500Principal = C72463ee.A0K(list, 0).getIssuerX500Principal();
            for (int i = 1; i != list.size(); i++) {
                if (issuerX500Principal.equals(C72463ee.A0K(list, i).getSubjectX500Principal())) {
                    issuerX500Principal = C72463ee.A0K(list, i).getIssuerX500Principal();
                } else {
                    ArrayList A0w = C12980iv.A0w(list.size());
                    ArrayList A0x = C12980iv.A0x(list);
                    for (int i2 = 0; i2 < list.size(); i2++) {
                        X509Certificate A0K = C72463ee.A0K(list, i2);
                        X500Principal subjectX500Principal = A0K.getSubjectX500Principal();
                        int i3 = 0;
                        while (true) {
                            if (i3 == list.size()) {
                                A0w.add(A0K);
                                list.remove(i2);
                                break;
                            } else if (!C72463ee.A0K(list, i3).getIssuerX500Principal().equals(subjectX500Principal)) {
                                i3++;
                            }
                        }
                    }
                    if (A0w.size() <= 1) {
                        for (int i4 = 0; i4 != A0w.size(); i4++) {
                            X500Principal issuerX500Principal2 = ((X509Certificate) A0w.get(i4)).getIssuerX500Principal();
                            int i5 = 0;
                            while (true) {
                                if (i5 < list.size()) {
                                    X509Certificate A0K2 = C72463ee.A0K(list, i5);
                                    if (issuerX500Principal2.equals(A0K2.getSubjectX500Principal())) {
                                        A0w.add(A0K2);
                                        list.remove(i5);
                                        break;
                                    }
                                    i5++;
                                }
                            }
                        }
                        if (list.size() <= 0) {
                            return A0w;
                        }
                    }
                    return A0x;
                }
            }
        }
        return list;
    }

    public static final AnonymousClass1TL A01(X509Certificate x509Certificate) {
        try {
            return new AnonymousClass1TO(x509Certificate.getEncoded()).A05();
        } catch (Exception e) {
            throw new CertificateEncodingException(C12960it.A0d(e.toString(), C12960it.A0k("Exception while encoding certificate: ")));
        }
    }

    @Override // java.security.cert.CertPath
    public List getCertificates() {
        return Collections.unmodifiableList(C12980iv.A0x(this.certificates));
    }

    @Override // java.security.cert.CertPath
    public byte[] getEncoded() {
        Iterator it = A00.iterator();
        if (!it.hasNext()) {
            return null;
        }
        Object next = it.next();
        if (next instanceof String) {
            return getEncoded((String) next);
        }
        return null;
    }

    @Override // java.security.cert.CertPath
    public byte[] getEncoded(String str) {
        char[] cArr;
        int length;
        int i;
        if (str.equalsIgnoreCase("PkiPath")) {
            C94954co r2 = new C94954co(10);
            List list = this.certificates;
            ListIterator listIterator = list.listIterator(list.size());
            while (listIterator.hasPrevious()) {
                r2.A06(A01((X509Certificate) listIterator.previous()));
            }
            try {
                return C72463ee.A0c(new AnonymousClass5NZ(r2));
            } catch (IOException e) {
                throw new CertificateEncodingException(C12960it.A0b("Exception thrown: ", e));
            }
        } else {
            int i2 = 0;
            if (str.equalsIgnoreCase("PKCS7")) {
                AnonymousClass5N3 r8 = new AnonymousClass5N3(null, AnonymousClass1TJ.A07);
                C94954co r22 = new C94954co(10);
                while (i2 != this.certificates.size()) {
                    r22.A06(A01(C72463ee.A0K(this.certificates, i2)));
                    i2++;
                }
                try {
                    return C72463ee.A0c(new AnonymousClass5N3(new AnonymousClass5N4(new AnonymousClass5NG(1), new C114785Nb(), new C114785Nb(r22), new C114785Nb(), r8), AnonymousClass1TJ.A2K));
                } catch (IOException e2) {
                    throw new CertificateEncodingException(C12960it.A0b("Exception thrown: ", e2));
                }
            } else if (str.equalsIgnoreCase("PEM")) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                C866748j r82 = new C866748j(new OutputStreamWriter(byteArrayOutputStream));
                while (i2 != this.certificates.size()) {
                    try {
                        C93564aN r23 = new C93564aN(C72463ee.A0K(this.certificates, i2).getEncoded());
                        String str2 = r23.A00;
                        StringBuilder A0k = C12960it.A0k("-----BEGIN ");
                        A0k.append(str2);
                        r82.write(C12960it.A0d("-----", A0k));
                        r82.newLine();
                        List list2 = r23.A01;
                        if (!list2.isEmpty()) {
                            Iterator it = list2.iterator();
                            if (it.hasNext()) {
                                it.next();
                                throw C12980iv.A0n("getName");
                            }
                            r82.newLine();
                        }
                        byte[] bArr = r23.A02;
                        int length2 = bArr.length;
                        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream(((length2 + 2) / 3) << 2);
                        try {
                            C88624Gk.A00.A9L(byteArrayOutputStream2, bArr, 0, length2);
                            byte[] byteArray = byteArrayOutputStream2.toByteArray();
                            int i3 = 0;
                            while (true) {
                                int length3 = byteArray.length;
                                if (i3 < length3) {
                                    int i4 = 0;
                                    while (true) {
                                        cArr = r82.A00;
                                        length = cArr.length;
                                        if (i4 != length && (i = i3 + i4) < length3) {
                                            cArr[i4] = (char) byteArray[i];
                                            i4++;
                                        }
                                    }
                                    r82.write(cArr, 0, i4);
                                    r82.newLine();
                                    i3 += length;
                                }
                            }
                            StringBuilder A0k2 = C12960it.A0k("-----END ");
                            A0k2.append(str2);
                            r82.write(C12960it.A0d("-----", A0k2));
                            r82.newLine();
                            i2++;
                        } catch (Exception e3) {
                            throw new AnonymousClass4CT(C12960it.A0d(e3.getMessage(), C12960it.A0k("exception encoding base64 string: ")), e3);
                        }
                    } catch (Exception unused) {
                        throw new CertificateEncodingException("can't encode certificate for PEM encoded path");
                    }
                }
                r82.close();
                return byteArrayOutputStream.toByteArray();
            } else {
                throw new CertificateEncodingException(C12960it.A0d(str, C12960it.A0k("unsupported encoding: ")));
            }
        }
    }
}
