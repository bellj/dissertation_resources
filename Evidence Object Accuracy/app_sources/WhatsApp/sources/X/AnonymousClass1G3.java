package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.1G3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1G3 extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public AnonymousClass1G3() {
        super(C27081Fy.A0i);
    }

    public C49692Lu A05() {
        C49692Lu r0 = ((C27081Fy) this.A00).A0e;
        if (r0 == null) {
            return C49692Lu.A05;
        }
        return r0;
    }

    public void A06(C57552nF r3) {
        A03();
        C27081Fy r1 = (C27081Fy) this.A00;
        r1.A03 = r3;
        r1.A01 |= 1;
    }

    public void A07(C57752nZ r3) {
        A03();
        C27081Fy r1 = (C27081Fy) this.A00;
        r1.A0K = r3;
        r1.A01 |= 8;
    }

    public void A08(C56882m6 r3) {
        A03();
        C27081Fy r1 = (C27081Fy) this.A00;
        r1.A0W = (C57692nT) r3.A02();
        r1.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
    }

    public void A09(C57692nT r3) {
        A03();
        C27081Fy r1 = (C27081Fy) this.A00;
        r1.A0W = r3;
        r1.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
    }

    public void A0A(C56892m7 r3) {
        A03();
        C27081Fy r1 = (C27081Fy) this.A00;
        r1.A0b = (C57242mi) r3.A02();
        r1.A00 |= 2;
    }

    public void A0B(AnonymousClass2Lw r4) {
        A03();
        C27081Fy r2 = (C27081Fy) this.A00;
        r2.A0e = (C49692Lu) r4.A02();
        r2.A00 |= 1048576;
    }
}
