package X;

import android.accounts.Account;
import android.content.Context;
import android.net.TrafficStats;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRoute;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.BrowserCompatHostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.DefaultHttpRoutePlanner;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1zY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44791zY {
    public static final ConnPerRoute A0K = new ConnPerRoute() { // from class: X.5FZ
        public final int getMaxForRoute(HttpRoute httpRoute) {
            return 2;
        }
    };
    public String A00;
    public boolean A01 = false;
    public boolean A02 = true;
    public final Context A03;
    public final AbstractC15710nm A04;
    public final C18790t3 A05;
    public final C15820nx A06;
    public final C71613dD A07;
    public final C27051Fv A08;
    public final C18640sm A09;
    public final C15810nw A0A;
    public final C15890o4 A0B;
    public final C14850m9 A0C;
    public final AbstractC14440lR A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final AtomicInteger A0H = new AtomicInteger(0);
    public final SSLSocketFactory A0I;
    public final DefaultHttpClient A0J;

    public C44791zY(Context context, AbstractC15710nm r10, C18790t3 r11, C15820nx r12, C27051Fv r13, C18640sm r14, C15810nw r15, C15890o4 r16, C14850m9 r17, C19930uu r18, AbstractC14440lR r19, String str, String str2) {
        String str3;
        this.A03 = context;
        this.A0C = r17;
        this.A04 = r10;
        this.A05 = r11;
        this.A0A = r15;
        this.A06 = r12;
        this.A08 = r13;
        this.A0B = r16;
        this.A0E = str;
        this.A09 = r14;
        this.A0D = r19;
        synchronized (r18) {
            str3 = r18.A01;
            if (str3 == null) {
                str3 = r18.A02(r18.A03, "2.22.17.70");
                r18.A01 = str3;
            }
        }
        this.A0G = str3;
        this.A0I = new C71803dX(r11);
        C71613dD r6 = new C71613dD(r11);
        this.A07 = r6;
        C71623dE r5 = new C71623dE(r11);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        basicHttpParams.setParameter("http.protocol.version", HttpVersion.HTTP_1_1);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, A0K);
        ConnManagerParams.getMaxConnectionsPerRoute(basicHttpParams).getMaxForRoute(new HttpRoute(new HttpHost("backup.googleapis.com")));
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, true);
        HttpProtocolParams.setUserAgent(basicHttpParams, this.A0G);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("https", new C71633dF(), 443));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        defaultHttpClient.setRoutePlanner(new DefaultHttpRoutePlanner(schemeRegistry));
        defaultHttpClient.addResponseInterceptor(r5);
        defaultHttpClient.addRequestInterceptor(r6);
        this.A0J = defaultHttpClient;
        this.A0F = str2;
    }

    public static /* synthetic */ HttpResponse A00(C44791zY r2, HttpRequestBase httpRequestBase) {
        httpRequestBase.getURI();
        httpRequestBase.setHeader("Host", "backup.googleapis.com");
        StringBuilder sb = new StringBuilder("Bearer ");
        sb.append(r2.A00);
        httpRequestBase.setHeader("Authorization", sb.toString());
        return r2.A0J.execute(httpRequestBase);
    }

    public static void A01(AbstractC15710nm r3, String str, int i) {
        if (i >= 400 && i < 500) {
            StringBuilder sb = new StringBuilder("gdrive-api-v2/unhandled/error/");
            sb.append(str);
            String obj = sb.toString();
            StringBuilder sb2 = new StringBuilder("status-code = ");
            sb2.append(i);
            r3.AaV(obj, sb2.toString(), false);
        }
    }

    public static final boolean A02(File file, File file2) {
        if (file.renameTo(file2)) {
            return true;
        }
        StringBuilder sb = new StringBuilder("gdrive-api-v2/rename-local/file/failed ");
        sb.append(file.getAbsolutePath());
        sb.append(" -> ");
        sb.append(file2.getAbsolutePath());
        Log.e(sb.toString());
        return false;
    }

    public Pair A03(String str, String str2, String str3, int i) {
        Throwable th;
        IOException e;
        HttpsURLConnection A07;
        HttpsURLConnection httpsURLConnection = null;
        if (A0B()) {
            StringBuilder sb = new StringBuilder("gdrive-api-v2/list-files/api is disabled for ");
            sb.append(str);
            sb.append(" transaction=");
            sb.append(str3);
            Log.w(sb.toString());
            return null;
        }
        TrafficStats.setThreadStatsTag(13);
        try {
            try {
                HashMap hashMap = new HashMap();
                hashMap.put("pageSize", Integer.toString(i));
                if (!TextUtils.isEmpty(str3)) {
                    hashMap.put("transaction_id", str3);
                }
                if (!TextUtils.isEmpty(str2)) {
                    hashMap.put("pageToken", str2);
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("clients/wa/backups/");
                sb2.append(str);
                sb2.append("/files");
                A07 = A07("GET", sb2.toString(), "application/json; charset=UTF-8", hashMap, false);
            } catch (IOException e2) {
                e = e2;
            }
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            int responseCode = A07.getResponseCode();
            if (responseCode == 200) {
                try {
                    JSONObject A02 = AnonymousClass1P1.A02(A07.getInputStream());
                    if (A02 != null) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("clients/wa/backups/");
                        sb3.append(str);
                        sb3.append("/files/");
                        String obj = sb3.toString();
                        JSONArray optJSONArray = A02.optJSONArray("files");
                        if (optJSONArray != null) {
                            ArrayList arrayList = new ArrayList(optJSONArray.length());
                            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                                JSONObject jSONObject = optJSONArray.getJSONObject(i2);
                                AnonymousClass3HV A00 = AnonymousClass3HV.A00(this.A06, obj, null, jSONObject, -1);
                                if (A00 != null) {
                                    arrayList.add(A00);
                                } else {
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append("gdrive-api-v2/list-files/missing some attrs ");
                                    sb4.append(jSONObject);
                                    Log.e(sb4.toString());
                                }
                            }
                            Pair create = Pair.create(arrayList, A02.optString("nextPageToken", null));
                            A07.disconnect();
                            TrafficStats.clearThreadStatsTag();
                            return create;
                        }
                    } else {
                        Log.e("gdrive-api-v2/list-files/empty response");
                        throw new C84173yX("empty file list", -1);
                    }
                } catch (JSONException e3) {
                    Log.e("gdrive-api-v2/list-files/invalid stream", e3);
                    throw new C84173yX(e3.getMessage(), -1);
                }
            } else if (responseCode != 401) {
                if (responseCode == 403) {
                    throw new C84093yP();
                } else if (responseCode != 429) {
                    String A002 = AnonymousClass1P1.A00(A07.getErrorStream());
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("gdrive-api-v2/list-files weird status code: ");
                    sb5.append(responseCode);
                    sb5.append(" ");
                    sb5.append(A002);
                    Log.e(sb5.toString());
                    A01(this.A04, "list-files", responseCode);
                    throw new C84173yX(A002, -1);
                } else {
                    C65163Ij.A01("list-files", A07, this.A0C.A07(916));
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                }
            }
            A0A();
            A07.disconnect();
            TrafficStats.clearThreadStatsTag();
            return Pair.create(Collections.emptyList(), null);
        } catch (IOException e4) {
            e = e4;
            httpsURLConnection = A07;
            Log.e("gdrive-api-v2/list-files failed with exception", e);
            throw new C84173yX(e.getMessage(), -1);
        } catch (Throwable th3) {
            th = th3;
            httpsURLConnection = A07;
            if (httpsURLConnection != null) {
                httpsURLConnection.disconnect();
            }
            TrafficStats.clearThreadStatsTag();
            throw th;
        }
    }

    public C65113Ie A04(String str) {
        Throwable th;
        IOException e;
        HttpsURLConnection A07;
        HttpsURLConnection httpsURLConnection = null;
        if (A0B()) {
            Log.i("gdrive-api-v2/get-backup/api disabled");
            return null;
        }
        TrafficStats.setThreadStatsTag(13);
        try {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("clients/wa/backups/");
                sb.append(str);
                A07 = A07("GET", sb.toString(), null, null, false);
            } catch (IOException e2) {
                e = e2;
            }
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            int responseCode = A07.getResponseCode();
            if (responseCode == 200) {
                C65113Ie A00 = C65113Ie.A00(this.A04, this.A06, this, this.A0C, A07.getInputStream(), str);
                A07.disconnect();
                TrafficStats.clearThreadStatsTag();
                return A00;
            } else if (responseCode == 401) {
                A0A();
                A07.disconnect();
                TrafficStats.clearThreadStatsTag();
                return null;
            } else if (responseCode == 429) {
                C65163Ij.A01("get-backup", A07, this.A0C.A07(916));
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            } else if (responseCode == 403) {
                throw new C84093yP();
            } else if (responseCode != 404) {
                A07.getURL();
                String A002 = AnonymousClass1P1.A00(A07.getErrorStream());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("gdrive-api-v2/get-backup/failed ");
                sb2.append(A002);
                Log.e(sb2.toString());
                throw new C84173yX(A002, -1);
            } else {
                throw new C84053yL(AnonymousClass1P1.A00(A07.getErrorStream()));
            }
        } catch (IOException e3) {
            e = e3;
            httpsURLConnection = A07;
            Log.e(e);
            throw new C84173yX(e.getMessage(), -1);
        } catch (Throwable th3) {
            th = th3;
            httpsURLConnection = A07;
            if (httpsURLConnection != null) {
                httpsURLConnection.disconnect();
            }
            TrafficStats.clearThreadStatsTag();
            throw th;
        }
    }

    public AnonymousClass3HV A05(AnonymousClass5TJ r16, AbstractC44761zV r17, C65113Ie r18, C64413Fl r19, int i) {
        TrafficStats.setThreadStatsTag(13);
        try {
            C14850m9 r12 = this.A0C;
            AbstractC15710nm r1 = this.A04;
            C15810nw r10 = this.A0A;
            return new AnonymousClass3HM(r1, this.A06, r16, this.A08, r17, r18, this, r19, this.A09, r10, this.A0B, r12, this.A0D, i).A01();
        } finally {
            TrafficStats.clearThreadStatsTag();
        }
    }

    public final String A06(String str, Map map) {
        String str2 = this.A0F;
        if (str2 != null) {
            if (map == null) {
                map = new HashMap(1);
            }
            AnonymousClass009.A0A("mode param should not be included in params", true ^ map.containsKey("mode"));
            map.put("mode", str2);
        } else if (map == null) {
            return str;
        }
        if (map.isEmpty()) {
            return str;
        }
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        for (Map.Entry entry : map.entrySet()) {
            buildUpon.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
        }
        return buildUpon.build().toString();
    }

    public HttpsURLConnection A07(String str, String str2, String str3, Map map, boolean z) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("/v1/");
            sb.append(str2);
            return A08(str, new URI("https", null, "backup.googleapis.com", -1, sb.toString(), null, null).toASCIIString(), str3, map, z);
        } catch (URISyntaxException e) {
            throw new IOException(e);
        }
    }

    public HttpsURLConnection A08(String str, String str2, String str3, Map map, boolean z) {
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) new URL(A06(str2, map)).openConnection();
        httpsURLConnection.setSSLSocketFactory(this.A0I);
        httpsURLConnection.setRequestProperty("Host", "backup.googleapis.com");
        httpsURLConnection.setHostnameVerifier(new C37591mg("backup.googleapis.com", new BrowserCompatHostnameVerifier()));
        StringBuilder sb = new StringBuilder("Bearer ");
        sb.append(this.A00);
        httpsURLConnection.setRequestProperty("Authorization", sb.toString());
        httpsURLConnection.setRequestProperty("User-Agent", this.A0G);
        httpsURLConnection.setConnectTimeout(15000);
        httpsURLConnection.setReadTimeout(30000);
        httpsURLConnection.setRequestMethod(str);
        if (str3 != null) {
            httpsURLConnection.setRequestProperty("Content-Type", str3);
        }
        httpsURLConnection.setDoOutput(z);
        this.A0H.incrementAndGet();
        return httpsURLConnection;
    }

    public synchronized void A09(boolean z) {
        if (this.A02 != z) {
            Log.i(z ? "gdrive-api-v2/enabled" : "gdrive-api-v2/disabled");
            this.A02 = z;
        }
    }

    public boolean A0A() {
        String str = this.A0E;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("gdrive-api-v2/auth-request asking GoogleAuthUtil for auth token: ");
            sb.append(C44771zW.A0B(str));
            Log.i(sb.toString());
            String str2 = this.A00;
            if (str2 != null) {
                AnonymousClass3JL.A04(this.A03, str2);
            }
            this.A00 = AnonymousClass3JL.A01(new Account(str, "com.google"), this.A03);
            Log.i("gdrive-api-v2/auth-request/received-auth-token");
            return true;
        } catch (C77433nH e) {
            Log.i("gdrive-api-v2/auth-request user recoverable exception happened and notification was published by the system to resolve it.");
            throw new C84073yN(e);
        } catch (C77443nI e2) {
            Log.e("gdrive-api-v2/auth-request Google Play services is unavailable, if it was unavailable from the beginning code would not have reached here, so, most likely it became unavailable while the backup/restore was in progress");
            Log.e("gdrive-api-v2/auth-request", e2);
            this.A00 = null;
            throw new C84073yN(e2);
        } catch (C77453nJ e3) {
            StringBuilder sb2 = new StringBuilder("gdrive-api-v2/auth-request permission to access Google Drive for ");
            sb2.append(C44771zW.A0B(str));
            sb2.append(" is not available and we cannot ask user for permission either.");
            Log.i(sb2.toString());
            throw new C84073yN(e3);
        } catch (AnonymousClass4CA e4) {
            Log.e("gdrive-api-v2/auth-request", e4);
            if ("BadUsername".equals(e4.getMessage())) {
                throw new C84183yY(e4);
            } else if ("ServiceUnavailable".equals(e4.getMessage())) {
                return false;
            } else {
                this.A00 = null;
                throw new C84073yN(e4);
            }
        } catch (IOException e5) {
            Log.e("gdrive-api-v2/auth-request", e5);
            this.A00 = null;
            return false;
        } catch (NullPointerException e6) {
            StringBuilder sb3 = new StringBuilder("gdrive-api-v2/auth-request unexpected NullPointerException while trying to get  auth token for the account ");
            sb3.append(C44771zW.A0B(str));
            Log.e(sb3.toString());
            Log.e("gdrive-api-v2/auth-request", e6);
            this.A00 = null;
            throw new C84073yN(e6);
        } catch (SecurityException e7) {
            Log.e("gdrive-api-v2/auth-request", e7);
            this.A00 = null;
            throw new C84073yN(e7);
        }
    }

    public synchronized boolean A0B() {
        return !this.A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0383, code lost:
        if (r3 == null) goto L_0x0394;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f0, code lost:
        if (A0D(r11, r38.A03, r11.length()) == false) goto L_0x00de;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0C(X.AnonymousClass5TI r36, X.AbstractC44761zV r37, X.AnonymousClass3HV r38, java.io.File r39) {
        /*
        // Method dump skipped, instructions count: 1125
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C44791zY.A0C(X.5TI, X.1zV, X.3HV, java.io.File):boolean");
    }

    public final boolean A0D(File file, String str, long j) {
        String A09 = C44771zW.A09(this.A0A, this.A0B, file, j);
        if (str.equals(A09)) {
            return true;
        }
        StringBuilder sb = new StringBuilder("gdrive-api-v2/save-file/check-md5 ");
        sb.append(file.getAbsolutePath());
        sb.append(" downloaded but its MD5(");
        sb.append(A09);
        sb.append(") does not match remote md5(");
        sb.append(str);
        sb.append(").");
        Log.e(sb.toString());
        return false;
    }
}
