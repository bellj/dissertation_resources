package X;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.finsky.externalreferrer.IGetInstallReferrerService;

/* renamed from: X.3Lj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65883Lj implements IInterface, IGetInstallReferrerService {
    public final IBinder A00;

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }

    public C65883Lj(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // com.google.android.finsky.externalreferrer.IGetInstallReferrerService
    public final Bundle A6n(Bundle bundle) {
        Parcel obtain;
        try {
            obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
            obtain.writeInt(1);
            bundle.writeToParcel(obtain, 0);
            obtain = Parcel.obtain();
            this.A00.transact(1, obtain, obtain, 0);
            obtain.readException();
            obtain.recycle();
            return (Bundle) C12970iu.A0F(obtain, Bundle.CREATOR);
        } catch (RuntimeException e) {
            throw e;
        } finally {
            obtain.recycle();
        }
    }
}
