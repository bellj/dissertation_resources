package X;

import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.location.LocationRequest;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.1UC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UC implements AbstractC14980mM, AbstractC15000mO {
    public final /* synthetic */ C244615p A00;

    @Override // X.AbstractC15010mP
    public void onConnectionFailed(C56492ky r1) {
    }

    @Override // X.AbstractC14990mN
    public void onConnectionSuspended(int i) {
    }

    public /* synthetic */ AnonymousClass1UC(C244615p r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC14990mN
    public void onConnected(Bundle bundle) {
        C244615p r6 = this.A00;
        if (r6.A07.A03()) {
            Map map = r6.A02;
            AnonymousClass009.A05(map);
            for (AnonymousClass1U2 r4 : map.values()) {
                LocationRequest A00 = C244615p.A00(r4);
                try {
                    AnonymousClass1U8 r2 = r6.A01;
                    C13020j0.A02(Looper.myLooper(), "Calling thread must be a prepared Looper thread.");
                    r2.A06(new AnonymousClass1UM(r2, r4, A00));
                } catch (SecurityException e) {
                    Log.w("FusedLocationManager/GmsConnectionCallbacks/onConnected/unable to request location updates", e);
                }
            }
            if (r6.A02.isEmpty()) {
                AnonymousClass1U8 r0 = r6.A01;
                AnonymousClass009.A05(r0);
                r0.A09();
            }
        }
    }
}
