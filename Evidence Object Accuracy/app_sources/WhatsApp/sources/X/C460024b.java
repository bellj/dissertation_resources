package X;

/* renamed from: X.24b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C460024b {
    public final String A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C460024b) && C16700pc.A0O(this.A00, ((C460024b) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PhoenixFlowInfo(config=");
        sb.append(this.A00);
        sb.append(')');
        return sb.toString();
    }

    public C460024b() {
        this("");
    }

    public C460024b(String str) {
        C16700pc.A0E(str, 1);
        this.A00 = str;
    }
}
