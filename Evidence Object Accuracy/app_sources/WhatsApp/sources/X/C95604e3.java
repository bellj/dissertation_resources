package X;

import android.media.MediaCodecInfo;
import android.util.Log;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: X.4e3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95604e3 {
    public static int A00 = -1;
    public static final HashMap A01 = C12970iu.A11();
    public static final Pattern A02 = Pattern.compile("^\\D?(\\d+)$");

    public static int A00() {
        C95494dp r0;
        MediaCodecInfo.CodecProfileLevel[] codecProfileLevelArr;
        int i;
        int i2 = A00;
        if (i2 == -1) {
            i2 = 0;
            List A03 = A03("video/avc", false, false);
            if (!A03.isEmpty() && (r0 = (C95494dp) A03.get(0)) != null) {
                MediaCodecInfo.CodecCapabilities codecCapabilities = r0.A00;
                if (codecCapabilities == null || (codecProfileLevelArr = codecCapabilities.profileLevels) == null) {
                    codecProfileLevelArr = new MediaCodecInfo.CodecProfileLevel[0];
                }
                int length = codecProfileLevelArr.length;
                int i3 = 0;
                while (i2 < length) {
                    int i4 = codecProfileLevelArr[i2].level;
                    if (i4 == 1 || i4 == 2) {
                        i = 25344;
                    } else {
                        switch (i4) {
                            case 8:
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                            case 32:
                                i = 101376;
                                continue;
                            case 64:
                                i = 202752;
                                continue;
                            case 128:
                            case 256:
                                i = 414720;
                                continue;
                            case 512:
                                i = 921600;
                                continue;
                            case EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH /* 1024 */:
                                i = 1310720;
                                continue;
                            case EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH /* 2048 */:
                            case 4096:
                                i = 2097152;
                                continue;
                            case DefaultCrypto.BUFFER_SIZE /* 8192 */:
                                i = 2228224;
                                continue;
                            case 16384:
                                i = 5652480;
                                continue;
                            case 32768:
                            case 65536:
                                i = 9437184;
                                continue;
                            default:
                                i = -1;
                                continue;
                        }
                    }
                    i3 = Math.max(i, i3);
                    i2++;
                }
                int i5 = 172800;
                if (AnonymousClass3JZ.A01 >= 21) {
                    i5 = 345600;
                }
                i2 = Math.max(i3, i5);
            }
            A00 = i2;
        }
        return i2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 10, insn: 0x055c: INVOKE  (r0 I:java.lang.StringBuilder) = (r10 I:java.lang.String) type: STATIC call: X.0it.A0j(java.lang.String):java.lang.StringBuilder, block:B:358:0x055c
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static android.util.Pair A01(
/*
[1896] Method generation error in method: X.4e3.A01(X.4mC):android.util.Pair, file: classes3.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r11v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0112, code lost:
        if (r1 == 16) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0027, code lost:
        if (A04(r15) == false) goto L_0x0029;
     */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0208 A[Catch: Exception -> 0x033d, TryCatch #0 {Exception -> 0x033d, blocks: (B:3:0x0004, B:5:0x0018, B:7:0x0022, B:11:0x002c, B:14:0x0038, B:19:0x0046, B:21:0x004e, B:23:0x0056, B:25:0x005e, B:27:0x0066, B:29:0x006e, B:34:0x007c, B:36:0x0084, B:38:0x008e, B:40:0x0098, B:45:0x00a6, B:47:0x00ae, B:49:0x00b8, B:51:0x00c0, B:53:0x00c8, B:55:0x00d0, B:57:0x00d8, B:59:0x00e0, B:61:0x00e8, B:63:0x00f0, B:65:0x00f8, B:67:0x0100, B:69:0x0108, B:73:0x0114, B:75:0x011c, B:77:0x0126, B:79:0x012e, B:81:0x0136, B:86:0x0146, B:88:0x014e, B:90:0x0156, B:92:0x015e, B:94:0x0168, B:96:0x0170, B:98:0x0178, B:100:0x0180, B:102:0x0188, B:104:0x0190, B:106:0x0198, B:111:0x01a7, B:113:0x01af, B:115:0x01b7, B:117:0x01c1, B:119:0x01c9, B:121:0x01cf, B:123:0x01d7, B:127:0x01e2, B:129:0x01ea, B:132:0x01f3, B:134:0x01fb, B:139:0x0208, B:141:0x0210, B:143:0x0218, B:144:0x021b, B:146:0x0223, B:149:0x022e, B:151:0x0236, B:154:0x0241, B:156:0x0249, B:159:0x0254, B:161:0x025c, B:206:0x0300, B:208:0x0306, B:210:0x031e, B:211:0x033b, B:166:0x026a, B:171:0x0280, B:172:0x0284, B:181:0x029c, B:182:0x02a0, B:183:0x02a3, B:185:0x02aa, B:186:0x02ae, B:188:0x02b4, B:189:0x02ba, B:191:0x02c2, B:193:0x02ca, B:201:0x02dd, B:202:0x02f0), top: B:216:0x0004, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x0280 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList A02(X.AnonymousClass4X5 r15, X.AnonymousClass5X8 r16) {
        /*
        // Method dump skipped, instructions count: 836
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95604e3.A02(X.4X5, X.5X8):java.util.ArrayList");
    }

    public static synchronized List A03(String str, boolean z, boolean z2) {
        List list;
        AnonymousClass5X8 r0;
        synchronized (C95604e3.class) {
            AnonymousClass4X5 r3 = new AnonymousClass4X5(str, z, z2);
            HashMap hashMap = A01;
            list = (List) hashMap.get(r3);
            if (list == null) {
                int i = AnonymousClass3JZ.A01;
                if (i >= 21) {
                    r0 = new C107354xC(z, z2);
                } else {
                    r0 = new C107344xB();
                }
                ArrayList A022 = A02(r3, r0);
                if (z && A022.isEmpty() && 21 <= i && i <= 23) {
                    A022 = A02(r3, new C107344xB());
                    if (!A022.isEmpty()) {
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append("MediaCodecList API didn't list secure decoder for: ");
                        A0h.append(str);
                        A0h.append(". Assuming: ");
                        Log.w("MediaCodecUtil", C12960it.A0d(((C95494dp) A022.get(0)).A03, A0h));
                    }
                }
                if ("audio/raw".equals(str)) {
                    if (i < 26 && AnonymousClass3JZ.A02.equals("R9") && A022.size() == 1 && ((C95494dp) A022.get(0)).A03.equals("OMX.MTK.AUDIO.DECODER.RAW")) {
                        A022.add(new C95494dp(null, "OMX.google.raw.decoder", "audio/raw", "audio/raw", false, false));
                    }
                    Collections.sort(A022, new AnonymousClass5CY(new AnonymousClass5SM() { // from class: X.4xD
                        @Override // X.AnonymousClass5SM
                        public final int AGR(Object obj) {
                            String str2 = ((C95494dp) obj).A03;
                            if (str2.startsWith("OMX.google") || str2.startsWith("c2.android")) {
                                return 1;
                            }
                            return (AnonymousClass3JZ.A01 >= 26 || !str2.equals("OMX.MTK.AUDIO.DECODER.RAW")) ? 0 : -1;
                        }
                    }));
                }
                if (i < 21 && A022.size() > 1) {
                    String str2 = ((C95494dp) A022.get(0)).A03;
                    if ("OMX.SEC.mp3.dec".equals(str2) || "OMX.SEC.MP3.Decoder".equals(str2) || "OMX.brcm.audio.mp3.decoder".equals(str2)) {
                        Collections.sort(A022, new AnonymousClass5CY(new AnonymousClass5SM() { // from class: X.4xE
                            @Override // X.AnonymousClass5SM
                            public final int AGR(Object obj) {
                                return ((C95494dp) obj).A03.startsWith("OMX.google") ? 1 : 0;
                            }
                        }));
                    }
                }
                if (i < 30 && A022.size() > 1 && "OMX.qti.audio.decoder.flac".equals(((C95494dp) A022.get(0)).A03)) {
                    A022.add(A022.remove(0));
                }
                list = Collections.unmodifiableList(A022);
                hashMap.put(r3, list);
            }
        }
        return list;
    }

    public static boolean A04(MediaCodecInfo mediaCodecInfo) {
        return mediaCodecInfo.isAlias();
    }

    public static boolean A05(MediaCodecInfo mediaCodecInfo) {
        return mediaCodecInfo.isHardwareAccelerated();
    }

    public static boolean A06(MediaCodecInfo mediaCodecInfo) {
        if (AnonymousClass3JZ.A01 >= 29) {
            return A07(mediaCodecInfo);
        }
        String name = mediaCodecInfo.getName();
        if (name != null) {
            name = name.toLowerCase(Locale.US);
        }
        if (name.startsWith("arc.")) {
            return false;
        }
        if (name.startsWith("omx.google.") || name.startsWith("omx.ffmpeg.") || ((name.startsWith("omx.sec.") && name.contains(".sw.")) || name.equals("omx.qcom.video.decoder.hevcswvdec") || name.startsWith("c2.android.") || name.startsWith("c2.google.") || (!name.startsWith("omx.") && !name.startsWith("c2.")))) {
            return true;
        }
        return false;
    }

    public static boolean A07(MediaCodecInfo mediaCodecInfo) {
        return mediaCodecInfo.isSoftwareOnly();
    }

    public static boolean A08(MediaCodecInfo mediaCodecInfo) {
        return mediaCodecInfo.isVendor();
    }
}
