package X;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.0B6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0B6 extends ViewGroup.MarginLayoutParams {
    public AnonymousClass03U A00;
    public boolean A01 = true;
    public boolean A02 = false;
    public final Rect A03 = new Rect();

    public AnonymousClass0B6(int i, int i2) {
        super(i, i2);
    }

    public AnonymousClass0B6(AnonymousClass0B6 r2) {
        super((ViewGroup.LayoutParams) r2);
    }

    public AnonymousClass0B6(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AnonymousClass0B6(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public AnonymousClass0B6(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }

    public int A00() {
        AnonymousClass03U r2 = this.A00;
        int i = r2.A06;
        if (i == -1) {
            return r2.A05;
        }
        return i;
    }
}
