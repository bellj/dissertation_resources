package X;

import android.os.Handler;
import com.whatsapp.registration.ChangeNumber;

/* renamed from: X.595  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass595 implements AnonymousClass2SS {
    public final /* synthetic */ ChangeNumber A00;

    public AnonymousClass595(ChangeNumber changeNumber) {
        this.A00 = changeNumber;
    }

    @Override // X.AnonymousClass2SS
    public void AT7(int i) {
        this.A00.A0J.sendEmptyMessage(3);
    }

    @Override // X.AnonymousClass2SS
    public void AT8(String str) {
        Handler handler;
        int i;
        ChangeNumber changeNumber = this.A00;
        String A04 = ((ActivityC13790kL) changeNumber).A01.A04();
        if (A04 == null || !A04.equals(str)) {
            handler = changeNumber.A0J;
            i = 2;
        } else {
            handler = changeNumber.A0J;
            i = 1;
        }
        handler.sendEmptyMessage(i);
    }
}
