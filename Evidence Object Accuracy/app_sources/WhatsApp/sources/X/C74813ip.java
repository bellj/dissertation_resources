package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.3ip  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74813ip extends AnonymousClass02M {
    public List A00;

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r1, int i) {
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75143jN(C12960it.A0E(viewGroup).inflate(R.layout.popular_categories_shimmer_cell, viewGroup, false));
    }
}
