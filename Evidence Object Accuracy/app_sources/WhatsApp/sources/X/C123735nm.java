package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.5nm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123735nm extends AbstractC130195yx {
    public C121035h9 A00;

    public C123735nm(Context context, AnonymousClass018 r3, AnonymousClass14X r4) {
        super(context, r3, r4, 2);
    }

    @Override // X.AbstractC130195yx
    public CharSequence A03() {
        AbstractC30791Yv A00 = this.A01.A00();
        Context context = this.A05;
        return A00.AA7(context, C12960it.A0X(context, super.A03(), C12970iu.A1b(), 0, R.string.payments_history_amount_credited));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r2 == 705) goto L_0x0019;
     */
    @Override // X.AbstractC130195yx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.AnonymousClass1IR r4) {
        /*
            r3 = this;
            super.A06(r4)
            X.5fA r0 = r3.A02
            X.63k r0 = r0.A01
            X.AnonymousClass009.A05(r0)
            X.5h9 r0 = (X.C121035h9) r0
            r3.A00 = r0
            int r2 = r4.A02
            r0 = 704(0x2c0, float:9.87E-43)
            if (r2 == r0) goto L_0x0019
            r1 = 705(0x2c1, float:9.88E-43)
            r0 = 0
            if (r2 != r1) goto L_0x001a
        L_0x0019:
            r0 = 1
        L_0x001a:
            r3.A03 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123735nm.A06(X.1IR):void");
    }
}
