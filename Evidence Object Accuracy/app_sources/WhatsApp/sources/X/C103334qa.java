package X;

import android.content.Context;
import com.whatsapp.registration.ChangeNumber;

/* renamed from: X.4qa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103334qa implements AbstractC009204q {
    public final /* synthetic */ ChangeNumber A00;

    public C103334qa(ChangeNumber changeNumber) {
        this.A00 = changeNumber;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
