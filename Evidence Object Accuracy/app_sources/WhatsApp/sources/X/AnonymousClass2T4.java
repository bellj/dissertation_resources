package X;

import android.content.Context;

/* renamed from: X.2T4  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2T4 extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass2T4(Context context) {
        super(context, null);
        A00();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v4, types: [X.31i] */
    /* JADX WARN: Type inference failed for: r1v5, types: [X.2T3] */
    public void A00() {
        AnonymousClass2P5 r0;
        AnonymousClass2T2 r1;
        AnonymousClass2P5 r02;
        C616531g r2;
        if (this instanceof AnonymousClass2T2) {
            AnonymousClass2T2 r12 = (AnonymousClass2T2) this;
            if (!r12.A00) {
                r12.A00 = true;
                r0 = (AnonymousClass2P5) r12.generatedComponent();
                r1 = r12;
            } else {
                return;
            }
        } else if (this instanceof AbstractC616731m) {
            AbstractC616731m r22 = (AbstractC616731m) this;
            if (r22 instanceof C616531g) {
                C616531g r23 = (C616531g) r22;
                if (!r23.A06) {
                    r23.A06 = true;
                    r02 = (AnonymousClass2P5) r23.generatedComponent();
                    r2 = r23;
                } else {
                    return;
                }
            } else if (!r22.A00) {
                r22.A00 = true;
                r02 = (AnonymousClass2P5) r22.generatedComponent();
                r2 = (C616631i) r22;
            } else {
                return;
            }
            AnonymousClass01J r13 = ((AnonymousClass2P6) r02).A06;
            ((AnonymousClass2T3) r2).A04 = (AnonymousClass018) r13.ANb.get();
            ((C616631i) r2).A07 = (C253218y) r13.AAF.get();
            return;
        } else if (!this.A01) {
            this.A01 = true;
            r0 = (AnonymousClass2P5) generatedComponent();
            r1 = (AnonymousClass2T3) this;
        } else {
            return;
        }
        r1.A04 = (AnonymousClass018) ((AnonymousClass2P6) r0).A06.ANb.get();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
