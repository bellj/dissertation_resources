package X;

import android.content.Context;
import android.util.Base64;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.5go  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120825go extends C120895gv {
    public final /* synthetic */ C120395g7 A00;
    public final /* synthetic */ C125895rz A01;
    public final /* synthetic */ C126365sl A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120825go(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120395g7 r11, C125895rz r12, C126365sl r13) {
        super(context, r8, r9, r10, "get-purpose-limiting-key");
        this.A00 = r11;
        this.A01 = r12;
        this.A02 = r13;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r1) {
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r1) {
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r7) {
        try {
            C129715yB r3 = new C129715yB(this.A00.A01, r7, this.A02);
            C91664Sp r4 = new C91664Sp(Base64.decode(r3.A06, 8), (int) r3.A01, r3.A02);
            IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A01.A00;
            AnonymousClass20B A00 = AnonymousClass20B.A00();
            AtomicBoolean atomicBoolean = A00.A00;
            if (!atomicBoolean.get()) {
                r4.A01 = A00.A02;
                if (!atomicBoolean.get()) {
                    r4.A00 = A00.A01;
                    ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0Q = r4;
                    return;
                }
                throw C12960it.A0U("key has been destroyed");
            }
            throw C12960it.A0U("key has been destroyed");
        } catch (AnonymousClass1V9 unused) {
            Log.e("CorruptStreamException when parsing UPIGetPurposeLimitingKeyResponseSuccess");
        }
    }
}
