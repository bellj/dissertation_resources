package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.profilo.provider.systemcounters.SystemCounterThread;

/* renamed from: X.2Zz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC52002Zz extends Handler {
    public final /* synthetic */ SystemCounterThread A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC52002Zz(Looper looper, SystemCounterThread systemCounterThread) {
        super(looper);
        this.A00 = systemCounterThread;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        SystemCounterThread systemCounterThread = this.A00;
        int i = message.what;
        int i2 = message.arg1;
        synchronized (systemCounterThread) {
            if (systemCounterThread.mEnabled) {
                if (i == 1) {
                    systemCounterThread.mSystemCounterLogger.A00();
                    systemCounterThread.logCounters();
                } else if (i == 2) {
                    systemCounterThread.logHighFrequencyThreadCounters();
                } else if (i == 3) {
                    systemCounterThread.logExpensiveCounters();
                } else {
                    throw C12970iu.A0f("Unknown message type");
                }
                systemCounterThread.mHandler.sendMessageDelayed(systemCounterThread.mHandler.obtainMessage(i, i2, 0), (long) i2);
            }
        }
    }
}
