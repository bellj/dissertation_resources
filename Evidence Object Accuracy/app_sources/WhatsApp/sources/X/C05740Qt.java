package X;

import android.view.ViewConfiguration;

/* renamed from: X.0Qt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05740Qt {
    public static float A00(ViewConfiguration viewConfiguration) {
        return viewConfiguration.getScaledHorizontalScrollFactor();
    }

    public static float A01(ViewConfiguration viewConfiguration) {
        return viewConfiguration.getScaledVerticalScrollFactor();
    }
}
