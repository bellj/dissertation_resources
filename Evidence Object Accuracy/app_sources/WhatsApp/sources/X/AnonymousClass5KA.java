package X;

import android.graphics.Bitmap;
import android.widget.ImageView;

/* renamed from: X.5KA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KA extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ C850040t this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KA(C850040t r2) {
        super(1);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        C16700pc.A0E(bitmap, 0);
        ((ImageView) this.this$0.A01.getValue()).setImageBitmap(bitmap);
        return AnonymousClass1WZ.A00;
    }
}
