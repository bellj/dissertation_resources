package X;

import com.whatsapp.util.Log;

/* renamed from: X.3WY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WY implements AbstractC32851cq {
    public final /* synthetic */ C627638m A00;

    public AnonymousClass3WY(C627638m r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        C627638m r2 = this.A00;
        r2.A00 = -2;
        Log.i(C12960it.A0d(r2.A03, C12960it.A0k("searchSupportTask/externalStorage/avail external storage not calculated, state=")));
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        this.A00.A00 = -2;
        Log.i("searchSupportTask/externalStorage/avail external storage not calculated, permission denied");
    }
}
