package X;

import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape0S0101200_I1;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.R;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;

/* renamed from: X.1vU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC42601vU implements AnonymousClass10L {
    @Override // X.AnonymousClass10L
    public void AMs() {
    }

    @Override // X.AnonymousClass10L
    public void AMt(boolean z) {
    }

    @Override // X.AnonymousClass10L
    public void AMz(long j, long j2) {
    }

    @Override // X.AnonymousClass10L
    public void AN0(long j, long j2) {
    }

    @Override // X.AnonymousClass10L
    public void AN1(long j, long j2) {
    }

    @Override // X.AnonymousClass10L
    public void AN2(long j, long j2) {
    }

    @Override // X.AnonymousClass10L
    public void AN3(long j, long j2) {
    }

    @Override // X.AnonymousClass10L
    public void AN4(int i) {
    }

    @Override // X.AnonymousClass10L
    public void AN5() {
    }

    @Override // X.AnonymousClass10L
    public void AN6(long j, long j2) {
    }

    @Override // X.AnonymousClass10L
    public void AN7() {
    }

    @Override // X.AnonymousClass10L
    public void APg() {
    }

    @Override // X.AnonymousClass10L
    public void APw(int i, Bundle bundle) {
    }

    @Override // X.AnonymousClass10L
    public void AVb() {
    }

    @Override // X.AnonymousClass10L
    public void AY3() {
    }

    @Override // X.AnonymousClass10L
    public void ALq(boolean z) {
        String str;
        String str2;
        if (this instanceof C58892rw) {
            C58892rw r2 = (C58892rw) this;
            StringBuilder sb = new StringBuilder("settings-gdrive/gdrive-backup-deletion-finished/");
            if (z) {
                str = "success";
            } else {
                str = "failed";
            }
            sb.append(str);
            Log.i(sb.toString());
            SettingsGoogleDrive settingsGoogleDrive = r2.A00;
            ((AnonymousClass022) settingsGoogleDrive.A0n.get()).A08("com.whatsapp.backup.google.google-encrypted-re-upload-worker");
            ((ActivityC13790kL) settingsGoogleDrive).A07.A0F();
            ((ActivityC13810kN) settingsGoogleDrive).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(settingsGoogleDrive, 43));
        } else if (this instanceof AnonymousClass214) {
            AnonymousClass214 r22 = (AnonymousClass214) this;
            StringBuilder sb2 = new StringBuilder("deleteacctconfirm/gdrive-observer/deletion-finished/");
            if (z) {
                str2 = "success";
            } else {
                str2 = "failed";
            }
            sb2.append(str2);
            Log.i(sb2.toString());
            r22.A00.open();
        }
    }

    @Override // X.AnonymousClass10L
    public void APx(int i, Bundle bundle) {
        if (this instanceof C42591vT) {
            C42591vT r3 = (C42591vT) this;
            ConversationsFragment conversationsFragment = r3.A04;
            if (conversationsFragment.A0c()) {
                r3.A01 = 2;
                if (i != 10) {
                    StringBuilder sb = new StringBuilder("conversations-gdrive-observer/error-during-restore/");
                    sb.append(C44771zW.A04(i));
                    Log.i(sb.toString());
                    r3.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_failed), conversationsFragment.A0I(R.string.gdrive_backup_notification_string_finished), 1, 0, false);
                    conversationsFragment.A0U.A03();
                }
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void APy(int i, Bundle bundle) {
        if (this instanceof C58902rx) {
            C58902rx r3 = (C58902rx) this;
            StringBuilder sb = new StringBuilder("gdrive-activity-observer/msgstore-download-error/");
            sb.append(C44771zW.A04(i));
            Log.i(sb.toString());
            ((ActivityC13810kN) r3.A01).A05.A0H(new RunnableBRunnable0Shape1S0201000_I1(bundle, r3, i, 2));
        }
    }

    @Override // X.AnonymousClass10L
    public void ASU() {
        ActivityC000900k A0B;
        if (this instanceof C42591vT) {
            C42591vT r4 = (C42591vT) this;
            Log.i("conversations-gdrive-observer/restore-cancelled");
            ConversationsFragment conversationsFragment = r4.A04;
            if (conversationsFragment.A0c() && (A0B = conversationsFragment.A0B()) != null && !A0B.isFinishing()) {
                conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape3S0200000_I0_3(r4, 24, A0B));
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASV(long j, long j2, boolean z) {
        ActivityC000900k A0B;
        String string;
        if (this instanceof C42591vT) {
            C42591vT r15 = (C42591vT) this;
            StringBuilder sb = new StringBuilder("conversations-gdrive-observer/restore-end ");
            sb.append(z);
            Log.i(sb.toString());
            ConversationsFragment conversationsFragment = r15.A04;
            if (conversationsFragment.A0c() && (A0B = conversationsFragment.A0B()) != null) {
                r15.A01 = 8;
                r15.A02 = -1;
                if (j > 0) {
                    string = A0B.getString(R.string.gdrive_media_restore_notification_string_finished_with_failures, C44891zj.A03(conversationsFragment.A1E, j2 - j), C44891zj.A03(conversationsFragment.A1E, j));
                } else {
                    string = A0B.getString(R.string.gdrive_media_restore_notification_string_finished_no_failures, C44891zj.A03(conversationsFragment.A1E, j2));
                }
                if (j2 > 0) {
                    r15.A00(A0B.getString(R.string.gdrive_media_restore_title_finished), string, 3, 100, false);
                } else if (j2 == 0) {
                    Log.i("conversations-gdrive-observer/restore-end/nothing-to-restore");
                    if (!A0B.isFinishing()) {
                        conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape3S0200000_I0_3(r15, 25, A0B));
                    }
                } else {
                    StringBuilder sb2 = new StringBuilder("conversations-gdrive-observer/restore-end total: ");
                    sb2.append(j2);
                    sb2.append(" failed: ");
                    sb2.append(j);
                    sb2.append(" result: ");
                    sb2.append(z);
                    Log.e(sb2.toString());
                }
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASW(long j, long j2) {
        int i;
        if (this instanceof C42591vT) {
            C42591vT r4 = (C42591vT) this;
            Log.i("conversations-gdrive-observer/restore-paused/no-data-connection");
            ConversationsFragment conversationsFragment = r4.A04;
            if (conversationsFragment.A0c()) {
                r4.A01 = 4;
                String A0I = conversationsFragment.A0I(R.string.gdrive_media_restore_notification_string_paused_for_data_connection);
                if (j2 > 0) {
                    i = (int) ((j * 100) / j2);
                } else {
                    i = -1;
                }
                r4.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_paused), A0I, 3, i, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASX(long j, long j2) {
        int i;
        if (this instanceof C42591vT) {
            C42591vT r4 = (C42591vT) this;
            Log.i("conversations-gdrive-observer/restore-paused/low-battery");
            ConversationsFragment conversationsFragment = r4.A04;
            if (conversationsFragment.A0c()) {
                r4.A01 = 5;
                String A0I = conversationsFragment.A0I(R.string.gdrive_media_restore_notification_string_paused_for_battery);
                if (j2 > 0) {
                    i = (int) ((j * 100) / j2);
                } else {
                    i = -1;
                }
                r4.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_paused), A0I, 3, i, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASY(long j, long j2) {
        int i;
        if (this instanceof C42591vT) {
            C42591vT r5 = (C42591vT) this;
            Log.i("conversations-gdrive-observer/restore-paused/sdcard-missing");
            ConversationsFragment conversationsFragment = r5.A04;
            if (conversationsFragment.A0c()) {
                r5.A01 = 7;
                if (j2 > 0) {
                    i = (int) ((j * 100) / j2);
                } else {
                    i = -1;
                }
                r5.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_paused), conversationsFragment.A0I(R.string.gdrive_restore_error_sdcard_missing_summary), 4, i, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASZ(long j, long j2) {
        int i;
        if (this instanceof C42591vT) {
            C42591vT r4 = (C42591vT) this;
            Log.i("conversations-gdrive-observer/restore-paused/sdcard-unmounted");
            ConversationsFragment conversationsFragment = r4.A04;
            if (conversationsFragment.A0c()) {
                r4.A01 = 6;
                String A0I = conversationsFragment.A0I(R.string.msg_store_backup_skipped_due_to_unmounted_sdcard_title);
                if (j2 > 0) {
                    i = (int) ((j * 100) / j2);
                } else {
                    i = -1;
                }
                r4.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_paused), A0I, 3, i, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASa(long j, long j2) {
        int i;
        if (this instanceof C42591vT) {
            C42591vT r4 = (C42591vT) this;
            Log.i("conversations-gdrive-observer/restore-paused/no-wifi");
            ConversationsFragment conversationsFragment = r4.A04;
            if (conversationsFragment.A0c()) {
                r4.A01 = 3;
                String A0I = conversationsFragment.A0I(R.string.gdrive_media_restore_notification_string_paused_for_wifi);
                if (j2 > 0) {
                    i = (int) ((j * 100) / j2);
                } else {
                    i = -1;
                }
                r4.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_paused), A0I, 3, i, false);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASb(int i) {
        if (this instanceof C42591vT) {
            C42591vT r9 = (C42591vT) this;
            ConversationsFragment conversationsFragment = r9.A04;
            if (conversationsFragment.A0c() && i > 0) {
                r9.A01 = 10;
                r9.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_running), conversationsFragment.A0J(R.string.gdrive_media_restore_notification_string_preparation_message_with_percentage_placeholder, conversationsFragment.A1E.A0K().format(((double) i) / 100.0d)), 4, i, true);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASc() {
        if (this instanceof C42591vT) {
            C42591vT r2 = (C42591vT) this;
            Log.i("conversations-gdrive-observer/restore-start");
            ConversationsFragment conversationsFragment = r2.A04;
            if (conversationsFragment.A0c()) {
                r2.A01 = 9;
                r2.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_running), conversationsFragment.A0I(R.string.gdrive_media_restore_notification_string_preparation_message), 4, -1, true);
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASd(long j, long j2, long j3) {
        if (this instanceof C42591vT) {
            C42591vT r13 = (C42591vT) this;
            ConversationsFragment conversationsFragment = r13.A04;
            if (conversationsFragment.A0c() && conversationsFragment.A0B() != null) {
                String A03 = C44891zj.A03(conversationsFragment.A1E, j);
                if (r13.A01 != 1 || !A03.equals(C44891zj.A03(conversationsFragment.A1E, r13.A02))) {
                    r13.A02 = j;
                    r13.A00(conversationsFragment.A0I(R.string.gdrive_media_restore_title_running), conversationsFragment.A0J(R.string.gdrive_media_restore_notification_string_no_failures_with_placeholder, A03, C44891zj.A03(conversationsFragment.A1E, j3), conversationsFragment.A1E.A0K().format(((double) j) / ((double) j3))), 3, (int) ((j * 100) / j3), true);
                    r13.A01 = 1;
                }
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASl(boolean z) {
        String str;
        if (this instanceof C58902rx) {
            C58902rx r3 = (C58902rx) this;
            StringBuilder sb = new StringBuilder("gdrive-activity-observer/msgstore-download-finished/");
            if (z) {
                str = "successful";
            } else {
                str = "failed";
            }
            sb.append(str);
            Log.i(sb.toString());
            RestoreFromBackupActivity restoreFromBackupActivity = r3.A01;
            int A03 = ((ActivityC13810kN) restoreFromBackupActivity).A09.A03();
            if (A03 == 10) {
                ((ActivityC13810kN) restoreFromBackupActivity).A05.A0H(new RunnableBRunnable0Shape1S0110000_I1(r3, 2, z));
                return;
            }
            StringBuilder sb2 = new StringBuilder("gdrive-activity-observer/msgstore-download-finished/get-error/");
            sb2.append(C44771zW.A04(A03));
            Log.i(sb2.toString());
        }
    }

    @Override // X.AnonymousClass10L
    public void ASm(long j, long j2) {
        if (this instanceof C58902rx) {
            C58902rx r2 = (C58902rx) this;
            int i = (int) ((100 * j) / j2);
            if (i - r2.A00 > 0) {
                r2.A00 = i;
                if (i % 10 == 0) {
                    StringBuilder sb = new StringBuilder("gdrive-activity-observer/msgstore-download-progress:");
                    sb.append(j);
                    sb.append("/");
                    sb.append(j2);
                    sb.append(" ");
                    sb.append(i);
                    sb.append("%");
                    Log.i(sb.toString());
                }
                ((ActivityC13810kN) r2.A01).A05.A0H(new RunnableBRunnable0Shape0S0101200_I1(r2, i, 2, j, j2));
            }
        }
    }

    @Override // X.AnonymousClass10L
    public void ASn() {
        if (this instanceof C58902rx) {
            C58902rx r3 = (C58902rx) this;
            ((ActivityC13810kN) r3.A01).A05.A0H(new RunnableBRunnable0Shape14S0100000_I1(r3, 17));
        }
    }
}
