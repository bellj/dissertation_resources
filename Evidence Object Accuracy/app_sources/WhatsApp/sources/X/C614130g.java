package X;

/* renamed from: X.30g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614130g extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Long A04;

    public C614130g() {
        super(3022, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A00);
        r3.Abe(4, this.A03);
        r3.Abe(5, this.A01);
        r3.Abe(2, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCtwaWelcomeMessage {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ctwaWelcomeMessageAction", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ctwaWelcomeMessageContainsIcebreakers", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ctwaWelcomeMessageError", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ctwaWelcomeMessageIcebreakersContainAutoreply", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "firstWelcomeMessageImpressionTime", this.A04);
        return C12960it.A0d("}", A0k);
    }
}
