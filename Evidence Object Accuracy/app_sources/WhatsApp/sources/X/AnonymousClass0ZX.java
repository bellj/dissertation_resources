package X;

import android.app.Activity;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.0ZX  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0ZX implements AbstractC11940h7 {
    public final AbstractC11940h7 A00;
    public final WeakHashMap A01 = new WeakHashMap();
    public final ReentrantLock A02 = new ReentrantLock();

    public AnonymousClass0ZX(AbstractC11940h7 r2) {
        this.A00 = r2;
    }

    @Override // X.AbstractC11940h7
    public void AYW(Activity activity, AnonymousClass0PZ r5) {
        C16700pc.A0E(activity, 0);
        ReentrantLock reentrantLock = this.A02;
        reentrantLock.lock();
        try {
            WeakHashMap weakHashMap = this.A01;
            if (!C16700pc.A0O(r5, (AnonymousClass0PZ) weakHashMap.get(activity))) {
                weakHashMap.put(activity, r5);
                reentrantLock.unlock();
                this.A00.AYW(activity, r5);
            }
        } finally {
            reentrantLock.unlock();
        }
    }
}
