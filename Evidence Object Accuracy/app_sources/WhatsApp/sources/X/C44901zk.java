package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.whatsapp.util.Log;

/* renamed from: X.1zk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44901zk extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass10K A00;

    public C44901zk(AnonymousClass10K r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        Log.i("gdrive-notification-manager/user-decided-to-restore-over-low-battery");
        AnonymousClass10K r4 = this.A00;
        C22730zY r3 = r4.A0G;
        r3.A08 = true;
        r3.A07(r3.A0O.A00);
        r3.A0Y.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(r3, 21));
        r4.A0J.A00.unregisterReceiver(this);
        r4.A03();
    }
}
