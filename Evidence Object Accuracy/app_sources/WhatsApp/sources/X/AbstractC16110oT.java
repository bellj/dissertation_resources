package X;

/* renamed from: X.0oT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16110oT implements Cloneable {
    public static final AnonymousClass00E DEFAULT_SAMPLING_RATE = new AnonymousClass00E(1, 20, 20);
    public static final int NOT_ALLOWED_PS_ID = -1;
    public final int channel;
    public final int code;
    public final int psIdKey;
    public final AnonymousClass00E samplingRate;

    public abstract void serialize(AnonymousClass1N6 v);

    public AbstractC16110oT(int i) {
        this(i, DEFAULT_SAMPLING_RATE, 0, -1);
    }

    public AbstractC16110oT(int i, AnonymousClass00E r2, int i2, int i3) {
        this.code = i;
        this.samplingRate = r2;
        this.channel = i2;
        this.psIdKey = i3;
    }

    public static AnonymousClass00E A00() {
        return new AnonymousClass00E(1, 1, 1);
    }

    public static void appendFieldToStringBuilder(StringBuilder sb, String str, Object obj) {
        if (obj != null) {
            sb.append(str);
            sb.append("=");
            sb.append(obj);
            sb.append(", ");
        }
    }

    @Override // java.lang.Object
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }

    public AnonymousClass00E getSamplingRate() {
        return this.samplingRate;
    }
}
