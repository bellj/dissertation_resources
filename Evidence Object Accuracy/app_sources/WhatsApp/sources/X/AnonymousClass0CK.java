package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* renamed from: X.0CK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CK extends AnonymousClass07H implements SubMenu {
    public AnonymousClass07H A00;
    public C07340Xp A01;

    public AnonymousClass0CK(Context context, AnonymousClass07H r2, C07340Xp r3) {
        super(context);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass07H
    public AnonymousClass07H A01() {
        return this.A00.A01();
    }

    @Override // X.AnonymousClass07H
    public String A03() {
        int itemId;
        C07340Xp r0 = this.A01;
        if (r0 == null || (itemId = r0.getItemId()) == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder("android:menu:actionviewstates");
        sb.append(":");
        sb.append(itemId);
        return sb.toString();
    }

    @Override // X.AnonymousClass07H
    public void A0C(AbstractC011605p r2) {
        this.A00.A0C(r2);
    }

    @Override // X.AnonymousClass07H
    public boolean A0G() {
        return this.A00.A0G();
    }

    @Override // X.AnonymousClass07H
    public boolean A0H() {
        return this.A00.A0H();
    }

    @Override // X.AnonymousClass07H
    public boolean A0I() {
        return this.A00.A0I();
    }

    @Override // X.AnonymousClass07H
    public boolean A0J(MenuItem menuItem, AnonymousClass07H r4) {
        return super.A0J(menuItem, r4) || this.A00.A0J(menuItem, r4);
    }

    @Override // X.AnonymousClass07H
    public boolean A0L(C07340Xp r2) {
        return this.A00.A0L(r2);
    }

    @Override // X.AnonymousClass07H
    public boolean A0M(C07340Xp r2) {
        return this.A00.A0M(r2);
    }

    @Override // android.view.SubMenu
    public MenuItem getItem() {
        return this.A01;
    }

    @Override // X.AnonymousClass07H, android.view.Menu
    public void setGroupDividerEnabled(boolean z) {
        this.A00.setGroupDividerEnabled(z);
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(int i) {
        if (i > 0) {
            super.A01 = AnonymousClass00T.A04(this.A0N, i);
        }
        this.A02 = null;
        A0E(false);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(Drawable drawable) {
        if (drawable != null) {
            super.A01 = drawable;
        }
        this.A02 = null;
        A0E(false);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(int i) {
        Resources resources = this.A0O;
        if (i > 0) {
            this.A05 = resources.getText(i);
        }
        this.A02 = null;
        A0E(false);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.A05 = charSequence;
        }
        this.A02 = null;
        A0E(false);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderView(View view) {
        if (view != null) {
            this.A02 = view;
            this.A05 = null;
            super.A01 = null;
        } else {
            this.A02 = null;
        }
        A0E(false);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setIcon(int i) {
        this.A01.setIcon(i);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setIcon(Drawable drawable) {
        this.A01.setIcon(drawable);
        return this;
    }

    @Override // X.AnonymousClass07H, android.view.Menu
    public void setQwertyMode(boolean z) {
        this.A00.setQwertyMode(z);
    }
}
