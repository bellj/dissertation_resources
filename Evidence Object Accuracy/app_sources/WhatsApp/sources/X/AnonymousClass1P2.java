package X;

import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.1P2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1P2 {
    public List A00;
    public final C15370n3 A01;
    public final UserJid A02;
    public final Boolean A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final boolean A07;

    public AnonymousClass1P2(C15370n3 r1, UserJid userJid, Boolean bool, String str, String str2, String str3, List list, boolean z) {
        this.A07 = z;
        this.A02 = userJid;
        this.A05 = str;
        this.A06 = str2;
        this.A04 = str3;
        this.A03 = bool;
        this.A01 = r1;
        this.A00 = list;
    }
}
