package X;

import android.app.Activity;
import android.view.View;
import com.facebook.redex.EmptyBaseViewOnClick0CListener;
import com.whatsapp.R;
import com.whatsapp.blocklist.UnblockDialogFragment;
import com.whatsapp.jid.UserJid;
import java.util.Collections;

/* renamed from: X.2jt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnClickListenerC55922jt extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public final Activity A00;
    public final AnonymousClass01F A01;
    public final C238013b A02;
    public final C15610nY A03;
    public final C15370n3 A04;
    public final C22050yP A05;

    public View$OnClickListenerC55922jt(Activity activity, AnonymousClass01F r2, C238013b r3, C15610nY r4, C15370n3 r5, C22050yP r6) {
        this.A05 = r6;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = activity;
        this.A04 = r5;
        this.A01 = r2;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        this.A05.A03(7);
        C238013b r8 = this.A02;
        C15370n3 r7 = this.A04;
        if (r8.A0I((UserJid) r7.A0B(UserJid.class))) {
            Activity activity = this.A00;
            UnblockDialogFragment.A00(new AnonymousClass543(activity, r8, (UserJid) C15370n3.A03(r7, UserJid.class)), C12960it.A0X(activity.getApplicationContext(), this.A03.A04(r7), C12970iu.A1b(), 0, R.string.unblock_to_create_group), 0, false).A1F(this.A01, null);
            return;
        }
        Activity activity2 = this.A00;
        activity2.startActivity(C14960mK.A0b(activity2.getApplicationContext(), Collections.singletonList(r7.A0B(UserJid.class).getRawString()), 7));
    }
}
