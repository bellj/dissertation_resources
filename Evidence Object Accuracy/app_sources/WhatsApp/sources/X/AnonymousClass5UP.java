package X;

import android.os.Bundle;
import androidx.core.view.inputmethod.InputContentInfoCompat;

/* renamed from: X.5UP  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5UP {
    boolean onCommitContent(InputContentInfoCompat inputContentInfoCompat, int i, Bundle bundle);
}
