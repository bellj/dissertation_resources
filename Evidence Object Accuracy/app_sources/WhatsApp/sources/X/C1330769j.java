package X;

/* renamed from: X.69j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1330769j implements AnonymousClass6MM {
    public final /* synthetic */ AnonymousClass6M0 A00;
    public final /* synthetic */ AnonymousClass605 A01;
    public final /* synthetic */ C128545wH A02;

    public C1330769j(AnonymousClass6M0 r1, AnonymousClass605 r2, C128545wH r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r2) {
        this.A00.AVD(r2);
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        AnonymousClass605 r9 = this.A01;
        C128545wH r10 = this.A02;
        AnonymousClass6M0 r8 = this.A00;
        C130775zx r2 = r9.A01;
        C121215hR r4 = new C121215hR(r9.A04.A00, r9.A02, r9.A05, r8, r9, r10);
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[1];
        int A1a = C12990iw.A1a("action", "verify-payment-pin", r3);
        r2.A02.A0F(r4, new AnonymousClass1V8(r10.A02(C130775zx.A00(Boolean.TRUE, str, "VERIFY", null, null, new Object[A1a], C117295Zj.A03(r2.A01))), "account", r3), "get", C26061Bw.A0L);
    }
}
