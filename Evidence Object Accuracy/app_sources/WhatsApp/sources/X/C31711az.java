package X;

/* renamed from: X.1az  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31711az {
    public C29191Rf A00;

    public C31711az(C31731b1 r6, byte[] bArr, int i, long j) {
        AnonymousClass1G4 A0T = C29191Rf.A06.A0T();
        A0T.A03();
        C29191Rf r1 = (C29191Rf) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = i;
        byte[] A00 = r6.A01.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        C29191Rf r12 = (C29191Rf) A0T.A00;
        r12.A00 |= 2;
        r12.A04 = A01;
        byte[] bArr2 = r6.A00.A00;
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
        A0T.A03();
        C29191Rf r13 = (C29191Rf) A0T.A00;
        r13.A00 |= 4;
        r13.A03 = A012;
        AbstractC27881Jp A013 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        C29191Rf r14 = (C29191Rf) A0T.A00;
        r14.A00 |= 8;
        r14.A05 = A013;
        A0T.A03();
        C29191Rf r15 = (C29191Rf) A0T.A00;
        r15.A00 |= 16;
        r15.A02 = j;
        this.A00 = (C29191Rf) A0T.A02();
    }

    public C31711az(byte[] bArr) {
        this.A00 = (C29191Rf) AbstractC27091Fz.A0E(C29191Rf.A06, bArr);
    }
}
