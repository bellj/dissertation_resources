package X;

import android.util.JsonWriter;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1BV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BV implements AnonymousClass1BW {
    public final C002701f A00;
    public final C16590pI A01;
    public final AnonymousClass19M A02;
    public final AnonymousClass1AB A03;
    public final Map A04 = new ConcurrentHashMap();

    public AnonymousClass1BV(C002701f r5, C16590pI r6, AnonymousClass19M r7, AnonymousClass1AB r8) {
        this.A01 = r6;
        this.A02 = r7;
        this.A03 = r8;
        this.A00 = r5;
        List<AbstractC470728v> A00 = AnonymousClass3GW.A00();
        for (AbstractC470728v r2 : A00) {
            this.A04.put(r2.AH5(), r2);
        }
    }

    @Override // X.AnonymousClass1BW
    public AbstractC38871oq A8G(Object obj, float f) {
        return new AnonymousClass577((AbstractC470728v) obj, Float.valueOf(f));
    }

    @Override // X.AnonymousClass1BW
    public /* bridge */ /* synthetic */ Object ADE(String str) {
        return this.A04.get(str);
    }

    @Override // X.AnonymousClass1BW
    public String ADj(Object obj) {
        return ((AbstractC470728v) obj).AH5();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:51:0x010d */
    /* JADX DEBUG: Multi-variable search result rejected for r8v3, resolved type: X.3YL */
    /* JADX DEBUG: Multi-variable search result rejected for r8v4, resolved type: X.3YL */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v2 */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AnonymousClass1BW
    public java.util.List AIX() {
        /*
        // Method dump skipped, instructions count: 365
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BV.AIX():java.util.List");
    }

    @Override // X.AnonymousClass1BW
    public void AZH(List list) {
        try {
            JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(new FileOutputStream(new File(this.A01.A00.getFilesDir(), "content_stickers")), AnonymousClass01V.A08));
            jsonWriter.setIndent("");
            jsonWriter.beginArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass577 r3 = (AnonymousClass577) it.next();
                jsonWriter.beginObject();
                jsonWriter.name("tag").value(r3.A01.AH5());
                jsonWriter.name("weight").value((double) r3.A00);
                jsonWriter.endObject();
            }
            jsonWriter.endArray();
            jsonWriter.close();
        } catch (IOException e) {
            Log.e(e);
        }
    }
}
