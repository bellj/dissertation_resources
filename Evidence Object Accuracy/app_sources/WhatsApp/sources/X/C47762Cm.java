package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.Arrays;
import org.chromium.net.UrlRequest;

/* renamed from: X.2Cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47762Cm {
    public static final EnumC87104Af A02 = EnumC87104Af.AUTO;
    public int A00 = 0;
    public float[] A01 = new float[0];

    public static void A00(C47762Cm r3, int i) {
        int i2 = r3.A00 + i;
        float[] fArr = r3.A01;
        int length = fArr.length;
        if (i2 > length) {
            int i3 = length << 1;
            if (i3 < i2) {
                i3 += i2 - i3;
            }
            r3.A01 = Arrays.copyOf(fArr, i3);
        }
    }

    public static boolean A01(float f) {
        return Float.compare(f, Float.NaN) == 0;
    }

    public String toString() {
        String str;
        AnonymousClass39w A00;
        float f;
        AnonymousClass39w A002;
        float f2;
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < this.A00) {
            EnumC868349a[] values = EnumC868349a.values();
            float[] fArr = this.A01;
            String str2 = "  flexBasis: ";
            switch (values[(int) fArr[i]].ordinal()) {
                case 0:
                    str2 = "  flex: ";
                    sb.append(str2);
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 1:
                    str2 = "  flexGrow: ";
                    sb.append(str2);
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 2:
                    str2 = "  flexShrink: ";
                    sb.append(str2);
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 3:
                    sb.append(str2);
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 4:
                    sb.append(str2);
                    sb.append(fArr[i + 1]);
                    sb.append("%\n");
                    i += 2;
                    break;
                case 6:
                    sb.append("  width: ");
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 7:
                    sb.append("  width: ");
                    sb.append(fArr[i + 1]);
                    sb.append("%\n");
                    i += 2;
                    break;
                case 9:
                    sb.append("  minWidth: ");
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 10:
                    sb.append("  minWidth: ");
                    sb.append(fArr[i + 1]);
                    sb.append("%\n");
                    i += 2;
                    break;
                case 11:
                    sb.append("  maxWidth: ");
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 12:
                    sb.append("  maxWidth: ");
                    sb.append(fArr[i + 1]);
                    sb.append("%\n");
                    i += 2;
                    break;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    sb.append("  height: ");
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    sb.append("  height: ");
                    sb.append(fArr[i + 1]);
                    sb.append("%\n");
                    i += 2;
                    break;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    sb.append("  minHeight: ");
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 17:
                    sb.append("  minHeight: ");
                    sb.append(fArr[i + 1]);
                    sb.append("%\n");
                    i += 2;
                    break;
                case 18:
                    sb.append("  maxHeight: ");
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 19:
                    sb.append("  maxHeight: ");
                    sb.append(fArr[i + 1]);
                    sb.append("%\n");
                    i += 2;
                    break;
                case C43951xu.A01:
                    EnumC87104Af r1 = EnumC87104Af.values()[(int) this.A01[i + 1]];
                    sb.append("  alignSelf: ");
                    sb.append(r1);
                    sb.append("\n");
                    i += 2;
                    break;
                case 21:
                    EnumC87144Aj r12 = EnumC87144Aj.values()[(int) this.A01[i + 1]];
                    sb.append("  positionType: ");
                    sb.append(r12);
                    sb.append("\n");
                    i += 2;
                    break;
                case 22:
                    str2 = "  aspectRatio: ";
                    sb.append(str2);
                    sb.append(fArr[i + 1]);
                    sb.append("\n");
                    i += 2;
                    break;
                case 23:
                    EnumC87124Ah r13 = EnumC87124Ah.values()[(int) this.A01[i + 1]];
                    sb.append("  display: ");
                    sb.append(r13);
                    sb.append("\n");
                    i += 2;
                    break;
                case 24:
                    A002 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    f2 = fArr[i + 2];
                    sb.append("  margin");
                    sb.append(A002);
                    sb.append(": ");
                    sb.append(f2);
                    sb.append("\n");
                    i += 3;
                    break;
                case 25:
                    A00 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    f = fArr[i + 2];
                    sb.append("  margin");
                    sb.append(A00);
                    sb.append(": ");
                    sb.append(f);
                    sb.append("%\n");
                    i += 3;
                    break;
                case 26:
                    AnonymousClass39w A003 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    sb.append("  margin");
                    sb.append(A003);
                    sb.append(": ");
                    sb.append("auto\n");
                    i += 2;
                    break;
                case 27:
                    A002 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    f2 = fArr[i + 2];
                    sb.append("  position");
                    sb.append(A002);
                    sb.append(": ");
                    sb.append(f2);
                    sb.append("\n");
                    i += 3;
                    break;
                case 28:
                    A00 = AnonymousClass39w.A00((int) fArr[i + 1]);
                    f = fArr[i + 2];
                    sb.append("  position");
                    sb.append(A00);
                    sb.append(": ");
                    sb.append(f);
                    sb.append("%\n");
                    i += 3;
                    break;
                case 29:
                    str = "  hasMeasureFunction: true\n";
                    sb.append(str);
                    i++;
                    break;
                case C25991Bp.A0S:
                    str = "  hasBaselineFunction: true\n";
                    sb.append(str);
                    i++;
                    break;
                case 31:
                    str = "  enableTextRounding: true\n";
                    sb.append(str);
                    i++;
                    break;
            }
        }
        if (sb.length() <= 0) {
            return "";
        }
        StringBuilder sb2 = new StringBuilder("{\n");
        sb2.append(sb.toString());
        sb2.append("}");
        return sb2.toString();
    }
}
