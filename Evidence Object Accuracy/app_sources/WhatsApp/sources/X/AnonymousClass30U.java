package X;

/* renamed from: X.30U  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30U extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;

    public AnonymousClass30U() {
        super(3298, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamUiRevokeAction {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageAction", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "uiRevokeActionDuration", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "uiRevokeActionSessionId", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
