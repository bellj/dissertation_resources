package X;

import android.graphics.Bitmap;
import java.io.File;
import java.lang.ref.Reference;

/* renamed from: X.5zz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130795zz {
    public static final String A03;
    public final C006202y A00 = new C006202y(50);
    public final C16590pI A01;
    public final AbstractC14440lR A02;

    static {
        StringBuilder A0k = C12960it.A0k("downloadable");
        String str = File.separator;
        A0k.append(str);
        A0k.append("bloks_pay");
        A0k.append(str);
        A03 = C12960it.A0d("image", A0k);
    }

    public C130795zz(C16590pI r3, AbstractC14440lR r4) {
        this.A01 = r3;
        this.A02 = r4;
    }

    public void A00(AnonymousClass6MK r5, String str) {
        Bitmap bitmap;
        C006202y r3 = this.A00;
        Reference reference = (Reference) r3.A04(str);
        if (reference == null || (bitmap = (Bitmap) reference.get()) == null) {
            C12960it.A1E(new C124145og(r3, r5, this.A01, str), this.A02);
        } else {
            r5.AWv(bitmap);
        }
    }
}
