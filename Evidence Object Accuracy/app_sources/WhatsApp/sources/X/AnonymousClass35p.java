package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.35p  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35p extends AbstractC69213Yj {
    public View A00;
    public List A01;
    public final int A02;
    public final AnonymousClass1AB A03;
    public final AbstractC116245Ur A04;

    public AnonymousClass35p(Context context, LayoutInflater layoutInflater, C14850m9 r3, AnonymousClass1AB r4, AbstractC116245Ur r5, int i, int i2) {
        super(context, layoutInflater, r3, i2);
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = i;
    }

    @Override // X.AbstractC69213Yj
    public void A03(View view) {
        this.A00 = view.findViewById(R.id.empty);
    }

    @Override // X.AbstractC69213Yj, X.AnonymousClass5WC
    public void AP2(View view, ViewGroup viewGroup, int i) {
        super.AP2(view, viewGroup, i);
        this.A00 = null;
    }
}
