package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.3Vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68363Vc implements AnonymousClass5TW {
    public final Context A00;
    public final ImageView A01;
    public final TextEmojiLabel A02;
    public final C28801Pb A03;
    public final AnonymousClass1J1 A04;
    public final AnonymousClass018 A05;
    public final C14850m9 A06;

    public C68363Vc(Context context, View view, C15610nY r5, AnonymousClass1J1 r6, AnonymousClass018 r7, C14850m9 r8, AnonymousClass12F r9) {
        this.A00 = context;
        this.A06 = r8;
        this.A05 = r7;
        this.A04 = r6;
        this.A01 = C12970iu.A0L(view, R.id.contactpicker_row_photo);
        C28801Pb r0 = new C28801Pb(view, r5, r9, (int) R.id.contactpicker_row_name);
        this.A03 = r0;
        C27531Hw.A06(r0.A01);
        this.A02 = C12970iu.A0U(view, R.id.contactpicker_row_status);
    }

    @Override // X.AnonymousClass5TW
    public void ANG(AnonymousClass5TX r6) {
        TextEmojiLabel textEmojiLabel;
        C15370n3 r4 = ((AnonymousClass540) r6).A00;
        ImageView imageView = this.A01;
        AnonymousClass028.A0k(imageView, C15380n4.A03(r4.A0D));
        AbstractView$OnClickListenerC34281fs.A02(imageView, this, r4, 16);
        this.A04.A06(imageView, r4);
        C28801Pb r3 = this.A03;
        r3.A06(r4);
        String A0G = this.A05.A0G(C248917h.A01(r4));
        if (C12980iv.A0q(r3.A01).equals(A0G) || C41861uH.A00(this.A06, r4.A0D)) {
            textEmojiLabel = this.A02;
            textEmojiLabel.setVisibility(8);
            A0G = "";
        } else {
            textEmojiLabel = this.A02;
            textEmojiLabel.setVisibility(0);
        }
        textEmojiLabel.setText(A0G);
    }
}
