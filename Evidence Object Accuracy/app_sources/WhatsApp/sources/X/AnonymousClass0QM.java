package X;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.0QM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QM {
    public C02560Cw A00;
    public C02560Cw A01;
    public AnonymousClass0O5 A02 = new AnonymousClass0O5();
    public AbstractC11710gj A03 = null;
    public ArrayList A04 = new ArrayList();
    public ArrayList A05 = new ArrayList();
    public ArrayList A06 = new ArrayList();
    public boolean A07 = true;
    public boolean A08 = true;

    public AnonymousClass0QM(C02560Cw r2) {
        this.A00 = r2;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        if (r0 == false) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        if (r23 != 0) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003e, code lost:
        r0 = r22.A0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0040, code lost:
        r1 = r0.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0042, code lost:
        if (r23 != 0) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0044, code lost:
        r0 = r22.A0c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0046, code lost:
        r8 = r0.A04;
        r14 = r11.A05;
        r1 = r14.A08.contains(r1);
        r2 = r11.A04;
        r0 = r2.A08.contains(r8);
        r17 = r11.A05();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005c, code lost:
        if (r1 == false) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005e, code lost:
        if (r0 == false) goto L_0x00d4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0060, code lost:
        r8 = r3.A01(r14, 0);
        r15 = r3.A00(r2, 0);
        r8 = r8 - r17;
        r10 = r2.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0070, code lost:
        if (r8 < ((long) (-r10))) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0072, code lost:
        r8 = r8 + ((long) r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0074, code lost:
        r0 = (long) r14.A00;
        r2 = ((-r15) - r17) - r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007d, code lost:
        if (r2 < r0) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007f, code lost:
        r2 = r2 - r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0080, code lost:
        r14 = r11.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0082, code lost:
        if (r23 != 0) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0084, code lost:
        r11 = r14.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008b, code lost:
        if (r11 <= 0.0f) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008d, code lost:
        r4 = (long) ((((float) r2) / r11) + (((float) r8) / (1.0f - r11)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0095, code lost:
        r8 = (float) r4;
        r0 = r0 + ((((long) ((r8 * r11) + 0.5f)) + r17) + ((long) ((r8 * (1.0f - r11)) + 0.5f)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a4, code lost:
        r0 = r0 - ((long) r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a8, code lost:
        if (r23 != 1) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00aa, code lost:
        r11 = r14.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ad, code lost:
        r11 = -1.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b0, code lost:
        if (r0 == false) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b2, code lost:
        r4 = r3.A00(r2, (long) r2.A00);
        r0 = ((long) (-r2.A00)) + r17;
        r2 = -r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c1, code lost:
        r0 = ((long) r14.A00) + r11.A05();
        r10 = r2.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00cc, code lost:
        r0 = r22.A0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00d0, code lost:
        r0 = r22.A0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d4, code lost:
        r2 = r3.A01(r14, (long) r14.A00);
        r0 = ((long) r14.A00) + r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00e0, code lost:
        r0 = java.lang.Math.max(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
        if (r11.A01 != r23) goto L_0x0027;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        r0 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A00(X.C02560Cw r22, int r23) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QM.A00(X.0Cw, int):int");
    }

    public void A01() {
        ArrayList arrayList = this.A05;
        arrayList.clear();
        C02560Cw r3 = this.A01;
        AnonymousClass0D3 r1 = r3.A0c;
        r1.A08();
        AnonymousClass0D2 r0 = r3.A0d;
        r0.A08();
        arrayList.add(r1);
        arrayList.add(r0);
        Iterator it = ((AbstractC02530Cr) r3).A00.iterator();
        HashSet hashSet = null;
        while (it.hasNext()) {
            AnonymousClass0QV r4 = (AnonymousClass0QV) it.next();
            if (r4 instanceof C02540Cs) {
                arrayList.add(new C02590Cz(r4));
            } else {
                if (r4.A0E()) {
                    AnonymousClass0D1 r12 = r4.A0a;
                    if (r12 == null) {
                        r12 = new AnonymousClass0D1(r4, 0);
                        r4.A0a = r12;
                    }
                    if (hashSet == null) {
                        hashSet = new HashSet();
                    }
                    hashSet.add(r12);
                } else {
                    arrayList.add(r4.A0c);
                }
                if (r4.A0F()) {
                    AnonymousClass0D1 r13 = r4.A0b;
                    if (r13 == null) {
                        r13 = new AnonymousClass0D1(r4, 1);
                        r4.A0b = r13;
                    }
                    if (hashSet == null) {
                        hashSet = new HashSet();
                    }
                    hashSet.add(r13);
                } else {
                    arrayList.add(r4.A0d);
                }
                if (r4 instanceof C02550Cv) {
                    arrayList.add(new AnonymousClass0D0(r4));
                }
            }
        }
        if (hashSet != null) {
            arrayList.addAll(hashSet);
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            ((AbstractC07280Xj) it2.next()).A08();
        }
        Iterator it3 = arrayList.iterator();
        while (it3.hasNext()) {
            AbstractC07280Xj r14 = (AbstractC07280Xj) it3.next();
            if (r14.A03 != r3) {
                r14.A06();
            }
        }
        ArrayList arrayList2 = this.A04;
        arrayList2.clear();
        AnonymousClass0PH.A03 = 0;
        C02560Cw r15 = this.A00;
        A06(r15.A0c, arrayList2, 0);
        A06(r15.A0d, arrayList2, 1);
        this.A07 = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x005c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x000a A[ADDED_TO_REGION, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r21 = this;
            r11 = r21
            X.0Cw r0 = r11.A00
            java.util.ArrayList r0 = r0.A00
            java.util.Iterator r10 = r0.iterator()
        L_0x000a:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x00a9
            java.lang.Object r14 = r10.next()
            X.0QV r14 = (X.AnonymousClass0QV) r14
            boolean r0 = r14.A0i
            if (r0 != 0) goto L_0x000a
            X.0JR[] r0 = r14.A0o
            r9 = 0
            r7 = r0[r9]
            r3 = 1
            r6 = r0[r3]
            int r2 = r14.A0D
            int r1 = r14.A0C
            X.0JR r13 = X.AnonymousClass0JR.WRAP_CONTENT
            if (r7 == r13) goto L_0x0030
            X.0JR r0 = X.AnonymousClass0JR.MATCH_CONSTRAINT
            if (r7 != r0) goto L_0x00a7
            if (r2 != r3) goto L_0x00a7
        L_0x0030:
            r8 = 1
        L_0x0031:
            if (r6 == r13) goto L_0x0039
            X.0JR r0 = X.AnonymousClass0JR.MATCH_CONSTRAINT
            if (r6 != r0) goto L_0x003a
            if (r1 != r3) goto L_0x003a
        L_0x0039:
            r9 = 1
        L_0x003a:
            X.0D3 r0 = r14.A0c
            X.0Cy r5 = r0.A06
            boolean r1 = r5.A0B
            X.0D2 r2 = r14.A0d
            X.0Cy r4 = r2.A06
            boolean r0 = r4.A0B
            if (r1 == 0) goto L_0x0082
            if (r0 == 0) goto L_0x0066
            X.0JR r12 = X.AnonymousClass0JR.FIXED
            int r15 = r5.A02
            int r0 = r4.A02
            r13 = r12
            r16 = r0
            r11.A03(r12, r13, r14, r15, r16)
        L_0x0056:
            r14.A0i = r3
        L_0x0058:
            boolean r0 = r14.A0i
            if (r0 == 0) goto L_0x000a
            X.0Cy r1 = r2.A01
            if (r1 == 0) goto L_0x000a
            int r0 = r14.A07
            r1.A01(r0)
            goto L_0x000a
        L_0x0066:
            if (r9 == 0) goto L_0x0058
            X.0JR r12 = X.AnonymousClass0JR.FIXED
            int r15 = r5.A02
            int r0 = r4.A02
            r16 = r0
            r11.A03(r12, r13, r14, r15, r16)
            X.0JR r1 = X.AnonymousClass0JR.MATCH_CONSTRAINT
            int r0 = r14.A00()
            if (r6 != r1) goto L_0x007e
            r4.A00 = r0
            goto L_0x0058
        L_0x007e:
            r4.A01(r0)
            goto L_0x0056
        L_0x0082:
            if (r0 == 0) goto L_0x0058
            if (r8 == 0) goto L_0x0058
            int r1 = r5.A02
            X.0JR r17 = X.AnonymousClass0JR.FIXED
            int r0 = r4.A02
            r15 = r11
            r16 = r13
            r18 = r14
            r19 = r1
            r20 = r0
            r15.A03(r16, r17, r18, r19, r20)
            X.0JR r1 = X.AnonymousClass0JR.MATCH_CONSTRAINT
            int r0 = r14.A01()
            if (r7 != r1) goto L_0x00a3
            r5.A00 = r0
            goto L_0x0058
        L_0x00a3:
            r5.A01(r0)
            goto L_0x0056
        L_0x00a7:
            r8 = 0
            goto L_0x0031
        L_0x00a9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QM.A02():void");
    }

    public final void A03(AnonymousClass0JR r3, AnonymousClass0JR r4, AnonymousClass0QV r5, int i, int i2) {
        AnonymousClass0O5 r1 = this.A02;
        r1.A05 = r3;
        r1.A06 = r4;
        r1.A00 = i;
        r1.A04 = i2;
        this.A03.ALU(r5, r1);
        r5.A06(r1.A03);
        r5.A05(r1.A02);
        r5.A0h = r1.A07;
        int i3 = r1.A01;
        r5.A07 = i3;
        boolean z = false;
        if (i3 > 0) {
            z = true;
        }
        r5.A0h = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0235, code lost:
        if (r1.A0C == 0) goto L_0x0225;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04(X.C02560Cw r28) {
        /*
        // Method dump skipped, instructions count: 569
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QM.A04(X.0Cw):void");
    }

    public final void A05(C07270Xi r13, C07270Xi r14, AnonymousClass0PH r15, ArrayList arrayList, int i) {
        AnonymousClass0PH r9 = r15;
        AbstractC07280Xj r2 = r13.A06;
        if (r2.A07 == null) {
            C02560Cw r1 = this.A00;
            if (!(r2 == r1.A0c || r2 == r1.A0d)) {
                if (r15 == null) {
                    r9 = new AnonymousClass0PH(r2);
                    arrayList.add(r9);
                }
                r2.A07 = r9;
                r9.A02.add(r2);
                r9.A01 = r2;
                C07270Xi r5 = r2.A05;
                for (AbstractC11720gk r7 : r5.A07) {
                    if (r7 instanceof C07270Xi) {
                        A05((C07270Xi) r7, r14, r9, arrayList, i);
                    }
                }
                C07270Xi r4 = r2.A04;
                for (AbstractC11720gk r72 : r4.A07) {
                    if (r72 instanceof C07270Xi) {
                        A05((C07270Xi) r72, r14, r9, arrayList, i);
                    }
                }
                if (i == 1 && (r2 instanceof AnonymousClass0D2)) {
                    for (AbstractC11720gk r73 : ((AnonymousClass0D2) r2).A00.A07) {
                        if (r73 instanceof C07270Xi) {
                            A05((C07270Xi) r73, r14, r9, arrayList, i);
                        }
                    }
                }
                for (C07270Xi r74 : r5.A08) {
                    A05(r74, r14, r9, arrayList, i);
                }
                for (C07270Xi r75 : r4.A08) {
                    A05(r75, r14, r9, arrayList, i);
                }
                if (i == 1 && (r2 instanceof AnonymousClass0D2)) {
                    for (C07270Xi r76 : ((AnonymousClass0D2) r2).A00.A08) {
                        A05(r76, r14, r9, arrayList, i);
                    }
                }
            }
        }
    }

    public final void A06(AbstractC07280Xj r15, ArrayList arrayList, int i) {
        C07270Xi r9;
        C07270Xi r3;
        C07270Xi r10 = r15.A05;
        for (AbstractC11720gk r32 : r10.A07) {
            if (r32 instanceof C07270Xi) {
                r3 = (C07270Xi) r32;
            } else if (r32 instanceof AbstractC07280Xj) {
                r3 = ((AbstractC07280Xj) r32).A05;
            }
            A05(r3, r15.A04, null, arrayList, i);
        }
        for (AbstractC11720gk r92 : r15.A04.A07) {
            if (r92 instanceof C07270Xi) {
                r9 = (C07270Xi) r92;
            } else if (r92 instanceof AbstractC07280Xj) {
                r9 = ((AbstractC07280Xj) r92).A04;
            }
            A05(r9, r10, null, arrayList, i);
        }
        if (i == 1) {
            for (AbstractC11720gk r33 : ((AnonymousClass0D2) r15).A00.A07) {
                if (r33 instanceof C07270Xi) {
                    A05((C07270Xi) r33, null, null, arrayList, i);
                }
            }
        }
    }
}
