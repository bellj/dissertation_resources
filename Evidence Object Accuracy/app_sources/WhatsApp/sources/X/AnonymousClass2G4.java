package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;

/* renamed from: X.2G4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2G4 extends AnimatorListenerAdapter {
    public final /* synthetic */ int A00;
    public final /* synthetic */ TextStatusComposerActivity A01;

    public AnonymousClass2G4(TextStatusComposerActivity textStatusComposerActivity, int i) {
        this.A01 = textStatusComposerActivity;
        this.A00 = i;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        TextStatusComposerActivity textStatusComposerActivity = this.A01;
        textStatusComposerActivity.A04.clearAnimation();
        ViewGroup viewGroup = textStatusComposerActivity.A04;
        int i = this.A00;
        viewGroup.setVisibility(i);
        if (i == 0) {
            textStatusComposerActivity.A07.post(new RunnableBRunnable0Shape12S0100000_I0_12(this, 43));
        }
    }
}
