package X;

import android.content.SharedPreferences;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.15R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15R {
    public final C17170qN A00;
    public final C14820m6 A01;
    public final AtomicLong A02 = new AtomicLong(-1);

    public AnonymousClass15R(C17170qN r4, C14820m6 r5) {
        this.A01 = r5;
        this.A00 = r4;
    }

    public long A00() {
        long mostSignificantBits;
        AtomicLong atomicLong = this.A02;
        long j = atomicLong.get();
        if (j == -1) {
            SharedPreferences sharedPreferences = this.A01.A00;
            j = sharedPreferences.getLong("qpl_id", -1);
            if (j != -1) {
                atomicLong.set(j);
            } else {
                synchronized (AnonymousClass15R.class) {
                    if (atomicLong.get() != -1) {
                        mostSignificantBits = atomicLong.get();
                    } else {
                        mostSignificantBits = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
                        atomicLong.set(mostSignificantBits);
                        sharedPreferences.edit().putLong("qpl_id", mostSignificantBits).apply();
                    }
                }
                return mostSignificantBits;
            }
        }
        return j;
    }
}
