package X;

import android.os.IBinder;
import com.google.android.gms.maps.internal.IMapViewDelegate;

/* renamed from: X.3rF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79813rF extends C65873Li implements IMapViewDelegate {
    public C79813rF(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IMapViewDelegate");
    }
}
