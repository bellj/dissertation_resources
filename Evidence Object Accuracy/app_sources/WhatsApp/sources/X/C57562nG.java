package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57562nG extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57562nG A08;
    public static volatile AnonymousClass255 A09;
    public int A00;
    public int A01 = 1;
    public int A02;
    public int A03 = 1;
    public int A04;
    public int A05;
    public int A06;
    public int A07;

    static {
        C57562nG r0 = new C57562nG();
        A08 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        int A02;
        switch (r7.ordinal()) {
            case 0:
                return A08;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57562nG r9 = (C57562nG) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A03;
                int i3 = r9.A00;
                this.A03 = r8.Afp(i2, r9.A03, A1R, C12960it.A1R(i3));
                this.A07 = r8.Afp(this.A07, r9.A07, C12960it.A1V(i & 2, 2), C12960it.A1V(i3 & 2, 2));
                this.A06 = r8.Afp(this.A06, r9.A06, C12960it.A1V(i & 4, 4), C12960it.A1V(i3 & 4, 4));
                this.A02 = r8.Afp(this.A02, r9.A02, C12960it.A1V(i & 8, 8), C12960it.A1V(i3 & 8, 8));
                this.A04 = r8.Afp(this.A04, r9.A04, C12960it.A1V(i & 16, 16), C12960it.A1V(i3 & 16, 16));
                this.A05 = r8.Afp(this.A05, r9.A05, C12960it.A1V(i & 32, 32), C12960it.A1V(i3 & 32, 32));
                this.A01 = r8.Afp(this.A01, r9.A01, C12960it.A1V(i & 64, 64), C12960it.A1V(i3 & 64, 64));
                if (r8 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            int i4 = 1;
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                A02 = r82.A02();
                                if (EnumC87314Ba.A00(A02) != null) {
                                    this.A00 = 1 | this.A00;
                                    this.A03 = A02;
                                } else {
                                    super.A0X(i4, A02);
                                }
                            } else if (A03 == 16) {
                                this.A00 |= 2;
                                this.A07 = r82.A02();
                            } else if (A03 == 24) {
                                this.A00 |= 4;
                                this.A06 = r82.A02();
                            } else if (A03 == 32) {
                                this.A00 |= 8;
                                this.A02 = r82.A02();
                            } else if (A03 == 40) {
                                this.A00 |= 16;
                                this.A04 = r82.A02();
                            } else if (A03 == 48) {
                                this.A00 |= 32;
                                this.A05 = r82.A02();
                            } else if (A03 == 56) {
                                A02 = r82.A02();
                                if (A02 == 1 || A02 == 2) {
                                    this.A00 |= 64;
                                    this.A01 = A02;
                                } else {
                                    i4 = 7;
                                    super.A0X(i4, A02);
                                }
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C57562nG();
            case 5:
                return new C81933uo();
            case 6:
                break;
            case 7:
                if (A09 == null) {
                    synchronized (C57562nG.class) {
                        if (A09 == null) {
                            A09 = AbstractC27091Fz.A09(A08);
                        }
                    }
                }
                return A09;
            default:
                throw C12970iu.A0z();
        }
        return A08;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A03(1, this.A03, 0);
        }
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A02(2, this.A07, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A02(3, this.A06, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A02(4, this.A02, i2);
        }
        if ((i3 & 16) == 16) {
            i2 = AbstractC27091Fz.A02(5, this.A04, i2);
        }
        if ((i3 & 32) == 32) {
            i2 = AbstractC27091Fz.A02(6, this.A05, i2);
        }
        if ((i3 & 64) == 64) {
            i2 = AbstractC27091Fz.A03(7, this.A01, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A03);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(2, this.A07);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A06);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A02);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0F(5, this.A04);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0F(6, this.A05);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0E(7, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
