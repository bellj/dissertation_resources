package X;

/* renamed from: X.29A  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass29A extends AbstractC17970rh {
    public AnonymousClass29A() {
        this(4);
    }

    public AnonymousClass29A(int i) {
        super(4);
    }

    @Override // X.AbstractC17970rh, X.AbstractC17980ri
    public AnonymousClass29A add(Object obj) {
        super.add(obj);
        return this;
    }

    public AnonymousClass1Mr build() {
        this.forceCopy = true;
        return AnonymousClass1Mr.asImmutableList(this.contents, this.size);
    }
}
