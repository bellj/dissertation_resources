package X;

import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.jid.UserJid;
import java.io.File;
import java.util.ArrayList;

/* renamed from: X.3C6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C6 {
    public byte A00;
    public int A01;
    public int A02;
    public Uri A03;
    public Bundle A04;
    public C44691zO A05;
    public C32701cb A06 = new C32701cb(null, null, false, false, false, false, false, false);
    public C15370n3 A07;
    public AbstractC14640lm A08;
    public C15580nU A09;
    public UserJid A0A;
    public File A0B;
    public Long A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public ArrayList A0Q;
    public ArrayList A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
}
