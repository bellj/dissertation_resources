package X;

/* renamed from: X.5Mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114605Mj extends AnonymousClass1TM {
    public AnonymousClass5NV A00;

    public C114605Mj(AnonymousClass5NV r1) {
        this.A00 = r1;
    }

    public static C114605Mj A00(Object obj) {
        if (obj instanceof C114605Mj) {
            return (C114605Mj) obj;
        }
        if (obj != null) {
            return new C114605Mj(AnonymousClass5NV.A00(obj));
        }
        return null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    public AnonymousClass5MW A03() {
        AnonymousClass1TN[] r1 = this.A00.A01;
        if (r1.length == 0) {
            return null;
        }
        return AnonymousClass5MW.A00(r1[0]);
    }

    public AnonymousClass5MW[] A04() {
        AnonymousClass1TN[] r4 = this.A00.A01;
        int length = r4.length;
        AnonymousClass5MW[] r2 = new AnonymousClass5MW[length];
        for (int i = 0; i != length; i++) {
            r2[i] = AnonymousClass5MW.A00(r4[i]);
        }
        return r2;
    }
}
