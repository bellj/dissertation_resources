package X;

import android.app.Activity;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;

/* renamed from: X.2KE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2KE implements AbstractC32851cq {
    public final /* synthetic */ AnonymousClass2F9 A00;

    public AnonymousClass2KE(AnonymousClass2F9 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        throw new IllegalStateException("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        throw new IllegalStateException("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        C36021jC.A01(this.A00.A01, 107);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        Activity activity = this.A00.A01;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_restore_from_backup_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_restore_from_backup;
        }
        RequestPermissionActivity.A0L(activity, R.string.permission_storage_need_write_access_on_restore_from_backup_request, i2, 200);
    }
}
