package X;

import android.content.res.Configuration;
import android.view.ContextThemeWrapper;

/* renamed from: X.0KB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KB {
    public static void A00(Configuration configuration, ContextThemeWrapper contextThemeWrapper) {
        contextThemeWrapper.applyOverrideConfiguration(configuration);
    }
}
