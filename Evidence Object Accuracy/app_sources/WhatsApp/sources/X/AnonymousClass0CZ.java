package X;

import android.view.View;

/* renamed from: X.0CZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CZ extends AnonymousClass0WW {
    public final /* synthetic */ AnonymousClass0CR A00;
    public final /* synthetic */ AnonymousClass0XQ A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CZ(View view, AnonymousClass0CR r2, AnonymousClass0XQ r3) {
        super(view);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass0WW
    public AbstractC12600iB A00() {
        AnonymousClass0CP r0 = this.A00.A00.A0H;
        if (r0 == null) {
            return null;
        }
        return r0.A00();
    }

    @Override // X.AnonymousClass0WW
    public boolean A02() {
        AnonymousClass0XQ r1 = this.A00.A00;
        if (r1.A0F != null) {
            return false;
        }
        r1.A01();
        return true;
    }

    @Override // X.AnonymousClass0WW
    public boolean A03() {
        this.A00.A00.A03();
        return true;
    }
}
