package X;

/* renamed from: X.1fA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33841fA extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Integer A08;
    public Long A09;
    public Long A0A;
    public Long A0B;

    public C33841fA() {
        super(1728, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(21, this.A05);
        r3.Abe(18, this.A09);
        r3.Abe(22, this.A00);
        r3.Abe(14, this.A01);
        r3.Abe(9, this.A02);
        r3.Abe(2, this.A06);
        r3.Abe(1, this.A07);
        r3.Abe(20, this.A0A);
        r3.Abe(19, this.A0B);
        r3.Abe(23, this.A08);
        r3.Abe(16, this.A03);
        r3.Abe(17, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        StringBuilder sb = new StringBuilder("WamForwardSend {");
        Integer num = this.A05;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "disappearingChatInitiator", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ephemeralityDuration", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isForwardedForward", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isFrequentlyForwarded", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsInternational", this.A02);
        Integer num2 = this.A06;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageMediaType", obj2);
        Integer num3 = this.A07;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "receiverDefaultDisappearingDuration", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "senderDefaultDisappearingDuration", this.A0B);
        Integer num4 = this.A08;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "typeOfGroup", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wouldBeFrequentlyForwardedAt3", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "wouldBeFrequentlyForwardedAt4", this.A04);
        sb.append("}");
        return sb.toString();
    }
}
