package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.2VB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VB implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99604kZ();
    public final double A00;
    public final double A01;
    public final int A02;
    public final int A03;
    public final Integer A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass2VB(Parcel parcel) {
        this.A08 = parcel.readString();
        this.A05 = parcel.readString();
        this.A04 = Integer.valueOf(parcel.readInt());
        this.A01 = parcel.readDouble();
        this.A03 = parcel.readInt();
        this.A07 = parcel.readString();
        this.A00 = parcel.readDouble();
        this.A06 = parcel.readString();
        this.A02 = parcel.readInt();
    }

    public AnonymousClass2VB(Integer num, String str, String str2, String str3, String str4, double d, double d2, int i, int i2) {
        this.A08 = str3;
        this.A04 = num;
        this.A05 = str4;
        this.A03 = i;
        this.A01 = d;
        this.A00 = d2;
        this.A07 = str;
        this.A06 = str2;
        this.A02 = i2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A08);
        parcel.writeString(this.A05);
        parcel.writeInt(this.A04.intValue());
        parcel.writeDouble(this.A01);
        parcel.writeInt(this.A03);
        parcel.writeString(this.A07);
        parcel.writeDouble(this.A00);
        parcel.writeString(this.A06);
        parcel.writeInt(this.A02);
    }
}
