package X;

import android.view.View;
import com.whatsapp.emoji.search.EmojiSearchContainer;

/* renamed from: X.2Be  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Be extends C469928m {
    public Runnable A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ EmojiSearchContainer A02;

    public AnonymousClass2Be(View view, EmojiSearchContainer emojiSearchContainer) {
        this.A02 = emojiSearchContainer;
        this.A01 = view;
    }
}
