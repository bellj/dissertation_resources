package X;

import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;

/* renamed from: X.5hF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121095hF extends AnonymousClass6BG {
    public final AnonymousClass12P A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final C18790t3 A04;
    public final C21740xu A05;
    public final C14830m7 A06;
    public final C14820m6 A07;
    public final AnonymousClass018 A08;
    public final C14850m9 A09;
    public final C1329568x A0A;
    public final C120285fv A0B;
    public final AnonymousClass699 A0C;
    public final C248217a A0D;
    public final C21860y6 A0E;
    public final C18660so A0F;
    public final AbstractC17860rW A0G;
    public final C18600si A0H;
    public final AnonymousClass61M A0I;
    public final C22710zW A0J;
    public final AbstractC16870pt A0K;
    public final C129385xd A0L;
    public final AnonymousClass17Z A0M;
    public final C130425zO A0N;
    public final C129945yY A0O;
    public final AnonymousClass69B A0P;
    public final C130065yk A0Q;
    public final AnonymousClass14X A0R;
    public final C22140ya A0S;

    @Override // X.AbstractC16830pp
    public int AGi() {
        return 2;
    }

    public C121095hF(AnonymousClass12P r9, C14900mE r10, C15570nT r11, C15450nH r12, C18790t3 r13, C21740xu r14, C15550nR r15, C15610nY r16, C14830m7 r17, C16590pI r18, C14820m6 r19, AnonymousClass018 r20, C14850m9 r21, C1329568x r22, C120285fv r23, AnonymousClass699 r24, C248217a r25, C21860y6 r26, C18660so r27, AbstractC17860rW r28, C18600si r29, AnonymousClass61M r30, C22710zW r31, C17070qD r32, AnonymousClass6BC r33, C129385xd r34, AnonymousClass17Z r35, C130425zO r36, C129945yY r37, AnonymousClass69B r38, C130065yk r39, AnonymousClass14X r40, C22140ya r41) {
        super(r15, r16, r18, r32, r40, "FBPAY");
        this.A06 = r17;
        this.A05 = r14;
        this.A09 = r21;
        this.A01 = r10;
        this.A02 = r11;
        this.A04 = r13;
        this.A03 = r12;
        this.A00 = r9;
        this.A0R = r40;
        this.A08 = r20;
        this.A0H = r29;
        this.A0A = r22;
        this.A0E = r26;
        this.A07 = r19;
        this.A0S = r41;
        this.A0L = r34;
        this.A0O = r37;
        this.A0P = r38;
        this.A0J = r31;
        this.A0B = r23;
        this.A0N = r36;
        this.A0M = r35;
        this.A0C = r24;
        this.A0D = r25;
        this.A0K = r33;
        this.A0G = r28;
        this.A0F = r27;
        this.A0Q = r39;
        this.A0I = r30;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x011a  */
    @Override // X.AnonymousClass6BG, X.AbstractC16830pp
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List AF1(X.AnonymousClass1IR r11, X.AnonymousClass1IS r12) {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C121095hF.AF1(X.1IR, X.1IS):java.util.List");
    }

    @Override // X.AbstractC16830pp
    public C47822Cw AHU(AnonymousClass1ZO r10, UserJid userJid, String str) {
        boolean z;
        int A01;
        Object obj;
        C15370n3 A0A;
        Pair pair = null;
        HashMap A11 = C12970iu.A11();
        HashMap A112 = C12970iu.A11();
        C14850m9 r8 = this.A09;
        boolean z2 = false;
        if (!r8.A07(1545) || r10 == null || TextUtils.isEmpty(r10.A06)) {
            z = false;
        } else {
            pair = new Pair(true, r10.A06);
            z = true;
        }
        AnonymousClass17Z r6 = this.A0M;
        if (r6.A0A() && r8.A07(888)) {
            C22710zW r0 = this.A0J;
            if (userJid != null) {
                A01 = r0.A00(userJid);
            } else {
                A01 = r0.A01(str);
            }
            if (A01 == 2 && (userJid == null || ((A0A = r6.A03.A0A(userJid)) != null && A0A.A0I()))) {
                z2 = true;
                if (r10 != null) {
                    C30911Zh r02 = r10.A03;
                    if (r02 == null) {
                        r02 = new C30911Zh();
                        r10.A03 = r02;
                    }
                    obj = C12990iw.A0l(r02.A01, 1);
                } else {
                    obj = "";
                }
                A112.put(C12970iu.A0h(), obj);
            }
        }
        if (z || z2) {
            return new C47822Cw(pair, userJid, A11, A112);
        }
        return null;
    }
}
