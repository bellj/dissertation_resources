package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/* renamed from: X.2kt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56442kt extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99024jd();
    public final LatLng A00;
    public final LatLng A01;
    public final LatLng A02;
    public final LatLng A03;
    public final LatLngBounds A04;

    public C56442kt(LatLng latLng, LatLng latLng2, LatLng latLng3, LatLng latLng4, LatLngBounds latLngBounds) {
        this.A02 = latLng;
        this.A03 = latLng2;
        this.A00 = latLng3;
        this.A01 = latLng4;
        this.A04 = latLngBounds;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C56442kt) {
                C56442kt r5 = (C56442kt) obj;
                if (!this.A02.equals(r5.A02) || !this.A03.equals(r5.A03) || !this.A00.equals(r5.A00) || !this.A01.equals(r5.A01) || !this.A04.equals(r5.A04)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[5];
        objArr[0] = this.A02;
        objArr[1] = this.A03;
        objArr[2] = this.A00;
        objArr[3] = this.A01;
        return C12980iv.A0B(this.A04, objArr, 4);
    }

    @Override // java.lang.Object
    public String toString() {
        C13290jS r2 = new C13290jS(this);
        r2.A00(this.A02, "nearLeft");
        r2.A00(this.A03, "nearRight");
        r2.A00(this.A00, "farLeft");
        r2.A00(this.A01, "farRight");
        r2.A00(this.A04, "latLngBounds");
        return r2.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A0B(parcel, this.A02, 2, i, false);
        C95654e8.A0B(parcel, this.A03, 3, i, false);
        C95654e8.A0B(parcel, this.A00, 4, i, false);
        C95654e8.A0B(parcel, this.A01, 5, i, false);
        C95654e8.A0B(parcel, this.A04, 6, i, false);
        C95654e8.A06(parcel, A01);
    }
}
