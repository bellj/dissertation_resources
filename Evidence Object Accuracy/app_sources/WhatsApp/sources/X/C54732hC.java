package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchQueryFragment;

/* renamed from: X.2hC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54732hC extends AbstractC05270Ox {
    public final /* synthetic */ BusinessDirectorySearchQueryFragment A00;

    public C54732hC(BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment) {
        this.A00 = businessDirectorySearchQueryFragment;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        View view;
        if (i2 != 0 && recyclerView.A0B == 1) {
            BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment = this.A00;
            if (C252718t.A00(((AnonymousClass01E) businessDirectorySearchQueryFragment).A0A) && (view = ((AnonymousClass01E) businessDirectorySearchQueryFragment).A0A) != null) {
                businessDirectorySearchQueryFragment.A0B.A01(view);
            }
        }
    }
}
