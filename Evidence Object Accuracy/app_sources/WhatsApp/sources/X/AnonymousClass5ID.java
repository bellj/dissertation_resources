package X;

import com.whatsapp.R;
import java.util.HashMap;

/* renamed from: X.5ID  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5ID extends HashMap<String, Integer> {
    public AnonymousClass5ID() {
        put("drugs", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_drugs));
        put("tobacco", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_tobacco));
        put("alcohol", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_alcohol));
        put("supplements", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_supplements));
        put("animals", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_animals));
        put("body_parts_fluids", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_human_parts));
        put("healthcare", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_healthcare_products));
        put("digital_services_products", Integer.valueOf((int) R.string.smb_soft_enforcement_warning_offering_digital_content));
    }
}
