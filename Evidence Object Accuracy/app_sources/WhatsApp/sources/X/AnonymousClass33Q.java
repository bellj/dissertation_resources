package X;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.shapepicker.ShapePickerRecyclerView;
import java.util.List;
import java.util.Map;

/* renamed from: X.33Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33Q extends AbstractC64423Fm {
    public long A00;
    public boolean A01;
    public final C235512c A02;
    public final List A03 = C12960it.A0l();
    public final Map A04;

    public AnonymousClass33Q(RecyclerView recyclerView, C89554Kk r4, ShapePickerRecyclerView shapePickerRecyclerView, C235512c r6) {
        super(recyclerView, r4, shapePickerRecyclerView, true);
        this.A02 = r6;
        this.A00 = 0;
        this.A04 = C12970iu.A11();
    }

    @Override // X.AbstractC64423Fm
    public void A02(C75453js r6, boolean z) {
        super.A02(r6, z);
        View view = r6.A0H;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        RecyclerView recyclerView = this.A06;
        Resources A09 = C12960it.A09(recyclerView);
        int i = R.dimen.shape_picker_sticker_subcategory_item_portrait_width;
        if (z) {
            i = R.dimen.shape_picker_sticker_subcategory_item_landscape_width;
        }
        layoutParams.width = A09.getDimensionPixelSize(i);
        view.setLayoutParams(layoutParams);
        ImageView imageView = r6.A01;
        ViewGroup.LayoutParams layoutParams2 = imageView.getLayoutParams();
        Resources A092 = C12960it.A09(recyclerView);
        int i2 = R.dimen.shape_picker_sticker_subcategory_icon_portrait_dimen;
        if (z) {
            i2 = R.dimen.shape_picker_sticker_subcategory_icon_landscape_dimen;
        }
        int dimensionPixelSize = A092.getDimensionPixelSize(i2);
        layoutParams2.width = dimensionPixelSize;
        layoutParams2.height = dimensionPixelSize;
        imageView.setLayoutParams(layoutParams2);
    }
}
