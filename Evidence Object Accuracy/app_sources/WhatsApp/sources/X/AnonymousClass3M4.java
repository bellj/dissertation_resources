package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import java.util.Arrays;

/* renamed from: X.3M4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M4 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(40);
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M4(Parcel parcel) {
        this.A04 = parcel.readString();
        this.A05 = parcel.readString();
        this.A02 = parcel.readString();
        this.A00 = parcel.readString();
        this.A03 = parcel.readString();
        this.A01 = parcel.readString();
    }

    public AnonymousClass3M4(String str, String str2, String str3, String str4, String str5, String str6) {
        this.A04 = str;
        this.A05 = str2;
        this.A02 = str3;
        this.A00 = str4;
        this.A03 = str5;
        this.A01 = str6;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass3M4)) {
            return false;
        }
        AnonymousClass3M4 r4 = (AnonymousClass3M4) obj;
        if (!C29941Vi.A00(this.A04, r4.A04) || !C29941Vi.A00(this.A05, r4.A05) || !C29941Vi.A00(this.A02, r4.A02) || !C29941Vi.A00(this.A00, r4.A00) || !C29941Vi.A00(this.A03, r4.A03) || !C29941Vi.A00(this.A01, r4.A01)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{this.A04, this.A05, this.A02, this.A00, this.A03, this.A01});
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A04);
        parcel.writeString(this.A05);
        parcel.writeString(this.A02);
        parcel.writeString(this.A00);
        parcel.writeString(this.A03);
        parcel.writeString(this.A01);
    }
}
