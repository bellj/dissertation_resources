package X;

import org.json.JSONObject;

/* renamed from: X.58S  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58S implements AnonymousClass5W9 {
    @Override // X.AnonymousClass5W9
    public AnonymousClass5UW A8V(JSONObject jSONObject) {
        C16700pc.A0E(jSONObject, 0);
        JSONObject jSONObject2 = jSONObject.getJSONObject("not");
        C16700pc.A0B(jSONObject2);
        AnonymousClass5UW A00 = AnonymousClass4ET.A00(jSONObject2);
        if (A00 != null) {
            return new AnonymousClass58F(A00);
        }
        throw C72453ed.A0h();
    }

    @Override // X.AnonymousClass5W9
    public String ADP() {
        return "not";
    }
}
