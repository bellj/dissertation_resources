package X;

import android.util.Log;
import java.util.HashMap;
import java.util.UUID;

/* renamed from: X.2kV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56202kV extends AbstractC64703Go {
    public int A00;

    public C56202kV() {
        UUID randomUUID = UUID.randomUUID();
        int leastSignificantBits = (int) (randomUUID.getLeastSignificantBits() & 2147483647L);
        if (leastSignificantBits == 0 && (leastSignificantBits = (int) (randomUUID.getMostSignificantBits() & 2147483647L)) == 0) {
            Log.e("GAv4", "UUID.randomUUID() returned 0.");
            leastSignificantBits = Integer.MAX_VALUE;
        }
        this.A00 = leastSignificantBits;
    }

    public final String toString() {
        HashMap A11 = C12970iu.A11();
        A11.put("screenName", null);
        Boolean bool = Boolean.FALSE;
        A11.put("interstitial", bool);
        A11.put("automatic", bool);
        A11.put("screenId", Integer.valueOf(this.A00));
        A11.put("referrerScreenId", C12980iv.A0i());
        A11.put("referrerScreenName", null);
        return AbstractC64703Go.A01("referrerUri", null, A11);
    }
}
