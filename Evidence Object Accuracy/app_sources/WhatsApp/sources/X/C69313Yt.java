package X;

import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel;
import java.util.Iterator;

/* renamed from: X.3Yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C69313Yt implements AbstractC17410ql {
    public final /* synthetic */ AvatarProfilePhotoViewModel A00;

    @Override // X.AbstractC17410ql
    public void AMj() {
    }

    public C69313Yt(AvatarProfilePhotoViewModel avatarProfilePhotoViewModel) {
        this.A00 = avatarProfilePhotoViewModel;
    }

    @Override // X.AbstractC17410ql
    public void AMk() {
        AvatarProfilePhotoViewModel avatarProfilePhotoViewModel = this.A00;
        if (!avatarProfilePhotoViewModel.A07.A01()) {
            avatarProfilePhotoViewModel.A0D.A0B(EnumC870349y.A02);
        }
    }

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
        AvatarProfilePhotoViewModel avatarProfilePhotoViewModel = this.A00;
        AnonymousClass016 r4 = avatarProfilePhotoViewModel.A00;
        C27541Hx A00 = C27541Hx.A00(null, null, C16700pc.A04(r4), null, null, 31, false, true);
        Iterator it = A00.A03.iterator();
        int i = 0;
        while (true) {
            if (it.hasNext()) {
                if (((AbstractC83923y7) it.next()).A00()) {
                    break;
                }
                i++;
            } else {
                i = -1;
                break;
            }
        }
        r4.A0A(A00);
        avatarProfilePhotoViewModel.A04(true, i);
    }
}
