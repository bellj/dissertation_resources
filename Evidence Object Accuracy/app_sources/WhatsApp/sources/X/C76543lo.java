package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.3lo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76543lo extends AbstractC106444vi implements Handler.Callback {
    public int A00;
    public int A01;
    public long A02;
    public AnonymousClass5SN A03;
    public boolean A04;
    public boolean A05;
    public final Handler A06;
    public final AbstractC117025Xv A07;
    public final C76733m7 A08;
    public final AnonymousClass5SO A09;
    public final long[] A0A;
    public final C100624mD[] A0B;

    @Override // X.AbstractC117055Yb
    public boolean AJx() {
        return true;
    }

    @Override // X.AbstractC117055Yb, X.AnonymousClass5WX
    public String getName() {
        return "MetadataRenderer";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C76543lo(Looper looper, AnonymousClass5SO r5) {
        super(5);
        Handler handler;
        AbstractC117025Xv r2 = AbstractC117025Xv.A00;
        this.A09 = r5;
        if (looper == null) {
            handler = null;
        } else {
            handler = new Handler(looper, this);
        }
        this.A06 = handler;
        this.A07 = r2;
        this.A08 = new C76733m7();
        this.A0B = new C100624mD[5];
        this.A0A = new long[5];
    }

    @Override // X.AbstractC106444vi
    public void A08() {
        Arrays.fill(this.A0B, (Object) null);
        this.A01 = 0;
        this.A00 = 0;
        this.A03 = null;
    }

    @Override // X.AbstractC106444vi
    public void A09(long j, boolean z) {
        Arrays.fill(this.A0B, (Object) null);
        this.A01 = 0;
        this.A00 = 0;
        this.A04 = false;
        this.A05 = false;
    }

    public final void A0B(C100624mD r7, List list) {
        int i = 0;
        while (true) {
            AnonymousClass5YX[] r5 = r7.A00;
            if (i < r5.length) {
                C100614mC AHq = r5[i].AHq();
                if (AHq != null) {
                    AbstractC117025Xv r1 = this.A07;
                    if (r1.Aec(AHq)) {
                        AnonymousClass5SN A8E = r1.A8E(AHq);
                        byte[] AHp = r5[i].AHp();
                        C76733m7 r12 = this.A08;
                        r12.clear();
                        r12.A01(AHp.length);
                        r12.A01.put(AHp);
                        r12.A00();
                        C100624mD A8g = A8E.A8g(r12);
                        if (A8g != null) {
                            A0B(A8g, list);
                        }
                        i++;
                    }
                }
                list.add(r5[i]);
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC117055Yb
    public boolean AJN() {
        return this.A05;
    }

    @Override // X.AbstractC117055Yb
    public void AaS(long j, long j2) {
        if (!this.A04 && this.A00 < 5) {
            C76733m7 r7 = this.A08;
            r7.clear();
            C89864Lr r2 = super.A0A;
            r2.A01 = null;
            r2.A00 = null;
            int A00 = A00(r2, r7, false);
            if (A00 == -4) {
                if (AnonymousClass4YO.A00(r7)) {
                    this.A04 = true;
                } else {
                    r7.A00 = this.A02;
                    r7.A00();
                    C100624mD A8g = this.A03.A8g(r7);
                    if (A8g != null) {
                        ArrayList A0w = C12980iv.A0w(A8g.A00.length);
                        A0B(A8g, A0w);
                        if (!A0w.isEmpty()) {
                            C100624mD r1 = new C100624mD(A0w);
                            int i = this.A01;
                            int i2 = this.A00;
                            int i3 = (i + i2) % 5;
                            this.A0B[i3] = r1;
                            this.A0A[i3] = ((C76763mA) r7).A00;
                            this.A00 = i2 + 1;
                        }
                    }
                }
            } else if (A00 == -5) {
                this.A02 = r2.A00.A0J;
            }
        }
        int i4 = this.A00;
        if (i4 > 0) {
            long[] jArr = this.A0A;
            int i5 = this.A01;
            if (jArr[i5] <= j) {
                C100624mD[] r3 = this.A0B;
                C100624mD r22 = r3[i5];
                Handler handler = this.A06;
                if (handler != null) {
                    handler.obtainMessage(0, r22).sendToTarget();
                } else {
                    this.A09.ASo(r22);
                }
                int i6 = this.A01;
                r3[i6] = null;
                this.A01 = (i6 + 1) % 5;
                i4 = this.A00 - 1;
                this.A00 = i4;
            }
        }
        if (this.A04 && i4 == 0) {
            this.A05 = true;
        }
    }

    @Override // X.AnonymousClass5WX
    public int Aeb(C100614mC r3) {
        int i;
        if (this.A07.Aec(r3)) {
            i = 2;
            if (r3.A0N == null) {
                i = 4;
            }
        } else {
            i = 0;
        }
        return i | 0 | 0;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            this.A09.ASo((C100624mD) message.obj);
            return true;
        }
        throw C72463ee.A0D();
    }
}
