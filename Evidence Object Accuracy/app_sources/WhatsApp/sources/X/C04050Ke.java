package X;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;

/* renamed from: X.0Ke  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04050Ke {
    public static ParcelFileDescriptor A00(ContentResolver contentResolver, Uri uri, CancellationSignal cancellationSignal, String str) {
        return contentResolver.openFileDescriptor(uri, str, cancellationSignal);
    }
}
