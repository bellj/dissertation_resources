package X;

import android.content.SharedPreferences;

/* renamed from: X.10O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10O {
    public String A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public boolean A04 = false;
    public boolean A05 = true;
    public boolean A06;
    public boolean A07;
    public final AnonymousClass01d A08;
    public final C14820m6 A09;

    public AnonymousClass10O(AnonymousClass01d r4, C14820m6 r5) {
        this.A08 = r4;
        this.A09 = r5;
        SharedPreferences sharedPreferences = r5.A00;
        this.A01 = sharedPreferences.getBoolean("pref_fail_too_many", false);
        this.A02 = sharedPreferences.getBoolean("pref_no_route_sms", false);
        this.A03 = sharedPreferences.getBoolean("pref_no_route_voice", false);
        this.A06 = sharedPreferences.getBoolean("pref_fail_too_many_attempts", false);
        this.A07 = sharedPreferences.getBoolean("pref_fail_too_many_guesses", false);
    }

    public String A00(String str) {
        boolean A0B = C003501n.A0B(this.A08);
        String str2 = this.A00;
        boolean z = this.A04;
        boolean z2 = this.A05;
        boolean z3 = this.A02;
        boolean z4 = this.A03;
        boolean z5 = this.A01;
        if ("register-phone".equals(str2)) {
            if (A0B) {
                return "register-phone-rtd";
            }
            if (z) {
                return "register-phone-no_number";
            }
            if (!z2) {
                return "register-phone-invalid";
            }
            return str;
        } else if (!"verify-sms".equals(str2)) {
            String str3 = "verify-tma";
            if (!str3.equals(str2)) {
                str3 = "verify-tmg";
                if (!str3.equals(str2)) {
                    return str;
                }
            }
            return str3;
        } else if (A0B) {
            return "verify-sms-rtd";
        } else {
            if (z3) {
                return z4 ? "verify-sms-no_routes_both" : "verify-sms-no_routes_sms";
            }
            if (z4) {
                return "verify-sms-no_routes_voice";
            }
            if (!z5) {
                return "verify-sms-normal";
            }
            return str;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(java.lang.String r11) {
        /*
            r10 = this;
            r10.A00 = r11
            java.lang.String r3 = "verify-tmg"
            boolean r0 = r11.equals(r3)
            r8 = 0
            r9 = 1
            java.lang.String r1 = "verify-tma"
            if (r0 == 0) goto L_0x0036
            r10.A07 = r9
            r10.A06 = r8
            X.0m6 r4 = r10.A09
            boolean r5 = r10.A01
            boolean r6 = r10.A02
            boolean r7 = r10.A03
        L_0x001c:
            r4.A1E(r5, r6, r7, r8, r9)
        L_0x001f:
            java.lang.String r0 = "verify-sms"
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x002e
            boolean r0 = r10.A07
            if (r0 == 0) goto L_0x002f
            r10.A00 = r3
        L_0x002e:
            return
        L_0x002f:
            boolean r0 = r10.A06
            if (r0 == 0) goto L_0x002e
            r10.A00 = r1
            return
        L_0x0036:
            boolean r0 = r11.equals(r1)
            if (r0 == 0) goto L_0x001f
            r10.A07 = r8
            r10.A06 = r9
            X.0m6 r4 = r10.A09
            boolean r5 = r10.A01
            boolean r6 = r10.A02
            boolean r7 = r10.A03
            r8 = 1
            r9 = 0
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10O.A01(java.lang.String):void");
    }

    public void A02(String str) {
        switch (str.hashCode()) {
            case -1976127222:
                if (str.equals("noRouteVoice")) {
                    this.A03 = true;
                    break;
                }
                break;
            case -1893373339:
                if (str.equals("validNumber")) {
                    this.A05 = true;
                    break;
                }
                break;
            case -1777505757:
                if (str.equals("notEmptyNumber")) {
                    this.A04 = false;
                    break;
                }
                break;
            case -1522953003:
                if (str.equals("failTooMany")) {
                    this.A01 = true;
                    break;
                }
                break;
            case -416647790:
                if (str.equals("notValidNumber")) {
                    this.A05 = false;
                    break;
                }
                break;
            case 1040735990:
                if (str.equals("emptyNumber")) {
                    this.A04 = true;
                    break;
                }
                break;
            case 1164419889:
                if (str.equals("noRouteSms")) {
                    this.A02 = true;
                    break;
                }
                break;
        }
        this.A09.A1E(this.A01, this.A02, this.A03, this.A06, this.A07);
    }
}
