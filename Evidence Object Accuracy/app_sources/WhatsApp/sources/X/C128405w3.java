package X;

import com.whatsapp.R;

/* renamed from: X.5w3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128405w3 {
    public int A00(int i) {
        if (i == 429) {
            return R.string.virality_payments_not_enabled_description_limit_hit;
        }
        if (i == 443) {
            return R.string.virality_payments_not_enabled_description_app_upgrade_needed;
        }
        if (i == 445) {
            return R.string.virality_payments_not_enabled_description_link_expired;
        }
        switch (i) {
            case 403:
                return R.string.virality_payments_ineligible;
            case 404:
                return R.string.virality_payments_not_enabled_description_link_expired;
            case 405:
                return R.string.virality_payments_not_enabled_description_incorrect_app;
            default:
                return R.string.payments_generic_error;
        }
    }
}
