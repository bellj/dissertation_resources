package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1PN  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1PN extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1PN A0C;
    public static volatile AnonymousClass255 A0D;
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;
    public String A09;
    public String A0A;
    public String A0B = "";

    static {
        AnonymousClass1PN r0 = new AnonymousClass1PN();
        A0C = r0;
        r0.A0W();
    }

    public AnonymousClass1PN() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A07 = r0;
        this.A06 = r0;
        this.A08 = r0;
        this.A0A = "";
        this.A09 = "";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        switch (r16.ordinal()) {
            case 0:
                return A0C;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                AnonymousClass1PN r3 = (AnonymousClass1PN) obj2;
                int i = this.A01;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A0B;
                int i2 = r3.A01;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A0B = r8.Afy(str, r3.A0B, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                AbstractC27881Jp r2 = this.A07;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A07 = r8.Afm(r2, r3.A07, z3, z4);
                boolean z5 = false;
                if ((this.A01 & 4) == 4) {
                    z5 = true;
                }
                AbstractC27881Jp r4 = this.A06;
                boolean z6 = false;
                if ((r3.A01 & 4) == 4) {
                    z6 = true;
                }
                this.A06 = r8.Afm(r4, r3.A06, z5, z6);
                boolean z7 = false;
                if ((this.A01 & 8) == 8) {
                    z7 = true;
                }
                AbstractC27881Jp r42 = this.A08;
                boolean z8 = false;
                if ((r3.A01 & 8) == 8) {
                    z8 = true;
                }
                this.A08 = r8.Afm(r42, r3.A08, z7, z8);
                int i3 = this.A01;
                boolean z9 = false;
                if ((i3 & 16) == 16) {
                    z9 = true;
                }
                String str2 = this.A0A;
                int i4 = r3.A01;
                boolean z10 = false;
                if ((i4 & 16) == 16) {
                    z10 = true;
                }
                this.A0A = r8.Afy(str2, r3.A0A, z9, z10);
                boolean z11 = false;
                if ((i3 & 32) == 32) {
                    z11 = true;
                }
                int i5 = this.A02;
                boolean z12 = false;
                if ((i4 & 32) == 32) {
                    z12 = true;
                }
                this.A02 = r8.Afp(i5, r3.A02, z11, z12);
                boolean z13 = false;
                if ((i3 & 64) == 64) {
                    z13 = true;
                }
                int i6 = this.A03;
                boolean z14 = false;
                if ((i4 & 64) == 64) {
                    z14 = true;
                }
                this.A03 = r8.Afp(i6, r3.A03, z13, z14);
                boolean z15 = false;
                if ((i3 & 128) == 128) {
                    z15 = true;
                }
                String str3 = this.A09;
                boolean z16 = false;
                if ((i4 & 128) == 128) {
                    z16 = true;
                }
                this.A09 = r8.Afy(str3, r3.A09, z15, z16);
                boolean z17 = false;
                if ((i3 & 256) == 256) {
                    z17 = true;
                }
                long j = this.A04;
                boolean z18 = false;
                if ((i4 & 256) == 256) {
                    z18 = true;
                }
                this.A04 = r8.Afs(j, r3.A04, z17, z18);
                boolean z19 = false;
                if ((i3 & 512) == 512) {
                    z19 = true;
                }
                float f = this.A00;
                boolean z20 = false;
                if ((i4 & 512) == 512) {
                    z20 = true;
                }
                this.A00 = r8.Afo(f, r3.A00, z19, z20);
                boolean z21 = false;
                if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z21 = true;
                }
                long j2 = this.A05;
                boolean z22 = false;
                if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z22 = true;
                }
                this.A05 = r8.Afs(j2, r3.A05, z21, z22);
                if (r8 == C463025i.A00) {
                    this.A01 = i3 | i4;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r82.A0A();
                                    this.A01 = 1 | this.A01;
                                    this.A0B = A0A;
                                    break;
                                case 18:
                                    this.A01 |= 2;
                                    this.A07 = r82.A08();
                                    break;
                                case 26:
                                    this.A01 |= 4;
                                    this.A06 = r82.A08();
                                    break;
                                case 34:
                                    this.A01 |= 8;
                                    this.A08 = r82.A08();
                                    break;
                                case 42:
                                    String A0A2 = r82.A0A();
                                    this.A01 |= 16;
                                    this.A0A = A0A2;
                                    break;
                                case 48:
                                    this.A01 |= 32;
                                    this.A02 = r82.A02();
                                    break;
                                case 56:
                                    this.A01 |= 64;
                                    this.A03 = r82.A02();
                                    break;
                                case 66:
                                    String A0A3 = r82.A0A();
                                    this.A01 |= 128;
                                    this.A09 = A0A3;
                                    break;
                                case C43951xu.A02 /* 72 */:
                                    this.A01 |= 256;
                                    this.A04 = r82.A06();
                                    break;
                                case 85:
                                    this.A01 |= 512;
                                    this.A00 = Float.intBitsToFloat(r82.A01());
                                    break;
                                case 88:
                                    this.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A05 = r82.A06();
                                    break;
                                default:
                                    if (A0a(r82, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass1PN();
            case 5:
                return new AnonymousClass1PO();
            case 6:
                break;
            case 7:
                if (A0D == null) {
                    synchronized (AnonymousClass1PN.class) {
                        if (A0D == null) {
                            A0D = new AnonymousClass255(A0C);
                        }
                    }
                }
                return A0D;
            default:
                throw new UnsupportedOperationException();
        }
        return A0C;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A01 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A0B);
        }
        int i3 = this.A01;
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A09(this.A07, 2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A09(this.A06, 3);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A09(this.A08, 4);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A07(5, this.A0A);
        }
        int i4 = this.A01;
        if ((i4 & 32) == 32) {
            i2 += CodedOutputStream.A04(6, this.A02);
        }
        if ((i4 & 64) == 64) {
            i2 += CodedOutputStream.A04(7, this.A03);
        }
        if ((i4 & 128) == 128) {
            i2 += CodedOutputStream.A07(8, this.A09);
        }
        int i5 = this.A01;
        if ((i5 & 256) == 256) {
            i2 += CodedOutputStream.A06(9, this.A04);
        }
        if ((i5 & 512) == 512) {
            i2 += 5;
        }
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A05(11, this.A05);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0B);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0K(this.A07, 2);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0K(this.A06, 3);
        }
        if ((this.A01 & 8) == 8) {
            codedOutputStream.A0K(this.A08, 4);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0I(5, this.A0A);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0F(6, this.A02);
        }
        if ((this.A01 & 64) == 64) {
            codedOutputStream.A0F(7, this.A03);
        }
        if ((this.A01 & 128) == 128) {
            codedOutputStream.A0I(8, this.A09);
        }
        if ((this.A01 & 256) == 256) {
            codedOutputStream.A0H(9, this.A04);
        }
        if ((this.A01 & 512) == 512) {
            codedOutputStream.A0D(10, Float.floatToRawIntBits(this.A00));
        }
        if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0H(11, this.A05);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
