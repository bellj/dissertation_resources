package X;

import android.content.Context;
import com.google.android.gms.maps.GoogleMapOptions;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* renamed from: X.32r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618232r extends C618632v {
    public final /* synthetic */ GroupChatLiveLocationsActivity2 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C618232r(Context context, GoogleMapOptions googleMapOptions, GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2) {
        super(context, googleMapOptions);
        this.A00 = groupChatLiveLocationsActivity2;
    }
}
