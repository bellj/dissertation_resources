package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.3J3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3J3 {
    public static volatile Handler A00;
    public static volatile Handler A01;

    public static Handler A00() {
        if (A01 == null) {
            synchronized (AnonymousClass3J3.class) {
                if (A01 == null) {
                    A01 = C12970iu.A0E();
                }
            }
        }
        return A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r2 == false) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object A01(java.util.concurrent.Future r7, int r8) {
        /*
            java.lang.String r4 = ", "
            java.lang.String r6 = "Unable to restore priority: "
            int r0 = android.os.Process.myTid()
            r5 = 1
            r3 = 0
            boolean r2 = X.C12980iv.A1V(r8, r0)
            boolean r0 = r7.isDone()
            if (r0 != 0) goto L_0x0017
            r1 = 1
            if (r2 != 0) goto L_0x0018
        L_0x0017:
            r1 = 0
        L_0x0018:
            boolean r0 = A03()
            if (r0 == 0) goto L_0x0035
            if (r1 == 0) goto L_0x0035
            int r0 = android.os.Process.myTid()
            int r0 = android.os.Process.getThreadPriority(r0)
            int r3 = android.os.Process.getThreadPriority(r8)
        L_0x002c:
            if (r0 >= r3) goto L_0x0036
            android.os.Process.setThreadPriority(r8, r0)     // Catch: SecurityException -> 0x0032
            goto L_0x0036
        L_0x0032:
            int r0 = r0 + 1
            goto L_0x002c
        L_0x0035:
            r5 = 0
        L_0x0036:
            java.lang.Object r0 = r7.get()     // Catch: ExecutionException -> 0x005f, InterruptedException | CancellationException -> 0x0054, all -> 0x0075
            if (r5 == 0) goto L_0x0053
            android.os.Process.setThreadPriority(r8, r3)     // Catch: IllegalArgumentException | SecurityException -> 0x0040
            goto L_0x0052
        L_0x0040:
            r2 = move-exception
            java.lang.StringBuilder r0 = X.C12960it.A0j(r6)
            r0.append(r8)
            java.lang.String r1 = X.C12960it.A0e(r4, r0, r3)
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1, r2)
            throw r0
        L_0x0052:
            return r0
        L_0x0053:
            return r0
        L_0x0054:
            r2 = move-exception
            java.lang.String r1 = r2.getMessage()     // Catch: all -> 0x0075
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch: all -> 0x0075
            r0.<init>(r1, r2)     // Catch: all -> 0x0075
            throw r0     // Catch: all -> 0x0075
        L_0x005f:
            r2 = move-exception
            java.lang.Throwable r1 = r2.getCause()     // Catch: all -> 0x0075
            boolean r0 = r1 instanceof java.lang.RuntimeException     // Catch: all -> 0x0075
            if (r0 == 0) goto L_0x006b
            java.lang.RuntimeException r1 = (java.lang.RuntimeException) r1     // Catch: all -> 0x0075
            throw r1     // Catch: all -> 0x0075
        L_0x006b:
            java.lang.String r1 = r2.getMessage()     // Catch: all -> 0x0075
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch: all -> 0x0075
            r0.<init>(r1, r2)     // Catch: all -> 0x0075
            throw r0     // Catch: all -> 0x0075
        L_0x0075:
            r0 = move-exception
            if (r5 == 0) goto L_0x008e
            android.os.Process.setThreadPriority(r8, r3)     // Catch: IllegalArgumentException | SecurityException -> 0x007c
            throw r0
        L_0x007c:
            r2 = move-exception
            java.lang.StringBuilder r0 = X.C12960it.A0j(r6)
            r0.append(r8)
            java.lang.String r1 = X.C12960it.A0e(r4, r0, r3)
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1, r2)
            throw r0
        L_0x008e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3J3.A01(java.util.concurrent.Future, int):java.lang.Object");
    }

    public static void A02(String str) {
        if (!AnonymousClass4FZ.A00 && !A03()) {
            if (str == null) {
                str = C12960it.A0d(Thread.currentThread().getName(), C12960it.A0k("This must run on the main thread; but is running on "));
            }
            throw C12960it.A0U(str);
        }
    }

    public static boolean A03() {
        return C12970iu.A1Z(Looper.getMainLooper().getThread(), Thread.currentThread());
    }
}
