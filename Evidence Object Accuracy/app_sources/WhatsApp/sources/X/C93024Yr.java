package X;

/* renamed from: X.4Yr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93024Yr {
    public static final AnonymousClass5WO A00(AnonymousClass5WO r3) {
        AbstractC113685Io r2;
        if ((r3 instanceof AbstractC113685Io) && (r2 = (AbstractC113685Io) r3) != null && (r3 = r2.A00) == null) {
            AnonymousClass5ZS r0 = (AnonymousClass5ZS) r2.ABi().get(AnonymousClass5ZS.A00);
            if (r0 == null) {
                r3 = r2;
            } else {
                r3 = new C114205Kp(r2, (AbstractC10990fX) r0);
            }
            r2.A00 = r3;
        }
        return r3;
    }
}
