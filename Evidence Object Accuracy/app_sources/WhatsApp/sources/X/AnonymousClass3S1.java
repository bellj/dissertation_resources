package X;

import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3S1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S1 implements AbstractC12560i7 {
    public final /* synthetic */ AnonymousClass0MU A00;
    public final /* synthetic */ AbstractC33071dF A01;
    public final /* synthetic */ C54692h8 A02;
    public final /* synthetic */ AnonymousClass4JZ A03;

    @Override // X.AbstractC12560i7
    public void AV2(boolean z) {
    }

    @Override // X.AbstractC12560i7
    public void AXa(MotionEvent motionEvent, RecyclerView recyclerView) {
    }

    public AnonymousClass3S1(AnonymousClass0MU r1, AbstractC33071dF r2, C54692h8 r3, AnonymousClass4JZ r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        if (r6 != 8) goto L_0x002f;
     */
    @Override // X.AbstractC12560i7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean ARR(android.view.MotionEvent r8, androidx.recyclerview.widget.RecyclerView r9) {
        /*
            r7 = this;
            int r6 = r8.getActionMasked()
            X.4JZ r5 = r7.A03
            r2 = 1
            r4 = 8
            r1 = 0
            X.2h8 r3 = r7.A02
            boolean r0 = r3.A04
            if (r5 == 0) goto L_0x001d
            if (r0 != 0) goto L_0x001c
            X.1dF r0 = r7.A01
            boolean r0 = r0.AdR()
            if (r0 == 0) goto L_0x001c
            if (r6 != r4) goto L_0x002f
        L_0x001c:
            return r1
        L_0x001d:
            if (r0 != 0) goto L_0x001c
            X.1dF r0 = r7.A01
            boolean r0 = r0.AdR()
            if (r0 == 0) goto L_0x001c
            r0 = 2
            if (r6 == r0) goto L_0x001c
            if (r6 == r4) goto L_0x001c
            if (r6 != r2) goto L_0x002f
            return r1
        L_0x002f:
            float r1 = r8.getY()
            int r0 = r3.A00
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x0041
            X.0MU r0 = r7.A00
            X.0hn r0 = r0.A00
            r0.AXZ(r8)
        L_0x0041:
            float r1 = r8.getY()
            int r0 = r3.A00
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x004d
            r2 = 0
        L_0x004d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3S1.ARR(android.view.MotionEvent, androidx.recyclerview.widget.RecyclerView):boolean");
    }
}
