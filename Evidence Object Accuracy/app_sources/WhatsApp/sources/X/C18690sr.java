package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0sr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18690sr extends AbstractC18500sY {
    public final C15570nT A00;
    public final C18460sU A01;
    public final C18680sq A02;

    public C18690sr(C15570nT r3, C18460sU r4, C18680sq r5, C18480sW r6) {
        super(r6, "broadcast_me_jid", Integer.MIN_VALUE);
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = r5;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("group_jid_row_id");
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            j = cursor.getLong(columnIndexOrThrow);
            i++;
            AbstractC15590nW r11 = (AbstractC15590nW) this.A01.A07(C29901Ve.class, (long) cursor.getInt(columnIndexOrThrow2));
            if (r11 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("broadcast-me-jid-migration/process-batch: groupJidRowId=");
                sb.append(cursor.getInt(columnIndexOrThrow2));
                sb.append(", rowId=");
                sb.append(j);
                sb.append(" SKIP Due to invalid BroadcastListJid");
                Log.w(sb.toString());
            } else {
                C18680sq r10 = this.A02;
                String valueOf = String.valueOf(r10.A07.A01(r11));
                C15570nT r0 = r10.A01;
                r0.A08();
                C27631Ih r02 = r0.A05;
                AnonymousClass009.A05(r02);
                String valueOf2 = String.valueOf(r10.A01(r02));
                C16310on A01 = r10.A08.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT _id FROM group_participant_user WHERE group_jid_row_id = ? AND user_jid_row_id = ?", new String[]{valueOf, valueOf2});
                    boolean moveToNext = A09.moveToNext();
                    A09.close();
                    A01.close();
                    if (moveToNext) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("broadcast-me-jid-migration/process-batch: groupJidRowId=");
                        sb2.append(cursor.getInt(columnIndexOrThrow2));
                        sb2.append(", rowId=");
                        sb2.append(j);
                        sb2.append(" SKIP since it already has me jid");
                        Log.i(sb2.toString());
                    } else {
                        C15570nT r03 = this.A00;
                        r03.A08();
                        C27631Ih r2 = r03.A05;
                        AnonymousClass009.A05(r2);
                        r10.A04(new AnonymousClass1YO((UserJid) r2, 2, false, false), r11);
                    }
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        }
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("broadcast_me_jid_ready", 2);
    }
}
