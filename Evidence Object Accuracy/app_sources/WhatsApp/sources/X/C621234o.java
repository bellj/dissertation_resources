package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.34o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C621234o extends AnonymousClass34r {
    public boolean A00;

    public C621234o(Context context, C15570nT r2, C15550nR r3, C15610nY r4, C63563Cb r5, C63543Bz r6, AnonymousClass01d r7, C14830m7 r8, AnonymousClass018 r9, AnonymousClass19M r10, C16630pM r11, AnonymousClass12F r12) {
        super(context, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
        A00();
    }

    @Override // X.AnonymousClass2V0
    public /* bridge */ /* synthetic */ void A05(AbstractC15340mz r2, List list) {
        AbstractC16130oV r22 = (AbstractC16130oV) r2;
        super.A05(r22, list);
        ((AnonymousClass34r) this).A00.setMessage(r22);
    }

    @Override // X.AnonymousClass34r
    public String getDefaultMessageText() {
        return this.A0C.A07;
    }

    @Override // X.AnonymousClass34r
    public int getDrawableRes() {
        return R.drawable.msg_status_image;
    }
}
