package X;

import android.view.View;
import android.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0dB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09580dB implements Runnable {
    public final /* synthetic */ AnonymousClass0FH A00;
    public final /* synthetic */ ArrayList A01;

    public RunnableC09580dB(AnonymousClass0FH r1, ArrayList arrayList) {
        this.A00 = r1;
        this.A01 = arrayList;
    }

    @Override // java.lang.Runnable
    public void run() {
        ArrayList arrayList = this.A01;
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            AnonymousClass03U r6 = (AnonymousClass03U) it.next();
            AnonymousClass0FH r5 = this.A00;
            View view = r6.A0H;
            ViewPropertyAnimator animate = view.animate();
            r5.A00.add(r6);
            animate.alpha(1.0f).setDuration(r5.A04()).setListener(new AnonymousClass09N(view, animate, r5, r6)).start();
        }
        arrayList.clear();
        this.A00.A01.remove(arrayList);
    }
}
