package X;

import org.json.JSONObject;

/* renamed from: X.58Z  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58Z implements AnonymousClass5W9 {
    @Override // X.AnonymousClass5W9
    public AnonymousClass5UW A8V(JSONObject jSONObject) {
        String A0u = C72453ed.A0u(jSONObject);
        String string = jSONObject.getString("stringEquals");
        C16700pc.A0B(string);
        return new AnonymousClass58O(A0u, string);
    }

    @Override // X.AnonymousClass5W9
    public String ADP() {
        return "stringEquals";
    }
}
