package X;

import android.text.TextUtils;
import android.text.format.Time;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3Ie  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65113Ie {
    public String A00;
    public String A01;
    public JSONObject A02;
    public boolean A03;
    public final long A04;
    public final long A05;
    public final AbstractC15710nm A06;
    public final C15820nx A07;
    public final C44791zY A08;
    public final C14850m9 A09;
    public final String A0A;
    public final JSONObject A0B;

    public C65113Ie(AbstractC15710nm r1, C15820nx r2, C44791zY r3, C14850m9 r4, String str, String str2, String str3, JSONObject jSONObject, long j, long j2) {
        this.A09 = r4;
        this.A06 = r1;
        this.A07 = r2;
        this.A08 = r3;
        this.A0A = str;
        this.A01 = str2;
        this.A05 = j;
        this.A04 = j2;
        this.A00 = str3;
        this.A0B = jSONObject;
    }

    public static C65113Ie A00(AbstractC15710nm r19, C15820nx r20, C44791zY r21, C14850m9 r22, InputStream inputStream, String str) {
        String A00;
        JSONObject jSONObject;
        try {
            A00 = AnonymousClass1P1.A00(inputStream);
        } catch (IOException | JSONException e) {
            Log.e("gdrive-api-v2/backup/unable to read stream", e);
        }
        if (TextUtils.isEmpty(A00)) {
            Log.e("gdrive-api-v2/backup/empty input");
            return null;
        }
        JSONObject A05 = C13000ix.A05(A00);
        String A01 = A01("name", A05);
        String A012 = A01("updateTime", A05);
        if (TextUtils.isEmpty(A012)) {
            Log.e("gdrive-api-v2/backup/no updateTime provided. malformed stream?");
            return null;
        }
        Time time = new Time();
        time.parse3339(A012);
        long millis = time.toMillis(true);
        long optLong = A05.optLong("sizeBytes", -1);
        String A013 = A01("activeTransactionId", A05);
        String A014 = A01("metadata", A05);
        if (!TextUtils.isEmpty(A014)) {
            jSONObject = C13000ix.A05(A014);
        } else {
            jSONObject = null;
        }
        if (A01 != null && millis > 0) {
            return new C65113Ie(r19, r20, r21, r22, str, A01, A013, jSONObject, millis, optLong);
        }
        return null;
    }

    public static String A01(String str, JSONObject jSONObject) {
        if (jSONObject.isNull(str)) {
            return null;
        }
        try {
            return jSONObject.getString(str);
        } catch (JSONException e) {
            Log.e("gdrive-api-v2/backup/get-string unexpected exception", e);
            return null;
        }
    }

    public final int A02(String str, int i) {
        JSONObject jSONObject = this.A0B;
        if (jSONObject != null) {
            if (jSONObject.has(str)) {
                return jSONObject.optInt(str, i);
            }
            JSONObject A06 = A06();
            if (A06 != null) {
                return A06.optInt(str, i);
            }
        }
        return i;
    }

    public final long A03(String str, long j) {
        JSONObject jSONObject = this.A0B;
        if (jSONObject != null) {
            if (jSONObject.has(str)) {
                return jSONObject.optLong(str, j);
            }
            JSONObject A06 = A06();
            if (A06 != null) {
                return A06.optLong(str, j);
            }
        }
        return j;
    }

    public synchronized String A04() {
        return this.A00;
    }

    public JSONObject A05() {
        JSONObject jSONObject = this.A0B;
        JSONObject jSONObject2 = null;
        if (jSONObject != null) {
            String optString = jSONObject.optString("backupExpiry");
            if (!TextUtils.isEmpty(optString)) {
                try {
                    jSONObject2 = C13000ix.A05(optString);
                    return jSONObject2;
                } catch (JSONException e) {
                    Log.w("gdrive-api-v2/get-backup-supported/failed to parse", e);
                }
            }
        }
        return jSONObject2;
    }

    public final synchronized JSONObject A06() {
        JSONObject jSONObject;
        if (this.A03) {
            jSONObject = this.A02;
        } else {
            JSONObject jSONObject2 = this.A0B;
            if (jSONObject2 != null) {
                this.A03 = true;
                String optString = jSONObject2.optString("encryptedData");
                if (!TextUtils.isEmpty(optString)) {
                    String A00 = this.A07.A00(optString);
                    if (!TextUtils.isEmpty(A00)) {
                        try {
                            this.A02 = C13000ix.A05(A00);
                        } catch (JSONException e) {
                            Log.e("gdrive-api-v2/backup/failed to parse decrypted metadata", e);
                        }
                        jSONObject = this.A02;
                    }
                }
            }
            return null;
        }
        return jSONObject;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Backup{jidUser='");
        char A00 = C12990iw.A00(this.A0A, A0k);
        A0k.append(", name='");
        A0k.append(this.A01);
        A0k.append(A00);
        A0k.append(", updateTime=");
        A0k.append(this.A05);
        A0k.append(", sizeBytes=");
        A0k.append(this.A04);
        A0k.append(", activeTransactionId='");
        A0k.append(A04());
        A0k.append(A00);
        A0k.append(", clientMetadata=");
        A0k.append(this.A0B);
        return C12970iu.A0v(A0k);
    }
}
