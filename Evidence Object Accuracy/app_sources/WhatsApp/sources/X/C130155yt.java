package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;

/* renamed from: X.5yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130155yt {
    public String A00;
    public boolean A01 = false;
    public final AnonymousClass12P A02;
    public final C14900mE A03;
    public final C20080vB A04;
    public final C18640sm A05;
    public final C16590pI A06;
    public final C18650sn A07;
    public final C18610sj A08;
    public final C129645y4 A09;
    public final AnonymousClass600 A0A;
    public final C130125yq A0B;
    public final AnonymousClass61F A0C;
    public final C130105yo A0D;
    public final C129675y7 A0E;
    public final C125075qe A0F;
    public final C19370u0 A0G;
    public final AnonymousClass18L A0H;
    public final List A0I = C12960it.A0l();
    public final Set A0J = C12970iu.A12();
    public final AtomicBoolean A0K = new AtomicBoolean(false);
    public final AtomicBoolean A0L = new AtomicBoolean(false);
    public final AtomicBoolean A0M = new AtomicBoolean(false);

    public C130155yt(AnonymousClass12P r3, C14900mE r4, C20080vB r5, C18640sm r6, C16590pI r7, C18650sn r8, C18610sj r9, C129645y4 r10, AnonymousClass600 r11, C130125yq r12, AnonymousClass61F r13, C130105yo r14, C129675y7 r15, C125075qe r16, C19370u0 r17, AnonymousClass18L r18) {
        this.A06 = r7;
        this.A03 = r4;
        this.A0E = r15;
        this.A0G = r17;
        this.A02 = r3;
        this.A0A = r11;
        this.A04 = r5;
        this.A0C = r13;
        this.A0F = r16;
        this.A0B = r12;
        this.A0D = r14;
        this.A08 = r9;
        this.A09 = r10;
        this.A05 = r6;
        this.A0H = r18;
        this.A07 = r8;
    }

    public static void A00(AbstractC136196Lo r2, C130155yt r3, C1310460z r4) {
        r3.A0B(r2, r4, "get", 5);
    }

    public static void A01(AbstractC136196Lo r2, C130155yt r3, C1310460z r4) {
        r3.A0B(r2, r4, "set", 5);
    }

    public static void A02(AbstractC136196Lo r6, C130155yt r7, C1310460z r8, int i) {
        r7.A09(r6, r8, Integer.valueOf(i), "set", 4);
    }

    public static void A03(AbstractC136196Lo r6, C130155yt r7, C1310460z r8, int i) {
        r7.A09(r6, r8, Integer.valueOf(i), "set", 5);
    }

    public static void A04(AbstractC136196Lo r5, C130155yt r6, C1310460z r7, int i, int i2) {
        r6.A09(r5, r7, Integer.valueOf(i), "set", i2);
    }

    public synchronized String A05() {
        String A0p;
        C130105yo r3 = this.A0D;
        A0p = C12980iv.A0p(r3.A02(), "uuid");
        if (AnonymousClass1US.A0C(A0p)) {
            A0p = C12990iw.A0n();
            C12970iu.A1D(C130105yo.A01(r3), "uuid", A0p);
        }
        AnonymousClass009.A05(A0p);
        return A0p;
    }

    public void A06() {
        this.A0L.set(false);
        this.A0M.set(false);
        this.A0K.set(false);
        synchronized (this) {
            this.A0J.clear();
        }
    }

    public void A07(AbstractC136196Lo r11) {
        if (this.A0L.get()) {
            A08(r11);
            return;
        }
        AnonymousClass61S[] r3 = new AnonymousClass61S[2];
        AnonymousClass61S.A05("action", "novi-get-backend-services-certificates", r3, 0);
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("key_type", "X25519"), r3, 1);
        C1310460z A0B = C117315Zl.A0B("service", AnonymousClass61S.A02("value", "AUTH"));
        ArrayList arrayList = A0F.A02;
        arrayList.add(A0B);
        C117305Zk.A1R("service", arrayList, AnonymousClass61S.A02("value", "GATEWAY"));
        C117305Zk.A1R("service", arrayList, AnonymousClass61S.A02("value", "MEDIA"));
        C117305Zk.A1R("service", arrayList, AnonymousClass61S.A02("value", "RISK"));
        C117305Zk.A1R("service", arrayList, AnonymousClass61S.A02("value", "WALLET_CORE"));
        A0A(new AbstractC136196Lo(r11, this) { // from class: X.69r
            public final /* synthetic */ AbstractC136196Lo A00;
            public final /* synthetic */ C130155yt A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r14) {
                C130155yt r4 = this.A01;
                AbstractC136196Lo r32 = this.A00;
                if (r14.A06()) {
                    Object obj = r14.A02;
                    AnonymousClass009.A05(obj);
                    List A0J = ((AnonymousClass1V8) obj).A0J("service_certificate");
                    List<C130655zl> list = r4.A0I;
                    list.clear();
                    Iterator it = A0J.iterator();
                    while (it.hasNext()) {
                        AnonymousClass1V8 A0d = C117305Zk.A0d(it);
                        String str = "";
                        try {
                            str = A0d.A0F("service").A0H("value");
                            List A0J2 = A0d.A0J("certificate");
                            ArrayList A0l = C12960it.A0l();
                            Iterator it2 = A0J2.iterator();
                            while (it2.hasNext()) {
                                A0l.add(C117305Zk.A0d(it2).A0H("value"));
                            }
                            C20080vB r8 = r4.A04;
                            X509Certificate A00 = C20080vB.A00("MIIEfTCCA2WgAwIBAgIUTRB3DSS1IoPy5PHlIVftCO3ytEswDQYJKoZIhvcNAQEL\nBQAweTEoMCYGA1UEAwwfRmFjZWJvb2sgUm9vdGNhbmFsIFByb2QgUm9vdCBDQTEL\nMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExEzARBgNVBAcMCk1lbmxv\nIFBhcmsxFjAUBgNVBAoMDUZhY2Vib29rIEluYy4wHhcNMTgwMjIxMDAwNjQzWhcN\nNDgwMjIxMDAwNjQzWjB5MSgwJgYDVQQDDB9GYWNlYm9vayBSb290Y2FuYWwgUHJv\nZCBSb290IENBMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTETMBEG\nA1UEBwwKTWVubG8gUGFyazEWMBQGA1UECgwNRmFjZWJvb2sgSW5jLjCCASIwDQYJ\nKoZIhvcNAQEBBQADggEPADCCAQoCggEBAO04IfUs0M4IPVwJHLAFSTulY1/R/cEk\nhDlIKmpRA3IiSG7eAgBxWuvUZti2zm4G0ftPVUJOqjhavu+EOW9iT6WBZojtRNsF\nkJKJIBrfwg3A9i2BMF7PUsDNMRkRnUmiZeQ5HY/sPLYCwp6rYLaUHC5E+73y9ByS\nssnmlJCPTsv+OgdFpFHJaSf0YOL33xheHDrdElYAibh4dOtg4v7lWh/D1vpLi4Y1\ngFD8BICeUIZe622gRnj84hCkkbE6kJyCqO3l2FXMPYZjhlUa8vRE4qsUUCAZmamW\nNDGKDH5z2EuC3glVU9B5NJdfn3FXh7/Pv49sV70hs+pGkOwwuhsJ1dUCAwEAAaOB\n/DCB+TAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBT6a6rC4jjPN1kw0KTesLkl\nsYoajDCBtgYDVR0jBIGuMIGrgBT6a6rC4jjPN1kw0KTesLklsYoajKF9pHsweTEo\nMCYGA1UEAwwfRmFjZWJvb2sgUm9vdGNhbmFsIFByb2QgUm9vdCBDQTELMAkGA1UE\nBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExEzARBgNVBAcMCk1lbmxvIFBhcmsx\nFjAUBgNVBAoMDUZhY2Vib29rIEluYy6CFE0Qdw0ktSKD8uTx5SFX7Qjt8rRLMA4G\nA1UdDwEB/wQEAwIBhjANBgkqhkiG9w0BAQsFAAOCAQEAW9EUmvvxgcxEPfxB7G7R\nwxwk6m4xPBTc0UArnWCLZRmRBmaKYPovC0brhKF7Dfn9IcXEhhmsLRnBy/1xtbWG\nW1kQzQeIUaDgXymE+dmnmorhuwepwELcsX7UB1RM0HoES3Z0Y2EvS4/iz5Q3GMEb\n/J5FVduXkm+NClL+6qAn4xHGpwGsa2Prpe8f9UZTCCiwwfT9IxvRpe/oTeE9G3VK\nUIb2ZHo1/PQSXAAxcyYAjVBHpiSW/C0iI5qqy9Lie27rkaShHA4X8xEkX0VfRRQF\n40UYnDkeEcv4yUiVBDTefvTzBpB2WihYr/FzBBkKF/9PBE+5uM8458vAmItA8vrr\nWA==\n", true);
                            AnonymousClass009.A05(A00);
                            list.add(new C130655zl(r8, str, A00, new Date(), A0l));
                        } catch (AnonymousClass1V9 | C124715pz unused) {
                            Log.e(C12960it.A0d(str, C12960it.A0k("PAY: can't construct Certificate Path - ")));
                            Log.e("PAY: NoviActionManager/downloadCertificateAndRegisterAppInstallation can't validate certificate");
                        }
                    }
                    C129655y5 r112 = r4.A0B.A00;
                    Map map = r112.A04;
                    map.clear();
                    r112.A00().edit().clear().apply();
                    for (C130655zl r1 : list) {
                        C127675us r7 = r1.A01;
                        JSONArray A0L = C117315Zl.A0L();
                        A0L.put(r7.A01.A02());
                        for (Certificate certificate : r1.A04) {
                            try {
                                A0L.put(C117305Zk.A0n(certificate.getEncoded()));
                            } catch (CertificateException unused2) {
                                Log.e("Failed encoding the certificate");
                            }
                        }
                        if (A0L.length() > 0) {
                            SharedPreferences.Editor edit = r112.A00().edit();
                            String str2 = r1.A02;
                            edit.putString(C12960it.A0d(str2, C12960it.A0k("service.")), A0L.toString());
                            edit.apply();
                            map.put(str2, r7);
                        }
                    }
                    C127675us A01 = r112.A01("AUTH");
                    C127675us A012 = r112.A01("GATEWAY");
                    C127675us A013 = r112.A01("MEDIA");
                    C127675us A014 = r112.A01("WALLET_CORE");
                    ArrayList A0l2 = C12960it.A0l();
                    if (A01 == null) {
                        A0l2.add("AUTH");
                    }
                    if (A012 == null) {
                        A0l2.add("GATEWAY");
                    }
                    if (A013 == null) {
                        A0l2.add("MEDIA");
                    }
                    if (A014 == null) {
                        A0l2.add("WALLET_CORE");
                    }
                    if (A0l2.isEmpty()) {
                        r4.A0L.set(true);
                        r4.A08(r32);
                        return;
                    }
                    Log.e(C12960it.A0d(Arrays.toString(A0l2.toArray()), C12960it.A0k("PAY: missing certificates: ")));
                }
                C130785zy.A04(r14.A00, r32, null);
            }
        }, A0F, null, "get", 0);
    }

    public final void A08(AbstractC136196Lo r20) {
        String A0d;
        int i;
        AnonymousClass61S[] r2 = new AnonymousClass61S[3];
        AnonymousClass61S.A04("action", "novi-register-app-installation", r2);
        r2[1] = AnonymousClass61S.A00("app_id", AnonymousClass029.A0B);
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("app_version", "2.22.17.70"), r2, 2);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 25) {
            A0d = Settings.Global.getString(this.A06.A00.getContentResolver(), "device_name");
        } else {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(Build.MANUFACTURER);
            A0h.append(" ");
            A0d = C12960it.A0d(Build.MODEL, A0h);
        }
        String str = this.A0G.A00().A01;
        Context context = this.A06.A00;
        String obj = C04100Kj.A00(C12980iv.A0H(context)).A00.AAS(0).toString();
        String str2 = Build.VERSION.RELEASE;
        String str3 = Build.BRAND;
        String str4 = Build.MODEL;
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        String id = timeZone.getID();
        ContentResolver contentResolver = context.getContentResolver();
        if (i2 >= 17) {
            i = Settings.Global.getInt(contentResolver, "auto_time_zone", 0);
        } else {
            i = Settings.System.getInt(contentResolver, "auto_time_zone", 0);
        }
        boolean A1V = C12960it.A1V(i, 1);
        boolean inDaylightTime = timeZone.inDaylightTime(new Date());
        boolean hasSystemFeature = context.getPackageManager().hasSystemFeature("android.hardware.camera.front");
        boolean hasSystemFeature2 = context.getPackageManager().hasSystemFeature("android.hardware.camera");
        boolean A03 = new AnonymousClass02r(context).A03();
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        AnonymousClass61S[] r1 = new AnonymousClass61S[14];
        AnonymousClass61S.A04("device_name", A0d, r1);
        r1[1] = AnonymousClass61S.A00("family_device_id", str);
        AnonymousClass61S.A05("os", "ANDROID", r1, 2);
        AnonymousClass61S.A05("device_locale", obj, r1, 3);
        AnonymousClass61S.A05("os_version", str2, r1, 4);
        AnonymousClass61S.A05("device_brand", str3, r1, 5);
        AnonymousClass61S.A05("device_model", str4, r1, 6);
        AnonymousClass61S.A05("device_timezone", id, r1, 7);
        r1[8] = new AnonymousClass61S("is_device_timezone_auto", A1V);
        r1[9] = new AnonymousClass61S("is_dst_enabled", inDaylightTime);
        r1[10] = new AnonymousClass61S("has_front_camera", hasSystemFeature);
        r1[11] = new AnonymousClass61S("has_back_camera", hasSystemFeature2);
        r1[12] = new AnonymousClass61S("is_push_notification_setting_enabled", A03);
        C117305Zk.A1K(A0F, "device_property", C12960it.A0m(AnonymousClass61S.A00("android_id", string), r1, 13));
        A0A(new AbstractC136196Lo(r20, this) { // from class: X.69s
            public final /* synthetic */ AbstractC136196Lo A00;
            public final /* synthetic */ C130155yt A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r11) {
                Object obj2;
                C130155yt r4 = this.A01;
                AbstractC136196Lo r3 = this.A00;
                if (r11.A06() && (obj2 = r11.A02) != null) {
                    r4.A0K.set(true);
                    try {
                        AnonymousClass1V8 r5 = (AnonymousClass1V8) obj2;
                        AnonymousClass1V8 A0E = r5.A0E("logging_key");
                        AnonymousClass1V8 A0E2 = r5.A0E("env");
                        if (A0E != null) {
                            String A0H = A0E.A0H("key");
                            String A0H2 = A0E.A0H("seed");
                            String A0H3 = A0E.A0H("root_key_id");
                            C130105yo r9 = r4.A0D;
                            C127395uQ r8 = new C127395uQ(Base64.decode(A0H, 0), Base64.decode(A0H2, 0), Base64.decode(A0H3, 0));
                            C12970iu.A1D(C130105yo.A01(r9).putString("wavi_event_log_key", Base64.encodeToString(r8.A00, 0)).putString("wavi_event_log_key_seed", Base64.encodeToString(r8.A01, 0)), "wavi_event_log_root_key_id", Base64.encodeToString(r8.A02, 0));
                        }
                        if (A0E2 != null) {
                            String A0H4 = A0E2.A0H("tier");
                            r4.A00 = A0H4;
                            if ("novi.wallet_core.prod".equals(A0H4) || "novi.wallet_core.prod_intern".equals(A0H4)) {
                                for (C130655zl r0 : r4.A0I) {
                                    if (!r0.A05) {
                                        Log.e("PAY: NoviActionManager/registerAppInstallation can't validate common name for certificate");
                                        C130785zy.A04(C117305Zk.A0L(), r3, null);
                                        r4.A06();
                                        return;
                                    }
                                }
                            }
                        }
                        C130785zy.A04(null, r3, Boolean.TRUE);
                        return;
                    } catch (AnonymousClass1V9 unused) {
                        Log.e("PAY: NoviActionManager/registerAppInstallation Failed to store logging keys");
                    }
                }
                C130785zy.A04(C117305Zk.A0L(), r3, null);
            }
        }, A0F, null, "set", 2);
    }

    public void A09(AbstractC136196Lo r16, C1310460z r17, Integer num, String str, int i) {
        if (this.A0K.get()) {
            A0A(r16, r17, num, str, i);
            return;
        }
        synchronized (this) {
            this.A0J.add(new C127815v6(r16, r17, num, str, i));
            if (this.A0M.compareAndSet(false, true)) {
                A07(new AbstractC136196Lo() { // from class: X.69q
                    @Override // X.AbstractC136196Lo
                    public final void AV8(C130785zy r12) {
                        C130155yt r5 = C130155yt.this;
                        Set<C127815v6> set = r5.A0J;
                        for (C127815v6 r1 : set) {
                            if (r12.A06()) {
                                String str2 = r1.A04;
                                int i2 = r1.A00;
                                r5.A0A(r1.A01, r1.A02, r1.A03, str2, i2);
                            } else {
                                C130785zy.A03(r12.A00, r1.A01);
                            }
                        }
                        set.clear();
                        AtomicBoolean atomicBoolean = r5.A0M;
                        synchronized (atomicBoolean) {
                            atomicBoolean.set(false);
                            atomicBoolean.notifyAll();
                        }
                    }
                });
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005d, code lost:
        if (r8.A02.A0H() == false) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0089, code lost:
        if (r24 == 0) goto L_0x0090;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0A(X.AbstractC136196Lo r20, X.C1310460z r21, java.lang.Integer r22, java.lang.String r23, int r24) {
        /*
            r19 = this;
            java.util.UUID r0 = java.util.UUID.randomUUID()
            r0.toString()
            r2 = 0
            r10 = r19
            r9 = r20
            X.5ut r5 = new X.5ut     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            r5.<init>()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            r4 = r21
            java.util.ArrayList r7 = r4.A01     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.util.Iterator r3 = r7.iterator()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
        L_0x0019:
            boolean r0 = r3.hasNext()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            if (r0 == 0) goto L_0x0031
            java.lang.Object r0 = r3.next()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            X.61S r0 = (X.AnonymousClass61S) r0     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.lang.String r1 = r0.A03     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.lang.String r0 = "client_request_id"
            boolean r0 = r0.equals(r1)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            if (r0 == 0) goto L_0x0019
            r6 = 0
            goto L_0x0032
        L_0x0031:
            r6 = 1
        L_0x0032:
            X.600 r8 = r10.A0A     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.lang.String r1 = r10.A05()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            X.0pI r3 = r10.A06     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.util.Locale r0 = X.C1310561a.A02(r3)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.lang.String r0 = r0.toString()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            r12 = r24
            java.util.List r0 = r8.A00(r1, r0, r12, r6)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.util.ArrayList r0 = X.C12980iv.A0x(r0)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            r7.addAll(r0)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            if (r24 < 0) goto L_0x008b
            r0 = 2
            if (r12 <= r0) goto L_0x005f
            r0 = 4
            if (r12 != r0) goto L_0x0089
            X.61F r0 = r8.A02     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            boolean r0 = r0.A0H()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            if (r0 != 0) goto L_0x008b
        L_0x005f:
            java.util.ArrayList r6 = r4.A02     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.util.Iterator r7 = r6.iterator()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
        L_0x0065:
            boolean r0 = r7.hasNext()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            r1 = 0
            if (r0 == 0) goto L_0x007d
            java.lang.Object r0 = r7.next()     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            X.60z r0 = (X.C1310460z) r0     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.lang.String r1 = r0.A00     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            java.lang.String r0 = "encryption_key_request"
            boolean r0 = r0.equals(r1)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            if (r0 == 0) goto L_0x0065
            goto L_0x0089
        L_0x007d:
            X.5yq r0 = r8.A01     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            if (r24 == 0) goto L_0x0082
            r1 = 1
        L_0x0082:
            X.60z r0 = X.AnonymousClass61L.A02(r0, r1)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            r6.add(r0)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
        L_0x0089:
            if (r24 == 0) goto L_0x0090
        L_0x008b:
            java.lang.String r0 = r4.A00     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            r4.A01(r5, r0)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
        L_0x0090:
            X.5yq r0 = r10.A0B     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            X.C1308960j.A02(r4, r0, r5)     // Catch: 5px -> 0x00b1, 5q1 -> 0x00c6
            X.0sj r13 = r10.A08
            X.1V8 r15 = r4.A00()
            android.content.Context r6 = r3.A00
            X.0mE r7 = r10.A03
            X.0sn r8 = r10.A07
            r11 = r22
            X.5fu r5 = new X.5fu
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            long r17 = X.C125195qr.A00
            r16 = r23
            r14 = r5
            r13.A0F(r14, r15, r16, r17)
            return
        L_0x00b1:
            X.61F r0 = r10.A0C
            X.5au r0 = r0.A0G
            r0.A0A(r2)
            r1 = 542720003(0x20594003, float:1.8401795E-19)
            X.20p r0 = new X.20p
            r0.<init>(r1)
            X.C130785zy.A04(r0, r9, r2)
            java.lang.String r0 = "PAY: failed to inject auth fields"
            goto L_0x00cf
        L_0x00c6:
            X.20p r0 = X.C117305Zk.A0L()
            X.C130785zy.A04(r0, r9, r2)
            java.lang.String r0 = "PAY: failed to encrypt fields"
        L_0x00cf:
            com.whatsapp.util.Log.e(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C130155yt.A0A(X.6Lo, X.60z, java.lang.Integer, java.lang.String, int):void");
    }

    public void A0B(AbstractC136196Lo r7, C1310460z r8, String str, int i) {
        A09(r7, r8, null, str, i);
    }
}
