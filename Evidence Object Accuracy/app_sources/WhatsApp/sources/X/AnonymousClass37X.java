package X;

import android.widget.TextView;
import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.37X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37X extends AbstractC16350or {
    public final AnonymousClass018 A00;
    public final C14410lO A01;
    public final WeakReference A02;
    public final List A03;

    public AnonymousClass37X(TextView textView, AnonymousClass018 r3, C14410lO r4, List list) {
        this.A01 = r4;
        this.A00 = r3;
        this.A03 = list;
        this.A02 = C12970iu.A10(textView);
    }
}
