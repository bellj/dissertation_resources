package X;

/* renamed from: X.3Zk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69483Zk implements AbstractC29031Pz {
    public final /* synthetic */ AnonymousClass3DS A00;
    public final /* synthetic */ C15370n3 A01;

    @Override // X.AbstractC29031Pz
    public String ADy() {
        return "type_of_chat";
    }

    public C69483Zk(AnonymousClass3DS r1, C15370n3 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC29031Pz
    public void A98(C28631Oi r4) {
        String str;
        C15370n3 r1 = this.A01;
        if (r1.A0K()) {
            int A00 = this.A00.A04.A00((AbstractC15590nW) C15370n3.A03(r1, C15580nU.class));
            str = A00 < 32 ? "GROUP_32" : A00 < 64 ? "GROUP_64" : A00 < 128 ? "GROUP_128" : A00 < 256 ? "GROUP_256" : A00 < 512 ? "GROUP_512" : "GROUP_MORE_512";
        } else {
            str = "one_to_one";
        }
        r4.A00(str.length(), "type_of_chat", str);
    }
}
