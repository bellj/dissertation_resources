package X;

import java.util.HashMap;

/* renamed from: X.02A  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass02A {
    public final AbstractC009404s A00;
    public final AnonymousClass05C A01;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass02A(X.AbstractC001400p r3) {
        /*
            r2 = this;
            X.05C r1 = r3.AHb()
            boolean r0 = r3 instanceof X.AbstractC001800t
            if (r0 == 0) goto L_0x0012
            X.00t r3 = (X.AbstractC001800t) r3
            X.04s r0 = r3.ACV()
        L_0x000e:
            r2.<init>(r0, r1)
            return
        L_0x0012:
            X.0Yo r0 = X.AnonymousClass0Yo.A00
            if (r0 != 0) goto L_0x000e
            X.0Yo r0 = new X.0Yo
            r0.<init>()
            X.AnonymousClass0Yo.A00 = r0
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02A.<init>(X.00p):void");
    }

    public AnonymousClass02A(AbstractC009404s r2, AbstractC001400p r3) {
        this(r2, r3.AHb());
    }

    public AnonymousClass02A(AbstractC009404s r1, AnonymousClass05C r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public AnonymousClass015 A00(Class cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            StringBuilder sb = new StringBuilder("androidx.lifecycle.ViewModelProvider.DefaultKey:");
            sb.append(canonicalName);
            String obj = sb.toString();
            HashMap hashMap = this.A01.A00;
            AnonymousClass015 r2 = (AnonymousClass015) hashMap.get(obj);
            boolean isInstance = cls.isInstance(r2);
            AbstractC009404s r1 = this.A00;
            if (!isInstance) {
                if (r1 instanceof AnonymousClass05F) {
                    r2 = ((AnonymousClass05F) r1).A01(cls, obj);
                } else {
                    r2 = r1.A7r(cls);
                }
                AnonymousClass015 r0 = (AnonymousClass015) hashMap.put(obj, r2);
                if (r0 != null) {
                    r0.A03();
                    return r2;
                }
            } else if (r1 instanceof AnonymousClass05G) {
                ((AnonymousClass05G) r1).A00(r2);
            }
            return r2;
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
}
