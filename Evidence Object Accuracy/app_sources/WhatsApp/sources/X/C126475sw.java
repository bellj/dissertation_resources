package X;

/* renamed from: X.5sw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126475sw {
    public final AnonymousClass1V8 A00;

    public C126475sw(AnonymousClass3CS r12, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-check-balance");
        if (C117295Zj.A1W(str, 1, false)) {
            C41141sy.A01(A0N, "credential-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 35, 35, false)) {
            C41141sy.A01(A0N, "seq-no", str2);
        }
        if (C117295Zj.A1V(str3, 1, false)) {
            C41141sy.A01(A0N, "device-id", str3);
        }
        if (C117295Zj.A1W(str4, 0, false)) {
            C41141sy.A01(A0N, "mpin", str4);
        }
        if (AnonymousClass3JT.A0E(str5, 1, 100, false)) {
            C41141sy.A01(A0N, "vpa", str5);
        }
        if (str6 != null && AnonymousClass3JT.A0E(str6, 1, 100, true)) {
            C41141sy.A01(A0N, "vpa-id", str6);
        }
        if (C117295Zj.A1Y(str7, false)) {
            C41141sy.A01(A0N, "upi-bank-info", str7);
        }
        this.A00 = C117295Zj.A0I(A0N, A0M, r12);
    }
}
