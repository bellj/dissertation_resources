package X;

/* renamed from: X.4xF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C107384xF implements AnonymousClass5SM {
    public final /* synthetic */ C100614mC A00;

    public /* synthetic */ C107384xF(C100614mC r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5SM
    public final int AGR(Object obj) {
        try {
            return ((C95494dp) obj).A0B(this.A00) ? 1 : 0;
        } catch (C87424Bl unused) {
            return -1;
        }
    }
}
