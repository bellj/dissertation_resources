package X;

/* renamed from: X.0S9  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0S9 {
    public static final AnonymousClass0S9 A01 = new AnonymousClass0S9("FLAT");
    public static final AnonymousClass0S9 A02 = new AnonymousClass0S9("HALF_OPENED");
    public final String A00;

    public AnonymousClass0S9(String str) {
        this.A00 = str;
    }

    public String toString() {
        return this.A00;
    }
}
