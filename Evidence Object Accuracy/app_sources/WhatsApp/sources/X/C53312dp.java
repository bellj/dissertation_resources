package X;

import android.content.Context;
import android.content.DialogInterface;

/* renamed from: X.2dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53312dp extends C004802e {
    public final DialogInterface$OnClickListenerC97374gx A00 = new DialogInterface$OnClickListenerC97374gx();
    public final DialogInterface$OnDismissListenerC97604hL A01 = new DialogInterface$OnDismissListenerC97604hL();

    public C53312dp(Context context) {
        super(context);
    }

    @Override // X.C004802e
    @Deprecated
    public C004802e A00(int i, DialogInterface.OnClickListener onClickListener) {
        super.A00(i, onClickListener);
        return this;
    }

    @Override // X.C004802e
    @Deprecated
    public C004802e A01(DialogInterface.OnClickListener onClickListener, CharSequence charSequence) {
        super.A01(onClickListener, charSequence);
        return this;
    }

    @Override // X.C004802e
    @Deprecated
    public C004802e A02(DialogInterface.OnClickListener onClickListener, CharSequence charSequence) {
        super.A02(onClickListener, charSequence);
        return this;
    }

    @Override // X.C004802e
    @Deprecated
    public C004802e A03(DialogInterface.OnClickListener onClickListener, CharSequence charSequence) {
        super.A03(onClickListener, charSequence);
        return this;
    }

    @Override // X.C004802e
    public C004802e A04(DialogInterface.OnDismissListener onDismissListener) {
        super.A04(onDismissListener);
        return this;
    }

    public void A0C(AbstractC001200n r2, AnonymousClass02B r3) {
        DialogInterface$OnDismissListenerC97604hL r0 = this.A01;
        super.A08(r0);
        r0.A00.A05(r2, r3);
    }

    public void A0D(AbstractC001200n r2, AnonymousClass02B r3, int i) {
        DialogInterface$OnClickListenerC97374gx r0 = this.A00;
        super.setNegativeButton(i, r0);
        if (r3 != null) {
            r0.A00.A05(r2, r3);
        }
    }

    public void A0E(AbstractC001200n r2, AnonymousClass02B r3, int i) {
        DialogInterface$OnClickListenerC97374gx r0 = this.A00;
        super.setPositiveButton(i, r0);
        r0.A02.A05(r2, r3);
    }

    public void A0F(AbstractC001200n r2, AnonymousClass02B r3, CharSequence charSequence) {
        DialogInterface$OnClickListenerC97374gx r0 = this.A00;
        super.A01(r0, charSequence);
        r0.A00.A05(r2, r3);
    }

    public void A0G(AbstractC001200n r2, AnonymousClass02B r3, CharSequence charSequence) {
        DialogInterface$OnClickListenerC97374gx r0 = this.A00;
        super.A03(r0, charSequence);
        r0.A02.A05(r2, r3);
    }

    @Override // X.C004802e
    public AnonymousClass04S create() {
        if (this.A01.A01.A02.A00 <= 0) {
            super.A04(null);
        }
        return super.create();
    }

    @Override // X.C004802e
    @Deprecated
    public C004802e setNegativeButton(int i, DialogInterface.OnClickListener onClickListener) {
        super.setNegativeButton(i, onClickListener);
        return this;
    }

    @Override // X.C004802e
    @Deprecated
    public C004802e setPositiveButton(int i, DialogInterface.OnClickListener onClickListener) {
        super.setPositiveButton(i, onClickListener);
        return this;
    }
}
