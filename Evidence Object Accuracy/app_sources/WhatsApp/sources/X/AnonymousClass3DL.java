package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.protocol.VoipStanzaChildNode;
import java.util.Map;

/* renamed from: X.3DL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DL {
    public final byte A00;
    public final Jid A01;
    public final VoipStanzaChildNode A02;
    public final VoipStanzaChildNode A03;
    public final String A04;
    public final String A05;
    public final Map A06;

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0075, code lost:
        r1 = com.whatsapp.jid.DeviceJid.of(r2.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007b, code lost:
        if (r1 == null) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        r0 = X.AnonymousClass1SF.A04(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0081, code lost:
        if (r0 == null) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0083, code lost:
        r0 = r0.getDataCopy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0087, code lost:
        r7.put(r1, r0);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008d, code lost:
        r0 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass3DL(com.whatsapp.jid.Jid r14, com.whatsapp.protocol.VoipStanzaChildNode r15, java.lang.String r16, java.lang.String r17) {
        /*
            r13 = this;
            r13.<init>()
            boolean r0 = X.C15380n4.A0I(r14)
            if (r0 == 0) goto L_0x00d2
            r13.A01 = r14
            r0 = r16
            r13.A04 = r0
            r13.A03 = r15
            r0 = r17
            r13.A05 = r0
            java.lang.String r6 = "destination"
            com.whatsapp.protocol.VoipStanzaChildNode[] r5 = r15.getChildrenCopy()
            r4 = 0
            if (r5 == 0) goto L_0x002d
            int r3 = r5.length
            r2 = 0
        L_0x0020:
            if (r2 >= r3) goto L_0x002d
            r1 = r5[r2]
            java.lang.String r0 = r1.tag
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x009d
            r4 = r1
        L_0x002d:
            r13.A02 = r4
            r6 = 0
            if (r4 == 0) goto L_0x00a0
            com.whatsapp.protocol.VoipStanzaChildNode[] r9 = r4.getChildrenCopy()
            r8 = 0
            if (r9 != 0) goto L_0x004c
            java.lang.String r0 = "voip/voipUtil/getRawKeysFromDestination no children under destination"
            com.whatsapp.util.Log.i(r0)
            java.lang.String r0 = "no <dest> node"
        L_0x0041:
            X.AnonymousClass009.A0A(r0, r6)
        L_0x0044:
            X.AnonymousClass009.A05(r8)
            r13.A06 = r8
            r13.A00 = r6
            return
        L_0x004c:
            java.util.HashMap r7 = X.C12970iu.A11()
            int r5 = r9.length
            r4 = 0
        L_0x0052:
            if (r4 >= r5) goto L_0x009b
            r12 = r9[r4]
            X.1W9[] r11 = r12.getAttributesCopy()
            if (r11 != 0) goto L_0x0065
            java.lang.String r0 = "voip/voipUtil/getRawKeysFromDestination no attribute in <to>"
            com.whatsapp.util.Log.e(r0)
            java.lang.String r0 = "no attr in <to>"
            goto L_0x0041
        L_0x0065:
            int r10 = r11.length
            r3 = 0
        L_0x0067:
            if (r3 >= r10) goto L_0x0092
            r2 = r11[r3]
            java.lang.String r1 = r2.A02
            java.lang.String r0 = "jid"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x008f
            com.whatsapp.jid.Jid r0 = r2.A01
            com.whatsapp.jid.DeviceJid r1 = com.whatsapp.jid.DeviceJid.of(r0)
            if (r1 == 0) goto L_0x0092
            com.whatsapp.protocol.VoipStanzaChildNode r0 = X.AnonymousClass1SF.A04(r12)
            if (r0 == 0) goto L_0x008d
            byte[] r0 = r0.getDataCopy()
        L_0x0087:
            r7.put(r1, r0)
            int r4 = r4 + 1
            goto L_0x0052
        L_0x008d:
            r0 = r8
            goto L_0x0087
        L_0x008f:
            int r3 = r3 + 1
            goto L_0x0067
        L_0x0092:
            java.lang.String r0 = "voip/voipUtil/getRawKeysFromDestination no deviceJid in <to>"
            com.whatsapp.util.Log.e(r0)
            java.lang.String r0 = "no deviceJid in <to>"
            goto L_0x0041
        L_0x009b:
            r8 = r7
            goto L_0x0044
        L_0x009d:
            int r2 = r2 + 1
            goto L_0x0020
        L_0x00a0:
            java.lang.Byte r0 = java.lang.Byte.valueOf(r6)
            r4 = r0
            java.util.HashMap r3 = X.C12970iu.A11()
            com.whatsapp.protocol.VoipStanzaChildNode r2 = X.AnonymousClass1SF.A04(r15)
            if (r2 == 0) goto L_0x00c9
            com.whatsapp.jid.DeviceJid r1 = com.whatsapp.jid.DeviceJid.of(r14)
            X.AnonymousClass009.A05(r1)
            byte[] r0 = r2.getDataCopy()
            r3.put(r1, r0)
            java.lang.Byte r0 = X.AnonymousClass1SF.A08(r2)
            if (r0 != 0) goto L_0x00c9
            java.lang.String r0 = "invalid retry count!"
            X.AnonymousClass009.A07(r0)
            r0 = r4
        L_0x00c9:
            byte r0 = r0.byteValue()
            r13.A00 = r0
            r13.A06 = r3
            return
        L_0x00d2:
            java.lang.String r0 = "CallOfferStanza: Wrong jid type: "
            java.lang.String r0 = X.C12960it.A0b(r0, r14)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3DL.<init>(com.whatsapp.jid.Jid, com.whatsapp.protocol.VoipStanzaChildNode, java.lang.String, java.lang.String):void");
    }

    public String toString() {
        String str;
        StringBuilder A0k = C12960it.A0k("jid=");
        A0k.append(this.A01);
        A0k.append(" callId=");
        A0k.append(this.A04);
        A0k.append(" payload=");
        A0k.append(this.A03);
        A0k.append(" format=");
        if (this.A02 != null) {
            str = "fan-out";
        } else {
            str = "legacy";
        }
        return C12960it.A0d(str, A0k);
    }
}
