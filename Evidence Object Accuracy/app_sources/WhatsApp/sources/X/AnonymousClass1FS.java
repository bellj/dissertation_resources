package X;

import android.text.TextUtils;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.1FS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FS implements AbstractC15920o8 {
    public final AbstractC15710nm A00;
    public final C14830m7 A01;
    public final C19990v2 A02;
    public final C247616t A03;
    public final C14850m9 A04;
    public final C20710wC A05;
    public final C20660w7 A06;
    public final C17230qT A07;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{209};
    }

    public AnonymousClass1FS(AbstractC15710nm r1, C14830m7 r2, C19990v2 r3, C247616t r4, C14850m9 r5, C20710wC r6, C20660w7 r7, C17230qT r8) {
        this.A01 = r2;
        this.A04 = r5;
        this.A00 = r1;
        this.A02 = r3;
        this.A06 = r7;
        this.A05 = r6;
        this.A07 = r8;
        this.A03 = r4;
    }

    public final Set A00(AnonymousClass1V8 r14, int i) {
        long j;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        r14.A0I("link_type", null);
        for (AnonymousClass1V8 r5 : r14.A0J("group")) {
            GroupJid groupJid = (GroupJid) r5.A0A(this.A00, C15580nU.class, "jid");
            String str = "";
            try {
                str = r5.A0I("subject", null);
                j = ((long) r5.A05("subject_ts", 0)) * 1000;
            } catch (AnonymousClass1V9 e) {
                Log.e("cannot get group subject from notification", e);
                j = 0;
            }
            if (groupJid != null && !TextUtils.isEmpty(str)) {
                linkedHashSet.add(new AnonymousClass1OU(groupJid, str, i, j));
            }
        }
        return linkedHashSet;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0065 A[Catch: all -> 0x008f, TRY_ENTER, TRY_LEAVE, TryCatch #5 {, blocks: (B:4:0x0016, B:6:0x001c, B:7:0x0026, B:10:0x0029, B:21:0x0068, B:11:0x003e, B:20:0x0065, B:32:0x008e), top: B:41:0x0016 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A01(X.C15580nU r14, X.AnonymousClass1V8 r15) {
        /*
            r13 = this;
            java.lang.String r0 = "prev_v_id"
            r1 = 0
            long r11 = r15.A08(r0, r1)
            java.lang.String r0 = "v_id"
            long r9 = r15.A08(r0, r1)
            X.16t r0 = r13.A03
            X.171 r7 = r0.A00
            java.util.Map r5 = r7.A03
            monitor-enter(r5)
            boolean r0 = r5.containsKey(r14)     // Catch: all -> 0x0096
            if (r0 == 0) goto L_0x0028
            java.lang.Object r0 = r5.get(r14)     // Catch: all -> 0x0096
            java.lang.Long r0 = (java.lang.Long) r0     // Catch: all -> 0x0096
            long r3 = r0.longValue()     // Catch: all -> 0x0096
        L_0x0026:
            monitor-exit(r5)     // Catch: all -> 0x0096
            goto L_0x0079
        L_0x0028:
            r0 = 1
            java.lang.String[] r8 = new java.lang.String[r0]     // Catch: all -> 0x0094
            r6 = 0
            X.0sU r0 = r7.A00     // Catch: all -> 0x0094
            long r3 = r0.A01(r14)     // Catch: all -> 0x0094
            java.lang.String r0 = java.lang.Long.toString(r3)     // Catch: all -> 0x0094
            r8[r6] = r0     // Catch: all -> 0x0094
            X.0p7 r0 = r7.A01     // Catch: all -> 0x0094
            X.0on r7 = r0.get()     // Catch: all -> 0x0094
            X.0op r3 = r7.A03     // Catch: all -> 0x008f
            java.lang.String r0 = "SELECT participant_version FROM group_notification_version WHERE group_jid_row_id = ?"
            android.database.Cursor r6 = r3.A09(r0, r8)     // Catch: all -> 0x008f
            if (r6 == 0) goto L_0x0049
            goto L_0x004c
        L_0x0049:
            r3 = 0
            goto L_0x005c
        L_0x004c:
            boolean r0 = r6.moveToNext()     // Catch: all -> 0x0088
            if (r0 == 0) goto L_0x0049
            java.lang.String r0 = "participant_version"
            int r0 = r6.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0088
            long r3 = r6.getLong(r0)     // Catch: all -> 0x0088
        L_0x005c:
            java.lang.Long r0 = java.lang.Long.valueOf(r3)     // Catch: all -> 0x0088
            r5.put(r14, r0)     // Catch: all -> 0x0088
            if (r6 == 0) goto L_0x0068
            r6.close()     // Catch: all -> 0x008f
        L_0x0068:
            r7.close()     // Catch: all -> 0x0094
            java.lang.Object r0 = r5.get(r14)     // Catch: all -> 0x0094
            java.lang.Long r0 = (java.lang.Long) r0     // Catch: all -> 0x0094
            X.AnonymousClass009.A05(r0)     // Catch: all -> 0x0094
            long r3 = r0.longValue()     // Catch: all -> 0x0094
            goto L_0x0026
        L_0x0079:
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0086
            int r0 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0086
            int r1 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x0087
        L_0x0086:
            r0 = 1
        L_0x0087:
            return r0
        L_0x0088:
            r0 = move-exception
            if (r6 == 0) goto L_0x008e
            r6.close()     // Catch: all -> 0x008e
        L_0x008e:
            throw r0     // Catch: all -> 0x008f
        L_0x008f:
            r0 = move-exception
            r7.close()     // Catch: all -> 0x0093
        L_0x0093:
            throw r0     // Catch: all -> 0x0094
        L_0x0094:
            r0 = move-exception
            throw r0     // Catch: all -> 0x0096
        L_0x0096:
            r0 = move-exception
            monitor-exit(r5)     // Catch: all -> 0x0096
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1FS.A01(X.0nU, X.1V8):boolean");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:850:0x15e7 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v18, types: [X.0mz] */
    /* JADX WARN: Type inference failed for: r7v7, types: [X.11C] */
    /* JADX WARN: Type inference failed for: r7v10, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r0v99, types: [X.0mz, X.1Xo] */
    /* JADX WARN: Type inference failed for: r7v48, types: [java.lang.Object, X.0wC] */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AbstractC15920o8
    public boolean AI8(android.os.Message r79, int r80) {
        /*
        // Method dump skipped, instructions count: 7134
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1FS.AI8(android.os.Message, int):boolean");
    }
}
