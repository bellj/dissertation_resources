package X;

import android.os.Bundle;
import android.os.Looper;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

/* renamed from: X.0Q3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Q3 {
    public final AbstractC001200n A00;
    public final AnonymousClass0EO A01;

    public AnonymousClass0Q3(AbstractC001200n r3, AnonymousClass05C r4) {
        this.A00 = r3;
        this.A01 = (AnonymousClass0EO) new AnonymousClass02A(AnonymousClass0EO.A02, r4).A00(AnonymousClass0EO.class);
    }

    public void A00(Bundle bundle, AnonymousClass03M r6) {
        AnonymousClass0EO r2 = this.A01;
        if (r2.A01) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            AnonymousClass0EN r1 = (AnonymousClass0EN) r2.A00.A01(0);
            AnonymousClass0QL r0 = null;
            if (r1 != null) {
                r0 = r1.A0C(false);
            }
            A01(bundle, r6, r0);
        } else {
            throw new IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    public final void A01(Bundle bundle, AnonymousClass03M r8, AnonymousClass0QL r9) {
        try {
            AnonymousClass0EO r5 = this.A01;
            r5.A01 = true;
            AnonymousClass0QL AOi = r8.AOi(bundle, 0);
            Class<?> cls = AOi.getClass();
            if (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) {
                AnonymousClass0EN r3 = new AnonymousClass0EN(bundle, AOi, r9);
                r5.A00.A03(0, r3);
                r5.A01 = false;
                AbstractC001200n r2 = this.A00;
                AnonymousClass0Ym r1 = new AnonymousClass0Ym(r8, r3.A04);
                r3.A05(r2, r1);
                AnonymousClass0Ym r0 = r3.A01;
                if (r0 != null) {
                    r3.A09(r0);
                }
                r3.A00 = r2;
                r3.A01 = r1;
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Object returned from onCreateLoader must not be a non-static inner member class: ");
            sb.append(AOi);
            throw new IllegalArgumentException(sb.toString());
        } catch (Throwable th) {
            this.A01.A01 = false;
            throw th;
        }
    }

    public void A02(AnonymousClass03M r6) {
        AnonymousClass0EO r2 = this.A01;
        if (r2.A01) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            AnonymousClass0EN r3 = (AnonymousClass0EN) r2.A00.A01(0);
            if (r3 == null) {
                A01(null, r6, null);
                return;
            }
            AbstractC001200n r22 = this.A00;
            AnonymousClass0Ym r1 = new AnonymousClass0Ym(r6, r3.A04);
            r3.A05(r22, r1);
            AnonymousClass0Ym r0 = r3.A01;
            if (r0 != null) {
                r3.A09(r0);
            }
            r3.A00 = r22;
            r3.A01 = r1;
        } else {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
    }

    @Deprecated
    public void A03(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        C013506i r5 = this.A01.A00;
        if (r5.A00 > 0) {
            printWriter.print(str);
            printWriter.println("Loaders:");
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("    ");
            String obj = sb.toString();
            for (int i = 0; i < r5.A00; i++) {
                AnonymousClass0EN r6 = (AnonymousClass0EN) r5.A02[i];
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(r5.A01[i]);
                printWriter.print(": ");
                printWriter.println(r6.toString());
                printWriter.print(obj);
                printWriter.print("mId=");
                printWriter.print(0);
                printWriter.print(" mArgs=");
                printWriter.println(r6.A03);
                printWriter.print(obj);
                printWriter.print("mLoader=");
                AnonymousClass0QL r1 = r6.A04;
                printWriter.println(r1);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(obj);
                sb2.append("  ");
                r1.A05(sb2.toString(), fileDescriptor, printWriter, strArr);
                if (r6.A01 != null) {
                    printWriter.print(obj);
                    printWriter.print("mCallbacks=");
                    printWriter.println(r6.A01);
                    AnonymousClass0Ym r12 = r6.A01;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(obj);
                    sb3.append("  ");
                    printWriter.print(sb3.toString());
                    printWriter.print("mDeliveredData=");
                    printWriter.println(r12.A00);
                }
                printWriter.print(obj);
                printWriter.print("mData=");
                Object A01 = r6.A01();
                StringBuilder sb4 = new StringBuilder(64);
                C04150Ko.A00(A01, sb4);
                sb4.append("}");
                printWriter.println(sb4.toString());
                printWriter.print(obj);
                printWriter.print("mStarted=");
                boolean z = false;
                if (((AnonymousClass017) r6).A00 > 0) {
                    z = true;
                }
                printWriter.println(z);
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        C04150Ko.A00(this.A00, sb);
        sb.append("}}");
        return sb.toString();
    }
}
