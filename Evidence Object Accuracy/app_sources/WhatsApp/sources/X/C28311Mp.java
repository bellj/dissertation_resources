package X;

import java.util.Map;

/* renamed from: X.1Mp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28311Mp<K, V> extends AbstractC17940re<Map.Entry<K, V>> {
    public final transient Object[] alternatingKeysAndValues;
    public final transient AbstractC17190qP map;
    public final transient int size;

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return true;
    }

    public C28311Mp(AbstractC17190qP r2, Object[] objArr, int i, int i2) {
        this.map = r2;
        this.alternatingKeysAndValues = objArr;
        this.size = i2;
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        Object key = entry.getKey();
        Object value = entry.getValue();
        if (value == null || !value.equals(this.map.get(key))) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        return asList().copyIntoArray(objArr, i);
    }

    @Override // X.AbstractC17940re
    public AnonymousClass1Mr createAsList() {
        return new C81023tL(this);
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public AnonymousClass1I5 iterator() {
        return asList().iterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return this.size;
    }
}
