package X;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.settings.chat.wallpaper.WallpaperPreview;

/* renamed from: X.35I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35I extends AbstractC55342iF {
    public final /* synthetic */ WallpaperPreview A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass35I(Context context, Resources resources, WallpaperPreview wallpaperPreview) {
        super(context, resources);
        this.A00 = wallpaperPreview;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return C12970iu.A09(this.A00.A0A);
    }

    @Override // X.AbstractC55342iF, X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        super.A0D(viewGroup, obj, i);
        C12980iv.A1M((AbstractC16350or) this.A00.A0E.remove(Integer.valueOf(i)));
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return C12970iu.A1Z(view, obj);
    }
}
