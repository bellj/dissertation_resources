package X;

import java.security.PrivilegedExceptionAction;

/* renamed from: X.5Bv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112045Bv implements PrivilegedExceptionAction {
    public final /* synthetic */ String A00;

    public C112045Bv(String str) {
        this.A00 = str;
    }

    @Override // java.security.PrivilegedExceptionAction
    public Object run() {
        return C94694cN.A00.getDeclaredMethod(this.A00, new Class[0]);
    }
}
