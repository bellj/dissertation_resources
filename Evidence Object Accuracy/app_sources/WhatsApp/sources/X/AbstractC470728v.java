package X;

import android.content.Context;

/* renamed from: X.28v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC470728v {
    public static final C37471mS[] A00 = new C37471mS[0];

    boolean A6v();

    AbstractC454821u A8X(Context context, AnonymousClass018 v, boolean z);

    C37471mS[] ACh();

    String AH5();

    boolean Aac();
}
