package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78143oR extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99314k6();
    public final int A00;
    public final boolean A01;

    public C78143oR(int i, boolean z) {
        this.A00 = i;
        this.A01 = z;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A09(parcel, 3, this.A01);
        C95654e8.A06(parcel, A00);
    }
}
