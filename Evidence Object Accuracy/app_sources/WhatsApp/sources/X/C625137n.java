package X;

import android.graphics.Bitmap;

/* renamed from: X.37n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625137n extends AbstractC16350or {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Bitmap A01;
    public final /* synthetic */ AnonymousClass21U A02;
    public final /* synthetic */ Runnable A03;
    public final /* synthetic */ Runnable A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C625137n(Bitmap bitmap, AbstractC001200n r2, AnonymousClass21U r3, Runnable runnable, Runnable runnable2, int i) {
        super(r2);
        this.A02 = r3;
        this.A01 = bitmap;
        this.A00 = i;
        this.A04 = runnable;
        this.A03 = runnable2;
    }
}
