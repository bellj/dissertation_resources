package X;

import java.io.Closeable;
import java.net.HttpURLConnection;

/* renamed from: X.0bt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08810bt implements Closeable {
    public final HttpURLConnection A00;

    public C08810bt(HttpURLConnection httpURLConnection) {
        this.A00 = httpURLConnection;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A00.disconnect();
    }
}
