package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0sp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC18670sp {
    public static final C32641cU A04 = new C32641cU("unset", null, false);
    public C18600si A00;
    public C17900ra A01;
    public boolean A02;
    public final C30931Zj A03 = C30931Zj.A00("BasePaymentAccountSetup", "onboarding", "COMMON");

    public synchronized C32641cU A00() {
        C32641cU r0;
        List A03 = A03(A02());
        r0 = null;
        if (!A03.isEmpty()) {
            r0 = (C32641cU) A03.get(0);
        }
        return r0;
    }

    public C32641cU A01(String str) {
        C32641cU[] r4;
        C17930rd A01 = this.A01.A01();
        if (A01 == null) {
            return null;
        }
        if (this.A02) {
            r4 = A01.A0C;
        } else {
            r4 = A01.A0B;
        }
        for (C32641cU r1 : r4) {
            if (r1.A03.equals(str)) {
                return r1;
            }
        }
        return null;
    }

    public synchronized List A02() {
        String str;
        ArrayList arrayList;
        C18600si r1 = this.A00;
        boolean z = this.A02;
        SharedPreferences A01 = r1.A01();
        if (z) {
            str = "payments_setup_completed_steps";
        } else {
            str = "payments_merchant_setup_completed_steps";
        }
        String string = A01.getString(str, "");
        arrayList = new ArrayList();
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    String string2 = jSONObject.getString(next);
                    boolean equals = string2.equals("skipped");
                    if (equals) {
                        string2 = "-1";
                    }
                    C32641cU r0 = new C32641cU(next, string2, false);
                    r0.A02 = equals;
                    arrayList.add(r0);
                }
            } catch (JSONException e) {
                this.A03.A0A("getCompletedStep threw: ", e);
            }
        }
        return arrayList;
    }

    public synchronized List A03(List list) {
        ArrayList arrayList;
        C32641cU[] r6;
        arrayList = new ArrayList();
        C17930rd A01 = this.A01.A01();
        if (A01 != null) {
            if (this.A02) {
                r6 = A01.A0C;
            } else {
                r6 = A01.A0B;
            }
            for (C32641cU r8 : r6) {
                int i = 0;
                while (true) {
                    if (i >= list.size()) {
                        i = -1;
                        break;
                    }
                    if (((C32641cU) list.get(i)).A03.equals(r8.A03)) {
                        break;
                    }
                    i++;
                }
                if (i < 0 || !((C32641cU) list.get(i)).A00.equals(r8.A00) || (((C32641cU) list.get(i)).A02 && !r8.A01)) {
                    arrayList.add(new C32641cU(r8.A03, r8.A00, r8.A01));
                }
            }
        }
        return arrayList;
    }

    public synchronized void A04() {
        this.A00.A0K(this.A02);
    }

    public synchronized void A05(C32641cU r6) {
        if (r6 != null) {
            List A02 = A02();
            Iterator it = A02.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C32641cU r2 = (C32641cU) it.next();
                if (r2.A03.equals(r6.A03)) {
                    A02.remove(r2);
                    A08(A02);
                    break;
                }
            }
        } else {
            C30931Zj r22 = this.A03;
            StringBuilder sb = new StringBuilder();
            sb.append("/removeCompletedStep step to remove cannot be null: ");
            sb.append(r6);
            r22.A06(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002f, code lost:
        r3.add(r7);
        r2 = r4.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        if (r2.hasNext() == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003c, code lost:
        r1 = (X.C32641cU) r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
        if (r1.A03.equals(r5) == false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004a, code lost:
        r4.remove(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004d, code lost:
        r2 = r6.A03;
        r1 = new java.lang.StringBuilder();
        r1.append("setCompletedStep setting step: ");
        r1.append(r7);
        r1.append(" as complete making completed steps: ");
        r1.append(r3);
        r1.append(" incomplete steps: ");
        r1.append(r4);
        r2.A06(r1.toString());
        A08(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A06(X.C32641cU r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            if (r7 == 0) goto L_0x007b
            java.lang.String r0 = "unset"
            java.lang.String r5 = r7.A03     // Catch: all -> 0x0078
            boolean r0 = r5.equals(r0)     // Catch: all -> 0x0078
            if (r0 != 0) goto L_0x007b
            java.util.List r3 = r6.A02()     // Catch: all -> 0x0078
            java.util.List r4 = r6.A03(r3)     // Catch: all -> 0x0078
            java.util.Iterator r1 = r3.iterator()     // Catch: all -> 0x0078
        L_0x001a:
            boolean r0 = r1.hasNext()     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x002f
            java.lang.Object r0 = r1.next()     // Catch: all -> 0x0078
            X.1cU r0 = (X.C32641cU) r0     // Catch: all -> 0x0078
            java.lang.String r0 = r0.A03     // Catch: all -> 0x0078
            boolean r0 = r0.equals(r5)     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x001a
            goto L_0x007b
        L_0x002f:
            r3.add(r7)     // Catch: all -> 0x0078
            java.util.Iterator r2 = r4.iterator()     // Catch: all -> 0x0078
        L_0x0036:
            boolean r0 = r2.hasNext()     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x004d
            java.lang.Object r1 = r2.next()     // Catch: all -> 0x0078
            X.1cU r1 = (X.C32641cU) r1     // Catch: all -> 0x0078
            java.lang.String r0 = r1.A03     // Catch: all -> 0x0078
            boolean r0 = r0.equals(r5)     // Catch: all -> 0x0078
            if (r0 == 0) goto L_0x0036
            r4.remove(r1)     // Catch: all -> 0x0078
        L_0x004d:
            X.1Zj r2 = r6.A03     // Catch: all -> 0x0078
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0078
            r1.<init>()     // Catch: all -> 0x0078
            java.lang.String r0 = "setCompletedStep setting step: "
            r1.append(r0)     // Catch: all -> 0x0078
            r1.append(r7)     // Catch: all -> 0x0078
            java.lang.String r0 = " as complete making completed steps: "
            r1.append(r0)     // Catch: all -> 0x0078
            r1.append(r3)     // Catch: all -> 0x0078
            java.lang.String r0 = " incomplete steps: "
            r1.append(r0)     // Catch: all -> 0x0078
            r1.append(r4)     // Catch: all -> 0x0078
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0078
            r2.A06(r0)     // Catch: all -> 0x0078
            r6.A08(r3)     // Catch: all -> 0x0078
            goto L_0x007b
        L_0x0078:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x007b:
            monitor-exit(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18670sp.A06(X.1cU):void");
    }

    public synchronized void A07(String str) {
        A06(A01(str));
    }

    public final synchronized void A08(List list) {
        String str;
        JSONObject jSONObject = new JSONObject();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C32641cU r3 = (C32641cU) it.next();
            String str2 = r3.A00;
            if (!TextUtils.isEmpty(str2)) {
                try {
                    String str3 = r3.A03;
                    if (r3.A02) {
                        str2 = "skipped";
                    }
                    jSONObject.put(str3, str2);
                } catch (JSONException e) {
                    this.A03.A0A("setCompletedStep threw: ", e);
                }
            }
        }
        C30931Zj r2 = this.A03;
        StringBuilder sb = new StringBuilder();
        sb.append("storing steps: ");
        sb.append(jSONObject);
        r2.A06(sb.toString());
        C18600si r0 = this.A00;
        boolean z = this.A02;
        String obj = jSONObject.toString();
        SharedPreferences.Editor edit = r0.A01().edit();
        if (z) {
            str = "payments_setup_completed_steps";
        } else {
            str = "payments_merchant_setup_completed_steps";
        }
        edit.putString(str, obj).apply();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
        if (A0E("tos_with_wallet") != false) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A09() {
        /*
            r2 = this;
            monitor-enter(r2)
            X.0ra r0 = r2.A01     // Catch: all -> 0x0023
            X.0rd r0 = r0.A01()     // Catch: all -> 0x0023
            r1 = 0
            if (r0 == 0) goto L_0x0021
            boolean r0 = r0.A07     // Catch: all -> 0x0023
            if (r0 != 0) goto L_0x0021
            java.lang.String r0 = "tos_no_wallet"
            boolean r0 = r2.A0E(r0)     // Catch: all -> 0x0023
            if (r0 != 0) goto L_0x0020
            java.lang.String r0 = "tos_with_wallet"
            boolean r0 = r2.A0E(r0)     // Catch: all -> 0x0023
            if (r0 == 0) goto L_0x0021
        L_0x0020:
            r1 = 1
        L_0x0021:
            monitor-exit(r2)
            return r1
        L_0x0023:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18670sp.A09():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000c, code lost:
        if (A0C() != false) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0A() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.A0B()     // Catch: all -> 0x0011
            if (r0 != 0) goto L_0x000e
            boolean r1 = r2.A0C()     // Catch: all -> 0x0011
            r0 = 0
            if (r1 == 0) goto L_0x000f
        L_0x000e:
            r0 = 1
        L_0x000f:
            monitor-exit(r2)
            return r0
        L_0x0011:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18670sp.A0A():boolean");
    }

    public synchronized boolean A0B() {
        return A0D(1);
    }

    public synchronized boolean A0C() {
        return A0D(2);
    }

    public final synchronized boolean A0D(int i) {
        C32641cU[] r4;
        C17930rd A01 = this.A01.A01();
        if (A01 != null) {
            C32641cU[] r0 = this.A02 ? A01.A0C : A01.A0B;
            if (!(r0 == null || r0.length == 0)) {
                List A02 = A02();
                if (this.A02) {
                    r4 = A01.A0C;
                } else {
                    r4 = A01.A0B;
                }
                for (C32641cU r6 : r4) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= A02.size()) {
                            i2 = -1;
                            break;
                        }
                        if (((C32641cU) A02.get(i2)).A03.equals(r6.A03)) {
                            break;
                        }
                        i2++;
                    }
                    if ((i != 1 && (i != 2 || r6.A03.equals("2fa"))) || (i2 >= 0 && ((C32641cU) A02.get(i2)).A00.equals(r6.A00))) {
                    }
                }
                return true;
            }
        }
        return false;
    }

    public boolean A0E(String str) {
        for (C32641cU r0 : A02()) {
            if (r0.A03.equals(str)) {
                return true;
            }
        }
        return false;
    }
}
