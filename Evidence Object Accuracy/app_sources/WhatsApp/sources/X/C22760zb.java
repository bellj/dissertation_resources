package X;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.PowerManager;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22760zb {
    public final C17200qQ A00;
    public final C22690zU A01;
    public final C22680zT A02;
    public final AbstractC15710nm A03;
    public final C14330lG A04;
    public final C15570nT A05;
    public final C16240og A06;
    public final C22670zS A07;
    public final C22730zY A08;
    public final C22740zZ A09;
    public final C18200s4 A0A;
    public final C22700zV A0B;
    public final C18640sm A0C;
    public final C15810nw A0D;
    public final AnonymousClass01d A0E;
    public final C16590pI A0F;
    public final C15890o4 A0G;
    public final C14820m6 A0H;
    public final C15880o3 A0I;
    public final C16490p7 A0J;
    public final C21390xL A0K;
    public final C22100yW A0L;
    public final C18290sD A0M;
    public final C14850m9 A0N;
    public final C21680xo A0O;
    public final C22050yP A0P;
    public final C21780xy A0Q;
    public final C19950uw A0R;
    public final C14840m8 A0S;
    public final C22710zW A0T;
    public final C22660zR A0U;
    public final C22720zX A0V;
    public final C15860o1 A0W;
    public final C22750za A0X;
    public final C22650zQ A0Y;
    public final C21750xv A0Z;
    public final C19930uu A0a;
    public final C17140qK A0b;
    public final C14890mD A0c;
    public final C14860mA A0d;
    public final AnonymousClass01N A0e;

    public C22760zb(C17200qQ r2, C22690zU r3, C22680zT r4, AbstractC15710nm r5, C14330lG r6, C15570nT r7, C16240og r8, C22670zS r9, C22730zY r10, C22740zZ r11, C18200s4 r12, C22700zV r13, C18640sm r14, C15810nw r15, AnonymousClass01d r16, C16590pI r17, C15890o4 r18, C14820m6 r19, C15880o3 r20, C16490p7 r21, C21390xL r22, C22100yW r23, C18290sD r24, C14850m9 r25, C21680xo r26, C22050yP r27, C21780xy r28, C19950uw r29, C14840m8 r30, C22710zW r31, C22660zR r32, C22720zX r33, C15860o1 r34, C22750za r35, C22650zQ r36, C21750xv r37, C19930uu r38, C17140qK r39, C14890mD r40, C14860mA r41, AnonymousClass01N r42) {
        this.A0F = r17;
        this.A0N = r25;
        this.A03 = r5;
        this.A05 = r7;
        this.A0a = r38;
        this.A04 = r6;
        this.A0c = r40;
        this.A0d = r41;
        this.A0D = r15;
        this.A0Z = r37;
        this.A0Y = r36;
        this.A00 = r2;
        this.A0U = r32;
        this.A0O = r26;
        this.A07 = r9;
        this.A0E = r16;
        this.A02 = r4;
        this.A0e = r42;
        this.A06 = r8;
        this.A0W = r34;
        this.A0P = r27;
        this.A0S = r30;
        this.A0K = r22;
        this.A0I = r20;
        this.A01 = r3;
        this.A0J = r21;
        this.A0B = r13;
        this.A0G = r18;
        this.A0H = r19;
        this.A0T = r31;
        this.A0V = r33;
        this.A0b = r39;
        this.A0L = r23;
        this.A0R = r29;
        this.A0M = r24;
        this.A0C = r14;
        this.A0A = r12;
        this.A08 = r10;
        this.A0Q = r28;
        this.A09 = r11;
        this.A0X = r35;
    }

    public static String A00(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                Log.e("findmissingpermissions/no-package-manager");
                return "";
            }
            PackageInfo packageInfo = packageManager.getPackageInfo("com.whatsapp", 4096);
            StringBuilder sb = new StringBuilder();
            String[] strArr = packageInfo.requestedPermissions;
            if (strArr != null) {
                int i = 0;
                while (true) {
                    int[] iArr = packageInfo.requestedPermissionsFlags;
                    if (i >= iArr.length) {
                        break;
                    }
                    if ((iArr[i] & 2) == 0) {
                        if (sb.length() != 0) {
                            sb.append(", ");
                        }
                        sb.append(strArr[i]);
                    }
                    i++;
                }
            }
            return sb.toString();
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(e);
            return "";
        }
    }

    public static void A01(Object obj, Object obj2, String str) {
        if (obj2 instanceof JSONObject) {
            try {
                ((JSONObject) obj2).put(str, obj);
            } catch (JSONException e) {
                Log.e("debug-builder/json/error ", e);
            }
        } else if (obj2 instanceof StringBuilder) {
            StringBuilder sb = (StringBuilder) obj2;
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append('\n');
        }
    }

    public static void A02(StringBuilder sb, String str) {
        if (str != null) {
            for (int i = 0; i < str.length(); i++) {
                sb.append(str.charAt(i));
                sb.append('.');
            }
        }
    }

    public static final boolean A03(File file) {
        if (!new File(file, ".nomedia").exists()) {
            return false;
        }
        StringBuilder sb = new StringBuilder("debug-builder/unexpected .nomedia in ");
        sb.append(file.getName());
        Log.w(sb.toString());
        return true;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r20v0, resolved type: org.json.JSONObject */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:322:0x07bc, code lost:
        if (r0.intValue() != 2) goto L_0x07be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x018b, code lost:
        if ((r1 instanceof com.whatsapp.support.DescribeProblemActivity) == false) goto L_0x018d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:437:0x0a68 A[Catch: Exception -> 0x0aef, TryCatch #8 {Exception -> 0x0aef, blocks: (B:3:0x0009, B:6:0x0041, B:21:0x0065, B:22:0x006b, B:24:0x0070, B:25:0x0084, B:46:0x00c0, B:51:0x00cb, B:62:0x00f5, B:63:0x0119, B:65:0x0144, B:66:0x0156, B:69:0x0160, B:71:0x0166, B:73:0x016a, B:74:0x0174, B:76:0x0186, B:89:0x01bb, B:90:0x01c6, B:92:0x01cc, B:94:0x01d6, B:98:0x01e3, B:99:0x01e6, B:101:0x01ec, B:103:0x01f1, B:104:0x01fd, B:108:0x020a, B:110:0x0215, B:111:0x0225, B:113:0x022d, B:115:0x0241, B:117:0x0245, B:118:0x025e, B:119:0x0261, B:122:0x0287, B:126:0x02c8, B:127:0x02cc, B:129:0x02d3, B:130:0x02d7, B:132:0x0334, B:135:0x0342, B:136:0x034a, B:137:0x034f, B:140:0x0359, B:141:0x035e, B:142:0x0371, B:146:0x037f, B:149:0x038a, B:150:0x039f, B:151:0x03b6, B:153:0x03f7, B:155:0x0404, B:156:0x0412, B:157:0x0417, B:159:0x0422, B:160:0x0427, B:162:0x046b, B:163:0x0474, B:164:0x0484, B:166:0x0493, B:167:0x04a2, B:169:0x04a6, B:170:0x04b6, B:171:0x04bb, B:172:0x04c5, B:174:0x04cb, B:175:0x04d0, B:177:0x04d6, B:178:0x04db, B:180:0x04e8, B:181:0x04ef, B:187:0x050e, B:188:0x0513, B:191:0x051b, B:192:0x051f, B:194:0x0525, B:195:0x052a, B:198:0x053c, B:200:0x0546, B:201:0x054e, B:205:0x0562, B:207:0x056e, B:209:0x057e, B:210:0x0582, B:213:0x0596, B:215:0x059c, B:217:0x05b4, B:220:0x05be, B:225:0x05cb, B:227:0x05dd, B:228:0x05e1, B:229:0x05e4, B:230:0x05fd, B:231:0x0600, B:232:0x0603, B:235:0x060c, B:238:0x061a, B:241:0x0624, B:243:0x0643, B:245:0x0649, B:247:0x0651, B:249:0x0659, B:252:0x0662, B:256:0x066b, B:257:0x066f, B:260:0x0676, B:263:0x0683, B:265:0x068d, B:267:0x0691, B:268:0x0699, B:270:0x06a1, B:273:0x06b6, B:276:0x06d6, B:277:0x06e3, B:278:0x06e8, B:279:0x06ed, B:280:0x06ff, B:282:0x0705, B:284:0x0711, B:287:0x071e, B:289:0x072a, B:290:0x0733, B:291:0x0746, B:295:0x074c, B:298:0x075b, B:299:0x0763, B:301:0x0776, B:302:0x077b, B:304:0x0781, B:306:0x0785, B:307:0x078b, B:309:0x078f, B:310:0x0795, B:312:0x0799, B:313:0x079f, B:315:0x07a3, B:316:0x07a9, B:318:0x07ad, B:319:0x07b3, B:321:0x07b7, B:327:0x07c6, B:331:0x07d1, B:332:0x07d6, B:338:0x0800, B:339:0x0806, B:340:0x080d, B:343:0x0817, B:344:0x0820, B:346:0x0828, B:348:0x0831, B:350:0x0837, B:351:0x083b, B:353:0x0841, B:354:0x0851, B:356:0x085a, B:361:0x086b, B:362:0x0880, B:364:0x0891, B:365:0x0896, B:368:0x08c4, B:372:0x08e4, B:374:0x08f2, B:377:0x0900, B:379:0x090f, B:380:0x0933, B:386:0x094e, B:391:0x0964, B:392:0x097b, B:401:0x0997, B:410:0x09b4, B:412:0x09cc, B:414:0x09d2, B:416:0x09da, B:417:0x09de, B:418:0x09e1, B:420:0x09f1, B:422:0x09fd, B:424:0x0a09, B:426:0x0a15, B:428:0x0a25, B:430:0x0a35, B:435:0x0a49, B:437:0x0a68, B:438:0x0a71, B:440:0x0a77, B:441:0x0a87, B:443:0x0aa4, B:444:0x0aad, B:446:0x0ab3, B:447:0x0ac3, B:449:0x0ace, B:450:0x0ada, B:451:0x0ae1, B:292:0x0747, B:293:0x0749, B:80:0x0193, B:88:0x01b8, B:454:0x0ae9), top: B:471:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:450:0x0ada A[Catch: Exception -> 0x0aef, TryCatch #8 {Exception -> 0x0aef, blocks: (B:3:0x0009, B:6:0x0041, B:21:0x0065, B:22:0x006b, B:24:0x0070, B:25:0x0084, B:46:0x00c0, B:51:0x00cb, B:62:0x00f5, B:63:0x0119, B:65:0x0144, B:66:0x0156, B:69:0x0160, B:71:0x0166, B:73:0x016a, B:74:0x0174, B:76:0x0186, B:89:0x01bb, B:90:0x01c6, B:92:0x01cc, B:94:0x01d6, B:98:0x01e3, B:99:0x01e6, B:101:0x01ec, B:103:0x01f1, B:104:0x01fd, B:108:0x020a, B:110:0x0215, B:111:0x0225, B:113:0x022d, B:115:0x0241, B:117:0x0245, B:118:0x025e, B:119:0x0261, B:122:0x0287, B:126:0x02c8, B:127:0x02cc, B:129:0x02d3, B:130:0x02d7, B:132:0x0334, B:135:0x0342, B:136:0x034a, B:137:0x034f, B:140:0x0359, B:141:0x035e, B:142:0x0371, B:146:0x037f, B:149:0x038a, B:150:0x039f, B:151:0x03b6, B:153:0x03f7, B:155:0x0404, B:156:0x0412, B:157:0x0417, B:159:0x0422, B:160:0x0427, B:162:0x046b, B:163:0x0474, B:164:0x0484, B:166:0x0493, B:167:0x04a2, B:169:0x04a6, B:170:0x04b6, B:171:0x04bb, B:172:0x04c5, B:174:0x04cb, B:175:0x04d0, B:177:0x04d6, B:178:0x04db, B:180:0x04e8, B:181:0x04ef, B:187:0x050e, B:188:0x0513, B:191:0x051b, B:192:0x051f, B:194:0x0525, B:195:0x052a, B:198:0x053c, B:200:0x0546, B:201:0x054e, B:205:0x0562, B:207:0x056e, B:209:0x057e, B:210:0x0582, B:213:0x0596, B:215:0x059c, B:217:0x05b4, B:220:0x05be, B:225:0x05cb, B:227:0x05dd, B:228:0x05e1, B:229:0x05e4, B:230:0x05fd, B:231:0x0600, B:232:0x0603, B:235:0x060c, B:238:0x061a, B:241:0x0624, B:243:0x0643, B:245:0x0649, B:247:0x0651, B:249:0x0659, B:252:0x0662, B:256:0x066b, B:257:0x066f, B:260:0x0676, B:263:0x0683, B:265:0x068d, B:267:0x0691, B:268:0x0699, B:270:0x06a1, B:273:0x06b6, B:276:0x06d6, B:277:0x06e3, B:278:0x06e8, B:279:0x06ed, B:280:0x06ff, B:282:0x0705, B:284:0x0711, B:287:0x071e, B:289:0x072a, B:290:0x0733, B:291:0x0746, B:295:0x074c, B:298:0x075b, B:299:0x0763, B:301:0x0776, B:302:0x077b, B:304:0x0781, B:306:0x0785, B:307:0x078b, B:309:0x078f, B:310:0x0795, B:312:0x0799, B:313:0x079f, B:315:0x07a3, B:316:0x07a9, B:318:0x07ad, B:319:0x07b3, B:321:0x07b7, B:327:0x07c6, B:331:0x07d1, B:332:0x07d6, B:338:0x0800, B:339:0x0806, B:340:0x080d, B:343:0x0817, B:344:0x0820, B:346:0x0828, B:348:0x0831, B:350:0x0837, B:351:0x083b, B:353:0x0841, B:354:0x0851, B:356:0x085a, B:361:0x086b, B:362:0x0880, B:364:0x0891, B:365:0x0896, B:368:0x08c4, B:372:0x08e4, B:374:0x08f2, B:377:0x0900, B:379:0x090f, B:380:0x0933, B:386:0x094e, B:391:0x0964, B:392:0x097b, B:401:0x0997, B:410:0x09b4, B:412:0x09cc, B:414:0x09d2, B:416:0x09da, B:417:0x09de, B:418:0x09e1, B:420:0x09f1, B:422:0x09fd, B:424:0x0a09, B:426:0x0a15, B:428:0x0a25, B:430:0x0a35, B:435:0x0a49, B:437:0x0a68, B:438:0x0a71, B:440:0x0a77, B:441:0x0a87, B:443:0x0aa4, B:444:0x0aad, B:446:0x0ab3, B:447:0x0ac3, B:449:0x0ace, B:450:0x0ada, B:451:0x0ae1, B:292:0x0747, B:293:0x0749, B:80:0x0193, B:88:0x01b8, B:454:0x0ae9), top: B:471:0x0009 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A04(android.content.Context r44, android.util.Pair r45, java.lang.String r46, java.lang.String r47, java.lang.String r48, java.lang.String r49, java.util.List r50, java.util.List r51, long r52, long r54, boolean r56, boolean r57) {
        /*
        // Method dump skipped, instructions count: 2864
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22760zb.A04(android.content.Context, android.util.Pair, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.List, java.util.List, long, long, boolean, boolean):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0039, code lost:
        if ("IN".equals(X.C22650zQ.A01(r0.cc, r0.number)) != false) goto L_0x003b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A05(java.io.File r18, java.lang.String r19, boolean r20) {
        /*
            r17 = this;
            java.lang.String r4 = "https://crashlogs.whatsapp.net/wa_clb_data"
            r9 = 1
            r2 = r17
            X.0m6 r0 = r2.A0H
            android.content.SharedPreferences r5 = r0.A00
            java.lang.String r3 = "in_log_del_on_upgrade_new"
            r0 = -1
            long r7 = r5.getLong(r3, r0)
            r5 = 2
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x00ce
            X.0nT r0 = r2.A05
            r0.A08()
            com.whatsapp.Me r0 = r0.A00
            if (r0 == 0) goto L_0x003b
            X.0zQ r0 = r2.A0Y
            X.0nT r0 = r0.A03
            r0.A08()
            com.whatsapp.Me r0 = r0.A00
            if (r0 == 0) goto L_0x00ce
            java.lang.String r1 = r0.cc
            java.lang.String r0 = r0.number
            java.lang.String r1 = X.C22650zQ.A01(r1, r0)
            java.lang.String r0 = "IN"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00ce
        L_0x003b:
            r3 = 0
            if (r9 == 0) goto L_0x0044
            java.lang.String r0 = "debug-builder/upload-logs can not upload logs"
            com.whatsapp.util.Log.e(r0)
        L_0x0043:
            return r3
        L_0x0044:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            android.os.ConditionVariable r0 = new android.os.ConditionVariable
            r0.<init>()
            X.23R r8 = new X.23R
            r8.<init>(r0, r2, r1)
            r7 = r18
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch: Exception -> 0x00d6
            r9.<init>(r7)     // Catch: Exception -> 0x00d6
            X.0uw r6 = r2.A0R     // Catch: all -> 0x00d1
            r5 = 6
            X.1Ni r4 = r6.A00(r8, r4, r5)     // Catch: all -> 0x00d1
            java.lang.String r6 = "access_token"
            java.lang.String r5 = "1063127757113399|745146ffa34413f9dbb5469f5370b7af"
            r4.A07(r6, r5)     // Catch: all -> 0x00d1
            r12 = 2
            if (r20 == 0) goto L_0x006c
            r12 = 1
        L_0x006c:
            java.lang.String r10 = "file"
            java.lang.String r11 = r7.getName()     // Catch: all -> 0x00d1
            long r15 = r7.length()     // Catch: all -> 0x00d1
            r13 = 0
            java.util.List r5 = r4.A0A     // Catch: all -> 0x00d1
            X.23S r8 = new X.23S     // Catch: all -> 0x00d1
            r8.<init>(r9, r10, r11, r12, r13, r15)     // Catch: all -> 0x00d1
            r5.add(r8)     // Catch: all -> 0x00d1
            java.lang.String r6 = "type"
            java.lang.String r5 = "support"
            r4.A07(r6, r5)     // Catch: all -> 0x00d1
            java.lang.String r5 = "from_jid"
            X.0nm r6 = r2.A03     // Catch: all -> 0x00d1
            java.lang.String r2 = r6.A00()     // Catch: all -> 0x00d1
            r4.A07(r5, r2)     // Catch: all -> 0x00d1
            java.lang.String r5 = "forced"
            java.lang.String r2 = "true"
            r4.A07(r5, r2)     // Catch: all -> 0x00d1
            java.lang.String r5 = "android_hprof_extras"
            X.16A r6 = (X.AnonymousClass16A) r6     // Catch: all -> 0x00d1
            X.1Na r2 = r6.A04(r3)     // Catch: all -> 0x00d1
            java.lang.String r2 = r2.A00()     // Catch: all -> 0x00d1
            r4.A07(r5, r2)     // Catch: all -> 0x00d1
            r5 = r19
            if (r19 == 0) goto L_0x00b7
            java.lang.String r2 = "ticket_id"
            r4.A07(r2, r5)     // Catch: all -> 0x00d1
        L_0x00b7:
            r4.A02(r3)     // Catch: all -> 0x00d1
            r9.close()     // Catch: Exception -> 0x00d6
            r4 = 100000(0x186a0, double:4.94066E-319)
            r0.block(r4)
            int r0 = r1.length()
            if (r0 == 0) goto L_0x0043
            java.lang.String r0 = r1.toString()
            return r0
        L_0x00ce:
            r9 = 0
            goto L_0x003b
        L_0x00d1:
            r0 = move-exception
            r9.close()     // Catch: all -> 0x00d5
        L_0x00d5:
            throw r0     // Catch: Exception -> 0x00d6
        L_0x00d6:
            r2 = move-exception
            java.lang.String r1 = "debug-builder/uploadLogsInternal/error-uploading-logs exception:"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22760zb.A05(java.io.File, java.lang.String, boolean):java.lang.String");
    }

    public void A06() {
        StringBuilder sb;
        boolean booleanValue;
        NotificationManager A08;
        PowerManager A0I;
        C29351Rv A02;
        AnonymousClass01I.A00();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("app-state");
        sb2.append("/settings/notifications-enabled: ");
        Context context = this.A0F.A00;
        sb2.append(new AnonymousClass02r(context).A03());
        Log.i(sb2.toString());
        StringBuilder sb3 = new StringBuilder();
        sb3.append("app-state");
        sb3.append("/google-play-services: ");
        boolean z = false;
        if (AnonymousClass1UB.A00(context) == 0) {
            z = true;
        }
        sb3.append(z);
        Log.i(sb3.toString());
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("app-state");
            sb4.append("/auth-keystore-result:");
            C17200qQ r1 = this.A00;
            synchronized (r1) {
                A02 = r1.A02();
            }
            sb4.append(A02.A00);
            Log.i(sb4.toString());
        }
        if (i >= 21 && (A0I = this.A0E.A0I()) != null) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("app-state");
            sb5.append("/power-save-mode:");
            sb5.append(A0I.isPowerSaveMode());
            Log.i(sb5.toString());
        }
        if (i >= 24) {
            StringBuilder sb6 = new StringBuilder();
            sb6.append("app-state");
            sb6.append("/bg-data-restricted:");
            sb6.append(this.A0C.A0C());
            Log.i(sb6.toString());
        }
        if (i >= 28 && (A08 = this.A0E.A08()) != null) {
            int currentInterruptionFilter = A08.getCurrentInterruptionFilter();
            StringBuilder sb7 = new StringBuilder();
            sb7.append("app-state");
            sb7.append("/do-not-disturb:");
            boolean z2 = true;
            if (currentInterruptionFilter == 1 || currentInterruptionFilter == 0) {
                z2 = false;
            }
            sb7.append(z2);
            Log.i(sb7.toString());
        }
        if (i >= 28) {
            AnonymousClass01d r3 = this.A0E;
            ActivityManager A03 = r3.A03();
            if (A03 != null) {
                StringBuilder sb8 = new StringBuilder();
                sb8.append("app-state");
                sb8.append("/background-restricted:");
                sb8.append(A03.isBackgroundRestricted());
                Log.i(sb8.toString());
            }
            UsageStatsManager A0A = r3.A0A();
            if (A0A != null) {
                long currentTimeMillis = System.currentTimeMillis();
                UsageEvents queryEventsForSelf = A0A.queryEventsForSelf(currentTimeMillis - TimeUnit.HOURS.toMillis(12), currentTimeMillis);
                UsageEvents.Event event = new UsageEvents.Event();
                while (queryEventsForSelf.getNextEvent(event)) {
                    if (event.getEventType() == 11) {
                        StringBuilder sb9 = new StringBuilder();
                        sb9.append("app-state");
                        sb9.append("/app-standby bucket:");
                        sb9.append(event.getAppStandbyBucket());
                        sb9.append(" time:");
                        sb9.append(event.getTimeStamp());
                        Log.i(sb9.toString());
                    }
                }
                sb = new StringBuilder();
                sb.append("app-state");
                sb.append("/current app-standby bucket:");
                sb.append(A0A.getAppStandbyBucket());
            } else {
                sb = new StringBuilder();
                sb.append("app-state");
                sb.append("/usage-stats-manager null");
            }
            Log.i(sb.toString());
            C18290sD r12 = this.A0M;
            C16490p7 r5 = r12.A08;
            r5.A04();
            if (r5.A01) {
                C16310on A01 = r5.get();
                try {
                    for (AbstractC18500sY r32 : new ArrayList(r12.A0A.A00().A00.values())) {
                        StringBuilder sb10 = new StringBuilder();
                        sb10.append("app-state");
                        sb10.append("/db-migration-status/");
                        sb10.append(r32.A0C);
                        sb10.append(":");
                        sb10.append(r32.A04());
                        Log.i(sb10.toString());
                    }
                    StringBuilder sb11 = new StringBuilder();
                    sb11.append("app-state");
                    sb11.append("/db-migration-status-overall:");
                    r5.A04();
                    C29561To r13 = r5.A05;
                    synchronized (r13) {
                        booleanValue = r13.A06(r13.A00).booleanValue();
                    }
                    sb11.append(booleanValue);
                    Log.i(sb11.toString());
                    r5.A04();
                    if (r13.A00 != null) {
                        StringBuilder sb12 = new StringBuilder();
                        sb12.append("app-state");
                        sb12.append("/");
                        sb12.append("message_view");
                        sb12.append(":");
                        C16330op r52 = A01.A03;
                        AnonymousClass009.A05(r52);
                        sb12.append(AnonymousClass1Uj.A00(r52, "view", "message_view"));
                        Log.i(sb12.toString());
                        StringBuilder sb13 = new StringBuilder();
                        sb13.append("app-state");
                        sb13.append("/");
                        sb13.append("available_message_view");
                        sb13.append(":");
                        AnonymousClass009.A05(r52);
                        sb13.append(AnonymousClass1Uj.A00(r52, "view", "available_message_view"));
                        Log.i(sb13.toString());
                        StringBuilder sb14 = new StringBuilder();
                        sb14.append("app-state");
                        sb14.append("/");
                        sb14.append("legacy_available_messages_view");
                        sb14.append(":");
                        AnonymousClass009.A05(r52);
                        sb14.append(AnonymousClass1Uj.A00(r52, "view", "legacy_available_messages_view"));
                        Log.i(sb14.toString());
                        StringBuilder sb15 = new StringBuilder();
                        sb15.append("app-state");
                        sb15.append("/");
                        sb15.append("deleted_messages_view");
                        sb15.append(":");
                        AnonymousClass009.A05(r52);
                        sb15.append(AnonymousClass1Uj.A00(r52, "view", "deleted_messages_view"));
                        Log.i(sb15.toString());
                        StringBuilder sb16 = new StringBuilder();
                        sb16.append("app-state");
                        sb16.append("/");
                        sb16.append("deleted_messages_ids_view");
                        sb16.append(":");
                        AnonymousClass009.A05(r52);
                        sb16.append(AnonymousClass1Uj.A00(r52, "view", "deleted_messages_ids_view"));
                        Log.i(sb16.toString());
                    }
                    A01.close();
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } else {
                StringBuilder sb17 = new StringBuilder();
                sb17.append("app-state");
                sb17.append("/db-migration-status-not-ready");
                Log.i(sb17.toString());
            }
            for (Map.Entry<String, ?> entry : this.A0O.A01.getAll().entrySet()) {
                if (!entry.getKey().startsWith("ab_props:sys:")) {
                    StringBuilder sb18 = new StringBuilder();
                    sb18.append("app-state");
                    sb18.append("/abprops config code key: ");
                    sb18.append(entry.getKey());
                    sb18.append(" config value: ");
                    sb18.append(entry.getValue());
                    Log.i(sb18.toString());
                }
            }
        }
    }
}
