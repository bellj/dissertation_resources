package X;

import java.lang.ref.WeakReference;

/* renamed from: X.3nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77863nz extends AnonymousClass29p {
    public final WeakReference A00;

    public C77863nz(C77733nl r2) {
        this.A00 = C12970iu.A10(r2);
    }

    @Override // X.AnonymousClass29p
    public final void A00() {
        C77733nl r0 = (C77733nl) this.A00.get();
        if (r0 != null) {
            C77733nl.A01(r0);
        }
    }
}
