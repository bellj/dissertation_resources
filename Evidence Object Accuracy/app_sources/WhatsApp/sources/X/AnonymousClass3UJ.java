package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import com.whatsapp.emoji.EmojiDescriptor;

/* renamed from: X.3UJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UJ implements AbstractC116455Vm {
    public final /* synthetic */ Resources A00;
    public final /* synthetic */ AnonymousClass1BR A01;

    @Override // X.AbstractC116455Vm
    public void AMr() {
    }

    public AnonymousClass3UJ(Resources resources, AnonymousClass1BR r2) {
        this.A01 = r2;
        this.A00 = resources;
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        C39511q1 r4 = new C39511q1(iArr);
        long A00 = EmojiDescriptor.A00(r4, false);
        AnonymousClass1BR r0 = this.A01;
        AnonymousClass19M r1 = r0.A0A;
        Resources resources = this.A00;
        Drawable A03 = r1.A03(resources, new C68723Wm(resources, r0, iArr), r4, A00);
        if (A03 != null) {
            C74493i9 r12 = r0.A04;
            AnonymousClass009.A05(r12);
            r12.A04(A03, 0);
            return;
        }
        C74493i9 r3 = r0.A04;
        AnonymousClass009.A05(r3);
        int i = 2;
        if (!C12960it.A1S((A00 > -1 ? 1 : (A00 == -1 ? 0 : -1)))) {
            i = 1;
        }
        r3.A04(null, i);
    }
}
