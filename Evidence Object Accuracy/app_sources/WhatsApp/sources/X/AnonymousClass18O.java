package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.18O  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass18O {
    public C1329668y A00;
    public Set A01 = new LinkedHashSet();

    public AnonymousClass18O(C1329668y r10) {
        C114075Kc r1;
        C16700pc.A0E(r10, 1);
        this.A00 = r10;
        String A08 = this.A00.A08();
        if (A08 != null && A08.length() != 0) {
            JSONArray jSONArray = new JSONArray(A08);
            int length = jSONArray.length();
            if (length <= Integer.MIN_VALUE) {
                r1 = C114075Kc.A00;
            } else {
                r1 = new C114075Kc(0, length - 1);
            }
            Iterator it = r1.iterator();
            while (it.hasNext()) {
                JSONObject jSONObject = jSONArray.getJSONObject(((AnonymousClass5DO) it).A00());
                this.A01.add(new AnonymousClass2SO(new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, jSONObject.getString("alias"), "upiAlias"), jSONObject.getString("aliasType"), jSONObject.getString("aliasId"), jSONObject.getString("aliasStatus")));
            }
        }
    }

    public final Set A00() {
        Set set = this.A01;
        ArrayList arrayList = new ArrayList();
        for (Object obj : set) {
            String str = ((AnonymousClass2SO) obj).A02;
            if (C16700pc.A0O(str, "deregistered_pending") || C16700pc.A0O(str, "active") || C16700pc.A0O(str, "active_pending")) {
                arrayList.add(obj);
            }
        }
        return AnonymousClass01Y.A08(arrayList);
    }

    public synchronized void A01(AnonymousClass2SO r8) {
        Object obj;
        C16700pc.A0E(r8, 0);
        Set set = this.A01;
        Iterator it = set.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (C16700pc.A0O(((AnonymousClass2SO) obj).A01, r8.A01)) {
                break;
            }
        }
        AnonymousClass2SO r5 = (AnonymousClass2SO) obj;
        if (r5 != null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet(C17540qy.A03(set.size()));
            boolean z = false;
            for (Object obj2 : set) {
                if (z || !C16700pc.A0O(obj2, r5)) {
                    linkedHashSet.add(obj2);
                } else {
                    z = true;
                }
            }
            LinkedHashSet linkedHashSet2 = new LinkedHashSet(C17540qy.A03(linkedHashSet.size() + 1));
            linkedHashSet2.addAll(linkedHashSet);
            linkedHashSet2.add(r8);
            if (A02(linkedHashSet2)) {
                set.remove(r5);
                set.add(r8);
            }
        }
    }

    public final boolean A02(Set set) {
        JSONArray jSONArray = new JSONArray();
        try {
            Iterator it = set.iterator();
            while (it.hasNext()) {
                AnonymousClass2SO r3 = (AnonymousClass2SO) it.next();
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("alias", r3.A00.A00);
                jSONObject.put("aliasType", r3.A03);
                jSONObject.put("aliasId", r3.A01);
                jSONObject.put("aliasStatus", r3.A02);
                jSONArray.put(jSONObject);
            }
            this.A00.A0K(jSONArray);
            return true;
        } catch (JSONException unused) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs setAlias threw: an exception ");
            return false;
        }
    }
}
