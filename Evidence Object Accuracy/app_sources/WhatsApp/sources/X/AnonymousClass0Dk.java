package X;

import android.view.WindowInsets;

/* renamed from: X.0Dk  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Dk extends AnonymousClass0PY {
    public final WindowInsets.Builder A00;

    public AnonymousClass0Dk() {
        super(new C018408o());
        this.A00 = new WindowInsets.Builder();
    }

    public AnonymousClass0Dk(C018408o r3) {
        super(r3);
        WindowInsets.Builder builder;
        WindowInsets A07 = r3.A07();
        if (A07 != null) {
            builder = new WindowInsets.Builder(A07);
        } else {
            builder = new WindowInsets.Builder();
        }
        this.A00 = builder;
    }

    @Override // X.AnonymousClass0PY
    public C018408o A00() {
        return C018408o.A02(this.A00.build());
    }

    @Override // X.AnonymousClass0PY
    public void A01(AnonymousClass0U7 r3) {
        this.A00.setStableInsets(r3.A02());
    }

    @Override // X.AnonymousClass0PY
    public void A02(AnonymousClass0U7 r3) {
        this.A00.setSystemWindowInsets(r3.A02());
    }
}
