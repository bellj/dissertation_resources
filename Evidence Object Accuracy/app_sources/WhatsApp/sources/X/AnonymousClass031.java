package X;

/* renamed from: X.031  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass031 extends AnonymousClass032 {
    public long blkIoTicks;
    public long cancelledWriteBytes;
    public long majorFaults;
    public long rcharBytes;
    public long readBytes;
    public long syscrCount;
    public long syscwCount;
    public long wcharBytes;
    public long writeBytes;

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A01(AnonymousClass032 r1) {
        A03((AnonymousClass031) r1);
        return this;
    }

    @Override // X.AnonymousClass032
    public /* bridge */ /* synthetic */ AnonymousClass032 A02(AnonymousClass032 r5, AnonymousClass032 r6) {
        AnonymousClass031 r52 = (AnonymousClass031) r5;
        AnonymousClass031 r62 = (AnonymousClass031) r6;
        if (r62 == null) {
            r62 = new AnonymousClass031();
        }
        if (r52 == null) {
            r62.A03(this);
            return r62;
        }
        r62.rcharBytes = this.rcharBytes - r52.rcharBytes;
        r62.wcharBytes = this.wcharBytes - r52.wcharBytes;
        r62.syscrCount = this.syscrCount - r52.syscrCount;
        r62.syscwCount = this.syscwCount - r52.syscwCount;
        r62.readBytes = this.readBytes - r52.readBytes;
        r62.writeBytes = this.writeBytes - r52.writeBytes;
        r62.cancelledWriteBytes = this.cancelledWriteBytes - r52.cancelledWriteBytes;
        r62.majorFaults = this.majorFaults - r52.majorFaults;
        r62.blkIoTicks = this.blkIoTicks - r52.blkIoTicks;
        return r62;
    }

    public void A03(AnonymousClass031 r3) {
        this.rcharBytes = r3.rcharBytes;
        this.wcharBytes = r3.wcharBytes;
        this.syscrCount = r3.syscrCount;
        this.syscwCount = r3.syscwCount;
        this.readBytes = r3.readBytes;
        this.writeBytes = r3.writeBytes;
        this.cancelledWriteBytes = r3.cancelledWriteBytes;
        this.majorFaults = r3.majorFaults;
        this.blkIoTicks = r3.blkIoTicks;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass031 r7 = (AnonymousClass031) obj;
            if (!(r7.rcharBytes == this.rcharBytes && r7.wcharBytes == this.wcharBytes && r7.syscrCount == this.syscrCount && r7.syscwCount == this.syscwCount && r7.readBytes == this.readBytes && r7.writeBytes == this.writeBytes && r7.cancelledWriteBytes == this.cancelledWriteBytes && r7.majorFaults == this.majorFaults && r7.blkIoTicks == this.blkIoTicks)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        long j = this.rcharBytes;
        long j2 = this.wcharBytes;
        long j3 = this.syscrCount;
        long j4 = this.syscwCount;
        long j5 = this.readBytes;
        long j6 = this.writeBytes;
        long j7 = this.cancelledWriteBytes;
        long j8 = this.majorFaults;
        long j9 = this.blkIoTicks;
        return (((((((((((((((((int) (j ^ (j >>> 32))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)))) * 31) + ((int) (j9 ^ (j9 >>> 32)));
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("DiskMetrics{rcharBytes=");
        sb.append(this.rcharBytes);
        sb.append(", wcharBytes=");
        sb.append(this.wcharBytes);
        sb.append(", syscrCount=");
        sb.append(this.syscrCount);
        sb.append(", syscwCount=");
        sb.append(this.syscwCount);
        sb.append(", readBytes=");
        sb.append(this.readBytes);
        sb.append(", writeBytes=");
        sb.append(this.writeBytes);
        sb.append(", cancelledWriteBytes=");
        sb.append(this.cancelledWriteBytes);
        sb.append(", majorFaults=");
        sb.append(this.majorFaults);
        sb.append(", blkIoTicks=");
        sb.append(this.blkIoTicks);
        sb.append("}");
        return sb.toString();
    }
}
