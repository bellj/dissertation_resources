package X;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;

/* renamed from: X.05H  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05H extends Fragment {
    public static void A00(Activity activity) {
        if (Build.VERSION.SDK_INT >= 29) {
            AnonymousClass0V0.registerIn(activity);
        }
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new AnonymousClass05H(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    public static void A01(Activity activity, AnonymousClass074 r2) {
        if (activity instanceof AbstractC001200n) {
            AbstractC009904y ADr = ((AbstractC001200n) activity).ADr();
            if (ADr instanceof C009804x) {
                ((C009804x) ADr).A04(r2);
            }
        }
    }

    public final void A02(AnonymousClass074 r3) {
        if (Build.VERSION.SDK_INT < 29) {
            A01(getActivity(), r3);
        }
    }

    @Override // android.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        A02(AnonymousClass074.ON_CREATE);
    }

    @Override // android.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        A02(AnonymousClass074.ON_DESTROY);
    }

    @Override // android.app.Fragment
    public void onPause() {
        super.onPause();
        A02(AnonymousClass074.ON_PAUSE);
    }

    @Override // android.app.Fragment
    public void onResume() {
        super.onResume();
        A02(AnonymousClass074.ON_RESUME);
    }

    @Override // android.app.Fragment
    public void onStart() {
        super.onStart();
        A02(AnonymousClass074.ON_START);
    }

    @Override // android.app.Fragment
    public void onStop() {
        super.onStop();
        A02(AnonymousClass074.ON_STOP);
    }
}
