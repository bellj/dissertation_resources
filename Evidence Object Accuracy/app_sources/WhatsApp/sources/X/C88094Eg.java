package X;

import android.view.View;
import android.view.ViewStub;

/* renamed from: X.4Eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C88094Eg {
    public static void A00(ViewStub viewStub, AnonymousClass5Wu r2) {
        viewStub.setLayoutResource(r2.ADq());
        viewStub.setOnInflateListener(new ViewStub.OnInflateListener() { // from class: X.4nX
            @Override // android.view.ViewStub.OnInflateListener
            public final void onInflate(ViewStub viewStub2, View view) {
                AnonymousClass5Wu.this.AYL(view);
            }
        });
        viewStub.inflate();
    }
}
