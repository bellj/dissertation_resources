package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.4db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95374db {
    public static final C113135Gd A00 = new C113135Gd();

    public static byte[] A00(String str) {
        try {
            return A00.A00(str, 0, str.length());
        } catch (Exception e) {
            throw new AnonymousClass4CS(C12960it.A0d(e.getMessage(), C12960it.A0k("exception decoding Hex string: ")), e);
        }
    }

    public static byte[] A01(String str, int i) {
        try {
            return A00.A00(str, 1, i);
        } catch (Exception e) {
            throw new AnonymousClass4CS(C12960it.A0d(e.getMessage(), C12960it.A0k("exception decoding Hex string: ")), e);
        }
    }

    public static byte[] A02(byte[] bArr, int i, int i2) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            A00.A9L(byteArrayOutputStream, bArr, i, i2);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            throw new AnonymousClass4CT(C12960it.A0d(e.getMessage(), C12960it.A0k("exception encoding Hex string: ")), e);
        }
    }
}
