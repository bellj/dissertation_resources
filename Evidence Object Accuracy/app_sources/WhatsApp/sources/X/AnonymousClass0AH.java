package X;

import android.os.AsyncTask;
import android.util.Log;
import com.caverock.androidsvg.SVGImageView;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.0AH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AH extends AsyncTask {
    public final /* synthetic */ SVGImageView A00;

    public /* synthetic */ AnonymousClass0AH(SVGImageView sVGImageView) {
        this.A00 = sVGImageView;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:18:0x0000 */
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: android.content.res.AssetFileDescriptor$AutoCloseInputStream */
    /* JADX DEBUG: Multi-variable search result rejected for r6v2, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: android.content.res.AssetFileDescriptor$AutoCloseInputStream */
    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: android.content.res.AssetFileDescriptor$AutoCloseInputStream */
    /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: android.content.res.AssetFileDescriptor$AutoCloseInputStream */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // android.os.AsyncTask
    public Object doInBackground(Object[] objArr) {
        try {
            objArr = (InputStream[]) objArr;
            try {
                return new C06560Ud().A0N(objArr[0]);
            } catch (C11160fq e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Parse error loading URI: ");
                sb.append(e.getMessage());
                Log.e("SVGImageView", sb.toString());
                try {
                    objArr[0].close();
                    return null;
                } catch (IOException unused) {
                    return null;
                }
            }
        } finally {
            try {
                objArr[0].close();
            } catch (IOException unused2) {
            }
        }
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        SVGImageView sVGImageView = this.A00;
        sVGImageView.A01 = (AnonymousClass0Q5) obj;
        sVGImageView.A00();
    }
}
