package X;

/* renamed from: X.43u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856943u extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;

    public C856943u() {
        super(2950, AbstractC16110oT.A00(), 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(1, this.A04);
        r3.Abe(14, this.A05);
        r3.Abe(15, this.A06);
        r3.Abe(16, this.A07);
        r3.Abe(10, this.A08);
        r3.Abe(6, this.A09);
        r3.Abe(13, this.A0A);
        r3.Abe(12, this.A0B);
        r3.Abe(11, this.A0C);
        r3.Abe(9, this.A0D);
        r3.Abe(8, this.A0E);
        r3.Abe(7, this.A0F);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPaymentsDaily {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "invitedUserCnt", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "invitedUserRegisteredCnt", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "inviterUserCnt", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "invitesReceivedToUserCnt", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "invitesSentToUserCnt", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "startTs", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalOneTimeMandateCnt", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalRecurringMandateCnt", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalTransactionReceivedCnt", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalTransactionSentCnt", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transactionReceivedWithBackgroundAndStickerCnt", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transactionReceivedWithBackgroundCnt", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transactionReceivedWithStickerCnt", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transactionSentWithBackgroundAndStickerCnt", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transactionSentWithBackgroundCnt", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "transactionSentWithStickerCnt", this.A0F);
        return C12960it.A0d("}", A0k);
    }
}
