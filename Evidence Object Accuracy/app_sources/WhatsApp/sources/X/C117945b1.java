package X;

/* renamed from: X.5b1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117945b1 extends AnonymousClass015 {
    public AbstractC28901Pl A00;
    public AnonymousClass1IR A01;
    public C27691It A02 = C13000ix.A03();
    public String A03;
    public final C14900mE A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final AnonymousClass018 A07;
    public final C241414j A08;
    public final C243515e A09;
    public final C17070qD A0A;
    public final C120565gO A0B;
    public final C1310060v A0C;
    public final AbstractC14440lR A0D;

    public C117945b1(C14900mE r2, C14830m7 r3, C16590pI r4, AnonymousClass018 r5, C241414j r6, C243515e r7, C17070qD r8, C120565gO r9, C1310060v r10, AbstractC14440lR r11, String str) {
        this.A06 = r4;
        this.A0D = r11;
        this.A08 = r6;
        this.A05 = r3;
        this.A04 = r2;
        this.A07 = r5;
        this.A0C = r10;
        this.A0B = r9;
        this.A0A = r8;
        this.A09 = r7;
        this.A03 = str;
    }
}
