package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.concurrent.Executor;

/* renamed from: X.3TU  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3TU implements AbstractC13670k8 {
    public AbstractC13630k2 A00;
    public final Object A01 = C12970iu.A0l();
    public final Executor A02;

    public AnonymousClass3TU(AbstractC13630k2 r2, Executor executor) {
        this.A02 = executor;
        this.A00 = r2;
    }

    @Override // X.AbstractC13670k8
    public final void Agv(C13600jz r4) {
        if (!r4.A0A() && !r4.A05) {
            synchronized (this.A01) {
                if (this.A00 != null) {
                    this.A02.execute(new RunnableBRunnable0Shape10S0200000_I1(r4, 23, this));
                }
            }
        }
    }
}
