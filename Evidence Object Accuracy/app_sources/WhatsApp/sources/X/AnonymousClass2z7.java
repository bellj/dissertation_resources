package X;

import android.content.Intent;
import android.view.MenuItem;
import com.whatsapp.Conversation;
import com.whatsapp.R;

/* renamed from: X.2z7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2z7 extends AnonymousClass1MD {
    public final /* synthetic */ Conversation A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2z7(AnonymousClass12P r44, Conversation conversation, AbstractC15710nm r46, C14900mE r47, C15570nT r48, C15450nH r49, C16170oZ r50, ActivityC13790kL r51, AnonymousClass12T r52, C18850tA r53, C15550nR r54, C22700zV r55, C15610nY r56, AnonymousClass1A6 r57, C255419u r58, AnonymousClass01d r59, C14830m7 r60, C14820m6 r61, AnonymousClass018 r62, C15600nX r63, C253218y r64, C242114q r65, C22100yW r66, C22180yf r67, AnonymousClass19M r68, C231510o r69, AnonymousClass193 r70, C14850m9 r71, C16120oU r72, C20710wC r73, AnonymousClass109 r74, C22370yy r75, AnonymousClass13H r76, C16630pM r77, C88054Ec r78, AnonymousClass12F r79, C253018w r80, AnonymousClass12U r81, C23000zz r82, C252718t r83, AbstractC14440lR r84, AnonymousClass01H r85) {
        super(r44, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72, r73, r74, r75, r76, r77, r78, r79, r80, r81, r82, r83, r84, r85);
        this.A00 = conversation;
    }

    @Override // X.AnonymousClass1MD
    public void A04() {
        super.A04();
        AnonymousClass19J r1 = ((AbstractActivityC13750kH) this.A00).A0b;
        if (r1.A01(2)) {
            r1.A00 = 3;
            r1.A00(1);
        }
    }

    @Override // X.AnonymousClass1MD, X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r8) {
        AbstractC15340mz r4;
        Conversation conversation = this.A00;
        C35451ht r0 = ((AbstractActivityC13750kH) conversation).A0J;
        if (!(r0 == null || r0.A04.size() == 0)) {
            if (menuItem.getItemId() == R.id.menuitem_add_to_contacts || menuItem.getItemId() == R.id.menuitem_message_contact) {
                boolean A3P = conversation.A3P(C20710wC.A01(A01()), menuItem.getItemId());
                A03();
                return A3P;
            }
            if (menuItem.getItemId() == R.id.menuitem_reply) {
                r4 = A01();
                if (!conversation.A1y.A05()) {
                    conversation.Ach(r4);
                    A03();
                }
            } else if (menuItem.getItemId() != R.id.menuitem_reply_privately) {
                return super.ALr(menuItem, r8);
            } else {
                r4 = A01();
            }
            conversation.A1h.A00.put(r4.A0B(), r4);
            Intent A0D = C12990iw.A0D(conversation, Conversation.class);
            A0D.putExtra("jid", C15380n4.A03(r4.A0B()));
            A0D.putExtra("args_conversation_screen_entry_point", 2);
            conversation.startActivity(A0D);
            conversation.finish();
            A03();
        }
        return true;
    }

    @Override // X.AnonymousClass1MD, X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        super.AP3(r4);
        Conversation conversation = this.A00;
        C35451ht r0 = ((AbstractActivityC13750kH) conversation).A0J;
        if (r0 != null) {
            r0.A00();
            ((AbstractActivityC13750kH) conversation).A0J = null;
        }
        conversation.A1g.A02();
        ((AbstractActivityC13750kH) conversation).A01 = null;
        conversation.A2j();
        conversation.A1g.getConversationCursorAdapter().A06 = ((AbstractActivityC13750kH) conversation).A01;
        if (conversation.A20.A07 == null) {
            conversation.A2s();
        }
    }
}
