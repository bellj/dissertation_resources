package X;

import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.3dO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71713dO<E> extends AbstractCollection<E> {
    public final AnonymousClass28S predicate;
    public final Collection unfiltered;

    public C71713dO(Collection collection, AnonymousClass28S r2) {
        this.unfiltered = collection;
        this.predicate = r2;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean add(Object obj) {
        if (this.predicate.A66(obj)) {
            return this.unfiltered.add(obj);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean addAll(Collection collection) {
        Iterator<E> it = collection.iterator();
        while (it.hasNext()) {
            if (!this.predicate.A66(it.next())) {
                throw new IllegalArgumentException();
            }
        }
        return this.unfiltered.addAll(collection);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public void clear() {
        AnonymousClass3JP.removeIf(this.unfiltered, this.predicate);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        if (C50912Rv.safeContains(this.unfiltered, obj)) {
            return this.predicate.A66(obj);
        }
        return false;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean containsAll(Collection collection) {
        return C50912Rv.containsAllImpl(this, collection);
    }

    public C71713dO createCombined(AnonymousClass28S r4) {
        Collection collection = this.unfiltered;
        AnonymousClass28S[] r0 = new AnonymousClass28S[2];
        C12970iu.A1U(this.predicate, r4, r0);
        return new C71713dO(collection, new AnonymousClass3TY(Arrays.asList(r0)));
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean isEmpty() {
        return !AnonymousClass3JP.any(this.unfiltered, this.predicate);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return AnonymousClass1I4.filter(this.unfiltered.iterator(), this.predicate);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean remove(Object obj) {
        return contains(obj) && this.unfiltered.remove(obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean removeAll(Collection collection) {
        Iterator<E> it = this.unfiltered.iterator();
        boolean z = false;
        while (it.hasNext()) {
            E next = it.next();
            if (this.predicate.A66(next) && collection.contains(next)) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean retainAll(Collection collection) {
        Iterator<E> it = this.unfiltered.iterator();
        boolean z = false;
        while (it.hasNext()) {
            E next = it.next();
            if (this.predicate.A66(next) && !collection.contains(next)) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public int size() {
        int i = 0;
        for (E e : this.unfiltered) {
            if (this.predicate.A66(e)) {
                i++;
            }
        }
        return i;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public Object[] toArray() {
        return AnonymousClass29B.newArrayList(iterator()).toArray();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public Object[] toArray(Object[] objArr) {
        return AnonymousClass29B.newArrayList(iterator()).toArray(objArr);
    }
}
