package X;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.RandomAccess;

/* renamed from: X.3JP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3JP {
    public static boolean any(Iterable iterable, AnonymousClass28S r1) {
        return AnonymousClass1I4.any(iterable.iterator(), r1);
    }

    public static Object getFirst(Iterable iterable, Object obj) {
        return AnonymousClass1I4.getNext(iterable.iterator(), obj);
    }

    public static Object getLast(Iterable iterable) {
        if (!(iterable instanceof List)) {
            return AnonymousClass1I4.getLast(iterable.iterator());
        }
        List list = (List) iterable;
        if (!list.isEmpty()) {
            return getLastInNonemptyList(list);
        }
        throw new NoSuchElementException();
    }

    public static Object getLastInNonemptyList(List list) {
        return list.get(C12980iv.A0C(list));
    }

    public static boolean removeIf(Iterable iterable, AnonymousClass28S r2) {
        if (!(iterable instanceof RandomAccess) || !(iterable instanceof List)) {
            return AnonymousClass1I4.removeIf(iterable.iterator(), r2);
        }
        return removeIfFromRandomAccessList((List) iterable, r2);
    }

    public static boolean removeIfFromRandomAccessList(List list, AnonymousClass28S r7) {
        int i = 0;
        int i2 = 0;
        while (i < list.size()) {
            Object obj = list.get(i);
            if (!r7.A66(obj)) {
                if (i > i2) {
                    try {
                        list.set(i2, obj);
                    } catch (IllegalArgumentException | UnsupportedOperationException unused) {
                        slowRemoveIfForRemainingElements(list, r7, i2, i);
                        return true;
                    }
                }
                i2++;
                continue;
            }
            i++;
        }
        list.subList(i2, list.size()).clear();
        if (i != i2) {
            return true;
        }
        return false;
    }

    public static void slowRemoveIfForRemainingElements(List list, AnonymousClass28S r3, int i, int i2) {
        int size = list.size();
        while (true) {
            size--;
            if (size > i2) {
                if (r3.A66(list.get(size))) {
                    list.remove(size);
                }
            }
        }
        while (true) {
            i2--;
            if (i2 >= i) {
                list.remove(i2);
            } else {
                return;
            }
        }
    }

    public static String toString(Iterable iterable) {
        return AnonymousClass1I4.toString(iterable.iterator());
    }
}
