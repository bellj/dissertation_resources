package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.13n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239213n {
    public final /* synthetic */ C19960ux A00;

    public C239213n(C19960ux r1) {
        this.A00 = r1;
    }

    public C59052tt A00(AbstractC116495Vr r10, UserJid userJid, String str) {
        AnonymousClass01J r1 = this.A00.A01;
        return new C59052tt((C14650lo) r1.A2V.get(), (C246816l) r1.A5q.get(), new AnonymousClass4JY((C246916m) r1.A6Z.get()), r10, userJid, (C17220qS) r1.ABt.get(), str);
    }
}
