package X;

/* renamed from: X.5bl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118405bl extends AnonymousClass0Yo {
    public final /* synthetic */ C128375w0 A00;
    public final /* synthetic */ String A01;

    public C118405bl(C128375w0 r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123375n3.class)) {
            C128375w0 r0 = this.A00;
            C16590pI r3 = r0.A0B;
            C14830m7 r2 = r0.A0A;
            C14850m9 r4 = r0.A0J;
            AbstractC14440lR r13 = r0.A0z;
            AnonymousClass60Y r8 = r0.A0c;
            C20920wX r1 = r0.A00;
            C130155yt r6 = r0.A0W;
            AnonymousClass61F r9 = r0.A0d;
            C129235xO r11 = r0.A0k;
            C130125yq r7 = r0.A0a;
            return new C123375n3(r1, r2, r3, r4, r0.A0S, r6, r7, r8, r9, r0.A0g, r11, r0.A0o, r13, this.A01);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviWithdrawAmountEntryViewModel");
    }
}
