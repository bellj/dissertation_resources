package X;

/* renamed from: X.4Xg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92744Xg {
    public final float A00;
    public final float A01;
    public final C92714Xd A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C92744Xg) {
                C92744Xg r5 = (C92744Xg) obj;
                if (!C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(Float.valueOf(this.A01), Float.valueOf(r5.A01)) || !C16700pc.A0O(Float.valueOf(this.A00), Float.valueOf(r5.A00))) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.A02.hashCode() * 31) + Float.floatToIntBits(this.A01)) * 31) + Float.floatToIntBits(this.A00);
    }

    public C92744Xg(C92714Xd r1, float f, float f2) {
        this.A02 = r1;
        this.A01 = f;
        this.A00 = f2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("StatusSize(size=");
        A0k.append(this.A02);
        A0k.append(", strokeWidth=");
        A0k.append(this.A01);
        A0k.append(", innerStrokeWidth=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
