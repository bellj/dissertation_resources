package X;

import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.4ER  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4ER {
    public static void A00(AnonymousClass584 r14, byte[] bArr, boolean z) {
        Set set;
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        short s = wrap.getShort();
        wrap.get(new byte[32]);
        byte[] bArr2 = new byte[(short) (((short) (wrap.get() & 255)) | 0)];
        wrap.get(bArr2);
        short s2 = wrap.getShort();
        byte b = wrap.get();
        C94354bf r6 = new C94354bf(C72453ed.A1X(wrap));
        AnonymousClass4VO A00 = r6.A00(43);
        if (A00 != null) {
            Set set2 = C88834Hh.A01;
            byte[] bArr3 = A00.A01;
            if (set2.contains(Short.valueOf((short) AnonymousClass3JS.A01(bArr3)))) {
                byte[] bArr4 = r14.A0n;
                if (bArr4 == null || AnonymousClass3JS.A04(bArr3, bArr4)) {
                    if (z) {
                        set = C88834Hh.A03;
                    } else {
                        set = C88834Hh.A04;
                    }
                    HashSet hashSet = new HashSet(r6.A03.keySet());
                    hashSet.removeAll(set);
                    if (hashSet.size() != 0) {
                        throw AnonymousClass1NR.A00("Unexpected extension provided by the server", (byte) 47);
                    }
                    Short sh = C88834Hh.A00;
                    if (s != 771) {
                        StringBuilder A0k = C12960it.A0k("Unexpected protocol version");
                        A0k.append((int) s);
                        throw AnonymousClass1NR.A00(C12960it.A0Z(sh, " != ", A0k), (byte) 80);
                    } else if (AnonymousClass3JS.A04(r14.A0l, bArr2)) {
                        short s3 = r14.A0V;
                        if (s3 != 0 && s3 != s2) {
                            throw AnonymousClass1NR.A00("Cipher suite in server hello does not match HelloRetryRequest cipher suite.", (byte) 47);
                        } else if (s2 != 4865) {
                            throw AnonymousClass1NR.A00("Server selected invalid cipher suite", (byte) 80);
                        } else if (b == 0) {
                            AnonymousClass4VO A002 = r6.A00(51);
                            if (A002 != null) {
                                ByteBuffer wrap2 = ByteBuffer.wrap(A002.A01);
                                short s4 = wrap2.getShort();
                                if (s4 == 29) {
                                    if (!z) {
                                        byte[] bArr5 = new byte[2];
                                        wrap2.get(bArr5);
                                        int A01 = AnonymousClass3JS.A01(bArr5);
                                        if (A01 == 32) {
                                            byte[] bArr6 = new byte[32];
                                            r14.A0m = bArr6;
                                            wrap2.get(bArr6);
                                        } else {
                                            StringBuilder A0k2 = C12960it.A0k("Key length mismatch ");
                                            A0k2.append(A01);
                                            throw AnonymousClass1NR.A00(C12960it.A0e(" != ", A0k2, 32), (byte) 80);
                                        }
                                    }
                                    AnonymousClass4VO A003 = r6.A00(41);
                                    if (!(r14.A0C.A03 == null || A003 == null)) {
                                        if (AnonymousClass3JS.A01(A003.A01) <= 0) {
                                            r14.A0b = true;
                                            r14.A0g = true;
                                        } else {
                                            throw AnonymousClass1NR.A00(C12960it.A0b("Incorrect PSK index value chosen by server ", A003), (byte) 80);
                                        }
                                    }
                                    if (z) {
                                        r14.A0n = bArr3;
                                        r14.A0V = s2;
                                        r14.A0W = s4;
                                        AnonymousClass4VO A004 = r6.A00(44);
                                        if (A004 != null) {
                                            r14.A0k = C72453ed.A1X(ByteBuffer.wrap(A004.A01));
                                        }
                                    }
                                    if (wrap.hasRemaining()) {
                                        throw AnonymousClass1NR.A00("Server Hello has more bytes than expected.", (byte) 80);
                                    }
                                    return;
                                }
                                throw AnonymousClass1NR.A00("Key share algorithm mismatch.", (byte) 80);
                            }
                            throw AnonymousClass1NR.A00("Key share extension not found.", (byte) 109);
                        } else {
                            throw AnonymousClass1NR.A00("Invalid compression method.0", (byte) 80);
                        }
                    } else {
                        throw AnonymousClass1NR.A00("Bad session id", (byte) 80);
                    }
                } else {
                    throw AnonymousClass1NR.A00("Supported version in server hello does not match HelloRetryRequest supported version.", (byte) 47);
                }
            } else {
                throw AnonymousClass1NR.A00("Server sent an unsupported version.", (byte) 110);
            }
        } else {
            throw AnonymousClass1NR.A00("Supported version extension not found.", (byte) 109);
        }
    }
}
