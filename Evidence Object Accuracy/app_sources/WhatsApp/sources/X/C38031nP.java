package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.regex.Pattern;

/* renamed from: X.1nP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38031nP implements AbstractC16830pp {
    public C15450nH A00;
    public AnonymousClass018 A01;
    public AbstractC16830pp A02;

    @Override // X.AbstractC16830pp
    public String AFO(AnonymousClass18M r2, AbstractC15340mz r3) {
        return "";
    }

    @Override // X.AbstractC16830pp
    public String AHL(AnonymousClass1IR r2) {
        return "";
    }

    @Override // X.AbstractC16830pp
    public /* synthetic */ AnonymousClass1V8 AZQ(AnonymousClass1V8 r1) {
        return r1;
    }

    public C38031nP(C15450nH r1, AnonymousClass018 r2, C17900ra r3, AbstractC16830pp r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r4;
        AdF(r3);
    }

    @Override // X.AbstractC16830pp
    public boolean A70() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.A70();
        }
        return false;
    }

    @Override // X.AbstractC16830pp
    public boolean A71() {
        AbstractC16830pp r0 = this.A02;
        return r0 != null && r0.A71();
    }

    @Override // X.AbstractC16830pp
    public void A9W(AnonymousClass1IR r2, AnonymousClass1IR r3) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            r0.A9W(r2, r3);
        }
    }

    @Override // X.AbstractC16830pp
    public Class AAV() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AAV();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AAW() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AAW();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Intent AAX(Context context) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AAX(context);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class ABI() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABI();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public C38161nc ABU() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABU();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class ABa() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABa();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class ABc() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABc();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class ABd() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABd();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17Y ABn() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABn();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass18N ABo() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABo();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass18M ABq() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABq();
        }
        return null;
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass5W2 ABr() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABr();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17U ABw() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ABw();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public int AC0(String str) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AC0(str);
        }
        return -1;
    }

    @Override // X.AbstractC16830pp
    public AbstractC38231nk ACH() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ACH();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public String ACI() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ACI();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Intent ACS(Context context, boolean z) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ACS(context, true);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Intent ACT(Context context, Uri uri) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ACT(context, uri);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public int ACY() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ACY();
        }
        return 0;
    }

    @Override // X.AbstractC16830pp
    public Intent ACd(Context context, String str, String str2) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ACd(context, str, str2);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC16870pt ACx() {
        AbstractC16830pp r0 = this.A02;
        AnonymousClass009.A05(r0);
        return r0.ACx();
    }

    @Override // X.AbstractC16830pp
    public Intent ADR(Context context) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.ADR(context);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17T AEE() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AEE();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC92724Xe AEF() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AEF();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass1V8 AEY(AnonymousClass20C r2) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AEY(r2);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AEb(Bundle bundle) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AEb(bundle);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC43531xB AEz() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AEz();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public List AF1(AnonymousClass1IR r2, AnonymousClass1IS r3) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF1(r2, r3);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public List AF2(AnonymousClass1IR r2, AnonymousClass1IS r3) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF2(r2, r3);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass18O AF4() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF4();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC116085Ub AF5() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF5();
        }
        return new C1111158d();
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass5Wu AF6(AnonymousClass018 r2, C14850m9 r3, C22460z7 r4, AbstractC116085Ub r5) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF6(r2, r3, r4, r5);
        }
        return new C69963aW(r2, r3, r4, r5);
    }

    @Override // X.AbstractC16830pp
    public Class AF7() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF7();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC450620a AF8() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF8();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public String AF9() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AF9();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass17W AFA() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFA();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass5UU AFB(C16590pI r2, C18600si r3) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFB(r2, r3);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public int AFC() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFC();
        }
        return R.string.localized_app_name;
    }

    @Override // X.AbstractC16830pp
    public Class AFD() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFD();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass5X2 AFE() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFE();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AFF() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFF();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public int AFH() {
        AbstractC16830pp r0 = this.A02;
        return r0 != null ? r0.AFH() : R.string.default_payment_id_name;
    }

    @Override // X.AbstractC16830pp
    public Pattern AFI() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFI();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC38191ng AFJ() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFJ();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC38141na AFL() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFL();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Pattern AFN() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFN();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AnonymousClass24Y AFQ() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFQ();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AFR() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFR();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public int AFS() {
        AbstractC16830pp r0 = this.A02;
        return r0 != null ? r0.AFS() : R.string.default_payment_pin_name;
    }

    @Override // X.AbstractC16830pp
    public Class AFT() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFT();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC51302Tt AFU() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFU();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AFV() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFV();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AFa() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFa();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public AbstractC38181ne AFb() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFb();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AFc() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFc();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AFd() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFd();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Intent AFe(Context context, String str, boolean z) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFe(context, str, true);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AFg() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AFg();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Class AG9() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AG9();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public String AGP(AnonymousClass1IR r2) {
        AbstractC16830pp r0 = this.A02;
        return r0 != null ? r0.AGP(r2) : "";
    }

    @Override // X.AbstractC16830pp
    public Class AGb() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AGb();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public int AGi() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AGi();
        }
        return 0;
    }

    @Override // X.AbstractC16830pp
    public String AH4(String str) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AH4(str);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public Intent AHG(Context context, String str) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AHG(context, str);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public int AHJ(AnonymousClass1IR r2) {
        return R.color.payments_status_gray;
    }

    @Override // X.AbstractC16830pp
    public C47822Cw AHU(AnonymousClass1ZO r2, UserJid userJid, String str) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AHU(r2, userJid, str);
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public boolean AIG() {
        AbstractC16830pp r0 = this.A02;
        return r0 != null && r0.AIG();
    }

    @Override // X.AbstractC16840pq
    public AbstractC30851Zb AIg() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AIg();
        }
        return null;
    }

    @Override // X.AbstractC16840pq
    public AbstractC30871Zd AIh() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AIh();
        }
        return null;
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass1ZO AIi() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AIi();
        }
        return null;
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass1ZX AIj() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AIj();
        }
        return null;
    }

    @Override // X.AbstractC16840pq
    public AbstractC30891Zf AIk() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AIk();
        }
        return null;
    }

    @Override // X.AbstractC16840pq
    public AnonymousClass1ZZ AIl() {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AIl();
        }
        return null;
    }

    @Override // X.AbstractC16830pp
    public boolean AJH() {
        AbstractC16830pp r0 = this.A02;
        return r0 != null && r0.AJH();
    }

    @Override // X.AbstractC16830pp
    public boolean AJs(Uri uri) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            return r0.AJs(uri);
        }
        return false;
    }

    @Override // X.AbstractC16830pp
    public boolean AKH(C89674Kw r3) {
        AbstractC16830pp r0 = this.A02;
        return r0 != null && r0.AKH(r3);
    }

    @Override // X.AbstractC16830pp
    public void AKc(Uri uri) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            r0.AKc(uri);
        }
    }

    @Override // X.AbstractC16830pp
    public void ALp(Context context, AbstractC13860kS r3, AnonymousClass1IR r4) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            r0.ALp(context, r3, r4);
        }
    }

    @Override // X.AbstractC16830pp
    public void AZN(C456522m r2, List list) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            r0.AZN(r2, list);
        }
    }

    @Override // X.AbstractC16830pp
    public void AdF(C17900ra r2) {
        AbstractC16830pp r0 = this.A02;
        if (r0 != null) {
            r0.AdF(r2);
        }
    }

    @Override // X.AbstractC16830pp
    public boolean AdP() {
        AbstractC16830pp r0 = this.A02;
        return r0 != null && r0.AdP();
    }
}
