package X;

import java.util.Arrays;

/* renamed from: X.4wD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106744wD implements AbstractC116785Ww {
    public AbstractC14070ko A00;
    public AnonymousClass4Y4 A01;
    public boolean A02;

    public final boolean A00(AnonymousClass5Yf r9) {
        C92864Xs r2 = new C92864Xs();
        if (r2.A01(r9, true) && (r2.A03 & 2) == 2) {
            int min = Math.min(r2.A00, 8);
            C95304dT A05 = C95304dT.A05(min);
            C95304dT.A06(r9, A05, min);
            A05.A0S(0);
            if (C95304dT.A00(A05) >= 5 && A05.A0C() == 127 && A05.A0I() == 1179402563) {
                this.A01 = new C76893mP();
                return true;
            }
            A05.A0S(0);
            try {
                if (C92954Yd.A00(A05, 1, true)) {
                    this.A01 = new C76903mQ();
                    return true;
                }
            } catch (AnonymousClass496 unused) {
            }
            A05.A0S(0);
            int A00 = C95304dT.A00(A05);
            byte[] bArr = C76913mR.A01;
            int length = bArr.length;
            if (A00 >= length) {
                byte[] bArr2 = new byte[length];
                A05.A0V(bArr2, 0, length);
                if (Arrays.equals(bArr2, bArr)) {
                    this.A01 = new C76913mR();
                    return true;
                }
            }
        }
        return false;
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:103:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:114:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r21, X.AnonymousClass4IG r22) {
        /*
        // Method dump skipped, instructions count: 617
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106744wD.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        AnonymousClass4Y4 r3 = this.A01;
        if (r3 != null) {
            C92314Vk r5 = r3.A0C;
            C92864Xs r0 = r5.A02;
            r0.A03 = 0;
            r0.A04 = 0;
            r0.A02 = 0;
            r0.A01 = 0;
            r0.A00 = 0;
            r5.A03.A0Q(0);
            r5.A00 = -1;
            r5.A01 = false;
            if (j == 0) {
                r3.A00(!r3.A0B);
            } else if (r3.A01 != 0) {
                long A0W = C72453ed.A0W((long) r3.A00, j2);
                r3.A05 = A0W;
                r3.A08.AeD(A0W);
                r3.A01 = 2;
            }
        }
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r2) {
        try {
            return A00(r2);
        } catch (AnonymousClass496 unused) {
            return false;
        }
    }
}
