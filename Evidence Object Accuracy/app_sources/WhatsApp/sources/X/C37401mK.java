package X;

/* renamed from: X.1mK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C37401mK extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public String A03;
    public String A04;
    public String A05;

    public C37401mK() {
        super(3600, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A03);
        r3.Abe(6, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A04);
        r3.Abe(4, this.A02);
        r3.Abe(5, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamAvatarBloksLaunch {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "avatarBloksLaunchErrorReason", this.A03);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "avatarBloksLaunchErrorType", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "avatarBloksLaunchEvent", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "avatarEditorSessionId", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "fetchT", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "waAvatarSessionId", this.A05);
        sb.append("}");
        return sb.toString();
    }
}
