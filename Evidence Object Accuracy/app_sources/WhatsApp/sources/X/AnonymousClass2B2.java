package X;

/* renamed from: X.2B2  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2B2 {
    public int A00;
    public final int A01;

    public AnonymousClass2B2(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
    }

    public void A00() {
        AnonymousClass4Xy r0;
        if (!(this instanceof AnonymousClass39C)) {
            AnonymousClass39D r2 = (AnonymousClass39D) this;
            if (!r2.A05) {
                r2.A05 = true;
                C43161wW r02 = r2.A04;
                if (r02 != null) {
                    r2.A02 = r02.A02();
                }
                r0 = r2.A0B;
            } else {
                return;
            }
        } else {
            AnonymousClass39C r1 = (AnonymousClass39C) this;
            r1.A01 = true;
            r0 = r1.A07;
        }
        r0.A02();
    }

    public void A01() {
        if (!(this instanceof AnonymousClass39C)) {
            AnonymousClass39D r1 = (AnonymousClass39D) this;
            r1.A0D.A00();
            r1.A0C.A00();
            r1.A0B.A00();
            r1.A0A.A00();
            r1.A00 = 4;
            return;
        }
        ((AnonymousClass39C) this).A08.A00();
    }
}
