package X;

import android.content.Context;
import android.os.Build;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Locale;
import java.util.zip.ZipFile;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.01n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C003501n {
    public static Boolean A00;
    public static Boolean A01;
    public static Boolean A02;
    public static String A03;
    public static final AnonymousClass01H A04 = new C002601e(null, new AnonymousClass01N() { // from class: X.026
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            try {
                Class.forName("androidx.test.espresso.Espresso");
                return Boolean.TRUE;
            } catch (ClassNotFoundException unused) {
                return Boolean.FALSE;
            }
        }
    });
    public static final byte[] A05 = {0, 2};

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 4, insn: 0x0029: INVOKE  (r4 I:java.lang.StringBuilder), (r0 I:java.lang.String) type: VIRTUAL call: java.lang.StringBuilder.append(java.lang.String):java.lang.StringBuilder, block:B:12:0x0027
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static java.lang.String A00() {
        /*
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r3 = 0
            java.lang.String r1 = "/proc/sys/kernel/osrelease"
            java.io.FileReader r0 = new java.io.FileReader     // Catch: all -> 0x001d
            r0.<init>(r1)     // Catch: all -> 0x001d
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch: all -> 0x001d
            r2.<init>(r0)     // Catch: all -> 0x001d
            java.lang.String r0 = r2.readLine()     // Catch: all -> 0x0020
            r4.append(r0)     // Catch: all -> 0x0020
            r2.close()     // Catch: IOException -> 0x0027
            goto L_0x002c
        L_0x001d:
            r0 = move-exception
            r2 = r3
            goto L_0x0021
        L_0x0020:
            r0 = move-exception
        L_0x0021:
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch: IOException -> 0x0027
        L_0x0026:
            throw r0     // Catch: IOException -> 0x0027
        L_0x0027:
            java.lang.String r0 = "Unknown release"
            r4.append(r0)
        L_0x002c:
            java.lang.String r1 = "/proc/sys/kernel/version"
            java.io.FileReader r0 = new java.io.FileReader     // Catch: all -> 0x0048
            r0.<init>(r1)     // Catch: all -> 0x0048
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch: all -> 0x0048
            r1.<init>(r0)     // Catch: all -> 0x0048
            r0 = 32
            r4.append(r0)     // Catch: all -> 0x004b
            java.lang.String r0 = r1.readLine()     // Catch: all -> 0x004b
            r4.append(r0)     // Catch: all -> 0x004b
            r1.close()     // Catch: IOException -> 0x0052
            goto L_0x0057
        L_0x0048:
            r0 = move-exception
            r1 = r2
            goto L_0x004c
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()     // Catch: IOException -> 0x0052
        L_0x0051:
            throw r0     // Catch: IOException -> 0x0052
        L_0x0052:
            java.lang.String r0 = " unknown version"
            r4.append(r0)
        L_0x0057:
            java.lang.String r0 = r4.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C003501n.A00():java.lang.String");
    }

    public static String A01(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            return A04(instance.digest());
        } catch (NoSuchAlgorithmException unused) {
            return null;
        }
    }

    public static String A02(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(str.getBytes());
            return A04(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public static String A03(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int length = bArr.length;
        char[] cArr2 = new char[length << 1];
        int i = 0;
        for (byte b : bArr) {
            int i2 = b & 255;
            int i3 = i + 1;
            cArr2[i] = cArr[i2 >>> 4];
            i = i3 + 1;
            cArr2[i3] = cArr[i2 & 15];
        }
        return new String(cArr2);
    }

    public static String A04(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            sb.append(Character.forDigit((b >>> 4) & 15, 16));
            sb.append(Character.forDigit(b & 15, 16));
        }
        return sb.toString();
    }

    public static SecretKey A05(byte[] bArr, byte[] bArr2, int i) {
        int length = bArr.length;
        char[] cArr = new char[length];
        for (int i2 = 0; i2 < length; i2++) {
            cArr[i2] = (char) bArr[i2];
        }
        try {
            try {
                return SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512").generateSecret(new PBEKeySpec(cArr, bArr2, i, 512));
            } catch (InvalidKeySpecException e) {
                throw new AssertionError(e);
            }
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public static SecretKey A06(byte[] bArr, byte[] bArr2, int i, int i2) {
        int length = bArr.length;
        char[] cArr = new char[length];
        for (int i3 = 0; i3 < length; i3++) {
            cArr[i3] = (char) bArr[i3];
        }
        try {
            try {
                return SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1And8BIT").generateSecret(new PBEKeySpec(cArr, bArr2, i, i2));
            } catch (InvalidKeySpecException e) {
                throw new AssertionError(e);
            }
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public static void A07(Context context) {
        new File(context.getFilesDir(), "rc2").delete();
    }

    public static void A08(File file, byte[] bArr) {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(bArr);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (Throwable th) {
            try {
                fileOutputStream.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public static boolean A09() {
        String str = A03;
        if (str == null) {
            String str2 = Build.MANUFACTURER;
            str = "";
            if (str2 == null) {
                str2 = str;
            }
            String str3 = Build.VERSION.RELEASE;
            if (str3 == null) {
                str3 = str;
            }
            String str4 = Build.DISPLAY;
            if (str4 == null) {
                str4 = str;
            }
            String str5 = Build.MODEL;
            if (str5 == null) {
                str5 = str;
            }
            String A002 = A00();
            Locale locale = Locale.US;
            if (A002.toLowerCase(locale).contains("cyanogen")) {
                str = "cyanogen";
            } else if (str2.toLowerCase(locale).contains("debug") || str3.toLowerCase(locale).contains("debug") || str4.toLowerCase(locale).contains("debug") || str5.toLowerCase(locale).contains("debug") || A002.toLowerCase(locale).contains("debug")) {
                str = "debug";
            } else if (str4.startsWith("Darky")) {
                str = "darky";
            } else if (str2.startsWith("XDAndroid")) {
                str = "xdandroid";
            } else if (str3.startsWith("FroydVillain")) {
                str = "froydvillain";
            } else if (str3.startsWith("VillainROM") || str4.startsWith("VillainROM")) {
                str = "villainrom";
            } else if (str3.startsWith("WildPuzzle")) {
                str = "wildpuzzle";
            } else if (str4.startsWith("MIUI")) {
                str = "miui";
            } else if (str4.startsWith("ITFUNZ")) {
                str = "itfunz";
            } else if (str4.startsWith("DebusROM")) {
                str = "debus";
            } else if (str4.startsWith("FreeX10")) {
                str = "freex10";
            } else if (str4.startsWith("Perception Build")) {
                str = "perception";
            } else if (str4.startsWith("Bionix")) {
                str = "bionix";
            } else if (str4.startsWith("Lite'ning Rom")) {
                str = "litening";
            } else if (str4.startsWith("GINGERVillain")) {
                str = "gingervillian";
            } else if (str4.startsWith("GingerReal")) {
                str = "gingerreal";
            } else if (str4.startsWith("R.U.R.1920")) {
                str = "rur1920";
            } else if (str5.startsWith("MoDaCo")) {
                str = "modaco";
            } else if (str4.startsWith("CriskeloROM")) {
                str = "criskelorom";
            } else if (str4.startsWith("LeeDrOiD")) {
                str = "leedroid";
            } else if (str4.startsWith("Dexter's FolioMod")) {
                str = "foliomod";
            } else if (str4.startsWith("Andro-ID")) {
                str = "andro-id";
            } else if (str4.startsWith("FroyoPlus")) {
                str = "froyoplus";
            } else if (str4.startsWith("PilotxRom")) {
                str = "pilotx";
            } else if (str4.startsWith("Achotjan")) {
                str = "achotjan";
            } else if (A002.contains("FuguMod")) {
                str = "fugu";
            } else if (A002.contains("fakeShmoo")) {
                str = "fakeshmoo";
            } else if (A002.contains("LorDmodNCTeam")) {
                str = "lordmod";
            } else if (A002.contains("-RCMIX")) {
                str = "rcmix";
            } else if (str4.contains("DamianGTO")) {
                str = "damiangto";
            }
            StringBuilder sb = new StringBuilder("app/custom-rom ");
            sb.append(str);
            Log.i(sb.toString());
            A03 = str;
        }
        return str.length() != 0;
    }

    public static boolean A0A(Context context, String str, byte[] bArr) {
        new String(bArr);
        File file = new File(context.getFilesDir(), "rc2");
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass029.A0M);
        sb.append(str);
        String obj = sb.toString();
        byte[] bArr2 = A05;
        try {
            byte[] A0D = A0D(4);
            byte[] A0D2 = A0D(16);
            SecretKeySpec secretKeySpec = new SecretKeySpec(new SecretKeySpec(A06(obj.getBytes(), A0D, 16, 128).getEncoded(), "AES").getEncoded(), "AES/OFB/NoPadding");
            Cipher instance = Cipher.getInstance("AES/OFB/NoPadding");
            instance.init(1, secretKeySpec, new IvParameterSpec(A0D2));
            byte[] doFinal = instance.doFinal(bArr);
            byte[] bArr3 = new byte[bArr2.length + A0D.length + A0D2.length + doFinal.length];
            byte[][] bArr4 = {bArr2, A0D, A0D2, doFinal};
            int i = 0;
            int i2 = 0;
            do {
                byte[] bArr5 = bArr4[i];
                System.arraycopy(bArr5, 0, bArr3, i2, bArr5.length);
                i2 += bArr5.length;
                i++;
            } while (i < 4);
            A08(file, bArr3);
            return Arrays.equals(bArr3, A0G(file));
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("unable to write ");
            sb2.append(file.toString());
            Log.e(sb2.toString(), e);
            return false;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:70:0x0041 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:20:0x0046 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:61:0x005f */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v0 */
    /* JADX WARN: Type inference failed for: r5v1 */
    /* JADX WARN: Type inference failed for: r5v2 */
    /* JADX WARN: Type inference failed for: r5v3 */
    /* JADX WARN: Type inference failed for: r5v4 */
    /* JADX WARN: Type inference failed for: r5v5, types: [boolean] */
    /* JADX WARN: Type inference failed for: r5v6 */
    /* JADX WARN: Type inference failed for: r5v7 */
    /* JADX WARN: Type inference failed for: r5v8 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0B(X.AnonymousClass01d r9) {
        /*
        // Method dump skipped, instructions count: 314
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C003501n.A0B(X.01d):boolean");
    }

    public static byte[] A0C() {
        try {
            KeyGenerator instance = KeyGenerator.getInstance("AES");
            instance.init(160, C002901h.A00());
            return instance.generateKey().getEncoded();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] A0D(int i) {
        byte[] bArr = new byte[i];
        C002901h.A00().nextBytes(bArr);
        return bArr;
    }

    public static byte[] A0E(Context context) {
        try {
            ZipFile zipFile = new ZipFile(context.getPackageCodePath());
            InputStream inputStream = zipFile.getInputStream(zipFile.getEntry("classes.dex"));
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                byte[] bArr = new byte[DefaultCrypto.BUFFER_SIZE];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read > 0) {
                        instance.update(bArr, 0, read);
                    } else {
                        byte[] digest = instance.digest();
                        inputStream.close();
                        zipFile.close();
                        return digest;
                    }
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (Exception e) {
            Log.e("app/md5/bytes/error ", e);
            try {
                return "null".getBytes(AnonymousClass01V.A08);
            } catch (UnsupportedEncodingException e2) {
                throw new Error(e2);
            }
        }
    }

    public static byte[] A0F(Context context, String str) {
        try {
            File file = new File(context.getFilesDir(), "rc2");
            StringBuilder sb = new StringBuilder();
            sb.append(AnonymousClass029.A0M);
            sb.append(str);
            String obj = sb.toString();
            byte[] bArr = A05;
            byte[] A0G = A0G(file);
            if (A0G == null) {
                return null;
            }
            try {
                int length = A0G.length;
                int length2 = bArr.length;
                if (length >= length2 + 4 + 16 + 20) {
                    byte[] bArr2 = new byte[length2];
                    System.arraycopy(A0G, 0, bArr2, 0, length2);
                    int i = length2 + 0;
                    if (Arrays.equals(bArr2, bArr)) {
                        byte[] bArr3 = new byte[4];
                        System.arraycopy(A0G, i, bArr3, 0, 4);
                        int i2 = i + 4;
                        byte[] bArr4 = new byte[16];
                        System.arraycopy(A0G, i2, bArr4, 0, 16);
                        int i3 = i2 + 16;
                        SecretKeySpec secretKeySpec = new SecretKeySpec(new SecretKeySpec(A06(obj.getBytes(), bArr3, 16, 128).getEncoded(), "AES").getEncoded(), "AES/OFB/NoPadding");
                        Cipher instance = Cipher.getInstance("AES/OFB/NoPadding");
                        instance.init(2, secretKeySpec, new IvParameterSpec(bArr4));
                        return instance.doFinal(A0G, i3, length - i3);
                    }
                    throw new AnonymousClass03G();
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(file.toString());
                sb2.append(" size mismatch");
                throw new InvalidParameterException(sb2.toString());
            } catch (Exception e) {
                file.delete();
                if (e instanceof AnonymousClass03G) {
                    throw e;
                }
                throw new RuntimeException(e);
            }
        } catch (AnonymousClass03G e2) {
            Log.w("recovery token header mismatch", e2);
            return null;
        }
    }

    public static byte[] A0G(File file) {
        if (!file.exists() || file.length() <= 0) {
            return null;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            byte[] bArr = (byte[]) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return bArr;
        } catch (Exception e) {
            Log.w("get byte array", e);
            return null;
        }
    }

    public static byte[] A0H(byte[] bArr, byte[]... bArr2) {
        try {
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(new SecretKeySpec(bArr, "HmacSHA1"));
            for (byte[] bArr3 : bArr2) {
                instance.update(bArr3);
            }
            return instance.doFinal();
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }
    }
}
