package X;

/* renamed from: X.41g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C851241g extends AnonymousClass2Dn {
    public final /* synthetic */ AnonymousClass1PZ A00;

    public C851241g(AnonymousClass1PZ r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        this.A00.A02();
    }

    @Override // X.AnonymousClass2Dn
    public void A02(AbstractC14640lm r3) {
        AnonymousClass1PZ r1 = this.A00;
        if (AnonymousClass1PZ.A00(r1, r3)) {
            r1.A03();
        }
    }
}
