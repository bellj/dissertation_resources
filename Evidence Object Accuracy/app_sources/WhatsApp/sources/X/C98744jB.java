package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98744jB implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                z = C12960it.A1S(C95664e9.A02(parcel, readInt));
            } else if (c == 3) {
                z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
            } else if (c == 4) {
                C95664e9.A0F(parcel, readInt, 4);
                i2 = parcel.readInt();
            } else if (c != 5) {
                C95664e9.A0D(parcel, readInt);
            } else {
                i3 = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56412kq(i, i2, i3, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C56412kq[i];
    }
}
