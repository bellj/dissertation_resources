package X;

import java.util.Map;

/* renamed from: X.2Tx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51322Tx {
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05 = Long.MAX_VALUE;
    public Map A06;
    public Map A07;
    public boolean A08;
    public final AbstractC15710nm A09;
    public final C14830m7 A0A;
    public final C16120oU A0B;
    public final C450720b A0C;
    public final AnonymousClass206 A0D;

    public C51322Tx(AbstractC15710nm r3, C14830m7 r4, C16120oU r5, C450720b r6, AnonymousClass206 r7, Map map, Map map2) {
        this.A0A = r4;
        this.A09 = r3;
        this.A0B = r5;
        this.A0C = r6;
        this.A07 = map;
        this.A06 = map2;
        this.A0D = r7;
    }

    public void A00(boolean z) {
        if (!this.A08) {
            this.A08 = true;
            if (this.A01 + this.A03 + this.A02 != 0) {
                C615930y r7 = new C615930y();
                r7.A05 = Long.valueOf(this.A04 - this.A00);
                long A00 = this.A0A.A00();
                long j = this.A00;
                r7.A01 = Long.valueOf(A00 - j);
                r7.A00 = Boolean.valueOf(!z);
                r7.A03 = Long.valueOf(this.A01);
                r7.A06 = Long.valueOf(this.A03);
                r7.A04 = Long.valueOf(this.A02);
                long j2 = this.A05;
                if (j2 != Long.MAX_VALUE) {
                    r7.A02 = Long.valueOf((j - j2) / 86400);
                }
                this.A0B.A07(r7);
            }
        }
    }
}
