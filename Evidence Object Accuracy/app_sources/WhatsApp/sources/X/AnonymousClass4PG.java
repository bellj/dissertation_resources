package X;

import java.util.List;

/* renamed from: X.4PG  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4PG {
    public final AnonymousClass3IT A00;
    public final C14170ky A01;
    public final List A02 = C12960it.A0l();

    public AnonymousClass4PG(C14170ky r3, AbstractC115095Qe r4) {
        C13020j0.A01(r3);
        this.A01 = r3;
        AnonymousClass3IT r1 = new AnonymousClass3IT(this, r4);
        r1.A06 = true;
        this.A00 = r1;
    }
}
