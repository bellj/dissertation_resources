package X;

import android.content.SharedPreferences;

/* renamed from: X.0xF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21330xF {
    public SharedPreferences A00;
    public final C16630pM A01;

    public C21330xF(C16630pM r1) {
        this.A01 = r1;
    }

    public synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("com.whatsapp_business_directory");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }
}
