package X;

import java.io.IOException;

/* renamed from: X.2GB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GB extends IOException implements AnonymousClass2GC {
    public final int errorCode;

    public AnonymousClass2GB(int i, String str) {
        super(str);
        this.errorCode = i;
    }

    public AnonymousClass2GB(String str, Throwable th, int i) {
        super(str, th);
        this.errorCode = i;
    }

    public AnonymousClass2GB(Throwable th) {
        super(th);
        this.errorCode = 200;
    }

    @Override // X.AnonymousClass2GC
    public int AER() {
        return this.errorCode;
    }

    @Override // java.lang.Throwable
    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.getMessage());
        sb.append(" (error_code=");
        sb.append(this.errorCode);
        sb.append(")");
        return sb.toString();
    }
}
