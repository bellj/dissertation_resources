package X;

import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;

/* renamed from: X.1iV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35681iV extends AbstractC35561iG {
    public C35691iW A00;
    public boolean A01;
    public final C14900mE A02;
    public final C22330yu A03;
    public final C15550nR A04;
    public final AnonymousClass10S A05;
    public final C15610nY A06;
    public final C21270x9 A07;
    public final C14830m7 A08;
    public final C14820m6 A09;
    public final AnonymousClass018 A0A;
    public final AbstractC18860tB A0B = new C35671iU(this);
    public final AnonymousClass12H A0C;
    public final C244215l A0D;
    public final AnonymousClass01H A0E;

    public C35681iV(AnonymousClass12P r29, AbstractC15710nm r30, C14900mE r31, Mp4Ops mp4Ops, C239613r r33, C18790t3 r34, C16170oZ r35, C22330yu r36, C15550nR r37, AnonymousClass10S r38, C15610nY r39, AnonymousClass1J1 r40, C21270x9 r41, C14830m7 r42, C16590pI r43, C14820m6 r44, AnonymousClass018 r45, C15650ng r46, AnonymousClass12H r47, C22810zg r48, C18470sV r49, C14850m9 r50, C244215l r51, C244415n r52, AnonymousClass109 r53, AnonymousClass1CH r54, AbstractC15340mz r55, C15860o1 r56, AnonymousClass1BD r57, AnonymousClass1CW r58, AnonymousClass1CZ r59, C48252Fe r60, AnonymousClass1CY r61, AbstractC14440lR r62, C17020q8 r63, AnonymousClass01H r64) {
        super(r29, r30, r31, mp4Ops, r33, r34, r35, r40, r43, r45, r46, r48, r49, r50, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63);
        this.A08 = r42;
        this.A02 = r31;
        this.A07 = r41;
        this.A04 = r37;
        this.A06 = r39;
        this.A0A = r45;
        this.A05 = r38;
        this.A0C = r47;
        this.A03 = r36;
        this.A09 = r44;
        this.A0D = r51;
        this.A0E = r64;
    }

    @Override // X.AbstractC33621eg, X.AbstractC33631eh
    public void A01() {
        super.A01();
        C33521eH r2 = A0R().A06;
        r2.A0C.A00();
        r2.A0A.A04(r2.A09);
        r2.A07.A04(r2.A06);
        r2.A0I.A04(r2.A0H);
        r2.A05.A0G(r2.A0L);
        r2.A00 = true;
        C35691iW r0 = this.A00;
        if (r0 != null) {
            r0.A03(true);
            this.A00 = null;
        }
        this.A0C.A04(this.A0B);
    }

    @Override // X.AbstractC33621eg, X.AbstractC33631eh
    public void A08(Rect rect) {
        View view = A0R().A02;
        if (view != null) {
            view.setPadding(rect.left, 0, rect.right, rect.bottom);
        }
        super.A08(rect);
    }

    @Override // X.AbstractC33621eg
    public View A09(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        View A09 = super.A09(layoutInflater, viewGroup);
        AnonymousClass009.A03(A09);
        AnonymousClass47E A0R = A0R();
        A0R.A0A.removeAllViews();
        layoutInflater.inflate(R.layout.status_playback_page_info_outgoing, A0R.A0A, true);
        C14830m7 r10 = this.A08;
        C14900mE r4 = this.A02;
        C21270x9 r9 = this.A07;
        C15550nR r6 = this.A04;
        C15610nY r8 = this.A06;
        AnonymousClass018 r12 = this.A0A;
        AnonymousClass10S r7 = this.A05;
        A0R.A06 = new C33521eH(A0R.A08, r4, this.A03, r6, r7, r8, r9, r10, this.A09, r12, this.A0D, this.A0E);
        A0R.A04 = (ImageView) A09.findViewById(R.id.status_playback_views_icon);
        A0R.A05 = (TextView) A09.findViewById(R.id.read_receipt_counter);
        A0R.A00 = A09.findViewById(R.id.delete);
        A0R.A01 = A09.findViewById(R.id.forward);
        A0R.A03 = A09.findViewById(R.id.more);
        A0R.A02 = A0R.A08.findViewById(R.id.list_container);
        return A09;
    }

    @Override // X.AbstractC35561iG, X.AbstractC33621eg
    public void A0H() {
        if (this.A01) {
            super.A0H();
        }
    }

    @Override // X.AbstractC33621eg
    public void A0I(int i) {
        super.A0I(i);
        if (i == 3) {
            A0R().A06.A04.sendAccessibilityEvent(8);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0075, code lost:
        if (X.C30041Vv.A15((X.AnonymousClass1X4) r5) == false) goto L_0x0077;
     */
    @Override // X.AbstractC33621eg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0K(android.view.View r9) {
        /*
            r8 = this;
            super.A0K(r9)
            X.47E r6 = r8.A0R()
            android.view.ViewGroup r3 = r6.A08
            int r2 = r3.getPaddingLeft()
            android.view.ViewGroup r0 = r6.A08
            int r1 = r0.getPaddingTop()
            android.view.ViewGroup r0 = r6.A08
            int r0 = r0.getPaddingRight()
            r7 = 0
            r3.setPadding(r2, r1, r0, r7)
            android.view.View r2 = r6.A06
            r1 = 42
            com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4 r0 = new com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4
            r0.<init>(r8, r1)
            r2.setOnClickListener(r0)
            android.view.ViewGroup r2 = r6.A0A
            r1 = 44
            com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2 r0 = new com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2
            r0.<init>(r8, r1)
            r2.setOnClickListener(r0)
            android.view.View r2 = r6.A00
            if (r2 == 0) goto L_0x0043
            r1 = 45
            com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2 r0 = new com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2
            r0.<init>(r8, r1)
            r2.setOnClickListener(r0)
        L_0x0043:
            android.view.View r2 = r6.A01
            if (r2 == 0) goto L_0x0051
            r1 = 46
            com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2 r0 = new com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2
            r0.<init>(r8, r1)
            r2.setOnClickListener(r0)
        L_0x0051:
            X.1iW r1 = r8.A00
            if (r1 == 0) goto L_0x0059
            r0 = 1
            r1.A03(r0)
        L_0x0059:
            X.1iW r2 = new X.1iW
            r2.<init>(r8)
            r8.A00 = r2
            X.0lR r1 = r8.A0R
            java.lang.Void[] r0 = new java.lang.Void[r7]
            r1.Aaz(r2, r0)
            X.0mz r5 = r8.A09
            boolean r0 = r5 instanceof X.AnonymousClass1X2
            if (r0 == 0) goto L_0x0077
            r0 = r5
            X.1X4 r0 = (X.AnonymousClass1X4) r0
            boolean r1 = X.C30041Vv.A15(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0078
        L_0x0077:
            r0 = 0
        L_0x0078:
            r8.A01 = r0
            if (r0 != 0) goto L_0x009c
            long r3 = r5.A12
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x009c
            r8.A0Q()
        L_0x0087:
            boolean r0 = X.C30041Vv.A0m(r5)
            if (r0 == 0) goto L_0x0094
            android.view.ViewGroup r1 = r6.A08
            r0 = 8
            r1.setVisibility(r0)
        L_0x0094:
            X.12H r1 = r8.A0C
            X.0tB r0 = r8.A0B
            r1.A03(r0)
            return
        L_0x009c:
            r8.A0P(r7)
            r8.A0H()
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C35681iV.A0K(android.view.View):void");
    }

    public AnonymousClass47E A0R() {
        C35571iH r0 = ((AbstractC33621eg) this).A01;
        if (r0 == null) {
            r0 = new AnonymousClass47E(this);
            ((AbstractC33621eg) this).A01 = r0;
        }
        return (AnonymousClass47E) r0;
    }
}
