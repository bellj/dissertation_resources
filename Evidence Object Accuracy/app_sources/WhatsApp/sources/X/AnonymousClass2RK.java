package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2RK  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2RK extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2RK A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public long A01;
    public C57222mg A02;
    public C56902m8 A03;
    public AnonymousClass1G8 A04;

    static {
        AnonymousClass2RK r0 = new AnonymousClass2RK();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r12, Object obj, Object obj2) {
        AnonymousClass1G9 r1;
        int i;
        switch (r12.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r4 = (AbstractC462925h) obj;
                AnonymousClass2RK r14 = (AnonymousClass2RK) obj2;
                this.A04 = (AnonymousClass1G8) r4.Aft(this.A04, r14.A04);
                this.A02 = (C57222mg) r4.Aft(this.A02, r14.A02);
                this.A03 = (C56902m8) r4.Aft(this.A03, r14.A03);
                int i2 = this.A00;
                boolean z = false;
                if ((i2 & 8) == 8) {
                    z = true;
                }
                long j = this.A01;
                int i3 = r14.A00;
                boolean z2 = false;
                if ((i3 & 8) == 8) {
                    z2 = true;
                }
                this.A01 = r4.Afs(j, r14.A01, z, z2);
                if (r4 == C463025i.A00) {
                    this.A00 = i2 | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r42 = (AnonymousClass253) obj;
                AnonymousClass254 r142 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r42.A03();
                            int i4 = 1;
                            if (A03 == 0) {
                                break;
                            } else {
                                if (A03 == 10) {
                                    if ((this.A00 & 1) == 1) {
                                        r1 = (AnonymousClass1G9) this.A04.A0T();
                                    } else {
                                        r1 = null;
                                    }
                                    AnonymousClass1G8 r0 = (AnonymousClass1G8) r42.A09(r142, AnonymousClass1G8.A05.A0U());
                                    this.A04 = r0;
                                    if (r1 != null) {
                                        r1.A04(r0);
                                        this.A04 = (AnonymousClass1G8) r1.A01();
                                    }
                                    i = this.A00;
                                } else if (A03 == 18) {
                                    i4 = 2;
                                    C82223vH r13 = (this.A00 & 2) == 2 ? (C82223vH) this.A02.A0T() : null;
                                    C57222mg r02 = (C57222mg) r42.A09(r142, C57222mg.A03.A0U());
                                    this.A02 = r02;
                                    if (r13 != null) {
                                        r13.A04(r02);
                                        this.A02 = (C57222mg) r13.A01();
                                    }
                                    i = this.A00;
                                } else if (A03 == 26) {
                                    i4 = 4;
                                    C82243vJ r15 = (this.A00 & 4) == 4 ? (C82243vJ) this.A03.A0T() : null;
                                    C56902m8 r03 = (C56902m8) r42.A09(r142, C56902m8.A00.A0U());
                                    this.A03 = r03;
                                    if (r15 != null) {
                                        r15.A04(r03);
                                        this.A03 = (C56902m8) r15.A01();
                                    }
                                    i = this.A00;
                                } else if (A03 == 32) {
                                    this.A00 |= 8;
                                    this.A01 = r42.A06();
                                } else if (!A0a(r42, A03)) {
                                    break;
                                }
                                this.A00 = i | i4;
                            }
                        } catch (IOException e) {
                            C28971Pt r16 = new C28971Pt(e.getMessage());
                            r16.unfinishedMessage = this;
                            throw new RuntimeException(r16);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2RK();
            case 5:
                return new C82233vI();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (AnonymousClass2RK.class) {
                        if (A06 == null) {
                            A06 = new AnonymousClass255(A05);
                        }
                    }
                }
                return A06;
            default:
                throw new UnsupportedOperationException();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A04;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            C57222mg r02 = this.A02;
            if (r02 == null) {
                r02 = C57222mg.A03;
            }
            i2 += CodedOutputStream.A0A(r02, 2);
        }
        if ((this.A00 & 4) == 4) {
            C56902m8 r03 = this.A03;
            if (r03 == null) {
                r03 = C56902m8.A00;
            }
            i2 += CodedOutputStream.A0A(r03, 3);
        }
        if ((this.A00 & 8) == 8) {
            i2 += CodedOutputStream.A05(4, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A04;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            C57222mg r02 = this.A02;
            if (r02 == null) {
                r02 = C57222mg.A03;
            }
            codedOutputStream.A0L(r02, 2);
        }
        if ((this.A00 & 4) == 4) {
            C56902m8 r03 = this.A03;
            if (r03 == null) {
                r03 = C56902m8.A00;
            }
            codedOutputStream.A0L(r03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(4, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
