package X;

import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;

/* renamed from: X.69f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1330369f implements AnonymousClass6MM {
    public final /* synthetic */ C129365xb A00;
    public final /* synthetic */ C128925wt A01;

    public C1330369f(C129365xb r1, C128925wt r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r2) {
        this.A01.A00(r2);
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        C129365xb r5 = this.A00;
        C18610sj r7 = r5.A04;
        AnonymousClass1W9[] r2 = new AnonymousClass1W9[4];
        C117305Zk.A1O("action", "pin-credential-check", r2);
        C117305Zk.A1P("token", str, r2);
        C117295Zj.A1P("credential-id", r5.A0B, r2);
        C117305Zk.A1Q("device-id", r5.A0A.A01(), r2);
        r7.A0F(new IDxRCallbackShape0S0200000_3_I1(r5.A00, r5.A01, r5.A03, this.A01, r5, 15), C117315Zl.A0G(r2), "get", C26061Bw.A0L);
    }
}
