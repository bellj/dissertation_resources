package X;

import java.math.BigInteger;

/* renamed from: X.0c7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08950c7 implements Comparable {
    public static final C08950c7 A05;
    public static final C08950c7 A06 = new C08950c7("", 0, 0, 0);
    public static final C08950c7 A07 = new C08950c7("", 0, 1, 0);
    public static final C08950c7 A08;
    public final int A00;
    public final int A01;
    public final int A02;
    public final String A03;
    public final AbstractC16710pd A04 = AnonymousClass4Yq.A00(new C11010fb(this));

    static {
        C08950c7 r0 = new C08950c7("", 1, 0, 0);
        A08 = r0;
        A05 = r0;
    }

    public C08950c7(String str, int i, int i2, int i3) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A03 = str;
    }

    /* renamed from: A00 */
    public int compareTo(C08950c7 r3) {
        C16700pc.A0E(r3, 0);
        return A01().compareTo(r3.A01());
    }

    public final BigInteger A01() {
        Object value = this.A04.getValue();
        C16700pc.A0B(value);
        return (BigInteger) value;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C08950c7)) {
            return false;
        }
        C08950c7 r4 = (C08950c7) obj;
        if (this.A00 == r4.A00 && this.A01 == r4.A01 && this.A02 == r4.A02) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((((527 + this.A00) * 31) + this.A01) * 31) + this.A02;
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        String str2 = this.A03;
        if (!AnonymousClass03C.A0J(str2)) {
            str = C16700pc.A08("-", str2);
        } else {
            str = "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00);
        sb.append('.');
        sb.append(this.A01);
        sb.append('.');
        sb.append(this.A02);
        sb.append(str);
        return sb.toString();
    }
}
