package X;

import java.util.Arrays;

/* renamed from: X.1nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38291nq {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final C32141bg A04;
    public final C32141bg A05;
    public final String A06;
    public final String A07;
    public final boolean A08;

    public /* synthetic */ C38291nq(C32141bg r1, C32141bg r2, String str, String str2, int i, int i2, int i3, int i4, boolean z) {
        this.A03 = i;
        this.A01 = i2;
        this.A07 = str;
        this.A06 = str2;
        this.A05 = r1;
        this.A04 = r2;
        this.A08 = z;
        this.A02 = i3;
        this.A00 = i4;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C38291nq r5 = (C38291nq) obj;
            if (!(this.A03 == r5.A03 && this.A01 == r5.A01 && this.A08 == r5.A08 && this.A02 == r5.A02 && this.A00 == r5.A00 && C29941Vi.A00(this.A07, r5.A07) && C29941Vi.A00(this.A06, r5.A06) && C29941Vi.A00(this.A05, r5.A05) && C29941Vi.A00(this.A04, r5.A04))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A03), Integer.valueOf(this.A01), this.A07, this.A06, this.A05, this.A04, Boolean.valueOf(this.A08), Integer.valueOf(this.A02), Integer.valueOf(this.A00)});
    }
}
