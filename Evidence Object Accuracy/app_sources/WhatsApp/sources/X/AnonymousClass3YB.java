package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.util.Arrays;

/* renamed from: X.3YB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YB implements AbstractC116715Wn {
    public final CallableC71513d3 A00;
    public final AnonymousClass3YD A01;

    public AnonymousClass3YB(C15450nH r10, C18790t3 r11, C14850m9 r12, C20110vE r13, C91624Sl r14, C43161wW r15, AbstractC38611oO r16, C22600zL r17) {
        AnonymousClass3YD r1 = new AnonymousClass3YD(r14.A00, r14.A01, r14.A03, r14.A04);
        this.A01 = r1;
        this.A00 = new CallableC71513d3(r10, r11, r12, r13, r15, new C90374Nq(r1, r14.A02), r16, r17);
    }

    @Override // X.AbstractC116715Wn
    public void A75() {
        this.A00.A75();
    }

    @Override // X.AbstractC116715Wn
    public AnonymousClass1t3 A9A() {
        String A0d;
        AnonymousClass1t3 A9A = this.A00.A9A();
        AnonymousClass1RN r4 = A9A.A00;
        if (r4.A00 == 0) {
            AnonymousClass3YD r0 = this.A01;
            String str = r0.A03;
            MessageDigest messageDigest = r0.A04;
            if (messageDigest == null) {
                A0d = C12960it.A0d(str, C12960it.A0k("MMS download failed in verifyFileSha256 with Exception; plainFileHash="));
            } else if (!Arrays.equals(messageDigest.digest(), Base64.decode(str, 0))) {
                StringBuilder A0k = C12960it.A0k("MMS download failed during media decryption due to plaintext hash mismatch; mediaHash=");
                A0k.append(str);
                A0k.append("; calculatedHash=");
                A0d = C12960it.A0d(Base64.encodeToString(messageDigest.digest(), 2), A0k);
            }
            Log.w(A0d);
            Log.e("encrypteddownloadtransfer/download/hash verification fail");
            return new AnonymousClass1t3(new AnonymousClass1RN(7, r4.A02, r4.A03));
        }
        return A9A;
    }

    @Override // X.AbstractC116715Wn
    public void cancel() {
        this.A00.cancel();
    }
}
