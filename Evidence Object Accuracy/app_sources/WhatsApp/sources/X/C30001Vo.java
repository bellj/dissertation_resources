package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.Jid;

/* renamed from: X.1Vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30001Vo extends Jid implements Parcelable {
    public static final C30001Vo A00 = new C30001Vo();
    public static final Parcelable.Creator CREATOR = new C100054lI();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "call";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 20;
    }

    public C30001Vo() {
        super("");
    }

    public C30001Vo(Parcel parcel) {
        super(parcel);
    }
}
