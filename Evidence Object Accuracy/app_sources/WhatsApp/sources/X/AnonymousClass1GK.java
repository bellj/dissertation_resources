package X;

import com.whatsapp.util.Log;

/* renamed from: X.1GK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1GK implements AnonymousClass1GL {
    public final /* synthetic */ C18930tI A00;

    @Override // X.AnonymousClass1GL
    public void AWu() {
    }

    public AnonymousClass1GK(C18930tI r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1GL
    public void AQ7(boolean z) {
        C27751Jb r4;
        C18930tI r2 = this.A00;
        synchronized (r2) {
            r4 = r2.A00;
        }
        if (r4 != null) {
            int i = r2.A06.A01().getInt("syncd_bootstrap_state", 0);
            StringBuilder sb = new StringBuilder("SyncdBootstrapManager/criticalBootstrapFailed currentState: ");
            sb.append(i);
            Log.i(sb.toString());
            int i2 = 1;
            if (i != 0) {
                if (i != 1) {
                    i2 = 2;
                    if (i != 2) {
                        if (i != 3) {
                            if (i == 4) {
                                Log.e("SyncdBootstrapManager/criticalBootstrapFailed should never reach here");
                            }
                            r2.A02(null);
                        }
                    }
                }
                r2.A05.A0B(r4.A02, i2, 0, 0, false);
                r2.A02(null);
            }
            r2.A05.A0D(r4.A02, i2, false);
            r2.A02(null);
        }
    }
}
