package X;

import android.widget.AbsListView;

/* renamed from: X.4om  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102214om implements AbsListView.OnScrollListener {
    public final /* synthetic */ AnonymousClass2Ew A00;

    public C102214om(AnonymousClass2Ew r1) {
        this.A00 = r1;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.A00.A08();
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.A00.A08();
    }
}
