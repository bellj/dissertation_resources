package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.1JP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JP extends AnonymousClass1JQ {
    public final String A00;

    public AnonymousClass1JP(AnonymousClass1JR r10, String str, String str2, long j) {
        super(C27791Jf.A03, r10, str, "critical_block", 3, j, false);
        this.A00 = str2;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        AnonymousClass1G4 A0T = C34701gZ.A02.A0T();
        String str = this.A00;
        A0T.A03();
        C34701gZ r1 = (C34701gZ) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = str;
        A01.A03();
        C27831Jk r12 = (C27831Jk) A01.A00;
        r12.A0C = (C34701gZ) A0T.A02();
        r12.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("LocaleSyncMutation{rowId=");
        sb.append(this.A07);
        sb.append(", locale=");
        sb.append(this.A00);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
