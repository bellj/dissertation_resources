package X;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.2bP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52622bP extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ ViewTreeObserver$OnPreDrawListenerC66463Nr A02;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return true;
    }

    public C52622bP(ViewTreeObserver$OnPreDrawListenerC66463Nr r1, int i, int i2) {
        this.A02 = r1;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        AnonymousClass238 r3;
        if (f == 1.0f) {
            r3 = this.A02.A01;
            r3.A02.getLayoutParams().height = this.A01;
        } else {
            ViewTreeObserver$OnPreDrawListenerC66463Nr r1 = this.A02;
            r3 = r1.A01;
            ViewGroup.LayoutParams layoutParams = r3.A02.getLayoutParams();
            int i = r1.A00;
            layoutParams.height = i + ((int) (((float) (this.A00 - i)) * f));
        }
        r3.A02.requestLayout();
    }
}
