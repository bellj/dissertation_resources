package X;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.util.Log;
import com.facebook.redex.IDxCListenerShape4S0000000_2_I1;
import com.facebook.redex.IDxCListenerShape4S0200000_2_I1;
import com.whatsapp.R;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.394  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass394 extends AbstractC16350or {
    public final C16210od A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final C15810nw A03;
    public final AnonymousClass01d A04;
    public final C14830m7 A05;
    public final C16590pI A06;
    public final C19350ty A07;
    public final AbstractC14440lR A08;
    public final WeakReference A09;
    public final AtomicLong A0A = new AtomicLong();

    public AnonymousClass394(Activity activity, C16210od r3, C14330lG r4, C14900mE r5, C15810nw r6, AnonymousClass01d r7, C14830m7 r8, C16590pI r9, C19350ty r10, AbstractC14440lR r11) {
        this.A09 = C12970iu.A10(activity);
        this.A06 = r9;
        this.A05 = r8;
        this.A02 = r5;
        this.A08 = r11;
        this.A01 = r4;
        this.A03 = r6;
        this.A04 = r7;
        this.A07 = r10;
        this.A00 = r3;
    }

    public static String A00(Context context, Uri uri, String str) {
        Cursor cursor = null;
        try {
            try {
                cursor = context.getContentResolver().query(uri, new String[]{str}, null, null, null);
                if (cursor.moveToFirst() && !cursor.isNull(0)) {
                    return cursor.getString(0);
                }
            } catch (Exception e) {
                Log.w("DocumentFile", C12960it.A0Z(e, "Failed query: ", C12960it.A0h()));
            }
            return null;
        } finally {
            A03(cursor);
        }
    }

    public static /* synthetic */ void A01(Activity activity, AnonymousClass394 r13) {
        AbstractC14440lR r11 = r13.A08;
        C16590pI r9 = r13.A06;
        C14830m7 r8 = r13.A05;
        C14900mE r5 = r13.A02;
        r11.Aaz(new AnonymousClass394(activity, r13.A00, r13.A01, r5, r13.A03, r13.A04, r8, r9, r13.A07, r11), new Uri[0]);
    }

    public static /* synthetic */ void A02(Activity activity, AnonymousClass394 r2) {
        activity.startActivity(C14960mK.A04(activity));
        r2.A07.A04("ManualExternalDirMigration");
    }

    public static void A03(AutoCloseable autoCloseable) {
        if (autoCloseable != null) {
            try {
                autoCloseable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
    }

    public static boolean A04(Context context, Uri uri) {
        if (DocumentsContract.isDocumentUri(context, uri)) {
            Cursor cursor = null;
            try {
                try {
                    cursor = context.getContentResolver().query(uri, new String[]{"flags"}, null, null, null);
                    if (cursor.moveToFirst() && !cursor.isNull(0)) {
                        long j = cursor.getLong(0);
                        A03(cursor);
                        if ((j & 512) != 0) {
                            return true;
                        }
                    }
                } catch (Exception e) {
                    Log.w("DocumentFile", C12960it.A0Z(e, "Failed query: ", C12960it.A0h()));
                }
                return false;
            } finally {
                A03(cursor);
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f4  */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r17) {
        /*
        // Method dump skipped, instructions count: 262
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass394.A05(java.lang.Object[]):java.lang.Object");
    }

    @Override // X.AbstractC16350or
    public void A06() {
        this.A0A.set(System.currentTimeMillis());
        this.A02.A06(0, R.string.manual_ext_dir_migration_progress);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C53312dp r3;
        int i;
        int i2;
        AnonymousClass4OG r9 = (AnonymousClass4OG) obj;
        this.A02.A03();
        StringBuilder A0k = C12960it.A0k("externaldirmigration/manual/migration results: moved ");
        A0k.append(r9.A01);
        A0k.append(" failed ");
        long j = r9.A00;
        A0k.append(j);
        C12960it.A1F(A0k);
        C15810nw r32 = this.A03;
        boolean z = !r32.A0A();
        if (!z) {
            r32.A09("scoped");
        }
        Activity activity = (Activity) this.A09.get();
        if (activity != null && !C36021jC.A03(activity) && this.A00.A00) {
            if (z) {
                r3 = new C53312dp(activity);
                r3.A07(R.string.manual_ext_dir_migration_completed_restart_needed_title);
                r3.A0A(C12960it.A0X(activity, activity.getString(R.string.localized_app_name), new Object[1], 0, R.string.manual_ext_dir_migration_restart_message));
                r3.A0B(false);
                i = R.string.ok;
                i2 = 13;
            } else if (j == 0) {
                C53312dp r33 = new C53312dp(activity);
                r33.A07(R.string.manual_ext_dir_migration_completed_title);
                r33.A06(R.string.manual_ext_dir_migration_success_message);
                r33.A0B(false);
                r33.setPositiveButton(R.string.ok, new IDxCListenerShape4S0000000_2_I1(18));
                C12970iu.A1J(r33);
                return;
            } else {
                r3 = new C53312dp(activity);
                r3.A07(R.string.manual_ext_dir_migration_incomplete_title);
                r3.A06(R.string.manual_ext_dir_migration_incomplete_message);
                r3.A0B(false);
                r3.setNegativeButton(R.string.cancel, new IDxCListenerShape4S0000000_2_I1(17));
                i = R.string.manual_ext_dir_migration_try_again;
                i2 = 12;
            }
            r3.setPositiveButton(i, new IDxCListenerShape4S0200000_2_I1(activity, i2, this));
            C12970iu.A1J(r3);
        } else if (z) {
            this.A07.A04("ManualExternalDirMigration");
        }
    }

    public final boolean A08(ContentResolver contentResolver, AnonymousClass0SD r15) {
        Uri uri = r15.A01;
        boolean z = true;
        Cursor query = contentResolver.query(uri, new String[]{"flags"}, null, null, null);
        if (query != null) {
            try {
                if (!query.moveToFirst() || query.isNull(0)) {
                    query.close();
                } else {
                    if ((query.getLong(0) & 4) == 0) {
                        z = false;
                    }
                    query.close();
                    if (z) {
                        try {
                            return DocumentsContract.deleteDocument(contentResolver, uri);
                        } catch (FileNotFoundException e) {
                            com.whatsapp.util.Log.e("externaldirmigration/manual/", e);
                            return false;
                        }
                    }
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        com.whatsapp.util.Log.w("externaldirmigration/manual/file deletion is not supported");
        return true;
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public final boolean A09(android.content.ContentResolver r16, X.AnonymousClass0SD r17, java.io.File r18, java.util.List r19, java.util.concurrent.atomic.AtomicLong r20) {
        /*
            r15 = this;
            r4 = 1
            r5 = r17
            if (r17 == 0) goto L_0x00bc
            android.content.Context r8 = r5.A00
            android.net.Uri r2 = r5.A01
            boolean r0 = A04(r8, r2)
            if (r0 != 0) goto L_0x00bc
            java.lang.String r0 = "_display_name"
            java.lang.String r1 = A00(r8, r2, r0)
            if (r1 != 0) goto L_0x001d
            java.lang.String r0 = "externaldirmigration/manual/file name is null"
        L_0x0019:
            com.whatsapp.util.Log.w(r0)
        L_0x001c:
            return r4
        L_0x001d:
            r0 = r18
            java.io.File r12 = new java.io.File
            r12.<init>(r0, r1)
            java.lang.String r7 = "mime_type"
            java.lang.String r3 = A00(r8, r2, r7)
            java.lang.String r0 = "vnd.android.document/directory"
            boolean r0 = r0.equals(r3)
            java.lang.String r6 = "externaldirmigration/manual/failed to delete source file for "
            r3 = 0
            r9 = r15
            r10 = r16
            r14 = r20
            r13 = r19
            if (r0 != 0) goto L_0x0080
            int r0 = r8.checkCallingOrSelfUriPermission(r2, r4)
            if (r0 != 0) goto L_0x00da
            java.lang.String r0 = A00(r8, r2, r7)
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00da
            boolean r0 = r12.exists()
            if (r0 == 0) goto L_0x005a
            java.lang.String r0 = "externaldirmigration/manual/target file already exists "
            java.lang.String r0 = X.C12960it.A0b(r0, r12)
            goto L_0x0019
        L_0x005a:
            java.io.InputStream r2 = r10.openInputStream(r2)     // Catch: IOException -> 0x00d0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch: all -> 0x00c9
            r1.<init>(r12)     // Catch: all -> 0x00c9
            X.C14350lI.A0G(r2, r1)     // Catch: all -> 0x00c4
            r13.add(r12)     // Catch: all -> 0x00c4
            boolean r0 = r15.A08(r10, r5)     // Catch: all -> 0x00c4
            if (r0 != 0) goto L_0x007a
            java.lang.StringBuilder r0 = X.C12960it.A0j(r6)     // Catch: all -> 0x00c4
            java.lang.String r0 = X.C12970iu.A0s(r12, r0)     // Catch: all -> 0x00c4
            com.whatsapp.util.Log.w(r0)     // Catch: all -> 0x00c4
        L_0x007a:
            r1.close()     // Catch: all -> 0x00c9
            if (r2 == 0) goto L_0x001c
            goto L_0x00c0
        L_0x0080:
            boolean r0 = r12.exists()
            if (r0 != 0) goto L_0x0096
            boolean r0 = r12.mkdirs()
            if (r0 != 0) goto L_0x0096
            java.lang.String r0 = "externaldirmigration/manual/failed to create target directory "
            java.lang.String r0 = X.C12960it.A0b(r0, r12)
            com.whatsapp.util.Log.e(r0)
            return r3
        L_0x0096:
            X.0SD[] r3 = r5.A01()
            int r2 = r3.length
            r1 = 0
        L_0x009c:
            if (r1 >= r2) goto L_0x00aa
            r11 = r3[r1]
            boolean r0 = r9.A09(r10, r11, r12, r13, r14)
            if (r0 != 0) goto L_0x00a7
            r4 = 0
        L_0x00a7:
            int r1 = r1 + 1
            goto L_0x009c
        L_0x00aa:
            if (r4 == 0) goto L_0x001c
            boolean r0 = r15.A08(r10, r5)
            if (r0 != 0) goto L_0x001c
            java.lang.StringBuilder r0 = X.C12960it.A0j(r6)
            java.lang.String r0 = X.C12970iu.A0s(r12, r0)
            goto L_0x0019
        L_0x00bc:
            java.lang.String r0 = "externaldirmigration/manual/doc file either null or virtual"
            goto L_0x0019
        L_0x00c0:
            r2.close()     // Catch: IOException -> 0x00d0
            return r4
        L_0x00c4:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x00c8
        L_0x00c8:
            throw r0     // Catch: all -> 0x00c9
        L_0x00c9:
            r0 = move-exception
            if (r2 == 0) goto L_0x00cf
            r2.close()     // Catch: all -> 0x00cf
        L_0x00cf:
            throw r0     // Catch: IOException -> 0x00d0
        L_0x00d0:
            r1 = move-exception
            java.lang.String r0 = "externaldirmigration/manual//failed to copy file"
            com.whatsapp.util.Log.e(r0, r1)
            r14.incrementAndGet()
            return r3
        L_0x00da:
            java.lang.String r0 = "externaldirmigration/manual/cannot read file "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12960it.A0d(r1, r0)
            com.whatsapp.util.Log.w(r0)
            r14.incrementAndGet()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass394.A09(android.content.ContentResolver, X.0SD, java.io.File, java.util.List, java.util.concurrent.atomic.AtomicLong):boolean");
    }
}
