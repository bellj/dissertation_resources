package X;

import android.graphics.Rect;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import com.whatsapp.R;

/* renamed from: X.18t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252718t {
    public final AnonymousClass01d A00;

    public C252718t(AnonymousClass01d r1) {
        this.A00 = r1;
    }

    public static boolean A00(View view) {
        Rect rect = new Rect();
        view.getRootView().getWindowVisibleDisplayFrame(rect);
        WindowManager A02 = AnonymousClass01d.A02(view.getContext());
        AnonymousClass009.A05(A02);
        return A02.getDefaultDisplay().getHeight() - rect.bottom >= view.getRootView().getResources().getDimensionPixelSize(R.dimen.ime_utils_window_density);
    }

    public void A01(View view) {
        InputMethodManager A0Q = this.A00.A0Q();
        AnonymousClass009.A05(A0Q);
        A0Q.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void A02(View view) {
        InputMethodManager A0Q = this.A00.A0Q();
        AnonymousClass009.A05(A0Q);
        A0Q.showSoftInput(view, 0);
    }
}
