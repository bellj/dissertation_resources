package X;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59932vc extends AbstractC37191le {
    public final AnonymousClass02H A00;
    public final RecyclerView A01;
    public final C251118d A02;
    public final C74813ip A03;

    public C59932vc(View view, C251118d r5) {
        super(view);
        AnonymousClass02H gridLayoutManager;
        this.A02 = r5;
        this.A01 = C12990iw.A0R(view, R.id.popular_categories_recycler_view);
        if (r5.A01()) {
            gridLayoutManager = C12990iw.A0Q(view);
        } else {
            view.getContext();
            gridLayoutManager = new GridLayoutManager(AbstractC37191le.A00(this));
        }
        this.A00 = gridLayoutManager;
        Resources resources = view.getResources();
        RecyclerView recyclerView = this.A01;
        recyclerView.A0l(new C74883iw(resources, this));
        recyclerView.setLayoutManager(this.A00);
        if (!r5.A01()) {
            recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: X.4nY
                @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
                public final void onGlobalLayout() {
                    C59932vc r0 = C59932vc.this;
                    GridLayoutManager gridLayoutManager2 = (GridLayoutManager) r0.A00;
                    int A0A = r0.A0A();
                    if (A0A != gridLayoutManager2.A00) {
                        gridLayoutManager2.A1h(A0A);
                    }
                }
            });
        }
        this.A03 = new C74813ip();
    }

    public final int A0A() {
        return AbstractC37191le.A00(this);
    }
}
