package X;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.SynchronousQueue;
import org.whispersystems.jobqueue.Job;

/* renamed from: X.1Lv  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Lv {
    public final AnonymousClass2C5 A00;
    public final LinkedList A01 = new LinkedList();
    public final Map A02;
    public final Set A03 = new HashSet();
    public final SynchronousQueue A04 = new SynchronousQueue();
    public final AnonymousClass1Lw A05;
    public final boolean A06;

    public AnonymousClass1Lv(AnonymousClass2C5 r3, boolean z) {
        AnonymousClass1Lw r1 = new AnonymousClass1Lw(this);
        this.A05 = r1;
        this.A02 = new HashMap();
        this.A06 = z;
        this.A00 = r3;
        r1.start();
    }

    public synchronized int A00(String str) {
        int intValue;
        if (str != null) {
            Integer num = (Integer) this.A02.get(str);
            if (num != null) {
                intValue = num.intValue();
            }
        }
        intValue = 0;
        return intValue;
    }

    public final void A01(Job job) {
        String str = job.parameters.groupId;
        if (str != null) {
            this.A02.put(str, Integer.valueOf(A00(str) + 1));
        }
    }

    public synchronized void A02(Job job) {
        this.A01.addFirst(job);
        A01(job);
        this.A05.A00.open();
    }
}
