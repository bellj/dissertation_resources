package X;

/* renamed from: X.0ws  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21110ws {
    public final C21100wr A00;
    public final C15990oG A01;

    public C21110ws(C21100wr r1, C15990oG r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public final void A00(C15950oC r3) {
        C21100wr r1 = this.A00;
        synchronized (r1) {
            this.A01.A0B.A01(r3);
            r1.A03(new AnonymousClass1RV(), r3);
        }
    }
}
