package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4wR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106884wR implements AnonymousClass5WY {
    public final /* synthetic */ C107094wm A00;

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public /* synthetic */ C106884wR(C107094wm r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        C107094wm r0 = this.A00;
        return (r0.A07 * SearchActionVerificationClientService.MS_TO_NS) / ((long) r0.A0B.A00);
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        C107094wm r8 = this.A00;
        long A0W = C72453ed.A0W((long) r8.A0B.A00, j);
        long j2 = r8.A09;
        long j3 = r8.A08;
        C94324bc r1 = new C94324bc(j, Math.max(j2, Math.min((j2 + ((A0W * (j3 - j2)) / r8.A07)) - C26061Bw.A0L, j3 - 1)));
        return new C92684Xa(r1, r1);
    }
}
