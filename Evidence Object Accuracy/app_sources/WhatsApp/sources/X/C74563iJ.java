package X;

import java.util.List;

/* renamed from: X.3iJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74563iJ extends AnonymousClass0Q0 {
    public final List A00;
    public final List A01;

    public C74563iJ(List list, List list2) {
        this.A01 = list;
        this.A00 = list2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        return ((AnonymousClass4OJ) this.A01.get(i)).A01.equals(((AnonymousClass4OJ) this.A00.get(i2)).A01);
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        return C12960it.A1V(((AnonymousClass4OJ) this.A01.get(i)).A00, ((AnonymousClass4OJ) this.A00.get(i2)).A00);
    }
}
