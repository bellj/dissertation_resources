package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0100100_I1;

/* renamed from: X.4Wq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92614Wq {
    public Runnable A00;
    public final AbstractC14440lR A01;

    public /* synthetic */ C92614Wq(AbstractC14440lR r1) {
        this.A01 = r1;
    }

    public synchronized void A00() {
        Runnable runnable = this.A00;
        if (runnable != null) {
            this.A01.AaP(runnable);
            this.A00 = null;
        }
    }

    public synchronized void A01(long j) {
        A00();
        this.A00 = this.A01.AbK(new RunnableBRunnable0Shape1S0100100_I1(this, j, 1), "LiteCameraView/GarbageCollector", j);
    }
}
