package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.0ta  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19110ta extends AbstractC18500sY implements AbstractC19010tQ {
    public final C15570nT A00;
    public final C16370ot A01;
    public final C20600w1 A02;
    public final C20590w0 A03;
    public final C20580vz A04;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19110ta(C15570nT r3, C16370ot r4, C20600w1 r5, C20590w0 r6, C20580vz r7, C18480sW r8) {
        super(r8, "receipt_device", Integer.MIN_VALUE);
        this.A00 = r3;
        this.A01 = r4;
        this.A04 = r7;
        this.A03 = r6;
        this.A02 = r5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0193, code lost:
        if (X.C15380n4.A0N(r0) != false) goto L_0x0195;
     */
    @Override // X.AbstractC18500sY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass2Ez A09(android.database.Cursor r31) {
        /*
        // Method dump skipped, instructions count: 651
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19110ta.A09(android.database.Cursor):X.2Ez");
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("receipt_device_migration_complete", 1);
    }

    @Override // X.AbstractC18500sY
    public boolean A0V(C29001Pw r5) {
        C16310on A01 = this.A05.get();
        try {
            if (TextUtils.isEmpty(AnonymousClass1Uj.A00(A01.A03, "table", "messages"))) {
                A0H();
                A01.close();
                return true;
            }
            A01.close();
            return super.A0V(r5);
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("receipt_device", null, null);
            C21390xL r1 = this.A06;
            r1.A03("receipt_device_migration_complete");
            r1.A03("migration_receipt_device_index");
            r1.A03("migration_receipt_device_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("ReceiptDeviceStore/ReceiptDeviceDatabaseMigration/resetMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
