package X;

import android.widget.Button;
import com.whatsapp.support.DescribeProblemActivity;

/* renamed from: X.47O  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47O extends C469928m {
    public final /* synthetic */ Button A00;
    public final /* synthetic */ DescribeProblemActivity A01;

    public AnonymousClass47O(Button button, DescribeProblemActivity describeProblemActivity) {
        this.A01 = describeProblemActivity;
        this.A00 = button;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.A00.setEnabled(C12960it.A1U(charSequence.length()));
    }
}
