package X;

import java.lang.reflect.Method;

/* renamed from: X.4HF  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4HF {
    public static final Method A00;
    public static final Method A01;

    static {
        Method method;
        Method method2;
        Class<?> cls;
        Method[] methods = Throwable.class.getMethods();
        C16700pc.A0B(methods);
        int length = methods.length;
        int i = 0;
        int i2 = 0;
        while (true) {
            method = null;
            if (i2 >= length) {
                method2 = null;
                break;
            }
            method2 = methods[i2];
            i2++;
            if (C16700pc.A0O(method2.getName(), "addSuppressed")) {
                Class<?>[] parameterTypes = method2.getParameterTypes();
                C16700pc.A0B(parameterTypes);
                if (parameterTypes.length == 1) {
                    cls = parameterTypes[0];
                } else {
                    cls = null;
                }
                if (C16700pc.A0O(cls, Throwable.class)) {
                    break;
                }
            }
        }
        A00 = method2;
        while (true) {
            if (i >= length) {
                break;
            }
            Method method3 = methods[i];
            i++;
            if (C16700pc.A0O(method3.getName(), "getSuppressed")) {
                method = method3;
                break;
            }
        }
        A01 = method;
    }
}
