package X;

import android.os.HandlerThread;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: X.1HH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HH extends AnonymousClass1HG {
    public final AnonymousClass1II A00;
    public final AnonymousClass1IJ A01 = new AnonymousClass1IJ(this);
    public final Set A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1HH(HandlerThread handlerThread, AnonymousClass1HE r4, Executor executor) {
        super(r4, executor, 2);
        AnonymousClass1II r1 = new AnonymousClass1II(handlerThread);
        this.A00 = r1;
        this.A02 = new HashSet();
    }

    public final boolean A01() {
        int i;
        AnonymousClass1II r2 = this.A00;
        synchronized (r2) {
            i = r2.A00;
        }
        return i == 1;
    }
}
