package X;

/* renamed from: X.0ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08140ae implements AbstractC12040hH {
    public final AnonymousClass0H6 A00;
    public final AbstractC12590iA A01;
    public final String A02;
    public final boolean A03;
    public final boolean A04;

    public C08140ae(AnonymousClass0H6 r1, AbstractC12590iA r2, String str, boolean z, boolean z2) {
        this.A02 = str;
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = z;
        this.A03 = z2;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C07990aP(r2, this, r3);
    }
}
