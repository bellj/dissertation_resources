package X;

import com.whatsapp.util.Log;

/* renamed from: X.1Y0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Y0 extends AnonymousClass1XB {
    public int A00;

    public AnonymousClass1Y0(AnonymousClass1IS r2, long j) {
        super(r2, 60, j);
    }

    @Override // X.AbstractC15340mz
    public synchronized void A0l(String str) {
        try {
            this.A00 = Integer.valueOf(str).intValue();
        } catch (NumberFormatException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("FMessageSystemEphemeralSettingNotApplied/setData cannot convert data to int, data=");
            sb.append(str);
            Log.w(sb.toString(), e);
            this.A00 = 0;
        }
    }
}
