package X;

import java.io.File;

/* renamed from: X.0lL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14380lL {
    public final int A00;
    public final int A01;
    public final long A02;
    public final C14390lM A03;
    public final C14400lN A04;
    public final C14370lK A05;
    public final File A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final int[] A0E;

    public C14380lL(C14390lM r2, C14400lN r3, C14370lK r4, File file, String str, String str2, String str3, int[] iArr, int i, int i2, long j, boolean z, boolean z2, boolean z3, boolean z4) {
        this.A05 = r4;
        this.A06 = file;
        this.A02 = j;
        this.A03 = r2;
        this.A08 = str;
        this.A07 = str2;
        this.A01 = i;
        this.A0C = z;
        this.A09 = str3;
        this.A0A = z2;
        this.A0D = z3;
        this.A0E = iArr;
        this.A00 = i2;
        this.A0B = z4;
        this.A04 = r3;
    }
}
