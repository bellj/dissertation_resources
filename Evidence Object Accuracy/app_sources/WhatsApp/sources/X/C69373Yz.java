package X;

import android.util.Pair;
import com.whatsapp.util.Log;

/* renamed from: X.3Yz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69373Yz implements AbstractC21730xt {
    public final /* synthetic */ C64603Ge A00;

    public C69373Yz(C64603Ge r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.w(C12960it.A0d(str, C12960it.A0k("JoinSubgroupProtocolHelper/onDeliveryFailure iqid=")));
        C468027s r1 = this.A00.A00.A01.A00;
        C12970iu.A1Q(r1.A0W, 0);
        C12970iu.A1Q(r1.A0U, 6);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        int i;
        C63753Cu r0;
        Object obj;
        Log.e("JoinSubgroupProtocolHelper/onError");
        Pair A01 = C41151sz.A01(r4);
        if (!(A01 == null || (obj = A01.first) == null)) {
            Log.e(C12960it.A0b("JoinSubgroupProtocolHelper/onError: ", obj));
            i = C12960it.A05(A01.first);
            if (!(i == 400 || i == 409 || i == 419 || i == 500)) {
                switch (i) {
                }
                C468027s r1 = r0.A01.A00;
                C12970iu.A1Q(r1.A0W, i);
                C12970iu.A1Q(r1.A0U, 6);
            }
            r0 = this.A00.A00;
            C468027s r1 = r0.A01.A00;
            C12970iu.A1Q(r1.A0W, i);
            C12970iu.A1Q(r1.A0U, 6);
        }
        r0 = this.A00.A00;
        i = -1;
        C468027s r1 = r0.A01.A00;
        C12970iu.A1Q(r1.A0W, i);
        C12970iu.A1Q(r1.A0U, 6);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r3, String str) {
        C63753Cu r0 = this.A00.A00;
        r0.A00.accept(r0.A03);
    }
}
