package X;

/* renamed from: X.2BJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2BJ {
    public final Runnable A00;
    public final String A01;
    public final byte[] A02;

    public AnonymousClass2BJ(Runnable runnable, String str, byte[] bArr) {
        this.A01 = str;
        this.A02 = bArr;
        this.A00 = runnable;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SendFieldStats{fieldStatsBlob=");
        byte[] bArr = this.A02;
        sb.append(bArr != null ? bArr.length : 0);
        sb.append(" bytes, successCallback=");
        sb.append(this.A00);
        sb.append(", errorCallback=");
        sb.append((Object) null);
        sb.append(", readErrorCallback=");
        sb.append((Object) null);
        sb.append('}');
        return sb.toString();
    }
}
