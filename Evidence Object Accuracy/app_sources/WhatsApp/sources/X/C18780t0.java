package X;

import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0t0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18780t0 {
    public static final long A08 = TimeUnit.DAYS.toSeconds(182);
    public Map A00;
    public final C15550nR A01;
    public final AnonymousClass10S A02;
    public final C33891fF A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final List A06 = new ArrayList();
    public final AtomicBoolean A07 = new AtomicBoolean(false);

    public C18780t0(C15550nR r3, AnonymousClass10S r4, C14830m7 r5, C14850m9 r6, C232010t r7) {
        this.A04 = r5;
        this.A05 = r6;
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = new C33891fF(r7);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x005a, code lost:
        if (r10 == 1) goto L_0x005c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(com.whatsapp.jid.UserJid r15, byte[] r16, long r17) {
        /*
            r14 = this;
            X.0m9 r1 = r14.A05
            r0 = 794(0x31a, float:1.113E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0087
            X.1fF r2 = r14.A03
            r0 = 3
            android.content.ContentValues r9 = new android.content.ContentValues
            r9.<init>(r0)
            java.lang.String r1 = r15.getRawString()
            java.lang.String r0 = "jid"
            r9.put(r0, r1)
            java.lang.String r0 = "incoming_tc_token"
            r1 = r16
            r9.put(r0, r1)
            java.lang.Long r1 = java.lang.Long.valueOf(r17)
            java.lang.String r0 = "incoming_tc_token_timestamp"
            r9.put(r0, r1)
            X.10t r0 = r2.A00
            X.0on r5 = r0.A02()
            X.1Lx r13 = r5.A00()     // Catch: all -> 0x0082
            java.lang.String r4 = "wa_trusted_contacts"
            java.lang.String r3 = "jid = ? AND incoming_tc_token_timestamp < ? "
            r6 = 2
            java.lang.String[] r2 = new java.lang.String[r6]     // Catch: all -> 0x007d
            java.lang.String r1 = r15.getRawString()     // Catch: all -> 0x007d
            r0 = 0
            r2[r0] = r1     // Catch: all -> 0x007d
            java.lang.String r0 = java.lang.String.valueOf(r17)     // Catch: all -> 0x007d
            r12 = 1
            r2[r12] = r0     // Catch: all -> 0x007d
            long r10 = X.AbstractC21570xd.A01(r9, r5, r4, r3, r2)     // Catch: all -> 0x007d
            r7 = 0
            int r0 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x005c
            r2 = 1
            int r1 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x005d
        L_0x005c:
            r0 = 1
        L_0x005d:
            X.AnonymousClass009.A0F(r0)     // Catch: all -> 0x007d
            int r0 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x0073
            X.0op r1 = r5.A03     // Catch: all -> 0x007d
            r0 = 4
            long r3 = r1.A06(r9, r4, r0)     // Catch: all -> 0x007d
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0072
            r6 = 0
        L_0x0072:
            r12 = r6
        L_0x0073:
            r13.A00()     // Catch: all -> 0x007d
            r13.close()     // Catch: all -> 0x0082
            r5.close()
            return r12
        L_0x007d:
            r0 = move-exception
            r13.close()     // Catch: all -> 0x0081
        L_0x0081:
            throw r0     // Catch: all -> 0x0082
        L_0x0082:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x0086
        L_0x0086:
            throw r0
        L_0x0087:
            r12 = 2
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18780t0.A00(com.whatsapp.jid.UserJid, byte[], long):int");
    }

    public long A01() {
        C14850m9 r1 = this.A05;
        long A02 = (long) r1.A02(865);
        long A022 = (long) r1.A02(909);
        long A01 = this.A04.A01() / 1000;
        long j = A01 / A02;
        long j2 = A022 - 1;
        boolean z = false;
        if (j >= j2) {
            z = true;
        }
        AnonymousClass009.A0C("Passed invalid parameters to tokenExpirationWithParameters (possible bad ABProps)", z);
        return Math.max((j - j2) * A02, A01 - A08);
    }

    public long A02() {
        C14850m9 r1 = this.A05;
        long A02 = (long) r1.A02(996);
        long A022 = (long) r1.A02(997);
        long A01 = this.A04.A01() / 1000;
        long j = A01 / A02;
        long j2 = A022 - 1;
        boolean z = false;
        if (j >= j2) {
            z = true;
        }
        AnonymousClass009.A0C("Passed invalid parameters to tokenExpirationWithParameters (possible bad ABProps)", z);
        return Math.max((j - j2) * A02, A01 - A08);
    }

    public Long A03(UserJid userJid) {
        Map A06 = A06();
        if (!this.A05.A07(995)) {
            return null;
        }
        if (!A06.containsKey(userJid)) {
            long A00 = this.A03.A00(userJid);
            if (A00 < A02()) {
                A06.put(userJid, null);
            } else {
                A06.put(userJid, Long.valueOf(A00));
            }
        }
        return (Long) A06.get(userJid);
    }

    public Map A04() {
        if (!this.A05.A07(995)) {
            return new HashMap();
        }
        C33891fF r0 = this.A03;
        HashMap hashMap = new HashMap();
        C16310on A01 = r0.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT jid, sent_tc_token_timestamp FROM wa_trusted_contacts_send", new String[0]);
            if (A09 != null) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("jid");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("sent_tc_token_timestamp");
                while (A09.moveToNext()) {
                    AbstractC14640lm A012 = AbstractC14640lm.A01(A09.getString(columnIndexOrThrow));
                    if (A012 != null) {
                        hashMap.put(A012, Long.valueOf(A09.getLong(columnIndexOrThrow2)));
                    }
                }
            }
            if (A09 != null) {
                A09.close();
            }
            A01.close();
            return hashMap;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Map A05() {
        if (!this.A05.A07(794)) {
            return new HashMap();
        }
        C33891fF r0 = this.A03;
        HashMap hashMap = new HashMap();
        C16310on A01 = r0.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT jid, incoming_tc_token, incoming_tc_token_timestamp FROM wa_trusted_contacts", new String[0]);
            if (A09 != null) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("jid");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("incoming_tc_token");
                int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("incoming_tc_token_timestamp");
                while (A09.moveToNext()) {
                    UserJid nullable = UserJid.getNullable(A09.getString(columnIndexOrThrow));
                    byte[] blob = A09.getBlob(columnIndexOrThrow2);
                    long j = A09.getLong(columnIndexOrThrow3);
                    AnonymousClass009.A05(blob);
                    hashMap.put(nullable, new AnonymousClass1PF(Long.valueOf(j), blob));
                }
            }
            if (A09 != null) {
                A09.close();
            }
            A01.close();
            return hashMap;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final synchronized Map A06() {
        Map map;
        map = this.A00;
        if (map == null) {
            map = Collections.synchronizedMap(new HashMap());
            this.A00 = map;
        }
        return map;
    }

    public final void A07(UserJid userJid) {
        for (C33901fG r0 : this.A06) {
            C22280yp r2 = r0.A00;
            C33911fH r02 = (C33911fH) r2.A06.get(userJid);
            if (r02 != null && r02.A02 == -1) {
                r2.A04(userJid);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x005d, code lost:
        if (r14 == 1) goto L_0x005f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(com.whatsapp.jid.UserJid r18, java.lang.Long r19) {
        /*
            r17 = this;
            r6 = r17
            X.0m9 r1 = r6.A05
            r0 = 995(0x3e3, float:1.394E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x0097
            X.1fF r2 = r6.A03
            r7 = r19
            long r12 = r7.longValue()
            r9 = 2
            android.content.ContentValues r11 = new android.content.ContentValues
            r11.<init>(r9)
            r8 = r18
            java.lang.String r1 = r8.getRawString()
            java.lang.String r0 = "jid"
            r11.put(r0, r1)
            java.lang.Long r1 = java.lang.Long.valueOf(r12)
            java.lang.String r0 = "sent_tc_token_timestamp"
            r11.put(r0, r1)
            X.10t r0 = r2.A00
            X.0on r5 = r0.A02()
            X.1Lx r16 = r5.A00()     // Catch: all -> 0x0082
            java.lang.String r10 = "wa_trusted_contacts_send"
            java.lang.String r3 = "jid = ? AND sent_tc_token_timestamp < ? "
            java.lang.String[] r2 = new java.lang.String[r9]     // Catch: all -> 0x007d
            java.lang.String r1 = r8.getRawString()     // Catch: all -> 0x007d
            r0 = 0
            r2[r0] = r1     // Catch: all -> 0x007d
            java.lang.String r0 = java.lang.String.valueOf(r12)     // Catch: all -> 0x007d
            r4 = 1
            r2[r4] = r0     // Catch: all -> 0x007d
            long r14 = X.AbstractC21570xd.A01(r11, r5, r10, r3, r2)     // Catch: all -> 0x007d
            r12 = 0
            int r0 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r0 == 0) goto L_0x005f
            r2 = 1
            int r1 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            r0 = 0
            if (r1 != 0) goto L_0x0060
        L_0x005f:
            r0 = 1
        L_0x0060:
            X.AnonymousClass009.A0F(r0)     // Catch: all -> 0x007d
            int r0 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x0076
            X.0op r1 = r5.A03     // Catch: all -> 0x007d
            r0 = 4
            long r3 = r1.A06(r11, r10, r0)     // Catch: all -> 0x007d
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0075
            r9 = 0
        L_0x0075:
            r4 = r9
        L_0x0076:
            r16.A00()     // Catch: all -> 0x007d
            r16.close()     // Catch: all -> 0x0082
            goto L_0x0087
        L_0x007d:
            r0 = move-exception
            r16.close()     // Catch: all -> 0x0081
        L_0x0081:
            throw r0     // Catch: all -> 0x0082
        L_0x0082:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x0086
        L_0x0086:
            throw r0
        L_0x0087:
            r5.close()
            r0 = 2
            if (r4 == r0) goto L_0x0097
            java.util.Map r0 = r6.A06()
            r0.put(r8, r7)
            r6.A07(r8)
        L_0x0097:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18780t0.A08(com.whatsapp.jid.UserJid, java.lang.Long):void");
    }

    public byte[] A09(UserJid userJid) {
        byte[] bArr;
        Long l;
        AnonymousClass1PF r0;
        if (this.A05.A07(794)) {
            C16310on A01 = this.A03.A00.get();
            try {
                Cursor A09 = A01.A03.A09("SELECT incoming_tc_token, incoming_tc_token_timestamp FROM wa_trusted_contacts WHERE jid= ?", new String[]{userJid.getRawString()});
                if (A09.moveToNext()) {
                    bArr = A09.getBlob(A09.getColumnIndexOrThrow("incoming_tc_token"));
                    l = Long.valueOf(A09.getLong(A09.getColumnIndexOrThrow("incoming_tc_token_timestamp")));
                } else {
                    bArr = null;
                    l = null;
                }
                A09.close();
                A01.close();
                if (bArr == null) {
                    r0 = null;
                } else {
                    r0 = new AnonymousClass1PF(l, bArr);
                }
                if (r0 != null) {
                    byte[] bArr2 = r0.A01;
                    if (r0.A00.longValue() >= A01()) {
                        return bArr2;
                    }
                }
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return null;
    }
}
