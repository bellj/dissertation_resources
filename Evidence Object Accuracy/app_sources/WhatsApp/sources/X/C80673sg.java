package X;

import android.view.View;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.ctwa.bizpreview.BusinessPreviewFragment;

/* renamed from: X.3sg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80673sg extends AnonymousClass2UH {
    public final /* synthetic */ BottomSheetBehavior A00;
    public final /* synthetic */ BusinessPreviewFragment A01;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C80673sg(BottomSheetBehavior bottomSheetBehavior, BusinessPreviewFragment businessPreviewFragment) {
        this.A01 = businessPreviewFragment;
        this.A00 = bottomSheetBehavior;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 5) {
            this.A01.A1B();
        } else if (i == 1 && this.A01.A02.getScrollY() != 0) {
            this.A00.A0M(3);
        }
    }
}
