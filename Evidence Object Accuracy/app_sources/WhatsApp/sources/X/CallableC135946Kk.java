package X;

import android.hardware.camera2.CaptureRequest;
import java.util.concurrent.Callable;

/* renamed from: X.6Kk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135946Kk implements Callable {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ C130185yw A01;
    public final /* synthetic */ AnonymousClass66I A02;
    public final /* synthetic */ C1310360y A03;

    public CallableC135946Kk(CaptureRequest.Builder builder, C130185yw r2, AnonymousClass66I r3, C1310360y r4) {
        this.A01 = r2;
        this.A03 = r4;
        this.A00 = builder;
        this.A02 = r3;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        CaptureRequest.Builder builder;
        C1310360y r3 = this.A03;
        if (r3 == null || (builder = this.A00) == null) {
            return this.A02;
        }
        builder.set(CaptureRequest.CONTROL_AF_TRIGGER, C12960it.A0V());
        CaptureRequest build = builder.build();
        AnonymousClass66I r1 = this.A02;
        r3.A04(build, r1);
        return r1;
    }
}
