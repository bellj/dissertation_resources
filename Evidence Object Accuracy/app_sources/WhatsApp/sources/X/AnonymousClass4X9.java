package X;

/* renamed from: X.4X9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4X9 {
    public final String A00;
    public final String A01;
    public final boolean A02;

    public AnonymousClass4X9(String str, String str2, boolean z) {
        this.A02 = z;
        this.A01 = str == null ? "" : str;
        this.A00 = str2 == null ? "" : str2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AnonymousClass4X9)) {
            return false;
        }
        AnonymousClass4X9 r4 = (AnonymousClass4X9) obj;
        if (this.A02 != r4.A02 || !this.A01.equals(r4.A01) || !this.A00.equals(r4.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = Boolean.valueOf(this.A02);
        objArr[1] = this.A01;
        return C12980iv.A0B(this.A00, objArr, 2);
    }
}
