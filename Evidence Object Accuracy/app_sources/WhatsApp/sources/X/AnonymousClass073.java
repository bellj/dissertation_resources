package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.073  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass073 {
    public static AnonymousClass072 A00 = new AnonymousClass0G7();
    public static ThreadLocal A01 = new ThreadLocal();
    public static ArrayList A02 = new ArrayList();

    public static AnonymousClass00N A00() {
        AnonymousClass00N r1;
        ThreadLocal threadLocal = A01;
        Reference reference = (Reference) threadLocal.get();
        if (reference != null && (r1 = (AnonymousClass00N) reference.get()) != null) {
            return r1;
        }
        AnonymousClass00N r12 = new AnonymousClass00N();
        threadLocal.set(new WeakReference(r12));
        return r12;
    }

    public static void A01(ViewGroup viewGroup, AnonymousClass072 r4) {
        ArrayList arrayList = A02;
        if (!arrayList.contains(viewGroup) && AnonymousClass028.A0r(viewGroup)) {
            arrayList.add(viewGroup);
            if (r4 == null) {
                r4 = A00;
            }
            AnonymousClass072 A03 = r4.clone();
            AbstractCollection abstractCollection = (AbstractCollection) A00().get(viewGroup);
            if (abstractCollection != null && abstractCollection.size() > 0) {
                Iterator it = abstractCollection.iterator();
                while (it.hasNext()) {
                    ((AnonymousClass072) it.next()).A0G(viewGroup);
                }
            }
            if (A03 != null) {
                A03.A0K(viewGroup, true);
            }
            viewGroup.getTag(R.id.transition_current_scene);
            viewGroup.setTag(R.id.transition_current_scene, null);
            if (A03 != null) {
                AnonymousClass0WB r1 = new AnonymousClass0WB(viewGroup, A03);
                viewGroup.addOnAttachStateChangeListener(r1);
                viewGroup.getViewTreeObserver().addOnPreDrawListener(r1);
            }
        }
    }
}
