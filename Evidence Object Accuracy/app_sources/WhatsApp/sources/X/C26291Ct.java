package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Ct  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26291Ct {
    public SharedPreferences A00;
    public final C14830m7 A01;
    public final C16630pM A02;

    public C26291Ct(C14830m7 r1, C16630pM r2) {
        this.A01 = r1;
        this.A02 = r2;
    }

    public C456522m A00() {
        C456522m A00;
        C14830m7 r5 = this.A01;
        long A002 = r5.A00();
        long j = A002 - (A002 % 86400000);
        SharedPreferences sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A02.A01("payment_daily_usage_preferences");
            this.A00 = sharedPreferences;
        }
        String string = sharedPreferences.getString(Long.toString(j), null);
        return (TextUtils.isEmpty(string) || (A00 = C456522m.A00(string)) == null) ? new C456522m(j, r5.A00()) : A00;
    }

    public void A01(C456522m r6) {
        long A00 = this.A01.A00();
        String l = Long.toString(A00 - (A00 % 86400000));
        try {
            JSONObject put = new JSONObject().put("start_ts", r6.A0G).put("log_start_date", r6.A0F).put("total_one_time_mandate_cnt", r6.A02).put("total_transaction_sent_cnt", r6.A05).put("total_recurring_mandate_cnt", r6.A03).put("total_transaction_received_cnt", r6.A04).put("transaction_sent_with_sticker_cnt", r6.A0B).put("transaction_sent_with_background_cnt", r6.A0A).put("transaction_received_with_sticker_cnt", r6.A08).put("transaction_received_with_background_cnt", r6.A07).put("transaction_sent_with_background_and_sticker_cnt", r6.A09).put("transaction_received_with_background_and_sticker_cnt", r6.A06).put("invites_sent_to_user_cnt", r6.A01);
            Set<Object> set = r6.A0C;
            JSONArray jSONArray = new JSONArray();
            for (Object obj : set) {
                jSONArray.put(obj);
            }
            JSONObject put2 = put.put("invited_user_cnt", jSONArray);
            Set<Object> set2 = r6.A0E;
            JSONArray jSONArray2 = new JSONArray();
            for (Object obj2 : set2) {
                jSONArray2.put(obj2);
            }
            JSONObject put3 = put2.put("invited_user_registered_cnt", jSONArray2).put("invites_received_to_user_cnt", r6.A00);
            Set<Object> set3 = r6.A0D;
            JSONArray jSONArray3 = new JSONArray();
            for (Object obj3 : set3) {
                jSONArray3.put(obj3);
            }
            String obj4 = put3.put("inviter_user_cnt", jSONArray3).toString();
            SharedPreferences sharedPreferences = this.A00;
            if (sharedPreferences == null) {
                sharedPreferences = this.A02.A01("payment_daily_usage_preferences");
                this.A00 = sharedPreferences;
            }
            sharedPreferences.edit().putString(l, obj4).apply();
        } catch (JSONException e) {
            e.getMessage();
        }
    }
}
