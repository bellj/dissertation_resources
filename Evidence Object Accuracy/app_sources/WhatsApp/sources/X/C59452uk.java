package X;

/* renamed from: X.2uk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59452uk extends AbstractC16320oo {
    public String A00;
    public String A01;
    public final int A02;
    public final AnonymousClass2K4 A03;
    public final AnonymousClass2K3 A04;
    public final C48122Ek A05;
    public final C30211Wn A06;
    public final String A07;

    public C59452uk(AbstractC15710nm r13, C14900mE r14, C16430p0 r15, AnonymousClass2K4 r16, AnonymousClass2K3 r17, C48122Ek r18, AnonymousClass2K1 r19, AnonymousClass1B4 r20, C16340oq r21, C30211Wn r22, C16590pI r23, C17170qN r24, AnonymousClass018 r25, C14850m9 r26, AbstractC14440lR r27, String str, String str2, String str3) {
        super(r13, r14, r15, r19, r20, r21, r24, r25, r26, r27);
        this.A04 = r17;
        this.A06 = r22;
        this.A05 = r18;
        this.A07 = str;
        this.A00 = str2;
        this.A01 = str3;
        this.A03 = r16;
        this.A02 = C16590pI.A00(r23).getDisplayMetrics().densityDpi;
        super.A01 = "2.0";
    }
}
