package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.2vQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59812vQ extends AbstractC37191le {
    public final WaImageView A00;
    public final WaTextView A01;

    public C59812vQ(View view) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.search_query);
        this.A00 = C12980iv.A0X(view, R.id.delete_button);
    }
}
