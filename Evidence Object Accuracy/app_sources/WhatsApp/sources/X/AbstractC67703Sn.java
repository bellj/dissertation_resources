package X;

import android.os.Looper;
import com.google.android.exoplayer2.Timeline;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.3Sn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC67703Sn implements AnonymousClass2CD {
    public Looper A00;
    public Timeline A01;
    public final AnonymousClass4P0 A02 = new AnonymousClass4P0(null, new CopyOnWriteArrayList(), 0);
    public final AnonymousClass1Or A03 = new AnonymousClass1Or(null, new CopyOnWriteArrayList(), 0);
    public final ArrayList A04 = C12980iv.A0w(1);
    public final HashSet A05 = new HashSet(1);

    public abstract void A00();

    public abstract void A02(AnonymousClass5QP v);

    public final void A01(Timeline timeline) {
        this.A01 = timeline;
        Iterator it = this.A04.iterator();
        while (it.hasNext()) {
            ((AnonymousClass5SQ) it.next()).AWC(timeline, this);
        }
    }

    @Override // X.AnonymousClass2CD
    public final void A8u(AnonymousClass5SQ r4) {
        HashSet hashSet = this.A05;
        boolean z = !hashSet.isEmpty();
        hashSet.remove(r4);
        if (z && hashSet.isEmpty() && (this instanceof AbstractC56052kF)) {
            Iterator A0t = C12990iw.A0t(((AbstractC56052kF) this).A02);
            while (A0t.hasNext()) {
                AnonymousClass4P6 r0 = (AnonymousClass4P6) A0t.next();
                r0.A01.A8u(r0.A00);
            }
        }
    }

    @Override // X.AnonymousClass2CD
    public final void A9J(AnonymousClass5SQ r4) {
        HashSet hashSet = this.A05;
        boolean isEmpty = hashSet.isEmpty();
        hashSet.add(r4);
        if (isEmpty && (this instanceof AbstractC56052kF)) {
            Iterator A0t = C12990iw.A0t(((AbstractC56052kF) this).A02);
            while (A0t.hasNext()) {
                AnonymousClass4P6 r0 = (AnonymousClass4P6) A0t.next();
                r0.A01.A9J(r0.A00);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r1 == r2) goto L_0x000b;
     */
    @Override // X.AnonymousClass2CD
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void AZV(X.AnonymousClass5SQ r4, X.AnonymousClass5QP r5) {
        /*
            r3 = this;
            android.os.Looper r2 = android.os.Looper.myLooper()
            android.os.Looper r1 = r3.A00
            if (r1 == 0) goto L_0x000b
            r0 = 0
            if (r1 != r2) goto L_0x000c
        L_0x000b:
            r0 = 1
        L_0x000c:
            X.C95314dV.A03(r0)
            com.google.android.exoplayer2.Timeline r1 = r3.A01
            java.util.ArrayList r0 = r3.A04
            r0.add(r4)
            android.os.Looper r0 = r3.A00
            if (r0 != 0) goto L_0x0025
            r3.A00 = r2
            java.util.HashSet r0 = r3.A05
            r0.add(r4)
            r3.A02(r5)
        L_0x0024:
            return
        L_0x0025:
            if (r1 == 0) goto L_0x0024
            r3.A9J(r4)
            r4.AWC(r1, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC67703Sn.AZV(X.5SQ, X.5QP):void");
    }

    @Override // X.AnonymousClass2CD
    public final void AaA(AnonymousClass5SQ r2) {
        ArrayList arrayList = this.A04;
        arrayList.remove(r2);
        if (arrayList.isEmpty()) {
            this.A00 = null;
            this.A01 = null;
            this.A05.clear();
            A00();
            return;
        }
        A8u(r2);
    }

    @Override // X.AnonymousClass2CD
    public final void AaI(AbstractC28711Os r5) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.A03.A02;
        Iterator it = copyOnWriteArrayList.iterator();
        while (it.hasNext()) {
            AnonymousClass4M2 r1 = (AnonymousClass4M2) it.next();
            if (r1.A01 == r5) {
                copyOnWriteArrayList.remove(r1);
            }
        }
    }
}
