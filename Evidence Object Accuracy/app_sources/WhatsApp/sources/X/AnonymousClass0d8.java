package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.0d8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0d8 implements Runnable {
    public final /* synthetic */ RunnableC10040dv A00;
    public final /* synthetic */ AnonymousClass0SZ A01;

    public AnonymousClass0d8(RunnableC10040dv r1, AnonymousClass0SZ r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        RunnableC10040dv r3 = this.A00;
        C06040Ry r2 = r3.A01;
        if (r2.A00 == r3.A00) {
            List list = r3.A02;
            AnonymousClass0SZ r1 = this.A01;
            r2.A01 = list;
            r2.A02 = Collections.unmodifiableList(list);
            r1.A01(r2.A04);
        }
    }
}
