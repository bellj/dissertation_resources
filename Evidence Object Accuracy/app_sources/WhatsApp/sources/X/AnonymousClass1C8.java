package X;

/* renamed from: X.1C8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1C8 {
    public long A00;
    public C14830m7 A01;
    public C14820m6 A02;
    public boolean A03 = false;

    public void A00() {
        if (this.A03) {
            long currentTimeMillis = System.currentTimeMillis() - this.A00;
            long j = this.A02.A00.getLong("language_selector_time_spent", 0);
            this.A02.A00.edit().putLong("language_selector_time_spent", j + currentTimeMillis).commit();
        }
        this.A03 = false;
    }
}
