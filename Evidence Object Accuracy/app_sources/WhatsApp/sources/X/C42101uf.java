package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1uf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42101uf {
    public final int A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final C47822Cw A04;
    public final C15370n3 A05;
    public final UserJid A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;
    public final boolean A0J;
    public final boolean A0K;
    public final boolean A0L;

    public /* synthetic */ C42101uf(AnonymousClass1Mg r3) {
        this.A05 = r3.A0J;
        this.A06 = r3.A0K;
        this.A09 = r3.A0L;
        this.A0D = r3.A0A;
        this.A03 = r3.A03;
        this.A07 = r3.A05;
        this.A0A = r3.A07;
        this.A0G = r3.A0D;
        this.A08 = r3.A06;
        this.A02 = r3.A02;
        this.A01 = r3.A01;
        this.A00 = r3.A00;
        this.A0C = r3.A09;
        this.A0K = r3.A0H;
        this.A0L = r3.A0I;
        this.A0J = r3.A0G;
        this.A0B = r3.A08;
        this.A0E = r3.A0B;
        this.A0I = r3.A0F;
        this.A0F = r3.A0C;
        this.A0H = r3.A0E;
        this.A04 = r3.A04;
    }
}
