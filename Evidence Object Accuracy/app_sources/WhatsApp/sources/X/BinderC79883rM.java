package X;

import android.os.IInterface;

/* renamed from: X.3rM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class BinderC79883rM extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115695So A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC79883rM(AbstractC115695So r2) {
        super("com.google.android.gms.maps.internal.IOnCameraIdleListener");
        this.A00 = r2;
    }
}
