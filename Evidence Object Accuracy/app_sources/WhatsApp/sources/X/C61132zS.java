package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.2zS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C61132zS extends AbstractC61152zU {
    public C20510vs A00;
    public AnonymousClass3D0 A01;
    public boolean A02;

    public C61132zS(Context context) {
        super(context);
        A00();
    }

    @Override // X.AbstractC74133hN
    public void A00() {
        if (!this.A02) {
            this.A02 = true;
            this.A04 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    @Override // X.AbstractC61152zU
    public int getNegativeButtonTextResId() {
        return R.string.no_thanks;
    }

    @Override // X.AbstractC61152zU
    public int getPositiveButtonIconResId() {
        return R.drawable.ic_settings_contacts;
    }

    @Override // X.AbstractC61152zU
    public int getPositiveButtonTextResId() {
        return R.string.vcards_view_all;
    }

    public void setup(C20510vs r1, AnonymousClass3D0 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
