package X;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Mx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28371Mx {
    public static Object[] copy(Object[] objArr, int i, int i2, Object[] objArr2) {
        return Arrays.copyOfRange(objArr, i, i2, objArr2.getClass());
    }

    public static Object[] newArray(Object[] objArr, int i) {
        return (Object[]) Array.newInstance(objArr.getClass().getComponentType(), i);
    }

    public static Set preservesInsertionOrderOnAddsSet() {
        return AnonymousClass5I9.create();
    }

    public static Map preservesInsertionOrderOnPutsMap() {
        return AnonymousClass5I4.create();
    }
}
