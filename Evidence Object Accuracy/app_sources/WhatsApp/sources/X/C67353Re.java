package X;

/* renamed from: X.3Re  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67353Re implements AbstractC009404s {
    public final /* synthetic */ C49012Iu A00;
    public final /* synthetic */ C15580nU A01;
    public final /* synthetic */ C36051jF A02;

    public C67353Re(C49012Iu r1, C15580nU r2, C36051jF r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C49012Iu r0 = this.A00;
        C15580nU r11 = this.A01;
        C36051jF r12 = this.A02;
        AnonymousClass01J r1 = r0.A00.A03;
        C15570nT A0S = C12970iu.A0S(r1);
        AbstractC14440lR A0T = C12960it.A0T(r1);
        C19990v2 A0c = C12980iv.A0c(r1);
        C15550nR A0O = C12960it.A0O(r1);
        C15610nY A0P = C12960it.A0P(r1);
        C22700zV A0a = C12980iv.A0a(r1);
        C15600nX A0d = C12980iv.A0d(r1);
        AnonymousClass11B r10 = (AnonymousClass11B) r1.AE2.get();
        return new C36041jE(A0S, A0O, (AnonymousClass11D) r1.A8r.get(), A0a, A0P, C12980iv.A0b(r1), A0c, A0d, r10, r11, r12, (C253118x) r1.AAW.get(), A0T);
    }
}
