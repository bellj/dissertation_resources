package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.Jid;

/* renamed from: X.1Vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29991Vn extends Jid implements Parcelable {
    public static final C29991Vn A00 = new C29991Vn();
    public static final Parcelable.Creator CREATOR = new C100064lJ();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "g.us";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 16;
    }

    public C29991Vn() {
        super("");
    }

    public C29991Vn(Parcel parcel) {
        super(parcel);
    }
}
