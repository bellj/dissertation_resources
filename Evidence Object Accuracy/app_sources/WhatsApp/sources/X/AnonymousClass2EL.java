package X;

import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;

/* renamed from: X.2EL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EL implements AnonymousClass2EJ {
    public final /* synthetic */ AnonymousClass2EM A00;

    public AnonymousClass2EL(AnonymousClass2EM r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2EJ
    public void APm(Pair pair) {
        AnonymousClass016 r1;
        int intValue = ((Number) pair.first).intValue();
        AnonymousClass2EM r0 = this.A00;
        if (405 == intValue) {
            r1 = r0.A03;
        } else {
            r1 = r0.A07;
        }
        if (r1 != null) {
            r1.A0A(Boolean.TRUE);
        }
    }

    @Override // X.AnonymousClass2EJ
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        this.A00.A0L.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(this, 10, obj));
    }
}
