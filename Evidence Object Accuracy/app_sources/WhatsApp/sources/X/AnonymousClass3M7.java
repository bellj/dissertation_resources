package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import java.math.BigDecimal;
import java.util.Date;

/* renamed from: X.3M7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3M7 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(41);
    public final C30711Yn A00;
    public final BigDecimal A01;
    public final Date A02;
    public final Date A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass3M7(C30711Yn r1, BigDecimal bigDecimal, Date date, Date date2) {
        this.A01 = bigDecimal;
        this.A00 = r1;
        this.A03 = date;
        this.A02 = date2;
    }

    public AnonymousClass3M7(Parcel parcel) {
        this.A01 = (BigDecimal) parcel.readSerializable();
        this.A00 = new C30711Yn(parcel.readString());
        this.A03 = (Date) parcel.readSerializable();
        this.A02 = (Date) parcel.readSerializable();
    }

    public boolean A00(Date date) {
        Date date2;
        Date date3 = this.A03;
        if (date3 == null || (date2 = this.A02) == null || (date.after(date3) && date.before(date2))) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3M7 r5 = (AnonymousClass3M7) obj;
            if (!C29941Vi.A00(this.A01, r5.A01) || !C29941Vi.A00(this.A00, r5.A00) || !C29941Vi.A00(this.A03, r5.A03) || !C29941Vi.A00(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i;
        int A08 = C12990iw.A08(this.A00, this.A01.hashCode() * 31) * 31;
        Date date = this.A03;
        int i2 = 0;
        if (date != null) {
            i = date.hashCode();
        } else {
            i = 0;
        }
        int i3 = (A08 + i) * 31;
        Date date2 = this.A02;
        if (date2 != null) {
            i2 = date2.hashCode();
        }
        return i3 + i2;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(this.A01);
        parcel.writeString(this.A00.A00);
        parcel.writeSerializable(this.A03);
        parcel.writeSerializable(this.A02);
    }
}
