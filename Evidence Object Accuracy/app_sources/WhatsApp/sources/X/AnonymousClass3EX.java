package X;

import com.facebook.redex.RunnableBRunnable0Shape0S2300100_I1;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3EX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3EX {
    public AnonymousClass016 A00;
    public final C21770xx A01;
    public final AnonymousClass19Q A02;
    public final UserJid A03;
    public final AbstractC14440lR A04;

    public AnonymousClass3EX(C21770xx r1, AnonymousClass19Q r2, UserJid userJid, AbstractC14440lR r4) {
        this.A04 = r4;
        this.A03 = userJid;
        this.A01 = r1;
        this.A02 = r2;
    }

    public void A00() {
        C12990iw.A1O(this.A04, this, 31);
    }

    public void A01(C44691zO r11, UserJid userJid, String str, String str2, long j) {
        this.A04.Ab2(new RunnableBRunnable0Shape0S2300100_I1(this, userJid, r11, str, str2, 0, j));
    }
}
