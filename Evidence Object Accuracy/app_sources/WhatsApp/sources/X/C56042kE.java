package X;

import android.net.Uri;
import java.util.Collections;
import java.util.List;

@Deprecated
/* renamed from: X.2kE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56042kE extends AbstractC56052kF {
    public final C56062kG A00;

    @Deprecated
    public C56042kE(Uri uri, AnonymousClass5SK r9, AbstractC47452At r10) {
        String str = null;
        AnonymousClass4y7 r4 = new AnonymousClass4y7();
        List emptyList = Collections.emptyList();
        List emptyList2 = Collections.emptyList();
        AnonymousClass4XB r1 = null;
        C95314dV.A04(true);
        if (uri != null) {
            r1 = new AnonymousClass4XB(uri, null, emptyList, emptyList2);
            str = uri.toString();
        }
        this.A00 = A00(r1, r9, r10, r4, str);
    }

    public /* synthetic */ C56042kE(Uri uri, AnonymousClass5SK r7, AbstractC47452At r8, AnonymousClass5QO r9, Object obj) {
        AnonymousClass4XB r4 = null;
        String str = null;
        List emptyList = Collections.emptyList();
        List emptyList2 = Collections.emptyList();
        C95314dV.A04(true);
        if (uri != null) {
            r4 = new AnonymousClass4XB(uri, obj, emptyList, emptyList2);
            str = uri.toString();
        }
        this.A00 = A00(r4, r7, r8, r9, str);
    }

    public static C56062kG A00(AnonymousClass4XB r13, AnonymousClass5SK r14, AbstractC47452At r15, AnonymousClass5QO r16, String str) {
        return new C56062kG(new AnonymousClass4XL(new AnonymousClass4WP(Long.MIN_VALUE), new AnonymousClass4XK(-3.4028235E38f, -3.4028235E38f, -9223372036854775807L, -9223372036854775807L, -9223372036854775807L), r13, new AnonymousClass4WF(), str), AbstractC116865Xf.A00, r14, r15, r16);
    }

    @Override // X.AbstractC56052kF, X.AbstractC67703Sn
    public void A02(AnonymousClass5QP r2) {
        super.A02(r2);
        A04(this.A00);
    }

    @Override // X.AnonymousClass2CD
    public AbstractC14080kp A8S(C28741Ov r2, AnonymousClass5VZ r3, long j) {
        return this.A00.A8S(r2, r3, j);
    }

    @Override // X.AnonymousClass2CD
    public AnonymousClass4XL AED() {
        return this.A00.A07;
    }

    @Override // X.AnonymousClass2CD
    public void Aa9(AbstractC14080kp r2) {
        this.A00.Aa9(r2);
    }
}
