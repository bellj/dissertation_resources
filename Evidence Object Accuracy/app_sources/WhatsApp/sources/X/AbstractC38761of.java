package X;

import android.graphics.Bitmap;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/* renamed from: X.1of  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC38761of {
    public boolean A00;
    public boolean A01;
    public final C14900mE A02;
    public final C64843Hc A03;
    public final AbstractC38741od A04;
    public final Object A05 = new Object();
    public final String A06;
    public final List A07;
    public final List A08;
    public final Map A09 = new HashMap();
    public final Stack A0A = new Stack();
    public final Stack A0B = new Stack();

    public AbstractC38761of(C14900mE r5, AbstractC38741od r6, File file, String str, int i, long j) {
        this.A02 = r5;
        this.A06 = str;
        this.A03 = new C64843Hc(file, j);
        this.A04 = r6;
        this.A07 = new ArrayList(i);
        this.A08 = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            this.A08.add(new AnonymousClass396(this, this.A06));
            this.A07.add(new AnonymousClass395(this, this.A06));
        }
    }

    public void A00(AnonymousClass5XB r5) {
        synchronized (this.A05) {
            AnonymousClass3BX r2 = (AnonymousClass3BX) this.A09.get(r5.getId());
            if (r2 != null && r5.getId().equals(r2.A03)) {
                r2.A05.remove(r5);
            }
        }
    }

    public void A01(AnonymousClass5XB r6, boolean z) {
        AbstractC38741od r2 = this.A04;
        r2.AS3(r6);
        C64843Hc r0 = this.A03;
        Bitmap bitmap = (Bitmap) r0.A02.A04(r6.getId());
        if (bitmap != null) {
            if (bitmap != C64843Hc.A05) {
                r2.AS7(bitmap, r6, true);
                return;
            } else if (!z) {
                r2.ARx(r6);
                return;
            }
        }
        r2.AMK(r6);
        AnonymousClass009.A01();
        synchronized (this.A05) {
            Map map = this.A09;
            AnonymousClass3BX r22 = (AnonymousClass3BX) map.get(r6.getId());
            if (r22 == null) {
                r22 = new AnonymousClass3BX(r6);
                map.put(r22.A03, r22);
            } else {
                r22.A05.put(r6, r6);
            }
            Stack stack = this.A0A;
            stack.remove(r22);
            this.A0B.remove(r22);
            stack.push(r22);
            synchronized (stack) {
                stack.notify();
            }
        }
        if (!this.A00) {
            for (Thread thread : this.A07) {
                if (thread.getState() == Thread.State.NEW) {
                    thread.start();
                }
            }
            this.A00 = true;
        }
        if (!this.A01) {
            for (Thread thread2 : this.A08) {
                if (thread2.getState() == Thread.State.NEW) {
                    thread2.start();
                }
            }
            this.A01 = true;
        }
    }

    public void A02(boolean z) {
        for (Thread thread : this.A08) {
            thread.interrupt();
        }
        for (Thread thread2 : this.A07) {
            thread2.interrupt();
        }
        C64843Hc r0 = this.A03;
        if (r0 != null) {
            r0.A03(z);
        }
        this.A01 = false;
        this.A00 = false;
    }
}
