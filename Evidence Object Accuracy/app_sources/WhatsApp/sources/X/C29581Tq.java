package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;

/* renamed from: X.1Tq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29581Tq implements AnonymousClass01N {
    public final /* synthetic */ C15690nk A00;

    public C29581Tq(C15690nk r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public /* bridge */ /* synthetic */ Object get() {
        Log.i("externalfilevalidator/initializing whitelist lazily");
        C14330lG r5 = this.A00.A02;
        AnonymousClass009.A05(r5);
        HashSet hashSet = new HashSet();
        hashSet.add(r5.A0A());
        hashSet.add(r5.A06());
        hashSet.add(r5.A08());
        hashSet.add(r5.A0J("personal"));
        hashSet.add(r5.A0J("business"));
        hashSet.add(r5.A07());
        File file = r5.A04().A09;
        C14330lG.A03(file, false);
        hashSet.add(file);
        Context context = r5.A03.A00;
        File file2 = new File(context.getFilesDir(), "Gifs");
        C14330lG.A03(file2, false);
        hashSet.add(file2);
        File file3 = new File(context.getCacheDir(), "stickers_cache");
        C14330lG.A03(file3, false);
        hashSet.add(file3);
        hashSet.add(r5.A04().A08);
        hashSet.add(r5.A04().A06);
        hashSet.add(r5.A04().A07);
        return Collections.unmodifiableSet(hashSet);
    }
}
