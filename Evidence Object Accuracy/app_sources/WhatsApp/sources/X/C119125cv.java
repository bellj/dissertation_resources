package X;

import android.view.View;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.util.Log;

/* renamed from: X.5cv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119125cv extends AnonymousClass2UH {
    public final /* synthetic */ PaymentBottomSheet A00;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C119125cv(PaymentBottomSheet paymentBottomSheet) {
        this.A00 = paymentBottomSheet;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 5 || i == 4) {
            Log.i(C12960it.A0W(i, "onStateChanged() new State:"));
            this.A00.A1B();
        }
    }
}
