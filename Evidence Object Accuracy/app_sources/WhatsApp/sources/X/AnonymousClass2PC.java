package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2PC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PC extends AnonymousClass2PA {
    public final boolean A00;

    public AnonymousClass2PC(Jid jid, String str, long j, boolean z) {
        super(jid, str, j);
        this.A00 = z;
    }
}
