package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.3jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75403jn extends AnonymousClass03U {
    public final ImageView A00;
    public final WaTextView A01;

    public C75403jn(View view) {
        super(view);
        this.A00 = C12970iu.A0K(view, R.id.chevron);
        this.A01 = (WaTextView) AnonymousClass028.A0D(view, R.id.bread_crumbs_name_text);
    }
}
