package X;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxLListenerShape13S0100000_1_I1;
import com.whatsapp.R;

/* renamed from: X.3Bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63443Bp {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public RecyclerView A05;
    public C54392ge A06;
    public final ViewTreeObserver.OnGlobalLayoutListener A07 = new IDxLListenerShape13S0100000_1_I1(this, 9);
    public final GridLayoutManager A08;
    public final AbstractC018308n A09;

    public C63443Bp(Context context, ViewGroup viewGroup, RecyclerView recyclerView, C54392ge r7) {
        int i;
        AbstractC018308n r2 = new C54602gz(this);
        this.A09 = r2;
        this.A01 = context.getResources().getDimensionPixelSize(R.dimen.sticker_picker_item);
        if (viewGroup != null) {
            i = viewGroup.getWidth();
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            AnonymousClass12P.A00(context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            i = displayMetrics.widthPixels;
        }
        this.A04 = i;
        int i2 = i / this.A01;
        this.A00 = i2;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(i2 <= 0 ? 1 : i2);
        this.A08 = gridLayoutManager;
        this.A05 = recyclerView;
        this.A06 = r7;
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.A0l(r2);
        recyclerView.setItemAnimator(null);
    }
}
