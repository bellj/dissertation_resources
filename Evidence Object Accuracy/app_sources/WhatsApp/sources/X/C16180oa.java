package X;

import android.content.SharedPreferences;

/* renamed from: X.0oa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16180oa {
    public SharedPreferences A00;
    public final C16630pM A01;
    public final String A02;

    public C16180oa(C16630pM r4) {
        this.A01 = r4;
        StringBuilder sb = new StringBuilder("acs_token_");
        sb.append("WA_BizDirectorySearch");
        this.A02 = sb.toString();
    }

    public final SharedPreferences A00() {
        synchronized (this) {
            if (this.A00 == null) {
                this.A00 = this.A01.A01(this.A02);
            }
        }
        return this.A00;
    }

    public void A01(int i) {
        SharedPreferences.Editor remove;
        SharedPreferences.Editor edit = A00().edit();
        if (i > 0) {
            remove = edit.putInt("token_not_ready_reason", i);
        } else {
            remove = edit.remove("token_not_ready_reason");
        }
        remove.apply();
    }

    public final void A02(String str, int i) {
        SharedPreferences.Editor remove;
        SharedPreferences.Editor edit = A00().edit();
        if (i >= 0) {
            remove = edit.putInt(str, i);
        } else {
            remove = edit.remove(str);
        }
        remove.apply();
    }

    public final void A03(String str, long j) {
        SharedPreferences.Editor remove;
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        SharedPreferences.Editor edit = A00().edit();
        if (i > 0) {
            remove = edit.putLong(str, j);
        } else {
            remove = edit.remove(str);
        }
        remove.apply();
    }

    public final void A04(String str, String str2) {
        SharedPreferences.Editor remove;
        SharedPreferences.Editor edit = A00().edit();
        if (str2 != null) {
            remove = edit.putString(str, str2);
        } else {
            remove = edit.remove(str);
        }
        remove.apply();
    }
}
