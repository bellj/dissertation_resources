package X;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.5dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC119405dv extends C28791Pa {
    public Toolbar A00;
    public String A01;
    public boolean A02 = false;
    public final AnonymousClass018 A03;
    public final WaBloksActivity A04;

    public abstract void A03(Intent intent, Bundle bundle);

    public abstract void A04(AbstractC115815Ta v);

    public AbstractC119405dv(AnonymousClass018 r2, WaBloksActivity waBloksActivity) {
        this.A03 = r2;
        this.A04 = waBloksActivity;
    }

    public AbstractC005102i A00() {
        AbstractC005102i A1U = this.A04.A1U();
        AnonymousClass009.A05(A1U);
        return A1U;
    }

    public void A01() {
        if (this instanceof C124435pV) {
            C124435pV r0 = (C124435pV) this;
            AbstractC28681Oo r1 = r0.A00;
            if (r1 != null) {
                AnonymousClass1AI.A08(r0.A04.A02, r1);
            }
        } else if (this instanceof C124425pU) {
            C124425pU r12 = (C124425pU) this;
            if (((AbstractC119405dv) r12).A02) {
                WaBloksActivity waBloksActivity = r12.A04;
                waBloksActivity.A04.A02(waBloksActivity.A0B).A01(new AnonymousClass6EA(r12.A01.A00, true));
            }
        }
    }

    public boolean A02() {
        if (this instanceof C124435pV) {
            return C12960it.A1W(((C124435pV) this).A00);
        }
        if (!(this instanceof C124425pU)) {
            return false;
        }
        return C12960it.A1S(this.A02 ? 1 : 0);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Drawable A00;
        WaBloksActivity waBloksActivity = this.A04;
        AnonymousClass009.A0F(C12970iu.A1Z(activity, waBloksActivity));
        if (bundle != null) {
            this.A01 = bundle.getString("bk_navigation_bar_title");
        }
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(waBloksActivity, R.id.wabloks_screen_toolbar);
        this.A00 = toolbar;
        toolbar.setTitle("");
        Toolbar toolbar2 = this.A00;
        toolbar2.A07();
        waBloksActivity.A1e(toolbar2);
        A00().A0M(true);
        Toolbar toolbar3 = this.A00;
        if (!(this instanceof C124425pU)) {
            A00 = AnonymousClass2GF.A00(waBloksActivity, this.A03, R.drawable.ic_back);
            C117305Zk.A13(waBloksActivity.getResources(), A00, R.color.wabloksui_screen_back_arrow);
        } else {
            A00 = ((C124425pU) this).A01.A00();
        }
        toolbar3.setNavigationIcon(A00);
        this.A00.setBackgroundColor(waBloksActivity.getResources().getColor(R.color.wabloksui_screen_toolbar));
        this.A00.setNavigationOnClickListener(C117305Zk.A0A(activity, 199));
        A03(activity.getIntent(), bundle);
    }

    @Override // X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        bundle.putString("bk_navigation_bar_title", this.A01);
        super.onActivitySaveInstanceState(activity, bundle);
    }
}
