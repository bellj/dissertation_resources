package X;

import android.os.Process;
import android.util.Pair;
import java.util.Map;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3SS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SS implements AnonymousClass5SD {
    public Pair A00 = null;
    public RunnableFuture A01;
    public final AtomicInteger A02 = new AtomicInteger(-1);

    public AnonymousClass3SS(C14260l7 r8, C89024Ij r9, C92554Wj r10, Map map) {
        this.A01 = new FutureTask(new CallableC71483d0(r8, r9, this, r10, map));
    }

    @Override // X.AnonymousClass5SD
    public Pair Aal() {
        RunnableFuture runnableFuture;
        Pair pair;
        synchronized (this) {
            runnableFuture = this.A01;
            pair = this.A00;
        }
        if (pair != null) {
            return pair;
        }
        if (runnableFuture != null) {
            AtomicInteger atomicInteger = this.A02;
            if (atomicInteger.compareAndSet(-1, Process.myTid())) {
                runnableFuture.run();
            }
            Pair pair2 = (Pair) AnonymousClass3J3.A01(runnableFuture, atomicInteger.get());
            synchronized (this) {
                this.A00 = pair2;
                this.A01 = null;
            }
            return pair2;
        }
        throw C12960it.A0U("The future task is null but there is no computed result");
    }
}
