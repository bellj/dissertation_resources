package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import org.json.JSONObject;

/* renamed from: X.0qH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17110qH {
    public SharedPreferences A00;
    public SharedPreferences A01;
    public final C16630pM A02;

    public C17110qH(C16630pM r1) {
        this.A02 = r1;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A02.A01("triggered_block_prefs");
            this.A00 = sharedPreferences;
        }
        AnonymousClass009.A05(sharedPreferences);
        return sharedPreferences;
    }

    public final JSONObject A01(AbstractC14640lm r4) {
        String string = A00().getString(r4.getRawString(), null);
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return new JSONObject(string);
    }

    public final void A02(AbstractC14640lm r4, JSONObject jSONObject) {
        A00().edit().putString(r4.getRawString(), jSONObject.toString()).apply();
    }
}
