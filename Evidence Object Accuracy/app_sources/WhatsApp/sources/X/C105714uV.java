package X;

import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;

/* renamed from: X.4uV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105714uV implements AbstractC11910h4 {
    public boolean A00;
    public final /* synthetic */ TabLayout A01;

    public C105714uV(TabLayout tabLayout) {
        this.A01 = tabLayout;
    }

    @Override // X.AbstractC11910h4
    public void ALv(AnonymousClass01A r3, AnonymousClass01A r4, ViewPager viewPager) {
        TabLayout tabLayout = this.A01;
        if (tabLayout.A0K == viewPager) {
            tabLayout.A0B(r4, this.A00);
        }
    }
}
