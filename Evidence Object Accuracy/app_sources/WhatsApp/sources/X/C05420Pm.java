package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.0Pm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05420Pm {
    public final String A00;
    public final String A01;
    public final String A02;
    public final List A03;
    public final List A04;

    public C05420Pm(String str, String str2, String str3, List list, List list2) {
        this.A02 = str;
        this.A00 = str2;
        this.A01 = str3;
        this.A03 = Collections.unmodifiableList(list);
        this.A04 = Collections.unmodifiableList(list2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C05420Pm r4 = (C05420Pm) obj;
            if (this.A02.equals(r4.A02) && this.A00.equals(r4.A00) && this.A01.equals(r4.A01) && this.A03.equals(r4.A03)) {
                return this.A04.equals(r4.A04);
            }
        }
        return false;
    }

    public int hashCode() {
        return (((((((this.A02.hashCode() * 31) + this.A00.hashCode()) * 31) + this.A01.hashCode()) * 31) + this.A03.hashCode()) * 31) + this.A04.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ForeignKey{referenceTable='");
        sb.append(this.A02);
        sb.append('\'');
        sb.append(", onDelete='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append(", onUpdate='");
        sb.append(this.A01);
        sb.append('\'');
        sb.append(", columnNames=");
        sb.append(this.A03);
        sb.append(", referenceColumnNames=");
        sb.append(this.A04);
        sb.append('}');
        return sb.toString();
    }
}
