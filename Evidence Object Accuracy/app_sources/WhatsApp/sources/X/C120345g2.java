package X;

import android.content.Context;
import java.util.concurrent.TimeUnit;

/* renamed from: X.5g2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120345g2 extends AbstractC92724Xe {
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final C18600si A02;

    public C120345g2(C14830m7 r1, C14820m6 r2, C18660so r3, AnonymousClass17T r4, C18600si r5, AbstractC16870pt r6) {
        super(r3, r4, r6);
        this.A00 = r1;
        this.A02 = r5;
        this.A01 = r2;
    }

    @Override // X.AbstractC92724Xe
    public void A00(Context context, String str) {
        C14820m6 r6 = this.A01;
        int i = (C12980iv.A0E(r6.A00, "payments_merchant_upsell_start_cool_off_timestamp") > -1 ? 1 : (C12980iv.A0E(r6.A00, "payments_merchant_upsell_start_cool_off_timestamp") == -1 ? 0 : -1));
        long currentTimeMillis = System.currentTimeMillis();
        if (i == 0) {
            currentTimeMillis += TimeUnit.DAYS.toMillis(30);
        }
        C18600si r7 = this.A02;
        C12970iu.A1B(C117295Zj.A05(r7), "payment_smb_upsell_view_count", C12970iu.A01(r7.A01(), "payment_smb_upsell_view_count") + 1);
        r6.A0q("payments_merchant_upsell_start_cool_off_timestamp", currentTimeMillis);
        super.A02.AKg(C12960it.A0V(), 36, "merchant_upsell_prompt", str);
        super.A01.A00(context, "merchant_upsell_prompt");
    }

    @Override // X.AbstractC92724Xe
    public void A01(String str) {
        C14820m6 r6 = this.A01;
        int i = (C12980iv.A0E(r6.A00, "payments_merchant_upsell_start_cool_off_timestamp") > -1 ? 1 : (C12980iv.A0E(r6.A00, "payments_merchant_upsell_start_cool_off_timestamp") == -1 ? 0 : -1));
        long currentTimeMillis = System.currentTimeMillis();
        if (i == 0) {
            currentTimeMillis += TimeUnit.DAYS.toMillis(30);
        }
        C18600si r7 = this.A02;
        C12970iu.A1B(C117295Zj.A05(r7), "payment_smb_upsell_view_count", C12970iu.A01(r7.A01(), "payment_smb_upsell_view_count") + 1);
        r6.A0q("payments_merchant_upsell_start_cool_off_timestamp", currentTimeMillis);
        super.A02.AKg(C12960it.A0V(), 10, "merchant_upsell_prompt", str);
    }

    @Override // X.AbstractC92724Xe
    public boolean A02() {
        return super.A02() && this.A01.A1J("payments_merchant_upsell_start_cool_off_timestamp", TimeUnit.DAYS.toMillis(30)) && C12970iu.A01(this.A02.A01(), "payment_smb_upsell_view_count") < 3;
    }
}
