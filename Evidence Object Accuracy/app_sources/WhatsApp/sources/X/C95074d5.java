package X;

import android.os.Trace;

/* renamed from: X.4d5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95074d5 {
    public static void A00() {
        if (AnonymousClass3JZ.A01 >= 18) {
            A01();
        }
    }

    public static void A01() {
        Trace.endSection();
    }

    public static void A02(String str) {
        if (AnonymousClass3JZ.A01 >= 18) {
            A03(str);
        }
    }

    public static void A03(String str) {
        Trace.beginSection(str);
    }
}
