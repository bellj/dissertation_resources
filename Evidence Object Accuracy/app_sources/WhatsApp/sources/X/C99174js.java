package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4js  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99174js implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        boolean z2 = false;
        float f = -1.0f;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 3:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 4:
                    i3 = C95664e9.A02(parcel, readInt);
                    break;
                case 5:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 6:
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 7:
                    f = C95664e9.A00(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78583p9(f, i, i2, i3, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78583p9[i];
    }
}
