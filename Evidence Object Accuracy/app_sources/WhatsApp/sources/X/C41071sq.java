package X;

/* renamed from: X.1sq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41071sq extends AbstractC16110oT {
    public Long A00;

    public C41071sq() {
        super(2508, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamMdAppStateCompanionsRemoval {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "syncdCompanionsRemovalRetryCount", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
