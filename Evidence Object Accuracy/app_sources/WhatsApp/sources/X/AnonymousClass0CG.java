package X;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.ActionBarContextView;
import java.lang.ref.WeakReference;

/* renamed from: X.0CG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CG extends AbstractC009504t implements AbstractC011605p {
    public Context A00;
    public AnonymousClass02Q A01;
    public AnonymousClass07H A02;
    public ActionBarContextView A03;
    public WeakReference A04;
    public boolean A05;

    public AnonymousClass0CG(Context context, AnonymousClass02Q r4, ActionBarContextView actionBarContextView) {
        this.A00 = context;
        this.A03 = actionBarContextView;
        this.A01 = r4;
        AnonymousClass07H r1 = new AnonymousClass07H(actionBarContextView.getContext());
        r1.A00 = 1;
        this.A02 = r1;
        r1.A0C(this);
    }

    @Override // X.AbstractC009504t
    public Menu A00() {
        return this.A02;
    }

    @Override // X.AbstractC009504t
    public MenuInflater A01() {
        return new AnonymousClass0Ax(this.A03.getContext());
    }

    @Override // X.AbstractC009504t
    public View A02() {
        WeakReference weakReference = this.A04;
        if (weakReference != null) {
            return (View) weakReference.get();
        }
        return null;
    }

    @Override // X.AbstractC009504t
    public CharSequence A03() {
        return this.A03.A0D;
    }

    @Override // X.AbstractC009504t
    public CharSequence A04() {
        return this.A03.A0E;
    }

    @Override // X.AbstractC009504t
    public void A05() {
        if (!this.A05) {
            this.A05 = true;
            this.A03.sendAccessibilityEvent(32);
            this.A01.AP3(this);
        }
    }

    @Override // X.AbstractC009504t
    public void A06() {
        this.A01.AU7(this.A02, this);
    }

    @Override // X.AbstractC009504t
    public void A07(int i) {
        A0A(this.A00.getString(i));
    }

    @Override // X.AbstractC009504t
    public void A08(int i) {
        A0B(this.A00.getString(i));
    }

    @Override // X.AbstractC009504t
    public void A09(View view) {
        this.A03.setCustomView(view);
        this.A04 = view != null ? new WeakReference(view) : null;
    }

    @Override // X.AbstractC009504t
    public void A0A(CharSequence charSequence) {
        this.A03.setSubtitle(charSequence);
    }

    @Override // X.AbstractC009504t
    public void A0B(CharSequence charSequence) {
        this.A03.setTitle(charSequence);
    }

    @Override // X.AbstractC009504t
    public void A0C(boolean z) {
        super.A01 = z;
        this.A03.setTitleOptional(z);
    }

    @Override // X.AbstractC009504t
    public boolean A0D() {
        return this.A03.A0H;
    }

    @Override // X.AbstractC011605p
    public boolean ASh(MenuItem menuItem, AnonymousClass07H r3) {
        return this.A01.ALr(menuItem, this);
    }

    @Override // X.AbstractC011605p
    public void ASi(AnonymousClass07H r2) {
        A06();
        AnonymousClass0XQ r0 = this.A03.A0A;
        if (r0 != null) {
            r0.A03();
        }
    }
}
