package X;

import android.content.ContentValues;
import com.whatsapp.util.Log;
import java.util.Locale;

/* renamed from: X.0yc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22150yc {
    public final C14830m7 A00;
    public final C43801xc A01;

    public C22150yc(AbstractC15710nm r3, C14830m7 r4, C16590pI r5, C231410n r6, C14850m9 r7) {
        this.A01 = new C43801xc(r5.A00, r3, r6, r7);
        this.A00 = r4;
    }

    public void A00(String str, String str2, Locale locale, byte[] bArr) {
        long A00 = this.A00.A00() / 1000;
        if (str == null) {
            str = "";
        }
        if (bArr == null) {
            bArr = new byte[0];
        }
        C16310on A02 = this.A01.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("lg", locale.getLanguage());
            contentValues.put("lc", locale.getCountry());
            contentValues.put("hash", str);
            contentValues.put("namespace", str2);
            contentValues.put("timestamp", Long.valueOf(A00));
            contentValues.put("data", bArr);
            A02.A03.A05(contentValues, "packs");
            A02.close();
            StringBuilder sb = new StringBuilder("language-pack-store/save-language-pack saved pack ");
            sb.append(AbstractC27291Gt.A05(locale));
            sb.append(" (");
            sb.append(str);
            sb.append(") ns=");
            sb.append(str2);
            Log.i(sb.toString());
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A01(String str, Locale locale) {
        long A00 = this.A00.A00() / 1000;
        C16310on A02 = this.A01.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("timestamp", Long.valueOf(A00));
            if (A02.A03.A00("packs", contentValues, "lg = ? AND lc = ? AND namespace = ?", new String[]{locale.getLanguage(), locale.getCountry(), str}) > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("language-pack-store/touch-language-pack updated timestamp for ");
                sb.append(locale);
                sb.append(" ns=");
                sb.append(str);
                Log.i(sb.toString());
            }
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
