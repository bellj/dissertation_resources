package X;

import android.os.Process;

/* renamed from: X.0fD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10790fD extends Thread {
    public final int A00;

    public C10790fD(Runnable runnable, String str, int i) {
        super(runnable, str);
        this.A00 = i;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        Process.setThreadPriority(this.A00);
        super.run();
    }
}
