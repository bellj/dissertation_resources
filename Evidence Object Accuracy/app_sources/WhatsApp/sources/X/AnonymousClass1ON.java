package X;

import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1ON  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ON extends AnonymousClass1OF implements AnonymousClass1OG {
    public int A00 = 0;
    public AnonymousClass1OO A01;
    public AnonymousClass1OG A02;
    public byte[] A03;
    public byte[] A04;
    public byte[] A05;
    public byte[] A06;
    public final C15820nx A07;
    public final AnonymousClass1OP A08;
    public final C14820m6 A09;
    public final C244315m A0A;
    public final JniBridge A0B;
    public final Object A0C = new Object();
    public final String A0D;

    public AnonymousClass1ON(C15820nx r2, AnonymousClass1OP r3, C14820m6 r4, C244315m r5, C32881ct r6, AbstractC14440lR r7, JniBridge jniBridge, String str) {
        super(r6, r7);
        this.A0B = jniBridge;
        this.A07 = r2;
        this.A09 = r4;
        this.A0A = r5;
        this.A0D = str;
        this.A08 = r3;
    }
}
