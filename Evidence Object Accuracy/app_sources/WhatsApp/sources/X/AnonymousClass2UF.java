package X;

/* renamed from: X.2UF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2UF extends AnonymousClass1MS {
    public final AbstractC15710nm A00;
    public final AbstractC450820c A01;
    public final C51322Tx A02;

    public AnonymousClass2UF(AbstractC15710nm r2, AbstractC450820c r3, C51322Tx r4) {
        super("ReaderThread");
        this.A01 = r3;
        this.A00 = r2;
        this.A02 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        if (X.AnonymousClass1V8.A02(r6, "notification") != false) goto L_0x0031;
     */
    @Override // java.lang.Thread, java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 494
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2UF.run():void");
    }
}
