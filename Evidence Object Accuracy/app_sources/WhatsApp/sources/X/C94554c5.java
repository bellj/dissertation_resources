package X;

/* renamed from: X.4c5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94554c5 {
    public static int A00(int i, int i2, boolean z) {
        int i3;
        int min = Math.min(4, A02(i, z));
        if (min > 0) {
            i3 = i2 / min;
        } else {
            i3 = 0;
        }
        double d = (double) i3;
        double d2 = 1.0d;
        if (C72463ee.A0Y(i, 12)) {
            d2 = 1.0d - 0.04d;
        }
        return (int) (d * d2);
    }

    public static int A01(int i, boolean z) {
        if (z) {
            int i2 = 1;
            if (i > 2) {
                i2 = 3;
                if (i <= 8) {
                    i2 = 2;
                }
            }
            if (i <= 0) {
                return 0;
            }
            if (i > 2) {
                return ((i + i2) - 1) / i2;
            }
            return i;
        } else if (i <= 2) {
            return 1;
        } else {
            return i <= 8 ? 2 : 3;
        }
    }

    public static int A02(int i, boolean z) {
        if (!z) {
            int i2 = 1;
            if (i > 2) {
                i2 = 3;
                if (i <= 8) {
                    i2 = 2;
                }
            }
            if (i <= 0) {
                return 0;
            }
            if (i > 2) {
                return ((i + i2) - 1) / i2;
            }
            return i;
        } else if (i <= 2) {
            return 1;
        } else {
            return i <= 8 ? 2 : 3;
        }
    }
}
