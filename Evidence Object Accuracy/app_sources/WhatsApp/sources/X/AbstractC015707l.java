package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.07l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC015707l implements Parcelable {
    public static final AbstractC015707l A01 = new AnonymousClass0E3();
    public static final Parcelable.Creator CREATOR = new AnonymousClass0VK();
    public final Parcelable A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ AbstractC015707l() {
        this.A00 = null;
    }

    public AbstractC015707l(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.A00 = readParcelable == null ? A01 : readParcelable;
    }

    public AbstractC015707l(Parcelable parcelable) {
        if (parcelable != null) {
            this.A00 = parcelable == A01 ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A00, i);
    }
}
