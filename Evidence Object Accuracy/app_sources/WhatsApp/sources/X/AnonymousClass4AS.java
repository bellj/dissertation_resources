package X;

/* renamed from: X.4AS  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4AS {
    ReadObject,
    ObjectReadName,
    ObjectReadValue,
    /* Fake field, exist only in values array */
    ReadArray,
    ArrayReadValue
}
