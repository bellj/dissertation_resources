package X;

import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.5bC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118055bC extends AnonymousClass015 {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final AnonymousClass016 A03 = C12980iv.A0T();
    public final C14830m7 A04;
    public final AnonymousClass60T A05;
    public final AnonymousClass61E A06;
    public final AnonymousClass605 A07;
    public final C130015yf A08;

    public AbstractC118055bC(C14830m7 r2, AnonymousClass60T r3, AnonymousClass61E r4, AnonymousClass605 r5, C130015yf r6) {
        this.A04 = r2;
        this.A07 = r5;
        this.A08 = r6;
        this.A06 = r4;
        this.A05 = r3;
    }

    public void A04(ActivityC13790kL r13, FingerprintBottomSheet fingerprintBottomSheet, C129155xG r15, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, String str, String str2, String str3) {
        fingerprintBottomSheet.A05 = new C119455e0(r13, fingerprintBottomSheet, this.A04, r15, new C133746By(r13, fingerprintBottomSheet, pinBottomSheetDialogFragment, this, str, str2, str3), this.A08);
        r13.Adm(fingerprintBottomSheet);
    }

    public void A05(ActivityC13790kL r19, FingerprintBottomSheet fingerprintBottomSheet, C129155xG r21, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, String str, String str2, String str3) {
        if (Build.VERSION.SDK_INT >= 23) {
            AnonymousClass61E r1 = this.A06;
            if (r1.A07() && r1.A02() == 1) {
                A04(r19, fingerprintBottomSheet, r21, pinBottomSheetDialogFragment, str, str2, str3);
                return;
            }
        }
        pinBottomSheetDialogFragment.A0B = new AnonymousClass6CH(r19, pinBottomSheetDialogFragment, this, str2, str3, str, R.string.register_wait_message);
        r19.Adm(pinBottomSheetDialogFragment);
    }

    public boolean A06(C452120p r8, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, String str) {
        int i = r8.A00;
        if (i != 1440 && i != 444 && i != 478 && i != 1441 && i != 445 && i != 1448 && i != 10718) {
            return false;
        }
        if (pinBottomSheetDialogFragment != null) {
            pinBottomSheetDialogFragment.A1M();
        }
        int i2 = r8.A00;
        if (i2 != 1440) {
            if (i2 != 1441) {
                if (i2 == 1448) {
                    this.A05.A02(r8, str, "PIN");
                } else if (i2 == 478 || i2 == 444) {
                    this.A05.A01.A01(str, "PIN");
                }
                if (pinBottomSheetDialogFragment != null) {
                    pinBottomSheetDialogFragment.A1C();
                }
                this.A03.A0A(r8);
                return true;
            }
            C130015yf r0 = this.A08;
            long j = r8.A02;
            r0.A02(j);
            if (pinBottomSheetDialogFragment == null) {
                return true;
            }
            pinBottomSheetDialogFragment.A1P(j * 1000, true);
            return true;
        } else if (pinBottomSheetDialogFragment == null) {
            return true;
        } else {
            pinBottomSheetDialogFragment.A1O(r8.A01);
            return true;
        }
    }
}
