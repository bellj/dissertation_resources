package X;

import android.database.Cursor;
import android.database.SQLException;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.0tm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19230tm extends AbstractC18500sY {
    public final C18460sU A00;

    public C19230tm(C18460sU r3, C18480sW r4) {
        super(r4, "migration_jid_store", Integer.MIN_VALUE);
        AnonymousClass009.A05(r3);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public long A06() {
        return super.A06();
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        return new AnonymousClass2Ez(0, 0);
    }

    @Override // X.AbstractC18500sY
    public boolean A0V(C29001Pw r14) {
        C16310on A02;
        AnonymousClass1Lx A00;
        this.A01.AaV("jid-store-migration-pending", null, false);
        C18460sU r9 = this.A00;
        C16490p7 r6 = r9.A01;
        r6.A04();
        File file = r6.A07;
        long length = file.length();
        StringBuilder sb = new StringBuilder("JidStore/populateJidTable/start/db size=");
        sb.append(length);
        Log.i(sb.toString());
        C28181Ma r7 = new C28181Ma("JidStore/populate");
        try {
            A02 = r6.A02();
            A00 = A02.A00();
        } catch (SQLException e) {
            Log.e("JidStore/populateJidTable/Error populating jid table", e);
            r9.A00.AaV("JidStore/populateJidTable", "JidStore/populateJidTable/error", true);
        }
        try {
            C16330op r10 = A02.A03;
            Cursor A09 = r10.A09("SELECT DISTINCT key_remote_jid FROM chat_list", new String[0]);
            r9.A0A(A09);
            if (A09 != null) {
                A09.close();
            }
            Cursor A092 = r10.A09("SELECT DISTINCT key_remote_jid FROM messages", new String[0]);
            r9.A0A(A092);
            if (A092 != null) {
                A092.close();
            }
            Cursor A093 = r10.A09("SELECT DISTINCT remote_resource FROM messages", new String[0]);
            r9.A0A(A093);
            if (A093 != null) {
                A093.close();
            }
            Cursor A094 = r10.A09("SELECT DISTINCT remote_resource FROM messages WHERE needs_push = 2", new String[0]);
            r9.A0A(A094);
            if (A094 != null) {
                A094.close();
            }
            Cursor A095 = r10.A09("SELECT DISTINCT mentioned_jids FROM messages WHERE mentioned_jids IS NOT NULL", new String[0]);
            r9.A0A(A095);
            if (A095 != null) {
                A095.close();
            }
            Cursor A096 = r10.A09("SELECT DISTINCT jid FROM group_participants", new String[0]);
            r9.A0A(A096);
            if (A096 != null) {
                A096.close();
            }
            Cursor A097 = r10.A09("SELECT DISTINCT jid FROM group_participants_history", new String[0]);
            r9.A0A(A097);
            if (A097 != null) {
                A097.close();
            }
            r9.A02.A04("jid_ready", 1);
            A00.A00();
            A00.close();
            A02.close();
            StringBuilder sb2 = new StringBuilder("JidStore/populateJidTable/time spent=");
            sb2.append(r7.A01());
            sb2.append("; count=");
            sb2.append(r9.A03.size());
            Log.i(sb2.toString());
            r6.A04();
            long length2 = file.length();
            StringBuilder sb3 = new StringBuilder("JidStore/populateJidTable/end/db size=");
            sb3.append(length2);
            sb3.append("; increase=");
            sb3.append(((double) length2) / ((double) length));
            Log.i(sb3.toString());
            return r9.A0C();
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
