package X;

import java.util.concurrent.Callable;

/* renamed from: X.6KU  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KU implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KU(AnonymousClass661 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        int i;
        AnonymousClass661 r1 = this.A01;
        if (!r1.isConnected() || !r1.A0f) {
            i = 0;
        } else {
            AnonymousClass63B r0 = r1.A0O;
            i = this.A00;
            r0.A00(i);
        }
        return Integer.valueOf(i);
    }
}
