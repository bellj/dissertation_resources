package X;

/* renamed from: X.6BD  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6BD implements AbstractC16870pt {
    public final C16120oU A00;
    public final C17900ra A01;
    public final AnonymousClass17V A02;

    @Override // X.AbstractC16870pt
    public AnonymousClass2SP A8H() {
        return null;
    }

    @Override // X.AbstractC16870pt
    public void AKa(C452120p r1, int i) {
    }

    @Override // X.AbstractC16870pt
    public void AKf(AnonymousClass2SP r1) {
    }

    @Override // X.AbstractC16870pt
    public void AKh(Integer num, Integer num2, String str, String str2, boolean z) {
    }

    @Override // X.AbstractC16870pt
    public void AeG() {
    }

    public AnonymousClass6BD(C16120oU r1, C17900ra r2, AnonymousClass17V r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public final AnonymousClass2SP A00(Integer num, Integer num2, String str, String str2) {
        AnonymousClass2SP r2 = new AnonymousClass2SP();
        C17900ra r1 = this.A01;
        if (r1.A01() != null) {
            r2.A0R = r1.A01().A03;
        }
        r2.A0Z = str;
        r2.A0U = this.A02.A00();
        r2.A09 = num;
        if (num2 != null) {
            r2.A08 = num2;
        }
        if (str2 != null) {
            r2.A0Y = str2;
        }
        return r2;
    }

    @Override // X.AbstractC16870pt
    public void AKg(Integer num, Integer num2, String str, String str2) {
        this.A00.A07(A00(num, num2, str, str2));
    }

    @Override // X.AbstractC16870pt
    public void AKi(AnonymousClass3FW r3, Integer num, Integer num2, String str, String str2) {
        AnonymousClass2SP A00 = A00(num, num2, str, str2);
        if (r3 != null) {
            A00.A0X = r3.toString();
        }
        this.A00.A07(A00);
    }

    @Override // X.AbstractC16870pt
    public void AKj(AnonymousClass3FW r2, Integer num, Integer num2, String str, String str2, String str3, String str4, boolean z, boolean z2) {
        AKi(r2, num, num2, "payment_transaction_details", str2);
    }

    @Override // X.AbstractC16870pt
    public void reset() {
        C117305Zk.A1L(this.A02);
    }
}
