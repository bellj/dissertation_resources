package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.5fC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119845fC extends AbstractC18830t7 {
    public final AnonymousClass018 A00;

    @Override // X.AbstractC18830t7
    public boolean A09(String str, byte[] bArr) {
        return true;
    }

    public C119845fC(C18790t3 r8, C16590pI r9, AnonymousClass018 r10, C18810t5 r11, C18800t4 r12, AbstractC14440lR r13) {
        super(r8, r9, r11, r12, r13, 14);
        this.A00 = r10;
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ String A01(Object obj) {
        return null;
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ void A04(Object obj, String str) {
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ boolean A07(InputStream inputStream, Object obj) {
        File A00 = A00("currency_metadata.json");
        if (A00 != null) {
            C14350lI.A0M(A00);
        }
        File A002 = A00("");
        if (A002 == null) {
            Log.e("PaymentCurrencyMetadataAssetManager/storeAssets/ Could not prepare resource directory");
            return false;
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(A002.getAbsolutePath(), "currency_metadata.json"));
            C14350lI.A0G(inputStream, fileOutputStream);
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            Log.e("PaymentCurrencyMetadataAssetManager/store/Failed!", e);
            return false;
        }
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ boolean A08(Object obj) {
        return !A06(A00("currency_metadata.json"));
    }

    public void A0A(AnonymousClass2DH r4) {
        if (!(!A06(A00("currency_metadata.json")))) {
            super.A03(r4, null, null, C39461pw.A00("currencies", this.A00.A06(), null, null));
        }
    }
}
