package X;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.VideoPort;
import java.util.HashSet;
import java.util.Map;

/* renamed from: X.2hx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC55202hx extends AnonymousClass03U {
    public int A00;
    public int A01;
    public AnimatorSet A02;
    public AnonymousClass02B A03;
    public CallGridViewModel A04;
    public C64363Fg A05;
    public boolean A06;
    public final C89374Js A07;
    public final AnonymousClass3WH A08;
    public final C15610nY A09;

    public abstract void A0G(C64363Fg v);

    public AbstractC55202hx(View view, C18720su r7, C89374Js r8, CallGridViewModel callGridViewModel, AnonymousClass130 r10, C15610nY r11) {
        super(view);
        AnonymousClass1O4 r1;
        this.A09 = r11;
        this.A07 = r8;
        this.A04 = callGridViewModel;
        synchronized (r7.A0C) {
            r1 = r7.A02;
            if (r1 == null) {
                r1 = new C41641tr(r7, (int) (AnonymousClass01V.A00 / 8192));
                r7.A02 = r1;
            }
        }
        this.A08 = new AnonymousClass3WH(r1, r10);
    }

    @Override // X.AnonymousClass03U
    public boolean A07() {
        return C12960it.A1W(this.A05);
    }

    public void A08() {
        if (!(this instanceof C60142w0)) {
            C60112vx r3 = (C60112vx) this;
            C64363Fg r0 = ((AbstractC55202hx) r3).A05;
            if (r0 != null) {
                r3.A0E(r3.A0G, r0.A0R, true, true);
                return;
            }
            return;
        }
        C60142w0 r2 = (C60142w0) this;
        C64363Fg r1 = ((AbstractC55202hx) r2).A05;
        if (r1 != null && r2.A0G != null) {
            r2.A0L(r1, true);
        }
    }

    public void A09() {
        C64363Fg r0;
        AnonymousClass02B r4;
        AnonymousClass02B r42;
        if (!(this instanceof C60142w0)) {
            C60112vx r6 = (C60112vx) this;
            if (r6.A07()) {
                CallGridViewModel callGridViewModel = ((AbstractC55202hx) r6).A04;
                if (!(callGridViewModel == null || (r42 = ((AbstractC55202hx) r6).A03) == null)) {
                    AnonymousClass3CZ r1 = callGridViewModel.A0E;
                    UserJid userJid = ((AbstractC55202hx) r6).A05.A0S;
                    Map map = r1.A01;
                    if (map.containsKey(userJid)) {
                        Map map2 = r1.A00;
                        if (r42.equals(map2.get(userJid))) {
                            map2.remove(userJid);
                            map.remove(userJid);
                        }
                    }
                    ((AbstractC55202hx) r6).A03 = null;
                }
                r6.A0I(false);
                VideoPort videoPort = r6.A06;
                if (videoPort != null) {
                    videoPort.setListener(null);
                }
                ((AbstractC55202hx) r6).A05 = null;
                View view = ((AnonymousClass03U) r6).A0H;
                view.setOnClickListener(null);
                view.setOnLongClickListener(null);
                View view2 = r6.A0B;
                if (view2 instanceof SurfaceView) {
                    view2.setVisibility(8);
                    return;
                }
                return;
            }
            return;
        }
        C60142w0 r62 = (C60142w0) this;
        if (r62 instanceof C60122vy) {
            C60122vy r63 = (C60122vy) r62;
            ((AbstractC55202hx) r63).A05 = null;
            CallGridViewModel callGridViewModel2 = ((AbstractC55202hx) r63).A04;
            if (callGridViewModel2 != null) {
                callGridViewModel2.A05.A09(r63.A01);
            }
        } else if (r62.A07()) {
            CallGridViewModel callGridViewModel3 = ((AbstractC55202hx) r62).A04;
            if (!(callGridViewModel3 == null || (r0 = ((AbstractC55202hx) r62).A05) == null || (r4 = ((AbstractC55202hx) r62).A03) == null)) {
                AnonymousClass3CZ r12 = callGridViewModel3.A0E;
                UserJid userJid2 = r0.A0S;
                Map map3 = r12.A01;
                if (map3.containsKey(userJid2)) {
                    Map map4 = r12.A00;
                    if (r4.equals(map4.get(userJid2))) {
                        map4.remove(userJid2);
                        map3.remove(userJid2);
                    }
                }
                ((AbstractC55202hx) r62).A03 = null;
            }
            ((AbstractC55202hx) r62).A05 = null;
            r62.A0H();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(int r6) {
        /*
            r5 = this;
            boolean r0 = r5 instanceof X.C60142w0
            if (r0 == 0) goto L_0x0065
            r4 = r5
            X.2w0 r4 = (X.C60142w0) r4
            boolean r0 = r4 instanceof X.C60132vz
            if (r0 != 0) goto L_0x0065
            int r0 = r4.A01
            if (r6 == r0) goto L_0x0037
            r4.A01 = r6
            r0 = 2
            r3 = 1050253722(0x3e99999a, float:0.3)
            if (r6 == r0) goto L_0x0070
            r3 = 1051260355(0x3ea8f5c3, float:0.33)
            if (r6 == r0) goto L_0x0070
            int r2 = r4.A05
        L_0x001e:
            com.whatsapp.components.button.ThumbnailButton r1 = r4.A0I
            android.view.ViewGroup$LayoutParams r0 = r1.getLayoutParams()
            X.064 r0 = (X.AnonymousClass064) r0
            r0.A04 = r3
            r0.A0X = r2
            r1.setLayoutParams(r0)
            r0 = 0
            com.whatsapp.calling.callgrid.view.VoiceParticipantAudioWave r2 = r4.A0H
            if (r2 == 0) goto L_0x0037
            float r1 = (float) r0
            r0 = 1
            r2.A02(r1, r0)
        L_0x0037:
            android.widget.TextView r2 = r4.A0B
            if (r2 == 0) goto L_0x0065
            int r1 = r4.A01
            if (r1 == 0) goto L_0x0065
            int r3 = r4.A08
            r0 = 1
            if (r1 == r0) goto L_0x0047
            r0 = 3
            if (r1 != r0) goto L_0x0060
        L_0x0047:
            android.view.View r0 = r4.A0H
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            int r1 = r0.height
            int r0 = r4.A04
            int r1 = r1 - r0
            if (r1 <= 0) goto L_0x0066
            float r1 = (float) r1
            r0 = 1042536202(0x3e23d70a, float:0.16)
            float r1 = r1 * r0
            int r1 = (int) r1
            int r0 = r4.A06
            int r3 = java.lang.Math.min(r1, r0)
        L_0x0060:
            r1 = 0
            float r0 = (float) r3
            r2.setTextSize(r1, r0)
        L_0x0065:
            return
        L_0x0066:
            java.lang.String r0 = "VoiceParticipantViewHolder/updateParticipantNameTextSize, gridHeight <= 0: "
            java.lang.String r0 = X.C12960it.A0W(r1, r0)
            com.whatsapp.util.Log.i(r0)
            goto L_0x0060
        L_0x0070:
            int r2 = r4.A07
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC55202hx.A0A(int):void");
    }

    public void A0B(int i) {
        if (!(this instanceof C60142w0)) {
            C60112vx r2 = (C60112vx) this;
            ((AnonymousClass03U) r2).A0H.setVisibility(i);
            View view = r2.A0B;
            if (view instanceof SurfaceView) {
                if (!r2.A07()) {
                    i = 8;
                }
                view.setVisibility(i);
                return;
            }
            return;
        }
        this.A0H.setVisibility(i);
    }

    public void A0C(MotionEvent motionEvent, View view) {
        if (view == null) {
            return;
        }
        if (motionEvent.getAction() == 0) {
            A0D(view, true);
        } else if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
            A0D(view, false);
        }
    }

    public final void A0D(View view, boolean z) {
        AnimatorSet animatorSet = this.A02;
        if (animatorSet != null && animatorSet.isRunning()) {
            this.A02.cancel();
        }
        View view2 = this.A0H;
        PropertyValuesHolder[] propertyValuesHolderArr = new PropertyValuesHolder[2];
        float[] fArr = new float[1];
        float f = 0.95f;
        float f2 = 1.0f;
        float f3 = 1.0f;
        if (z) {
            f3 = 0.95f;
        }
        fArr[0] = f3;
        propertyValuesHolderArr[0] = PropertyValuesHolder.ofFloat("scaleX", fArr);
        float[] fArr2 = new float[1];
        if (!z) {
            f = 1.0f;
        }
        fArr2[0] = f;
        propertyValuesHolderArr[1] = PropertyValuesHolder.ofFloat("scaleY", fArr2);
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(view2, propertyValuesHolderArr);
        PropertyValuesHolder[] propertyValuesHolderArr2 = new PropertyValuesHolder[1];
        float[] fArr3 = new float[1];
        if (!z) {
            f2 = 0.0f;
        }
        fArr3[0] = f2;
        propertyValuesHolderArr2[0] = PropertyValuesHolder.ofFloat("alpha", fArr3);
        ObjectAnimator ofPropertyValuesHolder2 = ObjectAnimator.ofPropertyValuesHolder(view, propertyValuesHolderArr2);
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether(ofPropertyValuesHolder, ofPropertyValuesHolder2);
        animatorSet2.setDuration((long) 200);
        animatorSet2.setInterpolator(new AccelerateDecelerateInterpolator());
        this.A02 = animatorSet2;
        animatorSet2.start();
    }

    public void A0E(ImageView imageView, C15370n3 r6, boolean z, boolean z2) {
        AnonymousClass1J1 r3;
        C89374Js r0 = this.A07;
        if (r0 != null && (r3 = (AnonymousClass1J1) C12990iw.A0l(r0.A00, z ? 1 : 0)) != null) {
            AnonymousClass3WH r2 = this.A08;
            r2.A00 = z2;
            HashSet hashSet = r2.A03;
            Integer valueOf = Integer.valueOf(imageView.hashCode());
            if (z) {
                hashSet.add(valueOf);
            } else {
                hashSet.remove(valueOf);
            }
            r3.A02(imageView, r2, r6, false);
        }
    }

    public void A0F(AnonymousClass3CK r2) {
        if (!(this instanceof C60142w0)) {
            ((C60112vx) this).A03 = r2;
        } else {
            ((C60142w0) this).A03 = r2;
        }
    }
}
