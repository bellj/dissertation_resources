package X;

/* renamed from: X.4YU  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4YU {
    public int A00;
    public int A01 = 0;
    public final CharSequence A02;

    public AnonymousClass4YU(CharSequence charSequence) {
        this.A02 = charSequence;
        this.A00 = charSequence.length() - 1;
    }

    public char A00(int i) {
        do {
            i++;
            if (!A0A(i)) {
                break;
            }
        } while (this.A02.charAt(i) == ' ');
        if (!(!A0A(i))) {
            return this.A02.charAt(i);
        }
        return ' ';
    }

    public int A01(char c, char c2, int i, boolean z) {
        CharSequence charSequence = this.A02;
        if (charSequence.charAt(i) == c) {
            int i2 = 1;
            int i3 = i + 1;
            while (A0A(i3)) {
                char charAt = charSequence.charAt(i3);
                if (charAt == '\'' || charAt == '\"') {
                    int A03 = A03(charAt, i3);
                    if (A03 != -1) {
                        i3 = A03 + 1;
                    } else {
                        StringBuilder A0k = C12960it.A0k("Could not find matching close quote for ");
                        A0k.append(charAt);
                        throw C82843wH.A00(C12960it.A0Z(charSequence, " when parsing : ", A0k));
                    }
                }
                if (z && charSequence.charAt(i3) == '/') {
                    int A032 = A03('/', i3);
                    if (A032 != -1) {
                        i3 = A032 + 1;
                    } else {
                        throw C82843wH.A00(C12960it.A0b("Could not find matching close for / when parsing regex in : ", charSequence));
                    }
                }
                if (charSequence.charAt(i3) == c) {
                    i2++;
                }
                if (charSequence.charAt(i3) == c2 && i2 - 1 == 0) {
                    return i3;
                }
                i3++;
            }
            return -1;
        }
        StringBuilder A0k2 = C12960it.A0k("Expected ");
        A0k2.append(c);
        A0k2.append(" but found ");
        throw C82843wH.A00(C72463ee.A0H(A0k2, charSequence.charAt(i)));
    }

    public int A02(char c, int i) {
        do {
            i++;
            if (!A0A(i)) {
                break;
            }
        } while (this.A02.charAt(i) == ' ');
        if (this.A02.charAt(i) != c) {
            return -1;
        }
        return i;
    }

    public int A03(char c, int i) {
        boolean z = false;
        for (int i2 = i + 1; !(!A0A(i2)); i2++) {
            if (z) {
                z = false;
            } else {
                CharSequence charSequence = this.A02;
                if ('\\' == charSequence.charAt(i2)) {
                    z = true;
                } else if (c == charSequence.charAt(i2)) {
                    return i2;
                }
            }
        }
        return -1;
    }

    public void A04() {
        while (true) {
            int i = this.A01;
            if (A0A(i) && i < this.A00 && this.A02.charAt(i) == ' ') {
                A07(1);
            } else {
                return;
            }
        }
    }

    public void A05() {
        int i;
        A04();
        while (true) {
            int i2 = this.A01;
            if (A0A(i2) && i2 < (i = this.A00) && this.A02.charAt(i) == ' ') {
                this.A00--;
            } else {
                return;
            }
        }
    }

    public void A06(char c) {
        A04();
        if (this.A02.charAt(this.A01) == c) {
            A07(1);
            return;
        }
        throw C82843wH.A00(String.format("Expected character: %c", Character.valueOf(c)));
    }

    public void A07(int i) {
        this.A01 += i;
    }

    public boolean A08(char c) {
        return C12960it.A1V(this.A02.charAt(this.A01), c);
    }

    public boolean A09(char c, int i) {
        int i2 = i + 1;
        while (!(!A0A(i2)) && this.A02.charAt(i2) == ' ') {
            i2++;
        }
        if ((!A0A(i2)) || this.A02.charAt(i2) != c) {
            return false;
        }
        return true;
    }

    public boolean A0A(int i) {
        return i >= 0 && i <= this.A00;
    }

    public boolean A0B(CharSequence charSequence) {
        A04();
        if (A0A((this.A01 + charSequence.length()) - 1)) {
            int i = this.A01;
            if (this.A02.subSequence(i, charSequence.length() + i).equals(charSequence)) {
                A07(charSequence.length());
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return this.A02.toString();
    }
}
