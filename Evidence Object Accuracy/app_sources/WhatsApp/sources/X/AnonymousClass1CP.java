package X;

import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import com.whatsapp.voipcalling.VideoPort;

/* renamed from: X.1CP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CP {
    public C14820m6 A00;
    public C14850m9 A01;

    public AnonymousClass1CP(C14820m6 r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public VideoPort A00(View view) {
        boolean A0N = AnonymousClass1SF.A0N(this.A00, this.A01);
        if (view instanceof SurfaceView) {
            return new C849340j((SurfaceView) view, A0N);
        }
        if (view instanceof TextureView) {
            return new C849240i((TextureView) view, A0N, A0N);
        }
        throw new IllegalArgumentException("createVideoPort must be called with either SurfaceView or TextureView");
    }
}
