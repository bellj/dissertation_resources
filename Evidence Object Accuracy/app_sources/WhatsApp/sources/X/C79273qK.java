package X;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: X.3qK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79273qK extends AbstractC79223qF {
    public final ByteBuffer A00;
    public final ByteBuffer A01;

    public C79273qK(ByteBuffer byteBuffer) {
        this.A00 = byteBuffer;
        this.A01 = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
    }
}
