package X;

import java.lang.reflect.Method;

/* renamed from: X.0P7  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0P7 {
    public final int A00;
    public final Method A01;

    public AnonymousClass0P7(Method method, int i) {
        this.A00 = i;
        this.A01 = method;
        method.setAccessible(true);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass0P7)) {
            return false;
        }
        AnonymousClass0P7 r4 = (AnonymousClass0P7) obj;
        if (this.A00 != r4.A00 || !this.A01.getName().equals(r4.A01.getName())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.A00 * 31) + this.A01.getName().hashCode();
    }
}
