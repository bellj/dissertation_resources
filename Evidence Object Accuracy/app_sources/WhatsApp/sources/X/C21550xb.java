package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.0xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21550xb {
    public final C14830m7 A00;
    public final C21500xW A01;
    public final C17690rE A02;
    public final C21540xa A03;
    public final C21530xZ A04;
    public final C21520xY A05;
    public final C21470xT A06;
    public final C16120oU A07;
    public final C21510xX A08;

    public C21550xb(C14830m7 r1, C21500xW r2, C17690rE r3, C21540xa r4, C21530xZ r5, C21520xY r6, C21470xT r7, C16120oU r8, C21510xX r9) {
        this.A00 = r1;
        this.A07 = r8;
        this.A06 = r7;
        this.A01 = r2;
        this.A08 = r9;
        this.A05 = r6;
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = r4;
    }

    public void A00(UserJid userJid) {
        if (this.A05.A00() && !this.A01.A00.A07(823)) {
            C17690rE r3 = this.A02;
            AbstractC455722e A00 = r3.A00(userJid);
            if (A00.A00 == 1) {
                r3.A02(A00);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        if (r0 != 1) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
        if (r6.A02.equals(r9.A05) != false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(com.whatsapp.jid.UserJid r8, X.AnonymousClass22F r9) {
        /*
            r7 = this;
            X.0rE r0 = r7.A02
            X.22e r6 = r0.A00(r8)
            r5 = 0
            r3 = 1
            r4 = 0
            if (r9 == 0) goto L_0x000c
            r4 = 1
        L_0x000c:
            int r0 = r6.A00
            if (r0 != r3) goto L_0x0011
            r5 = 1
        L_0x0011:
            if (r4 != 0) goto L_0x0015
            if (r5 == 0) goto L_0x003a
        L_0x0015:
            r2 = 0
            if (r9 != 0) goto L_0x003b
            if (r0 == r3) goto L_0x001b
        L_0x001a:
            r2 = 1
        L_0x001b:
            X.43U r1 = new X.43U
            r1.<init>()
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)
            r1.A01 = r0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)
            r1.A02 = r0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            r1.A00 = r0
            X.0oU r0 = r7.A07
            r0.A07(r1)
            r0.A0F(r3)
        L_0x003a:
            return
        L_0x003b:
            if (r0 != r3) goto L_0x001b
            X.22g r6 = (X.C455922g) r6
            java.lang.String r1 = r6.A01
            java.lang.String r0 = r9.A04
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001b
            java.lang.String r1 = r6.A02
            java.lang.String r0 = r9.A05
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x001b
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21550xb.A01(com.whatsapp.jid.UserJid, X.22F):void");
    }
}
