package X;

import android.content.Context;
import com.whatsapp.inappsupport.ui.ContactUsActivity;

/* renamed from: X.4qG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103134qG implements AbstractC009204q {
    public final /* synthetic */ ContactUsActivity A00;

    public C103134qG(ContactUsActivity contactUsActivity) {
        this.A00 = contactUsActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
