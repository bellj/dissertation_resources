package X;

import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.location.LiveLocationPrivacyActivity;

/* renamed from: X.1jd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36281jd implements AnonymousClass13Q {
    public final /* synthetic */ LiveLocationPrivacyActivity A00;

    public C36281jd(LiveLocationPrivacyActivity liveLocationPrivacyActivity) {
        this.A00 = liveLocationPrivacyActivity;
    }

    @Override // X.AnonymousClass13Q
    public void AWN(AbstractC14640lm r4) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(this, 34));
    }

    @Override // X.AnonymousClass13Q
    public void AWr(AbstractC14640lm r4) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(this, 35));
    }
}
