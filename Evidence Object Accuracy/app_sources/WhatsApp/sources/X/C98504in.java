package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4in  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98504in implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C29461Su(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C29461Su[i];
    }
}
