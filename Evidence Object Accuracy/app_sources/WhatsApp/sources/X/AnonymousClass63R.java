package X;

import android.media.MediaRecorder;

/* renamed from: X.63R  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63R implements MediaRecorder.OnInfoListener {
    public final /* synthetic */ AnonymousClass66O A00;

    public AnonymousClass63R(AnonymousClass66O r1) {
        this.A00 = r1;
    }

    @Override // android.media.MediaRecorder.OnInfoListener
    public void onInfo(MediaRecorder mediaRecorder, int i, int i2) {
        this.A00.A03.AST(mediaRecorder, i, i2, false);
    }
}
