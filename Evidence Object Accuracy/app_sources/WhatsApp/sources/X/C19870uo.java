package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.0uo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19870uo {
    public final C14850m9 A00;
    public final C17220qS A01;

    public C19870uo(C14850m9 r1, C17220qS r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static final boolean A00(AbstractC21730xt r6, String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1V8("error", new AnonymousClass1W9[]{new AnonymousClass1W9("code", 451), new AnonymousClass1W9("text", "commerce-features-disabled")}));
        r6.APv(new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("name", "IQErrorResponse")}, (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0])), str);
        return false;
    }

    public void A01(AbstractC21730xt r8, AnonymousClass1V8 r9, String str, int i) {
        try {
            if (this.A00.A07(1319)) {
                A00(r8, str);
            } else {
                this.A01.A09(r8, r9, str, i, 32000);
            }
        } catch (AnonymousClass1V9 e) {
            Log.e(e.getMessage());
        }
    }

    public void A02(AbstractC21730xt r8, AnonymousClass1V8 r9, String str, int i) {
        try {
            if (this.A00.A07(1319)) {
                A00(r8, str);
            } else {
                this.A01.A0A(r8, r9, str, i, 32000);
            }
        } catch (AnonymousClass1V9 e) {
            Log.e(e.getMessage());
        }
    }
}
