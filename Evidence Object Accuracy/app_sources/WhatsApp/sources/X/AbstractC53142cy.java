package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.ListItemWithRightIcon;

/* renamed from: X.2cy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC53142cy extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC53142cy(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v4, types: [X.2on] */
    public void A00() {
        AnonymousClass01J A00;
        ListItemWithRightIcon listItemWithRightIcon;
        if (this instanceof ListItemWithRightIcon) {
            ListItemWithRightIcon listItemWithRightIcon2 = (ListItemWithRightIcon) this;
            if (!listItemWithRightIcon2.A00) {
                listItemWithRightIcon2.A00 = true;
                A00 = AnonymousClass2P6.A00(listItemWithRightIcon2.generatedComponent());
                listItemWithRightIcon = listItemWithRightIcon2;
            } else {
                return;
            }
        } else if (!this.A01) {
            this.A01 = true;
            A00 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            listItemWithRightIcon = (AbstractC58392on) this;
        } else {
            return;
        }
        listItemWithRightIcon.A04 = C12960it.A0R(A00);
        listItemWithRightIcon.A03 = C12960it.A0Q(A00);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
