package X;

/* renamed from: X.0c3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08910c3 implements Cloneable {
    public float A00;
    public AnonymousClass0JP A01;

    public C08910c3(float f) {
        this.A00 = f;
        this.A01 = AnonymousClass0JP.px;
    }

    public C08910c3(AnonymousClass0JP r1, float f) {
        this.A00 = f;
        this.A01 = r1;
    }

    public float A00(float f) {
        float f2;
        float f3;
        int ordinal = this.A01.ordinal();
        float f4 = this.A00;
        switch (ordinal) {
            case 3:
                return f4 * f;
            case 4:
                f2 = f4 * f;
                f3 = 2.54f;
                break;
            case 5:
                f2 = f4 * f;
                f3 = 25.4f;
                break;
            case 6:
                f2 = f4 * f;
                f3 = 72.0f;
                break;
            case 7:
                f2 = f4 * f;
                f3 = 6.0f;
                break;
            default:
                return f4;
        }
        return f2 / f3;
    }

    public float A01(C06540Ua r6) {
        float sqrt;
        if (this.A01 != AnonymousClass0JP.percent) {
            return A02(r6);
        }
        AnonymousClass0S1 r1 = r6.A03;
        AnonymousClass0SG r0 = r1.A02;
        if (r0 == null && (r0 = r1.A03) == null) {
            return this.A00;
        }
        float f = r0.A03;
        float f2 = r0.A00;
        if (f == f2) {
            sqrt = this.A00 * f;
        } else {
            sqrt = this.A00 * ((float) (Math.sqrt((double) ((f * f) + (f2 * f2))) / 1.414213562373095d));
        }
        return sqrt / 100.0f;
    }

    public float A02(C06540Ua r4) {
        float f;
        float f2;
        float f3;
        float f4;
        switch (this.A01.ordinal()) {
            case 1:
                f4 = this.A00;
                f3 = r4.A03.A00.getTextSize();
                return f4 * f3;
            case 2:
                f4 = this.A00;
                f3 = r4.A03.A00.getTextSize() / 2.0f;
                return f4 * f3;
            case 3:
                f4 = this.A00;
                f3 = r4.A00;
                return f4 * f3;
            case 4:
                f = this.A00 * r4.A00;
                f2 = 2.54f;
                return f / f2;
            case 5:
                f = this.A00 * r4.A00;
                f2 = 25.4f;
                return f / f2;
            case 6:
                f = this.A00 * r4.A00;
                f2 = 72.0f;
                return f / f2;
            case 7:
                f = this.A00 * r4.A00;
                f2 = 6.0f;
                return f / f2;
            case 8:
                AnonymousClass0S1 r1 = r4.A03;
                AnonymousClass0SG r0 = r1.A02;
                if (!(r0 == null && (r0 = r1.A03) == null)) {
                    f = this.A00 * r0.A03;
                    f2 = 100.0f;
                    return f / f2;
                }
                return this.A00;
            default:
                return this.A00;
        }
    }

    public float A03(C06540Ua r3) {
        if (this.A01 != AnonymousClass0JP.percent) {
            return A02(r3);
        }
        AnonymousClass0S1 r1 = r3.A03;
        AnonymousClass0SG r0 = r1.A02;
        if (r0 == null && (r0 = r1.A03) == null) {
            return this.A00;
        }
        return (this.A00 * r0.A00) / 100.0f;
    }

    public float A04(C06540Ua r3, float f) {
        if (this.A01 == AnonymousClass0JP.percent) {
            return (this.A00 * f) / 100.0f;
        }
        return A02(r3);
    }

    public boolean A05() {
        return this.A00 < 0.0f;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(this.A00));
        sb.append(this.A01);
        return sb.toString();
    }
}
