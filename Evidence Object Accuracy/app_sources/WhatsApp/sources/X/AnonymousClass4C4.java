package X;

/* renamed from: X.4C4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4C4 extends Exception {
    public final int errorCode;
    public final C100614mC format;
    public final boolean isRecoverable;

    public AnonymousClass4C4(C100614mC r2, int i, boolean z) {
        super(C12960it.A0W(i, "AudioTrack write failed: "));
        this.isRecoverable = z;
        this.errorCode = i;
        this.format = r2;
    }
}
