package X;

import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;

/* renamed from: X.2Kh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49322Kh extends AbstractC16350or {
    public final C22260yn A00;
    public final C15550nR A01;
    public final C253318z A02;
    public final UserJid A03;
    public final C22410z2 A04;
    public final String A05;
    public final WeakReference A06;

    public /* synthetic */ C49322Kh(C22260yn r2, C15550nR r3, C253318z r4, UserJid userJid, C22410z2 r6, C48192Eu r7, String str) {
        this.A01 = r3;
        this.A00 = r2;
        this.A02 = r4;
        this.A04 = r6;
        this.A06 = new WeakReference(r7);
        this.A03 = userJid;
        this.A05 = str;
    }
}
