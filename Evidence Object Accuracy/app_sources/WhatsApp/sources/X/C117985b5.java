package X;

import android.text.TextUtils;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: X.5b5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117985b5 extends AnonymousClass015 {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public C27691It A01 = C13000ix.A03();
    public String A02;
    public final AbstractC001200n A03;
    public final C16590pI A04;
    public final C30861Zc A05;
    public final C18600si A06;
    public final C120525gK A07;
    public final C129145xF A08;
    public final C18590sh A09;

    public C117985b5(AbstractC001200n r2, C16590pI r3, C30861Zc r4, C18600si r5, C120525gK r6, C129145xF r7, C18590sh r8, String str) {
        this.A02 = str;
        this.A04 = r3;
        this.A09 = r8;
        this.A07 = r6;
        this.A05 = r4;
        this.A06 = r5;
        this.A03 = r2;
        this.A08 = r7;
    }

    public void A04(C127475uY r8) {
        C128145vd r1;
        int i = r8.A00;
        if (i != 0) {
            if (i == 1) {
                String str = r8.A01;
                C30861Zc r3 = this.A05;
                r1 = new C128145vd(0);
                r1.A05 = str;
                r1.A04 = r3.A0B;
                r1.A01 = (C119755f3) r3.A08;
                r1.A06 = (String) C117295Zj.A0R(r3.A09);
            } else if (i == 2) {
                HashMap hashMap = r8.A02;
                AnonymousClass016 r2 = this.A00;
                C127115ty.A00(this.A04.A00, r2, R.string.payments_action_verifying_identify);
                C30861Zc r4 = this.A05;
                C119755f3 r5 = (C119755f3) r4.A08;
                if (r5 == null) {
                    C127115ty.A01(r2);
                    r1 = new C128145vd(2);
                } else {
                    ArrayList A0l = C12960it.A0l();
                    C117295Zj.A1M("vpa", (String) AnonymousClass1ZS.A01(r5.A09), A0l);
                    if (!TextUtils.isEmpty(r5.A0F)) {
                        C117295Zj.A1M("vpa-id", r5.A0F, A0l);
                    }
                    C117295Zj.A1M("seq-no", this.A02, A0l);
                    C117295Zj.A1M("upi-bank-info", (String) C117295Zj.A0R(r5.A06), A0l);
                    C117295Zj.A1M("device-id", this.A09.A01(), A0l);
                    C117295Zj.A1M("credential-id", r4.A0A, A0l);
                    C117295Zj.A1M("mpin", C1308460e.A00("MPIN", hashMap), A0l);
                    AnonymousClass1V8 r32 = new AnonymousClass1V8("mpin", C117305Zk.A1b(A0l));
                    this.A08.A00(new C133496Az(this), this.A06.A02(), r32);
                    return;
                }
            } else {
                return;
            }
            this.A01.A0B(r1);
            return;
        }
        C127115ty.A00(this.A04.A00, this.A00, R.string.register_wait_message);
        this.A07.A00();
    }
}
