package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4xv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107774xv implements AbstractC116805Wy {
    public static final C107774xv A01 = new C107774xv();
    public final List A00;

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return 1;
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        return j < 0 ? 0 : -1;
    }

    public C107774xv() {
        this.A00 = Collections.emptyList();
    }

    public C107774xv(C93834ao r2) {
        this.A00 = Collections.singletonList(r2);
    }

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        return j >= 0 ? this.A00 : Collections.emptyList();
    }

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        C95314dV.A03(C12960it.A1T(i));
        return 0;
    }
}
