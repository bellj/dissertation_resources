package X;

import java.util.Arrays;

/* renamed from: X.5Lu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114505Lu extends C08960c8 {
    public final transient int[] A00;
    public final transient byte[][] A01;

    public /* synthetic */ C114505Lu(int[] iArr, byte[][] bArr) {
        super(C08960c8.A02.data);
        this.A01 = bArr;
        this.A00 = iArr;
    }

    @Override // X.C08960c8
    public byte A00(int i) {
        int i2;
        int[] iArr = this.A00;
        byte[][] bArr = this.A01;
        int length = bArr.length;
        AnonymousClass4F1.A00((long) iArr[length - 1], (long) i, 1);
        int binarySearch = Arrays.binarySearch(iArr, 0, length, i + 1);
        if (binarySearch < 0) {
            binarySearch ^= -1;
        }
        if (binarySearch == 0) {
            i2 = 0;
        } else {
            i2 = iArr[binarySearch - 1];
        }
        return bArr[binarySearch][(i - i2) + iArr[length + binarySearch]];
    }

    @Override // X.C08960c8
    public int A01() {
        return this.A00[this.A01.length - 1];
    }

    @Override // X.C08960c8
    public String A03() {
        return new C08960c8(A07()).A03();
    }

    @Override // X.C08960c8
    public boolean A04(C08960c8 r14, int i, int i2, int i3) {
        int i4;
        int i5 = 0;
        int i6 = 0;
        if (0 <= A01() - i3) {
            int i7 = i3 + 0;
            int[] iArr = this.A00;
            byte[][] bArr = this.A01;
            int length = bArr.length;
            int binarySearch = Arrays.binarySearch(iArr, 0, length, 1);
            if (binarySearch < 0) {
                binarySearch ^= -1;
            }
            while (i5 < i7) {
                if (binarySearch == 0) {
                    i4 = 0;
                } else {
                    i4 = iArr[binarySearch - 1];
                }
                int i8 = iArr[length + binarySearch];
                int min = Math.min(i7, (iArr[binarySearch] - i4) + i4) - i5;
                if (r14.A05(bArr[binarySearch], i6, i8 + (i5 - i4), min)) {
                    i6 += min;
                    i5 += min;
                    binarySearch++;
                }
            }
            return true;
        }
        return false;
    }

    @Override // X.C08960c8
    public boolean A05(byte[] bArr, int i, int i2, int i3) {
        int i4;
        int i5 = i;
        int i6 = i2;
        C16700pc.A0D(bArr, 1);
        if (i >= 0 && i5 <= A01() - i3 && i2 >= 0 && i6 <= bArr.length - i3) {
            int i7 = i3 + i;
            int[] iArr = this.A00;
            byte[][] bArr2 = this.A01;
            int length = bArr2.length;
            int binarySearch = Arrays.binarySearch(iArr, 0, length, i + 1);
            if (binarySearch < 0) {
                binarySearch ^= -1;
            }
            while (i5 < i7) {
                if (binarySearch == 0) {
                    i4 = 0;
                } else {
                    i4 = iArr[binarySearch - 1];
                }
                int i8 = iArr[length + binarySearch];
                int min = Math.min(i7, (iArr[binarySearch] - i4) + i4) - i5;
                int i9 = i8 + (i5 - i4);
                byte[] bArr3 = bArr2[binarySearch];
                C16700pc.A0D(bArr3, 0);
                for (int i10 = 0; i10 < min; i10++) {
                    if (bArr3[i10 + i9] == bArr[i10 + i6]) {
                    }
                }
                i6 += min;
                i5 += min;
                binarySearch++;
            }
            return true;
        }
        return false;
    }

    @Override // X.C08960c8
    public byte[] A06() {
        return A07();
    }

    public byte[] A07() {
        byte[] bArr = new byte[A01()];
        byte[][] bArr2 = this.A01;
        int length = bArr2.length;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            int[] iArr = this.A00;
            int i4 = iArr[length + i];
            int i5 = iArr[i];
            byte[] bArr3 = bArr2[i];
            int i6 = i5 - i2;
            C16700pc.A0D(bArr3, 0);
            System.arraycopy(bArr3, i4, bArr, i3, i6);
            i3 += i6;
            i++;
            i2 = i5;
        }
        return bArr;
    }

    @Override // X.C08960c8, java.lang.Object
    public boolean equals(Object obj) {
        C08960c8 r5;
        int A01;
        if (obj == this || ((obj instanceof C08960c8) && (r5 = (C08960c8) obj).A01() == (A01 = A01()) && A04(r5, 0, 0, A01))) {
            return true;
        }
        return false;
    }

    @Override // X.C08960c8, java.lang.Object
    public int hashCode() {
        int i = super.A00;
        if (i != 0) {
            return i;
        }
        byte[][] bArr = this.A01;
        int length = bArr.length;
        int i2 = 0;
        int i3 = 0;
        int i4 = 1;
        while (i2 < length) {
            int[] iArr = this.A00;
            int i5 = iArr[length + i2];
            int i6 = iArr[i2];
            byte[] bArr2 = bArr[i2];
            int i7 = (i6 - i3) + i5;
            while (i5 < i7) {
                i4 = (i4 * 31) + bArr2[i5];
                i5++;
            }
            i2++;
            i3 = i6;
        }
        super.A00 = i4;
        return i4;
    }

    @Override // X.C08960c8, java.lang.Object
    public String toString() {
        return new C08960c8(A07()).toString();
    }

    private final Object writeReplace() {
        return new C08960c8(A07());
    }
}
