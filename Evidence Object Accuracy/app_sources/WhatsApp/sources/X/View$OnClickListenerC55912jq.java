package X;

import android.os.SystemClock;
import android.view.View;
import com.facebook.redex.EmptyBaseViewOnClick0CListener;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;

/* renamed from: X.2jq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnClickListenerC55912jq extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public long A00;
    public AnonymousClass5W1 A01;
    public AbstractC92184Uw A02;
    public CallsHistoryFragment A03;
    public C14850m9 A04;

    public View$OnClickListenerC55912jq(AnonymousClass5W1 r1, AbstractC92184Uw r2, CallsHistoryFragment callsHistoryFragment, C14850m9 r4) {
        this.A03 = callsHistoryFragment;
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        View view2;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        CallsHistoryFragment callsHistoryFragment = this.A03;
        if (callsHistoryFragment.A01 != null) {
            AnonymousClass5W1 r1 = this.A01;
            if (r1.ADd() == 2) {
                callsHistoryFragment.A1G(((C1100854e) r1).A00, (C60172w8) this.A02);
                return;
            }
        }
        if (elapsedRealtime - this.A00 > 1000) {
            this.A00 = elapsedRealtime;
            AnonymousClass5W1 r2 = this.A01;
            AbstractC14640lm ADh = r2.ADh();
            int ADd = r2.ADd();
            AbstractC92184Uw r0 = this.A02;
            if (ADd == 2) {
                view2 = ((C60172w8) r0).A01.findViewById(R.id.contact_photo);
                AbstractC14640lm r02 = C12990iw.A0g(((C1100854e) r2).A00.A03, 0).A04;
                if (r02 != null) {
                    ADh = r02;
                    AnonymousClass3DG r12 = new AnonymousClass3DG(this.A04, ADh, C12990iw.A0j());
                    r12.A02 = AnonymousClass028.A0J(view2);
                    r12.A00(callsHistoryFragment.A0B(), view2);
                }
            } else {
                view2 = ((C60152w6) r0).A01.findViewById(R.id.contact_photo);
            }
            if (ADh == null) {
                return;
            }
            AnonymousClass3DG r12 = new AnonymousClass3DG(this.A04, ADh, C12990iw.A0j());
            r12.A02 = AnonymousClass028.A0J(view2);
            r12.A00(callsHistoryFragment.A0B(), view2);
        }
    }
}
