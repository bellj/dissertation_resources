package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98904jR implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78623pD[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        int i = 0;
        int i2 = 0;
        boolean z = true;
        boolean z2 = false;
        int i3 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 3:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 4:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 5:
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                case 6:
                    str3 = C95664e9.A08(parcel, readInt);
                    break;
                case 7:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case '\b':
                    str4 = C95664e9.A08(parcel, readInt);
                    break;
                case '\t':
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case '\n':
                    i3 = C95664e9.A02(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78623pD(str, str2, str3, str4, i, i2, i3, z, z2);
    }
}
