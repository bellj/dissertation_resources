package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.data.ProfilePhotoChange;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.0vC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20090vC {
    public final AbstractC15710nm A00;
    public final C15810nw A01;
    public final C14830m7 A02;
    public final C16510p9 A03;
    public final C18460sU A04;
    public final C16490p7 A05;
    public final C21390xL A06;
    public final C20930wY A07;
    public final C20320vZ A08;

    public C20090vC(AbstractC15710nm r1, C15810nw r2, C14830m7 r3, C16510p9 r4, C18460sU r5, C16490p7 r6, C21390xL r7, C20930wY r8, C20320vZ r9) {
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A08 = r9;
        this.A06 = r7;
        this.A07 = r8;
        this.A05 = r6;
    }

    public static AnonymousClass1IS A00(Cursor cursor, AbstractC14640lm r4) {
        int columnIndex = cursor.getColumnIndex("key_id");
        if (columnIndex < 0) {
            columnIndex = cursor.getColumnIndexOrThrow("key_id");
        }
        String string = cursor.getString(columnIndex);
        int columnIndex2 = cursor.getColumnIndex("from_me");
        if (columnIndex2 < 0) {
            columnIndex2 = cursor.getColumnIndexOrThrow("key_from_me");
        }
        boolean z = true;
        if (cursor.getInt(columnIndex2) != 1) {
            z = false;
        }
        if (string != null && !string.equals("-1")) {
            return new AnonymousClass1IS(r4, string, z);
        }
        StringBuilder sb = new StringBuilder("CachedMessageStore/getMessage/id is null or no messages for jid=");
        sb.append(r4);
        Log.w(sb.toString());
        return null;
    }

    public AbstractC15340mz A01(Cursor cursor) {
        String string = cursor.getString(cursor.getColumnIndexOrThrow("key_id"));
        boolean z = true;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("from_me")) != 1) {
            z = false;
        }
        AbstractC14640lm A05 = this.A03.A05(cursor.getLong(cursor.getColumnIndexOrThrow("chat_row_id")));
        if (A05 == null) {
            return null;
        }
        return A02(cursor, new AnonymousClass1IS(A05, string, z));
    }

    public AbstractC15340mz A02(Cursor cursor, AnonymousClass1IS r8) {
        if (cursor.getColumnIndex("table_version") != -1) {
            return A04(cursor, r8);
        }
        C16490p7 r5 = this.A05;
        C16310on A01 = r5.get();
        try {
            r5.A04();
            if (r5.A05.A06(A01.A03).booleanValue()) {
                this.A00.AaV("MainMessageStore/readMessage/read directly from old table instead of view.", null, true);
            }
            A01.close();
            A01 = r5.get();
            try {
                Cursor A09 = A01.A03.A09(AnonymousClass1Y7.A04, A0C(r8));
                if (A09.moveToNext()) {
                    AbstractC15340mz A04 = A04(A09, r8);
                    A09.close();
                    A01.close();
                    return A04;
                }
                A09.close();
                A01.close();
                return null;
            } finally {
            }
        } finally {
        }
    }

    public AbstractC15340mz A03(Cursor cursor, AnonymousClass1IS r10) {
        AbstractC15340mz A00;
        byte b = (byte) cursor.getInt(cursor.getColumnIndexOrThrow("message_type"));
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("timestamp"));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("status"));
        if (b != 0 ? b != 7 : i != 6) {
            A00 = this.A08.A01(r10, b, j);
        } else {
            C20930wY r1 = this.A07;
            int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("media_size"));
            if (i2 == 38) {
                A00 = r1.A06.A01(r10, (byte) 33, j);
            } else {
                A00 = C22140ya.A00(r1.A00, r10, null, i2, j);
            }
        }
        A00.A0c(cursor, this.A04);
        A00.A0b(cursor);
        return A00;
    }

    public final AbstractC15340mz A04(Cursor cursor, AnonymousClass1IS r24) {
        AbstractC15340mz r6;
        Cursor A09;
        Cursor A092;
        byte b = (byte) cursor.getInt(cursor.getColumnIndexOrThrow("message_type"));
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("timestamp"));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("status"));
        if (b != 0 ? b != 7 : i != 6) {
            r6 = this.A08.A01(r24, b, j);
        } else {
            C20930wY r3 = this.A07;
            if (!r3.A02()) {
                int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("media_size"));
                if (i2 == 38) {
                    r6 = r3.A06.A01(r24, (byte) 33, j);
                } else {
                    r6 = C22140ya.A00(r3.A00, r24, null, i2, j);
                }
            } else {
                long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                C16490p7 r4 = r3.A03;
                C16310on A01 = r4.get();
                try {
                    Cursor A093 = A01.A03.A09("SELECT action_type FROM message_system WHERE message_row_id = ?", new String[]{Long.toString(j2)});
                    if (!A093.moveToFirst()) {
                        r6 = null;
                    } else {
                        r6 = C22140ya.A00(r3.A00, r24, null, A093.getInt(A093.getColumnIndexOrThrow("action_type")), j);
                        r6.A0Z(1);
                        r6.A11 = j2;
                        C16310on A012 = r4.get();
                        if (r6 instanceof C30451Xl) {
                            A092 = A012.A03.A09("SELECT old_data FROM message_system_value_change WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                            try {
                                if (A092.moveToNext()) {
                                    ((C30451Xl) r6).A01 = A092.getString(A092.getColumnIndexOrThrow("old_data"));
                                }
                                A092.close();
                            } finally {
                            }
                        }
                        if (r6 instanceof C30481Xo) {
                            A09 = A012.A03.A09("SELECT old_data FROM message_system_value_change WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                            try {
                                if (A09.moveToNext()) {
                                    ((C30481Xo) r6).A00 = A09.getString(A09.getColumnIndexOrThrow("old_data"));
                                }
                                A09.close();
                            } finally {
                            }
                        }
                        if (r6 instanceof C30491Xp) {
                            Cursor A094 = A012.A03.A09("SELECT old_data FROM message_system_value_change WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                            try {
                                if (A094.moveToNext()) {
                                    ((C30491Xp) r6).A00 = A094.getString(A094.getColumnIndexOrThrow("old_data"));
                                }
                                A094.close();
                            } finally {
                                if (A094 != null) {
                                    try {
                                        A094.close();
                                    } catch (Throwable unused) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof C30461Xm) {
                            C16330op r9 = A012.A03;
                            Cursor A095 = r9.A09("SELECT is_me_joined FROM message_system_group WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                            try {
                                if (A095.moveToNext()) {
                                    ((C30461Xm) r6).A00 = A095.getInt(A095.getColumnIndexOrThrow("is_me_joined"));
                                }
                                A095.close();
                                A092 = r9.A09("SELECT user_jid_row_id FROM message_system_chat_participant WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                                try {
                                    ArrayList arrayList = new ArrayList();
                                    while (A092.moveToNext()) {
                                        UserJid of = UserJid.of(r3.A02.A03(A092.getLong(A092.getColumnIndexOrThrow("user_jid_row_id"))));
                                        if (of != null) {
                                            arrayList.add(of);
                                        }
                                    }
                                    if (!arrayList.isEmpty()) {
                                        r6.A0u(arrayList);
                                    }
                                    A092.close();
                                } finally {
                                    if (A092 != null) {
                                        try {
                                            A092.close();
                                        } catch (Throwable unused2) {
                                        }
                                    }
                                }
                            } finally {
                                if (A095 != null) {
                                    try {
                                        A095.close();
                                    } catch (Throwable unused3) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof C30501Xq) {
                            Cursor A096 = A012.A03.A09("SELECT new_photo_id, old_photo, new_photo FROM message_system_photo_change WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                            try {
                                if (A096.moveToNext()) {
                                    C30501Xq r10 = (C30501Xq) r6;
                                    r10.A16(A096.getString(A096.getColumnIndexOrThrow("new_photo_id")));
                                    ProfilePhotoChange profilePhotoChange = new ProfilePhotoChange();
                                    try {
                                        profilePhotoChange.newPhotoId = Integer.parseInt(A096.getString(A096.getColumnIndexOrThrow("new_photo_id")));
                                    } catch (NumberFormatException unused4) {
                                    }
                                    profilePhotoChange.newPhoto = A096.getBlob(A096.getColumnIndexOrThrow("new_photo"));
                                    profilePhotoChange.oldPhoto = A096.getBlob(A096.getColumnIndexOrThrow("old_photo"));
                                    r10.A00 = profilePhotoChange;
                                }
                                A096.close();
                            } finally {
                                if (A096 != null) {
                                    try {
                                        A096.close();
                                    } catch (Throwable unused5) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof C30511Xs) {
                            Cursor A097 = A012.A03.A09("SELECT old_jid_row_id, new_jid_row_id FROM message_system_number_change WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                            try {
                                if (A097.moveToNext()) {
                                    C30511Xs r102 = (C30511Xs) r6;
                                    long j3 = A097.getLong(A097.getColumnIndexOrThrow("old_jid_row_id"));
                                    C18460sU r7 = r3.A02;
                                    r102.A01 = UserJid.of(r7.A03(j3));
                                    r102.A00 = UserJid.of(r7.A03(A097.getLong(A097.getColumnIndexOrThrow("new_jid_row_id"))));
                                }
                                A097.close();
                            } finally {
                                if (A097 != null) {
                                    try {
                                        A097.close();
                                    } catch (Throwable unused6) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof C30541Xv) {
                            C16330op r72 = A012.A03;
                            A09 = r72.A09("SELECT sender_jid_row_id, receiver_jid_row_id, amount_with_symbol, remote_message_sender_jid_row_id, remote_message_from_me, remote_message_key FROM message_payment WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                            try {
                                if (A09.moveToNext()) {
                                    C30541Xv r103 = (C30541Xv) r6;
                                    C18460sU r92 = r3.A02;
                                    r103.A01 = (UserJid) r92.A07(UserJid.class, A09.getLong(A09.getColumnIndexOrThrow("sender_jid_row_id")));
                                    r103.A00 = (UserJid) r92.A07(UserJid.class, A09.getLong(A09.getColumnIndexOrThrow("receiver_jid_row_id")));
                                    r103.A03 = A09.getString(A09.getColumnIndexOrThrow("amount_with_symbol"));
                                    if (!A09.isNull(A09.getColumnIndexOrThrow("remote_message_from_me"))) {
                                        AbstractC14640lm r93 = (AbstractC14640lm) r92.A07(AbstractC14640lm.class, A09.getLong(A09.getColumnIndexOrThrow("remote_message_sender_jid_row_id")));
                                        boolean z = false;
                                        if (A09.getInt(A09.getColumnIndexOrThrow("remote_message_from_me")) == 1) {
                                            z = true;
                                        }
                                        r103.A02 = new AnonymousClass1IS(r93, A09.getString(A09.getColumnIndexOrThrow("remote_message_key")), z);
                                    }
                                }
                                A09.close();
                                if (r6 instanceof C30551Xw) {
                                    Cursor A098 = r72.A09("SELECT web_stub , amount , transfer_date , payment_sender_name , expiration FROM message_payment_transaction_reminder WHERE message_row_id = ?", new String[]{Long.toString(r6.A11)});
                                    try {
                                        if (A098.moveToNext()) {
                                            C30551Xw r8 = (C30551Xw) r6;
                                            r8.A02 = A098.getString(A098.getColumnIndexOrThrow("web_stub"));
                                            r8.A01 = A098.getString(A098.getColumnIndexOrThrow("amount"));
                                            r8.A04 = A098.getString(A098.getColumnIndexOrThrow("transfer_date"));
                                            r8.A03 = A098.getString(A098.getColumnIndexOrThrow("payment_sender_name"));
                                            r8.A00 = A098.getInt(A098.getColumnIndexOrThrow("expiration"));
                                        }
                                        A098.close();
                                    } finally {
                                        if (A098 != null) {
                                            try {
                                                A098.close();
                                            } catch (Throwable unused7) {
                                            }
                                        }
                                    }
                                }
                                if (r6 instanceof C30561Xx) {
                                    Cursor A099 = r72.A09("SELECT transaction_info, transaction_data, init_timestamp, update_timestamp, amount_data FROM message_payment_status_update WHERE message_row_id  = ?", new String[]{Long.toString(r6.A11)});
                                    try {
                                        if (A099.moveToNext()) {
                                            C30561Xx r73 = (C30561Xx) r6;
                                            r73.A03 = A099.getString(A099.getColumnIndexOrThrow("transaction_info"));
                                            r73.A01 = A099.getString(A099.getColumnIndexOrThrow("transaction_data"));
                                            r73.A02 = A099.getString(A099.getColumnIndexOrThrow("init_timestamp"));
                                            r73.A04 = A099.getString(A099.getColumnIndexOrThrow("update_timestamp"));
                                            r73.A00 = A099.getString(A099.getColumnIndexOrThrow("amount_data"));
                                        }
                                        A099.close();
                                    } finally {
                                        if (A099 != null) {
                                            try {
                                                A099.close();
                                            } catch (Throwable unused8) {
                                            }
                                        }
                                    }
                                }
                            } finally {
                                if (A09 != null) {
                                    try {
                                        A09.close();
                                    } catch (Throwable unused9) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof C30521Xt) {
                            C30521Xt r74 = (C30521Xt) r6;
                            Cursor A0910 = A012.A03.A09("SELECT device_added_count, device_removed_count FROM message_system_device_change WHERE message_row_id = ?", new String[]{Long.toString(r74.A11)});
                            try {
                                if (A0910.moveToNext()) {
                                    r74.A00 = A0910.getInt(A0910.getColumnIndexOrThrow("device_added_count"));
                                    r74.A01 = A0910.getInt(A0910.getColumnIndexOrThrow("device_removed_count"));
                                }
                                A0910.close();
                            } finally {
                                if (A0910 != null) {
                                    try {
                                        A0910.close();
                                    } catch (Throwable unused10) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof C30531Xu) {
                            C30531Xu r75 = (C30531Xu) r6;
                            Cursor A0911 = A012.A03.A09("SELECT privacy_provider, verified_biz_name, biz_state_id FROM message_system_initial_privacy_provider WHERE message_row_id = ?", new String[]{Long.toString(r75.A11)});
                            try {
                                if (A0911.moveToNext()) {
                                    r75.A00 = A0911.getInt(A0911.getColumnIndexOrThrow("biz_state_id"));
                                }
                                A0911.close();
                            } finally {
                                if (A0911 != null) {
                                    try {
                                        A0911.close();
                                    } catch (Throwable unused11) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof C30581Xz) {
                            C30581Xz r82 = (C30581Xz) r6;
                            boolean z2 = true;
                            Cursor A0912 = A012.A03.A09("SELECT is_blocked FROM message_system_block_contact WHERE message_row_id = ?", new String[]{Long.toString(r82.A11)});
                            try {
                                if (A0912.moveToNext()) {
                                    if (A0912.getInt(A0912.getColumnIndexOrThrow("is_blocked")) != 1) {
                                        z2 = false;
                                    }
                                    r82.A00 = z2;
                                }
                                A0912.close();
                            } finally {
                                if (A0912 != null) {
                                    try {
                                        A0912.close();
                                    } catch (Throwable unused12) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof AnonymousClass1Y0) {
                            AnonymousClass1Y0 r76 = (AnonymousClass1Y0) r6;
                            Cursor A0913 = A012.A03.A09("SELECT setting_duration FROM message_system_ephemeral_setting_not_applied WHERE message_row_id = ?", new String[]{Long.toString(r76.A11)});
                            try {
                                if (A0913.moveToNext()) {
                                    r76.A00 = A0913.getInt(A0913.getColumnIndexOrThrow("setting_duration"));
                                }
                                A0913.close();
                            } finally {
                                if (A0913 != null) {
                                    try {
                                        A0913.close();
                                    } catch (Throwable unused13) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof AnonymousClass1Y1) {
                            AnonymousClass1Y1 r77 = (AnonymousClass1Y1) r6;
                            Cursor A0914 = A012.A03.A09("SELECT message_row_id, privacy_message_type, business_name FROM message_system_business_state WHERE message_row_id = ?", new String[]{Long.toString(r77.A11)});
                            try {
                                if (A0914.moveToNext()) {
                                    r77.A00 = A0914.getInt(A0914.getColumnIndexOrThrow("privacy_message_type"));
                                    r77.A01 = A0914.getString(A0914.getColumnIndexOrThrow("business_name"));
                                }
                                A0914.close();
                            } finally {
                                if (A0914 != null) {
                                    try {
                                        A0914.close();
                                    } catch (Throwable unused14) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof AbstractC30571Xy) {
                            AbstractC30571Xy r94 = (AbstractC30571Xy) r6;
                            A01 = r3.A04.A00.get();
                            try {
                                boolean z3 = true;
                                Cursor A0915 = A01.A03.A09("SELECT service, invite_used FROM message_system_payment_invite_setup WHERE message_row_id =?", new String[]{Long.toString(r94.A11)});
                                if (A0915.moveToNext()) {
                                    r94.A00 = A0915.getInt(A0915.getColumnIndexOrThrow("service"));
                                    if (A0915.getInt(A0915.getColumnIndexOrThrow("invite_used")) != 1) {
                                        z3 = false;
                                    }
                                    r94.A01 = z3;
                                }
                                A0915.close();
                                A01.close();
                            } finally {
                            }
                        }
                        if (r6 instanceof AnonymousClass1Y2) {
                            AnonymousClass1Y2 r83 = (AnonymousClass1Y2) r6;
                            boolean z4 = true;
                            Cursor A0916 = A012.A03.A09("SELECT call_id, is_video_call FROM message_system_linked_group_call WHERE message_row_id = ?", new String[]{Long.toString(r83.A11)});
                            try {
                                if (A0916.moveToNext()) {
                                    r83.A00 = A0916.getString(A0916.getColumnIndexOrThrow("call_id"));
                                    if (A0916.getInt(A0916.getColumnIndexOrThrow("is_video_call")) != 1) {
                                        z4 = false;
                                    }
                                    r83.A01 = z4;
                                }
                                A0916.close();
                            } finally {
                                if (A0916 != null) {
                                    try {
                                        A0916.close();
                                    } catch (Throwable unused15) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof AnonymousClass1Y3) {
                            AnonymousClass1Y3 r84 = (AnonymousClass1Y3) r6;
                            Cursor A0917 = A012.A03.A09("SELECT old_group_type, new_group_type, linked_parent_group_jid_row_id FROM message_system_community_link_changed WHERE message_row_id = ?", new String[]{Long.toString(r84.A11)});
                            try {
                                if (A0917.moveToNext()) {
                                    int columnIndexOrThrow = A0917.getColumnIndexOrThrow("old_group_type");
                                    r84.A02 = A0917.isNull(columnIndexOrThrow) ? null : Integer.valueOf(A0917.getInt(columnIndexOrThrow));
                                    r84.A00 = A0917.getInt(A0917.getColumnIndexOrThrow("new_group_type"));
                                    r84.A01 = C15580nU.A02(r3.A02.A03((long) A0917.getInt(A0917.getColumnIndexOrThrow("linked_parent_group_jid_row_id"))));
                                }
                                A0917.close();
                            } finally {
                                if (A0917 != null) {
                                    try {
                                        A0917.close();
                                    } catch (Throwable unused16) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof AnonymousClass1Y4) {
                            AnonymousClass1Y4 r78 = (AnonymousClass1Y4) r6;
                            Cursor A0918 = A012.A03.A09("SELECT linked_parent_group_name FROM message_system_group_with_parent WHERE message_row_id = ?", new String[]{Long.toString(r78.A11)});
                            try {
                                if (A0918.moveToNext()) {
                                    r78.A00 = A0918.getString(A0918.getColumnIndexOrThrow("linked_parent_group_name"));
                                }
                                A0918.close();
                            } finally {
                                if (A0918 != null) {
                                    try {
                                        A0918.close();
                                    } catch (Throwable unused17) {
                                    }
                                }
                            }
                        }
                        if (r6 instanceof AnonymousClass1Y5) {
                            AnonymousClass1Y5 r95 = (AnonymousClass1Y5) r6;
                            Cursor A0919 = A012.A03.A09("SELECT subgroup_raw_jid, subgroup_subject, parent_group_jid_row_id FROM message_system_sibling_group_link_change WHERE message_row_id = ?", new String[]{Long.toString(r95.A11)});
                            try {
                                HashSet hashSet = new HashSet();
                                int columnIndexOrThrow2 = A0919.getColumnIndexOrThrow("subgroup_raw_jid");
                                int columnIndexOrThrow3 = A0919.getColumnIndexOrThrow("subgroup_subject");
                                int columnIndexOrThrow4 = A0919.getColumnIndexOrThrow("parent_group_jid_row_id");
                                C15580nU r1 = null;
                                while (A0919.moveToNext()) {
                                    hashSet.add(new AnonymousClass1OU(C15580nU.A04(A0919.getString(columnIndexOrThrow2)), A0919.getString(columnIndexOrThrow3), 2, 0));
                                    r1 = C15580nU.A02(r3.A02.A03((long) A0919.getInt(columnIndexOrThrow4)));
                                }
                                r95.A01 = r1;
                                int size = hashSet.size();
                                if (size < 0) {
                                    size = 0;
                                }
                                r95.A00 = size;
                                Set set = r95.A03;
                                set.clear();
                                set.addAll(hashSet);
                                A0919.close();
                            } finally {
                                if (A0919 != null) {
                                    try {
                                        A0919.close();
                                    } catch (Throwable unused18) {
                                    }
                                }
                            }
                        }
                        A012.close();
                    }
                    A093.close();
                    A01.close();
                } finally {
                }
            }
        }
        if (r6 == null) {
            return null;
        }
        r6.A0c(cursor, this.A04);
        if (!A0A()) {
            r6.A0b(cursor);
        }
        return r6;
    }

    public final void A05(ContentValues contentValues, AbstractC15340mz r7) {
        long A01;
        byte b;
        contentValues.put("sort_id", Long.valueOf(r7.A12));
        C16510p9 r1 = this.A03;
        AnonymousClass1IS r2 = r7.A0z;
        AbstractC14640lm r0 = r2.A00;
        AnonymousClass009.A05(r0);
        contentValues.put("chat_row_id", Long.valueOf(r1.A02(r0)));
        C30021Vq.A05(contentValues, "from_me", r2.A02);
        contentValues.put("key_id", r2.A01);
        AbstractC14640lm A0B = r7.A0B();
        if (A0B == null) {
            A01 = 0;
        } else {
            A01 = this.A04.A01(A0B);
        }
        contentValues.put("sender_jid_row_id", Long.valueOf(A01));
        contentValues.put("status", Integer.valueOf(r7.A0C));
        C30021Vq.A05(contentValues, "broadcast", r7.A0r);
        contentValues.put("recipient_count", Integer.valueOf(r7.A0A));
        C30021Vq.A04(contentValues, "participant_hash", r7.A0l);
        contentValues.put("origination_flags", Integer.valueOf(r7.A07()));
        contentValues.put("origin", Integer.valueOf(r7.A08));
        contentValues.put("timestamp", Long.valueOf(r7.A0I));
        contentValues.put("received_timestamp", Long.valueOf(r7.A0G));
        contentValues.put("receipt_server_timestamp", Long.valueOf(r7.A0H));
        if (!(r7 instanceof AnonymousClass1XB)) {
            b = r7.A0y;
        } else {
            b = 7;
        }
        contentValues.put("message_type", Integer.valueOf(b));
        C30021Vq.A04(contentValues, "text_data", r7.A0Q());
        C30021Vq.A05(contentValues, "starred", r7.A0v);
        contentValues.put("lookup_tables", Long.valueOf(r7.A0A()));
        contentValues.put("message_add_on_flags", Integer.valueOf(r7.A07));
        if (r7.A11 > 0) {
            contentValues.put("_id", Long.valueOf(r7.A11));
        }
    }

    public final void A06(ContentValues contentValues, AbstractC15340mz r7) {
        byte b;
        contentValues.put("status", Integer.valueOf(r7.A0C));
        contentValues.put("recipient_count", Integer.valueOf(r7.A0A));
        contentValues.put("origination_flags", Integer.valueOf(r7.A07()));
        contentValues.put("origin", Integer.valueOf(r7.A08));
        contentValues.put("timestamp", Long.valueOf(r7.A0I));
        long j = r7.A0G;
        if (j == 0) {
            j = this.A02.A00();
        }
        contentValues.put("received_timestamp", Long.valueOf(j));
        contentValues.put("receipt_server_timestamp", Long.valueOf(r7.A0H));
        if (!(r7 instanceof AnonymousClass1XB)) {
            b = r7.A0y;
        } else {
            b = 7;
        }
        contentValues.put("message_type", Integer.valueOf(b));
        C30021Vq.A04(contentValues, "text_data", r7.A0Q());
        contentValues.put("lookup_tables", Long.valueOf(r7.A0A()));
        contentValues.put("sort_id", Long.valueOf(r7.A12));
        contentValues.put("message_add_on_flags", Integer.valueOf(r7.A07));
    }

    public final void A07(ContentValues contentValues, AbstractC15340mz r5) {
        double d;
        double d2;
        contentValues.put("status", Integer.valueOf(r5.A0C));
        contentValues.put("needs_push", (Integer) 2);
        C30021Vq.A02(contentValues, r5);
        contentValues.put("timestamp", Long.valueOf(r5.A0I));
        C30021Vq.A04(contentValues, "media_url", r5.A0P());
        C30021Vq.A04(contentValues, "media_mime_type", r5.A0N());
        contentValues.put("media_wa_type", Byte.valueOf(r5.A0y));
        contentValues.put("media_size", Long.valueOf(r5.A09()));
        C30021Vq.A04(contentValues, "media_name", r5.A0O());
        C30021Vq.A04(contentValues, "media_caption", r5.A0K());
        C30021Vq.A04(contentValues, "media_hash", r5.A0M());
        contentValues.put("media_duration", Integer.valueOf(r5.A04()));
        contentValues.put("origin", Integer.valueOf(r5.A08));
        boolean z = r5 instanceof AnonymousClass1XP;
        if (!z) {
            d = 0.0d;
        } else {
            d = ((AnonymousClass1XP) r5).A00;
        }
        contentValues.put("latitude", Double.valueOf(d));
        if (!z) {
            d2 = 0.0d;
        } else {
            d2 = ((AnonymousClass1XP) r5).A01;
        }
        contentValues.put("longitude", Double.valueOf(d2));
        C30021Vq.A04(contentValues, "mentioned_jids", AnonymousClass1Y6.A00(r5.A0o));
        C30021Vq.A03(contentValues, C30021Vq.A00(this.A01, r5.A0H()));
        C30021Vq.A04(contentValues, "media_enc_hash", r5.A0L());
        contentValues.put("message_add_on_flags", Integer.valueOf(r5.A07));
    }

    public void A08(AbstractC15340mz r11, boolean z) {
        C16330op r7;
        String str;
        AnonymousClass1IS r3;
        ContentValues contentValues;
        String[] strArr;
        String str2;
        byte b;
        String[] strArr2;
        ContentValues contentValues2;
        String str3;
        C16330op r1;
        String str4;
        double d;
        double d2;
        int i;
        r11.A0Z(1);
        C16490p7 r0 = this.A05;
        C16310on A02 = r0.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            r0.A04();
            if (r0.A05.A0E(A02)) {
                if (z) {
                    List A0R = r11.A0R();
                    if (A0R == null || A0R.size() < r11.A0A) {
                        this.A00.AaV("message-table-scan", "update-main-message-v1", true);
                        contentValues2 = new ContentValues();
                        A07(contentValues2, r11);
                        r1 = A02.A03;
                        strArr2 = new String[]{r11.A0z.A01};
                        str4 = "messages";
                        str3 = "needs_push = 2 AND key_from_me = 1 AND key_id = ?";
                    } else {
                        ArrayList arrayList = new ArrayList(A0R);
                        AnonymousClass1IS r12 = r11.A0z;
                        arrayList.add(r12.A00);
                        ArrayList A06 = C15380n4.A06(arrayList);
                        A06.add(r12.A01);
                        contentValues2 = new ContentValues();
                        A07(contentValues2, r11);
                        StringBuilder sb = new StringBuilder("key_remote_jid IN ");
                        sb.append(AnonymousClass1Ux.A00(A06.size() - 1));
                        sb.append(" AND ");
                        sb.append("key_from_me = 1");
                        sb.append(" AND ");
                        sb.append("key_id = ?");
                        str3 = sb.toString();
                        r1 = A02.A03;
                        strArr2 = (String[]) A06.toArray(new String[0]);
                        str4 = "messages";
                    }
                } else {
                    if (A0A() && r11.A0E() != null && r11.A0F <= 0) {
                        Cursor A09 = A02.A03.A09("SELECT quoted_row_id FROM messages WHERE _id = ?", new String[]{String.valueOf(r11.A11)});
                        if (A09.moveToLast()) {
                            r11.A0F = A09.getLong(A09.getColumnIndexOrThrow("quoted_row_id"));
                        }
                        A09.close();
                    }
                    contentValues2 = new ContentValues();
                    contentValues2.put("status", Integer.valueOf(r11.A0C));
                    int i2 = 0;
                    if (r11.A0r) {
                        i2 = 2;
                    }
                    contentValues2.put("needs_push", Integer.valueOf(i2));
                    C30021Vq.A02(contentValues2, r11);
                    contentValues2.put("timestamp", Long.valueOf(r11.A0I));
                    C30021Vq.A04(contentValues2, "media_url", r11.A0P());
                    C30021Vq.A04(contentValues2, "media_mime_type", r11.A0N());
                    contentValues2.put("media_wa_type", Byte.valueOf(r11.A0y));
                    contentValues2.put("media_size", Long.valueOf(r11.A09()));
                    C30021Vq.A04(contentValues2, "media_name", r11.A0O());
                    C30021Vq.A04(contentValues2, "media_caption", r11.A0K());
                    C30021Vq.A04(contentValues2, "media_hash", r11.A0M());
                    contentValues2.put("media_duration", Integer.valueOf(r11.A04()));
                    contentValues2.put("origin", Integer.valueOf(r11.A08));
                    boolean z2 = r11 instanceof AnonymousClass1XP;
                    if (!z2) {
                        d = 0.0d;
                    } else {
                        d = ((AnonymousClass1XP) r11).A00;
                    }
                    contentValues2.put("latitude", Double.valueOf(d));
                    if (!z2) {
                        d2 = 0.0d;
                    } else {
                        d2 = ((AnonymousClass1XP) r11).A01;
                    }
                    contentValues2.put("longitude", Double.valueOf(d2));
                    C30021Vq.A04(contentValues2, "mentioned_jids", AnonymousClass1Y6.A00(r11.A0o));
                    C30021Vq.A03(contentValues2, C30021Vq.A00(this.A01, r11.A0H()));
                    contentValues2.put("edit_version", Integer.valueOf(r11.A01));
                    C30021Vq.A04(contentValues2, "media_enc_hash", r11.A0L());
                    C30021Vq.A04(contentValues2, "payment_transaction_id", r11.A0m);
                    contentValues2.put("forwarded", Integer.valueOf(r11.A07()));
                    contentValues2.put("preview_type", Integer.valueOf(r11.A05()));
                    contentValues2.put("quoted_row_id", Long.valueOf(r11.A0F));
                    contentValues2.put("lookup_tables", Long.valueOf(r11.A0A()));
                    if (!(r11 instanceof C30361Xc)) {
                        i = 0;
                    } else {
                        i = ((C30361Xc) r11).A00;
                    }
                    contentValues2.put("future_message_type", Integer.valueOf(i));
                    contentValues2.put("message_add_on_flags", Integer.valueOf(r11.A07));
                    AnonymousClass1IS r6 = r11.A0z;
                    AbstractC14640lm r02 = r6.A00;
                    AnonymousClass009.A05(r02);
                    strArr2 = new String[]{r02.getRawString(), String.valueOf(r6.A02 ? 1 : 0), r6.A01};
                    r1 = A02.A03;
                    str4 = "messages";
                    str3 = "key_remote_jid = ? AND key_from_me = ? AND key_id = ?";
                }
                r1.A00(str4, contentValues2, str3, strArr2);
            }
            if (A09()) {
                if (z) {
                    List<AbstractC14640lm> A0R2 = r11.A0R();
                    if (A0R2 == null || A0R2.size() <= 0) {
                        contentValues = new ContentValues();
                        A06(contentValues, r11);
                        r3 = r11.A0z;
                        strArr = new String[]{String.valueOf(r3.A01)};
                        r7 = A02.A03;
                        str2 = "message";
                        str = "broadcast = 1 AND from_me = 1 AND key_id = ?";
                    } else {
                        ArrayList arrayList2 = new ArrayList(A0R2.size() + 1);
                        C16510p9 r5 = this.A03;
                        r3 = r11.A0z;
                        arrayList2.add(String.valueOf(r5.A02(r3.A00)));
                        for (AbstractC14640lm r03 : A0R2) {
                            arrayList2.add(String.valueOf(r5.A02(r03)));
                        }
                        arrayList2.add(r3.A01);
                        contentValues = new ContentValues();
                        A06(contentValues, r11);
                        StringBuilder sb2 = new StringBuilder("chat_row_id IN ");
                        sb2.append(AnonymousClass1Ux.A00(arrayList2.size() - 1));
                        sb2.append(" AND ");
                        sb2.append("from_me = 1");
                        sb2.append(" AND ");
                        sb2.append("key_id = ?");
                        str = sb2.toString();
                        r7 = A02.A03;
                        strArr = (String[]) arrayList2.toArray(new String[0]);
                        str2 = "message";
                    }
                } else {
                    contentValues = new ContentValues();
                    C16510p9 r13 = this.A03;
                    r3 = r11.A0z;
                    AbstractC14640lm r04 = r3.A00;
                    AnonymousClass009.A05(r04);
                    contentValues.put("chat_row_id", Long.valueOf(r13.A02(r04)));
                    C30021Vq.A05(contentValues, "from_me", r3.A02);
                    contentValues.put("key_id", r3.A01);
                    contentValues.put("status", Integer.valueOf(r11.A0C));
                    C30021Vq.A05(contentValues, "broadcast", r11.A0r);
                    contentValues.put("recipient_count", Integer.valueOf(r11.A0A));
                    C30021Vq.A04(contentValues, "participant_hash", r11.A0l);
                    contentValues.put("origination_flags", Integer.valueOf(r11.A07()));
                    contentValues.put("origin", Integer.valueOf(r11.A08));
                    contentValues.put("timestamp", Long.valueOf(r11.A0I));
                    long j = r11.A0G;
                    if (j == 0) {
                        j = this.A02.A00();
                    }
                    contentValues.put("received_timestamp", Long.valueOf(j));
                    contentValues.put("receipt_server_timestamp", Long.valueOf(r11.A0H));
                    if (!(r11 instanceof AnonymousClass1XB)) {
                        b = r11.A0y;
                    } else {
                        b = 7;
                    }
                    contentValues.put("message_type", Integer.valueOf(b));
                    C30021Vq.A04(contentValues, "text_data", r11.A0Q());
                    contentValues.put("lookup_tables", Long.valueOf(r11.A0A()));
                    contentValues.put("sort_id", Long.valueOf(r11.A12));
                    contentValues.put("message_add_on_flags", Integer.valueOf(r11.A07));
                    strArr = A0C(r3);
                    r7 = A02.A03;
                    str2 = "message";
                    str = "chat_row_id = ? AND from_me = ? AND key_id = ?";
                }
                r7.A00(str2, contentValues, str, strArr);
                if (r11.A11 <= 0 && !z) {
                    Cursor A092 = r7.A09("SELECT _id FROM message_view WHERE chat_row_id = ? AND from_me = ? AND key_id = ?", A0C(r3));
                    if (A092.moveToNext()) {
                        r11.A11 = A092.getLong(A092.getColumnIndexOrThrow("_id"));
                    }
                    A092.close();
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A09() {
        return A0A() || this.A06.A01("migration_message_main_index", 0) > 0;
    }

    public boolean A0A() {
        return this.A06.A01("main_message_ready", 0) == 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ba A[Catch: all -> 0x0146, TRY_LEAVE, TryCatch #5 {all -> 0x014b, blocks: (B:3:0x0008, B:42:0x013a, B:4:0x000c, B:6:0x001c, B:7:0x0025, B:9:0x002b, B:10:0x0037, B:20:0x0092, B:21:0x0097, B:23:0x00b2, B:26:0x00ba, B:34:0x0117, B:36:0x0120, B:41:0x0137, B:27:0x00cb, B:28:0x00d5, B:30:0x00db, B:32:0x00e7, B:33:0x00f2, B:11:0x0048, B:12:0x004d, B:14:0x0054, B:17:0x0065, B:18:0x008c), top: B:49:0x0008 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0B(java.util.Set r17, boolean r18) {
        /*
        // Method dump skipped, instructions count: 336
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20090vC.A0B(java.util.Set, boolean):boolean");
    }

    public final String[] A0C(AnonymousClass1IS r4) {
        String[] strArr = new String[3];
        C16510p9 r1 = this.A03;
        AbstractC14640lm r0 = r4.A00;
        AnonymousClass009.A05(r0);
        strArr[0] = String.valueOf(r1.A02(r0));
        strArr[1] = r4.A02 ? "1" : "0";
        strArr[2] = r4.A01;
        return strArr;
    }
}
