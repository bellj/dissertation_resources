package X;

import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;

/* renamed from: X.69S  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69S implements AbstractC35651iS {
    public final /* synthetic */ PaymentTransactionHistoryActivity A00;

    public AnonymousClass69S(PaymentTransactionHistoryActivity paymentTransactionHistoryActivity) {
        this.A00 = paymentTransactionHistoryActivity;
    }

    @Override // X.AbstractC35651iS
    public void ATe(AnonymousClass1IR r2) {
        this.A00.A2f();
    }

    @Override // X.AbstractC35651iS
    public void ATf(AnonymousClass1IR r2) {
        this.A00.A2f();
    }
}
