package X;

import android.os.PowerManager;

/* renamed from: X.1pR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39191pR extends AbstractRunnableC39141pM {
    public final PowerManager.WakeLock A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C16590pI A03;
    public final C39181pQ A04;
    public final AnonymousClass152 A05;

    public C39191pR(PowerManager.WakeLock wakeLock, AbstractC15710nm r2, C14330lG r3, C16590pI r4, C39181pQ r5, AnonymousClass152 r6) {
        super(r5);
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A04 = r5;
        this.A00 = wakeLock;
    }
}
