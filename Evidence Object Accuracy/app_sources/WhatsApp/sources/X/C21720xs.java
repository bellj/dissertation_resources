package X;

import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape0S0300100_I0;
import com.whatsapp.util.Log;

/* renamed from: X.0xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21720xs implements AbstractC21730xt {
    public C16080oP A00;
    public final C17220qS A01;

    public C21720xs(C17220qS r1) {
        this.A01 = r1;
    }

    public String A00(byte[] bArr) {
        String str;
        if (this.A00 == null) {
            str = "PrivateStatsSender/requestToSign need to set iq response listener first";
        } else {
            C17220qS r8 = this.A01;
            String A01 = r8.A01();
            if (r8.A0D(this, new AnonymousClass1V8(new AnonymousClass1V8(new AnonymousClass1V8("blinded_credential", bArr, (AnonymousClass1W9[]) null), "sign_credential", new AnonymousClass1W9[]{new AnonymousClass1W9("version", "1")}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "privatestats"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9(AnonymousClass1VY.A00, "to")}), A01, 239, 32000)) {
                return A01;
            }
            str = "PrivateStatsSender/requestToSign failed to send iq request";
        }
        Log.e(str);
        return null;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("PrivateStatsSender/onDeliveryFailure iqId = ");
        sb.append(str);
        Log.e(sb.toString());
        C16080oP r2 = this.A00;
        if (r2 != null) {
            synchronized (r2) {
                if (!str.equalsIgnoreCase(r2.A0C)) {
                    Log.e("PrivateStats/onSendFailure mismatched iq id, reset");
                    r2.A05.A03(12);
                    r2.A02();
                } else {
                    r2.A03(5);
                }
            }
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        StringBuilder sb = new StringBuilder("PrivateStatsSender/onError iqId = ");
        sb.append(str);
        Log.e(sb.toString());
        AnonymousClass1V8 A0F = r5.A0F("error");
        int A06 = A0F.A06(A0F.A0H("code"), "code");
        A0F.A0I("text", "");
        C16080oP r2 = this.A00;
        if (r2 != null) {
            synchronized (r2) {
                if (!str.equalsIgnoreCase(r2.A0C)) {
                    Log.e("PrivateStats/onIqResponseError mismatched iq id, reset");
                    r2.A02();
                } else if (A06 == 500) {
                    r2.A03(3);
                } else {
                    Log.e("PrivateStats/onIqResponseError iq errors, stop attempting to send iq");
                    r2.A05.A03(11);
                    r2.A06(false, 2);
                }
            }
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r12, String str) {
        AnonymousClass1V8 A0F = r12.A0F("sign_credential");
        long A09 = A0F.A09(A0F.A0H("t"), "t");
        byte[] bArr = A0F.A0F("signed_credential").A01;
        byte[] bArr2 = A0F.A0F("acs_public_key").A01;
        AnonymousClass1V8 A0E = A0F.A0E("dleq_proof");
        if (A0E != null) {
            AnonymousClass1V8 A0F2 = A0E.A0F("c");
            AnonymousClass1V8 A0F3 = A0E.A0F("s");
            C16080oP r5 = this.A00;
            if (r5 != null) {
                byte[] bArr3 = A0F2.A01;
                byte[] bArr4 = A0F3.A01;
                synchronized (r5) {
                    if (bArr3 != null) {
                        r5.A05.A05("dleq_proof_c", Base64.encodeToString(bArr3, 10));
                    }
                    if (bArr4 != null) {
                        r5.A05.A05("dleq_proof_s", Base64.encodeToString(bArr4, 10));
                    }
                }
            }
        }
        C16080oP r52 = this.A00;
        if (r52 != null) {
            synchronized (r52) {
                if (!str.equalsIgnoreCase(r52.A0C)) {
                    Log.e("PrivateStatsToken/onReceiveSignedToken iq requests messed up, reset");
                    r52.A02();
                } else if (bArr2 == null || bArr == null) {
                    r52.A05.A03(10);
                    r52.A06(false, 3);
                } else {
                    r52.A07.Ab2(new RunnableBRunnable0Shape0S0300100_I0(r52, bArr2, bArr, 2, A09));
                }
            }
        }
    }
}
