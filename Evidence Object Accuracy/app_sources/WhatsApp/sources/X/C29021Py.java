package X;

/* renamed from: X.1Py  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C29021Py extends AbstractC16110oT {
    public Long A00;
    public String A01;

    public C29021Py() {
        super(2492, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamAndroidDatabaseMigrationLlks {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "llksLatency", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "llksState", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
