package X;

import java.util.List;

/* renamed from: X.0PZ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0PZ {
    public final List A00;

    public AnonymousClass0PZ(List list) {
        this.A00 = list;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !AnonymousClass0PZ.class.equals(obj.getClass())) {
            return false;
        }
        return C16700pc.A0O(this.A00, ((AnonymousClass0PZ) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        return AnonymousClass01Y.A03(this.A00);
    }
}
