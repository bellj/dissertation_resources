package X;

import android.content.Context;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.util.List;

/* renamed from: X.2Uy  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Uy extends AbstractC51472Uz {
    public AnonymousClass34X A00;
    public boolean A01;
    public final AnonymousClass12P A02;
    public final AbstractC15710nm A03;
    public final C14900mE A04;
    public final C15450nH A05;
    public final C15890o4 A06;
    public final C15670ni A07;
    public final AnonymousClass1CY A08;
    public final AbstractC14440lR A09;

    public AnonymousClass2Uy(Context context, AnonymousClass12P r16, AbstractC15710nm r17, C14900mE r18, C15570nT r19, C15450nH r20, C15550nR r21, C15610nY r22, C63563Cb r23, C63543Bz r24, AnonymousClass01d r25, C14830m7 r26, C15890o4 r27, AnonymousClass018 r28, C15670ni r29, AnonymousClass19M r30, C16630pM r31, AnonymousClass12F r32, AnonymousClass1CY r33, AbstractC14440lR r34) {
        super(context, r19, r21, r22, r23, r24, r25, r26, r28, r30, r31, r32);
        A00();
        this.A08 = r33;
        this.A04 = r18;
        this.A03 = r17;
        this.A09 = r34;
        this.A05 = r20;
        this.A02 = r16;
        this.A07 = r29;
        this.A06 = r27;
    }

    /* renamed from: A07 */
    public void A05(C16440p1 r4, List list) {
        super.A05(r4, list);
        this.A00.setMessage(r4, list);
        this.A00.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(r4, 27, this));
    }
}
