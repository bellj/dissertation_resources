package X;

import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;

/* renamed from: X.3hA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74013hA extends TranslateAnimation {
    public final /* synthetic */ AbstractC36001jA A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74013hA(AbstractC36001jA r10) {
        super(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        this.A00 = r10;
    }

    @Override // android.view.animation.TranslateAnimation, android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        AbstractC36001jA r1 = this.A00;
        r1.A0G((int) (((float) r1.A0J.getHeight()) * f));
    }
}
