package X;

import java.security.Provider;
import java.security.Signature;

/* renamed from: X.5GQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GQ implements AnonymousClass5VR {
    public final /* synthetic */ Provider A00;
    public final /* synthetic */ AbstractC113485Ht A01;

    public AnonymousClass5GQ(Provider provider, AbstractC113485Ht r2) {
        this.A01 = r2;
        this.A00 = provider;
    }

    @Override // X.AnonymousClass5VR
    public Signature A8Z(String str) {
        Provider provider = this.A00;
        return provider != null ? Signature.getInstance(str, provider) : Signature.getInstance(str);
    }
}
