package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.data.DataHolder;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.3fu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73243fu extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public AbstractBinderC73243fu() {
        attachInterface(this, "com.google.android.gms.wearable.internal.IWearableCallbacks");
    }

    public static UnsupportedOperationException A00(Parcel parcel, Parcelable.Creator creator) {
        C94644cH.A00(parcel, creator);
        return new UnsupportedOperationException();
    }

    @Override // android.os.Binder
    public final boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        String str;
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        AbstractBinderC80543sQ r4 = (AbstractBinderC80543sQ) this;
        switch (i) {
            case 2:
                throw A00(parcel, C78173oU.CREATOR);
            case 3:
                throw A00(parcel, C78263od.CREATOR);
            case 4:
                throw A00(parcel, C78203oX.CREATOR);
            case 5:
                throw A00(parcel, DataHolder.CREATOR);
            case 6:
                throw A00(parcel, C78093oM.CREATOR);
            case 7:
                throw A00(parcel, C78273oe.CREATOR);
            case 8:
                throw A00(parcel, C78223oZ.CREATOR);
            case 9:
                throw A00(parcel, C78233oa.CREATOR);
            case 10:
                C78193oW r1 = (C78193oW) C12970iu.A0F(parcel, C78193oW.CREATOR);
                if (r4 instanceof BinderC80593sW) {
                    BinderC80593sW r42 = (BinderC80593sW) r4;
                    ArrayList A0l = C12960it.A0l();
                    List list = r1.A01;
                    if (list != null) {
                        A0l.addAll(list);
                    }
                    int i3 = r1.A00;
                    switch (i3) {
                        case 4000:
                            str = "TARGET_NODE_NOT_CONNECTED";
                            break;
                        case 4001:
                            str = "DUPLICATE_LISTENER";
                            break;
                        case 4002:
                            str = "UNKNOWN_LISTENER";
                            break;
                        case 4003:
                            str = "DATA_ITEM_TOO_LARGE";
                            break;
                        case 4004:
                            str = "INVALID_TARGET_NODE";
                            break;
                        case 4005:
                            str = "ASSET_UNAVAILABLE";
                            break;
                        case 4006:
                            str = "DUPLICATE_CAPABILITY";
                            break;
                        case 4007:
                            str = "UNKNOWN_CAPABILITY";
                            break;
                        case 4008:
                            str = "WIFI_CREDENTIAL_SYNC_NO_CREDENTIAL_FETCHED";
                            break;
                        case 4009:
                            str = "UNSUPPORTED_BY_TARGET";
                            break;
                        case 4010:
                            str = "ACCOUNT_KEY_CREATION_FAILED";
                            break;
                        default:
                            str = AnonymousClass3A7.A00(i3);
                            break;
                    }
                    AnonymousClass516 r12 = new AnonymousClass516(new Status(i3, str), A0l);
                    AnonymousClass1UL r0 = r42.A00;
                    if (r0 != null) {
                        ((BasePendingResult) r0).A05(r12);
                        r42.A00 = null;
                        break;
                    }
                } else {
                    throw C12970iu.A0z();
                }
                break;
            case 11:
                C12970iu.A0F(parcel, Status.CREATOR);
                if (!(r4 instanceof BinderC80583sV)) {
                    throw C12970iu.A0z();
                }
                break;
            case 12:
                throw A00(parcel, C78363on.CREATOR);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                throw A00(parcel, C78183oV.CREATOR);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                throw A00(parcel, C78243ob.CREATOR);
            case 15:
                throw A00(parcel, C78013oE.CREATOR);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                throw A00(parcel, C78013oE.CREATOR);
            case 17:
                throw A00(parcel, C78123oP.CREATOR);
            case 18:
                throw A00(parcel, C78133oQ.CREATOR);
            case 19:
                throw A00(parcel, C77993oC.CREATOR);
            case C43951xu.A01:
                throw A00(parcel, C78003oD.CREATOR);
            case 21:
            case 24:
            case 25:
            case 31:
            case 32:
            case 33:
            default:
                return false;
            case 22:
                throw A00(parcel, C78113oO.CREATOR);
            case 23:
                throw A00(parcel, C78103oN.CREATOR);
            case 26:
                throw A00(parcel, C78023oF.CREATOR);
            case 27:
                throw A00(parcel, C78033oG.CREATOR);
            case 28:
                throw A00(parcel, C78143oR.CREATOR);
            case 29:
                throw A00(parcel, C78153oS.CREATOR);
            case C25991Bp.A0S:
                throw A00(parcel, C78333ok.CREATOR);
            case 34:
                throw A00(parcel, C78353om.CREATOR);
            case 35:
                throw A00(parcel, C78213oY.CREATOR);
            case 36:
                throw A00(parcel, C78253oc.CREATOR);
            case 37:
                throw A00(parcel, C78163oT.CREATOR);
        }
        parcel2.writeNoException();
        return true;
    }
}
