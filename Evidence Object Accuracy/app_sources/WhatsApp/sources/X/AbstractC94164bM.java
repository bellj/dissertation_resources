package X;

import java.util.Enumeration;
import java.util.Hashtable;

/* renamed from: X.4bM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94164bM {
    public static Hashtable A00(Hashtable hashtable) {
        Hashtable hashtable2 = new Hashtable();
        Enumeration keys = hashtable.keys();
        while (keys.hasMoreElements()) {
            Object nextElement = keys.nextElement();
            hashtable2.put(nextElement, hashtable.get(nextElement));
        }
        return hashtable2;
    }

    public String A01(AnonymousClass5N2 r8) {
        StringBuffer stringBuffer;
        if (!(this instanceof C114905Nn)) {
            C114895Nm r5 = (C114895Nm) this;
            stringBuffer = new StringBuffer();
            boolean z = true;
            for (C114605Mj r1 : r8.A03()) {
                if (z) {
                    z = false;
                } else {
                    stringBuffer.append(',');
                }
                C95344dY.A03(stringBuffer, r5.A01, r1);
            }
        } else {
            C114905Nn r52 = (C114905Nn) this;
            stringBuffer = new StringBuffer();
            C114605Mj[] A03 = r8.A03();
            boolean z2 = true;
            for (int length = A03.length - 1; length >= 0; length--) {
                if (z2) {
                    z2 = false;
                } else {
                    stringBuffer.append(',');
                }
                C95344dY.A03(stringBuffer, r52.A01, A03[length]);
            }
        }
        return stringBuffer.toString();
    }
}
