package X;

import android.view.View;
import android.view.WindowInsets;

/* renamed from: X.0T5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0T5 {
    public static WindowInsets A00(View view, WindowInsets windowInsets) {
        return view.dispatchApplyWindowInsets(windowInsets);
    }

    public static WindowInsets A01(View view, WindowInsets windowInsets) {
        return view.onApplyWindowInsets(windowInsets);
    }

    public static void A02(View view) {
        view.requestApplyInsets();
    }
}
