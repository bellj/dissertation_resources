package X;

import android.animation.ValueAnimator;
import android.graphics.PointF;
import android.view.animation.AccelerateInterpolator;

/* renamed from: X.3BU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BU {
    public float A00;
    public int A01;
    public ValueAnimator A02;
    public ValueAnimator A03;
    public PointF A04;
    public boolean A05;

    public AnonymousClass3BU(PointF pointF, AnonymousClass1IO r7, float f, float f2, int i, long j) {
        this.A04 = pointF;
        AccelerateInterpolator accelerateInterpolator = new AccelerateInterpolator(1.2f);
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        this.A02 = ofFloat;
        ofFloat.setDuration(250L);
        this.A02.setInterpolator(accelerateInterpolator);
        this.A02.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(f, f2, i) { // from class: X.3Jz
            public final /* synthetic */ float A00;
            public final /* synthetic */ float A01;
            public final /* synthetic */ int A02;

            {
                this.A00 = r2;
                this.A01 = r3;
                this.A02 = r4;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                AnonymousClass3BU r4 = AnonymousClass3BU.this;
                float f3 = this.A00;
                float f4 = this.A01;
                int i2 = this.A02;
                float A00 = C12960it.A00(valueAnimator);
                r4.A00 = Math.max(f3, ((f4 - f3) * A00) + f3);
                float f5 = (float) i2;
                r4.A01 = (int) Math.min(f5, Math.min(1.2f * A00, A00) * f5);
            }
        });
        this.A02.addListener(new AnonymousClass2YS(r7, this));
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(0.0f, 1.0f);
        this.A03 = ofFloat2;
        ofFloat2.setDuration(200L);
        this.A03.setInterpolator(accelerateInterpolator);
        this.A03.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(f, f2) { // from class: X.3Jx
            public final /* synthetic */ float A00;
            public final /* synthetic */ float A01;

            {
                this.A00 = r2;
                this.A01 = r3;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                AnonymousClass3BU r6 = AnonymousClass3BU.this;
                float f3 = this.A00;
                float f4 = this.A01;
                float A00 = C12960it.A00(valueAnimator);
                r6.A00 = Math.min(r6.A00, Math.max(f3, f4 - ((f4 - f3) * (0.5f * A00))));
                float f5 = (float) r6.A01;
                r6.A01 = (int) Math.max(f5 - (A00 * f5), 0.0f);
            }
        });
        this.A03.addListener(new AnonymousClass2YV(r7, this, j));
    }
}
