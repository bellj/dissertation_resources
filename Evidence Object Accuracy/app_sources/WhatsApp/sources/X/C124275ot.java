package X;

import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124275ot extends AbstractC16350or {
    public final AnonymousClass6M3 A00;
    public final C129795yJ A01;
    public final ArrayList A02;
    public final /* synthetic */ PaymentTransactionHistoryActivity A03;

    public C124275ot(AnonymousClass6M3 r2, PaymentTransactionHistoryActivity paymentTransactionHistoryActivity, C129795yJ r4, ArrayList arrayList) {
        ArrayList arrayList2;
        this.A03 = paymentTransactionHistoryActivity;
        if (arrayList != null) {
            arrayList2 = C12980iv.A0x(arrayList);
        } else {
            arrayList2 = null;
        }
        this.A02 = arrayList2;
        this.A00 = r2;
        this.A01 = r4;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:109:0x0055 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v1, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r5v2 */
    /* JADX WARN: Type inference failed for: r5v3, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a1, code lost:
        if (X.C32751cg.A03(r3.A05, r3.A0J.A0I(r7), r2, true) != false) goto L_0x00a3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x00eb A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x011b A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r10) {
        /*
        // Method dump skipped, instructions count: 410
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124275ot.A05(java.lang.Object[]):java.lang.Object");
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass01T r6 = (AnonymousClass01T) obj;
        AnonymousClass6M3 r4 = this.A00;
        PaymentTransactionHistoryActivity paymentTransactionHistoryActivity = this.A03;
        String str = paymentTransactionHistoryActivity.A0K;
        if (str == null) {
            str = "";
        }
        C30941Zk r2 = paymentTransactionHistoryActivity.A0V;
        Object obj2 = r6.A00;
        AnonymousClass009.A05(obj2);
        Object obj3 = r6.A01;
        AnonymousClass009.A05(obj3);
        r4.AVd(r2, str, (List) obj2, (List) obj3);
    }
}
