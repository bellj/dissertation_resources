package X;

import android.graphics.Typeface;

/* renamed from: X.0Rc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05820Rc {
    public final int A00;
    public final Typeface A01;

    public C05820Rc(int i) {
        this.A01 = null;
        this.A00 = i;
    }

    public C05820Rc(Typeface typeface) {
        this.A01 = typeface;
        this.A00 = 0;
    }
}
