package X;

import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import java.util.List;

/* renamed from: X.3eJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72283eJ extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $onError;
    public final /* synthetic */ AnonymousClass1J7 $onSuccess;
    public final /* synthetic */ C18190s3 this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72283eJ(C18190s3 r2, AnonymousClass1J7 r3, AnonymousClass1J7 r4) {
        super(1);
        this.this$0 = r2;
        this.$onError = r3;
        this.$onSuccess = r4;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        C65933Lq r5 = (C65933Lq) obj;
        C16700pc.A0E(r5, 0);
        List list = r5.A01;
        C18190s3 r0 = this.this$0;
        if (list == null) {
            r0.A00.A0H(new RunnableBRunnable0Shape16S0100000_I1_2(this.$onError, 4));
        } else {
            AnonymousClass1WI.A00(this.this$0.A00, this.$onSuccess, ((C235612d) r0.A03.get()).A00(list), 42);
        }
        return AnonymousClass1WZ.A00;
    }
}
