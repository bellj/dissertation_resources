package X;

/* renamed from: X.66Z  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66Z implements AbstractC116435Vk {
    public final /* synthetic */ AnonymousClass024 A00;
    public final /* synthetic */ AnonymousClass024 A01;

    public AnonymousClass66Z(AnonymousClass024 r1, AnonymousClass024 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC116435Vk
    public void AOH(String str) {
        this.A00.accept(str);
    }

    @Override // X.AbstractC116435Vk
    public void AT5(String str) {
        this.A01.accept(str);
    }
}
