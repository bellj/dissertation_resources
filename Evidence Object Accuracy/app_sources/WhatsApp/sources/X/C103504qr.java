package X;

import android.content.Context;
import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPickerActivity;

/* renamed from: X.4qr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103504qr implements AbstractC009204q {
    public final /* synthetic */ DownloadableWallpaperPickerActivity A00;

    public C103504qr(DownloadableWallpaperPickerActivity downloadableWallpaperPickerActivity) {
        this.A00 = downloadableWallpaperPickerActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
