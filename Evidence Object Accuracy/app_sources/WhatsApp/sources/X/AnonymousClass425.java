package X;

import com.whatsapp.calling.callhistory.CallLogActivity;
import com.whatsapp.jid.UserJid;
import java.util.Collection;

/* renamed from: X.425  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass425 extends C27131Gd {
    public final /* synthetic */ CallLogActivity A00;

    public AnonymousClass425(CallLogActivity callLogActivity) {
        this.A00 = callLogActivity;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r3) {
        CallLogActivity callLogActivity = this.A00;
        if (callLogActivity.A0O.equals(r3)) {
            callLogActivity.A2e();
        }
    }

    @Override // X.C27131Gd
    public void A02(UserJid userJid) {
        CallLogActivity callLogActivity = this.A00;
        if (callLogActivity.A0O.equals(userJid)) {
            callLogActivity.A2e();
        }
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        CallLogActivity callLogActivity = this.A00;
        if (callLogActivity.A0O.equals(userJid)) {
            callLogActivity.A2e();
        }
    }

    @Override // X.C27131Gd
    public void A05(Collection collection) {
        this.A00.A2e();
    }

    @Override // X.C27131Gd
    public void A06(Collection collection) {
        this.A00.A2e();
    }
}
