package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageButton;
import com.whatsapp.R;

/* renamed from: X.08i  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08i extends ImageButton implements AnonymousClass012, AnonymousClass03Y {
    public final AnonymousClass085 A00;
    public final AnonymousClass07j A01;

    public AnonymousClass08i(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.imageButtonStyle);
    }

    public AnonymousClass08i(Context context, AttributeSet attributeSet, int i) {
        super(AnonymousClass083.A00(context), attributeSet, i);
        AnonymousClass084.A03(getContext(), this);
        AnonymousClass085 r0 = new AnonymousClass085(this);
        this.A00 = r0;
        r0.A05(attributeSet, i);
        AnonymousClass07j r02 = new AnonymousClass07j(this);
        this.A01 = r02;
        r02.A02(attributeSet, i);
    }

    @Override // android.widget.ImageView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass07j r02 = this.A01;
        if (r02 != null) {
            r02.A00();
        }
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A01;
    }

    public ColorStateList getSupportImageTintList() {
        C016107p r0;
        AnonymousClass07j r02 = this.A01;
        if (r02 == null || (r0 = r02.A00) == null) {
            return null;
        }
        return r0.A00;
    }

    public PorterDuff.Mode getSupportImageTintMode() {
        C016107p r0;
        AnonymousClass07j r02 = this.A01;
        if (r02 == null || (r0 = r02.A00) == null) {
            return null;
        }
        return r0.A01;
    }

    @Override // android.widget.ImageView, android.view.View
    public boolean hasOverlappingRendering() {
        return this.A01.A03() && super.hasOverlappingRendering();
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A02(i);
        }
    }

    @Override // android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        AnonymousClass07j r0 = this.A01;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        AnonymousClass07j r0 = this.A01;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // android.widget.ImageView
    public void setImageResource(int i) {
        this.A01.A01(i);
    }

    @Override // android.widget.ImageView
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        AnonymousClass07j r0 = this.A01;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A03(colorStateList);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A04(mode);
        }
    }

    @Override // X.AnonymousClass03Y
    public void setSupportImageTintList(ColorStateList colorStateList) {
        AnonymousClass07j r2 = this.A01;
        if (r2 != null) {
            C016107p r1 = r2.A00;
            if (r1 == null) {
                r1 = new C016107p();
                r2.A00 = r1;
            }
            r1.A00 = colorStateList;
            r1.A02 = true;
            r2.A00();
        }
    }

    @Override // X.AnonymousClass03Y
    public void setSupportImageTintMode(PorterDuff.Mode mode) {
        AnonymousClass07j r2 = this.A01;
        if (r2 != null) {
            C016107p r1 = r2.A00;
            if (r1 == null) {
                r1 = new C016107p();
                r2.A00 = r1;
            }
            r1.A01 = mode;
            r1.A03 = true;
            r2.A00();
        }
    }
}
