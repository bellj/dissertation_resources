package X;

import java.lang.ref.ReferenceQueue;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.4MI  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4MI {
    public final ReferenceQueue A00 = new ReferenceQueue();
    public final ConcurrentHashMap A01 = new ConcurrentHashMap(16, 0.75f, 10);
}
