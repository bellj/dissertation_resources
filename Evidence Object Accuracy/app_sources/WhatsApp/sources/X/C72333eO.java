package X;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;

/* renamed from: X.3eO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C72333eO extends FileEntity {
    public final /* synthetic */ long A00;
    public final /* synthetic */ long A01;
    public final /* synthetic */ AnonymousClass3HM A02;
    public final /* synthetic */ FileInputStream A03;
    public final /* synthetic */ AtomicLong A04;
    public final /* synthetic */ HttpPut A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72333eO(AnonymousClass3HM r2, File file, FileInputStream fileInputStream, AtomicLong atomicLong, HttpPut httpPut, long j, long j2) {
        super(file, "application/binary");
        this.A02 = r2;
        this.A00 = j;
        this.A01 = j2;
        this.A03 = fileInputStream;
        this.A05 = httpPut;
        this.A04 = atomicLong;
    }

    public InputStream getContent() {
        return this.A03;
    }

    public long getContentLength() {
        return (this.A00 - this.A01) + 1;
    }

    public void writeTo(OutputStream outputStream) {
        InputStream content = getContent();
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                AnonymousClass3HM r5 = this.A02;
                if (r5.A05.A00()) {
                    int read = content.read(bArr, 0, 16384);
                    if (read <= 0) {
                        outputStream.flush();
                        break;
                    }
                    long j = (long) read;
                    this.A04.addAndGet(j);
                    r5.A03.AOt(j);
                    outputStream.write(bArr, 0, read);
                } else {
                    this.A05.abort();
                    if (content == null) {
                        return;
                    }
                }
            }
            content.close();
        } catch (Throwable th) {
            if (content != null) {
                try {
                    content.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }
}
