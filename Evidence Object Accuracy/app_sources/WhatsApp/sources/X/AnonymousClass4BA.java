package X;

/* renamed from: X.4BA  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BA {
    A03(0),
    A04(1),
    A01(2),
    A02(3);
    
    public final int mIntValue;

    AnonymousClass4BA(int i) {
        this.mIntValue = i;
    }
}
