package X;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.36m  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass36m extends AbstractC16350or {
    public final C15610nY A00;
    public final AnonymousClass018 A01;
    public final WeakReference A02;
    public final Set A03 = C12970iu.A12();

    public AnonymousClass36m(C15610nY r5, AbstractActivityC36611kC r6, AnonymousClass018 r7, List list) {
        super(r6);
        this.A02 = C12970iu.A10(r6);
        this.A00 = r5;
        this.A01 = r7;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A03.add(C12970iu.A0a(it).A0B(AbstractC14640lm.class));
        }
    }
}
