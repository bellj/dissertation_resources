package X;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;

/* renamed from: X.66W  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66W implements AbstractC17450qp {
    public final AbstractC17450qp A00;
    public final C124895qI A01;
    public final C126085sJ A02;

    public AnonymousClass66W(AbstractC17450qp r1, C124895qI r2, C126085sJ r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    /* renamed from: A00 */
    public Object A9j(C14230l4 r10, C14220l3 r11, C1093651k r12) {
        boolean z;
        int[] iArr;
        String str;
        String[] strArr;
        boolean z2;
        StringBuilder A0k = C12960it.A0k("PAY: WaExtensions evaluate: action=");
        String str2 = r12.A00;
        Log.d("Whatsapp", C12960it.A0d(str2, A0k));
        int i = 0;
        switch (str2.hashCode()) {
            case -1560255199:
                if (str2.equals("wa.action.CheckPin")) {
                    String A0g = C12960it.A0g(r11.A00, 0);
                    int length = A0g.length();
                    int[] iArr2 = new int[length];
                    int i2 = 9;
                    int i3 = 0;
                    for (int i4 = 0; i4 < length; i4++) {
                        int numericValue = Character.getNumericValue(A0g.charAt(i4));
                        iArr2[i4] = numericValue;
                        i2 = Math.min(i2, numericValue);
                        i3 = Math.max(i3, numericValue);
                    }
                    if (i2 != i3) {
                        boolean z3 = true;
                        boolean z4 = true;
                        while (i < length) {
                            z3 &= C12960it.A1V(iArr2[i], i2 + i);
                            z4 &= C12960it.A1V(iArr2[i], i3 - i);
                            if (z4 || z3) {
                                i++;
                            } else {
                                z2 = true;
                                return Boolean.valueOf(z2);
                            }
                        }
                    }
                    z2 = false;
                    return Boolean.valueOf(z2);
                }
                break;
            case -1120004527:
                if (str2.equals("wa.action.novi.EncryptLogEventV2")) {
                    String A0g2 = C12960it.A0g(r11.A00, 0);
                    C126085sJ r0 = this.A02;
                    if (r0 == null) {
                        return null;
                    }
                    try {
                        strArr = C125025qY.A00(C117315Zl.A0a(r0.A00.A07.A03(A0g2)));
                    } catch (IOException | IllegalStateException unused) {
                        com.whatsapp.util.Log.e("Pay: NoviEventLogger/logEvent Could not log event");
                        strArr = null;
                    }
                    if (strArr == null) {
                        return null;
                    }
                    ArrayList A0l = C12960it.A0l();
                    int length2 = strArr.length;
                    while (i < length2) {
                        A0l.add(strArr[i]);
                        i++;
                    }
                    return A0l;
                }
                break;
            case -246975243:
                if (str2.equals("wa.action.novi.EncryptLogEvent")) {
                    String A0g3 = C12960it.A0g(r11.A00, 0);
                    C126085sJ r02 = this.A02;
                    if (r02 == null) {
                        return null;
                    }
                    try {
                        str = r02.A00.A07.A03(A0g3);
                    } catch (IOException | IllegalStateException unused2) {
                        com.whatsapp.util.Log.e("Pay: NoviEventLogger/logEvent Could not log event");
                        str = null;
                    }
                    if (str != null) {
                        return str;
                    }
                    return null;
                }
                break;
            case 51260781:
                if (str2.equals("wa.action.CheckCardNumber")) {
                    String replaceAll = C12960it.A0g(r11.A00, 0).replaceAll("\\s", "");
                    boolean z5 = true;
                    int i5 = 0;
                    boolean z6 = false;
                    for (int length3 = replaceAll.length() - 1; length3 >= 0; length3--) {
                        int parseInt = Integer.parseInt(replaceAll.substring(length3, length3 + 1));
                        if (z6 && (parseInt = parseInt << 1) > 9) {
                            parseInt = (parseInt % 10) + 1;
                        }
                        i5 += parseInt;
                        z6 = !z6;
                    }
                    if (i5 % 10 != 0) {
                        z5 = false;
                    }
                    return Boolean.valueOf(z5);
                }
                break;
            case 268909162:
                if (str2.equals("wa.action.CheckCpfCnpj")) {
                    String replaceAll2 = C12960it.A0g(r11.A00, 0).replaceAll("[^\\d]", "");
                    int length4 = replaceAll2.length();
                    if (length4 == 11) {
                        iArr = C125335r5.A01;
                    } else {
                        if (length4 == 14) {
                            iArr = C125335r5.A00;
                        }
                        z = false;
                        return Boolean.valueOf(z);
                    }
                    int i6 = 0;
                    int i7 = 0;
                    while (true) {
                        int i8 = length4 - 2;
                        if (i6 < i8) {
                            int i9 = i6 + 1;
                            i7 += iArr[i9] * Character.getNumericValue(replaceAll2.charAt(i6));
                            i6 = i9;
                        } else {
                            int i10 = 11 - (i7 % 11);
                            if (i10 > 9) {
                                i10 = 0;
                            }
                            if (Character.getNumericValue(replaceAll2.charAt(i8)) == i10) {
                                int i11 = 0;
                                int i12 = 0;
                                while (true) {
                                    int i13 = length4 - 1;
                                    if (i11 < i13) {
                                        i12 += iArr[i11] * Character.getNumericValue(replaceAll2.charAt(i11));
                                        i11++;
                                    } else {
                                        int i14 = 11 - (i12 % 11);
                                        if (i14 > 9) {
                                            i14 = 0;
                                        }
                                        if (Character.getNumericValue(replaceAll2.charAt(i13)) == i14) {
                                            z = false;
                                            for (int i15 = 1; i15 < length4; i15++) {
                                                z = true;
                                                if (replaceAll2.charAt(i15 - 1) != replaceAll2.charAt(i15)) {
                                                    return Boolean.valueOf(z);
                                                }
                                                z = false;
                                            }
                                            return Boolean.valueOf(z);
                                        }
                                    }
                                }
                            }
                            z = false;
                            return Boolean.valueOf(z);
                        }
                    }
                }
                break;
            case 1206046387:
                if (str2.equals("wa.action.novi.GetDataEnv")) {
                    C126085sJ r03 = this.A02;
                    if (r03 == null) {
                        return null;
                    }
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append(r03.A00.A04.A00);
                    return C12960it.A0d(".android", A0h);
                }
                break;
        }
        AbstractC17450qp r04 = this.A00;
        if (r04 != null) {
            return r04.A9j(r11, r12, r10);
        }
        StringBuilder A0k2 = C12960it.A0k("WaExtensions/Bloks function: [");
        A0k2.append(str2);
        Log.e("WaExtensions", C12960it.A0d("] not implemented on client", A0k2));
        return null;
    }
}
