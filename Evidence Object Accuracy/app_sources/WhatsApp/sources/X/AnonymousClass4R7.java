package X;

/* renamed from: X.4R7  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4R7 {
    public final int A00;
    public final Object A01;
    public final C94214bR[] A02;
    public final AbstractC117085Ye[] A03;

    public AnonymousClass4R7(Object obj, C94214bR[] r3, AbstractC117085Ye[] r4) {
        this.A02 = r3;
        this.A03 = (AbstractC117085Ye[]) r4.clone();
        this.A01 = obj;
        this.A00 = r3.length;
    }
}
