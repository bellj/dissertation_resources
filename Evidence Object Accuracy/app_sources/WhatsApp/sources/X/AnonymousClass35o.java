package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.35o  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35o extends AbstractC69213Yj {
    public final AnonymousClass1AB A00;
    public final AbstractC116245Ur A01;
    public final AnonymousClass1BQ A02;

    public AnonymousClass35o(Context context, LayoutInflater layoutInflater, C14850m9 r3, AnonymousClass1AB r4, AbstractC116245Ur r5, AnonymousClass1BQ r6, int i) {
        super(context, layoutInflater, r3, i);
        this.A00 = r4;
        this.A01 = r5;
        this.A02 = r6;
    }

    @Override // X.AbstractC69213Yj
    public void A03(View view) {
        AnonymousClass016 r1 = this.A02.A03;
        if (r1.A01() != null && !C12980iv.A0z(r1).isEmpty()) {
            AnonymousClass028.A0D(view, R.id.empty).setVisibility(8);
            C12980iv.A1B(view, R.id.empty_text, 8);
        }
    }
}
