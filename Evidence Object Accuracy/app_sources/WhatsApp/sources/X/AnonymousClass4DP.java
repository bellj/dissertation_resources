package X;

import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.4DP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4DP {
    public static void A00(StringBuilder sb, HashMap hashMap) {
        sb.append("{");
        Iterator A0L = C72463ee.A0L(hashMap);
        boolean z = true;
        while (A0L.hasNext()) {
            String A0x = C12970iu.A0x(A0L);
            if (!z) {
                sb.append(",");
            }
            String str = (String) hashMap.get(A0x);
            sb.append("\"");
            sb.append(A0x);
            sb.append("\":");
            if (str == null) {
                sb.append("null");
            } else {
                sb.append("\"");
                sb.append(str);
                sb.append("\"");
            }
            z = false;
        }
        sb.append("}");
    }
}
