package X;

import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.support.faq.SearchFAQ;

/* renamed from: X.3gN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73523gN extends ClickableSpan {
    public final /* synthetic */ SearchFAQ A00;
    public final /* synthetic */ Runnable A01;

    public C73523gN(SearchFAQ searchFAQ, Runnable runnable) {
        this.A00 = searchFAQ;
        this.A01 = runnable;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        this.A01.run();
    }
}
