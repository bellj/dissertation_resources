package X;

import android.util.Pair;
import org.chromium.net.UrlRequest;

/* renamed from: X.60H  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60H {
    public static final C125495rL A0F = new C125495rL(7);
    public static final C125495rL A0G = new C125495rL(8);
    public static final C125495rL A0H = new C125495rL(10);
    public static final C125495rL A0I = new C125495rL(11);
    public static final C125495rL A0J = new C125495rL(13);
    public static final C125495rL A0K = new C125495rL(4);
    public static final C125495rL A0L = new C125495rL(14);
    public static final C125495rL A0M = new C125495rL(3);
    public static final C125495rL A0N = new C125495rL(1);
    public static final C125495rL A0O = new C125495rL(2);
    public static final C125495rL A0P = new C125495rL(5);
    public static final C125495rL A0Q = new C125495rL(0);
    public static final C125495rL A0R = new C125495rL(6);
    public static final C125495rL A0S = new C125495rL(12);
    public static final C125495rL A0T = new C125495rL(9);
    public Pair A00;
    public Boolean A01;
    public Float A02;
    public Float A03;
    public Float A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Integer A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public float[] A0C;
    public float[] A0D;
    public int[] A0E;

    public Object A00(C125495rL r3) {
        int i = r3.A00;
        switch (i) {
            case 0:
                return this.A0D;
            case 1:
                return this.A00;
            case 2:
                return this.A0A;
            case 3:
                return this.A03;
            case 4:
                return this.A09;
            case 5:
                return this.A0B;
            case 6:
                return this.A07;
            case 7:
                return this.A02;
            case 8:
                return this.A05;
            case 9:
                return this.A08;
            case 10:
                return this.A0C;
            case 11:
                return this.A0E;
            case 12:
                return this.A04;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return this.A01;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return this.A06;
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Invalid frame metadata key: "));
        }
    }

    public void A01(C125495rL r4, Object obj) {
        int i = r4.A00;
        switch (i) {
            case 0:
                this.A0D = (float[]) obj;
                return;
            case 1:
                this.A00 = (Pair) obj;
                return;
            case 2:
                this.A0A = (Long) obj;
                return;
            case 3:
                this.A03 = (Float) obj;
                return;
            case 4:
                this.A09 = (Long) obj;
                return;
            case 5:
                this.A0B = (Long) obj;
                return;
            case 6:
                this.A07 = (Integer) obj;
                return;
            case 7:
                this.A02 = (Float) obj;
                return;
            case 8:
                this.A05 = (Integer) obj;
                return;
            case 9:
                this.A08 = (Integer) obj;
                return;
            case 10:
                if (obj == null) {
                    this.A0C = null;
                    return;
                } else if (((float[]) obj).length == 4) {
                    float[] fArr = this.A0C;
                    if (fArr == null) {
                        fArr = new float[4];
                        this.A0C = fArr;
                    }
                    System.arraycopy(obj, 0, fArr, 0, 4);
                    return;
                } else {
                    throw C12970iu.A0f("Colour correction gain must be represented in a float array of length 4");
                }
            case 11:
                if (obj == null) {
                    this.A0E = null;
                    return;
                } else if (((int[]) obj).length == 18) {
                    int[] iArr = this.A0E;
                    if (iArr == null) {
                        iArr = new int[18];
                        this.A0E = iArr;
                    }
                    System.arraycopy(obj, 0, iArr, 0, 18);
                    return;
                } else {
                    throw C12970iu.A0f("Colour correction transform must be represented in a int array of length 18");
                }
            case 12:
                this.A04 = (Float) obj;
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A01 = (Boolean) obj;
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                this.A06 = (Integer) obj;
                return;
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Failed to frame metadata value: "));
        }
    }
}
