package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.4wA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106714wA implements AbstractC116785Ww {
    public long A00 = -1;
    public long A01;
    public AbstractC14070ko A02;
    public boolean A03;
    public boolean A04;
    public final C107204wx A05 = new C107204wx(null, true);
    public final C95054d0 A06;
    public final C95304dT A07 = C95304dT.A05(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
    public final C95304dT A08;

    public C106714wA() {
        C95304dT A05 = C95304dT.A05(10);
        this.A08 = A05;
        byte[] bArr = A05.A02;
        this.A06 = new C95054d0(bArr, bArr.length);
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r6) {
        this.A02 = r6;
        this.A05.A8b(r6, new C92824Xo(Integer.MIN_VALUE, 0, 1));
        r6.A9V();
    }

    @Override // X.AbstractC116785Ww
    public int AZn(AnonymousClass5Yf r10, AnonymousClass4IG r11) {
        C95314dV.A01(this.A02);
        C95304dT r6 = this.A07;
        int read = r10.read(r6.A02, 0, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
        boolean A1V = C12960it.A1V(read, -1);
        if (!this.A03) {
            C106904wT.A00(this.A02, -9223372036854775807L);
            this.A03 = true;
        }
        if (A1V) {
            return -1;
        }
        r6.A0S(0);
        r6.A0R(read);
        if (!this.A04) {
            this.A05.A09 = this.A01;
            this.A04 = true;
        }
        this.A05.A7a(r6);
        return 0;
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        this.A04 = false;
        this.A05.AbP();
        this.A01 = j2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0066, code lost:
        r10.Aaj();
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006f, code lost:
        if ((r3 - r2) >= 8192) goto L_0x0085;
     */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean Ae5(X.AnonymousClass5Yf r10) {
        /*
            r9 = this;
            r3 = 0
            r2 = 0
        L_0x0002:
            X.4dT r4 = r9.A08
            byte[] r1 = r4.A02
            r0 = 10
            r10.AZ4(r1, r3, r0)
            r4.A0S(r3)
            int r1 = r4.A0D()
            r0 = 4801587(0x494433, float:6.728456E-39)
            if (r1 == r0) goto L_0x0075
            r10.Aaj()
            r10.A5r(r2)
            long r5 = r9.A00
            r7 = -1
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x0028
            long r0 = (long) r2
            r9.A00 = r0
        L_0x0028:
            r5 = 0
            r3 = r2
        L_0x002a:
            r7 = 0
            r6 = 0
        L_0x002c:
            byte[] r1 = r4.A02
            r0 = 2
            r10.AZ4(r1, r5, r0)
            r4.A0S(r5)
            int r1 = r4.A0F()
            r0 = 65526(0xfff6, float:9.1821E-41)
            r1 = r1 & r0
            r0 = 65520(0xfff0, float:9.1813E-41)
            if (r1 != r0) goto L_0x0066
            r8 = 1
            int r7 = r7 + r8
            r1 = 4
            if (r7 < r1) goto L_0x004c
            r0 = 188(0xbc, float:2.63E-43)
            if (r6 <= r0) goto L_0x004c
            return r8
        L_0x004c:
            X.C95304dT.A06(r10, r4, r1)
            X.4d0 r1 = r9.A06
            r0 = 14
            r1.A07(r0)
            r0 = 13
            int r1 = r1.A04(r0)
            r0 = 6
            if (r1 <= r0) goto L_0x0085
            int r0 = r1 + -6
            r10.A5r(r0)
            int r6 = r6 + r1
            goto L_0x002c
        L_0x0066:
            r10.Aaj()
            int r3 = r3 + 1
            int r1 = r3 - r2
            r0 = 8192(0x2000, float:1.14794E-41)
            if (r1 >= r0) goto L_0x0085
            r10.A5r(r3)
            goto L_0x002a
        L_0x0075:
            r0 = 3
            r4.A0T(r0)
            int r1 = r4.A0B()
            int r0 = r1 + 10
            int r2 = r2 + r0
            r10.A5r(r1)
            goto L_0x0002
        L_0x0085:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106714wA.Ae5(X.5Yf):boolean");
    }
}
