package X;

import android.view.View;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.6CF  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CF implements AnonymousClass6MZ {
    public final /* synthetic */ PinBottomSheetDialogFragment A00;
    public final /* synthetic */ AbstractC1308360d A01;

    public AnonymousClass6CF(PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AbstractC1308360d r2) {
        this.A01 = r2;
        this.A00 = pinBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass6MZ
    public void AOP(String str) {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
        pinBottomSheetDialogFragment.A1N();
        AbstractC1308360d r3 = this.A01;
        ActivityC13790kL r6 = r3.A05;
        C14900mE r7 = r3.A03;
        C18610sj r9 = r3.A0D;
        C18650sn r8 = r3.A0B;
        AnonymousClass60T r10 = r3.A0G;
        C129215xM r5 = new C129215xM(r6, r7, r8, r9, r10, "PIN");
        AnonymousClass6B7 A0C = C117315Zl.A0C(r10, "FB", "PIN");
        if (A0C != null) {
            C128545wH r52 = new C128545wH(A0C);
            r3.A0F.A00(new C1331069m(r52, new C130775zx(r3.A04, r3.A07, r9), pinBottomSheetDialogFragment, r3), r52, str);
            return;
        }
        r5.A00(new C133396Ap(this, str), "FB");
    }

    @Override // X.AnonymousClass6MZ
    public void AQi(View view) {
        AbstractC1308360d r4 = this.A01;
        ActivityC13790kL r1 = r4.A05;
        if (r4 instanceof C122235l8) {
            C122235l8 r42 = (C122235l8) r4;
            C125605rW r3 = new C125605rW(r1);
            C12960it.A1E(new C124065oO(r3, r42.A01), r42.A02);
        }
    }
}
