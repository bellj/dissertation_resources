package X;

import com.whatsapp.R;

/* renamed from: X.68l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328468l implements AnonymousClass1FK {
    public final /* synthetic */ ActivityC13790kL A00;
    public final /* synthetic */ C129735yD A01;

    public C1328468l(ActivityC13790kL r1, C129735yD r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r10) {
        C129735yD r2 = this.A01;
        r2.A0G.A04(C12960it.A0b("onClosePaymentAccountConfirmed/onRequestError. paymentNetworkError: ", r10));
        AnonymousClass69D r3 = r2.A0C;
        C14850m9 r5 = r2.A0B;
        r3.A01(this.A00, r5, r2.A0D, r10.A00, R.string.payment_account_cannot_be_removed).show();
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r10) {
        C129735yD r2 = this.A01;
        r2.A0G.A06(C12960it.A0b("onClosePaymentAccountConfirmed/onResponseError. paymentNetworkError: ", r10));
        ActivityC13790kL r4 = this.A00;
        r4.AaN();
        r2.A0C.A01(r4, r2.A0B, r2.A0D, r10.A00, R.string.payment_account_cannot_be_removed).show();
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        C129735yD r2 = this.A01;
        r2.A0G.A06("onClosePaymentAccountConfirmed/onResponseSuccess");
        ActivityC13790kL r3 = this.A00;
        r3.AaN();
        C12960it.A0t(C117295Zj.A05(r2.A04), "payment_brazil_nux_dismissed", true);
        C36021jC.A01(r3, 100);
    }
}
