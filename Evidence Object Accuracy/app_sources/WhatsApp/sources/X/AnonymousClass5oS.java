package X;

import android.database.Cursor;
import android.text.Html;
import android.widget.TextSwitcher;
import com.whatsapp.R;

/* renamed from: X.5oS  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oS extends AbstractC16350or {
    public final /* synthetic */ TextSwitcher A00;
    public final /* synthetic */ AbstractActivityC121495iO A01;

    public AnonymousClass5oS(TextSwitcher textSwitcher, AbstractActivityC121495iO r2) {
        this.A01 = r2;
        this.A00 = textSwitcher;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        long j;
        C17070qD r0 = ((AbstractActivityC121685jC) this.A01).A0P;
        r0.A03();
        C241414j r3 = r0.A09;
        int[] iArr = {3};
        synchronized (r3) {
            j = 0;
            C16310on A01 = r3.A00.get();
            Cursor A08 = A01.A03.A08("contacts", C241414j.A02(iArr, 3), null, null, new String[]{"count(*)"}, null);
            if (A08 != null) {
                try {
                    if (A08.moveToNext()) {
                        j = A08.getLong(0);
                    }
                    A08.close();
                } catch (Throwable th) {
                    try {
                        A08.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            A01.close();
        }
        return Long.valueOf(j);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Long l = (Long) obj;
        if (l.longValue() >= 10) {
            AbstractActivityC121495iO r5 = this.A01;
            if (!r5.isFinishing()) {
                this.A00.setText(Html.fromHtml(C12960it.A0X(r5, l.toString(), C12970iu.A1b(), 0, R.string.payments_value_props_contacts_desc_text)));
            }
        }
        this.A01.A33(l);
    }
}
