package X;

import java.util.Arrays;

/* renamed from: X.3BI  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BI {
    public final AnonymousClass1V8 A00;

    public AnonymousClass3BI(AnonymousClass3CS r10, String str) {
        C41141sy r2 = new C41141sy("iq");
        C41141sy.A01(r2, "xmlns", "urn:xmpp:whatsapp:push");
        r2.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
        C41141sy r1 = new C41141sy("pn");
        if (AnonymousClass3JT.A0E(str, 0, 300, false)) {
            r1.A01 = str.getBytes();
        }
        r2.A05(r1.A03());
        r2.A07(r10.A00, C12960it.A0l());
        r10.A00(r2, Arrays.asList(new String[0]));
        this.A00 = r2.A03();
    }
}
