package X;

import android.app.AlertDialog;
import android.content.Context;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.whatsapp.R;

/* renamed from: X.6Ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C134376Ej implements AbstractC14590lg {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass61g A01;
    public final /* synthetic */ AnonymousClass61P A02;

    public /* synthetic */ C134376Ej(Context context, AnonymousClass61g r2, AnonymousClass61P r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = context;
    }

    @Override // X.AbstractC14590lg
    public final void accept(Object obj) {
        IDxCListenerShape10S0100000_3_I1 iDxCListenerShape10S0100000_3_I1;
        IDxCListenerShape10S0100000_3_I1 iDxCListenerShape10S0100000_3_I12;
        AlertDialog.Builder message;
        int i;
        AnonymousClass61P r3 = this.A02;
        AnonymousClass61g r5 = this.A01;
        Context context = this.A00;
        AnonymousClass22U r7 = (AnonymousClass22U) obj;
        r5.AaN();
        if (r7.A01 == 5 || (r3.A03.A07(1084) && r7.A00 == 5)) {
            r5.A91();
            AnonymousClass61P.A01(context);
            return;
        }
        int i2 = r7.A01;
        if (i2 == 1 || i2 == 6 || !r3.A04(r7)) {
            int i3 = r7.A01;
            if (i3 != 1 && i3 != 6) {
                iDxCListenerShape10S0100000_3_I1 = new IDxCListenerShape10S0100000_3_I1(r5, 78);
                iDxCListenerShape10S0100000_3_I12 = new IDxCListenerShape10S0100000_3_I1(r5, 80);
                message = new AlertDialog.Builder(context).setTitle(R.string.payment_sticker_upload_failure_dialog_title).setMessage(R.string.payment_sticker_upload_failure_dialog_message);
                i = R.string.payment_sticker_upload_failure_dialog_send_without_sticker_action;
            } else if (r3.A04(r7)) {
                iDxCListenerShape10S0100000_3_I1 = new IDxCListenerShape10S0100000_3_I1(r5, 79);
                iDxCListenerShape10S0100000_3_I12 = new IDxCListenerShape10S0100000_3_I1(r5, 81);
                message = new AlertDialog.Builder(context).setTitle(R.string.payment_background_upload_failure_dialog_title).setMessage(R.string.payment_background_upload_failure_dialog_message);
                i = R.string.payment_background_upload_failure_dialog_send_without_background_action;
            } else {
                r5.ASe(r7.A02);
                return;
            }
        } else {
            iDxCListenerShape10S0100000_3_I1 = new IDxCListenerShape10S0100000_3_I1(r5, 83);
            iDxCListenerShape10S0100000_3_I12 = new IDxCListenerShape10S0100000_3_I1(r5, 82);
            message = new AlertDialog.Builder(context).setTitle(R.string.payment_media_upload_failure_dialog_title).setMessage(R.string.payment_media_upload_failure_dialog_message);
            i = R.string.payment_media_upload_failure_dialog_send_without_media_action;
        }
        AnonymousClass61P.A00(message, iDxCListenerShape10S0100000_3_I1, iDxCListenerShape10S0100000_3_I12, i);
    }
}
