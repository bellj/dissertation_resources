package X;

import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import java.util.ArrayList;

/* renamed from: X.2Kg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49312Kg implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final C20710wC A01;
    public final C15580nU A02;
    public final ExecutorC27271Gr A03;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r1, String str) {
    }

    public /* synthetic */ C49312Kg(AbstractC15710nm r3, C20710wC r4, C15580nU r5, AbstractC14440lR r6) {
        this.A02 = r5;
        this.A00 = r3;
        this.A01 = r4;
        this.A03 = new ExecutorC27271Gr(r6, false);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r14, String str) {
        ArrayList arrayList = new ArrayList();
        AnonymousClass1V8 A0E = r14.A0E("sub_groups");
        if (A0E != null) {
            for (AnonymousClass1V8 r5 : A0E.A0J("group")) {
                try {
                    String A0I = r5.A0I("id", null);
                    AnonymousClass009.A05(A0I);
                    C15580nU A01 = C15380n4.A01(A0I);
                    String A0I2 = r5.A0I("subject", null);
                    long A012 = C28421Nd.A01(r5.A0I("s_t", null), 0) * 1000;
                    boolean z = false;
                    if (r5.A0E("default_sub_group") != null) {
                        z = true;
                    }
                    if (A0I2 == null) {
                        A0I2 = "";
                    }
                    int i = 2;
                    if (z) {
                        i = 3;
                    }
                    arrayList.add(new AnonymousClass1OU(A01, A0I2, i, A012));
                } catch (AnonymousClass1MW e) {
                    AnonymousClass009.A0D(e);
                    this.A00.AaV("Connection/handleInvalidJidReceived", "invalid-jid-received", true);
                }
            }
            this.A03.execute(new RunnableBRunnable0Shape7S0200000_I0_7(this, 11, arrayList));
        }
    }
}
