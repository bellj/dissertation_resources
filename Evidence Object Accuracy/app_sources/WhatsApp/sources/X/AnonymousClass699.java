package X;

import android.content.res.Resources;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.699  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass699 implements AnonymousClass17U {
    public final C241414j A00;
    public final AnonymousClass60Z A01;
    public final C129095xA A02;

    @Override // X.AnonymousClass17U
    public boolean AK8() {
        return true;
    }

    public AnonymousClass699(C241414j r1, AnonymousClass60Z r2, C129095xA r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass17U
    public AnonymousClass1W9 AAI(String str) {
        AnonymousClass1ZY r2;
        C30881Ze r0 = (C30881Ze) this.A00.A08(str);
        if (!(r0 == null || (r2 = r0.A08) == null)) {
            C119775f5 r22 = (C119775f5) r2;
            String A00 = this.A02.A00(r0.A01);
            if ("VISA".equals(r22.A03)) {
                AnonymousClass60Z r6 = this.A01;
                String str2 = r22.A06;
                try {
                    JSONObject A0a = C117295Zj.A0a();
                    A0a.put("alg", "PS256");
                    A0a.put("typ", "JOSE");
                    A0a.put("kid", A00);
                    A0a.put("iat", C117295Zj.A03(r6.A00));
                    String A06 = r6.A06(A0a.toString(), AnonymousClass60Z.A00(A00, null, str2));
                    if (A06 != null) {
                        return new AnonymousClass1W9("trusted-device-info", A06);
                    }
                } catch (JSONException e) {
                    Log.w("PAY: generateTrustedDeviceInfoJwsToken threw creating json string: ", e);
                }
            }
        }
        return null;
    }

    @Override // X.AnonymousClass17U
    public String AHK(Resources resources, AnonymousClass1IR r6, String str) {
        String str2 = r6.A0J;
        if (str2 != null) {
            int A00 = C28421Nd.A00(str2, -1);
            int i = r6.A02;
            if (i == 406 || i == 407 || i == 13 || i == 14) {
                int i2 = R.string.payment_failed_token_bank_network_no_supported;
                if (A00 != 20923) {
                    if (A00 == 2001) {
                        i2 = R.string.payment_failed_insufficient_fund;
                    } else {
                        StringBuilder A0k = C12960it.A0k("BrazilTransactionHelper/getTransactionStatusDetail : unhandled error code (");
                        A0k.append(A00);
                        Log.i(C12960it.A0d(")", A0k));
                    }
                }
                return resources.getString(i2);
            }
        }
        return null;
    }
}
