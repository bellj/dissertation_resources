package X;

import java.nio.ByteBuffer;

/* renamed from: X.3m2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76683m2 extends AbstractC106504vo {
    public static final int A00 = Float.floatToIntBits(Float.NaN);

    @Override // X.AnonymousClass5Xx
    public void AZk(ByteBuffer byteBuffer) {
        ByteBuffer byteBuffer2;
        int position = byteBuffer.position();
        int limit = byteBuffer.limit();
        int i = limit - position;
        int i2 = this.A00.A02;
        if (i2 == 536870912) {
            byteBuffer2 = A00((i / 3) << 2);
            while (position < limit) {
                int floatToIntBits = Float.floatToIntBits((float) (((double) (((byteBuffer.get(position) & 255) << 8) | ((byteBuffer.get(position + 1) & 255) << 16) | ((byteBuffer.get(position + 2) & 255) << 24))) * 4.656612875245797E-10d));
                if (floatToIntBits == A00) {
                    floatToIntBits = Float.floatToIntBits(0.0f);
                }
                byteBuffer2.putInt(floatToIntBits);
                position += 3;
            }
        } else if (i2 == 805306368) {
            byteBuffer2 = A00(i);
            while (position < limit) {
                int floatToIntBits2 = Float.floatToIntBits((float) (((double) ((byteBuffer.get(position) & 255) | ((byteBuffer.get(position + 1) & 255) << 8) | ((byteBuffer.get(position + 2) & 255) << 16) | ((byteBuffer.get(position + 3) & 255) << 24))) * 4.656612875245797E-10d));
                if (floatToIntBits2 == A00) {
                    floatToIntBits2 = Float.floatToIntBits(0.0f);
                }
                byteBuffer2.putInt(floatToIntBits2);
                position += 4;
            }
        } else {
            throw C72463ee.A0D();
        }
        byteBuffer.position(byteBuffer.limit());
        byteBuffer2.flip();
    }
}
