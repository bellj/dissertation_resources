package X;

/* renamed from: X.2ul  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59462ul extends AbstractC16320oo {
    public final AnonymousClass2K4 A00;
    public final AnonymousClass2K3 A01;
    public final C48122Ek A02;
    public final AnonymousClass2K1 A03;
    public final String A04;
    public final String A05;
    public final boolean A06;
    public final boolean A07;

    public C59462ul(AbstractC15710nm r13, C14900mE r14, C16430p0 r15, AnonymousClass2K4 r16, AnonymousClass2K3 r17, C48122Ek r18, AnonymousClass2K1 r19, AnonymousClass1B4 r20, C16340oq r21, C17170qN r22, AnonymousClass018 r23, C14850m9 r24, AbstractC14440lR r25, String str, String str2, boolean z, boolean z2) {
        super(r13, r14, r15, r19, r20, r21, r22, r23, r24, r25);
        this.A04 = str;
        this.A05 = str2;
        this.A01 = r17;
        this.A03 = r19;
        this.A00 = r16;
        this.A02 = r18;
        this.A06 = z;
        this.A07 = z2;
    }
}
