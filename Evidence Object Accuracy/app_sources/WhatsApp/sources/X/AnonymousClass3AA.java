package X;

import android.util.SparseArray;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3AA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AA {
    public static AnonymousClass28D A00(AnonymousClass3H7 r9, AnonymousClass28D r10, AnonymousClass28D r11, List list, List list2, Map map) {
        SparseArray sparseArray;
        AnonymousClass28D r0;
        int A05 = C12960it.A05(C12980iv.A0o(list2));
        Map A0u = C12990iw.A0u(list2, 1);
        String A0g = C12960it.A0g(list2, 2);
        List<AnonymousClass28D> A0L = r10.A0L(143);
        if (A05 < 0 || A05 >= A0L.size()) {
            StringBuilder A0k = C12960it.A0k("[");
            for (AnonymousClass28D r02 : A0L) {
                A0k.append(r02.A01);
                A0k.append(", ");
            }
            A0k.append("]");
            StringBuilder A0k2 = C12960it.A0k("BloksCrash: children-binding index ");
            A0k2.append(A05);
            A0k2.append(" scopeKey: ");
            A0k2.append(A0g);
            A0k2.append(" out of bounds for array of size ");
            A0k2.append(A0L.size());
            A0k2.append(" ");
            throw new IndexOutOfBoundsException(C12960it.A0d(A0k.toString(), A0k2));
        }
        AnonymousClass28D A0U = C12980iv.A0U(A0L, A05);
        LinkedList linkedList = new LinkedList(list);
        linkedList.add(A0g);
        StringBuilder A0h = C12960it.A0h();
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            A0h.append('|');
            A0h.append(A0x);
        }
        String obj = A0h.toString();
        if (A0u != null) {
            Iterator A0n = C12960it.A0n(A0u);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                StringBuilder A0j = C12960it.A0j(C12990iw.A0r(A15));
                A0j.append("#");
                String A0d = C12960it.A0d(obj, A0j);
                Object value = A15.getValue();
                r9.A0A.put(A0d, value);
                Set set = r9.A0C;
                if (set != null && !C87834De.A00(r9.A0B.get(A0d), value)) {
                    set.add(A0d);
                }
                map.put(A0d, A15.getValue());
            }
        }
        AnonymousClass4PX r03 = r9.A03;
        if (r11 == null || (sparseArray = (SparseArray) r03.A01.get(r11.A00)) == null || (r0 = (AnonymousClass28D) sparseArray.get(r9.A02.A00(A0U, linkedList))) == null) {
            return AnonymousClass3AD.A00(new C1092851c(r9, linkedList), A0U);
        }
        return r0;
    }
}
