package X;

import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.6AN  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AN implements AnonymousClass6MQ {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    public AnonymousClass6AN(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r2) {
        this.A00.A3A();
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r5) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        if (!AnonymousClass69E.A02(indiaUpiSendPaymentActivity, "upi-get-vpa", r5.A00, false)) {
            ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m.A06("could not get account vpa: showErrorAndFinish");
            indiaUpiSendPaymentActivity.A39();
        }
    }
}
