package X;

import android.hardware.Camera;
import java.util.List;

/* renamed from: X.63G  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63G implements Camera.PreviewCallback {
    public final /* synthetic */ C129935yX A00;

    public AnonymousClass63G(C129935yX r1) {
        this.A00 = r1;
    }

    @Override // android.hardware.Camera.PreviewCallback
    public void onPreviewFrame(byte[] bArr, Camera camera) {
        if (bArr != null) {
            AnonymousClass60C.A00().A01();
            C129975yb r4 = new C129975yb();
            C129935yX r3 = this.A00;
            int i = r3.A00;
            C129845yO r0 = r3.A01;
            r4.A00(i, bArr, r0.A02, r0.A01);
            List list = r3.A05.A00;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((AbstractC136166Lg) list.get(i2)).AUE(r4);
            }
            camera.addCallbackBuffer(bArr);
        }
    }
}
