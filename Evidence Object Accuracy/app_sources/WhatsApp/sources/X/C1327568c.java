package X;

/* renamed from: X.68c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1327568c implements AnonymousClass1FK {
    public final /* synthetic */ AbstractActivityC121515iQ A00;

    public C1327568c(AbstractActivityC121515iQ r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r5) {
        AbstractActivityC121515iQ r3 = this.A00;
        r3.A07.A0A(C12960it.A0b("incorrect format retry: get-methods request error: ", r5), null);
        r3.A39();
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r5) {
        AbstractActivityC121515iQ r3 = this.A00;
        r3.A07.A0A(C12960it.A0b("incorrect format retry: get-methods response error: ", r5), null);
        r3.A39();
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        ((ActivityC13830kP) this.A00).A05.Aaz(new C123995oH(this), new String[0]);
    }
}
