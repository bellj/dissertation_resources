package X;

import com.whatsapp.Main;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.24s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC461524s extends AbstractActivityC28171Kz {
    public boolean A00 = false;

    public AbstractActivityC461524s() {
        A0R(new AnonymousClass2FF(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            Main main = (Main) this;
            AnonymousClass2FL r1 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r3 = r1.A1E;
            ((ActivityC13830kP) main).A05 = (AbstractC14440lR) r3.ANe.get();
            ((ActivityC13810kN) main).A0C = (C14850m9) r3.A04.get();
            ((ActivityC13810kN) main).A05 = (C14900mE) r3.A8X.get();
            ((ActivityC13810kN) main).A03 = (AbstractC15710nm) r3.A4o.get();
            ((ActivityC13810kN) main).A04 = (C14330lG) r3.A7B.get();
            ((ActivityC13810kN) main).A0B = (AnonymousClass19M) r3.A6R.get();
            ((ActivityC13810kN) main).A0A = (C18470sV) r3.AK8.get();
            ((ActivityC13810kN) main).A06 = (C15450nH) r3.AII.get();
            ((ActivityC13810kN) main).A08 = (AnonymousClass01d) r3.ALI.get();
            ((ActivityC13810kN) main).A0D = (C18810t5) r3.AMu.get();
            ((ActivityC13810kN) main).A09 = (C14820m6) r3.AN3.get();
            ((ActivityC13810kN) main).A07 = (C18640sm) r3.A3u.get();
            ((ActivityC13790kL) main).A05 = (C14830m7) r3.ALb.get();
            ((ActivityC13790kL) main).A0D = (C252718t) r3.A9K.get();
            ((ActivityC13790kL) main).A01 = (C15570nT) r3.AAr.get();
            ((ActivityC13790kL) main).A04 = (C15810nw) r3.A73.get();
            ((ActivityC13790kL) main).A09 = r1.A06();
            ((ActivityC13790kL) main).A06 = (C14950mJ) r3.AKf.get();
            ((ActivityC13790kL) main).A00 = (AnonymousClass12P) r3.A0H.get();
            ((ActivityC13790kL) main).A02 = (C252818u) r3.AMy.get();
            ((ActivityC13790kL) main).A03 = (C22670zS) r3.A0V.get();
            ((ActivityC13790kL) main).A0A = (C21840y4) r3.ACr.get();
            ((ActivityC13790kL) main).A07 = (C15880o3) r3.ACF.get();
            ((ActivityC13790kL) main).A0C = (C21820y2) r3.AHx.get();
            ((ActivityC13790kL) main).A0B = (C15510nN) r3.AHZ.get();
            ((ActivityC13790kL) main).A08 = (C249317l) r3.A8B.get();
            ((AbstractActivityC28171Kz) main).A05 = (C20650w6) r3.A3A.get();
            ((AbstractActivityC28171Kz) main).A09 = (C18470sV) r3.AK8.get();
            ((AbstractActivityC28171Kz) main).A02 = (C20670w8) r3.AMw.get();
            ((AbstractActivityC28171Kz) main).A03 = (C15550nR) r3.A45.get();
            ((AbstractActivityC28171Kz) main).A0B = (C19890uq) r3.ABw.get();
            ((AbstractActivityC28171Kz) main).A0A = (C20710wC) r3.A8m.get();
            ((AbstractActivityC28171Kz) main).A0E = (AbstractC15850o0) r3.ANA.get();
            ((AbstractActivityC28171Kz) main).A04 = (C17050qB) r3.ABL.get();
            ((AbstractActivityC28171Kz) main).A0C = (C20740wF) r3.AIA.get();
            ((AbstractActivityC28171Kz) main).A0D = (C18350sJ) r3.AHX.get();
            ((AbstractActivityC28171Kz) main).A06 = (C20820wN) r3.ACG.get();
            ((AbstractActivityC28171Kz) main).A08 = (C26041Bu) r3.ACL.get();
            ((AbstractActivityC28171Kz) main).A00 = (AnonymousClass2FR) r1.A0P.get();
            ((AbstractActivityC28171Kz) main).A07 = (C20850wQ) r3.ACI.get();
            main.A05 = (C21740xu) r3.AM1.get();
            main.A04 = (C20640w5) r3.AHk.get();
            main.A06 = (C20690wA) r3.AJa.get();
            main.A0B = (WhatsAppLibLoader) r3.ANa.get();
            main.A01 = (C22690zU) r3.A35.get();
            main.A0A = (C16490p7) r3.ACJ.get();
            main.A07 = (C237412v) r3.A3k.get();
            main.A08 = new C18990tO((C18980tN) r1.A0G.get());
            main.A09 = (C231010j) r3.A3p.get();
            main.A0C = C18000rk.A00(r3.A8G);
        }
    }
}
