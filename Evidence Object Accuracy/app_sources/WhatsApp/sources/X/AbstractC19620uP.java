package X;

import java.util.Locale;

/* renamed from: X.0uP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC19620uP {
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final AnonymousClass1Q5 A02;

    public AbstractC19620uP(C14830m7 r10, C14820m6 r11, C16120oU r12, C21230x5 r13, AbstractC21180x0 r14, String str, int i) {
        this.A00 = r10;
        this.A01 = r11;
        AnonymousClass1Q5 r2 = new AnonymousClass1Q5(r10, r12, r13, r14, str, i);
        r2.A06.A03 = true;
        this.A02 = r2;
    }

    public final void A00(int i, String str) {
        C16700pc.A0E(str, 1);
        AnonymousClass1Q5 r3 = this.A02;
        r3.A04(i, str, true);
        A01(i, "timestamp_ms", System.currentTimeMillis());
        AbstractC21180x0 r4 = r3.A08;
        int i2 = r3.A06.A05;
        r4.AL1("is_debug_build", i2, i, false);
        String string = this.A01.A00.getString("pref_graphql_domain", "whatsapp.com");
        C16700pc.A0B(string);
        String lowerCase = string.toLowerCase(Locale.ROOT);
        C16700pc.A0B(lowerCase);
        r4.AL1("is_graphql_prod", i2, i, lowerCase.equals("whatsapp.com"));
    }

    public final void A01(int i, String str, long j) {
        AnonymousClass1Q5 r0 = this.A02;
        r0.A08.AKz(str, r0.A06.A05, i, j);
    }

    public final void A02(int i, String str, String str2) {
        C16700pc.A0E(str2, 2);
        AnonymousClass1Q5 r0 = this.A02;
        r0.A08.AL0(str, str2, r0.A06.A05, i);
    }
}
