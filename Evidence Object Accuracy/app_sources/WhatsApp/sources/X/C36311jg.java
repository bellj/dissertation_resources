package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.1jg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C36311jg {
    public final AnonymousClass5YC A00;

    public C36311jg(AnonymousClass5YC r1) {
        C13020j0.A01(r1);
        this.A00 = r1;
    }

    public LatLng A00() {
        Parcelable parcelable;
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A02 = r2.A02(4, r2.A01());
            Parcelable.Creator creator = LatLng.CREATOR;
            if (A02.readInt() == 0) {
                parcelable = null;
            } else {
                parcelable = (Parcelable) creator.createFromParcel(A02);
            }
            LatLng latLng = (LatLng) parcelable;
            A02.recycle();
            return latLng;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public Object A01() {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A02 = r2.A02(30, r2.A01());
            IObjectWrapper A01 = AbstractBinderC79573qo.A01(A02.readStrongBinder());
            A02.recycle();
            return BinderC56502l7.A00(A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public String A02() {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A02 = r2.A02(2, r2.A01());
            String readString = A02.readString();
            A02.recycle();
            return readString;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A03() {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            r2.A03(12, r2.A01());
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A04() {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            r2.A03(11, r2.A01());
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A05(C36331ji r4) {
        C65873Li r2;
        Parcel parcel;
        try {
            if (r4 == null) {
                r2 = (C65873Li) this.A00;
                parcel = r2.A01();
                parcel.writeStrongBinder(null);
            } else {
                IObjectWrapper iObjectWrapper = r4.A00;
                r2 = (C65873Li) this.A00;
                parcel = r2.A01();
                C65183In.A00(iObjectWrapper, parcel);
            }
            r2.A03(18, parcel);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A06(LatLng latLng) {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, latLng);
            r2.A03(3, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A07(Object obj) {
        try {
            AnonymousClass5YC r2 = this.A00;
            BinderC56502l7 r0 = new BinderC56502l7(obj);
            C65873Li r22 = (C65873Li) r2;
            Parcel A01 = r22.A01();
            C65183In.A00(r0, A01);
            r22.A03(29, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A08(String str) {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A01 = r2.A01();
            A01.writeString(str);
            r2.A03(5, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A09(boolean z) {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A01 = r2.A01();
            A01.writeInt(z ? 1 : 0);
            r2.A03(14, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public boolean A0A() {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A02 = r2.A02(15, r2.A01());
            boolean z = false;
            if (A02.readInt() != 0) {
                z = true;
            }
            A02.recycle();
            return z;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C36311jg)) {
            return false;
        }
        try {
            AnonymousClass5YC r2 = this.A00;
            AnonymousClass5YC r0 = ((C36311jg) obj).A00;
            C65873Li r22 = (C65873Li) r2;
            Parcel A01 = r22.A01();
            C65183In.A00(r0, A01);
            Parcel A02 = r22.A02(16, A01);
            boolean z = false;
            if (A02.readInt() != 0) {
                z = true;
            }
            A02.recycle();
            return z;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public int hashCode() {
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A02 = r2.A02(17, r2.A01());
            int readInt = A02.readInt();
            A02.recycle();
            return readInt;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
