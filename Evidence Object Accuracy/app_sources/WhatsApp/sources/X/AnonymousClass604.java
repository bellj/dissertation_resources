package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.604  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass604 {
    public final C15570nT A00;
    public final C16590pI A01;
    public final AnonymousClass018 A02;
    public final C17070qD A03;
    public final AnonymousClass14X A04;

    public AnonymousClass604(C15570nT r1, C16590pI r2, AnonymousClass018 r3, C17070qD r4, AnonymousClass14X r5) {
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
    }

    public static boolean A00(AnonymousClass1IR r2) {
        int i = r2.A03;
        return i == 100 || i == 200;
    }

    public final CharSequence A01(C14830m7 r9, long j) {
        if (j <= 0) {
            return null;
        }
        Context context = this.A01.A00;
        Object[] A1b = C12970iu.A1b();
        AnonymousClass018 r3 = this.A02;
        return C12960it.A0X(context, C38121nY.A05(r3, AnonymousClass1MY.A02(r3, r9.A02(j)), AnonymousClass3JK.A00(r3, r9.A02(j))), A1b, 0, R.string.time_and_date);
    }
}
