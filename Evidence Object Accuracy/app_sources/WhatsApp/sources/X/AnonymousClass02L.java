package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.02L  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass02L extends AnonymousClass02M {
    public final C06040Ry A00;

    public AnonymousClass02L(AnonymousClass02K r4) {
        this.A00 = new C06040Ry(new AnonymousClass0SC(r4).A00(), new AnonymousClass0Z2(this));
    }

    public AnonymousClass02L(C04770Mz r3) {
        this.A00 = new C06040Ry(r3, new AnonymousClass0Z2(this));
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.A02.size();
    }

    public Object A0E(int i) {
        return this.A00.A02.get(i);
    }

    public void A0F(List list) {
        C06040Ry r5 = this.A00;
        int i = r5.A00 + 1;
        r5.A00 = i;
        List list2 = r5.A01;
        if (list == list2) {
            return;
        }
        if (list == null) {
            int size = list2.size();
            r5.A01 = null;
            r5.A02 = Collections.emptyList();
            r5.A04.AUs(0, size);
        } else if (list2 == null) {
            r5.A01 = list;
            r5.A02 = Collections.unmodifiableList(list);
            r5.A04.ARP(0, list.size());
        } else {
            r5.A03.A01.execute(new RunnableC10040dv(r5, list2, list, i));
        }
    }
}
