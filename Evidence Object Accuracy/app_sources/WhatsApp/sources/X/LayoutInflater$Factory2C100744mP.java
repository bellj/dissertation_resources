package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import com.whatsapp.WaTextView;

/* renamed from: X.4mP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class LayoutInflater$Factory2C100744mP implements LayoutInflater.Factory2 {
    public final AnonymousClass025 A00;

    public LayoutInflater$Factory2C100744mP(AnonymousClass025 r1) {
        this.A00 = r1;
    }

    @Override // android.view.LayoutInflater.Factory2
    public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        if ("TextView".equals(str)) {
            return new WaTextView(context, attributeSet);
        }
        return this.A00.A04(null, str, context, attributeSet);
    }

    @Override // android.view.LayoutInflater.Factory
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView(null, str, context, attributeSet);
    }
}
