package X;

import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;

/* renamed from: X.485  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass485 extends AnonymousClass2NO {
    public final /* synthetic */ C48782Ht A00;

    public AnonymousClass485(C48782Ht r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2NO, X.AnonymousClass2NP
    public void AQ0(VoipPhysicalCamera voipPhysicalCamera) {
        this.A00.A02.sendEmptyMessage(1);
    }

    @Override // X.AnonymousClass2NO, X.AnonymousClass2NP
    public void AVt(VoipPhysicalCamera voipPhysicalCamera) {
        this.A00.A02.sendEmptyMessage(2);
    }

    @Override // X.AnonymousClass2NO, X.AnonymousClass2NP
    public void AXz(VoipPhysicalCamera voipPhysicalCamera) {
        this.A00.A02.sendEmptyMessage(2);
    }
}
