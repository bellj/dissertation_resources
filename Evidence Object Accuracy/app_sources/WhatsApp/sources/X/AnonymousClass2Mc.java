package X;

import android.view.animation.Animation;
import com.whatsapp.HomeActivity;

/* renamed from: X.2Mc  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Mc extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ HomeActivity A00;

    public AnonymousClass2Mc(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A0H.setIconified(false);
    }
}
