package X;

/* renamed from: X.0cJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09070cJ implements Runnable {
    public final /* synthetic */ AnonymousClass0XR A00;

    public RunnableC09070cJ(AnonymousClass0XR r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        C02360Bs r1 = this.A00.A0E;
        if (r1 != null) {
            r1.A0B = true;
            r1.requestLayout();
        }
    }
}
