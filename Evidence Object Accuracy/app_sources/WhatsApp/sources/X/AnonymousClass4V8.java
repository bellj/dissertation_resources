package X;

/* renamed from: X.4V8  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4V8 {
    public final int A00;

    public /* synthetic */ AnonymousClass4V8(int i) {
        this.A00 = i;
    }

    public float A00() {
        if (this instanceof AnonymousClass48G) {
            return ((AnonymousClass48G) this).A00;
        }
        if (this instanceof AnonymousClass48F) {
            return ((AnonymousClass48F) this).A00;
        }
        if (!(this instanceof AnonymousClass48E)) {
            return ((AnonymousClass48D) this).A00;
        }
        return ((AnonymousClass48E) this).A00;
    }
}
