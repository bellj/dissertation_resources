package X;

import android.view.View;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2iU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55452iU extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ AnonymousClass2Ew A00;

    public RunnableC55452iU(AnonymousClass2Ew r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass2Ew r3 = this.A00;
        View childAt = r3.A0F.getChildAt(0);
        if (childAt != null && r3.A0F.getFirstVisiblePosition() == 0) {
            int top = childAt.getTop();
            int i = r3.A08;
            if (top != i) {
                r3.A0F.setSelectionFromTop(0, i);
                r3.A0F.post(this);
                return;
            }
            ((ActivityC000900k) AnonymousClass12P.A02(r3)).A0d();
            r3.A0F.setOnScrollListener(new C102224on(this));
        }
    }
}
