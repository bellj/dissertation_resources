package X;

import android.util.Log;

/* renamed from: X.3cD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C70993cD implements AnonymousClass5WN {
    public final /* synthetic */ ActivityC000800j A00;
    public final /* synthetic */ AnonymousClass5TH A01;
    public final /* synthetic */ C64173En A02;

    public C70993cD(ActivityC000800j r1, AnonymousClass5TH r2, C64173En r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WN
    public void AVC(C28671On r5) {
        C16700pc.A0E(r5, 0);
        ActivityC000800j r3 = this.A00;
        if (r3 != null) {
            C28701Oq.A02(r3, r5, C14220l3.A01, this.A02, C71373cp.A00);
            this.A01.AOM(5);
            return;
        }
        throw C12980iv.A0n("null cannot be cast to non-null type android.content.Context");
    }

    @Override // X.AnonymousClass5WN
    public void AVH(C91064Qh r3) {
        C16700pc.A0E(r3, 0);
        if (r3.A00 == 5) {
            Log.e("AvatarEditorLauncher", "should launch as an async action");
        }
        this.A01.AOM(r3.A00);
    }
}
