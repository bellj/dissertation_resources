package X;

import android.view.animation.Animation;

/* renamed from: X.2oD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58142oD extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ int A00;
    public final /* synthetic */ ViewTreeObserver$OnPreDrawListenerC66463Nr A01;

    public C58142oD(ViewTreeObserver$OnPreDrawListenerC66463Nr r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass238 r2 = this.A01.A01;
        r2.A03.setTranscriptMode(this.A00);
        r2.A0D = false;
    }
}
