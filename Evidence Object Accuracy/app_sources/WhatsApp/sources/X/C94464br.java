package X;

/* renamed from: X.4br  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94464br {
    public static final C94464br A0D = new C94464br();
    public int A00;
    public C91134Qo A01;
    public C95294dS A02;
    public C94464br A03;
    public C94464br A04;
    public short A05;
    public short A06;
    public short A07;
    public short A08;
    public short A09;
    public short A0A;
    public int[] A0B;
    public int[] A0C;

    public final void A00(int i, int i2, int i3) {
        int[] iArr = this.A0B;
        if (iArr == null) {
            iArr = new int[6];
            this.A0B = iArr;
        }
        int i4 = iArr[0];
        int i5 = i4 + 2;
        int length = iArr.length;
        if (i5 >= length) {
            int[] iArr2 = new int[length + 6];
            System.arraycopy(iArr, 0, iArr2, 0, length);
            this.A0B = iArr2;
            iArr = iArr2;
        }
        int i6 = i4 + 1;
        iArr[i6] = i;
        int i7 = i6 + 1;
        iArr[i7] = i2 | i3;
        iArr[0] = i7;
    }

    public final void A01(C95044cz r4, int i, boolean z) {
        if ((this.A05 & 4) != 0) {
            int i2 = this.A00 - i;
            if (z) {
                r4.A03(i2);
            } else {
                r4.A04(i2);
            }
        } else if (z) {
            A00(i, 536870912, r4.A00);
            r4.A03(-1);
        } else {
            A00(i, 268435456, r4.A00);
            r4.A04(-1);
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: LoopRegionVisitor
        jadx.core.utils.exceptions.JadxOverflowException: LoopRegionVisitor.assignOnlyInLoop endless recursion
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    public final void A02(short r8) {
        /*
            r7 = this;
            X.4br r6 = X.C94464br.A0D
            r7.A04 = r6
            r5 = r7
        L_0x0005:
            if (r5 == r6) goto L_0x0030
            X.4br r4 = r5.A04
            r0 = 0
            r5.A04 = r0
            short r0 = r5.A0A
            if (r0 != 0) goto L_0x002e
            r5.A0A = r8
            X.4Qo r3 = r5.A01
            r2 = r3
        L_0x0015:
            if (r3 == 0) goto L_0x002e
            short r0 = r5.A05
            r0 = r0 & 16
            if (r0 == 0) goto L_0x0024
            X.4Qo r0 = r2.A00
            if (r3 != r0) goto L_0x0024
        L_0x0021:
            X.4Qo r3 = r3.A00
            goto L_0x0015
        L_0x0024:
            X.4br r1 = r3.A02
            X.4br r0 = r1.A04
            if (r0 != 0) goto L_0x0021
            r1.A04 = r4
            r4 = r1
            goto L_0x0021
        L_0x002e:
            r5 = r4
            goto L_0x0005
        L_0x0030:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C94464br.A02(short):void");
    }

    public final boolean A03(byte[] bArr, int i) {
        int A0O;
        this.A05 = (short) (this.A05 | 4);
        this.A00 = i;
        int[] iArr = this.A0B;
        boolean z = false;
        if (iArr != null) {
            for (int i2 = iArr[0]; i2 > 0; i2 -= 2) {
                int i3 = iArr[i2 - 1];
                int i4 = iArr[i2];
                int i5 = i - i3;
                int i6 = 268435455 & i4;
                if ((i4 & -268435456) == 268435456) {
                    if (i5 < -32768 || i5 > 32767) {
                        int i7 = bArr[i3] & 255;
                        int i8 = i7 + 20;
                        if (i7 < 198) {
                            i8 = i7 + 49;
                        }
                        bArr[i3] = (byte) i8;
                        z = true;
                    }
                    A0O = i6 + 1;
                    C72463ee.A0W(bArr, i5, i6);
                } else {
                    A0O = C72453ed.A0O(bArr, i6, i5);
                }
                bArr[A0O] = (byte) i5;
            }
        }
        return z;
    }

    public String toString() {
        return C12960it.A0f(C12960it.A0k("L"), System.identityHashCode(this));
    }
}
