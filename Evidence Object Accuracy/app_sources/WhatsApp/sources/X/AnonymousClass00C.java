package X;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* renamed from: X.00C  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00C {
    public static String A00() {
        StringBuilder sb = new StringBuilder();
        sb.append("2.22.17.70");
        sb.append("(");
        sb.append(388062852L);
        sb.append(")");
        return sb.toString();
    }

    public static String A01() {
        StringBuilder sb = new StringBuilder("2.22.17.70");
        sb.append('-');
        sb.append("website");
        sb.append("-");
        sb.append("release");
        return sb.toString();
    }

    public static String A02(String str) {
        String str2 = "";
        try {
            Process start = new ProcessBuilder("/system/bin/getprop", str).redirectErrorStream(true).start();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(start.getInputStream()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    str2 = readLine;
                } else {
                    start.destroy();
                    return str2;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return str2;
        }
    }
}
