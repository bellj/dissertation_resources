package X;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.6Do  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134166Do implements AbstractC41521tf {
    public final /* synthetic */ C122175l2 A00;

    @Override // X.AbstractC41521tf
    public void AQV() {
    }

    public C134166Do(C122175l2 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.A0H.getResources().getDimensionPixelSize(R.dimen.payment_bitmap_image_url_max_size);
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r4) {
        if (bitmap != null) {
            this.A00.A00.setImageBitmap(bitmap);
        } else {
            Adu(view);
        }
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C122175l2 r3 = this.A00;
        Drawable A01 = AnonymousClass2GE.A01(r3.A0H.getContext(), R.drawable.cart, R.color.order_reference_color);
        WaImageView waImageView = r3.A00;
        waImageView.setImageDrawable(A01);
        waImageView.setScaleX(0.5f);
        waImageView.setScaleY(0.5f);
    }
}
