package X;

/* renamed from: X.2Ae  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47322Ae {
    public AnonymousClass5W5 A00;
    public AnonymousClass33J A01;
    public boolean A02 = false;
    public final float A03;
    public final C454921v A04;
    public final AnonymousClass3EC A05;
    public final C454721t A06;
    public final C93694aa A07;
    public final boolean A08;

    public C47322Ae(C454921v r3, AnonymousClass3EC r4, C454721t r5, C93694aa r6, float f, boolean z) {
        this.A06 = r5;
        this.A04 = r3;
        this.A05 = r4;
        this.A03 = f;
        this.A08 = z;
        this.A07 = r6;
        if (z) {
            for (AnonymousClass33J r0 : r5.A01()) {
                r0.A0R(r6);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0019, code lost:
        if (r2 != 6) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00(android.view.MotionEvent r13, float r14, int r15, boolean r16) {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47322Ae.A00(android.view.MotionEvent, float, int, boolean):boolean");
    }
}
