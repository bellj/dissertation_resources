package X;

import android.os.SystemClock;
import com.whatsapp.R;

/* renamed from: X.34O  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass34O extends AbstractActivityC862746m implements AbstractC14730lx, AnonymousClass5RQ {
    public long A00;
    public boolean A01;

    @Override // X.AnonymousClass34P
    public void A2g(boolean z) {
        String str;
        A2C(R.string.contact_qr_wait);
        this.A0X = true;
        this.A01 = z;
        this.A00 = SystemClock.elapsedRealtime();
        AnonymousClass3ZF r11 = new AnonymousClass3ZF(((ActivityC13810kN) this).A05, ((AnonymousClass34P) this).A0K, new AnonymousClass4OD(((ActivityC13810kN) this).A09, this));
        C17220qS r10 = r11.A01;
        String A01 = r10.A01();
        AnonymousClass1W9[] r8 = new AnonymousClass1W9[2];
        boolean A1a = C12990iw.A1a("type", "contact", r8);
        if (z) {
            str = "revoke";
        } else {
            str = "get";
        }
        r8[1] = new AnonymousClass1W9("action", str);
        AnonymousClass1V8 r3 = new AnonymousClass1V8("qr", r8);
        AnonymousClass1W9[] r2 = new AnonymousClass1W9[3];
        C12960it.A1M("id", A01, r2, A1a ? 1 : 0);
        C12960it.A1M("xmlns", "w:qr", r2, 1);
        C12960it.A1M("type", "set", r2, 2);
        r10.A0A(r11, new AnonymousClass1V8(r3, "iq", r2), A01, 215, 32000);
    }
}
