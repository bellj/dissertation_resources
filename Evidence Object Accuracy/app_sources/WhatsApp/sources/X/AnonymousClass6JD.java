package X;

import java.util.ArrayList;

/* renamed from: X.6JD  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6JD implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass016 A01;
    public final /* synthetic */ C129685y8 A02;

    public /* synthetic */ AnonymousClass6JD(AnonymousClass016 r1, C129685y8 r2, int i) {
        this.A02 = r2;
        this.A00 = i;
        this.A01 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C129685y8 r6 = this.A02;
        int i = this.A00;
        AnonymousClass016 r4 = this.A01;
        ArrayList A0l = C12960it.A0l();
        AnonymousClass61S.A03("action", "novi-get-account-restriction", A0l);
        r6.A02.A0B(C117305Zk.A09(r4, r6, 7), C117315Zl.A0B("account", A0l), "get", i);
    }
}
