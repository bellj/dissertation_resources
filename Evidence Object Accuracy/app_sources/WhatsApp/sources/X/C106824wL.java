package X;

import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/* renamed from: X.4wL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106824wL implements AbstractC116785Ww {
    public static final C100614mC A0U;
    public static final byte[] A0V = {-94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12};
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public long A0B;
    public AbstractC14070ko A0C;
    public AnonymousClass4Y3 A0D;
    public C95304dT A0E;
    public boolean A0F;
    public boolean A0G;
    public AnonymousClass5X6[] A0H;
    public AnonymousClass5X6[] A0I;
    public final SparseArray A0J;
    public final C93914ax A0K = new C93914ax();
    public final C95304dT A0L = C95304dT.A05(16);
    public final C95304dT A0M = new C95304dT();
    public final C95304dT A0N = C95304dT.A05(5);
    public final C95304dT A0O = new C95304dT(C95464dl.A02);
    public final C95304dT A0P;
    public final ArrayDeque A0Q;
    public final ArrayDeque A0R;
    public final List A0S = Collections.unmodifiableList(Collections.emptyList());
    public final byte[] A0T;

    static {
        C93844ap A00 = C93844ap.A00();
        A00.A0R = "application/x-emsg";
        A0U = new C100614mC(A00);
    }

    public C106824wL() {
        byte[] bArr = new byte[16];
        this.A0T = bArr;
        this.A0P = new C95304dT(bArr);
        this.A0Q = new ArrayDeque();
        this.A0R = new ArrayDeque();
        this.A0J = new SparseArray();
        this.A08 = -9223372036854775807L;
        this.A0A = -9223372036854775807L;
        this.A0B = -9223372036854775807L;
        this.A0C = AbstractC14070ko.A00;
        this.A0I = new AnonymousClass5X6[0];
        this.A0H = new AnonymousClass5X6[0];
    }

    public static C112295Cv A00(List list) {
        int size = list.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            C76873mN r1 = (C76873mN) list.get(i);
            if (((AbstractC93864as) r1).A00 == 1886614376) {
                if (arrayList == null) {
                    arrayList = C12960it.A0l();
                }
                byte[] bArr = r1.A00.A02;
                C95304dT r11 = new C95304dT(bArr);
                if (r11.A00 >= 32 && C95304dT.A03(r11, 0) == C95304dT.A00(r11) + 4 && r11.A07() == 1886614376) {
                    int A07 = (r11.A07() >> 24) & 255;
                    if (A07 > 1) {
                        Log.w("PsshAtomUtil", C12960it.A0W(A07, "Unsupported pssh version: "));
                    } else {
                        UUID uuid = new UUID(r11.A0H(), r11.A0H());
                        if (A07 == 1) {
                            r11.A0T(r11.A0E() << 4);
                        }
                        int A0E = r11.A0E();
                        if (A0E == C95304dT.A00(r11)) {
                            byte[] bArr2 = new byte[A0E];
                            r11.A0V(bArr2, 0, A0E);
                            UUID uuid2 = new C89924Lx(uuid, bArr2).A00;
                            if (uuid2 != null) {
                                arrayList.add(new C100574m8("video/mp4", uuid2, bArr));
                            }
                        }
                    }
                }
                Log.w("FragmentedMp4Extractor", "Skipped pssh atom (failed to extract uuid)");
            }
        }
        if (arrayList != null) {
            return new C112295Cv(null, (C100574m8[]) arrayList.toArray(new C100574m8[0]), false);
        }
        return null;
    }

    public static void A01(AnonymousClass4UI r5, C95304dT r6, int i) {
        int A03 = C95304dT.A03(r6, i + 8) & 16777215;
        if ((A03 & 1) == 0) {
            boolean A1S = C12960it.A1S(A03 & 2);
            int A0E = r6.A0E();
            if (A0E == 0) {
                Arrays.fill(r5.A0F, 0, r5.A00, false);
                return;
            }
            int i2 = r5.A00;
            if (A0E == i2) {
                Arrays.fill(r5.A0F, 0, A0E, A1S);
                int A00 = C95304dT.A00(r6);
                C95304dT r2 = r5.A0H;
                r2.A0Q(A00);
                r5.A07 = true;
                r5.A09 = true;
                r6.A0V(r2.A02, 0, r2.A00);
                r2.A0S(0);
                r5.A09 = false;
                return;
            }
            StringBuilder A0k = C12960it.A0k("Senc sample count ");
            A0k.append(A0E);
            throw AnonymousClass496.A00(C12960it.A0e(" is different from fragment sample count", A0k, i2));
        }
        throw AnonymousClass496.A00("Overriding TrackEncryptionBox parameters is unsupported.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:128:0x036c, code lost:
        if (r32 == false) goto L_0x036e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x0642, code lost:
        r51.A02 = 0;
        r51.A00 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x0647, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(long r52) {
        /*
        // Method dump skipped, instructions count: 1608
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106824wL.A02(long):void");
    }

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r8) {
        this.A0C = r8;
        this.A02 = 0;
        this.A00 = 0;
        AnonymousClass5X6[] r0 = new AnonymousClass5X6[2];
        this.A0I = r0;
        int i = 0;
        int i2 = 100;
        AnonymousClass5X6[] r3 = (AnonymousClass5X6[]) Arrays.copyOf(r0, 0);
        this.A0I = r3;
        for (AnonymousClass5X6 r1 : r3) {
            r1.AA6(A0U);
        }
        List list = this.A0S;
        AnonymousClass5X6[] r02 = new AnonymousClass5X6[list.size()];
        this.A0H = r02;
        while (i < r02.length) {
            AnonymousClass5X6 Af4 = this.A0C.Af4(i2, 3);
            Af4.AA6((C100614mC) list.get(i));
            r02 = this.A0H;
            r02[i] = Af4;
            i++;
            i2++;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r15v0, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0421, code lost:
        if (r9 == 1701671783) goto L_0x0423;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x04a1, code lost:
        r12 = 4;
        r11 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x04a7, code lost:
        if (r30.A02 != 3) goto L_0x0538;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x04ab, code lost:
        if (r4.A06 != false) goto L_0x04f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x04ad, code lost:
        r1 = r4.A05.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x04b1, code lost:
        r6 = r4.A01;
        r2 = r1[r6];
        r30.A06 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x04b9, code lost:
        if (r6 >= r4.A03) goto L_0x04f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x04bb, code lost:
        r31.Ae3(r2);
        r1 = r4.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x04c2, code lost:
        if (r1 == null) goto L_0x04e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x04c4, code lost:
        r7 = r4.A08;
        r6 = r7.A0H;
        r1 = r1.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x04ca, code lost:
        if (r1 == 0) goto L_0x04cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x04cc, code lost:
        r6.A0T(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x04cf, code lost:
        r2 = r4.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x04d3, code lost:
        if (r7.A07 == false) goto L_0x04e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x04d9, code lost:
        if (r7.A0F[r2] == false) goto L_0x04e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x04db, code lost:
        r6.A0T(r6.A0F() * 6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x04e8, code lost:
        if (r4.A03() != false) goto L_0x04ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x04ea, code lost:
        r30.A0D = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x04ec, code lost:
        r30.A02 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x04ee, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x04f0, code lost:
        r1 = r4.A08.A0B;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x04fb, code lost:
        if (r4.A05.A03.A02 != 1) goto L_0x0505;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x04fd, code lost:
        r30.A06 = r2 - 8;
        r31.Ae3(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x0505, code lost:
        r2 = "audio/ac4".equals(r4.A05.A03.A07.A0T);
        r1 = r30.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0515, code lost:
        if (r2 == false) goto L_0x060b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0517, code lost:
        r30.A04 = r4.A00(r1, 7);
        r1 = r30.A06;
        r2 = r30.A0P;
        X.C95124dB.A01(r2, r1);
        r4.A07.AbC(r2, 7);
        r2 = r30.A04 + 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x052d, code lost:
        r30.A04 = r2;
        r30.A06 += r2;
        r30.A02 = 4;
        r30.A05 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x0538, code lost:
        r2 = r4.A05;
        r10 = r2.A03;
        r9 = r4.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x0540, code lost:
        if (r4.A06 != false) goto L_0x05fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x0542, code lost:
        r1 = r2.A07[r4.A01];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x0548, code lost:
        r3 = r10.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x054a, code lost:
        if (r3 == 0) goto L_0x0611;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x054c, code lost:
        r14 = r30.A0N;
        r5 = r14.A02;
        r5[0] = 0;
        r5[1] = 0;
        r5[2] = 0;
        r17 = r3 + 1;
        r13 = 4 - r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x055a, code lost:
        r3 = r30.A04;
        r6 = r30.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x055e, code lost:
        if (r3 >= r6) goto L_0x0622;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x0560, code lost:
        r6 = r30.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x0562, code lost:
        if (r6 != 0) goto L_0x05b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x0564, code lost:
        r31.readFully(r5, r13, r17);
        r3 = X.C95304dT.A03(r14, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:268:0x056d, code lost:
        if (r3 < r11) goto L_0x0692;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:0x056f, code lost:
        r30.A05 = r3 - 1;
        r3 = r30.A0O;
        r3.A0S(0);
        r9.AbC(r3, r12);
        r9.AbC(r14, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:270:0x0581, code lost:
        if (r30.A0H.length <= 0) goto L_0x05b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x0583, code lost:
        r6 = r10.A07.A0T;
        r16 = r5[r12];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x058f, code lost:
        if ("video/avc".equals(r6) == false) goto L_0x0596;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x0594, code lost:
        if ((r16 & 31) == 6) goto L_0x05a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x059c, code lost:
        if ("video/hevc".equals(r6) == false) goto L_0x05b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x05a3, code lost:
        if (((r16 & 126) >> r11) != 39) goto L_0x05b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x05a5, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x05a6, code lost:
        r30.A0G = r3;
        r30.A04 += 5;
        r30.A06 += r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x05b4, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x05b8, code lost:
        if (r30.A0G == false) goto L_0x05f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x05ba, code lost:
        r11 = r30.A0M;
        r11.A0Q(r6);
        r31.readFully(r11.A02, 0, r6);
        r9.AbC(r11, r30.A05);
        r6 = r30.A05;
        r12 = X.C95464dl.A00(r11.A02, r11.A00);
        r11.A0S("video/hevc".equals(r10.A07.A0T) ? 1 : 0);
        r11.A0R(r12);
        X.C92934Yb.A00(r11, r30.A0H, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x05e8, code lost:
        r30.A04 += r6;
        r30.A05 -= r6;
        r12 = 4;
        r11 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:286:0x05f6, code lost:
        r6 = r9.AbF(r31, r6, 0, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x05fb, code lost:
        r3 = r4.A08;
        r5 = r4.A01;
        r1 = r3.A0D[r5] + ((long) r3.A0A[r5]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:0x060b, code lost:
        r2 = r4.A00(r1, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x0611, code lost:
        r3 = r30.A04;
        r6 = r30.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:0x0615, code lost:
        if (r3 >= r6) goto L_0x0622;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x0617, code lost:
        r30.A04 += r9.AbF(r31, r6 - r3, 0, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x0624, code lost:
        if (r4.A06 != false) goto L_0x0670;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:294:0x0626, code lost:
        r7 = r4.A05.A04[r4.A01];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x062e, code lost:
        r5 = r4.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:296:0x0632, code lost:
        if (r5 == null) goto L_0x0637;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:297:0x0634, code lost:
        r7 = r7 | 1073741824;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x0637, code lost:
        if (r5 == null) goto L_0x066e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x0639, code lost:
        r3 = r5.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:300:0x063b, code lost:
        r9.AbG(r3, r7, r6, 0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:0x0644, code lost:
        r5 = r30.A0R;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:302:0x064a, code lost:
        if (r5.isEmpty() != false) goto L_0x067c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x064c, code lost:
        r5 = (X.C89914Lw) r5.removeFirst();
        r3 = r30.A03;
        r10 = r5.A00;
        r30.A03 = r3 - r10;
        r12 = r5.A01 + r1;
        r6 = r30.A0I;
        r5 = r6.length;
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x0660, code lost:
        if (r3 >= r5) goto L_0x0644;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x0662, code lost:
        r6[r3].AbG(null, 1, r10, r30.A03, r12);
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x066e, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:308:0x0678, code lost:
        if (r4.A08.A0G[r4.A01] == false) goto L_0x062e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x067a, code lost:
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:311:0x0680, code lost:
        if (r4.A03() != false) goto L_0x0685;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:312:0x0682, code lost:
        r30.A0D = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x0685, code lost:
        r30.A02 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:316:0x0698, code lost:
        throw X.AnonymousClass496.A00("Invalid NAL length");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:400:?, code lost:
        return 0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x02f0  */
    /* JADX WARNING: Removed duplicated region for block: B:349:0x06c3 A[SYNTHETIC] */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int AZn(X.AnonymousClass5Yf r31, X.AnonymousClass4IG r32) {
        /*
        // Method dump skipped, instructions count: 1738
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106824wL.AZn(X.5Yf, X.4IG):int");
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        SparseArray sparseArray = this.A0J;
        int size = sparseArray.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass4Y3) sparseArray.valueAt(i)).A02();
        }
        this.A0R.clear();
        this.A03 = 0;
        this.A0A = j2;
        this.A0Q.clear();
        this.A02 = 0;
        this.A00 = 0;
    }

    @Override // X.AbstractC116785Ww
    public boolean Ae5(AnonymousClass5Yf r2) {
        return AnonymousClass4Z0.A00(r2, true);
    }
}
