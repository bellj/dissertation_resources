package X;

import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.66Y  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66Y implements AbstractC116435Vk {
    public final /* synthetic */ PinBottomSheetDialogFragment A00;

    public AnonymousClass66Y(PinBottomSheetDialogFragment pinBottomSheetDialogFragment) {
        this.A00 = pinBottomSheetDialogFragment;
    }

    @Override // X.AbstractC116435Vk
    public void AOH(String str) {
        if (str.length() == 6) {
            PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
            if (pinBottomSheetDialogFragment.A0B != null && pinBottomSheetDialogFragment.A00 <= pinBottomSheetDialogFragment.A07.A00()) {
                pinBottomSheetDialogFragment.A0B.AOP(str);
            }
        }
    }

    @Override // X.AbstractC116435Vk
    public void AT5(String str) {
        if (str.length() == 6) {
            PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A00;
            if (pinBottomSheetDialogFragment.A0B != null && pinBottomSheetDialogFragment.A00 <= pinBottomSheetDialogFragment.A07.A00()) {
                pinBottomSheetDialogFragment.A0B.AOP(str);
            }
        }
    }
}
