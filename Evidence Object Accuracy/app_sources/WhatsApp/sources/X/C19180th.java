package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.0th  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19180th extends AbstractC18500sY {
    public static final Set A02;
    public final C16510p9 A00;
    public final C20120vF A01;

    static {
        HashSet hashSet = new HashSet();
        hashSet.add(2L);
        hashSet.add(1L);
        hashSet.add(25L);
        hashSet.add(3L);
        hashSet.add(28L);
        hashSet.add(13L);
        hashSet.add(29L);
        hashSet.add(20L);
        hashSet.add(9L);
        hashSet.add(26L);
        hashSet.add(23L);
        hashSet.add(37L);
        A02 = Collections.unmodifiableSet(hashSet);
    }

    public C19180th(C16510p9 r7, C20120vF r8, C18480sW r9) {
        this(r7, r8, r9, "message_media", Integer.MIN_VALUE);
    }

    public C19180th(C16510p9 r1, C20120vF r2, C18480sW r3, String str, int i) {
        super(r3, str, i);
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        String str;
        AbstractC15710nm r6;
        String str2;
        long j;
        Integer valueOf;
        int i;
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("thumb_image");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("media_wa_type");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("key_remote_jid");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("multicast_id");
        int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("media_url");
        int columnIndexOrThrow7 = cursor.getColumnIndexOrThrow("media_mime_type");
        int columnIndexOrThrow8 = cursor.getColumnIndexOrThrow("media_size");
        int columnIndexOrThrow9 = cursor.getColumnIndexOrThrow("media_name");
        int columnIndexOrThrow10 = cursor.getColumnIndexOrThrow("media_hash");
        int columnIndexOrThrow11 = cursor.getColumnIndexOrThrow("media_duration");
        int columnIndexOrThrow12 = cursor.getColumnIndexOrThrow("media_enc_hash");
        int columnIndexOrThrow13 = cursor.getColumnIndexOrThrow("timestamp");
        long j2 = -1;
        int i2 = 0;
        while (cursor.moveToNext()) {
            j2 = cursor.getLong(columnIndexOrThrow);
            i2++;
            if (A02.contains(Long.valueOf(cursor.getLong(columnIndexOrThrow3))) && A0W(cursor) && A0W(cursor)) {
                if (j2 < 1) {
                    Long valueOf2 = Long.valueOf(cursor.getLong(columnIndexOrThrow13));
                    AbstractC15710nm r62 = super.A01;
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.A0C);
                    sb.append("-invalid-row-id");
                    String obj = sb.toString();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("row_id=");
                    sb2.append(j2);
                    sb2.append(", time=");
                    sb2.append(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date(valueOf2.longValue())));
                    r62.AaV(obj, sb2.toString(), false);
                } else {
                    AbstractC14640lm A01 = AbstractC14640lm.A01(AnonymousClass1Tx.A02(cursor, columnIndexOrThrow4));
                    if (A01 != null) {
                        j = this.A00.A02(A01);
                        if (j < 1) {
                            StringBuilder sb3 = new StringBuilder("MediaCoreMessageStore/MediaMessageDatabaseMigration/processBatch/missing chat row_id; jid=");
                            sb3.append(A01);
                            Log.e(sb3.toString());
                        } else if (j >= 1) {
                            byte[] blob = cursor.getBlob(columnIndexOrThrow2);
                            C20120vF r8 = this.A01;
                            C16150oX A03 = r8.A03(blob);
                            if (A03 == null) {
                                Long valueOf3 = Long.valueOf(cursor.getLong(columnIndexOrThrow13));
                                StringBuilder sb4 = new StringBuilder("row_id= ");
                                sb4.append(j2);
                                sb4.append(", media_blob");
                                String obj2 = sb4.toString();
                                StringBuilder sb5 = new StringBuilder();
                                if (blob == null) {
                                    sb5.append(obj2);
                                    sb5.append("=null");
                                } else {
                                    sb5.append(obj2);
                                    sb5.append("=non-null, size=");
                                    sb5.append(blob.length);
                                }
                                String obj3 = sb5.toString();
                                StringBuilder sb6 = new StringBuilder();
                                sb6.append(obj3);
                                sb6.append(", time=");
                                sb6.append(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date(valueOf3.longValue())));
                                str2 = sb6.toString();
                                r6 = super.A01;
                                StringBuilder sb7 = new StringBuilder();
                                sb7.append(this.A0C);
                                sb7.append("-no-media-blob");
                                str = sb7.toString();
                                r6.AaV(str, str2, false);
                            } else {
                                int i3 = cursor.getInt(columnIndexOrThrow3);
                                long j3 = cursor.getLong(columnIndexOrThrow8);
                                String A022 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow5);
                                String A023 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow6);
                                String A024 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow7);
                                String A025 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow9);
                                String A026 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow10);
                                String A027 = AnonymousClass1Tx.A02(cursor, columnIndexOrThrow12);
                                if (i3 == 9 || i3 == 26) {
                                    valueOf = Integer.valueOf(cursor.getInt(columnIndexOrThrow11));
                                    i = 0;
                                } else {
                                    i = cursor.getInt(columnIndexOrThrow11);
                                    valueOf = null;
                                }
                                ContentValues contentValues = new ContentValues();
                                r8.A05(contentValues, A03);
                                C20120vF.A01(contentValues, valueOf, A022, A023, A024, A025, A026, A027, null, null, i, j2, j, j3, false);
                                C16310on A028 = this.A05.A02();
                                try {
                                    AnonymousClass1Lx A00 = A028.A00();
                                    C16330op r5 = A028.A03;
                                    long A029 = r5.A02(contentValues, "message_media");
                                    boolean z = true;
                                    if (A029 > 0) {
                                        if (j2 != A029) {
                                            z = false;
                                        }
                                        AnonymousClass009.A0C("MediaCoreMessageStore/processBatch/inserted row should have same row_id", z);
                                        r8.A06(A03, j2);
                                    } else {
                                        contentValues.remove("message_row_id");
                                        if (r5.A00("message_media", contentValues, "message_row_id = ?", new String[]{String.valueOf(j2)}) != 1) {
                                            throw new SQLiteException("MediaCoreMessageStore/processBatch/Failed to update message media.");
                                        }
                                    }
                                    A00.A00();
                                    A00.close();
                                    A028.close();
                                } catch (Throwable th) {
                                    try {
                                        A028.close();
                                    } catch (Throwable unused) {
                                    }
                                    throw th;
                                }
                            }
                        } else {
                            Long valueOf4 = Long.valueOf(cursor.getLong(columnIndexOrThrow13));
                            r6 = super.A01;
                            StringBuilder sb8 = new StringBuilder();
                            sb8.append(this.A0C);
                            sb8.append("-invalid-chat-row-id");
                            str = sb8.toString();
                            StringBuilder sb9 = new StringBuilder();
                            sb9.append("row_id=");
                            sb9.append(j2);
                            sb9.append(", chat_row_id=");
                            sb9.append(j);
                            sb9.append(", time=");
                            sb9.append(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date(valueOf4.longValue())));
                            str2 = sb9.toString();
                            r6.AaV(str, str2, false);
                        }
                    }
                    j = -1;
                    Long valueOf4 = Long.valueOf(cursor.getLong(columnIndexOrThrow13));
                    r6 = super.A01;
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(this.A0C);
                    sb8.append("-invalid-chat-row-id");
                    str = sb8.toString();
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append("row_id=");
                    sb9.append(j2);
                    sb9.append(", chat_row_id=");
                    sb9.append(j);
                    sb9.append(", time=");
                    sb9.append(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date(valueOf4.longValue())));
                    str2 = sb9.toString();
                    r6.AaV(str, str2, false);
                }
            }
        }
        return new AnonymousClass2Ez(j2, i2);
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A04("media_message_ready", 2);
    }

    public boolean A0W(Cursor cursor) {
        if (!(this instanceof C19190ti)) {
            return true;
        }
        long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        C16310on A01 = this.A05.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT message_row_id, chat_row_id, autotransfer_retry_enabled, multicast_id, media_job_uuid, transferred, transcoded, file_path, file_size, suspicious_content, trim_from, trim_to, face_x, face_y, media_key, media_key_timestamp, width, height, has_streaming_sidecar, gif_attribution, thumbnail_height_width_ratio, direct_path, first_scan_sidecar, first_scan_length, message_url, mime_type, file_length, media_name, file_hash, media_duration, page_count, enc_file_hash, partial_media_hash, partial_media_enc_hash, is_animated_sticker, original_file_hash, mute_video, media_caption FROM message_media WHERE message_row_id = ?", new String[]{String.valueOf(j)});
            if (!A09.moveToNext()) {
                A09.close();
                A01.close();
                return true;
            }
            this.A01.A02(A09);
            A09.close();
            A01.close();
            return false;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
