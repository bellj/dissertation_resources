package X;

/* renamed from: X.0qk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17400qk implements AbstractC17410ql {
    public final C235812f A00;

    public C17400qk(C235812f r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC17410ql
    public void AMj() {
        this.A00.A02(8);
    }

    @Override // X.AbstractC17410ql
    public void AMk() {
        this.A00.A03(null, null, null, 5);
    }

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
        C235812f r1 = this.A00;
        int i = 7;
        if (z) {
            i = 3;
        }
        r1.A02(i);
    }
}
