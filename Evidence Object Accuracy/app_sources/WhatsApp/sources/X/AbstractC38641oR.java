package X;

import java.util.concurrent.Executor;

/* renamed from: X.1oR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC38641oR extends AbstractRunnableC38601oN {
    public final C15450nH A00;
    public final C18790t3 A01;
    public final C14950mJ A02;
    public final C14850m9 A03;
    public final C20110vE A04;
    public final C22600zL A05;

    public AbstractC38641oR(C15450nH r1, C18790t3 r2, C14950mJ r3, C14850m9 r4, C20110vE r5, C22600zL r6, Executor executor) {
        super(executor);
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A05 = r6;
        this.A04 = r5;
    }
}
