package X;

import java.util.ListIterator;

/* renamed from: X.3t2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80833t2 extends AnonymousClass5DK implements ListIterator {
    public final /* synthetic */ C80853t4 this$1;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80833t2(C80853t4 r1) {
        super(r1);
        this.this$1 = r1;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80833t2(C80853t4 r2, int i) {
        super(r2, r2.getListDelegate().listIterator(i));
        this.this$1 = r2;
    }

    @Override // java.util.ListIterator
    public void add(Object obj) {
        boolean isEmpty = this.this$1.isEmpty();
        getDelegateListIterator().add(obj);
        C80853t4 r1 = this.this$1;
        AbstractC80933tC.access$208(r1.this$0);
        if (isEmpty) {
            r1.addToMap();
        }
    }

    private ListIterator getDelegateListIterator() {
        return (ListIterator) getDelegateIterator();
    }

    @Override // java.util.ListIterator
    public boolean hasPrevious() {
        return getDelegateListIterator().hasPrevious();
    }

    @Override // java.util.ListIterator
    public int nextIndex() {
        return getDelegateListIterator().nextIndex();
    }

    @Override // java.util.ListIterator
    public Object previous() {
        return getDelegateListIterator().previous();
    }

    @Override // java.util.ListIterator
    public int previousIndex() {
        return getDelegateListIterator().previousIndex();
    }

    @Override // java.util.ListIterator
    public void set(Object obj) {
        getDelegateListIterator().set(obj);
    }
}
