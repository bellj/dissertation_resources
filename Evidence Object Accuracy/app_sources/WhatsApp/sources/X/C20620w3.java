package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0w3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20620w3 {
    public final C006202y A00 = new C006202y(250);
    public final C18460sU A01;
    public final C16490p7 A02;
    public final C21390xL A03;

    public C20620w3(C18460sU r3, C16490p7 r4, C21390xL r5) {
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
    }

    public C39921ql A00(long j) {
        Long valueOf;
        C39921ql r0;
        C39921ql r5;
        C006202y r7 = this.A00;
        synchronized (r7) {
            valueOf = Long.valueOf(j);
            r0 = (C39921ql) r7.A04(valueOf);
        }
        if (r0 != null) {
            return r0;
        }
        C16490p7 r02 = this.A02;
        C16310on A01 = r02.get();
        try {
            synchronized (r7) {
                r5 = new C39921ql();
                C16310on A012 = r02.get();
                Cursor A09 = A012.A03.A09("SELECT receipt_user_jid_row_id,receipt_timestamp,read_timestamp,played_timestamp FROM receipt_user WHERE message_row_id = ?", new String[]{String.valueOf(j)});
                try {
                    int columnIndexOrThrow = A09.getColumnIndexOrThrow("receipt_user_jid_row_id");
                    int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("receipt_timestamp");
                    int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("read_timestamp");
                    int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("played_timestamp");
                    while (A09.moveToNext()) {
                        UserJid userJid = (UserJid) this.A01.A07(UserJid.class, A09.getLong(columnIndexOrThrow));
                        if (userJid != null) {
                            r5.A00.put(userJid, new C39931qm(A09.getLong(columnIndexOrThrow2), A09.getLong(columnIndexOrThrow3), A09.getLong(columnIndexOrThrow4)));
                        }
                    }
                    A09.close();
                    A012.close();
                    r7.A08(valueOf, r5);
                } catch (Throwable th) {
                    if (A09 != null) {
                        try {
                            A09.close();
                        } catch (Throwable unused) {
                        }
                    }
                    throw th;
                }
            }
            A01.close();
            return r5;
        } catch (Throwable th2) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public void A01(UserJid userJid, int i, long j, long j2) {
        String str;
        C21390xL r5 = this.A03;
        if ((r5.A00("receipt_user_ready", 0) == 2 || (j > 0 && j < r5.A01("migration_receipt_index", 0))) && A00(j).A00(userJid, i, j2)) {
            StringBuilder sb = new StringBuilder("receiptuserstore/insertOrUpdateUserReceiptForMessage/rowId=");
            sb.append(j);
            sb.append("; status=");
            sb.append(i);
            sb.append(" timestamp=");
            sb.append(j2);
            Log.i(sb.toString());
            ContentValues contentValues = new ContentValues(1);
            if (i == 5) {
                str = "receipt_timestamp";
            } else if (i == 8) {
                str = "played_timestamp";
            } else if (i == 13) {
                str = "read_timestamp";
            } else {
                StringBuilder sb2 = new StringBuilder("Unexpected message status ");
                sb2.append(i);
                sb2.append(" for user receipt");
                throw new IllegalArgumentException(sb2.toString());
            }
            contentValues.put(str, Long.valueOf(j2));
            C16310on A02 = this.A02.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                long A01 = this.A01.A01(userJid);
                boolean z = false;
                if (A01 != -1) {
                    z = true;
                }
                AnonymousClass009.A0C("invalid jid", z);
                C16330op r9 = A02.A03;
                if (((long) r9.A00("receipt_user", contentValues, "message_row_id=? AND receipt_user_jid_row_id=?", new String[]{String.valueOf(j), String.valueOf(A01)})) == 0) {
                    contentValues.put("message_row_id", Long.valueOf(j));
                    contentValues.put("receipt_user_jid_row_id", Long.valueOf(A01));
                    if (r9.A02(contentValues, "receipt_user") == -1) {
                        Log.e("ReceiptUserStore/insertOrUpdateUserReceiptForMessage/insert failed");
                    }
                }
                A00.A00();
                A00.close();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
