package X;

import com.whatsapp.jid.UserJid;
import java.util.Set;

/* renamed from: X.4Vt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92404Vt {
    public final int A00;
    public final int A01;
    public final UserJid A02;
    public final String A03;
    public final Set A04;

    public C92404Vt(UserJid userJid, String str, Set set, int i, int i2) {
        this.A02 = userJid;
        this.A01 = i;
        this.A00 = i2;
        this.A04 = set;
        this.A03 = str;
    }

    public String toString() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A02;
        A1a[1] = this.A04;
        return String.format("GetCategoriesRequest{bizJid=%s, categoryIds=%s'}", A1a);
    }
}
