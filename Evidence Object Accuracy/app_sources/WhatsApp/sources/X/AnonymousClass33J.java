package X;

import android.graphics.Bitmap;
import android.graphics.PointF;
import org.json.JSONObject;

/* renamed from: X.33J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33J extends AbstractC454821u {
    public int A00;
    public Bitmap A01;
    public PointF A02;
    public AbstractC64833Hb A03;
    public C93694aa A04;
    public boolean A05;
    public final float A06;

    public AnonymousClass33J(float f) {
        this.A00 = 1;
        this.A05 = false;
        C12990iw.A13(super.A01);
        this.A06 = f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        if (r6 != null) goto L_0x003b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass33J(org.json.JSONObject r19, float r20) {
        /*
        // Method dump skipped, instructions count: 256
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass33J.<init>(org.json.JSONObject, float):void");
    }

    @Override // X.AbstractC454821u
    public void A0N(JSONObject jSONObject) {
        super.A0N(jSONObject);
        this.A03.A05(jSONObject);
    }

    public void A0R(C93694aa r3) {
        this.A04 = r3;
        AbstractC64833Hb r1 = this.A03;
        if (r1 != null && (r1 instanceof AnonymousClass33P)) {
            ((AnonymousClass33P) r1).A00 = r3;
        }
    }
}
