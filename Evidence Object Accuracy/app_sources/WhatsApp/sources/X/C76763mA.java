package X;

import java.nio.ByteBuffer;

/* renamed from: X.3mA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C76763mA extends AnonymousClass4YO {
    public long A00;
    public ByteBuffer A01;
    public ByteBuffer A02;
    public boolean A03;
    public final int A04;
    public final C91844Tk A05 = new C91844Tk();

    public C76763mA(int i) {
        this.A04 = i;
    }

    public final void A00() {
        ByteBuffer byteBuffer = this.A01;
        if (byteBuffer != null) {
            byteBuffer.flip();
        }
        ByteBuffer byteBuffer2 = this.A02;
        if (byteBuffer2 != null) {
            byteBuffer2.flip();
        }
    }

    public void A01(int i) {
        int capacity;
        ByteBuffer allocateDirect;
        int i2 = i + 0;
        ByteBuffer byteBuffer = this.A01;
        if (byteBuffer == null) {
            int i3 = this.A04;
            if (i3 == 1) {
                allocateDirect = ByteBuffer.allocate(i2);
            } else if (i3 == 2) {
                allocateDirect = ByteBuffer.allocateDirect(i2);
            } else {
                throw new AnonymousClass4CQ(0, i2);
            }
        } else {
            int capacity2 = byteBuffer.capacity();
            int position = byteBuffer.position();
            int i4 = i2 + position;
            if (capacity2 >= i4) {
                this.A01 = byteBuffer;
                return;
            }
            int i5 = this.A04;
            if (i5 == 1) {
                allocateDirect = ByteBuffer.allocate(i4);
            } else if (i5 == 2) {
                allocateDirect = ByteBuffer.allocateDirect(i4);
            } else {
                ByteBuffer byteBuffer2 = this.A01;
                if (byteBuffer2 == null) {
                    capacity = 0;
                } else {
                    capacity = byteBuffer2.capacity();
                }
                throw new AnonymousClass4CQ(capacity, i4);
            }
            allocateDirect.order(byteBuffer.order());
            if (position > 0) {
                byteBuffer.flip();
                allocateDirect.put(byteBuffer);
            }
        }
        this.A01 = allocateDirect;
    }

    @Override // X.AnonymousClass4YO
    public void clear() {
        this.flags = 0;
        ByteBuffer byteBuffer = this.A01;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
        ByteBuffer byteBuffer2 = this.A02;
        if (byteBuffer2 != null) {
            byteBuffer2.clear();
        }
        this.A03 = false;
    }
}
