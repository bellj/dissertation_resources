package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.2ks  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56432ks extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99554kU();
    public Bundle A00;
    public Map A01;

    public C56432ks(Bundle bundle) {
        this.A00 = bundle;
    }

    public final int A00() {
        Bundle bundle = this.A00;
        String string = bundle.getString("google.original_priority");
        if (string == null) {
            string = bundle.getString("google.priority");
        }
        if ("high".equals(string)) {
            return 1;
        }
        return "normal".equals(string) ? 2 : 0;
    }

    public final int A01() {
        Bundle bundle = this.A00;
        String string = bundle.getString("google.delivered_priority");
        if (string == null) {
            if ("1".equals(bundle.getString("google.priority_reduced"))) {
                return 2;
            }
            string = bundle.getString("google.priority");
        }
        if ("high".equals(string)) {
            return 1;
        }
        if ("normal".equals(string)) {
            return 2;
        }
        return 0;
    }

    public final Map A02() {
        Map map = this.A01;
        AnonymousClass00N r4 = map;
        if (map == null) {
            Bundle bundle = this.A00;
            AnonymousClass00N r42 = new AnonymousClass00N();
            Iterator<String> it = bundle.keySet().iterator();
            while (it.hasNext()) {
                String A0x = C12970iu.A0x(it);
                Object obj = bundle.get(A0x);
                if ((obj instanceof String) && !A0x.startsWith("google.") && !A0x.startsWith("gcm.") && !A0x.equals("from") && !A0x.equals("message_type") && !A0x.equals("collapse_key")) {
                    r42.put(A0x, obj);
                }
            }
            this.A01 = r42;
            r4 = r42;
        }
        return r4;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A03(this.A00, parcel, 2);
        C95654e8.A06(parcel, A01);
    }
}
