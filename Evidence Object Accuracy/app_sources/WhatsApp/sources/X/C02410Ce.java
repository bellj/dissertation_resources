package X;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

/* renamed from: X.0Ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02410Ce extends AnonymousClass0XR implements AbstractC12310hi {
    public static Method A01;
    public AbstractC12310hi A00;

    static {
        try {
            if (Build.VERSION.SDK_INT <= 28) {
                A01 = PopupWindow.class.getDeclaredMethod("setTouchModal", Boolean.TYPE);
            }
        } catch (NoSuchMethodException unused) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }

    public C02410Ce(Context context, int i, int i2) {
        super(context, null, i, i2);
    }

    @Override // X.AnonymousClass0XR
    public C02360Bs A00(Context context, boolean z) {
        AnonymousClass0CW r0 = new AnonymousClass0CW(context, z);
        r0.A01 = this;
        return r0;
    }

    public void A02() {
        if (Build.VERSION.SDK_INT >= 23) {
            this.A0D.setEnterTransition(null);
        }
    }

    public void A03() {
        if (Build.VERSION.SDK_INT >= 23) {
            this.A0D.setExitTransition(null);
        }
    }

    public void A04() {
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = A01;
            if (method != null) {
                try {
                    method.invoke(this.A0D, false);
                } catch (Exception unused) {
                    Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
                }
            }
        } else {
            this.A0D.setTouchModal(false);
        }
    }

    @Override // X.AbstractC12310hi
    public void ARb(MenuItem menuItem, AnonymousClass07H r3) {
        AbstractC12310hi r0 = this.A00;
        if (r0 != null) {
            r0.ARb(menuItem, r3);
        }
    }

    @Override // X.AbstractC12310hi
    public void ARc(MenuItem menuItem, AnonymousClass07H r3) {
        AbstractC12310hi r0 = this.A00;
        if (r0 != null) {
            r0.ARc(menuItem, r3);
        }
    }
}
