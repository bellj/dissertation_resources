package X;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0pH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16580pH extends AbstractC16280ok {
    public final C16590pI A00;
    public final C231410n A01;
    public final AnonymousClass1I2 A02 = new AnonymousClass1I2(new C002601e(Collections.emptySet(), null));
    public final String A03;
    public final ReentrantReadWriteLock A04;

    public C16580pH(AbstractC15710nm r10, C16590pI r11, C231410n r12, C14850m9 r13, String str) {
        super(r11.A00, r10, str, null, 1, r13.A07(781));
        this.A03 = str;
        this.A00 = r11;
        this.A01 = r12;
        this.A04 = new ReentrantReadWriteLock();
    }

    @Override // X.AbstractC16280ok
    public synchronized C16330op A03() {
        C16330op r0;
        try {
            try {
                try {
                    r0 = AnonymousClass1Tx.A01(A00(), this.A01);
                } catch (SQLiteDatabaseCorruptException e) {
                    Log.w("chat-settings-store/corrupt/removing", e);
                    A04();
                    r0 = AnonymousClass1Tx.A01(super.A00(), this.A01);
                }
            } catch (SQLiteException e2) {
                if (e2.toString().contains("file is encrypted")) {
                    Log.w("chat-settings-store/encrypted/removing", e2);
                    A04();
                    r0 = AnonymousClass1Tx.A01(super.A00(), this.A01);
                } else {
                    throw e2;
                }
            }
        } catch (StackOverflowError e3) {
            Log.w("chat-settings-store/stackoverflowerror", e3);
            for (StackTraceElement stackTraceElement : e3.getStackTrace()) {
                if (stackTraceElement.getMethodName().equals("onCorruption")) {
                    Log.w("chat-settings-store/stackoverflowerror/corrupt/removing");
                    A04();
                    r0 = AnonymousClass1Tx.A01(super.A00(), this.A01);
                }
            }
            throw e3;
        }
        return r0;
    }

    public boolean A04() {
        boolean delete;
        synchronized (this) {
            close();
            Log.i("chat-settings-store/delete-database");
            File databasePath = this.A00.A00.getDatabasePath(this.A03);
            delete = databasePath.delete();
            AnonymousClass1Tx.A04(databasePath, "chat-settings-store");
            StringBuilder sb = new StringBuilder();
            sb.append("chat-settings-store/delete-database/result=");
            sb.append(delete);
            Log.i(sb.toString());
            if (delete) {
                Iterator it = this.A02.iterator();
                while (it.hasNext()) {
                    ((C33161dY) it.next()).A0C();
                }
            }
        }
        return delete;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Log.i("chat-settings-store/create");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS settings");
        sQLiteDatabase.execSQL("CREATE TABLE settings (_id INTEGER PRIMARY KEY AUTOINCREMENT,jid TEXT,deleted INTEGER,mute_end INTEGER,muted_notifications BOOLEAN,use_custom_notifications BOOLEAN,message_tone TEXT,message_vibrate INTEGER,message_popup INTEGER,message_light INTEGER,call_tone TEXT,call_vibrate INTEGER,status_muted INTEGER,pinned BOOLEAN,pinned_time INTEGER,low_pri_notifications BOOLEAN,media_visibility INTEGER,mute_reactions INTEGER);");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS jid_index ON settings(jid);");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("chat-settings-store/downgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        onCreate(sQLiteDatabase);
    }

    @Override // X.AbstractC16280ok, android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        C16330op A01 = AnonymousClass1Tx.A01(sQLiteDatabase, this.A01);
        String A00 = AnonymousClass1Uj.A00(A01, "table", "settings");
        if (A00 != null) {
            AnonymousClass1Uj.A02(A01, A00, "settings", "status_muted", "INTEGER", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "pinned", "BOOLEAN", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "pinned_time", "INTEGER", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "low_pri_notifications", "BOOLEAN", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "media_visibility", "INTEGER", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "mute_reactions", "BOOLEAN", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "wallpaper_light_type", "TEXT", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "wallpaper_light_value", "TEXT", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "wallpaper_dark_type", "TEXT", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "wallpaper_dark_value", "TEXT", "ChatSettingsDatabaseHelper");
            AnonymousClass1Uj.A02(A01, A00, "settings", "wallpaper_dark_opacity", "INTEGER", "ChatSettingsDatabaseHelper");
        }
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            ((C33161dY) it.next()).A0J(A01);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("chat-settings-store/upgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        onCreate(sQLiteDatabase);
    }
}
