package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100404lr implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1P4(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1P4[i];
    }
}
