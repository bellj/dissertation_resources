package X;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.PopupWindow;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.WaEditText;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0mr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15280mr extends PopupWindow {
    public int A00 = 0;
    public int A01 = -1;
    public boolean A02;
    public final Activity A03;
    public final AbstractC15710nm A04;
    public final AbstractC49822Mw A05;
    public final AnonymousClass01d A06;
    public final C14820m6 A07;
    public final C252718t A08;
    public final Runnable A09;
    public final Set A0A;

    public AbstractC15280mr(Activity activity, AbstractC15710nm r4, AbstractC49822Mw r5, AnonymousClass01d r6, C14820m6 r7, C252718t r8) {
        super(activity);
        this.A03 = activity;
        this.A08 = r8;
        this.A04 = r4;
        this.A06 = r6;
        this.A07 = r7;
        this.A05 = r5;
        this.A0A = new HashSet();
        this.A09 = new RunnableBRunnable0Shape1S0100000_I0_1(r5, 9);
    }

    public static boolean A02(Point point, View view) {
        int i;
        int i2;
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i3 = point.x;
        int i4 = iArr[0];
        if (i3 < i4 || i3 > i4 + view.getWidth() || (i = point.y) < (i2 = iArr[1]) || i > i2 + view.getHeight()) {
            return false;
        }
        return true;
    }

    public int A03(int i) {
        SharedPreferences sharedPreferences;
        String str;
        SharedPreferences.Editor edit;
        String str2;
        Point point = new Point();
        Activity activity = this.A03;
        activity.getWindowManager().getDefaultDisplay().getSize(point);
        int i2 = point.y;
        if (this.A00 != 1 || i <= 0 || A09()) {
            int i3 = activity.getResources().getConfiguration().orientation;
            if (i3 != 1) {
                if (i3 == 2) {
                    sharedPreferences = this.A07.A00;
                    str = "keyboard_height_landscape";
                }
                return (i2 * 3) >> 3;
            }
            sharedPreferences = this.A07.A00;
            str = "keyboard_height_portrait";
            int i4 = sharedPreferences.getInt(str, 0);
            if (i4 > 0) {
                return Math.min(i2 >> 1, i4);
            }
            return (i2 * 3) >> 3;
        }
        int min = Math.min(i2 >> 1, i);
        int i5 = activity.getResources().getConfiguration().orientation;
        if (i5 != 1) {
            if (i5 == 2) {
                edit = this.A07.A00.edit();
                str2 = "keyboard_height_landscape";
            }
            return min;
        }
        edit = this.A07.A00.edit();
        str2 = "keyboard_height_portrait";
        edit.putInt(str2, min).apply();
        return min;
    }

    public void A04() {
        this.A01 = A03(-1);
    }

    public void A05() {
        AbstractC49822Mw r2 = this.A05;
        ((View) r2).getHandler().removeCallbacks(this.A09);
        ((KeyboardPopupLayout) r2).A06 = true;
    }

    public void A06() {
        this.A02 = false;
    }

    public final void A07(AnonymousClass5TD r5, WaEditText waEditText, Runnable runnable) {
        InputMethodManager A0Q = this.A06.A0Q();
        waEditText.requestFocus();
        Handler handler = new Handler(Looper.getMainLooper());
        Set set = this.A0A;
        if (!r5.A6r(new ResultReceiverC73413gC(handler, runnable, set), A0Q)) {
            AbstractC49822Mw r2 = this.A05;
            ((KeyboardPopupLayout) r2).A06 = false;
            ((View) r2).requestLayout();
            set.remove(runnable);
        }
    }

    public void A08(WaEditText waEditText) {
        this.A02 = true;
        A05();
        dismiss();
        if (waEditText != null) {
            KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) this.A05;
            if (keyboardPopupLayout.A04 != null) {
                keyboardPopupLayout.A04 = null;
                keyboardPopupLayout.requestLayout();
            }
            A07(new AnonymousClass5TD() { // from class: X.52v
                @Override // X.AnonymousClass5TD
                public final boolean A6r(ResultReceiver resultReceiver, InputMethodManager inputMethodManager) {
                    return inputMethodManager.showSoftInput(WaEditText.this, 0, resultReceiver);
                }
            }, waEditText, new RunnableBRunnable0Shape1S0100000_I0_1(this, 11));
        }
    }

    public boolean A09() {
        return Build.VERSION.SDK_INT >= 24 && this.A03.isInMultiWindowMode();
    }

    @Override // android.widget.PopupWindow
    public void dismiss() {
        if (isShowing()) {
            A04();
            super.dismiss();
            AbstractC49822Mw r2 = this.A05;
            KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) r2;
            if (keyboardPopupLayout.A04 != null) {
                keyboardPopupLayout.A04 = null;
                keyboardPopupLayout.requestLayout();
            }
            ((View) r2).requestLayout();
        }
    }
}
