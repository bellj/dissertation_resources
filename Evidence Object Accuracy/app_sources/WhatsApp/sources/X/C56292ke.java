package X;

import android.content.Context;

/* renamed from: X.2ke  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56292ke extends AnonymousClass2RP implements AnonymousClass5Yi {
    public static final AbstractC77683ng A00;
    public static final AnonymousClass4DN A01;
    public static final AnonymousClass1UE A02;

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A01 = r3;
        C77623na r2 = new C77623na();
        A00 = r2;
        A02 = new AnonymousClass1UE(r2, r3, "Blockstore.API");
    }

    public C56292ke(Context context) {
        super(null, context, AbstractC116855Xe.A00, A02, C93404a7.A02);
    }
}
