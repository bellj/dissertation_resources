package X;

import android.animation.ValueAnimator;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.61u  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61u implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ FormItemEditText A01;

    public AnonymousClass61u(FormItemEditText formItemEditText, int i) {
        this.A01 = formItemEditText;
        this.A00 = i;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        FormItemEditText formItemEditText = this.A01;
        formItemEditText.A0M[this.A00] = ((Number) valueAnimator.getAnimatedValue()).floatValue();
        formItemEditText.invalidate();
    }
}
