package X;

import android.text.TextUtils;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.11G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11G {
    public String[] A00;
    public final C15550nR A01;
    public final AnonymousClass018 A02;
    public final C14850m9 A03;

    public AnonymousClass11G(C15550nR r1, AnonymousClass018 r2, C14850m9 r3) {
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r1;
    }

    public boolean A00() {
        C14850m9 r1 = this.A03;
        if (r1.A07(233)) {
            String A03 = r1.A03(379);
            if (!TextUtils.isEmpty(A03)) {
                String A06 = this.A02.A06();
                String[] split = A03.split(",");
                for (String str : split) {
                    if (!str.equals(A06)) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    public synchronized boolean A01(Jid jid) {
        boolean z;
        int i = 0;
        if (jid instanceof AbstractC14640lm) {
            AbstractC14640lm r7 = (AbstractC14640lm) jid;
            if (!C15380n4.A0J(r7)) {
                if (r7 instanceof UserJid) {
                    UserJid userJid = (UserJid) r7;
                    String[] strArr = this.A00;
                    if (strArr == null) {
                        String A03 = this.A03.A03(1031);
                        if (TextUtils.isEmpty(A03)) {
                            strArr = new String[0];
                        } else {
                            strArr = A03.split(",");
                        }
                        this.A00 = strArr;
                    }
                    String str = userJid.user;
                    int length = strArr.length;
                    while (true) {
                        if (i >= length) {
                            z = false;
                            break;
                        }
                        String str2 = strArr[i];
                        if (!TextUtils.isEmpty(str2) && str.startsWith(str2)) {
                            z = true;
                            break;
                        }
                        i++;
                    }
                }
            } else {
                z = this.A01.A0B(r7).A0Y;
            }
            return z;
        }
        return false;
    }
}
