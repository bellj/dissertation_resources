package X;

/* renamed from: X.0rA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17650rA {
    public final C16590pI A00;

    public C17650rA(C16590pI r1) {
        this.A00 = r1;
    }

    public C17660rB A00() {
        Object obj = ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, this.A00.A00)).A3x.get();
        if (obj != null) {
            Object obj2 = AbstractC17190qP.of((Object) 1, obj).get(1);
            AnonymousClass009.A05(obj2);
            return (C17660rB) obj2;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }

    public C21550xb A01() {
        return (C21550xb) ((AnonymousClass01J) AnonymousClass027.A00(AnonymousClass01J.class, this.A00.A00)).AAj.get();
    }
}
