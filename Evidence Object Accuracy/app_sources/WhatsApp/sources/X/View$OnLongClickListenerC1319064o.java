package X;

import android.view.View;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.64o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class View$OnLongClickListenerC1319064o implements View.OnLongClickListener {
    public final /* synthetic */ FormItemEditText A00;

    public View$OnLongClickListenerC1319064o(FormItemEditText formItemEditText) {
        this.A00 = formItemEditText;
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        FormItemEditText formItemEditText = this.A00;
        formItemEditText.setSelection(formItemEditText.getText().length());
        return true;
    }
}
