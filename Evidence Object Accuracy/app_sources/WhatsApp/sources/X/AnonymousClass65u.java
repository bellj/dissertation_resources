package X;

/* renamed from: X.65u  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65u implements AbstractC009404s {
    public final /* synthetic */ AbstractC16870pt A00;
    public final /* synthetic */ C128375w0 A01;

    public AnonymousClass65u(AbstractC16870pt r1, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C128375w0 r0 = this.A01;
        return new C123485nE(r0.A0A, r0.A0C, r0.A0P, r0.A0V, this.A00);
    }
}
