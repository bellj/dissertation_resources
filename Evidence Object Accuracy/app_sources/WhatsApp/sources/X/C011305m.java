package X;

import android.os.Bundle;

/* renamed from: X.05m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C011305m implements AnonymousClass05A {
    public final /* synthetic */ ActivityC000800j A00;

    public C011305m(ActivityC000800j r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass05A
    public Bundle AbH() {
        Bundle bundle = new Bundle();
        this.A00.A1V();
        return bundle;
    }
}
