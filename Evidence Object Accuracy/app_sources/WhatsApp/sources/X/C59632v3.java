package X;

/* renamed from: X.2v3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59632v3 extends C37171lc {
    public final AnonymousClass1WK A00;

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C59632v3) && C16700pc.A0O(this.A00, ((C59632v3) obj).A00));
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A00.hashCode();
    }

    public C59632v3(AnonymousClass1WK r2) {
        super(AnonymousClass39o.A0K);
        this.A00 = r2;
    }

    public String toString() {
        return C12960it.A0a(this.A00, C12960it.A0k("NearbyBusinessWidgetEmptyList(onLocationClickListener="));
    }
}
