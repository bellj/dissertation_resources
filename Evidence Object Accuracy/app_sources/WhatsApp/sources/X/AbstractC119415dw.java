package X;

import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.WaBloksActivity;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5dw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC119415dw extends C28791Pa implements AbstractC36791ka {
    public final AnonymousClass018 A00;
    public final WaBloksActivity A01;

    @Override // X.AbstractC36791ka
    public boolean AUA(Menu menu) {
        return false;
    }

    public AbstractC119415dw(AnonymousClass018 r1, WaBloksActivity waBloksActivity) {
        this.A00 = r1;
        this.A01 = waBloksActivity;
    }

    public void A00(AbstractC115815Ta r6) {
        if (this instanceof C124465pY) {
            C124465pY r2 = (C124465pY) this;
            r2.A02 = new C126195sU(r6.AAL());
            r2.A01();
        } else if (!(this instanceof C124445pW)) {
            C124455pX r4 = (C124455pX) this;
            List<AnonymousClass28D> A0L = r6.AAL().A0L(45);
            ArrayList A0l = C12960it.A0l();
            for (AnonymousClass28D r1 : A0L) {
                A0l.add(new AbstractC115815Ta() { // from class: X.679
                    @Override // X.AbstractC115815Ta
                    public final AnonymousClass28D AAL() {
                        return AnonymousClass28D.this;
                    }
                });
            }
            r4.A01 = A0l;
        }
    }

    @Override // X.AbstractC36791ka
    public void AOk(Menu menu) {
        MenuItem menuItem;
        if (this instanceof C124465pY) {
            C124465pY r4 = (C124465pY) this;
            MenuItem add = menu.add(0, 55, 0, "cart");
            r4.A00 = add;
            add.setShowAsAction(1);
            r4.A00.setIcon(R.drawable.ic_action_view_shop);
            r4.A00.setVisible(false);
            MenuItem add2 = menu.add(0, 56, 0, "more");
            r4.A01 = add2;
            add2.setShowAsAction(1);
            r4.A01.setIcon(R.drawable.ic_more_vertical);
            r4.A01.setVisible(false);
            r4.A01();
        } else if (!(this instanceof C124445pW)) {
            C124455pX r3 = (C124455pX) this;
            MenuItem add3 = menu.add(0, 56, 0, "");
            r3.A00 = add3;
            add3.setShowAsAction(0);
            r3.A00.setTitle(R.string.share_shops_link);
            r3.A00.setIcon(R.drawable.ic_more_vertical);
            r3.A00.setVisible(false);
            if (r3.A01 != null && (menuItem = r3.A00) != null) {
                menuItem.setVisible(true);
            }
        }
    }

    @Override // X.AbstractC36791ka
    public boolean ATH(MenuItem menuItem) {
        if (this instanceof C124465pY) {
            C124465pY r3 = (C124465pY) this;
            if (menuItem.getItemId() != 56) {
                return false;
            }
            AnonymousClass1AI.A08(((AbstractC119415dw) r3).A01.A02, new AbstractC28681Oo(new AbstractC115815Ta() { // from class: X.67A
                @Override // X.AbstractC115815Ta
                public final AnonymousClass28D AAL() {
                    return C124465pY.this.A02.A00.A0F(48);
                }
            }, r3) { // from class: X.67M
                public final /* synthetic */ AbstractC115815Ta A00;
                public final /* synthetic */ C124465pY A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AbstractC28681Oo
                public final AbstractC14200l1 AAN() {
                    return this.A00.AAL().A0G(35);
                }
            });
            return false;
        } else if (!(this instanceof C124455pX)) {
            return false;
        } else {
            C124455pX r32 = (C124455pX) this;
            if (r32.A01 != null && menuItem.getItemId() == 56) {
                AnonymousClass1AI.A08(((AbstractC119415dw) r32).A01.A02, new AbstractC28681Oo() { // from class: X.67J
                    @Override // X.AbstractC28681Oo
                    public final AbstractC14200l1 AAN() {
                        return ((AbstractC115815Ta) C12980iv.A0o(C124455pX.this.A01)).AAL().A0G(35);
                    }
                });
            }
            return false;
        }
    }
}
