package X;

/* renamed from: X.2Er  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2Er {
    public final String A00;
    public final String A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass2Er) {
                AnonymousClass2Er r5 = (AnonymousClass2Er) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.A00.hashCode() * 31) + this.A01.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("BlockReason(code=");
        sb.append(this.A00);
        sb.append(", reason=");
        sb.append(this.A01);
        sb.append(')');
        return sb.toString();
    }

    public AnonymousClass2Er(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }
}
