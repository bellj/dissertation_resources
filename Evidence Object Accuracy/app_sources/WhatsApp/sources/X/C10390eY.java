package X;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/* renamed from: X.0eY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10390eY implements Iterator, Map.Entry {
    public int A00;
    public int A01;
    public boolean A02 = false;
    public final /* synthetic */ AbstractC008904n A03;

    public C10390eY(AbstractC008904n r2) {
        this.A03 = r2;
        this.A00 = r2.A01() - 1;
        this.A01 = -1;
    }

    @Override // java.lang.Object, java.util.Map.Entry
    public boolean equals(Object obj) {
        if (!this.A02) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            AbstractC008904n r3 = this.A03;
            Object A03 = r3.A03(this.A01, 0);
            if (key != A03 && (key == null || !key.equals(A03))) {
                return false;
            }
            Object value = entry.getValue();
            Object A032 = r3.A03(this.A01, 1);
            if (value == A032 || (value != null && value.equals(A032))) {
                return true;
            }
            return false;
        }
    }

    @Override // java.util.Map.Entry
    public Object getKey() {
        if (this.A02) {
            return this.A03.A03(this.A01, 0);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    @Override // java.util.Map.Entry
    public Object getValue() {
        if (this.A02) {
            return this.A03.A03(this.A01, 1);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.A01 < this.A00;
    }

    @Override // java.lang.Object, java.util.Map.Entry
    public int hashCode() {
        int hashCode;
        if (this.A02) {
            AbstractC008904n r4 = this.A03;
            int i = this.A01;
            int i2 = 0;
            Object A03 = r4.A03(i, 0);
            Object A032 = r4.A03(i, 1);
            if (A03 == null) {
                hashCode = 0;
            } else {
                hashCode = A03.hashCode();
            }
            if (A032 != null) {
                i2 = A032.hashCode();
            }
            return hashCode ^ i2;
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        if (hasNext()) {
            this.A01++;
            this.A02 = true;
            return this;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        if (this.A02) {
            this.A03.A07(this.A01);
            this.A01--;
            this.A00--;
            this.A02 = false;
            return;
        }
        throw new IllegalStateException();
    }

    @Override // java.util.Map.Entry
    public Object setValue(Object obj) {
        if (this.A02) {
            return this.A03.A04(this.A01, obj);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getKey());
        sb.append("=");
        sb.append(getValue());
        return sb.toString();
    }
}
