package X;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/* renamed from: X.4LX  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4LX {
    public SecureRandom A00;

    public AnonymousClass4LX() {
        try {
            this.A00 = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException unused) {
            throw AnonymousClass1NR.A00("SHA1PRNGalgorithm not found.", (byte) 80);
        }
    }
}
