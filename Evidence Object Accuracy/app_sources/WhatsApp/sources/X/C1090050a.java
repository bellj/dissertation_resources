package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import java.lang.reflect.Field;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.chromium.net.UrlRequest;
import sun.misc.Unsafe;

/* renamed from: X.50a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1090050a implements AnonymousClass5XU {
    public static final Unsafe A0E = C95634e6.A05();
    public static final int[] A0F = new int[0];
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final AnonymousClass4DV A04;
    public final AbstractC94184bO A05;
    public final AbstractC115665Sl A06;
    public final AbstractC117135Yq A07;
    public final AbstractC115255Qu A08;
    public final AnonymousClass4DX A09;
    public final boolean A0A;
    public final int[] A0B;
    public final int[] A0C;
    public final Object[] A0D;

    public C1090050a(AnonymousClass4DV r1, AbstractC94184bO r2, AbstractC115665Sl r3, AbstractC117135Yq r4, AbstractC115255Qu r5, AnonymousClass4DX r6, int[] iArr, int[] iArr2, Object[] objArr, int i, int i2, int i3, int i4, boolean z) {
        this.A0B = iArr;
        this.A0D = objArr;
        this.A00 = i;
        this.A01 = i2;
        this.A0A = z;
        this.A0C = iArr2;
        this.A02 = i3;
        this.A03 = i4;
        this.A08 = r5;
        this.A05 = r2;
        this.A09 = r6;
        this.A04 = r1;
        this.A07 = r4;
        this.A06 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r5 >= 0) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(X.C93624aT r6, X.AnonymousClass5XU r7, byte[] r8, int r9, int r10) {
        /*
            int r4 = r9 + 1
            r3 = r8
            byte r5 = r8[r9]
            r1 = r6
            if (r5 >= 0) goto L_0x0010
            int r4 = A08(r6, r8, r5, r4)
            int r5 = r6.A00
            if (r5 < 0) goto L_0x0022
        L_0x0010:
            int r10 = r10 - r4
            if (r5 > r10) goto L_0x0022
            r0 = r7
            java.lang.Object r2 = r7.Agm()
            int r5 = r5 + r4
            r0.Agz(r1, r2, r3, r4, r5)
            r7.AhF(r2)
            r6.A02 = r2
            return r5
        L_0x0022:
            X.48y r0 = X.C868148y.A01()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.A00(X.4aT, X.5XU, byte[], int, int):int");
    }

    public static int A01(C93624aT r2, AnonymousClass5XU r3, byte[] bArr, int i, int i2, int i3) {
        C1090050a r1 = (C1090050a) r3;
        Object Agm = r1.Agm();
        int A0G = r1.A0G(r2, Agm, bArr, i, i2, i3);
        r1.AhF(Agm);
        r2.A02 = Agm;
        return A0G;
    }

    public static int A02(C93624aT r9, C94994cs r10, byte[] bArr, int i, int i2, int i3) {
        Object r1;
        int i4 = i2;
        if ((i >>> 3) != 0) {
            int i5 = i & 7;
            if (i5 == 0) {
                int A04 = A04(r9, bArr, i2);
                r10.A02(i, Long.valueOf(r9.A01));
                return A04;
            } else if (i5 == 1) {
                r10.A02(i, Long.valueOf(C72453ed.A0b(bArr, i2)));
                return i2 + 8;
            } else if (i5 == 2) {
                int A03 = A03(r9, bArr, i2);
                int i6 = r9.A00;
                if (i6 >= 0) {
                    int length = bArr.length;
                    if (i6 <= length - A03) {
                        if (i6 == 0) {
                            r1 = AbstractC111925Bi.A00;
                        } else {
                            AbstractC111925Bi.A01(A03, A03 + i6, length);
                            r1 = new C80273rz(AbstractC111925Bi.A01.Agu(bArr, A03, i6));
                        }
                        r10.A02(i, r1);
                        return A03 + i6;
                    }
                    throw C868148y.A01();
                }
                throw new C868148y("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
            } else if (i5 == 3) {
                C94994cs A00 = C94994cs.A00();
                int i7 = (i & -8) | 4;
                int i8 = 0;
                while (i4 < i3) {
                    i4 = A03(r9, bArr, i4);
                    i8 = r9.A00;
                    if (i8 == i7) {
                        break;
                    }
                    i4 = A02(r9, A00, bArr, i8, i4, i3);
                }
                if (i4 > i3 || i8 != i7) {
                    throw new C868148y("Failed to parse the message.");
                }
                r10.A02(i, A00);
                return i4;
            } else if (i5 == 5) {
                r10.A02(i, Integer.valueOf(C72453ed.A0K(bArr, i2)));
                return i2 + 4;
            } else {
                throw new C868148y("Protocol message contained an invalid tag (zero).");
            }
        } else {
            throw new C868148y("Protocol message contained an invalid tag (zero).");
        }
    }

    public static int A03(C93624aT r2, byte[] bArr, int i) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return A08(r2, bArr, b, i2);
        }
        r2.A00 = b;
        return i2;
    }

    public static int A04(C93624aT r9, byte[] bArr, int i) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            r9.A01 = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b = bArr[i2];
        long j2 = (j & 127) | (((long) (b & Byte.MAX_VALUE)) << 7);
        int i4 = 7;
        while (b < 0) {
            b = bArr[i3];
            i4 += 7;
            j2 |= ((long) (b & Byte.MAX_VALUE)) << i4;
            i3++;
        }
        r9.A01 = j2;
        return i3;
    }

    public static int A05(C93624aT r4, byte[] bArr, int i) {
        int A03 = A03(r4, bArr, i);
        int i2 = r4.A00;
        if (i2 < 0) {
            throw new C868148y("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
        } else if (i2 == 0) {
            r4.A02 = "";
            return A03;
        } else {
            r4.A02 = new String(bArr, A03, i2, C93104Zc.A02);
            return A03 + i2;
        }
    }

    public static int A06(C93624aT r16, byte[] bArr, int i) {
        String str;
        int i2;
        int i3;
        int A03 = A03(r16, bArr, i);
        int i4 = r16.A00;
        if (i4 < 0) {
            throw new C868148y("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
        } else if (i4 == 0) {
            r16.A02 = "";
            return A03;
        } else {
            int i5 = A03;
            if (!(C94634cG.A00 instanceof C80513sN)) {
                int length = bArr.length;
                if ((A03 | i4 | ((length - A03) - i4)) >= 0) {
                    int i6 = A03 + i4;
                    char[] cArr = new char[i4];
                    int i7 = 0;
                    while (i5 < i6) {
                        byte b = bArr[i5];
                        if (b < 0) {
                            break;
                        }
                        i5++;
                        cArr[i7] = (char) b;
                        i7++;
                    }
                    while (i5 < i6) {
                        int i8 = i5 + 1;
                        byte b2 = bArr[i5];
                        if (b2 >= 0) {
                            int i9 = i7 + 1;
                            cArr[i7] = (char) b2;
                            while (i8 < i6) {
                                byte b3 = bArr[i8];
                                if (b3 < 0) {
                                    break;
                                }
                                i8++;
                                cArr[i9] = (char) b3;
                                i9++;
                            }
                            i5 = i8;
                            i7 = i9;
                        } else if (b2 >= -32) {
                            if (b2 < -16) {
                                if (i8 < i6 - 1) {
                                    int i10 = i8 + 1;
                                    byte b4 = bArr[i8];
                                    i5 = i10 + 1;
                                    byte b5 = bArr[i10];
                                    i3 = i7 + 1;
                                    C92964Ye.A01(cArr, b2, b4, b5, i7);
                                } else {
                                    throw C868148y.A00();
                                }
                            } else if (i8 < i6 - 2) {
                                int i11 = i8 + 1;
                                byte b6 = bArr[i8];
                                int i12 = i11 + 1;
                                byte b7 = bArr[i11];
                                i5 = i12 + 1;
                                C92964Ye.A00(cArr, b2, b6, b7, bArr[i12], i7);
                                i3 = i7 + 1 + 1;
                            } else {
                                throw C868148y.A00();
                            }
                            i7 = i3;
                        } else if (i8 < i6) {
                            i5 = i8 + 1;
                            byte b8 = bArr[i8];
                            int i13 = i7 + 1;
                            if (b2 < -62 || b8 > -65) {
                                throw C868148y.A00();
                            }
                            cArr[i7] = (char) (((b2 & 31) << 6) | (b8 & 63));
                            i7 = i13;
                        } else {
                            throw C868148y.A00();
                        }
                    }
                    str = new String(cArr, 0, i7);
                } else {
                    Object[] objArr = new Object[3];
                    C12960it.A1P(objArr, length, 0);
                    C12960it.A1P(objArr, A03, 1);
                    C12990iw.A1V(objArr, i4);
                    throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", objArr));
                }
            } else {
                int length2 = bArr.length;
                if ((A03 | i4 | ((length2 - A03) - i4)) >= 0) {
                    int i14 = A03 + i4;
                    char[] cArr2 = new char[i4];
                    int i15 = 0;
                    while (i5 < i14) {
                        byte A00 = C95634e6.A00(bArr, (long) i5);
                        if (A00 < 0) {
                            break;
                        }
                        i5++;
                        cArr2[i15] = (char) A00;
                        i15++;
                    }
                    while (i5 < i14) {
                        int i16 = i5 + 1;
                        byte A002 = C95634e6.A00(bArr, (long) i5);
                        if (A002 >= 0) {
                            int i17 = i15 + 1;
                            cArr2[i15] = (char) A002;
                            while (i16 < i14) {
                                byte A003 = C95634e6.A00(bArr, (long) i16);
                                if (A003 < 0) {
                                    break;
                                }
                                i16++;
                                cArr2[i17] = (char) A003;
                                i17++;
                            }
                            i5 = i16;
                            i15 = i17;
                        } else if (A002 >= -32) {
                            if (A002 < -16) {
                                if (i16 < i14 - 1) {
                                    int i18 = i16 + 1;
                                    i5 = i18 + 1;
                                    i2 = i15 + 1;
                                    C92964Ye.A01(cArr2, A002, C95634e6.A00(bArr, (long) i16), C95634e6.A00(bArr, (long) i18), i15);
                                } else {
                                    throw C868148y.A00();
                                }
                            } else if (i16 < i14 - 2) {
                                int i19 = i16 + 1;
                                byte A004 = C95634e6.A00(bArr, (long) i16);
                                int i20 = i19 + 1;
                                i5 = i20 + 1;
                                C92964Ye.A00(cArr2, A002, A004, C95634e6.A00(bArr, (long) i19), C95634e6.A00(bArr, (long) i20), i15);
                                i2 = i15 + 1 + 1;
                            } else {
                                throw C868148y.A00();
                            }
                            i15 = i2;
                        } else if (i16 < i14) {
                            i5 = i16 + 1;
                            byte A005 = C95634e6.A00(bArr, (long) i16);
                            int i21 = i15 + 1;
                            if (A002 < -62 || A005 > -65) {
                                throw C868148y.A00();
                            }
                            cArr2[i15] = (char) (((A002 & 31) << 6) | (A005 & 63));
                            i15 = i21;
                        } else {
                            throw C868148y.A00();
                        }
                    }
                    str = new String(cArr2, 0, i15);
                } else {
                    Object[] objArr2 = new Object[3];
                    C12960it.A1P(objArr2, length2, 0);
                    C12960it.A1P(objArr2, A03, 1);
                    C12990iw.A1V(objArr2, i4);
                    throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", objArr2));
                }
            }
            r16.A02 = str;
            return A03 + i4;
        }
    }

    public static int A07(C93624aT r4, byte[] bArr, int i) {
        int A03 = A03(r4, bArr, i);
        int i2 = r4.A00;
        if (i2 >= 0) {
            int length = bArr.length;
            if (i2 > length - A03) {
                throw C868148y.A01();
            } else if (i2 == 0) {
                r4.A02 = AbstractC111925Bi.A00;
                return A03;
            } else {
                AbstractC111925Bi.A01(A03, A03 + i2, length);
                r4.A02 = new C80273rz(AbstractC111925Bi.A01.Agu(bArr, A03, i2));
                return A03 + i2;
            }
        } else {
            throw new C868148y("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
        }
    }

    public static int A08(C93624aT r4, byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = i & 127;
        int i7 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            i5 = b << 7;
        } else {
            int i8 = i6 | ((b & Byte.MAX_VALUE) << 7);
            int i9 = i7 + 1;
            byte b2 = bArr[i7];
            if (b2 >= 0) {
                i4 = b2 << 14;
            } else {
                i6 = i8 | ((b2 & Byte.MAX_VALUE) << 14);
                i7 = i9 + 1;
                byte b3 = bArr[i9];
                if (b3 >= 0) {
                    i5 = b3 << 21;
                } else {
                    i8 = i6 | ((b3 & Byte.MAX_VALUE) << 21);
                    i9 = i7 + 1;
                    byte b4 = bArr[i7];
                    if (b4 >= 0) {
                        i4 = b4 << 28;
                    } else {
                        i3 = i8 | ((b4 & Byte.MAX_VALUE) << 28);
                        while (true) {
                            i7 = i9 + 1;
                            if (bArr[i9] >= 0) {
                                break;
                            }
                            i9 = i7;
                        }
                        r4.A00 = i3;
                        return i7;
                    }
                }
            }
            r4.A00 = i8 | i4;
            return i9;
        }
        i3 = i6 | i5;
        r4.A00 = i3;
        return i7;
    }

    public static int A09(C80253rx r2, AnonymousClass5XU r3, Object obj, int i) {
        int i2 = i << 3;
        r2.A05(3 | i2);
        r3.Agy(r2.A01, obj);
        return i2 | 4;
    }

    public static int A0A(C80253rx r2, AnonymousClass5XU r3, Object obj, int i) {
        r2.A05((i << 3) | 2);
        AnonymousClass50T r4 = (AnonymousClass50T) obj;
        AbstractC80173rp r22 = (AbstractC80173rp) r4;
        int i2 = r22.zzc;
        if (i2 != -1) {
            return i2;
        }
        int Ah2 = r3.Ah2(r4);
        r22.zzc = Ah2;
        return Ah2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01e2, code lost:
        if (r40 == false) goto L_0x01e4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C1090050a A0B(X.AnonymousClass4DV r41, X.AbstractC94184bO r42, X.AbstractC115665Sl r43, X.AbstractC115245Qt r44, X.AbstractC115255Qu r45, X.AnonymousClass4DX r46) {
        /*
        // Method dump skipped, instructions count: 906
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.A0B(X.4DV, X.4bO, X.5Sl, X.5Qt, X.5Qu, X.4DX):X.50a");
    }

    public static Object A0C(Object obj, int i) {
        return C95634e6.A03(obj, (long) (i & 1048575));
    }

    public static Field A0D(Class cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 40 + name.length() + C12970iu.A07(arrays));
            A0t.append("Field ");
            A0t.append(str);
            A0t.append(" for ");
            A0t.append(name);
            A0t.append(" not found. Known fields are ");
            throw C12990iw.A0m(C12960it.A0d(arrays, A0t));
        }
    }

    public static List A0E(int i, Object obj) {
        return (List) C95634e6.A03(obj, (long) (i & 1048575));
    }

    public final int A0F(int i) {
        if (i >= this.A00 && i <= this.A01) {
            int i2 = 0;
            int[] iArr = this.A0B;
            int length = (iArr.length / 3) - 1;
            while (i2 <= length) {
                int i3 = (length + i2) >>> 1;
                int i4 = i3 * 3;
                int i5 = iArr[i4];
                if (i == i5) {
                    return i4;
                }
                if (i < i5) {
                    length = i3 - 1;
                } else {
                    i2 = i3 + 1;
                }
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:187:0x000b */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:158:0x00a6 */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a3 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A0G(X.C93624aT r40, java.lang.Object r41, byte[] r42, int r43, int r44, int r45) {
        /*
        // Method dump skipped, instructions count: 816
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.A0G(X.4aT, java.lang.Object, byte[], int, int, int):int");
    }

    public final int A0H(C93624aT r22, Object obj, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j) {
        int i9;
        Object obj2;
        Object obj3;
        Unsafe unsafe = A0E;
        long A09 = C72463ee.A09(this.A0B, i8 + 2);
        switch (i7) {
            case 51:
                if (i5 == 1) {
                    unsafe.putObject(obj, j, Double.valueOf(Double.longBitsToDouble(C72453ed.A0b(bArr, i))));
                    i9 = i + 8;
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 52:
                if (i5 == 5) {
                    unsafe.putObject(obj, j, Float.valueOf(Float.intBitsToFloat(C72453ed.A0K(bArr, i))));
                    i9 = i + 4;
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 53:
            case 54:
                if (i5 == 0) {
                    i9 = A04(r22, bArr, i);
                    unsafe.putObject(obj, j, Long.valueOf(r22.A01));
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 55:
            case 62:
                if (i5 == 0) {
                    i9 = A03(r22, bArr, i);
                    unsafe.putObject(obj, j, Integer.valueOf(r22.A00));
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 56:
            case 65:
                if (i5 == 1) {
                    unsafe.putObject(obj, j, Long.valueOf(C72453ed.A0b(bArr, i)));
                    i9 = i + 8;
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 57:
            case 64:
                if (i5 == 5) {
                    unsafe.putObject(obj, j, Integer.valueOf(C72453ed.A0K(bArr, i)));
                    i9 = i + 4;
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 58:
                if (i5 == 0) {
                    i9 = A04(r22, bArr, i);
                    unsafe.putObject(obj, j, Boolean.valueOf(C12960it.A1S((r22.A01 > 0 ? 1 : (r22.A01 == 0 ? 0 : -1)))));
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 59:
                if (i5 == 2) {
                    i9 = A03(r22, bArr, i);
                    int i10 = r22.A00;
                    if (i10 == 0) {
                        unsafe.putObject(obj, j, "");
                    } else {
                        if ((i6 & 536870912) != 0) {
                            if (C94634cG.A00.A01(bArr, i9, i9 + i10) != 0) {
                                throw C868148y.A00();
                            }
                        }
                        unsafe.putObject(obj, j, new String(bArr, i9, i10, C93104Zc.A02));
                        i9 += i10;
                    }
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 60:
                if (i5 == 2) {
                    i9 = A00(r22, A0J(i8), bArr, i, i2);
                    if (unsafe.getInt(obj, A09) == i4) {
                        obj3 = unsafe.getObject(obj, j);
                    } else {
                        obj3 = null;
                    }
                    Object obj4 = r22.A02;
                    if (obj3 != null) {
                        obj4 = C93104Zc.A00(obj3, obj4);
                    }
                    unsafe.putObject(obj, j, obj4);
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 61:
                if (i5 == 2) {
                    i9 = A07(r22, bArr, i);
                    unsafe.putObject(obj, j, r22.A02);
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 63:
                if (i5 == 0) {
                    i9 = A03(r22, bArr, i);
                    int i11 = r22.A00;
                    AbstractC115655Sk r4 = (AbstractC115655Sk) this.A0D[((i8 / 3) << 1) + 1];
                    if (r4 == null || r4.Agr(i11)) {
                        unsafe.putObject(obj, j, Integer.valueOf(i11));
                        unsafe.putInt(obj, A09, i4);
                        return i9;
                    }
                    AbstractC80173rp r8 = (AbstractC80173rp) obj;
                    C94994cs r2 = r8.zzb;
                    if (r2 == C94994cs.A05) {
                        r2 = C94994cs.A00();
                        r8.zzb = r2;
                    }
                    r2.A02(i3, Long.valueOf((long) i11));
                    return i9;
                }
                return i;
            case 66:
                if (i5 == 0) {
                    i9 = A03(r22, bArr, i);
                    int i12 = r22.A00;
                    unsafe.putObject(obj, j, Integer.valueOf((-(i12 & 1)) ^ (i12 >>> 1)));
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 67:
                if (i5 == 0) {
                    i9 = A04(r22, bArr, i);
                    unsafe.putObject(obj, j, Long.valueOf(C72453ed.A0U(r22.A01)));
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            case 68:
                if (i5 == 3) {
                    i9 = A01(r22, A0J(i8), bArr, i, i2, (i3 & -8) | 4);
                    if (unsafe.getInt(obj, A09) == i4) {
                        obj2 = unsafe.getObject(obj, j);
                    } else {
                        obj2 = null;
                    }
                    Object obj5 = r22.A02;
                    if (obj2 == null) {
                        unsafe.putObject(obj, j, obj5);
                    } else {
                        unsafe.putObject(obj, j, C93104Zc.A00(obj2, obj5));
                    }
                    unsafe.putInt(obj, A09, i4);
                    return i9;
                }
                return i;
            default:
                return i;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x020e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A0I(X.C93624aT r18, java.lang.Object r19, byte[] r20, int r21, int r22, int r23, int r24, int r25, int r26, int r27, long r28, long r30) {
        /*
        // Method dump skipped, instructions count: 1198
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.A0I(X.4aT, java.lang.Object, byte[], int, int, int, int, int, int, int, long, long):int");
    }

    public final AnonymousClass5XU A0J(int i) {
        int i2 = (i / 3) << 1;
        Object[] objArr = this.A0D;
        AnonymousClass5XU r0 = (AnonymousClass5XU) objArr[i2];
        if (r0 != null) {
            return r0;
        }
        AnonymousClass5XU A00 = C93994b5.A02.A00((Class) objArr[i2 + 1]);
        objArr[i2] = A00;
        return A00;
    }

    public final void A0K(Object obj, long j) {
        AnonymousClass5IN r0;
        Unsafe unsafe = A0E;
        Object object = unsafe.getObject(obj, j);
        AbstractC115665Sl r3 = this.A06;
        if (!((AnonymousClass5IN) object).zza) {
            AnonymousClass5IN r2 = AnonymousClass5IN.A00;
            if (r2.isEmpty()) {
                r0 = new AnonymousClass5IN();
            } else {
                r0 = new AnonymousClass5IN(r2);
            }
            r3.Agn(r0, object);
            unsafe.putObject(obj, j, r0);
        }
        throw new NoSuchMethodError();
    }

    public final boolean A0L(int i, Object obj) {
        int[] iArr = this.A0B;
        int i2 = iArr[i + 2];
        long j = (long) (i2 & 1048575);
        if (j == 1048575) {
            int i3 = iArr[i + 1];
            long j2 = (long) (i3 & 1048575);
            switch (C72463ee.A01(i3)) {
                case 0:
                    if (C95634e6.A01.A02(obj, j2) != 0.0d) {
                        return true;
                    }
                    break;
                case 1:
                    if (C95634e6.A01.A03(obj, j2) != 0.0f) {
                        return true;
                    }
                    break;
                case 2:
                case 3:
                case 5:
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    if (C95634e6.A01.A05(obj, j2) != 0) {
                        return true;
                    }
                    break;
                case 4:
                case 6:
                case 11:
                case 12:
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                case 15:
                    if (C95634e6.A01.A04(obj, j2) != 0) {
                        return true;
                    }
                    break;
                case 7:
                    return C95634e6.A01.A0C(obj, j2);
                case 8:
                    Object A03 = C95634e6.A03(obj, j2);
                    if (A03 instanceof String) {
                        if (!((String) A03).isEmpty()) {
                            return true;
                        }
                    } else if (!(A03 instanceof AbstractC111925Bi)) {
                        throw C72453ed.A0h();
                    } else if (!AbstractC111925Bi.A00.equals(A03)) {
                        return true;
                    }
                    break;
                case 9:
                case 17:
                    if (C95634e6.A03(obj, j2) != null) {
                        return true;
                    }
                    break;
                case 10:
                    if (!AbstractC111925Bi.A00.equals(C95634e6.A03(obj, j2))) {
                        return true;
                    }
                    break;
                default:
                    throw C72453ed.A0h();
            }
        } else {
            if ((C95634e6.A01.A04(obj, j) & (1 << (i2 >>> 20))) != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean A0M(Object obj, int i, int i2) {
        return C12960it.A1V(C95634e6.A01.A04(obj, C72463ee.A09(this.A0B, i2 + 2)), i);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009e  */
    @Override // X.AnonymousClass5XU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int Agk(java.lang.Object r10) {
        /*
            r9 = this;
            int[] r5 = r9.A0B
            int r4 = r5.length
            r3 = 0
            r6 = 0
        L_0x0005:
            if (r3 >= r4) goto L_0x00ed
            int r0 = r3 + 1
            r7 = r5[r0]
            r8 = r5[r3]
            r0 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r0 & r7
            long r1 = (long) r0
            int r0 = X.C72463ee.A01(r7)
            r7 = 37
            switch(r0) {
                case 0: goto L_0x001e;
                case 1: goto L_0x0027;
                case 2: goto L_0x0048;
                case 3: goto L_0x0048;
                case 4: goto L_0x003e;
                case 5: goto L_0x0048;
                case 6: goto L_0x003e;
                case 7: goto L_0x0035;
                case 8: goto L_0x00a7;
                case 9: goto L_0x0052;
                case 10: goto L_0x00e0;
                case 11: goto L_0x003e;
                case 12: goto L_0x003e;
                case 13: goto L_0x003e;
                case 14: goto L_0x0048;
                case 15: goto L_0x003e;
                case 16: goto L_0x0048;
                case 17: goto L_0x0052;
                case 18: goto L_0x00e0;
                case 19: goto L_0x00e0;
                case 20: goto L_0x00e0;
                case 21: goto L_0x00e0;
                case 22: goto L_0x00e0;
                case 23: goto L_0x00e0;
                case 24: goto L_0x00e0;
                case 25: goto L_0x00e0;
                case 26: goto L_0x00e0;
                case 27: goto L_0x00e0;
                case 28: goto L_0x00e0;
                case 29: goto L_0x00e0;
                case 30: goto L_0x00e0;
                case 31: goto L_0x00e0;
                case 32: goto L_0x00e0;
                case 33: goto L_0x00e0;
                case 34: goto L_0x00e0;
                case 35: goto L_0x00e0;
                case 36: goto L_0x00e0;
                case 37: goto L_0x00e0;
                case 38: goto L_0x00e0;
                case 39: goto L_0x00e0;
                case 40: goto L_0x00e0;
                case 41: goto L_0x00e0;
                case 42: goto L_0x00e0;
                case 43: goto L_0x00e0;
                case 44: goto L_0x00e0;
                case 45: goto L_0x00e0;
                case 46: goto L_0x00e0;
                case 47: goto L_0x00e0;
                case 48: goto L_0x00e0;
                case 49: goto L_0x00e0;
                case 50: goto L_0x00e0;
                case 51: goto L_0x0060;
                case 52: goto L_0x0075;
                case 53: goto L_0x00c5;
                case 54: goto L_0x00c5;
                case 55: goto L_0x00b4;
                case 56: goto L_0x00c5;
                case 57: goto L_0x00b4;
                case 58: goto L_0x008a;
                case 59: goto L_0x00a1;
                case 60: goto L_0x00da;
                case 61: goto L_0x00da;
                case 62: goto L_0x00b4;
                case 63: goto L_0x00b4;
                case 64: goto L_0x00b4;
                case 65: goto L_0x00c5;
                case 66: goto L_0x00b4;
                case 67: goto L_0x00c5;
                case 68: goto L_0x00da;
                default: goto L_0x001b;
            }
        L_0x001b:
            int r3 = r3 + 3
            goto L_0x0005
        L_0x001e:
            int r6 = r6 * 53
            X.4YS r0 = X.C95634e6.A01
            double r0 = r0.A02(r10, r1)
            goto L_0x0070
        L_0x0027:
            int r6 = r6 * 53
            X.4YS r0 = X.C95634e6.A01
            float r0 = r0.A03(r10, r1)
            int r1 = java.lang.Float.floatToIntBits(r0)
            goto L_0x00ea
        L_0x0035:
            int r6 = r6 * 53
            X.4YS r0 = X.C95634e6.A01
            boolean r0 = r0.A0C(r10, r1)
            goto L_0x009a
        L_0x003e:
            int r6 = r6 * 53
            X.4YS r0 = X.C95634e6.A01
            int r1 = r0.A04(r10, r1)
            goto L_0x00ea
        L_0x0048:
            int r6 = r6 * 53
            X.4YS r0 = X.C95634e6.A01
            long r0 = r0.A05(r10, r1)
            goto L_0x00d5
        L_0x0052:
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            if (r0 == 0) goto L_0x005c
            int r7 = r0.hashCode()
        L_0x005c:
            int r6 = r6 * 53
            int r6 = r6 + r7
            goto L_0x001b
        L_0x0060:
            boolean r0 = r9.A0M(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            double r0 = X.C72453ed.A00(r0)
        L_0x0070:
            long r0 = java.lang.Double.doubleToLongBits(r0)
            goto L_0x00d5
        L_0x0075:
            boolean r0 = r9.A0M(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            float r0 = X.C72453ed.A02(r0)
            int r1 = java.lang.Float.floatToIntBits(r0)
            goto L_0x00ea
        L_0x008a:
            boolean r0 = r9.A0M(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            boolean r0 = X.C12970iu.A1Y(r0)
        L_0x009a:
            r1 = 1237(0x4d5, float:1.733E-42)
            if (r0 == 0) goto L_0x00ea
            r1 = 1231(0x4cf, float:1.725E-42)
            goto L_0x00ea
        L_0x00a1:
            boolean r0 = r9.A0M(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
        L_0x00a7:
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            java.lang.String r0 = (java.lang.String) r0
            int r1 = r0.hashCode()
            goto L_0x00ea
        L_0x00b4:
            boolean r0 = r9.A0M(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            int r1 = X.C12960it.A05(r0)
            goto L_0x00ea
        L_0x00c5:
            boolean r0 = r9.A0M(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            long r0 = X.C12980iv.A0G(r0)
        L_0x00d5:
            int r1 = X.C72453ed.A0B(r0)
            goto L_0x00ea
        L_0x00da:
            boolean r0 = r9.A0M(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
        L_0x00e0:
            java.lang.Object r0 = X.C95634e6.A03(r10, r1)
            int r6 = r6 * 53
            int r1 = r0.hashCode()
        L_0x00ea:
            int r6 = r6 + r1
            goto L_0x001b
        L_0x00ed:
            int r1 = r6 * 53
            X.3rp r10 = (X.AbstractC80173rp) r10
            X.4cs r0 = r10.zzb
            int r0 = X.C12990iw.A08(r0, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.Agk(java.lang.Object):int");
    }

    @Override // X.AnonymousClass5XU
    public final Object Agm() {
        return ((AbstractC80173rp) this.A07).A06(4);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0082 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00db A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0019 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0019 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0019 A[SYNTHETIC] */
    @Override // X.AnonymousClass5XU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean Agt(java.lang.Object r11, java.lang.Object r12) {
        /*
            r10 = this;
            int[] r7 = r10.A0B
            int r6 = r7.length
            r9 = 0
            r5 = 0
        L_0x0005:
            r2 = 1
            if (r5 >= r6) goto L_0x00b4
            int r0 = r5 + 1
            r4 = r7[r0]
            r3 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r4 & r3
            long r1 = (long) r0
            int r0 = X.C72463ee.A01(r4)
            switch(r0) {
                case 0: goto L_0x001c;
                case 1: goto L_0x003a;
                case 2: goto L_0x00c3;
                case 3: goto L_0x00c3;
                case 4: goto L_0x006c;
                case 5: goto L_0x00c3;
                case 6: goto L_0x006c;
                case 7: goto L_0x0057;
                case 8: goto L_0x0083;
                case 9: goto L_0x0083;
                case 10: goto L_0x0083;
                case 11: goto L_0x006c;
                case 12: goto L_0x006c;
                case 13: goto L_0x006c;
                case 14: goto L_0x00c3;
                case 15: goto L_0x006c;
                case 16: goto L_0x00c3;
                case 17: goto L_0x0083;
                case 18: goto L_0x00a0;
                case 19: goto L_0x00a0;
                case 20: goto L_0x00a0;
                case 21: goto L_0x00a0;
                case 22: goto L_0x00a0;
                case 23: goto L_0x00a0;
                case 24: goto L_0x00a0;
                case 25: goto L_0x00a0;
                case 26: goto L_0x00a0;
                case 27: goto L_0x00a0;
                case 28: goto L_0x00a0;
                case 29: goto L_0x00a0;
                case 30: goto L_0x00a0;
                case 31: goto L_0x00a0;
                case 32: goto L_0x00a0;
                case 33: goto L_0x00a0;
                case 34: goto L_0x00a0;
                case 35: goto L_0x00a0;
                case 36: goto L_0x00a0;
                case 37: goto L_0x00a0;
                case 38: goto L_0x00a0;
                case 39: goto L_0x00a0;
                case 40: goto L_0x00a0;
                case 41: goto L_0x00a0;
                case 42: goto L_0x00a0;
                case 43: goto L_0x00a0;
                case 44: goto L_0x00a0;
                case 45: goto L_0x00a0;
                case 46: goto L_0x00a0;
                case 47: goto L_0x00a0;
                case 48: goto L_0x00a0;
                case 49: goto L_0x00a0;
                case 50: goto L_0x00a0;
                case 51: goto L_0x008e;
                case 52: goto L_0x008e;
                case 53: goto L_0x008e;
                case 54: goto L_0x008e;
                case 55: goto L_0x008e;
                case 56: goto L_0x008e;
                case 57: goto L_0x008e;
                case 58: goto L_0x008e;
                case 59: goto L_0x008e;
                case 60: goto L_0x008e;
                case 61: goto L_0x008e;
                case 62: goto L_0x008e;
                case 63: goto L_0x008e;
                case 64: goto L_0x008e;
                case 65: goto L_0x008e;
                case 66: goto L_0x008e;
                case 67: goto L_0x008e;
                case 68: goto L_0x008e;
                default: goto L_0x0019;
            }
        L_0x0019:
            int r5 = r5 + 3
            goto L_0x0005
        L_0x001c:
            boolean r3 = r10.A0L(r5, r11)
            boolean r0 = r10.A0L(r5, r12)
            if (r3 != r0) goto L_0x00db
            X.4YS r0 = X.C95634e6.A01
            double r3 = r0.A02(r11, r1)
            long r3 = java.lang.Double.doubleToLongBits(r3)
            double r0 = r0.A02(r12, r1)
            long r1 = java.lang.Double.doubleToLongBits(r0)
            goto L_0x00d7
        L_0x003a:
            boolean r3 = r10.A0L(r5, r11)
            boolean r0 = r10.A0L(r5, r12)
            if (r3 != r0) goto L_0x00db
            X.4YS r4 = X.C95634e6.A01
            float r0 = r4.A03(r11, r1)
            int r3 = java.lang.Float.floatToIntBits(r0)
            float r0 = r4.A03(r12, r1)
            int r0 = java.lang.Float.floatToIntBits(r0)
            goto L_0x0080
        L_0x0057:
            boolean r3 = r10.A0L(r5, r11)
            boolean r0 = r10.A0L(r5, r12)
            if (r3 != r0) goto L_0x00db
            X.4YS r0 = X.C95634e6.A01
            boolean r3 = r0.A0C(r11, r1)
            boolean r0 = r0.A0C(r12, r1)
            goto L_0x0080
        L_0x006c:
            boolean r3 = r10.A0L(r5, r11)
            boolean r0 = r10.A0L(r5, r12)
            if (r3 != r0) goto L_0x00db
            X.4YS r0 = X.C95634e6.A01
            int r3 = r0.A04(r11, r1)
            int r0 = r0.A04(r12, r1)
        L_0x0080:
            if (r3 == r0) goto L_0x0019
            return r9
        L_0x0083:
            boolean r3 = r10.A0L(r5, r11)
            boolean r0 = r10.A0L(r5, r12)
            if (r3 != r0) goto L_0x00db
            goto L_0x00a0
        L_0x008e:
            int r0 = r5 + 2
            r0 = r7[r0]
            r0 = r0 & r3
            long r3 = (long) r0
            X.4YS r0 = X.C95634e6.A01
            int r8 = r0.A04(r11, r3)
            int r0 = r0.A04(r12, r3)
            if (r8 != r0) goto L_0x00db
        L_0x00a0:
            java.lang.Object r3 = X.C95634e6.A03(r11, r1)
            java.lang.Object r0 = X.C95634e6.A03(r12, r1)
            if (r3 == r0) goto L_0x0019
            if (r3 == 0) goto L_0x00db
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x00db
            goto L_0x0019
        L_0x00b4:
            X.3rp r11 = (X.AbstractC80173rp) r11
            X.4cs r1 = r11.zzb
            X.3rp r12 = (X.AbstractC80173rp) r12
            X.4cs r0 = r12.zzb
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00db
            return r2
        L_0x00c3:
            boolean r3 = r10.A0L(r5, r11)
            boolean r0 = r10.A0L(r5, r12)
            if (r3 != r0) goto L_0x00db
            X.4YS r0 = X.C95634e6.A01
            long r3 = r0.A05(r11, r1)
            long r1 = r0.A05(r12, r1)
        L_0x00d7:
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0019
        L_0x00db:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.Agt(java.lang.Object, java.lang.Object):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:111:0x0309  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x032c  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0346  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0695  */
    /* JADX WARNING: Removed duplicated region for block: B:279:0x06ed  */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x070e  */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x0726  */
    /* JADX WARNING: Removed duplicated region for block: B:372:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:378:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:380:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:382:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:433:0x0455 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:439:0x0455 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:441:0x0455 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x0455 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x029a  */
    @Override // X.AnonymousClass5XU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Agy(X.AbstractC115295Qy r22, java.lang.Object r23) {
        /*
        // Method dump skipped, instructions count: 2296
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.Agy(X.5Qy, java.lang.Object):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a3  */
    @Override // X.AnonymousClass5XU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Agz(X.C93624aT r37, java.lang.Object r38, byte[] r39, int r40, int r41) {
        /*
        // Method dump skipped, instructions count: 626
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.Agz(X.4aT, java.lang.Object, byte[], int, int):void");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:291:0x02d2 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:289:0x0645 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v10, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v57, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v8, types: [java.lang.CharSequence, java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v12, types: [int] */
    /* JADX WARN: Type inference failed for: r1v55, types: [java.lang.CharSequence, java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v59, types: [int] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02a4  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x02b2  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x02ec  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0300  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x034a  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0359  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0395  */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x05ad  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x0617  */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x0625  */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x065f  */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x0673  */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x06bd  */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x06ca  */
    /* JADX WARNING: Removed duplicated region for block: B:285:0x0706  */
    /* JADX WARNING: Removed duplicated region for block: B:306:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:309:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:310:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:314:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:332:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:333:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:334:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:335:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:338:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:339:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x03c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x023a  */
    @Override // X.AnonymousClass5XU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int Ah2(java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 2110
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.Ah2(java.lang.Object):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:110:0x0019 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01a1  */
    @Override // X.AnonymousClass5XU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void AhA(java.lang.Object r13, java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 642
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1090050a.AhA(java.lang.Object, java.lang.Object):void");
    }

    @Override // X.AnonymousClass5XU
    public final void AhF(Object obj) {
        int i;
        Object unmodifiableList;
        int i2 = this.A02;
        while (true) {
            i = this.A03;
            if (i2 >= i) {
                break;
            }
            long A09 = C72463ee.A09(this.A0B, this.A0C[i2] + 1);
            Object A03 = C95634e6.A03(obj, A09);
            if (A03 != null) {
                ((AnonymousClass5IN) A03).zza = false;
                C95634e6.A09(obj, A09, A03);
            }
            i2++;
        }
        int[] iArr = this.A0C;
        int length = iArr.length;
        while (i < length) {
            AbstractC94184bO r3 = this.A05;
            long j = (long) iArr[i];
            if (!(r3 instanceof C80453sH)) {
                List list = (List) C95634e6.A03(obj, j);
                if (list instanceof AnonymousClass5Z4) {
                    unmodifiableList = ((AnonymousClass5Z4) list).AhK();
                } else if (!C80463sI.A00.isAssignableFrom(list.getClass())) {
                    if (!(list instanceof AbstractC115265Qv) || !(list instanceof AnonymousClass5Z6)) {
                        unmodifiableList = Collections.unmodifiableList(list);
                    } else {
                        AnonymousClass5I0 r7 = (AnonymousClass5I0) ((AnonymousClass5Z6) list);
                        if (r7.A00) {
                            r7.A00 = false;
                        }
                    }
                }
                C95634e6.A09(obj, j, unmodifiableList);
            } else {
                ((AnonymousClass5I0) ((AnonymousClass5Z6) C95634e6.A03(obj, j))).A00 = false;
            }
            i++;
        }
        ((AbstractC80173rp) obj).zzb.A02 = false;
    }

    @Override // X.AnonymousClass5XU
    public final boolean AhJ(Object obj) {
        boolean z;
        int i = 1048575;
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.A02) {
            int i4 = this.A0C[i3];
            int[] iArr = this.A0B;
            int i5 = iArr[i4];
            int i6 = iArr[i4 + 1];
            int i7 = iArr[i4 + 2];
            int i8 = i7 & 1048575;
            int i9 = 1 << (i7 >>> 20);
            if (i8 == i) {
                i8 = i;
            } else if (i8 != 1048575) {
                i2 = A0E.getInt(obj, (long) i8);
            }
            if ((268435456 & i6) != 0) {
                if (i8 == 1048575) {
                    if (!A0L(i4, obj)) {
                    }
                } else if ((i2 & i9) == 0) {
                }
                return false;
            }
            int i10 = (267386880 & i6) >>> 20;
            if (i10 == 9 || i10 == 17) {
                z = i8 == 1048575 ? A0L(i4, obj) : i2 & i9;
            } else {
                if (i10 != 27) {
                    if (i10 == 60 || i10 == 68) {
                        z = A0M(obj, i5, i4);
                    } else if (i10 != 49) {
                        if (i10 == 50 && !((AbstractMap) A0C(obj, i6)).isEmpty()) {
                            throw new NoSuchMethodError();
                        }
                        i3++;
                        i = i8;
                    }
                }
                List A0E2 = A0E(i6, obj);
                if (!A0E2.isEmpty()) {
                    AnonymousClass5XU A0J = A0J(i4);
                    for (int i11 = 0; i11 < A0E2.size(); i11++) {
                        if (!A0J.AhJ(A0E2.get(i11))) {
                            return false;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
                i3++;
                i = i8;
            }
            if (z && !A0J(i4).AhJ(A0C(obj, i6))) {
                return false;
            }
            i3++;
            i = i8;
        }
        return true;
    }
}
