package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Xm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30461Xm extends AnonymousClass1XB implements AbstractC30471Xn {
    public int A00;
    public List A01 = new ArrayList();
    public final AnonymousClass1YM A02;
    public transient long A03;
    public final transient AnonymousClass1OT A04;

    public C30461Xm(AnonymousClass1IS r2, AnonymousClass1OT r3, int i, long j) {
        super(r2, i, j);
        this.A04 = r3;
        this.A02 = null;
    }

    public C30461Xm(AnonymousClass1YM r5, AnonymousClass1OT r6, int i, long j) {
        super(new AnonymousClass1IS(C15380n4.A00(r6.A01), r6.A07, true), i, j);
        this.A02 = r5;
        A0Y(6);
        this.A04 = r6;
    }

    @Override // X.AbstractC30471Xn
    public AnonymousClass1OT AGs() {
        return this.A04;
    }
}
