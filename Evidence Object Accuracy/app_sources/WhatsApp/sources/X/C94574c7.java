package X;

/* renamed from: X.4c7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94574c7 {
    public static void A00(byte[] bArr, int[] iArr) {
        long A0c = C72453ed.A0c(bArr, 0);
        long A0a = C72453ed.A0a(bArr, 4) << 6;
        long A0a2 = C72453ed.A0a(bArr, 10) << 3;
        long A0c2 = C72453ed.A0c(bArr, 16);
        long A0a3 = C72453ed.A0a(bArr, 23) << 5;
        long A0a4 = (C72453ed.A0a(bArr, 29) & 8388607) << 2;
        long j = (A0a4 + 16777216) >> 25;
        long j2 = A0c + (19 * j);
        long j3 = (A0a + 16777216) >> 25;
        long A0a5 = (C72453ed.A0a(bArr, 7) << 5) + j3;
        long j4 = A0a - (j3 << 25);
        long j5 = (A0a2 + 16777216) >> 25;
        long A0a6 = (C72453ed.A0a(bArr, 13) << 2) + j5;
        long j6 = A0a2 - (j5 << 25);
        long j7 = (A0c2 + 16777216) >> 25;
        long A0a7 = (C72453ed.A0a(bArr, 20) << 7) + j7;
        long j8 = A0c2 - (j7 << 25);
        long j9 = (A0a3 + 16777216) >> 25;
        long A0a8 = (C72453ed.A0a(bArr, 26) << 4) + j9;
        long j10 = A0a3 - (j9 << 25);
        long j11 = (j2 + 33554432) >> 26;
        long j12 = j4 + j11;
        long j13 = j2 - (j11 << 26);
        long j14 = (A0a5 + 33554432) >> 26;
        long j15 = j6 + j14;
        long j16 = A0a5 - (j14 << 26);
        long j17 = (A0a6 + 33554432) >> 26;
        long j18 = j8 + j17;
        long j19 = A0a6 - (j17 << 26);
        long j20 = (A0a7 + 33554432) >> 26;
        long j21 = j10 + j20;
        long j22 = A0a7 - (j20 << 26);
        long j23 = (A0a8 + 33554432) >> 26;
        iArr[0] = (int) j13;
        iArr[1] = (int) j12;
        iArr[2] = (int) j16;
        iArr[3] = (int) j15;
        iArr[4] = (int) j19;
        iArr[5] = (int) j18;
        iArr[6] = (int) j22;
        iArr[7] = (int) j21;
        iArr[8] = (int) (A0a8 - (j23 << 26));
        iArr[9] = (int) ((A0a4 - (j << 25)) + j23);
    }
}
