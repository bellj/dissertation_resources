package X;

import android.content.Context;

/* renamed from: X.5g4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120365g4 extends AbstractC129955yZ {
    public final Context A00;
    public final AnonymousClass102 A01;
    public final C17220qS A02;
    public final AnonymousClass60Z A03;
    public final C129095xA A04;
    public final C128915ws A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;

    public C120365g4(Context context, C14900mE r14, C15570nT r15, C14830m7 r16, AnonymousClass102 r17, C241414j r18, C17220qS r19, AnonymousClass60Z r20, C18650sn r21, C18610sj r22, C17070qD r23, AnonymousClass60T r24, C129385xd r25, C129095xA r26, C128915ws r27, String str, String str2, String str3, String str4) {
        super(context, r14, r15, r16, r18, r21, r22, r23, r24, r25);
        this.A00 = context;
        this.A02 = r19;
        this.A01 = r17;
        this.A04 = r26;
        this.A03 = r20;
        this.A09 = str;
        this.A08 = str2;
        this.A06 = str3;
        this.A07 = str4;
        this.A05 = r27;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001f, code lost:
        if (r4 != 5) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(java.lang.String r15) {
        /*
            r14 = this;
            java.lang.String r0 = "PAY: BrazilVerifyCardOTPSendAction sendOtp"
            com.whatsapp.util.Log.i(r0)
            X.0qS r2 = r14.A02
            java.lang.String r1 = r2.A01()
            java.lang.String r10 = r14.A06
            java.lang.String r11 = r14.A08
            java.lang.String r13 = r14.A07
            X.14j r6 = r14.A04
            X.1Pl r0 = r6.A08(r10)
            X.1Ze r0 = (X.C30881Ze) r0
            if (r0 == 0) goto L_0x0021
            int r4 = r0.A01
            r3 = 5
            r0 = 1
            if (r4 == r3) goto L_0x0022
        L_0x0021:
            r0 = 0
        L_0x0022:
            r8 = 0
            if (r0 == 0) goto L_0x006f
            java.lang.StringBuilder r3 = X.C12960it.A0h()
            X.5xd r7 = r14.A09
            X.5vW r0 = r7.A00
            if (r0 == 0) goto L_0x008c
            java.lang.String r5 = r0.A02
            r0.A02 = r8
        L_0x0033:
            r3.append(r5)
            X.5xA r4 = r14.A04
            r0 = 5
            java.lang.String r0 = r4.A00(r0)
            r3.append(r0)
            X.0si r0 = r7.A0E
            java.lang.String r0 = r0.A06()
            r3.append(r0)
            X.1Pl r0 = r6.A08(r10)
            if (r0 == 0) goto L_0x0057
            X.1ZY r0 = r0.A08
            X.5f5 r0 = (X.C119775f5) r0
            if (r0 == 0) goto L_0x0057
            java.lang.String r8 = r0.A06
        L_0x0057:
            r3.append(r8)
            java.lang.String r0 = r14.A09
            java.lang.String r4 = X.C12960it.A0d(r0, r3)
            X.60Z r0 = r14.A03
            java.lang.String r3 = r0.A04(r4)
            java.lang.String r0 = r0.A05(r4)
            X.5sj r8 = new X.5sj
            r8.<init>(r3, r0, r5)
        L_0x006f:
            X.3CT r9 = new X.3CT
            r9.<init>(r1)
            r12 = r15
            X.5si r7 = new X.5si
            r7.<init>(r8, r9, r10, r11, r12, r13)
            X.1V8 r0 = r7.A00
            android.content.Context r11 = r14.A00
            X.0mE r12 = r14.A01
            X.0sn r13 = r14.A05
            r15 = 7
            com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1 r10 = new com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1
            r10.<init>(r11, r12, r13, r14, r15)
            X.C117295Zj.A1B(r2, r10, r0, r1)
            return
        L_0x008c:
            r5 = r8
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120365g4.A03(java.lang.String):void");
    }
}
