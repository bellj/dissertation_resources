package X;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileFilter;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.2fv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53992fv extends AnonymousClass0ER {
    public List A00;
    public final AnonymousClass018 A01;
    public final C14850m9 A02;
    public final File[] A03;

    public C53992fv(Context context, C14330lG r6, AnonymousClass018 r7, C14850m9 r8) {
        super(context);
        this.A02 = r8;
        this.A01 = r7;
        File file = r6.A04().A02;
        C14330lG.A03(file, false);
        this.A03 = new File[]{new File(Environment.getExternalStorageDirectory(), "Download"), Environment.getExternalStorageDirectory(), file, new File(Environment.getExternalStorageDirectory(), "Documents")};
    }

    @Override // X.AnonymousClass0QL
    public void A01() {
        A00();
        this.A00 = null;
    }

    @Override // X.AnonymousClass0QL
    public void A02() {
        A00();
    }

    @Override // X.AnonymousClass0QL
    public void A03() {
        List list = this.A00;
        if (list != null && !this.A05) {
            this.A00 = list;
            if (this.A06) {
                super.A04(list);
            }
        }
        boolean z = super.A03;
        super.A03 = false;
        this.A04 |= z;
        if (z || this.A00 == null) {
            A09();
        }
    }

    @Override // X.AnonymousClass0QL
    public /* bridge */ /* synthetic */ void A04(Object obj) {
        List list = (List) obj;
        if (!this.A05) {
            this.A00 = list;
            if (this.A06) {
                super.A04(list);
            }
        }
    }

    @Override // X.AnonymousClass0ER
    public /* bridge */ /* synthetic */ Object A06() {
        ArrayList A0w = C12980iv.A0w(128);
        for (File file : this.A03) {
            File[] listFiles = file.listFiles(new FileFilter() { // from class: X.5BH
                @Override // java.io.FileFilter
                public final boolean accept(File file2) {
                    if (!file2.isFile() || C22200yh.A0O(C14350lI.A07(file2.getAbsolutePath())) == null) {
                        return false;
                    }
                    return true;
                }
            });
            if (listFiles != null) {
                long A02 = ((long) this.A02.A02(542)) * 1048576;
                for (File file2 : listFiles) {
                    AnonymousClass4XI r3 = new AnonymousClass4XI(file2);
                    if (r3.A01 <= A02) {
                        A0w.add(r3);
                    }
                }
            }
        }
        Collator instance = Collator.getInstance(C12970iu.A14(this.A01));
        instance.setDecomposition(1);
        Collections.sort(A0w, new C112135Ce(instance));
        return A0w;
    }
}
