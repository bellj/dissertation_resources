package X;

import java.util.Arrays;

/* renamed from: X.0Xe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07230Xe implements AbstractC12730iP {
    public int A00 = 16;
    public int A01 = 16;
    public int A02 = -1;
    public int A03 = 0;
    public float[] A04 = new float[16];
    public int[] A05 = new int[16];
    public int[] A06 = new int[16];
    public int[] A07 = new int[16];
    public int[] A08 = new int[16];
    public int[] A09 = new int[16];
    public final C07240Xf A0A;
    public final AnonymousClass0NX A0B;

    public C07230Xe(C07240Xf r4, AnonymousClass0NX r5) {
        this.A0A = r4;
        this.A0B = r5;
        clear();
    }

    public int A00(AnonymousClass0QC r7) {
        int i;
        int i2;
        int[] iArr;
        if (!(this.A03 == 0 || (i2 = this.A05[(i = r7.A02) % this.A00]) == -1)) {
            int[] iArr2 = this.A09;
            if (iArr2[i2] == i) {
                return i2;
            }
            while (true) {
                iArr = this.A07;
                if (iArr[i2] == -1 || iArr2[iArr[i2]] == i) {
                    break;
                }
                i2 = iArr[i2];
            }
            int i3 = iArr[i2];
            if (i3 != -1 && iArr2[i3] == i) {
                return i3;
            }
        }
        return -1;
    }

    public final void A01(AnonymousClass0QC r3, float f, int i) {
        this.A09[i] = r3.A02;
        this.A04[i] = f;
        this.A08[i] = -1;
        this.A06[i] = -1;
        r3.A01(this.A0A);
        r3.A05++;
        this.A03++;
    }

    public final void A02(AnonymousClass0QC r5, int i) {
        int[] iArr;
        int i2 = r5.A02 % this.A00;
        int[] iArr2 = this.A05;
        int i3 = iArr2[i2];
        if (i3 == -1) {
            iArr2[i2] = i;
        } else {
            while (true) {
                iArr = this.A07;
                if (iArr[i3] == -1) {
                    break;
                }
                i3 = iArr[i3];
            }
            iArr[i3] = i;
        }
        this.A07[i] = -1;
    }

    @Override // X.AbstractC12730iP
    public void A5c(AnonymousClass0QC r7, float f, boolean z) {
        float f2 = -0.001f;
        if (f <= f2 || f >= 0.001f) {
            int A00 = A00(r7);
            if (A00 == -1) {
                AZg(r7, f);
                return;
            }
            float[] fArr = this.A04;
            float f3 = fArr[A00] + f;
            fArr[A00] = f3;
            if (f3 > f2 && f3 < 0.001f) {
                fArr[A00] = 0.0f;
                AaE(r7, z);
            }
        }
    }

    @Override // X.AbstractC12730iP
    public boolean A7d(AnonymousClass0QC r4) {
        return A00(r4) != -1;
    }

    @Override // X.AbstractC12730iP
    public void A95(float f) {
        int i = this.A03;
        int i2 = this.A02;
        for (int i3 = 0; i3 < i; i3++) {
            float[] fArr = this.A04;
            fArr[i2] = fArr[i2] / f;
            i2 = this.A06[i2];
            if (i2 == -1) {
                return;
            }
        }
    }

    @Override // X.AbstractC12730iP
    public float AAK(AnonymousClass0QC r3) {
        int A00 = A00(r3);
        if (A00 != -1) {
            return this.A04[A00];
        }
        return 0.0f;
    }

    @Override // X.AbstractC12730iP
    public int ACD() {
        return this.A03;
    }

    @Override // X.AbstractC12730iP
    public AnonymousClass0QC AHV(int i) {
        int i2 = this.A03;
        if (i2 != 0) {
            int i3 = this.A02;
            for (int i4 = 0; i4 < i2; i4++) {
                if (i4 == i && i3 != -1) {
                    return this.A0B.A03[this.A09[i3]];
                }
                i3 = this.A06[i3];
                if (i3 == -1) {
                    break;
                }
            }
        }
        return null;
    }

    @Override // X.AbstractC12730iP
    public float AHW(int i) {
        int i2 = this.A03;
        int i3 = this.A02;
        for (int i4 = 0; i4 < i2; i4++) {
            if (i4 == i) {
                return this.A04[i3];
            }
            i3 = this.A06[i3];
            if (i3 == -1) {
                return 0.0f;
            }
        }
        return 0.0f;
    }

    @Override // X.AbstractC12730iP
    public void AJ2() {
        int i = this.A03;
        int i2 = this.A02;
        for (int i3 = 0; i3 < i; i3++) {
            float[] fArr = this.A04;
            fArr[i2] = fArr[i2] * -1.0f;
            i2 = this.A06[i2];
            if (i2 == -1) {
                return;
            }
        }
    }

    @Override // X.AbstractC12730iP
    public void AZg(AnonymousClass0QC r10, float f) {
        int[] iArr;
        if (f <= (-0.001f) || f >= 0.001f) {
            int i = this.A03;
            if (i == 0) {
                A01(r10, f, 0);
                A02(r10, 0);
                this.A02 = 0;
                return;
            }
            int A00 = A00(r10);
            if (A00 != -1) {
                this.A04[A00] = f;
                return;
            }
            int i2 = i + 1;
            int i3 = this.A01;
            if (i2 >= i3) {
                i3 <<= 1;
                this.A09 = Arrays.copyOf(this.A09, i3);
                this.A04 = Arrays.copyOf(this.A04, i3);
                this.A08 = Arrays.copyOf(this.A08, i3);
                this.A06 = Arrays.copyOf(this.A06, i3);
                int[] copyOf = Arrays.copyOf(this.A07, i3);
                this.A07 = copyOf;
                for (int i4 = this.A01; i4 < i3; i4++) {
                    this.A09[i4] = -1;
                    copyOf[i4] = -1;
                }
                this.A01 = i3;
            }
            int i5 = this.A03;
            int i6 = this.A02;
            int i7 = -1;
            for (int i8 = 0; i8 < i5; i8++) {
                int[] iArr2 = this.A09;
                int i9 = iArr2[i6];
                int i10 = r10.A02;
                if (i9 != i10) {
                    if (iArr2[i6] < i10) {
                        i7 = i6;
                    }
                    i6 = this.A06[i6];
                    if (i6 == -1) {
                        break;
                    }
                } else {
                    this.A04[i6] = f;
                    return;
                }
            }
            int i11 = 0;
            while (true) {
                if (i11 >= i3) {
                    i11 = -1;
                    break;
                }
                if (this.A09[i11] == -1) {
                    break;
                }
                i11++;
            }
            A01(r10, f, i11);
            int[] iArr3 = this.A08;
            if (i7 != -1) {
                iArr3[i11] = i7;
                iArr = this.A06;
                iArr[i11] = iArr[i7];
                iArr[i7] = i11;
            } else {
                iArr3[i11] = -1;
                int i12 = this.A03;
                iArr = this.A06;
                if (i12 > 0) {
                    iArr[i11] = this.A02;
                    this.A02 = i11;
                } else {
                    iArr[i11] = -1;
                }
            }
            int i13 = iArr[i11];
            if (i13 != -1) {
                iArr3[i13] = i11;
            }
            A02(r10, i11);
            return;
        }
        AaE(r10, true);
    }

    @Override // X.AbstractC12730iP
    public float AaE(AnonymousClass0QC r10, boolean z) {
        int[] iArr;
        int A00 = A00(r10);
        if (A00 == -1) {
            return 0.0f;
        }
        int i = r10.A02;
        int i2 = i % this.A00;
        int[] iArr2 = this.A05;
        int i3 = iArr2[i2];
        if (i3 != -1) {
            int[] iArr3 = this.A09;
            if (iArr3[i3] == i) {
                int[] iArr4 = this.A07;
                iArr2[i2] = iArr4[i3];
                iArr4[i3] = -1;
            } else {
                while (true) {
                    iArr = this.A07;
                    if (iArr[i3] == -1 || iArr3[iArr[i3]] == i) {
                        break;
                    }
                    i3 = iArr[i3];
                }
                int i4 = iArr[i3];
                if (i4 != -1 && iArr3[i4] == i) {
                    iArr[i3] = iArr[i4];
                    iArr[i4] = -1;
                }
            }
        }
        float f = this.A04[A00];
        if (this.A02 == A00) {
            this.A02 = this.A06[A00];
        }
        this.A09[A00] = -1;
        int[] iArr5 = this.A08;
        int i5 = iArr5[A00];
        if (i5 != -1) {
            int[] iArr6 = this.A06;
            iArr6[i5] = iArr6[A00];
        }
        int i6 = this.A06[A00];
        if (i6 != -1) {
            iArr5[i6] = iArr5[A00];
        }
        this.A03--;
        r10.A05--;
        if (z) {
            r10.A02(this.A0A);
        }
        return f;
    }

    @Override // X.AbstractC12730iP
    public float Afd(C07240Xf r10, boolean z) {
        AnonymousClass0QC r0 = r10.A02;
        float AAK = AAK(r0);
        AaE(r0, z);
        C07230Xe r7 = (C07230Xe) r10.A01;
        int i = r7.A03;
        int i2 = 0;
        int i3 = 0;
        while (i2 < i) {
            int[] iArr = r7.A09;
            if (iArr[i3] != -1) {
                A5c(this.A0B.A03[iArr[i3]], r7.A04[i3] * AAK, z);
                i2++;
            }
            i3++;
        }
        return AAK;
    }

    @Override // X.AbstractC12730iP
    public void clear() {
        int i = this.A03;
        for (int i2 = 0; i2 < i; i2++) {
            AnonymousClass0QC AHV = AHV(i2);
            if (AHV != null) {
                AHV.A02(this.A0A);
            }
        }
        for (int i3 = 0; i3 < this.A01; i3++) {
            this.A09[i3] = -1;
            this.A07[i3] = -1;
        }
        for (int i4 = 0; i4 < this.A00; i4++) {
            this.A05[i4] = -1;
        }
        this.A03 = 0;
        this.A02 = -1;
    }

    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder();
        sb.append(hashCode());
        sb.append(" { ");
        String obj2 = sb.toString();
        int i = this.A03;
        for (int i2 = 0; i2 < i; i2++) {
            AnonymousClass0QC AHV = AHV(i2);
            if (AHV != null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(obj2);
                sb2.append(AHV);
                sb2.append(" = ");
                sb2.append(AHW(i2));
                sb2.append(" ");
                String obj3 = sb2.toString();
                int A00 = A00(AHV);
                StringBuilder sb3 = new StringBuilder();
                sb3.append(obj3);
                sb3.append("[p: ");
                String obj4 = sb3.toString();
                int i3 = this.A08[A00];
                StringBuilder sb4 = new StringBuilder();
                if (i3 != -1) {
                    sb4.append(obj4);
                    sb4.append(this.A0B.A03[this.A09[i3]]);
                } else {
                    sb4.append(obj4);
                    sb4.append("none");
                }
                String obj5 = sb4.toString();
                StringBuilder sb5 = new StringBuilder();
                sb5.append(obj5);
                sb5.append(", n: ");
                String obj6 = sb5.toString();
                int i4 = this.A06[A00];
                if (i4 != -1) {
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append(obj6);
                    sb6.append(this.A0B.A03[this.A09[i4]]);
                    obj = sb6.toString();
                } else {
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append(obj6);
                    sb7.append("none");
                    obj = sb7.toString();
                }
                StringBuilder sb8 = new StringBuilder();
                sb8.append(obj);
                sb8.append("]");
                obj2 = sb8.toString();
            }
        }
        StringBuilder sb9 = new StringBuilder();
        sb9.append(obj2);
        sb9.append(" }");
        return sb9.toString();
    }
}
