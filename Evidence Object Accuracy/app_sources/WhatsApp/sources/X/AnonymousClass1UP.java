package X;

import android.content.SharedPreferences;
import android.util.Base64;

/* renamed from: X.1UP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UP {
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public boolean A04;
    public byte[] A05;
    public byte[] A06;
    public byte[] A07;
    public final SharedPreferences A08;

    public AnonymousClass1UP(C16630pM r8) {
        byte[] decode;
        SharedPreferences A01 = r8.A01("qr_data");
        this.A08 = A01;
        this.A03 = A01.getString("ref", null);
        String string = A01.getString("key", null);
        if (string == null) {
            decode = null;
        } else {
            decode = Base64.decode(string, 0);
        }
        this.A07 = decode;
        this.A05 = null;
        this.A06 = null;
        if (decode != null) {
            byte[] bArr = new byte[32];
            this.A05 = bArr;
            this.A06 = new byte[32];
            System.arraycopy(decode, 0, bArr, 0, 32);
            System.arraycopy(this.A07, 32, this.A06, 0, 32);
        }
        this.A02 = A01.getString("token", null);
        this.A00 = A01.getString("browser", null);
        this.A01 = A01.getString("epoch", null);
        this.A04 = A01.getBoolean("browser_changed", false);
    }

    public final void A00(String str) {
        this.A08.edit().remove(str).commit();
    }
}
