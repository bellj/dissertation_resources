package X;

/* renamed from: X.0fX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC10990fX extends AbstractC112675Eh implements AnonymousClass5ZS {
    public static final C113605Ig A00 = new C113605Ig();

    public boolean A03(AnonymousClass5X4 r2) {
        return true;
    }

    public abstract void A04(Runnable runnable, AnonymousClass5X4 v);

    public AbstractC10990fX() {
        super(AnonymousClass5ZS.A00);
    }

    public static AnonymousClass5ZU A00(AnonymousClass5ZS r3, AbstractC115495Rt r4) {
        AnonymousClass5ZU r0;
        C16700pc.A0E(r4, 1);
        if (r4 instanceof AbstractC112725Em) {
            AbstractC112725Em r42 = (AbstractC112725Em) r4;
            AbstractC115495Rt key = r3.getKey();
            C16700pc.A0E(key, 0);
            if ((key == r42 || r42.A00 == key) && (r0 = (AnonymousClass5ZU) r42.A01.AJ4(r3)) != null) {
                return r0;
            }
            return null;
        } else if (AnonymousClass5ZS.A00 != r4) {
            return null;
        } else {
            return r3;
        }
    }

    public static AnonymousClass5X4 A01(AnonymousClass5ZS r2, AbstractC115495Rt r3) {
        C16700pc.A0E(r3, 1);
        if (r3 instanceof AbstractC112725Em) {
            AbstractC112725Em r32 = (AbstractC112725Em) r3;
            AbstractC115495Rt key = r2.getKey();
            C16700pc.A0E(key, 0);
            if (!(key == r32 || r32.A00 == key) || r32.A01.AJ4(r2) == null) {
                return r2;
            }
        } else if (AnonymousClass5ZS.A00 != r3) {
            return r2;
        }
        return C112775Er.A00;
    }

    public AbstractC10990fX A02(int i) {
        C88264Ex.A00(i);
        return new RunnableC11110fl(this, i);
    }

    @Override // X.AbstractC112675Eh, X.AnonymousClass5ZU, X.AnonymousClass5X4
    public AnonymousClass5ZU get(AbstractC115495Rt r2) {
        return A00(this, r2);
    }

    @Override // X.AbstractC112675Eh, X.AnonymousClass5X4
    public AnonymousClass5X4 minusKey(AbstractC115495Rt r2) {
        return A01(this, r2);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append('@');
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        return sb.toString();
    }
}
