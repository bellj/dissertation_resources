package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.WallpaperImagePreview;
import com.whatsapp.settings.chat.wallpaper.WallpaperMockChatView;

/* renamed from: X.35E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35E extends AbstractC74113hL {
    public Resources A00;
    public Button A01;
    public FrameLayout A02;
    public ProgressBar A03;
    public WallpaperImagePreview A04 = ((WallpaperImagePreview) AnonymousClass028.A0D(this, R.id.wallpaper_preview_background));
    public WallpaperImagePreview A05 = ((WallpaperImagePreview) AnonymousClass028.A0D(this, R.id.wallpaper_preview_background_doodle));
    public WallpaperImagePreview A06;

    public AnonymousClass35E(Context context, Resources resources, String str, String str2) {
        super(context);
        this.A00 = resources;
        FrameLayout.inflate(context, R.layout.wallpaper_preview_chat_item_view, this);
        WallpaperImagePreview wallpaperImagePreview = (WallpaperImagePreview) AnonymousClass028.A0D(this, R.id.wallpaper_preview_blur);
        this.A06 = wallpaperImagePreview;
        wallpaperImagePreview.setImageDrawable(null);
        ((WallpaperMockChatView) AnonymousClass028.A0D(this, R.id.wallpaper_preview_mock_chat)).setMessages(str, str2, null);
        this.A02 = (FrameLayout) AnonymousClass028.A0D(this, R.id.wallpaper_preview_download_container);
        this.A03 = (ProgressBar) AnonymousClass028.A0D(this, R.id.wallpaper_preview_progress_bar);
        this.A01 = (Button) AnonymousClass028.A0D(this, R.id.wallpaper_preview_download_btn);
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        this.A06.setVisibility(8);
        WallpaperImagePreview wallpaperImagePreview = this.A04;
        wallpaperImagePreview.setImageDrawable(null);
        wallpaperImagePreview.setBackgroundColor(i);
    }

    public void setDownloadClickListener(View.OnClickListener onClickListener) {
        this.A01.setOnClickListener(onClickListener);
    }

    public void setWallpaper(Bitmap bitmap) {
        this.A06.setVisibility(8);
        this.A04.setImageBitmap(bitmap);
    }

    public void setWallpaper(Drawable drawable) {
        this.A06.setVisibility(8);
        this.A04.setImageDrawable(drawable);
    }
}
