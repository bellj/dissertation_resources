package X;

/* renamed from: X.0Cu  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Cu extends C02550Cv {
    public int A00 = 0;
    public int A01 = 0;
    public boolean A02 = true;

    @Override // X.AnonymousClass0QV
    public boolean A0D() {
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0074, code lost:
        if (r0.A03() != false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0082, code lost:
        if (r0.A03() != false) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008d, code lost:
        if (r10 == false) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00fb, code lost:
        if (r11 == false) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0102, code lost:
        if (r10 != false) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0104, code lost:
        r16 = 5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x014d  */
    @Override // X.AnonymousClass0QV
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.C06240Ss r21) {
        /*
        // Method dump skipped, instructions count: 426
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Cu.A08(X.0Ss):void");
    }

    @Override // X.AnonymousClass0QV
    public String toString() {
        StringBuilder sb = new StringBuilder("[Barrier] ");
        sb.append(this.A0f);
        sb.append(" {");
        String obj = sb.toString();
        for (int i = 0; i < ((C02550Cv) this).A00; i++) {
            AnonymousClass0QV r2 = ((C02550Cv) this).A01[i];
            if (i > 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(obj);
                sb2.append(", ");
                obj = sb2.toString();
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append(obj);
            sb3.append(r2.A0f);
            obj = sb3.toString();
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append(obj);
        sb4.append("}");
        return sb4.toString();
    }
}
