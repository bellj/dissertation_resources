package X;

/* renamed from: X.23k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C458523k extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;

    public C458523k() {
        super(1174, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(6, this.A00);
        r3.Abe(1, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(5, this.A01);
        r3.Abe(2, this.A04);
        r3.Abe(3, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamStatusTabClose {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionFbShareError", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionId", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionReplyCount", this.A03);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionShareStatusCtaTap", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionTimeSpent", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "statusSessionViewCount", this.A05);
        sb.append("}");
        return sb.toString();
    }
}
