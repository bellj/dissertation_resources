package X;

import android.text.TextUtils;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.2K2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2K2 {
    public C30211Wn A00;
    public List A01;
    public Set A02 = new HashSet();
    public boolean A03 = false;
    public boolean A04 = false;
    public boolean A05 = false;
    public final C251118d A06;
    public final AbstractC115895Ti A07;
    public final AnonymousClass4N4 A08;
    public final AnonymousClass018 A09;

    public AnonymousClass2K2(C251118d r2, AbstractC115895Ti r3, AbstractC115905Tj r4, AnonymousClass018 r5) {
        this.A09 = r5;
        this.A06 = r2;
        this.A08 = r4.A7u(r3);
        this.A07 = r3;
    }

    public static void A00(C53862fQ r1) {
        AnonymousClass2K2 r12 = r1.A0I;
        r12.A07();
        r12.A01 = null;
    }

    public AnonymousClass2K3 A01() {
        List A06 = A06(false);
        int i = null;
        if (this.A05) {
            i = 1;
        }
        return new AnonymousClass2K3(i, A06, this.A04);
    }

    public final C37171lc A02(AnonymousClass2Jw r15) {
        int i;
        C14850m9 r4;
        ArrayList arrayList;
        if (this.A05) {
            i = 1;
        } else {
            i = null;
        }
        AnonymousClass4N4 r2 = this.A08;
        Set set = this.A02;
        C30211Wn r8 = this.A00;
        List list = this.A01;
        boolean z = this.A04;
        boolean z2 = this.A03;
        ArrayList arrayList2 = new ArrayList();
        if (list != null && !list.isEmpty()) {
            C251118d r42 = r2.A00;
            boolean z3 = false;
            if (r42.A02()) {
                if (r2.A01.AJK()) {
                    arrayList2.add(new AnonymousClass40B(z2));
                }
                arrayList2.add(new C59682vD(set, !set.isEmpty()));
                r4 = r42.A00;
                if (r4.A07(450) && r4.A07(1510)) {
                    if (i != null) {
                        z3 = true;
                    }
                    arrayList2.add(new AnonymousClass40D(z3));
                }
                if (r4.A07(450) && r4.A07(1511)) {
                    arrayList2.add(new AnonymousClass40C(z));
                }
            } else {
                arrayList2.add(new AnonymousClass409());
                r4 = r42.A00;
                if (r4.A07(450) && r4.A07(1510)) {
                    if (i != null) {
                        z3 = true;
                    }
                    arrayList2.add(new AnonymousClass40D(z3));
                }
                if (r4.A07(450) && r4.A07(1511)) {
                    arrayList2.add(new AnonymousClass40C(z));
                }
                if (r8 != null) {
                    arrayList = new ArrayList();
                    arrayList.add(new C59692vE(r8, true));
                } else {
                    arrayList = new ArrayList();
                    if (!list.isEmpty()) {
                        for (int i2 = 0; i2 < Math.min(list.size(), 6); i2++) {
                            arrayList.add(new C59692vE((C30211Wn) list.get(i2), false));
                        }
                        if (list.size() > 6) {
                            arrayList.add(new AnonymousClass40A());
                        }
                    }
                }
                arrayList2.addAll(arrayList);
            }
            if ((!set.isEmpty() || r8 != null || i != null || z || z2) && r4.A07(450) && r4.A07(1814)) {
                arrayList2.add(new AnonymousClass408());
            }
        }
        if (!arrayList2.isEmpty()) {
            return new C59522us(r15, arrayList2);
        }
        return null;
    }

    public C37171lc A03(AnonymousClass2Jw r7, List list) {
        C251118d r4 = this.A06;
        if (!r4.A00()) {
            return null;
        }
        HashSet hashSet = new HashSet();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C30211Wn r0 = (C30211Wn) it.next();
            hashSet.add(new C30211Wn(r0.A00, r0.A01));
        }
        if (r4.A02()) {
            hashSet.addAll(this.A02);
        } else {
            C30211Wn r02 = this.A00;
            if (r02 != null) {
                hashSet.add(r02);
            }
        }
        ArrayList arrayList = new ArrayList();
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            C30211Wn r03 = (C30211Wn) it2.next();
            arrayList.add(new AnonymousClass2x6(r03.A00, r03.A01));
        }
        this.A01 = arrayList;
        return A02(r7);
    }

    public C90954Pw A04() {
        List list = this.A01;
        if (list == null) {
            return null;
        }
        C30211Wn r3 = this.A00;
        ArrayList arrayList = new ArrayList(list);
        Collections.sort(arrayList, new Comparator(Collator.getInstance(AnonymousClass018.A00(this.A09.A00))) { // from class: X.5Cb
            public final /* synthetic */ Collator A00;

            {
                this.A00 = r1;
            }

            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return this.A00.compare(((C30211Wn) obj).A01, ((C30211Wn) obj2).A01);
            }
        });
        return new C90954Pw(r3, arrayList, new ArrayList(this.A02));
    }

    public String A05() {
        if (this.A02.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (C30211Wn r0 : this.A02) {
            arrayList.add(r0.A00);
        }
        return TextUtils.join(",", arrayList);
    }

    public final List A06(boolean z) {
        if (!this.A06.A02()) {
            C30211Wn r0 = this.A00;
            if (r0 != null && z) {
                return Collections.singletonList(r0.A00);
            }
        } else {
            ArrayList arrayList = new ArrayList();
            for (C30211Wn r02 : this.A02) {
                arrayList.add(r02.A00);
            }
            if (!arrayList.isEmpty()) {
                return arrayList;
            }
        }
        return null;
    }

    public void A07() {
        this.A04 = false;
        this.A00 = null;
        this.A05 = false;
        this.A02 = new HashSet();
        this.A03 = false;
    }

    public void A08(AnonymousClass07E r4) {
        boolean z;
        boolean z2;
        HashSet hashSet;
        Map map = r4.A02;
        Boolean bool = (Boolean) map.get("saved_open_now");
        boolean z3 = false;
        if (bool != null) {
            z = bool.booleanValue();
        } else {
            z = false;
        }
        this.A05 = z;
        Boolean bool2 = (Boolean) map.get("saved_has_catalog");
        if (bool2 != null) {
            z2 = bool2.booleanValue();
        } else {
            z2 = false;
        }
        this.A04 = z2;
        Boolean bool3 = (Boolean) map.get("saved_distance");
        if (bool3 != null) {
            z3 = bool3.booleanValue();
        }
        this.A03 = z3;
        this.A00 = (C30211Wn) map.get("saved_selected_single_choice_category");
        Collection collection = (Collection) map.get("saved_selected_multiple_choice_category");
        if (collection != null) {
            hashSet = new HashSet(collection);
        } else {
            hashSet = new HashSet();
        }
        this.A02 = hashSet;
        this.A01 = (List) map.get("saved_current_filter_categories");
    }

    public void A09(AnonymousClass07E r3) {
        r3.A04("saved_open_now", Boolean.valueOf(this.A05));
        r3.A04("saved_has_catalog", Boolean.valueOf(this.A04));
        r3.A04("saved_distance", Boolean.valueOf(this.A03));
        r3.A04("saved_selected_single_choice_category", this.A00);
        r3.A04("saved_selected_multiple_choice_category", new ArrayList(this.A02));
        r3.A04("saved_current_filter_categories", this.A01);
    }
}
