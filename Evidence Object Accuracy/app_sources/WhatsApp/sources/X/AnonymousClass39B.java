package X;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.facebook.redex.ViewOnClickCListenerShape12S0100000_I1_6;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.whatsapp.R;
import java.util.Formatter;
import java.util.Locale;

/* renamed from: X.39B */
/* loaded from: classes2.dex */
public class AnonymousClass39B extends AnonymousClass2G9 implements AnonymousClass004 {
    public C14850m9 A00;
    public AnonymousClass2G8 A01;
    public AnonymousClass5V0 A02;
    public AnonymousClass5V0 A03;
    public AnonymousClass5V0 A04;
    public AbstractC115465Rq A05;
    public AnonymousClass21T A06;
    public AnonymousClass2P7 A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public final int A0H;
    public final Handler A0I;
    public final View A0J;
    public final View A0K;
    public final View A0L;
    public final ViewGroup A0M;
    public final ViewGroup A0N;
    public final AlphaAnimation A0O;
    public final AlphaAnimation A0P;
    public final ImageButton A0Q;
    public final ImageButton A0R;
    public final ImageButton A0S;
    public final ImageButton A0T;
    public final ImageButton A0U;
    public final ImageButton A0V;
    public final ImageButton A0W;
    public final ImageButton A0X;
    public final ImageButton A0Y;
    public final ImageButton A0Z;
    public final LinearLayout A0a;
    public final LinearLayout A0b;
    public final ProgressBar A0c;
    public final SeekBar A0d;
    public final TextView A0e;
    public final TextView A0f;
    public final TextView A0g;
    public final C91824Tg A0h;
    public final Runnable A0i;
    public final StringBuilder A0j;
    public final Formatter A0k;
    public final boolean A0l;

    public AnonymousClass39B(Context context, C91824Tg r10, int i) {
        super(context);
        if (!this.A0C) {
            this.A0C = true;
            this.A00 = C12960it.A0S(AnonymousClass2P6.A00(generatedComponent()));
        }
        int i2 = 0;
        this.A0G = false;
        this.A0B = false;
        this.A0F = false;
        this.A09 = false;
        this.A08 = false;
        this.A0A = false;
        this.A0D = false;
        this.A0E = false;
        this.A0i = new RunnableBRunnable0Shape16S0100000_I1_2(this, 18);
        this.A0I = new Handler(Looper.myLooper(), new C65843Lf(this));
        LayoutInflater.from(context).inflate(R.layout.inline_video_control_view, this);
        this.A00.A02(820);
        boolean A07 = this.A00.A07(1891);
        this.A0l = A07;
        this.A0H = i;
        StringBuilder A0h = C12960it.A0h();
        this.A0j = A0h;
        this.A0k = new Formatter(A0h, Locale.getDefault());
        this.A0M = (ViewGroup) AnonymousClass028.A0D(this, R.id.controls);
        this.A0Q = (ImageButton) AnonymousClass028.A0D(this, R.id.close);
        this.A0S = (ImageButton) AnonymousClass028.A0D(this, R.id.fullscreen_close);
        this.A0T = (ImageButton) AnonymousClass028.A0D(this, R.id.fullscreen_minimize);
        this.A0U = (ImageButton) AnonymousClass028.A0D(this, R.id.landscape_close);
        this.A0V = (ImageButton) AnonymousClass028.A0D(this, R.id.landscape_minimize);
        this.A0R = (ImageButton) AnonymousClass028.A0D(this, R.id.fullscreen);
        this.A0Y = (ImageButton) AnonymousClass028.A0D(this, R.id.play_pause);
        this.A0Z = (ImageButton) AnonymousClass028.A0D(this, R.id.play_pause_fullscreen);
        this.A0c = (ProgressBar) AnonymousClass028.A0D(this, R.id.minimized_progress_bar);
        this.A0N = (ViewGroup) AnonymousClass028.A0D(this, R.id.fullscreen_controls);
        this.A0d = (SeekBar) AnonymousClass028.A0D(this, R.id.mediacontroller_progress);
        this.A0f = C12960it.A0I(this, R.id.time);
        this.A0e = C12960it.A0I(this, R.id.time_current);
        TextView A0I = C12960it.A0I(this, R.id.video_attribution);
        this.A0g = A0I;
        ImageButton imageButton = (ImageButton) AnonymousClass028.A0D(this, R.id.logo_button);
        this.A0W = imageButton;
        this.A0X = (ImageButton) AnonymousClass028.A0D(this, R.id.logo_icon);
        LinearLayout linearLayout = (LinearLayout) AnonymousClass028.A0D(this, R.id.logo_box);
        this.A0a = linearLayout;
        this.A0K = AnonymousClass028.A0D(this, R.id.loading);
        this.A0J = AnonymousClass028.A0D(this, R.id.background);
        this.A0L = AnonymousClass028.A0D(this, R.id.header);
        this.A0b = (LinearLayout) AnonymousClass028.A0D(this, R.id.media_controller_container);
        if (A07) {
            imageButton.setVisibility(8);
            linearLayout.setVisibility(!A0K() ? 8 : i2);
        } else {
            linearLayout.setVisibility(8);
            imageButton.setVisibility(!A0K() ? 8 : i2);
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, getAlpha());
        this.A0P = alphaAnimation;
        alphaAnimation.setDuration(250);
        alphaAnimation.setInterpolator(new DecelerateInterpolator(1.5f));
        A0I.setVisibility(8);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(getAlpha(), 0.0f);
        this.A0O = alphaAnimation2;
        alphaAnimation2.setDuration(250);
        alphaAnimation2.setInterpolator(new AccelerateInterpolator(1.5f));
        alphaAnimation2.setAnimationListener(new C58042o2(this));
        this.A0h = r10;
        onConfigurationChanged(getResources().getConfiguration());
    }

    public static /* synthetic */ void A01(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A04;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A02(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A02;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A03(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A03;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A04(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A03;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A05(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A02;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A06(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A03;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A07(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A04;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A08(AnonymousClass39B r0) {
        AnonymousClass5V0 r02 = r0.A02;
        if (r02 != null) {
            r02.AO1();
        }
    }

    public static /* synthetic */ void A09(AnonymousClass39B r2, AnonymousClass21T r3) {
        if (r2.A0D) {
            r2.A0D = false;
            r2.A0E = true;
            r2.A0I();
            r3.A09(0);
        } else if (r2.A08) {
        } else {
            if (r3.A0B()) {
                r2.A00();
            } else {
                r2.A0E();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r5 == false) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A0A(X.AnonymousClass39B r2, X.AnonymousClass21T r3, int r4, boolean r5) {
        /*
            boolean r0 = r3.A0B()
            r1 = 0
            if (r0 == 0) goto L_0x000a
            r0 = 1
            if (r5 != 0) goto L_0x000b
        L_0x000a:
            r0 = 0
        L_0x000b:
            r2.setKeepScreenOn(r0)
            boolean r0 = r2.A0E
            if (r0 == 0) goto L_0x001a
            r0 = 2
            if (r4 != r0) goto L_0x001a
            r2.A0E = r1
            r2.A0E()
        L_0x001a:
            android.widget.SeekBar r0 = r2.A0d
            r0.setVisibility(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass39B.A0A(X.39B, X.21T, int, boolean):void");
    }

    public static /* synthetic */ void A0B(AnonymousClass39B r2, AnonymousClass21T r3, boolean z) {
        r2.A09 = z;
        if (r3 instanceof AnonymousClass21S) {
            r2.A0K.setVisibility(C12960it.A02(z ? 1 : 0));
        }
        AnonymousClass4Xy r0 = r2.A0h.A03;
        if (z) {
            r0.A02();
        } else {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass2G9
    public void A0C() {
        if (!this.A0A && A05() && this.A06 != null) {
            this.A0A = true;
            if (this.A0B) {
                this.A0N.startAnimation(this.A0O);
            } else {
                ProgressBar progressBar = this.A0c;
                AlphaAnimation alphaAnimation = this.A0O;
                progressBar.startAnimation(alphaAnimation);
                if (!this.A0l && A0K()) {
                    this.A0W.startAnimation(alphaAnimation);
                }
            }
            if (this.A0B) {
                int i = 262;
                if (Build.VERSION.SDK_INT >= 19) {
                    i = 4358;
                }
                setSystemUiVisibility(i);
            }
        }
    }

    public void A0D() {
        Runnable runnable = this.A0i;
        removeCallbacks(runnable);
        AnonymousClass21T r0 = this.A06;
        if (r0 != null && r0.A0B()) {
            postDelayed(runnable, 3000);
        }
    }

    public void A0E() {
        AnonymousClass21T r0 = this.A06;
        if (r0 != null) {
            if (!r0.A0B()) {
                this.A06.A07();
                this.A0h.A06.A02();
            }
            A0D();
            A0I();
            A03(100);
        }
    }

    public final void A0F() {
        ViewGroup.MarginLayoutParams A0H;
        int i;
        Resources resources;
        int i2;
        int dimensionPixelSize;
        boolean z = this.A0l;
        if (z) {
            A0H = C12970iu.A0H(this.A0a);
        } else {
            A0H = C12970iu.A0H(this.A0W);
        }
        if (!this.A0B) {
            i = getResources().getDimensionPixelSize(R.dimen.inline_video_logo_right_portrait_margin);
            if (!z || A05()) {
                resources = getResources();
                i2 = R.dimen.inline_video_logo_bottom_portrait_margin;
                dimensionPixelSize = resources.getDimensionPixelSize(i2);
            } else {
                dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.inline_video_logo_bottom_portrait_margin) - getResources().getDimensionPixelSize(R.dimen.inline_video_progressbar_height);
            }
        } else {
            boolean A1V = C12960it.A1V(getResources().getConfiguration().orientation, 2);
            Resources resources2 = getResources();
            if (A1V) {
                i = resources2.getDimensionPixelSize(R.dimen.inline_video_logo_right_fullscreen_landscape_margin);
                if (!z || A05()) {
                    resources = getResources();
                    i2 = R.dimen.inline_video_logo_bottom_fullscreen_landscape_margin;
                    dimensionPixelSize = resources.getDimensionPixelSize(i2);
                }
                dimensionPixelSize = i;
            } else {
                i = resources2.getDimensionPixelSize(R.dimen.inline_video_logo_right_fullscreen_margin);
                if (!z || A05()) {
                    resources = getResources();
                    i2 = R.dimen.inline_video_logo_bottom_fullscreen_margin;
                    dimensionPixelSize = resources.getDimensionPixelSize(i2);
                }
                dimensionPixelSize = i;
            }
        }
        A0H.setMargins(0, 0, i, dimensionPixelSize);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.inline_video_player_button_padding);
        if (z) {
            LinearLayout linearLayout = this.A0a;
            linearLayout.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
            linearLayout.setLayoutParams(A0H);
            linearLayout.requestLayout();
            return;
        }
        ImageButton imageButton = this.A0W;
        imageButton.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
        imageButton.setLayoutParams(A0H);
        imageButton.requestLayout();
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0G() {
        /*
            r4 = this;
            android.content.res.Resources r0 = r4.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r1 = r0.orientation
            r0 = 2
            boolean r3 = X.C12960it.A1V(r1, r0)
            android.widget.ImageButton r1 = r4.A0T
            r2 = 8
            int r0 = X.C13010iy.A00(r3)
            r1.setVisibility(r0)
            android.widget.ImageButton r1 = r4.A0S
            int r0 = X.C13010iy.A00(r3)
            r1.setVisibility(r0)
            android.widget.ImageButton r1 = r4.A0V
            int r0 = X.C12990iw.A0A(r3)
            r1.setVisibility(r0)
            android.widget.ImageButton r0 = r4.A0U
            if (r3 == 0) goto L_0x0031
            r2 = 0
        L_0x0031:
            r0.setVisibility(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass39B.A0G():void");
    }

    public final void A0H() {
        View view;
        int paddingLeft;
        int i;
        boolean z = this.A0B;
        ImageButton imageButton = this.A0R;
        Context context = getContext();
        if (z) {
            C12960it.A0r(context, imageButton, R.string.exit_fullscreen);
            imageButton.setImageResource(R.drawable.ic_pip_collapse);
            view = this.A0L;
            paddingLeft = view.getPaddingLeft();
            i = getResources().getDimensionPixelSize(R.dimen.inline_controls_header_padding);
        } else {
            C12960it.A0r(context, imageButton, R.string.enter_fullscreen);
            imageButton.setImageResource(R.drawable.ic_pip_expand);
            view = this.A0L;
            paddingLeft = view.getPaddingLeft();
            i = 0;
        }
        C12990iw.A1A(view, paddingLeft, i);
        A0D();
    }

    public final void A0I() {
        AnonymousClass21T r0 = this.A06;
        if (r0 != null) {
            boolean A0B = r0.A0B();
            ImageButton imageButton = this.A0Y;
            int i = R.drawable.ic_pip_play;
            if (A0B) {
                i = R.drawable.ic_pip_pause;
            }
            imageButton.setImageResource(i);
            Context context = getContext();
            int i2 = R.string.play;
            if (A0B) {
                i2 = R.string.pause;
            }
            String string = context.getString(i2);
            imageButton.setContentDescription(string);
            ImageButton imageButton2 = this.A0Z;
            int i3 = R.drawable.ic_video_play_conv;
            if (A0B) {
                i3 = R.drawable.ic_video_pause_conv;
            }
            imageButton2.setImageResource(i3);
            imageButton2.setContentDescription(string);
        }
    }

    public void A0J(AnonymousClass21T r6) {
        A02();
        r6.A05();
        if (r6 instanceof AnonymousClass39H) {
            this.A0D = true;
            this.A0Y.setImageResource(R.drawable.ic_pip_replay);
            this.A0Z.setImageResource(R.drawable.ic_video_restart);
        } else {
            r6.A09(0);
            A0I();
        }
        this.A0c.setProgress(0);
        this.A0d.setProgress(0);
        this.A0e.setText(AnonymousClass2Bd.A01(this.A0j, this.A0k, 0));
        A03(500);
        if (!A05()) {
            A01();
        }
        AnonymousClass4Xy r1 = this.A0h.A06;
        if (r1.A02) {
            r1.A00();
        }
    }

    public final boolean A0K() {
        int i = this.A0H;
        if (i == 1 || i == 7 || i == 4) {
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A07;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A07 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        int dimensionPixelSize;
        int dimensionPixelSize2;
        Resources resources;
        int i;
        super.onConfigurationChanged(configuration);
        A0G();
        A0F();
        if (getResources().getConfiguration().orientation == 2) {
            dimensionPixelSize = 0;
            dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.inline_video_fullscreen_media_controller_bottom_padding);
            resources = getResources();
            i = R.dimen.inline_video_fullscreen_media_controller_horizontal_padding;
        } else {
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.inline_video_portrait_media_controller_bottom_margin);
            dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.inline_video_portrait_media_controller_bottom_padding);
            resources = getResources();
            i = R.dimen.inline_video_portrait_media_controller_horizontal_padding;
        }
        int dimensionPixelSize3 = resources.getDimensionPixelSize(i);
        LinearLayout linearLayout = this.A0b;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) linearLayout.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, dimensionPixelSize);
        layoutParams.setMarginStart(layoutParams.leftMargin);
        layoutParams.setMarginEnd(layoutParams.rightMargin);
        linearLayout.setPadding(dimensionPixelSize3, linearLayout.getPaddingTop(), dimensionPixelSize3, dimensionPixelSize2);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.requestLayout();
    }

    public void setBlockPlayButtonInput(boolean z) {
        this.A08 = z;
    }

    @Override // X.AnonymousClass2G9
    public void setCloseButtonListener(AnonymousClass5V0 r4) {
        this.A02 = r4;
        C12960it.A11(this.A0Q, this, 45);
        C12960it.A11(this.A0S, this, 48);
        this.A0U.setOnClickListener(new ViewOnClickCListenerShape12S0100000_I1_6(this, 1));
    }

    @Override // X.AnonymousClass2G9
    public void setFullscreenButtonClickListener(AnonymousClass5V0 r3) {
        this.A03 = r3;
        C12960it.A11(this.A0R, this, 49);
        C12960it.A11(this.A0T, this, 46);
        C12960it.A11(this.A0V, this, 47);
    }

    public void setPlayPauseListener(AbstractC115465Rq r1) {
        this.A05 = r1;
    }

    @Override // X.AnonymousClass2G9
    public void setPlayer(AnonymousClass21T r6) {
        ImageButton imageButton;
        this.A06 = r6;
        ImageButton imageButton2 = this.A0Y;
        C12960it.A0r(getContext(), imageButton2, R.string.pause);
        ViewOnClickCListenerShape3S0200000_I1_1 viewOnClickCListenerShape3S0200000_I1_1 = new ViewOnClickCListenerShape3S0200000_I1_1(this, 47, r6);
        imageButton2.setOnClickListener(viewOnClickCListenerShape3S0200000_I1_1);
        ImageButton imageButton3 = this.A0Z;
        C12960it.A0r(getContext(), imageButton3, R.string.pause);
        imageButton3.setOnClickListener(viewOnClickCListenerShape3S0200000_I1_1);
        imageButton3.setClickable(true);
        int i = 0;
        imageButton3.setVisibility(0);
        if (this.A0l) {
            imageButton = this.A0X;
        } else {
            imageButton = this.A0W;
        }
        imageButton.setVisibility(C12960it.A02(A0K() ? 1 : 0));
        ProgressBar progressBar = this.A0c;
        progressBar.setMax(1000);
        progressBar.setSecondaryProgress(1000);
        SeekBar seekBar = this.A0d;
        seekBar.setMax(1000);
        seekBar.setOnSeekBarChangeListener(new AnonymousClass3OT(this, r6));
        r6.A04 = new AbstractC47472Av(r6) { // from class: X.5Ag
            public final /* synthetic */ AnonymousClass21T A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC47472Av
            public final void ATt(boolean z, int i2) {
                AnonymousClass39B.A0A(AnonymousClass39B.this, this.A01, i2, z);
            }
        };
        r6.A01 = new AnonymousClass5V2() { // from class: X.5Ac
            @Override // X.AnonymousClass5V2
            public final void AOT(AnonymousClass21T r2) {
                AnonymousClass39B.this.A0J(r2);
            }
        };
        r6.A00 = new AnonymousClass5V1(r6) { // from class: X.5Aa
            public final /* synthetic */ AnonymousClass21T A01;

            {
                this.A01 = r2;
            }

            @Override // X.AnonymousClass5V1
            public final void ANM(AnonymousClass21T r3, boolean z) {
                AnonymousClass39B.A0B(AnonymousClass39B.this, this.A01, z);
            }
        };
        this.A0F = true;
        this.A0I.sendEmptyMessage(0);
        imageButton2.setClickable(true);
        imageButton2.setVisibility(0);
        this.A0R.setClickable(true);
        this.A0T.setClickable(true);
        this.A0V.setClickable(true);
        if (this.A0B) {
            A0G();
        }
        A0I();
        A0H();
        ViewGroup viewGroup = this.A0N;
        if (!this.A0B) {
            i = 8;
        }
        viewGroup.setVisibility(i);
    }

    @Override // X.AnonymousClass2G9
    public void setPlayerElevation(int i) {
        View A04;
        AnonymousClass21T r0 = this.A06;
        if (r0 != null && (A04 = r0.A04()) != null && A04.getParent() != null) {
            AnonymousClass028.A0V((View) this.A06.A04().getParent().getParent(), (float) i);
        }
    }

    public void setVideoAttribution(String str) {
        TextView textView;
        int i;
        if (str != null) {
            textView = this.A0g;
            textView.setText(str);
            i = 0;
        } else {
            textView = this.A0g;
            i = 8;
        }
        textView.setVisibility(i);
    }
}
