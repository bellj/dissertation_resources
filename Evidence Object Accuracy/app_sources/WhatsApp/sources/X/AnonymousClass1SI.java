package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.1SI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SI {
    public static File A00(Context context) {
        File file = new File(context.getCacheDir(), "minidumps");
        if (!file.exists() || !file.isDirectory()) {
            if (file.exists()) {
                file.delete();
            }
            if (!file.mkdirs()) {
                StringBuilder sb = new StringBuilder("Breakpad init failed to create crash directory: ");
                sb.append(file);
                throw new RuntimeException(sb.toString());
            }
        }
        return file;
    }
}
