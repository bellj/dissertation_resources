package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315163b implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(27);
    public C1316463o A00;
    public AnonymousClass6F2 A01;
    public final C1316563p A02;
    public final AnonymousClass63Z A03;
    public final C1316763r A04;
    public final C1315263c A05;
    public final String A06;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315163b(C1316463o r1, AnonymousClass6F2 r2, C1316563p r3, AnonymousClass63Z r4, C1316763r r5, C1315263c r6, String str) {
        this.A05 = r6;
        this.A03 = r4;
        this.A02 = r3;
        this.A06 = str;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A03, i);
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A06);
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A04, i);
    }
}
