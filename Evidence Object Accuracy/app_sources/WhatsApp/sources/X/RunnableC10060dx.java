package X;

import android.app.Notification;
import android.os.Build;
import androidx.work.impl.foreground.SystemForegroundService;

/* renamed from: X.0dx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10060dx implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ Notification A02;
    public final /* synthetic */ SystemForegroundService A03;

    public RunnableC10060dx(Notification notification, SystemForegroundService systemForegroundService, int i, int i2) {
        this.A03 = systemForegroundService;
        this.A00 = i;
        this.A02 = notification;
        this.A01 = i2;
    }

    @Override // java.lang.Runnable
    public void run() {
        int i = Build.VERSION.SDK_INT;
        SystemForegroundService systemForegroundService = this.A03;
        int i2 = this.A00;
        Notification notification = this.A02;
        if (i >= 29) {
            systemForegroundService.startForeground(i2, notification, this.A01);
        } else {
            systemForegroundService.startForeground(i2, notification);
        }
    }
}
