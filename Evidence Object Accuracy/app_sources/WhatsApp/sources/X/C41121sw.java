package X;

import android.content.SharedPreferences;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape0S0510000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200100_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.1sw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41121sw implements AbstractC41131sx {
    public AnonymousClass1JM A00;
    public C49222Ju A01;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final C41891uK A04;
    public final AnonymousClass2KO A05 = new AnonymousClass2KO(this);
    public final AnonymousClass16S A06;
    public final AnonymousClass16T A07;
    public final C233411h A08;
    public final C14830m7 A09;
    public final C14820m6 A0A;
    public final C15990oG A0B;
    public final C18240s8 A0C;
    public final C22100yW A0D;
    public final C17220qS A0E;
    public final AnonymousClass27W A0F;
    public final AnonymousClass1F2 A0G;
    public final AbstractC14440lR A0H;

    public C41121sw(AbstractC15710nm r8, C14900mE r9, AnonymousClass16S r10, AnonymousClass16T r11, C233411h r12, C14830m7 r13, C14820m6 r14, C15990oG r15, C18240s8 r16, C22100yW r17, C17220qS r18, AnonymousClass27W r19, AnonymousClass1F2 r20, AbstractC14440lR r21) {
        this.A09 = r13;
        this.A03 = r9;
        this.A02 = r8;
        this.A0H = r21;
        this.A0E = r18;
        this.A0C = r16;
        this.A0F = r19;
        this.A08 = r12;
        this.A0B = r15;
        this.A0A = r14;
        this.A0D = r17;
        this.A04 = new C41891uK(r13, r14, r15, r16, r17, r21);
        this.A06 = r10;
        this.A07 = r11;
        this.A0G = r20;
    }

    public void A00() {
        this.A0F.AP6();
        C14830m7 r4 = this.A09;
        C14900mE r2 = this.A03;
        this.A01 = new C49222Ju(this.A02, r2, this.A05, r4, this.A0E);
    }

    public final void A01(AnonymousClass2KP r18, long j, boolean z) {
        SharedPreferences sharedPreferences = this.A0A.A00;
        int i = sharedPreferences.getInt("adv_raw_id", -1);
        int i2 = sharedPreferences.getInt("adv_current_key_index", -1);
        C41891uK r4 = this.A04;
        C41901uL A00 = C41891uK.A00(i, i2, j);
        try {
            AnonymousClass1JS r0 = r18.A00;
            AnonymousClass009.A05(r0);
            byte[] bArr = r0.A00.A01;
            C31951bN r02 = (C31951bN) r4.A03.A00.submit(new CallableC41951uQ(r4)).get();
            AnonymousClass009.A05(r02);
            C32051bX r8 = r02.A00;
            AnonymousClass1PA r9 = r02.A01.A00;
            byte[] A05 = C16050oM.A05(AnonymousClass01V.A0B, A00.A02(), bArr);
            AnonymousClass2KQ r3 = (AnonymousClass2KQ) C41981uT.A05.A0T();
            AbstractC27881Jp A002 = A00.A00();
            r3.A03();
            C41981uT r1 = (C41981uT) r3.A00;
            r1.A00 |= 1;
            r1.A03 = A002;
            byte[] bArr2 = r9.A01;
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
            r3.A03();
            C41981uT r12 = (C41981uT) r3.A00;
            r12.A00 |= 2;
            r12.A01 = A01;
            byte[] A052 = C15940oB.A05(r8, A05);
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(A052, 0, A052.length);
            r3.A03();
            C41981uT r13 = (C41981uT) r3.A00;
            r13.A00 |= 4;
            r13.A02 = A012;
            C41981uT r7 = (C41981uT) r3.A02();
            byte[] bArr3 = r18.A04;
            byte[] A02 = r7.A02();
            try {
                Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
                instance.init(new SecretKeySpec(bArr3, DefaultCrypto.HMAC_SHA256));
                byte[] doFinal = instance.doFinal(A02);
                AnonymousClass2KS r32 = (AnonymousClass2KS) AnonymousClass2KR.A03.A0T();
                AbstractC27881Jp A003 = r7.A00();
                r32.A03();
                AnonymousClass2KR r14 = (AnonymousClass2KR) r32.A00;
                r14.A00 |= 1;
                r14.A01 = A003;
                AbstractC27881Jp A013 = AbstractC27881Jp.A01(doFinal, 0, doFinal.length);
                r32.A03();
                AnonymousClass2KR r15 = (AnonymousClass2KR) r32.A00;
                r15.A00 |= 2;
                r15.A02 = A013;
                C41911uM A022 = r4.A02(A00);
                this.A03.A0H(new RunnableBRunnable0Shape0S0510000_I0(this, r18, A022, (AnonymousClass2KR) r32.A02(), r4.A03(A022), 3, z));
            } catch (Exception e) {
                throw new RuntimeException("Failed to calculate hmac-sha256", e);
            }
        } catch (Exception e2) {
            Log.e("CompanionDeviceQrHandler/handleQrCode", e2);
            if ((e2 instanceof ExecutionException) || (e2 instanceof InterruptedException)) {
                this.A03.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(this, 4));
                return;
            }
            throw new RuntimeException("Failed to generate adv protobufs", e2);
        }
    }

    public final void A02(AnonymousClass2KP r17, C41911uM r18, AnonymousClass2KR r19, C41921uN r20, boolean z) {
        long j;
        C49222Ju r10 = this.A01;
        if (r10 == null) {
            Log.e("CompanionDeviceQrHandler/No devicePairRequestProtocolHelper created");
            return;
        }
        C17220qS r9 = r10.A08;
        String A01 = r9.A01();
        r10.A00 = r17;
        r10.A01 = r18;
        r10.A03 = z;
        String str = r17.A02;
        byte[] bArr = r17.A03;
        if (r18 != null) {
            j = r18.A04;
        } else {
            j = -1;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1V8("ref", str, (AnonymousClass1W9[]) null));
        arrayList.add(new AnonymousClass1V8("pub-key", bArr, (AnonymousClass1W9[]) null));
        if (!(r19 == null || r20 == null)) {
            arrayList.add(new AnonymousClass1V8("device-identity", r19.A02(), (AnonymousClass1W9[]) null));
            arrayList.add(new AnonymousClass1V8("key-index-list", r20.A02(), new AnonymousClass1W9[]{new AnonymousClass1W9("ts", j)}));
        }
        r9.A09(r10, new AnonymousClass1V8(new AnonymousClass1V8("pair-device", new AnonymousClass1W9[0], (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0])), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "md"), new AnonymousClass1W9("type", "set")}), A01, 219, 32000);
    }

    @Override // X.AbstractC41131sx
    public void A5V() {
        C49222Ju r1 = this.A01;
        if (r1 != null) {
            r1.A02 = false;
        }
        C22100yW r2 = this.A0D;
        synchronized (r2.A0O) {
            r2.A01 = null;
        }
    }

    @Override // X.AbstractC41131sx
    public AnonymousClass1JM ACX() {
        return this.A00;
    }

    @Override // X.AbstractC41131sx
    public void AI4(String str) {
        boolean z;
        boolean z2;
        C49222Ju r0 = this.A01;
        if (r0 == null || !r0.A02) {
            C22100yW r02 = this.A0D;
            synchronized (r02.A0O) {
                AnonymousClass1JM r1 = r02.A00;
                z = false;
                if (r1 != null) {
                    z = true;
                }
            }
            if (!z) {
                AnonymousClass2KP A00 = AnonymousClass2KP.A00(str);
                if (A00 != null) {
                    if (A00.A01 != null) {
                        A00();
                        A02(A00, null, null, null, false);
                        return;
                    } else if (!(A00.A00 == null || A00.A04 == null)) {
                        long A01 = this.A04.A01();
                        if (A01 == -1) {
                            Log.e("CompanionDeviceQrHandler/handleQrCode invalid local ts");
                            this.A0F.ARS();
                            return;
                        }
                        A00();
                        this.A0H.Ab2(new RunnableBRunnable0Shape0S1200100_I0(this, A00, str, 2, A01));
                        return;
                    }
                }
                this.A0F.ART();
                return;
            }
        }
        Log.e("CompanionDeviceQrHandler/handleQrCode/request already in progress");
        AbstractC15710nm r3 = this.A02;
        C22100yW r03 = this.A0D;
        synchronized (r03.A0O) {
            AnonymousClass1JM r12 = r03.A00;
            z2 = false;
            if (r12 != null) {
                z2 = true;
            }
        }
        r3.AaV("CompanionDeviceQrHandler/request already in progress", String.valueOf(z2), false);
    }
}
