package X;

/* renamed from: X.2i8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55282i8 extends AnonymousClass0FK {
    public final C112245Cq A00;

    public C55282i8(AnonymousClass02M r2, C19990v2 r3) {
        super(r2);
        this.A00 = new C112245Cq(r3);
    }

    @Override // X.AnonymousClass0Z4
    public /* bridge */ /* synthetic */ boolean A01(Object obj, Object obj2) {
        AnonymousClass4NK r4 = (AnonymousClass4NK) obj;
        AnonymousClass4NK r5 = (AnonymousClass4NK) obj2;
        int i = r4.A00;
        if (i != r5.A00) {
            return false;
        }
        if (i == 1) {
            return ((AnonymousClass1OU) r4.A01).A02.equals(((AnonymousClass1OU) r5.A01).A02);
        }
        return true;
    }

    @Override // X.AnonymousClass0Z4
    public /* bridge */ /* synthetic */ boolean A02(Object obj, Object obj2) {
        AnonymousClass4NK r4 = (AnonymousClass4NK) obj;
        AnonymousClass4NK r5 = (AnonymousClass4NK) obj2;
        int i = r4.A00;
        if (i != r5.A00) {
            return false;
        }
        if (i == 1) {
            return ((AnonymousClass1OU) r4.A01).A02.equals(((AnonymousClass1OU) r5.A01).A02);
        }
        return true;
    }

    @Override // X.AnonymousClass0Z4, java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        AnonymousClass4NK r4 = (AnonymousClass4NK) obj;
        AnonymousClass4NK r5 = (AnonymousClass4NK) obj2;
        int i = r4.A00;
        int i2 = r5.A00;
        if (i != i2) {
            return i - i2;
        }
        if (i != 1) {
            return 0;
        }
        return this.A00.compare((AnonymousClass1OU) r4.A01, (AnonymousClass1OU) r5.A01);
    }
}
