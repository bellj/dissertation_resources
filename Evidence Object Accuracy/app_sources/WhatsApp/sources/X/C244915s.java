package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import java.util.Collections;

/* renamed from: X.15s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C244915s {
    public final C15570nT A00;
    public final C15990oG A01;
    public final C18240s8 A02;
    public final C245015t A03;
    public final C18770sz A04;
    public final ExecutorC27271Gr A05;

    public C244915s(C15570nT r3, C15990oG r4, C18240s8 r5, C245015t r6, C18770sz r7, AbstractC14440lR r8) {
        this.A00 = r3;
        this.A02 = r5;
        this.A04 = r7;
        this.A01 = r4;
        this.A03 = r6;
        this.A05 = new ExecutorC27271Gr(r8, false);
    }

    public C31911bJ A00(UserJid userJid) {
        C15950oC A02 = C15940oB.A02(DeviceJid.of(userJid));
        C15990oG r5 = this.A01;
        AnonymousClass1JS A0E = r5.A0E(A02);
        if (A0E == null) {
            return null;
        }
        C15570nT r0 = this.A00;
        r0.A08();
        C27631Ih r02 = r0.A05;
        AnonymousClass009.A05(r02);
        return r5.A0D(C32591cP.A00(r02.user), C32591cP.A00(A02.A02), Collections.emptyList(), Collections.singletonList(A0E.A00));
    }
}
