package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaTextView;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2uZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59362uZ extends AbstractC75723kJ {
    public final View A00;
    public final AnonymousClass12P A01;
    public final WaButton A02;
    public final WaTextView A03;
    public final WaTextView A04;
    public final AnonymousClass1lN A05;
    public final AnonymousClass1lL A06;

    public C59362uZ(View view, AnonymousClass12P r4, AnonymousClass1lN r5, AnonymousClass1lL r6, UserJid userJid) {
        super(view);
        this.A01 = r4;
        this.A06 = r6;
        this.A05 = r5;
        this.A00 = AnonymousClass028.A0D(view, R.id.collection_divider);
        WaButton waButton = (WaButton) AnonymousClass028.A0D(view, R.id.button_collection_see_all);
        this.A02 = waButton;
        this.A04 = C12960it.A0N(view, R.id.textview_collection_title);
        this.A03 = C12960it.A0N(view, R.id.textview_collection_subtitle);
        C12960it.A13(waButton, this, userJid, 7);
    }

    @Override // X.AbstractC75723kJ
    public /* bridge */ /* synthetic */ void A09(AbstractC89244Jf r5) {
        C84703zk r52 = (C84703zk) r5;
        this.A04.setText(r52.A00);
        int i = 0;
        this.A00.setVisibility(C12960it.A02(r52.A01 ? 1 : 0));
        WaButton waButton = this.A02;
        if ("catalog_products_all_items_collection_id".equals(r52.A02)) {
            i = 8;
        }
        waButton.setVisibility(i);
    }
}
