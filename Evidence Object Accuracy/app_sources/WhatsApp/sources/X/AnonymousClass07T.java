package X;

import android.view.View;
import java.util.Comparator;

/* renamed from: X.07T  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07T implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        AnonymousClass0B3 r1 = (AnonymousClass0B3) ((View) obj).getLayoutParams();
        AnonymousClass0B3 r3 = (AnonymousClass0B3) ((View) obj2).getLayoutParams();
        boolean z = r1.A04;
        if (z != r3.A04) {
            return z ? 1 : -1;
        }
        return r1.A03 - r3.A03;
    }
}
