package X;

/* renamed from: X.4W1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4W1 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public String A06;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00(int r11) {
        /*
            r10 = this;
            r1 = -2097152(0xffffffffffe00000, float:NaN)
            r0 = r11 & r1
            boolean r0 = X.C12960it.A1V(r0, r1)
            r1 = 0
            if (r0 == 0) goto L_0x009e
            int r9 = r11 >>> 19
            r4 = 3
            r9 = r9 & r4
            r3 = 1
            if (r9 == r3) goto L_0x009e
            int r8 = r11 >>> 17
            r8 = r8 & r4
            if (r8 == 0) goto L_0x009e
            int r7 = r11 >>> 12
            r0 = 15
            r7 = r7 & r0
            if (r7 == 0) goto L_0x009e
            if (r7 == r0) goto L_0x009e
            int r2 = r11 >>> 10
            r2 = r2 & r4
            if (r2 == r4) goto L_0x009e
            r10.A05 = r9
            java.lang.String[] r1 = X.C94714cQ.A06
            int r0 = 3 - r8
            r0 = r1[r0]
            r10.A06 = r0
            int[] r0 = X.C94714cQ.A05
            r5 = r0[r2]
            r10.A03 = r5
            r2 = 2
            if (r9 != r2) goto L_0x0094
            int r5 = r5 / r2
        L_0x0039:
            r10.A03 = r5
        L_0x003b:
            int r6 = r11 >>> 9
            r6 = r6 & r3
            r0 = 1152(0x480, float:1.614E-42)
            if (r8 == r3) goto L_0x008f
            if (r8 == r2) goto L_0x0048
            if (r8 != r4) goto L_0x0099
            r0 = 384(0x180, float:5.38E-43)
        L_0x0048:
            r10.A04 = r0
            if (r8 != r4) goto L_0x0069
            if (r9 != r4) goto L_0x0066
            int[] r0 = X.C94714cQ.A00
        L_0x0050:
            int r7 = r7 - r3
            r0 = r0[r7]
            r10.A00 = r0
            int r0 = r0 * 12
            int r0 = r0 / r5
            int r0 = r0 + r6
            int r0 = r0 << 2
        L_0x005b:
            r10.A02 = r0
        L_0x005d:
            int r0 = r11 >> 6
            r0 = r0 & r4
            if (r0 != r4) goto L_0x0063
            r2 = 1
        L_0x0063:
            r10.A01 = r2
            return r3
        L_0x0066:
            int[] r0 = X.C94714cQ.A04
            goto L_0x0050
        L_0x0069:
            r1 = 144(0x90, float:2.02E-43)
            if (r9 != r4) goto L_0x007e
            if (r8 != r2) goto L_0x007b
            int[] r0 = X.C94714cQ.A01
        L_0x0071:
            int r7 = r7 - r3
            r0 = r0[r7]
            r10.A00 = r0
            int r0 = r0 * 144
            int r0 = r0 / r5
            int r0 = r0 + r6
            goto L_0x005b
        L_0x007b:
            int[] r0 = X.C94714cQ.A02
            goto L_0x0071
        L_0x007e:
            int[] r0 = X.C94714cQ.A03
            int r7 = r7 - r3
            r0 = r0[r7]
            r10.A00 = r0
            if (r8 != r3) goto L_0x0089
            r1 = 72
        L_0x0089:
            int r1 = r1 * r0
            int r1 = r1 / r5
            int r1 = r1 + r6
            r10.A02 = r1
            goto L_0x005d
        L_0x008f:
            if (r9 == r4) goto L_0x0048
            r0 = 576(0x240, float:8.07E-43)
            goto L_0x0048
        L_0x0094:
            if (r9 != 0) goto L_0x003b
            int r5 = r5 >> 2
            goto L_0x0039
        L_0x0099:
            java.lang.IllegalArgumentException r0 = X.C72453ed.A0h()
            throw r0
        L_0x009e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4W1.A00(int):boolean");
    }
}
