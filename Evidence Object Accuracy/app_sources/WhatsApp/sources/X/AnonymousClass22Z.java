package X;

import java.io.File;

/* renamed from: X.22Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22Z extends AbstractC38591oM implements AbstractC28771Oy {
    public final C30921Zi A00;
    public final C22370yy A01;
    public final AnonymousClass1VC A02 = new AnonymousClass1VC();
    public final File A03;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public AnonymousClass22Z(C14900mE r10, C15450nH r11, C18790t3 r12, C14950mJ r13, C30921Zi r14, C14850m9 r15, C20110vE r16, C22370yy r17, C22600zL r18, File file) {
        super(r11, r12, r13, r15, r16, r18, r10.A06);
        this.A01 = r17;
        this.A00 = r14;
        this.A03 = file;
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r1, C28781Oz r2) {
    }
}
