package X;

/* renamed from: X.1z7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44531z7 {
    public static final String A00;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(C16500p8.A00);
        sb.append(" FROM ");
        sb.append("message_ephemeral ephemeral");
        sb.append(" JOIN ");
        sb.append("available_message_view messages");
        sb.append(" ON ");
        sb.append("ephemeral.message_row_id = messages._id");
        sb.append(" WHERE ");
        sb.append("ephemeral.keep_in_chat != 1");
        sb.append(" AND ");
        sb.append("ephemeral.expire_timestamp < ?");
        A00 = sb.toString();
    }
}
