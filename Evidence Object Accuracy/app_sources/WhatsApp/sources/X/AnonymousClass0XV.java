package X;

/* renamed from: X.0XV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XV implements AnonymousClass02O {
    public final /* synthetic */ AnonymousClass01E A00;

    public AnonymousClass0XV(AnonymousClass01E r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass02O
    public /* bridge */ /* synthetic */ Object apply(Object obj) {
        AnonymousClass01E r2 = this.A00;
        AnonymousClass05V r1 = r2.A0F;
        if (r1 instanceof AbstractC001600r) {
            return r1.AAb();
        }
        return ((ActivityC001000l) r2.A0C()).A03;
    }
}
