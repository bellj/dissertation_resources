package X;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.CopyableTextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/* renamed from: X.5jw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121755jw extends AbstractActivityC121815k4 {
    public FrameLayout A00;
    public C15650ng A01;
    public C129925yW A02;
    public C18650sn A03;
    public C20390vg A04;
    public C18600si A05;
    public C18610sj A06;
    public AnonymousClass61M A07;
    public C18620sk A08;
    public C129095xA A09;
    public C117765aa A0A;
    public C117635aK A0B;
    public C18590sh A0C;
    public final C30931Zj A0D = C117305Zk.A0V("PaymentCardDetailsActivity", "payment-settings");

    @Override // X.AbstractView$OnClickListenerC121765jx
    public void A2i(AbstractC28901Pl r6, boolean z) {
        View.OnClickListener r0;
        super.A2i(r6, z);
        C30881Ze r2 = (C30881Ze) r6;
        AnonymousClass009.A05(r2);
        ((AbstractView$OnClickListenerC121765jx) this).A03.setText(C1311161i.A05(this, r2));
        AnonymousClass1ZY r02 = r2.A08;
        if (r02 != null) {
            boolean A0A = r02.A0A();
            CopyableTextView copyableTextView = ((AbstractView$OnClickListenerC121765jx) this).A04;
            if (!A0A) {
                copyableTextView.setText(R.string.payment_method_unverified);
                ((AbstractView$OnClickListenerC121765jx) this).A04.A03 = null;
                A2l(1);
                C117765aa r3 = this.A0A;
                if (r3 != null) {
                    String str = ((AbstractView$OnClickListenerC121765jx) this).A09.A0A;
                    if (!(this instanceof BrazilPaymentCardDetailsActivity)) {
                        r0 = new View.OnClickListener(str) { // from class: X.649
                            public final /* synthetic */ String A01;

                            {
                                this.A01 = r2;
                            }

                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view) {
                                AbstractActivityC121755jw r1 = AbstractActivityC121755jw.this;
                                String str2 = this.A01;
                                r1.A2C(R.string.payment_get_verify_card_data);
                                C18610sj r7 = r1.A06;
                                C1329969b r62 = new C1329969b(r1, str2);
                                AnonymousClass1W9[] r22 = new AnonymousClass1W9[2];
                                C117305Zk.A1O("action", "get-method", r22);
                                C117305Zk.A1P("credential-id", str2, r22);
                                C117305Zk.A1I(r7, new C619933k(r7.A05.A00, r7.A01, r7.A0B, r62, r7, str2), C117315Zl.A0G(r22));
                            }
                        };
                    } else {
                        r0 = new AnonymousClass646((BrazilPaymentCardDetailsActivity) this, str);
                    }
                    r3.setAlertButtonClickListener(r0);
                }
            } else {
                copyableTextView.setVisibility(8);
            }
        }
        AnonymousClass1ZY r03 = r6.A08;
        AnonymousClass009.A05(r03);
        if (r03.A0A()) {
            C117765aa r04 = this.A0A;
            if (r04 != null) {
                r04.setVisibility(8);
                C117635aK r1 = this.A0B;
                if (r1 != null) {
                    r1.setBottomDividerSpaceVisibility(0);
                }
            }
            ((AbstractView$OnClickListenerC121765jx) this).A04.setVisibility(8);
        }
    }

    public void A2k() {
        findViewById(R.id.payment_method_details_container).setVisibility(8);
        C117635aK r1 = new C117635aK(this);
        this.A0B = r1;
        r1.setCard((C30881Ze) ((AbstractView$OnClickListenerC121765jx) this).A09);
        ((ViewGroup) findViewById(R.id.payment_method_container)).addView(this.A0B, 0);
    }

    public final void A2l(int i) {
        this.A0A = new C117765aa(this);
        this.A00.removeAllViews();
        this.A00.addView(this.A0A);
        C117635aK r0 = this.A0B;
        if (r0 != null) {
            r0.setBottomDividerSpaceVisibility(8);
            this.A0A.setTopDividerVisibility(8);
        }
        this.A0A.setAlertType(i);
    }

    public void A2m(AnonymousClass1FK r5, String str, String str2) {
        C18610sj r3 = this.A06;
        LinkedList linkedList = new LinkedList();
        C117295Zj.A1M("action", "edit-default-credential", linkedList);
        C117295Zj.A1M("credential-id", str, linkedList);
        C117295Zj.A1M("version", "2", linkedList);
        if (!TextUtils.isEmpty(str2)) {
            C117295Zj.A1M("payment-type", str2.toUpperCase(Locale.US), linkedList);
        }
        r3.A0A(r5, C117295Zj.A0K(linkedList));
    }

    @Override // X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1) {
            this.A0D.A06("onActivityResult 1");
            ((AbstractView$OnClickListenerC121765jx) this).A0H.Ab2(new Runnable() { // from class: X.6Gu
                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121755jw r3 = AbstractActivityC121755jw.this;
                    C20390vg r2 = r3.A04;
                    List singletonList = Collections.singletonList(((AbstractView$OnClickListenerC121765jx) r3).A09.A0A);
                    synchronized (r2) {
                        Iterator it = singletonList.iterator();
                        while (it.hasNext()) {
                            r2.A03(C12970iu.A0x(it));
                        }
                        if (TextUtils.isEmpty(r2.A04.A02("unread_payment_method_credential_ids"))) {
                            r2.A01.A02(22);
                        }
                    }
                    C17070qD r0 = ((AbstractView$OnClickListenerC121765jx) r3).A0D;
                    r0.A03();
                    ((AbstractView$OnClickListenerC121765jx) r3).A05.A0H(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x004b: INVOKE  
                          (wrap: X.0mE : 0x0044: IGET  (r1v3 X.0mE A[REMOVE]) = (wrap: ?? : ?: CAST (X.5jx) (r3v0 'r3' X.5jw)) X.5jx.A05 X.0mE)
                          (wrap: X.6IS : 0x0048: CONSTRUCTOR  (r0v11 X.6IS A[REMOVE]) = 
                          (wrap: X.1Pl : 0x0040: INVOKE  (r2v1 X.1Pl A[REMOVE]) = 
                          (wrap: X.14j : 0x003a: IGET  (r1v2 X.14j A[REMOVE]) = (r0v8 'r0' X.0qD) X.0qD.A09 X.14j)
                          (wrap: java.lang.String : 0x003e: IGET  (r0v10 java.lang.String A[REMOVE]) = (wrap: X.1Pl : 0x003c: IGET  (r0v9 X.1Pl A[REMOVE]) = (wrap: ?? : ?: CAST (X.5jx) (r3v0 'r3' X.5jw)) X.5jx.A09 X.1Pl) X.1Pl.A0A java.lang.String)
                         type: VIRTUAL call: X.14j.A08(java.lang.String):X.1Pl)
                          (r3v0 'r3' X.5jw)
                         call: X.6IS.<init>(X.1Pl, X.5jw):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6Gu.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0048: CONSTRUCTOR  (r0v11 X.6IS A[REMOVE]) = 
                          (wrap: X.1Pl : 0x0040: INVOKE  (r2v1 X.1Pl A[REMOVE]) = 
                          (wrap: X.14j : 0x003a: IGET  (r1v2 X.14j A[REMOVE]) = (r0v8 'r0' X.0qD) X.0qD.A09 X.14j)
                          (wrap: java.lang.String : 0x003e: IGET  (r0v10 java.lang.String A[REMOVE]) = (wrap: X.1Pl : 0x003c: IGET  (r0v9 X.1Pl A[REMOVE]) = (wrap: ?? : ?: CAST (X.5jx) (r3v0 'r3' X.5jw)) X.5jx.A09 X.1Pl) X.1Pl.A0A java.lang.String)
                         type: VIRTUAL call: X.14j.A08(java.lang.String):X.1Pl)
                          (r3v0 'r3' X.5jw)
                         call: X.6IS.<init>(X.1Pl, X.5jw):void type: CONSTRUCTOR in method: X.6Gu.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6IS, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                        this = this;
                        X.5jw r3 = X.AbstractActivityC121755jw.this
                        X.0vg r2 = r3.A04
                        X.1Pl r0 = r3.A09
                        java.lang.String r0 = r0.A0A
                        java.util.List r0 = java.util.Collections.singletonList(r0)
                        monitor-enter(r2)
                        java.util.Iterator r1 = r0.iterator()     // Catch: all -> 0x004f
                    L_0x0011:
                        boolean r0 = r1.hasNext()     // Catch: all -> 0x004f
                        if (r0 == 0) goto L_0x001f
                        java.lang.String r0 = X.C12970iu.A0x(r1)     // Catch: all -> 0x004f
                        r2.A03(r0)     // Catch: all -> 0x004f
                        goto L_0x0011
                    L_0x001f:
                        X.0xL r1 = r2.A04     // Catch: all -> 0x004f
                        java.lang.String r0 = "unread_payment_method_credential_ids"
                        java.lang.String r0 = r1.A02(r0)     // Catch: all -> 0x004f
                        boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch: all -> 0x004f
                        if (r0 == 0) goto L_0x0034
                        X.0sK r1 = r2.A01     // Catch: all -> 0x004f
                        r0 = 22
                        r1.A02(r0)     // Catch: all -> 0x004f
                    L_0x0034:
                        monitor-exit(r2)
                        X.0qD r0 = r3.A0D
                        r0.A03()
                        X.14j r1 = r0.A09
                        X.1Pl r0 = r3.A09
                        java.lang.String r0 = r0.A0A
                        X.1Pl r2 = r1.A08(r0)
                        X.0mE r1 = r3.A05
                        X.6IS r0 = new X.6IS
                        r0.<init>(r2, r3)
                        r1.A0H(r0)
                        return
                    L_0x004f:
                        r0 = move-exception
                        monitor-exit(r2)
                        throw r0
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.RunnableC135006Gu.run():void");
                }
            });
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    @Override // X.AbstractView$OnClickListenerC121765jx, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        int A2e;
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0A(R.string.payment_card_details_title);
            if (!(this instanceof BrazilPaymentCardDetailsActivity)) {
                A2h();
                A2e = 0;
            } else {
                A2h();
                A2e = A2e(2131952554);
            }
            ((AbstractView$OnClickListenerC121765jx) this).A0G.A0B(((AbstractView$OnClickListenerC121765jx) this).A0G.getCurrentContentInsetLeft(), A2e);
        }
        this.A00 = (FrameLayout) findViewById(R.id.method_details_alert_container);
    }
}
