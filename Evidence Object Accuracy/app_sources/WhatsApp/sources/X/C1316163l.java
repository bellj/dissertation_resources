package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1316163l implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(23);
    public final C130575zd A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1316163l(C130575zd r1, String str, String str2, String str3, String str4) {
        this.A04 = str;
        this.A02 = str2;
        this.A03 = str3;
        this.A01 = str4;
        this.A00 = r1;
    }

    public C1316163l(Parcel parcel) {
        this.A04 = parcel.readString();
        this.A02 = parcel.readString();
        this.A03 = parcel.readString();
        this.A01 = parcel.readString();
        this.A00 = new C130575zd(parcel.readDouble(), parcel.readDouble());
    }

    public static C1316163l A00(AnonymousClass1V8 r8) {
        if (r8 == null) {
            return null;
        }
        AnonymousClass1V8 A0E = r8.A0E("brand");
        AnonymousClass1V8 A0E2 = r8.A0E("coordinates");
        if (A0E == null || A0E2 == null) {
            return null;
        }
        String A0I = r8.A0I("id", null);
        String A0I2 = r8.A0I("full_address", null);
        String A0I3 = r8.A0I("thumbnail_image_url", null);
        String A0I4 = A0E.A0I("name", null);
        C130575zd A00 = C130575zd.A00(A0E2);
        if (AnonymousClass1US.A0C(A0I) || AnonymousClass1US.A0C(A0I2) || AnonymousClass1US.A0C(A0I4) || A00 == null) {
            return null;
        }
        return new C1316163l(A00, A0I, A0I2, A0I3, A0I4);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A04);
        parcel.writeString(this.A02);
        parcel.writeString(this.A03);
        parcel.writeString(this.A01);
        C130575zd r2 = this.A00;
        parcel.writeDouble(r2.A00);
        parcel.writeDouble(r2.A01);
    }
}
