package X;

import android.os.Handler;
import android.os.Message;

/* renamed from: X.4iK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98214iK implements Handler.Callback {
    public final /* synthetic */ C64883Hh A00;

    public C98214iK(C64883Hh r1) {
        this.A00 = r1;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what != 0) {
            return false;
        }
        C64883Hh r3 = this.A00;
        AnonymousClass4PT r2 = (AnonymousClass4PT) message.obj;
        synchronized (r3.A03) {
            if (r3.A00 == r2 || r3.A01 == r2) {
                r3.A06(r2, 2);
            }
        }
        return true;
    }
}
