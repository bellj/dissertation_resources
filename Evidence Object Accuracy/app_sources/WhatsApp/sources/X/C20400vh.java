package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0vh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20400vh {
    public final C16590pI A00;
    public final C18360sK A01;
    public final C241414j A02;
    public final C18050rp A03;
    public final C18600si A04;
    public final C22710zW A05;
    public final C17070qD A06;
    public final C15860o1 A07;
    public final AbstractC14440lR A08;

    public C20400vh(C16590pI r1, C18360sK r2, C241414j r3, C18050rp r4, C18600si r5, C22710zW r6, C17070qD r7, C15860o1 r8, AbstractC14440lR r9) {
        this.A00 = r1;
        this.A08 = r9;
        this.A03 = r4;
        this.A06 = r7;
        this.A07 = r8;
        this.A04 = r5;
        this.A05 = r6;
        this.A01 = r2;
        this.A02 = r3;
    }

    public void A00() {
        this.A08.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(this, 13));
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01() {
        /*
            r10 = this;
            X.0zW r0 = r10.A05
            boolean r0 = r0.A03()
            if (r0 == 0) goto L_0x00cd
            X.0si r6 = r10.A04
            android.content.SharedPreferences r1 = r6.A01()
            java.lang.String r0 = "payment_kyc_info"
            r4 = 0
            java.lang.String r2 = r1.getString(r0, r4)
            if (r2 == 0) goto L_0x007c
            boolean r0 = X.AnonymousClass1US.A0C(r2)
            if (r0 != 0) goto L_0x007c
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch: JSONException -> 0x0075
            r1.<init>(r2)     // Catch: JSONException -> 0x0075
            java.lang.String r0 = "state"
            java.lang.String r7 = r1.getString(r0)     // Catch: JSONException -> 0x0075
            java.lang.String r0 = "rejection-code"
            r9 = -1
            int r8 = r1.optInt(r0, r9)     // Catch: JSONException -> 0x0075
            java.lang.String r0 = "actions-requested"
            org.json.JSONObject r1 = r1.optJSONObject(r0)     // Catch: JSONException -> 0x0075
            if (r1 == 0) goto L_0x0065
            java.lang.String r0 = "obligation"
            java.lang.String r5 = r1.getString(r0)     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            java.lang.String r0 = "actions"
            org.json.JSONArray r2 = r1.getJSONArray(r0)     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            r3.<init>()     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            r1 = 0
        L_0x004a:
            int r0 = r2.length()     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            if (r1 >= r0) goto L_0x005a
            java.lang.String r0 = r2.getString(r1)     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            r3.add(r0)     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            int r1 = r1 + 1
            goto L_0x004a
        L_0x005a:
            X.24f r2 = new X.24f     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            r2.<init>(r5, r3)     // Catch: JSONException -> 0x0060, JSONException -> 0x0075
            goto L_0x0066
        L_0x0060:
            java.lang.String r0 = "PAY: PaymentKycActionsRequested fromJsonString threw exception"
            com.whatsapp.util.Log.e(r0)     // Catch: JSONException -> 0x0075
        L_0x0065:
            r2 = 0
        L_0x0066:
            if (r8 == r9) goto L_0x0069
            goto L_0x006b
        L_0x0069:
            r1 = r4
            goto L_0x006f
        L_0x006b:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch: JSONException -> 0x0075
        L_0x006f:
            X.24g r0 = new X.24g     // Catch: JSONException -> 0x0075
            r0.<init>(r2, r1, r7)     // Catch: JSONException -> 0x0075
            goto L_0x007b
        L_0x0075:
            java.lang.String r0 = "PAY: PaymentKycInfo fromJsonString threw exception"
            com.whatsapp.util.Log.e(r0)
            goto L_0x007c
        L_0x007b:
            r4 = r0
        L_0x007c:
            android.content.SharedPreferences r2 = r6.A01()
            java.lang.String r1 = "payment_kyc_update_ack"
            r0 = 1
            boolean r0 = r2.getBoolean(r1, r0)
            if (r4 == 0) goto L_0x00c5
            if (r0 != 0) goto L_0x00c5
            java.lang.String r1 = r4.A02
            java.lang.String r0 = "PENDING"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00ae
            X.0pI r0 = r10.A00
            android.content.Context r1 = r0.A00
            r0 = 2131889672(0x7f120e08, float:1.9414014E38)
            java.lang.String r3 = r1.getString(r0)
            r0 = 2131889671(0x7f120e07, float:1.9414012E38)
        L_0x00a3:
            java.lang.String r2 = r1.getString(r0)
            r1 = 0
            java.lang.String r0 = "KYC"
            r10.A04(r3, r2, r0, r1)
            return
        L_0x00ae:
            java.lang.String r0 = "COMPLETED"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00cd
            X.0pI r0 = r10.A00
            android.content.Context r1 = r0.A00
            r0 = 2131889670(0x7f120e06, float:1.941401E38)
            java.lang.String r3 = r1.getString(r0)
            r0 = 2131889669(0x7f120e05, float:1.9414008E38)
            goto L_0x00a3
        L_0x00c5:
            X.0sK r2 = r10.A01
            r1 = 26
            r0 = 0
            r2.A04(r1, r0)
        L_0x00cd:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20400vh.A01():void");
    }

    public void A02(String str) {
        this.A08.Ab2(new RunnableBRunnable0Shape0S1100000_I0(29, str, this));
    }

    public final void A03(String str) {
        if (this.A05.A03()) {
            ArrayList arrayList = new ArrayList();
            C18050rp r0 = this.A03;
            if (str != null) {
                C460124c A01 = r0.A01(str);
                if (A01 != null) {
                    arrayList.add(A01);
                }
            } else {
                arrayList.addAll(r0.A02());
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C460124c r4 = (C460124c) it.next();
                C32631cT r3 = r4.A03;
                boolean z = this.A04.A01().getBoolean("payment_step_up_update_ack", true);
                if (r3 == null || z) {
                    this.A01.A04(28, str);
                    return;
                }
                A04(r4.A08, r4.A05, "STEP_UP", r4.A06);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x013b, code lost:
        if (r0 == false) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04(java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18) {
        /*
        // Method dump skipped, instructions count: 390
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20400vh.A04(java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }
}
