package X;

import android.net.Uri;
import java.util.HashMap;

/* renamed from: X.31s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617031s extends AnonymousClass2JJ {
    public static final Uri A00;

    static {
        StringBuilder A0k = C12960it.A0k("content://");
        A0k.append("com.whatsapp");
        A00 = Uri.parse(C12960it.A0d(".provider.media/items", A0k));
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C617031s(X.C16590pI r7, X.C19390u2 r8, java.lang.String r9, int r10) {
        /*
            r6 = this;
            r5 = 2
            android.net.Uri r0 = X.C617031s.A00
            android.net.Uri$Builder r1 = r0.buildUpon()
            java.lang.String r0 = "bucketId"
            r4 = r9
            android.net.Uri$Builder r2 = r1.appendQueryParameter(r0, r9)
            r0 = 1
            java.lang.String r1 = "include"
            if (r10 == r0) goto L_0x002c
            if (r10 == r5) goto L_0x0029
            r0 = 4
            if (r10 != r0) goto L_0x001e
            java.lang.String r0 = "video"
        L_0x001b:
            r2.appendQueryParameter(r1, r0)
        L_0x001e:
            android.net.Uri r1 = r2.build()
            r0 = r6
            r3 = r8
            r2 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x0029:
            java.lang.String r0 = "gif"
            goto L_0x001b
        L_0x002c:
            java.lang.String r0 = "images"
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C617031s.<init>(X.0pI, X.0u2, java.lang.String, int):void");
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        return C12970iu.A11();
    }
}
