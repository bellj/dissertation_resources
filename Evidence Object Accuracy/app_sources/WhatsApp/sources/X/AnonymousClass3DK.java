package X;

import android.graphics.RectF;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3DK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DK {
    public RectF A00;
    public AbstractC454821u A01;
    public final C89524Kh A02;
    public final C64243Eu A03;
    public final AnonymousClass3EC A04;
    public final C64213Er A05;
    public final C64073Ed A06;

    public AnonymousClass3DK(C89524Kh r2, C64243Eu r3, C64213Er r4, C64073Ed r5) {
        this.A03 = r3;
        this.A04 = new AnonymousClass3EC(r3);
        this.A06 = r5;
        this.A05 = r4;
        this.A02 = r2;
    }

    public final void A00() {
        C64243Eu r2 = this.A03;
        if (r2.A07 != null) {
            RectF A0K = C12980iv.A0K();
            r2.A09.mapRect(A0K, r2.A07);
            int i = r2.A04;
            int i2 = r2.A03;
            float width = A0K.width() / A0K.height();
            float f = (float) i;
            float f2 = (float) i2;
            if (width > f / f2) {
                f2 = f / width;
            } else {
                f = f2 * width;
            }
            float f3 = (float) (i >> 1);
            float f4 = (float) (i2 >> 1);
            float f5 = f / 2.0f;
            float f6 = f2 / 2.0f;
            RectF rectF = new RectF(f3 - f5, f4 - f6, f3 + f5, f4 + f6);
            RectF rectF2 = this.A00;
            if (rectF2 == null || !rectF2.equals(rectF)) {
                this.A00 = rectF;
                C64213Er r4 = this.A05;
                RectF rectF3 = r4.A02;
                rectF3.set(rectF);
                Map map = r4.A05;
                Iterator A0o = C12960it.A0o(map);
                while (A0o.hasNext()) {
                    AbstractC64383Fi r0 = (AbstractC64383Fi) A0o.next();
                    View view = r0.A06;
                    if (view != null) {
                        r0.A07.removeView(view);
                    }
                }
                map.clear();
                Integer A0V = C12960it.A0V();
                Handler handler = r4.A09;
                ViewGroup viewGroup = r4.A0A;
                Vibrator vibrator = r4.A03;
                map.put(A0V, new AnonymousClass337(rectF3, handler, vibrator, viewGroup, 1));
                map.put(C12970iu.A0g(), new AnonymousClass337(rectF3, handler, vibrator, viewGroup, 2));
                map.put(C12970iu.A0h(), new AnonymousClass338(rectF3, handler, vibrator, viewGroup));
            }
        }
    }
}
