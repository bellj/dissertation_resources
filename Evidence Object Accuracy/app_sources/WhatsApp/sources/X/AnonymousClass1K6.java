package X;

import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.1K6  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1K6<E> extends List<E>, RandomAccess {
    AnonymousClass1K6 ALZ(int i);
}
