package X;

import android.view.View;

/* renamed from: X.5mu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123285mu extends AbstractC125975s7 {
    public View.OnClickListener A00;
    public boolean A01;
    public boolean A02;
    public final CharSequence A03;
    public final CharSequence A04;
    public final CharSequence A05;
    public final CharSequence A06;
    public final CharSequence A07;

    public C123285mu(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, CharSequence charSequence5, boolean z, boolean z2) {
        super(1004);
        this.A04 = charSequence;
        this.A05 = charSequence2;
        this.A07 = charSequence3;
        this.A03 = charSequence4;
        this.A01 = z;
        this.A02 = z2;
        this.A06 = charSequence5;
    }
}
