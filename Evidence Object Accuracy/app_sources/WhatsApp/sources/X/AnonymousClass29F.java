package X;

import android.accounts.Account;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import java.lang.ref.WeakReference;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.29F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29F extends AbstractC16350or {
    public final C14900mE A00;
    public final C22730zY A01;
    public final AnonymousClass3C0 A02;
    public final C15810nw A03;
    public final C16590pI A04;
    public final C14820m6 A05;
    public final C15880o3 A06;
    public final C28181Ma A07 = new C28181Ma("gdrive-activity/one-time-setup");
    public final WeakReference A08;
    public final Set A09;
    public final Timer A0A = new Timer("perform-one-time-setup");
    public final AtomicBoolean A0B;
    public final AtomicBoolean A0C;
    public final Account[] A0D;

    public AnonymousClass29F(C14900mE r3, C22730zY r4, RestoreFromBackupActivity restoreFromBackupActivity, AnonymousClass3C0 r6, C15810nw r7, C16590pI r8, C14820m6 r9, C15880o3 r10, Set set, AtomicBoolean atomicBoolean, AtomicBoolean atomicBoolean2, Account[] accountArr) {
        this.A04 = r8;
        this.A00 = r3;
        this.A03 = r7;
        this.A06 = r10;
        this.A05 = r9;
        this.A02 = r6;
        this.A01 = r4;
        this.A0D = accountArr;
        this.A09 = set;
        this.A0C = atomicBoolean;
        this.A0B = atomicBoolean2;
        this.A08 = new WeakReference(restoreFromBackupActivity);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Can't wrap try/catch for region: R(12:80|(3:82|(1:84)|85)(18:(1:87)(4:88|(1:90)(1:94)|91|(2:93|95)(2:96|(1:98)(1:100)))|101|149|102|(2:104|(1:106))|109|148|110|(3:160|111|(1:113)(1:174))|114|120|(3:122|(1:124)|125)|126|3e6|130|137|138|139)|99|109|148|110|(4:160|111|(0)(0)|113)|114|120|(0)|126|3e6) */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x03a4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x03a5, code lost:
        com.whatsapp.util.Log.e(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x03f8, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x03f9, code lost:
        com.whatsapp.util.Log.e("gdrive/backup/selector/one-time-setup/read-storage-permission-withdrawn/exiting", r1);
        r0 = (com.whatsapp.backup.google.RestoreFromBackupActivity) r13.A0G.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0406, code lost:
        if (r0 != null) goto L_0x0408;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x0408, code lost:
        r0.A2m();
        r0.finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x040f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0410, code lost:
        com.whatsapp.util.Log.e("gdrive/backup/selector/decide", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01c4, code lost:
        if (r8 != null) goto L_0x01e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01c6, code lost:
        r5 = r39.A0C;
        r5.set(X.C44771zW.A0L(r0, r0.A09(), r9.getString("registration_jid", null), r5.get()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01df, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01e0, code lost:
        com.whatsapp.util.Log.w(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01e5, code lost:
        r13 = r8.A02;
        X.AnonymousClass009.A00();
        r19 = new X.C28181Ma("gdrive/backup/selector/decide");
        r12 = r8.A01;
        r9 = r12.A0B;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01f8, code lost:
        if (r9 != null) goto L_0x01fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01fa, code lost:
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01fc, code lost:
        r7 = r9.optBoolean("encryptedBackupEnabled", false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0202, code lost:
        r18 = X.AnonymousClass29K.A02(r13.A06, r12);
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0209, code lost:
        if (r18 == null) goto L_0x0416;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x020b, code lost:
        r11 = r13.A0C;
        r15 = r11.A0J();
        r10 = r15.length;
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0213, code lost:
        if (r5 >= r10) goto L_0x0244;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0215, code lost:
        r2 = r15[r5];
        r1 = X.C44771zW.A08(r13.A09.A00, r13.A08, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0221, code lost:
        if (r1 == null) goto L_0x0230;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0223, code lost:
        r3 = (X.AnonymousClass3HV) r18.get(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x022b, code lost:
        if (r3 != null) goto L_0x0245;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x022d, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0230, code lost:
        r0 = new java.lang.StringBuilder("gdrive/backup/selector/decide upload title is null for ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0243, code lost:
        throw new java.lang.IllegalStateException(r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0244, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0245, code lost:
        r10 = r11.A09();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x024a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x024b, code lost:
        com.whatsapp.util.Log.e("gdrive/backup/selector/device unable to access local backup", r1);
        r10 = null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x038f A[Catch: all -> 0x039f, LOOP:2: B:160:0x038b->B:113:0x038f, LOOP_END, TRY_LEAVE, TryCatch #11 {all -> 0x039f, blocks: (B:111:0x038b, B:113:0x038f), top: B:160:0x038b }] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x03ac A[Catch: all -> 0x041d, TryCatch #9 {all -> 0x041d, blocks: (B:16:0x005a, B:18:0x0061, B:20:0x006b, B:21:0x0072, B:23:0x007a, B:24:0x0096, B:25:0x009d, B:39:0x011f, B:41:0x0143, B:47:0x0151, B:49:0x0153, B:50:0x0168, B:52:0x0185, B:54:0x01a3, B:56:0x01c0, B:58:0x01c6, B:60:0x01e0, B:61:0x01e5, B:64:0x01fc, B:65:0x0202, B:67:0x020b, B:69:0x0215, B:71:0x0223, B:73:0x022d, B:74:0x0230, B:75:0x0243, B:77:0x0245, B:79:0x024b, B:80:0x0251, B:82:0x0268, B:85:0x026f, B:87:0x0275, B:88:0x0280, B:90:0x0284, B:91:0x0286, B:93:0x0299, B:94:0x02a2, B:96:0x02a8, B:98:0x02bc, B:100:0x0307, B:101:0x034f, B:102:0x0352, B:104:0x0358, B:108:0x0367, B:109:0x036c, B:110:0x0386, B:114:0x0395, B:116:0x03a0, B:117:0x03a3, B:119:0x03a5, B:120:0x03a8, B:122:0x03ac, B:124:0x03b8, B:125:0x03be, B:126:0x03c0, B:127:0x03e6, B:129:0x03f3, B:132:0x03f9, B:134:0x0408, B:136:0x0410, B:128:0x03e7), top: B:157:0x005a, inners: #2, #4, #5, #6, #7, #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x03e7 A[Catch: all -> 0x03f5, TRY_ENTER, TRY_LEAVE, TryCatch #9 {all -> 0x041d, blocks: (B:16:0x005a, B:18:0x0061, B:20:0x006b, B:21:0x0072, B:23:0x007a, B:24:0x0096, B:25:0x009d, B:39:0x011f, B:41:0x0143, B:47:0x0151, B:49:0x0153, B:50:0x0168, B:52:0x0185, B:54:0x01a3, B:56:0x01c0, B:58:0x01c6, B:60:0x01e0, B:61:0x01e5, B:64:0x01fc, B:65:0x0202, B:67:0x020b, B:69:0x0215, B:71:0x0223, B:73:0x022d, B:74:0x0230, B:75:0x0243, B:77:0x0245, B:79:0x024b, B:80:0x0251, B:82:0x0268, B:85:0x026f, B:87:0x0275, B:88:0x0280, B:90:0x0284, B:91:0x0286, B:93:0x0299, B:94:0x02a2, B:96:0x02a8, B:98:0x02bc, B:100:0x0307, B:101:0x034f, B:102:0x0352, B:104:0x0358, B:108:0x0367, B:109:0x036c, B:110:0x0386, B:114:0x0395, B:116:0x03a0, B:117:0x03a3, B:119:0x03a5, B:120:0x03a8, B:122:0x03ac, B:124:0x03b8, B:125:0x03be, B:126:0x03c0, B:127:0x03e6, B:129:0x03f3, B:132:0x03f9, B:134:0x0408, B:136:0x0410, B:128:0x03e7), top: B:157:0x005a, inners: #2, #4, #5, #6, #7, #10 }] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0395 A[EDGE_INSN: B:174:0x0395->B:114:0x0395 ?: BREAK  , SYNTHETIC] */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r40) {
        /*
        // Method dump skipped, instructions count: 1062
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass29F.A05(java.lang.Object[]):java.lang.Object");
    }
}
