package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;
import java.math.RoundingMode;

/* renamed from: X.6B9  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6B9 implements AbstractC136446Mo {
    public final AbstractC30791Yv A00;
    public final AbstractC30791Yv A01;
    public final AnonymousClass63Y A02;
    public final AnonymousClass63Y A03;

    public AnonymousClass6B9(AnonymousClass63Y r2, AnonymousClass63Y r3) {
        this.A03 = r2;
        this.A02 = r3;
        this.A01 = r2.A00;
        this.A00 = r3.A00;
    }

    @Override // X.AbstractC136446Mo
    public AnonymousClass6F2 A9k(AnonymousClass6F2 r7, C1315863i r8, int i) {
        BigDecimal bigDecimal;
        AbstractC30791Yv r0;
        BigDecimal bigDecimal2;
        AbstractC30791Yv r1;
        if (C117305Zk.A1V(this.A00, ((AbstractC30781Yu) r7.A00).A04)) {
            if (i == 1) {
                i = 2;
            } else if (i == 0) {
                return r7;
            }
            if (i == 4) {
                bigDecimal2 = r8.A05.A05;
                r1 = this.A03.A02;
            } else if (i == 2) {
                BigDecimal bigDecimal3 = r8.A05.A05;
                AnonymousClass63Y r3 = this.A03;
                return C127695uu.A00(r3.A01, C127695uu.A00(r3.A02, r7, bigDecimal3, false), r8.A03.A05, true);
            } else if (i == 5) {
                return r7;
            } else {
                bigDecimal2 = r8.A02.A05;
                r1 = this.A02.A01;
            }
            return r7.A05(new C127695uu(r1, bigDecimal2, 0, true));
        }
        if (i == 1 || i == 0) {
            i = 5;
        }
        if (i == 4) {
            return C127695uu.A01(r7, this.A03, r8.A03.A05, 0, true);
        } else if (i == 2) {
            return r7;
        } else {
            AnonymousClass6F2 A00 = C127695uu.A00(this.A03.A02, r7, r8.A03.A05, false);
            C1316563p r02 = r8.A05;
            if (i == 5) {
                bigDecimal = r02.A05;
                r0 = this.A02.A02;
            } else {
                BigDecimal bigDecimal4 = r02.A05;
                AnonymousClass63Y r12 = this.A02;
                A00 = C127695uu.A01(A00, r12, bigDecimal4, 1, false);
                bigDecimal = r8.A02.A05;
                r0 = r12.A01;
            }
            return C127695uu.A00(r0, A00, bigDecimal, true);
        }
    }

    @Override // X.AbstractC136446Mo
    public CharSequence AHI(Context context, AnonymousClass018 r9, C1315863i r10) {
        int i = 4;
        BigDecimal A01 = C130275z5.A01(BigDecimal.ONE.divide(r10.A05.A05, 4, RoundingMode.HALF_EVEN), r10.A03.A05);
        AbstractC30791Yv r3 = this.A02.A02;
        Object[] objArr = new Object[2];
        objArr[0] = r3.AAB(r9, BigDecimal.ONE, 2);
        AbstractC30791Yv r1 = this.A03.A01;
        if (BigDecimal.ONE.equals(A01)) {
            i = 0;
        }
        return r3.AA7(context, C12960it.A0X(context, C117305Zk.A0k(r9, r1, A01, i), objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate));
    }

    @Override // X.AbstractC136446Mo
    public boolean AKE(C1315863i r3) {
        return ((AbstractC30781Yu) this.A01).A04.equals(r3.A03.A04) && ((AbstractC30781Yu) this.A00).A04.equals(r3.A05.A04);
    }
}
