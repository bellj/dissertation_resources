package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3em  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72543em extends AnimatorListenerAdapter {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass2cj A01;

    public C72543em(AnonymousClass2cj r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2cj r1 = this.A01;
        r1.A05 = this.A00;
        r1.A00 = 0.0f;
    }
}
