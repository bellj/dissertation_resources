package X;

/* renamed from: X.1Qz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C29131Qz {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public long A05;
    public AnonymousClass1R1 A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public boolean A0D;
    public boolean A0E;
    public byte[] A0F;
    public final AnonymousClass1R0 A0G;
    public final EnumC29121Qy A0H;
    public final String A0I;
    public final boolean A0J;

    public C29131Qz(EnumC29121Qy r2, String str, boolean z) {
        this.A0H = r2;
        this.A0G = null;
        this.A0I = str;
        this.A0J = z;
    }

    public C29131Qz(AnonymousClass1R0 r2, EnumC29121Qy r3) {
        this.A0H = r3;
        this.A0G = r2;
        this.A0I = null;
        this.A0J = false;
    }
}
