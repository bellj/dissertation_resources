package X;

import android.view.View;
import com.whatsapp.authentication.FingerprintBottomSheet;

/* renamed from: X.3sb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80623sb extends AnonymousClass2UH {
    public final /* synthetic */ FingerprintBottomSheet A00;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C80623sb(FingerprintBottomSheet fingerprintBottomSheet) {
        this.A00 = fingerprintBottomSheet;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 4 || i == 5) {
            this.A00.A1B();
        }
    }
}
