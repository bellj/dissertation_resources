package X;

import java.util.Arrays;

/* renamed from: X.1PA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PA {
    public final byte A00;
    public final byte[] A01;

    public AnonymousClass1PA(byte[] bArr, byte b) {
        this.A01 = bArr;
        this.A00 = b;
    }

    public byte[] A00() {
        return C16050oM.A05(new byte[]{this.A00}, this.A01);
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AnonymousClass1PA)) {
            return false;
        }
        return Arrays.equals(this.A01, ((AnonymousClass1PA) obj).A01);
    }

    public int hashCode() {
        return Arrays.hashCode(this.A01);
    }
}
