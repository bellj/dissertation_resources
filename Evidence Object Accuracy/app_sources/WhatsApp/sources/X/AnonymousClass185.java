package X;

import android.content.Context;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* renamed from: X.185  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass185 {
    public C461124n A00;
    public Boolean A01;
    public final C003301l A02;
    public final C16120oU A03;
    public final AnonymousClass184 A04;
    public final AbstractC21180x0 A05;

    public AnonymousClass185(C003301l r1, C16120oU r2, AnonymousClass184 r3, AbstractC21180x0 r4) {
        this.A05 = r4;
        this.A03 = r2;
        this.A02 = r1;
        this.A04 = r3;
    }

    public void A00() {
        C461124n r10;
        if ((A03() || this.A05.AJj(689639794)) && (r10 = this.A00) != null) {
            AnonymousClass04F r8 = r10.A03;
            if (r8.A03) {
                r8.A03 = false;
                AnonymousClass04G r0 = r8.A05;
                r0.A03 = false;
                r0.A05.removeFrameCallback(r0.A04);
                double min = Math.min(r8.A01, 3600.0d);
                double min2 = Math.min(r8.A00, 1000.0d);
                long millis = TimeUnit.NANOSECONDS.toMillis(Math.min(r8.A02, AnonymousClass04F.A07));
                AnonymousClass04H r02 = r8.A06;
                AnonymousClass04I r14 = new AnonymousClass04I(min, min2, millis);
                C461124n r9 = ((C461224o) r02).A00;
                Integer num = r9.A00;
                if (num != null) {
                    if (r9.A02) {
                        Map map = r9.A06;
                        if (!map.containsKey(num)) {
                            map.put(r9.A00, new C461324p());
                        }
                        C461324p r4 = (C461324p) map.get(r9.A00);
                        r4.A02++;
                        r4.A00 += r14.A00;
                        r4.A01 += r14.A01;
                        r4.A03 += r14.A02;
                    }
                    if (r9.A01) {
                        double d = r14.A00;
                        if (!Double.isNaN(d)) {
                            AbstractC21180x0 r13 = r9.A05;
                            long j = r14.A02;
                            r13.AKv(689639794, "timeSpent", j);
                            double d2 = (double) j;
                            r13.AL2("smallFrames", (r14.A01 * 60000.0d) / d2, 689639794);
                            r13.AL2("largeFrames", (d * 60000.0d) / d2, 689639794);
                            Integer num2 = r9.A00;
                            if (num2 != null) {
                                r13.AKu(689639794, "scrollSurface", num2.intValue());
                            }
                        }
                    }
                }
                r8.A01 = 0.0d;
                r8.A00 = 0.0d;
                r8.A02 = 0;
            }
            r10.A00 = null;
            AbstractC21180x0 r2 = this.A05;
            if (r2.AJj(689639794)) {
                r2.AL7(689639794, 2);
            }
        }
    }

    public void A01(int i) {
        if (this.A00 != null) {
            AbstractC21180x0 r2 = this.A05;
            r2.ALE(689639794);
            C461124n r4 = this.A00;
            boolean A03 = A03();
            boolean AJj = r2.AJj(689639794);
            r4.A01 = AJj;
            r4.A02 = A03;
            if (AJj || A03) {
                AnonymousClass04F r1 = r4.A03;
                if (!r1.A03) {
                    r1.A03 = true;
                    AnonymousClass04G r3 = r1.A05;
                    if (!r3.A03) {
                        r3.A00 = -1;
                    }
                    r3.A03 = true;
                    r3.A05.postFrameCallback(r3.A04);
                }
                r4.A00 = Integer.valueOf(i);
            }
        }
    }

    public void A02(Context context) {
        if ((A03() || this.A05.ALA(689639794)) && this.A00 == null) {
            this.A00 = new C461124n(context, this.A02, this.A03, this.A05);
        }
    }

    public final boolean A03() {
        Boolean bool = this.A01;
        if (bool == null) {
            bool = Boolean.valueOf(new AnonymousClass00E(1, 1, 1).A00());
            this.A01 = bool;
        }
        return bool.booleanValue();
    }
}
