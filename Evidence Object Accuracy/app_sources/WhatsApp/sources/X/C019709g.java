package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

/* renamed from: X.09g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C019709g extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass0IY A00;

    public C019709g(AnonymousClass0IY r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        int type;
        AnonymousClass0IY r2 = this.A00;
        NetworkInfo activeNetworkInfo = r2.A03.getActiveNetworkInfo();
        if (activeNetworkInfo != null && (type = activeNetworkInfo.getType()) != r2.A00) {
            r2.A03();
            r2.A00 = type;
        }
    }
}
