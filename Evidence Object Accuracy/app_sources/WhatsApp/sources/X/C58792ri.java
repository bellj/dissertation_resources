package X;

import android.net.TrafficStats;
import com.whatsapp.backup.google.GoogleBackupService;
import com.whatsapp.util.Log;
import java.io.IOException;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.2ri  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58792ri extends AbstractC83933y8 {
    public final /* synthetic */ GoogleBackupService A00;
    public final /* synthetic */ C44791zY A01;
    public final /* synthetic */ String A02;

    public C58792ri(GoogleBackupService googleBackupService, C44791zY r2, String str) {
        this.A00 = googleBackupService;
        this.A01 = r2;
        this.A02 = str;
    }

    @Override // X.AbstractC83933y8, X.AbstractC83953yA, X.AbstractC83983yD, X.AbstractC84003yF, X.AbstractC84013yG, X.AbstractC84023yH, X.AnonymousClass4UU
    public /* bridge */ /* synthetic */ Object A00(int i) {
        HttpsURLConnection httpsURLConnection;
        int responseCode;
        C44791zY r4 = this.A01;
        String str = this.A02;
        Log.i(C12960it.A0d(str, C12960it.A0k("gdrive-api-v2/delete-backup/")));
        boolean z = false;
        if (r4.A0B()) {
            Log.i("gdrive-api-v2/delete-backup/api disabled");
        } else {
            try {
                TrafficStats.setThreadStatsTag(13);
                httpsURLConnection = null;
                try {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("clients/wa/backups/");
                    httpsURLConnection = r4.A07("DELETE", C12960it.A0d(str, A0h), null, null, false);
                    responseCode = httpsURLConnection.getResponseCode();
                    StringBuilder A0j = C12960it.A0j("gdrive-api-v2/delete-backup/");
                    A0j.append(responseCode);
                    C12960it.A1F(A0j);
                } catch (IOException e) {
                    Log.e(e);
                }
                if (responseCode == 403) {
                    throw new C84093yP();
                } else if (responseCode == 200) {
                    z = true;
                }
            } finally {
                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
                TrafficStats.clearThreadStatsTag();
            }
        }
        return Boolean.valueOf(z);
    }
}
