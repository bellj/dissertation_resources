package X;

import android.view.View;

/* renamed from: X.4uY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C105744uY implements AbstractC11920h5 {
    @Override // X.AbstractC11920h5
    public final void Af5(View view, float f) {
        if (f < 0.0f || f >= 1.0f) {
            view.setTranslationX(0.0f);
            view.setAlpha(1.0f);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            return;
        }
        view.setTranslationX((-f) * ((float) view.getWidth()));
        view.setAlpha(Math.max(0.0f, 1.0f - f));
        float max = Math.max(0.0f, 1.0f - (f * 0.3f));
        view.setScaleX(max);
        view.setScaleY(max);
    }
}
