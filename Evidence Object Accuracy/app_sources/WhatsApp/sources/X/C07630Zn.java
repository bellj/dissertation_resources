package X;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.foreground.SystemForegroundService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* renamed from: X.0Zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07630Zn implements AbstractC11960h9, AbstractC12450hw {
    public static final String A0A = C06390Tk.A01("SystemFgDispatcher");
    public Context A00;
    public AnonymousClass022 A01;
    public AbstractC11970hA A02;
    public String A03;
    public final AnonymousClass0Zu A04;
    public final AbstractC11500gO A05;
    public final Object A06 = new Object();
    public final Map A07;
    public final Map A08;
    public final Set A09;

    @Override // X.AbstractC12450hw
    public void AM8(List list) {
    }

    public C07630Zn(Context context) {
        this.A00 = context;
        AnonymousClass022 A00 = AnonymousClass022.A00(context);
        this.A01 = A00;
        AbstractC11500gO r2 = A00.A06;
        this.A05 = r2;
        this.A03 = null;
        this.A07 = new LinkedHashMap();
        this.A09 = new HashSet();
        this.A08 = new HashMap();
        this.A04 = new AnonymousClass0Zu(this.A00, this, r2);
        this.A01.A03.A02(this);
    }

    public void A00() {
        this.A02 = null;
        synchronized (this.A06) {
            this.A04.A00();
        }
        this.A01.A03.A03(this);
    }

    public void A01(Intent intent) {
        Handler handler;
        RunnableC10060dx r4;
        String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            C06390Tk.A00().A04(A0A, String.format("Started foreground service %s", intent), new Throwable[0]);
            String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
            WorkDatabase workDatabase = this.A01.A04;
            ((C07760a2) this.A05).A01.execute(new RunnableC09960dn(workDatabase, this, stringExtra));
        } else if (!"ACTION_NOTIFY".equals(action)) {
            if ("ACTION_CANCEL_WORK".equals(action)) {
                C06390Tk.A00().A04(A0A, String.format("Stopping foreground work for %s", intent), new Throwable[0]);
                String stringExtra2 = intent.getStringExtra("KEY_WORKSPEC_ID");
                if (!(stringExtra2 == null || TextUtils.isEmpty(stringExtra2))) {
                    AnonymousClass022 r2 = this.A01;
                    ((C07760a2) r2.A06).A01.execute(new C03170Gl(r2, UUID.fromString(stringExtra2)));
                    return;
                }
                return;
            } else if ("ACTION_STOP_FOREGROUND".equals(action)) {
                C06390Tk.A00().A04(A0A, "Stopping foreground service", new Throwable[0]);
                AbstractC11970hA r0 = this.A02;
                if (r0 != null) {
                    r0.stop();
                    return;
                }
                return;
            } else {
                return;
            }
        }
        int i = 0;
        int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        String stringExtra3 = intent.getStringExtra("KEY_WORKSPEC_ID");
        Notification notification = (Notification) intent.getParcelableExtra("KEY_NOTIFICATION");
        C06390Tk.A00().A02(A0A, String.format("Notifying with (id: %s, workSpecId: %s, notificationType: %s)", Integer.valueOf(intExtra), stringExtra3, Integer.valueOf(intExtra2)), new Throwable[0]);
        if (!(notification == null || this.A02 == null)) {
            C05360Pg r02 = new C05360Pg(intExtra, notification, intExtra2);
            Map map = this.A07;
            map.put(stringExtra3, r02);
            if (TextUtils.isEmpty(this.A03)) {
                this.A03 = stringExtra3;
                SystemForegroundService systemForegroundService = (SystemForegroundService) this.A02;
                handler = systemForegroundService.A01;
                r4 = new RunnableC10060dx(notification, systemForegroundService, intExtra, intExtra2);
            } else {
                SystemForegroundService systemForegroundService2 = (SystemForegroundService) this.A02;
                systemForegroundService2.A01.post(new RunnableC09970do(notification, systemForegroundService2, intExtra));
                if (intExtra2 != 0 && Build.VERSION.SDK_INT >= 29) {
                    for (Map.Entry entry : map.entrySet()) {
                        i |= ((C05360Pg) entry.getValue()).A00;
                    }
                    C05360Pg r03 = (C05360Pg) map.get(this.A03);
                    if (r03 != null) {
                        AbstractC11970hA r22 = this.A02;
                        int i2 = r03.A01;
                        Notification notification2 = r03.A02;
                        SystemForegroundService systemForegroundService3 = (SystemForegroundService) r22;
                        handler = systemForegroundService3.A01;
                        r4 = new RunnableC10060dx(notification2, systemForegroundService3, i2, i);
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            handler.post(r4);
        }
    }

    @Override // X.AbstractC12450hw
    public void AM9(List list) {
        if (!list.isEmpty()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                C06390Tk.A00().A02(A0A, String.format("Constraints unmet for WorkSpec %s", str), new Throwable[0]);
                AnonymousClass022 r3 = this.A01;
                ((C07760a2) r3.A06).A01.execute(new RunnableC10210eD(r3, str, true));
            }
        }
    }

    @Override // X.AbstractC11960h9
    public void AQ1(String str, boolean z) {
        Map.Entry entry;
        synchronized (this.A06) {
            C004401z r0 = (C004401z) this.A08.remove(str);
            if (r0 != null) {
                Set set = this.A09;
                if (set.remove(r0)) {
                    this.A04.A01(set);
                }
            }
        }
        Map map = this.A07;
        C05360Pg r9 = (C05360Pg) map.remove(str);
        if (str.equals(this.A03) && map.size() > 0) {
            Iterator it = map.entrySet().iterator();
            do {
                entry = (Map.Entry) it.next();
            } while (it.hasNext());
            this.A03 = (String) entry.getKey();
            if (this.A02 != null) {
                C05360Pg r02 = (C05360Pg) entry.getValue();
                AbstractC11970hA r5 = this.A02;
                int i = r02.A01;
                SystemForegroundService systemForegroundService = (SystemForegroundService) r5;
                systemForegroundService.A01.post(new RunnableC10060dx(r02.A02, systemForegroundService, i, r02.A00));
                SystemForegroundService systemForegroundService2 = (SystemForegroundService) this.A02;
                systemForegroundService2.A01.post(new RunnableC09650dI(systemForegroundService2, i));
            }
        }
        AbstractC11970hA r52 = this.A02;
        if (r9 != null && r52 != null) {
            C06390Tk A00 = C06390Tk.A00();
            String str2 = A0A;
            int i2 = r9.A01;
            A00.A02(str2, String.format("Removing Notification (id: %s, workSpecId: %s ,notificationType: %s)", Integer.valueOf(i2), str, Integer.valueOf(r9.A00)), new Throwable[0]);
            SystemForegroundService systemForegroundService3 = (SystemForegroundService) r52;
            systemForegroundService3.A01.post(new RunnableC09650dI(systemForegroundService3, i2));
        }
    }
}
