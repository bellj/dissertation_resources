package X;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0Z5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Z5 implements AbstractC11860gz {
    public int A00 = 0;
    public AbstractC12350hm A01 = new C07420Xx(30);
    public final AbstractC12550i6 A02;
    public final AnonymousClass0MW A03;
    public final ArrayList A04 = new ArrayList();
    public final ArrayList A05 = new ArrayList();

    public AnonymousClass0Z5(AbstractC12550i6 r3) {
        this.A02 = r3;
        this.A03 = new AnonymousClass0MW(this);
    }

    public int A00(int i, int i2) {
        ArrayList arrayList = this.A05;
        int size = arrayList.size();
        while (i2 < size) {
            C05390Pj r3 = (C05390Pj) arrayList.get(i2);
            int i3 = r3.A00;
            if (i3 == 8) {
                int i4 = r3.A02;
                if (i4 == i) {
                    i = r3.A01;
                } else {
                    if (i4 < i) {
                        i--;
                    }
                    if (r3.A01 <= i) {
                        i++;
                    }
                }
            } else {
                int i5 = r3.A02;
                if (i5 > i) {
                    continue;
                } else if (i3 == 2) {
                    int i6 = r3.A01;
                    i -= i6;
                    if (i < i5 + i6) {
                        return -1;
                    }
                } else if (i3 == 1) {
                    i += r3.A01;
                }
            }
            i2++;
        }
        return i;
    }

    public final int A01(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        ArrayList arrayList = this.A05;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            C05390Pj r6 = (C05390Pj) arrayList.get(size);
            int i7 = r6.A00;
            int i8 = r6.A02;
            if (i7 == 8) {
                int i9 = r6.A01;
                int i10 = i8;
                int i11 = i9;
                if (i8 < i9) {
                    i11 = i8;
                    i10 = i9;
                }
                if (i < i11 || i > i10) {
                    if (i < i8) {
                        if (i2 == 1) {
                            r6.A02 = i8 + 1;
                            i4 = i9 + 1;
                        } else if (i2 == 2) {
                            r6.A02 = i8 - 1;
                            i4 = i9 - 1;
                        }
                        r6.A01 = i4;
                    }
                } else if (i11 == i8) {
                    if (i2 == 1) {
                        i6 = i9 + 1;
                    } else {
                        if (i2 == 2) {
                            i6 = i9 - 1;
                        }
                        i++;
                    }
                    r6.A01 = i6;
                    i++;
                } else {
                    if (i2 == 1) {
                        i5 = i8 + 1;
                    } else {
                        if (i2 == 2) {
                            i5 = i8 - 1;
                        }
                        i--;
                    }
                    r6.A02 = i5;
                    i--;
                }
            } else if (i8 > i) {
                if (i2 == 1) {
                    i3 = i8 + 1;
                } else if (i2 == 2) {
                    i3 = i8 - 1;
                }
                r6.A02 = i3;
            } else if (i7 == 1) {
                i -= r6.A01;
            } else if (i7 == 2) {
                i += r6.A01;
            }
        }
        for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
            C05390Pj r2 = (C05390Pj) arrayList.get(size2);
            int i12 = r2.A00;
            int i13 = r2.A01;
            if (i12 == 8) {
                if (i13 != r2.A02 && i13 >= 0) {
                }
                arrayList.remove(size2);
                r2.A03 = null;
                this.A01.Aa6(r2);
            } else {
                if (i13 > 0) {
                }
                arrayList.remove(size2);
                r2.A03 = null;
                this.A01.Aa6(r2);
            }
        }
        return i;
    }

    public void A02() {
        ArrayList arrayList = this.A05;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass0Yz) this.A02).A00((C05390Pj) arrayList.get(i));
        }
        A08(arrayList);
        this.A00 = 0;
    }

    public void A03() {
        A02();
        ArrayList arrayList = this.A04;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            C05390Pj r8 = (C05390Pj) arrayList.get(i);
            int i2 = r8.A00;
            if (i2 == 1) {
                AbstractC12550i6 r2 = this.A02;
                ((AnonymousClass0Yz) r2).A00(r8);
                r2.ALl(r8.A02, r8.A01);
            } else if (i2 == 2) {
                AnonymousClass0Yz r0 = (AnonymousClass0Yz) this.A02;
                r0.A00(r8);
                int i3 = r8.A02;
                int i4 = r8.A01;
                RecyclerView recyclerView = r0.A00;
                recyclerView.A0e(i3, i4, true);
                recyclerView.A0k = true;
                recyclerView.A0y.A00 += i4;
            } else if (i2 == 4) {
                AbstractC12550i6 r3 = this.A02;
                ((AnonymousClass0Yz) r3).A00(r8);
                r3.AKs(r8.A03, r8.A02, r8.A01);
            } else if (i2 == 8) {
                AbstractC12550i6 r22 = this.A02;
                ((AnonymousClass0Yz) r22).A00(r8);
                r22.ALm(r8.A02, r8.A01);
            }
        }
        A08(arrayList);
        this.A00 = 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0241, code lost:
        if (r1.A0K.A02.contains(r2.A0H) != false) goto L_0x0243;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0281, code lost:
        if (r9 == 0) goto L_0x0298;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0296, code lost:
        if (r3 == 0) goto L_0x0298;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0298, code lost:
        A05(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a7, code lost:
        if (r4.A01 != (r2 - r13)) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x010c, code lost:
        if (r4.A01 != (r13 - r2)) goto L_0x010e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0110, code lost:
        r14 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x013b, code lost:
        if (r1 > r4.A02) goto L_0x013d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x013d, code lost:
        r5.A01 = r1 - r4.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0142, code lost:
        r7.set(r8, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0149, code lost:
        if (r5.A02 == r5.A01) goto L_0x0155;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x014b, code lost:
        r7.set(r6, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x014e, code lost:
        if (r2 == null) goto L_0x0004;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0150, code lost:
        r7.add(r8, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0155, code lost:
        r7.remove(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0180, code lost:
        if (r1 >= r4.A02) goto L_0x013d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x005a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0112 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x00ac A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x00b9 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0004 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
        // Method dump skipped, instructions count: 678
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Z5.A04():void");
    }

    public final void A05(C05390Pj r12) {
        int i;
        int i2 = r12.A00;
        if (i2 == 1 || i2 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int A01 = A01(r12.A02, i2);
        int i3 = r12.A02;
        int i4 = r12.A00;
        if (i4 == 2) {
            i = 0;
        } else if (i4 == 4) {
            i = 1;
        } else {
            StringBuilder sb = new StringBuilder("op should be remove or update.");
            sb.append(r12);
            throw new IllegalArgumentException(sb.toString());
        }
        int i5 = 1;
        for (int i6 = 1; i6 < r12.A01; i6++) {
            int A012 = A01(r12.A02 + (i * i6), i4);
            i4 = r12.A00;
            if (i4 == 2 ? A012 != A01 : !(i4 == 4 && A012 == A01 + 1)) {
                C05390Pj ALk = ALk(r12.A03, i4, A01, i5);
                A07(ALk, i3);
                ALk.A03 = null;
                this.A01.Aa6(ALk);
                i4 = r12.A00;
                if (i4 == 4) {
                    i3 += i5;
                }
                A01 = A012;
                i5 = 1;
            } else {
                i5++;
            }
        }
        Object obj = r12.A03;
        r12.A03 = null;
        AbstractC12350hm r2 = this.A01;
        r2.Aa6(r12);
        if (i5 > 0) {
            C05390Pj ALk2 = ALk(obj, r12.A00, A01, i5);
            A07(ALk2, i3);
            ALk2.A03 = null;
            r2.Aa6(ALk2);
        }
    }

    public final void A06(C05390Pj r5) {
        this.A05.add(r5);
        int i = r5.A00;
        if (i == 1) {
            this.A02.ALl(r5.A02, r5.A01);
        } else if (i == 2) {
            AbstractC12550i6 r0 = this.A02;
            int i2 = r5.A02;
            int i3 = r5.A01;
            RecyclerView recyclerView = ((AnonymousClass0Yz) r0).A00;
            recyclerView.A0e(i2, i3, false);
            recyclerView.A0k = true;
        } else if (i == 4) {
            this.A02.AKs(r5.A03, r5.A02, r5.A01);
        } else if (i == 8) {
            this.A02.ALm(r5.A02, r5.A01);
        } else {
            StringBuilder sb = new StringBuilder("Unknown update op type for ");
            sb.append(r5);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public void A07(C05390Pj r5, int i) {
        AbstractC12550i6 r2 = this.A02;
        AnonymousClass0Yz r3 = (AnonymousClass0Yz) r2;
        r3.A00(r5);
        int i2 = r5.A00;
        if (i2 == 2) {
            int i3 = r5.A01;
            RecyclerView recyclerView = r3.A00;
            recyclerView.A0e(i, i3, true);
            recyclerView.A0k = true;
            recyclerView.A0y.A00 += i3;
        } else if (i2 == 4) {
            r2.AKs(r5.A03, i, r5.A01);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    public void A08(List list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            C05390Pj r1 = (C05390Pj) list.get(i);
            r1.A03 = null;
            this.A01.Aa6(r1);
        }
        list.clear();
    }

    public final boolean A09(int i) {
        ArrayList arrayList = this.A05;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            C05390Pj r4 = (C05390Pj) arrayList.get(i2);
            int i3 = r4.A00;
            if (i3 != 8) {
                if (i3 == 1) {
                    int i4 = r4.A02;
                    int i5 = r4.A01 + i4;
                    while (i4 < i5) {
                        if (A00(i4, i2 + 1) == i) {
                            return true;
                        }
                        i4++;
                    }
                    continue;
                } else {
                    continue;
                }
            } else if (A00(r4.A01, i2 + 1) == i) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC11860gz
    public C05390Pj ALk(Object obj, int i, int i2, int i3) {
        C05390Pj r0 = (C05390Pj) this.A01.A5a();
        if (r0 == null) {
            return new C05390Pj(obj, i, i2, i3);
        }
        r0.A00 = i;
        r0.A02 = i2;
        r0.A01 = i3;
        r0.A03 = obj;
        return r0;
    }
}
