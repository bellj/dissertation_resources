package X;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Ds  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26501Ds {
    public final C22680zT A00;
    public final C14900mE A01;
    public final C14650lo A02;
    public final C15550nR A03;
    public final AnonymousClass01d A04;
    public final C16590pI A05;
    public final AnonymousClass018 A06;
    public final C15690nk A07;

    public C26501Ds(C22680zT r1, C14900mE r2, C14650lo r3, C15550nR r4, AnonymousClass01d r5, C16590pI r6, AnonymousClass018 r7, C15690nk r8) {
        this.A05 = r6;
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
        this.A06 = r7;
        this.A00 = r1;
        this.A07 = r8;
        this.A02 = r3;
    }

    public C41431tW A00(String str) {
        C41391tS r1 = new C41391tS();
        C41401tT r0 = new C41401tT();
        try {
            r1.A01(str, r0);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            List<C41411tU> list = r0.A04;
            if (list.size() <= 257) {
                StringBuilder sb = new StringBuilder("contactpicker/contact array separation (size: ");
                sb.append(list.size());
                sb.append(")");
                C28181Ma r7 = new C28181Ma(sb.toString());
                for (C41411tU r2 : list) {
                    C16590pI r12 = this.A05;
                    C15550nR r6 = this.A03;
                    AnonymousClass018 r5 = this.A06;
                    C30721Yo A06 = C30721Yo.A06(this.A02, r6, r12, r5, r2);
                    if (A06 != null) {
                        C41421tV r02 = new C41421tV(this.A00, r5);
                        try {
                            C41421tV.A00(r6, A06);
                            C30731Yp r03 = new C30731Yp(r02.A01(A06), A06);
                            arrayList2.add(r03);
                            arrayList.add(r03.A00);
                        } catch (C41341tN e) {
                            Log.e(new C41351tO(e));
                            throw new C41381tR();
                        }
                    }
                }
                r7.A01();
                String str2 = null;
                if (arrayList2.size() == 1) {
                    str2 = ((C30731Yp) arrayList2.get(0)).A01.A08();
                }
                return new C41431tW(str2, arrayList, arrayList2);
            }
            StringBuilder sb2 = new StringBuilder("Too many vCards for a contact array message: ");
            sb2.append(list.size());
            Log.w(sb2.toString());
            throw new C41371tQ();
        } catch (C41341tN unused) {
            throw new C41361tP();
        }
    }

    public String A01(Uri uri) {
        ContentResolver A0C = this.A04.A0C();
        if (A0C != null) {
            C15690nk r1 = this.A07;
            r1.A01(uri);
            try {
                AssetFileDescriptor openAssetFileDescriptor = A0C.openAssetFileDescriptor(uri, "r");
                if (openAssetFileDescriptor != null) {
                    r1.A02(openAssetFileDescriptor.getParcelFileDescriptor());
                    FileInputStream createInputStream = openAssetFileDescriptor.createInputStream();
                    try {
                        C37601mh r12 = new C37601mh(createInputStream, 10000000);
                        String A00 = AnonymousClass1P1.A00(r12);
                        AnonymousClass009.A05(A00);
                        r12.close();
                        if (createInputStream != null) {
                            createInputStream.close();
                        }
                        openAssetFileDescriptor.close();
                        return A00;
                    } catch (Throwable th) {
                        if (createInputStream != null) {
                            try {
                                createInputStream.close();
                            } catch (Throwable unused) {
                            }
                        }
                        throw th;
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable to open uri=");
                    sb.append(uri);
                    throw new IOException(sb.toString());
                }
            } catch (SecurityException e) {
                throw new IOException(e);
            }
        } else {
            throw new IOException("Unable to open uri; cr=null");
        }
    }

    public void A02(C41341tN r9) {
        C14900mE r1;
        int i;
        Log.e("vcardloader/exception", new C41351tO(r9));
        if (r9 instanceof C41361tP) {
            r1 = this.A01;
            i = R.string.vcard_format_unsupport;
        } else if (r9 instanceof C41371tQ) {
            this.A01.A0E(this.A06.A0I(new Object[]{257}, R.plurals.contact_array_message_reach_limit, 257), 0);
            return;
        } else if (r9 instanceof C41381tR) {
            r1 = this.A01;
            i = R.string.must_have_displayname;
        } else {
            return;
        }
        r1.A07(i, 0);
    }
}
