package X;

import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.jid.UserJid;
import java.math.BigDecimal;

/* renamed from: X.1XF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XF extends AbstractC16130oV implements AbstractC16400ox, AbstractC16420oz {
    public int A00;
    public int A01;
    public int A02;
    public UserJid A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public BigDecimal A09;
    public final transient AbstractC15710nm A0A;

    public AnonymousClass1XF(AbstractC15710nm r10, C16150oX r11, AnonymousClass1IS r12, AnonymousClass1XF r13, long j) {
        super(r11, r12, r13, r13.A0y, j, true);
        this.A0A = r10;
        this.A06 = r13.A06;
        this.A07 = r13.A07;
        this.A00 = r13.A00;
        this.A01 = r13.A01;
        this.A02 = r13.A02;
        this.A05 = r13.A05;
        this.A03 = r13.A03;
        this.A08 = r13.A08;
        this.A04 = r13.A04;
        this.A09 = r13.A09;
    }

    public AnonymousClass1XF(AbstractC15710nm r2, AnonymousClass1IS r3, long j) {
        super(r3, (byte) 44, j);
        this.A0A = r2;
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r12) {
        AnonymousClass1G3 r4 = r12.A03;
        C57682nS r0 = ((C27081Fy) r4.A00).A0R;
        if (r0 == null) {
            r0 = C57682nS.A0D;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        String str = this.A06;
        if (str != null) {
            A0T.A03();
            C57682nS r1 = (C57682nS) A0T.A00;
            r1.A00 |= 1;
            r1.A08 = str;
        }
        String str2 = this.A07;
        if (str2 != null) {
            A0T.A03();
            C57682nS r13 = (C57682nS) A0T.A00;
            r13.A00 |= 64;
            r13.A09 = str2;
        }
        int i = this.A00;
        A0T.A03();
        C57682nS r14 = (C57682nS) A0T.A00;
        r14.A00 |= 4;
        r14.A01 = i;
        String str3 = this.A05;
        if (str3 != null) {
            A0T.A03();
            C57682nS r15 = (C57682nS) A0T.A00;
            r15.A00 |= 32;
            r15.A07 = str3;
        }
        int i2 = this.A01;
        if (i2 != 1) {
            AbstractC15710nm r2 = this.A0A;
            StringBuilder sb = new StringBuilder("status=");
            sb.append(i2);
            r2.AaV("FMessageOrder/setOrderStatus: Unexpected status", sb.toString(), true);
        } else {
            EnumC87154Ak r22 = EnumC87154Ak.A01;
            A0T.A03();
            C57682nS r16 = (C57682nS) A0T.A00;
            r16.A00 |= 8;
            r16.A02 = r22.value;
        }
        int i3 = this.A02;
        if (i3 != 1) {
            AbstractC15710nm r23 = this.A0A;
            StringBuilder sb2 = new StringBuilder("surface=");
            sb2.append(i3);
            r23.AaV("FMessageOrder/setOrderSurface: Unexpected surface", sb2.toString(), true);
        } else {
            EnumC87164Al r24 = EnumC87164Al.A01;
            A0T.A03();
            C57682nS r17 = (C57682nS) A0T.A00;
            r17.A00 |= 16;
            r17.A03 = r24.value;
        }
        UserJid userJid = this.A03;
        if (userJid != null) {
            String rawString = userJid.getRawString();
            A0T.A03();
            C57682nS r18 = (C57682nS) A0T.A00;
            r18.A00 |= 128;
            r18.A0A = rawString;
        }
        String str4 = this.A08;
        if (str4 != null) {
            A0T.A03();
            C57682nS r19 = (C57682nS) A0T.A00;
            r19.A00 |= 256;
            r19.A0B = str4;
        }
        if (!TextUtils.isEmpty(this.A04) && this.A09 != null) {
            String str5 = this.A04;
            A0T.A03();
            C57682nS r110 = (C57682nS) A0T.A00;
            r110.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
            r110.A0C = str5;
            long longValue = this.A09.multiply(C30701Ym.A00).longValue();
            A0T.A03();
            C57682nS r5 = (C57682nS) A0T.A00;
            r5.A00 |= 512;
            r5.A04 = longValue;
        }
        C16460p3 A0G = A0G();
        if (!(A0G == null || A0G.A07() == null)) {
            byte[] A07 = A0G.A07();
            AbstractC27881Jp A01 = AbstractC27881Jp.A01(A07, 0, A07.length);
            A0T.A03();
            C57682nS r111 = (C57682nS) A0T.A00;
            r111.A00 |= 2;
            r111.A05 = A01;
        }
        AnonymousClass1PG r7 = r12.A04;
        byte[] bArr = r12.A09;
        if (C32411c7.A0U(r7, this, bArr)) {
            C43261wh A0P = C32411c7.A0P(r12.A00, r12.A02, r7, this, bArr, r12.A06);
            A0T.A03();
            C57682nS r112 = (C57682nS) A0T.A00;
            r112.A06 = A0P;
            r112.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        }
        r4.A03();
        C27081Fy r25 = (C27081Fy) r4.A00;
        r25.A0R = (C57682nS) A0T.A02();
        r25.A00 |= 536870912;
    }

    @Override // X.AbstractC16400ox
    public AbstractC15340mz A7M(AnonymousClass1IS r8) {
        return new AnonymousClass1XF(this.A0A, ((AbstractC16130oV) this).A02, r8, this, this.A0I);
    }
}
