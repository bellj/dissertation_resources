package X;

import android.widget.TextView;

/* renamed from: X.0R1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0R1 {
    public static void A00(TextView textView, int i) {
        textView.setAutoSizeTextTypeWithDefaults(i);
    }

    public static void A01(TextView textView, int i, int i2, int i3, int i4) {
        textView.setAutoSizeTextTypeUniformWithConfiguration(i, i2, i3, i4);
    }
}
