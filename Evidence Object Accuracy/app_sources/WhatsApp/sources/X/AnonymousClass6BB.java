package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;

/* renamed from: X.6BB  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6BB implements AbstractC136446Mo {
    public final AbstractC30791Yv A00;
    public final AbstractC30791Yv A01;
    public final AnonymousClass63Y A02;
    public final AnonymousClass63Y A03;
    public final boolean A04;

    public AnonymousClass6BB(AnonymousClass63Y r3, AnonymousClass63Y r4) {
        this.A03 = r3;
        this.A02 = r4;
        AbstractC30791Yv r0 = r3.A00;
        this.A01 = r0;
        AbstractC30791Yv r1 = r4.A00;
        this.A00 = r1;
        this.A04 = C117305Zk.A1V(r1, ((AbstractC30781Yu) r0).A04);
    }

    @Override // X.AbstractC136446Mo
    public AnonymousClass6F2 A9k(AnonymousClass6F2 r7, C1315863i r8, int i) {
        AnonymousClass63Y r4;
        AnonymousClass6F2 r3;
        C1316563p r0;
        BigDecimal bigDecimal;
        AbstractC30791Yv r02;
        C127695uu r32;
        boolean A1V = C117305Zk.A1V(this.A00, ((AbstractC30781Yu) r7.A00).A04);
        if (!this.A04) {
            if (A1V) {
                if (i == 1) {
                    i = 4;
                } else if (i == 0) {
                    return r7;
                }
                if (i == 4) {
                    r32 = new C127695uu(this.A03.A02, r8.A05.A05, 0, true);
                } else if (i == 2) {
                    BigDecimal bigDecimal2 = r8.A05.A05;
                    r4 = this.A03;
                    r3 = C127695uu.A00(r4.A02, r7, bigDecimal2, false);
                    r0 = r8.A03;
                } else if (i == 5) {
                    return r7;
                } else {
                    r32 = new C127695uu(this.A02.A01, r8.A02.A05, 1, true);
                }
                return r7.A05(r32);
            }
            if (!(i == 1 || i == 0)) {
                if (i == 4) {
                    return r7;
                }
                if (i == 2) {
                    bigDecimal = r8.A03.A05;
                    r02 = this.A03.A01;
                    return C127695uu.A00(r02, r7, bigDecimal, true);
                } else if (i != 5) {
                    BigDecimal bigDecimal3 = r8.A05.A05;
                    r4 = this.A02;
                    r3 = C127695uu.A01(r7, r4, bigDecimal3, 1, false);
                    r0 = r8.A02;
                }
            }
            bigDecimal = r8.A05.A05;
            r02 = this.A02.A02;
            return C127695uu.A00(r02, r7, bigDecimal, true);
            return C127695uu.A00(r4.A01, r3, r0.A05, true);
        } else if (i == 1 || i == 0 || i == 4 || i == 5) {
            return r7;
        } else {
            return C127695uu.A00(this.A03.A01, r7, r8.A03.A05, true);
        }
    }

    @Override // X.AbstractC136446Mo
    public CharSequence AHI(Context context, AnonymousClass018 r10, C1315863i r11) {
        boolean z = this.A04;
        AnonymousClass63Y r0 = this.A03;
        if (z) {
            return C130275z5.A00(context, r10, r0, r11);
        }
        AbstractC30791Yv r5 = r0.A02;
        AbstractC30791Yv r7 = this.A02.A02;
        Object[] objArr = new Object[2];
        int i = 0;
        objArr[0] = r5.AAB(r10, BigDecimal.ONE, 2);
        BigDecimal bigDecimal = r11.A05.A05;
        if (!BigDecimal.ONE.equals(bigDecimal)) {
            i = 4;
        }
        return r5.AA7(context, C12960it.A0X(context, C117305Zk.A0k(r10, r7, bigDecimal, i), objArr, 1, R.string.novi_send_money_review_transaction_exchange_rate));
    }

    @Override // X.AbstractC136446Mo
    public boolean AKE(C1315863i r4) {
        String str = ((AbstractC30781Yu) this.A01).A04;
        C1316563p r2 = r4.A05;
        return str.equals(r2.A03) && ((AbstractC30781Yu) this.A00).A04.equals(r2.A04);
    }
}
