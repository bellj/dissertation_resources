package X;

import android.content.Context;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.3C1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C1 {
    public final Context A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C15820nx A03;
    public final C22730zY A04;
    public final C26551Dx A05;
    public final AnonymousClass10N A06;
    public final AbstractC44761zV A07;
    public final C44791zY A08;
    public final C15810nw A09;
    public final C15890o4 A0A;
    public final C14820m6 A0B;
    public final C16120oU A0C;
    public final C44941zo A0D;
    public final C26981Fo A0E;
    public final String A0F;
    public final AtomicLong A0G = new AtomicLong(0);
    public final AtomicLong A0H;
    public final AtomicLong A0I;

    public AnonymousClass3C1(Context context, AbstractC15710nm r5, C14330lG r6, C15820nx r7, C22730zY r8, C26551Dx r9, AnonymousClass10N r10, AbstractC44761zV r11, C44791zY r12, C15810nw r13, C15890o4 r14, C14820m6 r15, C16120oU r16, C44941zo r17, C26981Fo r18, String str, AtomicLong atomicLong, AtomicLong atomicLong2) {
        this.A01 = r5;
        this.A02 = r6;
        this.A0C = r16;
        this.A09 = r13;
        this.A08 = r12;
        this.A03 = r7;
        this.A0E = r18;
        this.A05 = r9;
        this.A0A = r14;
        this.A0B = r15;
        this.A0F = str;
        this.A06 = r10;
        this.A00 = context;
        this.A0H = atomicLong2;
        this.A0I = atomicLong;
        this.A07 = r11;
        this.A04 = r8;
        this.A0D = r17;
    }
}
