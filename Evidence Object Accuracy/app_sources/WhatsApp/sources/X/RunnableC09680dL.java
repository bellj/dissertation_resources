package X;

import androidx.work.ListenableWorker;

/* renamed from: X.0dL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09680dL implements Runnable {
    public final /* synthetic */ RunnableC10230eF A00;
    public final /* synthetic */ AnonymousClass040 A01;

    public RunnableC09680dL(RunnableC10230eF r1, AnonymousClass040 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            C05360Pg r5 = (C05360Pg) this.A01.get();
            if (r5 != null) {
                C06390Tk A00 = C06390Tk.A00();
                String str = RunnableC10230eF.A06;
                RunnableC10230eF r4 = this.A00;
                A00.A02(str, String.format("Updating notification for %s", r4.A03.A0G), new Throwable[0]);
                ListenableWorker listenableWorker = r4.A02;
                listenableWorker.A02 = true;
                r4.A04.A08(r4.A01.AcA(r4.A00, r5, listenableWorker.A01.A08));
                return;
            }
            throw new IllegalStateException(String.format("Worker was marked important (%s) but did not provide ForegroundInfo", this.A00.A03.A0G));
        } catch (Throwable th) {
            this.A00.A04.A0A(th);
        }
    }
}
