package X;

/* renamed from: X.4xT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C107524xT implements AbstractC117025Xv {
    @Override // X.AbstractC117025Xv
    public AnonymousClass5SN A8E(C100614mC r3) {
        String str = r3.A0T;
        if (str != null) {
            switch (str.hashCode()) {
                case -1354451219:
                    if (str.equals("application/vnd.dvb.ait")) {
                        return new C76953mV();
                    }
                    break;
                case -1348231605:
                    if (str.equals("application/x-icy")) {
                        return new C76983mY();
                    }
                    break;
                case -1248341703:
                    if (str.equals("application/id3")) {
                        return new C76993mZ(null);
                    }
                    break;
                case 1154383568:
                    if (str.equals("application/x-emsg")) {
                        return new C76963mW();
                    }
                    break;
                case 1652648887:
                    if (str.equals("application/x-scte35")) {
                        return new C76973mX();
                    }
                    break;
            }
        }
        throw C12970iu.A0f(C12960it.A0d(str, C12960it.A0k("Attempted to create decoder for unsupported MIME type: ")));
    }

    @Override // X.AbstractC117025Xv
    public boolean Aec(C100614mC r3) {
        String str = r3.A0T;
        return "application/id3".equals(str) || "application/x-emsg".equals(str) || "application/x-scte35".equals(str) || "application/x-icy".equals(str) || "application/vnd.dvb.ait".equals(str);
    }
}
