package X;

/* renamed from: X.3F1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3F1 {
    public final C71183cW A00;

    public AnonymousClass3F1(C71183cW r1) {
        this.A00 = r1;
    }

    public final float A00(int i, int i2, int i3, int i4) {
        double d;
        int i5 = i;
        int i6 = i3;
        boolean z = true;
        int i7 = i2;
        int i8 = i4;
        if (C12980iv.A05(i4, i2) <= C12980iv.A05(i6, i5)) {
            z = false;
            i7 = i5;
            i5 = i2;
            i8 = i6;
            i6 = i4;
        }
        int A05 = C12980iv.A05(i8, i7);
        int i9 = i6 - i5;
        int abs = Math.abs(i9);
        int i10 = (-A05) / 2;
        int i11 = -1;
        int i12 = -1;
        if (i7 < i8) {
            i12 = 1;
        }
        if (i5 < i6) {
            i11 = 1;
        }
        int i13 = i8 + i12;
        int i14 = i5;
        int i15 = 0;
        for (int i16 = i7; i16 != i13; i16 += i12) {
            int i17 = i16;
            int i18 = i14;
            if (z) {
                i17 = i14;
                i18 = i16;
            }
            if (C12960it.A1V(i15, 1) == this.A00.A03(i17, i18)) {
                if (i15 == 2) {
                    int i19 = i16 - i7;
                    int i20 = i14 - i5;
                    d = (double) ((i19 * i19) + (i20 * i20));
                    break;
                }
                i15++;
            }
            i10 += abs;
            if (i10 > 0) {
                if (i14 == i6) {
                    break;
                }
                i14 += i11;
                i10 -= A05;
            }
        }
        if (i15 != 2) {
            return Float.NaN;
        }
        int i21 = i13 - i7;
        d = (double) ((i21 * i21) + (i9 * i9));
        return (float) Math.sqrt(d);
    }

    public final float A01(int i, int i2, int i3, int i4) {
        float f;
        float f2;
        float f3;
        float A00 = A00(i, i2, i3, i4);
        int i5 = i - (i3 - i);
        int i6 = 0;
        if (i5 < 0) {
            f = ((float) i) / ((float) (i - i5));
            i5 = 0;
        } else {
            int i7 = this.A00.A02;
            f = 1.0f;
            if (i5 >= i7) {
                int i8 = i7 - 1;
                f = ((float) (i8 - i)) / ((float) (i5 - i));
                i5 = i8;
            }
        }
        float f4 = (float) i2;
        int i9 = (int) (f4 - (((float) (i4 - i2)) * f));
        if (i9 < 0) {
            f3 = (float) (i2 - i9);
        } else {
            int i10 = this.A00.A00;
            i6 = i9;
            f2 = 1.0f;
            if (i9 >= i10) {
                i6 = i10 - 1;
                f4 = (float) (i6 - i2);
                f3 = (float) (i9 - i2);
            }
            return (A00 + A00(i, i2, (int) (((float) i) + (((float) (i5 - i)) * f2)), i6)) - 1.0f;
        }
        f2 = f4 / f3;
        return (A00 + A00(i, i2, (int) (((float) i) + (((float) (i5 - i)) * f2)), i6)) - 1.0f;
    }

    public final float A02(AnonymousClass3HP r6, AnonymousClass3HP r7) {
        int i = (int) r6.A00;
        int i2 = (int) r6.A01;
        int i3 = (int) r7.A00;
        int i4 = (int) r7.A01;
        float A01 = A01(i, i2, i3, i4);
        float A012 = A01(i3, i4, i, i2);
        if (Float.isNaN(A01)) {
            return A012 / 7.0f;
        }
        return Float.isNaN(A012) ? A01 / 7.0f : (A01 + A012) / 14.0f;
    }
}
