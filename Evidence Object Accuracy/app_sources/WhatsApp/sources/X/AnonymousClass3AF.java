package X;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/* renamed from: X.3AF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3AF {
    public static C89084Ip A00(AnonymousClass5X9 r14) {
        String AeX;
        String AeX2;
        String AeX3;
        HashMap hashMap;
        C89084Ip r9 = new C89084Ip();
        AnonymousClass39n AZ7 = r14.AZ7();
        AnonymousClass39n r8 = AnonymousClass39n.A0A;
        if (AZ7 != r8) {
            r14.Ae4();
            return null;
        }
        while (true) {
            AnonymousClass39n ALh = r14.ALh();
            AnonymousClass39n r7 = AnonymousClass39n.A04;
            if (ALh == r7) {
                return r9;
            }
            String AZ6 = r14.AZ6();
            boolean A1X = C12990iw.A1X(C87894Dl.A00(AZ6), 32);
            r14.ALh();
            if (!A1X && "layout".equals(AZ6)) {
                C89074Io r6 = new C89074Io();
                if (r14.AZ7() != r8) {
                    r14.Ae4();
                    r6 = null;
                } else {
                    while (r14.ALh() != r7) {
                        String AZ62 = r14.AZ6();
                        boolean A03 = A03(AZ62);
                        r14.ALh();
                        if (!A03 && "bloks_payload".equals(AZ62)) {
                            C63433Bo r5 = new C63433Bo();
                            if (r14.AZ7() != r8) {
                                r14.Ae4();
                                r5 = null;
                            } else {
                                while (r14.ALh() != r7) {
                                    String AZ63 = r14.AZ6();
                                    boolean A032 = A03(AZ63);
                                    r14.ALh();
                                    if (!A032) {
                                        if ("action".equals(AZ63)) {
                                            r5.A02 = AnonymousClass3G8.A00(r14.AZ8());
                                        } else if ("tree".equals(AZ63)) {
                                            r5.A00 = (AnonymousClass28D) AnonymousClass3A8.A00(r14);
                                        } else {
                                            ArrayList arrayList = null;
                                            ArrayList arrayList2 = null;
                                            ArrayList arrayList3 = null;
                                            ArrayList arrayList4 = null;
                                            ArrayList arrayList5 = null;
                                            ArrayList arrayList6 = null;
                                            ArrayList arrayList7 = null;
                                            C89064In r4 = null;
                                            if ("data".equals(AZ63)) {
                                                if (r14.AZ7() == AnonymousClass39n.A09) {
                                                    arrayList2 = C12960it.A0l();
                                                    while (r14.ALh() != AnonymousClass39n.A02) {
                                                        C93484aF r10 = new C93484aF();
                                                        if (r14.AZ7() != r8) {
                                                            r14.Ae4();
                                                        } else {
                                                            while (r14.ALh() != r7) {
                                                                String AZ64 = r14.AZ6();
                                                                boolean A033 = A03(AZ64);
                                                                r14.ALh();
                                                                if (!A033) {
                                                                    if ("id".equals(AZ64)) {
                                                                        r10.A00 = A01(r14);
                                                                    } else if ("type".equals(AZ64)) {
                                                                        r10.A01 = A01(r14);
                                                                    } else if ("data".equals(AZ64)) {
                                                                        if (r14.AZ7() == r8) {
                                                                            hashMap = C12970iu.A11();
                                                                            while (r14.ALh() != r7) {
                                                                                String AZ65 = r14.AZ6();
                                                                                r14.ALh();
                                                                                hashMap.put(AZ65, AnonymousClass3G7.A00(r14));
                                                                            }
                                                                        } else {
                                                                            hashMap = null;
                                                                        }
                                                                        r10.A02 = hashMap;
                                                                    }
                                                                }
                                                                r14.Ae4();
                                                            }
                                                            arrayList2.add(r10);
                                                        }
                                                    }
                                                }
                                                r5.A04 = arrayList2;
                                            } else if ("embedded_payloads".equals(AZ63)) {
                                                if (r14.AZ7() == AnonymousClass39n.A09) {
                                                    arrayList3 = C12960it.A0l();
                                                    while (r14.ALh() != AnonymousClass39n.A02) {
                                                        C64573Gb r1 = new C64573Gb();
                                                        if (r14.AZ7() != r8) {
                                                            r14.Ae4();
                                                        } else {
                                                            while (r14.ALh() != r7) {
                                                                String AZ66 = r14.AZ6();
                                                                boolean A034 = A03(AZ66);
                                                                r14.ALh();
                                                                if (!A034) {
                                                                    if ("id".equals(AZ66)) {
                                                                        if (r14.AZ8().AJn()) {
                                                                            AeX3 = null;
                                                                        } else {
                                                                            AeX3 = r14.AZ8().AeX();
                                                                        }
                                                                        r1.A01 = AeX3;
                                                                    } else if ("payload".equals(AZ66)) {
                                                                        r1.A00 = A00(r14);
                                                                    }
                                                                }
                                                                r14.Ae4();
                                                            }
                                                            arrayList3.add(r1);
                                                        }
                                                    }
                                                }
                                                r5.A05 = arrayList3;
                                            } else if ("referenced".equals(AZ63)) {
                                                if (r14.AZ7() == AnonymousClass39n.A09) {
                                                    arrayList4 = C12960it.A0l();
                                                    while (r14.ALh() != AnonymousClass39n.A02) {
                                                        A02(r14, arrayList4);
                                                    }
                                                }
                                                r5.A09 = arrayList4;
                                            } else if ("referenced_external".equals(AZ63)) {
                                                if (r14.AZ7() == AnonymousClass39n.A09) {
                                                    arrayList5 = C12960it.A0l();
                                                    while (r14.ALh() != AnonymousClass39n.A02) {
                                                        A02(r14, arrayList5);
                                                    }
                                                }
                                                r5.A08 = arrayList5;
                                            } else if ("referenced_embedded_payloads".equals(AZ63)) {
                                                if (r14.AZ7() == AnonymousClass39n.A09) {
                                                    arrayList6 = C12960it.A0l();
                                                    while (r14.ALh() != AnonymousClass39n.A02) {
                                                        A02(r14, arrayList6);
                                                    }
                                                }
                                                r5.A07 = arrayList6;
                                            } else if ("props".equals(AZ63)) {
                                                if (r14.AZ7() == AnonymousClass39n.A09) {
                                                    arrayList7 = C12960it.A0l();
                                                    while (r14.ALh() != AnonymousClass39n.A02) {
                                                        C93344a1 r2 = new C93344a1();
                                                        if (r14.AZ7() != r8) {
                                                            r14.Ae4();
                                                        } else {
                                                            while (r14.ALh() != r7) {
                                                                String AZ67 = r14.AZ6();
                                                                boolean A035 = A03(AZ67);
                                                                r14.ALh();
                                                                if (!A035) {
                                                                    if ("id".equals(AZ67)) {
                                                                        r2.A00 = A01(r14);
                                                                    } else if ("name".equals(AZ67)) {
                                                                        r2.A01 = A01(r14);
                                                                    }
                                                                }
                                                                r14.Ae4();
                                                            }
                                                            arrayList7.add(r2);
                                                        }
                                                    }
                                                }
                                                r5.A06 = arrayList7;
                                            } else if ("error_attribution".equals(AZ63)) {
                                                C89064In r22 = new C89064In();
                                                if (r14.AZ7() != r8) {
                                                    r14.Ae4();
                                                } else {
                                                    while (r14.ALh() != r7) {
                                                        String AZ68 = r14.AZ6();
                                                        boolean A036 = A03(AZ68);
                                                        r14.ALh();
                                                        if (!A036 && "logging_id".equals(AZ68)) {
                                                            if (r14.AZ8().AJn()) {
                                                                AeX2 = null;
                                                            } else {
                                                                AeX2 = r14.AZ8().AeX();
                                                            }
                                                            r22.A00 = AeX2;
                                                        }
                                                        r14.Ae4();
                                                    }
                                                    r4 = r22;
                                                }
                                                r5.A01 = r4;
                                            } else if ("component_queries".equals(AZ63)) {
                                                AnonymousClass39n AZ72 = r14.AZ7();
                                                AnonymousClass39n r3 = AnonymousClass39n.A09;
                                                if (AZ72 == r3) {
                                                    arrayList = C12960it.A0l();
                                                    while (true) {
                                                        AnonymousClass39n ALh2 = r14.ALh();
                                                        AnonymousClass39n r23 = AnonymousClass39n.A02;
                                                        if (ALh2 == r23) {
                                                            break;
                                                        }
                                                        C90754Pc r12 = new C90754Pc();
                                                        if (r14.AZ7() != r8) {
                                                            r14.Ae4();
                                                        } else {
                                                            while (r14.ALh() != r7) {
                                                                String AZ69 = r14.AZ6();
                                                                boolean A037 = A03(AZ69);
                                                                r14.ALh();
                                                                if (!A037) {
                                                                    HashMap hashMap2 = null;
                                                                    HashSet hashSet = null;
                                                                    if ("id".equals(AZ69) || "app_id".equals(AZ69)) {
                                                                        if (!r14.AZ8().AJn()) {
                                                                            r14.AZ8().AeX();
                                                                        }
                                                                    } else if ("params".equals(AZ69)) {
                                                                        r12.A00 = AnonymousClass3G8.A00(r14.AZ8());
                                                                    } else if ("deps".equals(AZ69)) {
                                                                        if (r14.AZ7() == r3) {
                                                                            hashSet = C12970iu.A12();
                                                                            while (r14.ALh() != r23) {
                                                                                A02(r14, hashSet);
                                                                            }
                                                                        }
                                                                        r12.A02 = hashSet;
                                                                    } else if ("targets".equals(AZ69)) {
                                                                        if (r14.AZ7() == r8) {
                                                                            HashMap A11 = C12970iu.A11();
                                                                            while (r14.ALh() != r7) {
                                                                                String AZ610 = r14.AZ6();
                                                                                r14.ALh();
                                                                                if (r14.AZ7() == AnonymousClass39n.A07) {
                                                                                    A11.put(AZ610, null);
                                                                                } else if (!r14.AZ8().AJn() && (AeX = r14.AZ8().AeX()) != null) {
                                                                                    A11.put(AZ610, AeX);
                                                                                }
                                                                            }
                                                                            hashMap2 = A11;
                                                                        }
                                                                        r12.A01 = hashMap2;
                                                                    } else if ("cache_ttl".equals(AZ69)) {
                                                                        r14.AZ8().AKo();
                                                                    }
                                                                }
                                                                r14.Ae4();
                                                            }
                                                            arrayList.add(r12);
                                                        }
                                                    }
                                                }
                                                r5.A03 = arrayList;
                                            }
                                        }
                                    }
                                    r14.Ae4();
                                }
                            }
                            r6.A00 = r5;
                        }
                        r14.Ae4();
                    }
                }
                r9.A00 = r6;
            }
            r14.Ae4();
        }
    }

    public static String A01(AnonymousClass5X9 r2) {
        if (r2.AZ8().AJn()) {
            return null;
        }
        return r2.AZ8().AeX();
    }

    public static void A02(AnonymousClass5X9 r1, AbstractCollection abstractCollection) {
        String AeX;
        if (!r1.AZ8().AJn() && (AeX = r1.AZ8().AeX()) != null) {
            abstractCollection.add(AeX);
        }
    }

    public static boolean A03(String str) {
        return C87894Dl.A00(str) >= 32;
    }
}
