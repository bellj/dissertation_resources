package X;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

/* renamed from: X.0eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ExecutorC10610eu implements Executor {
    public final Object A00 = new Object();
    public final ArrayDeque A01 = new ArrayDeque();
    public final Executor A02;
    public volatile Runnable A03;

    public ExecutorC10610eu(Executor executor) {
        this.A02 = executor;
    }

    public void A00() {
        synchronized (this.A00) {
            Runnable runnable = (Runnable) this.A01.poll();
            this.A03 = runnable;
            if (runnable != null) {
                this.A02.execute(this.A03);
            }
        }
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        synchronized (this.A00) {
            this.A01.add(new RunnableC09660dJ(this, runnable));
            if (this.A03 == null) {
                A00();
            }
        }
    }
}
