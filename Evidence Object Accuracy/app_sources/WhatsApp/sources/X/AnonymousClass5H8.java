package X;

/* renamed from: X.5H8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5H8 extends RuntimeException {
    public AnonymousClass5H8() {
    }

    public AnonymousClass5H8(String str) {
        super(str);
    }

    public AnonymousClass5H8(String str, Throwable th) {
        super(str, th);
    }

    public AnonymousClass5H8(Throwable th) {
        super(th);
    }
}
