package X;

import android.text.Spannable;
import android.text.style.URLSpan;
import android.util.Pair;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3Av  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63243Av {
    public static void A00(Spannable spannable, C22710zW r6, C17070qD r7) {
        Pattern AFI;
        if (r6.A07() && (AFI = r7.A02().AFI()) != null) {
            Matcher matcher = AFI.matcher(spannable);
            while (matcher.find()) {
                Pair A00 = AnonymousClass3IG.A00(C12960it.A0D(Integer.valueOf(matcher.start()), matcher.end()), AnonymousClass3IG.A01, spannable);
                int A05 = C12960it.A05(A00.first);
                int A052 = C12960it.A05(A00.second);
                spannable.setSpan(new URLSpan(C12970iu.A0s(spannable.subSequence(A05, A052), C12960it.A0k("wapay://pay/"))), A05, A052, 0);
            }
        }
    }
}
