package X;

import com.google.android.material.chip.Chip;
import com.whatsapp.R;

/* renamed from: X.2vl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60022vl extends C60052vo {
    public C60022vl(Chip chip, AnonymousClass2Jw r2) {
        super(chip, r2);
    }

    @Override // X.C60052vo, X.AbstractC75703kH
    public void A08(AnonymousClass4UW r4) {
        super.A08(r4);
        Chip chip = ((C60052vo) this).A00;
        C12970iu.A19(chip.getContext(), chip, R.string.biz_dir_filters_more);
        C12960it.A0z(chip, this, 26);
    }
}
