package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;

/* renamed from: X.0td  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19140td extends AbstractC18500sY implements AbstractC19010tQ {
    public final C18460sU A00;
    public final C20860wR A01;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19140td(C18460sU r3, C20860wR r4, C18480sW r5) {
        super(r5, "message_revoked", 1);
        this.A00 = r3;
        this.A01 = r4;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        long j;
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("media_name");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("media_caption");
        C16310on A02 = this.A05.A02();
        int i = 0;
        long j2 = -1;
        while (cursor.moveToNext()) {
            try {
                ContentValues contentValues = new ContentValues();
                j2 = cursor.getLong(columnIndexOrThrow);
                contentValues.put("message_row_id", Long.valueOf(j2));
                C30021Vq.A04(contentValues, "revoked_key_id", cursor.getString(columnIndexOrThrow2));
                Jid nullable = Jid.getNullable(cursor.getString(columnIndexOrThrow3));
                if (nullable == null) {
                    j = -1;
                } else {
                    j = this.A00.A01(nullable);
                }
                if (j == -1) {
                    contentValues.putNull("admin_jid_row_id");
                } else {
                    contentValues.put("admin_jid_row_id", Long.valueOf(j));
                }
                A02.A03.A03(contentValues, "message_revoked");
                i++;
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(j2, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("message_revoked", null, null);
            C21390xL r1 = this.A06;
            r1.A03("revoked_ready");
            r1.A03("migration_message_revoked_index");
            r1.A03("migration_message_revoked_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i("RevokedMessageStore/resetDatabaseMigration/done");
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
