package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5h9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121035h9 extends AbstractC1316063k {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(24);
    public final C1315063a A00;
    public final String A01;

    public C121035h9(C1316563p r2, AbstractC128515wE r3, C1316363n r4, AnonymousClass1V8 r5, String str, int i) {
        super(r5);
        this.A01 = str;
        this.A00 = new C1315063a(r2, r3, r4, i);
    }

    public /* synthetic */ C121035h9(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        Parcelable A0I = C12990iw.A0I(parcel, C1315063a.class);
        AnonymousClass009.A05(A0I);
        this.A00 = (C1315063a) A0I;
    }

    public C121035h9(String str) {
        super(str);
        AbstractC128515wE r4;
        JSONObject A05 = C13000ix.A05(str);
        this.A01 = A05.optString("parentTransactionId");
        String optString = A05.optString("method");
        int i = C13000ix.A05(optString).getInt("type");
        if (i == 0) {
            JSONObject A052 = C13000ix.A05(optString);
            r4 = new C120935gz(A052.getString("bank-name"), A052.getString("account-number"));
        } else if (i == 1) {
            JSONObject A053 = C13000ix.A05(optString);
            r4 = new C120945h0(new C128505wD(A053.getString("is-prepaid")), new C128505wD(A053.getString("is-debit")), A053.getString("last4"), A053.getInt("network-type"));
        } else {
            throw new JSONException("Missing type attribute");
        }
        AnonymousClass009.A05(r4);
        C1316563p A00 = C1316563p.A00(A05.optString("quote"));
        AnonymousClass009.A05(A00);
        C1316363n A01 = C1316363n.A01(A05.optString("amount"));
        AnonymousClass009.A05(A01);
        this.A00 = new C1315063a(A00, r4, A01, A05.getInt("status"));
    }

    public static C121035h9 A00(AnonymousClass102 r7, AnonymousClass1V8 r8, String str) {
        AbstractC128515wE r4;
        if (r8 == null) {
            return null;
        }
        AnonymousClass1V8 A0E = r8.A0E("bank");
        if (A0E != null) {
            r4 = new C120935gz(A0E.A0H("bank-name"), A0E.A0H("account-number"));
        } else {
            AnonymousClass1V8 A0E2 = r8.A0E("card");
            if (A0E2 != null) {
                r4 = new C120945h0(new C128505wD(A0E2.A0I("is-prepaid", null)), new C128505wD(A0E2.A0I("is-debit", null)), A0E2.A0H("last4"), C30881Ze.A05(A0E2.A0H("network-type")));
            } else {
                throw new AnonymousClass1V9("Unsupported Type");
            }
        }
        return new C121035h9(C1315863i.A00(r7, r8.A0F("quote")), r4, C1316363n.A00(r7, r8.A0F("transaction-amount")), r8, str, C31001Zq.A00(6, r8.A0H("status")));
    }

    @Override // X.AbstractC1316063k
    public void A05(JSONObject jSONObject) {
        JSONObject jSONObject2;
        String str;
        try {
            jSONObject.put("parentTransactionId", this.A01);
            C1315063a r2 = this.A00;
            AbstractC128515wE r5 = r2.A02;
            if (!(r5 instanceof C120945h0)) {
                C120935gz r52 = (C120935gz) r5;
                jSONObject2 = C117295Zj.A0a();
                try {
                    jSONObject2.put("type", ((AbstractC128515wE) r52).A00);
                    jSONObject2.put("bank-name", r52.A01);
                    jSONObject2.put("account-number", r52.A00);
                } catch (JSONException unused) {
                    str = "PAY: NoviMethodInfo/Bank toJson threw exception";
                    Log.e(str);
                    jSONObject.put("method", jSONObject2);
                    jSONObject.put("quote", r2.A01.A02());
                    jSONObject.put("amount", r2.A03.A02());
                    jSONObject.put("status", r2.A00);
                }
            } else {
                C120945h0 r53 = (C120945h0) r5;
                jSONObject2 = C117295Zj.A0a();
                try {
                    jSONObject2.put("type", ((AbstractC128515wE) r53).A00);
                    jSONObject2.put("last4", r53.A03);
                    jSONObject2.put("is-prepaid", r53.A02);
                    jSONObject2.put("is-debit", r53.A01);
                    jSONObject2.put("network-type", r53.A00);
                } catch (JSONException unused2) {
                    str = "PAY: NoviMethodInfo/Card toJson threw exception";
                    Log.e(str);
                    jSONObject.put("method", jSONObject2);
                    jSONObject.put("quote", r2.A01.A02());
                    jSONObject.put("amount", r2.A03.A02());
                    jSONObject.put("status", r2.A00);
                }
            }
            jSONObject.put("method", jSONObject2);
            jSONObject.put("quote", r2.A01.A02());
            jSONObject.put("amount", r2.A03.A02());
            jSONObject.put("status", r2.A00);
        } catch (JSONException unused3) {
            Log.w("PAY:NoviDepositTransaction failed to create the JSON");
        }
    }

    @Override // X.AbstractC1316063k, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeParcelable(this.A00, i);
    }
}
