package X;

import android.util.Base64;
import android.util.Pair;
import com.facebook.redex.IDxAListenerShape0S0300000_3_I1;
import com.facebook.redex.IDxAListenerShape0S1200000_3_I1;
import com.facebook.redex.IDxAListenerShape1S0200000_3_I1;
import com.whatsapp.util.Log;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.61D  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61D {
    public final C20920wX A00;
    public final C14830m7 A01;
    public final C14850m9 A02;
    public final C130155yt A03;
    public final C130125yq A04;
    public final AnonymousClass61C A05;

    public AnonymousClass61D(C20920wX r1, C14830m7 r2, C14850m9 r3, C130155yt r4, C130125yq r5, AnonymousClass61C r6) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = r5;
        this.A05 = r6;
    }

    public static final C1310460z A00(C1316363n r6) {
        C1310460z A02 = C130325zE.A02(r6.A01, "local");
        C1310460z A022 = C130325zE.A02(r6.A02, "trading");
        C1310460z A0B = C117315Zl.A0B("transaction-amount", C12980iv.A0x(Collections.singletonList(new AnonymousClass61S("quote-id", r6.A00))));
        ArrayList arrayList = A0B.A02;
        arrayList.add(A02);
        arrayList.add(A022);
        return A0B;
    }

    public static void A01(AbstractC136196Lo r4, C130155yt r5, C128715wY r6, C1316663q r7, String str, int i) {
        C1310460z A01 = AnonymousClass61S.A01("novi-start-step-up-challenge");
        AnonymousClass61S[] r3 = new AnonymousClass61S[3];
        AnonymousClass61S.A04("type", str, r3);
        r3[1] = AnonymousClass61S.A00("entry_flow", r7.A03);
        C117305Zk.A1K(A01, "step_up", C12960it.A0m(AnonymousClass61S.A00("metadata", r7.A04), r3, 2));
        C130155yt.A02(new IDxAListenerShape0S1200000_3_I1(r4, r6, str, 0), r5, A01, i);
    }

    public void A02(Pair pair, AbstractC136196Lo r13, AnonymousClass61E r14, String str) {
        String encodeToString = Base64.encodeToString(((PublicKey) pair.second).getEncoded(), 2);
        String encodeToString2 = Base64.encodeToString(AnonymousClass61L.A03(((PublicKey) pair.second).getEncoded()), 2);
        String str2 = AnonymousClass600.A03;
        C130155yt r2 = this.A03;
        String A05 = r2.A05();
        long A00 = this.A01.A00();
        JSONObject A0a = C117295Zj.A0a();
        try {
            C117295Zj.A1L(str2, A05, A0a, A00);
            A0a.put("signing_key_registration", C117295Zj.A0a().put("key_id", encodeToString2).put("key_type", "ECDSA_SECP256R1").put("pub_key_b64", encodeToString));
            A0a.put("account_id", str);
        } catch (JSONException unused) {
            Log.e("PAY: IntentPayloadHelper/getSigningKeyRegistrationIntentPayload/toJson can't construct json");
        }
        C130125yq r5 = this.A04;
        C129585xx r4 = new C129585xx(r5, "REGISTER_BIOMETRIC_KEY", A0a);
        C1310460z A01 = AnonymousClass61S.A01("novi-register-signing-key");
        AnonymousClass61S[] r6 = new AnonymousClass61S[4];
        AnonymousClass61S.A04("key_id", encodeToString2, r6);
        AnonymousClass61S.A05("key_type", "ECDSA_SECP256R1", r6, 1);
        AnonymousClass61S.A05("key", encodeToString, r6, 2);
        C117305Zk.A1K(A01, "signing_key_request", C12960it.A0m(AnonymousClass61S.A00("scope", "BIOMETRIC"), r6, 3));
        String A012 = r4.A01(r5.A02());
        AnonymousClass009.A05(A012);
        C117305Zk.A1K(A01, "register_signing_key_signed_intent", AnonymousClass61S.A02("value", A012));
        C130155yt.A01(new IDxAListenerShape0S0300000_3_I1(pair, r13, r14, 0), r2, A01);
    }

    public void A03(AnonymousClass102 r13, AbstractC136196Lo r14, AnonymousClass61F r15) {
        String str;
        boolean A07 = this.A02.A07(1157);
        C130155yt r4 = this.A03;
        if (A07) {
            C127595uk r1 = new C127595uk(4744828878971297L, null, null, "WaviGetAccountPersonalInformation");
            IDxAListenerShape1S0200000_3_I1 A09 = C117305Zk.A09(r14, r13, 2);
            UUID.randomUUID().toString();
            C129645y4 r0 = r4.A09;
            String A00 = r0.A00();
            String A01 = r0.A01(r1);
            if (AnonymousClass1US.A0C(A00) || AnonymousClass1US.A0C(A01)) {
                r4.A0C.A06();
                C130785zy r02 = new C130785zy(C117305Zk.A0L(), null);
                r02.A01 = null;
                A09.AV8(r02);
                return;
            }
            C126695tI r9 = new C126695tI(A00, A01);
            if ("mutation".equals("query")) {
                str = "set";
            } else {
                str = "get";
            }
            AnonymousClass61S[] r7 = new AnonymousClass61S[5];
            AnonymousClass61S.A05("action", "novi-graphql-get-account-info", r7, 0);
            AnonymousClass61S.A05("client_request_id", C12990iw.A0n(), r7, 1);
            AnonymousClass61S.A05("composite_header", r9.A00, r7, 2);
            AnonymousClass61S.A05("graphql_input", r9.A01, r7, 3);
            r4.A0B(C117305Zk.A09(A09, r4, 0), C117295Zj.A0F(new AnonymousClass61S(1), r7, 4), str, 7);
            return;
        }
        r4.A0B(new IDxAListenerShape0S0300000_3_I1(r15, r14, r13, 2), AnonymousClass61S.A01("novi-get-account-info"), "get", 5);
    }
}
