package X;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

/* renamed from: X.0Gg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03140Gg extends AnonymousClass0Gj {
    public static final String A00 = C06390Tk.A01("BatteryChrgTracker");

    public C03140Gg(Context context, AbstractC11500gO r2) {
        super(context, r2);
    }

    @Override // X.AbstractC06170Sl
    public /* bridge */ /* synthetic */ Object A00() {
        int intExtra;
        Intent registerReceiver = this.A01.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            C06390Tk.A00().A03(A00, "getInitialState - null intent received", new Throwable[0]);
            return null;
        }
        boolean z = true;
        if (Build.VERSION.SDK_INT < 23 ? registerReceiver.getIntExtra("plugged", 0) == 0 : !((intExtra = registerReceiver.getIntExtra("status", -1)) == 2 || intExtra == 5)) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    @Override // X.AnonymousClass0Gj
    public IntentFilter A05() {
        String str;
        IntentFilter intentFilter = new IntentFilter();
        if (Build.VERSION.SDK_INT >= 23) {
            intentFilter.addAction("android.os.action.CHARGING");
            str = "android.os.action.DISCHARGING";
        } else {
            intentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
            str = "android.intent.action.ACTION_POWER_DISCONNECTED";
        }
        intentFilter.addAction(str);
        return intentFilter;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass0Gj
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(android.content.Context r7, android.content.Intent r8) {
        /*
            r6 = this;
            java.lang.String r5 = r8.getAction()
            if (r5 == 0) goto L_0x0024
            X.0Tk r4 = X.C06390Tk.A00()
            java.lang.String r3 = X.C03140Gg.A00
            r0 = 1
            java.lang.Object[] r1 = new java.lang.Object[r0]
            r2 = 0
            r1[r2] = r5
            java.lang.String r0 = "Received %s"
            java.lang.String r1 = java.lang.String.format(r0, r1)
            java.lang.Throwable[] r0 = new java.lang.Throwable[r2]
            r4.A02(r3, r1, r0)
            int r0 = r5.hashCode()
            switch(r0) {
                case -1886648615: goto L_0x0025;
                case -54942926: goto L_0x0028;
                case 948344062: goto L_0x0033;
                case 1019184907: goto L_0x0036;
                default: goto L_0x0024;
            }
        L_0x0024:
            return
        L_0x0025:
            java.lang.String r0 = "android.intent.action.ACTION_POWER_DISCONNECTED"
            goto L_0x002a
        L_0x0028:
            java.lang.String r0 = "android.os.action.DISCHARGING"
        L_0x002a:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0024
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            goto L_0x0040
        L_0x0033:
            java.lang.String r0 = "android.os.action.CHARGING"
            goto L_0x0038
        L_0x0036:
            java.lang.String r0 = "android.intent.action.ACTION_POWER_CONNECTED"
        L_0x0038:
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0024
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
        L_0x0040:
            r6.A04(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03140Gg.A06(android.content.Context, android.content.Intent):void");
    }
}
