package X;

import android.os.Handler;
import android.os.Looper;
import com.google.android.exoplayer2.Timeline;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.2kF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC56052kF extends AbstractC67703Sn {
    public Handler A00;
    public AnonymousClass5QP A01;
    public final HashMap A02 = C12970iu.A11();

    @Override // X.AbstractC67703Sn
    public void A00() {
        HashMap hashMap = this.A02;
        Iterator A0t = C12990iw.A0t(hashMap);
        while (A0t.hasNext()) {
            AnonymousClass4P6 r2 = (AnonymousClass4P6) A0t.next();
            AnonymousClass2CD r1 = r2.A01;
            r1.AaA(r2.A00);
            r1.AaI(r2.A02);
        }
        hashMap.clear();
    }

    @Override // X.AbstractC67703Sn
    public void A02(AnonymousClass5QP r4) {
        this.A01 = r4;
        Looper myLooper = Looper.myLooper();
        C95314dV.A01(myLooper);
        this.A00 = new Handler(myLooper, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0081, code lost:
        if (r11 != 0) goto L_0x0083;
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(com.google.android.exoplayer2.Timeline r14) {
        /*
        // Method dump skipped, instructions count: 225
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC56052kF.A03(com.google.android.exoplayer2.Timeline):void");
    }

    public final void A04(AnonymousClass2CD r7) {
        HashMap hashMap = this.A02;
        C95314dV.A03(!hashMap.containsKey(null));
        C107554xW r4 = new AnonymousClass5SQ() { // from class: X.4xW
            @Override // X.AnonymousClass5SQ
            public final void AWC(Timeline timeline, AnonymousClass2CD r42) {
                AbstractC56052kF.this.A03(timeline);
            }
        };
        AnonymousClass3Sp r5 = new AnonymousClass3Sp(this);
        hashMap.put(null, new AnonymousClass4P6(r4, r7, r5));
        AbstractC67703Sn r3 = (AbstractC67703Sn) r7;
        r3.A03.A02.add(new AnonymousClass4M2(this.A00, r5));
        r3.A02.A02.add(new C89874Ls(this.A00, r5));
        r7.AZV(r4, this.A01);
        if (!(!this.A05.isEmpty())) {
            r7.A8u(r4);
        }
    }

    @Override // X.AnonymousClass2CD
    public void ALT() {
        if (!(this instanceof C56032kD)) {
            Iterator A0t = C12990iw.A0t(this.A02);
            while (A0t.hasNext()) {
                ((AnonymousClass4P6) A0t.next()).A01.ALT();
            }
        }
    }
}
