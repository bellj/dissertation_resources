package X;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.auth.TokenData;

/* renamed from: X.4yZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108144yZ implements AnonymousClass5SW {
    public final /* synthetic */ Account A00;
    public final /* synthetic */ Bundle A01;
    public final /* synthetic */ String A02 = "oauth2:https://www.googleapis.com/auth/drive.appdata";

    public C108144yZ(Account account, Bundle bundle) {
        this.A00 = account;
        this.A01 = bundle;
    }

    @Override // X.AnonymousClass5SW
    public final /* synthetic */ Object Ah8(IBinder iBinder) {
        AnonymousClass5Y6 r5;
        if (iBinder == null) {
            r5 = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.auth.IAuthManagerService");
            r5 = queryLocalInterface instanceof AnonymousClass5Y6 ? (AnonymousClass5Y6) queryLocalInterface : new C78893pi(iBinder);
        }
        Account account = this.A00;
        String str = this.A02;
        Bundle bundle = this.A01;
        C98414ie r52 = (C98414ie) r5;
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(r52.A01);
        C95154dE.A00(obtain, account);
        obtain.writeString(str);
        C95154dE.A00(obtain, bundle);
        Parcel A00 = r52.A00(5, obtain);
        Bundle bundle2 = (Bundle) C12970iu.A0F(A00, Bundle.CREATOR);
        A00.recycle();
        AnonymousClass3JL.A05(bundle2);
        bundle2.setClassLoader(TokenData.class.getClassLoader());
        Bundle bundle3 = bundle2.getBundle("tokenDetails");
        if (bundle3 != null) {
            bundle3.setClassLoader(TokenData.class.getClassLoader());
            Parcelable parcelable = bundle3.getParcelable("TokenData");
            if (parcelable != null) {
                return parcelable;
            }
        }
        String string = bundle2.getString("Error");
        Intent intent = (Intent) bundle2.getParcelable("userRecoveryIntent");
        EnumC869049k[] values = EnumC869049k.values();
        EnumC869049k r1 = null;
        for (EnumC869049k r2 : values) {
            if (r2.zzek.equals(string)) {
                r1 = r2;
            }
        }
        if (EnumC869049k.A05.equals(r1) || EnumC869049k.A07.equals(r1) || EnumC869049k.A08.equals(r1) || EnumC869049k.A09.equals(r1) || EnumC869049k.A06.equals(r1) || EnumC869049k.A0A.equals(r1) || EnumC869049k.A01.equals(r1) || EnumC869049k.A0C.equals(r1) || EnumC869049k.A0D.equals(r1) || EnumC869049k.A0E.equals(r1) || EnumC869049k.A0F.equals(r1) || EnumC869049k.A0G.equals(r1) || EnumC869049k.A0H.equals(r1) || EnumC869049k.A0J.equals(r1) || EnumC869049k.A0B.equals(r1) || EnumC869049k.A0I.equals(r1)) {
            C63703Cp r6 = AnonymousClass3JL.A01;
            String valueOf = String.valueOf(r1);
            StringBuilder A0t = C12980iv.A0t(valueOf.length() + 31);
            A0t.append("isUserRecoverableError status: ");
            Log.w("Auth", r6.A03.concat(C72463ee.A0G("GoogleAuthUtil", new Object[]{C12960it.A0d(valueOf, A0t)})));
            throw new C77453nJ(intent, string);
        } else if (EnumC869049k.A02.equals(r1) || EnumC869049k.A03.equals(r1) || EnumC869049k.A04.equals(r1)) {
            throw C12990iw.A0i(string);
        } else {
            throw new AnonymousClass4CA(string);
        }
    }
}
