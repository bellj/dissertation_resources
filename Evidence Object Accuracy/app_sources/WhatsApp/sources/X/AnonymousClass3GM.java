package X;

/* renamed from: X.3GM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GM {
    public static String A00(C16120oU r3, int i) {
        String A0n = C12990iw.A0n();
        AnonymousClass310 r1 = new AnonymousClass310();
        A01(r1, A0n, 1, i);
        r3.A07(r1);
        return A0n;
    }

    public static void A01(AnonymousClass310 r1, String str, int i, int i2) {
        r1.A01 = Integer.valueOf(i);
        r1.A06 = str;
        r1.A00 = Integer.valueOf(i2);
        r1.A02 = C12970iu.A0g();
    }
}
