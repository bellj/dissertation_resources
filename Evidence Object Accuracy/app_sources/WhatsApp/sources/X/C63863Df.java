package X;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.3Df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63863Df {
    public final LinkedList A00 = new LinkedList();

    public AbstractC454821u A00(List list) {
        LinkedList linkedList = this.A00;
        if (!linkedList.isEmpty()) {
            AbstractC94414bm r2 = (AbstractC94414bm) linkedList.removeLast();
            r2.A01(list);
            if ((r2 instanceof AnonymousClass45P) && !linkedList.isEmpty()) {
                AbstractC94414bm r1 = (AbstractC94414bm) linkedList.getLast();
                if ((r1 instanceof AnonymousClass33O) && r1.A00 == r2.A00) {
                    A00(list);
                }
            } else if (r2 instanceof AnonymousClass45N) {
                return r2.A00;
            }
        }
        return null;
    }

    public String A01(List list) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("version", 1);
        JSONArray jSONArray = new JSONArray();
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            AbstractC94414bm r3 = (AbstractC94414bm) it.next();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("shape_index", list.indexOf(r3.A00));
            jSONObject2.put("type", r3.A00());
            r3.A03(jSONObject2);
            jSONArray.put(jSONObject2);
        }
        jSONObject.put("actions", jSONArray);
        return jSONObject.toString();
    }
}
