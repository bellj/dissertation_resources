package X;

import java.lang.reflect.Method;

/* renamed from: X.3GY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GY {
    public static final Method A00;
    public static final Method A01;
    public static final Method A02;
    public static final Method A03;
    public static final Method A04;
    public static final Method A05;
    public static final Method A06;
    public static final Method A07;

    /* JADX WARNING: Can't wrap try/catch for region: R(24:2|(2:47|3)|5|(21:39|7|9|41|10|12|53|13|15|(12:45|17|19|(9:51|21|25|(6:49|27|31|(2:43|33)|34|35)|30|31|(0)|34|35)|24|25|(0)|30|31|(0)|34|35)|18|19|(0)|24|25|(0)|30|31|(0)|34|35)|8|9|41|10|12|53|13|15|(0)|18|19|(0)|24|25|(0)|30|31|(0)|34|35) */
    /* JADX WARNING: Can't wrap try/catch for region: R(25:2|47|3|5|(21:39|7|9|41|10|12|53|13|15|(12:45|17|19|(9:51|21|25|(6:49|27|31|(2:43|33)|34|35)|30|31|(0)|34|35)|24|25|(0)|30|31|(0)|34|35)|18|19|(0)|24|25|(0)|30|31|(0)|34|35)|8|9|41|10|12|53|13|15|(0)|18|19|(0)|24|25|(0)|30|31|(0)|34|35) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003f, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0051, code lost:
        r0 = null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ae A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x005a A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x008e A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0078 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    static {
        /*
            java.lang.Class<java.lang.String> r10 = java.lang.String.class
            java.lang.String r2 = "add"
            android.os.Process.myUid()
            r4 = 1
            r5 = 0
            r3 = 0
            java.lang.Class[] r1 = new java.lang.Class[r4]     // Catch: Exception -> 0x0017
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: Exception -> 0x0017
            r1[r5] = r0     // Catch: Exception -> 0x0017
            java.lang.Class<android.os.WorkSource> r0 = android.os.WorkSource.class
            java.lang.reflect.Method r0 = r0.getMethod(r2, r1)     // Catch: Exception -> 0x0017
            goto L_0x0018
        L_0x0017:
            r0 = r3
        L_0x0018:
            X.AnonymousClass3GY.A00 = r0
            boolean r0 = X.C472729v.A00()
            r9 = 2
            if (r0 == 0) goto L_0x0030
            java.lang.Class[] r1 = new java.lang.Class[r9]     // Catch: Exception -> 0x0030
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: Exception -> 0x0030
            r1[r5] = r0     // Catch: Exception -> 0x0030
            r1[r4] = r10     // Catch: Exception -> 0x0030
            java.lang.Class<android.os.WorkSource> r0 = android.os.WorkSource.class
            java.lang.reflect.Method r0 = r0.getMethod(r2, r1)     // Catch: Exception -> 0x0030
            goto L_0x0031
        L_0x0030:
            r0 = r3
        L_0x0031:
            X.AnonymousClass3GY.A01 = r0
            java.lang.Class<android.os.WorkSource> r2 = android.os.WorkSource.class
            java.lang.String r1 = "size"
            java.lang.Class[] r0 = new java.lang.Class[r5]     // Catch: Exception -> 0x003f
            java.lang.reflect.Method r0 = r2.getMethod(r1, r0)     // Catch: Exception -> 0x003f
            goto L_0x0040
        L_0x003f:
            r0 = r3
        L_0x0040:
            X.AnonymousClass3GY.A02 = r0
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch: Exception -> 0x0051
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: Exception -> 0x0051
            r2[r5] = r0     // Catch: Exception -> 0x0051
            java.lang.Class<android.os.WorkSource> r1 = android.os.WorkSource.class
            java.lang.String r0 = "get"
            java.lang.reflect.Method r0 = r1.getMethod(r0, r2)     // Catch: Exception -> 0x0051
            goto L_0x0052
        L_0x0051:
            r0 = r3
        L_0x0052:
            X.AnonymousClass3GY.A03 = r0
            boolean r0 = X.C472729v.A00()
            if (r0 == 0) goto L_0x0069
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch: Exception -> 0x0069
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: Exception -> 0x0069
            r2[r5] = r0     // Catch: Exception -> 0x0069
            java.lang.Class<android.os.WorkSource> r1 = android.os.WorkSource.class
            java.lang.String r0 = "getName"
            java.lang.reflect.Method r0 = r1.getMethod(r0, r2)     // Catch: Exception -> 0x0069
            goto L_0x006a
        L_0x0069:
            r0 = r3
        L_0x006a:
            X.AnonymousClass3GY.A04 = r0
            int r7 = android.os.Build.VERSION.SDK_INT
            r6 = 28
            boolean r0 = X.C12990iw.A1X(r7, r6)
            java.lang.String r8 = "WorkSourceUtil"
            if (r0 == 0) goto L_0x0089
            java.lang.Class<android.os.WorkSource> r2 = android.os.WorkSource.class
            java.lang.String r1 = "createWorkChain"
            java.lang.Class[] r0 = new java.lang.Class[r5]     // Catch: Exception -> 0x0083
            java.lang.reflect.Method r0 = r2.getMethod(r1, r0)     // Catch: Exception -> 0x0083
            goto L_0x008a
        L_0x0083:
            r1 = move-exception
            java.lang.String r0 = "Missing WorkChain API createWorkChain"
            android.util.Log.w(r8, r0, r1)
        L_0x0089:
            r0 = r3
        L_0x008a:
            X.AnonymousClass3GY.A05 = r0
            if (r7 < r6) goto L_0x00a9
            java.lang.String r0 = "android.os.WorkSource$WorkChain"
            java.lang.Class r2 = java.lang.Class.forName(r0)     // Catch: Exception -> 0x00a3
            java.lang.Class[] r1 = new java.lang.Class[r9]     // Catch: Exception -> 0x00a3
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch: Exception -> 0x00a3
            r1[r5] = r0     // Catch: Exception -> 0x00a3
            r1[r4] = r10     // Catch: Exception -> 0x00a3
            java.lang.String r0 = "addNode"
            java.lang.reflect.Method r0 = r2.getMethod(r0, r1)     // Catch: Exception -> 0x00a3
            goto L_0x00aa
        L_0x00a3:
            r1 = move-exception
            java.lang.String r0 = "Missing WorkChain class"
            android.util.Log.w(r8, r0, r1)
        L_0x00a9:
            r0 = r3
        L_0x00aa:
            X.AnonymousClass3GY.A06 = r0
            if (r7 < r6) goto L_0x00bb
            java.lang.Class<android.os.WorkSource> r2 = android.os.WorkSource.class
            java.lang.String r1 = "isEmpty"
            java.lang.Class[] r0 = new java.lang.Class[r5]     // Catch: Exception -> 0x00bb
            java.lang.reflect.Method r3 = r2.getMethod(r1, r0)     // Catch: Exception -> 0x00bb
            r3.setAccessible(r4)     // Catch: Exception -> 0x00bb
        L_0x00bb:
            X.AnonymousClass3GY.A07 = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3GY.<clinit>():void");
    }
}
