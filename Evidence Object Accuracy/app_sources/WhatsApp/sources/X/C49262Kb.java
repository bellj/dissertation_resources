package X;

import java.util.EnumMap;
import java.util.Map;

/* renamed from: X.2Kb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C49262Kb {
    public Map A00 = null;
    public AnonymousClass3HP[] A01;
    public final String A02;
    public final byte[] A03;

    public C49262Kb(String str, byte[] bArr, AnonymousClass3HP[] r4) {
        System.currentTimeMillis();
        this.A02 = str;
        this.A03 = bArr;
        this.A01 = r4;
    }

    public void A00(AnonymousClass4AQ r3, Object obj) {
        Map map = this.A00;
        if (map == null) {
            map = new EnumMap(AnonymousClass4AQ.class);
            this.A00 = map;
        }
        map.put(r3, obj);
    }

    public String toString() {
        return this.A02;
    }
}
