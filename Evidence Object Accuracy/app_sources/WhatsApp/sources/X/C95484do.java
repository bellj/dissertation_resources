package X;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

/* renamed from: X.4do  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95484do {
    public int A00;
    public AbstractC79223qF A01;
    public final ByteBuffer A02;

    public static int A02(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (j & Long.MIN_VALUE) == 0 ? 9 : 10;
    }

    public final void A05(int i) {
        byte b = (byte) i;
        ByteBuffer byteBuffer = this.A02;
        if (byteBuffer.hasRemaining()) {
            byteBuffer.put(b);
            return;
        }
        throw new C867548r(byteBuffer.position(), byteBuffer.limit());
    }

    public final void A06(int i) {
        while ((i & -128) != 0) {
            A05((i & 127) | 128);
            i >>>= 7;
        }
        A05(i);
    }

    public final void A09(long j) {
        while (true) {
            int i = (int) j;
            if ((-128 & j) == 0) {
                A05(i);
                return;
            } else {
                A05((i & 127) | 128);
                j >>>= 7;
            }
        }
    }

    public C95484do(byte[] bArr, int i) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr, 0, i);
        this.A02 = wrap;
        wrap.order(ByteOrder.LITTLE_ENDIAN);
    }

    public static int A00(int i) {
        int A07 = C72453ed.A07(i << 3);
        int A03 = A03("");
        return A07 + C72453ed.A07(A03) + A03;
    }

    public static int A01(int i) {
        return C72453ed.A07(i);
    }

    public static int A03(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < 128) {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < 2048) {
                i3 += (127 - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < 2048) {
                        i += (127 - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i2) >= 65536) {
                                i2++;
                            } else {
                                throw C12970iu.A0f(C12960it.A0e("Unpaired surrogate at index ", C12980iv.A0t(39), i2));
                            }
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        throw C12970iu.A0f(C72453ed.A0n(i3));
    }

    public static void A04(CharSequence charSequence, ByteBuffer byteBuffer) {
        int i;
        char c;
        int i2;
        char charAt;
        int i3;
        if (!byteBuffer.isReadOnly()) {
            int i4 = 0;
            if (byteBuffer.hasArray()) {
                try {
                    byte[] array = byteBuffer.array();
                    int arrayOffset = byteBuffer.arrayOffset() + byteBuffer.position();
                    int remaining = byteBuffer.remaining();
                    int length = charSequence.length();
                    int i5 = remaining + arrayOffset;
                    while (i4 < length) {
                        int i6 = i4 + arrayOffset;
                        if (i6 >= i5 || (charAt = charSequence.charAt(i4)) >= 128) {
                            break;
                        }
                        array[i6] = (byte) charAt;
                        i4++;
                    }
                    if (i4 == length) {
                        i = arrayOffset + length;
                    } else {
                        i = arrayOffset + i4;
                        while (i4 < length) {
                            char charAt2 = charSequence.charAt(i4);
                            if (charAt2 >= 128 || i >= i5) {
                                if (charAt2 < 2048 && i <= i5 - 2) {
                                    i = C72453ed.A0M(array, i, charAt2);
                                } else if ((charAt2 < 55296 || 57343 < charAt2) && i <= i5 - 3) {
                                    i = C72453ed.A0N(array, i, charAt2);
                                    i2 = i + 1;
                                    c = (charAt2 & '?') | 128;
                                } else if (i <= i5 - 4) {
                                    int i7 = i4 + 1;
                                    if (i7 != charSequence.length()) {
                                        char charAt3 = charSequence.charAt(i7);
                                        if (Character.isSurrogatePair(charAt2, charAt3)) {
                                            i = C72453ed.A0I(array, charAt2, charAt3, i);
                                            i4 = i7;
                                        } else {
                                            i4 = i7;
                                        }
                                    }
                                    throw C12970iu.A0f(C12960it.A0e("Unpaired surrogate at index ", C12980iv.A0t(39), i4 - 1));
                                } else {
                                    StringBuilder A0t = C12980iv.A0t(37);
                                    A0t.append("Failed writing ");
                                    A0t.append(charAt2);
                                    throw new ArrayIndexOutOfBoundsException(C12960it.A0e(" at index ", A0t, i));
                                }
                                i4++;
                            } else {
                                i2 = i + 1;
                                c = charAt2;
                            }
                            array[i] = c == 1 ? (byte) 1 : 0;
                            i = i2;
                            i4++;
                        }
                    }
                    byteBuffer.position(i - byteBuffer.arrayOffset());
                } catch (ArrayIndexOutOfBoundsException e) {
                    BufferOverflowException bufferOverflowException = new BufferOverflowException();
                    bufferOverflowException.initCause(e);
                    throw bufferOverflowException;
                }
            } else {
                int length2 = charSequence.length();
                while (i4 < length2) {
                    char charAt4 = charSequence.charAt(i4);
                    char c2 = charAt4;
                    if (charAt4 >= 128) {
                        if (charAt4 < 2048) {
                            i3 = (charAt4 >>> 6) | 960;
                        } else if (charAt4 < 55296 || 57343 < charAt4) {
                            byteBuffer.put((byte) ((charAt4 >>> '\f') | 480));
                            i3 = ((charAt4 >>> 6) & 63) | 128;
                        } else {
                            int i8 = i4 + 1;
                            if (i8 != charSequence.length()) {
                                char charAt5 = charSequence.charAt(i8);
                                if (Character.isSurrogatePair(charAt4, charAt5)) {
                                    int codePoint = Character.toCodePoint(charAt4, charAt5);
                                    byteBuffer.put((byte) ((codePoint >>> 18) | 240));
                                    byteBuffer.put((byte) (((codePoint >>> 12) & 63) | 128));
                                    byteBuffer.put((byte) (((codePoint >>> 6) & 63) | 128));
                                    byteBuffer.put((byte) ((codePoint & 63) | 128));
                                    i4 = i8;
                                    i4++;
                                } else {
                                    i4 = i8;
                                }
                            }
                            throw C12970iu.A0f(C12960it.A0e("Unpaired surrogate at index ", C12980iv.A0t(39), i4 - 1));
                        }
                        byteBuffer.put((byte) i3);
                        c2 = (charAt4 & '?') | 128;
                    }
                    byteBuffer.put(c2 == 1 ? (byte) 1 : 0);
                    i4++;
                }
            }
        } else {
            throw new ReadOnlyBufferException();
        }
    }

    public final void A07(int i, String str) {
        A06((i << 3) | 2);
        try {
            int length = str.length();
            int A07 = C72453ed.A07(length);
            if (A07 == C72453ed.A07(length * 3)) {
                ByteBuffer byteBuffer = this.A02;
                int position = byteBuffer.position();
                if (byteBuffer.remaining() >= A07) {
                    byteBuffer.position(position + A07);
                    A04(str, byteBuffer);
                    int position2 = byteBuffer.position();
                    byteBuffer.position(position);
                    A06((position2 - position) - A07);
                    byteBuffer.position(position2);
                    return;
                }
                throw new C867548r(position + A07, byteBuffer.limit());
            }
            A06(A03(str));
            A04(str, this.A02);
        } catch (BufferOverflowException e) {
            ByteBuffer byteBuffer2 = this.A02;
            C867548r r0 = new C867548r(byteBuffer2.position(), byteBuffer2.limit());
            r0.initCause(e);
            throw r0;
        }
    }

    public final void A08(int i, byte[] bArr) {
        A06((i << 3) | 2);
        int length = bArr.length;
        A06(length);
        ByteBuffer byteBuffer = this.A02;
        if (byteBuffer.remaining() >= length) {
            byteBuffer.put(bArr, 0, length);
            return;
        }
        throw new C867548r(byteBuffer.position(), byteBuffer.limit());
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0A(X.AbstractC117125Yn r8, int r9) {
        /*
            r7 = this;
            X.3qF r0 = r7.A01
            if (r0 != 0) goto L_0x0092
            java.nio.ByteBuffer r5 = r7.A02
            boolean r0 = r5.hasArray()
            if (r0 == 0) goto L_0x0076
            X.3qJ r0 = new X.3qJ
            r0.<init>(r5)
        L_0x0011:
            r7.A01 = r0
        L_0x0013:
            int r0 = r5.position()
            r7.A00 = r0
        L_0x0019:
            X.3qF r6 = r7.A01
            r0 = 2
            X.AnonymousClass4UO.A06(r6, r9, r0)
            int r0 = r8.Ah0()
            r6.A05(r0)
            X.5XT r1 = X.C72453ed.A0d(r8)
            X.4zU r0 = r6.A00
            if (r0 != 0) goto L_0x0033
            X.4zU r0 = new X.4zU
            r0.<init>(r6)
        L_0x0033:
            r1.Agw(r0, r8)
            boolean r0 = r6 instanceof X.C79293qM
            if (r0 != 0) goto L_0x0068
            boolean r0 = r6 instanceof X.C79273qK
            if (r0 != 0) goto L_0x005a
            X.3qL r6 = (X.C79283qL) r6
            boolean r0 = r6 instanceof X.C79263qJ
            if (r0 == 0) goto L_0x0053
            X.3qJ r6 = (X.C79263qJ) r6
            java.nio.ByteBuffer r3 = r6.A01
            int r2 = r6.A00
            int r1 = r6.A00
            int r0 = r6.A02
            int r1 = r1 - r0
            int r2 = r2 + r1
            r3.position(r2)
        L_0x0053:
            int r0 = r5.position()
            r7.A00 = r0
            return
        L_0x005a:
            X.3qK r6 = (X.C79273qK) r6
            java.nio.ByteBuffer r1 = r6.A00
            java.nio.ByteBuffer r0 = r6.A01
            int r0 = r0.position()
            r1.position(r0)
            goto L_0x0053
        L_0x0068:
            X.3qM r6 = (X.C79293qM) r6
            java.nio.ByteBuffer r4 = r6.A04
            long r2 = r6.A00
            long r0 = r6.A01
            long r2 = r2 - r0
            int r0 = (int) r2
            r4.position(r0)
            goto L_0x0053
        L_0x0076:
            boolean r0 = r5.isDirect()
            if (r0 == 0) goto L_0x00b0
            boolean r0 = r5.isReadOnly()
            if (r0 != 0) goto L_0x00b0
            boolean r0 = X.C95624e5.A07
            if (r0 == 0) goto L_0x008c
            X.3qM r0 = new X.3qM
            r0.<init>(r5)
            goto L_0x0011
        L_0x008c:
            X.3qK r0 = new X.3qK
            r0.<init>(r5)
            goto L_0x0011
        L_0x0092:
            int r1 = r7.A00
            java.nio.ByteBuffer r5 = r7.A02
            int r0 = r5.position()
            if (r1 == r0) goto L_0x0019
            X.3qF r4 = r7.A01
            byte[] r3 = r5.array()
            int r2 = r7.A00
            int r1 = r5.position()
            int r0 = r7.A00
            int r1 = r1 - r0
            r4.A0D(r3, r2, r1)
            goto L_0x0013
        L_0x00b0:
            java.lang.String r0 = "ByteBuffer is read-only"
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95484do.A0A(X.5Yn, int):void");
    }

    public final void A0B(AbstractC94974cq r3, int i) {
        A06((i << 3) | 2);
        if (r3.A00 < 0) {
            r3.A00 = r3.A03();
        }
        A06(r3.A00);
        r3.A05(this);
    }
}
