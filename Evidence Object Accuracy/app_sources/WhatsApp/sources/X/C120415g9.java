package X;

import android.content.Context;

/* renamed from: X.5g9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120415g9 extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C18650sn A02;
    public final AnonymousClass6BE A03;
    public final C30931Zj A04 = C117305Zk.A0V("IndiaUpiDeregisterMapperActions", "network");
    public final C18590sh A05;

    public C120415g9(Context context, C14900mE r4, C1308460e r5, C18650sn r6, C18610sj r7, AnonymousClass6BE r8, C18590sh r9) {
        super(r5.A04, r7);
        this.A00 = context;
        this.A01 = r4;
        this.A05 = r9;
        this.A02 = r6;
        this.A03 = r8;
    }
}
