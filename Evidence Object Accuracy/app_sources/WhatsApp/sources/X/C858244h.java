package X;

import com.whatsapp.conversationslist.ConversationsFragment;
import java.util.Set;

/* renamed from: X.44h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C858244h extends AbstractC33331dp {
    public final /* synthetic */ ConversationsFragment A00;

    public C858244h(ConversationsFragment conversationsFragment) {
        this.A00 = conversationsFragment;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        ConversationsFragment.A03(this.A00);
    }
}
