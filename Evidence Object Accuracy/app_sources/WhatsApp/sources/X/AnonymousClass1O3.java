package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.1O3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1O3 extends Handler {
    public final /* synthetic */ C18720su A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1O3(Looper looper, C18720su r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        if (message.arg1 == 0) {
            boolean z = true;
            C18720su r4 = this.A00;
            synchronized (r4.A0H) {
                AnonymousClass1O4 r0 = r4.A04;
                if (r0 != null) {
                    r0.A02(59000);
                    if (r4.A04.A00.A01() > 0) {
                        z = false;
                    }
                }
            }
            synchronized (r4.A0D) {
                AnonymousClass1O4 r02 = r4.A03;
                if (r02 != null) {
                    r02.A02(59000);
                    if (r4.A03.A00.A01() > 0) {
                        z = false;
                    }
                }
            }
            synchronized (r4.A0I) {
                if (r4.A0M != null) {
                    r4.A0M.A02(59000);
                    if (r4.A0M.A00.A01() > 0) {
                        z = false;
                    }
                }
            }
            synchronized (r4.A0J) {
                if (r4.A0N != null) {
                    r4.A0N.A02(59000);
                    if (r4.A0N.A00.A01() > 0) {
                        z = false;
                    }
                }
            }
            if (!z) {
                sendEmptyMessageDelayed(0, 60000);
            }
        }
    }
}
