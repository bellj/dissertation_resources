package X;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.gms.common.api.Status;
import com.whatsapp.R;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;

/* renamed from: X.2Yy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51822Yy extends BroadcastReceiver {
    public boolean A00;
    public final C14820m6 A01;
    public final Object A02 = C12970iu.A0l();
    public final WeakReference A03;
    public volatile boolean A04 = false;

    public C51822Yy(C14820m6 r2, VerifyPhoneNumber verifyPhoneNumber) {
        this.A03 = C12970iu.A10(verifyPhoneNumber);
        this.A01 = r2;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        SharedPreferences.Editor editor;
        String A0d;
        String str;
        String str2;
        if (!this.A04) {
            synchronized (this.A02) {
                if (!this.A04) {
                    AnonymousClass22D.A00(context);
                    this.A04 = true;
                }
            }
        }
        if ("com.google.android.gms.auth.api.phone.SMS_RETRIEVED".equals(intent.getAction())) {
            Log.i("smsretrieverreceiver/text/intent");
            if (this.A00) {
                str2 = "smsretrieverreceiver/already received";
            } else {
                VerifyPhoneNumber verifyPhoneNumber = (VerifyPhoneNumber) this.A03.get();
                if (verifyPhoneNumber == null) {
                    str2 = "receivedtextreceiver/activity is null";
                } else if (verifyPhoneNumber.AJN()) {
                    str2 = "smsretrieverreceiver/destroyed";
                } else {
                    Bundle extras = intent.getExtras();
                    if (extras == null) {
                        str = "smsretrieverreceiver/bundle-null";
                    } else {
                        Status status = (Status) extras.get("com.google.android.gms.auth.api.phone.EXTRA_STATUS");
                        if (status == null) {
                            str = "smsretrieverreceiver/status-null";
                        } else {
                            int i = status.A01;
                            if (i == 0) {
                                String string = extras.getString("com.google.android.gms.auth.api.phone.EXTRA_SMS_MESSAGE");
                                if (string == null) {
                                    AnonymousClass23M.A0J(this.A01, "null-sms-message");
                                    return;
                                }
                                Matcher matcher = new AnonymousClass4L0(verifyPhoneNumber.getString(R.string.localized_app_name)).A00.matcher(string);
                                if (!matcher.find()) {
                                    A0d = null;
                                } else {
                                    StringBuilder A0h = C12960it.A0h();
                                    A0h.append(matcher.group(1));
                                    A0d = C12960it.A0d(matcher.group(2), A0h);
                                }
                                if (C28421Nd.A00(A0d, -1) != -1) {
                                    this.A00 = true;
                                    verifyPhoneNumber.A3X(A0d);
                                } else {
                                    Log.w("verifysms/smsretriever/no-code");
                                    AnonymousClass23M.A0J(this.A01, "server-send-mismatch-empty");
                                }
                                editor = C12960it.A08(this.A01).putInt("sms_retriever_retry_count", 0);
                            } else if (i == 15) {
                                C14820m6 r1 = this.A01;
                                SharedPreferences sharedPreferences = r1.A00;
                                int A01 = C12970iu.A01(sharedPreferences, "sms_retriever_retry_count");
                                if (A01 < 2) {
                                    C13600jz A012 = new C56282kd((Activity) verifyPhoneNumber).A01(new C77803ns(), 1);
                                    AnonymousClass3TS r0 = new AbstractC13640k3(A01) { // from class: X.3TS
                                        public final /* synthetic */ int A00;

                                        {
                                            this.A00 = r2;
                                        }

                                        @Override // X.AbstractC13640k3
                                        public final void AX4(Object obj) {
                                            C51822Yy r2 = C51822Yy.this;
                                            int i2 = this.A00;
                                            Log.i("verifysms/smsretriever/re-registered sms retriever client");
                                            C14820m6 r02 = r2.A01;
                                            C12970iu.A1B(C12960it.A08(r02), "sms_retriever_retry_count", i2 + 1);
                                        }
                                    };
                                    Executor executor = C13650k5.A00;
                                    A012.A06(r0, executor);
                                    A012.A05(new AbstractC13630k2() { // from class: X.3TR
                                        @Override // X.AbstractC13630k2
                                        public final void AQA(Exception exc) {
                                            C51822Yy r12 = C51822Yy.this;
                                            Log.e("verifysms/smsretriever/failure registering sms retriever client/ ", exc);
                                            C14820m6 r13 = r12.A01;
                                            AnonymousClass23M.A0J(r13, "timeout-waiting-for-sms");
                                            C12970iu.A1B(C12960it.A08(r13), "sms_retriever_retry_count", 0);
                                        }
                                    }, executor);
                                    return;
                                }
                                AnonymousClass23M.A0J(r1, "timeout-waiting-for-sms");
                                editor = sharedPreferences.edit().putInt("sms_retriever_retry_count", 0);
                            } else {
                                return;
                            }
                            editor.apply();
                            return;
                        }
                    }
                    Log.e(str);
                    return;
                }
            }
            Log.i(str2);
        }
    }
}
