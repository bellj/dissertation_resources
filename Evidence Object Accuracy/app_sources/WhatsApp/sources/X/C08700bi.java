package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.0bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08700bi implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setAlpha(((Number) obj2).floatValue());
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return !C87834De.A00(obj, obj2);
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setAlpha(1.0f);
    }
}
