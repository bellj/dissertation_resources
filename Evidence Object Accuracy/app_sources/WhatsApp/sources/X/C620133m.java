package X;

import android.content.Context;

/* renamed from: X.33m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C620133m extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C18610sj A01;
    public final /* synthetic */ AnonymousClass3GZ A02;
    public final /* synthetic */ boolean A03 = true;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C620133m(Context context, C14900mE r3, AnonymousClass1FK r4, C18650sn r5, C18610sj r6, AnonymousClass3GZ r7) {
        super(context, r3, r5);
        this.A01 = r6;
        this.A00 = r4;
        this.A02 = r7;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r3) {
        this.A01.A0I.A05(C12960it.A0b("Tos onRequestError: ", r3));
        this.A00.AV3(r3);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r3) {
        this.A01.A0I.A05(C12960it.A0b("Tos onResponseError: ", r3));
        this.A00.AVA(r3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001e, code lost:
        if ("1".equals(r3.A02) != false) goto L_0x0020;
     */
    @Override // X.AbstractC451020e
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.AnonymousClass1V8 r7) {
        /*
            r6 = this;
            r5 = 0
            X.46N r4 = new X.46N     // Catch: 1V9 -> 0x0034
            r4.<init>()     // Catch: 1V9 -> 0x0034
            X.3GZ r1 = r6.A02     // Catch: 1V9 -> 0x0034
            X.0sj r0 = r6.A01     // Catch: 1V9 -> 0x0034
            X.0nm r0 = r0.A00     // Catch: 1V9 -> 0x0034
            X.3HH r3 = new X.3HH     // Catch: 1V9 -> 0x0034
            r3.<init>(r0, r7, r1)     // Catch: 1V9 -> 0x0034
            boolean r0 = r6.A03     // Catch: 1V9 -> 0x0034
            java.lang.String r2 = "1"
            if (r0 == 0) goto L_0x0020
            java.lang.String r0 = r3.A02     // Catch: 1V9 -> 0x0034
            boolean r1 = r2.equals(r0)     // Catch: 1V9 -> 0x0034
            r0 = 0
            if (r1 == 0) goto L_0x0021
        L_0x0020:
            r0 = 1
        L_0x0021:
            r4.A02 = r0     // Catch: 1V9 -> 0x0034
            java.lang.String r0 = r3.A04     // Catch: 1V9 -> 0x0034
            boolean r0 = r2.equals(r0)     // Catch: 1V9 -> 0x0034
            r4.A00 = r0     // Catch: 1V9 -> 0x0034
            java.lang.String r0 = r3.A05     // Catch: 1V9 -> 0x0034
            boolean r0 = r2.equals(r0)     // Catch: 1V9 -> 0x0034
            r4.A01 = r0     // Catch: 1V9 -> 0x0034
            goto L_0x003b
        L_0x0034:
            X.46N r4 = new X.46N
            r4.<init>()
            r4.A02 = r5
        L_0x003b:
            X.1FK r0 = r6.A00
            r0.AVB(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C620133m.A04(X.1V8):void");
    }
}
