package X;

/* renamed from: X.1a4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31141a4 extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;

    public C31141a4() {
        super(2600, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamAndroidKeystoreAuthkeyFailure {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidKeystoreState", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numFailures", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "numSuccessfulReads", this.A02);
        sb.append("}");
        return sb.toString();
    }
}
