package X;

import android.graphics.Bitmap;
import com.whatsapp.storage.StorageUsageMediaPreviewView;

/* renamed from: X.56v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1107756v implements AnonymousClass23D {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AbstractC35601iM A01;
    public final /* synthetic */ StorageUsageMediaPreviewView A02;
    public final /* synthetic */ String A03;

    public C1107756v(AbstractC35601iM r1, StorageUsageMediaPreviewView storageUsageMediaPreviewView, String str, int i) {
        this.A02 = storageUsageMediaPreviewView;
        this.A01 = r1;
        this.A00 = i;
        this.A03 = str;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A01.A02);
        return C12960it.A0d(this.A03, A0h);
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        Bitmap Aem = this.A01.Aem(this.A00);
        return Aem == null ? StorageUsageMediaPreviewView.A0B : Aem;
    }
}
