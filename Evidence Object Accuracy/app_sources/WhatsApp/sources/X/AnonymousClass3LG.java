package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.whatsapp.backup.google.RestoreFromBackupActivity;

/* renamed from: X.3LG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LG implements ServiceConnection {
    public final /* synthetic */ RestoreFromBackupActivity A00;

    public AnonymousClass3LG(RestoreFromBackupActivity restoreFromBackupActivity) {
        this.A00 = restoreFromBackupActivity;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        RestoreFromBackupActivity restoreFromBackupActivity = this.A00;
        restoreFromBackupActivity.A0v.set(true);
        restoreFromBackupActivity.A0k.open();
        restoreFromBackupActivity.A0I.A02(restoreFromBackupActivity.A0n);
        C25701Ak r0 = restoreFromBackupActivity.A0C;
        r0.A00.A03(restoreFromBackupActivity.A0o);
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        RestoreFromBackupActivity restoreFromBackupActivity = this.A00;
        restoreFromBackupActivity.A0h = false;
        if (!restoreFromBackupActivity.A0v.compareAndSet(true, false)) {
            AnonymousClass10J r0 = restoreFromBackupActivity.A0I;
            r0.A01.A04(restoreFromBackupActivity.A0n);
            C25701Ak r02 = restoreFromBackupActivity.A0C;
            r02.A00.A04(restoreFromBackupActivity.A0o);
            restoreFromBackupActivity.A0k.close();
        }
    }
}
