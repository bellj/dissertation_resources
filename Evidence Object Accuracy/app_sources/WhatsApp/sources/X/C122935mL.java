package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.5mL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122935mL extends C122985mQ {
    public int A00;
    public int A01;
    public View.OnClickListener A02;
    public ViewGroup.MarginLayoutParams A03;
    public String A04;
    public boolean A05;
    public boolean A06 = false;

    public C122935mL() {
        super(1000);
    }

    public static C122935mL A00(View.OnClickListener onClickListener, ViewGroup.MarginLayoutParams marginLayoutParams, String str, int i, boolean z) {
        C122935mL r1 = new C122935mL();
        r1.A00 = i;
        r1.A01 = R.color.fb_pay_hub_icon_tint;
        r1.A04 = str;
        r1.A02 = onClickListener;
        r1.A05 = z;
        r1.A03 = marginLayoutParams;
        return r1;
    }
}
