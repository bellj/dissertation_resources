package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2h3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54642h3 extends AbstractC018308n {
    public boolean A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public /* synthetic */ C54642h3(int i, int i2, int i3) {
        this.A03 = i;
        this.A01 = i2;
        this.A02 = i3;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r8, RecyclerView recyclerView) {
        int i;
        int length;
        super.A01(rect, view, r8, recyclerView);
        int A00 = RecyclerView.A00(view);
        boolean z = this.A00;
        int width = recyclerView.getWidth();
        if (z) {
            int i2 = this.A01;
            length = AnonymousClass33R.A01.length;
            i = (width - (i2 * length)) >> 1;
        } else {
            i = this.A02;
            length = AnonymousClass33R.A01.length;
            int i3 = ((width - (i << 1)) - (this.A03 * length)) / ((length << 1) - 2);
            rect.left = i3;
            rect.right = i3;
        }
        if (A00 == 0) {
            rect.left = i;
        } else if (A00 == length - 1) {
            rect.right = i;
        }
    }
}
