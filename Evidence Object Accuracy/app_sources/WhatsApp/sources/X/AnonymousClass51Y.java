package X;

/* renamed from: X.51Y  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass51Y implements AnonymousClass5T2 {
    public AnonymousClass0BD A00;
    public boolean A01;

    public AnonymousClass51Y(boolean z) {
        this.A01 = z;
    }

    @Override // X.AnonymousClass5T2
    public boolean Abl(C14260l7 r3, Object obj, int i) {
        if (i != 38) {
            return false;
        }
        boolean A02 = C64983Hr.A02(obj);
        this.A01 = A02;
        AnonymousClass0BD r0 = this.A00;
        if (r0 == null) {
            return true;
        }
        r0.setRefreshing(A02);
        return true;
    }
}
