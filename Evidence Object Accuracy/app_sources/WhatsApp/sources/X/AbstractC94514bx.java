package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.4bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94514bx {
    public static boolean[] A0J;
    public static boolean[] A0K;
    public static boolean[] A0L;
    public static boolean[] A0M;
    public static boolean[] A0N;
    public char A00;
    public int A01;
    public Object A02;
    public String A03;
    public String A04;
    public C92284Vg A05;
    public final AnonymousClass4X0 A06 = new AnonymousClass4X0();
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;

    public abstract Object A09(boolean[] zArr);

    static {
        boolean[] zArr = new boolean[126];
        A0J = zArr;
        boolean[] zArr2 = new boolean[126];
        A0K = zArr2;
        boolean[] zArr3 = new boolean[126];
        A0L = zArr3;
        boolean[] zArr4 = new boolean[126];
        A0M = zArr4;
        boolean[] zArr5 = new boolean[126];
        A0N = zArr5;
        zArr3[26] = true;
        zArr3[58] = true;
        zArr4[26] = true;
        zArr4[125] = true;
        zArr4[44] = true;
        zArr2[26] = true;
        zArr2[93] = true;
        zArr2[44] = true;
        zArr5[26] = true;
        zArr[58] = true;
        zArr[44] = true;
        zArr[26] = true;
        zArr[125] = true;
        zArr[93] = true;
    }

    public AbstractC94514bx(int i) {
        boolean z = false;
        this.A08 = C12960it.A1U(i & 4);
        this.A09 = C12960it.A1U(i & 2);
        this.A0A = C12960it.A1U(i & 1);
        this.A0E = C12960it.A1U(i & 8);
        this.A0I = C12960it.A1U(i & 16);
        this.A07 = C12960it.A1U(i & 32);
        this.A0B = C12960it.A1U(i & 64);
        this.A0H = C12960it.A1U(i & 128);
        this.A0C = C12980iv.A1V(i & 768, 768);
        this.A0D = C12960it.A1T(i & 512);
        this.A0F = C12960it.A1U(i & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        this.A0G = (i & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) > 0 ? true : z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003b, code lost:
        if (r2 == ':') goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003f, code lost:
        if (r2 == ']') goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0043, code lost:
        if (r2 == '}') goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0045, code lost:
        r2 = A02(r8, X.AbstractC94514bx.A0K);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004d, code lost:
        if ((r8 instanceof X.C114475Lr) != false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0051, code lost:
        if ((r8 instanceof X.C114485Ls) != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0055, code lost:
        if ((r8 instanceof X.C114465Lq) != false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0059, code lost:
        if ((r8 instanceof X.C114445Lo) != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005d, code lost:
        if ((r8 instanceof X.C114495Lt) != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x005f, code lost:
        r1 = X.C12960it.A0k("Invalid or non Implemented status");
        r1.append(" addValue(Object current, Object value) in ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0076, code lost:
        throw X.C12990iw.A0m(X.C12970iu.A0s(r8.getClass(), r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0077, code lost:
        ((java.util.List) r3).add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x007e, code lost:
        ((java.util.AbstractCollection) r3).add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009b, code lost:
        if (r5 == false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x009f, code lost:
        if (r7.A0B != false) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a7, code lost:
        throw X.AnonymousClass5LL.A00(r2, r7.A01, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00a8, code lost:
        A04();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00af, code lost:
        return r8.A03(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b6, code lost:
        throw X.AnonymousClass5LL.A00(r2, r7.A01, 0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A00(X.AnonymousClass4YL r8) {
        /*
            r7 = this;
            java.lang.Object r3 = r8.A01()
            char r1 = r7.A00
            r0 = 91
            if (r1 != r0) goto L_0x00c3
            r7.A04()
            char r2 = r7.A00
            r4 = 44
            r1 = 0
            if (r2 != r4) goto L_0x001f
            boolean r0 = r7.A0B
            if (r0 != 0) goto L_0x001f
            int r0 = r7.A01
            X.4CH r0 = X.AnonymousClass5LL.A00(r2, r0, r1)
            throw r0
        L_0x001f:
            r6 = 1
        L_0x0020:
            r5 = 0
        L_0x0021:
            char r2 = r7.A00
            r0 = 9
            if (r2 == r0) goto L_0x0097
            r0 = 10
            if (r2 == r0) goto L_0x0097
            r0 = 13
            if (r2 == r0) goto L_0x0097
            r0 = 26
            if (r2 == r0) goto L_0x00b7
            r0 = 32
            if (r2 == r0) goto L_0x0097
            if (r2 == r4) goto L_0x0085
            r0 = 58
            if (r2 == r0) goto L_0x00b0
            r0 = 93
            if (r2 == r0) goto L_0x009b
            r0 = 125(0x7d, float:1.75E-43)
            if (r2 == r0) goto L_0x00b0
            boolean[] r0 = X.AbstractC94514bx.A0K
            java.lang.Object r2 = r7.A02(r8, r0)
            boolean r0 = r8 instanceof X.C114475Lr
            if (r0 != 0) goto L_0x007e
            boolean r0 = r8 instanceof X.C114485Ls
            if (r0 != 0) goto L_0x0077
            boolean r0 = r8 instanceof X.C114465Lq
            if (r0 != 0) goto L_0x007e
            boolean r0 = r8 instanceof X.C114445Lo
            if (r0 != 0) goto L_0x0077
            boolean r0 = r8 instanceof X.C114495Lt
            if (r0 != 0) goto L_0x0077
            java.lang.String r0 = "Invalid or non Implemented status"
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = " addValue(Object current, Object value) in "
            r1.append(r0)
            java.lang.Class r0 = r8.getClass()
            java.lang.String r0 = X.C12970iu.A0s(r0, r1)
            java.lang.RuntimeException r0 = X.C12990iw.A0m(r0)
            throw r0
        L_0x0077:
            r0 = r3
            java.util.List r0 = (java.util.List) r0
            r0.add(r2)
            goto L_0x0020
        L_0x007e:
            r0 = r3
            java.util.AbstractCollection r0 = (java.util.AbstractCollection) r0
            r0.add(r2)
            goto L_0x0020
        L_0x0085:
            if (r5 == 0) goto L_0x0092
            boolean r0 = r7.A0B
            if (r0 != 0) goto L_0x0092
            int r0 = r7.A01
            X.4CH r0 = X.AnonymousClass5LL.A00(r2, r0, r1)
            throw r0
        L_0x0092:
            r7.A04()
            r5 = 1
            goto L_0x0021
        L_0x0097:
            r7.A04()
            goto L_0x0021
        L_0x009b:
            if (r5 == 0) goto L_0x00a8
            boolean r0 = r7.A0B
            if (r0 != 0) goto L_0x00a8
            int r0 = r7.A01
            X.4CH r0 = X.AnonymousClass5LL.A00(r2, r0, r1)
            throw r0
        L_0x00a8:
            r7.A04()
            java.lang.Object r0 = r8.A03(r3)
            return r0
        L_0x00b0:
            int r0 = r7.A01
            X.4CH r0 = X.AnonymousClass5LL.A00(r2, r0, r1)
            throw r0
        L_0x00b7:
            int r3 = r7.A01
            int r3 = r3 - r6
            r2 = 3
            java.lang.String r1 = "EOF"
            X.4CH r0 = new X.4CH
            r0.<init>(r1, r3, r2)
            throw r0
        L_0x00c3:
            java.lang.String r0 = "Internal Error"
            java.lang.RuntimeException r0 = X.C12990iw.A0m(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC94514bx.A00(X.4YL):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:80:0x011d, code lost:
        A04();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0124, code lost:
        return r14.A03(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x012b, code lost:
        throw X.AnonymousClass5LL.A00(r1, r13.A01, 0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x011d A[EDGE_INSN: B:97:0x011d->B:80:0x011d ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A01(X.AnonymousClass4YL r14) {
        /*
        // Method dump skipped, instructions count: 307
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC94514bx.A01(X.4YL):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0093, code lost:
        throw X.AnonymousClass5LL.A00(r3, r4.A01, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00fd, code lost:
        return A09(r6);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A02(X.AnonymousClass4YL r5, boolean[] r6) {
        /*
        // Method dump skipped, instructions count: 478
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC94514bx.A02(X.4YL, boolean[]):java.lang.Object");
    }

    public void A03() {
        String str = this.A04;
        int length = str.length();
        if (length == 1) {
            return;
        }
        if (length != 2) {
            char charAt = str.charAt(0);
            char charAt2 = this.A04.charAt(1);
            if (charAt == '-') {
                char charAt3 = this.A04.charAt(2);
                if (charAt2 == '0' && charAt3 >= '0' && charAt3 <= '9') {
                    throw AnonymousClass5LL.A01(this, 6);
                }
            } else if (charAt == '0' && charAt2 >= '0' && charAt2 <= '9') {
                throw AnonymousClass5LL.A01(this, 6);
            }
        } else if (str.equals("00")) {
            throw new AnonymousClass4CH(str, this.A01, 6);
        }
    }

    public void A04() {
        char charAt;
        AnonymousClass5LL r2 = (AnonymousClass5LL) this;
        int i = ((AbstractC94514bx) r2).A01 + 1;
        ((AbstractC94514bx) r2).A01 = i;
        if (i >= r2.A00) {
            charAt = 26;
        } else {
            charAt = r2.A01.charAt(i);
        }
        ((AbstractC94514bx) r2).A00 = charAt;
    }

    public void A05() {
        char charAt;
        AnonymousClass5LL r2 = (AnonymousClass5LL) this;
        int i = ((AbstractC94514bx) r2).A01 + 1;
        ((AbstractC94514bx) r2).A01 = i;
        if (i >= r2.A00) {
            charAt = 26;
        } else {
            charAt = r2.A01.charAt(i);
        }
        ((AbstractC94514bx) r2).A00 = charAt;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:80:0x00ed, code lost:
        if (r2 < '0') goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00f7, code lost:
        if (r2 < 'A') goto L_0x00f9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
        // Method dump skipped, instructions count: 422
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC94514bx.A06():void");
    }

    public void A07() {
        while (true) {
            char c = this.A00;
            if (c <= ' ' && c != 26) {
                A05();
            } else {
                return;
            }
        }
    }

    public void A08(boolean[] zArr) {
        while (true) {
            char c = this.A00;
            if (c == 26) {
                return;
            }
            if (c < 0 || c >= '~' || !zArr[c]) {
                A05();
            } else {
                return;
            }
        }
    }
}
