package X;

import com.whatsapp.group.GroupCallButtonController;

/* renamed from: X.3c2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70903c2 implements AbstractC47972Dm {
    public final /* synthetic */ GroupCallButtonController A00;

    @Override // X.AbstractC47972Dm
    public void ANV() {
    }

    public C70903c2(GroupCallButtonController groupCallButtonController) {
        this.A00 = groupCallButtonController;
    }

    @Override // X.AbstractC47972Dm
    public void ANX(AnonymousClass1YT r4) {
        StringBuilder A0k = C12960it.A0k("GroupCallButtonController/onCallLogUpdated groupJid: ");
        GroupCallButtonController groupCallButtonController = this.A00;
        A0k.append(groupCallButtonController.A03);
        C12960it.A1F(A0k);
        if (groupCallButtonController.A03.equals(r4.A04)) {
            if (!C29941Vi.A00(r4.A06, groupCallButtonController.A07)) {
                groupCallButtonController.A07 = r4.A06;
                AnonymousClass1s3 r0 = groupCallButtonController.A02;
                if (r0 != null) {
                    r0.A00.A02();
                }
            }
            if (groupCallButtonController.A07 == null) {
                r4 = null;
            }
            groupCallButtonController.A04 = r4;
        }
    }
}
