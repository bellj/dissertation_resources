package X;

import android.content.LocusId;

/* renamed from: X.0Qh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05620Qh {
    public static LocusId A00(String str) {
        return new LocusId(str);
    }

    public static String A01(LocusId locusId) {
        return locusId.getId();
    }
}
