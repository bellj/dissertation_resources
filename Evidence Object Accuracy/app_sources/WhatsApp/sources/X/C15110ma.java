package X;

import android.app.job.JobParameters;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;

/* renamed from: X.0ma  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C15110ma {
    public static Boolean A02;
    public final Context A00;
    public final Handler A01 = new HandlerC73363g7();

    public C15110ma(Context context) {
        this.A00 = context;
    }

    public static boolean A00(Context context) {
        C13020j0.A01(context);
        Boolean bool = A02;
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean z = false;
        try {
            ServiceInfo serviceInfo = context.getPackageManager().getServiceInfo(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsService"), 0);
            if (serviceInfo != null) {
                if (serviceInfo.enabled) {
                    z = true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        A02 = Boolean.valueOf(z);
        return z;
    }

    public final void A01(JobParameters jobParameters) {
        Context context = this.A00;
        C56582lF r3 = C14160kx.A00(context).A0C;
        C14160kx.A01(r3);
        String string = jobParameters.getExtras().getString("action");
        r3.A0D("Local AnalyticsJobService called. action", string);
        if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(string)) {
            RunnableC76113l5 r2 = new Runnable(jobParameters, r3, this) { // from class: X.3l5
                public final /* synthetic */ JobParameters A00;
                public final /* synthetic */ C56582lF A01;
                public final /* synthetic */ C15110ma A02;

                {
                    this.A02 = r3;
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.A02.A02(this.A00, this.A01);
                }
            };
            C56572lE r1 = C14160kx.A00(context).A06;
            C14160kx.A01(r1);
            r1.A0H(new C108714zX(this, r2));
        }
    }

    public final /* synthetic */ void A02(JobParameters jobParameters, C56582lF r4) {
        r4.A09("AnalyticsJobService processed last dispatch request");
        ((AbstractC116385Vf) this.A00).Ago(jobParameters, false);
    }

    public final void A03(Intent intent, int i) {
        try {
            synchronized (C15060mU.A02) {
                C15130mc r1 = C15060mU.A00;
                if (r1 != null && r1.A03()) {
                    r1.A00();
                }
            }
        } catch (SecurityException unused) {
        }
        Context context = this.A00;
        C56582lF r3 = C14160kx.A00(context).A0C;
        C14160kx.A01(r3);
        if (intent == null) {
            r3.A0A("AnalyticsService started with null intent");
            return;
        }
        String action = intent.getAction();
        C15050mT.A06(r3, Integer.valueOf(i), action, null, "Local AnalyticsService called. startId, action", 2);
        if ("com.google.android.gms.analytics.ANALYTICS_DISPATCH".equals(action)) {
            RunnableBRunnable0Shape0S0201000_I0 runnableBRunnable0Shape0S0201000_I0 = new RunnableBRunnable0Shape0S0201000_I0(this, r3, i, 0);
            C56572lE r12 = C14160kx.A00(context).A06;
            C14160kx.A01(r12);
            r12.A0H(new C108714zX(this, runnableBRunnable0Shape0S0201000_I0));
        }
    }
}
