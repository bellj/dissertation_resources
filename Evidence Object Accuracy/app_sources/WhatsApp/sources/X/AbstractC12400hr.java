package X;

import android.animation.Animator;

/* renamed from: X.0hr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12400hr {
    @Override // android.animation.Animator.AnimatorPauseListener, X.AbstractC12400hr
    void onAnimationPause(Animator animator);

    @Override // android.animation.Animator.AnimatorPauseListener, X.AbstractC12400hr
    void onAnimationResume(Animator animator);
}
