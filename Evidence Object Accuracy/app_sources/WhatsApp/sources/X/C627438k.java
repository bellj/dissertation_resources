package X;

import java.lang.ref.WeakReference;

/* renamed from: X.38k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C627438k extends AbstractC16350or {
    public final int A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass01d A03;
    public final C16590pI A04;
    public final C14820m6 A05;
    public final C25961Bm A06;
    public final C20800wL A07;
    public final C863046q A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final WeakReference A0F;
    public final boolean A0G;
    public final boolean A0H;

    public C627438k(AnonymousClass01d r2, C16590pI r3, C14820m6 r4, C25961Bm r5, C20800wL r6, AbstractC44431yx r7, C863046q r8, String str, String str2, String str3, String str4, String str5, String str6, int i, int i2, int i3, boolean z, boolean z2) {
        this.A0G = z;
        this.A0B = str;
        this.A0D = str2;
        this.A0C = str3;
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
        this.A0E = str4;
        this.A0A = str5;
        this.A04 = r3;
        this.A08 = r8;
        this.A0H = z2;
        this.A09 = str6;
        this.A03 = r2;
        this.A0F = C12970iu.A10(r7);
        this.A07 = r6;
        this.A05 = r4;
        this.A06 = r5;
        if (str3.equals("sms")) {
            AnonymousClass009.A04(str5);
        }
    }
}
