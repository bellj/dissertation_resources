package X;

import android.content.res.ColorStateList;
import android.widget.CompoundButton;

/* renamed from: X.0iK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12680iK {
    void Ad3(ColorStateList colorStateList, boolean z);

    int getMeasuredHeight();

    int getMeasuredWidth();

    void measure(int i, int i2);

    void setChecked(boolean z);

    void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener);

    @Override // X.AbstractC12680iK
    void setThumbTintList(ColorStateList colorStateList);
}
