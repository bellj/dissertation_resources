package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.group.view.custom.GroupDetailsCard;

/* renamed from: X.1s4  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1s4 extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass1s4(Context context) {
        super(context);
        A00();
    }

    public AnonymousClass1s4(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AnonymousClass1s4(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        if (!this.A01) {
            this.A01 = true;
            GroupDetailsCard groupDetailsCard = (GroupDetailsCard) this;
            AnonymousClass01J r2 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            groupDetailsCard.A04 = (C15570nT) r2.AAr.get();
            groupDetailsCard.A0F = (AnonymousClass19M) r2.A6R.get();
            groupDetailsCard.A0N = (AnonymousClass19Z) r2.A2o.get();
            groupDetailsCard.A03 = (AnonymousClass12P) r2.A0H.get();
            groupDetailsCard.A07 = (C15550nR) r2.A45.get();
            groupDetailsCard.A08 = (C15610nY) r2.AMe.get();
            groupDetailsCard.A0J = (C20710wC) r2.A8m.get();
            groupDetailsCard.A0M = (AnonymousClass12F) r2.AJM.get();
            groupDetailsCard.A0K = (AnonymousClass1E5) r2.AKs.get();
            groupDetailsCard.A0D = (C15600nX) r2.A8x.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
