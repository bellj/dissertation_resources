package X;

import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;

/* renamed from: X.0WS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WS implements View.OnTouchListener {
    public final /* synthetic */ AnonymousClass0XR A00;

    public AnonymousClass0WS(AnonymousClass0XR r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (action == 0) {
            AnonymousClass0XR r2 = this.A00;
            PopupWindow popupWindow = r2.A0D;
            if (popupWindow == null || !popupWindow.isShowing() || x < 0 || x >= popupWindow.getWidth() || y < 0 || y >= popupWindow.getHeight()) {
                return false;
            }
            r2.A0L.postDelayed(r2.A0P, 250);
            return false;
        } else if (action != 1) {
            return false;
        } else {
            AnonymousClass0XR r0 = this.A00;
            r0.A0L.removeCallbacks(r0.A0P);
            return false;
        }
    }
}
