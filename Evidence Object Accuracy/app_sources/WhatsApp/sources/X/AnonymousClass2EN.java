package X;

import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2EN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EN extends AnonymousClass2EI {
    public AnonymousClass4RN A00;
    public AnonymousClass2EJ A01;
    public final C14900mE A02;
    public final C90824Pj A03;
    public final C19870uo A04;
    public final C17220qS A05;
    public final C19840ul A06;

    public AnonymousClass2EN(C14900mE r1, C14650lo r2, C90824Pj r3, C19870uo r4, C17220qS r5, C19840ul r6) {
        super(r2);
        this.A02 = r1;
        this.A06 = r6;
        this.A05 = r5;
        this.A03 = r3;
        this.A04 = r4;
    }

    public void A02(AnonymousClass4RN r13, AnonymousClass2EJ r14) {
        this.A01 = r14;
        String A01 = this.A05.A01();
        this.A06.A03("cart_view_tag");
        C19870uo r5 = this.A04;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1V8("width", Integer.toString(r13.A01), (AnonymousClass1W9[]) null));
        arrayList.add(new AnonymousClass1V8("height", Integer.toString(r13.A00), (AnonymousClass1W9[]) null));
        AnonymousClass1V8 r0 = new AnonymousClass1V8("image_dimensions", (AnonymousClass1W9[]) null, (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0]));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(r0);
        for (String str : r13.A03) {
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new AnonymousClass1V8("id", str, (AnonymousClass1W9[]) null));
            arrayList2.add(new AnonymousClass1V8("product", (AnonymousClass1W9[]) null, (AnonymousClass1V8[]) arrayList3.toArray(new AnonymousClass1V8[0])));
        }
        C14650lo r02 = super.A01;
        UserJid userJid = r13.A02;
        String A012 = r02.A07.A01(userJid);
        if (A012 != null) {
            this.A00 = r13;
            arrayList2.add(new AnonymousClass1V8("direct_connection_encrypted_info", A012, (AnonymousClass1W9[]) null));
        }
        r5.A01(this, new AnonymousClass1V8(new AnonymousClass1V8("cart", new AnonymousClass1W9[]{new AnonymousClass1W9("op", "refresh"), new AnonymousClass1W9("biz_jid", userJid.getRawString())}, (AnonymousClass1V8[]) arrayList2.toArray(new AnonymousClass1V8[0])), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("smax_id", "11"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "fb:thrift_iq"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9(AnonymousClass1VY.A00, "to")}), A01, 253);
        StringBuilder sb = new StringBuilder("RefreshCartProtocol/sendRefreshCartRequest/biz_jid=");
        sb.append(userJid);
        Log.i(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A06.A02("cart_view_tag");
        this.A01.APm(new Pair(2, "delivery failed"));
        Log.i("RefreshCartProtocol/onDeliveryFailure");
        this.A00 = null;
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        StringBuilder sb = new StringBuilder("RefreshCartProtocol/onDirectConnectionError/jid=");
        sb.append(userJid.getRawString());
        Log.e(sb.toString());
        if (this.A01 != null) {
            this.A02.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(this, 5));
        }
        this.A00 = null;
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        if (this.A00 == null) {
            Log.e("RefreshCartProtocol/onDirectConnectionSucceeded/directConnectionRequestToRetry is null");
            return;
        }
        StringBuilder sb = new StringBuilder("RefreshCartProtocol/onDirectConnectionSucceeded/jid=");
        sb.append(userJid.getRawString());
        Log.i(sb.toString());
        A02(this.A00, this.A01);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        this.A06.A02("cart_view_tag");
        Pair A01 = C41151sz.A01(r5);
        AnonymousClass4RN r0 = this.A00;
        if (r0 == null || A01 == null || !A01(r0.A02, ((Number) A01.first).intValue())) {
            this.A00 = null;
            this.A02.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 7, A01));
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r27, String str) {
        C90074Mm r4;
        String A0G;
        C44731zS r10;
        this.A06.A02("cart_view_tag");
        C90824Pj r3 = this.A03;
        AnonymousClass1V8 A0E = r27.A0E("cart");
        if (A0E != null) {
            List<AnonymousClass1V8> A0J = A0E.A0J("product");
            ArrayList arrayList = new ArrayList();
            for (AnonymousClass1V8 r6 : A0J) {
                C44691zO A02 = r3.A01.A02(r6);
                if (A02 == null) {
                    AnonymousClass1V8 A0E2 = r6.A0E("id");
                    AnonymousClass1V8 A0E3 = r6.A0E("status");
                    if (A0E2 == null) {
                        A0G = null;
                    } else {
                        A0G = A0E2.A0G();
                    }
                    if (A0E3 == null) {
                        r10 = null;
                    } else {
                        r10 = new C44731zS(null, null, null, 3, false);
                    }
                    if (A0G != null) {
                        A02 = new C44691zO(null, r10, null, null, A0G, "", null, null, null, null, null, new ArrayList(), 0, 99, true, false);
                    }
                }
                arrayList.add(A02);
            }
            r4 = new C90074Mm(r3.A02.A00(A0E.A0E("price")), arrayList);
        } else {
            r4 = null;
        }
        this.A00 = null;
        this.A02.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 8, r4));
    }
}
