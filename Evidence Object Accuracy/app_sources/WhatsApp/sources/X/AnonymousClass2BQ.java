package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Map;

/* renamed from: X.2BQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2BQ extends AnonymousClass2BS {
    public boolean A00 = false;
    public final C47632Bt A01;
    public final String A02;

    public AnonymousClass2BQ(C14820m6 r8, C14850m9 r9, AnonymousClass16D r10, AnonymousClass23W r11, AnonymousClass16F r12, RandomAccessFile randomAccessFile, String str, int i) {
        super(r11, r12, randomAccessFile, i, 2);
        this.A01 = new C47632Bt(r8, r9, r10, r12);
        this.A02 = str;
    }

    public void A01(ByteBuffer byteBuffer, boolean z) {
        AnonymousClass2CC e;
        StringBuilder sb;
        String str;
        int i;
        int limit = byteBuffer.limit();
        C47632Bt r8 = this.A01;
        r8.A04 = new byte[10240];
        r8.A00 = 0;
        byteBuffer.position(0);
        byteBuffer.position(C94114bH.A06.length);
        byte[] bArr = new byte[EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH];
        try {
            int position = byteBuffer.position();
            String str2 = null;
            boolean z2 = false;
            int i2 = 0;
            boolean z3 = false;
            int i3 = 0;
            while (true) {
                if (position < limit) {
                    try {
                        AnonymousClass04W A05 = AnonymousClass02X.A05(byteBuffer);
                        int i4 = A05.A01;
                        if (i4 == 0) {
                            if (A05.A00 == 6005) {
                                str2 = (String) A05.A02;
                                int position2 = byteBuffer.position() - position;
                                byteBuffer.position(position);
                                byteBuffer.get(bArr, 0, position2);
                                z2 = r8.A02(bArr, position2, str2);
                            } else {
                                int position3 = byteBuffer.position() - position;
                                byteBuffer.position(position);
                                byteBuffer.get(bArr, 0, position3);
                                for (Map.Entry entry : r8.A03.entrySet()) {
                                    ((C47682By) entry.getValue()).A06(bArr, position3);
                                }
                                if (position3 + 0 > 2048) {
                                    position3 = EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                }
                                int i5 = r8.A00;
                                if (position3 > 10240 - i5) {
                                    Log.e("privatestatsuploadqueue/writetoCommonAttrBuffer too many common attributes");
                                    z2 = false;
                                } else {
                                    System.arraycopy(bArr, 0, r8.A04, i5, position3);
                                    r8.A00 += position3;
                                    z2 = true;
                                }
                            }
                            i2 = position;
                        } else if (i4 == 1) {
                            i = byteBuffer.position();
                            if ((byteBuffer.get(position) & 4) != 0) {
                                int i6 = i - position;
                                byteBuffer.position(position);
                                byteBuffer.get(bArr, 0, i6);
                                z2 = r8.A02(bArr, i6, str2);
                                byteBuffer.position(i);
                                i2 = i3;
                                z3 = false;
                            } else {
                                i3 = position;
                                z3 = true;
                            }
                        } else if (i4 == 2 && (byteBuffer.get(position) & 4) != 0 && z3) {
                            i = byteBuffer.position();
                            int i7 = i - i3;
                            byteBuffer.position(i3);
                            if (i7 <= 2048) {
                                byteBuffer.get(bArr, 0, i7);
                                z2 = r8.A02(bArr, i7, str2);
                            } else {
                                byte[] bArr2 = new byte[i7];
                                byteBuffer.get(bArr2, 0, i7);
                                z2 = r8.A02(bArr2, i7, str2);
                            }
                            byteBuffer.position(i);
                            i2 = i3;
                            z3 = false;
                        }
                        if (!z2) {
                            break;
                        }
                        position = byteBuffer.position();
                    } catch (BufferUnderflowException unused) {
                        throw new AnonymousClass2CC("Incomplete buffer");
                    }
                } else if (z2) {
                    r8.A04 = null;
                    r8.A00 = 0;
                    return;
                }
            }
            if (!z) {
                r8.A04 = null;
                r8.A00 = 0;
            } else {
                AnonymousClass1NA A00 = A00(1);
                try {
                    int i8 = r8.A00;
                    if (i8 > 0) {
                        A00.A03(r8.A04, i8);
                        r8.A04 = null;
                        r8.A00 = 0;
                    }
                    int limit2 = byteBuffer.limit() - i2;
                    byteBuffer.position(i2);
                    if (limit2 <= 2048) {
                        byteBuffer.get(bArr, 0, limit2);
                        A00.A03(bArr, limit2);
                    } else {
                        byte[] bArr3 = new byte[limit2];
                        byteBuffer.get(bArr3, 0, limit2);
                        A00.A03(bArr3, limit2);
                    }
                    try {
                        A00.A01();
                        this.A00 = true;
                        return;
                    } catch (IOException e2) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("privatestatseventbuffermanager/splitBuffer: ioexception while flushing back buffer");
                        sb2.append(e2.toString());
                        Log.e(sb2.toString());
                        return;
                    }
                } catch (IndexOutOfBoundsException e3) {
                    StringBuilder sb3 = new StringBuilder("privatestatseventbuffermanager/splitBuffer: unexpected runtime exception when writing to back buffer ");
                    sb3.append(e3.toString());
                    Log.e(sb3.toString());
                    try {
                        A00.A00();
                        A00.A01();
                    } catch (Exception e4) {
                        StringBuilder sb4 = new StringBuilder("privatestatseventbuffermanager/splitBuffer see exception when clearing the back buffer ");
                        sb4.append(e4.toString());
                        Log.e(sb4.toString());
                    }
                }
            }
            this.A00 = false;
        } catch (AnonymousClass2CC e5) {
            e = e5;
            sb = new StringBuilder();
            str = "privatestatseventbuffermanager/splitbuffer invalid buf content";
            sb.append(str);
            sb.append(e.toString());
            Log.e(sb.toString());
        } catch (Throwable th) {
            e = th;
            sb = new StringBuilder();
            str = "privateStatseventbuffermanager/splitbuffer unexpected errors ";
            sb.append(str);
            sb.append(e.toString());
            Log.e(sb.toString());
        }
    }

    @Override // X.AnonymousClass2BS, X.AnonymousClass1N9
    public void AIm() {
        super.AIm();
        this.A01.A00(this.A02);
    }

    @Override // X.AnonymousClass2BS, X.AnonymousClass1N9
    public void AIx() {
        super.AIx();
        this.A01.A00(this.A02);
    }
}
