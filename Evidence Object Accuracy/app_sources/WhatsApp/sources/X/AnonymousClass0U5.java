package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.whatsapp.R;

/* renamed from: X.0U5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0U5 {
    public int A00;
    public int A01 = -1;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public Drawable A07;
    public Handler A08;
    public Message A09;
    public Message A0A;
    public Message A0B;
    public View A0C;
    public View A0D;
    public Button A0E;
    public Button A0F;
    public Button A0G;
    public ImageView A0H;
    public ListAdapter A0I;
    public ListView A0J;
    public TextView A0K;
    public TextView A0L;
    public NestedScrollView A0M;
    public CharSequence A0N;
    public CharSequence A0O;
    public CharSequence A0P;
    public CharSequence A0Q;
    public CharSequence A0R;
    public boolean A0S;
    public final Context A0T;
    public final View.OnClickListener A0U = new AnonymousClass0WD(this);
    public final Window A0V;
    public final AnonymousClass04T A0W;

    public AnonymousClass0U5(Context context, Window window, AnonymousClass04T r7) {
        this.A0T = context;
        this.A0W = r7;
        this.A0V = window;
        this.A08 = new AnonymousClass0AP(r7);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, AnonymousClass07O.A04, R.attr.alertDialogStyle, 0);
        this.A00 = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.getResourceId(2, 0);
        this.A03 = obtainStyledAttributes.getResourceId(4, 0);
        this.A04 = obtainStyledAttributes.getResourceId(5, 0);
        this.A05 = obtainStyledAttributes.getResourceId(7, 0);
        this.A02 = obtainStyledAttributes.getResourceId(3, 0);
        this.A0S = obtainStyledAttributes.getBoolean(6, true);
        obtainStyledAttributes.getDimensionPixelSize(1, 0);
        obtainStyledAttributes.recycle();
        r7.A01();
    }

    public static final ViewGroup A00(View view, View view2) {
        if (view == null) {
            if (view2 instanceof ViewStub) {
                view2 = ((ViewStub) view2).inflate();
            }
            return (ViewGroup) view2;
        }
        if (view2 != null) {
            ViewParent parent = view2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view2);
            }
        }
        if (view instanceof ViewStub) {
            view = ((ViewStub) view).inflate();
        }
        return (ViewGroup) view;
    }

    public static void A01(View view, View view2, View view3) {
        int i = 0;
        if (view2 != null) {
            int i2 = 4;
            if (view.canScrollVertically(-1)) {
                i2 = 0;
            }
            view2.setVisibility(i2);
        }
        if (view3 != null) {
            if (!view.canScrollVertically(1)) {
                i = 4;
            }
            view3.setVisibility(i);
        }
    }

    public static boolean A02(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            while (childCount > 0) {
                childCount--;
                if (A02(viewGroup.getChildAt(childCount))) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0144, code lost:
        if (r9.getVisibility() == 8) goto L_0x0146;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0043, code lost:
        if (r11 != null) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x004a, code lost:
        if (A02(r11) == false) goto L_0x004c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0238  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x02b4  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x02b9  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x02c9  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x02d9  */
    /* JADX WARNING: Removed duplicated region for block: B:140:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x011c  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x017e  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01a7  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01c2  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01e1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 831
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0U5.A03():void");
    }
}
