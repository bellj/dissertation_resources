package X;

import android.view.View;

/* renamed from: X.0Pv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05510Pv {
    public int A00;
    public int A01;
    public AbstractC06220Sq A02;
    public boolean A03;
    public boolean A04;

    public C05510Pv() {
        A00();
    }

    public void A00() {
        this.A01 = -1;
        this.A00 = Integer.MIN_VALUE;
        this.A03 = false;
        this.A04 = false;
    }

    public void A01(View view, int i) {
        int A0B;
        int A07;
        boolean z = this.A03;
        AbstractC06220Sq r0 = this.A02;
        if (z) {
            int A08 = r0.A08(view);
            AbstractC06220Sq r2 = this.A02;
            if (Integer.MIN_VALUE == r2.A00) {
                A07 = 0;
            } else {
                A07 = r2.A07() - r2.A00;
            }
            A0B = A08 + A07;
        } else {
            A0B = r0.A0B(view);
        }
        this.A00 = A0B;
        this.A01 = i;
    }

    public void A02(View view, int i) {
        int A07;
        int min;
        AbstractC06220Sq r2 = this.A02;
        if (Integer.MIN_VALUE == r2.A00 || (A07 = r2.A07() - r2.A00) >= 0) {
            A01(view, i);
            return;
        }
        this.A01 = i;
        boolean z = this.A03;
        AbstractC06220Sq r0 = this.A02;
        if (z) {
            int A02 = (r0.A02() - A07) - this.A02.A08(view);
            this.A00 = this.A02.A02() - A02;
            if (A02 > 0) {
                int A09 = this.A00 - this.A02.A09(view);
                int A06 = this.A02.A06();
                int min2 = A09 - (A06 + Math.min(this.A02.A0B(view) - A06, 0));
                if (min2 < 0) {
                    min = this.A00 + Math.min(A02, -min2);
                } else {
                    return;
                }
            } else {
                return;
            }
        } else {
            int A0B = r0.A0B(view);
            int A062 = A0B - this.A02.A06();
            this.A00 = A0B;
            if (A062 > 0) {
                int A022 = (this.A02.A02() - Math.min(0, (this.A02.A02() - A07) - this.A02.A08(view))) - (A0B + this.A02.A09(view));
                if (A022 < 0) {
                    min = this.A00 - Math.min(A062, -A022);
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        this.A00 = min;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AnchorInfo{mPosition=");
        sb.append(this.A01);
        sb.append(", mCoordinate=");
        sb.append(this.A00);
        sb.append(", mLayoutFromEnd=");
        sb.append(this.A03);
        sb.append(", mValid=");
        sb.append(this.A04);
        sb.append('}');
        return sb.toString();
    }
}
