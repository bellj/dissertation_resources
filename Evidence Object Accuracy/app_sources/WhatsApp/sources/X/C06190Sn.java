package X;

import android.os.Build;
import android.view.DisplayCutout;

/* renamed from: X.0Sn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06190Sn {
    public final DisplayCutout A00;

    public C06190Sn(DisplayCutout displayCutout) {
        this.A00 = displayCutout;
    }

    public static C06190Sn A00(DisplayCutout displayCutout) {
        if (displayCutout == null) {
            return null;
        }
        return new C06190Sn(displayCutout);
    }

    public int A01() {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06520Ty.A00(this.A00);
        }
        return 0;
    }

    public int A02() {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06520Ty.A01(this.A00);
        }
        return 0;
    }

    public int A03() {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06520Ty.A02(this.A00);
        }
        return 0;
    }

    public int A04() {
        if (Build.VERSION.SDK_INT >= 28) {
            return C06520Ty.A03(this.A00);
        }
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C06190Sn.class != obj.getClass()) {
            return false;
        }
        return C015407h.A01(this.A00, ((C06190Sn) obj).A00);
    }

    public int hashCode() {
        DisplayCutout displayCutout = this.A00;
        if (displayCutout == null) {
            return 0;
        }
        return displayCutout.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("DisplayCutoutCompat{");
        sb.append(this.A00);
        sb.append("}");
        return sb.toString();
    }
}
