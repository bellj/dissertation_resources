package X;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.33c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C619233c extends PhotoView {
    public final /* synthetic */ MediaViewFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C619233c(Context context, MediaViewFragment mediaViewFragment) {
        super(context);
        this.A00 = mediaViewFragment;
    }

    @Override // com.whatsapp.mediaview.PhotoView, android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        this.A00.A1M(C12960it.A1S((((PhotoView) this).A00 > this.A04 ? 1 : (((PhotoView) this).A00 == this.A04 ? 0 : -1))), true);
        return super.onDoubleTap(motionEvent);
    }

    @Override // com.whatsapp.mediaview.PhotoView, android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        this.A00.A1M(false, true);
        return super.onScaleBegin(scaleGestureDetector);
    }

    @Override // com.whatsapp.mediaview.PhotoView, android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        super.onScaleEnd(scaleGestureDetector);
        MediaViewFragment mediaViewFragment = this.A00;
        boolean z = false;
        if (((PhotoView) this).A00 <= this.A04) {
            z = true;
        }
        mediaViewFragment.A1M(z, true);
    }
}
