package X;

import android.widget.ImageView;

/* renamed from: X.2Vd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51512Vd {
    public AnonymousClass28F A00;
    public Object A01;
    public final float A02;
    public final int A03;
    public final ImageView A04;
    public final Object A05;

    public /* synthetic */ C51512Vd(ImageView imageView, AnonymousClass28F r2, Object obj, Object obj2, float f, int i) {
        this.A01 = obj;
        this.A04 = imageView;
        this.A05 = obj2;
        this.A00 = r2;
        this.A03 = i;
        this.A02 = f;
    }
}
