package X;

/* renamed from: X.24v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C461724v extends AbstractC16110oT {
    public Double A00;
    public Double A01;
    public Integer A02;
    public Long A03;

    public C461724v() {
        super(1638, new AnonymousClass00E(1, 50, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A03);
        r3.Abe(12, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamAndroidScrollPerfEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "frameDropsPerMin", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "largeFrameDropsPerMin", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "scrollDurationT", this.A03);
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "surface", obj);
        sb.append("}");
        return sb.toString();
    }
}
