package X;

import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1W4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1W4 implements AbstractC21730xt {
    public AnonymousClass1W6 A00;
    public final UserJid A01;
    public final C17220qS A02;

    public AnonymousClass1W4(UserJid userJid, C17220qS r2) {
        this.A01 = userJid;
        this.A02 = r2;
    }

    public void A00(AnonymousClass1W6 r15) {
        this.A00 = r15;
        C17220qS r7 = this.A02;
        String A01 = r7.A01();
        r7.A09(this, new AnonymousClass1V8(new AnonymousClass1V8("public_key", new AnonymousClass1W9[]{new AnonymousClass1W9("jid", this.A01.getRawString())}), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("xmlns", "w:biz:catalog"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("smax_id", "52"), new AnonymousClass1W9("id", A01)}), A01, 283, 32000);
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("GetBusinessPublicKeyProtocol/delivery-error with iqId ");
        sb.append(str);
        Log.w(sb.toString());
        AnonymousClass1W6 r1 = this.A00;
        if (r1 != null) {
            r1.AQu(this.A01);
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        StringBuilder sb = new StringBuilder("GetBusinessPublicKeyProtocol/onError with iqId ");
        sb.append(str);
        Log.w(sb.toString());
        Pair A01 = C41151sz.A01(r4);
        if (A01 != null) {
            StringBuilder sb2 = new StringBuilder("GetBusinessPublicKeyProtocol/onError error_code=");
            sb2.append(A01.first);
            Log.w(sb2.toString());
        }
        AnonymousClass1W6 r1 = this.A00;
        if (r1 != null) {
            r1.AQu(this.A01);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        AnonymousClass1V8 A0E;
        AnonymousClass1V8 A0E2 = r4.A0E("public_key");
        if (!(A0E2 == null || (A0E = A0E2.A0E("pem")) == null)) {
            String A0G = A0E.A0G();
            if (!TextUtils.isEmpty(A0G)) {
                AnonymousClass1W6 r1 = this.A00;
                if (r1 != null) {
                    UserJid userJid = this.A01;
                    AnonymousClass009.A05(A0G);
                    r1.AQv(userJid, A0G);
                    return;
                }
                return;
            }
        }
        AnonymousClass1W6 r12 = this.A00;
        if (r12 != null) {
            r12.AQu(this.A01);
        }
    }
}
