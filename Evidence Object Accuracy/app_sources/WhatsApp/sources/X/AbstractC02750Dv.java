package X;

import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;

/* renamed from: X.0Dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC02750Dv extends AbstractC117195Yx, ReadableByteChannel {
    C10730f6 A6i();

    C10730f6 AB0();

    long AIW(C08960c8 v);

    InputStream AIz();

    AbstractC02750Dv AZ1();

    boolean AaY(long j);

    int AbU(AnonymousClass5I3 v);

    byte readByte();
}
