package X;

/* renamed from: X.4BY  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BY {
    A05(0),
    A06(1),
    A03(2),
    A02(3),
    A01(4),
    A04(5);
    
    public final int value;

    AnonymousClass4BY(int i) {
        this.value = i;
    }

    public static AnonymousClass4BY A00(int i) {
        if (i == 0) {
            return A05;
        }
        if (i == 1) {
            return A06;
        }
        if (i == 2) {
            return A03;
        }
        if (i == 3) {
            return A02;
        }
        if (i == 4) {
            return A01;
        }
        if (i != 5) {
            return null;
        }
        return A04;
    }
}
