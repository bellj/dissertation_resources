package X;

import android.content.Context;
import android.os.Build;
import android.view.GestureDetector;

/* renamed from: X.0MU  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0MU {
    public final AbstractC12360hn A00;

    public AnonymousClass0MU(Context context, GestureDetector.OnGestureListener onGestureListener) {
        if (Build.VERSION.SDK_INT > 17) {
            this.A00 = new AnonymousClass0Y3(context, onGestureListener);
        } else {
            this.A00 = new AnonymousClass0Y4(context, onGestureListener);
        }
    }
}
