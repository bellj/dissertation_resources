package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.DynamicButtonsLayout;
import com.whatsapp.conversation.conversationrow.DynamicButtonsRowContentLayout;

/* renamed from: X.2yE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60562yE extends C60792ye {
    public boolean A00;
    public final DynamicButtonsLayout A01 = ((DynamicButtonsLayout) findViewById(R.id.dynamic_reply_buttons));
    public final DynamicButtonsRowContentLayout A02 = ((DynamicButtonsRowContentLayout) findViewById(R.id.dynamic_reply_buttons_message_content));

    public C60562yE(Context context, AbstractC13890kV r3, AnonymousClass1XO r4) {
        super(context, r3, r4);
        A0Z();
        A0X();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001d, code lost:
        if (r2 != null) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0X() {
        /*
            r3 = this;
            com.whatsapp.conversation.conversationrow.DynamicButtonsRowContentLayout r0 = r3.A02
            r0.A00(r3)
            X.0mz r1 = r3.A0O
            X.1Xj r0 = r1.A0F()
            X.1Xk r0 = r0.A00
            if (r0 == 0) goto L_0x0036
            com.whatsapp.Conversation r0 = r3.A0o()
            if (r0 == 0) goto L_0x0036
            X.1Xj r0 = r1.A0F()
            X.1Xk r0 = r0.A00
            java.util.List r2 = r0.A02
            if (r2 == 0) goto L_0x0031
        L_0x001f:
            int r0 = r2.size()
            if (r0 <= 0) goto L_0x0031
            com.whatsapp.conversation.conversationrow.DynamicButtonsLayout r1 = r3.A01
            X.3CO r0 = r3.A1b
            r1.A04(r0, r2)
            r0 = 0
        L_0x002d:
            r1.setVisibility(r0)
            return
        L_0x0031:
            com.whatsapp.conversation.conversationrow.DynamicButtonsLayout r1 = r3.A01
            r0 = 8
            goto L_0x002d
        L_0x0036:
            java.util.ArrayList r2 = X.C12960it.A0l()
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60562yE.A0X():void");
    }

    @Override // X.AnonymousClass2xh, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            ((C60792ye) this).A02 = A08.A2e();
            ((C60792ye) this).A01 = (C26171Ch) A08.A2a.get();
        }
    }

    @Override // X.C60792ye, X.AnonymousClass1OY
    public void A0s() {
        A0X();
        super.A0s();
    }

    @Override // X.C60792ye, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A0X();
        }
    }

    @Override // X.C60792ye, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_buttons_location_left;
    }

    @Override // X.C60792ye, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_buttons_location_left;
    }

    @Override // X.C60792ye, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_buttons_location_right;
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        AnonymousClass1OY.A0G(this.A01, this);
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth(), AnonymousClass1OY.A04(this, this.A01, getMeasuredHeight()));
    }
}
