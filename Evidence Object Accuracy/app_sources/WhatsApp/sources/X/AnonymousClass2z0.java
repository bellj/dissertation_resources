package X;

import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.search.views.itemviews.AudioPlayerView;

/* renamed from: X.2z0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2z0 extends AnonymousClass3WO {
    public final /* synthetic */ C60752yZ A00;
    public final /* synthetic */ C35191hP A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2z0(C60752yZ r1, ConversationRowAudioPreview conversationRowAudioPreview, C35191hP r3, AnonymousClass5U0 r4, AnonymousClass5U1 r5, AudioPlayerView audioPlayerView) {
        super(conversationRowAudioPreview, r4, r5, audioPlayerView);
        this.A00 = r1;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass2MF
    public C30421Xi ACr() {
        return (C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) this.A00).A0O);
    }
}
