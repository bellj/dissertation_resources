package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.backup.google.SingleChoiceListDialogFragment;
import java.util.List;

/* renamed from: X.2do  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53302do extends SimpleAdapter {
    public final /* synthetic */ int A00;
    public final /* synthetic */ SingleChoiceListDialogFragment A01;
    public final /* synthetic */ String[] A02;
    public final /* synthetic */ boolean[] A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53302do(Context context, SingleChoiceListDialogFragment singleChoiceListDialogFragment, List list, int[] iArr, String[] strArr, String[] strArr2, boolean[] zArr, int i) {
        super(context, list, R.layout.simple_list_item_2_single_choice, strArr, iArr);
        this.A01 = singleChoiceListDialogFragment;
        this.A03 = zArr;
        this.A00 = i;
        this.A02 = strArr2;
    }

    @Override // android.widget.SimpleAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        Context A0p;
        int i2;
        View view2 = super.getView(i, view, viewGroup);
        TextView A0J = C12960it.A0J(view2, 16908308);
        TextView A0J2 = C12960it.A0J(view2, 16908309);
        SingleChoiceListDialogFragment singleChoiceListDialogFragment = this.A01;
        if (singleChoiceListDialogFragment.A0c()) {
            boolean[] zArr = this.A03;
            if (zArr == null || zArr[i]) {
                A0p = singleChoiceListDialogFragment.A0p();
                i2 = R.color.settings_item_title_text;
            } else {
                A0p = singleChoiceListDialogFragment.A0p();
                i2 = R.color.settings_disabled_text;
            }
            C12960it.A0s(A0p, A0J, i2);
            C12960it.A0s(singleChoiceListDialogFragment.A0p(), A0J2, i2);
        }
        C12990iw.A19(A0J2, TextUtils.isEmpty(A0J2.getText()) ? 1 : 0);
        int i3 = this.A00;
        if (i3 < 0 || !TextUtils.equals(this.A02[i3], A0J.getText())) {
            ((CompoundButton) view2.findViewById(R.id.radio)).setChecked(false);
            return view2;
        }
        ((CompoundButton) view2.findViewById(R.id.radio)).setChecked(true);
        return view2;
    }
}
