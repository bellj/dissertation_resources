package X;

import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.util.Log;

/* renamed from: X.55o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1104455o implements AbstractC32851cq {
    public final /* synthetic */ RestoreFromBackupActivity A00;

    public C1104455o(RestoreFromBackupActivity restoreFromBackupActivity) {
        this.A00 = restoreFromBackupActivity;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        Log.w(C12960it.A0d(str, C12960it.A0k("gdrive-activity/restore-from-backup/error-card-not-found/state=")));
        RestoreFromBackupActivity.A0D(this.A00);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        Log.w("gdrive-activity/restore-from-backup/error-permission-unavailable");
        RestoreFromBackupActivity restoreFromBackupActivity = this.A00;
        restoreFromBackupActivity.A30(true);
        restoreFromBackupActivity.A35();
    }
}
