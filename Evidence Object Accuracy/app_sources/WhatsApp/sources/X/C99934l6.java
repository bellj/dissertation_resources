package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4l6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99934l6 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C51242Tl(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C51242Tl[i];
    }
}
