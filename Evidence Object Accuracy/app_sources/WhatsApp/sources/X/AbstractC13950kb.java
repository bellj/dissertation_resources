package X;

import androidx.fragment.app.DialogFragment;
import com.whatsapp.picker.search.PickerSearchDialogFragment;

/* renamed from: X.0kb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC13950kb {
    void ATj(PickerSearchDialogFragment pickerSearchDialogFragment);

    void Adk(DialogFragment dialogFragment);
}
