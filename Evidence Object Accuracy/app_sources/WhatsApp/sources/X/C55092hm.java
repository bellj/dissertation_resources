package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageButton;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55092hm extends AnonymousClass03U {
    public final C15570nT A00;
    public final TextEmojiLabel A01;
    public final C28801Pb A02;
    public final WaImageButton A03;
    public final ThumbnailButton A04;
    public final C15550nR A05;
    public final C15610nY A06;
    public final C19990v2 A07;
    public final C15600nX A08;
    public final AnonymousClass11F A09;

    public C55092hm(View view, C15570nT r4, C15550nR r5, C15610nY r6, C19990v2 r7, C15600nX r8, AnonymousClass11F r9, AnonymousClass12F r10) {
        super(view);
        this.A09 = r9;
        this.A00 = r4;
        this.A07 = r7;
        this.A04 = (ThumbnailButton) AnonymousClass028.A0D(view, R.id.group_icon);
        this.A06 = r6;
        C28801Pb r0 = new C28801Pb(view, r6, r10, (int) R.id.group_name);
        this.A02 = r0;
        this.A05 = r5;
        this.A08 = r8;
        TextEmojiLabel textEmojiLabel = r0.A01;
        AnonymousClass028.A0a(textEmojiLabel, 2);
        C27531Hw.A06(textEmojiLabel);
        this.A01 = C12970iu.A0T(view, R.id.group_status);
        this.A03 = (WaImageButton) AnonymousClass028.A0D(view, R.id.remove_button);
    }
}
