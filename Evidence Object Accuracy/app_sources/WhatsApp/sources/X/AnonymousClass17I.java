package X;

import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.17I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass17I {
    public final AbstractC15710nm A00;
    public final C14820m6 A01;
    public final AnonymousClass122 A02;
    public final AnonymousClass124 A03;
    public final AnonymousClass17A A04;
    public final AnonymousClass01H A05;

    public AnonymousClass17I(AbstractC15710nm r1, C14820m6 r2, AnonymousClass122 r3, AnonymousClass124 r4, AnonymousClass17A r5, AnonymousClass01H r6) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
        this.A05 = r6;
    }

    public static JSONObject A00(Map map) {
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry entry : map.entrySet()) {
            jSONObject.put((String) entry.getKey(), entry.getValue());
        }
        return jSONObject;
    }

    public String A01(String str) {
        String A00;
        C31091Zz A002 = this.A03.A00(AnonymousClass029.A0M, str.getBytes(AnonymousClass01V.A0A));
        if (A002 != null && (A00 = A002.A00()) != null && str.equals(A02(A00))) {
            return A00;
        }
        this.A00.AaV("FBCredentialsStore/encryptFbUsers", "Failed to encrypt fb users", true);
        throw new IllegalStateException("Failed to encrypt fb users");
    }

    public final String A02(String str) {
        byte[] bArr;
        try {
            C31091Zz A00 = AnonymousClass122.A00(new JSONArray(str));
            if (A00 == null) {
                bArr = null;
            } else {
                bArr = this.A03.A01(A00, AnonymousClass029.A0M);
            }
            if (bArr != null) {
                return new String(bArr, AnonymousClass01V.A0A);
            }
            this.A00.AaV("FBCredentialsStore/decryptFbUsers", "Failed to decrypt fb users", true);
            throw new IllegalStateException("Failed to decrypt fb users");
        } catch (JSONException e) {
            this.A00.AaV("FBCredentialsStore/decryptFbUsers", e.getMessage(), true);
            throw new IllegalStateException("Failed to decrypt fb users", e);
        }
    }

    public final Map A03() {
        String A02;
        AnonymousClass01H r7 = this.A05;
        String string = ((AnonymousClass17D) r7.get()).A00().getString("pref_fb_user_credentials_encrypted", null);
        if (string == null) {
            SharedPreferences sharedPreferences = this.A01.A00;
            A02 = sharedPreferences.getString("pref_fb_user_credentials", null);
            if (A02 != null) {
                ((AnonymousClass17D) r7.get()).A01(A01(A02));
                sharedPreferences.edit().remove("pref_fb_user_credentials").apply();
            }
        } else {
            A02 = A02(string);
        }
        if (A02 == null) {
            return new HashMap();
        }
        try {
            HashMap hashMap = new HashMap();
            JSONObject jSONObject = new JSONObject(A02);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.getString(next));
            }
            boolean z = false;
            for (Object obj : hashMap.keySet()) {
                JSONObject jSONObject2 = new JSONObject((String) hashMap.get(obj));
                if (jSONObject2.has("accessToken")) {
                    String string2 = jSONObject2.getString("accessToken");
                    jSONObject2.remove("accessToken");
                    jSONObject2.put("access_token", string2);
                    hashMap.put(obj, jSONObject2.toString());
                    z = true;
                }
            }
            if (!z) {
                return hashMap;
            }
            ((AnonymousClass17D) r7.get()).A01(A01(A00(hashMap).toString()));
            return hashMap;
        } catch (JSONException e) {
            throw new IllegalStateException("FBCredentialsStore : Failed to parse data from store", e);
        }
    }
}
