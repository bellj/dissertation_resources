package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.4xQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107494xQ implements AnonymousClass5YX {
    public static final C100614mC A06;
    public static final C100614mC A07;
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(5);
    public int A00;
    public final long A01;
    public final long A02;
    public final String A03;
    public final String A04;
    public final byte[] A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    static {
        C93844ap A00 = C93844ap.A00();
        A00.A0R = "application/id3";
        A06 = new C100614mC(A00);
        C93844ap A002 = C93844ap.A00();
        A002.A0R = "application/x-scte35";
        A07 = new C100614mC(A002);
    }

    public C107494xQ(Parcel parcel) {
        this.A03 = parcel.readString();
        this.A04 = parcel.readString();
        this.A01 = parcel.readLong();
        this.A02 = parcel.readLong();
        this.A05 = parcel.createByteArray();
    }

    public C107494xQ(String str, String str2, byte[] bArr, long j, long j2) {
        this.A03 = str;
        this.A04 = str2;
        this.A01 = j;
        this.A02 = j2;
        this.A05 = bArr;
    }

    @Override // X.AnonymousClass5YX
    public byte[] AHp() {
        if (AHq() != null) {
            return this.A05;
        }
        return null;
    }

    @Override // X.AnonymousClass5YX
    public C100614mC AHq() {
        String str;
        String str2 = this.A03;
        switch (str2.hashCode()) {
            case -1468477611:
                if (str2.equals("urn:scte:scte35:2014:bin")) {
                    return A07;
                }
                return null;
            case -795945609:
                str = "https://aomedia.org/emsg/ID3";
                break;
            case 1303648457:
                str = "https://developer.apple.com/streaming/emsg-id3";
                break;
            default:
                return null;
        }
        if (str2.equals(str)) {
            return A06;
        }
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C107494xQ.class != obj.getClass()) {
                return false;
            }
            C107494xQ r7 = (C107494xQ) obj;
            if (this.A01 != r7.A01 || this.A02 != r7.A02 || !AnonymousClass3JZ.A0H(this.A03, r7.A03) || !AnonymousClass3JZ.A0H(this.A04, r7.A04) || !Arrays.equals(this.A05, r7.A05)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        int A0E = (527 + C72453ed.A0E(this.A03)) * 31;
        String str = this.A04;
        if (str != null) {
            i2 = str.hashCode();
        }
        int A0A = C72453ed.A0A(C72453ed.A0A((A0E + i2) * 31, this.A01), this.A02) + Arrays.hashCode(this.A05);
        this.A00 = A0A;
        return A0A;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("EMSG: scheme=");
        A0k.append(this.A03);
        A0k.append(", id=");
        A0k.append(this.A02);
        A0k.append(", durationMs=");
        A0k.append(this.A01);
        A0k.append(", value=");
        return C12960it.A0d(this.A04, A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A03);
        parcel.writeString(this.A04);
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A02);
        parcel.writeByteArray(this.A05);
    }
}
