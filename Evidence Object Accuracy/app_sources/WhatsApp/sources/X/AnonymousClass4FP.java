package X;

/* renamed from: X.4FP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4FP {
    public static int A00(C92024Uf r30, byte[] bArr, byte[] bArr2, byte[] bArr3, long j) {
        byte[] bArr4 = new byte[32];
        byte[] bArr5 = new byte[32];
        byte[] bArr6 = new byte[32];
        byte[] bArr7 = new byte[64];
        byte[] bArr8 = new byte[32];
        AnonymousClass4SH r3 = new AnonymousClass4SH();
        C91164Qr r25 = new C91164Qr();
        if (j >= 64 && (bArr2[63] & 224) == 0) {
            int[] iArr = new int[10];
            int[] iArr2 = new int[10];
            int[] iArr3 = new int[10];
            int[] iArr4 = new int[10];
            int[] iArr5 = new int[10];
            int[] iArr6 = r3.A02;
            C94574c7.A00(bArr3, iArr6);
            int[] iArr7 = r3.A03;
            iArr7[0] = 1;
            C72453ed.A1T(iArr7);
            AnonymousClass4FG.A00(iArr, iArr6);
            AnonymousClass4FE.A00(iArr2, iArr, AnonymousClass4HK.A00);
            AnonymousClass4FH.A00(iArr, iArr, iArr7);
            AnonymousClass4FB.A00(iArr2, iArr2, iArr7);
            AnonymousClass4FG.A00(iArr3, iArr2);
            AnonymousClass4FE.A00(iArr3, iArr3, iArr2);
            int[] iArr8 = r3.A01;
            AnonymousClass4FG.A00(iArr8, iArr3);
            AnonymousClass4FE.A00(iArr8, iArr8, iArr2);
            AnonymousClass4FE.A00(iArr8, iArr8, iArr);
            int[] iArr9 = new int[10];
            int[] iArr10 = new int[10];
            int[] iArr11 = new int[10];
            AnonymousClass4FG.A00(iArr9, iArr8);
            AnonymousClass4FG.A00(iArr10, iArr9);
            AnonymousClass4FG.A00(iArr10, iArr10);
            AnonymousClass4FE.A00(iArr10, iArr8, iArr10);
            AnonymousClass4FE.A00(iArr9, iArr9, iArr10);
            AnonymousClass4FG.A00(iArr9, iArr9);
            AnonymousClass4FE.A00(iArr9, iArr10, iArr9);
            AnonymousClass4FG.A00(iArr10, iArr9);
            int i = 1;
            do {
                AnonymousClass4FG.A00(iArr10, iArr10);
                i++;
            } while (i < 5);
            AnonymousClass4FE.A00(iArr9, iArr10, iArr9);
            AnonymousClass4FG.A00(iArr10, iArr9);
            int i2 = 1;
            do {
                AnonymousClass4FG.A00(iArr10, iArr10);
                i2++;
            } while (i2 < 10);
            AnonymousClass4FE.A00(iArr10, iArr10, iArr9);
            AnonymousClass4FG.A00(iArr11, iArr10);
            int i3 = 1;
            do {
                AnonymousClass4FG.A00(iArr11, iArr11);
                i3++;
            } while (i3 < 20);
            AnonymousClass4FE.A00(iArr10, iArr11, iArr10);
            AnonymousClass4FG.A00(iArr10, iArr10);
            int i4 = 1;
            do {
                AnonymousClass4FG.A00(iArr10, iArr10);
                i4++;
            } while (i4 < 10);
            AnonymousClass4FE.A00(iArr9, iArr10, iArr9);
            AnonymousClass4FG.A00(iArr10, iArr9);
            int i5 = 1;
            do {
                AnonymousClass4FG.A00(iArr10, iArr10);
                i5++;
            } while (i5 < 50);
            AnonymousClass4FE.A00(iArr10, iArr10, iArr9);
            AnonymousClass4FG.A00(iArr11, iArr10);
            int i6 = 1;
            do {
                AnonymousClass4FG.A00(iArr11, iArr11);
                i6++;
            } while (i6 < 100);
            AnonymousClass4FE.A00(iArr10, iArr11, iArr10);
            AnonymousClass4FG.A00(iArr10, iArr10);
            int i7 = 1;
            do {
                AnonymousClass4FG.A00(iArr10, iArr10);
                i7++;
            } while (i7 < 50);
            AnonymousClass4FE.A00(iArr9, iArr10, iArr9);
            AnonymousClass4FG.A00(iArr9, iArr9);
            AnonymousClass4FG.A00(iArr9, iArr9);
            AnonymousClass4FE.A00(iArr8, iArr9, iArr8);
            AnonymousClass4FE.A00(iArr8, iArr8, iArr3);
            AnonymousClass4FE.A00(iArr8, iArr8, iArr);
            AnonymousClass4FG.A00(iArr4, iArr8);
            AnonymousClass4FE.A00(iArr4, iArr4, iArr2);
            AnonymousClass4FH.A00(iArr5, iArr4, iArr);
            byte[] bArr9 = new byte[32];
            AnonymousClass4FI.A00(bArr9, iArr5);
            byte[] bArr10 = C88644Gm.A00;
            int i8 = 0;
            int i9 = 0;
            do {
                i9 |= bArr9[i8] ^ bArr10[i8];
                i8++;
            } while (i8 < 32);
            if (i9 != 0) {
                AnonymousClass4FB.A00(iArr5, iArr4, iArr);
                byte[] bArr11 = new byte[32];
                AnonymousClass4FI.A00(bArr11, iArr5);
                int i10 = 0;
                int i11 = 0;
                do {
                    i11 |= bArr11[i10] ^ bArr10[i10];
                    i10++;
                } while (i10 < 32);
                if (i11 == 0) {
                    AnonymousClass4FE.A00(iArr8, iArr8, AnonymousClass4HK.A01);
                }
            }
            byte[] bArr12 = new byte[32];
            AnonymousClass4FI.A00(bArr12, iArr8);
            if ((bArr12[0] & 1) == ((bArr3[31] >>> 7) & 1)) {
                AnonymousClass4FF.A00(iArr8, iArr8);
            }
            AnonymousClass4FE.A00(r3.A00, iArr8, iArr6);
            r30.A00(new byte[64], bArr3, 32);
            System.arraycopy(bArr3, 0, bArr4, 0, 32);
            System.arraycopy(bArr2, 0, bArr5, 0, 32);
            System.arraycopy(bArr2, 32, bArr6, 0, 32);
            System.arraycopy(bArr2, 0, bArr, 0, (int) j);
            System.arraycopy(bArr4, 0, bArr, 32, 32);
            r30.A00(bArr7, bArr, j);
            C94584c8.A00(bArr7);
            byte[] bArr13 = new byte[256];
            byte[] bArr14 = new byte[256];
            AnonymousClass4SF[] r14 = new AnonymousClass4SF[8];
            int i12 = 0;
            do {
                r14[i12] = new AnonymousClass4SF();
                i12++;
            } while (i12 < 8);
            AnonymousClass4SG r13 = new AnonymousClass4SG();
            AnonymousClass4SH r12 = new AnonymousClass4SH();
            AnonymousClass4SH r5 = new AnonymousClass4SH();
            AnonymousClass4ZO.A00(bArr13, bArr7);
            AnonymousClass4ZO.A00(bArr14, bArr6);
            AnonymousClass4SF r0 = r14[0];
            AnonymousClass4ZP.A00(r0, r3);
            AnonymousClass4FO.A00(r13, r3);
            AnonymousClass4FM.A00(r13, r5);
            AnonymousClass4FJ.A00(r0, r13, r5);
            AnonymousClass4FM.A00(r13, r12);
            A01(r13, r12, r5, r14, 1);
            A01(r13, r12, r5, r14, 2);
            A01(r13, r12, r5, r14, 3);
            A01(r13, r12, r5, r14, 4);
            A01(r13, r12, r5, r14, 5);
            A01(r13, r12, r5, r14, 6);
            AnonymousClass4ZP.A00(r14[7], r12);
            int[] iArr12 = r25.A00;
            iArr12[0] = 0;
            C72453ed.A1T(iArr12);
            int[] iArr13 = r25.A01;
            iArr13[0] = 1;
            C72453ed.A1T(iArr13);
            int[] iArr14 = r25.A02;
            iArr14[0] = 1;
            C72453ed.A1T(iArr14);
            int i13 = 255;
            while (bArr13[i13] == 0 && bArr14[i13] == 0) {
                i13--;
                if (i13 < 0) {
                    break;
                }
            }
            do {
                AnonymousClass4FN.A00(r13, r25);
                byte b = bArr13[i13];
                if (b > 0) {
                    AnonymousClass4FM.A00(r13, r12);
                    AnonymousClass4FJ.A00(r14[b / 2], r13, r12);
                } else if (b < 0) {
                    AnonymousClass4FM.A00(r13, r12);
                    AnonymousClass4SF r10 = r14[(-b) / 2];
                    int[] iArr15 = new int[10];
                    int[] iArr16 = r13.A01;
                    int[] iArr17 = r12.A02;
                    int[] iArr18 = r12.A01;
                    AnonymousClass4FB.A00(iArr16, iArr17, iArr18);
                    int[] iArr19 = r13.A02;
                    AnonymousClass4FH.A00(iArr19, iArr17, iArr18);
                    int[] iArr20 = r13.A03;
                    AnonymousClass4FE.A00(iArr20, iArr16, r10.A01);
                    AnonymousClass4FE.A00(iArr19, iArr19, r10.A02);
                    int[] iArr21 = r13.A00;
                    AnonymousClass4FE.A00(iArr21, r10.A00, r12.A00);
                    AnonymousClass4FE.A00(iArr16, r12.A03, r10.A03);
                    AnonymousClass4FB.A00(iArr15, iArr16, iArr16);
                    AnonymousClass4FH.A00(iArr16, iArr20, iArr19);
                    AnonymousClass4FB.A00(iArr19, iArr20, iArr19);
                    AnonymousClass4FH.A00(iArr20, iArr15, iArr21);
                    AnonymousClass4FB.A00(iArr21, iArr15, iArr21);
                }
                byte b2 = bArr14[i13];
                if (b2 > 0) {
                    AnonymousClass4FM.A00(r13, r12);
                    AnonymousClass4FK.A00(r13, r12, AnonymousClass4ZO.A00[b2 / 2]);
                } else if (b2 < 0) {
                    AnonymousClass4FM.A00(r13, r12);
                    C93524aJ r9 = AnonymousClass4ZO.A00[(-b2) / 2];
                    int[] iArr22 = new int[10];
                    int[] iArr23 = r13.A01;
                    int[] iArr24 = r12.A02;
                    int[] iArr25 = r12.A01;
                    AnonymousClass4FB.A00(iArr23, iArr24, iArr25);
                    int[] iArr26 = r13.A02;
                    AnonymousClass4FH.A00(iArr26, iArr24, iArr25);
                    int[] iArr27 = r13.A03;
                    AnonymousClass4FE.A00(iArr27, iArr23, r9.A01);
                    AnonymousClass4FE.A00(iArr26, iArr26, r9.A02);
                    int[] iArr28 = r13.A00;
                    AnonymousClass4FE.A00(iArr28, r9.A00, r12.A00);
                    int[] iArr29 = r12.A03;
                    AnonymousClass4FB.A00(iArr22, iArr29, iArr29);
                    AnonymousClass4FH.A00(iArr23, iArr27, iArr26);
                    AnonymousClass4FB.A00(iArr26, iArr27, iArr26);
                    AnonymousClass4FH.A00(iArr27, iArr22, iArr28);
                    AnonymousClass4FB.A00(iArr28, iArr22, iArr28);
                }
                AnonymousClass4FL.A00(r13, r25);
                i13--;
            } while (i13 >= 0);
            int[] iArr30 = new int[10];
            int[] iArr31 = new int[10];
            int[] iArr32 = new int[10];
            AnonymousClass4FD.A00(iArr30, iArr14);
            AnonymousClass4FE.A00(iArr31, iArr12, iArr30);
            AnonymousClass4FE.A00(iArr32, iArr13, iArr30);
            AnonymousClass4FI.A00(bArr8, iArr32);
            byte b3 = bArr8[31];
            byte[] bArr15 = new byte[32];
            AnonymousClass4FI.A00(bArr15, iArr31);
            C72463ee.A0P(b3, bArr8, (bArr15[0] & 1) << 7, 31);
            int i14 = 0;
            int i15 = 0;
            do {
                i15 |= bArr8[i14] ^ bArr5[i14];
                i14++;
            } while (i14 < 32);
            if (i15 == 0) {
                System.arraycopy(bArr, 64, bArr, 0, (int) (j - 64));
                return 0;
            }
        }
        return -1;
    }

    public static void A01(AnonymousClass4SG r1, AnonymousClass4SH r2, AnonymousClass4SH r3, AnonymousClass4SF[] r4, int i) {
        AnonymousClass4SF r0 = r4[i];
        AnonymousClass4ZP.A00(r0, r2);
        AnonymousClass4FJ.A00(r0, r1, r3);
        AnonymousClass4FM.A00(r1, r2);
    }
}
