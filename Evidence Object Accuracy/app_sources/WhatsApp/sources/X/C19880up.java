package X;

/* renamed from: X.0up  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19880up {
    public final AbstractC15710nm A00;
    public final C18430sR A01;
    public final C14650lo A02;
    public final C19850um A03;
    public final C19810ui A04;
    public final C17560r0 A05;
    public final C17840rU A06;
    public final C17570r1 A07;
    public final C19830uk A08;
    public final C18640sm A09;
    public final C15680nj A0A;
    public final C19870uo A0B;
    public final C17220qS A0C;
    public final C19840ul A0D;
    public final C19860un A0E;
    public final AbstractC14440lR A0F;
    public final AnonymousClass01H A0G;

    public C19880up(AbstractC15710nm r2, C18430sR r3, C14650lo r4, C19850um r5, C19810ui r6, C17560r0 r7, C17840rU r8, C17570r1 r9, C19830uk r10, C18640sm r11, C15680nj r12, C19870uo r13, C17220qS r14, C19840ul r15, C19860un r16, AbstractC14440lR r17, AnonymousClass01H r18) {
        this.A08 = r10;
        this.A0C = r14;
        this.A0D = r15;
        this.A0G = r18;
        this.A03 = r5;
        this.A05 = r7;
        this.A06 = r8;
        this.A02 = r4;
        this.A09 = r11;
        this.A07 = r9;
        this.A00 = r2;
        this.A0E = r16;
        this.A0A = r12;
        this.A0F = r17;
        this.A04 = r6;
        this.A0B = r13;
        this.A01 = r3;
    }
}
