package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.1zI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44631zI extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C44631zI A0e;
    public static volatile AnonymousClass255 A0f;
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public String A04 = "";
    public String A05 = "";
    public String A06 = "";
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;

    static {
        C44631zI r0 = new C44631zI();
        A0e = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r23, Object obj, Object obj2) {
        switch (r23.ordinal()) {
            case 0:
                return A0e;
            case 1:
                AbstractC462925h r15 = (AbstractC462925h) obj;
                C44631zI r9 = (C44631zI) obj2;
                int i = this.A01;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A04;
                int i2 = r9.A01;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A04 = r15.Afy(str, r9.A04, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A05;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A05 = r15.Afy(str2, r9.A05, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                String str3 = this.A06;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A06 = r15.Afy(str3, r9.A06, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                int i3 = this.A00;
                boolean z8 = false;
                if ((i2 & 8) == 8) {
                    z8 = true;
                }
                this.A00 = r15.Afp(i3, r9.A00, z7, z8);
                boolean z9 = false;
                if ((i & 16) == 16) {
                    z9 = true;
                }
                boolean z10 = this.A0B;
                boolean z11 = false;
                if ((i2 & 16) == 16) {
                    z11 = true;
                }
                this.A0B = r15.Afl(z9, z10, z11, r9.A0B);
                boolean z12 = false;
                if ((i & 32) == 32) {
                    z12 = true;
                }
                boolean z13 = this.A0C;
                boolean z14 = false;
                if ((i2 & 32) == 32) {
                    z14 = true;
                }
                this.A0C = r15.Afl(z12, z13, z14, r9.A0C);
                boolean z15 = false;
                if ((i & 64) == 64) {
                    z15 = true;
                }
                boolean z16 = this.A0F;
                boolean z17 = false;
                if ((i2 & 64) == 64) {
                    z17 = true;
                }
                this.A0F = r15.Afl(z15, z16, z17, r9.A0F);
                boolean z18 = false;
                if ((i & 128) == 128) {
                    z18 = true;
                }
                boolean z19 = this.A09;
                boolean z20 = false;
                if ((i2 & 128) == 128) {
                    z20 = true;
                }
                this.A09 = r15.Afl(z18, z19, z20, r9.A09);
                boolean z21 = false;
                if ((i & 256) == 256) {
                    z21 = true;
                }
                boolean z22 = this.A0H;
                boolean z23 = false;
                if ((i2 & 256) == 256) {
                    z23 = true;
                }
                this.A0H = r15.Afl(z21, z22, z23, r9.A0H);
                boolean z24 = false;
                if ((i & 512) == 512) {
                    z24 = true;
                }
                boolean z25 = this.A0J;
                boolean z26 = false;
                if ((i2 & 512) == 512) {
                    z26 = true;
                }
                this.A0J = r15.Afl(z24, z25, z26, r9.A0J);
                boolean z27 = false;
                if ((i & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z27 = true;
                }
                boolean z28 = this.A0R;
                boolean z29 = false;
                if ((i2 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z29 = true;
                }
                this.A0R = r15.Afl(z27, z28, z29, r9.A0R);
                boolean z30 = false;
                if ((i & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z30 = true;
                }
                boolean z31 = this.A0W;
                boolean z32 = false;
                if ((i2 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z32 = true;
                }
                this.A0W = r15.Afl(z30, z31, z32, r9.A0W);
                boolean z33 = false;
                if ((i & 4096) == 4096) {
                    z33 = true;
                }
                boolean z34 = this.A0d;
                boolean z35 = false;
                if ((i2 & 4096) == 4096) {
                    z35 = true;
                }
                this.A0d = r15.Afl(z33, z34, z35, r9.A0d);
                boolean z36 = false;
                if ((i & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z36 = true;
                }
                boolean z37 = this.A0L;
                boolean z38 = false;
                if ((i2 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z38 = true;
                }
                this.A0L = r15.Afl(z36, z37, z38, r9.A0L);
                boolean z39 = false;
                if ((i & 16384) == 16384) {
                    z39 = true;
                }
                boolean z40 = this.A0T;
                boolean z41 = false;
                if ((i2 & 16384) == 16384) {
                    z41 = true;
                }
                this.A0T = r15.Afl(z39, z40, z41, r9.A0T);
                boolean z42 = false;
                if ((i & 32768) == 32768) {
                    z42 = true;
                }
                boolean z43 = this.A0G;
                boolean z44 = false;
                if ((i2 & 32768) == 32768) {
                    z44 = true;
                }
                this.A0G = r15.Afl(z42, z43, z44, r9.A0G);
                boolean z45 = false;
                if ((i & 65536) == 65536) {
                    z45 = true;
                }
                boolean z46 = this.A0N;
                boolean z47 = false;
                if ((i2 & 65536) == 65536) {
                    z47 = true;
                }
                this.A0N = r15.Afl(z45, z46, z47, r9.A0N);
                boolean z48 = false;
                if ((i & C25981Bo.A0F) == 131072) {
                    z48 = true;
                }
                boolean z49 = this.A0Q;
                boolean z50 = false;
                if ((i2 & C25981Bo.A0F) == 131072) {
                    z50 = true;
                }
                this.A0Q = r15.Afl(z48, z49, z50, r9.A0Q);
                boolean z51 = false;
                if ((i & 262144) == 262144) {
                    z51 = true;
                }
                boolean z52 = this.A0c;
                boolean z53 = false;
                if ((i2 & 262144) == 262144) {
                    z53 = true;
                }
                this.A0c = r15.Afl(z51, z52, z53, r9.A0c);
                boolean z54 = false;
                if ((i & 524288) == 524288) {
                    z54 = true;
                }
                boolean z55 = this.A0M;
                boolean z56 = false;
                if ((i2 & 524288) == 524288) {
                    z56 = true;
                }
                this.A0M = r15.Afl(z54, z55, z56, r9.A0M);
                boolean z57 = false;
                if ((i & 1048576) == 1048576) {
                    z57 = true;
                }
                boolean z58 = this.A0O;
                boolean z59 = false;
                if ((i2 & 1048576) == 1048576) {
                    z59 = true;
                }
                this.A0O = r15.Afl(z57, z58, z59, r9.A0O);
                boolean z60 = false;
                if ((i & 2097152) == 2097152) {
                    z60 = true;
                }
                boolean z61 = this.A0A;
                boolean z62 = false;
                if ((i2 & 2097152) == 2097152) {
                    z62 = true;
                }
                this.A0A = r15.Afl(z60, z61, z62, r9.A0A);
                boolean z63 = false;
                if ((i & 4194304) == 4194304) {
                    z63 = true;
                }
                boolean z64 = this.A0E;
                boolean z65 = false;
                if ((i2 & 4194304) == 4194304) {
                    z65 = true;
                }
                this.A0E = r15.Afl(z63, z64, z65, r9.A0E);
                boolean z66 = false;
                if ((i & 8388608) == 8388608) {
                    z66 = true;
                }
                boolean z67 = this.A0I;
                boolean z68 = false;
                if ((i2 & 8388608) == 8388608) {
                    z68 = true;
                }
                this.A0I = r15.Afl(z66, z67, z68, r9.A0I);
                boolean z69 = false;
                if ((i & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                    z69 = true;
                }
                boolean z70 = this.A0X;
                boolean z71 = false;
                if ((i2 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                    z71 = true;
                }
                this.A0X = r15.Afl(z69, z70, z71, r9.A0X);
                boolean z72 = false;
                if ((i & 33554432) == 33554432) {
                    z72 = true;
                }
                boolean z73 = this.A0S;
                boolean z74 = false;
                if ((i2 & 33554432) == 33554432) {
                    z74 = true;
                }
                this.A0S = r15.Afl(z72, z73, z74, r9.A0S);
                boolean z75 = false;
                if ((i & 67108864) == 67108864) {
                    z75 = true;
                }
                boolean z76 = this.A0P;
                boolean z77 = false;
                if ((i2 & 67108864) == 67108864) {
                    z77 = true;
                }
                this.A0P = r15.Afl(z75, z76, z77, r9.A0P);
                boolean z78 = false;
                if ((i & 134217728) == 134217728) {
                    z78 = true;
                }
                boolean z79 = this.A0V;
                boolean z80 = false;
                if ((i2 & 134217728) == 134217728) {
                    z80 = true;
                }
                this.A0V = r15.Afl(z78, z79, z80, r9.A0V);
                boolean z81 = false;
                if ((i & 268435456) == 268435456) {
                    z81 = true;
                }
                boolean z82 = this.A0Y;
                boolean z83 = false;
                if ((i2 & 268435456) == 268435456) {
                    z83 = true;
                }
                this.A0Y = r15.Afl(z81, z82, z83, r9.A0Y);
                boolean z84 = false;
                if ((i & 536870912) == 536870912) {
                    z84 = true;
                }
                boolean z85 = this.A0U;
                boolean z86 = false;
                if ((i2 & 536870912) == 536870912) {
                    z86 = true;
                }
                this.A0U = r15.Afl(z84, z85, z86, r9.A0U);
                boolean z87 = false;
                if ((i & 1073741824) == 1073741824) {
                    z87 = true;
                }
                boolean z88 = this.A0Z;
                boolean z89 = false;
                if ((i2 & 1073741824) == 1073741824) {
                    z89 = true;
                }
                this.A0Z = r15.Afl(z87, z88, z89, r9.A0Z);
                boolean z90 = false;
                if ((i & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                    z90 = true;
                }
                boolean z91 = this.A0D;
                boolean z92 = false;
                if ((i2 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                    z92 = true;
                }
                this.A0D = r15.Afl(z90, z91, z92, r9.A0D);
                int i4 = this.A02;
                boolean z93 = true;
                if ((i4 & 1) != 1) {
                    z93 = false;
                }
                boolean z94 = this.A0a;
                int i5 = r9.A02;
                boolean z95 = true;
                if ((i5 & 1) != 1) {
                    z95 = false;
                }
                this.A0a = r15.Afl(z93, z94, z95, r9.A0a);
                boolean z96 = false;
                if ((i4 & 2) == 2) {
                    z96 = true;
                }
                boolean z97 = this.A0K;
                boolean z98 = false;
                if ((i5 & 2) == 2) {
                    z98 = true;
                }
                this.A0K = r15.Afl(z96, z97, z98, r9.A0K);
                boolean z99 = false;
                if ((i4 & 4) == 4) {
                    z99 = true;
                }
                boolean z100 = this.A0b;
                boolean z101 = false;
                if ((i5 & 4) == 4) {
                    z101 = true;
                }
                this.A0b = r15.Afl(z99, z100, z101, r9.A0b);
                boolean z102 = false;
                if ((i4 & 8) == 8) {
                    z102 = true;
                }
                boolean z103 = this.A08;
                boolean z104 = false;
                if ((i5 & 8) == 8) {
                    z104 = true;
                }
                this.A08 = r15.Afl(z102, z103, z104, r9.A08);
                boolean z105 = false;
                if ((i4 & 16) == 16) {
                    z105 = true;
                }
                boolean z106 = this.A07;
                boolean z107 = false;
                if ((i5 & 16) == 16) {
                    z107 = true;
                }
                this.A07 = r15.Afl(z105, z106, z107, r9.A07);
                boolean z108 = false;
                if ((i4 & 32) == 32) {
                    z108 = true;
                }
                long j = this.A03;
                boolean z109 = false;
                if ((i5 & 32) == 32) {
                    z109 = true;
                }
                this.A03 = r15.Afs(j, r9.A03, z108, z109);
                if (r15 == C463025i.A00) {
                    this.A01 = i | i2;
                    this.A02 = i4 | i5;
                }
                return this;
            case 2:
                AnonymousClass253 r152 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r152.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r152.A0A();
                                    this.A01 = 1 | this.A01;
                                    this.A04 = A0A;
                                    break;
                                case 18:
                                    String A0A2 = r152.A0A();
                                    this.A01 |= 2;
                                    this.A05 = A0A2;
                                    break;
                                case 26:
                                    String A0A3 = r152.A0A();
                                    this.A01 |= 4;
                                    this.A06 = A0A3;
                                    break;
                                case 32:
                                    this.A01 |= 8;
                                    this.A00 = r152.A02();
                                    break;
                                case 40:
                                    this.A01 |= 16;
                                    this.A0B = r152.A0F();
                                    break;
                                case 48:
                                    this.A01 |= 32;
                                    this.A0C = r152.A0F();
                                    break;
                                case 56:
                                    this.A01 |= 64;
                                    this.A0F = r152.A0F();
                                    break;
                                case 64:
                                    this.A01 |= 128;
                                    this.A09 = r152.A0F();
                                    break;
                                case C43951xu.A02:
                                    this.A01 |= 256;
                                    this.A0H = r152.A0F();
                                    break;
                                case 80:
                                    this.A01 |= 512;
                                    this.A0J = r152.A0F();
                                    break;
                                case 88:
                                    this.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A0R = r152.A0F();
                                    break;
                                case 96:
                                    this.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A0W = r152.A0F();
                                    break;
                                case 104:
                                    this.A01 |= 4096;
                                    this.A0d = r152.A0F();
                                    break;
                                case 112:
                                    this.A01 |= DefaultCrypto.BUFFER_SIZE;
                                    this.A0L = r152.A0F();
                                    break;
                                case 120:
                                    this.A01 |= 16384;
                                    this.A0T = r152.A0F();
                                    break;
                                case 128:
                                    this.A01 |= 32768;
                                    this.A0G = r152.A0F();
                                    break;
                                case 136:
                                    this.A01 |= 65536;
                                    this.A0N = r152.A0F();
                                    break;
                                case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                                    this.A01 |= C25981Bo.A0F;
                                    this.A0Q = r152.A0F();
                                    break;
                                case 152:
                                    this.A01 |= 262144;
                                    this.A0c = r152.A0F();
                                    break;
                                case 160:
                                    this.A01 |= 524288;
                                    this.A0M = r152.A0F();
                                    break;
                                case 168:
                                    this.A01 |= 1048576;
                                    this.A0O = r152.A0F();
                                    break;
                                case MediaCodecVideoEncoder.MIN_ENCODER_WIDTH /* 176 */:
                                    this.A01 |= 2097152;
                                    this.A0A = r152.A0F();
                                    break;
                                case 184:
                                    this.A01 |= 4194304;
                                    this.A0E = r152.A0F();
                                    break;
                                case 192:
                                    this.A01 |= 8388608;
                                    this.A0I = r152.A0F();
                                    break;
                                case 200:
                                    this.A01 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
                                    this.A0X = r152.A0F();
                                    break;
                                case 208:
                                    this.A01 |= 33554432;
                                    this.A0S = r152.A0F();
                                    break;
                                case 216:
                                    this.A01 |= 67108864;
                                    this.A0P = r152.A0F();
                                    break;
                                case 224:
                                    this.A01 |= 134217728;
                                    this.A0V = r152.A0F();
                                    break;
                                case 232:
                                    this.A01 |= 268435456;
                                    this.A0Y = r152.A0F();
                                    break;
                                case 240:
                                    this.A01 |= 536870912;
                                    this.A0U = r152.A0F();
                                    break;
                                case 248:
                                    this.A01 |= 1073741824;
                                    this.A0Z = r152.A0F();
                                    break;
                                case 256:
                                    this.A01 |= Integer.MIN_VALUE;
                                    this.A0D = r152.A0F();
                                    break;
                                case 264:
                                    this.A02 |= 1;
                                    this.A0a = r152.A0F();
                                    break;
                                case 272:
                                    this.A02 |= 2;
                                    this.A0K = r152.A0F();
                                    break;
                                case 280:
                                    this.A02 |= 4;
                                    this.A0b = r152.A0F();
                                    break;
                                case 288:
                                    this.A02 |= 8;
                                    this.A08 = r152.A0F();
                                    break;
                                case 296:
                                    this.A02 |= 16;
                                    this.A07 = r152.A0F();
                                    break;
                                case 304:
                                    this.A02 |= 32;
                                    this.A03 = r152.A06();
                                    break;
                                default:
                                    if (A0a(r152, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C44631zI();
            case 5:
                return new C44621zH();
            case 6:
                break;
            case 7:
                if (A0f == null) {
                    synchronized (C44631zI.class) {
                        if (A0f == null) {
                            A0f = new AnonymousClass255(A0e);
                        }
                    }
                }
                return A0f;
            default:
                throw new UnsupportedOperationException();
        }
        return A0e;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A01 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A04);
        }
        if ((this.A01 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A05);
        }
        if ((this.A01 & 4) == 4) {
            i2 += CodedOutputStream.A07(3, this.A06);
        }
        int i3 = this.A01;
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A03(4, this.A00);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A00(5);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A00(6);
        }
        if ((i3 & 64) == 64) {
            i2 += CodedOutputStream.A00(7);
        }
        if ((i3 & 128) == 128) {
            i2 += CodedOutputStream.A00(8);
        }
        if ((i3 & 256) == 256) {
            i2 += CodedOutputStream.A00(9);
        }
        if ((i3 & 512) == 512) {
            i2 += CodedOutputStream.A00(10);
        }
        if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A00(11);
        }
        if ((i3 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A00(12);
        }
        if ((i3 & 4096) == 4096) {
            i2 += CodedOutputStream.A00(13);
        }
        if ((i3 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i2 += CodedOutputStream.A00(14);
        }
        if ((i3 & 16384) == 16384) {
            i2 += CodedOutputStream.A00(15);
        }
        if ((i3 & 32768) == 32768) {
            i2 += CodedOutputStream.A00(16);
        }
        if ((i3 & 65536) == 65536) {
            i2 += CodedOutputStream.A00(17);
        }
        if ((i3 & C25981Bo.A0F) == 131072) {
            i2 += CodedOutputStream.A00(18);
        }
        if ((i3 & 262144) == 262144) {
            i2 += CodedOutputStream.A00(19);
        }
        if ((i3 & 524288) == 524288) {
            i2 += CodedOutputStream.A00(20);
        }
        if ((i3 & 1048576) == 1048576) {
            i2 += CodedOutputStream.A00(21);
        }
        if ((i3 & 2097152) == 2097152) {
            i2 += CodedOutputStream.A00(22);
        }
        if ((i3 & 4194304) == 4194304) {
            i2 += CodedOutputStream.A00(23);
        }
        if ((i3 & 8388608) == 8388608) {
            i2 += CodedOutputStream.A00(24);
        }
        if ((i3 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            i2 += CodedOutputStream.A00(25);
        }
        if ((i3 & 33554432) == 33554432) {
            i2 += CodedOutputStream.A00(26);
        }
        if ((i3 & 67108864) == 67108864) {
            i2 += CodedOutputStream.A00(27);
        }
        if ((i3 & 134217728) == 134217728) {
            i2 += CodedOutputStream.A00(28);
        }
        if ((i3 & 268435456) == 268435456) {
            i2 += CodedOutputStream.A00(29);
        }
        if ((i3 & 536870912) == 536870912) {
            i2 += CodedOutputStream.A00(30);
        }
        if ((i3 & 1073741824) == 1073741824) {
            i2 += CodedOutputStream.A00(31);
        }
        if ((i3 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            i2 += CodedOutputStream.A00(32);
        }
        int i4 = this.A02;
        if ((i4 & 1) == 1) {
            i2 += CodedOutputStream.A00(33);
        }
        if ((i4 & 2) == 2) {
            i2 += CodedOutputStream.A00(34);
        }
        if ((i4 & 4) == 4) {
            i2 += CodedOutputStream.A00(35);
        }
        if ((i4 & 8) == 8) {
            i2 += CodedOutputStream.A00(36);
        }
        if ((i4 & 16) == 16) {
            i2 += CodedOutputStream.A00(37);
        }
        if ((i4 & 32) == 32) {
            i2 += CodedOutputStream.A05(38, this.A03);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0I(1, this.A04);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0I(2, this.A05);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0I(3, this.A06);
        }
        if ((this.A01 & 8) == 8) {
            codedOutputStream.A0E(4, this.A00);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0J(5, this.A0B);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0J(6, this.A0C);
        }
        if ((this.A01 & 64) == 64) {
            codedOutputStream.A0J(7, this.A0F);
        }
        if ((this.A01 & 128) == 128) {
            codedOutputStream.A0J(8, this.A09);
        }
        if ((this.A01 & 256) == 256) {
            codedOutputStream.A0J(9, this.A0H);
        }
        if ((this.A01 & 512) == 512) {
            codedOutputStream.A0J(10, this.A0J);
        }
        if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0J(11, this.A0R);
        }
        if ((this.A01 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0J(12, this.A0W);
        }
        if ((this.A01 & 4096) == 4096) {
            codedOutputStream.A0J(13, this.A0d);
        }
        if ((this.A01 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0J(14, this.A0L);
        }
        if ((this.A01 & 16384) == 16384) {
            codedOutputStream.A0J(15, this.A0T);
        }
        if ((this.A01 & 32768) == 32768) {
            codedOutputStream.A0J(16, this.A0G);
        }
        if ((this.A01 & 65536) == 65536) {
            codedOutputStream.A0J(17, this.A0N);
        }
        if ((this.A01 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0J(18, this.A0Q);
        }
        if ((this.A01 & 262144) == 262144) {
            codedOutputStream.A0J(19, this.A0c);
        }
        if ((this.A01 & 524288) == 524288) {
            codedOutputStream.A0J(20, this.A0M);
        }
        if ((this.A01 & 1048576) == 1048576) {
            codedOutputStream.A0J(21, this.A0O);
        }
        if ((this.A01 & 2097152) == 2097152) {
            codedOutputStream.A0J(22, this.A0A);
        }
        if ((this.A01 & 4194304) == 4194304) {
            codedOutputStream.A0J(23, this.A0E);
        }
        if ((this.A01 & 8388608) == 8388608) {
            codedOutputStream.A0J(24, this.A0I);
        }
        if ((this.A01 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            codedOutputStream.A0J(25, this.A0X);
        }
        if ((this.A01 & 33554432) == 33554432) {
            codedOutputStream.A0J(26, this.A0S);
        }
        if ((this.A01 & 67108864) == 67108864) {
            codedOutputStream.A0J(27, this.A0P);
        }
        if ((this.A01 & 134217728) == 134217728) {
            codedOutputStream.A0J(28, this.A0V);
        }
        if ((this.A01 & 268435456) == 268435456) {
            codedOutputStream.A0J(29, this.A0Y);
        }
        if ((this.A01 & 536870912) == 536870912) {
            codedOutputStream.A0J(30, this.A0U);
        }
        if ((this.A01 & 1073741824) == 1073741824) {
            codedOutputStream.A0J(31, this.A0Z);
        }
        if ((this.A01 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            codedOutputStream.A0J(32, this.A0D);
        }
        if ((this.A02 & 1) == 1) {
            codedOutputStream.A0J(33, this.A0a);
        }
        if ((this.A02 & 2) == 2) {
            codedOutputStream.A0J(34, this.A0K);
        }
        if ((this.A02 & 4) == 4) {
            codedOutputStream.A0J(35, this.A0b);
        }
        if ((this.A02 & 8) == 8) {
            codedOutputStream.A0J(36, this.A08);
        }
        if ((this.A02 & 16) == 16) {
            codedOutputStream.A0J(37, this.A07);
        }
        if ((this.A02 & 32) == 32) {
            codedOutputStream.A0H(38, this.A03);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
