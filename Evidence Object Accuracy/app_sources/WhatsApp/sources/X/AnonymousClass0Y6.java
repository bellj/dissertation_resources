package X;

import android.view.View;

/* renamed from: X.0Y6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Y6 implements AnonymousClass07F {
    public final /* synthetic */ LayoutInflater$Factory2C011505o A00;

    public AnonymousClass0Y6(LayoutInflater$Factory2C011505o r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass07F
    public C018408o AMH(View view, C018408o r6) {
        int A06 = r6.A06();
        int A0J = this.A00.A0J(null, r6);
        if (A06 != A0J) {
            r6 = r6.A08(r6.A04(), A0J, r6.A05(), r6.A03());
        }
        return AnonymousClass028.A0I(view, r6);
    }
}
