package X;

import android.content.Context;

/* renamed from: X.1OZ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1OZ extends AbstractC28551Oa {
    public boolean A00;

    public AnonymousClass1OZ(Context context, AbstractC13890kV r2, AbstractC15340mz r3) {
        super(context, r2, r3);
        A0Z();
    }

    @Override // X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass1OY r1 = (AnonymousClass1OY) this;
            AnonymousClass2P6 r2 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J r3 = r2.A06;
            ((AbstractC28551Oa) r1).A0L = (C14850m9) r3.A04.get();
            ((AbstractC28551Oa) r1).A0P = (AnonymousClass1CY) r3.ABO.get();
            ((AbstractC28551Oa) r1).A0F = (AbstractC15710nm) r3.A4o.get();
            ((AbstractC28551Oa) r1).A0N = (C244415n) r3.AAg.get();
            ((AbstractC28551Oa) r1).A0J = (AnonymousClass01d) r3.ALI.get();
            ((AbstractC28551Oa) r1).A0K = (AnonymousClass018) r3.ANb.get();
            ((AbstractC28551Oa) r1).A0M = (C22050yP) r3.A7v.get();
            ((AbstractC28551Oa) r1).A0G = (AnonymousClass19I) r3.A4a.get();
            r1.A0k = (C14830m7) r3.ALb.get();
            r1.A0J = (C14900mE) r3.A8X.get();
            r1.A13 = (AnonymousClass13H) r3.ABY.get();
            r1.A1P = (AbstractC14440lR) r3.ANe.get();
            r1.A0L = (C15570nT) r3.AAr.get();
            r1.A0h = (AnonymousClass19P) r3.ACQ.get();
            r1.A0M = (C239613r) r3.AI9.get();
            r1.A0O = (C18790t3) r3.AJw.get();
            r1.A0n = (C19990v2) r3.A3M.get();
            r1.A10 = (AnonymousClass19M) r3.A6R.get();
            r1.A0N = (C15450nH) r3.AII.get();
            r1.A0v = (C21250x7) r3.AJh.get();
            r1.A0w = (C18470sV) r3.AK8.get();
            r1.A0R = (C16170oZ) r3.AM4.get();
            r1.A1Q = (AnonymousClass19Z) r3.A2o.get();
            r1.A0K = (AnonymousClass18U) r3.AAU.get();
            r1.A12 = (C14410lO) r3.AB3.get();
            r1.A0I = (AnonymousClass12P) r3.A0H.get();
            r1.A0a = (C21270x9) r3.A4A.get();
            r1.A0s = (C20040v7) r3.AAK.get();
            r1.A15 = (C17220qS) r3.ABt.get();
            r1.A0X = (C15550nR) r3.A45.get();
            r1.A0U = (C253619c) r3.AId.get();
            r1.A0Z = (C15610nY) r3.AMe.get();
            r1.A1M = (C252018m) r3.A7g.get();
            r1.A1A = (C17070qD) r3.AFC.get();
            r1.A0t = (AnonymousClass1BK) r3.AFZ.get();
            r1.A0b = (C253318z) r3.A4B.get();
            r1.A0p = (C15650ng) r3.A4m.get();
            r1.A0V = (C238013b) r3.A1Z.get();
            r1.A11 = (C20710wC) r3.A8m.get();
            r1.A14 = (C22910zq) r3.A9O.get();
            r1.A1J = (AnonymousClass12F) r3.AJM.get();
            r1.A1F = r3.A41();
            r1.A1E = (AnonymousClass12V) r3.A0p.get();
            r1.A1I = (C240514a) r3.AJL.get();
            r1.A1O = (AnonymousClass19O) r3.ACO.get();
            r1.A17 = (C26151Cf) r3.ADi.get();
            r1.A1H = (C26701Em) r3.ABc.get();
            r1.A0x = (AnonymousClass132) r3.ALx.get();
            r1.A0S = (C19850um) r3.A2v.get();
            r1.A0y = (C21400xM) r3.ABd.get();
            r1.A0z = (C15670ni) r3.AIb.get();
            r1.A1N = (C23000zz) r3.ALg.get();
            r1.A0Y = (C22700zV) r3.AMN.get();
            r1.A0m = (C14820m6) r3.AN3.get();
            r1.A0W = (C22640zP) r3.A3Z.get();
            r1.A19 = (C22710zW) r3.AF7.get();
            r1.A0T = (AnonymousClass19Q) r3.A2u.get();
            r1.A1K = (AnonymousClass1AB) r3.AKI.get();
            r1.A18 = (AnonymousClass18T) r3.AE9.get();
            r1.A0r = (C15600nX) r3.A8x.get();
            r1.A0u = (C22440z5) r3.AG6.get();
            r1.A1D = (C16630pM) r3.AIc.get();
            r1.A0j = (C18640sm) r3.A3u.get();
            r1.A1L = (C26671Ej) r3.AKR.get();
            r1.A1G = r3.A42();
            r1.A0o = (C20830wO) r3.A4W.get();
            r1.A0q = (C242814x) r3.A71.get();
            r1.A0d = (AnonymousClass19K) r3.AFh.get();
            r1.A16 = (AnonymousClass19J) r3.ACA.get();
            r1.A1R = (C237512w) r3.AAD.get();
            r1.A0c = (AnonymousClass1AO) r3.AFg.get();
            r1.A0l = (C17170qN) r3.AMt.get();
            r1.A0i = (C17000q6) r3.ACv.get();
            r1.A1B = (C21190x1) r3.A3G.get();
            r1.A0f = r2.A02();
        }
    }
}
