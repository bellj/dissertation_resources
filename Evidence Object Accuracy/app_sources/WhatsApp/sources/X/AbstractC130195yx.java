package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.5yx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC130195yx {
    public C15370n3 A00 = null;
    public AnonymousClass1IR A01;
    public C119825fA A02;
    public boolean A03 = false;
    public final int A04;
    public final Context A05;
    public final AnonymousClass018 A06;
    public final AnonymousClass14X A07;

    public AbstractC130195yx(Context context, AnonymousClass018 r3, AnonymousClass14X r4, int i) {
        this.A04 = i;
        this.A05 = context;
        this.A07 = r4;
        this.A06 = r3;
    }

    public static boolean A00(AbstractC30781Yu r1, C1316363n r2, Object obj, int i) {
        if (i == 1) {
            return ((C30801Yw) obj).A00(r2.A01.A00);
        }
        return r1.A04.equals(((AbstractC30781Yu) r2.A01.A00).A04);
    }

    public int A01() {
        AnonymousClass1IR r0;
        if (this instanceof C123765np) {
            C123765np r2 = (C123765np) this;
            int i = r2.A00;
            if (i != 5) {
                switch (i) {
                    case 8:
                    case 9:
                    case 10:
                        return R.color.payments_status_gray;
                    default:
                        r0 = ((AbstractC130195yx) r2).A01;
                        break;
                }
            } else {
                return R.color.payments_status_gray;
            }
        } else if (!(this instanceof C123755no)) {
            r0 = this.A01;
        } else {
            C123755no r22 = (C123755no) this;
            int i2 = r22.A00;
            if (i2 == 6 || i2 == 7 || i2 == 8) {
                return R.color.payments_status_gray;
            }
            r0 = ((AbstractC130195yx) r22).A01;
        }
        return AnonymousClass14X.A01(r0);
    }

    public AnonymousClass6F2 A02() {
        C1316363n r0;
        C1316363n r02;
        AnonymousClass6F2 r03;
        if (this instanceof C123765np) {
            AbstractC121025h8 r04 = ((C123765np) this).A01;
            AnonymousClass009.A05(r04);
            r0 = r04.A06;
        } else if (this instanceof C123745nn) {
            C120985h4 r05 = ((C123745nn) this).A01;
            AnonymousClass009.A05(r05);
            r0 = r05.A00;
        } else if (!(this instanceof C123755no)) {
            C121035h9 r06 = ((C123735nm) this).A00;
            AnonymousClass009.A05(r06);
            r0 = r06.A00.A03;
        } else {
            C123755no r2 = (C123755no) this;
            if (!((AbstractC130195yx) r2).A01.A0F()) {
                if (r2.A08()) {
                    C120995h5 r07 = r2.A01;
                    AnonymousClass009.A05(r07);
                    r02 = r07.A01.A05.A00;
                    r03 = r02.A01;
                    AnonymousClass009.A05(r03);
                    return r03;
                }
                int i = ((AbstractC130195yx) r2).A01.A03;
                if (!(i == 2 || i == 200 || i == 20)) {
                    r03 = null;
                    AnonymousClass009.A05(r03);
                    return r03;
                }
            }
            C120995h5 r08 = r2.A01;
            AnonymousClass009.A05(r08);
            r02 = r08.A01.A03.A01;
            r03 = r02.A01;
            AnonymousClass009.A05(r03);
            return r03;
        }
        return r0.A01;
    }

    public CharSequence A03() {
        return this.A07.A0I(this.A01);
    }

    public String A04() {
        return this.A07.A0J(this.A01);
    }

    public String A05() {
        Context context;
        int i;
        if (this instanceof C123765np) {
            context = ((C123765np) this).A02;
            i = R.string.payment_transaction_type_withdrawal;
        } else if (this instanceof C123745nn) {
            context = this.A05;
            i = R.string.novi_title;
        } else if (this instanceof C123755no) {
            return ((C123755no) this).A02;
        } else {
            context = this.A05;
            i = R.string.payment_transaction_type_deposit;
        }
        return context.getString(i);
    }

    public void A06(AnonymousClass1IR r2) {
        this.A01 = r2;
        AbstractC30891Zf r0 = r2.A0A;
        AnonymousClass009.A05(r0);
        this.A02 = (C119825fA) r0;
    }

    public boolean A07() {
        C1316363n r4;
        String str;
        AnonymousClass6F2 r0;
        boolean A1V;
        if (this instanceof C123765np) {
            AbstractC121025h8 r02 = ((C123765np) this).A01;
            AnonymousClass009.A05(r02);
            r4 = r02.A06;
        } else if (this instanceof C123745nn) {
            C120985h4 r03 = ((C123745nn) this).A01;
            AnonymousClass009.A05(r03);
            r4 = r03.A00;
        } else if (!(this instanceof C123755no)) {
            C121035h9 r04 = ((C123735nm) this).A00;
            AnonymousClass009.A05(r04);
            r4 = r04.A00.A03;
        } else {
            C123755no r7 = (C123755no) this;
            C120995h5 r2 = r7.A01;
            AnonymousClass009.A05(r2);
            C1315163b r1 = r2.A01;
            C1316363n r6 = r1.A05.A00;
            AnonymousClass009.A05(r2);
            C1316363n r5 = r1.A03.A01;
            AbstractC30791Yv r42 = r6.A02.A00;
            AbstractC30781Yu r12 = (AbstractC30781Yu) r42;
            int i = r12.A00;
            if (!r7.A08()) {
                AbstractC30791Yv r13 = r5.A02.A00;
                if (i == 1) {
                    A1V = ((C30801Yw) r13).A00(r5.A01.A00);
                } else {
                    str = ((AbstractC30781Yu) r13).A04;
                    r0 = r5.A01;
                    A1V = C117305Zk.A1V(r0.A00, str);
                }
            } else if (i == 1) {
                A1V = ((C30801Yw) r42).A00(r6.A01.A00);
            } else {
                str = r12.A04;
                r0 = r6.A01;
                A1V = C117305Zk.A1V(r0.A00, str);
            }
            return !A1V;
        }
        AbstractC30791Yv r3 = r4.A02.A00;
        AbstractC30781Yu r22 = (AbstractC30781Yu) r3;
        return !A00(r22, r4, r3, r22.A00);
    }
}
