package X;

import com.whatsapp.jid.Jid;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29871Vb {
    public final C29861Va A00;
    public final Map A01 = Collections.synchronizedMap(new HashMap());

    public C29871Vb(AnonymousClass018 r2) {
        this.A00 = new C29861Va(r2);
    }

    public void A00(C15370n3 r4) {
        Map map;
        Object obj;
        Jid A0B = r4.A0B(AbstractC14640lm.class);
        if (A0B != null && (obj = (map = this.A01).get(A0B)) != null && obj != r4) {
            map.remove(A0B);
        }
    }
}
