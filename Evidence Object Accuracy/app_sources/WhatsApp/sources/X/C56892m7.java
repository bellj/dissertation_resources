package X;

/* renamed from: X.2m7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56892m7 extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public C56892m7() {
        super(C57242mi.A03);
    }

    public void A05(AbstractC27881Jp r3) {
        C57242mi r1 = (C57242mi) AnonymousClass1G4.A00(this);
        r1.A00 |= 2;
        r1.A01 = r3;
    }

    public void A06(String str) {
        C57242mi r1 = (C57242mi) AnonymousClass1G4.A00(this);
        r1.A00 |= 1;
        r1.A02 = str;
    }
}
