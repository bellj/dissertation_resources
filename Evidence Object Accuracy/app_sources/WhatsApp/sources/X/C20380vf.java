package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0vf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20380vf {
    public final C16590pI A00;
    public final C18360sK A01;
    public final AnonymousClass018 A02;
    public final C16490p7 A03;
    public final C20370ve A04;
    public final C21390xL A05;
    public final C22710zW A06;
    public final C17070qD A07;
    public final C30931Zj A08 = C30931Zj.A00("MessagelessPaymentNotification", "notification", "COMMON");
    public final AnonymousClass14X A09;
    public final C15860o1 A0A;
    public final AbstractC14440lR A0B;

    public C20380vf(C16590pI r4, C18360sK r5, AnonymousClass018 r6, C16490p7 r7, C20370ve r8, C21390xL r9, C22710zW r10, C17070qD r11, AnonymousClass14X r12, C15860o1 r13, AbstractC14440lR r14) {
        this.A00 = r4;
        this.A0B = r14;
        this.A09 = r12;
        this.A02 = r6;
        this.A07 = r11;
        this.A0A = r13;
        this.A05 = r9;
        this.A03 = r7;
        this.A06 = r10;
        this.A01 = r5;
        this.A04 = r8;
    }

    public void A00() {
        C21390xL r2 = this.A05;
        if (!TextUtils.isEmpty(r2.A02("unread_messageless_transaction_ids"))) {
            r2.A06("unread_messageless_transaction_ids", "");
            this.A01.A04(17, null);
        }
    }

    public void A01() {
        this.A0B.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(this, 10));
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02() {
        /*
        // Method dump skipped, instructions count: 571
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20380vf.A02():void");
    }

    public final synchronized void A03(String str) {
        if (TextUtils.isEmpty(str)) {
            this.A08.A05("removeUnreadMessagelessPaymentTransaction empty transaction id");
        } else {
            C21390xL r6 = this.A05;
            String A02 = r6.A02("unread_messageless_transaction_ids");
            if (A02 == null) {
                A02 = "";
            }
            HashSet hashSet = new HashSet(Arrays.asList(TextUtils.split(A02, ";")));
            if (hashSet.remove(str)) {
                C30931Zj r2 = this.A08;
                StringBuilder sb = new StringBuilder();
                sb.append("removeUnreadMessagelessPaymentTransaction/removed id:");
                sb.append(str);
                r2.A06(sb.toString());
            }
            r6.A06("unread_messageless_transaction_ids", TextUtils.join(";", hashSet));
        }
    }

    public synchronized void A04(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            A03((String) it.next());
        }
        if (TextUtils.isEmpty(this.A05.A02("unread_messageless_transaction_ids"))) {
            this.A01.A04(17, null);
        }
    }
}
