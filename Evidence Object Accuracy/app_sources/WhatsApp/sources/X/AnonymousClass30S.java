package X;

/* renamed from: X.30S  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30S extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Long A02;

    public AnonymousClass30S() {
        super(2540, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(2, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPrekeysFetch {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "onIdentityChange", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "prekeysFetchContext", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "prekeysFetchCount", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
