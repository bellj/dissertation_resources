package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: X.09E  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09E extends AnimatorListenerAdapter {
    public boolean A00 = false;
    public final View A01;

    public AnonymousClass09E(View view) {
        this.A01 = view;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        View view = this.A01;
        AnonymousClass0U3.A04.A05(view, 1.0f);
        if (this.A00) {
            view.setLayerType(0, null);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        View view = this.A01;
        if (view.hasOverlappingRendering() && view.getLayerType() == 0) {
            this.A00 = true;
            view.setLayerType(2, null);
        }
    }
}
