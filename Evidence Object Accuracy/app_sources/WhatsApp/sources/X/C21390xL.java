package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.SystemClock;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0xL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21390xL {
    public final C14830m7 A00;
    public final C20290vW A01;
    public final C16490p7 A02;
    public final AbstractC14440lR A03;
    public final Object A04 = new Object();
    public final Map A05 = new HashMap();

    public C21390xL(C14830m7 r2, C20290vW r3, C16490p7 r4, AbstractC14440lR r5) {
        this.A00 = r2;
        this.A03 = r5;
        this.A01 = r3;
        this.A02 = r4;
    }

    public int A00(String str, int i) {
        String A02 = A02(str);
        if (A02 == null) {
            return i;
        }
        return Integer.parseInt(A02);
    }

    public long A01(String str, long j) {
        String A02 = A02(str);
        if (A02 == null) {
            return j;
        }
        return Long.parseLong(A02);
    }

    public String A02(String str) {
        long uptimeMillis = SystemClock.uptimeMillis();
        Object obj = this.A04;
        synchronized (obj) {
            Map map = this.A05;
            if (map.containsKey(str)) {
                return (String) map.get(str);
            }
            String str2 = null;
            C16310on A01 = this.A02.get();
            Cursor A09 = A01.A03.A09("SELECT value FROM props WHERE key = ?", new String[]{str});
            if (A09.moveToNext()) {
                str2 = A09.getString(0);
            }
            A09.close();
            A01.close();
            synchronized (obj) {
                map.put(str, str2);
            }
            this.A01.A00("PropsMessageStore/getProp", SystemClock.uptimeMillis() - uptimeMillis);
            return str2;
        }
    }

    public void A03(String str) {
        C16310on A02 = this.A02.A02();
        try {
            A02.A03.A01("props", "key = ?", new String[]{str});
            A02.close();
            synchronized (this.A04) {
                this.A05.remove(str);
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A04(String str, int i) {
        A06(str, String.valueOf(i));
    }

    public void A05(String str, long j) {
        A06(str, String.valueOf(j));
    }

    public void A06(String str, String str2) {
        long uptimeMillis = SystemClock.uptimeMillis();
        C16310on A02 = this.A02.A02();
        try {
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("key", str);
            contentValues.put("value", str2);
            A02.A03.A05(contentValues, "props");
            A02.close();
            synchronized (this.A04) {
                this.A05.put(str, str2);
            }
            this.A01.A00("PropsMessageStore/setProp", SystemClock.uptimeMillis() - uptimeMillis);
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
