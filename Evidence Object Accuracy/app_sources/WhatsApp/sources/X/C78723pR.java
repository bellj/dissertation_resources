package X;

import android.os.Bundle;
import android.os.IBinder;

/* renamed from: X.3pR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78723pR extends AbstractC78733pS {
    public final IBinder A00;
    public final /* synthetic */ AbstractC95064d1 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C78723pR(Bundle bundle, IBinder iBinder, AbstractC95064d1 r3, int i) {
        super(bundle, r3, i);
        this.A01 = r3;
        this.A00 = iBinder;
    }
}
