package X;

import java.util.Map;

/* renamed from: X.6Bn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C133636Bn implements AbstractC16960q2 {
    public final AnonymousClass018 A00;

    @Override // X.AbstractC16960q2
    public Class A9u() {
        return EnumC124625pq.class;
    }

    public C133636Bn(AnonymousClass018 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16960q2
    public /* bridge */ /* synthetic */ Object Aam(Enum r5, Object obj, Map map) {
        String str;
        int i;
        AnonymousClass20C r6 = (AnonymousClass20C) obj;
        EnumC124625pq r52 = (EnumC124625pq) r5;
        boolean A0N = C16700pc.A0N(r6, r52);
        int A01 = C117315Zl.A01(r52, C125265qy.A00);
        if (A01 != A0N) {
            if (A01 == 2) {
                i = (int) (r6.A02.A00.doubleValue() * ((double) r6.A00));
            } else if (A01 == 3) {
                i = r6.A00;
            } else if (A01 == 4) {
                str = r6.A01.AA9(this.A00, r6.A02.A00);
            } else if (A01 == 5) {
                str = r6.A01.AAB(this.A00, r6.A02.A00, 0);
            } else {
                throw new C113285Gx();
            }
            return Integer.valueOf(i);
        }
        str = ((AbstractC30781Yu) r6.A01).A04;
        C16700pc.A0B(str);
        return str;
    }
}
