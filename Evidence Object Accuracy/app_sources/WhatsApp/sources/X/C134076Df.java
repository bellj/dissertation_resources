package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.6Df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134076Df implements AnonymousClass5Wu {
    public View A00;
    public View A01;
    public View A02;
    public WaTextView A03;

    /* renamed from: A00 */
    public void A6Q(AnonymousClass4OZ r5) {
        View view;
        if (r5 != null) {
            int i = r5.A00;
            if (!(i == -2 || i == -1)) {
                if (i == 0) {
                    this.A01.setVisibility(8);
                    view = this.A00;
                } else if (i == 1) {
                    this.A00.setVisibility(0);
                    this.A01.setVisibility(0);
                    view = this.A02;
                } else if (i != 2) {
                    return;
                }
                view.setVisibility(8);
                return;
            }
            this.A01.setVisibility(8);
            this.A02.setVisibility(0);
            C117315Zl.A0N(this.A03, r5.A01);
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_payment_target_container;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = view;
        this.A03 = C12960it.A0N(view, R.id.payment_target_view);
        this.A01 = AnonymousClass028.A0D(view, R.id.payment_target_container_shimmer);
        this.A02 = AnonymousClass028.A0D(view, R.id.payment_target_view_container);
    }
}
