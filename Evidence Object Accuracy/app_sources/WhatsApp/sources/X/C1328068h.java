package X;

/* renamed from: X.68h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328068h implements AnonymousClass1FK {
    public final /* synthetic */ C14580lf A00;
    public final /* synthetic */ C120285fv A01;

    public C1328068h(C14580lf r1, C120285fv r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        this.A00.A02(Boolean.FALSE);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        this.A00.A02(Boolean.FALSE);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r3) {
        this.A00.A02(Boolean.TRUE);
    }
}
