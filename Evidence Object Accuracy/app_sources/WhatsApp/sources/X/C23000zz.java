package X;

import android.content.SharedPreferences;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape2S0300000_I0_2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/* renamed from: X.0zz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C23000zz {
    public Runnable A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final C17220qS A06;
    public final AnonymousClass163 A07;
    public final AnonymousClass164 A08;
    public final C32881ct A09;
    public final C32881ct A0A;
    public final AbstractC14440lR A0B;
    public final Runnable A0C = new RunnableBRunnable0Shape12S0100000_I0_12(this, 46);
    public final List A0D = Collections.singletonList("20210210");

    public C23000zz(C14900mE r6, C15570nT r7, C18640sm r8, C14830m7 r9, C14850m9 r10, C17220qS r11, AnonymousClass163 r12, AnonymousClass164 r13, AbstractC14440lR r14) {
        this.A04 = r9;
        this.A05 = r10;
        this.A01 = r6;
        this.A02 = r7;
        this.A0B = r14;
        this.A06 = r11;
        this.A07 = r12;
        this.A08 = r13;
        this.A03 = r8;
        this.A09 = new C32881ct(new Random(), 5, 16000);
        this.A0A = new C32881ct(new Random(), 5, 16000);
    }

    public int A00(String str) {
        SharedPreferences A00 = this.A08.A00();
        StringBuilder sb = new StringBuilder("tos_acceptance_state_");
        sb.append(str);
        return A00.getInt(sb.toString(), 0);
    }

    public void A01(C33361ds r6) {
        this.A09.A01();
        AnonymousClass164 r4 = this.A08;
        r4.A00().edit().putLong("request_refresh_rate_seconds", r6.A00).apply();
        for (C33371dt r0 : r6.A01) {
            String str = r0.A01;
            r4.A01(str, r0.A00);
            r4.A02(str, System.currentTimeMillis());
        }
        this.A01.A0H(new RunnableBRunnable0Shape12S0100000_I0_12(this.A07, 45));
    }

    public final void A02(List list, long j) {
        RunnableBRunnable0Shape2S0300000_I0_2 runnableBRunnable0Shape2S0300000_I0_2;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (A00(str) == 1) {
                arrayList.add(str);
            }
        }
        if (arrayList.isEmpty()) {
            runnableBRunnable0Shape2S0300000_I0_2 = null;
        } else {
            runnableBRunnable0Shape2S0300000_I0_2 = new RunnableBRunnable0Shape2S0300000_I0_2(this, list, arrayList, 0);
        }
        this.A00 = runnableBRunnable0Shape2S0300000_I0_2;
        if (runnableBRunnable0Shape2S0300000_I0_2 == null) {
            this.A0A.A01();
        } else {
            this.A0B.AbK(runnableBRunnable0Shape2S0300000_I0_2, "ToSGatingRepository/postTosAcceptanceState", j);
        }
    }
}
