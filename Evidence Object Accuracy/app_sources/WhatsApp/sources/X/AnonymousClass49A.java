package X;

import java.io.InputStream;

/* renamed from: X.49A  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass49A extends InputStream {
    public int A00;
    public final InputStream A01;

    public AnonymousClass49A(InputStream inputStream, int i) {
        this.A01 = inputStream;
        this.A00 = i;
    }

    public void A00() {
        InputStream inputStream = this.A01;
        if (inputStream instanceof C114885Nl) {
            C114885Nl r1 = (C114885Nl) inputStream;
            r1.A02 = true;
            r1.A01();
        }
    }
}
