package X;

import android.content.Context;
import android.os.Handler;
import java.util.ArrayList;

/* renamed from: X.4vk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106464vk implements AnonymousClass5SH {
    public final Context A00;

    public C106464vk(Context context) {
        this.A00 = context;
    }

    @Override // X.AnonymousClass5SH
    public AbstractC117055Yb[] A8T(Handler handler, AbstractC72373eU r18, AnonymousClass5SO r19, AnonymousClass5SS r20, AnonymousClass5XS r21) {
        ArrayList A0l = C12960it.A0l();
        Context context = this.A00;
        AbstractC117015Xu r9 = AbstractC117015Xu.A00;
        A0l.add(new C76933mT(context, handler, AbstractC116825Xa.A00, r9, r21));
        A0l.add(new C76943mU(context, handler, AnonymousClass3IX.A00(context), r18, r9, new AnonymousClass5Xx[0]));
        A0l.add(new C76553lp(handler.getLooper(), r20));
        return (AbstractC117055Yb[]) A0l.toArray(new AbstractC117055Yb[0]);
    }
}
