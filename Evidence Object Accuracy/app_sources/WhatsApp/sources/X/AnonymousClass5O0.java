package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5O0  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5O0 extends EnumC87374Bg {
    public AnonymousClass5O0() {
        super("UTF8", 1);
    }

    @Override // X.AnonymousClass5WQ
    public byte[] A7j(char[] cArr) {
        return AbstractC94944cn.A01(cArr);
    }

    @Override // X.AnonymousClass5WQ
    public String AHM() {
        return "UTF8";
    }
}
