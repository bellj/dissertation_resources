package X;

/* renamed from: X.1qo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC39951qo {
    A0U(0),
    A0L(1),
    A0K(2),
    A0J(3),
    A0T(4),
    /* Fake field, exist only in values array */
    EF29(5),
    A0D(6),
    A0I(7),
    A0G(8),
    A0H(9),
    A0E(10),
    A0F(11),
    A0M(12),
    A0N(13),
    A0P(14),
    A0O(15),
    A0C(16),
    A01(17),
    A03(18),
    A02(19),
    A09(20),
    A0B(21),
    A07(22),
    A08(23),
    A0A(24),
    A06(25),
    A04(26),
    A05(27),
    /* Fake field, exist only in values array */
    EF17(28),
    A0S(29),
    A0R(30),
    A0Q(31);
    
    public final int value;

    EnumC39951qo(int i) {
        this.value = i;
    }
}
