package X;

import android.view.View;
import androidx.appcompat.widget.AppCompatSpinner;

/* renamed from: X.0CY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CY extends AnonymousClass0WW {
    public final /* synthetic */ C02400Cd A00;
    public final /* synthetic */ AppCompatSpinner A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CY(View view, C02400Cd r2, AppCompatSpinner appCompatSpinner) {
        super(view);
        this.A01 = appCompatSpinner;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0WW
    public AbstractC12600iB A00() {
        return this.A00;
    }

    @Override // X.AnonymousClass0WW
    public boolean A03() {
        AppCompatSpinner appCompatSpinner = this.A01;
        if (appCompatSpinner.A02.AK4()) {
            return true;
        }
        appCompatSpinner.A01();
        return true;
    }
}
