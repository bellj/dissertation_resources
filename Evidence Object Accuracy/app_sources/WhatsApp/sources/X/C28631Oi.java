package X;

import android.util.Pair;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1Oi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28631Oi {
    public static final AtomicInteger A0I = new AtomicInteger(0);
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final AbstractC20460vn A04;
    public final AnonymousClass1Q1 A05;
    public final AnonymousClass15V A06;
    public final Integer A07;
    public final ConcurrentHashMap A08 = new ConcurrentHashMap();
    public final ConcurrentHashMap A09 = new ConcurrentHashMap();
    public final ConcurrentHashMap A0A = new ConcurrentHashMap();
    public final ConcurrentHashMap A0B = new ConcurrentHashMap();
    public final AtomicInteger A0C = new AtomicInteger(0);
    public final AtomicInteger A0D = new AtomicInteger(-1);
    public final AtomicInteger A0E = new AtomicInteger(0);
    public final AtomicLong A0F = new AtomicLong(-1);
    public final boolean A0G;
    public final boolean A0H;

    public C28631Oi(AbstractC20460vn r4, AnonymousClass1Q1 r5, AnonymousClass15V r6, Integer num, int i, long j, long j2, boolean z, boolean z2) {
        this.A04 = r4;
        this.A06 = r6;
        this.A00 = i;
        this.A05 = r5;
        this.A07 = num;
        this.A03 = j2;
        this.A02 = j;
        this.A0H = z;
        this.A01 = A0I.incrementAndGet();
        this.A0G = z2;
    }

    public void A00(int i, String str, Object obj) {
        if (str.length() > 50) {
            this.A04.A5w(this.A00, str);
            return;
        }
        AtomicInteger atomicInteger = this.A0C;
        if (atomicInteger.get() + i >= 1000) {
            this.A04.A5x(this.A00, str, i);
            return;
        }
        atomicInteger.addAndGet(i);
        if (this.A08.putIfAbsent(str, obj) != null) {
            atomicInteger.addAndGet(-i);
        }
    }

    public void A01(long j, short s) {
        if (this.A0F.compareAndSet(-1, j)) {
            this.A0D.set(s);
            this.A06.A02(this, 2);
        }
    }

    public void A02(Serializable serializable, String str, String str2) {
        this.A0A.put(new Pair(str, str2), serializable);
    }

    public void A03(String str, long j) {
        AtomicInteger atomicInteger = this.A0E;
        if (atomicInteger.get() >= 1000) {
            this.A04.ALO(this.A00);
        } else if (str.length() > 50) {
            this.A04.AZI(this.A00, str);
        } else {
            int andIncrement = atomicInteger.getAndIncrement();
            this.A0B.put(Integer.valueOf(andIncrement), new AnonymousClass1Q0(str, j));
        }
    }
}
