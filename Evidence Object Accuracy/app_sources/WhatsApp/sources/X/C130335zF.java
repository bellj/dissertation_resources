package X;

import android.text.TextUtils;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.5zF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130335zF {
    public static AnonymousClass01T A00(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            Object obj = str;
            if (TextUtils.isEmpty(str2)) {
                List asList = Arrays.asList(str.split("\\|"));
                if (asList.size() == 2) {
                    Object A0o = C12980iv.A0o(asList);
                    str2 = TextUtils.join(".", asList);
                    obj = A0o;
                }
            }
            return C117315Zl.A05(obj, str2);
        }
        return null;
    }
}
