package X;

import com.whatsapp.avatar.home.AvatarHomeViewModel;

/* renamed from: X.58i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1111658i implements AbstractC17410ql {
    public final /* synthetic */ AvatarHomeViewModel A00;

    @Override // X.AbstractC17410ql
    public void AMk() {
    }

    public C1111658i(AvatarHomeViewModel avatarHomeViewModel) {
        this.A00 = avatarHomeViewModel;
    }

    @Override // X.AbstractC17410ql
    public void AMj() {
        AvatarHomeViewModel.A00(this.A00, false);
    }

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
        AvatarHomeViewModel.A00(this.A00, true);
    }
}
