package X;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;

/* renamed from: X.3LP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LP implements SensorEventListener {
    public final /* synthetic */ AnonymousClass294 A00;

    @Override // android.hardware.SensorEventListener
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public AnonymousClass3LP(AnonymousClass294 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f3, code lost:
        if (r6 != 3) goto L_0x00f5;
     */
    @Override // android.hardware.SensorEventListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSensorChanged(android.hardware.SensorEvent r14) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3LP.onSensorChanged(android.hardware.SensorEvent):void");
    }
}
