package X;

/* renamed from: X.3FI  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FI {
    public final long A00;
    public final long A01;
    public final C15820nx A02;
    public final String A03;
    public final String A04;

    public AnonymousClass3FI(C15820nx r1, String str, String str2, long j, long j2) {
        this.A02 = r1;
        this.A04 = str;
        this.A03 = str2;
        this.A00 = j;
        this.A01 = j2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3FI.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3FI r7 = (AnonymousClass3FI) obj;
            if (this.A00 != r7.A00 || this.A01 != r7.A01 || !this.A04.equals(r7.A04) || !this.A03.equals(r7.A03)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[4];
        objArr[0] = this.A04;
        objArr[1] = this.A03;
        objArr[2] = Long.valueOf(this.A00);
        return C12980iv.A0B(Long.valueOf(this.A01), objArr, 3);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("FileMetadata{name='");
        char A00 = C12990iw.A00(this.A04, A0k);
        A0k.append(", md5Hash='");
        A0k.append(this.A03);
        A0k.append(A00);
        A0k.append(", sizeBytes=");
        A0k.append(this.A00);
        A0k.append(", updateTime=");
        A0k.append(this.A01);
        return C12970iu.A0v(A0k);
    }
}
