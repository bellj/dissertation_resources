package X;

/* renamed from: X.28X  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass28X extends AnonymousClass28Y {
    public static final AnonymousClass28W EMPTY = new AnonymousClass28X(new Object[0], 0, 0, 0);
    public final Object[] array;

    public AnonymousClass28X(Object[] objArr, int i, int i2, int i3) {
        super(0, 0);
        this.array = objArr;
    }

    @Override // X.AnonymousClass28Y
    public Object get(int i) {
        return this.array[0 + i];
    }
}
