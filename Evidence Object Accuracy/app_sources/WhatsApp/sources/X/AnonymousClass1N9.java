package X;

import android.util.SparseArray;
import java.util.List;

/* renamed from: X.1N9  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1N9 {
    boolean A6w();

    void A7C();

    AnonymousClass1NA A8e();

    void A9F(List list);

    boolean AA2();

    SparseArray ACP();

    int AEc(int i);

    boolean AIB();

    void AIm();

    void AIx();

    void AKX();

    void Aau();
}
