package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2AS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2AS extends RecyclerView {
    public int A00 = -1;
    public int A01;
    public int A02 = getResources().getDimensionPixelSize(R.dimen.icebreaker_recyclerview_default_max_height);
    public int A03;
    public int A04 = -1;
    public View A05;
    public ViewGroup A06;
    public TextView A07;
    public AnonymousClass4NW A08;
    public C64633Gh A09 = new C64633Gh(this);
    public C64633Gh A0A = new C64633Gh(this);
    public C54152gG A0B;
    public C252718t A0C;
    public List A0D;
    public boolean A0E = false;

    public AnonymousClass2AS(Context context, View view, ViewGroup viewGroup, TextView textView, AnonymousClass4NW r8, C252718t r9) {
        super(context);
        this.A05 = view;
        this.A07 = textView;
        this.A06 = viewGroup;
        this.A0C = r9;
        this.A08 = r8;
        setBackgroundColor(AnonymousClass00T.A00(getContext(), R.color.icebreaker_recyclerview_background));
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.A0B = new C54152gG();
        getContext();
        setLayoutManager(new LinearLayoutManager());
        setAdapter(this.A0B);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r18, int r19) {
        /*
        // Method dump skipped, instructions count: 295
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2AS.onMeasure(int, int):void");
    }

    public void setData(String str, List list, AnonymousClass5RV r7) {
        this.A0D = new ArrayList();
        if (str != null && !TextUtils.isEmpty(str)) {
            this.A0D.add(new AnonymousClass42T(str));
        }
        this.A00 = -1;
        this.A04 = -1;
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                this.A0D.add(new C61282zm((C92664Wv) list.get(i), r7));
                if (i == 0) {
                    this.A03 = this.A0D.size() - 1;
                } else if (i == 2) {
                    this.A04 = this.A0D.size() - 1;
                }
                this.A0D.add(new AnonymousClass4WS(3));
            }
        }
        this.A0B.A0F(this.A0D);
    }
}
