package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: X.09K  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09K extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnonymousClass0QQ A01;
    public final /* synthetic */ AbstractC12530i4 A02;

    public AnonymousClass09K(View view, AnonymousClass0QQ r2, AbstractC12530i4 r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = view;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A02.AMB(this.A00);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A02.AMC(this.A00);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A02.AMD(this.A00);
    }
}
