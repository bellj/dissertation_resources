package X;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.3Fv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64513Fv {
    public int A00 = -1;
    public int A01 = -1;
    public int A02 = -1;
    public AnonymousClass18Q A03;
    public String A04;
    public final ArrayList A05 = C12960it.A0l();
    public final HashMap A06 = C12970iu.A11();
    public final CopyOnWriteArrayList A07 = new CopyOnWriteArrayList();

    public int A00(String str) {
        Number number = (Number) this.A06.get(str);
        if (number != null) {
            return number.intValue();
        }
        return 0;
    }

    public void A01() {
        this.A07.add("done");
    }

    public synchronized void A02(String str) {
        AnonymousClass18Q r1 = this.A03;
        if (r1 != null) {
            r1.AL9(str, 123);
        }
        this.A07.add(str);
    }

    public synchronized void A03(String str) {
        AnonymousClass18Q r1 = this.A03;
        if (r1 != null) {
            r1.AL9(str, 123);
        }
        this.A07.add(str);
    }

    public synchronized void A04(String str) {
        AnonymousClass18Q r1 = this.A03;
        if (r1 != null) {
            r1.AL9(str, 123);
        }
        this.A07.add(str);
    }

    public synchronized void A05(String str) {
        this.A05.add(str);
        this.A07.add(C12960it.A0d("-success", C12960it.A0j(str)));
    }

    public synchronized void A06(String str, int i) {
        int i2;
        HashMap hashMap = this.A06;
        Integer num = (Integer) hashMap.get(str);
        if (num == null) {
            i2 = 0;
        } else {
            i2 = num.intValue();
        }
        Integer A0i = C12970iu.A0i(str, hashMap, Integer.valueOf(i2).intValue() + 1);
        this.A00 = i;
        this.A04 = str;
        CopyOnWriteArrayList copyOnWriteArrayList = this.A07;
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append("-error-");
        A0j.append(A0i);
        copyOnWriteArrayList.add(C12960it.A0e("-", A0j, i));
    }

    public boolean A07(String str) {
        Number number = (Number) this.A06.get(str);
        if (!this.A05.contains(str)) {
            return number == null || number.intValue() < 3;
        }
        return false;
    }

    public String toString() {
        return this.A07.toString();
    }
}
