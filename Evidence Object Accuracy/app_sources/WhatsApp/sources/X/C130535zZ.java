package X;

import java.util.ArrayList;

/* renamed from: X.5zZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130535zZ {
    public static final ArrayList A01;
    public static final ArrayList A02;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A02 = C12960it.A0m("1", strArr, 1);
        String[] strArr2 = new String[2];
        strArr2[0] = "0";
        A01 = C12960it.A0m("1", strArr2, 1);
    }

    public C130535zZ(AnonymousClass3CT r14, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-set-mpin");
        if (str != null && C117295Zj.A1U(str)) {
            C41141sy.A01(A0N, "vpa", str);
        }
        if (str2 != null && C117295Zj.A1U(str2)) {
            C41141sy.A01(A0N, "vpa-id", str2);
        }
        if (str3 != null && AnonymousClass3JT.A0E(str3, 1, 100000, true)) {
            C41141sy.A01(A0N, "upi-bank-info", str3);
        }
        if (AnonymousClass3JT.A0E(str4, 1, 100, false)) {
            C41141sy.A01(A0N, "credential-id", str4);
        }
        if (AnonymousClass3JT.A0E(str5, 35, 35, false)) {
            C41141sy.A01(A0N, "seq-no", str5);
        }
        if (C117295Zj.A1V(str6, 1, false)) {
            C41141sy.A01(A0N, "device-id", str6);
        }
        if (AnonymousClass3JT.A0E(str7, 0, 10000, false)) {
            C41141sy.A01(A0N, "otp", str7);
        }
        if (AnonymousClass3JT.A0E(str8, 0, 10000, false)) {
            C41141sy.A01(A0N, "mpin", str8);
        }
        if (str9 != null && AnonymousClass3JT.A0E(str9, 0, 10000, true)) {
            C41141sy.A01(A0N, "atm-pin", str9);
        }
        if (AnonymousClass3JT.A0E(str10, 6, 6, false)) {
            C41141sy.A01(A0N, "debit-last-6", str10);
        }
        if (AnonymousClass3JT.A0E(str11, 1, 2, false)) {
            C41141sy.A01(A0N, "debit-exp-month", str11);
        }
        if (AnonymousClass3JT.A0E(str12, 2, 2, false)) {
            C41141sy.A01(A0N, "debit-exp-year", str12);
        }
        A0N.A0A("1", "default-debit", A02);
        A0N.A0A("1", "default-credit", A01);
        this.A00 = C117295Zj.A0J(A0N, A0M, r14);
    }
}
