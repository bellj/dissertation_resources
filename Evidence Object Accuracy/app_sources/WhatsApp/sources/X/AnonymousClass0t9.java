package X;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0t9  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass0t9 {
    public AnonymousClass1WC A00 = new AnonymousClass1WC(this);
    public AnonymousClass1WD A01;
    public final C247316q A02;
    public final C14820m6 A03;
    public final AnonymousClass018 A04;
    public final C14850m9 A05;
    public final AtomicBoolean A06 = new AtomicBoolean(false);
    public final AbstractC16710pd A07 = AnonymousClass4Yq.A00(new AnonymousClass1WH(this));

    public AnonymousClass0t9(C247316q r3, C14820m6 r4, AnonymousClass018 r5, C14850m9 r6) {
        C16700pc.A0E(r4, 1);
        C16700pc.A0E(r5, 2);
        C16700pc.A0E(r6, 4);
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
        this.A05 = r6;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:23:0x0039 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v0, types: [X.0t9] */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v1, types: [X.1WF] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass1WE A00(java.lang.String r7) {
        /*
            r6 = this;
            X.1WG r0 = r6.A02()
            r5 = 0
            if (r0 == 0) goto L_0x0037
            java.util.List r0 = r0.A00
            if (r0 == 0) goto L_0x0037
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.Iterator r3 = r0.iterator()
        L_0x0014:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0039
            java.lang.Object r2 = r3.next()
            r1 = r2
            X.1WE r1 = (X.AnonymousClass1WE) r1
            java.lang.String r0 = r1.A03
            boolean r0 = X.C16700pc.A0O(r0, r7)
            if (r0 == 0) goto L_0x0014
            java.lang.String r1 = r1.A06
            java.lang.String r0 = "android"
            boolean r0 = X.C16700pc.A0O(r1, r0)
            if (r0 == 0) goto L_0x0014
            r4.add(r2)
            goto L_0x0014
        L_0x0037:
            X.1WF r4 = X.AnonymousClass1WF.A00
        L_0x0039:
            X.1WE r2 = r6.A01(r4)
            X.0pd r0 = r6.A07
            java.lang.Object r0 = r0.getValue()
            java.util.Map r0 = (java.util.Map) r0
            java.lang.Object r1 = r0.get(r7)
            if (r1 == 0) goto L_0x0056
            if (r2 == 0) goto L_0x0056
            java.lang.String r0 = r2.A00
            boolean r0 = X.C16700pc.A0O(r0, r1)
            if (r0 != 0) goto L_0x0056
            return r5
        L_0x0056:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0t9.A00(java.lang.String):X.1WE");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0076, code lost:
        if (r3 == null) goto L_0x0078;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass1WE A01(java.util.List r10) {
        /*
            r9 = this;
            java.util.Iterator r2 = r10.iterator()
        L_0x0004:
            boolean r0 = r2.hasNext()
            r8 = 0
            if (r0 == 0) goto L_0x0096
            java.lang.Object r3 = r2.next()
            r0 = r3
            X.1WE r0 = (X.AnonymousClass1WE) r0
            java.lang.String r1 = r0.A05
            X.018 r0 = r9.A04
            java.lang.String r0 = r0.A07()
            boolean r0 = X.C16700pc.A0O(r1, r0)
            if (r0 == 0) goto L_0x0004
        L_0x0020:
            X.1WE r3 = (X.AnonymousClass1WE) r3
            if (r3 != 0) goto L_0x0082
            java.util.Iterator r2 = r10.iterator()
        L_0x0028:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0080
            java.lang.Object r3 = r2.next()
            r0 = r3
            X.1WE r0 = (X.AnonymousClass1WE) r0
            java.lang.String r1 = r0.A05
            X.018 r0 = r9.A04
            android.content.Context r0 = r0.A00
            java.util.Locale r0 = X.AnonymousClass018.A00(r0)
            java.lang.String r0 = r0.getLanguage()
            boolean r0 = X.C16700pc.A0O(r1, r0)
            if (r0 == 0) goto L_0x0028
        L_0x0049:
            X.1WE r3 = (X.AnonymousClass1WE) r3
            if (r3 != 0) goto L_0x0082
            java.util.Iterator r2 = r10.iterator()
        L_0x0051:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x007e
            java.lang.Object r3 = r2.next()
            r0 = r3
            X.1WE r0 = (X.AnonymousClass1WE) r0
            java.lang.String r1 = r0.A05
            java.lang.String r0 = "en"
            boolean r0 = X.C16700pc.A0O(r1, r0)
            if (r0 == 0) goto L_0x0051
        L_0x0068:
            X.1WE r3 = (X.AnonymousClass1WE) r3
            if (r3 != 0) goto L_0x0082
            r1 = 0
            boolean r0 = r10.isEmpty()
            if (r0 == 0) goto L_0x0079
            r3 = 0
        L_0x0074:
            X.1WE r3 = (X.AnonymousClass1WE) r3
            if (r3 != 0) goto L_0x0082
        L_0x0078:
            return r8
        L_0x0079:
            java.lang.Object r3 = r10.get(r1)
            goto L_0x0074
        L_0x007e:
            r3 = r8
            goto L_0x0068
        L_0x0080:
            r3 = r8
            goto L_0x0049
        L_0x0082:
            long r4 = r3.A01
            java.util.Date r0 = new java.util.Date
            r0.<init>()
            long r6 = r0.getTime()
            r0 = 1000(0x3e8, float:1.401E-42)
            long r0 = (long) r0
            long r6 = r6 / r0
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0078
            return r3
        L_0x0096:
            r3 = r8
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0t9.A01(java.util.List):X.1WE");
    }

    public final AnonymousClass1WG A02() {
        String string = this.A03.A00.getString("commerce_metadata", null);
        if (string != null) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                ArrayList arrayList = new ArrayList();
                JSONArray optJSONArray = jSONObject.optJSONArray("bloksLinks");
                int i = 0;
                if (optJSONArray != null) {
                    int length = optJSONArray.length();
                    while (i < length) {
                        int i2 = i + 1;
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        if (optJSONObject != null) {
                            String optString = optJSONObject.optString("url");
                            String optString2 = optJSONObject.optString("locale");
                            long optLong = optJSONObject.optLong("expiresData", 0);
                            String optString3 = optJSONObject.optString("appId");
                            String optString4 = optJSONObject.optString("version");
                            String optString5 = optJSONObject.optString("platform");
                            String optString6 = optJSONObject.optString("bizJid");
                            long optLong2 = optJSONObject.optLong("flowVersionId");
                            String optString7 = optJSONObject.optString("signature");
                            C16700pc.A0B(optString);
                            C16700pc.A0B(optString2);
                            C16700pc.A0B(optString3);
                            C16700pc.A0B(optString5);
                            arrayList.add(new AnonymousClass1WE(Long.valueOf(optLong2), optString, optString2, optString3, optString4, optString5, optString6, optString7, optLong));
                        }
                        i = i2;
                    }
                }
                return new AnonymousClass1WG(arrayList);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public final void A03() {
        AtomicBoolean atomicBoolean = this.A06;
        if (!atomicBoolean.get()) {
            atomicBoolean.set(true);
            C247316q r7 = this.A02;
            r7.A00 = this.A00;
            C17220qS r6 = r7.A02;
            String A01 = r6.A01();
            C16700pc.A0B(A01);
            r6.A09(r7, new AnonymousClass1V8(new AnonymousClass1V8(new AnonymousClass1V8("bloks_links", new AnonymousClass1W9[0]), "commerce_metadata", new AnonymousClass1W9[0]), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("xmlns", "fb:thrift_iq"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("smax_id", "91"), new AnonymousClass1W9("id", A01)}), A01, 326, 32000);
        }
    }
}
