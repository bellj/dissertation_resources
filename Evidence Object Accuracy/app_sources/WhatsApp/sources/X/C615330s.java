package X;

/* renamed from: X.30s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615330s extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public String A05;
    public String A06;

    public C615330s() {
        super(1780, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(3, this.A00);
        r3.Abe(5, this.A04);
        r3.Abe(6, this.A05);
        r3.Abe(1, this.A01);
        r3.Abe(7, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidFetchBloksRequest {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bloksCategory", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bloksFetchRetryCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bloksFetchSuccess", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bloksFetchTimeT", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "bloksVersion", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isTriggeredOnBackground", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "triggeringSource", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
