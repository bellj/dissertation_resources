package X;

import android.widget.AbsListView;
import android.widget.ListView;

/* renamed from: X.3O4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3O4 implements AbsListView.OnScrollListener {
    public int A00;
    public int A01;
    public final /* synthetic */ AbstractActivityC35431hr A02;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public AnonymousClass3O4(AbstractActivityC35431hr r1) {
        this.A02 = r1;
    }

    public final void A00(int i, int i2) {
        AbstractC15340mz A05;
        AbstractActivityC35431hr r4 = this.A02;
        int count = r4.A07.getCount();
        while (i <= i2) {
            ListView A2e = r4.A2e();
            AnonymousClass009.A03(A2e);
            int headerViewsCount = i - A2e.getHeaderViewsCount();
            if (headerViewsCount >= 0 && headerViewsCount <= count - 1 && (A05 = r4.A07.getItem(headerViewsCount)) != null && A05.A0y == 13) {
                r4.A8w(A05.A0z);
            }
            i++;
        }
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        int i4;
        if (!(i2 == 0 || (i4 = this.A01) == 0)) {
            int i5 = i + i2;
            int i6 = this.A00;
            int i7 = i4 + i6;
            if (i6 < i) {
                A00(i6, i - 1);
            } else if (i5 < i7) {
                A00(i5 + 1, i7);
            }
        }
        this.A00 = i;
        this.A01 = i2;
    }
}
