package X;

import java.util.List;

/* renamed from: X.4Q3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Q3 {
    public final List A00;
    public final List A01;
    public final List A02;

    public AnonymousClass4Q3(List list, List list2, List list3) {
        this.A02 = list;
        this.A00 = list2;
        this.A01 = list3;
    }
}
