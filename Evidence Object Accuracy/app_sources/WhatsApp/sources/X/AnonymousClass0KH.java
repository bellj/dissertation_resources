package X;

import android.hardware.biometrics.BiometricPrompt;

/* renamed from: X.0KH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KH {
    public static void A00(BiometricPrompt.Builder builder, int i) {
        builder.setAllowedAuthenticators(i);
    }
}
