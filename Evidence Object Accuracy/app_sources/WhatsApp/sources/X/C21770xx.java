package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.Jid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Future;

/* renamed from: X.0xx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21770xx {
    public C27151Gf A00 = null;
    public final C21760xw A01;

    public C21770xx(C21760xw r2) {
        this.A01 = r2;
    }

    public static final int A00(C92584Wm r5, C16310on r6, Jid jid) {
        return r6.A03.A00("cart_item", A01(r5, jid), "business_id=?  AND product_id=?", new String[]{jid.getRawString(), r5.A01.A0D});
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.ContentValues A01(X.C92584Wm r9, com.whatsapp.jid.Jid r10) {
        /*
            X.1zO r4 = r9.A01
            java.math.BigDecimal r3 = r4.A05
            r0 = 4652007308841189376(0x408f400000000000, double:1000.0)
            r7 = 0
            if (r3 == 0) goto L_0x00c1
            java.math.BigDecimal r2 = new java.math.BigDecimal
            r2.<init>(r0)
            java.math.BigDecimal r2 = r3.multiply(r2)
            long r2 = r2.longValue()
            java.lang.Long r8 = java.lang.Long.valueOf(r2)
        L_0x001d:
            X.3M7 r2 = r4.A02
            if (r2 == 0) goto L_0x00bd
            java.math.BigDecimal r3 = r2.A01
            java.math.BigDecimal r2 = new java.math.BigDecimal
            r2.<init>(r0)
            java.math.BigDecimal r0 = r3.multiply(r2)
            long r0 = r0.longValue()
            java.lang.Long r6 = java.lang.Long.valueOf(r0)
            X.3M7 r0 = r4.A02
            java.util.Date r0 = r0.A03
            if (r0 == 0) goto L_0x00bb
            long r0 = r0.getTime()
            java.lang.Long r5 = java.lang.Long.valueOf(r0)
        L_0x0042:
            X.3M7 r0 = r4.A02
            java.util.Date r0 = r0.A02
            if (r0 == 0) goto L_0x00bf
            long r0 = r0.getTime()
            java.lang.Long r3 = java.lang.Long.valueOf(r0)
        L_0x0050:
            X.1Yn r0 = r4.A03
            if (r0 == 0) goto L_0x0056
            java.lang.String r7 = r0.A00
        L_0x0056:
            android.content.ContentValues r2 = new android.content.ContentValues
            r2.<init>()
            java.lang.String r1 = r10.getRawString()
            java.lang.String r0 = "business_id"
            r2.put(r0, r1)
            java.lang.String r1 = r4.A0D
            java.lang.String r0 = "product_id"
            r2.put(r0, r1)
            java.lang.String r1 = r4.A04
            java.lang.String r0 = "product_title"
            r2.put(r0, r1)
            java.lang.String r0 = "product_price_1000"
            r2.put(r0, r8)
            java.lang.String r0 = "product_currency_code"
            r2.put(r0, r7)
            java.util.List r1 = r4.A06
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x00b8
            r0 = 0
            java.lang.Object r0 = r1.get(r0)
            X.1zT r0 = (X.C44741zT) r0
            java.lang.String r1 = r0.A04
        L_0x008d:
            java.lang.String r0 = "product_image_id"
            r2.put(r0, r1)
            long r0 = r9.A00
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "product_quantity"
            r2.put(r0, r1)
            java.lang.String r0 = "product_sale_price_1000"
            r2.put(r0, r6)
            java.lang.String r0 = "product_sale_start_date"
            r2.put(r0, r5)
            java.lang.String r0 = "product_sale_end_date"
            r2.put(r0, r3)
            long r0 = r4.A08
            java.lang.Long r1 = java.lang.Long.valueOf(r0)
            java.lang.String r0 = "product_max_available"
            r2.put(r0, r1)
            return r2
        L_0x00b8:
            java.lang.String r1 = ""
            goto L_0x008d
        L_0x00bb:
            r5 = r7
            goto L_0x0042
        L_0x00bd:
            r6 = r7
            r5 = r7
        L_0x00bf:
            r3 = r7
            goto L_0x0050
        L_0x00c1:
            r8 = r7
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21770xx.A01(X.4Wm, com.whatsapp.jid.Jid):android.content.ContentValues");
    }

    public static final C92584Wm A02(Cursor cursor) {
        Long valueOf;
        Long valueOf2;
        Long valueOf3;
        Long valueOf4;
        long j;
        C30711Yn r11;
        BigDecimal bigDecimal;
        String string = cursor.getString(cursor.getColumnIndexOrThrow("product_id"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("product_title"));
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("product_price_1000");
        AnonymousClass3M7 r10 = null;
        if (cursor.isNull(columnIndexOrThrow)) {
            valueOf = null;
        } else {
            valueOf = Long.valueOf(cursor.getLong(columnIndexOrThrow));
        }
        String string3 = cursor.getString(cursor.getColumnIndexOrThrow("product_currency_code"));
        long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("product_quantity"));
        String string4 = cursor.getString(cursor.getColumnIndexOrThrow("product_image_id"));
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("product_sale_price_1000");
        if (cursor.isNull(columnIndexOrThrow2)) {
            valueOf2 = null;
        } else {
            valueOf2 = Long.valueOf(cursor.getLong(columnIndexOrThrow2));
        }
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("product_sale_start_date");
        if (cursor.isNull(columnIndexOrThrow3)) {
            valueOf3 = null;
        } else {
            valueOf3 = Long.valueOf(cursor.getLong(columnIndexOrThrow3));
        }
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("product_sale_end_date");
        if (cursor.isNull(columnIndexOrThrow4)) {
            valueOf4 = null;
        } else {
            valueOf4 = Long.valueOf(cursor.getLong(columnIndexOrThrow4));
        }
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("product_max_available");
        if (cursor.isNull(columnIndexOrThrow5)) {
            j = 99;
        } else {
            j = cursor.getLong(columnIndexOrThrow5);
        }
        if (!TextUtils.isEmpty(string3)) {
            r11 = new C30711Yn(string3);
        } else {
            r11 = null;
        }
        if (valueOf == null || r11 == null) {
            bigDecimal = null;
        } else {
            bigDecimal = C30701Ym.A00(r11, valueOf.longValue());
        }
        if (!(valueOf2 == null || r11 == null)) {
            BigDecimal A00 = C30701Ym.A00(r11, valueOf2.longValue());
            r10 = (valueOf3 == null || valueOf4 == null) ? new AnonymousClass3M7(r11, A00, null, null) : new AnonymousClass3M7(r11, A00, new Date(valueOf3.longValue()), new Date(valueOf4.longValue()));
        }
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.isEmpty(string4)) {
            arrayList.add(new C44741zT(string4, "", "", 0, 0));
        }
        return new C92584Wm(new C44691zO(null, null, r10, r11, string, string2, null, null, null, null, bigDecimal, arrayList, 0, j, true, false), j2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        if (r1 != null) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C92584Wm A03(com.whatsapp.jid.Jid r7, java.lang.String r8) {
        /*
            r6 = this;
            X.0xw r0 = r6.A01
            X.1xb r0 = r0.A00()
            X.0on r5 = r0.get()
            X.0op r4 = r5.A03     // Catch: all -> 0x003b
            java.lang.String r3 = "SELECT product_id, product_title, product_price_1000, product_currency_code, product_image_id, product_quantity, product_sale_price_1000, product_sale_start_date, product_sale_end_date, product_max_available FROM cart_item WHERE business_id=? AND product_id=?"
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x003b
            r1 = 0
            java.lang.String r0 = r7.getRawString()     // Catch: all -> 0x003b
            r2[r1] = r0     // Catch: all -> 0x003b
            r0 = 1
            r2[r0] = r8     // Catch: all -> 0x003b
            android.database.Cursor r1 = r4.A09(r3, r2)     // Catch: all -> 0x003b
            if (r1 == 0) goto L_0x0031
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x002c
            if (r0 == 0) goto L_0x0031
            X.4Wm r0 = A02(r1)     // Catch: all -> 0x002c
            goto L_0x0034
        L_0x002c:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0030
        L_0x0030:
            throw r0     // Catch: all -> 0x003b
        L_0x0031:
            r0 = 0
            if (r1 == 0) goto L_0x0037
        L_0x0034:
            r1.close()     // Catch: all -> 0x003b
        L_0x0037:
            r5.close()
            return r0
        L_0x003b:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x003f
        L_0x003f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21770xx.A03(com.whatsapp.jid.Jid, java.lang.String):X.4Wm");
    }

    public Future A04(Jid jid) {
        AnonymousClass1VC r6 = new AnonymousClass1VC();
        try {
            C16310on A01 = this.A01.A00().get();
            Cursor A09 = A01.A03.A09("SELECT product_id, product_title, product_price_1000, product_currency_code, product_image_id, product_quantity, product_sale_price_1000, product_sale_start_date, product_sale_end_date, product_max_available FROM cart_item WHERE business_id=?", new String[]{jid.getRawString()});
            try {
                ArrayList arrayList = new ArrayList();
                if (A09 != null) {
                    while (A09.moveToNext()) {
                        arrayList.add(A02(A09));
                    }
                }
                r6.A01(arrayList);
                if (A09 != null) {
                    A09.close();
                }
                A01.close();
                return r6;
            } catch (Throwable th) {
                if (A09 != null) {
                    try {
                        A09.close();
                    } catch (Throwable unused) {
                    }
                }
                throw th;
            }
        } catch (Exception e) {
            r6.A00(e);
            return r6;
        }
    }

    public void A05(C92584Wm r9, Jid jid) {
        C16310on A02 = this.A01.A00().A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            String str = r9.A01.A0D;
            C16330op r6 = A02.A03;
            int i = 0;
            Cursor A09 = r6.A09("SELECT product_quantity FROM cart_item WHERE business_id=? AND product_id=?", new String[]{jid.getRawString(), str});
            if (A09 != null) {
                if (A09.moveToNext()) {
                    i = A09.getInt(A09.getColumnIndexOrThrow("product_quantity"));
                }
                A09.close();
            }
            if (((long) i) > 0) {
                A00(r9, A02, jid);
            } else {
                r6.A02(A01(r9, jid), "cart_item");
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A06(Jid jid) {
        C16310on A02 = this.A01.A00().A02();
        try {
            A02.A03.A0C("DELETE FROM cart_item WHERE business_id=?", new String[]{jid.getRawString()});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A07(Jid jid, String str) {
        C16310on A02 = this.A01.A00().A02();
        try {
            A02.A03.A0C("DELETE FROM cart_item WHERE business_id=? AND product_id=?", new String[]{jid.getRawString(), str});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
