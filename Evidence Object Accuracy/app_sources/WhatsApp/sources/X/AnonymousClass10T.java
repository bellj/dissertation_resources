package X;

import android.content.Context;
import android.content.res.Resources;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import java.io.File;

/* renamed from: X.10T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10T {
    public final C14330lG A00;
    public final C15570nT A01;
    public final C18720su A02;
    public final C16590pI A03;

    public AnonymousClass10T(C14330lG r1, C15570nT r2, C18720su r3, C16590pI r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public File A00(C15370n3 r6) {
        StringBuilder sb;
        if (r6 instanceof AnonymousClass1VR) {
            return A02(r6);
        }
        Jid A0B = r6.A0B(AbstractC14640lm.class);
        if (A0B == null) {
            return null;
        }
        boolean A0F = this.A01.A0F(A0B);
        Context context = this.A03.A00;
        if (A0F) {
            return new File(context.getFilesDir(), "me.jpg");
        }
        File file = new File(context.getCacheDir(), "Profile Pictures");
        if (!file.exists()) {
            file.mkdirs();
        }
        String str = A0B.user;
        if (str != null) {
            sb = new StringBuilder();
        } else {
            sb = new StringBuilder();
            str = A0B.getRawString();
        }
        sb.append(str);
        sb.append(".jpg");
        return new File(file, sb.toString());
    }

    public File A01(C15370n3 r5) {
        String rawString;
        if (r5 instanceof AnonymousClass1VR) {
            return A02(r5);
        }
        Jid A0B = r5.A0B(AbstractC14640lm.class);
        if (A0B == null) {
            return null;
        }
        File file = new File(this.A03.A00.getFilesDir(), "Avatars");
        if (!file.exists()) {
            file.mkdirs();
        }
        if (this.A01.A0F(A0B)) {
            rawString = "me";
        } else {
            rawString = A0B.getRawString();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(rawString);
        sb.append(".j");
        return new File(file, sb.toString());
    }

    public File A02(C15370n3 r5) {
        String str;
        C14340lH A04;
        if (r5 instanceof AnonymousClass1VR) {
            C14330lG r3 = this.A00;
            StringBuilder sb = new StringBuilder("tmpp");
            sb.append(((AnonymousClass1VR) r5).A00);
            str = sb.toString();
            A04 = r3.A04();
        } else {
            str = "tmpp";
            A04 = this.A00.A04();
        }
        File file = A04.A09;
        C14330lG.A03(file, false);
        return C14330lG.A00(file, str);
    }

    public void A03(C15370n3 r3) {
        File A00 = A00(r3);
        if (A00 != null && A00.exists()) {
            A00.delete();
        }
        File A01 = A01(r3);
        if (A01 != null && A01.exists()) {
            A01.delete();
        }
    }

    public void A04(C15370n3 r6) {
        String A0C = r6.A0C();
        C006202y r3 = this.A02.A01().A00;
        for (String str : r3.A05().keySet()) {
            if (str.startsWith(A0C)) {
                r3.A07(str);
            }
        }
        r6.A0X = true;
    }

    public boolean A05(C15370n3 r4) {
        Resources resources = this.A03.A00.getResources();
        return this.A02.A01().A00(r4.A0E(resources.getDimension(R.dimen.small_avatar_radius), resources.getDimensionPixelSize(R.dimen.small_avatar_size))) != null;
    }

    public boolean A06(C15370n3 r3) {
        File A01 = A01(r3);
        return ((A01 != null && A01.exists()) || (A01 = A00(r3)) != null) && A01.exists();
    }
}
