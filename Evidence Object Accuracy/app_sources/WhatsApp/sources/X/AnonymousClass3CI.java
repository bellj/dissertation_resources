package X;

import java.util.List;

/* renamed from: X.3CI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CI {
    public final List A00;

    public AnonymousClass3CI(List list) {
        this.A00 = list;
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        for (Object obj : this.A00) {
            A0h.append(System.getProperty("line.separator"));
            C12970iu.A1V(obj, A0h);
        }
        Object[] A1b = C12970iu.A1b();
        A1b[0] = A0h;
        return String.format("GetCategoriesResponse{categoryResponses=%s}", A1b);
    }
}
