package X;

import com.whatsapp.group.GroupChatInfo;
import java.lang.ref.WeakReference;

/* renamed from: X.38c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C626638c extends AbstractC16350or {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C22330yu A02;
    public final C22640zP A03;
    public final C15610nY A04;
    public final C15370n3 A05;
    public final C17220qS A06;
    public final AbstractC14440lR A07;
    public final WeakReference A08;

    public C626638c(AbstractC15710nm r2, C14900mE r3, C22330yu r4, C22640zP r5, C15610nY r6, C15370n3 r7, GroupChatInfo groupChatInfo, C17220qS r9, AbstractC14440lR r10) {
        this.A01 = r3;
        this.A00 = r2;
        this.A07 = r10;
        this.A06 = r9;
        this.A04 = r6;
        this.A02 = r4;
        this.A03 = r5;
        this.A05 = r7;
        this.A08 = C12970iu.A10(groupChatInfo);
    }
}
