package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;

/* renamed from: X.5bA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118035bA extends AnonymousClass015 {
    public AnonymousClass016 A00 = new AnonymousClass016(C12960it.A0l());
    public AnonymousClass1ZW A01;
    public C30931Zj A02 = C117305Zk.A0V("MerchantDetailsViewModel", "merchant-settings");
    public C27691It A03;
    public C27691It A04 = C13000ix.A03();
    public List A05;
    public List A06 = C12960it.A0l();
    public final AbstractC001200n A07;
    public final AbstractC15710nm A08;
    public final C14900mE A09;
    public final C14900mE A0A;
    public final C15570nT A0B;
    public final C18640sm A0C;
    public final C14830m7 A0D;
    public final C16590pI A0E;
    public final C15650ng A0F;
    public final C20370ve A0G;
    public final AnonymousClass102 A0H;
    public final C241414j A0I;
    public final C17220qS A0J;
    public final C1309660r A0K;
    public final C18650sn A0L;
    public final C18660so A0M;
    public final C18600si A0N;
    public final C18610sj A0O;
    public final C18620sk A0P;
    public final C17070qD A0Q;
    public final C17070qD A0R;
    public final AnonymousClass60T A0S;
    public final C18590sh A0T;
    public final AbstractC14440lR A0U;
    public final AbstractC14440lR A0V;

    public C118035bA(AbstractC001200n r4, AbstractC15710nm r5, C14900mE r6, C15570nT r7, C18640sm r8, C14830m7 r9, C16590pI r10, C15650ng r11, C20370ve r12, AnonymousClass102 r13, C241414j r14, C17220qS r15, C1309660r r16, C18650sn r17, C18660so r18, C18600si r19, C18610sj r20, C18620sk r21, C17070qD r22, AnonymousClass60T r23, C18590sh r24, AbstractC14440lR r25) {
        this.A07 = r4;
        this.A09 = r6;
        this.A0U = r25;
        this.A0I = r14;
        this.A0Q = r22;
        this.A0G = r12;
        this.A03 = C13000ix.A03();
        this.A0D = r9;
        this.A0A = r6;
        this.A08 = r5;
        this.A0B = r7;
        this.A0J = r15;
        this.A0E = r10;
        this.A0V = r25;
        this.A0T = r24;
        this.A0R = r22;
        this.A0F = r11;
        this.A0K = r16;
        this.A0N = r19;
        this.A0O = r20;
        this.A0H = r13;
        this.A0P = r21;
        this.A0C = r8;
        this.A0L = r17;
        this.A0S = r23;
        this.A0M = r18;
    }

    public static C27691It A00(C118035bA r2) {
        C128215vk r1 = new C128215vk(5);
        r1.A08 = false;
        C27691It r0 = r2.A03;
        r0.A0B(r1);
        return r0;
    }

    public final void A04(C119785f6 r10, String str) {
        C128215vk r1 = new C128215vk(5);
        r1.A08 = true;
        r1.A02 = R.string.payments_loading;
        this.A03.A0B(r1);
        Context context = this.A0E.A00;
        C14900mE r3 = this.A0A;
        new C129225xN(context, this.A08, r3, this.A0H, this.A0J, this.A0L, new AbstractC136206Lq(r10, this) { // from class: X.6AG
            public final /* synthetic */ C119785f6 A00;
            public final /* synthetic */ C118035bA A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136206Lq
            public final void AQz(C452120p r9, List list) {
                C118035bA r7 = this.A01;
                C119785f6 r6 = this.A00;
                C27691It A00 = C118035bA.A00(r7);
                if (r9 == null) {
                    JSONArray A002 = C1309660r.A00(list);
                    C128215vk r4 = new C128215vk(4);
                    HashMap A11 = C12970iu.A11();
                    r4.A07 = A11;
                    A11.put("update", "1");
                    HashMap hashMap = r4.A07;
                    AnonymousClass009.A05(A002);
                    hashMap.put("banks", A002.toString());
                    String str2 = ((AnonymousClass1ZX) r6).A02;
                    if (TextUtils.isEmpty(str2)) {
                        str2 = r7.A0B.A05();
                    }
                    r4.A07.put("business_name", str2);
                    r4.A06 = "brpay_m_payout_info";
                    A00.A0B(r4);
                    return;
                }
                C128215vk r0 = new C128215vk(7);
                r0.A04 = r9;
                A00.A0B(r0);
            }
        }, str).A00();
    }

    public void A05(C127105tx r14) {
        int i = r14.A00;
        if (i != 0) {
            if (i == 1) {
                C17070qD r0 = this.A0Q;
                r0.A03();
                List A0A = r0.A09.A0A();
                C30931Zj r2 = this.A02;
                StringBuilder A0k = C12960it.A0k("Remove merchant account. #methods=");
                A0k.append(A0A.size());
                C117295Zj.A1F(r2, A0k);
                int i2 = 1;
                if (A0A.size() <= 1) {
                    i2 = 0;
                }
                this.A04.A0B(new C126005sB(i2));
                return;
            } else if (i == 2) {
                boolean z = r14.A01;
                C128215vk r1 = new C128215vk(5);
                r1.A08 = true;
                r1.A02 = R.string.register_wait_message;
                this.A03.A0B(r1);
                C1327868f r02 = new C1327868f(this);
                if (z) {
                    Context context = this.A0E.A00;
                    C14900mE r3 = this.A0A;
                    AbstractC14440lR r12 = this.A0V;
                    C18590sh r11 = this.A0T;
                    C17070qD r10 = this.A0R;
                    new C129965ya(context, r3, this.A0C, this.A0F, this.A0L, this.A0N, this.A0O, this.A0P, r10, r11, r12).A00(r02);
                    return;
                }
                C14830m7 r4 = this.A0D;
                C14900mE r22 = this.A0A;
                C15570nT r32 = this.A0B;
                C17220qS r6 = this.A0J;
                C16590pI r5 = this.A0E;
                AbstractC14440lR r102 = this.A0V;
                new C129255xQ(r22, r32, r4, r5, r6, this.A0L, this.A0M, this.A0R, r102).A00(r02);
                return;
            } else if (i != 3) {
                return;
            }
        }
        this.A0U.Ab2(new Runnable() { // from class: X.6HO
            @Override // java.lang.Runnable
            public final void run() {
                AnonymousClass1ZW r62;
                C118035bA r8 = C118035bA.this;
                C241414j r13 = r8.A0I;
                if (r13.A09().size() > 0 && (r62 = (AnonymousClass1ZW) r13.A09().get(0)) != null) {
                    List A0D = r13.A0D();
                    Integer[] numArr = new Integer[1];
                    C12960it.A1P(numArr, 300, 0);
                    r8.A09.A0H(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0037: INVOKE  
                          (wrap: X.0mE : 0x0030: IGET  (r1v2 X.0mE A[REMOVE]) = (r8v0 'r8' X.5bA) X.5bA.A09 X.0mE)
                          (wrap: X.6Jx : 0x0034: CONSTRUCTOR  (r0v5 X.6Jx A[REMOVE]) = 
                          (r6v1 'r62' X.1ZW)
                          (r8v0 'r8' X.5bA)
                          (r5v0 'A0D' java.util.List)
                          (wrap: java.util.List : 0x002c: INVOKE  (r2v1 java.util.List A[REMOVE]) = 
                          (wrap: X.0ve : 0x001f: IGET  (r4v0 X.0ve A[REMOVE]) = (r8v0 'r8' X.5bA) X.5bA.A0G X.0ve)
                          (wrap: java.lang.Integer[] : 0x0022: NEW_ARRAY  (r2v0 java.lang.Integer[] A[REMOVE]) = (0 int) type: java.lang.Integer[])
                          (r1v1 'numArr' java.lang.Integer[])
                          (3 int)
                         type: VIRTUAL call: X.0ve.A0b(java.lang.Integer[], java.lang.Integer[], int):java.util.List)
                         call: X.6Jx.<init>(X.1ZW, X.5bA, java.util.List, java.util.List):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6HO.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0034: CONSTRUCTOR  (r0v5 X.6Jx A[REMOVE]) = 
                          (r6v1 'r62' X.1ZW)
                          (r8v0 'r8' X.5bA)
                          (r5v0 'A0D' java.util.List)
                          (wrap: java.util.List : 0x002c: INVOKE  (r2v1 java.util.List A[REMOVE]) = 
                          (wrap: X.0ve : 0x001f: IGET  (r4v0 X.0ve A[REMOVE]) = (r8v0 'r8' X.5bA) X.5bA.A0G X.0ve)
                          (wrap: java.lang.Integer[] : 0x0022: NEW_ARRAY  (r2v0 java.lang.Integer[] A[REMOVE]) = (0 int) type: java.lang.Integer[])
                          (r1v1 'numArr' java.lang.Integer[])
                          (3 int)
                         type: VIRTUAL call: X.0ve.A0b(java.lang.Integer[], java.lang.Integer[], int):java.util.List)
                         call: X.6Jx.<init>(X.1ZW, X.5bA, java.util.List, java.util.List):void type: CONSTRUCTOR in method: X.6HO.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Jx, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                        this = this;
                        X.5bA r8 = X.C118035bA.this
                        X.14j r1 = r8.A0I
                        java.util.List r0 = r1.A09()
                        int r0 = r0.size()
                        r7 = 0
                        if (r0 <= 0) goto L_0x003a
                        java.util.List r0 = r1.A09()
                        java.lang.Object r6 = r0.get(r7)
                        X.1ZW r6 = (X.AnonymousClass1ZW) r6
                        if (r6 == 0) goto L_0x003a
                        java.util.List r5 = r1.A0D()
                        X.0ve r4 = r8.A0G
                        r3 = 3
                        java.lang.Integer[] r2 = new java.lang.Integer[r7]
                        r0 = 1
                        java.lang.Integer[] r1 = new java.lang.Integer[r0]
                        r0 = 300(0x12c, float:4.2E-43)
                        X.C12960it.A1P(r1, r0, r7)
                        java.util.List r2 = r4.A0b(r2, r1, r3)
                        X.0mE r1 = r8.A09
                        X.6Jx r0 = new X.6Jx
                        r0.<init>(r6, r8, r5, r2)
                        r1.A0H(r0)
                    L_0x003a:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6HO.run():void");
                }
            });
        }
    }
