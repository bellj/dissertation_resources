package X;

import java.io.ByteArrayOutputStream;

/* renamed from: X.0Ij  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C03590Ij extends ByteArrayOutputStream {
    public C03590Ij(int i) {
        super(i);
    }

    @Override // java.io.ByteArrayOutputStream
    public byte[] toByteArray() {
        int i = ((ByteArrayOutputStream) this).count;
        byte[] bArr = ((ByteArrayOutputStream) this).buf;
        if (i == bArr.length) {
            return bArr;
        }
        return super.toByteArray();
    }
}
