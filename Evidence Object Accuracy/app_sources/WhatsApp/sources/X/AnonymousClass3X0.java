package X;

import android.graphics.Bitmap;
import com.whatsapp.mediacomposer.VideoComposerFragment;
import com.whatsapp.util.Log;

/* renamed from: X.3X0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X0 implements AnonymousClass23D {
    public final /* synthetic */ VideoComposerFragment A00;

    public AnonymousClass3X0(VideoComposerFragment videoComposerFragment) {
        this.A00 = videoComposerFragment;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        return this.A00.A0N.getAbsolutePath();
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        try {
            C38941ox r2 = new C38941ox();
            try {
                r2.setDataSource(this.A00.A0N.getAbsolutePath());
                Bitmap frameAtTime = r2.getFrameAtTime(1);
                r2.close();
                return frameAtTime;
            } catch (Throwable th) {
                try {
                    r2.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (Exception | NoSuchMethodError e) {
            Log.e("VideoComposerFragment/getvideothumb", e);
            return C26521Du.A01(this.A00.A0N);
        }
    }
}
