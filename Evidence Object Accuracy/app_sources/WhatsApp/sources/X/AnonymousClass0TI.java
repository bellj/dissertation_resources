package X;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0TI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TI {
    public static final String A00 = C06390Tk.A01("Schedulers");

    public static AbstractC12570i8 A00(Context context, AnonymousClass022 r9) {
        String str;
        AbstractC12570i8 r4;
        if (Build.VERSION.SDK_INT >= 23) {
            C07670Zr r42 = new C07670Zr(context, r9);
            AnonymousClass0RF.A00(context, SystemJobService.class, true);
            C06390Tk.A00().A02(A00, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return r42;
        }
        try {
            r4 = (AbstractC12570i8) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            C06390Tk A002 = C06390Tk.A00();
            str = A00;
            A002.A02(str, String.format("Created %s", "androidx.work.impl.background.gcm.GcmScheduler"), new Throwable[0]);
        } catch (Throwable th) {
            C06390Tk A003 = C06390Tk.A00();
            str = A00;
            A003.A02(str, "Unable to create GCM Scheduler", th);
        }
        if (r4 != null) {
            return r4;
        }
        C07660Zq r43 = new C07660Zq(context);
        AnonymousClass0RF.A00(context, SystemAlarmService.class, true);
        C06390Tk.A00().A02(str, "Created SystemAlarmScheduler", new Throwable[0]);
        return r43;
    }

    /* JADX INFO: finally extract failed */
    public static void A01(C05180Oo r34, WorkDatabase workDatabase, List list) {
        if (list != null && list.size() != 0) {
            AbstractC12700iM A0B = workDatabase.A0B();
            workDatabase.A03();
            try {
                int A002 = r34.A00();
                AnonymousClass0ZJ A003 = AnonymousClass0ZJ.A00("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY period_start_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
                A003.A6S(1, (long) A002);
                AnonymousClass0QN r1 = ((C07740a0) A0B).A01;
                r1.A02();
                Cursor A004 = AnonymousClass0LC.A00(r1, A003, false);
                int A005 = AnonymousClass0LB.A00(A004, "required_network_type");
                int A006 = AnonymousClass0LB.A00(A004, "requires_charging");
                int A007 = AnonymousClass0LB.A00(A004, "requires_device_idle");
                int A008 = AnonymousClass0LB.A00(A004, "requires_battery_not_low");
                int A009 = AnonymousClass0LB.A00(A004, "requires_storage_not_low");
                int A0010 = AnonymousClass0LB.A00(A004, "trigger_content_update_delay");
                int A0011 = AnonymousClass0LB.A00(A004, "trigger_max_content_delay");
                int A0012 = AnonymousClass0LB.A00(A004, "content_uri_triggers");
                int A0013 = AnonymousClass0LB.A00(A004, "id");
                int A0014 = AnonymousClass0LB.A00(A004, "state");
                int A0015 = AnonymousClass0LB.A00(A004, "worker_class_name");
                int A0016 = AnonymousClass0LB.A00(A004, "input_merger_class_name");
                int A0017 = AnonymousClass0LB.A00(A004, "input");
                int A0018 = AnonymousClass0LB.A00(A004, "output");
                int A0019 = AnonymousClass0LB.A00(A004, "initial_delay");
                int A0020 = AnonymousClass0LB.A00(A004, "interval_duration");
                int A0021 = AnonymousClass0LB.A00(A004, "flex_duration");
                int A0022 = AnonymousClass0LB.A00(A004, "run_attempt_count");
                int A0023 = AnonymousClass0LB.A00(A004, "backoff_policy");
                int A0024 = AnonymousClass0LB.A00(A004, "backoff_delay_duration");
                int A0025 = AnonymousClass0LB.A00(A004, "period_start_time");
                int A0026 = AnonymousClass0LB.A00(A004, "minimum_retention_duration");
                int A0027 = AnonymousClass0LB.A00(A004, "schedule_requested_at");
                int A0028 = AnonymousClass0LB.A00(A004, "run_in_foreground");
                int A0029 = AnonymousClass0LB.A00(A004, "out_of_quota_policy");
                ArrayList arrayList = new ArrayList(A004.getCount());
                while (A004.moveToNext()) {
                    String string = A004.getString(A0013);
                    String string2 = A004.getString(A0015);
                    C004101u r3 = new C004101u();
                    r3.A03 = AnonymousClass0UK.A03(A004.getInt(A005));
                    boolean z = false;
                    if (A004.getInt(A006) != 0) {
                        z = true;
                    }
                    r3.A05 = z;
                    boolean z2 = false;
                    if (A004.getInt(A007) != 0) {
                        z2 = true;
                    }
                    r3.A02(z2);
                    boolean z3 = false;
                    if (A004.getInt(A008) != 0) {
                        z3 = true;
                    }
                    r3.A04 = z3;
                    boolean z4 = false;
                    if (A004.getInt(A009) != 0) {
                        z4 = true;
                    }
                    r3.A07 = z4;
                    r3.A00 = A004.getLong(A0010);
                    r3.A01 = A004.getLong(A0011);
                    r3.A01(AnonymousClass0UK.A02(A004.getBlob(A0012)));
                    C004401z r2 = new C004401z(string, string2);
                    r2.A0D = AnonymousClass0UK.A05(A004.getInt(A0014));
                    r2.A0F = A004.getString(A0016);
                    r2.A0A = C006503b.A00(A004.getBlob(A0017));
                    r2.A0B = C006503b.A00(A004.getBlob(A0018));
                    r2.A03 = A004.getLong(A0019);
                    r2.A04 = A004.getLong(A0020);
                    r2.A02 = A004.getLong(A0021);
                    r2.A00 = A004.getInt(A0022);
                    r2.A08 = AnonymousClass0UK.A01(A004.getInt(A0023));
                    r2.A01 = A004.getLong(A0024);
                    r2.A06 = A004.getLong(A0025);
                    r2.A05 = A004.getLong(A0026);
                    r2.A07 = A004.getLong(A0027);
                    boolean z5 = false;
                    if (A004.getInt(A0028) != 0) {
                        z5 = true;
                    }
                    r2.A0H = z5;
                    r2.A0C = AnonymousClass0UK.A04(A004.getInt(A0029));
                    r2.A09 = r3;
                    arrayList.add(r2);
                }
                A004.close();
                A003.A01();
                List AAg = A0B.AAg(200);
                if (arrayList.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        A0B.AKt(((C004401z) it.next()).A0E, currentTimeMillis);
                    }
                }
                workDatabase.A05();
                workDatabase.A04();
                if (arrayList.size() > 0) {
                    C004401z[] r32 = (C004401z[]) arrayList.toArray(new C004401z[arrayList.size()]);
                    Iterator it2 = list.iterator();
                    while (it2.hasNext()) {
                        AbstractC12570i8 r12 = (AbstractC12570i8) it2.next();
                        if (r12.AIE()) {
                            r12.AbJ(r32);
                        }
                    }
                }
                if (AAg.size() > 0) {
                    C004401z[] r33 = (C004401z[]) AAg.toArray(new C004401z[AAg.size()]);
                    Iterator it3 = list.iterator();
                    while (it3.hasNext()) {
                        AbstractC12570i8 r13 = (AbstractC12570i8) it3.next();
                        if (!r13.AIE()) {
                            r13.AbJ(r33);
                        }
                    }
                }
            } catch (Throwable th) {
                workDatabase.A04();
                throw th;
            }
        }
    }
}
