package X;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiQrCodeScannedDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;

/* renamed from: X.69M  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69M implements AbstractC51302Tt {
    public final C15450nH A00;
    public final C1329668y A01;
    public final AnonymousClass6BE A02;

    public AnonymousClass69M(C15450nH r1, C1329668y r2, AnonymousClass6BE r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public void A00(Activity activity, Runnable runnable, String str, String str2, String str3) {
        C1310661b r5;
        int i;
        if (str == null || (r5 = C1310661b.A00(Uri.parse(str), str2)) == null) {
            r5 = null;
        } else {
            r5.A03 = str;
        }
        String A00 = C1329668y.A00(this.A01);
        if (r5 != null) {
            if (TextUtils.isEmpty(r5.A0C) || !r5.A0C.equals(A00)) {
                String str4 = r5.A0C;
                String str5 = r5.A06;
                String str6 = r5.A05;
                String str7 = r5.A07;
                if (C1309360o.A00(str4) && !str4.equalsIgnoreCase(A00) && ((str5 == null || str6 == null || C28421Nd.A02(str5, 0.0f).floatValue() <= C28421Nd.A02(str6, 0.0f).floatValue()) && AnonymousClass614.A03(str7))) {
                    Intent A0D = C12990iw.A0D(activity, IndiaUpiSendPaymentActivity.class);
                    C15450nH r4 = this.A00;
                    AnonymousClass614.A01(A0D, r4, r5);
                    A0D.putExtra("referral_screen", str3);
                    A0D.putExtra("extra_is_pay_money_only", !TextUtils.isEmpty(r5.A05));
                    A0D.putExtra("return-after-pay", "DEEP_LINK".equals(r5.A00));
                    A0D.putExtra("verify-vpa-in-background", true);
                    if (AnonymousClass614.A02(str3)) {
                        A0D.putExtra("extra_payment_preset_max_amount", String.valueOf(r4.A02(AbstractC15460nI.A20)));
                    }
                    A0D.addFlags(33554432);
                    activity.startActivity(A0D);
                    return;
                }
            } else {
                i = R.string.payments_deeplink_cannot_send_self;
                String string = activity.getString(i);
                this.A02.AKg(C12980iv.A0i(), null, "qr_code_scan_error", str3);
                C004802e A0S = C12980iv.A0S(activity);
                C117295Zj.A0q(A0S, runnable, 0, R.string.ok);
                A0S.A0A(string);
                A0S.A08(new DialogInterface.OnCancelListener(runnable) { // from class: X.61x
                    public final /* synthetic */ Runnable A00;

                    {
                        this.A00 = r1;
                    }

                    @Override // android.content.DialogInterface.OnCancelListener
                    public final void onCancel(DialogInterface dialogInterface) {
                        Runnable runnable2 = this.A00;
                        if (runnable2 != null) {
                            runnable2.run();
                        }
                    }
                });
                C12970iu.A1J(A0S);
            }
        }
        i = R.string.payments_deeplink_invalid_param;
        String string = activity.getString(i);
        this.A02.AKg(C12980iv.A0i(), null, "qr_code_scan_error", str3);
        C004802e A0S = C12980iv.A0S(activity);
        C117295Zj.A0q(A0S, runnable, 0, R.string.ok);
        A0S.A0A(string);
        A0S.A08(new DialogInterface.OnCancelListener(runnable) { // from class: X.61x
            public final /* synthetic */ Runnable A00;

            {
                this.A00 = r1;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                Runnable runnable2 = this.A00;
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
        });
        C12970iu.A1J(A0S);
    }

    @Override // X.AbstractC51302Tt
    public DialogFragment AG0(String str, String str2, int i) {
        String str3;
        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
        if (i == 3 || i == 9) {
            str3 = "GALLERY_QR_CODE";
        } else {
            str3 = "SCANNED_QR_CODE";
        }
        paymentBottomSheet.A01 = IndiaUpiQrCodeScannedDialogFragment.A00(str, str3, str2);
        return paymentBottomSheet;
    }

    @Override // X.AbstractC51302Tt
    public boolean AKF(String str) {
        C1310661b A00 = C1310661b.A00(Uri.parse(str), "SCANNED_QR_CODE");
        return A00 != null && !TextUtils.isEmpty(A00.A0C);
    }

    @Override // X.AbstractC51302Tt
    public void Adz(Activity activity, String str, String str2) {
        A00(activity, null, str, "SCANNED_QR_CODE", str2);
    }
}
