package X;

import android.view.View;
import androidx.appcompat.widget.ActionBarContextView;

/* renamed from: X.0YA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0YA implements AbstractC12530i4 {
    public int A00;
    public boolean A01 = false;
    public final /* synthetic */ ActionBarContextView A02;

    public AnonymousClass0YA(ActionBarContextView actionBarContextView) {
        this.A02 = actionBarContextView;
    }

    @Override // X.AbstractC12530i4
    public void AMB(View view) {
        this.A01 = true;
    }

    @Override // X.AbstractC12530i4
    public void AMC(View view) {
        if (!this.A01) {
            ActionBarContextView actionBarContextView = this.A02;
            actionBarContextView.A0C = null;
            AnonymousClass0YA.super.setVisibility(this.A00);
        }
    }

    @Override // X.AbstractC12530i4
    public void AMD(View view) {
        AnonymousClass0YA.super.setVisibility(0);
        this.A01 = false;
    }
}
