package X;

/* renamed from: X.1CZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CZ {
    public final AnonymousClass12P A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C14900mE A03;
    public final AnonymousClass18U A04;
    public final C15450nH A05;
    public final C21740xu A06;
    public final C22640zP A07;
    public final AnonymousClass01d A08;
    public final C16590pI A09;
    public final AnonymousClass018 A0A;
    public final AnonymousClass1BK A0B;
    public final AnonymousClass19M A0C;
    public final C14850m9 A0D;
    public final C244415n A0E;
    public final AnonymousClass1CH A0F;
    public final AnonymousClass1CJ A0G;
    public final AnonymousClass1AB A0H;
    public final AnonymousClass19O A0I;
    public final AbstractC14440lR A0J;
    public final AnonymousClass1CI A0K;
    public final AnonymousClass01H A0L;

    public AnonymousClass1CZ(AnonymousClass12P r2, AbstractC15710nm r3, C14330lG r4, C14900mE r5, AnonymousClass18U r6, C15450nH r7, C21740xu r8, C22640zP r9, AnonymousClass01d r10, C16590pI r11, AnonymousClass018 r12, AnonymousClass1BK r13, AnonymousClass19M r14, C14850m9 r15, C244415n r16, AnonymousClass1CH r17, AnonymousClass1CJ r18, AnonymousClass1AB r19, AnonymousClass19O r20, AbstractC14440lR r21, AnonymousClass1CI r22, AnonymousClass01H r23) {
        this.A0D = r15;
        this.A06 = r8;
        this.A03 = r5;
        this.A0J = r21;
        this.A02 = r4;
        this.A0C = r14;
        this.A05 = r7;
        this.A04 = r6;
        this.A00 = r2;
        this.A0E = r16;
        this.A08 = r10;
        this.A0A = r12;
        this.A0F = r17;
        this.A0B = r13;
        this.A0I = r20;
        this.A0K = r22;
        this.A07 = r9;
        this.A0H = r19;
        this.A0G = r18;
        this.A0L = r23;
        this.A09 = r11;
        this.A01 = r3;
    }
}
