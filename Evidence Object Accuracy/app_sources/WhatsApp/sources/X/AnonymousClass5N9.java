package X;

/* renamed from: X.5N9  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5N9 extends AnonymousClass1TL {
    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        return r3 instanceof AnonymousClass5N9;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return -1;
    }

    public String toString() {
        return "NULL";
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(AnonymousClass5ME.A01, 5, z);
    }
}
