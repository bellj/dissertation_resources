package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.0xl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21650xl {
    public static final long A04;
    public static final long A05;
    public final C14830m7 A00;
    public final C14850m9 A01;
    public final C21680xo A02;
    public final C21690xp A03;

    static {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        A05 = timeUnit.convert(15, TimeUnit.MINUTES);
        A04 = timeUnit.convert(10, TimeUnit.SECONDS);
    }

    public C21650xl(C14830m7 r1, C14850m9 r2, C21680xo r3, C21690xp r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public boolean A00() {
        return Math.abs(this.A00.A00() - this.A02.A00()) < Math.max(TimeUnit.MILLISECONDS.convert((long) this.A01.A02(200), TimeUnit.MINUTES), A05);
    }
}
