package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4BP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4BP extends Enum {
    public static final /* synthetic */ AnonymousClass4BP[] A00;
    public static final AnonymousClass4BP A01;
    public static final AnonymousClass4BP A02;
    public static final AnonymousClass4BP A03;
    public static final AnonymousClass4BP A04;
    public static final AnonymousClass4BP A05;
    public static final AnonymousClass4BP A06;
    public static final AnonymousClass4BP A07;
    public static final AnonymousClass4BP A08;
    public static final AnonymousClass4BP A09;
    public static final AnonymousClass4BP A0A;
    public static final AnonymousClass4BP A0B;
    public static final AnonymousClass4BP A0C;
    public static final AnonymousClass4BP A0D;
    public static final AnonymousClass4BP A0E;
    public static final AnonymousClass4BP A0F;
    public static final AnonymousClass4BP A0G;
    public static final AnonymousClass4BP A0H;
    public static final AnonymousClass4BP A0I;
    public static final AnonymousClass4BP A0J;
    public static final AnonymousClass4BP A0K;
    public static final AnonymousClass4BP A0L;
    public final String operatorString;

    static {
        AnonymousClass4BP r15 = new AnonymousClass4BP("GTE", ">=", 0);
        A08 = r15;
        AnonymousClass4BP r25 = new AnonymousClass4BP("LTE", "<=", 1);
        A0B = r25;
        AnonymousClass4BP r24 = new AnonymousClass4BP("EQ", "==", 2);
        A05 = r24;
        AnonymousClass4BP r23 = new AnonymousClass4BP("TSEQ", "===", 3);
        A0J = r23;
        AnonymousClass4BP r22 = new AnonymousClass4BP("NE", "!=", 4);
        A0D = r22;
        AnonymousClass4BP r21 = new AnonymousClass4BP("TSNE", "!==", 5);
        A0K = r21;
        AnonymousClass4BP r20 = new AnonymousClass4BP("LT", "<", 6);
        A0A = r20;
        AnonymousClass4BP r19 = new AnonymousClass4BP("GT", ">", 7);
        A07 = r19;
        AnonymousClass4BP r18 = new AnonymousClass4BP("REGEX", "=~", 8);
        A0G = r18;
        AnonymousClass4BP r17 = new AnonymousClass4BP("NIN", "NIN", 9);
        A0E = r17;
        AnonymousClass4BP r14 = new AnonymousClass4BP("IN", "IN", 10);
        A09 = r14;
        AnonymousClass4BP r13 = new AnonymousClass4BP("CONTAINS", "CONTAINS", 11);
        A03 = r13;
        AnonymousClass4BP r12 = new AnonymousClass4BP("ALL", "ALL", 12);
        A01 = r12;
        AnonymousClass4BP r11 = new AnonymousClass4BP("SIZE", "SIZE", 13);
        A0H = r11;
        AnonymousClass4BP r10 = new AnonymousClass4BP("EXISTS", "EXISTS", 14);
        A06 = r10;
        AnonymousClass4BP r9 = new AnonymousClass4BP("TYPE", "TYPE", 15);
        A0L = r9;
        AnonymousClass4BP r8 = new AnonymousClass4BP("MATCHES", "MATCHES", 16);
        A0C = r8;
        AnonymousClass4BP r7 = new AnonymousClass4BP("EMPTY", "EMPTY", 17);
        A04 = r7;
        AnonymousClass4BP r6 = new AnonymousClass4BP("SUBSETOF", "SUBSETOF", 18);
        A0I = r6;
        AnonymousClass4BP r5 = new AnonymousClass4BP("ANYOF", "ANYOF", 19);
        A02 = r5;
        AnonymousClass4BP r4 = new AnonymousClass4BP("NONEOF", "NONEOF", 20);
        A0F = r4;
        AnonymousClass4BP[] r3 = new AnonymousClass4BP[21];
        C72453ed.A1F(r15, r25, r24, r23, r3);
        r3[4] = r22;
        C12970iu.A1R(r21, r20, r19, r18, r3);
        C72453ed.A1G(r17, r14, r13, r12, r3);
        C72453ed.A1H(r11, r10, r9, r8, r3);
        C72453ed.A1I(r7, r6, r5, r3);
        r3[20] = r4;
        A00 = r3;
    }

    public AnonymousClass4BP(String str, String str2, int i) {
        this.operatorString = str2;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.operatorString;
    }

    public static AnonymousClass4BP valueOf(String str) {
        return (AnonymousClass4BP) Enum.valueOf(AnonymousClass4BP.class, str);
    }

    public static AnonymousClass4BP[] values() {
        return (AnonymousClass4BP[]) A00.clone();
    }
}
