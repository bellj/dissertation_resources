package X;

import java.util.ArrayList;

/* renamed from: X.0d7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09560d7 implements Runnable {
    public final /* synthetic */ AnonymousClass0Q9 A00;
    public final /* synthetic */ AbstractC06180Sm A01;

    public RunnableC09560d7(AnonymousClass0Q9 r1, AbstractC06180Sm r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AbstractC06180Sm r2 = this.A01;
        ArrayList arrayList = r2.A03;
        AnonymousClass0Q9 r1 = this.A00;
        arrayList.remove(r1);
        r2.A04.remove(r1);
    }
}
