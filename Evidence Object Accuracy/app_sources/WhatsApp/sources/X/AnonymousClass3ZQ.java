package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1201000_I1;
import java.lang.ref.WeakReference;

/* renamed from: X.3ZQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZQ implements AbstractC21730xt {
    public final C14900mE A00;
    public final C17220qS A01;
    public final WeakReference A02;
    public final boolean A03;

    public AnonymousClass3ZQ(C14900mE r2, C17220qS r3, AbstractC42921w7 r4, boolean z) {
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = C12970iu.A10(r4);
        this.A03 = z;
    }

    public void A00(C15580nU r14) {
        int i;
        String str;
        C17220qS r6 = this.A01;
        String A01 = r6.A01();
        if (this.A03) {
            i = 105;
            str = "set";
        } else {
            i = 106;
            str = "get";
        }
        AnonymousClass1V8 r4 = new AnonymousClass1V8("invite", null);
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[4];
        C12960it.A1M("id", A01, r3, 0);
        C12960it.A1M("xmlns", "w:g2", r3, 1);
        C12960it.A1M("type", str, r3, 2);
        r6.A09(this, AnonymousClass1V8.A00(r14, r4, r3, 3), A01, i, 32000);
    }

    public final void A01(String str, int i) {
        Object obj = this.A02.get();
        if (obj != null) {
            this.A00.A0H(new RunnableBRunnable0Shape1S1201000_I1(this, obj, str, i, 1));
        }
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        A01(null, 0);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        A01(null, C41151sz.A00(r3));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r5, String str) {
        AnonymousClass1V8 A0D = r5.A0D(0);
        AnonymousClass1V8.A01(A0D, "invite");
        A01(A0D.A0I("code", null), 0);
    }
}
