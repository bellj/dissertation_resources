package X;

import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1yf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44291yf implements AbstractC28771Oy {
    public final /* synthetic */ C26891Ff A00;
    public final /* synthetic */ C44271yd A01;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public C44291yf(C26891Ff r1, C44271yd r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        StringBuilder sb = new StringBuilder("BusinessActivityReportManager/download-report/on-download-canceled transferred -> ");
        sb.append(z);
        Log.i(sb.toString());
        if (!z) {
            C26891Ff r2 = this.A00;
            File A05 = r2.A01.A05();
            if (A05.exists() && !A05.delete()) {
                Log.e("BusinessActivityReportManager/reset/failed-delete-report-file");
            }
            r2.A03.A0O(2);
        }
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r4, C28781Oz r5) {
        StringBuilder sb = new StringBuilder("BusinessActivityReportManager/download-report/on-download-completed success -> ");
        boolean z = false;
        if (r4.A00 == 0) {
            z = true;
        }
        sb.append(z);
        Log.i(sb.toString());
        if (z) {
            C26891Ff r2 = this.A00;
            synchronized (r2) {
                r2.A03.A0O(4);
            }
            C44271yd r1 = this.A01;
            Log.i("BusinessActivityReportViewModel/download-report/on-success");
            BusinessActivityReportViewModel businessActivityReportViewModel = r1.A00;
            businessActivityReportViewModel.A02.A0A(Integer.valueOf(businessActivityReportViewModel.A05.A00()));
            return;
        }
        C44271yd r12 = this.A01;
        Log.i("BusinessActivityReportViewModel/download-report/on-error");
        BusinessActivityReportViewModel businessActivityReportViewModel2 = r12.A00;
        BusinessActivityReportViewModel.A00(businessActivityReportViewModel2);
        businessActivityReportViewModel2.A01.A0A(5);
        this.A00.A03.A0O(2);
    }
}
