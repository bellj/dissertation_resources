package X;

import android.content.SharedPreferences;
import org.json.JSONArray;

/* renamed from: X.19w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255619w {
    public JSONArray A00;
    public final C14820m6 A01;
    public final C16120oU A02;
    public final C255519v A03;

    public C255619w(C14820m6 r1, C16120oU r2, C255519v r3) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
    }

    public void A00() {
        C14820m6 r0 = this.A01;
        synchronized (r0.A04) {
            SharedPreferences sharedPreferences = r0.A00;
            sharedPreferences.edit().putInt("sticker_suggestion_sticker_sent_count", sharedPreferences.getInt("sticker_suggestion_sticker_sent_count", 0) + 1).apply();
        }
    }

    public void A01(int i, int i2, int i3) {
        AnonymousClass30T r2 = new AnonymousClass30T();
        r2.A00 = Integer.valueOf(i);
        r2.A01 = Long.valueOf((long) i2);
        r2.A02 = Long.valueOf((long) i3);
        this.A02.A07(r2);
    }
}
