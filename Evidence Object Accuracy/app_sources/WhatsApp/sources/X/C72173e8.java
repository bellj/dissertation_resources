package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3e8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72173e8 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass4A1 $requestType;
    public final /* synthetic */ AnonymousClass3IN this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72173e8(AnonymousClass4A1 r2, AnonymousClass3IN r3) {
        super(1);
        this.this$0 = r3;
        this.$requestType = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Object obj2;
        AbstractC16710pd r0;
        AnonymousClass28M r7 = (AnonymousClass28M) obj;
        if (r7 instanceof AnonymousClass28L) {
            AnonymousClass3IN r5 = this.this$0;
            AnonymousClass4A1 r4 = this.$requestType;
            AnonymousClass28L r72 = (AnonymousClass28L) r7;
            C16700pc.A0E(r72, 0);
            List<C44691zO> list = r72.A00.A01;
            ArrayList A0l = C12960it.A0l();
            for (C44691zO r1 : list) {
                C16700pc.A0B(r1);
                A0l.add(new AnonymousClass419(r1));
            }
            obj2 = AnonymousClass3IN.A00(r4, new C60302wc(A0l), new C60292wb(A0l));
            r0 = r5.A03;
        } else if (r7 instanceof AnonymousClass418) {
            AnonymousClass3IN r42 = this.this$0;
            AnonymousClass4A1 r3 = this.$requestType;
            AnonymousClass41C r2 = AnonymousClass41C.A00;
            obj2 = AnonymousClass3IN.A00(r3, new AnonymousClass2wY(r2), new AnonymousClass2wX(r2));
            r0 = r42.A03;
        } else if (C16700pc.A0O(r7, AnonymousClass417.A00)) {
            AnonymousClass3IN r43 = this.this$0;
            AnonymousClass4A1 r32 = this.$requestType;
            AnonymousClass41B r22 = AnonymousClass41B.A00;
            obj2 = AnonymousClass3IN.A00(r32, new AnonymousClass2wY(r22), new AnonymousClass2wX(r22));
            r0 = r43.A03;
        } else if (C16700pc.A0O(r7, AnonymousClass416.A00)) {
            AnonymousClass3IN r02 = this.this$0;
            obj2 = AnonymousClass41D.A00;
            r0 = r02.A03;
        } else if (C16700pc.A0O(r7, AnonymousClass28N.A00)) {
            AnonymousClass4A1 r23 = this.$requestType;
            AnonymousClass4A1 r12 = AnonymousClass4A1.A02;
            AnonymousClass3IN r03 = this.this$0;
            if (r23 == r12) {
                obj2 = AnonymousClass41E.A00;
                r0 = r03.A03;
            } else {
                obj2 = AnonymousClass41D.A00;
                r0 = r03.A03;
            }
        } else {
            C16700pc.A0O(r7, AnonymousClass28O.A00);
            return AnonymousClass1WZ.A00;
        }
        C12990iw.A0P(r0).A0B(obj2);
        return AnonymousClass1WZ.A00;
    }
}
