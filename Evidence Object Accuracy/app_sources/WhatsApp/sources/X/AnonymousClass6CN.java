package X;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.BrazilPaymentTypePickerFragment;
import com.whatsapp.payments.ui.ConfirmPaymentFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.PaymentRailPickerFragment;
import com.whatsapp.payments.ui.PaymentTypePickerFragment;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.util.List;

/* renamed from: X.6CN  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CN implements AnonymousClass6N0 {
    public final /* synthetic */ C14580lf A00;
    public final /* synthetic */ C30821Yy A01;
    public final /* synthetic */ AnonymousClass2S0 A02;
    public final /* synthetic */ BrazilPaymentActivity A03;
    public final /* synthetic */ ConfirmPaymentFragment A04;
    public final /* synthetic */ PaymentBottomSheet A05;

    @Override // X.AnonymousClass6N0
    public void ATZ(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    @Override // X.AnonymousClass6N0
    public void AXm(PaymentBottomSheet paymentBottomSheet) {
    }

    @Override // X.AnonymousClass6N0
    public void AXo(PaymentBottomSheet paymentBottomSheet) {
    }

    public AnonymousClass6CN(C14580lf r1, C30821Yy r2, AnonymousClass2S0 r3, BrazilPaymentActivity brazilPaymentActivity, ConfirmPaymentFragment confirmPaymentFragment, PaymentBottomSheet paymentBottomSheet) {
        this.A03 = brazilPaymentActivity;
        this.A00 = r1;
        this.A05 = paymentBottomSheet;
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = confirmPaymentFragment;
    }

    public final void A00(AnonymousClass1ZO r19, AbstractC28901Pl r20, AnonymousClass1KC r21, PaymentBottomSheet paymentBottomSheet) {
        BrazilPaymentActivity brazilPaymentActivity = this.A03;
        AnonymousClass2S0 r1 = this.A02;
        C30821Yy r6 = this.A01;
        AnonymousClass61I.A01(AnonymousClass61I.A00(((ActivityC13790kL) brazilPaymentActivity).A05, r6, r1, null, true), brazilPaymentActivity.A0K, 47, "payment_confirm_prompt", null, 1);
        if (r19 != null) {
            int A05 = r19.A05();
            if (!BrazilPaymentActivity.A03(r20, A05)) {
                ((ActivityC13830kP) brazilPaymentActivity).A05.Ab2(new Runnable(r19, this) { // from class: X.6I6
                    public final /* synthetic */ AnonymousClass1ZO A00;
                    public final /* synthetic */ AnonymousClass6CN A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        AnonymousClass6CN r0 = this.A01;
                        AnonymousClass1ZO r12 = this.A00;
                        C17070qD r02 = ((AbstractActivityC121685jC) r0.A03).A0P;
                        r02.A03();
                        r02.A09.A0J(r12);
                    }
                });
            } else if (paymentBottomSheet != null) {
                ATW(paymentBottomSheet, A05);
                return;
            } else {
                return;
            }
        }
        AnonymousClass1ZY r0 = r20.A08;
        AnonymousClass009.A05(r0);
        if (r0.A0A()) {
            ((AbstractActivityC121685jC) brazilPaymentActivity).A0P.A00().A03(null, r20);
            this.A05.A1B();
            if (Build.VERSION.SDK_INT < 23 || !brazilPaymentActivity.A0O.A07() || brazilPaymentActivity.A0O.A02() != 1) {
                String obj = r6.toString();
                String str = brazilPaymentActivity.A0o;
                PinBottomSheetDialogFragment A00 = C125035qZ.A00();
                A00.A0B = new AnonymousClass6CG(r6, r20, r21, A00, brazilPaymentActivity, str, obj);
                brazilPaymentActivity.A0N.AL9("enter_pin", brazilPaymentActivity.A00);
                brazilPaymentActivity.Adm(A00);
                return;
            }
            brazilPaymentActivity.A2t(r6, r20, r21, r6.toString(), brazilPaymentActivity.A0o);
            return;
        }
        String str2 = r20.A0A;
        brazilPaymentActivity.A2C(R.string.payment_get_verify_card_data);
        C14830m7 r8 = ((ActivityC13790kL) brazilPaymentActivity).A05;
        C14900mE r5 = ((ActivityC13810kN) brazilPaymentActivity).A05;
        C15570nT r62 = ((ActivityC13790kL) brazilPaymentActivity).A01;
        C17220qS r10 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0H;
        C18590sh r02 = brazilPaymentActivity.A0W;
        C17070qD r14 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0P;
        C18610sj r13 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0M;
        C129095xA r15 = brazilPaymentActivity.A0M;
        new C129355xa(brazilPaymentActivity, r5, r62, ((ActivityC13810kN) brazilPaymentActivity).A07, r8, brazilPaymentActivity.A05, r10, brazilPaymentActivity.A09, ((AbstractActivityC121685jC) brazilPaymentActivity).A0K, r13, r14, r15, r02, str2).A00(new AnonymousClass6AI(brazilPaymentActivity, str2));
    }

    @Override // X.AnonymousClass6N0
    public void AOW(View view, View view2, AnonymousClass1ZO r8, AbstractC28901Pl r9, PaymentBottomSheet paymentBottomSheet) {
        C14580lf r4 = this.A00;
        if (r4 != null) {
            BrazilPaymentActivity brazilPaymentActivity = this.A03;
            brazilPaymentActivity.A2C(R.string.register_wait_message);
            AnonymousClass61P r2 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0V;
            C117315Zl.A0R(r2.A00, r4, new C134376Ej(brazilPaymentActivity, new C133816Cf(r8, r9, this, paymentBottomSheet), r2));
            return;
        }
        A00(r8, r9, null, paymentBottomSheet);
    }

    @Override // X.AnonymousClass6N0
    public void ATW(PaymentBottomSheet paymentBottomSheet, int i) {
        BrazilPaymentActivity brazilPaymentActivity = this.A03;
        AnonymousClass2S0 r2 = this.A02;
        C30821Yy r1 = this.A01;
        AnonymousClass61I.A01(AnonymousClass61I.A00(((ActivityC13790kL) brazilPaymentActivity).A05, r1, r2, null, true), brazilPaymentActivity.A0K, 84, "payment_confirm_prompt", null, 1);
        brazilPaymentActivity.A02.A00(new AbstractC14590lg(this.A04, paymentBottomSheet, i) { // from class: X.6Ek
            public final /* synthetic */ int A00;
            public final /* synthetic */ ConfirmPaymentFragment A02;
            public final /* synthetic */ PaymentBottomSheet A03;

            {
                this.A02 = r2;
                this.A00 = r4;
                this.A03 = r3;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AnonymousClass6CN r5 = AnonymousClass6CN.this;
                ConfirmPaymentFragment confirmPaymentFragment = this.A02;
                int i2 = this.A00;
                PaymentBottomSheet paymentBottomSheet2 = this.A03;
                List list = (List) obj;
                AnonymousClass009.A05(list);
                PaymentMethodsListPickerFragment A00 = PaymentMethodsListPickerFragment.A00(list);
                A00.A0X(confirmPaymentFragment, 0);
                A00.A08 = new C121835k8(r5, i2);
                paymentBottomSheet2.A1L(A00);
            }
        });
    }

    @Override // X.AnonymousClass6N0
    public void ATc(PaymentBottomSheet paymentBottomSheet, int i) {
        C117315Zl.A0P(PaymentRailPickerFragment.A00(i), this.A04, paymentBottomSheet);
    }

    @Override // X.AnonymousClass6N0
    public void ATg(PaymentBottomSheet paymentBottomSheet, int i) {
        Bundle A00 = PaymentTypePickerFragment.A00(i);
        BrazilPaymentTypePickerFragment brazilPaymentTypePickerFragment = new BrazilPaymentTypePickerFragment();
        brazilPaymentTypePickerFragment.A0U(A00);
        C117315Zl.A0P(brazilPaymentTypePickerFragment, this.A04, paymentBottomSheet);
    }
}
