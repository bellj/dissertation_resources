package X;

/* renamed from: X.2DL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2DL extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;

    public AnonymousClass2DL() {
        super(3014, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamPrekeysDepletion {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceSizeBucket", obj);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj2);
        Integer num3 = this.A02;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "prekeysFetchReason", obj3);
        sb.append("}");
        return sb.toString();
    }
}
