package X;

import android.media.MediaCodec;

/* renamed from: X.3m5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C76713m5 extends AnonymousClass4CJ {
    public final C95494dp codecInfo;
    public final String diagnosticInfo;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C76713m5(X.C95494dp r4, java.lang.Throwable r5) {
        /*
            r3 = this;
            java.lang.String r0 = "Decoder failed: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r2 = 0
            if (r4 != 0) goto L_0x0020
            r0 = r2
        L_0x000a:
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r3.<init>(r0, r5)
            r3.codecInfo = r4
            int r1 = X.AnonymousClass3JZ.A01
            r0 = 21
            if (r1 < r0) goto L_0x001d
            java.lang.String r2 = A00(r5)
        L_0x001d:
            r3.diagnosticInfo = r2
            return
        L_0x0020:
            java.lang.String r0 = r4.A03
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76713m5.<init>(X.4dp, java.lang.Throwable):void");
    }

    public static String A00(Throwable th) {
        if (th instanceof MediaCodec.CodecException) {
            return ((MediaCodec.CodecException) th).getDiagnosticInfo();
        }
        return null;
    }
}
