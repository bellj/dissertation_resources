package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1YU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YU implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100474ly();
    public final int A00;
    public final UserJid A01;
    public final String A02;
    public final boolean A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1YU(int i, UserJid userJid, String str, boolean z) {
        this.A01 = userJid;
        this.A03 = z;
        this.A02 = str;
        this.A00 = i;
    }

    public /* synthetic */ AnonymousClass1YU(Parcel parcel) {
        this.A01 = (UserJid) parcel.readParcelable(UserJid.class.getClassLoader());
        this.A03 = parcel.readInt() > 0;
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A02 = readString;
        this.A00 = parcel.readInt();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1YU r5 = (AnonymousClass1YU) obj;
            if (!this.A01.equals(r5.A01) || this.A03 != r5.A03 || !TextUtils.equals(this.A02, r5.A02) || this.A00 != r5.A00) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode = (this.A01.hashCode() + 31) * 31;
        int i = 1237;
        if (this.A03) {
            i = 1231;
        }
        return ((((hashCode + i) * 31) + this.A02.hashCode()) * 31) + this.A00;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("CallLog.Key[jid=");
        sb.append(this.A01);
        sb.append("; fromMe=");
        sb.append(this.A03);
        sb.append("; callId=");
        sb.append(this.A02);
        sb.append("; transactionId=");
        sb.append(this.A00);
        sb.append("]");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A01, i);
        parcel.writeInt(this.A03 ? 1 : 0);
        parcel.writeString(this.A02);
        parcel.writeInt(this.A00);
    }
}
