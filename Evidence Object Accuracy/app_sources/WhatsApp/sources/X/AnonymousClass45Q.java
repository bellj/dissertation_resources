package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.45Q  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45Q extends AbstractC64833Hb {
    public float A00;
    public int A01 = 0;
    public final float A02;
    public final long A03;
    public final long A04;
    public final C94124bI A05;
    public final C92464Vz A06;
    public final List A07 = C12960it.A0l();
    public final float[] A08 = new float[4];

    public AnonymousClass45Q(Bitmap bitmap, Paint paint, PointF pointF, PointF pointF2, float f, float f2, int i, long j) {
        super(bitmap, paint, pointF2, f, i);
        this.A04 = j;
        C92464Vz r2 = new C92464Vz(pointF, j);
        this.A06 = r2;
        this.A05 = r2.A01.A03;
        this.A02 = f2;
        this.A03 = j;
        this.A00 = A06(i);
        A04(pointF, j);
    }

    @Override // X.AbstractC64833Hb
    public void A01(Bitmap bitmap, PointF pointF, int i) {
        super.A01(bitmap, pointF, i);
        this.A00 = A06(i);
    }

    @Override // X.AbstractC64833Hb
    public void A02(Canvas canvas) {
        A07(canvas, 0);
    }

    @Override // X.AbstractC64833Hb
    public void A03(PointF pointF, long j) {
        long max = Math.max(j, this.A03 + 1);
        super.A03.add(pointF);
        this.A07.add(Long.valueOf(max - this.A04));
        C92464Vz r6 = this.A06;
        C73023fY r9 = r6.A03;
        r9.set(pointF);
        while (((double) r6.A00) + 3.0d < ((double) max)) {
            r6.A00();
        }
        C73023fY r5 = r6.A04;
        float A01 = C72453ed.A01(r5, r9);
        float f = A01;
        C73023fY r2 = new C73023fY();
        while (f > 0.0f && A01 > 0.0f) {
            r2.set(r5);
            r6.A00();
            A01 = C72453ed.A01(r5, r2);
            f -= A01;
        }
        C92424Vv r4 = r6.A01;
        long j2 = r4.A02;
        C73023fY r1 = r4.A00;
        if (r1 != r4.A01) {
            r4.A00(r1, j2);
            r4.A01 = r4.A00;
        }
        Canvas canvas = super.A00;
        if (canvas != null) {
            A07(canvas, this.A01);
        }
    }

    @Override // X.AbstractC64833Hb
    public void A04(PointF pointF, long j) {
        if (this.A03 <= j) {
            super.A03.add(pointF);
            this.A07.add(Long.valueOf(j - this.A04));
            C92464Vz r5 = this.A06;
            r5.A03.set(pointF);
            while (((double) r5.A00) + 3.0d < ((double) j)) {
                r5.A00();
            }
            Canvas canvas = super.A00;
            if (canvas != null) {
                A07(canvas, this.A01);
            }
        }
    }

    @Override // X.AbstractC64833Hb
    public void A05(JSONObject jSONObject) {
        super.A05(jSONObject);
        JSONArray jSONArray = new JSONArray();
        for (Object obj : this.A07) {
            jSONArray.put(obj);
        }
        jSONObject.put("times", jSONArray);
    }

    public final float A06(int i) {
        float f = (super.A01 * 0.03f) - 0.125f;
        if (f < 0.125f) {
            f = 0.125f;
        } else if (f > 1.0f) {
            f = 1.0f;
        }
        return (f * this.A02) / ((float) i);
    }

    public final void A07(Canvas canvas, int i) {
        float A00;
        if (canvas != null) {
            C94124bI r5 = this.A05;
            float f = this.A00;
            r5.A03 = f;
            int floor = ((int) Math.floor((double) (r5.A02 / f))) + 1;
            while (true) {
                this.A01 = i;
                if (i < floor) {
                    float[] fArr = this.A08;
                    float f2 = ((float) i) * r5.A03;
                    if (f2 < r5.A01 || f2 > r5.A00) {
                        Map.Entry floorEntry = r5.A06.floorEntry(Float.valueOf(f2));
                        if (floorEntry == null) {
                            r5.A04 = null;
                            r5.A01 = 1.0f;
                            A00 = 0.0f;
                        } else {
                            r5.A04 = (C94264bW) floorEntry.getValue();
                            float A02 = C72453ed.A02(floorEntry.getKey());
                            r5.A01 = A02;
                            A00 = A02 + r5.A04.A00();
                        }
                        r5.A00 = A00;
                    }
                    C94264bW r7 = r5.A04;
                    float f3 = 1.0f;
                    if (r7 != null) {
                        float A002 = r7.A00();
                        if (A002 != 0.0f) {
                            f3 = (f2 - r5.A01) / A002;
                        }
                    } else {
                        r7 = r5.A05;
                    }
                    r7.A01(fArr, f3);
                    PointF pointF = new PointF(fArr[2], fArr[3]);
                    float f4 = super.A01;
                    float f5 = pointF.x;
                    float f6 = pointF.y;
                    double sqrt = Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
                    float f7 = AnonymousClass4GJ.A00;
                    float sqrt2 = (((float) Math.sqrt(sqrt)) - 0.0f) / ((0.4f * f4) - 0.0f);
                    if (sqrt2 < 0.0f) {
                        sqrt2 = 0.0f;
                    } else if (sqrt2 > 1.0f) {
                        sqrt2 = 1.0f;
                    }
                    float f8 = fArr[0];
                    float f9 = fArr[1];
                    Paint paint = super.A02;
                    paint.setStrokeWidth((float) ((int) (f4 * (1.0f - (f7 * ((sqrt2 * sqrt2) * (3.0f - (sqrt2 * 2.0f))))))));
                    canvas.drawPoint(f8, f9, paint);
                    i = this.A01 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
