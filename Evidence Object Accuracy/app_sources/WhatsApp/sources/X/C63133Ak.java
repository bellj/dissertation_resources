package X;

import android.content.ContentValues;
import android.os.Build;
import java.io.File;

/* renamed from: X.3Ak  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63133Ak {
    public static ContentValues A00(File file, String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("mime_type", str2);
        contentValues.put("_display_name", file.getName());
        if (Build.VERSION.SDK_INT >= 29) {
            contentValues.put("relative_path", C12960it.A0d("/Whatsapp/", C12960it.A0j(str)));
            return contentValues;
        }
        contentValues.put("_data", file.getPath());
        return contentValues;
    }
}
