package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3p4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78533p4 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98834jK();
    public final int A00;
    public final String A01;
    public final ArrayList A02;

    public C78533p4(String str, ArrayList arrayList, int i) {
        this.A00 = i;
        this.A01 = str;
        this.A02 = arrayList;
    }

    public C78533p4(String str, Map map) {
        ArrayList A0l;
        this.A00 = 1;
        this.A01 = str;
        if (map == null) {
            A0l = null;
        } else {
            A0l = C12960it.A0l();
            Iterator A10 = C72453ed.A10(map);
            while (A10.hasNext()) {
                String A0x = C12970iu.A0x(A10);
                A0l.add(new C78543p5((C78633pE) map.get(A0x), A0x));
            }
        }
        this.A02 = A0l;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0F(parcel, this.A02, 3, C95654e8.A0K(parcel, this.A01));
        C95654e8.A06(parcel, A00);
    }
}
