package X;

import java.lang.ref.WeakReference;

/* renamed from: X.37I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37I extends AbstractC16350or {
    public final AnonymousClass132 A00;
    public final C15580nU A01;
    public final WeakReference A02;

    public AnonymousClass37I(AbstractC13860kS r2, AnonymousClass132 r3, C15580nU r4) {
        this.A02 = C12970iu.A10(r2);
        this.A00 = r3;
        this.A01 = r4;
    }
}
