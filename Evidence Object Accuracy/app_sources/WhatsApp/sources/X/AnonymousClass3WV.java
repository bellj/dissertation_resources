package X;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import com.whatsapp.community.CommunitySubgroupsBottomSheet;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/* renamed from: X.3WV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WV implements AbstractC33091dK {
    public final Context A00;
    public final C14900mE A01;
    public final C22640zP A02;
    public final C14830m7 A03;
    public final C20040v7 A04;
    public final C21190x1 A05;
    public final C240514a A06;
    public final AnonymousClass12F A07;
    public final AbstractC14440lR A08;

    @Override // X.AbstractC33091dK
    public /* synthetic */ void A7D() {
    }

    @Override // X.AbstractC33091dK
    public /* synthetic */ void AO7(ViewHolder viewHolder, AbstractC15340mz r2) {
    }

    public AnonymousClass3WV(Context context, C14900mE r2, C22640zP r3, C14830m7 r4, C20040v7 r5, C21190x1 r6, C240514a r7, AnonymousClass12F r8, AbstractC14440lR r9) {
        this.A00 = context;
        this.A03 = r4;
        this.A01 = r2;
        this.A08 = r9;
        this.A04 = r5;
        this.A05 = r6;
        this.A07 = r8;
        this.A06 = r7;
        this.A02 = r3;
    }

    public final void A00(AbstractC14640lm r6, int i) {
        C14960mK r0 = new C14960mK();
        Context context = this.A00;
        Intent putExtra = r0.A0j(context, r6).putExtra("start_t", SystemClock.uptimeMillis());
        C35741ib.A00(putExtra, "CommunityHomeActivity:onClickConversation");
        this.A05.A00();
        context.startActivity(putExtra);
        if (r6 instanceof C15580nU) {
            this.A08.Ab2(new RunnableBRunnable0Shape1S0201000_I1(r6, this, i, 7));
        }
    }

    @Override // X.AbstractC33091dK
    public /* synthetic */ AbstractC14640lm ADL() {
        return null;
    }

    @Override // X.AbstractC33091dK
    public List AFi() {
        return Collections.emptyList();
    }

    @Override // X.AbstractC33091dK
    public /* synthetic */ Set AGa() {
        return C12970iu.A12();
    }

    @Override // X.AbstractC33091dK
    public void AO5(ViewHolder viewHolder, AbstractC14640lm r3, int i) {
        if (!(this instanceof C60392wt)) {
            A00(r3, i);
            return;
        }
        C60392wt r0 = (C60392wt) this;
        r0.A00(r3, i);
        CommunitySubgroupsBottomSheet communitySubgroupsBottomSheet = r0.A00;
        communitySubgroupsBottomSheet.A1B();
        communitySubgroupsBottomSheet.A0C().finish();
    }

    @Override // X.AbstractC33091dK
    public void AO6(View view, ViewHolder viewHolder, AbstractC14640lm r5, int i, int i2) {
        if (!(this instanceof C60392wt)) {
            A00(r5, -1);
            return;
        }
        C60392wt r1 = (C60392wt) this;
        r1.A00(r5, -1);
        CommunitySubgroupsBottomSheet communitySubgroupsBottomSheet = r1.A00;
        communitySubgroupsBottomSheet.A1B();
        communitySubgroupsBottomSheet.A0C().finish();
    }

    @Override // X.AbstractC33091dK
    public void AO8(AnonymousClass1JV r2) {
        Log.e("CommunityHomeActivity/pending group in search results");
    }

    @Override // X.AbstractC33091dK
    public void ASJ(View view, ViewHolder viewHolder, AbstractC14640lm r4, int i) {
        A00(r4, -1);
    }

    @Override // X.AbstractC33091dK
    public /* synthetic */ boolean AaG(Jid jid) {
        return false;
    }
}
