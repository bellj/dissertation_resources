package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1VP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1VP implements AnonymousClass1JI {
    public final AnonymousClass1JI A00;
    public final /* synthetic */ C237913a A01;

    public /* synthetic */ AnonymousClass1VP(C237913a r1, AnonymousClass1JI r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass1JI
    public void AOv(UserJid userJid) {
        AnonymousClass1JI r0 = this.A00;
        if (r0 != null) {
            r0.AOv(userJid);
        }
    }

    @Override // X.AnonymousClass1JI
    public void APn(UserJid userJid, int i) {
        AnonymousClass1JI r0 = this.A00;
        if (r0 != null) {
            r0.APn(userJid, i);
        }
        this.A01.A02.post(new RunnableBRunnable0Shape1S0100000_I0_1(this, 22));
    }

    @Override // X.AnonymousClass1JI
    public void AT4(UserJid userJid) {
        AnonymousClass1JI r0 = this.A00;
        if (r0 != null) {
            r0.AT4(userJid);
        }
    }

    @Override // X.AnonymousClass1JI
    public void AWV(UserJid userJid, String str, long j) {
        AnonymousClass1JI r0 = this.A00;
        if (r0 != null) {
            r0.AWV(userJid, str, j);
        }
        this.A01.A02.post(new RunnableBRunnable0Shape0S1100000_I0(2, str, this));
    }
}
