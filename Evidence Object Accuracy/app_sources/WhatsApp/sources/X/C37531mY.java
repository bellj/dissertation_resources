package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.msys.util.Provider;

/* renamed from: X.1mY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37531mY implements Provider {
    public final /* synthetic */ C22870zm A00;

    public C37531mY(C22870zm r1) {
        this.A00 = r1;
    }

    @Override // com.facebook.msys.util.Provider
    public Object get() {
        return DefaultCrypto.mCrypto;
    }
}
