package X;

import java.util.UUID;

/* renamed from: X.12f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C235812f {
    public Integer A00;
    public String A01;
    public String A02;
    public final C16120oU A03;

    public C235812f(C16120oU r2) {
        C16700pc.A0E(r2, 1);
        this.A03 = r2;
    }

    public final void A00(int i) {
        Integer num = this.A00;
        if (num != null && num.intValue() == i) {
            this.A02 = null;
            this.A00 = null;
        }
    }

    public final void A01(int i) {
        if (this.A02 == null && this.A00 == null) {
            this.A02 = UUID.randomUUID().toString();
            this.A00 = Integer.valueOf(i);
        }
    }

    public final void A02(int i) {
        C37411mL r1 = new C37411mL();
        r1.A03 = this.A02;
        r1.A02 = this.A01;
        r1.A00 = this.A00;
        r1.A01 = Integer.valueOf(i);
        this.A03.A07(r1);
    }

    public final void A03(Integer num, Long l, String str, int i) {
        C37401mK r1 = new C37401mK();
        r1.A01 = Integer.valueOf(i);
        r1.A05 = this.A02;
        r1.A04 = this.A01;
        r1.A00 = num;
        r1.A03 = str;
        r1.A02 = l;
        this.A03.A07(r1);
    }
}
