package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0TN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TN {
    public static C05850Rf A00 = C05850Rf.A00("k");

    public static List A00(C05540Py r8, AbstractC12050hI r9, AbstractC08850bx r10, float f, boolean z) {
        ArrayList arrayList = new ArrayList();
        if (r10.A05() == EnumC03770Jb.STRING) {
            AnonymousClass0R5.A00("Lottie doesn't support expressions.");
            r8.A0E.add("Lottie doesn't support expressions.");
            return arrayList;
        }
        r10.A0A();
        while (r10.A0H()) {
            if (r10.A04(A00) != 0) {
                r10.A0E();
            } else if (r10.A05() == EnumC03770Jb.BEGIN_ARRAY) {
                r10.A09();
                if (r10.A05() == EnumC03770Jb.NUMBER) {
                    arrayList.add(new AnonymousClass0U8(r9.AYt(r10, f)));
                } else {
                    while (r10.A0H()) {
                        arrayList.add(AnonymousClass0U2.A01(r8, r9, r10, f, true, z));
                    }
                }
                r10.A0B();
            } else {
                arrayList.add(new AnonymousClass0U8(r9.AYt(r10, f)));
            }
        }
        r10.A0C();
        A01(arrayList);
        return arrayList;
    }

    public static void A01(List list) {
        int i;
        Object obj;
        int size = list.size();
        int i2 = 0;
        while (true) {
            i = size - 1;
            if (i2 >= i) {
                break;
            }
            AnonymousClass0U8 r2 = (AnonymousClass0U8) list.get(i2);
            i2++;
            AnonymousClass0U8 r1 = (AnonymousClass0U8) list.get(i2);
            r2.A08 = Float.valueOf(r1.A0A);
            if (r2.A09 == null && (obj = r1.A0F) != null) {
                r2.A09 = obj;
                if (r2 instanceof AnonymousClass0HJ) {
                    ((AnonymousClass0HJ) r2).A03();
                }
            }
        }
        AnonymousClass0U8 r12 = (AnonymousClass0U8) list.get(i);
        if ((r12.A0F == null || r12.A09 == null) && list.size() > 1) {
            list.remove(r12);
        }
    }
}
