package X;

import com.whatsapp.DisplayExceptionDialogFactory$ClockWrongDialogFragment;
import com.whatsapp.DisplayExceptionDialogFactory$LoginFailedDialogFragment;
import com.whatsapp.DisplayExceptionDialogFactory$SoftwareExpiredDialogFragment;

/* renamed from: X.1x9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43511x9 {
    public static boolean A00(AbstractC13860kS r2) {
        if (r2.AJN() || !(r2 instanceof ActivityC13810kN)) {
            return false;
        }
        ((ActivityC13810kN) r2).A2M("DoNotShareCodeDialogTag");
        r2.Adm(new DisplayExceptionDialogFactory$LoginFailedDialogFragment());
        return true;
    }

    public static boolean A01(AbstractC13860kS r2, C19890uq r3, C20220vP r4) {
        if (r2.AJN() || !(r2 instanceof ActivityC13810kN)) {
            return false;
        }
        r4.A03 = true;
        r3.A0G(true);
        r2.Adm(new DisplayExceptionDialogFactory$ClockWrongDialogFragment());
        return true;
    }

    public static boolean A02(AbstractC13860kS r2, C19890uq r3, C20220vP r4) {
        if (r2.AJN() || !(r2 instanceof ActivityC13810kN)) {
            return false;
        }
        r4.A03 = true;
        r3.A0G(true);
        r2.Adm(new DisplayExceptionDialogFactory$SoftwareExpiredDialogFragment());
        return true;
    }
}
