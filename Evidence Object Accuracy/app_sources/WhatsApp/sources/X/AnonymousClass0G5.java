package X;

import android.view.ViewGroup;

/* renamed from: X.0G5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0G5 extends AnonymousClass08s {
    public final /* synthetic */ ViewGroup A00;
    public final /* synthetic */ AnonymousClass071 A01;

    public AnonymousClass0G5(ViewGroup viewGroup, AnonymousClass071 r2) {
        this.A01 = r2;
        this.A00 = viewGroup;
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXq(AnonymousClass072 r3) {
        AnonymousClass0LH.A00(this.A00, false);
        r3.A09(this);
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXr(AnonymousClass072 r3) {
        AnonymousClass0LH.A00(this.A00, false);
    }

    @Override // X.AnonymousClass08s, X.AbstractC018608t
    public void AXs(AnonymousClass072 r3) {
        AnonymousClass0LH.A00(this.A00, true);
    }
}
