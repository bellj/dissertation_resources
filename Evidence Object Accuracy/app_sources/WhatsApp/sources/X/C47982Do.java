package X;

import android.icu.text.DateTimePatternGenerator;
import android.icu.text.NumberFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.ULocale;
import android.text.TextUtils;
import com.whatsapp.jobqueue.job.HSMRehydrationUtil$SendStructUnavailableException;
import com.whatsapp.util.Log;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: X.2Do  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47982Do {
    public static Pattern A00;

    public static C43551xD A00(C17030q9 r1, String str, String str2, Locale[] localeArr) {
        C43551xD A02 = r1.A02(str, localeArr);
        if (A02 == null) {
            StringBuilder sb = new StringBuilder("HSMRehydrationUtil/requestLanguagePack/error missing hsm pack after requirements satisfied, loggableString=");
            sb.append(str2);
            Log.e(sb.toString());
            throw new HSMRehydrationUtil$SendStructUnavailableException(1002);
        } else if (A02.A02.size() != 0) {
            return A02;
        } else {
            StringBuilder sb2 = new StringBuilder("HSMRehydrationUtil/requestLanguagePack/error server had no hsm pack for namespace, loggableString=");
            sb2.append(str2);
            Log.e(sb2.toString());
            throw new HSMRehydrationUtil$SendStructUnavailableException(1002);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:275:0x04a0, code lost:
        if (r9 >= r21) goto L_0x04b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x04a4, code lost:
        if (r0[r9] != false) goto L_0x04b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x04a6, code lost:
        r2 = new java.lang.StringBuilder("HSMRehydrationUtil/validateMessageAndBuildHsmText/error not all params are  used, paramIndex=");
        r2.append(r9);
        r0 = "; ";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x04b3, code lost:
        r9 = r9 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x04b6, code lost:
        r19.appendTail(r22);
        r10 = r22.toString();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A01(android.content.Context r23, X.AnonymousClass22G r24, X.C43551xD r25, X.AnonymousClass229 r26, java.lang.String r27, boolean r28, boolean r29) {
        /*
        // Method dump skipped, instructions count: 1358
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47982Do.A01(android.content.Context, X.22G, X.1xD, X.229, java.lang.String, boolean, boolean):java.lang.String");
    }

    public static String A02(C57562nG r11, Locale locale) {
        int i;
        int i2;
        StringBuilder sb = new StringBuilder();
        sb.append(locale);
        sb.append("@calendar=persian");
        ULocale uLocale = new ULocale(sb.toString());
        Calendar instance = Calendar.getInstance(uLocale);
        instance.clear();
        StringBuilder sb2 = new StringBuilder();
        boolean z = false;
        if ((r11.A00 & 2) == 2) {
            z = true;
        }
        if (z) {
            instance.set(1, r11.A07);
            sb2.append("yyyy");
        }
        if ((r11.A00 & 4) == 4) {
            instance.set(2, r11.A06 - 1);
            sb2.append("MMMM");
        }
        boolean z2 = false;
        if ((r11.A00 & 8) == 8) {
            z2 = true;
        }
        if (z2) {
            instance.set(5, r11.A02);
            sb2.append("d");
        }
        int i3 = r11.A00;
        if ((i3 & 1) == 1) {
            sb2.append("EEEE");
        }
        if ((i3 & 16) == 16) {
            sb2.append("jjmm");
            instance.set(10, r11.A04);
            boolean z3 = false;
            if ((r11.A00 & 32) == 32) {
                z3 = true;
            }
            if (z3) {
                i2 = r11.A05;
            } else {
                i2 = 0;
            }
            instance.set(12, i2);
        }
        int i4 = r11.A00;
        if (!((i4 & 1) != 1 || (i4 & 2) == 2 || (i4 & 4) == 4 || (i4 & 8) == 8 || (i4 & 16) == 16)) {
            EnumC87314Ba A002 = EnumC87314Ba.A00(r11.A03);
            if (A002 == null) {
                A002 = EnumC87314Ba.A02;
            }
            switch (A002.ordinal()) {
                case 0:
                    instance.set(7, 2);
                    break;
                case 1:
                    i = 3;
                    instance.set(7, i);
                    break;
                case 2:
                    i = 4;
                    instance.set(7, i);
                    break;
                case 3:
                    instance.set(7, 5);
                    break;
                case 4:
                    i = 6;
                    instance.set(7, i);
                    break;
                case 5:
                    instance.set(7, 7);
                    break;
                case 6:
                    instance.set(7, 1);
                    break;
                default:
                    Log.w("HSMRehydrationUtil/validateMessageAndBuildHsmText/error localized component time had invalid day-of-week");
                    return null;
            }
        }
        String obj = sb2.toString();
        return new SimpleDateFormat(DateTimePatternGenerator.getInstance(uLocale).getBestPattern(obj).replaceAll("([^\\p{Alpha}']|('[\\p{Alpha}]+'))*G+([^\\p{Alpha}']|('[\\p{Alpha}]+'))*", ""), uLocale).format(instance.getTime());
    }

    public static String A03(Locale locale, int i) {
        return NumberFormat.getIntegerInstance(locale).format((long) i);
    }

    public static void A04(AnonymousClass229 r2, String str) {
        if ((r2.A00 & 2) != 2 || TextUtils.isEmpty(r2.A06) || (r2.A00 & 1) != 1 || TextUtils.isEmpty(r2.A09)) {
            StringBuilder sb = new StringBuilder("HSMRehydrationUtil/validateHsmMessage/error hsm missing element, loggableString=");
            sb.append(str);
            Log.e(sb.toString());
            throw new HSMRehydrationUtil$SendStructUnavailableException(null);
        }
    }
}
