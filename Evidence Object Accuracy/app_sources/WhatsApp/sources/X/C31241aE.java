package X;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;

/* renamed from: X.1aE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31241aE {
    public static byte[] A00(byte[]... bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (byte[] bArr2 : bArr) {
                byteArrayOutputStream.write(bArr2);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[][] A01(byte[] bArr, int i, int i2) {
        System.arraycopy(bArr, 0, r3[0], 0, i);
        byte[][] bArr2 = {new byte[i], new byte[i2]};
        System.arraycopy(bArr, i, bArr2[1], 0, i2);
        return bArr2;
    }

    public static byte[][] A02(byte[] bArr, int i, int i2, int i3) {
        String obj;
        if (bArr != null && i2 >= 0) {
            int i4 = i + i2;
            if (bArr.length >= i4 + i3) {
                System.arraycopy(bArr, 0, r2[0], 0, i);
                System.arraycopy(bArr, i, r2[1], 0, i2);
                byte[][] bArr2 = {new byte[i], new byte[i2], new byte[i3]};
                System.arraycopy(bArr, i4, bArr2[2], 0, i3);
                return bArr2;
            }
        }
        StringBuilder sb = new StringBuilder("Input too small: ");
        if (bArr == null) {
            obj = null;
        } else {
            int length = bArr.length;
            StringBuffer stringBuffer = new StringBuffer();
            for (int i5 = 0; i5 < length; i5++) {
                byte b = bArr[0 + i5];
                stringBuffer.append("(byte)0x");
                char[] cArr = C88694Gr.A00;
                stringBuffer.append(cArr[(b >> 4) & 15]);
                stringBuffer.append(cArr[b & 15]);
                stringBuffer.append(", ");
            }
            obj = stringBuffer.toString();
        }
        sb.append(obj);
        throw new ParseException(sb.toString(), 0);
    }
}
