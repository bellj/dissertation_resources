package X;

import android.text.TextUtils;

/* renamed from: X.2KT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2KT extends C14320lF {
    public C32361c2 A00;
    public boolean A01;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2KT(X.C18790t3 r2, X.AnonymousClass1JD r3) {
        /*
            r1 = this;
            java.lang.String r0 = r3.A02
            r1.<init>(r2, r0)
            r1.A09 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2KT.<init>(X.0t3, X.1JD):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2KT(X.C18790t3 r2, X.C32361c2 r3) {
        /*
            r1 = this;
            java.lang.String r0 = r3.A06
            r1.<init>(r2, r0)
            r1.A09 = r0
            r1.A0E(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2KT.<init>(X.0t3, X.1c2):void");
    }

    public void A0E(C32361c2 r4) {
        this.A00 = r4;
        this.A0E = r4.A08;
        this.A0B = r4.A02;
        this.A09 = r4.A06;
        this.A0H = r4.A0C;
        byte[] bArr = r4.A00;
        if (bArr == null || bArr.length <= 0) {
            String str = r4.A07;
            if (str != null) {
                this.A0F.add(str);
            }
        } else {
            this.A0H = bArr;
        }
        String str2 = r4.A03;
        if (!TextUtils.isEmpty(str2)) {
            this.A07 = new AnonymousClass3JH(-1, str2, -1);
        }
    }
}
