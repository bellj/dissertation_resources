package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: X.3fP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72933fP extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass483 A00;

    public C72933fP(AnonymousClass483 r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        this.A00.A00();
    }
}
