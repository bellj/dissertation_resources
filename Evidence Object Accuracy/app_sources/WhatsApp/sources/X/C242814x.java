package X;

import android.database.Cursor;
import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.14x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242814x {
    public final C16490p7 A00;

    public C242814x(C16490p7 r1) {
        this.A00 = r1;
    }

    public void A00(AbstractC15340mz r18) {
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT title, body, media_type, thumbnail_url, full_thumbnail, micro_thumbnail, media_url, source_type, source_id, source_url, render_larger_thumbnail, show_ad_attribution, has_icebreaker_auto_response FROM message_external_ad_content WHERE message_row_id = ?", new String[]{String.valueOf(r18.A11)});
            if (A09.moveToLast()) {
                String string = A09.getString(A09.getColumnIndexOrThrow("title"));
                String string2 = A09.getString(A09.getColumnIndexOrThrow("body"));
                int i = A09.getInt(A09.getColumnIndexOrThrow("media_type"));
                String string3 = A09.getString(A09.getColumnIndexOrThrow("thumbnail_url"));
                byte[] blob = A09.getBlob(A09.getColumnIndexOrThrow("micro_thumbnail"));
                byte[] blob2 = A09.getBlob(A09.getColumnIndexOrThrow("full_thumbnail"));
                String string4 = A09.getString(A09.getColumnIndexOrThrow("media_url"));
                String string5 = A09.getString(A09.getColumnIndexOrThrow("source_type"));
                String string6 = A09.getString(A09.getColumnIndexOrThrow("source_id"));
                String string7 = A09.getString(A09.getColumnIndexOrThrow("source_url"));
                boolean z = false;
                if (A09.getInt(A09.getColumnIndexOrThrow("render_larger_thumbnail")) != 0) {
                    z = true;
                }
                boolean z2 = false;
                if (A09.getInt(A09.getColumnIndexOrThrow("show_ad_attribution")) != 0) {
                    z2 = true;
                }
                boolean z3 = false;
                if (A09.getInt(A09.getColumnIndexOrThrow("has_icebreaker_auto_response")) != 0) {
                    z3 = true;
                }
                r18.A0N = new C32361c2(string, string2, string3, string4, string5, string6, string7, blob, blob2, i, z, z2, z3);
                r18.A0T(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            }
            A09.close();
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
