package X;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Build;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.MultiNetworkCallback;
import com.whatsapp.voipcalling.Voip;
import java.net.DatagramSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2CS */
/* loaded from: classes2.dex */
public class AnonymousClass2CS {
    public ConnectivityManager.NetworkCallback A00;
    public Network A01;
    public DatagramSocket A02;
    public boolean A03 = false;
    public final ConnectivityManager A04;
    public final ScheduledExecutorService A05;

    public AnonymousClass2CS(ConnectivityManager connectivityManager) {
        ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        this.A04 = connectivityManager;
        this.A05 = newSingleThreadScheduledExecutor;
    }

    public static /* synthetic */ void A02(AnonymousClass2CS r3) {
        AnonymousClass009.A0A("provider must not have already started", !r3.A03);
        if (r3.A03) {
            Log.e("voip/weak-wifi/startup: provider is already started");
            return;
        }
        Voip.nativeRegisterMultiNetworkCallback(new MultiNetworkCallback(r3));
        r3.A03 = true;
    }

    public static /* synthetic */ void A03(AnonymousClass2CS r2) {
        AnonymousClass009.A0A("provider must not have already shutdown", r2.A03);
        if (!r2.A03) {
            Log.e("voip/weak-wifi/shutdown: provider is already shutdown");
            return;
        }
        r2.A0A(true);
        Voip.nativeUnregisterMultiNetworkCallback();
        r2.A03 = false;
    }

    public static /* synthetic */ void A05(AnonymousClass2CS r1, boolean z) {
        if (!r1.A03) {
            Log.i("voip/weak-wifi/closeAlternativeSocket: provider is not running");
        } else {
            r1.A0A(z);
        }
    }

    public static /* synthetic */ void A06(AnonymousClass2CS r6, boolean z, boolean z2) {
        String str;
        String str2;
        String str3;
        if (!r6.A03) {
            Log.i("voip/weak-wifi/createAlternativeSocket: provider is not running");
        } else if (r6.A01 != null) {
            StringBuilder sb = new StringBuilder("voip/weak-wifi/re-use-alt-network: ");
            if (z) {
                str2 = "cellular";
            } else {
                str2 = "wifi";
            }
            sb.append(str2);
            sb.append("; test_network_cond=");
            if (z2) {
                str3 = "true";
            } else {
                str3 = "false";
            }
            sb.append(str3);
            Log.i(sb.toString());
            if (r6.A02 != null) {
                Log.i("voip/weak-wifi/create-alt-sock: previously created sock was not closed");
                Voip.notifyFailureToCreateAlternativeSocket(z2);
                return;
            }
            r6.A09(r6.A01, z2);
        } else {
            NetworkRequest.Builder builder = new NetworkRequest.Builder();
            if (z) {
                builder.addTransportType(0);
                str = "voip/weak-wifi/alt-network: cellular";
            } else {
                builder.addTransportType(1);
                str = "voip/weak-wifi/alt-network: wifi";
            }
            Log.i(str);
            builder.addCapability(12);
            r6.A00 = new C51892Zn(r6, r6.A05.schedule(new RunnableC55472iX(r6, z2), 5000, TimeUnit.MILLISECONDS), z2);
            int i = Build.VERSION.SDK_INT;
            ConnectivityManager connectivityManager = r6.A04;
            NetworkRequest build = builder.build();
            ConnectivityManager.NetworkCallback networkCallback = r6.A00;
            if (i >= 26) {
                connectivityManager.requestNetwork(build, networkCallback, 5000);
            } else {
                connectivityManager.requestNetwork(build, networkCallback);
            }
        }
    }

    public void A07() {
        ScheduledExecutorService scheduledExecutorService = this.A05;
        scheduledExecutorService.execute(new RunnableBRunnable0Shape13S0100000_I0_13(this, 35));
        scheduledExecutorService.shutdown();
    }

    public void A08() {
        this.A05.execute(new RunnableBRunnable0Shape13S0100000_I0_13(this, 34));
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003f A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A09(android.net.Network r6, boolean r7) {
        /*
            r5 = this;
            java.net.DatagramSocket r0 = r5.A02
            r4 = 0
            r3 = 1
            r1 = 0
            if (r0 != 0) goto L_0x0008
            r1 = 1
        L_0x0008:
            java.lang.String r0 = "alternative socket must not have created"
            X.AnonymousClass009.A0A(r0, r1)
            java.net.DatagramSocket r0 = r5.A02
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "voip/weak-wifi/alt-sock: socket already created"
            com.whatsapp.util.Log.i(r0)
        L_0x0017:
            com.whatsapp.voipcalling.Voip.notifyFailureToCreateAlternativeSocket(r7)
            return
        L_0x001b:
            r0 = 15
            android.net.TrafficStats.setThreadStatsTag(r0)     // Catch: SocketException -> 0x0033, IOException -> 0x002f
            r5.A01 = r6     // Catch: SocketException -> 0x0033, IOException -> 0x002f
            java.net.DatagramSocket r1 = new java.net.DatagramSocket     // Catch: SocketException -> 0x0033, IOException -> 0x002f
            r1.<init>()     // Catch: SocketException -> 0x0033, IOException -> 0x002f
            r5.A02 = r1     // Catch: SocketException -> 0x0033, IOException -> 0x002f
            android.net.Network r0 = r5.A01     // Catch: SocketException -> 0x0033, IOException -> 0x002f
            r0.bindSocket(r1)     // Catch: SocketException -> 0x0033, IOException -> 0x002f
            goto L_0x003a
        L_0x002f:
            java.lang.String r0 = "voip/weak-wifi/create-sock: io exception to bind socket to alternative network."
            goto L_0x0036
        L_0x0033:
            java.lang.String r0 = "voip/weak-wifi/create-sock: socket exception to create alternative socket."
        L_0x0036:
            com.whatsapp.util.Log.e(r0)
            goto L_0x003b
        L_0x003a:
            r4 = 1
        L_0x003b:
            java.net.DatagramSocket r2 = r5.A02
            if (r2 == 0) goto L_0x0043
            if (r4 == 0) goto L_0x0043
            r4 = 0
            goto L_0x0047
        L_0x0043:
            r5.A0A(r3)
            goto L_0x0017
        L_0x0047:
            java.lang.String r0 = "1.1.1.1"
            java.net.InetAddress r1 = r6.getByName(r0)     // Catch: UnknownHostException -> 0x0068
            r0 = 53
            r2.connect(r1, r0)     // Catch: UnknownHostException -> 0x0068
            java.net.DatagramSocket r0 = r5.A02     // Catch: UnknownHostException -> 0x0068
            java.net.InetAddress r1 = r0.getLocalAddress()     // Catch: UnknownHostException -> 0x0068
            boolean r0 = r1.isAnyLocalAddress()     // Catch: UnknownHostException -> 0x0068
            if (r0 != 0) goto L_0x0062
            java.lang.String r4 = r1.getHostAddress()     // Catch: UnknownHostException -> 0x0068
        L_0x0062:
            java.net.DatagramSocket r0 = r5.A02     // Catch: UnknownHostException -> 0x0068
            r0.disconnect()     // Catch: UnknownHostException -> 0x0068
            goto L_0x006e
        L_0x0068:
            java.lang.String r0 = "voip/weak-wifi/create-sock: unknown host exception to retrieve local ip."
            com.whatsapp.util.Log.e(r0)
        L_0x006e:
            java.net.DatagramSocket r0 = r5.A02
            android.os.ParcelFileDescriptor r0 = android.os.ParcelFileDescriptor.fromDatagramSocket(r0)
            int r3 = r0.detachFd()
            java.net.DatagramSocket r0 = r5.A02
            int r2 = r0.getLocalPort()
            java.lang.String r0 = "voip/weak-wifi/create-sock: ip="
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = "; port="
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = "; fd="
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = "; test_network_cond = "
            r1.append(r0)
            r1.append(r7)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            if (r7 == 0) goto L_0x00ae
            com.whatsapp.voipcalling.Voip.startTestNetworkConditionWithAlternativeSocket(r3, r4, r2)
            return
        L_0x00ae:
            com.whatsapp.voipcalling.Voip.switchNetworkWithAlternativeSocket(r3, r4, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2CS.A09(android.net.Network, boolean):void");
    }

    public final void A0A(boolean z) {
        if (this.A02 != null) {
            Log.i("voip/weak-wifi/close-sock");
            this.A02.close();
            this.A02 = null;
        }
        if (z) {
            ConnectivityManager.NetworkCallback networkCallback = this.A00;
            if (networkCallback != null) {
                try {
                    this.A04.unregisterNetworkCallback(networkCallback);
                    Log.i("voip/weak-wifi/unregister-alt-network-callback: succeeded.");
                } catch (IllegalArgumentException unused) {
                    Log.i("voip/weak-wifi/unregister-alt-network-callback: failed.");
                }
                this.A00 = null;
            }
            this.A01 = null;
        }
    }
}
