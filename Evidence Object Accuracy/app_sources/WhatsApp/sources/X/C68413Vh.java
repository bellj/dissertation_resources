package X;

/* renamed from: X.3Vh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68413Vh implements AbstractC115825Tb {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ C14230l4 A01;
    public final /* synthetic */ AbstractC14200l1 A02;

    public C68413Vh(C14260l7 r1, C14230l4 r2, AbstractC14200l1 r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC115825Tb
    public final void AVM(boolean z) {
        AbstractC14200l1 r3 = this.A02;
        if (r3 != null) {
            C14210l2 r2 = new C14210l2();
            r2.A04(this.A00, 0);
            r2.A04(Boolean.valueOf(z), 1);
            C14250l6.A00(this.A01, new C14220l3(r2.A00), r3);
        }
    }
}
