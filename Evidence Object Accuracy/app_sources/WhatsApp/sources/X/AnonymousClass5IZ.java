package X;

import javax.crypto.spec.IvParameterSpec;

/* renamed from: X.5IZ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IZ extends IvParameterSpec {
    public final int A00;
    public final byte[] A01 = AnonymousClass1TT.A02(null);

    public AnonymousClass5IZ(byte[] bArr, int i) {
        super(bArr);
        this.A00 = i;
    }
}
