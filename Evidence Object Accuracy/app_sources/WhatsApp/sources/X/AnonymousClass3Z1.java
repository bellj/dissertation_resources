package X;

import com.whatsapp.jid.UserJid;
import java.util.HashSet;

/* renamed from: X.3Z1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z1 implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass1BO A00;
    public final /* synthetic */ C27691It A01;

    public AnonymousClass3Z1(AnonymousClass1BO r1, C27691It r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A01.A0A(Boolean.FALSE);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        this.A01.A0A(Boolean.FALSE);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r9, String str) {
        AnonymousClass1BO r5 = this.A00;
        AnonymousClass1V8 A0E = r9.A0F("privacy").A0E("list");
        if (A0E != null) {
            HashSet A12 = C12970iu.A12();
            String A0I = A0E.A0I("dhash", null);
            for (AnonymousClass1V8 r3 : A0E.A0J("user")) {
                A12.add(r3.A0B(r5.A00, UserJid.class, "jid"));
            }
            AnonymousClass4O8 r0 = new AnonymousClass4O8(A0I, A12);
            r5.A04(r0.A00, r0.A01, false);
        }
        this.A01.A0A(Boolean.TRUE);
    }
}
