package X;

import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;
import com.whatsapp.catalogcategory.view.fragment.CatalogAllCategoryFragment;

/* renamed from: X.3di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71913di extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogAllCategoryFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71913di(CatalogAllCategoryFragment catalogAllCategoryFragment) {
        super(0);
        this.this$0 = catalogAllCategoryFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        CatalogAllCategoryFragment catalogAllCategoryFragment = this.this$0;
        AnonymousClass4JB r0 = catalogAllCategoryFragment.A01;
        if (r0 != null) {
            return new CategoryThumbnailLoader(catalogAllCategoryFragment, new C37071lG(C12990iw.A0V(r0.A00.A01.A1E)));
        }
        throw C16700pc.A06("thumbnailLoaderFactory");
    }
}
