package X;

import android.os.ConditionVariable;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.22J  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass22J {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ ConditionVariable A02;
    public final /* synthetic */ ConditionVariable A03;
    public final /* synthetic */ C22470z8 A04;

    public AnonymousClass22J(ConditionVariable conditionVariable, ConditionVariable conditionVariable2, C22470z8 r3, int i, int i2) {
        this.A04 = r3;
        this.A02 = conditionVariable;
        this.A03 = conditionVariable2;
        this.A00 = i;
        this.A01 = i2;
    }

    public void A00() {
        ConditionVariable conditionVariable = this.A03;
        if (conditionVariable != null && this.A00 == 0) {
            conditionVariable.open();
        }
        if (3 == this.A00) {
            C22470z8 r2 = this.A04;
            r2.A07.getAndIncrement();
            ReentrantLock reentrantLock = r2.A09;
            if (reentrantLock.isHeldByCurrentThread()) {
                r2.A08.signalAll();
                reentrantLock.unlock();
            }
        }
    }
}
