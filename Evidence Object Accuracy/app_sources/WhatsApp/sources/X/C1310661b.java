package X;

import android.net.Uri;
import android.text.TextUtils;
import java.util.Locale;

/* renamed from: X.61b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1310661b {
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public boolean A0D;

    public C1310661b() {
    }

    public C1310661b(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12) {
        this.A04 = A03(str);
        this.A05 = A02(str2);
        this.A0B = A03(str3);
        this.A02 = A03(str4);
        this.A0A = A03(str5);
        this.A08 = A03(str6);
        this.A01 = A03(str7);
        this.A07 = A03(str8);
        this.A06 = A02(str9);
        this.A0C = A03(str10);
        this.A09 = A03(str11);
        this.A00 = str12;
        String str13 = this.A0C;
        if (str13 != null) {
            this.A0C = str13.toLowerCase(Locale.US);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0056, code lost:
        if (r24.equals("GALLERY_QR_CODE") != false) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C1310661b A00(android.net.Uri r23, java.lang.String r24) {
        /*
        // Method dump skipped, instructions count: 301
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1310661b.A00(android.net.Uri, java.lang.String):X.61b");
    }

    public static String A01(Uri uri, String str) {
        int indexOf;
        String queryParameter = uri.getQueryParameter(str);
        return (queryParameter == null || (indexOf = queryParameter.indexOf(63)) <= 0) ? queryParameter : queryParameter.replace(queryParameter.substring(indexOf), "");
    }

    public static final String A02(String str) {
        if (TextUtils.isEmpty(str) || "null".equalsIgnoreCase(str) || "0".equals(str) || "0.0".equals(str) || "0.00".equals(str)) {
            return null;
        }
        return str.trim();
    }

    public static final String A03(String str) {
        if (TextUtils.isEmpty(str) || "null".equalsIgnoreCase(str)) {
            return null;
        }
        return str.trim();
    }

    public static final void A04(String str, String str2, StringBuffer stringBuffer) {
        if (!TextUtils.isEmpty(str2)) {
            if (stringBuffer.length() > 0) {
                stringBuffer.append("&");
            }
            stringBuffer.append(str);
            stringBuffer.append("=");
            stringBuffer.append(Uri.encode(str2, "@"));
        }
    }

    public String A05() {
        String A0d;
        StringBuffer stringBuffer = new StringBuffer();
        A04("pn", this.A04, stringBuffer);
        A04("am", this.A05, stringBuffer);
        A04("tr", this.A0B, stringBuffer);
        A04("mc", this.A02, stringBuffer);
        A04("tid", this.A0A, stringBuffer);
        A04("url", this.A08, stringBuffer);
        A04("mode", this.A01, stringBuffer);
        A04("purpose", this.A07, stringBuffer);
        A04("mam", this.A06, stringBuffer);
        A04("pa", this.A0C, stringBuffer);
        A04("sign", this.A09, stringBuffer);
        String obj = stringBuffer.toString();
        StringBuilder A0k = C12960it.A0k("upi://pay");
        if (TextUtils.isEmpty(obj)) {
            A0d = "";
        } else {
            A0d = C12960it.A0d(obj, C12960it.A0k("?"));
        }
        return C12960it.A0d(A0d, A0k);
    }
}
