package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3p9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78583p9 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99174js();
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public boolean A04;
    public boolean A05;

    public C78583p9() {
    }

    public C78583p9(float f, int i, int i2, int i3, boolean z, boolean z2) {
        this.A01 = i;
        this.A02 = i2;
        this.A03 = i3;
        this.A04 = z;
        this.A05 = z2;
        this.A00 = f;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A01);
        C95654e8.A07(parcel, 3, this.A02);
        C95654e8.A07(parcel, 4, this.A03);
        C95654e8.A09(parcel, 5, this.A04);
        C95654e8.A09(parcel, 6, this.A05);
        C95654e8.A05(parcel, this.A00, 7);
        C95654e8.A06(parcel, A00);
    }
}
