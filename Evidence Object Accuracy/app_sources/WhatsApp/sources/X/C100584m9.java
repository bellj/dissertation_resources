package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.4m9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100584m9 implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(34);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int[] A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C100584m9(Parcel parcel) {
        this.A01 = parcel.readInt();
        int readByte = parcel.readByte();
        this.A02 = readByte;
        int[] iArr = new int[readByte];
        this.A04 = iArr;
        parcel.readIntArray(iArr);
        this.A03 = parcel.readInt();
        this.A00 = parcel.readInt();
    }

    public C100584m9(int... iArr) {
        this.A01 = 0;
        int length = iArr.length;
        int[] copyOf = Arrays.copyOf(iArr, length);
        this.A04 = copyOf;
        this.A02 = length;
        this.A03 = 2;
        this.A00 = 0;
        Arrays.sort(copyOf);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C100584m9.class != obj.getClass()) {
                return false;
            }
            C100584m9 r5 = (C100584m9) obj;
            if (!(this.A01 == r5.A01 && Arrays.equals(this.A04, r5.A04) && this.A03 == r5.A03 && this.A00 == r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((((this.A01 * 31) + Arrays.hashCode(this.A04)) * 31) + this.A03) * 31) + this.A00;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A01);
        int[] iArr = this.A04;
        parcel.writeInt(iArr.length);
        parcel.writeIntArray(iArr);
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A00);
    }
}
