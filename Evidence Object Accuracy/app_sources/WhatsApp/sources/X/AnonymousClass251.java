package X;

/* renamed from: X.251  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass251 extends AnonymousClass1JQ {
    public final C462024y A00;

    public AnonymousClass251(C462024y r10) {
        super(C27791Jf.A03, null, null, "regular_low", 7, r10.A00, false);
        this.A00 = r10;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("FavoriteStickerMutation{rowId=");
        sb.append(this.A07);
        sb.append(", stickerFileHash=");
        C462024y r3 = this.A00;
        sb.append(r3.A05);
        sb.append(", isFavoriteSticker=");
        sb.append(r3.A06);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", directPath=");
        sb.append(r3.A02);
        sb.append(", mediaKey=");
        sb.append(r3.A04);
        sb.append(", lastUploadTimestamp=");
        sb.append(r3.A01);
        sb.append(", stickerHashWithoutMetadata=");
        sb.append(r3.A03);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
