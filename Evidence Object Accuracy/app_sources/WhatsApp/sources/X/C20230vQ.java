package X;

import android.content.SharedPreferences;
import android.database.Cursor;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0vQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20230vQ {
    public List A00;
    public final C14900mE A01;
    public final C21290xB A02;
    public final C15550nR A03;
    public final C15610nY A04;
    public final C21270x9 A05;
    public final AnonymousClass01d A06;
    public final C16590pI A07;
    public final C18360sK A08;
    public final C14820m6 A09;
    public final AnonymousClass018 A0A;
    public final C18750sx A0B;
    public final C21320xE A0C;
    public final C15600nX A0D;
    public final C16490p7 A0E;
    public final C21250x7 A0F;
    public final C21310xD A0G;
    public final C20710wC A0H;
    public final C15860o1 A0I;
    public final C21300xC A0J;
    public final AbstractC14440lR A0K;
    public final C21260x8 A0L;
    public final C21280xA A0M;

    public C20230vQ(C14900mE r2, C21290xB r3, C15550nR r4, C15610nY r5, C21270x9 r6, AnonymousClass01d r7, C16590pI r8, C18360sK r9, C14820m6 r10, AnonymousClass018 r11, C18750sx r12, C21320xE r13, C15600nX r14, C16490p7 r15, C21250x7 r16, C21310xD r17, C20710wC r18, C15860o1 r19, C21300xC r20, AbstractC14440lR r21, C21260x8 r22, C21280xA r23) {
        this.A07 = r8;
        this.A01 = r2;
        this.A0K = r21;
        this.A0F = r16;
        this.A0L = r22;
        this.A05 = r6;
        this.A0M = r23;
        this.A02 = r3;
        this.A03 = r4;
        this.A06 = r7;
        this.A04 = r5;
        this.A0A = r11;
        this.A0J = r20;
        this.A0H = r18;
        this.A0B = r12;
        this.A0I = r19;
        this.A0G = r17;
        this.A0E = r15;
        this.A09 = r10;
        this.A0C = r13;
        this.A08 = r9;
        this.A0D = r14;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0089, code lost:
        if (r3.A0H == false) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0092, code lost:
        if (r3.A0H != false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009d, code lost:
        if (r3.A0B() == false) goto L_0x009f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00fc, code lost:
        if (r14 != false) goto L_0x00fe;
     */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x04b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(X.C20230vQ r27, boolean r28) {
        /*
        // Method dump skipped, instructions count: 1428
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20230vQ.A00(X.0vQ, boolean):void");
    }

    public void A01() {
        A03();
        if (!this.A00.isEmpty()) {
            StringBuilder sb = new StringBuilder("missedcallnotification/clear ");
            sb.append(this.A00.size());
            Log.i(sb.toString());
            this.A09.A00.edit().remove("first_missed_call").apply();
            this.A00.clear();
            A02();
            this.A01.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(this.A0C, 41));
            this.A02.A01();
        }
    }

    public void A02() {
        StringBuilder sb = new StringBuilder("missedcallnotification/clearNotification updateHash=");
        sb.append(true);
        Log.i(sb.toString());
        this.A08.A04(4, null);
        SharedPreferences sharedPreferences = this.A09.A00;
        sharedPreferences.edit().putString("dismissed_call_notification_hash", sharedPreferences.getString("last_call_notification_hash", null)).apply();
    }

    public final synchronized void A03() {
        ArrayList arrayList;
        if (this.A00 == null) {
            long j = this.A09.A00.getLong("first_missed_call", 0);
            if (j > 0) {
                C18750sx r3 = this.A0B;
                arrayList = new ArrayList();
                ReentrantReadWriteLock reentrantReadWriteLock = r3.A0L;
                reentrantReadWriteLock.readLock().lock();
                C18740sw r12 = r3.A07;
                ArrayList arrayList2 = new ArrayList();
                C16310on A01 = r12.A03.get();
                C16330op r11 = A01.A03;
                Cursor A09 = r11.A09("SELECT _id, call_id, jid_row_id, from_me, transaction_id, timestamp, video_call, duration, call_result, bytes_transferred, call_log.group_jid_row_id, is_joinable_group_call, call_creator_device_jid_row_id, call_random_id FROM call_log WHERE call_result = 2 AND from_me = 0 AND timestamp >= ? ORDER BY _id DESC LIMIT 100", new String[]{Long.toString(j)});
                if (A09 != null) {
                    try {
                        int columnIndexOrThrow = A09.getColumnIndexOrThrow("_id");
                        if (A09.moveToLast()) {
                            do {
                                Cursor A092 = r11.A09("SELECT _id, jid_row_id, call_result FROM call_log_participant_v2 WHERE call_log_row_id = ? ORDER BY _id", new String[]{Long.toString(A09.getLong(columnIndexOrThrow))});
                                AnonymousClass1YT A012 = r12.A01(A09, A092);
                                if (A012 != null) {
                                    arrayList2.add(A012);
                                }
                                if (A092 != null) {
                                    A092.close();
                                }
                            } while (A09.moveToPrevious());
                        }
                        A09.close();
                    } catch (Throwable th) {
                        try {
                            A09.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
                A01.close();
                StringBuilder sb = new StringBuilder("CallLogStore/getMissedCalls/size:");
                sb.append(arrayList2.size());
                Log.i(sb.toString());
                arrayList.addAll(arrayList2);
                reentrantReadWriteLock.readLock().unlock();
                StringBuilder sb2 = new StringBuilder("CallsMessageStore/getMissedCalls/size:");
                sb2.append(arrayList.size());
                Log.i(sb2.toString());
                Collections.sort(arrayList, new Comparator() { // from class: X.24l
                    @Override // java.util.Comparator
                    public final int compare(Object obj, Object obj2) {
                        return (((AnonymousClass1YT) obj).A09 > ((AnonymousClass1YT) obj2).A09 ? 1 : (((AnonymousClass1YT) obj).A09 == ((AnonymousClass1YT) obj2).A09 ? 0 : -1));
                    }
                });
            } else {
                arrayList = new ArrayList();
            }
            this.A00 = arrayList;
            StringBuilder sb3 = new StringBuilder();
            sb3.append("missedcallnotification/init count:");
            sb3.append(arrayList.size());
            sb3.append(" timestamp:");
            sb3.append(j);
            Log.i(sb3.toString());
        }
    }

    public void A04(boolean z) {
        this.A0K.Ab2(new RunnableBRunnable0Shape0S0110000_I0(this, 14, z));
    }
}
