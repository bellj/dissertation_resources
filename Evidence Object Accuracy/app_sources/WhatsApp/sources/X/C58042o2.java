package X;

import android.view.animation.Animation;

/* renamed from: X.2o2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58042o2 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AnonymousClass39B A00;

    public C58042o2(AnonymousClass39B r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass39B r3 = this.A00;
        r3.removeCallbacks(r3.A0i);
        r3.A0A = false;
        boolean z = r3.A0l;
        if (!z) {
            r3.A0W.setVisibility(4);
        }
        if (r3.A0B) {
            r3.A0N.setVisibility(4);
        } else {
            r3.A0c.setVisibility(4);
        }
        if (z) {
            r3.A0F();
        }
    }
}
