package X;

import android.graphics.Bitmap;
import java.io.File;

/* renamed from: X.43y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C857343y extends AbstractC35601iM {
    public final long A00;

    @Override // X.AbstractC35611iN
    public String AES() {
        return "image/gif";
    }

    @Override // X.AbstractC35611iN
    public int getType() {
        return 2;
    }

    public C857343y(AbstractC16130oV r1, File file, long j, long j2) {
        super(r1, file, j);
        this.A00 = j2;
    }

    @Override // X.AbstractC35601iM, X.AbstractC35611iN
    public long ACb() {
        return this.A00;
    }

    @Override // X.AbstractC35611iN
    public Bitmap Aem(int i) {
        return C26521Du.A01(this.A04);
    }
}
