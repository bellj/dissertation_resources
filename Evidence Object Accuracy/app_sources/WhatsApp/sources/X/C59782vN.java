package X;

import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.whatsapp.businessdirectory.view.custom.FilterBottomSheetDialogFragment;

/* renamed from: X.2vN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59782vN extends AbstractC37191le {
    public final AppCompatCheckBox A00;
    public final FilterBottomSheetDialogFragment A01;

    public C59782vN(AppCompatCheckBox appCompatCheckBox, FilterBottomSheetDialogFragment filterBottomSheetDialogFragment) {
        super(appCompatCheckBox);
        this.A00 = appCompatCheckBox;
        this.A01 = filterBottomSheetDialogFragment;
        appCompatCheckBox.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    }
}
