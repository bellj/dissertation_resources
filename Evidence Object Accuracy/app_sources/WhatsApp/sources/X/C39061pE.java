package X;

import java.io.FileOutputStream;
import java.io.OutputStream;

/* renamed from: X.1pE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39061pE {
    public boolean A00;
    public final AnonymousClass4XO A01;
    public final /* synthetic */ C39041pA A02;

    public /* synthetic */ C39061pE(AnonymousClass4XO r1, C39041pA r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    public OutputStream A00() {
        C867048m r0;
        synchronized (this.A02) {
            AnonymousClass4XO r1 = this.A01;
            if (r1.A01 == this) {
                r0 = new C867048m(this, new FileOutputStream(r1.A01(0)));
            } else {
                throw new IllegalStateException();
            }
        }
        return r0;
    }

    public void A01() {
        boolean z = this.A00;
        C39041pA r1 = this.A02;
        if (z) {
            C39041pA.A02(this, r1, false);
            r1.A0B(this.A01.A03);
            return;
        }
        C39041pA.A02(this, r1, true);
    }
}
