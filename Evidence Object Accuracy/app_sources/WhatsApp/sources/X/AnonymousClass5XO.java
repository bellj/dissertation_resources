package X;

import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: X.5XO  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XO {
    AnonymousClass1UI AgV(AnonymousClass1UI v);

    AnonymousClass1UI AgY(AnonymousClass1UI v);

    void Agd();

    void Age();

    void Agf(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    void Agg();

    boolean Agh();

    boolean Agi(AnonymousClass5QY v);
}
