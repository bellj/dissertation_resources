package X;

import android.content.Context;
import android.view.View;
import android.widget.AbsListView;
import androidx.viewpager.widget.ViewPager;

/* renamed from: X.3S9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3S9 implements AnonymousClass070 {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass3IZ A01;
    public final /* synthetic */ AnonymousClass018 A02;
    public final /* synthetic */ C16630pM A03;

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public AnonymousClass3S9(Context context, AnonymousClass3IZ r2, AnonymousClass018 r3, C16630pM r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = context;
        this.A03 = r4;
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        AnonymousClass3IZ r3 = this.A01;
        ViewPager viewPager = r3.A0L;
        View findViewWithTag = viewPager.findViewWithTag(Integer.valueOf(r3.A00));
        if (findViewWithTag != null) {
            ((AbsListView) findViewWithTag).setOnScrollListener(null);
        }
        if (C28141Kv.A01(this.A02)) {
            r3.A00 = i;
        } else {
            int length = (r3.A0S.length - 1) - i;
            i = length;
            r3.A00 = length;
        }
        r3.A00(i);
        View findViewWithTag2 = viewPager.findViewWithTag(Integer.valueOf(r3.A00));
        if (findViewWithTag2 != null) {
            ((AbsListView) findViewWithTag2).setOnScrollListener(r3.A0J);
            findViewWithTag2.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC66433No(findViewWithTag2, this));
        }
    }
}
