package X;

import android.media.MediaPlayer;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.4iD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98144iD implements MediaPlayer.OnPreparedListener {
    public final /* synthetic */ VideoSurfaceView A00;

    public C98144iD(VideoSurfaceView videoSurfaceView) {
        this.A00 = videoSurfaceView;
    }

    @Override // android.media.MediaPlayer.OnPreparedListener
    public void onPrepared(MediaPlayer mediaPlayer) {
        VideoSurfaceView videoSurfaceView = this.A00;
        videoSurfaceView.A02 = 2;
        videoSurfaceView.A0I = true;
        videoSurfaceView.A0H = true;
        videoSurfaceView.A0G = true;
        MediaPlayer.OnPreparedListener onPreparedListener = videoSurfaceView.A0B;
        if (onPreparedListener != null) {
            onPreparedListener.onPrepared(videoSurfaceView.A0C);
        }
        videoSurfaceView.A08 = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();
        videoSurfaceView.A07 = videoHeight;
        StringBuilder A0k = C12960it.A0k("videoview/onPrepare: ");
        A0k.append(videoSurfaceView.A08);
        Log.i(C12960it.A0e("x", A0k, videoHeight));
        int i = videoSurfaceView.A03;
        if (i >= 0) {
            videoSurfaceView.seekTo(i);
        }
        if (!(videoSurfaceView.A08 == 0 || videoSurfaceView.A07 == 0)) {
            videoSurfaceView.getHolder().setFixedSize(videoSurfaceView.A08, videoSurfaceView.A07);
            if (!(videoSurfaceView.A05 == videoSurfaceView.A08 && videoSurfaceView.A04 == videoSurfaceView.A07)) {
                return;
            }
        }
        if (videoSurfaceView.A06 == 3) {
            videoSurfaceView.start();
        }
    }
}
