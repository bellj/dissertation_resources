package X;

import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.backup.google.RestoreFromBackupActivity;

/* renamed from: X.29J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29J extends ClickableSpan {
    public final /* synthetic */ RestoreFromBackupActivity A00;

    public AnonymousClass29J(RestoreFromBackupActivity restoreFromBackupActivity) {
        this.A00 = restoreFromBackupActivity;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        RestoreFromBackupActivity.A0K(this.A00);
    }
}
