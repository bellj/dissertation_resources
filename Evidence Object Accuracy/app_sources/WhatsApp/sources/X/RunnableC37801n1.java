package X;

import android.net.TrafficStats;
import android.net.Uri;
import android.os.SystemClock;
import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/* renamed from: X.1n1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class RunnableC37801n1 extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C22600zL A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;

    public /* synthetic */ RunnableC37801n1(C22600zL r1, String str, String str2, String str3, int i) {
        this.A01 = r1;
        this.A02 = str;
        this.A03 = str2;
        this.A04 = str3;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C22600zL r2 = this.A01;
        String str = this.A02;
        String str2 = this.A03;
        String str3 = this.A04;
        int i = this.A00;
        r2.A0E();
        synchronized (r2.A0I) {
            if (r2.A08() != null) {
                r2.A06(str, str2, str3, i, false).A00(new AnonymousClass221() { // from class: X.3Y3
                    /* JADX INFO: finally extract failed */
                    @Override // X.AnonymousClass221
                    public final C95514dr Aav(C28481Nj r6) {
                        AbstractC37631mk A01;
                        try {
                            C22600zL r0 = C22600zL.this;
                            String str4 = r6.A02;
                            AnonymousClass157 r3 = r0.A0E;
                            String str5 = r6.A05;
                            Log.i(C12960it.A0d(str4, C12960it.A0k("prewarmer/sendrequest/checking authority ")));
                            try {
                                try {
                                    TrafficStats.setThreadStatsTag(2);
                                    Uri.Builder builder = new Uri.Builder();
                                    builder.scheme("https").encodedAuthority(str4).appendPath("prewarm");
                                    URL url = new URL(builder.build().toString());
                                    SystemClock.elapsedRealtime();
                                    A01 = r3.A00.A01(str5, "POST", url);
                                } catch (MalformedURLException e) {
                                    Log.e("prewarmer/sendrequest/error forming URL", e);
                                }
                            } catch (IOException e2) {
                                C19940uv r1 = r3.A01;
                                if (r1.A02(e2)) {
                                    r1.A00();
                                }
                                Log.w("prewarmer/sendrequest/error opening connection", e2);
                            }
                            try {
                                SystemClock.elapsedRealtime();
                                A01.A7O();
                                A01.close();
                                TrafficStats.clearThreadStatsTag();
                                return C95514dr.A02(r6);
                            } catch (Throwable th) {
                                try {
                                    A01.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            TrafficStats.clearThreadStatsTag();
                            throw th2;
                        }
                    }
                });
            }
        }
    }
}
