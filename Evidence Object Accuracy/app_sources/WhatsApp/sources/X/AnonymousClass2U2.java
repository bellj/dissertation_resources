package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.2U2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2U2 {
    public static AnonymousClass1s9 A00(Context context, int i) {
        Class<?> loadClass;
        try {
            ClassLoader classLoader = AnonymousClass2U2.class.getClassLoader();
            if (classLoader == null || (loadClass = classLoader.loadClass("com.whatsapp.camera.litecamera.LiteCameraView")) == null) {
                return null;
            }
            return (AnonymousClass1s9) loadClass.getConstructor(Context.class, Integer.TYPE).newInstance(context, Integer.valueOf(i));
        } catch (Exception e) {
            Log.e("LiteCamera is not available", e);
            return null;
        }
    }
}
