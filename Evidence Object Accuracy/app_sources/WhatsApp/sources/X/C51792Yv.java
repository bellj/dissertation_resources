package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.gallerypicker.GalleryPickerFragment;
import com.whatsapp.util.Log;

/* renamed from: X.2Yv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51792Yv extends BroadcastReceiver {
    public final /* synthetic */ GalleryPickerFragment A00;

    public C51792Yv(GalleryPickerFragment galleryPickerFragment) {
        this.A00 = galleryPickerFragment;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str;
        GalleryPickerFragment galleryPickerFragment = this.A00;
        String action = intent.getAction();
        if (action != null) {
            switch (action.hashCode()) {
                case -1514214344:
                    if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        Log.i("gallerypicker/receivemediabroadcast/ACTION_MEDIA_MOUNTED");
                        return;
                    }
                    return;
                case -1142424621:
                    if (action.equals("android.intent.action.MEDIA_SCANNER_FINISHED")) {
                        Log.i("gallerypicker/receivemediabroadcast/ACTION_MEDIA_SCANNER_FINISHED");
                        galleryPickerFragment.A1C(false, false);
                        return;
                    }
                    return;
                case -963871873:
                    if (action.equals("android.intent.action.MEDIA_UNMOUNTED")) {
                        str = "gallerypicker/receivemediabroadcast/ACTION_MEDIA_UNMOUNTED";
                        break;
                    } else {
                        return;
                    }
                case -625887599:
                    if (action.equals("android.intent.action.MEDIA_EJECT")) {
                        str = "gallerypicker/receivemediabroadcast/ACTION_MEDIA_EJECT";
                        break;
                    } else {
                        return;
                    }
                case 1412829408:
                    if (action.equals("android.intent.action.MEDIA_SCANNER_STARTED")) {
                        Log.i("gallerypicker/receivemediabroadcast/ACTION_MEDIA_SCANNER_STARTED");
                        galleryPickerFragment.A1C(false, true);
                        return;
                    }
                    return;
                default:
                    return;
            }
            Log.i(str);
            galleryPickerFragment.A1C(true, false);
        }
    }
}
