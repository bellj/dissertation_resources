package X;

import android.view.View;
import android.view.accessibility.AccessibilityManager;
import com.whatsapp.calling.views.CallResponseLayout;

/* renamed from: X.2es  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53692es extends AnonymousClass04X {
    public int A00;
    public int A01;
    public final /* synthetic */ CallResponseLayout A02;

    public /* synthetic */ C53692es(CallResponseLayout callResponseLayout) {
        this.A02 = callResponseLayout;
    }

    @Override // X.AnonymousClass04X
    public int A03(View view, int i, int i2) {
        return view.getLeft();
    }

    @Override // X.AnonymousClass04X
    public int A04(View view, int i, int i2) {
        CallResponseLayout callResponseLayout = this.A02;
        int paddingTop = callResponseLayout.getPaddingTop();
        return Math.min(Math.max(i, paddingTop), callResponseLayout.getHeight() - view.getHeight());
    }

    @Override // X.AnonymousClass04X
    public void A05(View view, float f, float f2) {
        AbstractC115955To r0;
        CallResponseLayout callResponseLayout = this.A02;
        AccessibilityManager A0P = callResponseLayout.A03.A0P();
        if (A0P == null || !A0P.isTouchExplorationEnabled()) {
            if (this.A01 - view.getTop() > callResponseLayout.getHeight() / 3 && (r0 = callResponseLayout.A02) != null) {
                r0.ANY();
                if (!callResponseLayout.A07) {
                    return;
                }
            }
            callResponseLayout.A09.A0C(this.A00, this.A01);
            if (callResponseLayout.A06) {
                callResponseLayout.A01.startAnimation(C65293Iy.A01(callResponseLayout.A01));
                callResponseLayout.A00.setVisibility(0);
            }
            callResponseLayout.invalidate();
            return;
        }
        callResponseLayout.A02.ANY();
    }

    @Override // X.AnonymousClass04X
    public void A06(View view, int i) {
        this.A00 = view.getLeft();
        this.A01 = view.getTop();
        CallResponseLayout callResponseLayout = this.A02;
        if (callResponseLayout.A06) {
            callResponseLayout.A01.setAnimation(null);
            callResponseLayout.A00.setVisibility(8);
        }
    }

    @Override // X.AnonymousClass04X
    public void A07(View view, int i, int i2, int i3, int i4) {
        CallResponseLayout callResponseLayout = this.A02;
        if (!callResponseLayout.A06 && this.A01 - view.getTop() > callResponseLayout.A08.getScaledTouchSlop() && callResponseLayout.A00.getVisibility() == 0) {
            callResponseLayout.A00.clearAnimation();
            callResponseLayout.A00.setVisibility(4);
        }
    }

    @Override // X.AnonymousClass04X
    public boolean A08(View view, int i) {
        return C12970iu.A1Z(view, this.A02.A01);
    }
}
