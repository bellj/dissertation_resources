package X;

import android.os.Build;
import java.util.Locale;

/* renamed from: X.069  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass069 {
    public static final Locale A00 = new Locale("", "");

    public static int A00(Locale locale) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass06B.A00(locale);
        }
        if (locale == null || locale.equals(A00)) {
            return 0;
        }
        String A002 = AnonymousClass0RL.A00(locale);
        if (A002 == null) {
            byte directionality = Character.getDirectionality(locale.getDisplayName(locale).charAt(0));
            if (directionality == 1 || directionality == 2) {
                return 1;
            }
            return 0;
        } else if (A002.equalsIgnoreCase("Arab") || A002.equalsIgnoreCase("Hebr")) {
            return 1;
        } else {
            return 0;
        }
    }
}
