package X;

import java.util.Map;

/* renamed from: X.2De  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47902De implements AbstractC18940tJ {
    public final /* synthetic */ C18850tA A00;
    public final /* synthetic */ Map A01;

    public C47902De(C18850tA r1, Map map) {
        this.A00 = r1;
        this.A01 = map;
    }

    @Override // X.AbstractC18940tJ
    public void AXL() {
        this.A00.A0B.A04(this);
    }

    @Override // X.AbstractC18940tJ
    public void AXM() {
        C18850tA r2 = this.A00;
        r2.A0B.A04(this);
        Map A01 = r2.A0X.A01();
        C233411h r22 = r2.A0I;
        boolean equals = A01.equals(this.A01);
        AnonymousClass436 r1 = new AnonymousClass436();
        r1.A00 = Boolean.valueOf(equals);
        r22.A06.A07(r1);
    }
}
