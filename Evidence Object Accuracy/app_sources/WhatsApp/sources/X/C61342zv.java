package X;

import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.util.JsonReader;
import com.whatsapp.Me;
import com.whatsapp.util.Log;

/* renamed from: X.2zv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61342zv extends AbstractC18830t7 {
    public final C15570nT A00;
    public final C14820m6 A01;
    public final AnonymousClass018 A02;
    public final C20340vb A03;
    public final AbstractC17860rW A04;
    public final AbstractC14440lR A05;

    public C61342zv(C15570nT r9, C18790t3 r10, C16590pI r11, C14820m6 r12, AnonymousClass018 r13, C20340vb r14, C18810t5 r15, AbstractC17860rW r16, C18800t4 r17, AbstractC14440lR r18) {
        super(r10, r11, r15, r17, r18, 14);
        this.A05 = r18;
        this.A00 = r9;
        this.A02 = r13;
        this.A01 = r12;
        this.A03 = r14;
        this.A04 = r16;
    }

    public static C30921Zi A00(JsonReader jsonReader) {
        jsonReader.beginObject();
        long j = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            switch (nextName.hashCode()) {
                case -1724546052:
                    if (!nextName.equals("description")) {
                        break;
                    } else {
                        str4 = jsonReader.nextString();
                        break;
                    }
                case -1391167122:
                    if (!nextName.equals("mimetype")) {
                        break;
                    } else {
                        str2 = jsonReader.nextString();
                        break;
                    }
                case -1362486862:
                    if (!nextName.equals("file-size")) {
                        break;
                    } else {
                        j = jsonReader.nextLong();
                        break;
                    }
                case -1221029593:
                    if (!nextName.equals("height")) {
                        break;
                    } else {
                        i2 = jsonReader.nextInt();
                        break;
                    }
                case -718200573:
                    if (!nextName.equals("subtext-color")) {
                        break;
                    } else {
                        i5 = Color.parseColor(jsonReader.nextString());
                        break;
                    }
                case -342731470:
                    if (!nextName.equals("fullsize-url")) {
                        break;
                    } else {
                        str3 = jsonReader.nextString();
                        break;
                    }
                case 3355:
                    if (!nextName.equals("id")) {
                        break;
                    } else {
                        str = jsonReader.nextString();
                        break;
                    }
                case 113126854:
                    if (!nextName.equals("width")) {
                        break;
                    } else {
                        i = jsonReader.nextInt();
                        break;
                    }
                case 748171971:
                    if (!nextName.equals("text-color")) {
                        break;
                    } else {
                        i4 = Color.parseColor(jsonReader.nextString());
                        break;
                    }
                case 2018420361:
                    if (!nextName.equals("placeholder-color")) {
                        break;
                    } else {
                        i3 = Color.parseColor(jsonReader.nextString());
                        break;
                    }
            }
        }
        jsonReader.endObject();
        if (str != null && j != 0 && ((long) i) != 0 && ((long) i2) != 0 && str2 != null && str3 != null && i3 != 0 && i4 != 0 && i5 != 0) {
            return new C30921Zi(str, str2, str3, str4, null, i, i2, i3, i4, i5, j);
        }
        StringBuilder A0k = C12960it.A0k("PAY: PaymentBackgroundMetadataNetworkClient/parseMetadata/missing field/id=");
        A0k.append(str);
        A0k.append(", fileSize=");
        A0k.append(j);
        A0k.append(", width=");
        A0k.append(i);
        A0k.append(", height=");
        A0k.append(i2);
        A0k.append(", mimetype=");
        A0k.append(str2);
        A0k.append(", fullsizeUrl=");
        A0k.append(str3);
        A0k.append(", placeholderColor=");
        A0k.append(i3);
        A0k.append(", textColor=");
        A0k.append(i4);
        Log.e(C12960it.A0e(", subtextColor=", A0k, i5));
        return null;
    }

    public void A0A(AnonymousClass2DH r5, String str) {
        String A05;
        String A0d;
        String str2;
        String str3;
        C15570nT r0 = this.A00;
        r0.A08();
        Me me = r0.A00;
        if (me != null) {
            A05 = C22650zQ.A01(me.cc, me.number);
        } else {
            A05 = this.A02.A05();
        }
        AnonymousClass4O1 r3 = new AnonymousClass4O1(str, A05);
        if (TextUtils.isEmpty("")) {
            A0d = null;
        } else {
            StringBuilder A0k = C12960it.A0k("https://www.");
            A0k.append("");
            A0d = C12960it.A0d(".facebook.com/cdn/cacheable/whatsapp", A0k);
        }
        if (!TextUtils.isEmpty(A0d)) {
            str2 = C12960it.A0d("/payments/background", C12960it.A0j(A0d));
        } else {
            str2 = "https://static.whatsapp.net/payments/background";
        }
        Uri.Builder buildUpon = Uri.parse(str2).buildUpon();
        String str4 = r3.A01;
        if (!TextUtils.isEmpty(str4)) {
            str3 = "id";
        } else {
            str4 = r3.A00;
            str3 = "country";
        }
        buildUpon.appendQueryParameter(str3, str4);
        super.A03(r5, null, r3, buildUpon.toString());
    }
}
