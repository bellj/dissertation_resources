package X;

import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;

/* renamed from: X.3cE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71003cE implements AnonymousClass5WN {
    public final /* synthetic */ AbstractC28681Oo A00;
    public final /* synthetic */ AbstractC28681Oo A01;
    public final /* synthetic */ AnonymousClass1AK A02;
    public final /* synthetic */ Object A03;

    public C71003cE(AbstractC28681Oo r1, AbstractC28681Oo r2, AnonymousClass1AK r3, Object obj) {
        this.A02 = r3;
        this.A03 = obj;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5WN
    public void AVC(C28671On r10) {
        AnonymousClass1AK r0 = this.A02;
        Object obj = this.A03;
        AbstractC28681Oo r6 = this.A01;
        AbstractC28681Oo r7 = this.A00;
        AnonymousClass1AJ r2 = r0.A00;
        RunnableBRunnable0Shape0S0400000_I0 runnableBRunnable0Shape0S0400000_I0 = new RunnableBRunnable0Shape0S0400000_I0(r10, obj, r6, r7, 34);
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnableBRunnable0Shape0S0400000_I0.run();
        } else {
            r2.A00.post(runnableBRunnable0Shape0S0400000_I0);
        }
    }

    @Override // X.AnonymousClass5WN
    public void AVH(C91064Qh r6) {
        AnonymousClass1AJ r4 = this.A02.A00;
        RunnableBRunnable0Shape1S0200000_I0_1 runnableBRunnable0Shape1S0200000_I0_1 = new RunnableBRunnable0Shape1S0200000_I0_1(this.A03, 24, this.A00.AAN());
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnableBRunnable0Shape1S0200000_I0_1.run();
        } else {
            r4.A00.post(runnableBRunnable0Shape1S0200000_I0_1);
        }
    }
}
