package X;

import android.content.Context;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import java.util.List;

/* renamed from: X.0kx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C14160kx {
    public static volatile C14160kx A0F;
    public final Context A00;
    public final Context A01;
    public final C77403nE A02;
    public final C14170ky A03;
    public final AbstractC115095Qe A04 = C108454z5.A00;
    public final C56612lI A05;
    public final C56572lE A06;
    public final C56552lC A07;
    public final C56592lG A08;
    public final C63713Cq A09 = new C63713Cq(this);
    public final C79683r0 A0A;
    public final C56602lH A0B;
    public final C56582lF A0C;
    public final C15070mW A0D;
    public final C56562lD A0E;

    public C14160kx(AnonymousClass4MJ r12) {
        Context context = r12.A00;
        C13020j0.A02(context, "Application context can't be null");
        Context context2 = r12.A01;
        C13020j0.A01(context2);
        this.A00 = context;
        this.A01 = context2;
        C56582lF r5 = new C56582lF(this);
        r5.A0F();
        ((AbstractC15040mS) r5).A00 = true;
        this.A0C = r5;
        A01(r5);
        String str = AnonymousClass4H2.A00;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 134);
        sb.append("Google Analytics ");
        sb.append(str);
        sb.append(" is starting up. To enable debug logging on a device run:\n  adb shell setprop log.tag.GAv4 DEBUG\n  adb logcat -s GAv4");
        C15050mT.A06(r5, null, null, null, sb.toString(), 4);
        C15070mW r0 = new C15070mW(this);
        r0.A0F();
        ((AbstractC15040mS) r0).A00 = true;
        this.A0D = r0;
        C56562lD r02 = new C56562lD(this);
        r02.A0F();
        ((AbstractC15040mS) r02).A00 = true;
        this.A0E = r02;
        C56572lE r52 = new C56572lE(this);
        C56592lG r8 = new C56592lG(this);
        C56612lI r7 = new C56612lI(this);
        C56552lC r6 = new C56552lC(this);
        C79683r0 r3 = new C79683r0(this);
        C13020j0.A01(context);
        if (C14170ky.A06 == null) {
            synchronized (C14170ky.class) {
                if (C14170ky.A06 == null) {
                    C14170ky.A06 = new C14170ky(context);
                }
            }
        }
        C14170ky r1 = C14170ky.A06;
        r1.A00 = new C111965Bn(this);
        this.A03 = r1;
        C77403nE r2 = new C77403nE(this);
        r8.A0F();
        ((AbstractC15040mS) r8).A00 = true;
        this.A08 = r8;
        r7.A0F();
        ((AbstractC15040mS) r7).A00 = true;
        this.A05 = r7;
        r6.A0F();
        ((AbstractC15040mS) r6).A00 = true;
        this.A07 = r6;
        r3.A0F();
        ((AbstractC15040mS) r3).A00 = true;
        this.A0A = r3;
        C56602lH r03 = new C56602lH(this);
        r03.A0F();
        ((AbstractC15040mS) r03).A00 = true;
        this.A0B = r03;
        r52.A0F();
        ((AbstractC15040mS) r52).A00 = true;
        this.A06 = r52;
        C56562lD r13 = ((C77423nG) r2).A01.A0E;
        A01(r13);
        r13.A0G();
        r13.A0G();
        if (r13.A04) {
            r13.A0G();
            r2.A02 = r13.A05;
        }
        r13.A0G();
        r2.A01 = true;
        this.A02 = r2;
        C15030mR r32 = r52.A00;
        r32.A0G();
        C13020j0.A04("Analytics backend already started", !r32.A01);
        r32.A01 = true;
        C14170ky r22 = ((C15050mT) r32).A00.A03;
        C13020j0.A01(r22);
        r22.A03.submit(new RunnableBRunnable0Shape0S0100000_I0(r32, 6));
    }

    public static C14160kx A00(Context context) {
        C13020j0.A01(context);
        if (A0F == null) {
            synchronized (C14160kx.class) {
                if (A0F == null) {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    C14160kx r3 = new C14160kx(new AnonymousClass4MJ(context));
                    A0F = r3;
                    synchronized (C77403nE.class) {
                        List<Runnable> list = C77403nE.A04;
                        if (list != null) {
                            for (Runnable runnable : list) {
                                runnable.run();
                            }
                            C77403nE.A04 = null;
                        }
                    }
                    long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                    long longValue = ((Long) C88904Hw.A0G.A00()).longValue();
                    if (elapsedRealtime2 > longValue) {
                        C56582lF r32 = r3.A0C;
                        A01(r32);
                        r32.A07(Long.valueOf(elapsedRealtime2), Long.valueOf(longValue), "Slow initialization (ms)");
                    }
                }
            }
        }
        return A0F;
    }

    public static final void A01(AbstractC15040mS r1) {
        C13020j0.A02(r1, "Analytics service not created/initialized");
        boolean z = r1.A00;
        boolean z2 = false;
        if (z) {
            z2 = true;
        }
        C13020j0.A03("Analytics service not initialized", z2);
    }
}
