package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.chromium.net.UrlRequest;

/* renamed from: X.1Xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC30391Xf extends AbstractC15340mz {
    public boolean A00 = false;
    public boolean A01;
    public final List A02 = new ArrayList();

    public AbstractC30391Xf(AnonymousClass1IS r2, byte b, long j) {
        super(r2, b, j);
        A0Y(6);
    }

    @Override // X.AbstractC15340mz
    public void A0Y(int i) {
        super.A0Y(6);
    }

    public AnonymousClass1YT A14() {
        AnonymousClass1YT r0;
        AnonymousClass1YT r02;
        int i;
        if (!(this instanceof C30381Xe)) {
            C30401Xg r2 = (C30401Xg) this;
            synchronized (r2.A10) {
                r02 = r2.A03;
                if (r02 == null) {
                    boolean z = ((AbstractC30391Xf) r2).A01;
                    int i2 = r2.A00;
                    if (i2 <= 0) {
                        if (r2.A0z.A02) {
                            switch (r2.A01) {
                                case 1:
                                case C43951xu.A01 /* 20 */:
                                    break;
                                case 2:
                                case 22:
                                    i = 4;
                                    break;
                                case 3:
                                case 6:
                                case 7:
                                case 8:
                                case 10:
                                case 15:
                                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                case 17:
                                case 19:
                                    i = 3;
                                    break;
                                case 4:
                                case 5:
                                case 9:
                                    break;
                                case 11:
                                case 12:
                                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                case 18:
                                    i = 1;
                                    break;
                                case 21:
                                    i = 6;
                                    break;
                                default:
                                    i = 0;
                                    break;
                            }
                            r02 = AnonymousClass1YT.A01(r2, i2, i, r2.A02, z, false);
                            r2.A03 = r02;
                        }
                        i = 2;
                        r02 = AnonymousClass1YT.A01(r2, i2, i, r2.A02, z, false);
                        r2.A03 = r02;
                    }
                    i = 5;
                    r02 = AnonymousClass1YT.A01(r2, i2, i, r2.A02, z, false);
                    r2.A03 = r02;
                }
            }
            return r02;
        }
        C30381Xe r22 = (C30381Xe) this;
        synchronized (r22.A10) {
            r0 = r22.A00;
            if (r0 == null) {
                r0 = AnonymousClass1YT.A01(r22, 0, 2, 0, ((AbstractC30391Xf) r22).A01, true);
                r22.A00 = r0;
            }
        }
        return r0;
    }

    public List A15() {
        AnonymousClass1YT A14;
        List list = this.A02;
        if (list.isEmpty() && this.A00 && (A14 = A14()) != null) {
            list.add(A14);
        }
        return new ArrayList(list);
    }

    public void A16(List list) {
        List list2 = this.A02;
        list2.clear();
        list2.addAll(list);
        Collections.sort(list2, new Comparator() { // from class: X.5CR
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return ((AnonymousClass1YT) obj).A0B.A00 - ((AnonymousClass1YT) obj2).A0B.A00;
            }
        });
    }
}
