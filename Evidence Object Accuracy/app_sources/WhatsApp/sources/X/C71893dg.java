package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3dg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71893dg extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AbstractActivityC60272wQ this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71893dg(AbstractActivityC60272wQ r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        AbstractActivityC60272wQ r2 = this.this$0;
        UserJid A2e = r2.A2e();
        C48882Ih r0 = this.this$0.A00;
        if (r0 != null) {
            return AnonymousClass3RX.A00(r2, r0, A2e);
        }
        throw C16700pc.A06("cartMenuViewModelFactory");
    }
}
