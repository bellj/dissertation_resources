package X;

import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1Xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30351Xb extends AbstractC15340mz implements AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public String A00;
    public List A01;
    public List A02;
    public final C14650lo A03;
    public final C15550nR A04;
    public final C16590pI A05;
    public final AnonymousClass018 A06;

    public C30351Xb(C14650lo r2, C15550nR r3, C16590pI r4, AnonymousClass018 r5, AnonymousClass1IS r6, long j) {
        super(r6, (byte) 14, j);
        this.A05 = r4;
        this.A04 = r3;
        this.A06 = r5;
        this.A03 = r2;
        super.A02 = 1;
    }

    public C30351Xb(C14650lo r8, C15550nR r9, C16590pI r10, AnonymousClass018 r11, AnonymousClass1IS r12, C30351Xb r13, long j, boolean z) {
        super(r13, r12, j, z);
        this.A05 = r10;
        this.A04 = r9;
        this.A06 = r11;
        this.A03 = r8;
        super.A02 = 1;
        this.A01 = r13.A01;
        this.A00 = r13.A00;
    }

    public List A14() {
        List list = this.A01;
        if (list != null) {
            return list;
        }
        List A00 = AnonymousClass2GM.A00(A13());
        this.A01 = A00;
        return A00;
    }

    public void A15(List list) {
        this.A01 = new ArrayList(list);
        this.A02 = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            new ObjectOutputStream(byteArrayOutputStream).writeObject(this.A01);
            A0w(byteArrayOutputStream.toByteArray());
        } catch (IOException unused) {
            throw new AssertionError("ObjectOutputStream backed by ByteArrayOutputStream should not throw IOException");
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r14) {
        AnonymousClass1G3 r3 = r14.A03;
        C57302mo r0 = ((C27081Fy) r3.A00).A09;
        if (r0 == null) {
            r0 = C57302mo.A04;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        if (!TextUtils.isEmpty(this.A00)) {
            String str = this.A00;
            A0T.A03();
            C57302mo r1 = (C57302mo) A0T.A00;
            r1.A00 |= 1;
            r1.A03 = str;
        }
        List A14 = A14();
        int i = 0;
        while (i < A14.size() && i < 257) {
            String str2 = (String) A14.get(i);
            AnonymousClass1G4 A0T2 = C57292mn.A04.A0T();
            String A00 = C30721Yo.A00(this.A03, this.A04, this.A05, this.A06, str2);
            if (A00 != null) {
                A0T2.A03();
                C57292mn r12 = (C57292mn) A0T2.A00;
                r12.A00 |= 1;
                r12.A02 = A00;
            }
            A0T2.A03();
            C57292mn r13 = (C57292mn) A0T2.A00;
            r13.A00 |= 2;
            r13.A03 = str2;
            AbstractC27091Fz A02 = A0T2.A02();
            A0T.A03();
            C57302mo r4 = (C57302mo) A0T.A00;
            AnonymousClass1K6 r15 = r4.A01;
            if (!((AnonymousClass1K7) r15).A00) {
                r15 = AbstractC27091Fz.A0G(r15);
                r4.A01 = r15;
            }
            r15.add(A02);
            i++;
        }
        AnonymousClass1PG r9 = r14.A04;
        byte[] bArr = r14.A09;
        if (C32411c7.A0U(r9, this, bArr)) {
            C43261wh A0P = C32411c7.A0P(r14.A00, r14.A02, r9, this, bArr, r14.A06);
            A0T.A03();
            C57302mo r16 = (C57302mo) A0T.A00;
            r16.A02 = A0P;
            r16.A00 |= 2;
        }
        r3.A03();
        C27081Fy r17 = (C27081Fy) r3.A00;
        r17.A09 = (C57302mo) A0T.A02();
        r17.A00 |= 4096;
    }

    @Override // X.AbstractC16410oy
    public /* bridge */ /* synthetic */ AbstractC15340mz A7L(AnonymousClass1IS r11, long j) {
        C16590pI r3 = this.A05;
        return new C30351Xb(this.A03, this.A04, r3, this.A06, r11, this, j, false);
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r11) {
        C16590pI r3 = this.A05;
        return new C30351Xb(this.A03, this.A04, r3, this.A06, r11, this, this.A0I, true);
    }
}
