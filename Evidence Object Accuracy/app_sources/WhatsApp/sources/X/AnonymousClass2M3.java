package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2M3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2M3 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2M3 A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public String A01 = "";
    public String A02 = "";

    static {
        AnonymousClass2M3 r0 = new AnonymousClass2M3();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        switch (r8.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass2M3 r10 = (AnonymousClass2M3) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A01;
                int i2 = r10.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r9.Afy(str, r10.A01, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A02;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A02 = r9.Afy(str2, r10.A02, z3, z4);
                if (r9 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A032 = r92.A03();
                            if (A032 == 0) {
                                break;
                            } else if (A032 == 10) {
                                String A0A = r92.A0A();
                                this.A00 = 1 | this.A00;
                                this.A01 = A0A;
                            } else if (A032 == 18) {
                                String A0A2 = r92.A0A();
                                this.A00 |= 2;
                                this.A02 = A0A2;
                            } else if (!A0a(r92, A032)) {
                                break;
                            }
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2M3();
            case 5:
                return new C81663uN();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (AnonymousClass2M3.class) {
                        if (A04 == null) {
                            A04 = new AnonymousClass255(A03);
                        }
                    }
                }
                return A04;
            default:
                throw new UnsupportedOperationException();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
