package X;

import android.net.Uri;
import java.util.Map;

/* renamed from: X.2BW  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass2BW extends AnonymousClass2BY {
    void A5p(AnonymousClass5QP v);

    Map AGF();

    Uri AHS();

    long AYZ(AnonymousClass3H3 v);

    void close();
}
