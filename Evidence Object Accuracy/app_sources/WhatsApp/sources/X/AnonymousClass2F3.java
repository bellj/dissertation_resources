package X;

import com.whatsapp.util.Log;

/* renamed from: X.2F3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2F3 implements AnonymousClass2F5 {
    public volatile int A00 = 0;
    public final /* synthetic */ C15730no A01;

    @Override // X.AnonymousClass2F5
    public void ANg() {
    }

    @Override // X.AnonymousClass2F5
    public void ANh() {
    }

    @Override // X.AnonymousClass2F5
    public void AOL() {
    }

    @Override // X.AnonymousClass2F5
    public void APl(int i) {
    }

    @Override // X.AnonymousClass2F5
    public void AQ5() {
    }

    public /* synthetic */ AnonymousClass2F3(C15730no r2) {
        this.A01 = r2;
    }

    @Override // X.AnonymousClass2F5
    public void AUL(int i) {
        StringBuilder sb = new StringBuilder("ExportFlowManager/onProgress; progress=");
        sb.append(i);
        Log.i(sb.toString());
        this.A00 = i;
    }
}
