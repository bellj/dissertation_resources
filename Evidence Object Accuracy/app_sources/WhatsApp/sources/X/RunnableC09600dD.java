package X;

/* renamed from: X.0dD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09600dD implements Runnable {
    public final /* synthetic */ ExecutorC10600et A00;
    public final /* synthetic */ Runnable A01;

    public RunnableC09600dD(ExecutorC10600et r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.A01.run();
        } finally {
            this.A00.A00();
        }
    }
}
