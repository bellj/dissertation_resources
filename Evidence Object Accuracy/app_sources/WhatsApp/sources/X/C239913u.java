package X;

import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.13u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239913u {
    public final C14900mE A00;
    public final C14850m9 A01;
    public final C26701Em A02;
    public final C26711En A03;
    public final AnonymousClass1EK A04;
    public final AnonymousClass160 A05;
    public final AbstractC14440lR A06;

    public C239913u(C14900mE r1, C14850m9 r2, C26701Em r3, C26711En r4, AnonymousClass1EK r5, AnonymousClass160 r6, AbstractC14440lR r7) {
        this.A01 = r2;
        this.A00 = r1;
        this.A06 = r7;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A04 = r5;
    }

    public void A00(AbstractC15340mz r10) {
        C38101nW A14;
        C38101nW A142;
        C38111nX r1;
        if (r10 != null) {
            if (AnonymousClass01I.A01()) {
                Log.w("message is lazy loaded on ui thread", new Throwable());
            }
            if (r10 instanceof C30421Xi) {
                C26711En r12 = this.A03;
                C30421Xi r6 = (C30421Xi) r10;
                if (r6.A00 == null && r6.A12(32768)) {
                    C240914e r0 = r12.A00;
                    long j = r6.A11;
                    C16310on A01 = r0.A00.get();
                    try {
                        Cursor A09 = A01.A03.A09("SELECT message_row_id, waveform FROM audio_data WHERE message_row_id = ?", new String[]{Long.toString(j)});
                        if (A09.moveToLast()) {
                            r1 = new C38111nX(A09.getBlob(A09.getColumnIndexOrThrow("waveform")));
                            A09.close();
                            A01.close();
                        } else {
                            A09.close();
                            A01.close();
                            r1 = null;
                        }
                        r6.A00 = r1;
                        if (r1 == null) {
                            r6.A0U(32768);
                        } else {
                            r6.A0T(32768);
                        }
                    } catch (Throwable th) {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            }
            this.A05.A00(r10);
            AnonymousClass1EK r2 = this.A04;
            synchronized (r2) {
                if ((r10 instanceof AbstractC16130oV) && (A142 = ((AbstractC16130oV) r10).A14()) != null) {
                    r2.A00(A142);
                    A142.A00 = true;
                }
                AbstractC15340mz A0E = r10.A0E();
                if ((A0E instanceof AbstractC16130oV) && (A14 = ((AbstractC16130oV) A0E).A14()) != null) {
                    A14.A00 = true;
                }
            }
            for (Number number : C21400xM.A0M) {
                this.A02.A01(r10, number.byteValue());
            }
            if (!C30051Vw.A17(r10)) {
                throw new IllegalStateException("ensureCompletelyLoaded failed");
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r1.A12(32768) != false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AbstractC15340mz r4, java.lang.Runnable r5) {
        /*
            r3 = this;
            boolean r0 = r4 instanceof X.C30421Xi
            if (r0 == 0) goto L_0x0021
            r1 = r4
            X.1Xi r1 = (X.C30421Xi) r1
            X.1nX r0 = r1.A00
            if (r0 != 0) goto L_0x0021
            r0 = 32768(0x8000, float:4.5918E-41)
            boolean r0 = r1.A12(r0)
            if (r0 == 0) goto L_0x0021
        L_0x0014:
            X.0lR r2 = r3.A06
            r1 = 34
            com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1 r0 = new com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1
            r0.<init>(r3, r4, r5, r1)
            r2.Ab2(r0)
            return
        L_0x0021:
            X.160 r0 = r3.A05
            boolean r0 = r0.A03(r4)
            if (r0 != 0) goto L_0x0014
            X.1EK r0 = r3.A04
            boolean r0 = r0.A01(r4)
            if (r0 != 0) goto L_0x0014
            if (r4 == 0) goto L_0x0050
            java.util.List r0 = X.C21400xM.A0M
            java.util.Iterator r1 = r0.iterator()
        L_0x0039:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0050
            java.lang.Object r0 = r1.next()
            java.lang.Number r0 = (java.lang.Number) r0
            byte r0 = r0.byteValue()
            boolean r0 = X.C26701Em.A00(r4, r0)
            if (r0 == 0) goto L_0x0039
            goto L_0x0014
        L_0x0050:
            X.0mE r0 = r3.A00
            r0.A0I(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C239913u.A01(X.0mz, java.lang.Runnable):void");
    }
}
