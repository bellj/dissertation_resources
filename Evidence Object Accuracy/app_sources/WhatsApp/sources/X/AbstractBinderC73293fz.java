package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.3fz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73293fz extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public AbstractBinderC73293fz(String str) {
        attachInterface(this, str);
    }

    public boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (this instanceof BinderC79893rN) {
            BinderC79893rN r2 = (BinderC79893rN) this;
            if (i != 1) {
                return false;
            }
            r2.A00.ASM((LatLng) C12970iu.A0F(parcel, LatLng.CREATOR));
        } else if (this instanceof BinderC79883rM) {
            BinderC79883rM r1 = (BinderC79883rM) this;
            if (i != 1) {
                return false;
            }
            r1.A00.ANc();
        } else if (this instanceof BinderC79873rL) {
            BinderC79873rL r22 = (BinderC79873rL) this;
            if (i != 1) {
                return false;
            }
            r22.A00.ANd(parcel.readInt());
        } else if (!(this instanceof BinderC79863rK)) {
            BinderC79853rJ r12 = (BinderC79853rJ) this;
            if (i == 1) {
                r12.A00.AQZ();
            } else if (i != 2) {
                return false;
            } else {
                r12.A00.ANe();
            }
        } else {
            BinderC79863rK r13 = (BinderC79863rK) this;
            if (i != 1) {
                return false;
            }
            r13.A00.ASN();
        }
        parcel2.writeNoException();
        return true;
    }

    @Override // android.os.Binder
    public final boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        return A00(i, parcel, parcel2, i2);
    }
}
