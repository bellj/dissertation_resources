package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.5Zz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class HandlerC117455Zz extends Handler {
    public final C119755f3 A00;
    public final C1308460e A01;
    public final C1329668y A02;
    public final C18590sh A03;
    public final String A04;
    public final /* synthetic */ AnonymousClass60I A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HandlerC117455Zz(Looper looper, C119755f3 r2, C1308460e r3, C1329668y r4, AnonymousClass60I r5, C18590sh r6, String str) {
        super(looper);
        this.A05 = r5;
        this.A03 = r6;
        this.A01 = r3;
        this.A02 = r4;
        this.A04 = str;
        this.A00 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        if (r1 != null) goto L_0x005e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
    @Override // android.os.Handler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r23) {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerC117455Zz.handleMessage(android.os.Message):void");
    }
}
