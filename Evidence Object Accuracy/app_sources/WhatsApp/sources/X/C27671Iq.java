package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

/* renamed from: X.1Iq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27671Iq extends AbstractC15340mz implements AbstractC16400ox, AbstractC27681Ir, AbstractC16420oz {
    public int A00;
    public int A01;
    public String A02;
    public List A03;
    public final List A04 = new ArrayList();

    public C27671Iq(C14850m9 r11, C57522nC r12, AnonymousClass1IS r13, long j, boolean z) {
        super(r13, (byte) 66, j);
        String str;
        if ((r12.A00 & 2) == 2) {
            String str2 = r12.A05;
            if (!z) {
                int min = Math.min(r11.A02(1406), 500);
                if (TextUtils.isEmpty(str2) || str2.length() > min) {
                    throw new C43271wi("poll_creation_invalid_name", 34);
                }
                this.A02 = str2;
                if (r12.A03.size() > 0) {
                    AnonymousClass1K6<C57042mN> r3 = r12.A03;
                    int min2 = Math.min(r11.A02(1408), 12);
                    int min3 = Math.min(r11.A02(1407), 255);
                    if (r3 == null || r3.size() < 2) {
                        throw new C43271wi("poll_creation_invalid_options_count", 34);
                    }
                    LinkedHashSet linkedHashSet = new LinkedHashSet();
                    for (C57042mN r32 : r3) {
                        if ((r32.A00 & 1) == 1) {
                            str = r32.A01;
                        } else {
                            str = null;
                        }
                        if (TextUtils.isEmpty(str) || str.length() > min3) {
                            throw new C43271wi("poll_creation_invalid_option", 34);
                        }
                        C27701Iu r1 = new C27701Iu(str);
                        if (!linkedHashSet.contains(r1)) {
                            linkedHashSet.add(r1);
                        }
                    }
                    if (linkedHashSet.size() < 2 || linkedHashSet.size() > min2) {
                        throw new C43271wi("poll_creation_invalid_options_count", 34);
                    }
                    List list = this.A04;
                    list.clear();
                    list.addAll(linkedHashSet);
                    if ((r12.A00 & 4) == 4) {
                        int i = r12.A01;
                        if (i < 0 || i > this.A04.size()) {
                            throw new C43271wi("poll_creation_invalid_selectable_options_count", 34);
                        }
                        this.A01 = i;
                        return;
                    }
                    throw new C43271wi("poll_creation_missing_selectable_options_count", 34);
                }
                throw new C43271wi("poll_creation_missing_options", 34);
            }
            this.A02 = str2;
            return;
        }
        throw new C43271wi("poll_creation_missing_name", 34);
    }

    public C27671Iq(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 66, j);
    }

    public C27671Iq(AnonymousClass1IS r8, C27671Iq r9, long j) {
        super(r9, r8, j, true);
        this.A02 = r9.A02;
    }

    public void A14(List list) {
        for (C27701Iu r6 : this.A04) {
            int i = 0;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                if (((C27711Iw) ((AnonymousClass1Iv) it.next())).A05.contains(Long.valueOf(r6.A01))) {
                    i++;
                }
            }
            r6.A00 = i;
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r13) {
        AnonymousClass1G3 r4 = r13.A03;
        C57522nC r0 = ((C27081Fy) r4.A00).A0T;
        if (r0 == null) {
            r0 = C57522nC.A06;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        if (!TextUtils.isEmpty(this.A02)) {
            String str = this.A02;
            A0T.A03();
            C57522nC r1 = (C57522nC) A0T.A00;
            r1.A00 |= 2;
            r1.A05 = str;
        }
        AnonymousClass1PG r8 = r13.A04;
        byte[] bArr = r13.A09;
        if (C32411c7.A0U(r8, this, bArr)) {
            C43261wh A0P = C32411c7.A0P(r13.A00, r13.A02, r8, this, bArr, r13.A06);
            A0T.A03();
            C57522nC r12 = (C57522nC) A0T.A00;
            r12.A04 = A0P;
            r12.A00 |= 8;
        }
        int i = this.A01;
        A0T.A03();
        C57522nC r14 = (C57522nC) A0T.A00;
        r14.A00 |= 4;
        r14.A01 = i;
        int i2 = 0;
        while (true) {
            List list = this.A04;
            if (i2 < list.size()) {
                AnonymousClass1G4 A0T2 = C57042mN.A02.A0T();
                String str2 = ((C27701Iu) list.get(i2)).A03;
                A0T2.A03();
                C57042mN r15 = (C57042mN) A0T2.A00;
                r15.A00 |= 1;
                r15.A01 = str2;
                AbstractC27091Fz A02 = A0T2.A02();
                A0T.A03();
                C57522nC r2 = (C57522nC) A0T.A00;
                AnonymousClass1K6 r16 = r2.A03;
                if (!((AnonymousClass1K7) r16).A00) {
                    r16 = AbstractC27091Fz.A0G(r16);
                    r2.A03 = r16;
                }
                r16.add(A02);
                i2++;
            } else {
                r4.A03();
                C27081Fy r17 = (C27081Fy) r4.A00;
                r17.A0T = (C57522nC) A0T.A02();
                r17.A01 |= 64;
                return;
            }
        }
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r4) {
        return new C27671Iq(r4, this, this.A0I);
    }

    @Override // X.AbstractC27681Ir
    public List AGt() {
        return Collections.singletonList(new AnonymousClass1V8("meta", new AnonymousClass1W9[]{new AnonymousClass1W9("polltype", "creation")}));
    }
}
