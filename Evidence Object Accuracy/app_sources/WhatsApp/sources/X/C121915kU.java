package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.5kU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121915kU extends C118735cI {
    public final ImageView A00;

    public C121915kU(View view) {
        super(view);
        this.A00 = C12970iu.A0K(view, R.id.upi_logo);
    }
}
