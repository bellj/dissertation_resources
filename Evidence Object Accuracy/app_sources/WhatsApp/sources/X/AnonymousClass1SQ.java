package X;

import com.whatsapp.anr.SigquitBasedANRDetector;
import java.io.File;

/* renamed from: X.1SQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SQ {
    public final File A00;
    public final /* synthetic */ SigquitBasedANRDetector A01;

    public AnonymousClass1SQ(SigquitBasedANRDetector sigquitBasedANRDetector, File file) {
        this.A01 = sigquitBasedANRDetector;
        this.A00 = file;
    }
}
