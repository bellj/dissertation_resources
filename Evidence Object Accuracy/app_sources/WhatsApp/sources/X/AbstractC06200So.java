package X;

import android.view.animation.Interpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.0So  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC06200So {
    public static final Interpolator A01 = new animation.InterpolatorC07040Wl();
    public static final Interpolator A02 = new animation.InterpolatorC07050Wm();
    public int A00 = -1;

    public abstract int A01(AnonymousClass03U v, RecyclerView recyclerView);

    public boolean A04() {
        return true;
    }

    public boolean A05() {
        return true;
    }

    public boolean A06(AnonymousClass03U r2, AnonymousClass03U r3, RecyclerView recyclerView) {
        return true;
    }

    public abstract boolean A07(AnonymousClass03U v, AnonymousClass03U v2, RecyclerView recyclerView);

    public final int A00(AnonymousClass03U r6, RecyclerView recyclerView) {
        int A012 = A01(r6, recyclerView);
        int A05 = AnonymousClass028.A05(recyclerView);
        int i = A012 & 3158064;
        if (i == 0) {
            return A012;
        }
        int i2 = A012 & (i ^ -1);
        if (A05 != 0) {
            int i3 = i >> 1;
            i2 |= -3158065 & i3;
            i = i3 & 3158064;
        }
        return i2 | (i >> 2);
    }

    public int A02(RecyclerView recyclerView, int i, int i2, long j) {
        int i3 = this.A00;
        if (i3 == -1) {
            i3 = recyclerView.getResources().getDimensionPixelSize(R.dimen.item_touch_helper_max_drag_scroll_per_frame);
            this.A00 = i3;
        }
        float abs = (float) Math.abs(i2);
        float f = 1.0f;
        int signum = (int) (((float) (((int) Math.signum((float) i2)) * i3)) * A02.getInterpolation(Math.min(1.0f, (abs * 1.0f) / ((float) i))));
        if (j <= 2000) {
            f = ((float) j) / 2000.0f;
        }
        int interpolation = (int) (((float) signum) * A01.getInterpolation(f));
        if (interpolation == 0) {
            return i2 > 0 ? 1 : -1;
        }
        return interpolation;
    }

    public void A03(AnonymousClass03U r1, int i) {
    }
}
