package X;

import java.util.List;

/* renamed from: X.4Sv  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Sv {
    public Long A00;
    public List A01 = null;
    public boolean A02;
    public final long A03;
    public final AnonymousClass3FY A04;

    public AnonymousClass4Sv(AnonymousClass3FY r2, long j) {
        this.A03 = j;
        this.A04 = r2;
    }
}
