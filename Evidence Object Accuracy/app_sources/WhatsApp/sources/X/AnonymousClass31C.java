package X;

/* renamed from: X.31C  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31C extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Long A07;
    public String A08;
    public String A09;

    public AnonymousClass31C() {
        super(3524, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A08);
        r3.Abe(2, this.A01);
        r3.Abe(9, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(4, this.A09);
        r3.Abe(5, this.A04);
        r3.Abe(6, this.A05);
        r3.Abe(7, this.A00);
        r3.Abe(8, this.A07);
        r3.Abe(10, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamE2eMessageDecryptFailSender {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "clientMessageId", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eCiphertextType", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eDestination", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eFailureReason", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eSenderJid", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "e2eSenderType", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageMediaType", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "offline", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "retryCount", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "revokeType", C12960it.A0Y(this.A06));
        return C12960it.A0d("}", A0k);
    }
}
