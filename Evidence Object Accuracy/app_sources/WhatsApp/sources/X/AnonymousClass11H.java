package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.util.Pair;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.11H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11H implements AbstractC18270sB {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final AnonymousClass10Y A05;
    public final C16490p7 A06;
    public final C21860y6 A07;
    public final C22710zW A08;
    public final C17070qD A09;
    public final C30931Zj A0A = C30931Zj.A00("PaymentStatusNotifier", "notification", "COMMON");
    public final AnonymousClass14X A0B;
    public final C22140ya A0C;
    public final AnonymousClass01H A0D;

    public AnonymousClass11H(C15570nT r4, C15550nR r5, C15610nY r6, C14830m7 r7, C16590pI r8, AnonymousClass10Y r9, C16490p7 r10, C21860y6 r11, C22710zW r12, C17070qD r13, AnonymousClass14X r14, C22140ya r15, AnonymousClass01H r16) {
        this.A04 = r8;
        this.A03 = r7;
        this.A00 = r4;
        this.A0B = r14;
        this.A01 = r5;
        this.A02 = r6;
        this.A09 = r13;
        this.A05 = r9;
        this.A06 = r10;
        this.A07 = r11;
        this.A0C = r15;
        this.A08 = r12;
        this.A0D = r16;
    }

    public synchronized void A00() {
        C14830m7 r7;
        ArrayList arrayList;
        ArrayList arrayList2;
        C16490p7 r12;
        C16310on A02;
        C16490p7 r13;
        C15650ng r6 = (C15650ng) this.A0D.get();
        C17070qD r9 = this.A09;
        r9.A03();
        C20370ve r5 = r9.A08;
        synchronized (r5) {
            r7 = r5.A02;
            long A00 = r7.A00();
            List<AnonymousClass1IR> A0R = r5.A0R();
            arrayList = new ArrayList();
            try {
                r13 = r5.A04;
                A02 = r13.A02();
            } catch (SQLiteDatabaseCorruptException unused) {
                r5.A09.A05("expireOldPendingRequests failed.");
            }
            try {
                AnonymousClass1Lx A002 = A02.A00();
                for (AnonymousClass1IR r122 : A0R) {
                    AbstractC30891Zf r1 = r122.A0A;
                    if (r1 == null || r1.A07() < A00) {
                        ContentValues contentValues = new ContentValues();
                        Pair A05 = C20370ve.A05(r122.A0L, r122.A0K);
                        contentValues.put("status", (Integer) 16);
                        contentValues.put("timestamp", Integer.valueOf((int) (A00 / 1000)));
                        C30931Zj r3 = r5.A09;
                        StringBuilder sb = new StringBuilder();
                        sb.append("expireOldPendingRequests key id:");
                        sb.append(r122.A0L);
                        r3.A06(sb.toString());
                        if (r5.A0f()) {
                            C20370ve.A08(contentValues, A02, r122);
                        }
                        r13.A04();
                        if (r13.A05.A0E(A02)) {
                            A02.A03.A00("pay_transactions", contentValues, (String) A05.first, (String[]) A05.second);
                        }
                        arrayList.add(r122);
                    }
                }
                A002.A00();
                A002.close();
                A02.close();
            } finally {
            }
        }
        r9.A03();
        synchronized (r5) {
            long A003 = r7.A00();
            List<AnonymousClass1IR> A0b = r5.A0b(new Integer[]{20}, new Integer[]{40}, -1);
            arrayList2 = new ArrayList();
            try {
                r12 = r5.A04;
                A02 = r12.A02();
            } catch (SQLiteDatabaseCorruptException unused2) {
                r5.A09.A05("expirePendingMandateRequests failed.");
            }
            try {
                AnonymousClass1Lx A004 = A02.A00();
                for (AnonymousClass1IR r11 : A0b) {
                    AbstractC30891Zf r14 = r11.A0A;
                    if (r14 == null || r14.A07() < A003) {
                        ContentValues contentValues2 = new ContentValues();
                        Pair A052 = C20370ve.A05(r11.A0L, r11.A0K);
                        contentValues2.put("status", (Integer) 16);
                        contentValues2.put("timestamp", Integer.valueOf((int) (A003 / 1000)));
                        C30931Zj r32 = r5.A09;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("expireOldPendingRequests key id:");
                        sb2.append(r11.A0L);
                        r32.A06(sb2.toString());
                        if (r5.A0f()) {
                            C20370ve.A08(contentValues2, A02, r11);
                        }
                        r12.A04();
                        if (r12.A05.A0E(A02)) {
                            A02.A03.A00("pay_transactions", contentValues2, (String) A052.first, (String[]) A052.second);
                        }
                        arrayList2.add(r11);
                    }
                }
                A004.A00();
                A004.close();
                A02.close();
            } finally {
            }
        }
        arrayList.addAll(arrayList2);
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            AnonymousClass1IR r72 = (AnonymousClass1IR) it.next();
            C30541Xv A0A = this.A0C.A0A(r72.A0C, 44, this.A03.A00());
            A0A.A01 = r72.A0D;
            A0A.A00 = r72.A0E;
            A0A.A03 = AnonymousClass14X.A06(r72.A08, r72.A0I);
            A0A.A02 = new AnonymousClass1IS(r72.A0C, r72.A0L, r72.A0Q);
            r6.A0r(A0A, 16);
            synchronized (r6) {
                AbstractC15340mz A0E = r6.A0E(r72);
                if (A0E != null) {
                    r72.A02 = 16;
                    A0E.A0L = r72;
                    r6.A0e.A00(A0E, 16);
                    r6.A0a.A0N(A0E);
                }
            }
        }
    }

    @Override // X.AbstractC18270sB
    public void ARF() {
        ArrayList arrayList;
        C30931Zj r5;
        C16490p7 r8;
        C16310on A02;
        AbstractC38181ne AFb;
        ArrayList arrayList2;
        String str;
        String[] strArr;
        C15570nT r0 = this.A00;
        r0.A08();
        if (r0.A00 != null && this.A08.A03()) {
            C16490p7 r02 = this.A06;
            r02.A04();
            if (r02.A01) {
                C17070qD r3 = this.A09;
                AnonymousClass17Y ABn = r3.A02().ABn();
                if ((ABn == null || ABn.AdJ(null)) && !this.A07.A0C() && (AFb = r3.A02().AFb()) != null) {
                    C15650ng r52 = (C15650ng) this.A0D.get();
                    r3.A03();
                    C20370ve r6 = r3.A08;
                    synchronized (r6) {
                        String[] strArr2 = {Integer.toString(102)};
                        r6.A0g();
                        try {
                            A02 = r6.A04.get();
                        } catch (IllegalStateException e) {
                            r6.A09.A0A("readUnacceptedTransactions/IllegalStateException ", e);
                            arrayList2 = new ArrayList();
                        }
                        try {
                            C16330op r82 = A02.A03;
                            if (r6.A0g()) {
                                str = "pay_transaction";
                            } else {
                                str = "pay_transactions";
                            }
                            if (r6.A0g()) {
                                strArr = C20370ve.A0B;
                            } else {
                                strArr = C20370ve.A0A;
                            }
                            Cursor A08 = r82.A08(str, "status=? AND metadata LIKE '%expiryTs%'", "timestamp ASC", "100", strArr, strArr2);
                            arrayList2 = new ArrayList(A08.getCount());
                            while (A08.moveToNext()) {
                                try {
                                    arrayList2.add(r6.A0I(A08));
                                } catch (AnonymousClass1MW e2) {
                                    r6.A09.A0A("readUnacceptedTransactions/InvalidJidException - Skipped unaccepted transaction with invalid JID", e2);
                                }
                            }
                            C30931Zj r83 = r6.A09;
                            StringBuilder sb = new StringBuilder();
                            sb.append("readUnacceptedTransactions returned: ");
                            sb.append(arrayList2.size());
                            r83.A06(sb.toString());
                            A08.close();
                            A02.close();
                        } finally {
                        }
                    }
                    if (arrayList2.size() == 0) {
                        this.A0A.A03(null, "sendAcceptPaymentReminderNotificationsIfNeeded skipped. No pending transaction with expiry timestamp.");
                    } else {
                        for (AbstractC15340mz r1 : AFb.A6m(arrayList2)) {
                            r52.A0r(r1, 16);
                        }
                    }
                }
                A00();
                r3.A03();
                C20370ve r12 = r3.A08;
                synchronized (r12) {
                    long A00 = r12.A02.A00();
                    if (r12.A0g()) {
                        Pair A0C = r12.A0C();
                        String str2 = (String) A0C.first;
                        String[] strArr3 = (String[]) A0C.second;
                        r12.A0g();
                        try {
                            A02 = r12.A04.get();
                            try {
                                Cursor A082 = A02.A03.A08("pay_transaction", str2, "init_timestamp DESC", "", C20370ve.A0B, strArr3);
                                arrayList = new ArrayList(A082.getCount());
                                while (A082.moveToNext()) {
                                    try {
                                        arrayList.add(r12.A0I(A082));
                                    } catch (AnonymousClass1MW e3) {
                                        r12.A09.A0A("readPendingAndActiveWithdrawalsV2/InvalidJidException - Skipped pending withdrawal with invalid JID", e3);
                                    }
                                }
                                r5 = r12.A09;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("readPendingAndActiveWithdrawalsV2 returned: ");
                                sb2.append(arrayList.size());
                                r5.A03(null, sb2.toString());
                                A082.close();
                                A02.close();
                            } finally {
                            }
                        } catch (IllegalStateException e4) {
                            r5 = r12.A09;
                            r5.A0A("readPendingAndActiveWithdrawalsV2/IllegalStateException ", e4);
                            arrayList = new ArrayList();
                        }
                    } else {
                        Pair A0C2 = r12.A0C();
                        String str3 = (String) A0C2.first;
                        String[] strArr4 = (String[]) A0C2.second;
                        try {
                            C16310on A01 = r12.A04.get();
                            try {
                                Cursor A083 = A01.A03.A08("pay_transactions", str3, "init_timestamp DESC", "", C20370ve.A0A, strArr4);
                                arrayList = new ArrayList(A083.getCount());
                                while (A083.moveToNext()) {
                                    try {
                                        arrayList.add(r12.A0I(A083));
                                    } catch (AnonymousClass1MW e5) {
                                        r12.A09.A0A("PaymentTransactionStore/readPendingAndActiveWithdrawals/InvalidJidException - Skipped pending withdrawal with invalid JID", e5);
                                    }
                                }
                                r5 = r12.A09;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("readPendingAndActiveWithdrawals returned: ");
                                sb3.append(arrayList.size());
                                r5.A03(null, sb3.toString());
                                A083.close();
                                A01.close();
                            } finally {
                                try {
                                    A01.close();
                                } catch (Throwable unused) {
                                }
                            }
                        } catch (IllegalStateException e6) {
                            r5 = r12.A09;
                            r5.A0A("PaymentTransactionStore/readPendingAndActiveWithdrawals/IllegalStateException ", e6);
                            arrayList = new ArrayList();
                        }
                    }
                    try {
                        r8 = r12.A04;
                        A02 = r8.A02();
                    } catch (SQLiteDatabaseCorruptException unused2) {
                        r5.A05("expireOldWithdrawals failed.");
                    }
                    try {
                        AnonymousClass1Lx A002 = A02.A00();
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            AnonymousClass1IR r10 = (AnonymousClass1IR) it.next();
                            AbstractC30891Zf r03 = r10.A0A;
                            if (r03 != null) {
                                long A07 = r03.A07();
                                if (A07 > 0 && A07 < A00) {
                                }
                            }
                            ContentValues contentValues = new ContentValues();
                            Pair A05 = C20370ve.A05(r10.A0L, r10.A0K);
                            contentValues.put("status", (Integer) 607);
                            contentValues.put("timestamp", Integer.valueOf((int) (A00 / 1000)));
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("expireOldWithdrawals key id:");
                            sb4.append(r10.A0K);
                            r5.A06(sb4.toString());
                            if (r12.A0f()) {
                                A02.A03.A00("pay_transaction", contentValues, "id=?", new String[]{r10.A0K});
                            }
                            r8.A04();
                            if (r8.A05.A0E(A02)) {
                                A02.A03.A00("pay_transactions", contentValues, (String) A05.first, (String[]) A05.second);
                            }
                        }
                        A002.A00();
                        A002.close();
                        A02.close();
                    } finally {
                        try {
                            A02.close();
                        } catch (Throwable unused3) {
                        }
                    }
                }
            }
        }
    }
}
