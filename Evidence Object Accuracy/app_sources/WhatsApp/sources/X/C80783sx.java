package X;

import java.util.Iterator;

/* renamed from: X.3sx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80783sx extends AbstractC468828b {
    public final Iterator itr;
    public final /* synthetic */ C81293tm this$0;

    public C80783sx(C81293tm r2) {
        this.this$0 = r2;
        this.itr = r2.val$set1.iterator();
    }

    @Override // X.AbstractC468828b
    public Object computeNext() {
        while (this.itr.hasNext()) {
            Object next = this.itr.next();
            if (this.this$0.val$set2.contains(next)) {
                return next;
            }
        }
        return endOfData();
    }
}
