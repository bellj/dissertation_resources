package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0ZA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZA implements AbstractC12650iG {
    public final /* synthetic */ AnonymousClass02H A00;

    public AnonymousClass0ZA(AnonymousClass02H r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12650iG
    public View ABJ(int i) {
        return this.A00.A0D(i);
    }

    @Override // X.AbstractC12650iG
    public int ABL(View view) {
        return view.getRight() + ((AnonymousClass0B6) view.getLayoutParams()).A03.right + ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin;
    }

    @Override // X.AbstractC12650iG
    public int ABN(View view) {
        return (view.getLeft() - ((AnonymousClass0B6) view.getLayoutParams()).A03.left) - ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin;
    }

    @Override // X.AbstractC12650iG
    public int AEw() {
        AnonymousClass02H r0 = this.A00;
        return r0.A03 - r0.A0A();
    }

    @Override // X.AbstractC12650iG
    public int AEy() {
        return this.A00.A09();
    }
}
