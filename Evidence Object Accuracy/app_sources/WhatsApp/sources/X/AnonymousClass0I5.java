package X;

import android.graphics.Path;
import android.util.Log;

/* renamed from: X.0I5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0I5 extends AnonymousClass0P4 {
    public float A00;
    public float A01;
    public Path A02;
    public final /* synthetic */ C06540Ua A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0I5(Path path, C06540Ua r2, float f, float f2) {
        super(r2);
        this.A03 = r2;
        this.A00 = f;
        this.A01 = f2;
        this.A02 = path;
    }

    @Override // X.AnonymousClass0P4
    public void A00(String str) {
        C06540Ua r2 = this.A03;
        if (r2.A0j()) {
            Path path = new Path();
            r2.A03.A00.getTextPath(str, 0, str.length(), this.A00, this.A01, path);
            this.A02.addPath(path);
        }
        this.A00 += r2.A03.A00.measureText(str);
    }

    @Override // X.AnonymousClass0P4
    public boolean A01(AbstractC03250Hb r4) {
        if (!(r4 instanceof C03480Hy)) {
            return true;
        }
        Log.w("SVGAndroidRenderer", String.format("Using <textPath> elements in a clip path is not supported.", new Object[0]));
        return false;
    }
}
