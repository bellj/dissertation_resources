package X;

/* renamed from: X.51V  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass51V implements AnonymousClass5T0 {
    public final /* synthetic */ C94874cg A00;
    public final /* synthetic */ boolean A01;

    public AnonymousClass51V(C94874cg r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (r2.A01 != false) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        r1 = X.AnonymousClass39w.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000c, code lost:
        if (r2.A01 != false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        r1 = X.AnonymousClass39w.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        X.AnonymousClass3JQ.A03(r1, r2.A00, (java.lang.String) r4);
     */
    @Override // X.AnonymousClass5T0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean Afj(int r3, java.lang.Object r4) {
        /*
            r2 = this;
            switch(r3) {
                case 55: goto L_0x0005;
                case 56: goto L_0x000a;
                case 57: goto L_0x001f;
                case 58: goto L_0x0024;
                case 59: goto L_0x0018;
                case 60: goto L_0x0003;
                case 61: goto L_0x0029;
                default: goto L_0x0003;
            }
        L_0x0003:
            r0 = 0
            return r0
        L_0x0005:
            X.4cg r1 = r2.A00
            X.39w r0 = X.AnonymousClass39w.A01
            goto L_0x002d
        L_0x000a:
            boolean r0 = r2.A01
            if (r0 == 0) goto L_0x001c
        L_0x000e:
            X.39w r1 = X.AnonymousClass39w.A03
        L_0x0010:
            X.4cg r0 = r2.A00
            java.lang.String r4 = (java.lang.String) r4
            X.AnonymousClass3JQ.A03(r1, r0, r4)
            goto L_0x0003
        L_0x0018:
            boolean r0 = r2.A01
            if (r0 == 0) goto L_0x000e
        L_0x001c:
            X.39w r1 = X.AnonymousClass39w.A02
            goto L_0x0010
        L_0x001f:
            X.4cg r1 = r2.A00
            X.39w r0 = X.AnonymousClass39w.A02
            goto L_0x002d
        L_0x0024:
            X.4cg r1 = r2.A00
            X.39w r0 = X.AnonymousClass39w.A03
            goto L_0x002d
        L_0x0029:
            X.4cg r1 = r2.A00
            X.39w r0 = X.AnonymousClass39w.A04
        L_0x002d:
            java.lang.String r4 = (java.lang.String) r4
            X.AnonymousClass3JQ.A03(r0, r1, r4)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass51V.Afj(int, java.lang.Object):boolean");
    }
}
