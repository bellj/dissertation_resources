package X;

import java.util.ArrayList;

/* renamed from: X.3HF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HF {
    public static final ArrayList A06;
    public static final ArrayList A07;
    public final AnonymousClass1V8 A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A07 = C12960it.A0m("1", strArr, 1);
        String[] strArr2 = new String[2];
        strArr2[0] = "0";
        A06 = C12960it.A0m("1", strArr2, 1);
    }

    public AnonymousClass3HF(AnonymousClass1V8 r13) {
        this.A03 = (String) AnonymousClass3JT.A04(null, r13, String.class, 1L, 1000L, null, new String[]{"credential-id"}, false);
        this.A01 = (String) AnonymousClass3JT.A03(null, r13, String.class, 1L, 10L, null, new String[]{"country"}, false);
        this.A02 = (String) AnonymousClass3JT.A03(null, r13, String.class, 1L, 100L, null, new String[]{"created"}, false);
        this.A05 = AnonymousClass3JT.A08(r13, A07, new String[]{"p2p-eligible"});
        this.A04 = AnonymousClass3JT.A08(r13, A06, new String[]{"p2m-eligible"});
        this.A00 = r13;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3HF.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3HF r5 = (AnonymousClass3HF) obj;
            if (!C29941Vi.A00(this.A05, r5.A05) || !C29941Vi.A00(this.A04, r5.A04) || !this.A03.equals(r5.A03) || !C29941Vi.A00(this.A01, r5.A01) || !C29941Vi.A00(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[5];
        objArr[0] = this.A05;
        objArr[1] = this.A04;
        objArr[2] = this.A03;
        objArr[3] = this.A01;
        return C12980iv.A0B(this.A02, objArr, 4);
    }
}
