package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3fv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractBinderC73253fv extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public AbstractBinderC73253fv(String str) {
        attachInterface(this, str);
    }

    public boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        AbstractBinderC78873pg r2 = (AbstractBinderC78873pg) this;
        switch (i) {
            case 101:
                Parcelable.Creator creator = GoogleSignInAccount.CREATOR;
                if (parcel.readInt() != 0) {
                    creator.createFromParcel(parcel);
                }
                Parcelable.Creator creator2 = Status.CREATOR;
                if (parcel.readInt() != 0) {
                    creator2.createFromParcel(parcel);
                }
                throw C12970iu.A0z();
            case 102:
                Status status = (Status) C12970iu.A0F(parcel, Status.CREATOR);
                if (r2 instanceof BinderC77503nO) {
                    ((BinderC77503nO) r2).A00.A05(status);
                    break;
                } else {
                    throw C12970iu.A0z();
                }
            case 103:
                Status status2 = (Status) C12970iu.A0F(parcel, Status.CREATOR);
                if (r2 instanceof BinderC77513nP) {
                    ((BinderC77513nP) r2).A00.A05(status2);
                    break;
                } else {
                    throw C12970iu.A0z();
                }
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }

    @Override // android.os.Binder
    public final boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        return A00(i, parcel, parcel2, i2);
    }
}
