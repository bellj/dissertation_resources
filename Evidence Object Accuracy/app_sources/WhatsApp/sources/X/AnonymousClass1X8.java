package X;

/* renamed from: X.1X8  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1X8 extends AbstractC16130oV {
    public AnonymousClass1X8(C16150oX r1, AnonymousClass1IS r2, AnonymousClass1X8 r3, byte b, long j, boolean z) {
        super(r1, r2, r3, b, j, z);
    }

    public AnonymousClass1X8(AnonymousClass1IS r1, byte b, long j) {
        super(r1, b, j);
    }

    @Override // X.AbstractC15340mz
    public C16460p3 A0G() {
        C16460p3 A0G = super.A0G();
        AnonymousClass009.A05(A0G);
        return A0G;
    }

    public C81963ur A1B(C81963ur r10, C39971qq r11) {
        boolean z = r11.A08;
        boolean z2 = r11.A06;
        C81963ur A1C = A1C(r10, z, z2);
        if (A1C != null) {
            AnonymousClass1PG r5 = r11.A04;
            byte[] bArr = r11.A09;
            if (C32411c7.A0U(r5, this, bArr)) {
                C43261wh A0P = C32411c7.A0P(r11.A00, r11.A02, r5, this, bArr, z2);
                A1C.A03();
                C40821sO r1 = (C40821sO) A1C.A00;
                r1.A0J = A0P;
                r1.A00 |= 4096;
            }
        }
        return A1C;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c1, code lost:
        if (r14 == false) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c5, code lost:
        if (r3.A0U != null) goto L_0x00c7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C81963ur A1C(X.C81963ur r13, boolean r14, boolean r15) {
        /*
        // Method dump skipped, instructions count: 761
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1X8.A1C(X.3ur, boolean, boolean):X.3ur");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x021a, code lost:
        if ((r2 & 32) == 32) goto L_0x021c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x021c, code lost:
        r5.A08 = r12.A04;
        r5.A06 = r12.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x022e, code lost:
        if (r13 != false) goto L_0x0211;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01eb, code lost:
        if (r13 == false) goto L_0x022a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x016d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1D(X.C40821sO r12, boolean r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 670
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1X8.A1D(X.1sO, boolean, boolean):void");
    }
}
