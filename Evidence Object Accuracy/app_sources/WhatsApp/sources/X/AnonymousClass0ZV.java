package X;

import android.util.Log;

/* renamed from: X.0ZV  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0ZV implements AbstractC11930h6 {
    @Override // X.AbstractC11930h6
    public void A8f(String str, String str2) {
        C16700pc.A0E(str, 0);
        C16700pc.A0E(str2, 1);
        Log.d(str, str2);
    }
}
