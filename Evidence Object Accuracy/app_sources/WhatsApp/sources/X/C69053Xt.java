package X;

import android.os.Parcel;

/* renamed from: X.3Xt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69053Xt implements AnonymousClass2SN {
    public C69053Xt(Parcel parcel) {
        Object readValue = parcel.readValue(Class.class.getClassLoader());
        AnonymousClass009.A05(readValue);
        parcel.readParcelable(((Class) readValue).getClassLoader());
        AnonymousClass009.A05(null);
    }

    @Override // X.AnonymousClass2SN
    public String A64(String str, Object obj) {
        throw C12980iv.A0n("get");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        throw C12980iv.A0n("equals");
    }

    public int hashCode() {
        return C12970iu.A08(null, C12970iu.A1b());
    }
}
