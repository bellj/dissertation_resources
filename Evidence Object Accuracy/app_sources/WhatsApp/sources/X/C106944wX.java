package X;

/* renamed from: X.4wX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106944wX implements AnonymousClass5WY {
    public final long A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final AnonymousClass5SJ A05;

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public C106944wX(AnonymousClass5SJ r1, long j, long j2, long j3, long j4, long j5) {
        this.A05 = r1;
        this.A03 = j;
        this.A02 = j2;
        this.A04 = j3;
        this.A01 = j4;
        this.A00 = j5;
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A03;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        C94324bc r1 = new C94324bc(j, C93754ag.A00(this.A05.Aep(j), 0, this.A02, this.A04, this.A01, this.A00));
        return new C92684Xa(r1, r1);
    }
}
