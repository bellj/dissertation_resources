package X;

import android.content.Context;
import com.whatsapp.twofactor.TwoFactorAuthActivity;

/* renamed from: X.4r6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103654r6 implements AbstractC009204q {
    public final /* synthetic */ TwoFactorAuthActivity A00;

    public C103654r6(TwoFactorAuthActivity twoFactorAuthActivity) {
        this.A00 = twoFactorAuthActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
