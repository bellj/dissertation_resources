package X;

import java.util.Arrays;

/* renamed from: X.4bA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94044bA {
    public byte A00;
    public short A01;
    public byte[] A02;

    public C94044bA(byte[] bArr) {
        byte b;
        int length = bArr.length;
        do {
            length--;
            b = bArr[length];
        } while (b == 0);
        this.A01 = (short) ((length - length) - 1);
        this.A00 = b;
        this.A02 = Arrays.copyOfRange(bArr, 0, length);
    }

    public C94044bA(byte[] bArr, byte b) {
        this.A02 = bArr;
        this.A00 = b;
        this.A01 = 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("WtTlsInnerPlainText{content= [");
        byte[] bArr = this.A02;
        A0k.append(bArr.length);
        A0k.append("] ");
        A0k.append(AnonymousClass3JS.A03(bArr));
        A0k.append(", contentType=");
        A0k.append((int) this.A00);
        A0k.append(", zeros=");
        A0k.append((int) this.A01);
        return C12970iu.A0v(A0k);
    }
}
