package X;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.1Ue  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29671Ue {
    public static void A00(Context context, C18360sK r3, String str) {
        A01(context, r3, context.getString(R.string.error_notification_title), str, 2);
    }

    public static void A01(Context context, C18360sK r5, String str, String str2, int i) {
        Log.i("errorreporter/reporterror");
        PendingIntent A00 = AnonymousClass1UY.A00(context, 1, C14960mK.A04(context), 0);
        C005602s A002 = C22630zO.A00(context);
        A002.A0J = "critical_app_alerts@1";
        A002.A0I = "err";
        A002.A03 = 1;
        A002.A0B(str);
        A002.A0A(str);
        A002.A09(str2);
        A002.A09 = A00;
        C18360sK.A01(A002, R.drawable.notifybar_error);
        if (Build.VERSION.SDK_INT >= 21) {
            A002.A06 = 1;
        }
        r5.A03(i, A002.A01());
    }
}
