package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import com.whatsapp.R;

/* renamed from: X.0TO  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0TO {
    public static final int[][] A00 = {new int[]{16842910, 16842912}, new int[]{16842910, -16842912}, new int[]{-16842910, 16842912}, new int[]{-16842910, -16842912}};

    public static ColorStateList A00(Context context, Integer num, Integer num2, Integer num3, Integer num4) {
        int color;
        int defaultColor;
        int colorForState;
        int colorForState2;
        if (num != null) {
            color = num.intValue();
        } else {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.colorControlActivated});
            color = obtainStyledAttributes.getColor(0, -7829368);
            obtainStyledAttributes.recycle();
        }
        TypedArray obtainStyledAttributes2 = context.getTheme().obtainStyledAttributes(new int[]{R.attr.colorSwitchThumbNormal});
        ColorStateList colorStateList = obtainStyledAttributes2.getColorStateList(0);
        obtainStyledAttributes2.recycle();
        if (num2 != null) {
            defaultColor = num2.intValue();
        } else if (colorStateList == null) {
            defaultColor = 0;
        } else {
            defaultColor = colorStateList.getDefaultColor();
        }
        if (num3 != null) {
            colorForState = num3.intValue();
        } else if (colorStateList == null) {
            colorForState = 0;
        } else {
            colorForState = colorStateList.getColorForState(new int[]{-16842910}, colorStateList.getDefaultColor());
        }
        if (num4 != null) {
            colorForState2 = num4.intValue();
        } else if (colorStateList == null) {
            colorForState2 = 0;
        } else {
            colorForState2 = colorStateList.getColorForState(new int[]{-16842910}, colorStateList.getDefaultColor());
        }
        return new ColorStateList(A00, new int[]{color, defaultColor, colorForState, colorForState2});
    }

    public static ColorStateList A01(Context context, Integer num, Integer num2, Integer num3, Integer num4) {
        int color;
        int color2;
        int A06;
        int A062;
        if (num != null) {
            color = num.intValue();
        } else {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{16843818});
            color = obtainStyledAttributes.getColor(0, -7829368);
            obtainStyledAttributes.recycle();
        }
        if (num2 != null) {
            color2 = num2.intValue();
        } else {
            TypedArray obtainStyledAttributes2 = context.getTheme().obtainStyledAttributes(new int[]{16842800});
            color2 = obtainStyledAttributes2.getColor(0, -7829368);
            obtainStyledAttributes2.recycle();
        }
        if (num3 != null) {
            A06 = num3.intValue();
        } else {
            A06 = C016907y.A06(color, Math.round(((float) Color.alpha(color)) * 0.25f));
        }
        if (num4 != null) {
            A062 = num4.intValue();
        } else {
            A062 = C016907y.A06(color2, Math.round(((float) Color.alpha(color2)) * 0.25f));
        }
        return new ColorStateList(A00, new int[]{color, color2, A06, A062});
    }
}
