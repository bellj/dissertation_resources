package X;

/* renamed from: X.4Ex  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88264Ex {
    public static final void A00(int i) {
        if (i < 1) {
            throw C12970iu.A0f(C16700pc.A08("Expected positive parallelism level, but got ", Integer.valueOf(i)));
        }
    }
}
