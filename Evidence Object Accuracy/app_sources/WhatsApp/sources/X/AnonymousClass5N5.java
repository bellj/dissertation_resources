package X;

/* renamed from: X.5N5  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N5 extends AnonymousClass5NH {
    public AnonymousClass5N5(byte[] bArr) {
        super(bArr);
    }

    @Override // X.AnonymousClass5NH, X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return this;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A00);
    }
}
