package X;

import com.whatsapp.util.Log;

/* renamed from: X.2KK  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2KK implements AnonymousClass27W {
    public final C18910tG A00;

    public AnonymousClass2KK(C18910tG r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass27W
    public void AP6() {
        Log.i("SeamlessManager_DevicePairScannerCallback/DevicePair onDevicePairingRequested");
    }

    @Override // X.AnonymousClass27W
    public void APl(int i) {
        StringBuilder sb = new StringBuilder("SeamlessManager_DevicePairScannerCallback/DevicePair onError ");
        sb.append(i);
        Log.i(sb.toString());
        C18910tG r3 = this.A00;
        AnonymousClass2KN r2 = new AnonymousClass2KN();
        r2.A00 = 14;
        r2.A02 = 103L;
        r3.A02(r2.A00());
        r3.A00();
    }

    @Override // X.AnonymousClass27W
    public void ARS() {
        Log.i("SeamlessManager_DevicePairScannerCallback/DevicePair onInvalidDeviceTime");
        C18910tG r3 = this.A00;
        AnonymousClass2KN r2 = new AnonymousClass2KN();
        r2.A00 = 14;
        r2.A02 = 105L;
        r3.A02(r2.A00());
        r3.A00();
    }

    @Override // X.AnonymousClass27W
    public void ART() {
        Log.i("SeamlessManager_DevicePairScannerCallback/DevicePair onInvalidQrCode");
        C18910tG r3 = this.A00;
        AnonymousClass2KN r2 = new AnonymousClass2KN();
        r2.A00 = 14;
        r2.A02 = 104L;
        r3.A02(r2.A00());
        r3.A00();
    }

    @Override // X.AnonymousClass27W
    public void AUt() {
        Log.i("SeamlessManager_DevicePairScannerCallback/DevicePair onRemovedAllDevices");
    }

    @Override // X.AnonymousClass27W
    public void AWu() {
        Log.i("SeamlessManager_DevicePairScannerCallback/DevicePair onSuccess");
    }

    @Override // X.AnonymousClass27W
    public void AXK() {
        Log.i("SeamlessManager_DevicePairScannerCallback/DevicePair onSyncdDeleteAllError");
        C18910tG r3 = this.A00;
        AnonymousClass2KN r2 = new AnonymousClass2KN();
        r2.A00 = 14;
        r2.A02 = 102L;
        r3.A02(r2.A00());
        r3.A00();
    }
}
