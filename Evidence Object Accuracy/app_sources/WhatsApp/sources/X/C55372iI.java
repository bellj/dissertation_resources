package X;

import com.whatsapp.greenalert.GreenAlertActivity;

/* renamed from: X.2iI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55372iI extends AnonymousClass077 {
    public final /* synthetic */ GreenAlertActivity A00;

    public C55372iI(GreenAlertActivity greenAlertActivity) {
        this.A00 = greenAlertActivity;
    }

    @Override // X.AnonymousClass077, X.AnonymousClass070
    public void ATQ(int i) {
        int i2;
        GreenAlertActivity greenAlertActivity = this.A00;
        int currentLogicalItem = greenAlertActivity.A06.getCurrentLogicalItem();
        AnonymousClass12S r2 = greenAlertActivity.A0D;
        if (currentLogicalItem == 1) {
            i2 = 7;
            if (C43901xo.A02(greenAlertActivity.A0E)) {
                i2 = 3;
            }
        } else {
            i2 = 11;
        }
        r2.A01(Integer.valueOf(i2));
        greenAlertActivity.A2g(currentLogicalItem);
        greenAlertActivity.A2h(currentLogicalItem);
    }
}
