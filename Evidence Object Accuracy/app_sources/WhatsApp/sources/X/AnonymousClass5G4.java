package X;

import java.util.Hashtable;

/* renamed from: X.5G4  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G4 implements AnonymousClass20J {
    public static Hashtable A07;
    public int A00;
    public int A01;
    public AnonymousClass5XI A02;
    public AnonymousClass5WS A03;
    public AnonymousClass5WS A04;
    public byte[] A05;
    public byte[] A06;

    @Override // X.AnonymousClass20J
    public int A97(byte[] bArr, int i) {
        AnonymousClass5XI r6 = this.A02;
        byte[] bArr2 = this.A06;
        int i2 = this.A00;
        r6.A97(bArr2, i2);
        AnonymousClass5WS r1 = this.A04;
        if (r1 != null) {
            ((AnonymousClass5WS) r6).Aag(r1);
            r6.update(bArr2, i2, r6.ACZ());
        } else {
            r6.update(bArr2, 0, bArr2.length);
        }
        int A97 = r6.A97(bArr, i);
        while (i2 < bArr2.length) {
            bArr2[i2] = 0;
            i2++;
        }
        AnonymousClass5WS r0 = this.A03;
        if (r0 != null) {
            ((AnonymousClass5WS) r6).Aag(r0);
            return A97;
        }
        byte[] bArr3 = this.A05;
        r6.update(bArr3, 0, bArr3.length);
        return A97;
    }

    @Override // X.AnonymousClass20J
    public int AE2() {
        return this.A01;
    }

    @Override // X.AnonymousClass20J
    public void AfG(byte b) {
        this.A02.AfG(b);
    }

    @Override // X.AnonymousClass20J
    public void reset() {
        AnonymousClass5XI r3 = this.A02;
        r3.reset();
        byte[] bArr = this.A05;
        r3.update(bArr, 0, bArr.length);
    }

    @Override // X.AnonymousClass20J
    public void update(byte[] bArr, int i, int i2) {
        this.A02.update(bArr, i, i2);
    }

    static {
        Hashtable hashtable = new Hashtable();
        A07 = hashtable;
        hashtable.put("GOST3411", 32);
        A07.put("MD2", 16);
        A07.put("MD4", 64);
        A07.put("MD5", 64);
        A07.put("RIPEMD128", 64);
        A07.put("RIPEMD160", 64);
        A07.put("SHA-1", 64);
        A07.put("SHA-224", 64);
        A07.put("SHA-256", 64);
        A07.put("SHA-384", 128);
        A07.put("SHA-512", 128);
        A07.put("Tiger", 64);
        A07.put("Whirlpool", 64);
    }

    public AnonymousClass5G4(AnonymousClass5XI r4) {
        int intValue;
        if (r4 instanceof AbstractC117275Zf) {
            intValue = ((AbstractC117275Zf) r4).AB4();
        } else {
            Number number = (Number) A07.get(r4.AAf());
            if (number != null) {
                intValue = number.intValue();
            } else {
                throw C12970iu.A0f(C12960it.A0d(r4.AAf(), C12960it.A0k("unknown digest passed: ")));
            }
        }
        this.A02 = r4;
        int ACZ = r4.ACZ();
        this.A01 = ACZ;
        this.A00 = intValue;
        this.A05 = new byte[intValue];
        this.A06 = new byte[intValue + ACZ];
    }

    @Override // X.AnonymousClass20J
    public void AIc(AnonymousClass20L r10) {
        byte[] bArr;
        int length;
        AnonymousClass5XI r7 = this.A02;
        r7.reset();
        byte[] bArr2 = ((AnonymousClass20K) r10).A00;
        int length2 = bArr2.length;
        int i = this.A00;
        if (length2 > i) {
            r7.update(bArr2, 0, length2);
            bArr = this.A05;
            r7.A97(bArr, 0);
            length2 = this.A01;
        } else {
            bArr = this.A05;
            System.arraycopy(bArr2, 0, bArr, 0, length2);
        }
        while (true) {
            length = bArr.length;
            if (length2 >= length) {
                break;
            }
            bArr[length2] = 0;
            length2++;
        }
        byte[] bArr3 = this.A06;
        System.arraycopy(bArr, 0, bArr3, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            C72463ee.A0P(bArr[i2], bArr, 54, i2);
        }
        for (int i3 = 0; i3 < i; i3++) {
            C72463ee.A0P(bArr3[i3], bArr3, 92, i3);
        }
        boolean z = r7 instanceof AnonymousClass5WS;
        if (z) {
            AnonymousClass5WS A7l = ((AnonymousClass5WS) r7).A7l();
            this.A04 = A7l;
            ((AnonymousClass5XI) A7l).update(bArr3, 0, i);
        }
        r7.update(bArr, 0, length);
        if (z) {
            this.A03 = ((AnonymousClass5WS) r7).A7l();
        }
    }
}
