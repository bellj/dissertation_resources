package X;

import android.content.Context;

/* renamed from: X.21H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21H implements AnonymousClass21I {
    public final C06150Sj A00;

    @Override // X.AnonymousClass21I
    public void A6I(AnonymousClass02N r1, AnonymousClass21K r2) {
    }

    public AnonymousClass21H(Context context) {
        this.A00 = new C06150Sj(new AnonymousClass0XW(context));
    }

    @Override // X.AnonymousClass21I
    public boolean A6u() {
        return this.A00.A03(255) == 0;
    }

    @Override // X.AnonymousClass21I
    public boolean AIC() {
        return this.A00.A03(255) != 11;
    }

    @Override // X.AnonymousClass21I
    public boolean AJT() {
        return this.A00.A03(255) != 12;
    }
}
