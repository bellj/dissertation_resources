package X;

import android.app.Activity;
import android.os.IBinder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.0Zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07530Zb implements AbstractC12440hv {
    public static final ReentrantLock A02 = new ReentrantLock();
    public static volatile C07530Zb A03;
    public AbstractC12430hu A00;
    public final CopyOnWriteArrayList A01 = new CopyOnWriteArrayList();

    public C07530Zb(AbstractC12430hu r3) {
        this.A00 = r3;
        AbstractC12430hu r1 = this.A00;
        if (r1 != null) {
            r1.Ac7(new AnonymousClass0ZW(this));
        }
    }

    public final void A00(Activity activity) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.A01;
        if (!(copyOnWriteArrayList instanceof Collection) || !copyOnWriteArrayList.isEmpty()) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                if (C16700pc.A0O(((C05970Rr) it.next()).A01, activity)) {
                    return;
                }
            }
        }
        AbstractC12430hu r0 = this.A00;
        if (r0 != null) {
            r0.AYV(activity);
        }
    }

    public final boolean A01(Activity activity) {
        CopyOnWriteArrayList copyOnWriteArrayList = this.A01;
        if (!(copyOnWriteArrayList instanceof Collection) || !copyOnWriteArrayList.isEmpty()) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                if (C16700pc.A0O(((C05970Rr) it.next()).A01, activity)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // X.AbstractC12440hv
    public void Aa1(Activity activity, AnonymousClass024 r7, Executor executor) {
        Object obj;
        AnonymousClass0PZ r2;
        C16700pc.A0E(activity, 0);
        ReentrantLock reentrantLock = A02;
        reentrantLock.lock();
        try {
            AbstractC12430hu r22 = this.A00;
            if (r22 == null) {
                r7.accept(new AnonymousClass0PZ(C16770pj.A0G()));
            } else {
                boolean A01 = A01(activity);
                C05970Rr r3 = new C05970Rr(activity, r7, executor);
                CopyOnWriteArrayList copyOnWriteArrayList = this.A01;
                copyOnWriteArrayList.add(r3);
                if (!A01) {
                    AnonymousClass0ZY r23 = (AnonymousClass0ZY) r22;
                    IBinder A00 = AnonymousClass0LL.A00(activity);
                    if (A00 != null) {
                        r23.A02(activity, A00);
                    } else {
                        activity.getWindow().getDecorView().addOnAttachStateChangeListener(new AnonymousClass0WA(activity, r23));
                    }
                } else {
                    Iterator it = copyOnWriteArrayList.iterator();
                    do {
                        obj = null;
                        if (!it.hasNext()) {
                            break;
                        }
                        obj = it.next();
                    } while (!activity.equals(((C05970Rr) obj).A01));
                    C05970Rr r1 = (C05970Rr) obj;
                    if (!(r1 == null || (r2 = r1.A00) == null)) {
                        r3.A00 = r2;
                        r3.A03.execute(new RunnableC09620dF(r3, r2));
                    }
                }
            }
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // X.AbstractC12440hv
    public void AfD(AnonymousClass024 r7) {
        C16700pc.A0E(r7, 0);
        synchronized (A02) {
            if (this.A00 != null) {
                ArrayList arrayList = new ArrayList();
                CopyOnWriteArrayList copyOnWriteArrayList = this.A01;
                Iterator it = copyOnWriteArrayList.iterator();
                while (it.hasNext()) {
                    C05970Rr r1 = (C05970Rr) it.next();
                    if (r1.A02 == r7) {
                        arrayList.add(r1);
                    }
                }
                copyOnWriteArrayList.removeAll(arrayList);
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    A00(((C05970Rr) it2.next()).A01);
                }
            }
        }
    }
}
