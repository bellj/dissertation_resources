package X;

import java.text.Collator;
import java.util.Locale;

/* renamed from: X.5HL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HL extends ThreadLocal {
    public final /* synthetic */ Locale A00;

    public AnonymousClass5HL(Locale locale) {
        this.A00 = locale;
    }

    @Override // java.lang.ThreadLocal
    public /* bridge */ /* synthetic */ Object initialValue() {
        Collator instance = Collator.getInstance(this.A00);
        instance.setDecomposition(1);
        instance.setStrength(0);
        return instance;
    }
}
