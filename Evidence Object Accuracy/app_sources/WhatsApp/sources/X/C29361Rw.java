package X;

import javax.security.auth.Destroyable;

/* renamed from: X.1Rw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29361Rw implements Destroyable {
    public boolean A00;
    public final C29371Rx A01;
    public final AnonymousClass2ST A02;

    public C29361Rw(C29371Rx r1, AnonymousClass2ST r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    public static C29361Rw A00() {
        C90604On A01 = C32001bS.A00().A01();
        return new C29361Rw(new C29371Rx(A01.A00), new AnonymousClass2ST(A01.A01));
    }

    public static C29361Rw A01(byte[] bArr) {
        if (bArr == null || bArr.length != 64) {
            return null;
        }
        byte[][] A06 = C16050oM.A06(bArr, 32, 32);
        return new C29361Rw(new C29371Rx(A06[0]), new AnonymousClass2ST(A06[1]));
    }

    public byte[] A02() {
        return C16050oM.A05(this.A01.A01, this.A02.A01);
    }

    @Override // javax.security.auth.Destroyable
    public void destroy() {
        if (!this.A00) {
            this.A01.destroy();
            this.A02.destroy();
            this.A00 = true;
        }
    }

    @Override // javax.security.auth.Destroyable
    public boolean isDestroyed() {
        return this.A00;
    }
}
