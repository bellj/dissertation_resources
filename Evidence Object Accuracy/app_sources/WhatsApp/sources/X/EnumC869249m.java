package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC869249m extends Enum {
    public static final /* synthetic */ EnumC869249m[] A00;
    public static final EnumC869249m A01;
    public static final EnumC869249m A02;
    public static final EnumC869249m A03;
    public static final EnumC869249m A04;
    public static final EnumC869249m A05;
    public static final EnumC869249m A06;
    public static final EnumC869249m A07;
    public static final EnumC869249m A08;
    public static final EnumC869249m A09;
    public static final EnumC869249m A0A;
    public final Class zzlh;
    public final Class zzli;
    public final Object zzlj;

    public EnumC869249m(Class cls, Class cls2, Object obj, String str, int i) {
        this.zzlh = cls;
        this.zzli = cls2;
        this.zzlj = obj;
    }

    public static EnumC869249m[] values() {
        return (EnumC869249m[]) A00.clone();
    }

    static {
        EnumC869249m r2 = new EnumC869249m(Void.class, Void.class, null, "VOID", 0);
        A01 = r2;
        Class cls = Integer.TYPE;
        EnumC869249m r8 = new EnumC869249m(cls, Integer.class, 0, "INT", 1);
        A02 = r8;
        EnumC869249m r14 = new EnumC869249m(Long.TYPE, Long.class, 0L, "LONG", 2);
        A03 = r14;
        EnumC869249m r15 = new EnumC869249m(Float.TYPE, Float.class, Float.valueOf(0.0f), "FLOAT", 3);
        A04 = r15;
        EnumC869249m r0 = new EnumC869249m(Double.TYPE, Double.class, Double.valueOf(0.0d), "DOUBLE", 4);
        A05 = r0;
        EnumC869249m r16 = new EnumC869249m(Boolean.TYPE, Boolean.class, Boolean.FALSE, "BOOLEAN", 5);
        A06 = r16;
        EnumC869249m r17 = new EnumC869249m(String.class, String.class, "", "STRING", 6);
        A07 = r17;
        EnumC869249m r23 = new EnumC869249m(AbstractC111915Bh.class, AbstractC111915Bh.class, AbstractC111915Bh.A00, "BYTE_STRING", 7);
        A08 = r23;
        EnumC869249m r24 = new EnumC869249m(cls, Integer.class, null, "ENUM", 8);
        A09 = r24;
        EnumC869249m r30 = new EnumC869249m(Object.class, Object.class, null, "MESSAGE", 9);
        A0A = r30;
        EnumC869249m[] r1 = new EnumC869249m[10];
        r1[0] = r2;
        r1[1] = r8;
        C12980iv.A1P(r14, r15, r0, r1);
        r1[5] = r16;
        r1[6] = r17;
        r1[7] = r23;
        r1[8] = r24;
        r1[9] = r30;
        A00 = r1;
    }
}
