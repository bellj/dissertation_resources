package X;

/* renamed from: X.4Xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92734Xf {
    public final int A00;
    public final int A01;
    public final int A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C92734Xf) {
                C92734Xf r5 = (C92734Xf) obj;
                if (!(this.A01 == r5.A01 && this.A02 == r5.A02 && this.A00 == r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.A01 * 31) + this.A02) * 31) + this.A00;
    }

    public C92734Xf(int i, int i2, int i3) {
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("WDSButtonState(normal=");
        A0k.append(this.A01);
        A0k.append(", pressed=");
        A0k.append(this.A02);
        A0k.append(", disabled=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
