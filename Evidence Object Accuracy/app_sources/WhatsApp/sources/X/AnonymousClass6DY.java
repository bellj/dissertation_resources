package X;

import android.os.Vibrator;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiQrTabActivity;
import com.whatsapp.payments.ui.IndiaUpiScanQrCodeFragment;
import com.whatsapp.util.Log;

/* renamed from: X.6DY  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DY implements AnonymousClass27Z {
    public final /* synthetic */ IndiaUpiScanQrCodeFragment A00;

    public AnonymousClass6DY(IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment) {
        this.A00 = indiaUpiScanQrCodeFragment;
    }

    @Override // X.AnonymousClass27Z
    public void ANb(int i) {
        C14900mE r1;
        int i2;
        IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment = this.A00;
        if (indiaUpiScanQrCodeFragment.A08.A03()) {
            r1 = indiaUpiScanQrCodeFragment.A04;
            i2 = R.string.error_camera_disabled_during_video_call;
        } else if (i != 2) {
            r1 = indiaUpiScanQrCodeFragment.A04;
            i2 = R.string.cannot_start_camera;
        } else {
            return;
        }
        r1.A07(i2, 1);
    }

    @Override // X.AnonymousClass27Z
    public void AUF() {
        Log.i("qractivity/previewready");
        IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment = this.A00;
        indiaUpiScanQrCodeFragment.A09 = null;
        indiaUpiScanQrCodeFragment.A19();
    }

    @Override // X.AnonymousClass27Z
    public void AUS(C49262Kb r9) {
        IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment = this.A00;
        String str = r9.A02;
        if (str == null || str.equals(indiaUpiScanQrCodeFragment.A09)) {
            indiaUpiScanQrCodeFragment.A07.Aab();
            return;
        }
        indiaUpiScanQrCodeFragment.A09 = str;
        IndiaUpiQrTabActivity indiaUpiQrTabActivity = (IndiaUpiQrTabActivity) indiaUpiScanQrCodeFragment.A0C();
        Vibrator A0K = ((ActivityC13810kN) indiaUpiQrTabActivity).A08.A0K();
        if (A0K != null) {
            A0K.vibrate(75);
        }
        boolean A07 = ((ActivityC13810kN) indiaUpiQrTabActivity).A0C.A07(1354);
        AnonymousClass69M r2 = indiaUpiQrTabActivity.A03;
        if (A07) {
            r2.A00(indiaUpiQrTabActivity, new Runnable() { // from class: X.6GD
                @Override // java.lang.Runnable
                public final void run() {
                    IndiaUpiScanQrCodeFragment indiaUpiScanQrCodeFragment2 = IndiaUpiQrTabActivity.this.A09;
                    indiaUpiScanQrCodeFragment2.A09 = null;
                    indiaUpiScanQrCodeFragment2.A07.Aab();
                }
            }, str, "SCANNED_QR_CODE", "payments_camera");
        } else {
            indiaUpiQrTabActivity.Adl(r2.AG0(str, "payments_camera", 4), "SCANNED_QR_CODE");
        }
    }
}
