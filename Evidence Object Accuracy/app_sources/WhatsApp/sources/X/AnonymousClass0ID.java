package X;

import android.graphics.Canvas;

/* renamed from: X.0ID  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ID extends AnonymousClass0PQ {
    public final /* synthetic */ AnonymousClass0IR A00;

    public AnonymousClass0ID(AnonymousClass0IR r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0PQ
    public void A00(Canvas canvas, float f, float f2) {
        super.A00(canvas, f, f2);
        if (this.A06 == null) {
            canvas.drawBitmap(AnonymousClass0IR.A04, f, f2, this.A04);
        }
    }
}
