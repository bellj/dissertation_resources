package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C1315963j implements Parcelable {
    public static final AnonymousClass63W CREATOR = new AnonymousClass63W();
    public final int A00;
    public final int A01;
    public final EnumC124535ph A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C1315963j) {
                C1315963j r5 = (C1315963j) obj;
                if (!(this.A02 == r5.A02 && this.A01 == r5.A01 && this.A00 == r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((this.A02.hashCode() * 31) + this.A01) * 31) + this.A00;
    }

    public C1315963j() {
        this(EnumC124535ph.A02, -1, -1);
    }

    public C1315963j(EnumC124535ph r2, int i, int i2) {
        C16700pc.A0E(r2, 1);
        this.A02 = r2;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("CheckoutErrorContent(code=");
        A0k.append(this.A02);
        A0k.append(", titleRes=");
        A0k.append(this.A01);
        A0k.append(", descriptionRes=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        C16700pc.A0E(parcel, 0);
        parcel.writeSerializable(this.A02);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
    }
}
