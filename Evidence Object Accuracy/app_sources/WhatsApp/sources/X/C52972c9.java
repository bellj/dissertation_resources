package X;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2c9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52972c9 extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final Context A02;
    public final ViewGroup.MarginLayoutParams A03;
    public final TextView A04;

    public C52972c9(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        this.A02 = context;
        FrameLayout.inflate(getContext(), R.layout.search_group_header, this);
        this.A04 = C12960it.A0I(this, R.id.title);
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-1, -2);
        this.A03 = marginLayoutParams;
        setLayoutParams(marginLayoutParams);
        setBackgroundResource(R.color.primary_surface);
    }

    public void A00(String str, boolean z, int i) {
        int dimensionPixelSize;
        this.A04.setText(str);
        ViewGroup.MarginLayoutParams marginLayoutParams = this.A03;
        if (z) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.search_header_bottom_margin) - AnonymousClass3G9.A01(this.A02, (float) i);
        }
        marginLayoutParams.bottomMargin = dimensionPixelSize;
        setLayoutParams(marginLayoutParams);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
