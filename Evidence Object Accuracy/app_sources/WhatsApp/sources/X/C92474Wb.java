package X;

import android.os.Handler;

/* renamed from: X.4Wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92474Wb {
    public final Handler A00;
    public final AbstractC72373eU A01;

    public C92474Wb(Handler handler, AbstractC72373eU r2) {
        this.A00 = r2 == null ? null : handler;
        this.A01 = r2;
    }

    public void A00(AnonymousClass4U3 r3) {
        synchronized (r3) {
        }
        Handler handler = this.A00;
        if (handler != null) {
            C12980iv.A18(handler, this, r3, 3);
        }
    }
}
