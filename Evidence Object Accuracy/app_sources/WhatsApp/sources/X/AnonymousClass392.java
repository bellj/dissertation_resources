package X;

import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.dialogs.ProgressDialogFragment;
import java.lang.ref.WeakReference;
import java.util.Set;

/* renamed from: X.392  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass392 extends AbstractC16350or {
    public C15370n3 A00;
    public ProgressDialogFragment A01;
    public Set A02;
    public final DialogFragment A03;
    public final AnonymousClass132 A04;
    public final WeakReference A05;
    public final boolean A06;

    public AnonymousClass392(DialogFragment dialogFragment, AnonymousClass01F r3, AnonymousClass132 r4, C15370n3 r5, boolean z) {
        this.A05 = C12970iu.A10(r3);
        this.A04 = r4;
        this.A03 = dialogFragment;
        this.A00 = r5;
        this.A06 = z;
    }

    public AnonymousClass392(DialogFragment dialogFragment, AnonymousClass01F r3, AnonymousClass132 r4, Set set, boolean z) {
        this.A05 = C12970iu.A10(r3);
        this.A04 = r4;
        this.A03 = dialogFragment;
        this.A02 = set;
        this.A06 = z;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        DialogFragment dialogFragment;
        Number number = (Number) obj;
        AnonymousClass01F r4 = (AnonymousClass01F) this.A05.get();
        if (r4 != null && !r4.A0L) {
            ProgressDialogFragment progressDialogFragment = this.A01;
            if (((AnonymousClass01E) progressDialogFragment).A03 >= 7) {
                progressDialogFragment.A1B();
            } else {
                progressDialogFragment.A02 = true;
            }
            C15370n3 r0 = this.A00;
            if (r0 != null) {
                dialogFragment = this.A03;
                AnonymousClass3GF.A00(dialogFragment, r0);
            } else {
                Set set = this.A02;
                dialogFragment = this.A03;
                Bundle A0D = C12970iu.A0D();
                A0D.putStringArrayList("selection_jids", C15380n4.A06(set));
                dialogFragment.A0U(A0D);
            }
            Bundle bundle = ((AnonymousClass01E) dialogFragment).A05;
            bundle.putInt("unsent_count", number.intValue());
            bundle.putBoolean("chatContainsStarredMessages", this.A06);
            AnonymousClass009.A05(r4);
            C004902f r1 = new C004902f(r4);
            r1.A09(dialogFragment, null);
            r1.A02();
        }
    }
}
