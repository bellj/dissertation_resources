package X;

import java.security.Signature;

/* renamed from: X.5GP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GP implements AnonymousClass5VR {
    public final /* synthetic */ String A00;
    public final /* synthetic */ AbstractC113485Ht A01;

    public AnonymousClass5GP(String str, AbstractC113485Ht r2) {
        this.A01 = r2;
        this.A00 = str;
    }

    @Override // X.AnonymousClass5VR
    public Signature A8Z(String str) {
        String str2 = this.A00;
        return str2 != null ? Signature.getInstance(str, str2) : Signature.getInstance(str);
    }
}
