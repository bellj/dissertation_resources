package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.InteractiveMessageButton;
import com.whatsapp.conversation.conversationrow.InteractiveMessageView;
import java.util.Set;

/* renamed from: X.2xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60422xo extends AnonymousClass1OY {
    public boolean A00;
    public final InteractiveMessageButton A01 = ((InteractiveMessageButton) AnonymousClass028.A0D(this, R.id.button));
    public final InteractiveMessageView A02;

    public C60422xo(Context context, AbstractC13890kV r5, C16380ov r6) {
        super(context, r5, r6);
        A0Z();
        InteractiveMessageView interactiveMessageView = (InteractiveMessageView) AnonymousClass028.A0D(this, R.id.interactive_view);
        this.A02 = interactiveMessageView;
        AnonymousClass1OY.A0R(this, interactiveMessageView, r6);
        AbstractC15340mz fMessage = getFMessage();
        this.A02.A00(this, fMessage);
        this.A01.A00(this, ((AbstractC28551Oa) this).A0a, fMessage);
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r4, boolean z) {
        boolean A1X = C12960it.A1X(r4, getFMessage());
        super.A1D(r4, z);
        if (z || A1X) {
            AbstractC15340mz fMessage = getFMessage();
            this.A02.A00(this, fMessage);
            this.A01.A00(this, ((AbstractC28551Oa) this).A0a, fMessage);
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_multi_element_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_multi_element_left;
    }

    @Override // X.AnonymousClass1OY
    public Set getInnerFrameLayouts() {
        Set innerFrameLayouts = super.getInnerFrameLayouts();
        InteractiveMessageView interactiveMessageView = this.A02;
        if (interactiveMessageView != null) {
            innerFrameLayouts.add(interactiveMessageView.getInnerFrameLayout());
        }
        return innerFrameLayouts;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_multi_element_right;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C16380ov);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
