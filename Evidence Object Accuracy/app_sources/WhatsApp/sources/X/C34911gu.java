package X;

import java.util.Collections;

/* renamed from: X.1gu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34911gu implements AbstractC34921gv {
    @Override // X.AbstractC34921gv
    public String AEZ() {
        return "f";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0030, code lost:
        if (r5 != 29) goto L_0x0032;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003c  */
    @Override // X.AbstractC34921gv
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Set AEH(X.AbstractC15340mz r7) {
        /*
            r6 = this;
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
            byte r5 = r7.A0y
            r4 = 23
            r2 = 1
            if (r5 == r2) goto L_0x006f
            r1 = 2
            r0 = 97
            if (r5 == r1) goto L_0x0074
            r0 = 3
            if (r5 == r0) goto L_0x006c
            r0 = 9
            if (r5 == r0) goto L_0x0072
            r0 = 13
            if (r5 == r0) goto L_0x0069
            if (r5 == r4) goto L_0x006f
            r0 = 37
            if (r5 == r0) goto L_0x006f
            r0 = 25
            if (r5 == r0) goto L_0x006f
            r0 = 26
            if (r5 == r0) goto L_0x0072
            r0 = 28
            if (r5 == r0) goto L_0x006c
            r0 = 29
            if (r5 == r0) goto L_0x0069
        L_0x0032:
            java.lang.String r0 = X.C20050v8.A00(r7)
            java.util.ArrayList r0 = X.C33771f3.A05(r0, r2)
            if (r0 == 0) goto L_0x0045
            r0 = 108(0x6c, float:1.51E-43)
            java.lang.String r0 = java.lang.Character.toString(r0)
            r3.add(r0)
        L_0x0045:
            X.0mz r1 = r7.A0E()
            boolean r0 = r1 instanceof X.AnonymousClass1XV
            if (r0 == 0) goto L_0x0067
            X.1IS r0 = r1.A0z
            java.lang.String r1 = r0.A01
            java.lang.String r0 = "product_inquiry"
            boolean r0 = r1.startsWith(r0)
            if (r0 == 0) goto L_0x0067
        L_0x0059:
            if (r5 == r4) goto L_0x005d
            if (r2 == 0) goto L_0x0066
        L_0x005d:
            r0 = 112(0x70, float:1.57E-43)
            java.lang.String r0 = java.lang.Character.toString(r0)
            r3.add(r0)
        L_0x0066:
            return r3
        L_0x0067:
            r2 = 0
            goto L_0x0059
        L_0x0069:
            r0 = 103(0x67, float:1.44E-43)
            goto L_0x0074
        L_0x006c:
            r0 = 118(0x76, float:1.65E-43)
            goto L_0x0074
        L_0x006f:
            r0 = 105(0x69, float:1.47E-43)
            goto L_0x0074
        L_0x0072:
            r0 = 100
        L_0x0074:
            java.lang.String r0 = java.lang.Character.toString(r0)
            r3.add(r0)
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34911gu.AEH(X.0mz):java.util.Set");
    }

    @Override // X.AbstractC34921gv
    public C34941gx AEa(C15250mo r3) {
        if (r3.A02 == 0) {
            return null;
        }
        C34941gx r1 = new C34941gx();
        r1.A00 = Collections.singleton(Character.toString((char) r3.A02));
        return r1;
    }
}
