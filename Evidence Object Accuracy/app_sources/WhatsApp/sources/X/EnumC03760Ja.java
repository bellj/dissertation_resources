package X;

/* renamed from: X.0Ja  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03760Ja {
    UNKNOWN,
    HORIZONTAL_DIMENSION,
    VERTICAL_DIMENSION,
    LEFT,
    RIGHT,
    TOP,
    BOTTOM,
    BASELINE
}
