package X;

/* renamed from: X.0fh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11070fh extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C11070fh() {
        super(1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r3.getRect().height() != 0) goto L_0x001b;
     */
    @Override // X.AnonymousClass1J7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object AJ4(java.lang.Object r3) {
        /*
            r2 = this;
            androidx.window.sidecar.SidecarDisplayFeature r3 = (androidx.window.sidecar.SidecarDisplayFeature) r3
            r0 = 0
            X.C16700pc.A0E(r3, r0)
            android.graphics.Rect r0 = r3.getRect()
            int r0 = r0.width()
            if (r0 != 0) goto L_0x001b
            android.graphics.Rect r0 = r3.getRect()
            int r1 = r0.height()
            r0 = 0
            if (r1 == 0) goto L_0x001c
        L_0x001b:
            r0 = 1
        L_0x001c:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C11070fh.AJ4(java.lang.Object):java.lang.Object");
    }
}
