package X;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.View;

/* renamed from: X.0Ab  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02110Ab extends ClickableSpan {
    public final int A00;
    public final int A01;
    public final AnonymousClass04Z A02;

    public C02110Ab(AnonymousClass04Z r1, int i, int i2) {
        this.A01 = i;
        this.A02 = r1;
        this.A00 = i2;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.A01);
        AnonymousClass04Z r0 = this.A02;
        r0.A02.performAction(this.A00, bundle);
    }
}
