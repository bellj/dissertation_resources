package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.0bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08710bj implements AnonymousClass5WW {
    public final C14260l7 A00;

    public C08710bj(C14260l7 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        if (obj3 instanceof C63303Bb) {
            view.setContentDescription(((C63303Bb) obj3).A05);
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        if (obj3 instanceof C63303Bb) {
            view.setContentDescription(null);
        }
    }
}
