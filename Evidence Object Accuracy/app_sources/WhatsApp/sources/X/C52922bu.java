package X;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* renamed from: X.2bu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52922bu extends FrameLayout {
    public AnonymousClass11I A00;
    public final C620634h A01;

    public C52922bu(Context context, AnonymousClass11I r5) {
        super(context);
        this.A00 = r5;
        C12960it.A0E(this).inflate(R.layout.search_directory_row, (ViewGroup) this, true);
        C12960it.A0I(this, R.id.title).setText(R.string.biz_dir_consumer_search_section_title_v2);
        C620634h r0 = new C620634h(context);
        this.A01 = r0;
        ((ViewGroup) AnonymousClass028.A0D(this, R.id.chips_container)).addView(r0);
    }
}
