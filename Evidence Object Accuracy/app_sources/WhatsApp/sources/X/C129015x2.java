package X;

import android.widget.ImageView;
import com.whatsapp.util.Log;

/* renamed from: X.5x2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129015x2 {
    public final /* synthetic */ ImageView A00;
    public final /* synthetic */ AbstractC28901Pl A01;
    public final /* synthetic */ C119775f5 A02;
    public final /* synthetic */ C129945yY A03;

    public C129015x2(ImageView imageView, AbstractC28901Pl r2, C119775f5 r3, C129945yY r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = imageView;
    }

    public void A00(C452120p r3, String str) {
        StringBuilder A0k = C12960it.A0k("PAY: fetchCardArtImageContentDetails Couldn't get card art for: ");
        A0k.append(str);
        Log.w(C12960it.A0Z(r3, " with error: ", A0k));
    }
}
