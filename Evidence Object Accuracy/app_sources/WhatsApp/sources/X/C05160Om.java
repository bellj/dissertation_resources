package X;

import android.util.Log;
import android.view.animation.Interpolator;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0Om  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05160Om {
    public int A00 = 0;
    public int A01 = Integer.MIN_VALUE;
    public int A02 = 0;
    public int A03 = 0;
    public int A04 = -1;
    public Interpolator A05 = null;
    public boolean A06 = false;

    public void A00(RecyclerView recyclerView) {
        int i = this.A04;
        if (i >= 0) {
            this.A04 = -1;
            recyclerView.A0W(i);
        } else if (this.A06) {
            Interpolator interpolator = this.A05;
            if (interpolator == null || this.A01 >= 1) {
                int i2 = this.A01;
                if (i2 >= 1) {
                    if (interpolator == null) {
                        RunnableC10160e8 r5 = recyclerView.A0z;
                        if (i2 == Integer.MIN_VALUE) {
                            int i3 = this.A02;
                            int i4 = this.A03;
                            r5.A02(RecyclerView.A1B, i3, i4, r5.A00(i3, i4));
                        } else {
                            r5.A02(RecyclerView.A1B, this.A02, this.A03, i2);
                        }
                    } else {
                        recyclerView.A0z.A02(interpolator, this.A02, this.A03, i2);
                    }
                    int i5 = this.A00 + 1;
                    this.A00 = i5;
                    if (i5 > 10) {
                        Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
                    }
                } else {
                    throw new IllegalStateException("Scroll duration must be a positive number");
                }
            } else {
                throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
            }
        } else {
            this.A00 = 0;
            return;
        }
        this.A06 = false;
    }
}
