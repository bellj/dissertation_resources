package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.3Rz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67563Rz implements AbstractC12640iF {
    public int A00 = -1;
    public final AnonymousClass02M A01;
    public final /* synthetic */ C36911kq A02;

    public C67563Rz(AnonymousClass02M r2, C36911kq r3) {
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AbstractC12640iF
    public void ANr(Object obj, int i, int i2) {
        this.A01.A01.A04(obj, i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ARP(int i, int i2) {
        int i3 = this.A00;
        if (i3 == -1 || i3 >= i) {
            this.A00 = i;
            if (i == 0) {
                SearchViewModel searchViewModel = this.A02.A0r;
                AnonymousClass016 r1 = searchViewModel.A0F;
                if (r1.A01() == null || C12960it.A05(r1.A01()) == 0) {
                    C12960it.A1A(searchViewModel.A0T, 0);
                }
            }
        }
        this.A01.A01.A02(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ASr(int i, int i2) {
        this.A01.A01.A01(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void AUs(int i, int i2) {
        this.A01.A01.A03(i, i2);
    }
}
