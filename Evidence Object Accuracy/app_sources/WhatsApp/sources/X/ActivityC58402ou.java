package X;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;

/* renamed from: X.2ou  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ActivityC58402ou extends AbstractActivityC83633xY {
    public AnonymousClass2GP A00;
    public C22670zS A01;
    public C15810nw A02;
    public AnonymousClass01d A03;
    public C21840y4 A04;
    public C15510nN A05;
    public C21820y2 A06;

    @Override // X.ActivityC44201yU, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A00 = new AnonymousClass2GP(Looper.getMainLooper(), this.A04, this.A06);
        this.A01.A00(this);
    }

    @Override // android.app.Activity, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC44201yU, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (this.A00.hasMessages(0)) {
            this.A00.removeMessages(0);
        }
        this.A04.A00();
    }

    @Override // X.ActivityC44201yU, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        this.A00.sendEmptyMessageDelayed(0, 3000);
        if (!this.A05.A01() && this.A05.A00() != 2) {
            StringBuilder A0k = C12960it.A0k("settings/resume/wrong-state ");
            A0k.append(this.A05.A00());
            C12960it.A1F(A0k);
            startActivity(C14960mK.A04(this));
            finish();
        }
        if (Build.VERSION.SDK_INT < 23 || !this.A01.A05()) {
            this.A01.A01(false);
            return;
        }
        Intent className = C12970iu.A0A().setClassName(getPackageName(), "com.whatsapp.authentication.AppAuthenticationActivity");
        className.setFlags(C25981Bo.A0F);
        if (!this.A09) {
            ((ActivityC44201yU) this).A04 = className;
            ((ActivityC44201yU) this).A07 = 202;
        } else {
            startActivityForResult(className, 202);
        }
        overridePendingTransition(0, 0);
    }
}
