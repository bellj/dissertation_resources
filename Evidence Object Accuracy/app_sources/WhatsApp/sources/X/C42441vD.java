package X;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0100000_I0_1;
import com.whatsapp.R;
import com.whatsapp.voipcalling.VoipErrorDialogFragment;

/* renamed from: X.1vD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42441vD extends AbstractC42451vE {
    public AnonymousClass11L A00;
    public AnonymousClass11J A01;
    public C18750sx A02;
    public C236812p A03;
    public C16120oU A04;
    public AnonymousClass11G A05;
    public AnonymousClass11H A06;
    public AnonymousClass1AT A07;
    public AnonymousClass37Q A08;
    public C17140qK A09;
    public AnonymousClass01H A0A;
    public final View.OnClickListener A0B = new ViewOnClickCListenerShape1S0100000_I0_1(this, 37);
    public final TextView A0C;
    public final AnonymousClass5V8 A0D = new AnonymousClass5V8() { // from class: X.5Av
        @Override // X.AnonymousClass5V8
        public final void ANW(AnonymousClass1YT r2) {
            C42441vD.A0Y(C42441vD.this, r2);
        }
    };

    public C42441vD(Context context, AbstractC13890kV r4, AnonymousClass1XB r5) {
        super(context, r4, r5);
        setClickable(false);
        this.A0C = (TextView) findViewById(R.id.info);
        A1M();
    }

    public static /* synthetic */ void A0Y(C42441vD r4, AnonymousClass1YT r5) {
        if (r5 == null || r5.A06 == null) {
            r4.getVoipErrorFragmentBridge();
            ((ActivityC13810kN) AnonymousClass12P.A00(r4.getContext())).Adl(VoipErrorDialogFragment.A01(new C50102Ob(), 12), null);
        } else {
            r4.A1Q.A06(r4.getContext(), r5, 8);
        }
        r4.A08 = null;
        r4.A0s();
    }

    public static boolean A0Z(AnonymousClass1XB r2) {
        int i;
        if (C30041Vv.A0j(r2) || ((C30041Vv.A0d(r2) && (r2 instanceof C30531Xu) && ((C30531Xu) r2).A00 == 0) || C30041Vv.A0f(r2) || C30041Vv.A0g(r2) || (i = r2.A00) == 62 || i == 63)) {
            return true;
        }
        return false;
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1M();
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != ((AbstractC28551Oa) this).A0O) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A1M();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:121:0x018f, code lost:
        if (r7 == 59) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x01a2, code lost:
        if (r2.A0D(r0) == false) goto L_0x01a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x01a6, code lost:
        if ((r11 instanceof X.AnonymousClass1Y5) == false) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x01aa, code lost:
        if (r7 == 77) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x01ae, code lost:
        if (r7 != 78) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x01b4, code lost:
        if (r7 == 93) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x01b8, code lost:
        if (r7 == 94) goto L_0x00d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x01ba, code lost:
        r9.setOnClickListener(null);
        r9.setClickable(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x01c1, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x01db, code lost:
        if (r1 == 3) goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x01e4, code lost:
        if (r1 != 3) goto L_0x009f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006d, code lost:
        if ((r11 instanceof X.AnonymousClass1Y1) != false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0159, code lost:
        if (r1 != false) goto L_0x015b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x015e, code lost:
        if (r0 != false) goto L_0x00d9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M() {
        /*
        // Method dump skipped, instructions count: 674
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42441vD.A1M():void");
    }

    public int getBackgroundResource() {
        AnonymousClass1XB r2 = (AnonymousClass1XB) ((AbstractC28551Oa) this).A0O;
        AbstractC14640lm r1 = r2.A0z.A00;
        if (this.A05.A01(r1)) {
            if (r2.A00 == 67 && (r2 instanceof C30531Xu)) {
                return R.drawable.message_balloon_mint;
            }
            if (r2 instanceof C30461Xm) {
                return R.drawable.date_balloon;
            }
            return R.drawable.business_balloon;
        } else if (C38311ns.A00(((AbstractC28551Oa) this).A0L, r1) || C41861uH.A00(((AbstractC28551Oa) this).A0L, r1)) {
            return R.drawable.message_balloon_mint;
        } else {
            int i = r2.A00;
            if (i == 69 && (r2 instanceof AnonymousClass1Y1)) {
                int i2 = ((AnonymousClass1Y1) r2).A00;
                if (i2 == 2 || i2 == 3) {
                    return R.drawable.message_balloon_mint;
                }
                return R.drawable.security_balloon;
            } else if (i == 67 && (r2 instanceof C30531Xu)) {
                switch (((C30531Xu) r2).A00) {
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        return R.drawable.message_balloon_mint;
                    default:
                        return R.drawable.security_balloon;
                }
            } else if (A0Z(r2)) {
                return R.drawable.security_balloon;
            } else {
                if (C30041Vv.A0s(r2) || C30041Vv.A0c(r2)) {
                    return R.drawable.business_balloon;
                }
                if (!C30041Vv.A0d(r2) || !(r2 instanceof C30531Xu) || ((C30531Xu) r2).A00 == 0) {
                    return R.drawable.date_balloon;
                }
                return R.drawable.business_balloon;
            }
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public AnonymousClass1XB getFMessage() {
        return (AnonymousClass1XB) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    public int getTextColor() {
        AnonymousClass1XB r2 = (AnonymousClass1XB) ((AbstractC28551Oa) this).A0O;
        AbstractC14640lm r1 = r2.A0z.A00;
        if (this.A05.A01(r1)) {
            if (r2.A00 == 67 && (r2 instanceof C30531Xu)) {
                return R.color.message_balloon_mint_text;
            }
            if (r2 instanceof C30461Xm) {
                return R.color.conversation_divider_text;
            }
            return R.color.bubbleBusinessText;
        } else if (C38311ns.A00(((AbstractC28551Oa) this).A0L, r1) || C41861uH.A00(((AbstractC28551Oa) this).A0L, r1)) {
            return R.color.message_balloon_mint_text;
        } else {
            int i = r2.A00;
            if (i == 67 && (r2 instanceof C30531Xu)) {
                switch (((C30531Xu) r2).A00) {
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        return R.color.message_balloon_mint_text;
                    default:
                        return R.color.bubbleSecurityText;
                }
            } else if (i == 69 && (r2 instanceof AnonymousClass1Y1)) {
                int i2 = ((AnonymousClass1Y1) r2).A00;
                if (i2 == 2 || i2 == 3) {
                    return R.color.message_balloon_mint_text;
                }
                return R.color.bubbleSecurityText;
            } else if (A0Z(r2)) {
                return R.color.bubbleSecurityText;
            } else {
                if (C30041Vv.A0s(r2) || C30041Vv.A0c(r2)) {
                    return R.color.bubbleBusinessText;
                }
                if (!C30041Vv.A0d(r2) || !(r2 instanceof C30531Xu) || ((C30531Xu) r2).A00 == 0) {
                    return R.color.conversation_divider_text;
                }
                return R.color.bubbleBusinessText;
            }
        }
    }

    private AnonymousClass115 getVoipErrorFragmentBridge() {
        return (AnonymousClass115) ((C18980tN) this.A0A.get()).A00(AnonymousClass115.class);
    }

    @Override // X.AnonymousClass1OY, android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        AnonymousClass37Q r1 = this.A08;
        if (r1 != null) {
            r1.A03(true);
            this.A08 = null;
        }
        super.onDetachedFromWindow();
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1XB);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
