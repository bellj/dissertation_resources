package X;

import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;

/* renamed from: X.13X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13X implements AbstractC15920o8 {
    public final AnonymousClass13W A00;
    public final C20660w7 A01;
    public final C17230qT A02;
    public final AbstractC14440lR A03;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{221};
    }

    public AnonymousClass13X(AnonymousClass13W r1, C20660w7 r2, C17230qT r3, AbstractC14440lR r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        if (i != 221) {
            return false;
        }
        Object obj = message.obj;
        AnonymousClass009.A05(obj);
        AnonymousClass1OT r4 = (AnonymousClass1OT) obj;
        AnonymousClass1V4 A00 = this.A02.A00(1, r4.A00);
        if (A00 != null) {
            A00.A02(3);
        }
        this.A03.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 22, r4));
        return true;
    }
}
