package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.15z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C245615z {
    public final AnonymousClass166 A00;
    public final AnonymousClass1Q5 A01;

    public static String A00(int i) {
        switch (i) {
            case 1:
                return "client_rendered";
            case 2:
                return "client_saved";
            case 3:
                return "client_written_wire";
            case 4:
                return "client_queued";
            case 5:
                return "client_waiting_to_encrypt";
            case 6:
                return "client_ready_to_send";
            case 7:
                return "client_encrypt";
            case 8:
                return "client_prekeys_fetch";
            default:
                return "unknown";
        }
    }

    public C245615z(C14830m7 r12, C14850m9 r13, C16120oU r14, AnonymousClass166 r15, C21230x5 r16, AbstractC21180x0 r17) {
        AnonymousClass1Q5 r4 = new AnonymousClass1Q5(r12, r14, r16, r17, "MessageSendPerfQplTracker", 154474694);
        AnonymousClass1Q6 r3 = r4.A06;
        char c = !r13.A07(1191) ? (char) 1 : 3;
        boolean z = r3.A03;
        if (c != 1) {
            if (c != 2) {
                if (c == 3) {
                    z = true;
                }
                r3.A03 = z;
                this.A01 = r4;
                this.A00 = r15;
            }
            z = false;
        }
        z |= false;
        r3.A03 = z;
        this.A01 = r4;
        this.A00 = r15;
    }

    public String A01(String str, int i, int i2, int i3) {
        Integer num;
        if (i3 == 0) {
            AnonymousClass166 r2 = this.A00;
            synchronized (r2) {
                Map map = (Map) r2.A00.get(Integer.valueOf(i));
                if (map == null || (num = (Integer) map.get(Integer.valueOf(i2))) == null) {
                    i3 = 0;
                } else {
                    i3 = num.intValue();
                }
            }
        }
        if (i3 <= 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_");
        sb.append(i3);
        return sb.toString();
    }

    public void A02(int i, int i2) {
        this.A01.A01(i, A01(A00(i2), i, i2, 0));
        AnonymousClass166 r3 = this.A00;
        synchronized (r3) {
            Map map = (Map) r3.A00.get(Integer.valueOf(i));
            if (map != null) {
                Integer valueOf = Integer.valueOf(i2);
                Integer num = (Integer) map.get(valueOf);
                if (num != null) {
                    map.put(valueOf, Integer.valueOf(num.intValue() + 1));
                }
            }
        }
    }

    public void A03(int i, int i2) {
        this.A01.A02(i, A01(A00(i2), i, i2, 0));
        AnonymousClass166 r1 = this.A00;
        synchronized (r1) {
            r1.A00(i, i2, 0);
        }
    }

    public void A04(int i, int i2) {
        boolean z;
        if (i2 != 8 && i2 != 7) {
            AnonymousClass166 r2 = this.A00;
            synchronized (r2) {
                Map map = (Map) r2.A00.get(Integer.valueOf(i));
                if (map != null) {
                    z = true;
                    if (((Integer) map.get(Integer.valueOf(i2))) == null) {
                    }
                }
                z = false;
            }
            if (z) {
                A02(i, i2);
            }
            int i3 = 2;
            switch (i2) {
                case 1:
                    break;
                case 2:
                    i3 = 5;
                    break;
                case 3:
                    A06(i, 2);
                    return;
                case 4:
                    i3 = 1;
                    break;
                case 5:
                    i3 = 6;
                    break;
                case 6:
                    i3 = 3;
                    break;
                default:
                    return;
            }
            A03(i, i3);
        }
    }

    public void A05(int i, int i2, int i3, int i4) {
        if (A07(i)) {
            A03(i, i2);
        } else if (i4 != 7 && i4 != 10 && i4 != 11 && i4 != 12) {
            AnonymousClass1Q5 r3 = this.A01;
            r3.A04(i, "message_send", false);
            AnonymousClass166 r4 = this.A00;
            synchronized (r4) {
                r4.A00.put(Integer.valueOf(i), new HashMap());
            }
            if (i3 >= 0) {
                int i5 = i3 + 1;
                r3.A02(i, A01(A00(i2), i, i2, i5));
                r4.A00(i, i2, i5);
            } else {
                A03(i, i2);
            }
            r3.A07.AKz("wa_type", r3.A06.A05, i, (long) i4);
        }
    }

    public void A06(int i, short s) {
        this.A01.A05(i, s);
        AnonymousClass166 r2 = this.A00;
        synchronized (r2) {
            r2.A00.remove(Integer.valueOf(i));
        }
    }

    public boolean A07(int i) {
        boolean containsKey;
        AnonymousClass166 r2 = this.A00;
        synchronized (r2) {
            containsKey = r2.A00.containsKey(Integer.valueOf(i));
        }
        return containsKey;
    }
}
