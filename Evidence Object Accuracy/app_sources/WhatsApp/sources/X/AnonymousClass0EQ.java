package X;

import android.app.Application;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.0EQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EQ extends AnonymousClass0Yo {
    public static AnonymousClass0EQ A01;
    public Application A00;

    public AnonymousClass0EQ(Application application) {
        this.A00 = application;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (!AnonymousClass014.class.isAssignableFrom(cls)) {
            return super.A7r(cls);
        }
        try {
            return (AnonymousClass015) cls.getConstructor(Application.class).newInstance(this.A00);
        } catch (IllegalAccessException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot create an instance of ");
            sb.append(cls);
            throw new RuntimeException(sb.toString(), e);
        } catch (InstantiationException e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot create an instance of ");
            sb2.append(cls);
            throw new RuntimeException(sb2.toString(), e2);
        } catch (NoSuchMethodException e3) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Cannot create an instance of ");
            sb3.append(cls);
            throw new RuntimeException(sb3.toString(), e3);
        } catch (InvocationTargetException e4) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Cannot create an instance of ");
            sb4.append(cls);
            throw new RuntimeException(sb4.toString(), e4);
        }
    }
}
