package X;

import android.text.TextUtils;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.HashMap;

/* renamed from: X.5ws  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128915ws {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ C128915ws(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity, String str) {
        this.A01 = brazilPayBloksActivity;
        this.A00 = r1;
        this.A02 = str;
    }

    public final void A00(C30881Ze r7, C452120p r8) {
        int i;
        BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
        AnonymousClass3FE r5 = this.A00;
        String str = this.A02;
        if (r8 != null || r7 == null) {
            HashMap A11 = C12970iu.A11();
            A11.put("remaining_validates", "1");
            C14830m7 r1 = ((ActivityC13790kL) brazilPayBloksActivity).A05;
            if (!TextUtils.isEmpty(str)) {
                i = Integer.parseInt(str);
            } else {
                i = 60;
            }
            A11.put("next_resend_ts", String.valueOf((long) Math.ceil(((double) (r1.A00() + ((long) (i * 1000)))) / 1000.0d)));
            AbstractActivityC121705jc.A0l(r5, A11, r8 != null ? r8.A00 : 0);
            return;
        }
        C117315Zl.A0T(r5);
    }
}
