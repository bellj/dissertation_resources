package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57522nC extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57522nC A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02 = AbstractC27881Jp.A01;
    public AnonymousClass1K6 A03 = AnonymousClass277.A01;
    public C43261wh A04;
    public String A05 = "";

    static {
        C57522nC r0 = new C57522nC();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        C81603uH r1;
        switch (r5.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C57522nC r7 = (C57522nC) obj2;
                this.A02 = r6.Afm(this.A02, r7.A02, C12960it.A1R(this.A00), C12960it.A1R(r7.A00));
                this.A05 = r6.Afy(this.A05, r7.A05, C12960it.A1V(this.A00 & 2, 2), C12960it.A1V(r7.A00 & 2, 2));
                this.A03 = r6.Afr(this.A03, r7.A03);
                this.A01 = r6.Afp(this.A01, r7.A01, C12960it.A1V(this.A00 & 4, 4), C12960it.A1V(r7.A00 & 4, 4));
                this.A04 = (C43261wh) r6.Aft(this.A04, r7.A04);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r62.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            this.A00 |= 1;
                            this.A02 = r62.A08();
                        } else if (A03 == 18) {
                            String A0A = r62.A0A();
                            this.A00 |= 2;
                            this.A05 = A0A;
                        } else if (A03 == 26) {
                            AnonymousClass1K6 r12 = this.A03;
                            if (!((AnonymousClass1K7) r12).A00) {
                                r12 = AbstractC27091Fz.A0G(r12);
                                this.A03 = r12;
                            }
                            r12.add((C57042mN) AbstractC27091Fz.A0H(r62, r72, C57042mN.A02));
                        } else if (A03 == 32) {
                            this.A00 |= 4;
                            this.A01 = r62.A02();
                        } else if (A03 == 42) {
                            if ((this.A00 & 8) == 8) {
                                r1 = (C81603uH) this.A04.A0T();
                            } else {
                                r1 = null;
                            }
                            C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r62, r72, C43261wh.A0O);
                            this.A04 = r0;
                            if (r1 != null) {
                                this.A04 = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 8;
                        } else if (!A0a(r62, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                AbstractC27091Fz.A0R(this.A03);
                return null;
            case 4:
                return new C57522nC();
            case 5:
                return new C82203vF();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C57522nC.class) {
                        if (A07 == null) {
                            A07 = AbstractC27091Fz.A09(A06);
                        }
                    }
                }
                return A07;
            default:
                throw C12970iu.A0z();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i = CodedOutputStream.A09(this.A02, 1) + 0;
        } else {
            i = 0;
        }
        if ((i3 & 2) == 2) {
            i = AbstractC27091Fz.A04(2, this.A05, i);
        }
        for (int i4 = 0; i4 < this.A03.size(); i4++) {
            i = AbstractC27091Fz.A08((AnonymousClass1G1) this.A03.get(i4), 3, i);
        }
        int i5 = this.A00;
        if ((i5 & 4) == 4) {
            i = AbstractC27091Fz.A02(4, this.A01, i);
        }
        if ((i5 & 8) == 8) {
            C43261wh r0 = this.A04;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i = AbstractC27091Fz.A08(r0, 5, i);
        }
        return AbstractC27091Fz.A07(this, i);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A02, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A05);
        }
        int i = 0;
        while (i < this.A03.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A03, i, 3);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(4, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            C43261wh r0 = this.A04;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 5);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
