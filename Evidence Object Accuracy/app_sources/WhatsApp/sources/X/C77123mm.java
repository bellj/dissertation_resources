package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3mm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77123mm extends AbstractC107394xG {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(29);
    public final List A00;

    public /* synthetic */ C77123mm(Parcel parcel) {
        int readInt = parcel.readInt();
        ArrayList A0w = C12980iv.A0w(readInt);
        for (int i = 0; i < readInt; i++) {
            A0w.add(new C93794ak(parcel));
        }
        this.A00 = Collections.unmodifiableList(A0w);
    }

    public C77123mm(List list) {
        this.A00 = Collections.unmodifiableList(list);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        List list = this.A00;
        int size = list.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            C93794ak r5 = (C93794ak) list.get(i2);
            parcel.writeLong(r5.A04);
            parcel.writeByte(r5.A0A ? (byte) 1 : 0);
            parcel.writeByte(r5.A08 ? (byte) 1 : 0);
            parcel.writeByte(r5.A09 ? (byte) 1 : 0);
            List list2 = r5.A06;
            int size2 = list2.size();
            parcel.writeInt(size2);
            for (int i3 = 0; i3 < size2; i3++) {
                AnonymousClass4M1 r1 = (AnonymousClass4M1) list2.get(i3);
                parcel.writeInt(r1.A00);
                parcel.writeLong(r1.A01);
            }
            parcel.writeLong(r5.A05);
            parcel.writeByte(r5.A07 ? (byte) 1 : 0);
            parcel.writeLong(r5.A03);
            parcel.writeInt(r5.A02);
            parcel.writeInt(r5.A00);
            parcel.writeInt(r5.A01);
        }
    }
}
