package X;

import android.view.View;

/* renamed from: X.0b2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0b2 implements AbstractC12080hL {
    @Override // X.AbstractC12080hL
    public int AFn(View view, int i) {
        View view2;
        if (view.getParent() instanceof View) {
            view2 = (View) view.getParent();
        } else {
            view2 = view;
        }
        return Math.min(view.getMeasuredHeight(), i - ((int) (((float) Math.min(view2.getWidth(), view2.getHeight())) / 1.7777778f)));
    }
}
