package X;

import com.whatsapp.jid.Jid;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.2Ks  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49432Ks extends AbstractC49422Kr {
    public static final AnonymousClass1W9[] A0I = new AnonymousClass1W9[0];
    public LinkedHashMap A00 = new LinkedHashMap();
    public LinkedHashMap A01 = new LinkedHashMap();
    public Map A02 = new HashMap();
    public final C15570nT A03;
    public final C15450nH A04;
    public final C14830m7 A05;
    public final AnonymousClass13N A06;
    public final C450720b A07;
    public final C20160vJ A08;
    public final C17230qT A09;
    public final C230810h A0A;
    public final AnonymousClass11H A0B;
    public final AnonymousClass1EX A0C;
    public final C22940zt A0D;
    public final AnonymousClass206 A0E;
    public final C18910tG A0F;
    public final C26601Ec A0G;
    public final C33991fP A0H;

    public C49432Ks(AbstractC15710nm r8, C15570nT r9, C15450nH r10, C14830m7 r11, C14850m9 r12, C16120oU r13, AnonymousClass13N r14, C450720b r15, C20160vJ r16, C17230qT r17, C230810h r18, AnonymousClass11H r19, AnonymousClass1EX r20, C22940zt r21, AnonymousClass206 r22, C18910tG r23, C26601Ec r24, C33991fP r25, Map map) {
        super(r8, r12, r13, r15, map);
        this.A05 = r11;
        this.A03 = r9;
        this.A04 = r10;
        this.A0C = r20;
        this.A0F = r23;
        this.A08 = r16;
        this.A06 = r14;
        this.A0D = r21;
        this.A09 = r17;
        this.A0A = r18;
        this.A0B = r19;
        this.A0G = r24;
        this.A07 = r15;
        this.A0E = r22;
        this.A0H = r25;
    }

    public static final int A00(String str) {
        switch (str.hashCode()) {
            case -1073880421:
                if (str.equals("missed")) {
                    return 2;
                }
                break;
            case -934710369:
                if (str.equals("reject")) {
                    return 4;
                }
                break;
            case -665462704:
                if (str.equals("unavailable")) {
                    return 3;
                }
                break;
            case -579210487:
                if (str.equals("connected")) {
                    return 5;
                }
                break;
            case -123173735:
                if (str.equals("canceled")) {
                    return 1;
                }
                break;
        }
        return 0;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v97, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:454:0x0ed7, code lost:
        if ("delete".equals(r3) != false) goto L_0x0ed9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:328:0x0a13  */
    /* JADX WARNING: Removed duplicated region for block: B:733:0x171c A[Catch: 1wi -> 0x17e3, TryCatch #2 {1wi -> 0x17e3, blocks: (B:88:0x0313, B:90:0x031f, B:91:0x0338, B:93:0x0340, B:95:0x034e, B:96:0x0366, B:98:0x0374, B:100:0x037e, B:101:0x0385, B:102:0x0389, B:104:0x0391, B:107:0x039e, B:109:0x03b1, B:111:0x03b9, B:113:0x03c2, B:115:0x03c8, B:117:0x03d2, B:118:0x03fa, B:119:0x040d, B:121:0x0419, B:122:0x042b, B:124:0x0431, B:127:0x047b, B:129:0x048d, B:130:0x048f, B:133:0x04d9, B:134:0x0500, B:135:0x0527, B:136:0x052c, B:137:0x0530, B:138:0x053b, B:140:0x0543, B:141:0x0553, B:143:0x0559, B:149:0x0591, B:151:0x0599, B:156:0x05ac, B:157:0x05b6, B:159:0x05cb, B:160:0x05d4, B:162:0x05dc, B:163:0x05f3, B:165:0x05fb, B:166:0x0612, B:168:0x061a, B:169:0x062a, B:171:0x0630, B:173:0x063e, B:174:0x0641, B:175:0x0644, B:176:0x064d, B:178:0x0655, B:179:0x0665, B:181:0x066b, B:184:0x0688, B:186:0x069b, B:188:0x06a1, B:189:0x06aa, B:191:0x06b2, B:192:0x06c7, B:194:0x06d3, B:196:0x06f0, B:198:0x06f4, B:199:0x06fb, B:200:0x0700, B:202:0x0706, B:204:0x070a, B:205:0x0711, B:206:0x0716, B:208:0x071c, B:211:0x0728, B:212:0x072c, B:213:0x0730, B:215:0x0738, B:216:0x0749, B:218:0x0752, B:219:0x075f, B:221:0x0768, B:223:0x0771, B:224:0x07a3, B:226:0x07ab, B:228:0x07b9, B:230:0x07bf, B:231:0x07df, B:233:0x07e7, B:236:0x07f5, B:237:0x07fe, B:239:0x0806, B:242:0x0812, B:243:0x081b, B:245:0x0823, B:248:0x0831, B:250:0x0839, B:252:0x0845, B:255:0x0855, B:257:0x085e, B:262:0x086f, B:263:0x0878, B:264:0x0880, B:266:0x0889, B:271:0x089a, B:272:0x08a3, B:273:0x08aa, B:274:0x08bd, B:276:0x08c6, B:279:0x08d4, B:280:0x08d9, B:282:0x08e2, B:285:0x08ef, B:287:0x08f8, B:290:0x0905, B:292:0x090d, B:293:0x0916, B:295:0x091f, B:297:0x0929, B:298:0x0930, B:299:0x0934, B:301:0x093e, B:302:0x0945, B:309:0x095b, B:311:0x0963, B:312:0x096c, B:314:0x0975, B:315:0x097e, B:317:0x0986, B:320:0x0994, B:321:0x09a0, B:322:0x09ac, B:323:0x09b5, B:324:0x09be, B:325:0x0a07, B:331:0x0a1b, B:336:0x0a5a, B:337:0x0a67, B:339:0x0a6d, B:343:0x0a8d, B:344:0x0a97, B:345:0x0a9e, B:346:0x0ad2, B:348:0x0ada, B:350:0x0af7, B:352:0x0afd, B:353:0x0b17, B:355:0x0b1f, B:358:0x0b27, B:359:0x0b2c, B:361:0x0b36, B:363:0x0b4c, B:365:0x0b63, B:367:0x0b67, B:369:0x0b6f, B:370:0x0b71, B:372:0x0b79, B:373:0x0b80, B:374:0x0b81, B:376:0x0b87, B:377:0x0b91, B:378:0x0b92, B:379:0x0b99, B:380:0x0b9a, B:382:0x0bb1, B:384:0x0bb9, B:385:0x0bc0, B:388:0x0bc7, B:390:0x0bcf, B:391:0x0bf4, B:393:0x0bfc, B:395:0x0c4d, B:396:0x0c56, B:398:0x0c5c, B:399:0x0c7a, B:400:0x0cae, B:402:0x0cb9, B:404:0x0cbf, B:405:0x0cd9, B:407:0x0ce1, B:408:0x0d24, B:410:0x0d2a, B:411:0x0d3a, B:413:0x0d45, B:414:0x0d54, B:416:0x0d5c, B:417:0x0d6b, B:419:0x0d73, B:420:0x0d82, B:422:0x0d8a, B:423:0x0d94, B:425:0x0d9c, B:427:0x0dac, B:428:0x0dae, B:430:0x0dbc, B:431:0x0dbe, B:433:0x0dc4, B:434:0x0dcd, B:435:0x0dd5, B:437:0x0ddd, B:438:0x0e0b, B:439:0x0e30, B:441:0x0e3f, B:443:0x0e61, B:444:0x0e66, B:445:0x0e8d, B:447:0x0e95, B:449:0x0eba, B:452:0x0eca, B:453:0x0ed3, B:455:0x0ed9, B:456:0x0efe, B:458:0x0f08, B:460:0x0f21, B:462:0x0f2a, B:464:0x0f33, B:466:0x0f3b, B:468:0x0f43, B:470:0x0f4b, B:472:0x0f57, B:473:0x0f59, B:474:0x0f7e, B:476:0x0f89, B:478:0x0f8f, B:479:0x0fb2, B:480:0x0fea, B:482:0x0ff2, B:483:0x102e, B:485:0x103b, B:487:0x1057, B:488:0x105c, B:493:0x1069, B:495:0x1075, B:496:0x1078, B:499:0x109b, B:501:0x10a4, B:504:0x10af, B:506:0x10bb, B:507:0x10be, B:513:0x10e4, B:514:0x10f7, B:516:0x1100, B:517:0x110b, B:520:0x1113, B:521:0x1117, B:525:0x1122, B:528:0x112d, B:529:0x1131, B:530:0x1134, B:531:0x113b, B:534:0x1143, B:536:0x1151, B:538:0x115b, B:541:0x1161, B:542:0x1172, B:544:0x117c, B:548:0x1184, B:549:0x1195, B:552:0x119d, B:554:0x11a7, B:555:0x11b1, B:558:0x11bb, B:561:0x11cf, B:563:0x11da, B:564:0x11df, B:567:0x11e7, B:569:0x11ef, B:572:0x11f9, B:574:0x1206, B:575:0x121f, B:577:0x1223, B:579:0x122b, B:581:0x123e, B:583:0x1248, B:584:0x1273, B:586:0x1279, B:588:0x1283, B:589:0x12a9, B:591:0x12b3, B:593:0x12bd, B:594:0x12e1, B:596:0x12e9, B:598:0x12f1, B:600:0x12fa, B:603:0x1304, B:604:0x1322, B:606:0x132a, B:607:0x133e, B:609:0x1344, B:611:0x1352, B:612:0x1356, B:613:0x137c, B:615:0x1384, B:617:0x1398, B:618:0x13c0, B:620:0x13c9, B:621:0x13f0, B:622:0x1408, B:623:0x1427, B:624:0x144f, B:626:0x1453, B:628:0x145d, B:630:0x146c, B:633:0x147a, B:635:0x1484, B:637:0x1492, B:638:0x149b, B:639:0x14a2, B:640:0x14a5, B:642:0x14ae, B:644:0x14ba, B:646:0x14ca, B:647:0x14d4, B:648:0x14d6, B:649:0x1508, B:651:0x150c, B:654:0x1528, B:656:0x1532, B:658:0x153a, B:660:0x1542, B:661:0x1565, B:663:0x156f, B:665:0x1579, B:666:0x1589, B:668:0x1591, B:669:0x1596, B:670:0x1599, B:671:0x15c0, B:673:0x15c8, B:674:0x15f0, B:676:0x15f8, B:677:0x1620, B:679:0x1628, B:682:0x1630, B:683:0x1635, B:685:0x163f, B:687:0x164b, B:689:0x1651, B:691:0x1655, B:693:0x1658, B:695:0x165e, B:697:0x1669, B:699:0x1672, B:701:0x167a, B:703:0x1684, B:704:0x168a, B:705:0x1692, B:706:0x169a, B:708:0x16a6, B:710:0x16b1, B:714:0x16b7, B:716:0x16bb, B:718:0x16c6, B:720:0x16cc, B:721:0x16d6, B:722:0x16df, B:723:0x16ea, B:724:0x16f1, B:725:0x16f4, B:726:0x16f7, B:727:0x16fd, B:729:0x1701, B:731:0x170a, B:733:0x171c, B:734:0x1743, B:735:0x174a, B:736:0x174b, B:737:0x177f, B:739:0x178d, B:740:0x17df), top: B:760:0x0313, inners: #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:734:0x1743 A[Catch: 1wi -> 0x17e3, TryCatch #2 {1wi -> 0x17e3, blocks: (B:88:0x0313, B:90:0x031f, B:91:0x0338, B:93:0x0340, B:95:0x034e, B:96:0x0366, B:98:0x0374, B:100:0x037e, B:101:0x0385, B:102:0x0389, B:104:0x0391, B:107:0x039e, B:109:0x03b1, B:111:0x03b9, B:113:0x03c2, B:115:0x03c8, B:117:0x03d2, B:118:0x03fa, B:119:0x040d, B:121:0x0419, B:122:0x042b, B:124:0x0431, B:127:0x047b, B:129:0x048d, B:130:0x048f, B:133:0x04d9, B:134:0x0500, B:135:0x0527, B:136:0x052c, B:137:0x0530, B:138:0x053b, B:140:0x0543, B:141:0x0553, B:143:0x0559, B:149:0x0591, B:151:0x0599, B:156:0x05ac, B:157:0x05b6, B:159:0x05cb, B:160:0x05d4, B:162:0x05dc, B:163:0x05f3, B:165:0x05fb, B:166:0x0612, B:168:0x061a, B:169:0x062a, B:171:0x0630, B:173:0x063e, B:174:0x0641, B:175:0x0644, B:176:0x064d, B:178:0x0655, B:179:0x0665, B:181:0x066b, B:184:0x0688, B:186:0x069b, B:188:0x06a1, B:189:0x06aa, B:191:0x06b2, B:192:0x06c7, B:194:0x06d3, B:196:0x06f0, B:198:0x06f4, B:199:0x06fb, B:200:0x0700, B:202:0x0706, B:204:0x070a, B:205:0x0711, B:206:0x0716, B:208:0x071c, B:211:0x0728, B:212:0x072c, B:213:0x0730, B:215:0x0738, B:216:0x0749, B:218:0x0752, B:219:0x075f, B:221:0x0768, B:223:0x0771, B:224:0x07a3, B:226:0x07ab, B:228:0x07b9, B:230:0x07bf, B:231:0x07df, B:233:0x07e7, B:236:0x07f5, B:237:0x07fe, B:239:0x0806, B:242:0x0812, B:243:0x081b, B:245:0x0823, B:248:0x0831, B:250:0x0839, B:252:0x0845, B:255:0x0855, B:257:0x085e, B:262:0x086f, B:263:0x0878, B:264:0x0880, B:266:0x0889, B:271:0x089a, B:272:0x08a3, B:273:0x08aa, B:274:0x08bd, B:276:0x08c6, B:279:0x08d4, B:280:0x08d9, B:282:0x08e2, B:285:0x08ef, B:287:0x08f8, B:290:0x0905, B:292:0x090d, B:293:0x0916, B:295:0x091f, B:297:0x0929, B:298:0x0930, B:299:0x0934, B:301:0x093e, B:302:0x0945, B:309:0x095b, B:311:0x0963, B:312:0x096c, B:314:0x0975, B:315:0x097e, B:317:0x0986, B:320:0x0994, B:321:0x09a0, B:322:0x09ac, B:323:0x09b5, B:324:0x09be, B:325:0x0a07, B:331:0x0a1b, B:336:0x0a5a, B:337:0x0a67, B:339:0x0a6d, B:343:0x0a8d, B:344:0x0a97, B:345:0x0a9e, B:346:0x0ad2, B:348:0x0ada, B:350:0x0af7, B:352:0x0afd, B:353:0x0b17, B:355:0x0b1f, B:358:0x0b27, B:359:0x0b2c, B:361:0x0b36, B:363:0x0b4c, B:365:0x0b63, B:367:0x0b67, B:369:0x0b6f, B:370:0x0b71, B:372:0x0b79, B:373:0x0b80, B:374:0x0b81, B:376:0x0b87, B:377:0x0b91, B:378:0x0b92, B:379:0x0b99, B:380:0x0b9a, B:382:0x0bb1, B:384:0x0bb9, B:385:0x0bc0, B:388:0x0bc7, B:390:0x0bcf, B:391:0x0bf4, B:393:0x0bfc, B:395:0x0c4d, B:396:0x0c56, B:398:0x0c5c, B:399:0x0c7a, B:400:0x0cae, B:402:0x0cb9, B:404:0x0cbf, B:405:0x0cd9, B:407:0x0ce1, B:408:0x0d24, B:410:0x0d2a, B:411:0x0d3a, B:413:0x0d45, B:414:0x0d54, B:416:0x0d5c, B:417:0x0d6b, B:419:0x0d73, B:420:0x0d82, B:422:0x0d8a, B:423:0x0d94, B:425:0x0d9c, B:427:0x0dac, B:428:0x0dae, B:430:0x0dbc, B:431:0x0dbe, B:433:0x0dc4, B:434:0x0dcd, B:435:0x0dd5, B:437:0x0ddd, B:438:0x0e0b, B:439:0x0e30, B:441:0x0e3f, B:443:0x0e61, B:444:0x0e66, B:445:0x0e8d, B:447:0x0e95, B:449:0x0eba, B:452:0x0eca, B:453:0x0ed3, B:455:0x0ed9, B:456:0x0efe, B:458:0x0f08, B:460:0x0f21, B:462:0x0f2a, B:464:0x0f33, B:466:0x0f3b, B:468:0x0f43, B:470:0x0f4b, B:472:0x0f57, B:473:0x0f59, B:474:0x0f7e, B:476:0x0f89, B:478:0x0f8f, B:479:0x0fb2, B:480:0x0fea, B:482:0x0ff2, B:483:0x102e, B:485:0x103b, B:487:0x1057, B:488:0x105c, B:493:0x1069, B:495:0x1075, B:496:0x1078, B:499:0x109b, B:501:0x10a4, B:504:0x10af, B:506:0x10bb, B:507:0x10be, B:513:0x10e4, B:514:0x10f7, B:516:0x1100, B:517:0x110b, B:520:0x1113, B:521:0x1117, B:525:0x1122, B:528:0x112d, B:529:0x1131, B:530:0x1134, B:531:0x113b, B:534:0x1143, B:536:0x1151, B:538:0x115b, B:541:0x1161, B:542:0x1172, B:544:0x117c, B:548:0x1184, B:549:0x1195, B:552:0x119d, B:554:0x11a7, B:555:0x11b1, B:558:0x11bb, B:561:0x11cf, B:563:0x11da, B:564:0x11df, B:567:0x11e7, B:569:0x11ef, B:572:0x11f9, B:574:0x1206, B:575:0x121f, B:577:0x1223, B:579:0x122b, B:581:0x123e, B:583:0x1248, B:584:0x1273, B:586:0x1279, B:588:0x1283, B:589:0x12a9, B:591:0x12b3, B:593:0x12bd, B:594:0x12e1, B:596:0x12e9, B:598:0x12f1, B:600:0x12fa, B:603:0x1304, B:604:0x1322, B:606:0x132a, B:607:0x133e, B:609:0x1344, B:611:0x1352, B:612:0x1356, B:613:0x137c, B:615:0x1384, B:617:0x1398, B:618:0x13c0, B:620:0x13c9, B:621:0x13f0, B:622:0x1408, B:623:0x1427, B:624:0x144f, B:626:0x1453, B:628:0x145d, B:630:0x146c, B:633:0x147a, B:635:0x1484, B:637:0x1492, B:638:0x149b, B:639:0x14a2, B:640:0x14a5, B:642:0x14ae, B:644:0x14ba, B:646:0x14ca, B:647:0x14d4, B:648:0x14d6, B:649:0x1508, B:651:0x150c, B:654:0x1528, B:656:0x1532, B:658:0x153a, B:660:0x1542, B:661:0x1565, B:663:0x156f, B:665:0x1579, B:666:0x1589, B:668:0x1591, B:669:0x1596, B:670:0x1599, B:671:0x15c0, B:673:0x15c8, B:674:0x15f0, B:676:0x15f8, B:677:0x1620, B:679:0x1628, B:682:0x1630, B:683:0x1635, B:685:0x163f, B:687:0x164b, B:689:0x1651, B:691:0x1655, B:693:0x1658, B:695:0x165e, B:697:0x1669, B:699:0x1672, B:701:0x167a, B:703:0x1684, B:704:0x168a, B:705:0x1692, B:706:0x169a, B:708:0x16a6, B:710:0x16b1, B:714:0x16b7, B:716:0x16bb, B:718:0x16c6, B:720:0x16cc, B:721:0x16d6, B:722:0x16df, B:723:0x16ea, B:724:0x16f1, B:725:0x16f4, B:726:0x16f7, B:727:0x16fd, B:729:0x1701, B:731:0x170a, B:733:0x171c, B:734:0x1743, B:735:0x174a, B:736:0x174b, B:737:0x177f, B:739:0x178d, B:740:0x17df), top: B:760:0x0313, inners: #3 }] */
    @Override // X.AbstractC49422Kr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass1V8 r65) {
        /*
        // Method dump skipped, instructions count: 6193
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49432Ks.A01(X.1V8):void");
    }

    public final AnonymousClass1OT A02(AnonymousClass1V8 r6, long j) {
        AbstractC15710nm r1 = super.A01;
        Jid A0A = r6.A0A(r1, Jid.class, "participant");
        AnonymousClass1VH r2 = new AnonymousClass1VH();
        r2.A01 = r6.A0A(r1, Jid.class, "from");
        r2.A05 = "notification";
        r2.A07 = r6.A0I("id", null);
        r2.A08 = r6.A0I("type", null);
        r2.A02 = A0A;
        r2.A00 = j;
        return r2.A00();
    }
}
