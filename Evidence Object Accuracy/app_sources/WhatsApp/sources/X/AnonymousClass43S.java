package X;

/* renamed from: X.43S  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43S extends AbstractC16110oT {
    public Long A00;
    public String A01;
    public String A02;

    public AnonymousClass43S() {
        super(2052, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(2, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidPerfTimer {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "androidPerfDuration", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "androidPerfExtraData", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "androidPerfName", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
