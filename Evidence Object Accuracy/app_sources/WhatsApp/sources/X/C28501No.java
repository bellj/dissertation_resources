package X;

/* renamed from: X.1No  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28501No extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public String A0I;

    public C28501No() {
        super(1006, new AnonymousClass00E(1, 20, 100), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(20, this.A05);
        r3.Abe(10, this.A06);
        r3.Abe(19, this.A07);
        r3.Abe(22, this.A08);
        r3.Abe(14, this.A09);
        r3.Abe(16, this.A0A);
        r3.Abe(17, this.A0B);
        r3.Abe(12, this.A00);
        r3.Abe(21, this.A0C);
        r3.Abe(6, this.A01);
        r3.Abe(5, this.A02);
        r3.Abe(15, this.A0D);
        r3.Abe(7, this.A0E);
        r3.Abe(8, this.A03);
        r3.Abe(11, this.A0F);
        r3.Abe(13, this.A0G);
        r3.Abe(18, this.A0H);
        r3.Abe(9, this.A04);
        r3.Abe(1, this.A0I);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamContactSyncEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncBusinessResponseNew", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncChangedVersionRowCount", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncDeviceResponseNew", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncDisappearingModeResponseNew", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncErrorCode", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncFailureProtocol", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncLatency", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncNoop", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncPayResponseNew", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncRequestClearWaSyncData", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncRequestIsUrgent", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncRequestProtocol", this.A0D);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncRequestRetryCount", this.A0E);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncRequestShouldRetry", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncRequestedCount", this.A0F);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncResponseCount", this.A0G);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncStatusResponseNew", this.A0H);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncSuccess", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "contactSyncType", this.A0I);
        sb.append("}");
        return sb.toString();
    }
}
