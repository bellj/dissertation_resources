package X;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;

/* renamed from: X.2Sq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51052Sq implements AnonymousClass005 {
    public final AnonymousClass01E A00;
    public final Object A01 = new Object();
    public volatile Object A02;

    public C51052Sq(AnonymousClass01E r2) {
        this.A00 = r2;
    }

    public static final Context A00(Context context) {
        while ((context instanceof ContextWrapper) && !(context instanceof Activity)) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        return context;
    }

    public static C51052Sq A01(AnonymousClass01E r1) {
        return new C51052Sq(r1);
    }

    @Override // X.AnonymousClass005
    public Object generatedComponent() {
        ActivityC000900k r6;
        ActivityC000900k r1;
        if (this.A02 == null) {
            synchronized (this.A01) {
                if (this.A02 == null) {
                    AnonymousClass01E r5 = this.A00;
                    AnonymousClass05V r0 = r5.A0F;
                    if (r0 == null) {
                        r6 = null;
                    } else {
                        r6 = r0.A04;
                    }
                    if (r6 != null) {
                        AnonymousClass2PX.A00("Hilt Fragments must be attached to an @AndroidEntryPoint Activity. Found: %s", new Object[]{r6.getClass()}, r6 instanceof AnonymousClass005);
                        AnonymousClass05V r02 = r5.A0F;
                        if (r02 == null) {
                            r1 = null;
                        } else {
                            r1 = r02.A04;
                        }
                        AnonymousClass2FL r03 = (AnonymousClass2FL) ((AnonymousClass2FJ) AnonymousClass027.A00(AnonymousClass2FJ.class, r1));
                        AnonymousClass4RI r04 = new AnonymousClass4RI(r03.A1C, r03.A1D, r03.A1E);
                        r04.A00 = r5;
                        this.A02 = new C51112Sw(r04.A01, r04.A02, r04.A03);
                    } else {
                        throw new NullPointerException("Hilt Fragments must be attached before creating the component.");
                    }
                }
            }
        }
        return this.A02;
    }
}
