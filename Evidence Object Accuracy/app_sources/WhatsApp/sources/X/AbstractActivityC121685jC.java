package X;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.facebook.redex.IDxObserverShape5S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.BrazilOrderDetailsActivity;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.BrazilSmbPaymentActivity;
import com.whatsapp.payments.ui.NoviAmountEntryActivity;
import com.whatsapp.payments.ui.NoviSharedPaymentActivity;
import com.whatsapp.payments.ui.PaymentCheckoutOrderDetailsPresenter$$ExternalSyntheticLambda0;
import com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity;
import com.whatsapp.payments.ui.orderdetails.PaymentCheckoutOrderDetailsViewV2;
import com.whatsapp.payments.ui.widget.PaymentView;
import com.whatsapp.picker.search.PickerSearchDialogFragment;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5jC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121685jC extends AbstractActivityC119235dO implements AbstractC1310861e {
    public int A00;
    public int A01 = 6;
    public long A02;
    public Bundle A03;
    public C14580lf A04;
    public C25841Ba A05;
    public C20730wE A06;
    public C16590pI A07;
    public C20830wO A08;
    public C15650ng A09;
    public C15370n3 A0A;
    public AnonymousClass1ZO A0B;
    public C30921Zi A0C;
    public C18050rp A0D;
    public AbstractC14640lm A0E;
    public UserJid A0F;
    public UserJid A0G;
    public C17220qS A0H;
    public C21860y6 A0I;
    public C124105oc A0J;
    public C18650sn A0K;
    public AbstractC17860rW A0L;
    public C18610sj A0M;
    public C17900ra A0N;
    public C22710zW A0O;
    public C17070qD A0P;
    public C129135xE A0Q;
    public C129065x7 A0R;
    public C20350vc A0S;
    public AnonymousClass17Z A0T;
    public AnonymousClass2S0 A0U;
    public AnonymousClass61P A0V;
    public C129175xI A0W;
    public C118025b9 A0X;
    public C129395xe A0Y;
    public C21190x1 A0Z;
    public C69893aP A0a;
    public C20320vZ A0b;
    public AnonymousClass1KS A0c;
    public AnonymousClass1AB A0d;
    public Integer A0e;
    public String A0f;
    public String A0g;
    public String A0h;
    public String A0i;
    public String A0j;
    public String A0k;
    public String A0l;
    public String A0m;
    public String A0n;
    public String A0o;
    public String A0p;
    public List A0q;
    public boolean A0r;
    public boolean A0s;
    public boolean A0t;
    public boolean A0u;

    public static void A1o(ActivityC13790kL r5, AbstractC16870pt r6, AnonymousClass2S0 r7, int i) {
        AnonymousClass61I.A01(AnonymousClass61I.A00(r5.A05, null, r7, null, true), r6, Integer.valueOf(i), "new_payment", null, 1);
    }

    public static boolean A1p(AbstractActivityC121685jC r1) {
        return "p2m".equals(r1.A0o);
    }

    public PaymentView A2e() {
        if (this instanceof NoviSharedPaymentActivity) {
            return ((NoviSharedPaymentActivity) this).A0J;
        }
        if (this instanceof NoviAmountEntryActivity) {
            return ((NoviAmountEntryActivity) this).A07;
        }
        if (!(this instanceof AbstractActivityC121665jA)) {
            return ((BrazilPaymentActivity) this).A0V;
        }
        AbstractActivityC121665jA r1 = (AbstractActivityC121665jA) this;
        if (!(r1 instanceof AbstractActivityC121525iS)) {
            return null;
        }
        return ((AbstractActivityC121525iS) r1).A0W;
    }

    public C28861Ph A2f(String str, List list) {
        AbstractC15340mz r6;
        UserJid userJid;
        C20320vZ r3 = this.A0b;
        AbstractC14640lm r5 = this.A0E;
        AnonymousClass009.A05(r5);
        long j = this.A02;
        if (j != 0) {
            r6 = this.A09.A0K.A00(j);
        } else {
            r6 = null;
        }
        C28861Ph A03 = r3.A03(null, r5, r6, str, list, 0, false);
        if (C15380n4.A0J(this.A0E) && (userJid = this.A0G) != null) {
            A03.A0e(userJid);
        }
        return A03;
    }

    public void A2g(int i) {
        Intent A0g;
        boolean z = this instanceof BrazilSmbPaymentActivity;
        AbstractC14640lm r1 = this.A0E;
        if (!z) {
            if (r1 != null) {
                A0g = new C14960mK().A0g(this, this.A08.A01(r1));
                C35741ib.A00(A0g, "BasePaymentsActivity");
                A0g.putExtra("show_keyboard", false);
                A0g.putExtra("start_t", SystemClock.uptimeMillis());
                this.A0Z.A00();
                A2D(A0g);
            }
        } else if (r1 != null) {
            A0g = new C14960mK().A0g(this, this.A08.A01(r1));
            C35741ib.A00(A0g, "BrazilSmbPaymentActivity");
            A0g.putExtra("show_keyboard", false);
            A0g.putExtra("start_t", SystemClock.uptimeMillis());
            if (i == 1) {
                A0g.putExtra("extra_merchant_upsell_enabled", true);
            }
            this.A0Z.A00();
            A2D(A0g);
        }
        finish();
    }

    public void A2h(Bundle bundle) {
        AnonymousClass1ZO A0F;
        if (this instanceof NoviSharedPaymentActivity) {
            NoviSharedPaymentActivity noviSharedPaymentActivity = (NoviSharedPaymentActivity) this;
            AbstractC005102i A1U = noviSharedPaymentActivity.A1U();
            if (A1U != null) {
                C117295Zj.A0g(noviSharedPaymentActivity, A1U, R.string.novi_payments_send_money);
            }
            noviSharedPaymentActivity.A0D.A0B(new AbstractC136286Ly() { // from class: X.6BI
                @Override // X.AbstractC136286Ly
                public final void AT6(C1315463e r7) {
                    AnonymousClass63Y r0;
                    NoviSharedPaymentActivity noviSharedPaymentActivity2 = NoviSharedPaymentActivity.this;
                    if (r7 == null || (r0 = r7.A00) == null) {
                        noviSharedPaymentActivity2.A03 = C17930rd.A00(C17930rd.A01(AnonymousClass1ZT.A01(C248917h.A04(((ActivityC13790kL) noviSharedPaymentActivity2).A01.A03()))).A03).A02;
                        C17930rd A01 = ((AbstractActivityC121685jC) noviSharedPaymentActivity2).A0N.A01();
                        AnonymousClass009.A05(A01);
                        Iterator it = A01.A05.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            AbstractC30791Yv r3 = (AbstractC30791Yv) it.next();
                            AbstractC30781Yu r2 = (AbstractC30781Yu) r3;
                            if (!C117305Zk.A1V(noviSharedPaymentActivity2.A03, r2.A04) && r2.A00 == 0) {
                                noviSharedPaymentActivity2.A03 = r3;
                                break;
                            }
                        }
                    } else {
                        noviSharedPaymentActivity2.A03 = r0.A00;
                    }
                    ((ActivityC13810kN) noviSharedPaymentActivity2).A05.A0H(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0013: INVOKE  
                          (wrap: X.0mE : 0x000c: IGET  (r1v1 X.0mE A[REMOVE]) = (wrap: ?? : ?: CAST (X.0kN) (r5v0 'noviSharedPaymentActivity2' com.whatsapp.payments.ui.NoviSharedPaymentActivity)) X.0kN.A05 X.0mE)
                          (wrap: X.6KD : 0x0010: CONSTRUCTOR  (r0v15 X.6KD A[REMOVE]) = (r5v0 'noviSharedPaymentActivity2' com.whatsapp.payments.ui.NoviSharedPaymentActivity) call: X.6KD.<init>(com.whatsapp.payments.ui.NoviSharedPaymentActivity):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6BI.AT6(X.63e):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0010: CONSTRUCTOR  (r0v15 X.6KD A[REMOVE]) = (r5v0 'noviSharedPaymentActivity2' com.whatsapp.payments.ui.NoviSharedPaymentActivity) call: X.6KD.<init>(com.whatsapp.payments.ui.NoviSharedPaymentActivity):void type: CONSTRUCTOR in method: X.6BI.AT6(X.63e):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6KD, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                        this = this;
                        com.whatsapp.payments.ui.NoviSharedPaymentActivity r5 = com.whatsapp.payments.ui.NoviSharedPaymentActivity.this
                        if (r7 == 0) goto L_0x0017
                        X.63Y r0 = r7.A00
                        if (r0 == 0) goto L_0x0017
                        X.1Yv r0 = r0.A00
                        r5.A03 = r0
                    L_0x000c:
                        X.0mE r1 = r5.A05
                        X.6KD r0 = new X.6KD
                        r0.<init>(r5)
                        r1.A0H(r0)
                        return
                    L_0x0017:
                        X.0nT r0 = r5.A01
                        com.whatsapp.jid.UserJid r0 = r0.A03()
                        java.lang.String r0 = X.C248917h.A04(r0)
                        java.lang.String r0 = X.AnonymousClass1ZT.A01(r0)
                        X.0rd r0 = X.C17930rd.A01(r0)
                        java.lang.String r0 = r0.A03
                        X.0rd r0 = X.C17930rd.A00(r0)
                        X.1Yv r0 = r0.A02
                        r5.A03 = r0
                        X.0ra r0 = r5.A0N
                        X.0rd r0 = r0.A01()
                        X.AnonymousClass009.A05(r0)
                        java.util.LinkedHashSet r0 = r0.A05
                        java.util.Iterator r4 = r0.iterator()
                    L_0x0042:
                        boolean r0 = r4.hasNext()
                        if (r0 == 0) goto L_0x000c
                        java.lang.Object r3 = r4.next()
                        X.1Yv r3 = (X.AbstractC30791Yv) r3
                        r2 = r3
                        X.1Yu r2 = (X.AbstractC30781Yu) r2
                        java.lang.String r1 = r2.A04
                        X.1Yv r0 = r5.A03
                        boolean r0 = X.C117305Zk.A1V(r0, r1)
                        if (r0 != 0) goto L_0x0042
                        int r0 = r2.A00
                        if (r0 != 0) goto L_0x0042
                        r5.A03 = r3
                        goto L_0x000c
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6BI.AT6(X.63e):void");
                }
            });
        } else if (this instanceof NoviAmountEntryActivity) {
            NoviAmountEntryActivity noviAmountEntryActivity = (NoviAmountEntryActivity) this;
            C130305zC.A01(noviAmountEntryActivity, ((ActivityC13830kP) noviAmountEntryActivity).A01, C117305Zk.A0a(noviAmountEntryActivity), noviAmountEntryActivity.A0A, R.drawable.ic_back, false);
            PaymentView paymentView = (PaymentView) noviAmountEntryActivity.findViewById(R.id.payment_view);
            noviAmountEntryActivity.A07 = paymentView;
            AbstractC128555wI r5 = noviAmountEntryActivity.A03;
            C134096Dh r6 = new C134096Dh();
            AbstractC118095bG r4 = noviAmountEntryActivity.A05;
            C134106Di r7 = noviAmountEntryActivity.A04;
            AnonymousClass01d r3 = ((ActivityC13810kN) noviAmountEntryActivity).A08;
            paymentView.A0C(noviAmountEntryActivity);
            paymentView.setPaymentContactContainerVisibility(8);
            paymentView.A0E(r6, R.id.payment_service_container_stub, R.id.payment_service_container_inflated);
            r6.A01.setVisibility(0);
            r4.A07(noviAmountEntryActivity);
            r4.A0O.A05(noviAmountEntryActivity, C117305Zk.A0B(r6, 9));
            C134156Dn r62 = new C134156Dn();
            paymentView.A0E(r62, R.id.payment_confirm_button_container, R.id.payment_confirm_button_container_inflated);
            r4.A0Q.A05(noviAmountEntryActivity, C117305Zk.A0B(r62, 8));
            r4.A0P.A05(noviAmountEntryActivity, C117305Zk.A0B(r7, 10));
            r4.A07.A05(noviAmountEntryActivity, C117305Zk.A0B(paymentView, 12));
            r4.A06.A05(noviAmountEntryActivity, new IDxObserverShape5S0200000_3_I1(paymentView, 0, r3));
            r4.A0B.A05(noviAmountEntryActivity, C117305Zk.A0B(noviAmountEntryActivity, 6));
            r4.A0C.A05(noviAmountEntryActivity, C117305Zk.A0B(noviAmountEntryActivity, 4));
            r4.A09.A05(noviAmountEntryActivity, C117305Zk.A0B(noviAmountEntryActivity, 7));
            r4.A0E.A05(noviAmountEntryActivity, C117305Zk.A0B(paymentView, 11));
            if (!(r5 instanceof C121425hu)) {
                C123475nD r2 = (C123475nD) r4;
                r2.A08.A05(noviAmountEntryActivity, C117305Zk.A0B(noviAmountEntryActivity, 26));
                r2.A0A.A05(noviAmountEntryActivity, C117305Zk.A0B(noviAmountEntryActivity, 24));
                r2.A03.A05(noviAmountEntryActivity, C117305Zk.A0B(noviAmountEntryActivity, 25));
            } else {
                C121425hu r63 = (C121425hu) r5;
                C123465nC r22 = (C123465nC) r4;
                if (r63.A02 == 1) {
                    C1316163l r0 = r63.A01;
                    AnonymousClass009.A05(r0);
                    r22.A00 = 1;
                    r22.A02 = r0;
                } else {
                    C30861Zc r1 = r63.A00;
                    AnonymousClass009.A05(r1);
                    r22.A00 = 2;
                    r22.A01 = r1;
                }
                r7.A00 = new View.OnClickListener(noviAmountEntryActivity, r63, r22) { // from class: X.64L
                    public final /* synthetic */ NoviAmountEntryActivity A00;
                    public final /* synthetic */ C121425hu A01;
                    public final /* synthetic */ C123465nC A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        String str;
                        C130075yl r8;
                        AnonymousClass016 A0T;
                        C130155yt r72;
                        C1310460z A0F2;
                        int i;
                        C121425hu r02 = this.A01;
                        C123465nC r52 = this.A02;
                        NoviAmountEntryActivity noviAmountEntryActivity2 = this.A00;
                        AnonymousClass60Y r73 = r02.A03;
                        if (r02.A02 == 1) {
                            str = "WITHDRAW_FULL_CASH_BAL_CLICK";
                        } else {
                            str = "WITHDRAW_FULL_BAL_CLICK";
                        }
                        AnonymousClass60Y.A02(r73, str, "WITHDRAW_MONEY", "ENTER_AMOUNT", "LINK");
                        int i2 = r52.A00;
                        if (i2 == 1) {
                            C117315Zl.A0Q(r52.A09);
                            r8 = r52.A03;
                            C1316163l r03 = r52.A02;
                            AnonymousClass009.A05(r03);
                            String str2 = r03.A04;
                            A0T = C12980iv.A0T();
                            r72 = r8.A05;
                            AnonymousClass61S[] r23 = new AnonymousClass61S[2];
                            AnonymousClass61S.A04("action", "novi-get-cash-withdrawal-full-balance-quote", r23);
                            A0F2 = C117295Zj.A0F(AnonymousClass61S.A00("store_id", str2), r23, 1);
                            i = 17;
                        } else if (i2 == 2) {
                            C117315Zl.A0Q(r52.A09);
                            r8 = r52.A03;
                            C30861Zc r32 = r52.A01;
                            AnonymousClass009.A05(r32);
                            A0T = C12980iv.A0T();
                            r72 = r8.A05;
                            AnonymousClass61S[] r24 = new AnonymousClass61S[2];
                            AnonymousClass61S.A04("action", "novi-get-fi-withdrawal-full-balance-quote", r24);
                            A0F2 = C117295Zj.A0F(AnonymousClass61S.A00("credential_id", r32.A0A), r24, 1);
                            i = 16;
                        } else {
                            r52.A09.A0B(Boolean.FALSE);
                            return;
                        }
                        C130155yt.A00(C117305Zk.A09(A0T, r8, i), r72, A0F2);
                        C117295Zj.A0s(noviAmountEntryActivity2, A0T, r52, 159);
                    }
                };
                r22.A08.A05(noviAmountEntryActivity, new IDxObserverShape5S0200000_3_I1(noviAmountEntryActivity, 3, r63));
            }
            r5.A00 = r3;
            r4.A0R.A05(noviAmountEntryActivity, C117305Zk.A0B(noviAmountEntryActivity, 5));
        } else if (!(this instanceof AbstractActivityC121665jA)) {
            BrazilPaymentActivity brazilPaymentActivity = (BrazilPaymentActivity) this;
            if (!(brazilPaymentActivity instanceof BrazilOrderDetailsActivity)) {
                AbstractC005102i A1U2 = brazilPaymentActivity.A1U();
                if (A1U2 != null) {
                    Context context = brazilPaymentActivity.A01;
                    boolean z = brazilPaymentActivity.A0s;
                    int i = R.string.new_payment;
                    if (z) {
                        i = R.string.payments_send_money;
                    }
                    A1U2.A0I(context.getString(i));
                    A1U2.A0M(true);
                    if (!brazilPaymentActivity.A0s) {
                        A1U2.A07(0.0f);
                    }
                }
                brazilPaymentActivity.setContentView(R.layout.send_payment_screen);
                PaymentView paymentView2 = (PaymentView) brazilPaymentActivity.findViewById(R.id.payment_view);
                brazilPaymentActivity.A0V = paymentView2;
                paymentView2.A0C(brazilPaymentActivity);
                Intent intent = brazilPaymentActivity.getIntent();
                if (intent != null) {
                    brazilPaymentActivity.A0Y = intent.getStringExtra("referral_screen");
                }
                C20830wO r23 = ((AbstractActivityC121685jC) brazilPaymentActivity).A08;
                UserJid userJid = ((AbstractActivityC121685jC) brazilPaymentActivity).A0G;
                AnonymousClass009.A05(userJid);
                ((AbstractActivityC121685jC) brazilPaymentActivity).A0A = r23.A01(userJid);
                C17070qD r12 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0P;
                r12.A03();
                AnonymousClass1ZO A05 = r12.A09.A05(((AbstractActivityC121685jC) brazilPaymentActivity).A0G);
                ((AbstractActivityC121685jC) brazilPaymentActivity).A0B = A05;
                if (A05 == null || A05.A05 == null) {
                    ((ActivityC13830kP) brazilPaymentActivity).A05.Ab2(new Runnable() { // from class: X.6Fg
                        @Override // java.lang.Runnable
                        public final void run() {
                            BrazilPaymentActivity brazilPaymentActivity2 = BrazilPaymentActivity.this;
                            C119695ex r13 = new C119695ex();
                            r13.A05 = ((AbstractActivityC121685jC) brazilPaymentActivity2).A0G;
                            r13.A0B(false);
                            r13.A09(0);
                            C17070qD r02 = ((AbstractActivityC121685jC) brazilPaymentActivity2).A0P;
                            r02.A03();
                            r02.A09.A0J(r13);
                        }
                    });
                }
                PaymentView paymentView3 = brazilPaymentActivity.A0V;
                C15370n3 r32 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0A;
                String A04 = brazilPaymentActivity.A03.A04(r32);
                paymentView3.A1F = A04;
                paymentView3.A0H.setText(A04);
                paymentView3.A07.setVisibility(8);
                paymentView3.A0Y.A06(paymentView3.A0W, r32);
                if (((AbstractActivityC121685jC) brazilPaymentActivity).A0O.A08()) {
                    UserJid userJid2 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0G;
                    if (((AbstractActivityC121685jC) brazilPaymentActivity).A0O.A06() && (A0F = C117305Zk.A0F(userJid2, ((AbstractActivityC121685jC) brazilPaymentActivity).A0P)) != null && A0F.A01 < C117315Zl.A02(brazilPaymentActivity)) {
                        C124105oc r13 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0J;
                        if (r13 != null) {
                            r13.A03(true);
                        }
                        C124105oc r24 = new C124105oc(((AbstractActivityC121685jC) brazilPaymentActivity).A06, userJid2, ((AbstractActivityC121685jC) brazilPaymentActivity).A0P);
                        ((AbstractActivityC121685jC) brazilPaymentActivity).A0J = r24;
                        C12960it.A1E(r24, ((ActivityC13830kP) brazilPaymentActivity).A05);
                    }
                }
                if (brazilPaymentActivity.A2n()) {
                    C14580lf r33 = new C14580lf();
                    ((ActivityC13830kP) brazilPaymentActivity).A05.Ab2(new Runnable(r33, brazilPaymentActivity) { // from class: X.6I1
                        public final /* synthetic */ C14580lf A00;
                        public final /* synthetic */ AbstractActivityC121685jC A01;

                        {
                            this.A01 = r2;
                            this.A00 = r1;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            AbstractActivityC121685jC r42 = this.A01;
                            r42.A0R.A00(r42.A0G, new C133306Ag(this.A00, r42));
                        }
                    });
                    ((AbstractActivityC121685jC) brazilPaymentActivity).A04 = r33;
                }
                if (!C117305Zk.A1U(((ActivityC13810kN) brazilPaymentActivity).A0C) || ((ActivityC13810kN) brazilPaymentActivity).A0C.A07(979)) {
                    AnonymousClass61I.A03(AnonymousClass61I.A00(((ActivityC13790kL) brazilPaymentActivity).A05, null, ((AbstractActivityC121685jC) brazilPaymentActivity).A0U, null, true), brazilPaymentActivity.A0K, "new_payment", brazilPaymentActivity.A0Y);
                    return;
                }
                C118025b9 A00 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0Y.A00(brazilPaymentActivity);
                ((AbstractActivityC121685jC) brazilPaymentActivity).A0X = A00;
                if (A00 != null) {
                    A00.A05.Ab2(new RunnableC135466Io(A00, false));
                    C117295Zj.A0r(brazilPaymentActivity, ((AbstractActivityC121685jC) brazilPaymentActivity).A0X.A00, 15);
                    C118025b9 r52 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0X;
                    r52.A05.Ab2(new RunnableC135686Jk(((AbstractActivityC121685jC) brazilPaymentActivity).A0G, r52, C117295Zj.A03(((ActivityC13790kL) brazilPaymentActivity).A05)));
                    return;
                }
                return;
            }
            BrazilOrderDetailsActivity brazilOrderDetailsActivity = (BrazilOrderDetailsActivity) brazilPaymentActivity;
            AbstractC005102i A1U3 = brazilOrderDetailsActivity.A1U();
            if (A1U3 != null) {
                A1U3.A0M(true);
            }
            PaymentCheckoutOrderDetailsViewV2 paymentCheckoutOrderDetailsViewV2 = (PaymentCheckoutOrderDetailsViewV2) C12960it.A0F(LayoutInflater.from(brazilOrderDetailsActivity), null, R.layout.checkout_order_details_screen);
            brazilOrderDetailsActivity.A08 = paymentCheckoutOrderDetailsViewV2;
            brazilOrderDetailsActivity.setContentView(paymentCheckoutOrderDetailsViewV2);
            brazilOrderDetailsActivity.A0G = brazilOrderDetailsActivity.getIntent().getStringExtra("extra_order_id");
            brazilOrderDetailsActivity.A00 = brazilOrderDetailsActivity.getIntent().getLongExtra("extra_order_expiry_ts_in_sec", 0);
            AnonymousClass1IS A02 = C38211ni.A02(brazilOrderDetailsActivity.getIntent());
            AnonymousClass009.A05(A02);
            brazilOrderDetailsActivity.A0E = A02;
            C14850m9 r8 = ((ActivityC13810kN) brazilOrderDetailsActivity).A0C;
            Resources resources = brazilOrderDetailsActivity.getResources();
            AnonymousClass14X r122 = brazilOrderDetailsActivity.A0D;
            C123695nh r42 = new C123695nh(resources, ((BrazilPaymentActivity) brazilOrderDetailsActivity).A04, ((AbstractActivityC121685jC) brazilOrderDetailsActivity).A08, r8, brazilOrderDetailsActivity.A04, ((AbstractActivityC121685jC) brazilOrderDetailsActivity).A0P, brazilOrderDetailsActivity.A0I, r122);
            brazilOrderDetailsActivity.A06 = r42;
            C129165xH r34 = new C129165xH(brazilOrderDetailsActivity.A03, brazilOrderDetailsActivity, r42, ((ActivityC13830kP) brazilOrderDetailsActivity).A05);
            brazilOrderDetailsActivity.A07 = r34;
            ((ActivityC001000l) brazilOrderDetailsActivity).A06.A00(new PaymentCheckoutOrderDetailsPresenter$$ExternalSyntheticLambda0(r34));
            brazilOrderDetailsActivity.A0A = (C118155bM) C117315Zl.A06(new AnonymousClass65v(brazilOrderDetailsActivity.A02, ((ActivityC13810kN) brazilOrderDetailsActivity).A0C, ((AbstractActivityC121685jC) brazilOrderDetailsActivity).A0G, ((BrazilPaymentActivity) brazilOrderDetailsActivity).A0F, brazilOrderDetailsActivity.A09, brazilOrderDetailsActivity.A0E, ((ActivityC13830kP) brazilOrderDetailsActivity).A05, true), brazilOrderDetailsActivity).A00(C118155bM.class);
            if (bundle == null || bundle.getBundle("save_order_detail_state_key") == null) {
                brazilOrderDetailsActivity.A0A.A04();
            } else {
                brazilOrderDetailsActivity.A0A.A05(bundle);
            }
            C117295Zj.A0r(brazilOrderDetailsActivity, brazilOrderDetailsActivity.A0A.A02, 14);
        }
    }

    public void A2i(Bundle bundle) {
        Intent A0D = C12990iw.A0D(this, PaymentGroupParticipantPickerActivity.class);
        AbstractC14640lm r0 = this.A0E;
        AnonymousClass009.A05(r0);
        A0D.putExtra("extra_jid", r0.getRawString());
        if (bundle != null) {
            A0D.putExtras(bundle);
        }
        startActivity(A0D);
        finish();
    }

    public void A2j(C30821Yy r14) {
        AbstractC15340mz r10;
        PaymentView A2e = A2e();
        if (A2e != null) {
            PaymentView A2e2 = A2e();
            if (A2e2 == null || A2e2.getStickerIfSelected() == null) {
                ((ActivityC13830kP) this).A05.Ab2(new Runnable(r14, this, A2e) { // from class: X.6JU
                    public final /* synthetic */ C30821Yy A00;
                    public final /* synthetic */ AbstractActivityC121685jC A01;
                    public final /* synthetic */ PaymentView A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        UserJid of;
                        AbstractActivityC121685jC r5 = this.A01;
                        PaymentView paymentView = this.A02;
                        C30821Yy r4 = this.A00;
                        C18610sj r3 = r5.A0M;
                        C28861Ph A2f = r5.A2f(paymentView.getPaymentNote(), paymentView.getMentionedJids());
                        AbstractC14640lm r1 = r5.A0E;
                        if (C15380n4.A0J(r1)) {
                            of = r5.A0G;
                        } else {
                            of = UserJid.of(r1);
                        }
                        r3.A07(r4, null, of, A2f);
                    }
                });
                A2g(1);
                return;
            }
            A2C(R.string.register_wait_message);
            C20350vc r6 = this.A0S;
            AnonymousClass009.A03(A2e);
            AnonymousClass1KS stickerIfSelected = A2e.getStickerIfSelected();
            AnonymousClass009.A05(stickerIfSelected);
            AbstractC14640lm r8 = this.A0E;
            AnonymousClass009.A05(r8);
            UserJid userJid = this.A0G;
            long j = this.A02;
            if (j != 0) {
                r10 = this.A09.A0K.A00(j);
            } else {
                r10 = null;
            }
            C117315Zl.A0R(((ActivityC13810kN) this).A05, r6.A01(A2e.getPaymentBackground(), r8, userJid, r10, stickerIfSelected, A2e.getStickerSendOrigin()), new AbstractC14590lg(r14, this, A2e) { // from class: X.6Eg
                public final /* synthetic */ C30821Yy A00;
                public final /* synthetic */ AbstractActivityC121685jC A01;
                public final /* synthetic */ PaymentView A02;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                    this.A02 = r3;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    AbstractActivityC121685jC r2 = this.A01;
                    C30821Yy r3 = this.A00;
                    PaymentView paymentView = this.A02;
                    AnonymousClass61P r1 = r2.A0V;
                    AbstractC14640lm r5 = r2.A0E;
                    AnonymousClass009.A05(r5);
                    UserJid userJid2 = r2.A0G;
                    long j2 = r2.A02;
                    String paymentNote = paymentView.getPaymentNote();
                    List mentionedJids = paymentView.getMentionedJids();
                    r1.A03(r2, r3, paymentView.getPaymentBackground(), r5, userJid2, (AnonymousClass22U) obj, new C133836Ch(r2, paymentView), paymentNote, mentionedJids, j2);
                }
            });
        }
    }

    public void A2k(AbstractC30891Zf r4) {
        C118025b9 r0;
        AnonymousClass617 r02;
        AnonymousClass2S0 r03;
        C50942Ry r04;
        if (C117305Zk.A1U(((ActivityC13810kN) this).A0C) && (r0 = this.A0X) != null && (r02 = (AnonymousClass617) r0.A00.A01()) != null && (r03 = (AnonymousClass2S0) r02.A01) != null && (r04 = r03.A01) != null) {
            r4.A00 = new C30971Zn(String.valueOf(r04.A08.A01), null, null, null);
        }
    }

    public void A2l(AbstractC16870pt r7, AnonymousClass2S0 r8) {
        AnonymousClass61I.A01(AnonymousClass61I.A00(((ActivityC13790kL) this).A05, null, r8, null, true), r7, 50, "new_payment", null, 2);
    }

    public void A2m(String str) {
        int i;
        PaymentView A2e = A2e();
        if (A2e != null) {
            TextView A0J = C12960it.A0J(A2e, R.id.gift_tool_tip);
            if (C12980iv.A1W(A2e.A0p.A01(), "payment_incentive_tooltip_viewed") || A0J == null || str == null) {
                i = 8;
            } else {
                A0J.setText(str);
                i = 0;
            }
            A0J.setVisibility(i);
            int i2 = this.A01;
            A2e.A02 = i2;
            FrameLayout frameLayout = A2e.A06;
            if (i2 != 0) {
                frameLayout.setVisibility(8);
                return;
            }
            frameLayout.setVisibility(0);
            C12960it.A0t(C117295Zj.A05(A2e.A0p), "payment_incentive_tooltip_viewed", true);
        }
    }

    public boolean A2n() {
        C15370n3 r0;
        C22710zW r1 = this.A0O;
        return r1.A08() && r1.A03.A07(1746) && (r0 = this.A0A) != null && r0.A0J();
    }

    @Override // X.AbstractC13950kb
    public void ATj(PickerSearchDialogFragment pickerSearchDialogFragment) {
        this.A0a.A01(pickerSearchDialogFragment);
    }

    @Override // X.AbstractC13950kb
    public void Adk(DialogFragment dialogFragment) {
        Adm(dialogFragment);
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1001) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            this.A0G = UserJid.getNullable(intent.getStringExtra("extra_receiver_jid"));
            A2h(this.A03);
        } else if (i2 == 0 && this.A0G == null) {
            finish();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        AbstractC38041nQ r1;
        AbstractC16830pp AFX;
        Integer num;
        super.onCreate(bundle);
        this.A03 = bundle;
        if (getIntent() != null) {
            this.A00 = getIntent().getIntExtra("extra_conversation_message_type", 0);
            this.A0E = AbstractC14640lm.A01(getIntent().getStringExtra("extra_jid"));
            this.A0G = UserJid.getNullable(getIntent().getStringExtra("extra_receiver_jid"));
            this.A0m = getIntent().getStringExtra("extra_tpp_transaction_request_id");
            this.A02 = getIntent().getLongExtra("extra_quoted_msg_row_id", 0);
            this.A0i = getIntent().getStringExtra("extra_payment_preset_amount");
            this.A0n = getIntent().getStringExtra("extra_transaction_id");
            this.A0k = getIntent().getStringExtra("extra_payment_preset_min_amount");
            this.A0j = getIntent().getStringExtra("extra_payment_preset_max_amount");
            this.A0l = getIntent().getStringExtra("extra_request_message_key");
            this.A0s = getIntent().getBooleanExtra("extra_is_pay_money_only", true);
            this.A0h = getIntent().getStringExtra("extra_payment_note");
            this.A0C = (C30921Zi) getIntent().getParcelableExtra("extra_payment_background");
            this.A0c = (AnonymousClass1KS) getIntent().getParcelableExtra("extra_payment_sticker");
            int intExtra = getIntent().getIntExtra("extra_payment_sticker_send_origin", -1);
            if (intExtra != -1) {
                num = Integer.valueOf(intExtra);
            } else {
                num = null;
            }
            this.A0e = num;
            this.A0q = C15380n4.A07(UserJid.class, getIntent().getStringArrayListExtra("extra_mentioned_jids"));
            this.A0F = UserJid.getNullable(getIntent().getStringExtra("extra_inviter_jid"));
            String stringExtra = getIntent().getStringExtra("extra_transaction_type");
            if (stringExtra == null) {
                stringExtra = "p2p";
            }
            this.A0o = stringExtra;
            this.A0p = getIntent().getStringExtra("extra_transaction_token");
            this.A0r = getIntent().getBooleanExtra("extra_transaction_is_merchant", false);
            this.A0t = getIntent().getBooleanExtra("extra_transaction_is_valid_merchant", false);
            this.A0g = getIntent().getStringExtra("extra_order_type");
            this.A0f = getIntent().getStringExtra("extra_payment_config_id");
        }
        if (!(this instanceof NoviSharedPaymentActivity)) {
            String str = null;
            if (this.A0N.A01() != null) {
                r1 = this.A0P.A01(this.A0N.A01().A03);
            } else {
                r1 = null;
            }
            AbstractC30791Yv A00 = this.A0N.A00();
            if (A00 != null) {
                str = ((AbstractC30781Yu) A00).A04;
            }
            if (r1 != null && (AFX = r1.AFX(str)) != null && AFX.AdP()) {
                this.A05.A0D(null, "payment_view");
                return;
            }
            return;
        }
        NoviSharedPaymentActivity noviSharedPaymentActivity = (NoviSharedPaymentActivity) this;
        C1310561a.A04(noviSharedPaymentActivity, ((ActivityC13810kN) noviSharedPaymentActivity).A05, ((AbstractActivityC121685jC) noviSharedPaymentActivity).A0K, ((AbstractActivityC121685jC) noviSharedPaymentActivity).A0M, new IDxAListenerShape19S0100000_3_I1(noviSharedPaymentActivity, 11), noviSharedPaymentActivity.A0E);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C124105oc r1 = this.A0J;
        if (r1 != null) {
            r1.A03(true);
            this.A0J = null;
        }
    }
}
