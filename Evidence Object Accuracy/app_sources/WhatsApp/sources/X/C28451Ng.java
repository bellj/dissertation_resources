package X;

import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.1Ng  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28451Ng implements AbstractC28461Nh {
    public final /* synthetic */ AnonymousClass16A A00;
    public final /* synthetic */ boolean A01;

    public C28451Ng(AnonymousClass16A r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void AOt(long j) {
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        StringBuilder sb = new StringBuilder("app/CrashLogs/uploadCrashData/error received: ");
        sb.append(str);
        Log.e(sb.toString());
        this.A00.A0A("upload-error-from-server", this.A01);
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }
}
