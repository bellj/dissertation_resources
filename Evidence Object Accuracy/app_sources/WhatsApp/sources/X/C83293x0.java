package X;

import android.view.animation.Animation;

/* renamed from: X.3x0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83293x0 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AbstractC36001jA A00;

    public C83293x0(AbstractC36001jA r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AbstractC36001jA r2 = this.A00;
        r2.A0J.setVisibility(8);
        r2.A0G(0);
    }
}
