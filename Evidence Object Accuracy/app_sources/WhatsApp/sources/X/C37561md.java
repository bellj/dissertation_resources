package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DataTaskListener;
import com.facebook.msys.mci.NetworkSession;
import com.facebook.msys.mci.NetworkUtils;
import com.facebook.msys.mci.UrlRequest;
import com.facebook.msys.mci.UrlResponse;
import com.facebook.msys.mci.common.RunnableNRunnableShape0S1203000_I0;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: X.1md  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37561md implements AbstractC37571me {
    public int A00;
    public final DataTaskListener A01;
    public final C18790t3 A02;
    public final C18800t4 A03;
    public final C19930uu A04;
    public final AbstractC14440lR A05;
    public final File A06;
    public final Map A07 = Collections.synchronizedMap(new HashMap());

    public C37561md(C18790t3 r2, C18800t4 r3, C19930uu r4, AbstractC14440lR r5, File file) {
        this.A04 = r4;
        this.A05 = r5;
        this.A02 = r2;
        this.A03 = r3;
        this.A06 = file;
        this.A01 = new AnonymousClass3SQ(this);
    }

    public final UrlResponse A00(NetworkSession networkSession, UrlRequest urlRequest, FileInputStream fileInputStream, OutputStream outputStream, String str, boolean z, boolean z2) {
        int read;
        urlRequest.getUrl();
        byte[] httpBody = urlRequest.getHttpBody();
        HttpsURLConnection A01 = A01(urlRequest, fileInputStream, httpBody);
        try {
            if (A01.getDoOutput()) {
                try {
                    DataOutputStream dataOutputStream = new DataOutputStream(new AnonymousClass23V(this.A02, A01.getOutputStream(), null, 29));
                    int i = 0;
                    try {
                        if (fileInputStream != null) {
                            int available = fileInputStream.available();
                            int min = Math.min(10240, available);
                            byte[] bArr = new byte[min];
                            do {
                                read = fileInputStream.read(bArr, 0, min);
                                dataOutputStream.write(bArr, 0, read);
                                int available2 = fileInputStream.available();
                                min = Math.min(10240, available2);
                                int i2 = available - available2;
                                if (z) {
                                    networkSession.executeInNetworkContext(new RunnableNRunnableShape0S1203000_I0(networkSession, this, str, read, i2, available, 1));
                                    continue;
                                }
                            } while (read > 0);
                        } else {
                            int length = httpBody.length;
                            while (i < length) {
                                int min2 = Math.min(10240, length);
                                dataOutputStream.write(httpBody, i, min2);
                                length -= min2;
                                i += min2;
                                if (z) {
                                    networkSession.executeInNetworkContext(new C75983kq(networkSession, this, str, httpBody, min2, i));
                                }
                            }
                        }
                        dataOutputStream.close();
                    } catch (Throwable th) {
                        try {
                            dataOutputStream.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (IllegalArgumentException | IndexOutOfBoundsException e) {
                    throw new IOException("Failed to setup connection", e);
                }
            }
            A02(networkSession, outputStream, str, A01, z2);
            UrlResponse urlResponse = new UrlResponse(urlRequest, A01.getResponseCode(), NetworkUtils.flattenHeaders(A01.getHeaderFields()));
            A01.getResponseCode();
            return urlResponse;
        } finally {
            A01.disconnect();
        }
    }

    public final HttpsURLConnection A01(UrlRequest urlRequest, FileInputStream fileInputStream, byte[] bArr) {
        int length;
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) new URL(urlRequest.getUrl()).openConnection();
        Map httpHeaders = urlRequest.getHttpHeaders();
        httpsURLConnection.setDoInput(true);
        httpsURLConnection.setRequestMethod(urlRequest.getHttpMethod());
        httpsURLConnection.setRequestProperty("User-Agent", this.A04.A00());
        httpsURLConnection.setRequestProperty("WaMsysRequest", "1");
        if (httpHeaders.containsKey("X-Forwarded-Host")) {
            httpsURLConnection.setSSLSocketFactory((SSLSocketFactory) SSLSocketFactory.getDefault());
        } else {
            if (httpHeaders.containsKey("Host")) {
                httpsURLConnection.setHostnameVerifier(new C37591mg((String) httpHeaders.get("Host"), HttpsURLConnection.getDefaultHostnameVerifier()));
            }
            httpsURLConnection.setSSLSocketFactory(this.A03.A02());
        }
        int i = this.A00;
        if (i > 0) {
            httpsURLConnection.setConnectTimeout(i);
            httpsURLConnection.setReadTimeout(this.A00);
        }
        if (!(bArr == null && fileInputStream == null)) {
            httpsURLConnection.setDoOutput(true);
            if (fileInputStream != null) {
                length = fileInputStream.available();
            } else {
                AnonymousClass009.A05(bArr);
                length = bArr.length;
            }
            httpsURLConnection.setFixedLengthStreamingMode(length);
        }
        for (Map.Entry entry : httpHeaders.entrySet()) {
            httpsURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        return httpsURLConnection;
    }

    public final void A02(NetworkSession networkSession, OutputStream outputStream, String str, HttpURLConnection httpURLConnection, boolean z) {
        try {
            try {
                AnonymousClass1oJ r2 = new AnonymousClass1oJ(this.A02, httpURLConnection.getInputStream(), null, 29);
                try {
                    int contentLength = httpURLConnection.getContentLength();
                    byte[] bArr = new byte[10240];
                    int i = 0;
                    while (true) {
                        int read = r2.read(bArr);
                        if (read != -1) {
                            outputStream.write(bArr, 0, read);
                            i += read;
                            if (z) {
                                networkSession.executeInNetworkContext(new RunnableNRunnableShape0S1203000_I0(networkSession, this, str, read, i, contentLength, 0));
                            }
                        } else {
                            r2.close();
                            return;
                        }
                    }
                } catch (Throwable th) {
                    try {
                        r2.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (IllegalArgumentException | IndexOutOfBoundsException e) {
                throw new IOException("Malformed Http Response", e);
            }
        } catch (IOException unused2) {
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode >= 400 && responseCode <= 500) {
                String format = String.format(null, "[HTTP status=%d] Error Content = ", Integer.valueOf(responseCode));
                try {
                    InputStream errorStream = httpURLConnection.getErrorStream();
                    if (errorStream != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(format);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        byte[] bArr2 = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
                        while (true) {
                            int read2 = errorStream.read(bArr2);
                            if (read2 == -1) {
                                break;
                            }
                            byteArrayOutputStream.write(bArr2, 0, read2);
                        }
                        sb.append(byteArrayOutputStream.toString());
                        format = sb.toString();
                        errorStream.close();
                    }
                } catch (IOException unused3) {
                }
                throw new IOException(format);
            }
        }
    }
}
