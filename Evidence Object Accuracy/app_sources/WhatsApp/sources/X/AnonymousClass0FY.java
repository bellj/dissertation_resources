package X;

/* renamed from: X.0FY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FY extends AbstractC05330Pd {
    public final /* synthetic */ C07740a0 A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "UPDATE workspec SET output=? WHERE id=?";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FY(AnonymousClass0QN r1, C07740a0 r2) {
        super(r1);
        this.A00 = r2;
    }
}
