package X;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1wA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42951wA {
    public AbstractC15340mz A00;
    public final AbstractC15710nm A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final AnonymousClass01d A06;
    public final AnonymousClass018 A07;
    public final C15670ni A08;
    public final C22630zO A09;

    public C42951wA(AbstractC15710nm r1, C15570nT r2, C15450nH r3, C15550nR r4, C15610nY r5, AnonymousClass01d r6, AnonymousClass018 r7, C15670ni r8, C22630zO r9, AbstractC15340mz r10) {
        this.A01 = r1;
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r6;
        this.A05 = r5;
        this.A07 = r7;
        this.A09 = r9;
        this.A08 = r8;
        this.A00 = r10;
    }

    public long A00() {
        AbstractC15340mz r0;
        if (this instanceof C43001wF) {
            r0 = ((C43001wF) this).A00.A01;
        } else if (!(this instanceof C43041wJ)) {
            r0 = this.A00;
        } else {
            r0 = ((C43041wJ) this).A00.A00;
        }
        return r0.A11;
    }

    public C15370n3 A01() {
        AbstractC15340mz r0 = this.A00;
        AbstractC14640lm A0B = r0.A0B();
        AbstractC14640lm r1 = r0.A0z.A00;
        if (C15380n4.A0J(r1) && A0B != null) {
            return this.A04.A0B(A0B);
        }
        C15550nR r02 = this.A04;
        AnonymousClass009.A05(r1);
        return r02.A0B(r1);
    }

    public C38201nh A02(C15370n3 r12) {
        String A0B;
        AbstractC14640lm A0B2;
        if (this instanceof C43001wF) {
            C43001wF r7 = (C43001wF) this;
            AnonymousClass1Iv r1 = r7.A00.A02;
            int i = 2;
            if (C15380n4.A0J(r1.A0z.A00)) {
                i = 1;
            }
            AbstractC14640lm A0B3 = r1.A0B();
            C15610nY r13 = r7.A05;
            String A0B4 = r13.A0B(r12, i, false);
            if (r12.A0K() && A0B3 != null) {
                String A0B5 = r13.A0B(r7.A04.A0B(A0B3), i, false);
                int i2 = r7.A00.A00 - 1;
                if (i2 != 0) {
                    A0B4 = r7.A01.A00.getResources().getQuantityString(R.plurals.reactions_group_notification_title, i2, A0B5, Integer.valueOf(i2), A0B4);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(A0B5);
                    sb.append(" @ ");
                    sb.append(A0B4);
                    A0B4 = sb.toString();
                }
            }
            AnonymousClass009.A05(A0B4);
            return new C38201nh(A0B4, r7.A09());
        } else if (!(this instanceof C43041wJ)) {
            C22630zO r5 = this.A09;
            AbstractC15340mz r4 = this.A00;
            if (r4 == null) {
                return new C38201nh("", "");
            }
            int i3 = 2;
            if (C15380n4.A0J(r4.A0z.A00)) {
                i3 = 1;
            }
            if (!r12.A0K() || (r4 instanceof AnonymousClass1XB) || (A0B2 = r4.A0B()) == null) {
                A0B = r5.A07.A0B(r12, i3, false);
            } else {
                C15610nY r2 = r5.A07;
                String A0B6 = r2.A0B(r5.A06.A0B(A0B2), i3, false);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(A0B6);
                sb2.append(" @ ");
                sb2.append(r2.A0B(r12, i3, false));
                A0B = sb2.toString();
            }
            return new C38201nh(A0B, r5.A0F(r4));
        } else {
            C43041wJ r72 = (C43041wJ) this;
            C43031wI r6 = r72.A00;
            C30311Wx r14 = r6.A01;
            int i4 = 2;
            if (C15380n4.A0J(r14.A0z.A00)) {
                i4 = 1;
            }
            AbstractC14640lm A0B7 = r14.A0B();
            C15610nY r22 = r72.A05;
            String A0B8 = r22.A0B(r12, i4, false);
            if (r12.A0K() && A0B7 != null) {
                String A0B9 = r22.A0B(r72.A04.A0B(A0B7), i4, false);
                StringBuilder sb3 = new StringBuilder();
                sb3.append(A0B9);
                sb3.append(" @ ");
                sb3.append(A0B8);
                A0B8 = sb3.toString();
            }
            AnonymousClass009.A05(A0B8);
            return new C38201nh(A0B8, r72.A01.A00.getString(R.string.kic_notification_text_single, r72.A09.A0F(r6.A00)));
        }
    }

    public CharSequence A03(C15370n3 r10, boolean z) {
        CharSequence[] charSequenceArr;
        String str;
        StringBuilder sb;
        CharSequence charSequence;
        StringBuilder sb2;
        CharSequence obj;
        if (this instanceof C43001wF) {
            C43001wF r3 = (C43001wF) this;
            charSequenceArr = new CharSequence[2];
            int i = 2;
            if (C15380n4.A0J(r3.A00.A02.A0z.A00)) {
                i = 1;
            }
            if (r10.A0K()) {
                AbstractC15340mz r7 = ((C42951wA) r3).A00;
                boolean z2 = r7.A0z.A02;
                if (z) {
                    if (!z2) {
                        CharSequence A08 = r3.A08(r3.A09.A0H(r7.A0B(), i));
                        sb = new StringBuilder();
                        sb.append((Object) A08);
                        sb.append(" @ ");
                        sb.append(r3.A05.A0B(r10, i, false));
                        sb.append(": ");
                        charSequence = sb.toString();
                        charSequenceArr[0] = charSequence;
                        str = r3.A09();
                    }
                } else if (!z2) {
                    CharSequence A082 = r3.A08(r3.A09.A0H(r7.A0B(), i));
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append((Object) A082);
                    sb3.append(": ");
                    charSequence = C22630zO.A01(sb3.toString());
                    charSequenceArr[0] = charSequence;
                    str = r3.A09();
                }
            } else if (z) {
                String A0B = r3.A05.A0B(r10, i, false);
                AnonymousClass009.A05(A0B);
                CharSequence A083 = r3.A08(A0B);
                sb = new StringBuilder();
                sb.append((Object) A083);
                sb.append(": ");
                charSequence = sb.toString();
                charSequenceArr[0] = charSequence;
                str = r3.A09();
            }
            sb = new StringBuilder();
            sb.append(r3.A05.A0B(r10, i, false));
            sb.append(": ");
            charSequence = sb.toString();
            charSequenceArr[0] = charSequence;
            str = r3.A09();
        } else if (!(this instanceof C43041wJ)) {
            return this.A09.A0D(r10, this.A00, z, false);
        } else {
            C43041wJ r32 = (C43041wJ) this;
            charSequenceArr = new CharSequence[2];
            C43031wI r2 = r32.A00;
            int i2 = 2;
            if (C15380n4.A0J(r2.A01.A0z.A00)) {
                i2 = 1;
            }
            if (r10.A0K()) {
                String A0H = r32.A09.A0H(((C42951wA) r32).A00.A0B(), i2);
                boolean z3 = ((C42951wA) r32).A00.A0z.A02;
                if (z) {
                    if (!z3) {
                        sb2 = new StringBuilder();
                        sb2.append((Object) A0H);
                        sb2.append(" @ ");
                        sb2.append(r32.A05.A0B(r10, i2, false));
                        sb2.append(": ");
                        obj = sb2.toString();
                    }
                } else if (!z3) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append((Object) A0H);
                    sb4.append(": ");
                    obj = C22630zO.A01(sb4.toString());
                }
                sb2 = new StringBuilder();
                sb2.append(r32.A05.A0B(r10, i2, false));
                sb2.append(": ");
                obj = sb2.toString();
            } else {
                String A0B2 = r32.A05.A0B(r10, i2, false);
                AnonymousClass009.A05(A0B2);
                if (z) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append((Object) A0B2);
                    sb5.append(": ");
                    obj = sb5.toString();
                }
                sb2 = new StringBuilder();
                sb2.append(r32.A05.A0B(r10, i2, false));
                sb2.append(": ");
                obj = sb2.toString();
            }
            charSequenceArr[0] = obj;
            str = r32.A01.A00.getString(R.string.kic_notification_text_single, r32.A09.A0F(r2.A00));
        }
        charSequenceArr[1] = str;
        return TextUtils.concat(charSequenceArr);
    }

    public String A04() {
        ContentResolver A0C = this.A06.A0C();
        if (A0C == null) {
            Log.w("messagenotification cr=null");
        } else {
            Uri A05 = this.A04.A05(A0C, A01());
            if (A05 != null) {
                return A05.toString();
            }
        }
        return null;
    }

    public void A05(C005602s r5) {
        Bundle bundle = new Bundle();
        bundle.putLong("last_row_id", this.A00.A11);
        Bundle bundle2 = r5.A0D;
        if (bundle2 == null) {
            r5.A0D = new Bundle(bundle);
        } else {
            bundle2.putAll(bundle);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.C005602s r15, X.C15370n3 r16, java.lang.StringBuilder r17, boolean r18) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42951wA.A06(X.02s, X.0n3, java.lang.StringBuilder, boolean):void");
    }

    public boolean A07(Context context, C005602s r8, C15370n3 r9) {
        C16150oX r1;
        File file;
        AbstractC15340mz r12 = this.A00;
        if ((!(r12 instanceof C30421Xi) && !(r12 instanceof AnonymousClass1X7)) || (r1 = ((AbstractC16130oV) r12).A02) == null || !r1.A0P || (file = r1.A0F) == null || !file.exists()) {
            return false;
        }
        Intent action = new C14960mK().A0g(context, r9).setAction(Conversation.A5A);
        C35741ib.A01(action, "UpdateMessageNotificationRunnable");
        C38211ni.A00(action, this.A00.A0z);
        PendingIntent A00 = AnonymousClass1UY.A00(context, 4, action, 134217728);
        AbstractC15340mz r13 = this.A00;
        boolean z = r13 instanceof AnonymousClass1X7;
        int i = R.drawable.notification_action_audio;
        if (z) {
            i = R.drawable.notification_action_image;
        }
        AnonymousClass018 r3 = this.A07;
        byte b = r13.A0y;
        int i2 = R.string.play;
        if (b == 1) {
            i2 = R.string.view;
        }
        r8.A04(i, r3.A09(i2), A00);
        return true;
    }
}
