package X;

import android.os.Parcel;
import android.support.v4.app.INotificationSideChannel;

/* renamed from: X.02t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C005702t implements AbstractC005802u {
    public final int A00;
    public final String A01;
    public final String A02;

    public C005702t(String str, String str2, int i) {
        this.A01 = str;
        this.A00 = i;
        this.A02 = str2;
    }

    @Override // X.AbstractC005802u
    public void AbX(INotificationSideChannel iNotificationSideChannel) {
        String str = this.A01;
        int i = this.A00;
        String str2 = this.A02;
        C06900Vo r6 = (C06900Vo) iNotificationSideChannel;
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            obtain.writeString(str);
            obtain.writeInt(i);
            obtain.writeString(str2);
            r6.A00.transact(2, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CancelTask[");
        sb.append("packageName:");
        sb.append(this.A01);
        sb.append(", id:");
        sb.append(this.A00);
        sb.append(", tag:");
        sb.append(this.A02);
        sb.append(", all:");
        sb.append(false);
        sb.append("]");
        return sb.toString();
    }
}
