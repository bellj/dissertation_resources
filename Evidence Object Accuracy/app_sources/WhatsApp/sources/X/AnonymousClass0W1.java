package X;

import android.view.MenuItem;

/* renamed from: X.0W1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W1 implements MenuItem.OnMenuItemClickListener {
    public final MenuItem.OnMenuItemClickListener A00;
    public final /* synthetic */ AnonymousClass0CJ A01;

    public AnonymousClass0W1(MenuItem.OnMenuItemClickListener onMenuItemClickListener, AnonymousClass0CJ r2) {
        this.A01 = r2;
        this.A00 = onMenuItemClickListener;
    }

    @Override // android.view.MenuItem.OnMenuItemClickListener
    public boolean onMenuItemClick(MenuItem menuItem) {
        return this.A00.onMenuItemClick(this.A01.A00(menuItem));
    }
}
