package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2Cs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C47802Cs extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C47802Cs A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01;
    public long A02;
    public AnonymousClass1G8 A03;

    static {
        C47802Cs r0 = new C47802Cs();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        AnonymousClass1G9 r1;
        switch (r16.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C47802Cs r2 = (C47802Cs) obj2;
                this.A03 = (AnonymousClass1G8) r8.Aft(this.A03, r2.A03);
                int i = this.A00;
                boolean z = false;
                if ((i & 2) == 2) {
                    z = true;
                }
                int i2 = this.A01;
                int i3 = r2.A00;
                boolean z2 = false;
                if ((i3 & 2) == 2) {
                    z2 = true;
                }
                this.A01 = r8.Afp(i2, r2.A01, z, z2);
                boolean z3 = false;
                if ((i & 4) == 4) {
                    z3 = true;
                }
                long j = this.A02;
                boolean z4 = false;
                if ((i3 & 4) == 4) {
                    z4 = true;
                }
                this.A02 = r8.Afs(j, r2.A02, z3, z4);
                if (r8 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            if ((this.A00 & 1) == 1) {
                                r1 = (AnonymousClass1G9) this.A03.A0T();
                            } else {
                                r1 = null;
                            }
                            AnonymousClass1G8 r0 = (AnonymousClass1G8) r82.A09(r22, AnonymousClass1G8.A05.A0U());
                            this.A03 = r0;
                            if (r1 != null) {
                                r1.A04(r0);
                                this.A03 = (AnonymousClass1G8) r1.A01();
                            }
                            this.A00 |= 1;
                        } else if (A03 == 16) {
                            int A02 = r82.A02();
                            if (AnonymousClass4BS.A00(A02) == null) {
                                super.A0X(2, A02);
                            } else {
                                this.A00 |= 2;
                                this.A01 = A02;
                            }
                        } else if (A03 == 24) {
                            this.A00 |= 4;
                            this.A02 = r82.A06();
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C47802Cs();
            case 5:
                return new C82073v2();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C47802Cs.class) {
                        if (A05 == null) {
                            A05 = new AnonymousClass255(A04);
                        }
                    }
                }
                return A05;
            default:
                throw new UnsupportedOperationException();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A02(2, this.A01);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A05(3, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A03;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0H(3, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
