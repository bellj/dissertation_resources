package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4RP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4RP {
    public final /* synthetic */ AnonymousClass19T A00;
    public final /* synthetic */ UserJid A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;

    public AnonymousClass4RP(AnonymousClass19T r1, UserJid userJid, String str, String str2) {
        this.A00 = r1;
        this.A03 = str;
        this.A01 = userJid;
        this.A02 = str2;
    }
}
