package X;

import android.content.Context;

/* renamed from: X.0sg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18580sg {
    public final AnonymousClass01c A00;
    public final C18560se A01;
    public final C18530sb A02;
    public final C18510sZ A03;
    public final C18540sc A04;
    public final C18570sf A05;

    public C18580sg(AnonymousClass01c r1, C18560se r2, C18530sb r3, C18510sZ r4, C18540sc r5, C18570sf r6) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
        this.A01 = r2;
        this.A05 = r6;
    }

    public void A00(Context context, AbstractC17450qp r16, C64173En r17) {
        C18530sb r10 = this.A02;
        C18510sZ r6 = this.A03;
        C18540sc r8 = this.A04;
        C64723Gq r7 = C64723Gq.A00;
        AnonymousClass4U8 r2 = new AnonymousClass4U8(context, AnonymousClass4ZA.A00, AnonymousClass4ZB.A00, r6, r7, r8, r16, r10, this.A05);
        AnonymousClass4N1 r13 = new AnonymousClass4N1(r17, this);
        r2.A05 = r13;
        AnonymousClass01c r0 = this.A00;
        if (r0 != null) {
            r2.A01 = r0;
        }
        Context context2 = r2.A00;
        AbstractC17450qp r102 = r2.A03;
        C87844Df r5 = new C87844Df();
        C87884Dk r9 = r2.A02;
        C18530sb r11 = r2.A04;
        C18520sa r62 = r2.A08;
        C18550sd r82 = r2.A0A;
        C64723Gq r72 = r2.A09;
        AnonymousClass4ZA r3 = r2.A06;
        C18570sf r12 = r2.A0B;
        AnonymousClass4ZB r4 = r2.A07;
        AnonymousClass01c r22 = r2.A01;
        if (r22 == null) {
            r22 = C08410b6.A00;
        }
        C65093Ic.A0D = new C65093Ic(context2, r22, r3, r4, r5, r62, r72, r82, r9, r102, r11, r12, r13);
    }
}
