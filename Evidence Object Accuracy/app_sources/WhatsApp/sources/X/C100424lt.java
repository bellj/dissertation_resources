package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100424lt implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C49662Lr(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C49662Lr[i];
    }
}
