package X;

import android.content.SharedPreferences;

/* renamed from: X.0yA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21900yA {
    public final SharedPreferences A00;
    public final AbstractC15460nI A01;

    public C21900yA(AbstractC15460nI r2, C16630pM r3) {
        this.A01 = r2;
        this.A00 = r3.A02(AnonymousClass01V.A07);
    }
}
