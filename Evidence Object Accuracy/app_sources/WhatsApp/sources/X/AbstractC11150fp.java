package X;

/* renamed from: X.0fp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC11150fp extends AnonymousClass5F5 implements AnonymousClass5ZY {
    public abstract Object A04();

    @Override // X.AnonymousClass5F5
    public AnonymousClass5LD A03() {
        return super.A03();
    }

    public final void A05(C114215Kq r2, AnonymousClass5LD r3) {
        r2.A0C(new C114105Kf(this, r3));
    }

    public boolean A06(AnonymousClass5LD r7) {
        int A00;
        AnonymousClass5LE A02 = A02();
        AnonymousClass5LA r2 = new AnonymousClass5LA(this, r7);
        do {
            A00 = A02.A04().A00(r2, r7, A02);
            if (A00 == 1) {
                return true;
            }
        } while (A00 != 2);
        return false;
    }

    public final boolean A07(AnonymousClass5LD r2) {
        return A06(r2);
    }

    @Override // X.AbstractC12260hd
    public final C06080Sc AKJ() {
        return new C06080Sc(this);
    }
}
