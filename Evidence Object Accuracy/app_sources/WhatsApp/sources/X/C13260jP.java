package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0jP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13260jP extends BroadcastReceiver {
    public static AtomicReference A01 = new AtomicReference();
    public final Context A00;

    public C13260jP(Context context) {
        this.A00 = context;
    }

    public static /* synthetic */ void A00(Context context) {
        AtomicReference atomicReference = A01;
        if (atomicReference.get() == null) {
            C13260jP r2 = new C13260jP(context);
            if (atomicReference.compareAndSet(null, r2)) {
                context.registerReceiver(r2, new IntentFilter("android.intent.action.USER_UNLOCKED"));
            }
        }
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        synchronized (C13030j1.A09) {
            for (C13030j1 r0 : C13030j1.A0A.values()) {
                r0.A03();
            }
        }
        this.A00.unregisterReceiver(this);
    }
}
