package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.0wX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20920wX {
    public static final String A09 = A02("xｘ#＃~～");
    public static final Map A0A;
    public static final Map A0B;
    public static final Map A0C;
    public static final Map A0D;
    public static final Logger A0E = Logger.getLogger(C20920wX.class.getName());
    public static final Pattern A0F = Pattern.compile("(\\p{Nd})");
    public static final Pattern A0G = Pattern.compile("\\$CC");
    public static final Pattern A0H;
    public static final Pattern A0I = Pattern.compile("\\$FG");
    public static final Pattern A0J = Pattern.compile("\\(?\\$1\\)?");
    public static final Pattern A0K = Pattern.compile("(\\$\\d)");
    public static final Pattern A0L = Pattern.compile("(\\D+)");
    public static final Pattern A0M = Pattern.compile("\\$NP");
    public static final Pattern A0N = Pattern.compile("[+＋]+");
    public static final Pattern A0O = Pattern.compile("[\\\\/] *x");
    public static final Pattern A0P = Pattern.compile("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]+");
    public static final Pattern A0Q = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");
    public static final Pattern A0R = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");
    public static final Pattern A0S = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    public static final Pattern A0T;
    public static final Pattern A0U = Pattern.compile("[+＋\\p{Nd}]");
    public static volatile C20920wX A0V;
    public C92134Ur A00 = new C92134Ur(100);
    public C22680zT A01;
    public String A02 = "/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto";
    public Map A03 = null;
    public final Map A04 = Collections.synchronizedMap(new HashMap());
    public final Map A05 = Collections.synchronizedMap(new HashMap());
    public final Set A06 = new HashSet();
    public final Set A07 = new HashSet(35);
    public final Set A08 = new HashSet(320);

    static {
        HashMap hashMap = new HashMap();
        hashMap.put('0', '0');
        hashMap.put('1', '1');
        hashMap.put('2', '2');
        hashMap.put('3', '3');
        hashMap.put('4', '4');
        hashMap.put('5', '5');
        hashMap.put('6', '6');
        hashMap.put('7', '7');
        hashMap.put('8', '8');
        hashMap.put('9', '9');
        HashMap hashMap2 = new HashMap(40);
        hashMap2.put('A', '2');
        hashMap2.put('B', '2');
        hashMap2.put('C', '2');
        hashMap2.put('D', '3');
        hashMap2.put('E', '3');
        hashMap2.put('F', '3');
        hashMap2.put('G', '4');
        hashMap2.put('H', '4');
        hashMap2.put('I', '4');
        hashMap2.put('J', '5');
        hashMap2.put('K', '5');
        hashMap2.put('L', '5');
        hashMap2.put('M', '6');
        hashMap2.put('N', '6');
        hashMap2.put('O', '6');
        hashMap2.put('P', '7');
        hashMap2.put('Q', '7');
        hashMap2.put('R', '7');
        hashMap2.put('S', '7');
        hashMap2.put('T', '8');
        hashMap2.put('U', '8');
        hashMap2.put('V', '8');
        hashMap2.put('W', '9');
        hashMap2.put('X', '9');
        hashMap2.put('Y', '9');
        hashMap2.put('Z', '9');
        Map unmodifiableMap = Collections.unmodifiableMap(hashMap2);
        A0B = unmodifiableMap;
        HashMap hashMap3 = new HashMap(100);
        hashMap3.putAll(unmodifiableMap);
        hashMap3.putAll(hashMap);
        A0C = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.putAll(hashMap);
        hashMap4.put('+', '+');
        hashMap4.put('*', '*');
        A0D = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        for (Character ch : unmodifiableMap.keySet()) {
            char charValue = ch.charValue();
            Character valueOf = Character.valueOf(Character.toLowerCase(charValue));
            Character valueOf2 = Character.valueOf(charValue);
            hashMap5.put(valueOf, valueOf2);
            hashMap5.put(valueOf2, valueOf2);
        }
        hashMap5.putAll(hashMap);
        hashMap5.put('-', '-');
        hashMap5.put((char) 65293, '-');
        hashMap5.put((char) 8208, '-');
        hashMap5.put((char) 8209, '-');
        hashMap5.put((char) 8210, '-');
        hashMap5.put((char) 8211, '-');
        hashMap5.put((char) 8212, '-');
        hashMap5.put((char) 8213, '-');
        hashMap5.put((char) 8722, '-');
        hashMap5.put('/', '/');
        hashMap5.put((char) 65295, '/');
        hashMap5.put(' ', ' ');
        hashMap5.put((char) 12288, ' ');
        hashMap5.put((char) 8288, ' ');
        hashMap5.put('.', '.');
        hashMap5.put((char) 65294, '.');
        A0A = Collections.unmodifiableMap(hashMap5);
        StringBuilder sb = new StringBuilder();
        Map map = A0B;
        sb.append(Arrays.toString(map.keySet().toArray()).replaceAll("[, \\[\\]]", ""));
        sb.append(Arrays.toString(map.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
        String obj = sb.toString();
        StringBuilder sb2 = new StringBuilder("\\p{Nd}{2}|[+＋]*+(?:[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*]*\\p{Nd}){3,}[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*");
        sb2.append(obj);
        sb2.append("\\p{Nd}");
        sb2.append("]*");
        String obj2 = sb2.toString();
        StringBuilder sb3 = new StringBuilder(",");
        sb3.append("xｘ#＃~～");
        String A02 = A02(sb3.toString());
        StringBuilder sb4 = new StringBuilder("(?:");
        sb4.append(A02);
        sb4.append(")$");
        A0H = Pattern.compile(sb4.toString(), 66);
        StringBuilder sb5 = new StringBuilder();
        sb5.append(obj2);
        sb5.append("(?:");
        sb5.append(A02);
        sb5.append(")?");
        A0T = Pattern.compile(sb5.toString(), 66);
    }

    public C20920wX(C22680zT r9) {
        HashMap hashMap = new HashMap(286);
        ArrayList arrayList = new ArrayList(25);
        arrayList.add("US");
        arrayList.add("AG");
        arrayList.add("AI");
        arrayList.add("AS");
        arrayList.add("BB");
        arrayList.add("BM");
        arrayList.add("BS");
        arrayList.add("CA");
        arrayList.add("DM");
        arrayList.add("DO");
        arrayList.add("GD");
        arrayList.add("GU");
        arrayList.add("JM");
        arrayList.add("KN");
        arrayList.add("KY");
        arrayList.add("LC");
        arrayList.add("MP");
        arrayList.add("MS");
        arrayList.add("PR");
        arrayList.add("SX");
        arrayList.add("TC");
        arrayList.add("TT");
        arrayList.add("VC");
        arrayList.add("VG");
        arrayList.add("VI");
        hashMap.put(1, arrayList);
        ArrayList arrayList2 = new ArrayList(2);
        arrayList2.add("RU");
        arrayList2.add("KZ");
        hashMap.put(7, arrayList2);
        ArrayList arrayList3 = new ArrayList(1);
        arrayList3.add("EG");
        hashMap.put(20, arrayList3);
        ArrayList arrayList4 = new ArrayList(1);
        arrayList4.add("ZA");
        hashMap.put(27, arrayList4);
        ArrayList arrayList5 = new ArrayList(1);
        arrayList5.add("GR");
        hashMap.put(30, arrayList5);
        ArrayList arrayList6 = new ArrayList(1);
        arrayList6.add("NL");
        hashMap.put(31, arrayList6);
        ArrayList arrayList7 = new ArrayList(1);
        arrayList7.add("BE");
        hashMap.put(32, arrayList7);
        ArrayList arrayList8 = new ArrayList(1);
        arrayList8.add("FR");
        hashMap.put(33, arrayList8);
        ArrayList arrayList9 = new ArrayList(1);
        arrayList9.add("ES");
        hashMap.put(34, arrayList9);
        ArrayList arrayList10 = new ArrayList(1);
        arrayList10.add("HU");
        hashMap.put(36, arrayList10);
        ArrayList arrayList11 = new ArrayList(1);
        arrayList11.add("IT");
        hashMap.put(39, arrayList11);
        ArrayList arrayList12 = new ArrayList(1);
        arrayList12.add("RO");
        hashMap.put(40, arrayList12);
        ArrayList arrayList13 = new ArrayList(1);
        arrayList13.add("CH");
        hashMap.put(41, arrayList13);
        ArrayList arrayList14 = new ArrayList(1);
        arrayList14.add("AT");
        hashMap.put(43, arrayList14);
        ArrayList arrayList15 = new ArrayList(4);
        arrayList15.add("GB");
        arrayList15.add("GG");
        arrayList15.add("IM");
        arrayList15.add("JE");
        hashMap.put(44, arrayList15);
        ArrayList arrayList16 = new ArrayList(1);
        arrayList16.add("DK");
        hashMap.put(45, arrayList16);
        ArrayList arrayList17 = new ArrayList(1);
        arrayList17.add("SE");
        hashMap.put(46, arrayList17);
        ArrayList arrayList18 = new ArrayList(2);
        arrayList18.add("NO");
        arrayList18.add("SJ");
        hashMap.put(47, arrayList18);
        ArrayList arrayList19 = new ArrayList(1);
        arrayList19.add("PL");
        hashMap.put(48, arrayList19);
        ArrayList arrayList20 = new ArrayList(1);
        arrayList20.add("DE");
        hashMap.put(49, arrayList20);
        ArrayList arrayList21 = new ArrayList(1);
        arrayList21.add("PE");
        hashMap.put(51, arrayList21);
        ArrayList arrayList22 = new ArrayList(1);
        arrayList22.add("MX");
        hashMap.put(52, arrayList22);
        ArrayList arrayList23 = new ArrayList(1);
        arrayList23.add("CU");
        hashMap.put(53, arrayList23);
        ArrayList arrayList24 = new ArrayList(1);
        arrayList24.add("AR");
        hashMap.put(54, arrayList24);
        ArrayList arrayList25 = new ArrayList(1);
        arrayList25.add("BR");
        hashMap.put(55, arrayList25);
        ArrayList arrayList26 = new ArrayList(1);
        arrayList26.add("CL");
        hashMap.put(56, arrayList26);
        ArrayList arrayList27 = new ArrayList(1);
        arrayList27.add("CO");
        hashMap.put(57, arrayList27);
        ArrayList arrayList28 = new ArrayList(1);
        arrayList28.add("VE");
        hashMap.put(58, arrayList28);
        ArrayList arrayList29 = new ArrayList(1);
        arrayList29.add("MY");
        hashMap.put(60, arrayList29);
        ArrayList arrayList30 = new ArrayList(3);
        arrayList30.add("AU");
        arrayList30.add("CC");
        arrayList30.add("CX");
        hashMap.put(61, arrayList30);
        ArrayList arrayList31 = new ArrayList(1);
        arrayList31.add("ID");
        hashMap.put(62, arrayList31);
        ArrayList arrayList32 = new ArrayList(1);
        arrayList32.add("PH");
        hashMap.put(63, arrayList32);
        ArrayList arrayList33 = new ArrayList(1);
        arrayList33.add("NZ");
        hashMap.put(64, arrayList33);
        ArrayList arrayList34 = new ArrayList(1);
        arrayList34.add("SG");
        hashMap.put(65, arrayList34);
        ArrayList arrayList35 = new ArrayList(1);
        arrayList35.add("TH");
        hashMap.put(66, arrayList35);
        ArrayList arrayList36 = new ArrayList(1);
        arrayList36.add("JP");
        hashMap.put(81, arrayList36);
        ArrayList arrayList37 = new ArrayList(1);
        arrayList37.add("KR");
        hashMap.put(82, arrayList37);
        ArrayList arrayList38 = new ArrayList(1);
        arrayList38.add("VN");
        hashMap.put(84, arrayList38);
        ArrayList arrayList39 = new ArrayList(1);
        arrayList39.add("CN");
        hashMap.put(86, arrayList39);
        ArrayList arrayList40 = new ArrayList(1);
        arrayList40.add("TR");
        hashMap.put(90, arrayList40);
        ArrayList arrayList41 = new ArrayList(1);
        arrayList41.add("IN");
        hashMap.put(91, arrayList41);
        ArrayList arrayList42 = new ArrayList(1);
        arrayList42.add("PK");
        hashMap.put(92, arrayList42);
        ArrayList arrayList43 = new ArrayList(1);
        arrayList43.add("AF");
        hashMap.put(93, arrayList43);
        ArrayList arrayList44 = new ArrayList(1);
        arrayList44.add("LK");
        hashMap.put(94, arrayList44);
        ArrayList arrayList45 = new ArrayList(1);
        arrayList45.add("MM");
        hashMap.put(95, arrayList45);
        ArrayList arrayList46 = new ArrayList(1);
        arrayList46.add("IR");
        hashMap.put(98, arrayList46);
        ArrayList arrayList47 = new ArrayList(1);
        arrayList47.add("SS");
        hashMap.put(211, arrayList47);
        ArrayList arrayList48 = new ArrayList(2);
        arrayList48.add("MA");
        arrayList48.add("EH");
        hashMap.put(212, arrayList48);
        ArrayList arrayList49 = new ArrayList(1);
        arrayList49.add("DZ");
        hashMap.put(213, arrayList49);
        ArrayList arrayList50 = new ArrayList(1);
        arrayList50.add("TN");
        hashMap.put(216, arrayList50);
        ArrayList arrayList51 = new ArrayList(1);
        arrayList51.add("LY");
        hashMap.put(218, arrayList51);
        ArrayList arrayList52 = new ArrayList(1);
        arrayList52.add("GM");
        hashMap.put(220, arrayList52);
        ArrayList arrayList53 = new ArrayList(1);
        arrayList53.add("SN");
        hashMap.put(221, arrayList53);
        ArrayList arrayList54 = new ArrayList(1);
        arrayList54.add("MR");
        hashMap.put(222, arrayList54);
        ArrayList arrayList55 = new ArrayList(1);
        arrayList55.add("ML");
        hashMap.put(223, arrayList55);
        ArrayList arrayList56 = new ArrayList(1);
        arrayList56.add("GN");
        hashMap.put(224, arrayList56);
        ArrayList arrayList57 = new ArrayList(1);
        arrayList57.add("CI");
        hashMap.put(225, arrayList57);
        ArrayList arrayList58 = new ArrayList(1);
        arrayList58.add("BF");
        hashMap.put(226, arrayList58);
        ArrayList arrayList59 = new ArrayList(1);
        arrayList59.add("NE");
        hashMap.put(227, arrayList59);
        ArrayList arrayList60 = new ArrayList(1);
        arrayList60.add("TG");
        hashMap.put(228, arrayList60);
        ArrayList arrayList61 = new ArrayList(1);
        arrayList61.add("BJ");
        hashMap.put(229, arrayList61);
        ArrayList arrayList62 = new ArrayList(1);
        arrayList62.add("MU");
        hashMap.put(230, arrayList62);
        ArrayList arrayList63 = new ArrayList(1);
        arrayList63.add("LR");
        hashMap.put(231, arrayList63);
        ArrayList arrayList64 = new ArrayList(1);
        arrayList64.add("SL");
        hashMap.put(232, arrayList64);
        ArrayList arrayList65 = new ArrayList(1);
        arrayList65.add("GH");
        hashMap.put(233, arrayList65);
        ArrayList arrayList66 = new ArrayList(1);
        arrayList66.add("NG");
        hashMap.put(234, arrayList66);
        ArrayList arrayList67 = new ArrayList(1);
        arrayList67.add("TD");
        hashMap.put(235, arrayList67);
        ArrayList arrayList68 = new ArrayList(1);
        arrayList68.add("CF");
        hashMap.put(236, arrayList68);
        ArrayList arrayList69 = new ArrayList(1);
        arrayList69.add("CM");
        hashMap.put(237, arrayList69);
        ArrayList arrayList70 = new ArrayList(1);
        arrayList70.add("CV");
        hashMap.put(238, arrayList70);
        ArrayList arrayList71 = new ArrayList(1);
        arrayList71.add("ST");
        hashMap.put(239, arrayList71);
        ArrayList arrayList72 = new ArrayList(1);
        arrayList72.add("GQ");
        hashMap.put(240, arrayList72);
        ArrayList arrayList73 = new ArrayList(1);
        arrayList73.add("GA");
        hashMap.put(241, arrayList73);
        ArrayList arrayList74 = new ArrayList(1);
        arrayList74.add("CG");
        hashMap.put(242, arrayList74);
        ArrayList arrayList75 = new ArrayList(1);
        arrayList75.add("CD");
        hashMap.put(243, arrayList75);
        ArrayList arrayList76 = new ArrayList(1);
        arrayList76.add("AO");
        hashMap.put(244, arrayList76);
        ArrayList arrayList77 = new ArrayList(1);
        arrayList77.add("GW");
        hashMap.put(245, arrayList77);
        ArrayList arrayList78 = new ArrayList(1);
        arrayList78.add("IO");
        hashMap.put(246, arrayList78);
        ArrayList arrayList79 = new ArrayList(1);
        arrayList79.add("AC");
        hashMap.put(247, arrayList79);
        ArrayList arrayList80 = new ArrayList(1);
        arrayList80.add("SC");
        hashMap.put(248, arrayList80);
        ArrayList arrayList81 = new ArrayList(1);
        arrayList81.add("SD");
        hashMap.put(249, arrayList81);
        ArrayList arrayList82 = new ArrayList(1);
        arrayList82.add("RW");
        hashMap.put(250, arrayList82);
        ArrayList arrayList83 = new ArrayList(1);
        arrayList83.add("ET");
        hashMap.put(251, arrayList83);
        ArrayList arrayList84 = new ArrayList(1);
        arrayList84.add("SO");
        hashMap.put(252, arrayList84);
        ArrayList arrayList85 = new ArrayList(1);
        arrayList85.add("DJ");
        hashMap.put(253, arrayList85);
        ArrayList arrayList86 = new ArrayList(1);
        arrayList86.add("KE");
        hashMap.put(254, arrayList86);
        ArrayList arrayList87 = new ArrayList(1);
        arrayList87.add("TZ");
        hashMap.put(255, arrayList87);
        ArrayList arrayList88 = new ArrayList(1);
        arrayList88.add("UG");
        hashMap.put(256, arrayList88);
        ArrayList arrayList89 = new ArrayList(1);
        arrayList89.add("BI");
        hashMap.put(257, arrayList89);
        ArrayList arrayList90 = new ArrayList(1);
        arrayList90.add("MZ");
        hashMap.put(258, arrayList90);
        ArrayList arrayList91 = new ArrayList(1);
        arrayList91.add("ZM");
        hashMap.put(260, arrayList91);
        ArrayList arrayList92 = new ArrayList(1);
        arrayList92.add("MG");
        hashMap.put(261, arrayList92);
        ArrayList arrayList93 = new ArrayList(2);
        arrayList93.add("RE");
        arrayList93.add("YT");
        hashMap.put(262, arrayList93);
        ArrayList arrayList94 = new ArrayList(1);
        arrayList94.add("ZW");
        hashMap.put(263, arrayList94);
        ArrayList arrayList95 = new ArrayList(1);
        arrayList95.add("NA");
        hashMap.put(264, arrayList95);
        ArrayList arrayList96 = new ArrayList(1);
        arrayList96.add("MW");
        hashMap.put(265, arrayList96);
        ArrayList arrayList97 = new ArrayList(1);
        arrayList97.add("LS");
        hashMap.put(266, arrayList97);
        ArrayList arrayList98 = new ArrayList(1);
        arrayList98.add("BW");
        hashMap.put(267, arrayList98);
        ArrayList arrayList99 = new ArrayList(1);
        arrayList99.add("SZ");
        hashMap.put(268, arrayList99);
        ArrayList arrayList100 = new ArrayList(1);
        arrayList100.add("KM");
        hashMap.put(269, arrayList100);
        ArrayList arrayList101 = new ArrayList(1);
        arrayList101.add("SH");
        hashMap.put(290, arrayList101);
        ArrayList arrayList102 = new ArrayList(1);
        arrayList102.add("ER");
        hashMap.put(291, arrayList102);
        ArrayList arrayList103 = new ArrayList(1);
        arrayList103.add("AW");
        hashMap.put(297, arrayList103);
        ArrayList arrayList104 = new ArrayList(1);
        arrayList104.add("FO");
        hashMap.put(298, arrayList104);
        ArrayList arrayList105 = new ArrayList(1);
        arrayList105.add("GL");
        hashMap.put(299, arrayList105);
        ArrayList arrayList106 = new ArrayList(1);
        arrayList106.add("GI");
        hashMap.put(350, arrayList106);
        ArrayList arrayList107 = new ArrayList(1);
        arrayList107.add("PT");
        hashMap.put(351, arrayList107);
        ArrayList arrayList108 = new ArrayList(1);
        arrayList108.add("LU");
        hashMap.put(352, arrayList108);
        ArrayList arrayList109 = new ArrayList(1);
        arrayList109.add("IE");
        hashMap.put(353, arrayList109);
        ArrayList arrayList110 = new ArrayList(1);
        arrayList110.add("IS");
        hashMap.put(354, arrayList110);
        ArrayList arrayList111 = new ArrayList(1);
        arrayList111.add("AL");
        hashMap.put(355, arrayList111);
        ArrayList arrayList112 = new ArrayList(1);
        arrayList112.add("MT");
        hashMap.put(356, arrayList112);
        ArrayList arrayList113 = new ArrayList(1);
        arrayList113.add("CY");
        hashMap.put(357, arrayList113);
        ArrayList arrayList114 = new ArrayList(2);
        arrayList114.add("FI");
        arrayList114.add("AX");
        hashMap.put(358, arrayList114);
        ArrayList arrayList115 = new ArrayList(1);
        arrayList115.add("BG");
        hashMap.put(359, arrayList115);
        ArrayList arrayList116 = new ArrayList(1);
        arrayList116.add("LT");
        hashMap.put(370, arrayList116);
        ArrayList arrayList117 = new ArrayList(1);
        arrayList117.add("LV");
        hashMap.put(371, arrayList117);
        ArrayList arrayList118 = new ArrayList(1);
        arrayList118.add("EE");
        hashMap.put(372, arrayList118);
        ArrayList arrayList119 = new ArrayList(1);
        arrayList119.add("MD");
        hashMap.put(373, arrayList119);
        ArrayList arrayList120 = new ArrayList(1);
        arrayList120.add("AM");
        hashMap.put(374, arrayList120);
        ArrayList arrayList121 = new ArrayList(1);
        arrayList121.add("BY");
        hashMap.put(375, arrayList121);
        ArrayList arrayList122 = new ArrayList(1);
        arrayList122.add("AD");
        hashMap.put(376, arrayList122);
        ArrayList arrayList123 = new ArrayList(1);
        arrayList123.add("MC");
        hashMap.put(377, arrayList123);
        ArrayList arrayList124 = new ArrayList(1);
        arrayList124.add("SM");
        hashMap.put(378, arrayList124);
        ArrayList arrayList125 = new ArrayList(1);
        arrayList125.add("VA");
        hashMap.put(379, arrayList125);
        ArrayList arrayList126 = new ArrayList(1);
        arrayList126.add("UA");
        hashMap.put(380, arrayList126);
        ArrayList arrayList127 = new ArrayList(1);
        arrayList127.add("RS");
        hashMap.put(381, arrayList127);
        ArrayList arrayList128 = new ArrayList(1);
        arrayList128.add("ME");
        hashMap.put(382, arrayList128);
        ArrayList arrayList129 = new ArrayList(1);
        arrayList129.add("HR");
        hashMap.put(385, arrayList129);
        ArrayList arrayList130 = new ArrayList(1);
        arrayList130.add("SI");
        hashMap.put(386, arrayList130);
        ArrayList arrayList131 = new ArrayList(1);
        arrayList131.add("BA");
        hashMap.put(387, arrayList131);
        ArrayList arrayList132 = new ArrayList(1);
        arrayList132.add("MK");
        hashMap.put(389, arrayList132);
        ArrayList arrayList133 = new ArrayList(1);
        arrayList133.add("CZ");
        hashMap.put(420, arrayList133);
        ArrayList arrayList134 = new ArrayList(1);
        arrayList134.add("SK");
        hashMap.put(421, arrayList134);
        ArrayList arrayList135 = new ArrayList(1);
        arrayList135.add("LI");
        hashMap.put(423, arrayList135);
        ArrayList arrayList136 = new ArrayList(1);
        arrayList136.add("FK");
        hashMap.put(500, arrayList136);
        ArrayList arrayList137 = new ArrayList(1);
        arrayList137.add("BZ");
        hashMap.put(501, arrayList137);
        ArrayList arrayList138 = new ArrayList(1);
        arrayList138.add("GT");
        hashMap.put(502, arrayList138);
        ArrayList arrayList139 = new ArrayList(1);
        arrayList139.add("SV");
        hashMap.put(503, arrayList139);
        ArrayList arrayList140 = new ArrayList(1);
        arrayList140.add("HN");
        hashMap.put(504, arrayList140);
        ArrayList arrayList141 = new ArrayList(1);
        arrayList141.add("NI");
        hashMap.put(505, arrayList141);
        ArrayList arrayList142 = new ArrayList(1);
        arrayList142.add("CR");
        hashMap.put(506, arrayList142);
        ArrayList arrayList143 = new ArrayList(1);
        arrayList143.add("PA");
        hashMap.put(507, arrayList143);
        ArrayList arrayList144 = new ArrayList(1);
        arrayList144.add("PM");
        hashMap.put(508, arrayList144);
        ArrayList arrayList145 = new ArrayList(1);
        arrayList145.add("HT");
        hashMap.put(509, arrayList145);
        ArrayList arrayList146 = new ArrayList(3);
        arrayList146.add("GP");
        arrayList146.add("BL");
        arrayList146.add("MF");
        hashMap.put(590, arrayList146);
        ArrayList arrayList147 = new ArrayList(1);
        arrayList147.add("BO");
        hashMap.put(591, arrayList147);
        ArrayList arrayList148 = new ArrayList(1);
        arrayList148.add("GY");
        hashMap.put(592, arrayList148);
        ArrayList arrayList149 = new ArrayList(1);
        arrayList149.add("EC");
        hashMap.put(593, arrayList149);
        ArrayList arrayList150 = new ArrayList(1);
        arrayList150.add("GF");
        hashMap.put(594, arrayList150);
        ArrayList arrayList151 = new ArrayList(1);
        arrayList151.add("PY");
        hashMap.put(595, arrayList151);
        ArrayList arrayList152 = new ArrayList(1);
        arrayList152.add("MQ");
        hashMap.put(596, arrayList152);
        ArrayList arrayList153 = new ArrayList(1);
        arrayList153.add("SR");
        hashMap.put(597, arrayList153);
        ArrayList arrayList154 = new ArrayList(1);
        arrayList154.add("UY");
        hashMap.put(598, arrayList154);
        ArrayList arrayList155 = new ArrayList(2);
        arrayList155.add("CW");
        arrayList155.add("BQ");
        hashMap.put(599, arrayList155);
        ArrayList arrayList156 = new ArrayList(1);
        arrayList156.add("TL");
        hashMap.put(670, arrayList156);
        ArrayList arrayList157 = new ArrayList(1);
        arrayList157.add("NF");
        hashMap.put(672, arrayList157);
        ArrayList arrayList158 = new ArrayList(1);
        arrayList158.add("BN");
        hashMap.put(673, arrayList158);
        ArrayList arrayList159 = new ArrayList(1);
        arrayList159.add("NR");
        hashMap.put(674, arrayList159);
        ArrayList arrayList160 = new ArrayList(1);
        arrayList160.add("PG");
        hashMap.put(675, arrayList160);
        ArrayList arrayList161 = new ArrayList(1);
        arrayList161.add("TO");
        hashMap.put(676, arrayList161);
        ArrayList arrayList162 = new ArrayList(1);
        arrayList162.add("SB");
        hashMap.put(677, arrayList162);
        ArrayList arrayList163 = new ArrayList(1);
        arrayList163.add("VU");
        hashMap.put(678, arrayList163);
        ArrayList arrayList164 = new ArrayList(1);
        arrayList164.add("FJ");
        hashMap.put(679, arrayList164);
        ArrayList arrayList165 = new ArrayList(1);
        arrayList165.add("PW");
        hashMap.put(680, arrayList165);
        ArrayList arrayList166 = new ArrayList(1);
        arrayList166.add("WF");
        hashMap.put(681, arrayList166);
        ArrayList arrayList167 = new ArrayList(1);
        arrayList167.add("CK");
        hashMap.put(682, arrayList167);
        ArrayList arrayList168 = new ArrayList(1);
        arrayList168.add("NU");
        hashMap.put(683, arrayList168);
        ArrayList arrayList169 = new ArrayList(1);
        arrayList169.add("WS");
        hashMap.put(685, arrayList169);
        ArrayList arrayList170 = new ArrayList(1);
        arrayList170.add("KI");
        hashMap.put(686, arrayList170);
        ArrayList arrayList171 = new ArrayList(1);
        arrayList171.add("NC");
        hashMap.put(687, arrayList171);
        ArrayList arrayList172 = new ArrayList(1);
        arrayList172.add("TV");
        hashMap.put(688, arrayList172);
        ArrayList arrayList173 = new ArrayList(1);
        arrayList173.add("PF");
        hashMap.put(689, arrayList173);
        ArrayList arrayList174 = new ArrayList(1);
        arrayList174.add("TK");
        hashMap.put(690, arrayList174);
        ArrayList arrayList175 = new ArrayList(1);
        arrayList175.add("FM");
        hashMap.put(691, arrayList175);
        ArrayList arrayList176 = new ArrayList(1);
        arrayList176.add("MH");
        hashMap.put(692, arrayList176);
        ArrayList arrayList177 = new ArrayList(1);
        arrayList177.add("001");
        hashMap.put(800, arrayList177);
        ArrayList arrayList178 = new ArrayList(1);
        arrayList178.add("001");
        hashMap.put(808, arrayList178);
        ArrayList arrayList179 = new ArrayList(1);
        arrayList179.add("KP");
        hashMap.put(850, arrayList179);
        ArrayList arrayList180 = new ArrayList(1);
        arrayList180.add("HK");
        hashMap.put(852, arrayList180);
        ArrayList arrayList181 = new ArrayList(1);
        arrayList181.add("MO");
        hashMap.put(853, arrayList181);
        ArrayList arrayList182 = new ArrayList(1);
        arrayList182.add("KH");
        hashMap.put(855, arrayList182);
        ArrayList arrayList183 = new ArrayList(1);
        arrayList183.add("LA");
        hashMap.put(856, arrayList183);
        ArrayList arrayList184 = new ArrayList(1);
        arrayList184.add("001");
        hashMap.put(870, arrayList184);
        ArrayList arrayList185 = new ArrayList(1);
        arrayList185.add("001");
        hashMap.put(878, arrayList185);
        ArrayList arrayList186 = new ArrayList(1);
        arrayList186.add("BD");
        hashMap.put(880, arrayList186);
        ArrayList arrayList187 = new ArrayList(1);
        arrayList187.add("001");
        hashMap.put(881, arrayList187);
        ArrayList arrayList188 = new ArrayList(1);
        arrayList188.add("001");
        hashMap.put(882, arrayList188);
        ArrayList arrayList189 = new ArrayList(1);
        arrayList189.add("001");
        hashMap.put(883, arrayList189);
        ArrayList arrayList190 = new ArrayList(1);
        arrayList190.add("TW");
        hashMap.put(886, arrayList190);
        ArrayList arrayList191 = new ArrayList(1);
        arrayList191.add("001");
        hashMap.put(888, arrayList191);
        ArrayList arrayList192 = new ArrayList(1);
        arrayList192.add("MV");
        hashMap.put(960, arrayList192);
        ArrayList arrayList193 = new ArrayList(1);
        arrayList193.add("LB");
        hashMap.put(961, arrayList193);
        ArrayList arrayList194 = new ArrayList(1);
        arrayList194.add("JO");
        hashMap.put(962, arrayList194);
        ArrayList arrayList195 = new ArrayList(1);
        arrayList195.add("SY");
        hashMap.put(963, arrayList195);
        ArrayList arrayList196 = new ArrayList(1);
        arrayList196.add("IQ");
        hashMap.put(964, arrayList196);
        ArrayList arrayList197 = new ArrayList(1);
        arrayList197.add("KW");
        hashMap.put(965, arrayList197);
        ArrayList arrayList198 = new ArrayList(1);
        arrayList198.add("SA");
        hashMap.put(966, arrayList198);
        ArrayList arrayList199 = new ArrayList(1);
        arrayList199.add("YE");
        hashMap.put(967, arrayList199);
        ArrayList arrayList200 = new ArrayList(1);
        arrayList200.add("OM");
        hashMap.put(968, arrayList200);
        ArrayList arrayList201 = new ArrayList(1);
        arrayList201.add("PS");
        hashMap.put(970, arrayList201);
        ArrayList arrayList202 = new ArrayList(1);
        arrayList202.add("AE");
        hashMap.put(971, arrayList202);
        ArrayList arrayList203 = new ArrayList(1);
        arrayList203.add("IL");
        hashMap.put(972, arrayList203);
        ArrayList arrayList204 = new ArrayList(1);
        arrayList204.add("BH");
        hashMap.put(973, arrayList204);
        ArrayList arrayList205 = new ArrayList(1);
        arrayList205.add("QA");
        hashMap.put(974, arrayList205);
        ArrayList arrayList206 = new ArrayList(1);
        arrayList206.add("BT");
        hashMap.put(975, arrayList206);
        ArrayList arrayList207 = new ArrayList(1);
        arrayList207.add("MN");
        hashMap.put(976, arrayList207);
        ArrayList arrayList208 = new ArrayList(1);
        arrayList208.add("NP");
        hashMap.put(977, arrayList208);
        ArrayList arrayList209 = new ArrayList(1);
        arrayList209.add("001");
        hashMap.put(979, arrayList209);
        ArrayList arrayList210 = new ArrayList(1);
        arrayList210.add("TJ");
        hashMap.put(992, arrayList210);
        ArrayList arrayList211 = new ArrayList(1);
        arrayList211.add("TM");
        hashMap.put(993, arrayList211);
        ArrayList arrayList212 = new ArrayList(1);
        arrayList212.add("AZ");
        hashMap.put(994, arrayList212);
        ArrayList arrayList213 = new ArrayList(1);
        arrayList213.add("GE");
        hashMap.put(995, arrayList213);
        ArrayList arrayList214 = new ArrayList(1);
        arrayList214.add("KG");
        hashMap.put(996, arrayList214);
        ArrayList arrayList215 = new ArrayList(1);
        arrayList215.add("UZ");
        hashMap.put(998, arrayList215);
        this.A01 = r9;
        this.A03 = hashMap;
        this.A02 = "/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto";
        for (Map.Entry entry : hashMap.entrySet()) {
            List list = (List) entry.getValue();
            if (list.size() != 1 || !"001".equals(list.get(0))) {
                this.A08.addAll(list);
            } else {
                this.A06.add(entry.getKey());
            }
        }
        if (this.A08.remove("001")) {
            A0E.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.A07.addAll((Collection) hashMap.get(1));
    }

    public static C20920wX A00() {
        if (A0V == null) {
            synchronized (C20920wX.class) {
                if (A0V == null) {
                    A0V = new C20920wX(C22680zT.A00());
                }
            }
        }
        return A0V;
    }

    public static String A01(C71133cR r4) {
        String str = "0";
        String str2 = "";
        if (r4.italianLeadingZero_) {
            str2 = str;
        }
        StringBuilder sb = new StringBuilder(str2);
        if (!r4.secondLeadingZero_) {
            str = "";
        }
        sb.append(str);
        sb.append(r4.nationalNumber_);
        return sb.toString();
    }

    public static String A02(String str) {
        StringBuilder sb = new StringBuilder(";ext=(\\p{Nd}{1,7})|[  \\t,]*(?:e?xt(?:ensi(?:ó?|ó))?n?|ｅ?ｘｔｎ?|[");
        sb.append(str);
        sb.append("]|int|anexo|ｉｎｔ)[:\\.．]?[  \\t,-]*");
        sb.append("(\\p{Nd}{1,7})");
        sb.append("#?|[- ]+(");
        sb.append("\\p{Nd}");
        sb.append("{1,5})#");
        return sb.toString();
    }

    public static String A03(String str) {
        StringBuilder sb = new StringBuilder(str.length());
        for (char c : str.toCharArray()) {
            int digit = Character.digit(c, 10);
            if (digit != -1) {
                sb.append(digit);
            }
        }
        return sb.toString();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static final void A04(EnumC868649g r2, StringBuilder sb, int i) {
        switch (r2.ordinal()) {
            case 0:
                break;
            case 1:
                sb.insert(0, " ");
                break;
            case 2:
            default:
                return;
            case 3:
                sb.insert(0, "-");
                sb.insert(0, i);
                sb.insert(0, '+');
                sb.insert(0, "tel:");
                return;
        }
        sb.insert(0, i);
        sb.insert(0, '+');
    }

    public static void A05(StringBuilder sb) {
        String str;
        String obj = sb.toString();
        if (A0S.matcher(obj).matches()) {
            Map map = A0C;
            int length = obj.length();
            StringBuilder sb2 = new StringBuilder(length);
            for (int i = 0; i < length; i++) {
                Object obj2 = map.get(Character.valueOf(Character.toUpperCase(obj.charAt(i))));
                if (obj2 != null) {
                    sb2.append(obj2);
                }
            }
            str = sb2.toString();
        } else {
            str = A03(obj);
        }
        sb.replace(0, sb.length(), str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f8, code lost:
        if (r1 == X.AnonymousClass49O.A01) goto L_0x00fa;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x009f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A06(X.C71123cQ r9, X.C71133cR r10, java.lang.String r11, java.lang.StringBuilder r12, boolean r13) {
        /*
        // Method dump skipped, instructions count: 286
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20920wX.A06(X.3cQ, X.3cR, java.lang.String, java.lang.StringBuilder, boolean):int");
    }

    public int A07(StringBuilder sb, StringBuilder sb2) {
        if (sb.length() != 0 && sb.charAt(0) != '0') {
            int length = sb.length();
            int i = 1;
            while (i <= length) {
                int parseInt = Integer.parseInt(sb.substring(0, i));
                if (!this.A03.containsKey(Integer.valueOf(parseInt))) {
                    i++;
                    if (i > 3) {
                        break;
                    }
                } else {
                    sb2.append(sb.substring(i));
                    return parseInt;
                }
            }
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0080, code lost:
        if (r2 == r1) goto L_0x0082;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass49Q A08(X.C71133cR r7, X.C71133cR r8) {
        /*
            r6 = this;
            X.3cR r4 = new X.3cR
            r4.<init>()
            r4.A00(r7)
            X.3cR r3 = new X.3cR
            r3.<init>()
            r3.A00(r8)
            r5 = 0
            r4.hasRawInput = r5
            java.lang.String r2 = ""
            r4.rawInput_ = r2
            r4.hasCountryCodeSource = r5
            X.4AP r0 = X.AnonymousClass4AP.FROM_NUMBER_WITH_PLUS_SIGN
            r4.countryCodeSource_ = r0
            r4.hasPreferredDomesticCarrierCode = r5
            r4.preferredDomesticCarrierCode_ = r2
            r3.hasRawInput = r5
            r3.rawInput_ = r2
            r3.hasCountryCodeSource = r5
            r3.countryCodeSource_ = r0
            r3.hasPreferredDomesticCarrierCode = r5
            r3.preferredDomesticCarrierCode_ = r2
            boolean r0 = r4.hasExtension
            if (r0 == 0) goto L_0x003d
            java.lang.String r0 = r4.extension_
            int r0 = r0.length()
            if (r0 != 0) goto L_0x003d
            r4.hasExtension = r5
            r4.extension_ = r2
        L_0x003d:
            boolean r1 = r3.hasExtension
            if (r1 == 0) goto L_0x004e
            java.lang.String r0 = r3.extension_
            int r0 = r0.length()
            if (r0 != 0) goto L_0x004e
            r3.hasExtension = r5
            r1 = 0
            r3.extension_ = r2
        L_0x004e:
            boolean r0 = r4.hasExtension
            if (r0 == 0) goto L_0x0061
            if (r1 == 0) goto L_0x0061
            java.lang.String r1 = r4.extension_
            java.lang.String r0 = r3.extension_
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0061
        L_0x005e:
            X.49Q r0 = X.AnonymousClass49Q.A02
            return r0
        L_0x0061:
            int r2 = r4.countryCode_
            int r1 = r3.countryCode_
            if (r2 == 0) goto L_0x0072
            if (r1 == 0) goto L_0x0072
            boolean r0 = r4.A01(r3)
            if (r0 == 0) goto L_0x0080
            X.49Q r0 = X.AnonymousClass49Q.A00
            return r0
        L_0x0072:
            r0 = 1
            r4.hasCountryCode = r0
            r4.countryCode_ = r1
            boolean r0 = r4.A01(r3)
            if (r0 == 0) goto L_0x0082
            X.49Q r0 = X.AnonymousClass49Q.A03
            return r0
        L_0x0080:
            if (r2 != r1) goto L_0x005e
        L_0x0082:
            long r0 = r4.nationalNumber_
            java.lang.String r2 = java.lang.String.valueOf(r0)
            long r0 = r3.nationalNumber_
            java.lang.String r1 = java.lang.String.valueOf(r0)
            boolean r0 = r2.endsWith(r1)
            if (r0 != 0) goto L_0x009a
            boolean r0 = r1.endsWith(r2)
            if (r0 == 0) goto L_0x005e
        L_0x009a:
            X.49Q r0 = X.AnonymousClass49Q.A04
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20920wX.A08(X.3cR, X.3cR):X.49Q");
    }

    public AnonymousClass49Q A09(C71133cR r10, String str) {
        try {
            return A08(r10, A0E(str, "ZZ"));
        } catch (AnonymousClass4C8 e) {
            if (e.errorType == EnumC87074Ac.INVALID_COUNTRY_CODE) {
                String A0F2 = A0F(r10.countryCode_);
                try {
                    if (!A0F2.equals("ZZ")) {
                        AnonymousClass49Q A08 = A08(r10, A0E(str, A0F2));
                        return A08 == AnonymousClass49Q.A00 ? AnonymousClass49Q.A03 : A08;
                    }
                    C71133cR r4 = new C71133cR();
                    A0H(r4, str, null, false, false);
                    return A08(r10, r4);
                } catch (AnonymousClass4C8 unused) {
                    return AnonymousClass49Q.A01;
                }
            }
            return AnonymousClass49Q.A01;
        }
    }

    public final EnumC868849i A0A(C71123cQ r3, String str) {
        AnonymousClass5BG r1 = r3.generalDesc_;
        if (r1.hasNationalNumberPattern && A0K(r1, str)) {
            if (A0K(r3.personalNumber_, str)) {
                return EnumC868849i.A05;
            }
            if (A0K(r3.tollFree_, str)) {
                return EnumC868849i.A08;
            }
            if (A0K(r3.sharedCost_, str)) {
                return EnumC868849i.A07;
            }
            if (A0K(r3.voip_, str)) {
                return EnumC868849i.A0C;
            }
            if (A0K(r3.premiumRate_, str)) {
                return EnumC868849i.A06;
            }
            if (A0K(r3.pager_, str)) {
                return EnumC868849i.A04;
            }
            if (A0K(r3.uan_, str)) {
                return EnumC868849i.A09;
            }
            if (A0K(r3.voicemail_, str)) {
                return EnumC868849i.A0B;
            }
            boolean A0K2 = A0K(r3.fixedLine_, str);
            boolean z = r3.sameMobileAndFixedLinePattern_;
            if (A0K2) {
                if (z || A0K(r3.mobile_, str)) {
                    return EnumC868849i.A02;
                }
                return EnumC868849i.A01;
            } else if (!z && A0K(r3.mobile_, str)) {
                return EnumC868849i.A03;
            }
        }
        return EnumC868849i.A0A;
    }

    public C71113cP A0B(String str, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C71113cP r3 = (C71113cP) it.next();
            int size = r3.leadingDigitsPattern_.size();
            if (size != 0) {
                if (!this.A00.A00((String) r3.leadingDigitsPattern_.get(size - 1)).matcher(str).lookingAt()) {
                    continue;
                }
            }
            if (this.A00.A00(r3.pattern_).matcher(str).matches()) {
                return r3;
            }
        }
        return null;
    }

    public C71123cQ A0C(int i) {
        Map map = this.A04;
        synchronized (map) {
            Map map2 = this.A03;
            Integer valueOf = Integer.valueOf(i);
            if (!map2.containsKey(valueOf)) {
                return null;
            }
            if (!map.containsKey(valueOf)) {
                A0I("001", i);
            }
            return (C71123cQ) map.get(valueOf);
        }
    }

    public C71123cQ A0D(String str) {
        if (str == null || !this.A08.contains(str)) {
            return null;
        }
        Map map = this.A05;
        C71123cQ r0 = (C71123cQ) map.get(str);
        if (r0 != null) {
            return r0;
        }
        synchronized (map) {
            if (!map.containsKey(str)) {
                A0I(str, 0);
            }
        }
        return (C71123cQ) map.get(str);
    }

    public C71133cR A0E(String str, String str2) {
        C71133cR r1 = new C71133cR();
        A0H(r1, str, str2, false, true);
        return r1;
    }

    public String A0F(int i) {
        List list = (List) this.A03.get(Integer.valueOf(i));
        if (list == null) {
            return "ZZ";
        }
        return (String) list.get(0);
    }

    public String A0G(EnumC868649g r9, C71133cR r10) {
        C71123cQ A0D2;
        List list;
        String str;
        if (r10.nationalNumber_ == 0 && r10.hasRawInput) {
            String str2 = r10.rawInput_;
            if (str2.length() > 0) {
                return str2;
            }
        }
        StringBuilder sb = new StringBuilder(20);
        sb.setLength(0);
        int i = r10.countryCode_;
        String A01 = A01(r10);
        EnumC868649g r0 = EnumC868649g.E164;
        if (r9 == r0) {
            sb.append(A01);
            A04(r0, sb, i);
        } else if (!this.A03.containsKey(Integer.valueOf(i))) {
            sb.append(A01);
        } else {
            String A0F2 = A0F(i);
            if ("001".equals(A0F2)) {
                A0D2 = A0C(i);
            } else {
                A0D2 = A0D(A0F2);
            }
            AnonymousClass009.A05(A0D2);
            if (A0D2.intlNumberFormat_.size() == 0 || r9 == EnumC868649g.NATIONAL) {
                list = A0D2.numberFormat_;
            } else {
                list = A0D2.intlNumberFormat_;
            }
            C71113cP A0B2 = A0B(A01, list);
            if (A0B2 != null) {
                String str3 = A0B2.format_;
                Matcher matcher = this.A00.A00(A0B2.pattern_).matcher(A01);
                EnumC868649g r02 = EnumC868649g.NATIONAL;
                String str4 = A0B2.nationalPrefixFormattingRule_;
                if (r9 != r02 || str4 == null || str4.length() <= 0) {
                    A01 = matcher.replaceAll(str3);
                } else {
                    A01 = matcher.replaceAll(A0K.matcher(str3).replaceFirst(str4));
                }
                if (r9 == EnumC868649g.RFC3966) {
                    Matcher matcher2 = A0P.matcher(A01);
                    if (matcher2.lookingAt()) {
                        A01 = matcher2.replaceFirst("");
                    }
                    A01 = matcher2.reset(A01).replaceAll("-");
                }
            }
            sb.append(A01);
            if (r10.hasExtension) {
                String str5 = r10.extension_;
                if (str5.length() > 0) {
                    if (r9 == EnumC868649g.RFC3966) {
                        str = ";ext=";
                    } else if (A0D2.hasPreferredExtnPrefix) {
                        str = A0D2.preferredExtnPrefix_;
                    } else {
                        str = " ext. ";
                    }
                    sb.append(str);
                    sb.append(str5);
                }
            }
            A04(r9, sb, i);
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:107:0x0227  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01cb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0H(X.C71133cR r13, java.lang.String r14, java.lang.String r15, boolean r16, boolean r17) {
        /*
        // Method dump skipped, instructions count: 601
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20920wX.A0H(X.3cR, java.lang.String, java.lang.String, boolean, boolean):void");
    }

    public void A0I(String str, int i) {
        boolean equals = "001".equals(str);
        AnonymousClass1ND A01 = this.A01.A01(str);
        if (A01 != null) {
            C71123cQ r3 = new C71123cQ();
            String str2 = A01.A03;
            r3.hasId = true;
            r3.id_ = str2;
            int i2 = A01.A00;
            r3.hasCountryCode = true;
            r3.countryCode_ = i2;
            String str3 = A01.A02;
            r3.hasInternationalPrefix = true;
            r3.internationalPrefix_ = str3;
            LinkedList linkedList = new LinkedList();
            String[] strArr = A01.A0A;
            if (strArr != null) {
                for (int i3 = 0; i3 < strArr.length; i3++) {
                    C71113cP r9 = new C71113cP();
                    String str4 = strArr[i3];
                    r9.hasPattern = true;
                    r9.pattern_ = str4;
                    String str5 = A01.A08[i3];
                    r9.hasFormat = true;
                    r9.format_ = str5;
                    String[] strArr2 = A01.A09;
                    if (strArr2 != null && i3 < strArr2.length && strArr2[i3] != null && !strArr2[i3].equals("N/A")) {
                        String[] split = TextUtils.split(strArr2[i3], "#");
                        for (String str6 : split) {
                            r9.leadingDigitsPattern_.add(str6);
                        }
                    }
                    r3.intlNumberFormat_.add(r9);
                    r3.numberFormat_.add(r9);
                    StringBuilder sb = new StringBuilder("(");
                    sb.append(r9.pattern_);
                    sb.append(")");
                    linkedList.add(sb.toString());
                }
            }
            AnonymousClass5BG r2 = new AnonymousClass5BG();
            String join = TextUtils.join("|", linkedList);
            r2.hasNationalNumberPattern = true;
            r2.nationalNumberPattern_ = join;
            r2.hasPossibleNumberPattern = true;
            r2.possibleNumberPattern_ = join;
            r3.hasGeneralDesc = true;
            r3.generalDesc_ = r2;
            r3.hasPersonalNumber = true;
            r3.personalNumber_ = r2;
            if (equals) {
                this.A04.put(Integer.valueOf(i), r3);
            } else {
                this.A05.put(str, r3);
            }
        } else {
            StringBuilder sb2 = new StringBuilder("empty metadata: ");
            sb2.append(str);
            throw new RuntimeException(sb2.toString());
        }
    }

    public boolean A0J(C71123cQ r11, StringBuilder sb, StringBuilder sb2) {
        int length = sb.length();
        String str = r11.nationalPrefixForParsing_;
        if (!(length == 0 || str.length() == 0)) {
            C92134Ur r2 = this.A00;
            Matcher matcher = r2.A00(str).matcher(sb);
            if (matcher.lookingAt()) {
                Pattern A00 = r2.A00(r11.generalDesc_.nationalNumberPattern_);
                boolean matches = A00.matcher(sb).matches();
                int groupCount = matcher.groupCount();
                String str2 = r11.nationalPrefixTransformRule_;
                if (str2 != null && str2.length() != 0 && matcher.group(groupCount) != null) {
                    StringBuilder sb3 = new StringBuilder(sb);
                    sb3.replace(0, length, matcher.replaceFirst(str2));
                    if (!matches || A00.matcher(sb3.toString()).matches()) {
                        if (sb2 != null && groupCount > 1) {
                            sb2.append(matcher.group(1));
                        }
                        sb.replace(0, sb.length(), sb3.toString());
                        return true;
                    }
                } else if (matches && !A00.matcher(sb.substring(matcher.end())).matches()) {
                    return false;
                } else {
                    if (!(sb2 == null || groupCount <= 0 || matcher.group(groupCount) == null)) {
                        sb2.append(matcher.group(1));
                    }
                    sb.delete(0, matcher.end());
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean A0K(AnonymousClass5BG r4, String str) {
        C92134Ur r1 = this.A00;
        return r1.A00(r4.possibleNumberPattern_).matcher(str).matches() && r1.A00(r4.nationalNumberPattern_).matcher(str).matches();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004d, code lost:
        if (r1 != r0.countryCode_) goto L_0x004f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0L(X.C71133cR r7) {
        /*
            r6 = this;
            int r5 = r7.countryCode_
            java.util.Map r1 = r6.A03
            java.lang.Integer r0 = java.lang.Integer.valueOf(r5)
            java.lang.Object r2 = r1.get(r0)
            java.util.List r2 = (java.util.List) r2
            if (r2 != 0) goto L_0x0075
            java.lang.String r4 = A01(r7)
            java.util.logging.Logger r3 = X.C20920wX.A0E
            java.util.logging.Level r2 = java.util.logging.Level.WARNING
            java.lang.String r0 = "Missing/invalid country_code ("
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r5)
            java.lang.String r0 = ") for number "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = r1.toString()
            r3.log(r2, r0)
        L_0x0031:
            r5 = 0
        L_0x0032:
            int r1 = r7.countryCode_
            java.lang.String r0 = "001"
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0070
            X.3cQ r3 = r6.A0C(r1)
        L_0x0040:
            r2 = 0
            if (r3 == 0) goto L_0x004f
            if (r0 != 0) goto L_0x0050
            X.3cQ r0 = r6.A0D(r5)
            if (r0 == 0) goto L_0x00c1
            int r0 = r0.countryCode_
            if (r1 == r0) goto L_0x0050
        L_0x004f:
            return r2
        L_0x0050:
            X.5BG r0 = r3.generalDesc_
            java.lang.String r1 = A01(r7)
            boolean r0 = r0.hasNationalNumberPattern
            if (r0 != 0) goto L_0x0067
            int r1 = r1.length()
            r0 = 2
            if (r1 <= r0) goto L_0x004f
            r0 = 16
            if (r1 > r0) goto L_0x004f
        L_0x0065:
            r2 = 1
            return r2
        L_0x0067:
            X.49i r1 = r6.A0A(r3, r1)
            X.49i r0 = X.EnumC868849i.A0A
            if (r1 == r0) goto L_0x004f
            goto L_0x0065
        L_0x0070:
            X.3cQ r3 = r6.A0D(r5)
            goto L_0x0040
        L_0x0075:
            int r1 = r2.size()
            r0 = 1
            if (r1 != r0) goto L_0x0084
            r0 = 0
            java.lang.Object r5 = r2.get(r0)
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x0032
        L_0x0084:
            java.lang.String r4 = A01(r7)
            java.util.Iterator r3 = r2.iterator()
        L_0x008c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0031
            java.lang.Object r5 = r3.next()
            java.lang.String r5 = (java.lang.String) r5
            X.3cQ r2 = r6.A0D(r5)
            X.AnonymousClass009.A05(r2)
            boolean r0 = r2.hasLeadingDigits
            if (r0 == 0) goto L_0x00b7
            X.4Ur r1 = r6.A00
            java.lang.String r0 = r2.leadingDigits_
            java.util.regex.Pattern r0 = r1.A00(r0)
            java.util.regex.Matcher r0 = r0.matcher(r4)
            boolean r0 = r0.lookingAt()
            if (r0 == 0) goto L_0x008c
            goto L_0x0032
        L_0x00b7:
            X.49i r1 = r6.A0A(r2, r4)
            X.49i r0 = X.EnumC868849i.A0A
            if (r1 == r0) goto L_0x008c
            goto L_0x0032
        L_0x00c1:
            java.lang.String r1 = "Invalid region code: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r5)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20920wX.A0L(X.3cR):boolean");
    }
}
