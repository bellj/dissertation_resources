package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.5G3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G3 implements AnonymousClass20J {
    public int A00;
    public int A01;
    public AnonymousClass5XE A02;
    public byte[] A03;
    public byte[] A04;
    public byte[] A05;
    public byte[] A06;
    public byte[] A07;
    public byte[] A08;

    @Override // X.AnonymousClass20J
    public int AE2() {
        return this.A01;
    }

    @Override // X.AnonymousClass20J
    public void AfG(byte b) {
        int i = this.A00;
        byte[] bArr = this.A06;
        if (i == bArr.length) {
            this.A02.AZY(bArr, this.A07, 0, 0);
            this.A00 = 0;
            i = 0;
        }
        this.A00 = i + 1;
        bArr[i] = b;
    }

    @Override // X.AnonymousClass20J
    public void reset() {
        int i = 0;
        while (true) {
            byte[] bArr = this.A06;
            if (i < bArr.length) {
                bArr[i] = 0;
                i++;
            } else {
                this.A00 = 0;
                this.A02.reset();
                return;
            }
        }
    }

    public AnonymousClass5G3(AnonymousClass5XE r5) {
        int AAt = r5.AAt();
        int i = AAt << 3;
        if (i % 8 != 0) {
            throw C12970iu.A0f("MAC size must be multiple of 8");
        } else if (i <= i) {
            this.A02 = new C112955Fl(r5);
            this.A01 = i >> 3;
            int i2 = 135;
            switch (i) {
                case 64:
                case 320:
                    i2 = 27;
                    break;
                case 128:
                case 192:
                    break;
                case 160:
                    i2 = 45;
                    break;
                case 224:
                    i2 = 777;
                    break;
                case 256:
                    i2 = 1061;
                    break;
                case 384:
                    i2 = 4109;
                    break;
                case 448:
                    i2 = 2129;
                    break;
                case 512:
                    i2 = 293;
                    break;
                case 768:
                    i2 = 655377;
                    break;
                case EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH /* 1024 */:
                    i2 = 524355;
                    break;
                case EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH /* 2048 */:
                    i2 = 548865;
                    break;
                default:
                    throw C12970iu.A0f(C12960it.A0W(i, "Unknown block size for CMAC: "));
            }
            byte[] bArr = new byte[4];
            AbstractC95434di.A01(bArr, i2, 0);
            this.A08 = bArr;
            this.A07 = new byte[AAt];
            this.A06 = new byte[AAt];
            this.A05 = new byte[AAt];
            this.A00 = 0;
        } else {
            throw C12970iu.A0f(C12960it.A0W(i, "MAC size must be less or equal to "));
        }
    }

    public final byte[] A00(byte[] bArr) {
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        int i = length;
        int i2 = 0;
        while (true) {
            i--;
            if (i >= 0) {
                int i3 = bArr[i] & 255;
                C72463ee.A0O(i2, bArr2, i3 << 1, i);
                i2 = (i3 >>> 7) & 1;
            } else {
                int i4 = (-i2) & 255;
                int i5 = length - 3;
                byte b = bArr2[i5];
                byte[] bArr3 = this.A08;
                C72463ee.A0P(b, bArr2, bArr3[1] & i4, i5);
                int i6 = length - 2;
                C72463ee.A0P(bArr3[2] & i4, bArr2, bArr2[i6], i6);
                int i7 = length - 1;
                C72463ee.A0P(i4 & bArr3[3], bArr2, bArr2[i7], i7);
                return bArr2;
            }
        }
    }

    @Override // X.AnonymousClass20J
    public int A97(byte[] bArr, int i) {
        byte[] bArr2;
        AnonymousClass5XE r5 = this.A02;
        int AAt = r5.AAt();
        int i2 = this.A00;
        if (i2 == AAt) {
            bArr2 = this.A03;
        } else {
            new AnonymousClass5GC().A5m(this.A06, i2);
            bArr2 = this.A04;
        }
        int i3 = 0;
        while (true) {
            byte[] bArr3 = this.A07;
            if (i3 < bArr3.length) {
                byte[] bArr4 = this.A06;
                i3 = C72453ed.A0Q(bArr2, bArr4, i3, bArr4[i3]);
            } else {
                r5.AZY(this.A06, bArr3, 0, 0);
                int i4 = this.A01;
                System.arraycopy(bArr3, 0, bArr, i, i4);
                reset();
                return i4;
            }
        }
    }

    @Override // X.AnonymousClass20J
    public void AIc(AnonymousClass20L r5) {
        if (r5 == null || (r5 instanceof AnonymousClass20K)) {
            AnonymousClass5XE r3 = this.A02;
            r3.AIf(r5, true);
            byte[] bArr = this.A05;
            byte[] bArr2 = new byte[bArr.length];
            r3.AZY(bArr, bArr2, 0, 0);
            byte[] A00 = A00(bArr2);
            this.A03 = A00;
            this.A04 = A00(A00);
            reset();
            return;
        }
        throw C12970iu.A0f("CMac mode only permits key to be set.");
    }

    @Override // X.AnonymousClass20J
    public void update(byte[] bArr, int i, int i2) {
        if (i2 >= 0) {
            AnonymousClass5XE r5 = this.A02;
            int AAt = r5.AAt();
            int i3 = this.A00;
            int i4 = AAt - i3;
            if (i2 > i4) {
                byte[] bArr2 = this.A06;
                System.arraycopy(bArr, i, bArr2, i3, i4);
                byte[] bArr3 = this.A07;
                r5.AZY(bArr2, bArr3, 0, 0);
                this.A00 = 0;
                i2 -= i4;
                i += i4;
                while (i2 > AAt) {
                    r5.AZY(bArr, bArr3, i, 0);
                    i2 -= AAt;
                    i += AAt;
                }
            }
            System.arraycopy(bArr, i, this.A06, this.A00, i2);
            this.A00 += i2;
            return;
        }
        throw C12970iu.A0f("Can't have a negative input length!");
    }
}
