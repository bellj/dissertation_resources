package X;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.0Zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07620Zm implements AbstractC11960h9 {
    public static final String A0A = C06390Tk.A01("SystemAlarmDispatcher");
    public Intent A00;
    public AbstractC11420gG A01;
    public final Context A02;
    public final Handler A03;
    public final C07650Zp A04;
    public final AnonymousClass022 A05;
    public final C07610Zl A06;
    public final AnonymousClass0SJ A07 = new AnonymousClass0SJ();
    public final AbstractC11500gO A08;
    public final List A09;

    public C07620Zm(Context context) {
        Context applicationContext = context.getApplicationContext();
        this.A02 = applicationContext;
        this.A06 = new C07610Zl(applicationContext);
        AnonymousClass022 A00 = AnonymousClass022.A00(context);
        this.A05 = A00;
        C07650Zp r1 = A00.A03;
        this.A04 = r1;
        this.A08 = A00.A06;
        r1.A02(this);
        this.A09 = new ArrayList();
        this.A00 = null;
        this.A03 = new Handler(Looper.getMainLooper());
    }

    public void A00() {
        C06390Tk.A00().A02(A0A, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.A04.A03(this);
        ScheduledExecutorService scheduledExecutorService = this.A07.A03;
        if (!scheduledExecutorService.isShutdown()) {
            scheduledExecutorService.shutdownNow();
        }
        this.A01 = null;
    }

    public final void A01() {
        if (this.A03.getLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Needs to be invoked on the main thread.");
        }
    }

    public final void A02() {
        A01();
        PowerManager.WakeLock A00 = AnonymousClass0RM.A00(this.A02, "ProcessCommand");
        try {
            A00.acquire();
            AbstractC11500gO r0 = this.A05.A06;
            ((C07760a2) r0).A01.execute(new RunnableC09340ck(this));
        } finally {
            A00.release();
        }
    }

    public void A03(Intent intent, int i) {
        boolean z;
        C06390Tk A00 = C06390Tk.A00();
        String str = A0A;
        boolean z2 = false;
        A00.A02(str, String.format("Adding command %s (%s)", intent, Integer.valueOf(i)), new Throwable[0]);
        A01();
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            C06390Tk.A00().A05(str, "Unknown command. Ignoring", new Throwable[0]);
            return;
        }
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            A01();
            List list = this.A09;
            synchronized (list) {
                Iterator it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if ("ACTION_CONSTRAINTS_CHANGED".equals(((Intent) it.next()).getAction())) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
            }
            if (z) {
                return;
            }
        }
        intent.putExtra("KEY_START_ID", i);
        List list2 = this.A09;
        synchronized (list2) {
            if (!list2.isEmpty()) {
                z2 = true;
            }
            list2.add(intent);
            if (!z2) {
                A02();
            }
        }
    }

    @Override // X.AbstractC11960h9
    public void AQ1(String str, boolean z) {
        Intent intent = new Intent(this.A02, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        this.A03.post(new RunnableC09950dm(intent, this, 0));
    }
}
