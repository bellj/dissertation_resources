package X;

/* renamed from: X.1yc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44261yc {
    public static final String A00;
    public static final String A01;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        String str = C16500p8.A00;
        sb.append(AnonymousClass1Ux.A01("message", str));
        sb.append(" FROM ");
        sb.append("message_view AS message");
        sb.append(" JOIN ");
        sb.append("status AS status_list");
        sb.append(" ON ");
        sb.append("status_list.jid_row_id = message.sender_jid_row_id");
        sb.append(" WHERE ");
        sb.append("message.chat_row_id = ?");
        sb.append(" AND ");
        sb.append("message.from_me = 0");
        sb.append(" AND ");
        sb.append("status_list.last_read_message_table_id >= message._id");
        sb.append(" AND ");
        sb.append("status_list.last_read_receipt_sent_message_table_id < message._id");
        sb.append(" AND ");
        sb.append("status_list.last_read_receipt_sent_message_table_id > 0");
        sb.append(" AND ");
        sb.append("message.message_type != 15");
        sb.append(" ORDER BY message.");
        sb.append("_id");
        sb.append(" DESC LIMIT 4096");
        A00 = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT ");
        sb2.append(AnonymousClass1Ux.A01("message", str));
        sb2.append(" FROM ");
        sb2.append("message_view AS message");
        sb2.append(" JOIN ");
        sb2.append("jid AS jid");
        sb2.append(" ON jid.raw_string = message.sender_jid_raw_string");
        sb2.append(" JOIN ");
        sb2.append("status AS status_list");
        sb2.append(" ON ");
        sb2.append("status_list.jid_row_id = jid._id");
        sb2.append(" WHERE ");
        sb2.append("message.chat_row_id = ?");
        sb2.append(" AND ");
        sb2.append("message.from_me = 0");
        sb2.append(" AND ");
        sb2.append("status_list.last_read_message_table_id >= message._id");
        sb2.append(" AND ");
        sb2.append("status_list.last_read_receipt_sent_message_table_id < message._id");
        sb2.append(" AND ");
        sb2.append("status_list.last_read_receipt_sent_message_table_id > 0");
        sb2.append(" AND ");
        sb2.append("message.message_type != 15");
        sb2.append(" ORDER BY message.");
        sb2.append("_id");
        sb2.append(" DESC LIMIT 4096");
        A01 = sb2.toString();
    }
}
