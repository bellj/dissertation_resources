package X;

import android.view.MenuItem;
import com.whatsapp.documentpicker.DocumentPickerActivity;

/* renamed from: X.4mU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class MenuItem$OnActionExpandListenerC100794mU implements MenuItem.OnActionExpandListener {
    public final /* synthetic */ DocumentPickerActivity A00;

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return true;
    }

    public MenuItem$OnActionExpandListenerC100794mU(DocumentPickerActivity documentPickerActivity) {
        this.A00 = documentPickerActivity;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        DocumentPickerActivity documentPickerActivity = this.A00;
        documentPickerActivity.A0H = null;
        DocumentPickerActivity.A02(documentPickerActivity);
        return true;
    }
}
