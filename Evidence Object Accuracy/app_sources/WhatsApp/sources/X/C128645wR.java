package X;

import android.app.Activity;
import com.whatsapp.payments.receiver.IndiaUpiPayIntentReceiverActivity;

/* renamed from: X.5wR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128645wR {
    public final C21860y6 A00;

    public C128645wR(C21860y6 r1) {
        this.A00 = r1;
    }

    public void A00(Activity activity) {
        boolean z;
        C21860y6 r1 = this.A00;
        if (r1.A0B()) {
            z = true;
        } else {
            r1.A0C();
            z = false;
        }
        AnonymousClass01U.A01(activity, IndiaUpiPayIntentReceiverActivity.class, z);
    }
}
