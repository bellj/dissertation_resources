package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4nt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101664nt implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ C33421e0 A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101664nt(C33421e0 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0012, code lost:
        if (r1.A03.isShowing() != false) goto L_0x0014;
     */
    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onGlobalLayout() {
        /*
            r5 = this;
            X.1e0 r1 = r5.A00
            android.view.View r4 = r1.A00
            boolean r0 = X.C252718t.A00(r4)
            r3 = 0
            if (r0 != 0) goto L_0x0014
            X.0mq r0 = r1.A03
            boolean r0 = r0.isShowing()
            r2 = 0
            if (r0 == 0) goto L_0x0015
        L_0x0014:
            r2 = 1
        L_0x0015:
            r0 = 2131364100(0x7f0a0904, float:1.8348028E38)
            android.view.View r1 = r4.findViewById(r0)
            r0 = 8
            if (r2 == 0) goto L_0x0021
            r0 = 0
        L_0x0021:
            r1.setVisibility(r0)
            r0 = 2131363267(0x7f0a05c3, float:1.8346338E38)
            android.view.View r1 = r4.findViewById(r0)
            r0 = 8
            if (r2 == 0) goto L_0x0030
            r0 = 0
        L_0x0030:
            r1.setVisibility(r0)
            r0 = 2131364101(0x7f0a0905, float:1.834803E38)
            android.view.View r0 = r4.findViewById(r0)
            if (r2 == 0) goto L_0x003e
            r3 = 8
        L_0x003e:
            r0.setVisibility(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.ViewTreeObserver$OnGlobalLayoutListenerC101664nt.onGlobalLayout():void");
    }
}
