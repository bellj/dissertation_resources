package X;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Base64;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.chromium.net.UrlRequest;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.0vJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20160vJ {
    public final C15550nR A00;
    public final C18780t0 A01;
    public final C16370ot A02;
    public final C19990v2 A03;
    public final C15650ng A04;
    public final C15600nX A05;
    public final C22810zg A06;
    public final AnonymousClass16O A07;
    public final C239913u A08;

    public C20160vJ(C15550nR r1, C18780t0 r2, C16370ot r3, C19990v2 r4, C15650ng r5, C15600nX r6, C22810zg r7, AnonymousClass16O r8, C239913u r9) {
        this.A03 = r4;
        this.A00 = r1;
        this.A06 = r7;
        this.A07 = r8;
        this.A04 = r5;
        this.A08 = r9;
        this.A02 = r3;
        this.A01 = r2;
        this.A05 = r6;
    }

    public static final AnonymousClass1G9 A00(AnonymousClass1Iv r3) {
        AnonymousClass1G9 r2 = (AnonymousClass1G9) AnonymousClass1G8.A05.A0T();
        AnonymousClass1IS r1 = r3.A0z;
        r2.A07(C15380n4.A03(r1.A00));
        r2.A08(r1.A02);
        r2.A05(r1.A01);
        AbstractC14640lm A0B = r3.A0B();
        if (A0B != null) {
            r2.A06(A0B.getRawString());
        }
        return r2;
    }

    public AnonymousClass1sN A01(AnonymousClass1G6 r37) {
        AnonymousClass1JX r0;
        String str;
        long j;
        AbstractC27881Jp r02;
        byte[] bArr = null;
        if (r37 == null) {
            Log.w("invalid data in web message node: WMI is null");
            return null;
        }
        AnonymousClass16O r03 = this.A07;
        EnumC40021qv A00 = EnumC40021qv.A00(r37.A06);
        if (A00 == null) {
            A00 = EnumC40021qv.A2B;
        }
        switch (A00.ordinal()) {
            case 1:
                C14850m9 r6 = r03.A0E;
                r0 = new C40031qw(r03.A00, r03.A02, r6, EnumC40021qv.A28, r03.A0K, null, "REVOKED_WEB_QUERY", (byte) 15, false);
                break;
            case 2:
            case 9:
            case 10:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case 19:
            case 37:
            case 47:
            case 48:
            case 49:
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
            case 51:
            case 52:
            case 53:
            case 57:
            case 58:
            case 59:
            case 62:
            case 63:
            case 66:
            case 67:
            case 68:
            case 74:
            case 118:
            case 119:
            case 120:
            case 121:
            case 123:
            case 124:
            case 125:
            case 135:
            case 136:
            case 138:
            case 139:
            case 140:
            case 142:
            case 146:
            default:
                C14830m7 r3 = r03.A07;
                C14850m9 r32 = r03.A0E;
                AbstractC15710nm r33 = r03.A00;
                C15570nT r34 = r03.A01;
                C16590pI r35 = r03.A08;
                C15450nH r15 = r03.A02;
                C15550nR r14 = r03.A04;
                AnonymousClass018 r13 = r03.A09;
                C20320vZ r12 = r03.A0K;
                C17070qD r11 = r03.A0H;
                AnonymousClass1EX r10 = r03.A0I;
                AnonymousClass13N r9 = r03.A0F;
                C22940zt r8 = r03.A0J;
                AnonymousClass11H r7 = r03.A0G;
                r0 = new C40081r1(r33, r34, r15, r03.A03, r14, r3, r35, r13, r03.A0A, r03.A0D, r32, r9, r7, r11, r10, r8, null, r12, r03.A0N, false, false);
                break;
            case 3:
                r0 = new C40061qz(r03.A00, r03.A02, r03.A0K, null, false);
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
            case 18:
            case 60:
            case 61:
            case 64:
            case 65:
            case 99:
            case 100:
                r0 = new C40181rB(r03.A00, r03.A02, null, false);
                break;
            case C43951xu.A01 /* 20 */:
                r0 = new C40091r2(r03.A00, r03.A02, null, false);
                break;
            case 21:
                r0 = new C39991qs(r03.A00, r03.A02, null, false);
                break;
            case 22:
                r0 = new C40011qu(r03.A00, r03.A02, null, false);
                break;
            case 23:
                r0 = new C40171rA(r03.A00, r03.A02, null, false);
                break;
            case 24:
                r0 = new C40191rC(r03.A00, r03.A02, null, false);
                break;
            case 25:
                r0 = new C40211rE(r03.A00, r03.A02, null, false);
                break;
            case 26:
                r0 = new C40221rF(r03.A00, r03.A02, null, false);
                break;
            case 27:
            case 35:
                r0 = new C40001qt(r03.A00, r03.A01, r03.A02, null, false);
                break;
            case 28:
            case 36:
                r0 = new C40041qx(r03.A00, r03.A02, null, false);
                break;
            case 29:
                r0 = new C40111r4(r03.A00, r03.A02, null, false);
                break;
            case C25991Bp.A0S /* 30 */:
                r0 = new C40121r5(r03.A00, r03.A02, null, false);
                break;
            case 31:
                r0 = new C40151r8(r03.A00, r03.A01, r03.A02, null, false);
                break;
            case 32:
                r0 = new C40101r3(r03.A00, r03.A02, null, false);
                break;
            case 33:
                r0 = new C40071r0(r03.A00, r03.A02, null, false);
                break;
            case 34:
                AbstractC15710nm r5 = r03.A00;
                C15450nH r4 = r03.A02;
                C15570nT r04 = r03.A01;
                r04.A08();
                r0 = new C40051qy(r5, r4, r04.A05, null, false);
                break;
            case 38:
                r0 = new C40141r7(r03.A00, r03.A02, null, false);
                break;
            case 39:
            case 79:
            case 81:
            case 84:
            case 86:
            case 94:
            case 96:
            case 126:
            case 127:
                r0 = new C40331rQ(r03.A00, r03.A02, null, false);
                break;
            case 40:
            case 41:
            case 45:
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                r0 = new C39981qr(r03.A00, r03.A02, r03.A0K, null, false);
                break;
            case 42:
                r0 = new C40201rD(r03.A00, r03.A02, null, false);
                break;
            case 43:
                r0 = new C40131r6(r03.A00, r03.A02, null, false);
                break;
            case 44:
                r0 = new C40231rG(r03.A00, r03.A02, false);
                break;
            case 54:
            case 55:
            case 56:
                r0 = new C40241rH(r03.A00, r03.A02, r03.A0G, null, false);
                break;
            case 69:
                r0 = new C40271rK(r03.A00, r03.A02, null, false);
                break;
            case 70:
                r0 = new C40251rI(r03.A00, r03.A02, null, false);
                break;
            case 71:
                r0 = new C40261rJ(r03.A00, r03.A02, null, false);
                break;
            case C43951xu.A02 /* 72 */:
                r0 = new C40281rL(r03.A00, r03.A02, null, false);
                break;
            case 73:
                r0 = new C40291rM(r03.A00, r03.A02, null, false);
                break;
            case 75:
            case 128:
            case 129:
                r0 = new C40351rS(r03.A00, r03.A02, null, false);
                break;
            case 76:
            case 77:
            case 78:
            case 80:
            case 82:
            case 83:
            case 85:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 95:
            case 97:
            case 98:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case C43951xu.A03 /* 108 */:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
                r0 = new C40311rO(r03.A00, r03.A02, null, false);
                break;
            case 122:
                r0 = new C40301rN(r03.A00, r03.A02, null, false);
                break;
            case 130:
                r0 = new C40341rR(r03.A00, r03.A01, r03.A02, null, false);
                break;
            case 131:
                r0 = new C40361rT(r03.A00, r03.A02, null, false);
                break;
            case 132:
                C14850m9 r62 = r03.A0E;
                r0 = new C40031qw(r03.A00, r03.A02, r62, EnumC40021qv.A01, r03.A0K, null, "ADMIN_REVOKED_WEB_QUERY", (byte) 64, false);
                break;
            case 133:
                r0 = new C40371rU(r03.A00, r03.A02, null, null, false);
                break;
            case 134:
            case 137:
                r0 = new C40381rV(r03.A00, r03.A02, r03.A04, r03.A05, r03.A0B, null, r03.A0L, false);
                break;
            case 141:
                r0 = new C40391rW(r03.A00, r03.A01, r03.A02, null, false);
                break;
            case 143:
                r0 = new C40401rX(r03.A00, r03.A02, null, false);
                break;
            case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                r0 = new C40431ra(r03.A00, r03.A02, null, false);
                break;
            case 145:
                r0 = new C40441rb(r03.A00, r03.A02, null, false);
                break;
            case 147:
                r0 = new C40411rY(r03.A00, r03.A02, null, false);
                break;
            case 148:
                r0 = new C40421rZ(r03.A00, r03.A02, null, false);
                break;
            case 149:
                r0 = new C40451rc(r03.A00, r03.A02, null, r03.A0L, false);
                break;
            case 150:
                r0 = new C40461rd(r03.A00, r03.A02, r03.A04, r03.A05, null, r03.A0L, false);
                break;
        }
        AnonymousClass1G8 r72 = r37.A0M;
        if (r72 == null) {
            r72 = AnonymousClass1G8.A05;
        }
        boolean z = true;
        if ((r72.A00 & 1) != 1) {
            z = false;
        }
        if (z) {
            AnonymousClass1IS r73 = new AnonymousClass1IS(AbstractC14640lm.A01(r72.A03), r72.A01, r72.A04);
            if (r73.A00 != null && !TextUtils.isEmpty(r73.A01)) {
                if ((r37.A01 & 4) == 4) {
                    j = r37.A0A * 1000;
                } else {
                    j = 0;
                }
                AbstractC15340mz A03 = r0.A03(r37, r73, j);
                if (A03 == null) {
                    str = "Unable to read FMessage from WMI.";
                    Log.e(str);
                    return null;
                }
                A03.A0r = r37.A0a;
                if (C30041Vv.A0p(A03)) {
                    AnonymousClass1IS r92 = A03.A0z;
                    if (!r92.A02 && ((AnonymousClass1XB) A03).A00 != 64) {
                        A03 = r0.A03(r37, new AnonymousClass1IS(r92.A00, r92.A01, true), j);
                    }
                }
                if ((r37.A01 & 2) == 2) {
                    C27081Fy r63 = r37.A0L;
                    if (r63 == null) {
                        r63 = C27081Fy.A0i;
                    }
                    int i = r63.A00;
                    if ((i & 4) == 4) {
                        C40821sO r36 = r63.A0J;
                        if (r36 == null) {
                            r36 = C40821sO.A0R;
                        }
                        if ((r36.A00 & 128) == 128) {
                            r02 = r36.A0B;
                            bArr = r02.A04();
                        }
                    }
                    if (r63.A0b()) {
                        C40831sP r38 = r63.A0C;
                        if (r38 == null) {
                            r38 = C40831sP.A0L;
                        }
                        if ((r38.A00 & 64) == 64) {
                            r02 = r38.A09;
                            bArr = r02.A04();
                        }
                    }
                    if ((i & 128) == 128) {
                        C40841sQ r39 = r63.A02;
                        if (r39 == null) {
                            r39 = C40841sQ.A0E;
                        }
                        if ((r39.A00 & 64) == 64) {
                            r02 = r39.A06;
                            bArr = r02.A04();
                        }
                    }
                    if ((i & 256) == 256) {
                        C40851sR r310 = r63.A0f;
                        if (r310 == null) {
                            r310 = C40851sR.A0O;
                        }
                        if ((r310.A00 & 32) == 32) {
                            r02 = r310.A0A;
                            bArr = r02.A04();
                        }
                    }
                }
                return new AnonymousClass1sN(A03, bArr);
            }
        }
        Log.w("invalid data in web message node: key is malformed");
        str = "WebMessageInfo key is malformed.";
        Log.e(str);
        return null;
    }

    public AnonymousClass1G7 A02(AbstractC15340mz r17) {
        List<AnonymousClass1Iv> list;
        C40481rf r0;
        AnonymousClass1G7 r4 = null;
        if (r17 != null) {
            if (r17 instanceof AnonymousClass1Iv) {
                Log.e("FMessageAddOn should be passed as part of the parent message and not stand alone");
            } else {
                this.A08.A00(r17);
                AnonymousClass1JX A00 = this.A07.A00(r17, null, false, true);
                if (A00 != null) {
                    r4 = A00.A02();
                    C22810zg r3 = this.A06;
                    for (Map.Entry entry : r3.A00(r17).A00.entrySet()) {
                        C39931qm r10 = (C39931qm) entry.getValue();
                        AnonymousClass1G4 A0T = C40771sI.A08.A0T();
                        String rawString = ((Jid) entry.getKey()).getRawString();
                        A0T.A03();
                        C40771sI r1 = (C40771sI) A0T.A00;
                        r1.A01 |= 1;
                        r1.A07 = rawString;
                        A0T.A03();
                        C40771sI r9 = (C40771sI) A0T.A00;
                        r9.A01 |= 2;
                        r9.A04 = r10.A00 / 1000;
                        A0T.A03();
                        C40771sI r92 = (C40771sI) A0T.A00;
                        r92.A01 |= 4;
                        r92.A03 = r10.A02 / 1000;
                        A0T.A03();
                        C40771sI r93 = (C40771sI) A0T.A00;
                        r93.A01 |= 8;
                        r93.A02 = r10.A01 / 1000;
                        r4.A03();
                        AnonymousClass1G6 r8 = (AnonymousClass1G6) r4.A00;
                        AnonymousClass1K6 r12 = r8.A0J;
                        if (!((AnonymousClass1K7) r12).A00) {
                            r12 = AbstractC27091Fz.A0G(r12);
                            r8.A0J = r12;
                        }
                        r12.add(A0T.A02());
                        C32611cR A002 = r3.A05.A02.A00(r17);
                        HashSet hashSet = new HashSet();
                        HashSet hashSet2 = new HashSet();
                        for (Map.Entry entry2 : A002.A00.entrySet()) {
                            Jid jid = (Jid) entry2.getKey();
                            C32621cS r02 = (C32621cS) entry2.getValue();
                            if (r02 != null) {
                                boolean z = false;
                                if (r02.A00 > 0) {
                                    z = true;
                                }
                                String rawString2 = jid.getRawString();
                                if (z) {
                                    hashSet2.add(rawString2);
                                } else {
                                    hashSet.add(rawString2);
                                }
                            }
                        }
                        A0T.A03();
                        C40771sI r94 = (C40771sI) A0T.A00;
                        AnonymousClass1K6 r13 = r94.A06;
                        if (!((AnonymousClass1K7) r13).A00) {
                            r13 = AbstractC27091Fz.A0G(r13);
                            r94.A06 = r13;
                        }
                        AnonymousClass1G5.A01(hashSet, r13);
                        A0T.A03();
                        C40771sI r2 = (C40771sI) A0T.A00;
                        AnonymousClass1K6 r14 = r2.A05;
                        if (!((AnonymousClass1K7) r14).A00) {
                            r14 = AbstractC27091Fz.A0G(r14);
                            r2.A05 = r14;
                        }
                        AnonymousClass1G5.A01(hashSet2, r14);
                    }
                    int i = r17.A07;
                    if (i != 0) {
                        if ((i & 1) == 1 && (r0 = r17.A0V) != null) {
                            for (AnonymousClass1Iv r15 : r0.A02()) {
                                AnonymousClass1X9 r32 = (AnonymousClass1X9) r15;
                                AnonymousClass1G4 A0T2 = C40781sJ.A06.A0T();
                                AnonymousClass1G9 A003 = A00(r15);
                                A0T2.A03();
                                C40781sJ r16 = (C40781sJ) A0T2.A00;
                                r16.A02 = (AnonymousClass1G8) A003.A02();
                                r16.A00 |= 1;
                                if (!TextUtils.isEmpty(r32.A01)) {
                                    String str = r32.A01;
                                    A0T2.A03();
                                    C40781sJ r18 = (C40781sJ) A0T2.A00;
                                    r18.A00 |= 2;
                                    r18.A04 = str;
                                }
                                if (r32.A0C != 17) {
                                    A0T2.A03();
                                    C40781sJ r19 = (C40781sJ) A0T2.A00;
                                    r19.A00 |= 16;
                                    r19.A05 = true;
                                }
                                long j = r32.A00;
                                A0T2.A03();
                                C40781sJ r110 = (C40781sJ) A0T2.A00;
                                r110.A00 |= 8;
                                r110.A01 = j;
                                r4.A03();
                                AnonymousClass1G6 r22 = (AnonymousClass1G6) r4.A00;
                                AnonymousClass1K6 r111 = r22.A0I;
                                if (!((AnonymousClass1K7) r111).A00) {
                                    r111 = AbstractC27091Fz.A0G(r111);
                                    r22.A0I = r111;
                                }
                                r111.add(A0T2.A02());
                            }
                        }
                        if ((r17.A07 & 2) == 2 && (r17 instanceof C27671Iq) && (list = ((C27671Iq) r17).A03) != null) {
                            for (AnonymousClass1Iv r102 : list) {
                                C27711Iw r23 = (C27711Iw) r102;
                                AnonymousClass1G4 A0T3 = C40791sK.A04.A0T();
                                AnonymousClass1G4 A0T4 = C40801sL.A01.A0T();
                                List<String> list2 = r23.A01;
                                if (list2 == null) {
                                    break;
                                }
                                for (String str2 : list2) {
                                    byte[] decode = Base64.decode(str2, 2);
                                    AbstractC27881Jp A01 = AbstractC27881Jp.A01(decode, 0, decode.length);
                                    A0T4.A03();
                                    C40801sL r33 = (C40801sL) A0T4.A00;
                                    AnonymousClass1K6 r112 = r33.A00;
                                    if (!((AnonymousClass1K7) r112).A00) {
                                        r112 = AbstractC27091Fz.A0G(r112);
                                        r33.A00 = r112;
                                    }
                                    r112.add(A01);
                                }
                                A0T3.A03();
                                C40791sK r113 = (C40791sK) A0T3.A00;
                                r113.A02 = (C40801sL) A0T4.A02();
                                r113.A00 |= 2;
                                AnonymousClass1G9 A004 = A00(r102);
                                A0T3.A03();
                                C40791sK r114 = (C40791sK) A0T3.A00;
                                r114.A03 = (AnonymousClass1G8) A004.A02();
                                r114.A00 |= 1;
                                long j2 = r23.A00;
                                A0T3.A03();
                                C40791sK r34 = (C40791sK) A0T3.A00;
                                r34.A00 |= 4;
                                r34.A01 = j2;
                                r4.A03();
                                AnonymousClass1G6 r24 = (AnonymousClass1G6) r4.A00;
                                AnonymousClass1K6 r115 = r24.A0H;
                                if (!((AnonymousClass1K7) r115).A00) {
                                    r115 = AbstractC27091Fz.A0G(r115);
                                    r24.A0H = r115;
                                }
                                r115.add(A0T3.A02());
                            }
                        }
                    }
                    if ((r17 instanceof C27671Iq) && ((C27671Iq) r17).A00 != 0) {
                        AnonymousClass1G4 A0T5 = C40811sM.A02.A0T();
                        A0T5.A03();
                        C40811sM r116 = (C40811sM) A0T5.A00;
                        r116.A00 |= 1;
                        r116.A01 = true;
                        r4.A03();
                        AnonymousClass1G6 r25 = (AnonymousClass1G6) r4.A00;
                        r25.A0T = (C40811sM) A0T5.A02();
                        r25.A01 |= Integer.MIN_VALUE;
                        return r4;
                    }
                }
            }
        }
        return r4;
    }

    public Map A03(Cursor cursor, C40881sU r16) {
        int i;
        int i2;
        int i3;
        C18780t0 r0 = this.A01;
        Map A05 = r0.A05();
        Map A04 = r0.A04();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        while (cursor.moveToNext()) {
            AbstractC15340mz A01 = this.A04.A0K.A01(cursor);
            if (A01 != null) {
                C16370ot r02 = this.A02;
                AnonymousClass1IS r1 = A01.A0z;
                r02.A05(r1);
                if (r16 != null) {
                    r16.A00.A0J(A01);
                }
                AbstractC14640lm r5 = r1.A00;
                AnonymousClass1PC r4 = (AnonymousClass1PC) linkedHashMap.get(r5);
                if (r4 == null) {
                    r4 = (AnonymousClass1PC) AnonymousClass1PB.A0e.A0T();
                    AnonymousClass009.A05(r5);
                    String rawString = r5.getRawString();
                    r4.A03();
                    AnonymousClass1PB r12 = (AnonymousClass1PB) r4.A00;
                    r12.A01 |= 1;
                    r12.A0O = rawString;
                    C19990v2 r9 = this.A03;
                    AnonymousClass1PE A06 = r9.A06(r5);
                    if (A06 != null) {
                        if (A06.A06() != null) {
                            String A062 = A06.A06();
                            r4.A03();
                            AnonymousClass1PB r13 = (AnonymousClass1PB) r4.A00;
                            r13.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                            r13.A0P = A062;
                        }
                        synchronized (A06) {
                            i = A06.A03;
                        }
                        boolean z = false;
                        if (i == 1) {
                            z = true;
                        }
                        r4.A03();
                        AnonymousClass1PB r14 = (AnonymousClass1PB) r4.A00;
                        r14.A01 |= DefaultCrypto.BUFFER_SIZE;
                        r14.A0Z = z;
                        boolean z2 = A06.A0f;
                        r4.A03();
                        AnonymousClass1PB r15 = (AnonymousClass1PB) r4.A00;
                        r15.A01 |= 16384;
                        r15.A0U = z2;
                        long max = Math.max(A06.A02() / 1000, 0L);
                        r4.A03();
                        AnonymousClass1PB r132 = (AnonymousClass1PB) r4.A00;
                        r132.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                        r132.A09 = max;
                        int max2 = Math.max(A06.A0Y.expiration, 0);
                        r4.A03();
                        AnonymousClass1PB r17 = (AnonymousClass1PB) r4.A00;
                        r17.A01 |= 128;
                        r17.A04 = max2;
                        long max3 = Math.max(A06.A0Y.ephemeralSettingTimestamp, 0L);
                        r4.A03();
                        AnonymousClass1PB r3 = (AnonymousClass1PB) r4.A00;
                        r3.A01 |= 256;
                        r3.A0B = max3;
                        synchronized (A06) {
                            i2 = A06.A08;
                        }
                        int max4 = Math.max(i2, 0);
                        r4.A03();
                        AnonymousClass1PB r18 = (AnonymousClass1PB) r4.A00;
                        r18.A01 |= 16;
                        r18.A07 = max4;
                        int i4 = (((long) A06.A06) > -1 ? 1 : (((long) A06.A06) == -1 ? 0 : -1));
                        boolean z3 = false;
                        if (i4 == 0) {
                            z3 = true;
                        }
                        r4.A03();
                        AnonymousClass1PB r32 = (AnonymousClass1PB) r4.A00;
                        r32.A01 |= C25981Bo.A0F;
                        r32.A0Y = z3;
                        boolean A0E = r9.A0E(r5);
                        r4.A03();
                        AnonymousClass1PB r19 = (AnonymousClass1PB) r4.A00;
                        r19.A01 |= 32;
                        r19.A0a = A0E;
                        AnonymousClass1PE r03 = (AnonymousClass1PE) r9.A0B().get(r5);
                        if (r03 == null) {
                            i3 = 0;
                        } else {
                            i3 = r03.A04;
                        }
                        r4.A03();
                        AnonymousClass1PB r33 = (AnonymousClass1PB) r4.A00;
                        r33.A01 |= 65536;
                        r33.A08 = i3;
                        if (r5 instanceof GroupJid) {
                            String A03 = this.A05.A03((AbstractC15590nW) r5);
                            r4.A03();
                            AnonymousClass1PB r110 = (AnonymousClass1PB) r4.A00;
                            r110.A01 |= 4096;
                            r110.A0S = A03;
                            boolean z4 = this.A00.A0B(r5).A0a;
                            r4.A03();
                            AnonymousClass1PB r2 = (AnonymousClass1PB) r4.A00;
                            r2.A01 |= 67108864;
                            r2.A0c = z4;
                        } else {
                            AnonymousClass1PF r7 = (AnonymousClass1PF) A05.get(r5);
                            if (r7 != null) {
                                byte[] bArr = r7.A01;
                                AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
                                r4.A03();
                                AnonymousClass1PB r22 = (AnonymousClass1PB) r4.A00;
                                r22.A01 |= 262144;
                                r22.A0H = A012;
                                long longValue = r7.A00.longValue();
                                r4.A03();
                                AnonymousClass1PB r72 = (AnonymousClass1PB) r4.A00;
                                r72.A01 |= 524288;
                                r72.A0F = longValue;
                            }
                            Number number = (Number) A04.get(r5);
                            if (number != null) {
                                long longValue2 = number.longValue();
                                r4.A03();
                                AnonymousClass1PB r73 = (AnonymousClass1PB) r4.A00;
                                r73.A01 |= 33554432;
                                r73.A0E = longValue2;
                            }
                        }
                    }
                    linkedHashMap.put(r5, r4);
                }
                A05(r4, A01);
            }
        }
        return linkedHashMap;
    }

    public void A04(AbstractC15590nW r8, AnonymousClass1PC r9) {
        AnonymousClass1YO r5;
        EnumC40871sT r2;
        AnonymousClass1YM A02 = this.A05.A02(r8);
        Iterator it = A02.A06().iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (!(next == null || (r5 = (AnonymousClass1YO) A02.A02.get(next)) == null)) {
                AnonymousClass1G4 A0T = C40861sS.A04.A0T();
                String rawString = r5.A03.getRawString();
                A0T.A03();
                C40861sS r1 = (C40861sS) A0T.A00;
                r1.A01 |= 1;
                r1.A03 = rawString;
                int i = r5.A01;
                if (i != 0) {
                    if (i == 1) {
                        r2 = EnumC40871sT.A01;
                    } else if (i != 2) {
                        r2 = null;
                    } else {
                        r2 = EnumC40871sT.A03;
                    }
                    A0T.A03();
                    C40861sS r12 = (C40861sS) A0T.A00;
                    r12.A01 |= 2;
                    r12.A02 = r2.value;
                }
                r9.A03();
                AnonymousClass1PB r22 = (AnonymousClass1PB) r9.A00;
                AnonymousClass1K6 r13 = r22.A0J;
                if (!((AnonymousClass1K7) r13).A00) {
                    r13 = AbstractC27091Fz.A0G(r13);
                    r22.A0J = r13;
                }
                r13.add(A0T.A02());
            }
        }
    }

    public void A05(AnonymousClass1PC r6, AbstractC15340mz r7) {
        AnonymousClass1G7 A02 = A02(r7);
        if (A02 != null) {
            AnonymousClass1G4 A0T = C40761sH.A04.A0T();
            A0T.A03();
            C40761sH r1 = (C40761sH) A0T.A00;
            r1.A03 = (AnonymousClass1G6) A02.A02();
            r1.A01 |= 1;
            long j = r7.A11;
            A0T.A03();
            C40761sH r12 = (C40761sH) A0T.A00;
            r12.A01 |= 2;
            r12.A02 = j;
            AbstractC27091Fz A022 = A0T.A02();
            r6.A03();
            AnonymousClass1PB r2 = (AnonymousClass1PB) r6.A00;
            AnonymousClass1K6 r13 = r2.A0I;
            if (!((AnonymousClass1K7) r13).A00) {
                r13 = AbstractC27091Fz.A0G(r13);
                r2.A0I = r13;
            }
            r13.add(A022);
        }
    }
}
