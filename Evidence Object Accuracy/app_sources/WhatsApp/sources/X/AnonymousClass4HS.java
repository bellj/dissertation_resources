package X;

import com.whatsapp.R;

/* renamed from: X.4HS  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4HS {
    public static final int[] A00 = {16842766, 16842904, 16843087, 16843088, 16843365, 16843379, R.attr.action, R.attr.icon, R.attr.overrideButtonMigration, R.attr.variant, R.attr.wdsButtonSize};
    public static final int[] A01 = {R.attr.orientation};
    public static final int[] A02 = {R.attr.badgeType, R.attr.profilePhotoShape, R.attr.profilePhotoSize, R.attr.statusIndicatorEnabled};
}
