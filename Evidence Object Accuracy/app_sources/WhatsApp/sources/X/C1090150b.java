package X;

/* renamed from: X.50b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C1090150b implements AbstractC115285Qx {
    public static final AbstractC116395Vg A01 = new AnonymousClass50U();
    public final AbstractC116395Vg A00;

    public C1090150b() {
        AbstractC116395Vg r1;
        AbstractC116395Vg[] r2 = new AbstractC116395Vg[2];
        r2[0] = AnonymousClass50W.A00;
        try {
            r1 = (AbstractC116395Vg) C72453ed.A0k(Class.forName("com.google.protobuf.DescriptorMessageInfoFactory"), "getInstance");
        } catch (Exception unused) {
            r1 = A01;
        }
        r2[1] = r1;
        this.A00 = new AnonymousClass50V(r2);
    }
}
