package X;

import com.whatsapp.status.StatusRecipientsActivity;
import java.lang.ref.WeakReference;
import java.util.Collection;

/* renamed from: X.386  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass386 extends AbstractC16350or {
    public final C14900mE A00;
    public final C20670w8 A01;
    public final C18470sV A02;
    public final AnonymousClass1BD A03;
    public final WeakReference A04;
    public final Collection A05;
    public final boolean A06;

    public AnonymousClass386(C14900mE r2, C20670w8 r3, C18470sV r4, StatusRecipientsActivity statusRecipientsActivity, AnonymousClass1BD r6, Collection collection, boolean z) {
        super(statusRecipientsActivity);
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = r6;
        this.A04 = C12970iu.A10(statusRecipientsActivity);
        this.A05 = collection;
        this.A06 = z;
    }
}
