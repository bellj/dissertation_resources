package X;

import android.graphics.Bitmap;
import com.whatsapp.util.Log;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3ED  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3ED {
    public final C18790t3 A00;
    public final C18810t5 A01;
    public final AnonymousClass4V5 A02;

    public AnonymousClass3ED(C18790t3 r1, C18810t5 r2, AnonymousClass4V5 r3) {
        C16700pc.A0G(r2, r1);
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public final Bitmap A00(String str) {
        InputStream AAZ = this.A01.A00().A02(str).AAZ(this.A00, null, 30);
        C16700pc.A0B(AAZ);
        AnonymousClass4V5 r3 = this.A02;
        C16700pc.A0E(str, 0);
        String A01 = C003501n.A01(str);
        AnonymousClass009.A05(A01);
        C16700pc.A0B(A01);
        r3.A00().A02(AAZ, A01);
        String A012 = C003501n.A01(str);
        AnonymousClass009.A05(A012);
        C16700pc.A0B(A012);
        return r3.A00().A00(A012, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public final List A01(List list, boolean z) {
        Object r2;
        if (z) {
            this.A02.A00().A03(true);
        }
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (z) {
                try {
                    r2 = A00(A0x);
                } catch (Throwable th) {
                    r2 = new AnonymousClass5BR(th);
                }
            } else {
                AnonymousClass4V5 r1 = this.A02;
                C16700pc.A0E(A0x, 0);
                String A01 = C003501n.A01(A0x);
                AnonymousClass009.A05(A01);
                C16700pc.A0B(A01);
                r2 = r1.A00().A00(A01, Integer.MAX_VALUE, Integer.MAX_VALUE);
                if (r2 == null) {
                    r2 = A00(A0x);
                }
            }
            Throwable A00 = AnonymousClass5BU.A00(r2);
            if (A00 != null) {
                Log.e("AvatarProfilePhotoPosesFetcher/fetchPoses", A00);
            }
            if (r2 instanceof AnonymousClass5BR) {
                r2 = null;
            }
            if (r2 != null) {
                A0l.add(r2);
            }
        }
        return A0l;
    }
}
