package X;

import com.facebook.redex.EmptyBaseRunnable0;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;

/* renamed from: X.3l3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class RunnableC76093l3 extends EmptyBaseRunnable0 implements Runnable {
    public boolean A00;
    public final /* synthetic */ AspectRatioFrameLayout A01;

    public /* synthetic */ RunnableC76093l3(AspectRatioFrameLayout aspectRatioFrameLayout) {
        this.A01 = aspectRatioFrameLayout;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A00 = false;
    }
}
