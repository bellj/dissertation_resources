package X;

import android.view.View;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;

/* renamed from: X.3XR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XR implements AnonymousClass5UF {
    public AbstractC15290ms A00;
    public AnonymousClass3DT A01;
    public C63463Br A02;
    public C54462gl A03;
    public C69893aP A04;
    public final AnonymousClass1AC A05;
    public final C253719d A06;
    public final AbstractC253919f A07;
    public final AbstractView$OnClickListenerC34281fs A08 = new ViewOnClickCListenerShape18S0100000_I1_1(this, 22);

    public AnonymousClass3XR(AnonymousClass1AC r3, C253719d r4, AbstractC253919f r5) {
        this.A06 = r4;
        this.A07 = r5;
        this.A05 = r3;
    }

    public void A00() {
        View view = this.A01.A02;
        if (view != null && view.getVisibility() == 0) {
            C54462gl r1 = this.A03;
            if (r1 == null) {
                r1 = new AnonymousClass320(this);
                this.A03 = r1;
            }
            this.A01.A00(r1);
            this.A03.A0F(this.A07.A00());
        }
    }

    @Override // X.AnonymousClass5UF
    public void AR4(C66013Ly r2) {
        AbstractC14000kg r0;
        this.A00.Aak();
        C63463Br r02 = this.A02;
        if (r02 != null && (r0 = r02.A06.A00) != null) {
            r0.AR4(r2);
        }
    }
}
