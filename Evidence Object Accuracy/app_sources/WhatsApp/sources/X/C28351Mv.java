package X;

/* renamed from: X.1Mv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28351Mv<E> extends AbstractC17940re<E> {
    public static final C28351Mv EMPTY;
    public static final Object[] EMPTY_ARRAY;
    public final transient Object[] elements;
    public final transient int hashCode;
    public final transient int mask;
    public final transient int size;
    public final transient Object[] table;

    @Override // X.AbstractC17950rf
    public int internalArrayStart() {
        return 0;
    }

    @Override // X.AbstractC17940re
    public boolean isHashCodeFast() {
        return true;
    }

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return false;
    }

    static {
        Object[] objArr = new Object[0];
        EMPTY_ARRAY = objArr;
        EMPTY = new C28351Mv(objArr, 0, objArr, 0, 0);
    }

    public C28351Mv(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.elements = objArr;
        this.hashCode = i;
        this.table = objArr2;
        this.mask = i2;
        this.size = i3;
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        Object[] objArr = this.table;
        if (obj != null && objArr.length != 0) {
            int smearedHash = C28301Mo.smearedHash(obj);
            while (true) {
                int i = smearedHash & this.mask;
                Object obj2 = objArr[i];
                if (obj2 == null) {
                    break;
                } else if (obj2.equals(obj)) {
                    return true;
                } else {
                    smearedHash = i + 1;
                }
            }
        }
        return false;
    }

    @Override // X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        System.arraycopy(this.elements, 0, objArr, i, this.size);
        return i + this.size;
    }

    @Override // X.AbstractC17940re
    public AnonymousClass1Mr createAsList() {
        return AnonymousClass1Mr.asImmutableList(this.elements, this.size);
    }

    @Override // X.AbstractC17940re, java.util.Collection, java.lang.Object, java.util.Set
    public int hashCode() {
        return this.hashCode;
    }

    @Override // X.AbstractC17950rf
    public Object[] internalArray() {
        return this.elements;
    }

    @Override // X.AbstractC17950rf
    public int internalArrayEnd() {
        return this.size;
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public AnonymousClass1I5 iterator() {
        return asList().iterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return this.size;
    }
}
