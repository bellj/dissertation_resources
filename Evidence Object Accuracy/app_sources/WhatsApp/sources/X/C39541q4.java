package X;

import java.util.Arrays;

/* renamed from: X.1q4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39541q4 {
    public final int[] A00;

    public C39541q4(int... iArr) {
        AnonymousClass009.A0E(iArr.length <= 5);
        this.A00 = iArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C39541q4)) {
            return false;
        }
        return Arrays.equals(this.A00, ((C39541q4) obj).A00);
    }

    public int hashCode() {
        return Arrays.hashCode(this.A00);
    }
}
