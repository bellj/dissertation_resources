package X;

import android.graphics.PathMeasure;

/* renamed from: X.0fF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10810fF extends ThreadLocal {
    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        return new PathMeasure();
    }
}
