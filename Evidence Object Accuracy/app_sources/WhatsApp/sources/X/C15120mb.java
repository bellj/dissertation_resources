package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import java.io.Closeable;
import java.util.List;

/* renamed from: X.0mb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C15120mb extends AbstractC15040mS implements Closeable {
    public static final String A03 = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", "hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id");
    public static final String A04 = String.format("SELECT MAX(%s) FROM %s WHERE 1;", "hit_time", "hits2");
    public final C93924ay A00;
    public final C93924ay A01;
    public final AnonymousClass2ZL A02;

    public C15120mb(C14160kx r3) {
        super(r3);
        AbstractC115095Qe r1 = ((C15050mT) this).A00.A04;
        this.A00 = new C93924ay(r1);
        this.A01 = new C93924ay(r1);
        this.A02 = new AnonymousClass2ZL(r3.A00, this);
    }

    public static final long A00(C15120mb r6, String str, String[] strArr) {
        Cursor cursor = null;
        try {
            try {
                cursor = r6.A0I().rawQuery(str, strArr);
                if (cursor.moveToFirst()) {
                    long j = cursor.getLong(0);
                    cursor.close();
                    return j;
                }
                cursor.close();
                return 0;
            } catch (SQLiteException e) {
                C15050mT.A06(r6, str, e, null, "Database error", 6);
                throw e;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final long A0H() {
        Cursor cursor;
        try {
            C14170ky.A00();
            A0G();
            cursor = null;
            try {
                Cursor rawQuery = A0I().rawQuery("SELECT COUNT(*) FROM hits2", null);
                if (rawQuery.moveToFirst()) {
                    long j = rawQuery.getLong(0);
                    rawQuery.close();
                    return j;
                }
                throw new SQLiteException("Database returned empty set");
            } catch (SQLiteException e) {
                C15050mT.A06(this, "SELECT COUNT(*) FROM hits2", e, null, "Database error", 6);
                throw e;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    public final SQLiteDatabase A0I() {
        try {
            return this.A02.getWritableDatabase();
        } catch (SQLiteException e) {
            A0E("Error opening database", e);
            throw e;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:37:0x00d8 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r18v0, types: [X.0mb, X.0mT, X.0mS] */
    /* JADX WARN: Type inference failed for: r6v0, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r6v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r7v0, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r6v2 */
    /* JADX WARN: Type inference failed for: r9v0, types: [java.lang.String[]] */
    /* JADX WARN: Type inference failed for: r5v2, types: [java.lang.Object[]] */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0086, code lost:
        if (r9.startsWith("http:") == false) goto L_0x0088;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A0J(long r19) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15120mb.A0J(long):java.util.List");
    }

    public final void A0K(List list) {
        C14170ky.A00();
        A0G();
        if (!list.isEmpty()) {
            StringBuilder sb = new StringBuilder("hit_id");
            sb.append(" in (");
            for (int i = 0; i < list.size(); i++) {
                Number number = (Number) list.get(i);
                if (number == null || number.longValue() == 0) {
                    throw new SQLiteException("Invalid hit id");
                }
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(number);
            }
            sb.append(")");
            String obj = sb.toString();
            try {
                SQLiteDatabase A0I = A0I();
                A0D("Deleting dispatched hits. count", Integer.valueOf(list.size()));
                int delete = A0I.delete("hits2", obj, null);
                if (delete != list.size()) {
                    C15050mT.A06(this, Integer.valueOf(list.size()), Integer.valueOf(delete), obj, "Deleted fewer hits then expected", 5);
                }
            } catch (SQLiteException e) {
                A0C("Error deleting hits", e);
                throw e;
            }
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public final void close() {
        try {
            this.A02.close();
        } catch (SQLiteException e) {
            A0C("Sql error closing database", e);
        } catch (IllegalStateException e2) {
            A0C("Error closing database", e2);
        }
    }
}
