package X;

import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import org.spongycastle.jcajce.provider.symmetric.AES;

/* renamed from: X.5PQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5PQ extends AnonymousClass5IV implements AnonymousClass5S1 {
    public static final Class A0F = AnonymousClass1TA.A00(AnonymousClass5PQ.class, "javax.crypto.spec.GCMParameterSpec");
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public String A04;
    public String A05;
    public PBEParameterSpec A06;
    public AnonymousClass5XE A07;
    public C113035Ft A08;
    public C113075Fx A09;
    public AES.ECB.AnonymousClass1 A0A;
    public AnonymousClass5XV A0B;
    public boolean A0C;
    public boolean A0D;
    public Class[] A0E;

    public static final boolean A06(String str) {
        return "CCM".equals(str) || "EAX".equals(str) || "GCM".equals(str) || "OCB".equals(str);
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public int engineGetBlockSize() {
        AnonymousClass5XE r0 = this.A07;
        if (r0 == null) {
            return -1;
        }
        return r0.AAt();
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public int engineGetOutputSize(int i) {
        return this.A0B.AEp(i);
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public void engineInit(int i, Key key, SecureRandom secureRandom) {
        try {
            engineInit(i, key, (AlgorithmParameterSpec) null, secureRandom);
        } catch (InvalidAlgorithmParameterException e) {
            throw new InvalidKeyException(e.getMessage());
        }
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public byte[] engineUpdate(byte[] bArr, int i, int i2) {
        AnonymousClass5XV r1 = this.A0B;
        int AHQ = r1.AHQ(i2);
        if (AHQ > 0) {
            byte[] bArr2 = new byte[AHQ];
            int AZZ = r1.AZZ(bArr, i, i2, bArr2, 0);
            if (AZZ != 0) {
                if (AZZ == AHQ) {
                    return bArr2;
                }
                byte[] bArr3 = new byte[AZZ];
                System.arraycopy(bArr2, 0, bArr3, 0, AZZ);
                return bArr3;
            }
        } else {
            r1.AZZ(bArr, i, i2, null, 0);
        }
        return null;
    }

    @Override // javax.crypto.CipherSpi
    public void engineUpdateAAD(byte[] bArr, int i, int i2) {
        this.A0B.AfL(bArr, i, i2);
    }

    public AnonymousClass5PQ(C94474bs r3, int i) {
        A05(this, true);
        this.A07 = r3.A01;
        this.A0B = new AnonymousClass5GS(r3);
        this.A0C = true;
        this.A01 = i >> 3;
    }

    public AnonymousClass5PQ(AnonymousClass5XE r2) {
        A04(this);
        this.A07 = r2;
        this.A0B = new AnonymousClass5GS(r2);
    }

    public AnonymousClass5PQ(AnonymousClass5XE r2, int i) {
        A05(this, true);
        this.A07 = r2;
        this.A0C = true;
        this.A0B = new AnonymousClass5GS(r2);
        this.A01 = i >> 3;
    }

    public AnonymousClass5PQ(AnonymousClass5XE r2, int i, int i2, int i3, int i4) {
        A04(this);
        this.A07 = r2;
        this.A03 = i;
        this.A00 = i2;
        this.A02 = i3;
        this.A01 = i4;
        this.A0B = new AnonymousClass5GS(r2);
    }

    public AnonymousClass5PQ(AbstractC117285Zg r2) {
        A04(this);
        AnonymousClass5XE AHO = r2.AHO();
        this.A07 = AHO;
        this.A01 = AHO.AAt();
        this.A0B = new AnonymousClass5GR(r2);
    }

    public AnonymousClass5PQ(AbstractC117285Zg r2, int i, boolean z) {
        A04(this);
        this.A07 = r2.AHO();
        this.A0C = z;
        this.A01 = i;
        this.A0B = new AnonymousClass5GR(r2);
    }

    public AnonymousClass5PQ(AES.ECB.AnonymousClass1 r3) {
        A04(this);
        this.A07 = new C71643dG();
        this.A0A = r3;
        this.A0B = new AnonymousClass5GS(new C71643dG());
    }

    public static AlgorithmParameters A02(String str, AnonymousClass5IV r2) {
        AlgorithmParameters instance = AlgorithmParameters.getInstance(str, ((AnonymousClass5GT) r2.A07).A00);
        r2.A01 = instance;
        return instance;
    }

    public static AnonymousClass20L A03(String str, AlgorithmParameterSpec algorithmParameterSpec, byte[] bArr, int i, int i2, int i3, int i4) {
        if (algorithmParameterSpec == null || !(algorithmParameterSpec instanceof PBEParameterSpec)) {
            throw C72463ee.A0J("Need a PBEParameter spec with a PBE key.");
        }
        PBEParameterSpec pBEParameterSpec = (PBEParameterSpec) algorithmParameterSpec;
        AbstractC94944cn A01 = C95114dA.A01(i, i2);
        byte[] salt = pBEParameterSpec.getSalt();
        int iterationCount = pBEParameterSpec.getIterationCount();
        A01.A01 = bArr;
        A01.A02 = salt;
        A01.A00 = iterationCount;
        AnonymousClass20L A04 = i4 != 0 ? A01.A04(i3, i4) : A01.A03(i3);
        if (str.startsWith("DES")) {
            AnonymousClass20L r0 = A04;
            if (A04 instanceof C113075Fx) {
                r0 = ((C113075Fx) r0).A00;
            }
            AnonymousClass5OK.A00(((AnonymousClass20K) r0).A00);
        }
        return A04;
    }

    public static void A04(AnonymousClass5PQ r5) {
        r5.A0E = new Class[]{RC2ParameterSpec.class, RC5ParameterSpec.class, A0F, AnonymousClass5C2.class, IvParameterSpec.class, PBEParameterSpec.class};
        r5.A03 = -1;
        r5.A01 = 0;
        r5.A0C = true;
        r5.A06 = null;
        r5.A05 = null;
        r5.A04 = null;
    }

    public static void A05(AnonymousClass5PQ r4, boolean z) {
        Class[] clsArr = new Class[6];
        clsArr[0] = RC2ParameterSpec.class;
        clsArr[z ? 1 : 0] = RC5ParameterSpec.class;
        clsArr[2] = A0F;
        clsArr[3] = AnonymousClass5C2.class;
        clsArr[4] = IvParameterSpec.class;
        clsArr[5] = PBEParameterSpec.class;
        r4.A0E = clsArr;
        r4.A03 = -1;
        r4.A01 = 0;
        r4.A0C = z;
        r4.A06 = null;
        r4.A05 = null;
        r4.A04 = null;
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public int engineDoFinal(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        int AZZ;
        if (this.A0B.AEp(i2) + i3 <= bArr2.length) {
            if (i2 != 0) {
                try {
                    AZZ = this.A0B.AZZ(bArr, i, i2, bArr2, i3);
                } catch (C114975Nu e) {
                    throw new IllegalBlockSizeException(e.getMessage());
                } catch (AnonymousClass5O2 e2) {
                    throw new IllegalBlockSizeException(e2.getMessage());
                }
            } else {
                AZZ = 0;
            }
            return AZZ + this.A0B.A97(bArr2, i3 + AZZ);
        }
        throw new ShortBufferException("output buffer too short for input.");
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public byte[] engineDoFinal(byte[] bArr, int i, int i2) {
        int AEp = this.A0B.AEp(i2);
        byte[] bArr2 = new byte[AEp];
        int AZZ = i2 != 0 ? this.A0B.AZZ(bArr, i, i2, bArr2, 0) : 0;
        try {
            int A97 = AZZ + this.A0B.A97(bArr2, AZZ);
            if (A97 == AEp) {
                return bArr2;
            }
            if (A97 <= AEp) {
                byte[] bArr3 = new byte[A97];
                System.arraycopy(bArr2, 0, bArr3, 0, A97);
                return bArr3;
            }
            throw new IllegalBlockSizeException("internal buffer overflow");
        } catch (AnonymousClass5O2 e) {
            throw new IllegalBlockSizeException(e.getMessage());
        }
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public byte[] engineGetIV() {
        C113035Ft r0 = this.A08;
        if (r0 != null) {
            return AnonymousClass1TT.A02(r0.A03);
        }
        C113075Fx r02 = this.A09;
        if (r02 != null) {
            return r02.A01;
        }
        return null;
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public AlgorithmParameters engineGetParameters() {
        if (super.A01 == null) {
            if (this.A06 != null) {
                try {
                    A02(this.A05, this).init(this.A06);
                } catch (Exception unused) {
                    return null;
                }
            } else if (this.A08 != null) {
                if (this.A07 == null) {
                    try {
                        A02(AnonymousClass1TJ.A0r.A01, this).init(new AnonymousClass5N5(AnonymousClass1TT.A02(this.A08.A03)).A01());
                    } catch (Exception e) {
                        throw C12990iw.A0m(e.toString());
                    }
                } else {
                    try {
                        A02("GCM", this).init(new C114635Mm(AnonymousClass1TT.A02(this.A08.A03), this.A08.A00 / 8).A01());
                    } catch (Exception e2) {
                        throw C12990iw.A0m(e2.toString());
                    }
                }
            } else if (this.A09 != null) {
                String AAf = this.A0B.AHO().AAf();
                int indexOf = AAf.indexOf(47);
                if (indexOf >= 0) {
                    AAf = AAf.substring(0, indexOf);
                }
                try {
                    A02(AAf, this).init(new IvParameterSpec(this.A09.A01));
                } catch (Exception e3) {
                    throw C12990iw.A0m(e3.toString());
                }
            }
        }
        return super.A01;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        if (r0 != null) goto L_0x0021;
     */
    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void engineInit(int r4, java.security.Key r5, java.security.AlgorithmParameters r6, java.security.SecureRandom r7) {
        /*
            r3 = this;
            if (r6 == 0) goto L_0x001d
            java.lang.Class[] r2 = r3.A0E
            java.lang.Class<java.security.spec.AlgorithmParameterSpec> r0 = java.security.spec.AlgorithmParameterSpec.class
            java.security.spec.AlgorithmParameterSpec r0 = r6.getParameterSpec(r0)     // Catch: Exception -> 0x000b
            goto L_0x001f
        L_0x000b:
            r1 = 0
        L_0x000c:
            int r0 = r2.length
            if (r1 == r0) goto L_0x0027
            r0 = r2[r1]
            if (r0 == 0) goto L_0x001a
            r0 = r2[r1]     // Catch: Exception -> 0x001a
            java.security.spec.AlgorithmParameterSpec r0 = r6.getParameterSpec(r0)     // Catch: Exception -> 0x001a
            goto L_0x001f
        L_0x001a:
            int r1 = r1 + 1
            goto L_0x000c
        L_0x001d:
            r0 = 0
            goto L_0x0021
        L_0x001f:
            if (r0 == 0) goto L_0x0027
        L_0x0021:
            r3.engineInit(r4, r5, r0, r7)
            r3.A01 = r6
            return
        L_0x0027:
            java.lang.String r0 = "can't handle parameter "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = r6.toString()
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            java.security.InvalidAlgorithmParameterException r0 = X.C72463ee.A0J(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5PQ.engineInit(int, java.security.Key, java.security.AlgorithmParameters, java.security.SecureRandom):void");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:152:0x02ce */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:177:0x0334 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v5 */
    /* JADX WARN: Type inference failed for: r7v6, types: [X.20L] */
    /* JADX WARN: Type inference failed for: r1v23, types: [X.5Fx] */
    /* JADX WARN: Type inference failed for: r7v11, types: [X.5Ft] */
    /* JADX WARN: Type inference failed for: r7v18, types: [X.20L, X.5Fr] */
    /* JADX WARN: Type inference failed for: r7v19 */
    /* JADX WARN: Type inference failed for: r7v20, types: [X.5OL, X.20L] */
    /* JADX WARN: Type inference failed for: r7v22 */
    /* JADX WARN: Type inference failed for: r7v44 */
    /* JADX WARN: Type inference failed for: r7v45 */
    /* JADX WARN: Type inference failed for: r7v46 */
    /* JADX WARN: Type inference failed for: r7v49 */
    /* JADX WARN: Type inference failed for: r7v50 */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x02f8  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0342  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x036f A[Catch: IllegalArgumentException -> 0x039c, Exception -> 0x0391, TryCatch #4 {IllegalArgumentException -> 0x039c, Exception -> 0x0391, blocks: (B:185:0x0348, B:186:0x035f, B:187:0x0360, B:188:0x0363, B:189:0x0366, B:191:0x036f, B:193:0x0373), top: B:213:0x0340 }] */
    /* JADX WARNING: Removed duplicated region for block: B:214:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0172  */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void engineInit(int r18, java.security.Key r19, java.security.spec.AlgorithmParameterSpec r20, java.security.SecureRandom r21) {
        /*
        // Method dump skipped, instructions count: 963
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5PQ.engineInit(int, java.security.Key, java.security.spec.AlgorithmParameterSpec, java.security.SecureRandom):void");
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public void engineSetMode(String str) {
        AnonymousClass5XV r1;
        AnonymousClass5XE r2 = this.A07;
        if (r2 != null) {
            String A01 = AnonymousClass1T7.A01(str);
            this.A04 = A01;
            if (A01.equals("ECB")) {
                this.A01 = 0;
                r1 = new AnonymousClass5GS(r2);
            } else if (A01.equals("CBC")) {
                this.A01 = r2.AAt();
                r1 = new AnonymousClass5GS(new C112955Fl(r2));
            } else if (A01.startsWith("OFB")) {
                int AAt = r2.AAt();
                this.A01 = AAt;
                r1 = A01.length() != 3 ? new AnonymousClass5GS(new AnonymousClass5O4(r2, Integer.parseInt(A01.substring(3)))) : new AnonymousClass5GS(new AnonymousClass5O4(r2, AAt << 3));
            } else if (A01.startsWith("CFB")) {
                int AAt2 = r2.AAt();
                this.A01 = AAt2;
                r1 = A01.length() != 3 ? new AnonymousClass5GS(new AnonymousClass5O5(r2, Integer.parseInt(A01.substring(3)))) : new AnonymousClass5GS(new AnonymousClass5O5(r2, AAt2 << 3));
            } else if (A01.startsWith("PGPCFB")) {
                boolean equals = A01.equals("PGPCFBWITHIV");
                if (equals || A01.length() == 6) {
                    this.A01 = r2.AAt();
                    r1 = new AnonymousClass5GS(new C112975Fn(r2, equals));
                } else {
                    throw new NoSuchAlgorithmException(C12960it.A0d(A01, C12960it.A0k("no mode support for ")));
                }
            } else if (A01.equals("OPENPGPCFB")) {
                this.A01 = 0;
                r1 = new AnonymousClass5GS(new C112965Fm(r2));
            } else if (A01.equals("SIC")) {
                int AAt3 = r2.AAt();
                this.A01 = AAt3;
                if (AAt3 >= 16) {
                    this.A0C = false;
                    r1 = new AnonymousClass5GS(new C94474bs(new AnonymousClass5O8(r2)));
                } else {
                    throw C12970iu.A0f("Warning: SIC-Mode can become a twotime-pad if the blocksize of the cipher is too small. Use a cipher with a block size of at least 128 bits (e.g. AES)");
                }
            } else if (A01.equals("CTR")) {
                this.A01 = r2.AAt();
                this.A0C = false;
                r1 = new AnonymousClass5GS(new C94474bs(new AnonymousClass5O8(r2)));
            } else if (A01.equals("GOFB")) {
                this.A01 = r2.AAt();
                r1 = new AnonymousClass5GS(new C94474bs(new AnonymousClass5O6(r2)));
            } else if (A01.equals("GCFB")) {
                this.A01 = r2.AAt();
                r1 = new AnonymousClass5GS(new C94474bs(new AnonymousClass5O7(r2)));
            } else if (A01.equals("CTS")) {
                this.A01 = r2.AAt();
                r1 = new AnonymousClass5GS(new C114935Nq(new C112955Fl(r2)));
            } else if (A01.equals("CCM")) {
                this.A01 = 12;
                r1 = new AnonymousClass5GR(new AnonymousClass5G8(r2));
            } else if (A01.equals("OCB")) {
                if (this.A0A != null) {
                    this.A01 = 15;
                    r1 = new AnonymousClass5GR(new AnonymousClass5GA(r2, new C71643dG()));
                } else {
                    throw new NoSuchAlgorithmException(C12960it.A0d(str, C12960it.A0j("can't support mode ")));
                }
            } else if (A01.equals("EAX")) {
                this.A01 = r2.AAt();
                r1 = new AnonymousClass5GR(new AnonymousClass5G9(r2));
            } else if (A01.equals("GCM")) {
                this.A01 = r2.AAt();
                r1 = new AnonymousClass5GR(new AnonymousClass5GB(r2));
            } else {
                throw new NoSuchAlgorithmException(C12960it.A0d(str, C12960it.A0j("can't support mode ")));
            }
            this.A0B = r1;
            return;
        }
        throw new NoSuchAlgorithmException("no mode supported for this algorithm");
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public void engineSetPadding(String str) {
        AnonymousClass5GS r2;
        if (this.A07 != null) {
            String A01 = AnonymousClass1T7.A01(str);
            if (A01.equals("NOPADDING")) {
                AnonymousClass5XV r1 = this.A0B;
                if (r1.AgA()) {
                    r2 = new AnonymousClass5GS(new C94474bs(r1.AHO()));
                } else {
                    return;
                }
            } else if (A01.equals("WITHCTS") || A01.equals("CTSPADDING") || A01.equals("CS3PADDING")) {
                r2 = new AnonymousClass5GS(new C114935Nq(this.A0B.AHO()));
            } else {
                this.A0D = true;
                if (A06(this.A04)) {
                    throw new NoSuchPaddingException("Only NoPadding can be used with AEAD modes.");
                } else if (A01.equals("PKCS5PADDING") || A01.equals("PKCS7PADDING")) {
                    r2 = new AnonymousClass5GS(this.A0B.AHO());
                } else if (A01.equals("ZEROBYTEPADDING")) {
                    r2 = new AnonymousClass5GS(this.A0B.AHO(), new AnonymousClass5GF());
                } else if (A01.equals("ISO10126PADDING") || A01.equals("ISO10126-2PADDING")) {
                    r2 = new AnonymousClass5GS(this.A0B.AHO(), new AnonymousClass5GG());
                } else if (A01.equals("X9.23PADDING") || A01.equals("X923PADDING")) {
                    r2 = new AnonymousClass5GS(this.A0B.AHO(), new AnonymousClass5GH());
                } else if (A01.equals("ISO7816-4PADDING") || A01.equals("ISO9797-1PADDING")) {
                    r2 = new AnonymousClass5GS(this.A0B.AHO(), new AnonymousClass5GC());
                } else if (A01.equals("TBCPADDING")) {
                    r2 = new AnonymousClass5GS(this.A0B.AHO(), new AnonymousClass5GE());
                } else {
                    StringBuilder A0k = C12960it.A0k("Padding ");
                    A0k.append(str);
                    throw new NoSuchPaddingException(C12960it.A0d(" unknown.", A0k));
                }
            }
            this.A0B = r2;
            return;
        }
        throw new NoSuchPaddingException("no padding supported for this algorithm");
    }

    @Override // X.AnonymousClass5IV, javax.crypto.CipherSpi
    public int engineUpdate(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        AnonymousClass5XV r2 = this.A0B;
        if (r2.AHQ(i2) + i3 <= bArr2.length) {
            try {
                return r2.AZZ(bArr, i, i2, bArr2, i3);
            } catch (AnonymousClass5O2 e) {
                throw C12960it.A0U(e.toString());
            }
        } else {
            throw new ShortBufferException("output buffer too short for input.");
        }
    }

    @Override // javax.crypto.CipherSpi
    public void engineUpdateAAD(ByteBuffer byteBuffer) {
        int remaining = byteBuffer.remaining();
        if (remaining < 1) {
            return;
        }
        if (byteBuffer.hasArray()) {
            engineUpdateAAD(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), remaining);
            byteBuffer.position(byteBuffer.limit());
        } else if (remaining <= 512) {
            byte[] bArr = new byte[remaining];
            byteBuffer.get(bArr);
            engineUpdateAAD(bArr, 0, remaining);
            Arrays.fill(bArr, (byte) 0);
        } else {
            byte[] bArr2 = new byte[512];
            do {
                int min = Math.min(512, remaining);
                byteBuffer.get(bArr2, 0, min);
                engineUpdateAAD(bArr2, 0, min);
                remaining -= min;
            } while (remaining > 0);
            Arrays.fill(bArr2, (byte) 0);
        }
    }
}
