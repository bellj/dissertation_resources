package X;

import android.content.Context;
import com.whatsapp.storage.StorageUsageActivity;

/* renamed from: X.4r0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103594r0 implements AbstractC009204q {
    public final /* synthetic */ StorageUsageActivity A00;

    public C103594r0(StorageUsageActivity storageUsageActivity) {
        this.A00 = storageUsageActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
