package X;

import android.os.Build;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/* renamed from: X.01y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC004301y {
    public C004401z A00;
    public Set A01 = new HashSet();
    public UUID A02;
    public boolean A03 = false;

    public abstract AnonymousClass020 A01();

    public AbstractC004301y(Class cls) {
        UUID randomUUID = UUID.randomUUID();
        this.A02 = randomUUID;
        String obj = randomUUID.toString();
        String name = cls.getName();
        this.A00 = new C004401z(obj, name);
        this.A01.add(name);
    }

    public final AnonymousClass020 A00() {
        boolean z;
        AnonymousClass020 A01 = A01();
        C004101u r2 = this.A00.A09;
        int i = Build.VERSION.SDK_INT;
        if ((i < 24 || !r2.A03()) && !r2.A04 && !r2.A05 && (i < 23 || !r2.A04())) {
            z = false;
        } else {
            z = true;
        }
        if (!this.A00.A0H || !z) {
            this.A02 = UUID.randomUUID();
            C004401z r1 = new C004401z(this.A00);
            this.A00 = r1;
            r1.A0E = this.A02.toString();
            return A01;
        }
        throw new IllegalArgumentException("Expedited jobs only support network and storage constraints");
    }

    public void A02(long j, TimeUnit timeUnit) {
        this.A00.A03 = timeUnit.toMillis(j);
        if (Long.MAX_VALUE - System.currentTimeMillis() <= this.A00.A03) {
            throw new IllegalArgumentException("The given initial delay is too large and will cause an overflow!");
        }
    }

    public final void A03(EnumC007503u r9, TimeUnit timeUnit, long j) {
        this.A03 = true;
        C004401z r4 = this.A00;
        r4.A08 = r9;
        long millis = timeUnit.toMillis(j);
        if (millis > 18000000) {
            C06390Tk.A00().A05(C004401z.A0J, "Backoff delay duration exceeds maximum value", new Throwable[0]);
            millis = 18000000;
        } else if (millis < 10000) {
            C06390Tk.A00().A05(C004401z.A0J, "Backoff delay duration less than minimum value", new Throwable[0]);
            millis = 10000;
        }
        r4.A01 = millis;
    }
}
