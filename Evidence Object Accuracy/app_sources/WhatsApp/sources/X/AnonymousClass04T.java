package X;

import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.04T  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04T extends Dialog implements AbstractC002200x {
    public AnonymousClass025 A00;
    public final AbstractC001300o A01;

    @Override // X.AbstractC002200x
    public void AXC(AbstractC009504t r1) {
    }

    @Override // X.AbstractC002200x
    public void AXD(AbstractC009504t r1) {
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass04T(android.content.Context r6, int r7) {
        /*
            r5 = this;
            r0 = r7
            if (r7 != 0) goto L_0x0015
            android.util.TypedValue r3 = new android.util.TypedValue
            r3.<init>()
            android.content.res.Resources$Theme r2 = r6.getTheme()
            r1 = 2130968893(0x7f04013d, float:1.7546452E38)
            r0 = 1
            r2.resolveAttribute(r1, r3, r0)
            int r0 = r3.resourceId
        L_0x0015:
            r5.<init>(r6, r0)
            X.0Y5 r0 = new X.0Y5
            r0.<init>(r5)
            r5.A01 = r0
            X.025 r4 = r5.A00()
            if (r7 != 0) goto L_0x0037
            android.util.TypedValue r3 = new android.util.TypedValue
            r3.<init>()
            android.content.res.Resources$Theme r2 = r6.getTheme()
            r1 = 2130968893(0x7f04013d, float:1.7546452E38)
            r0 = 1
            r2.resolveAttribute(r1, r3, r0)
            int r7 = r3.resourceId
        L_0x0037:
            r0 = r4
            X.05o r0 = (X.LayoutInflater$Factory2C011505o) r0
            r0.A02 = r7
            r0 = 0
            r4.A0D(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass04T.<init>(android.content.Context, int):void");
    }

    public AnonymousClass025 A00() {
        AnonymousClass025 r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        LayoutInflater$Factory2C011505o r02 = new LayoutInflater$Factory2C011505o(getContext(), getWindow(), this, this);
        this.A00 = r02;
        return r02;
    }

    public void A01() {
        LayoutInflater$Factory2C011505o r3 = (LayoutInflater$Factory2C011505o) A00();
        if (r3.A0Z) {
            r3.A0Z = false;
        }
        r3.A0P();
        r3.A0i = true;
    }

    public boolean A02(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    @Override // android.app.Dialog
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A00().A0F(view, layoutParams);
    }

    @Override // android.app.Dialog, android.content.DialogInterface
    public void dismiss() {
        super.dismiss();
        A00().A08();
    }

    @Override // android.app.Dialog, android.view.Window.Callback
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return AnonymousClass0MK.A00(keyEvent, getWindow().getDecorView(), this, this.A01);
    }

    @Override // android.app.Dialog
    public View findViewById(int i) {
        LayoutInflater$Factory2C011505o r0 = (LayoutInflater$Factory2C011505o) A00();
        r0.A0M();
        return r0.A08.findViewById(i);
    }

    @Override // android.app.Dialog
    public void invalidateOptionsMenu() {
        A00().A07();
    }

    @Override // android.app.Dialog
    public void onCreate(Bundle bundle) {
        A00().A06();
        super.onCreate(bundle);
        A00().A0D(bundle);
    }

    @Override // android.app.Dialog
    public void onStop() {
        super.onStop();
        A00().A09();
    }

    @Override // android.app.Dialog
    public void setContentView(int i) {
        A00().A0A(i);
    }

    @Override // android.app.Dialog
    public void setContentView(View view) {
        A00().A0E(view);
    }

    @Override // android.app.Dialog
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        A00().A0G(view, layoutParams);
    }

    @Override // android.app.Dialog
    public void setTitle(int i) {
        super.setTitle(i);
        A00().A0H(getContext().getString(i));
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        A00().A0H(charSequence);
    }
}
