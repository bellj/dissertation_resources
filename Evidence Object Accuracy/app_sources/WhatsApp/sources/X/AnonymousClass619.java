package X;

import com.whatsapp.util.Log;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.619  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass619 {
    public final String A00;
    public final List A01;
    public final List A02;
    public final List A03;
    public final List A04;

    public AnonymousClass619(String str, List list, List list2, List list3, List list4) {
        this.A00 = str;
        this.A01 = list;
        this.A02 = list2;
        this.A04 = list3;
        this.A03 = list4;
    }

    public static AnonymousClass619 A00(AnonymousClass1V8 r11) {
        AnonymousClass1V8[] r3;
        AnonymousClass1V8[] r5;
        AnonymousClass1V8[] r52;
        AnonymousClass1V8[] r53;
        String A0H = r11.A0H("text");
        ArrayList A0l = C12960it.A0l();
        AnonymousClass1V8 A0E = r11.A0E("colors");
        if (!(A0E == null || (r53 = A0E.A03) == null)) {
            for (AnonymousClass1V8 r1 : r53) {
                A0l.add(new C119515eZ(r1));
            }
        }
        ArrayList A0l2 = C12960it.A0l();
        AnonymousClass1V8 A0E2 = r11.A0E("links");
        if (!(A0E2 == null || (r52 = A0E2.A03) == null)) {
            for (AnonymousClass1V8 r12 : r52) {
                A0l2.add(new C119525ea(r12));
            }
        }
        ArrayList A0l3 = C12960it.A0l();
        AnonymousClass1V8 A0E3 = r11.A0E("styles");
        if (!(A0E3 == null || (r5 = A0E3.A03) == null)) {
            for (AnonymousClass1V8 r13 : r5) {
                A0l3.add(new C119545ec(r13));
            }
        }
        ArrayList A0l4 = C12960it.A0l();
        AnonymousClass1V8 A0E4 = r11.A0E("scales");
        if (!(A0E4 == null || (r3 = A0E4.A03) == null)) {
            for (AnonymousClass1V8 r14 : r3) {
                A0l4.add(new C119535eb(r14));
            }
        }
        return new AnonymousClass619(A0H, A0l, A0l2, A0l3, A0l4);
    }

    public static AnonymousClass619 A01(AnonymousClass1V8 r0, String str) {
        return A00(r0.A0F(str));
    }

    public static AnonymousClass619 A02(JSONObject jSONObject) {
        String str = "color_ranges";
        if (!jSONObject.has(str)) {
            str = "colors";
        }
        JSONArray jSONArray = jSONObject.getJSONArray(str);
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < jSONArray.length(); i++) {
            A0l.add(new C119515eZ(jSONArray.getJSONObject(i)));
        }
        String str2 = "link_ranges";
        if (!jSONObject.has(str2)) {
            str2 = "links";
        }
        JSONArray jSONArray2 = jSONObject.getJSONArray(str2);
        ArrayList A0l2 = C12960it.A0l();
        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
            A0l2.add(new C119525ea(jSONArray2.getJSONObject(i2)));
        }
        String str3 = "inline_style_ranges";
        if (!jSONObject.has(str3)) {
            str3 = "styles";
        }
        JSONArray jSONArray3 = jSONObject.getJSONArray(str3);
        ArrayList A0l3 = C12960it.A0l();
        for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
            A0l3.add(new C119545ec(jSONArray3.getJSONObject(i3)));
        }
        String str4 = "scale_size_ranges";
        if (!jSONObject.has(str4)) {
            str4 = "scales";
        }
        JSONArray jSONArray4 = jSONObject.getJSONArray(str4);
        ArrayList A0l4 = C12960it.A0l();
        for (int i4 = 0; i4 < jSONArray4.length(); i4++) {
            A0l4.add(new C119535eb(jSONArray4.getJSONObject(i4)));
        }
        return new AnonymousClass619(jSONObject.getString("text"), A0l, A0l2, A0l3, A0l4);
    }

    public static List A03(List list) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            try {
                A0l.add(((C130745zu) it.next()).A01().toString());
            } catch (JSONException e) {
                Log.e(C12960it.A0b("[PAY] TextWithEntities/rangeToBloksList/exception: ", e));
            }
        }
        return A0l;
    }

    public static void A04(AnonymousClass619 r2, AbstractMap abstractMap) {
        abstractMap.put("body_text", r2.A00);
        abstractMap.put("body_colors", A03(r2.A01));
        abstractMap.put("body_links", A03(r2.A02));
        abstractMap.put("body_styles", A03(r2.A04));
        abstractMap.put("body_scales", A03(r2.A03));
    }

    public static void A05(Iterator it, JSONArray jSONArray) {
        jSONArray.put(((C130745zu) it.next()).A01());
    }

    public Object A06() {
        HashMap A11 = C12970iu.A11();
        A11.put("text", this.A00);
        ArrayList A0l = C12960it.A0l();
        Iterator it = this.A04.iterator();
        while (it.hasNext()) {
            C130745zu.A00(A0l, it);
        }
        A11.put("styles", A0l);
        ArrayList A0l2 = C12960it.A0l();
        Iterator it2 = this.A01.iterator();
        while (it2.hasNext()) {
            C130745zu.A00(A0l, it2);
        }
        A11.put("colors", A0l2);
        ArrayList A0l3 = C12960it.A0l();
        Iterator it3 = this.A02.iterator();
        while (it3.hasNext()) {
            C130745zu.A00(A0l, it3);
        }
        A11.put("links", A0l3);
        return A11;
    }

    public JSONObject A07() {
        JSONArray A0L = C117315Zl.A0L();
        Iterator it = this.A01.iterator();
        while (it.hasNext()) {
            A05(it, A0L);
        }
        JSONArray A0L2 = C117315Zl.A0L();
        Iterator it2 = this.A02.iterator();
        while (it2.hasNext()) {
            A05(it2, A0L2);
        }
        JSONArray A0L3 = C117315Zl.A0L();
        Iterator it3 = this.A04.iterator();
        while (it3.hasNext()) {
            A05(it3, A0L3);
        }
        JSONArray A0L4 = C117315Zl.A0L();
        Iterator it4 = this.A03.iterator();
        while (it4.hasNext()) {
            A05(it4, A0L4);
        }
        return C117295Zj.A0a().put("text", this.A00).put("color_ranges", A0L).put("link_ranges", A0L2).put("inline_style_ranges", A0L3).put("scale_size_ranges", A0L4);
    }
}
