package X;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;

/* renamed from: X.0S6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0S6 {
    public static final String A00 = C06390Tk.A01("WorkerFactory");

    public final ListenableWorker A00(Context context, WorkerParameters workerParameters, String str) {
        Throwable th;
        C06390Tk r3;
        String str2;
        String str3;
        ListenableWorker listenableWorker = null;
        try {
            Class<? extends U> asSubclass = Class.forName(str).asSubclass(ListenableWorker.class);
            if (asSubclass != 0) {
                try {
                    ListenableWorker listenableWorker2 = (ListenableWorker) asSubclass.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                    listenableWorker = listenableWorker2;
                    if (listenableWorker2 != null && listenableWorker2.A03) {
                        throw new IllegalStateException(String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", getClass().getName(), str));
                    }
                } catch (Throwable th2) {
                    th = th2;
                    r3 = C06390Tk.A00();
                    str2 = A00;
                    str3 = "Could not instantiate ";
                    StringBuilder sb = new StringBuilder(str3);
                    sb.append(str);
                    r3.A03(str2, sb.toString(), th);
                    return listenableWorker;
                }
            }
        } catch (Throwable th3) {
            th = th3;
            r3 = C06390Tk.A00();
            str2 = A00;
            str3 = "Invalid class: ";
        }
        return listenableWorker;
    }
}
