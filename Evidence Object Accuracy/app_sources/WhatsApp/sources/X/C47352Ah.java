package X;

import com.whatsapp.util.Log;
import java.util.Arrays;

/* renamed from: X.2Ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47352Ah {
    public final byte[] A00;
    public final byte[] A01;

    public C47352Ah(byte[] bArr, byte[] bArr2) {
        this.A00 = bArr;
        this.A01 = bArr2;
    }

    public static String A00(byte[] bArr) {
        int length;
        int i = 0;
        while (true) {
            length = bArr.length;
            if (i >= length || bArr[i] != 45) {
                break;
            }
            i++;
        }
        if (i != length) {
            return new String(bArr, i, length - i).trim();
        }
        Log.e("BackupFooter/verify-jid/empty-suffix");
        return "";
    }

    public C44561zA A01(C16600pJ r6, String str, String str2) {
        int i;
        StringBuilder sb = new StringBuilder("BackupFooter/verify-integrity/actual-digest/  ");
        sb.append(str);
        Log.i(sb.toString());
        String A04 = C003501n.A04(this.A00);
        StringBuilder sb2 = new StringBuilder("BackupFooter/verify-integrity/expected-digest/");
        sb2.append(A04);
        Log.i(sb2.toString());
        if (str.equals(A04)) {
            Log.i("BackupFooter/verify-integrity/digest-matches/success");
            if (this.A01 == null || str2 == null || !A02(str2)) {
                i = 1;
            } else {
                r6.A00("BackupFooter/verify-integrity/jid-mismatch", 4);
                return new C44561zA(4, null);
            }
        } else {
            StringBuilder sb3 = new StringBuilder("BackupFooter/verify-integrity/failed expected-digest:");
            sb3.append(A04);
            sb3.append(" actual-digest:");
            sb3.append(str);
            r6.A00(sb3.toString(), 4);
            i = 2;
        }
        return new C44561zA(i, null);
    }

    public boolean A02(String str) {
        byte[] bArr = this.A01;
        if (bArr == null) {
            return false;
        }
        String A00 = A00(bArr);
        if (str.endsWith(A00)) {
            return false;
        }
        StringBuilder sb = new StringBuilder("BackupFooter/has-jid-user-mismatch/expected-jid-user-ends-with: ");
        sb.append(A00);
        sb.append("  actual-jid-user: ");
        sb.append(str);
        Log.e(sb.toString());
        return true;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("BackupFooter{digest=");
        sb.append(Arrays.toString(this.A00));
        sb.append(", jidSuffix=");
        byte[] bArr = this.A01;
        if (bArr != null) {
            str = Arrays.toString(bArr);
        } else {
            str = "null";
        }
        sb.append(str);
        sb.append('}');
        return sb.toString();
    }
}
