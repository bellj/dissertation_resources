package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.3Z7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z7 implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final AnonymousClass5WD A01;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    public /* synthetic */ AnonymousClass3Z7(AbstractC15710nm r1, AnonymousClass5WD r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        this.A01.APl(C41151sz.A00(r3));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r20, String str) {
        AnonymousClass1V8 A0E = r20.A0E("linked_group");
        if (A0E != null) {
            List A0J = A0E.A0J("group");
            if (A0J.size() != 0) {
                AnonymousClass1V8 r3 = (AnonymousClass1V8) C12980iv.A0o(A0J);
                AbstractC15710nm r2 = this.A00;
                UserJid userJid = (UserJid) r3.A0A(r2, UserJid.class, "creator");
                long A01 = C28421Nd.A01(r3.A0I("creation", null), 0) * 1000;
                String A0I = r3.A0I("subject", null);
                long A012 = C28421Nd.A01(r3.A0I("s_t", null), 0) * 1000;
                int i = 0;
                if (r3.A0E("default_sub_group") != null) {
                    i = 3;
                }
                String A0I2 = r3.A0I("id", null);
                if (A0I2 != null) {
                    try {
                        C15580nU A013 = C15380n4.A01(A0I2);
                        HashMap A11 = C12970iu.A11();
                        C50862Rh.A05(r2, r3, A11);
                        this.A01.AWt(A013, userJid, C50862Rh.A03(r2, r3, r3.A0E("description")), A0I, A11, C28421Nd.A00(r3.A0I("size", null), A11.size()), i, A01, A012);
                    } catch (AnonymousClass1MW e) {
                        Log.e("GetSubgroupInfoProtocolCallbackonSuccess/invalid jid exception", e);
                    }
                }
            }
        }
    }
}
