package X;

import android.content.Intent;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.BrazilPaymentTransactionDetailActivity;

/* renamed from: X.5om  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124205om extends AbstractC16350or {
    public final /* synthetic */ AbstractC30791Yv A00;
    public final /* synthetic */ C30821Yy A01;
    public final /* synthetic */ AbstractC28901Pl A02;
    public final /* synthetic */ AnonymousClass1KC A03;
    public final /* synthetic */ C119815f9 A04;
    public final /* synthetic */ BrazilPaymentActivity A05;
    public final /* synthetic */ C28861Ph A06;
    public final /* synthetic */ String A07;
    public final /* synthetic */ String A08;

    public C124205om(AbstractC30791Yv r1, C30821Yy r2, AbstractC28901Pl r3, AnonymousClass1KC r4, C119815f9 r5, BrazilPaymentActivity brazilPaymentActivity, C28861Ph r7, String str, String str2) {
        this.A05 = brazilPaymentActivity;
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A04 = r5;
        this.A07 = str;
        this.A08 = str2;
        this.A06 = r7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r3 == null) goto L_0x0011;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r32) {
        /*
            r31 = this;
            r0 = r31
            com.whatsapp.payments.ui.BrazilPaymentActivity r1 = r0.A05
            com.whatsapp.payments.ui.widget.PaymentView r2 = r1.A2e()
            if (r2 == 0) goto L_0x0011
            X.1KS r3 = r2.getStickerIfSelected()
            r2 = 1
            if (r3 != 0) goto L_0x0012
        L_0x0011:
            r2 = 0
        L_0x0012:
            java.lang.String r4 = "p2m"
            if (r2 == 0) goto L_0x0089
            X.5xI r12 = r1.A0W
            com.whatsapp.payments.ui.widget.PaymentView r2 = r1.A0V
            if (r2 == 0) goto L_0x0086
            X.1KS r20 = r2.getStickerIfSelected()
        L_0x0020:
            X.AnonymousClass009.A05(r20)
            X.1KC r5 = r0.A03
            r28 = 0
            if (r5 == 0) goto L_0x0083
            X.0lQ r2 = r5.A0J
            X.AnonymousClass009.A05(r2)
            java.lang.String r8 = r2.A0D
        L_0x0030:
            X.0lm r7 = r1.A0E
            X.AnonymousClass009.A05(r7)
            com.whatsapp.jid.UserJid r6 = r1.A0G
            long r2 = r1.A02
            r10 = 0
            int r9 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r9 == 0) goto L_0x0047
            X.0ng r9 = r1.A09
            X.0ot r9 = r9.A0K
            X.0mz r28 = r9.A00(r2)
        L_0x0047:
            com.whatsapp.payments.ui.widget.PaymentView r2 = r1.A0V
            if (r2 == 0) goto L_0x0080
            java.lang.Integer r30 = r2.getStickerSendOrigin()
        L_0x004f:
            X.1Yy r14 = r0.A01
            X.1Yv r13 = r0.A00
            X.1Pl r15 = r0.A02
            X.5f9 r2 = r0.A04
            java.lang.String r3 = r1.A0l
            java.lang.String r1 = r0.A07
            r17 = 0
            java.lang.String r0 = r0.A08
            boolean r24 = r4.equals(r0)
            X.0vc r0 = r12.A04
            r25 = r0
            r26 = r7
            r27 = r6
            r29 = r20
            X.1Vy r19 = r25.A02(r26, r27, r28, r29, r30)
            r21 = r8
            r22 = r3
            r23 = r1
            r18 = r5
            r16 = r2
            X.1IR r0 = r12.A00(r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return r0
        L_0x0080:
            r30 = 0
            goto L_0x004f
        L_0x0083:
            r8 = r28
            goto L_0x0030
        L_0x0086:
            r20 = 0
            goto L_0x0020
        L_0x0089:
            X.0sj r5 = r1.A0M
            X.1Ph r11 = r0.A06
            X.1Yy r7 = r0.A01
            X.1Yv r6 = r0.A00
            X.1Pl r8 = r0.A02
            X.5f9 r9 = r0.A04
            java.lang.String r12 = r1.A0l
            java.lang.String r13 = r0.A07
            r10 = 0
            java.lang.String r0 = r0.A08
            boolean r14 = r4.equals(r0)
            X.1IR r0 = r5.A02(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124205om.A05(java.lang.Object[]):java.lang.Object");
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass1IR r5 = (AnonymousClass1IR) obj;
        BrazilPaymentActivity brazilPaymentActivity = this.A05;
        brazilPaymentActivity.A0N.AL9("send_payment", brazilPaymentActivity.A00);
        brazilPaymentActivity.A0F.A03(brazilPaymentActivity.A0c);
        brazilPaymentActivity.A0a = true;
        if (!brazilPaymentActivity.A0u || r5 == null) {
            brazilPaymentActivity.A2g(0);
            return;
        }
        Intent A0D = C12990iw.A0D(brazilPaymentActivity, BrazilPaymentTransactionDetailActivity.class);
        C117305Zk.A11(A0D, r5, r5.A0C);
        A0D.putExtra("referral_screen", brazilPaymentActivity.A0Y);
        A0D.setFlags(67108864);
        A0D.putExtra("extra_action_bar_display_close", true);
        brazilPaymentActivity.A2G(A0D, true);
        brazilPaymentActivity.AaN();
    }
}
