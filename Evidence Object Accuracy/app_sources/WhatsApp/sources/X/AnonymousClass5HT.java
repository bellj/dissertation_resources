package X;

import java.lang.ref.WeakReference;

/* renamed from: X.5HT  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5HT extends WeakReference {
    public final int A00;

    public AnonymousClass5HT(Throwable th) {
        super(th, null);
        this.A00 = System.identityHashCode(th);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == AnonymousClass5HT.class) {
            if (this != obj) {
                AnonymousClass5HT r5 = (AnonymousClass5HT) obj;
                if (!(this.A00 == r5.A00 && get() == r5.get())) {
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.A00;
    }
}
