package X;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Vibrator;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.DoodleView;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3MO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MO implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener {
    public final GestureDetector.OnGestureListener A00;
    public final AnonymousClass0MU A01;
    public final AnonymousClass5Yv A02;
    public final DoodleView A03;
    public final AnonymousClass3DK A04;
    public final C64673Gl A05;
    public final C91464Rv A06 = new C91464Rv(this);
    public final C73653ga A07;
    public final C89544Kj A08;
    public final C454721t A09;

    public AnonymousClass3MO(GestureDetector.OnGestureListener onGestureListener, AnonymousClass5Yv r4, DoodleView doodleView, AnonymousClass3DK r6, C89544Kj r7, C454721t r8) {
        this.A03 = doodleView;
        this.A04 = r6;
        this.A08 = r7;
        this.A09 = r8;
        this.A02 = r4;
        this.A00 = onGestureListener;
        this.A01 = new AnonymousClass0MU(doodleView.getContext(), this);
        C73653ga r0 = new C73653ga(doodleView.getContext(), this);
        this.A07 = r0;
        C04200Kt.A00(r0);
        this.A05 = new C64673Gl(doodleView.getContext(), this);
    }

    public void A00(PointF pointF, PointF pointF2) {
        float f;
        float f2;
        PointF pointF3 = pointF;
        AnonymousClass3DK r6 = this.A04;
        AbstractC454821u r0 = r6.A01;
        if (r0 != null && !(r0 instanceof AnonymousClass33J)) {
            if (pointF3.x != 0.0f || pointF3.y != 0.0f) {
                r6.A00();
                AnonymousClass3EC r3 = r6.A04;
                RectF rectF = r6.A01.A02;
                PointF A01 = r3.A01(new PointF(rectF.centerX(), rectF.centerY()));
                if (r6.A03.A01 == 1.0f) {
                    C64213Er r13 = r6.A05;
                    AbstractC454821u r12 = r6.A01;
                    if (r13.A04 != r12) {
                        r13.A04 = r12;
                        r13.A01();
                    }
                    float f3 = pointF3.x;
                    float f4 = pointF3.y;
                    RectF rectF2 = r13.A02;
                    PointF pointF4 = new PointF(rectF2.centerX(), rectF2.centerY());
                    if (r13.A06) {
                        f = r13.A00;
                    } else {
                        f = A01.x;
                    }
                    float f5 = f + f3;
                    if (r13.A07) {
                        f2 = r13.A01;
                    } else {
                        f2 = A01.y;
                    }
                    PointF pointF5 = new PointF(f5, f2 + f4);
                    if (r13.A07) {
                        float centerY = rectF2.centerY();
                        float f6 = r13.A08;
                        if (centerY + f6 < pointF5.y || rectF2.centerY() - f6 > pointF5.y) {
                            r13.A07 = false;
                            f4 += r13.A01 - A01.y;
                            r13.A01 = 0.0f;
                        } else {
                            r13.A01 += f4;
                            f4 = 0.0f;
                        }
                    } else {
                        float f7 = A01.y;
                        float f8 = pointF4.y;
                        if ((f7 <= f8 && pointF3.y + f7 >= f8) || (f7 >= f8 && pointF3.y + f7 <= f8)) {
                            r13.A07 = true;
                            r13.A01 = pointF5.y;
                            f4 = f8 - f7;
                        }
                    }
                    if (r13.A06) {
                        float centerX = rectF2.centerX();
                        float f9 = r13.A08;
                        if (centerX + f9 < pointF5.x || rectF2.centerX() - f9 > pointF5.x) {
                            r13.A06 = false;
                            f3 += r13.A00 - A01.x;
                            r13.A00 = 0.0f;
                        } else {
                            r13.A00 += f3;
                            f3 = 0.0f;
                        }
                    } else {
                        float f10 = A01.x;
                        float f11 = pointF4.x;
                        if ((f10 <= f11 && pointF3.x + f10 >= f11) || (f10 >= f11 && pointF3.x + f10 <= f11)) {
                            r13.A06 = true;
                            r13.A00 = pointF5.x;
                            f3 = f11 - f10;
                        }
                    }
                    PointF pointF6 = new PointF(f3, f4);
                    PointF pointF7 = new PointF(A01.x + pointF6.x, A01.y + pointF6.y);
                    Map map = r13.A05;
                    AnonymousClass338 r5 = (AnonymousClass338) map.get(C12970iu.A0h());
                    if (r5.A03) {
                        r13.A00(pointF7, r12, 0.0f);
                    }
                    float f12 = pointF3.x;
                    float f13 = pointF3.y;
                    boolean A1U = C12960it.A1U((Math.sqrt((double) ((f12 * f12) + (f13 * f13))) > 200.0d ? 1 : (Math.sqrt((double) ((f12 * f12) + (f13 * f13))) == 200.0d ? 0 : -1)));
                    Iterator A0o = C12960it.A0o(map);
                    while (A0o.hasNext()) {
                        AbstractC64383Fi r7 = (AbstractC64383Fi) A0o.next();
                        if (!(r7 instanceof AnonymousClass338)) {
                            int i = ((AnonymousClass337) r7).A00;
                            if (i == 1) {
                                if (r13.A07) {
                                    if (!A1U) {
                                        if (r5.A03 && r5.A01 == 0) {
                                        }
                                        r7.A02();
                                    }
                                }
                                r7.A01();
                            } else if (i == 2) {
                                if (r13.A06) {
                                    if (!A1U) {
                                        if (r5.A03 && r5.A01 == 2) {
                                        }
                                        r7.A02();
                                    }
                                }
                                r7.A01();
                            }
                        }
                    }
                    pointF3 = pointF6;
                }
                C64073Ed r72 = r6.A06;
                boolean A012 = r72.A01(pointF2.x, pointF2.y);
                boolean z = r72.A00;
                if (A012) {
                    if (!z) {
                        AnonymousClass2ZR r2 = r72.A04;
                        r2.A00 = C12960it.A09(r72.A03).getColor(R.color.trashHoverBackground);
                        r2.invalidateSelf();
                        Vibrator vibrator = r72.A02;
                        if (vibrator != null) {
                            try {
                                vibrator.vibrate(3);
                            } catch (NullPointerException e) {
                                Log.e("Vibrator is broken on this device.", e);
                            }
                        }
                    }
                } else if (z) {
                    r72.A00();
                }
                r72.A00 = A012;
                float f14 = pointF3.x;
                float f15 = pointF3.y;
                float[] fArr = r3.A02;
                fArr[0] = f14;
                fArr[1] = f15;
                Matrix matrix = r3.A00;
                matrix.reset();
                C64243Eu r32 = r3.A01;
                matrix.setRotate((float) (-r32.A02));
                matrix.mapPoints(fArr);
                float f16 = fArr[0];
                float f17 = r32.A00 * r32.A01;
                PointF pointF8 = new PointF(f16 / f17, fArr[1] / f17);
                r6.A01.A02.offset(pointF8.x, pointF8.y);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0036, code lost:
        if (((r1 * r1) + (r0 * r0)) <= (r2 * r2)) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A01(float r8, float r9) {
        /*
            r7 = this;
            X.21t r1 = r7.A09
            r0 = 0
            r1.A02 = r0
            r1.A00 = r0
            X.21u r4 = r1.A01
            X.5Yv r0 = r7.A02
            X.3YI r0 = (X.AnonymousClass3YI) r0
            X.2Ab r5 = r0.A01
            X.3Fp r6 = r5.A03
            boolean r0 = r6 instanceof X.AnonymousClass333
            if (r0 == 0) goto L_0x003f
            X.333 r6 = (X.AnonymousClass333) r6
            if (r4 == 0) goto L_0x0038
            com.whatsapp.mediacomposer.VideoComposerFragment r0 = r6.A00
            android.view.View r3 = r0.A07
            float r2 = X.C12990iw.A03(r3)
            r0 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r0
            float r1 = r3.getX()
            float r1 = r1 + r2
            float r0 = r3.getY()
            float r0 = r0 + r2
            float r1 = r1 - r8
            float r0 = r0 - r9
            float r1 = r1 * r1
            float r0 = r0 * r0
            float r1 = r1 + r0
            float r2 = r2 * r2
            int r0 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x003f
        L_0x0038:
            com.whatsapp.mediacomposer.VideoComposerFragment r0 = r6.A00
            r0.A1J()
        L_0x003d:
            r1 = 1
        L_0x003e:
            return r1
        L_0x003f:
            boolean r0 = r4 instanceof X.AnonymousClass33L
            if (r0 == 0) goto L_0x005e
            X.2Ag r1 = r5.A0Q
            r0 = 3
            r1.A09(r0)
            r0 = r4
            X.33L r0 = (X.AnonymousClass33L) r0
            r5.A07(r0)
        L_0x004f:
            r1 = 0
            if (r4 == 0) goto L_0x003e
            boolean r0 = r4.A0E()
            if (r0 == 0) goto L_0x003e
            com.whatsapp.mediacomposer.doodle.DoodleView r0 = r7.A03
            r0.invalidate()
            goto L_0x003d
        L_0x005e:
            if (r4 == 0) goto L_0x004f
            boolean r0 = r4.A0J()
            if (r0 == 0) goto L_0x004f
            X.2Ag r1 = r5.A0Q
            android.graphics.Paint r0 = r4.A01
            int r0 = r0.getColor()
            r1.A01 = r0
            r0 = 2
            r1.A09(r0)
            com.whatsapp.mediacomposer.doodle.ColorPickerComponent r0 = r5.A0E
            r0.A00()
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3MO.A01(float, float):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0055  */
    @Override // android.view.GestureDetector.OnDoubleTapListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onDoubleTap(android.view.MotionEvent r6) {
        /*
            r5 = this;
            X.21t r4 = r5.A09
            r0 = 0
            r4.A02 = r0
            r4.A00 = r0
            X.21u r0 = r4.A01
            if (r0 == 0) goto L_0x0053
            java.util.List r3 = r4.A04
            boolean r0 = r3.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0053
            X.21u r0 = r4.A01
            if (r0 != 0) goto L_0x004e
            r2 = -1
        L_0x001a:
            int r1 = r3.size()
            r0 = 1
            int r1 = r1 - r0
            if (r2 == r1) goto L_0x0053
            X.21u r2 = r4.A01
            if (r2 != 0) goto L_0x0049
            r0 = -1
        L_0x0027:
            X.45O r1 = new X.45O
            r1.<init>(r2, r0)
            X.3Df r0 = r4.A03
            java.util.LinkedList r0 = r0.A00
            r0.add(r1)
            X.21u r0 = r4.A01
            r3.remove(r0)
            X.21u r0 = r4.A01
            r3.add(r0)
            r0 = 1
        L_0x003e:
            r1 = 1
            if (r0 == 0) goto L_0x0055
            com.whatsapp.mediacomposer.doodle.DoodleView r0 = r5.A03
            X.21v r0 = r0.A0E
            r0.A01()
            return r1
        L_0x0049:
            int r0 = r3.indexOf(r2)
            goto L_0x0027
        L_0x004e:
            int r2 = r3.indexOf(r0)
            goto L_0x001a
        L_0x0053:
            r0 = 0
            goto L_0x003e
        L_0x0055:
            android.view.GestureDetector$OnGestureListener r0 = r5.A00
            if (r0 == 0) goto L_0x0062
            android.view.GestureDetector$OnDoubleTapListener r0 = (android.view.GestureDetector.OnDoubleTapListener) r0
            boolean r0 = r0.onDoubleTap(r6)
            if (r0 == 0) goto L_0x0062
            return r1
        L_0x0062:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3MO.onDoubleTap(android.view.MotionEvent):boolean");
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        GestureDetector.OnGestureListener onGestureListener = this.A00;
        return onGestureListener != null && ((GestureDetector.OnDoubleTapListener) onGestureListener).onDoubleTapEvent(motionEvent);
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        GestureDetector.OnGestureListener onGestureListener;
        return this.A09.A01 == null && (onGestureListener = this.A00) != null && onGestureListener.onDown(motionEvent);
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        GestureDetector.OnGestureListener onGestureListener;
        return this.A09.A01 == null && (onGestureListener = this.A00) != null && onGestureListener.onFling(motionEvent, motionEvent2, f, f2);
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
        GestureDetector.OnGestureListener onGestureListener = this.A00;
        if (onGestureListener != null) {
            onGestureListener.onLongPress(motionEvent);
        }
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        float scaleFactor = scaleGestureDetector.getScaleFactor();
        float f = ((C73653ga) scaleGestureDetector).A00;
        AbstractC454821u r7 = this.A08.A00;
        if (r7 == null) {
            GestureDetector.OnGestureListener onGestureListener = this.A00;
            if (onGestureListener == null || !((ScaleGestureDetector.OnScaleGestureListener) onGestureListener).onScale(scaleGestureDetector)) {
                return false;
            }
            return true;
        } else if (r7 instanceof AnonymousClass33I) {
            int i = 0;
            if (Math.abs(f) < 1.0f) {
                i = 1;
            }
            r7.A07(scaleFactor, i ^ 1);
            return true;
        } else if ((r7 instanceof AnonymousClass33G) || (r7 instanceof AnonymousClass33F)) {
            int i2 = 0;
            if (Math.abs(Math.tan(Math.atan((double) f) - Math.toRadians((double) r7.A00))) < 1.0d) {
                i2 = 1;
            }
            r7.A07(scaleFactor, 1 ^ i2);
            return true;
        } else {
            r7.A06(scaleFactor);
            return true;
        }
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        C89544Kj r1 = this.A08;
        AbstractC454821u r0 = this.A09.A01;
        r1.A00 = r0;
        if (r0 != null) {
            return true;
        }
        GestureDetector.OnGestureListener onGestureListener = this.A00;
        return onGestureListener != null && ((ScaleGestureDetector.OnScaleGestureListener) onGestureListener).onScaleBegin(scaleGestureDetector);
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        this.A08.A00 = null;
        GestureDetector.OnGestureListener onGestureListener = this.A00;
        if (onGestureListener != null) {
            ((ScaleGestureDetector.OnScaleGestureListener) onGestureListener).onScaleEnd(scaleGestureDetector);
        }
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        GestureDetector.OnGestureListener onGestureListener;
        return this.A09.A01 == null && (onGestureListener = this.A00) != null && onGestureListener.onScroll(motionEvent, motionEvent2, f, f2);
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onShowPress(MotionEvent motionEvent) {
        GestureDetector.OnGestureListener onGestureListener = this.A00;
        if (onGestureListener != null) {
            onGestureListener.onShowPress(motionEvent);
        }
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        int indexOf;
        GestureDetector.OnGestureListener onGestureListener;
        C454721t r1 = this.A09;
        List list = r1.A04;
        if (list.size() == 1) {
            return false;
        }
        AbstractC454821u r0 = r1.A01;
        if (r0 == null) {
            indexOf = -1;
        } else {
            indexOf = list.indexOf(r0);
        }
        if (indexOf == C12990iw.A09(list, 1)) {
            return false;
        }
        if (A01(motionEvent.getX(), motionEvent.getY()) || ((onGestureListener = this.A00) != null && ((GestureDetector.OnDoubleTapListener) onGestureListener).onSingleTapConfirmed(motionEvent))) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
        if (r1 == X.C12990iw.A09(r2, 1)) goto L_0x0020;
     */
    @Override // android.view.GestureDetector.OnGestureListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onSingleTapUp(android.view.MotionEvent r5) {
        /*
            r4 = this;
            com.whatsapp.mediacomposer.doodle.DoodleView r0 = r4.A03
            X.21u r0 = r0.A00(r5)
            X.21t r1 = r4.A09
            r1.A01 = r0
            r3 = 1
            if (r0 == 0) goto L_0x0032
            java.util.List r2 = r1.A04
            int r0 = r2.size()
            if (r0 == r3) goto L_0x0020
            X.21u r0 = r1.A01
            if (r0 != 0) goto L_0x002d
            r1 = -1
        L_0x001a:
            int r0 = X.C12990iw.A09(r2, r3)
            if (r1 != r0) goto L_0x0032
        L_0x0020:
            float r1 = r5.getX()
            float r0 = r5.getY()
            boolean r0 = r4.A01(r1, r0)
            return r0
        L_0x002d:
            int r1 = r2.indexOf(r0)
            goto L_0x001a
        L_0x0032:
            android.view.GestureDetector$OnGestureListener r0 = r4.A00
            if (r0 == 0) goto L_0x003d
            boolean r0 = r0.onSingleTapUp(r5)
            if (r0 == 0) goto L_0x003d
            return r3
        L_0x003d:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3MO.onSingleTapUp(android.view.MotionEvent):boolean");
    }
}
