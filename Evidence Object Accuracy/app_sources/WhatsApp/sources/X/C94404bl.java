package X;

import android.net.Uri;

/* renamed from: X.4bl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94404bl {
    public static final AnonymousClass4XL A0E;
    public static final Object A0F = C12970iu.A0l();
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public AnonymousClass4XK A06;
    public AnonymousClass4XL A07 = A0E;
    @Deprecated
    public Object A08;
    public Object A09 = A0F;
    public boolean A0A;
    @Deprecated
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;

    static {
        AnonymousClass3DU r1 = new AnonymousClass3DU();
        r1.A07 = "com.google.android.exoplayer2.Timeline";
        r1.A06 = Uri.EMPTY;
        A0E = r1.A00();
    }

    public void A00(AnonymousClass4XK r3, AnonymousClass4XL r4, Object obj, long j, long j2, long j3, long j4, boolean z, boolean z2) {
        AnonymousClass4XL r0;
        Object obj2;
        AnonymousClass4XB r02;
        this.A09 = obj;
        if (r4 != null) {
            r0 = r4;
        } else {
            r0 = A0E;
        }
        this.A07 = r0;
        if (r4 == null || (r02 = r4.A02) == null) {
            obj2 = null;
        } else {
            obj2 = r02.A01;
        }
        this.A08 = obj2;
        this.A04 = j;
        this.A05 = j2;
        this.A03 = j3;
        this.A0D = z;
        this.A0A = z2;
        this.A0B = C12960it.A1W(r3);
        this.A06 = r3;
        this.A02 = j4;
        this.A00 = 0;
        this.A01 = 0;
        this.A0C = false;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || !C94404bl.class.equals(obj.getClass())) {
                return false;
            }
            C94404bl r7 = (C94404bl) obj;
            if (!(AnonymousClass3JZ.A0H(this.A09, r7.A09) && AnonymousClass3JZ.A0H(this.A07, r7.A07) && AnonymousClass3JZ.A0H(null, null) && AnonymousClass3JZ.A0H(this.A06, r7.A06) && this.A04 == r7.A04 && this.A05 == r7.A05 && this.A03 == r7.A03 && this.A0D == r7.A0D && this.A0A == r7.A0A && this.A0C == r7.A0C && this.A02 == r7.A02 && this.A00 == r7.A00 && this.A01 == r7.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int A08 = ((C12990iw.A08(this.A07, (217 + this.A09.hashCode()) * 31) * 31) + 0) * 31;
        AnonymousClass4XK r0 = this.A06;
        if (r0 != null) {
            i = r0.hashCode();
        }
        int i2 = (int) (0 ^ (0 >>> 32));
        return ((((C72453ed.A0A((((((((C72453ed.A0A(C72453ed.A0A(C72453ed.A0A((A08 + i) * 31, this.A04), this.A05), this.A03) + (this.A0D ? 1 : 0)) * 31) + (this.A0A ? 1 : 0)) * 31) + (this.A0C ? 1 : 0)) * 31) + i2) * 31, this.A02) + this.A00) * 31) + this.A01) * 31) + i2;
    }
}
