package X;

/* renamed from: X.5xQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129255xQ {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C17220qS A04;
    public final C18650sn A05;
    public final C18660so A06;
    public final C17070qD A07;
    public final AbstractC14440lR A08;

    public C129255xQ(C14900mE r1, C15570nT r2, C14830m7 r3, C16590pI r4, C17220qS r5, C18650sn r6, C18660so r7, C17070qD r8, AbstractC14440lR r9) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        this.A08 = r9;
        this.A07 = r8;
        this.A05 = r6;
        this.A06 = r7;
    }

    public void A00(AnonymousClass1FK r10) {
        String A0U = C117295Zj.A0U(this.A01, this.A02);
        C17220qS r3 = this.A04;
        String A01 = r3.A01();
        C117295Zj.A1B(r3, new C120175fk(this.A03.A00, this.A00, r10, this.A05, this), new C126295se(new AnonymousClass3CT(A01), A0U).A00, A01);
    }
}
