package X;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import com.whatsapp.R;

/* renamed from: X.3Ip  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65203Ip {
    public static final int[] A00 = {16843848};

    public static void A00(View view) {
        view.setOutlineProvider(ViewOutlineProvider.BOUNDS);
    }

    public static void A01(View view, float f) {
        int integer = view.getResources().getInteger(R.integer.app_bar_elevation_anim_duration);
        StateListAnimator stateListAnimator = new StateListAnimator();
        long j = (long) integer;
        stateListAnimator.addState(new int[]{16842766, R.attr.state_liftable, -R.attr.state_lifted}, ObjectAnimator.ofFloat(view, "elevation", 0.0f).setDuration(j));
        stateListAnimator.addState(new int[]{16842766}, ObjectAnimator.ofFloat(view, "elevation", f).setDuration(j));
        stateListAnimator.addState(new int[0], ObjectAnimator.ofFloat(view, "elevation", 0.0f).setDuration(0L));
        view.setStateListAnimator(stateListAnimator);
    }

    public static void A02(View view, AttributeSet attributeSet) {
        Context context = view.getContext();
        TypedArray A002 = C50582Qc.A00(context, attributeSet, A00, new int[0], 0, 2131952622);
        try {
            if (A002.hasValue(0)) {
                view.setStateListAnimator(AnimatorInflater.loadStateListAnimator(context, A002.getResourceId(0, 0)));
            }
        } finally {
            A002.recycle();
        }
    }
}
