package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;

/* renamed from: X.082  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass082 {
    public static final C006202y A00 = new C006202y(16);
    public static final C06400Tl A01;

    static {
        C06400Tl r0;
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            r0 = new AnonymousClass0DI();
        } else if (i >= 28) {
            r0 = new AnonymousClass0DF();
        } else if (i >= 26) {
            r0 = new AnonymousClass0DG();
        } else if (i >= 24 && AnonymousClass0DJ.A00()) {
            r0 = new AnonymousClass0DJ();
        } else if (i >= 21) {
            r0 = new AnonymousClass0DH();
        } else {
            r0 = new C06400Tl();
        }
        A01 = r0;
    }

    public static Typeface A00(Context context, Typeface typeface, int i) {
        AnonymousClass0MQ r1;
        Typeface A04;
        if (context != null) {
            if (Build.VERSION.SDK_INT < 21) {
                C06400Tl r5 = A01;
                long A02 = C06400Tl.A02(typeface);
                if (!(A02 == 0 || (r1 = (AnonymousClass0MQ) r5.A00.get(Long.valueOf(A02))) == null || (A04 = r5.A04(context, context.getResources(), r1, i)) == null)) {
                    return A04;
                }
            }
            return Typeface.create(typeface, i);
        }
        throw new IllegalArgumentException("Context cannot be null");
    }

    public static String A01(Resources resources, String str, int i, int i2, int i3) {
        StringBuilder sb = new StringBuilder(resources.getResourcePackageName(i));
        sb.append('-');
        sb.append(str);
        sb.append('-');
        sb.append(i2);
        sb.append('-');
        sb.append(i);
        sb.append('-');
        sb.append(i3);
        return sb.toString();
    }
}
