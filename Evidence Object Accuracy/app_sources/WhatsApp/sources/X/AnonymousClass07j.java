package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.widget.ImageView;

/* renamed from: X.07j  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07j {
    public C016107p A00;
    public C016107p A01;
    public final ImageView A02;

    public AnonymousClass07j(ImageView imageView) {
        this.A02 = imageView;
    }

    public void A00() {
        ImageView imageView = this.A02;
        Drawable drawable = imageView.getDrawable();
        if (drawable != null) {
            C014706y.A02(drawable);
            int i = Build.VERSION.SDK_INT;
            if (i <= 21 && i == 21) {
                C016107p r2 = this.A01;
                if (r2 == null) {
                    r2 = new C016107p();
                    this.A01 = r2;
                }
                r2.A00 = null;
                r2.A02 = false;
                r2.A01 = null;
                r2.A03 = false;
                ColorStateList A00 = C016207q.A00(imageView);
                if (A00 != null) {
                    r2.A02 = true;
                    r2.A00 = A00;
                }
                PorterDuff.Mode A01 = C016207q.A01(imageView);
                if (A01 != null) {
                    r2.A03 = true;
                    r2.A01 = A01;
                }
                if (r2.A02 || r2.A03) {
                    C012005t.A02(drawable, r2, imageView.getDrawableState());
                    return;
                }
            }
            C016107p r1 = this.A00;
            if (r1 != null) {
                C012005t.A02(drawable, r1, imageView.getDrawableState());
            }
        }
    }

    public void A01(int i) {
        Drawable drawable;
        ImageView imageView = this.A02;
        if (i != 0) {
            drawable = C012005t.A01().A04(imageView.getContext(), i);
            if (drawable != null) {
                C014706y.A02(drawable);
            }
        } else {
            drawable = null;
        }
        imageView.setImageDrawable(drawable);
        A00();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041 A[Catch: all -> 0x0060, TryCatch #0 {all -> 0x0060, blocks: (B:3:0x0018, B:5:0x001f, B:7:0x0026, B:9:0x0034, B:10:0x0037, B:11:0x003a, B:13:0x0041, B:14:0x0048, B:16:0x004f), top: B:22:0x0018 }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004f A[Catch: all -> 0x0060, TRY_LEAVE, TryCatch #0 {all -> 0x0060, blocks: (B:3:0x0018, B:5:0x001f, B:7:0x0026, B:9:0x0034, B:10:0x0037, B:11:0x003a, B:13:0x0041, B:14:0x0048, B:16:0x004f), top: B:22:0x0018 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(android.util.AttributeSet r11, int r12) {
        /*
            r10 = this;
            android.widget.ImageView r7 = r10.A02
            android.content.Context r1 = r7.getContext()
            int[] r8 = X.AnonymousClass07O.A05
            r0 = 0
            r6 = r11
            r9 = r12
            X.06h r2 = X.C013406h.A00(r1, r11, r8, r12, r0)
            android.content.Context r4 = r7.getContext()
            android.content.res.TypedArray r5 = r2.A02
            X.AnonymousClass028.A0L(r4, r5, r6, r7, r8, r9)
            android.graphics.drawable.Drawable r0 = r7.getDrawable()     // Catch: all -> 0x0060
            r4 = -1
            if (r0 != 0) goto L_0x0037
            r0 = 1
            int r3 = r5.getResourceId(r0, r4)     // Catch: all -> 0x0060
            if (r3 == r4) goto L_0x003a
            android.content.Context r1 = r7.getContext()     // Catch: all -> 0x0060
            X.05t r0 = X.C012005t.A01()     // Catch: all -> 0x0060
            android.graphics.drawable.Drawable r0 = r0.A04(r1, r3)     // Catch: all -> 0x0060
            if (r0 == 0) goto L_0x003a
            r7.setImageDrawable(r0)     // Catch: all -> 0x0060
        L_0x0037:
            X.C014706y.A02(r0)     // Catch: all -> 0x0060
        L_0x003a:
            r1 = 2
            boolean r0 = r5.hasValue(r1)     // Catch: all -> 0x0060
            if (r0 == 0) goto L_0x0048
            android.content.res.ColorStateList r0 = r2.A01(r1)     // Catch: all -> 0x0060
            X.C016307r.A00(r0, r7)     // Catch: all -> 0x0060
        L_0x0048:
            r0 = 3
            boolean r0 = r5.hasValue(r0)     // Catch: all -> 0x0060
            if (r0 == 0) goto L_0x005c
            r0 = 3
            int r1 = r5.getInt(r0, r4)     // Catch: all -> 0x0060
            r0 = 0
            android.graphics.PorterDuff$Mode r0 = X.C014706y.A00(r0, r1)     // Catch: all -> 0x0060
            X.C016307r.A01(r0, r7)     // Catch: all -> 0x0060
        L_0x005c:
            r2.A04()
            return
        L_0x0060:
            r0 = move-exception
            r2.A04()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07j.A02(android.util.AttributeSet, int):void");
    }

    public boolean A03() {
        return Build.VERSION.SDK_INT < 21 || !(this.A02.getBackground() instanceof RippleDrawable);
    }
}
