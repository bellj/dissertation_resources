package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.2ND  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ND extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;

    public AnonymousClass2ND(AnonymousClass2L8 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r9) {
        AnonymousClass1V8 A0E = r9.A0E("2fa");
        AnonymousClass009.A05(A0E);
        C450720b r3 = this.A00.A0H;
        boolean z = true;
        boolean z2 = false;
        if (A0E.A0E("code") != null) {
            z2 = true;
        }
        if (A0E.A0E("email") == null) {
            z = false;
        }
        StringBuilder sb = new StringBuilder("xmpp/reader/on-get-two-factor-auth-response code=");
        sb.append(z2);
        sb.append(" email");
        sb.append(z);
        Log.i(sb.toString());
        AbstractC450820c r4 = r3.A00;
        Bundle bundle = new Bundle();
        bundle.putBoolean("code", z2);
        bundle.putBoolean("email", z);
        r4.AYY(Message.obtain(null, 0, 106, 0, bundle));
    }
}
