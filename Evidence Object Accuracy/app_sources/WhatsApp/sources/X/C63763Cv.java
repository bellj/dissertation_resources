package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.mediacomposer.bottombar.recipients.RecipientsView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.3Cv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63763Cv {
    public final Context A00;
    public final AnonymousClass018 A01;
    public final RecipientsView A02;
    public final boolean A03;

    public C63763Cv(AnonymousClass018 r2, RecipientsView recipientsView, boolean z) {
        this.A02 = recipientsView;
        this.A01 = r2;
        this.A00 = recipientsView.getContext();
        this.A03 = z;
    }

    public void A00(C15610nY r6, C32731ce r7, List list, boolean z, boolean z2) {
        RecipientsView recipientsView;
        int i = 0;
        if (this.A03) {
            ArrayList A0H = r6.A0H(this.A00, r7, list);
            CharSequence charSequence = null;
            if (z) {
                charSequence = (CharSequence) A0H.remove(0);
            }
            recipientsView = this.A02;
            recipientsView.setRecipientsChips(A0H, charSequence);
            recipientsView.setRecipientsContentDescription(list.size());
        } else {
            ArrayList A0l = C12960it.A0l();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Jid jid = (Jid) it.next();
                if (!C15380n4.A0N(jid)) {
                    A0l.add(jid);
                }
            }
            Set A12 = C12970iu.A12();
            List A0I = r6.A0I(A12, -1, r6.A0N(A0l, A12), false);
            if (z) {
                Context context = this.A00;
                int i2 = r7.A00;
                int i3 = R.string.status_media_privacy_custom;
                if (i2 == 0) {
                    i3 = R.string.status_media_privacy_contacts;
                }
                A0I.add(0, context.getString(i3));
            }
            recipientsView = this.A02;
            recipientsView.setRecipientsText(C32721cd.A00(this.A01, A0I, true));
        }
        if (z2) {
            if (list.isEmpty()) {
                i = 8;
            }
            recipientsView.setVisibility(i);
        }
    }
}
