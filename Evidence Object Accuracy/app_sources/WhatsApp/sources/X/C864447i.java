package X;

import java.util.List;

/* renamed from: X.47i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C864447i extends AbstractC16350or {
    public final int A00;
    public final AbstractC49382Kn A01;
    public final C235512c A02;

    public C864447i(AbstractC49382Kn r1, C235512c r2, int i) {
        this.A00 = i;
        this.A02 = r2;
        this.A01 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return this.A02.A0D(this.A00);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        List list = (List) obj;
        AnonymousClass009.A05(list);
        this.A01.AWd(list);
    }
}
