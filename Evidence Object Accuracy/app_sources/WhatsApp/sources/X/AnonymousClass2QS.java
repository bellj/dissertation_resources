package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/* renamed from: X.2QS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QS extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new C99564kV();
    public int A00;

    public /* synthetic */ AnonymousClass2QS(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
    }

    public AnonymousClass2QS(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00);
    }
}
