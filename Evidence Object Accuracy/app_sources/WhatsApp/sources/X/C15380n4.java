package X;

import android.content.Intent;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.0n4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15380n4 {
    public static AbstractC14640lm A00(Jid jid) {
        if (jid instanceof DeviceJid) {
            return ((DeviceJid) jid).getUserJid();
        }
        if (jid instanceof AbstractC14640lm) {
            return (AbstractC14640lm) jid;
        }
        return null;
    }

    public static C15580nU A01(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('@');
        sb.append("g.us");
        String obj = sb.toString();
        Jid nullable = Jid.getNullable(obj);
        if (nullable instanceof C15580nU) {
            return (C15580nU) nullable;
        }
        throw new AnonymousClass1MW(obj);
    }

    public static UserJid A02(String str) {
        C29831Uv r1 = C29831Uv.A00;
        return ("".equals(str) || r1.getRawString().equals(str)) ? r1 : UserJid.getNullable(str);
    }

    public static String A03(Jid jid) {
        if (jid == null) {
            return null;
        }
        return jid.getRawString();
    }

    public static String A04(String str) {
        Jid nullable = Jid.getNullable(str);
        return (nullable == null || !Jid.class.isAssignableFrom(nullable.getClass())) ? str : nullable.toString();
    }

    public static String A05(String[] strArr) {
        if (strArr == null) {
            return "null";
        }
        int length = strArr.length - 1;
        if (length == -1) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder("[");
        int i = 0;
        while (true) {
            sb.append(A04(strArr[i]));
            if (i == length) {
                sb.append(']');
                return sb.toString();
            }
            sb.append(", ");
            i++;
        }
    }

    public static ArrayList A06(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        A0D(collection, arrayList);
        return arrayList;
    }

    public static List A07(Class cls, Iterable iterable) {
        ArrayList arrayList = new ArrayList();
        if (iterable != null) {
            Iterator it = iterable.iterator();
            while (it.hasNext()) {
                Jid nullable = Jid.getNullable((String) it.next());
                if (cls.isInstance(nullable)) {
                    arrayList.add(cls.cast(nullable));
                }
            }
        }
        return arrayList;
    }

    public static List A08(String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (str != null) {
                Jid nullable = Jid.getNullable(str);
                if (UserJid.class.isInstance(nullable)) {
                    arrayList.add(UserJid.class.cast(nullable));
                }
            }
        }
        return arrayList;
    }

    public static Set A09(AbstractC15710nm r2, Set set) {
        HashSet hashSet = new HashSet(set.size());
        A0B(r2, set, hashSet);
        return hashSet;
    }

    public static void A0A(Intent intent, Jid jid) {
        intent.putExtra("chat_jid", A03(jid));
    }

    public static void A0B(AbstractC15710nm r4, Iterable iterable, Collection collection) {
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            if (deviceJid == null) {
                r4.AaV("Jids/deviceJidsToUserJids/null-jid", null, true);
            } else {
                collection.add(deviceJid.getUserJid());
            }
        }
    }

    public static void A0C(Class cls, Collection collection, Collection collection2) {
        if (collection != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                Jid nullable = Jid.getNullable((String) it.next());
                if (cls.isInstance(nullable)) {
                    collection2.add(cls.cast(nullable));
                }
            }
        }
    }

    public static void A0D(Iterable iterable, Collection collection) {
        if (iterable != null) {
            Iterator it = iterable.iterator();
            while (it.hasNext()) {
                Jid jid = (Jid) it.next();
                if (jid != null) {
                    collection.add(jid.getRawString());
                }
            }
        }
    }

    public static boolean A0E(Jid jid) {
        int type;
        return jid != null && ((type = jid.getType()) == 0 || type == 1 || type == 18 || type == 3 || type == 7) && !A0M(jid);
    }

    public static boolean A0F(Jid jid) {
        if (jid == null) {
            return false;
        }
        int type = jid.getType();
        return type == 3 || type == 6 || type == 5;
    }

    public static boolean A0G(Jid jid) {
        return jid != null && jid.getType() == 3;
    }

    public static boolean A0H(Jid jid) {
        return jid != null && "broadcast".equals(jid.getServer()) && !A0N(jid);
    }

    public static boolean A0I(Jid jid) {
        if (jid == null) {
            return false;
        }
        int type = jid.getType();
        return type == 10 || type == 0 || type == 17 || type == 20;
    }

    public static boolean A0J(Jid jid) {
        if (jid == null) {
            return false;
        }
        int type = jid.getType();
        return type == 1 || type == 2;
    }

    public static boolean A0K(Jid jid) {
        return jid != null && jid.getType() == 1;
    }

    public static boolean A0L(Jid jid) {
        return jid != null && jid.getType() == 0;
    }

    public static boolean A0M(Jid jid) {
        return jid != null && jid.getType() == 7;
    }

    public static boolean A0N(Jid jid) {
        return jid != null && jid.getType() == 5;
    }

    public static boolean A0O(Jid jid) {
        return jid != null && jid.getType() == 2;
    }

    public static boolean A0P(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (A0N((Jid) it.next())) {
                return true;
            }
        }
        return false;
    }

    public static String[] A0Q(Collection collection) {
        return (String[]) A06(collection).toArray(new String[0]);
    }
}
