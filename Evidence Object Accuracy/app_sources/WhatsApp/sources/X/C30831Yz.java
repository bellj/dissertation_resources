package X;

import android.text.TextUtils;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashSet;

/* renamed from: X.1Yz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30831Yz {
    public static final HashSet A00 = new HashSet(Collections.singletonList("GTQ"));

    public static C30711Yn A00(String str) {
        C30711Yn r0 = C30711Yn.A02;
        if (!TextUtils.isEmpty(str)) {
            return new C30711Yn(str);
        }
        return r0;
    }

    public static String A01(AnonymousClass018 r4, String str, String str2, BigDecimal bigDecimal, int i, boolean z) {
        AnonymousClass1Z0 A01 = A00(str).A01(r4, i, z);
        String A02 = A01.A07.A02(bigDecimal);
        if (A01.A02.A02) {
            boolean z2 = false;
            if (bigDecimal.compareTo(BigDecimal.ZERO) < 0) {
                z2 = true;
            }
            A02 = A01.A01(A02, z2);
        }
        HashSet hashSet = A00;
        if (!hashSet.contains(str)) {
            return A02;
        }
        String replace = A02.replace(" ", "");
        if (!hashSet.contains(str)) {
            str2 = A00(str).A02(r4);
        }
        return replace.replace(str, str2);
    }

    public static String A02(AnonymousClass018 r4, String str, String str2, BigDecimal bigDecimal, boolean z) {
        String A03 = A00(str).A03(r4, bigDecimal, z);
        HashSet hashSet = A00;
        if (!hashSet.contains(str)) {
            return A03;
        }
        String replace = A03.replace(" ", "");
        if (!hashSet.contains(str)) {
            str2 = A00(str).A02(r4);
        }
        return replace.replace(str, str2);
    }

    public static String A03(AnonymousClass018 r1, String str, BigDecimal bigDecimal, boolean z) {
        NumberFormat instance = DecimalFormat.getInstance(AnonymousClass018.A00(r1.A00));
        instance.setMaximumFractionDigits(bigDecimal.scale());
        instance.setMinimumFractionDigits(bigDecimal.scale());
        String format = instance.format(bigDecimal);
        if (!z) {
            return format;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(format);
        sb.append(" ");
        sb.append(str);
        return sb.toString();
    }
}
