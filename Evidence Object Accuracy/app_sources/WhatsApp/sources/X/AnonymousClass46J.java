package X;

import android.view.KeyEvent;
import android.widget.EditText;
import com.whatsapp.R;

/* renamed from: X.46J  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass46J extends AbstractC69143Yc {
    public AnonymousClass46J() {
        super(R.drawable.ic_key_decimal);
    }

    @Override // X.AnonymousClass5W6
    public void AUC(EditText editText) {
        editText.dispatchKeyEvent(new KeyEvent(0, 0, 0, 158, 0));
        editText.dispatchKeyEvent(new KeyEvent(0, 0, 1, 158, 0));
    }
}
