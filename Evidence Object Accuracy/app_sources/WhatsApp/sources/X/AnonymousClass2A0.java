package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

/* renamed from: X.2A0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2A0 {
    public static int A00;
    public static boolean A01;
    public static final Object A02 = new Object();

    public static int A00(Context context) {
        synchronized (A02) {
            if (!A01) {
                A01 = true;
                try {
                    Bundle bundle = C15080mX.A00(context).A00.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
                    if (bundle != null) {
                        bundle.getString("com.google.app.id");
                        A00 = bundle.getInt("com.google.android.gms.version");
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    Log.wtf("MetadataValueReader", "This should never happen.", e);
                }
            }
        }
        return A00;
    }
}
