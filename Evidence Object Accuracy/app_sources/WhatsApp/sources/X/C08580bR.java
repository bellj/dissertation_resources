package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.whatsapp.R;

/* renamed from: X.0bR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08580bR implements AbstractC12210hY {
    public final /* synthetic */ int A00 = R.drawable.ic_map_pin;

    @Override // X.AbstractC12210hY
    public Bitmap A89() {
        return BitmapFactory.decodeResource(AnonymousClass03Q.A02.getResources(), this.A00);
    }
}
