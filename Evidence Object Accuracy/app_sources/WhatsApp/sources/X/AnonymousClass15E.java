package X;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/* renamed from: X.15E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15E {
    public final C15900o5 A00;
    public final C16590pI A01;
    public final C14820m6 A02;
    public final C21780xy A03;

    public AnonymousClass15E(C15900o5 r1, C16590pI r2, C14820m6 r3, C21780xy r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
    }

    public static final Uri A00(String str, String str2, String str3) {
        Uri.Builder appendPath = new Uri.Builder().scheme("content").authority("com.whatsapp.provider.MigrationContentProvider").appendPath(str);
        if (str2 != null) {
            appendPath.appendQueryParameter("query_param_country_code", str2);
        }
        if (str3 != null) {
            appendPath.appendQueryParameter("query_param_phone_number", str3);
        }
        return appendPath.build();
    }

    public static final byte[] A01(Bundle bundle, String str, byte[] bArr) {
        byte[] byteArray = bundle.getByteArray(str);
        if (byteArray == null) {
            Log.e("MigrateFileDirectlyHelper/getEncryptedData erk is null");
            return null;
        }
        try {
            PrivateKey generatePrivate = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(bArr));
            Cipher instance = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            instance.init(2, generatePrivate);
            return instance.doFinal(byteArray);
        } catch (InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException unused) {
            Log.e("MigrateFileDirectlyHelper/getEncryptedData failed to decrypt erk");
            return null;
        }
    }

    public final int A02(File file, FileDescriptor fileDescriptor) {
        Log.i("MigrateFileDirectlyHelper/replaceFile");
        try {
            C32871cs r3 = new C32871cs(this.A03.A00, file);
            FileInputStream fileInputStream = new FileInputStream(fileDescriptor);
            try {
                C14350lI.A0H(fileInputStream.getChannel(), Channels.newChannel(r3));
                r3.close();
                fileInputStream.close();
                r3.close();
                return 19;
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("MigrateFileDirectlyHelper/replaceFile/error while moving file. File to replace is ");
            sb.append(file.toString());
            sb.append(" error message is: ");
            sb.append(e);
            Log.e(sb.toString());
            return (e.getMessage() == null || !e.getMessage().contains("No space")) ? 15 : 5;
        }
    }

    public int A03(File file, String str) {
        try {
            ContentResolver contentResolver = this.A01.A00.getContentResolver();
            C14820m6 r0 = this.A02;
            ParcelFileDescriptor openFileDescriptor = contentResolver.openFileDescriptor(A00(str, r0.A0B(), r0.A0C()), "r");
            if (openFileDescriptor != null) {
                try {
                    if (openFileDescriptor.getFileDescriptor() != null) {
                        int A02 = A02(file, openFileDescriptor.getFileDescriptor());
                        openFileDescriptor.close();
                        return A02;
                    }
                } catch (Throwable th) {
                    if (openFileDescriptor != null) {
                        try {
                            openFileDescriptor.close();
                        } catch (Throwable unused) {
                        }
                    }
                    throw th;
                }
            }
            Log.i("MigrateFileDirectlyHelper/migrateFile/consumer file is null");
            if (openFileDescriptor == null) {
                return 15;
            }
            openFileDescriptor.close();
            return 15;
        } catch (IOException | SecurityException e) {
            StringBuilder sb = new StringBuilder("MigrateFileDirectlyHelper/migrateFile/error while fetching internal file: ");
            sb.append(str);
            sb.append(" from consumer app. error message is: ");
            sb.append(e);
            Log.e(sb.toString());
            return 15;
        }
    }

    public boolean A04() {
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
            instance.initialize(2024);
            KeyPair generateKeyPair = instance.generateKeyPair();
            Bundle bundle = new Bundle();
            C14820m6 r7 = this.A02;
            bundle.putString("query_param_country_code", r7.A0B());
            bundle.putString("query_param_phone_number", r7.A0C());
            bundle.putByteArray("pk", generateKeyPair.getPublic().getEncoded());
            Bundle call = this.A01.A00.getContentResolver().call(A00("", r7.A0B(), r7.A0C()), "retrieve_rk", (String) null, bundle);
            if (call == null) {
                Log.e("MigrateFileDirectlyHelper/migrateBackupEncryptionKey null returned");
                return false;
            }
            byte[] encoded = generateKeyPair.getPrivate().getEncoded();
            byte[] A01 = A01(call, "erk", encoded);
            if (A01 == null) {
                Log.e("MigrateFileDirectlyHelper/migrateBackupEncryptionKey root key is null");
                return false;
            }
            C15900o5 r5 = this.A00;
            r5.A02(A01);
            byte[] A012 = A01(call, "ph", encoded);
            byte[] A013 = A01(call, "ps", encoded);
            int i = call.getInt("ic", 100000);
            r7.A14(true);
            if (A012 == null || A013 == null) {
                r7.A15(true);
                return true;
            }
            r5.A01(new C16560pF(A012, A013, i));
            r7.A15(false);
            return true;
        } catch (IOException | NoSuchAlgorithmException e) {
            Log.e("MigrateFileDirectlyHelper/migrateBackupEncryptionKey failed", e);
            return false;
        }
    }
}
