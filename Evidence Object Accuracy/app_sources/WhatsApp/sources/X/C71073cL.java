package X;

import com.whatsapp.companiondevice.LinkedDevicesViewModel;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3cL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71073cL implements AnonymousClass1UU {
    public final /* synthetic */ LinkedDevicesViewModel A00;

    @Override // X.AnonymousClass1UU
    public void AYT(int i) {
    }

    public C71073cL(LinkedDevicesViewModel linkedDevicesViewModel) {
        this.A00 = linkedDevicesViewModel;
    }

    @Override // X.AnonymousClass1UU
    public void AYU() {
        LinkedDevicesViewModel linkedDevicesViewModel = this.A00;
        List A04 = linkedDevicesViewModel.A0B.A04();
        Collections.sort(A04, linkedDevicesViewModel.A0C);
        linkedDevicesViewModel.A08.A0A(A04);
    }
}
