package X;

import com.whatsapp.authentication.FingerprintBottomSheet;

/* renamed from: X.5dx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119425dx extends AbstractC58672rA {
    public final ActivityC13810kN A00;
    public final FingerprintBottomSheet A01;
    public final C14830m7 A02;
    public final AnonymousClass607 A03;
    public final C130015yf A04;
    public final C133326Ai A05;

    public C119425dx(ActivityC13810kN r1, FingerprintBottomSheet fingerprintBottomSheet, C14830m7 r3, AnonymousClass607 r4, C130015yf r5, C133326Ai r6) {
        this.A02 = r3;
        this.A04 = r5;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = fingerprintBottomSheet;
        this.A05 = r6;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        FingerprintBottomSheet fingerprintBottomSheet = this.A01;
        fingerprintBottomSheet.A1G(true);
        fingerprintBottomSheet.A1C();
    }

    @Override // X.AbstractC58672rA
    public void A02() {
        this.A05.A00();
    }

    @Override // X.AbstractC58672rA
    public void A04(AnonymousClass02N r7, AnonymousClass21K r8) {
        long A00 = this.A04.A00() * 1000;
        if (A00 > this.A02.A00()) {
            this.A01.A1L(A00);
            return;
        }
        AnonymousClass607 r4 = this.A03;
        C1323666p r3 = new C1323666p(r8, this);
        r4.A0Q.Ab2(new AnonymousClass6FU(r4));
        AnonymousClass60T r1 = r4.A0H;
        String str = r4.A0U;
        AnonymousClass6B7 A0C = C117315Zl.A0C(r1, str, "PIN");
        if (A0C != null) {
            r4.A01(r7, r3, new C128545wH(A0C));
            return;
        }
        r3.A01.A05.A04.A0N.A02(185478830, "br_get_provider_key_tag");
        r4.A0G.A00(new C133436At(r7, r4, r3), str);
    }
}
