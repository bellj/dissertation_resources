package X;

import java.util.List;

/* renamed from: X.4ZH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4ZH {
    public static final AnonymousClass00E A00 = new AnonymousClass00E(1, 1);

    public static boolean A00(C14850m9 r3, List list) {
        return r3.A07(815) && r3.A07(1267) && C15380n4.A0P(list);
    }
}
