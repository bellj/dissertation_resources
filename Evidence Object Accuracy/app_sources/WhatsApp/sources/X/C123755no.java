package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.5no  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123755no extends AbstractC130195yx {
    public int A00 = 0;
    public C120995h5 A01;
    public String A02;
    public final Context A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final C14830m7 A06;

    public C123755no(Context context, C15550nR r3, C15610nY r4, C14830m7 r5, AnonymousClass018 r6, AnonymousClass14X r7) {
        super(context, r6, r7, 1);
        this.A06 = r5;
        this.A03 = context;
        this.A04 = r3;
        this.A05 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        if (r1 == 20) goto L_0x0012;
     */
    @Override // X.AbstractC130195yx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.CharSequence A03() {
        /*
            r6 = this;
            X.1IR r2 = r6.A01
            int r1 = r2.A03
            r0 = 2
            if (r1 == r0) goto L_0x0012
            r0 = 200(0xc8, float:2.8E-43)
            if (r1 == r0) goto L_0x0012
            r0 = 20
            r5 = 2131890449(0x7f121111, float:1.941559E38)
            if (r1 != r0) goto L_0x0015
        L_0x0012:
            r5 = 2131890448(0x7f121110, float:1.9415588E38)
        L_0x0015:
            X.1Yv r4 = r2.A00()
            android.content.Context r3 = r6.A03
            java.lang.Object[] r2 = X.C12970iu.A1b()
            r1 = 0
            java.lang.CharSequence r0 = super.A03()
            java.lang.String r0 = X.C12960it.A0X(r3, r0, r2, r1, r5)
            java.lang.CharSequence r0 = r4.AA7(r3, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123755no.A03():java.lang.CharSequence");
    }

    @Override // X.AbstractC130195yx
    public String A04() {
        Context context;
        int i;
        int i2 = this.A00;
        if (i2 == 5 || i2 == 6) {
            context = this.A03;
            i = R.string.novi_payments_transaction_status_in_review;
        } else if (i2 == 7) {
            context = this.A03;
            i = R.string.novi_payments_transaction_status_claim_approved;
        } else if (i2 == 8) {
            context = this.A03;
            i = R.string.novi_payments_transaction_status_claim_declined;
        } else if (i2 != 13) {
            return super.A04();
        } else {
            context = this.A03;
            i = R.string.payments_transaction_status_processing;
        }
        return context.getString(i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00b7  */
    @Override // X.AbstractC130195yx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.AnonymousClass1IR r6) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123755no.A06(X.1IR):void");
    }

    public final boolean A08() {
        int i = super.A01.A03;
        return i == 1 || i == 100 || i == 10;
    }
}
