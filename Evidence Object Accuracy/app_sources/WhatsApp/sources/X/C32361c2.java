package X;

/* renamed from: X.1c2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32361c2 {
    public byte[] A00;
    public final int A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final byte[] A0C;

    public C32361c2(String str, String str2, String str3, String str4, String str5, String str6, String str7, byte[] bArr, byte[] bArr2, int i, boolean z, boolean z2, boolean z3) {
        this.A08 = str;
        this.A02 = str2;
        this.A01 = i;
        this.A07 = str3;
        this.A03 = str4;
        this.A0C = bArr;
        this.A00 = bArr2;
        this.A05 = str5;
        this.A04 = str6;
        this.A06 = str7;
        this.A0A = z;
        this.A0B = z2;
        this.A09 = z3;
    }
}
