package X;

import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.3Ci  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63633Ci {
    public int A00;
    public int A01;
    public final /* synthetic */ AnonymousClass3EZ A02;

    public C63633Ci(AnonymousClass3EZ r1) {
        this.A02 = r1;
    }

    public void A00(UserJid userJid, Integer num, int i) {
        if (i == 1) {
            this.A01 = 2;
            this.A00 = R.string.community_admin_promotion_failed;
        } else if (i != 2) {
            Log.e(C12960it.A0W(i, "onCompleteWithError/Unsupported operation: "));
            return;
        } else {
            this.A01 = 4;
            AnonymousClass3EZ r2 = this.A02;
            boolean A0F = r2.A02.A0F(userJid);
            int i2 = R.string.community_admin_dismiss_failed;
            if (A0F) {
                i2 = R.string.community_admin_dismiss_self_failed;
            }
            this.A00 = i2;
            if (A0F && num != null) {
                int intValue = num.intValue();
                if (intValue == 3) {
                    r2.A04.A02(false);
                } else if (intValue == 4) {
                    AnonymousClass3F3 r22 = r2.A04;
                    AnonymousClass30L r1 = new AnonymousClass30L();
                    r1.A01 = Integer.valueOf(r22.A00);
                    r1.A02 = C12980iv.A0k();
                    r1.A00 = Boolean.FALSE;
                    r22.A01.A07(r1);
                }
            }
        }
        int i3 = this.A01;
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialog_id", i3);
        AnonymousClass3EZ r23 = this.A02;
        ActivityC13790kL r3 = r23.A03;
        A0D.putCharSequence("message", r3.getString(this.A00));
        A0D.putString("user_jid", userJid.getRawString());
        AnonymousClass4KE r24 = r23.A01;
        A0D.putString("positive_button", r3.getString(R.string.community_admin_promotion_failed_try_again));
        A0D.putString("negative_button", r3.getString(R.string.cancel));
        ActivityC13810kN.A0y(A0D, r3, r24);
    }
}
