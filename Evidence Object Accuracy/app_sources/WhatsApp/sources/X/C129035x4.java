package X;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/* renamed from: X.5x4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129035x4 {
    public final AnonymousClass178 A00;
    public final C127535ue A01;
    public final Map A02 = new AnonymousClass00N();
    public final Map A03 = new AnonymousClass00N();

    public C129035x4(AnonymousClass178 r6, C127535ue r7, Map map, Map map2) {
        this.A00 = r6;
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            Object key = A15.getKey();
            Object value = A15.getValue();
            AnonymousClass009.A05(key);
            for (Object obj : (Set) key) {
                this.A02.put(obj, value);
            }
        }
        Iterator A0n2 = C12960it.A0n(map2);
        while (A0n2.hasNext()) {
            Map.Entry A152 = C12970iu.A15(A0n2);
            Object key2 = A152.getKey();
            Object value2 = A152.getValue();
            AnonymousClass009.A05(key2);
            for (Object obj2 : (Set) key2) {
                this.A03.put(obj2, value2);
            }
        }
        this.A01 = r7;
    }

    public final C127535ue A00(String str) {
        Object value;
        Map map = this.A02;
        if (map.containsKey(str)) {
            value = map.get(str);
        } else {
            Iterator A0n = C12960it.A0n(this.A03);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                if (((Pattern) A15.getKey()).matcher(str).matches()) {
                    value = A15.getValue();
                }
            }
            return this.A01;
        }
        return (C127535ue) value;
    }
}
