package X;

import java.security.BasicPermission;
import java.security.Permission;
import java.util.StringTokenizer;

/* renamed from: X.1T6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1T6 extends BasicPermission {
    public final String actions;
    public final int permissionMask;

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof AnonymousClass1T6)) {
                return false;
            }
            AnonymousClass1T6 r4 = (AnonymousClass1T6) obj;
            if (this.permissionMask != r4.permissionMask || !getName().equals(r4.getName())) {
                return false;
            }
        }
        return true;
    }

    @Override // java.security.BasicPermission, java.security.Permission
    public String getActions() {
        return this.actions;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return getName().hashCode() + this.permissionMask;
    }

    @Override // java.security.BasicPermission, java.security.Permission
    public boolean implies(Permission permission) {
        if (!(permission instanceof AnonymousClass1T6) || !getName().equals(permission.getName())) {
            return false;
        }
        int i = this.permissionMask;
        int i2 = ((AnonymousClass1T6) permission).permissionMask;
        return (i & i2) == i2;
    }

    public AnonymousClass1T6(String str) {
        super("SC", str);
        this.actions = str;
        StringTokenizer stringTokenizer = new StringTokenizer(AnonymousClass1T7.A00(str), " ,");
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            if (nextToken.equals("threadlocalecimplicitlyca")) {
                i |= 1;
            } else if (nextToken.equals("ecimplicitlyca")) {
                i |= 2;
            } else if (nextToken.equals("threadlocaldhdefaultparams")) {
                i |= 4;
            } else if (nextToken.equals("dhdefaultparams")) {
                i |= 8;
            } else if (nextToken.equals("acceptableeccurves")) {
                i |= 16;
            } else if (nextToken.equals("additionalecparameters")) {
                i |= 32;
            } else if (nextToken.equals("all")) {
                i |= 63;
            }
        }
        if (i != 0) {
            this.permissionMask = i;
            return;
        }
        throw new IllegalArgumentException("unknown permissions passed to mask");
    }
}
