package X;

import com.whatsapp.jobqueue.job.SendStatusPrivacyListJob;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.46h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C862246h extends AbstractC35941j2 {
    public final /* synthetic */ SendStatusPrivacyListJob A00;
    public final /* synthetic */ AtomicInteger A01;

    public C862246h(SendStatusPrivacyListJob sendStatusPrivacyListJob, AtomicInteger atomicInteger) {
        this.A00 = sendStatusPrivacyListJob;
        this.A01 = atomicInteger;
    }

    @Override // X.AbstractC35941j2
    public void A00(int i) {
        this.A01.set(i);
        SendStatusPrivacyListJob sendStatusPrivacyListJob = this.A00;
        String str = sendStatusPrivacyListJob.webId;
        if (str != null) {
            sendStatusPrivacyListJob.A01.A0H(str, i);
        }
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r4) {
        SendStatusPrivacyListJob sendStatusPrivacyListJob = this.A00;
        String str = sendStatusPrivacyListJob.webId;
        if (str != null) {
            sendStatusPrivacyListJob.A01.A0H(str, 200);
        }
    }
}
