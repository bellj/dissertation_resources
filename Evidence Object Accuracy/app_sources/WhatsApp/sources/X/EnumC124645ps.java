package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5ps  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124645ps extends Enum implements AnonymousClass5UX {
    public static final /* synthetic */ EnumC124645ps[] A00;
    public static final EnumC124645ps A01;
    public static final EnumC124645ps A02;
    public static final EnumC124645ps A03;
    public static final EnumC124645ps A04;
    public static final EnumC124645ps A05;
    public static final EnumC124645ps A06;
    public final String fieldName;

    public static EnumC124645ps valueOf(String str) {
        return (EnumC124645ps) Enum.valueOf(EnumC124645ps.class, str);
    }

    public static EnumC124645ps[] values() {
        return (EnumC124645ps[]) A00.clone();
    }

    static {
        EnumC124645ps r9 = new EnumC124645ps("PRIMARY_PAYMENT_METHOD", "primary_payment_method", 0);
        A06 = r9;
        EnumC124645ps r8 = new EnumC124645ps("PAYMENT_METHOD", "payment_method", 1);
        A05 = r8;
        EnumC124645ps r7 = new EnumC124645ps("ALL_PAYMENT_METHODS", "all_payment_methods", 2);
        A01 = r7;
        EnumC124645ps r6 = new EnumC124645ps("CONTACT", "contact", 3);
        A02 = r6;
        EnumC124645ps r5 = new EnumC124645ps("ORDER", "order", 4);
        A04 = r5;
        EnumC124645ps r4 = new EnumC124645ps("DEVICE", "device", 5);
        A03 = r4;
        EnumC124645ps r1 = new EnumC124645ps("CURRENCY_AMOUNT", "currency_amount", 6);
        EnumC124645ps[] r0 = new EnumC124645ps[7];
        C72453ed.A1F(r9, r8, r7, r6, r0);
        C117305Zk.A1M(r5, r4, r0);
        r0[6] = r1;
        A00 = r0;
    }

    public EnumC124645ps(String str, String str2, int i) {
        this.fieldName = str2;
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
