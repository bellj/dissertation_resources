package X;

/* renamed from: X.0OU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OU {
    public AnonymousClass0PE A00;
    public AnonymousClass0J9 A01;
    public C08900c2 A02;

    public AnonymousClass0OU(AnonymousClass0PE r1, AnonymousClass0J9 r2, C08900c2 r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(this.A00));
        sb.append(" {...} (src=");
        sb.append(this.A01);
        sb.append(")");
        return sb.toString();
    }
}
