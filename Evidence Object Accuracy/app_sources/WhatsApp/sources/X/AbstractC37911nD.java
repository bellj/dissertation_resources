package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Mac;

/* renamed from: X.1nD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC37911nD extends FilterInputStream {
    public int A00 = 0;
    public Mac A01;
    public boolean A02 = false;
    public final int A03 = 10;
    public final C37921nE A04;
    public final C91314Rg A05 = new C91314Rg();
    public final C37891nB A06;
    public final List A07;
    public final byte[] A08;

    public AbstractC37911nD(C37891nB r3, InputStream inputStream) {
        super(inputStream);
        this.A06 = r3;
        this.A07 = new ArrayList();
        this.A01 = AbstractC65063Hz.A02(r3.A01, r3.A02);
        this.A08 = new byte[DefaultCrypto.BUFFER_SIZE];
        this.A04 = new C37921nE(this);
    }

    public final byte[] A00(byte[] bArr, int i) {
        byte[] bArr2 = new byte[16];
        if (i < 16) {
            C91314Rg r5 = this.A05;
            int i2 = 16 - i;
            boolean z = false;
            if (i2 <= r5.A00) {
                z = true;
            }
            AnonymousClass009.A0F(z);
            byte[] bArr3 = new byte[i2];
            for (int i3 = 0; i3 < i2; i3++) {
                int i4 = (r5.A02 - i2) + i3;
                if (i4 < 0) {
                    i4 += r5.A01;
                }
                bArr3[i3] = r5.A03[i4];
            }
            System.arraycopy(bArr3, 0, bArr2, 0, i2);
            System.arraycopy(bArr, 0, bArr2, i2, i);
            return bArr2;
        }
        System.arraycopy(bArr, i - 16, bArr2, 0, 16);
        return bArr2;
    }

    @Override // java.io.FilterInputStream, java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() {
        super.close();
        if (this.A00 > 0) {
            byte[] doFinal = this.A01.doFinal();
            int i = this.A03;
            byte[] bArr = new byte[i];
            System.arraycopy(doFinal, 0, bArr, 0, i);
            this.A07.add(bArr);
        }
        this.A02 = true;
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() {
        int read;
        byte[] bArr = new byte[1];
        do {
            read = read(bArr, 0, 1);
            if (read == -1) {
                return -1;
            }
        } while (read == 0);
        return bArr[0];
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0017, code lost:
        if (r0.A00 == (r0.A01.size() - 1)) goto L_0x0019;
     */
    @Override // java.io.FilterInputStream, java.io.InputStream
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read(byte[] r13, int r14, int r15) {
        /*
        // Method dump skipped, instructions count: 319
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC37911nD.read(byte[], int, int):int");
    }

    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) {
        byte[] bArr = this.A08;
        return (long) read(bArr, 0, (int) Math.min((long) bArr.length, j));
    }
}
