package X;

import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape0S1630000_I1;
import com.facebook.redex.ViewOnClickCListenerShape2S0200000_I1;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.InfoCard;
import com.whatsapp.R;
import com.whatsapp.biz.BusinessHoursView;
import com.whatsapp.biz.BusinessProfileFieldView;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.WaMapView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/* renamed from: X.3IU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IU {
    public boolean A00;
    public boolean A01;
    public final View A02;
    public final AnonymousClass12P A03;
    public final ActivityC13810kN A04;
    public final InfoCard A05;
    public final C15570nT A06;
    public final BusinessHoursView A07;
    public final BusinessProfileFieldView A08;
    public final BusinessProfileFieldView A09;
    public final C246716k A0A;
    public final C25781Au A0B;
    public final AnonymousClass2VB A0C;
    public final C15610nY A0D;
    public final AnonymousClass018 A0E;
    public final C15370n3 A0F;
    public final C244415n A0G;
    public final Integer A0H;
    public final List A0I;
    public final List A0J;
    public final boolean A0K;

    public AnonymousClass3IU(View view, AnonymousClass12P r5, ActivityC13810kN r6, C15570nT r7, C246716k r8, C25781Au r9, AnonymousClass2VB r10, C15610nY r11, AnonymousClass018 r12, C15370n3 r13, C244415n r14, Integer num, boolean z, boolean z2) {
        ArrayList A0l = C12960it.A0l();
        this.A0J = A0l;
        ArrayList A0l2 = C12960it.A0l();
        this.A0I = A0l2;
        this.A06 = r7;
        this.A03 = r5;
        this.A0G = r14;
        this.A0D = r11;
        this.A0E = r12;
        this.A0A = r8;
        this.A0B = r9;
        this.A02 = view;
        this.A0C = r10;
        this.A0H = num;
        this.A08 = (BusinessProfileFieldView) view.findViewById(R.id.business_location);
        this.A09 = (BusinessProfileFieldView) view.findViewById(R.id.business_email);
        A0l.add(view.findViewById(R.id.business_link));
        A0l.add(view.findViewById(R.id.business_link_2));
        InfoCard infoCard = null;
        if (z) {
            A0l2.add(view.findViewById(R.id.brand_link));
            A0l2.add(view.findViewById(R.id.brand_link_2));
            infoCard = (InfoCard) view.findViewById(R.id.brand_link_card);
        }
        this.A05 = infoCard;
        this.A07 = (BusinessHoursView) view.findViewById(R.id.business_hours);
        this.A04 = r6;
        this.A0F = r13;
        this.A0K = z;
        this.A01 = z2;
    }

    public static void A00(AnonymousClass12P r18, BusinessProfileFieldView businessProfileFieldView, C25781Au r20, AnonymousClass2VB r21, Integer num, String str, int i, boolean z, boolean z2, boolean z3) {
        boolean z4;
        String A0g;
        if (businessProfileFieldView.A06 != null) {
            businessProfileFieldView.A06.setTextColor(AnonymousClass00T.A00(businessProfileFieldView.getContext(), R.color.business_profile_link));
            if (i == 0) {
                String text = businessProfileFieldView.getText();
                if (!TextUtils.isEmpty(text)) {
                    String A00 = C88004Dw.A00(text);
                    if (!(businessProfileFieldView.getText() == null || businessProfileFieldView.A06 == null || businessProfileFieldView.A05 == null)) {
                        boolean A1S = C12960it.A1S(A01(businessProfileFieldView.getText()) ? 1 : 0);
                        Uri parse = Uri.parse(C88004Dw.A00(businessProfileFieldView.getText()));
                        if (!(!A1S || parse == null || parse.getPathSegments().size() == 0)) {
                            int i2 = R.drawable.ic_business_instagram;
                            if (!A1S) {
                                i2 = R.drawable.ic_business_link;
                            }
                            businessProfileFieldView.setIcon(i2);
                            if (A1S) {
                                businessProfileFieldView.setText(C12990iw.A0q(businessProfileFieldView, R.string.business_details_subtitle_instagram), null);
                            } else if (!A1S) {
                                A0g = "";
                                businessProfileFieldView.setSubText(A0g);
                                int A002 = AnonymousClass00T.A00(businessProfileFieldView.getContext(), R.color.body_gray);
                                int A003 = AnonymousClass00T.A00(businessProfileFieldView.getContext(), R.color.list_item_info);
                                businessProfileFieldView.A06.setTextColor(A002);
                                businessProfileFieldView.A05.setTextColor(A003);
                                z4 = true;
                                businessProfileFieldView.setOnClickListener(new View.OnClickListener(Uri.parse(C12960it.A0d(Uri.encode(A00), C12960it.A0k("https://l.wl.co/l?u="))), r18, businessProfileFieldView, r20, r21, num, str, z4, z2, z3, z) { // from class: X.2jx
                                    public final /* synthetic */ Uri A00;
                                    public final /* synthetic */ AnonymousClass12P A01;
                                    public final /* synthetic */ BusinessProfileFieldView A02;
                                    public final /* synthetic */ C25781Au A03;
                                    public final /* synthetic */ AnonymousClass2VB A04;
                                    public final /* synthetic */ Integer A05;
                                    public final /* synthetic */ String A06;
                                    public final /* synthetic */ boolean A07;
                                    public final /* synthetic */ boolean A08;
                                    public final /* synthetic */ boolean A09;
                                    public final /* synthetic */ boolean A0A;

                                    {
                                        this.A03 = r4;
                                        this.A06 = r7;
                                        this.A07 = r8;
                                        this.A05 = r6;
                                        this.A08 = r9;
                                        this.A09 = r10;
                                        this.A0A = r11;
                                        this.A04 = r5;
                                        this.A01 = r2;
                                        this.A02 = r3;
                                        this.A00 = r1;
                                    }

                                    @Override // android.view.View.OnClickListener
                                    public final void onClick(View view) {
                                        C25781Au r6 = this.A03;
                                        String str2 = this.A06;
                                        boolean z5 = this.A07;
                                        Integer num2 = this.A05;
                                        boolean z6 = this.A08;
                                        boolean z7 = this.A09;
                                        boolean z8 = this.A0A;
                                        AnonymousClass2VB r4 = this.A04;
                                        AnonymousClass12P r3 = this.A01;
                                        BusinessProfileFieldView businessProfileFieldView2 = this.A02;
                                        Uri uri = this.A00;
                                        r6.A05(Integer.valueOf(C12980iv.A03(z5 ? 1 : 0)), num2, str2, 2, z6, z7);
                                        if (z8) {
                                            r6.A01(r4, 10);
                                        }
                                        r3.A06(businessProfileFieldView2.getContext(), C12970iu.A0B(uri));
                                    }
                                });
                            }
                            A0g = C12960it.A0g(parse.getPathSegments(), 0);
                            businessProfileFieldView.setSubText(A0g);
                            int A002 = AnonymousClass00T.A00(businessProfileFieldView.getContext(), R.color.body_gray);
                            int A003 = AnonymousClass00T.A00(businessProfileFieldView.getContext(), R.color.list_item_info);
                            businessProfileFieldView.A06.setTextColor(A002);
                            businessProfileFieldView.A05.setTextColor(A003);
                            z4 = true;
                            businessProfileFieldView.setOnClickListener(new View.OnClickListener(Uri.parse(C12960it.A0d(Uri.encode(A00), C12960it.A0k("https://l.wl.co/l?u="))), r18, businessProfileFieldView, r20, r21, num, str, z4, z2, z3, z) { // from class: X.2jx
                                public final /* synthetic */ Uri A00;
                                public final /* synthetic */ AnonymousClass12P A01;
                                public final /* synthetic */ BusinessProfileFieldView A02;
                                public final /* synthetic */ C25781Au A03;
                                public final /* synthetic */ AnonymousClass2VB A04;
                                public final /* synthetic */ Integer A05;
                                public final /* synthetic */ String A06;
                                public final /* synthetic */ boolean A07;
                                public final /* synthetic */ boolean A08;
                                public final /* synthetic */ boolean A09;
                                public final /* synthetic */ boolean A0A;

                                {
                                    this.A03 = r4;
                                    this.A06 = r7;
                                    this.A07 = r8;
                                    this.A05 = r6;
                                    this.A08 = r9;
                                    this.A09 = r10;
                                    this.A0A = r11;
                                    this.A04 = r5;
                                    this.A01 = r2;
                                    this.A02 = r3;
                                    this.A00 = r1;
                                }

                                @Override // android.view.View.OnClickListener
                                public final void onClick(View view) {
                                    C25781Au r6 = this.A03;
                                    String str2 = this.A06;
                                    boolean z5 = this.A07;
                                    Integer num2 = this.A05;
                                    boolean z6 = this.A08;
                                    boolean z7 = this.A09;
                                    boolean z8 = this.A0A;
                                    AnonymousClass2VB r4 = this.A04;
                                    AnonymousClass12P r3 = this.A01;
                                    BusinessProfileFieldView businessProfileFieldView2 = this.A02;
                                    Uri uri = this.A00;
                                    r6.A05(Integer.valueOf(C12980iv.A03(z5 ? 1 : 0)), num2, str2, 2, z6, z7);
                                    if (z8) {
                                        r6.A01(r4, 10);
                                    }
                                    r3.A06(businessProfileFieldView2.getContext(), C12970iu.A0B(uri));
                                }
                            });
                        }
                    }
                    z4 = false;
                    businessProfileFieldView.setOnClickListener(new View.OnClickListener(Uri.parse(C12960it.A0d(Uri.encode(A00), C12960it.A0k("https://l.wl.co/l?u="))), r18, businessProfileFieldView, r20, r21, num, str, z4, z2, z3, z) { // from class: X.2jx
                        public final /* synthetic */ Uri A00;
                        public final /* synthetic */ AnonymousClass12P A01;
                        public final /* synthetic */ BusinessProfileFieldView A02;
                        public final /* synthetic */ C25781Au A03;
                        public final /* synthetic */ AnonymousClass2VB A04;
                        public final /* synthetic */ Integer A05;
                        public final /* synthetic */ String A06;
                        public final /* synthetic */ boolean A07;
                        public final /* synthetic */ boolean A08;
                        public final /* synthetic */ boolean A09;
                        public final /* synthetic */ boolean A0A;

                        {
                            this.A03 = r4;
                            this.A06 = r7;
                            this.A07 = r8;
                            this.A05 = r6;
                            this.A08 = r9;
                            this.A09 = r10;
                            this.A0A = r11;
                            this.A04 = r5;
                            this.A01 = r2;
                            this.A02 = r3;
                            this.A00 = r1;
                        }

                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            C25781Au r6 = this.A03;
                            String str2 = this.A06;
                            boolean z5 = this.A07;
                            Integer num2 = this.A05;
                            boolean z6 = this.A08;
                            boolean z7 = this.A09;
                            boolean z8 = this.A0A;
                            AnonymousClass2VB r4 = this.A04;
                            AnonymousClass12P r3 = this.A01;
                            BusinessProfileFieldView businessProfileFieldView2 = this.A02;
                            Uri uri = this.A00;
                            r6.A05(Integer.valueOf(C12980iv.A03(z5 ? 1 : 0)), num2, str2, 2, z6, z7);
                            if (z8) {
                                r6.A01(r4, 10);
                            }
                            r3.A06(businessProfileFieldView2.getContext(), C12970iu.A0B(uri));
                        }
                    });
                }
            } else if (i == 1) {
                String text2 = businessProfileFieldView.getText();
                if (!TextUtils.isEmpty(text2)) {
                    businessProfileFieldView.setOnClickListener(new ViewOnClickCListenerShape0S1630000_I1(r20, num, r21, r18, businessProfileFieldView, Uri.parse(C12960it.A0d(text2, C12960it.A0k("mailto:"))), str, 1, z2, z3, z));
                }
            } else if (i == 2) {
                String text3 = businessProfileFieldView.getText();
                if (!TextUtils.isEmpty(text3)) {
                    businessProfileFieldView.setOnClickListener(new ViewOnClickCListenerShape0S1630000_I1(r20, num, r21, r18, businessProfileFieldView, C12970iu.A0B(Uri.parse(C12960it.A0d(Uri.encode(text3), C12960it.A0k("geo:0,0?q=")))), str, 0, z2, z3, z));
                }
            }
        }
    }

    public static boolean A01(String str) {
        Uri parse = Uri.parse(C88004Dw.A00(str));
        return parse.getHost().equalsIgnoreCase("www.instagram.com") || parse.getHost().equalsIgnoreCase("instagram.com") || parse.getHost().equalsIgnoreCase("instagr.am") || parse.getHost().equalsIgnoreCase("www.instagr.am");
    }

    public void A02(C30141Wg r26) {
        UserJid A05;
        C30231Wp r4;
        boolean z;
        C30181Wk r1;
        int[] iArr;
        int length;
        int i;
        String join;
        String str;
        InfoCard infoCard;
        String str2;
        Double d;
        int dimension;
        int i2;
        int i3;
        Object[] objArr;
        C30171Wj r42 = r26.A03;
        String str3 = r42.A03;
        if ((this.A0A.A00() & 8) > 0) {
            ActivityC13810kN r11 = this.A04;
            String str4 = r42.A00.A03;
            String str5 = r42.A02;
            String str6 = "";
            if (!TextUtils.isEmpty(str3)) {
                i3 = R.string.edit_business_address_full_address;
                objArr = new Object[3];
                objArr[0] = str3;
                if (TextUtils.isEmpty(str4)) {
                    str4 = str6;
                }
                objArr[1] = str4;
                if (TextUtils.isEmpty(str5)) {
                    str5 = str6;
                }
                objArr[2] = str5;
            } else {
                i3 = R.string.edit_business_address_partial_address;
                objArr = new Object[2];
                if (TextUtils.isEmpty(str4)) {
                    str4 = str6;
                }
                objArr[0] = str4;
                if (TextUtils.isEmpty(str5)) {
                    str5 = str6;
                }
                objArr[1] = str5;
            }
            String string = r11.getString(i3, objArr);
            if (string != null) {
                str6 = string.trim();
            }
            str3 = str6;
        }
        BusinessProfileFieldView businessProfileFieldView = this.A08;
        businessProfileFieldView.setText(str3, null);
        AnonymousClass12P r2 = this.A03;
        C25781Au r22 = this.A0B;
        C15370n3 r3 = this.A0F;
        if (r3 == null) {
            A05 = null;
        } else {
            A05 = C15370n3.A05(r3);
        }
        String A03 = C15380n4.A03(A05);
        boolean A0J = r3.A0J();
        AnonymousClass2VB r23 = this.A0C;
        Integer num = this.A0H;
        A00(r2, businessProfileFieldView, r22, r23, num, A03, 2, A0J, this.A01, this.A00);
        View view = this.A02;
        ViewGroup A0P = C12980iv.A0P(view, R.id.business_profile_field_bottom_container);
        C30161Wi r24 = r42.A00;
        Double d2 = r24.A00;
        int i4 = 0;
        if (d2 == null || (d = r24.A01) == null) {
            if (!TextUtils.isEmpty(businessProfileFieldView.getText())) {
                businessProfileFieldView.setVisibility(0);
            }
            View findViewById = A0P.findViewById(R.id.map_frame);
            if (findViewById != null) {
                A0P.removeView(findViewById);
            }
        } else {
            ActivityC13810kN r13 = this.A04;
            View.inflate(r13, R.layout.business_profile_map, A0P);
            View findViewById2 = view.findViewById(R.id.map_frame);
            View findViewById3 = view.findViewById(R.id.map_button);
            LatLng latLng = new LatLng(d2.doubleValue(), d.doubleValue());
            String text = businessProfileFieldView.getText();
            String A04 = this.A0D.A04(r3);
            StringBuilder A0k = C12960it.A0k("geo:0,0?q=");
            A0k.append(d2);
            A0k.append(",");
            A0k.append(d);
            A0k.append("(");
            if (TextUtils.isEmpty(text)) {
                text = A04;
            }
            A0k.append(text);
            ViewOnClickCListenerShape2S0200000_I1 viewOnClickCListenerShape2S0200000_I1 = new ViewOnClickCListenerShape2S0200000_I1(this, 6, C12970iu.A0B(Uri.parse(C12960it.A0d(")", A0k))));
            findViewById3.setOnClickListener(viewOnClickCListenerShape2S0200000_I1);
            businessProfileFieldView.setOnClickListener(viewOnClickCListenerShape2S0200000_I1);
            ViewGroup viewGroup = (ViewGroup) r13.findViewById(R.id.map_holder);
            WaMapView waMapView = new WaMapView(viewGroup.getContext());
            waMapView.A01(latLng, null, this.A0G);
            waMapView.A00(latLng);
            viewGroup.addView(waMapView, -1, -1);
            waMapView.setVisibility(0);
            View A0D = AnonymousClass028.A0D(businessProfileFieldView, R.id.field_textview);
            C12990iw.A19(A0D, TextUtils.isEmpty(businessProfileFieldView.getText()) ? 1 : 0);
            boolean isEmpty = TextUtils.isEmpty(businessProfileFieldView.getText());
            Resources resources = A0D.getResources();
            int i5 = R.dimen.business_field_map_padding_top;
            if (isEmpty) {
                i5 = R.dimen.business_field_map_padding_top_empty;
            }
            int dimension2 = (int) resources.getDimension(i5);
            AnonymousClass018 r5 = this.A0E;
            if (C28141Kv.A01(r5)) {
                dimension = 0;
            } else {
                dimension = (int) A0D.getResources().getDimension(R.dimen.business_field_map_padding_right);
            }
            if (C28141Kv.A01(r5)) {
                i2 = (int) A0D.getResources().getDimension(R.dimen.business_field_map_padding_right);
            } else {
                i2 = 0;
            }
            findViewById2.setPadding(dimension, dimension2, i2, (int) A0D.getResources().getDimension(R.dimen.business_field_map_padding_bottom));
            findViewById2.setVisibility(0);
            businessProfileFieldView.setVisibility(0);
        }
        for (BusinessProfileFieldView businessProfileFieldView2 : this.A0J) {
            int i6 = i4 + 1;
            List list = r26.A0F;
            if (i4 < list.size()) {
                str2 = C12960it.A0g(list, i4);
            } else {
                str2 = null;
            }
            if (!this.A0K || !A01(str2)) {
                businessProfileFieldView2.setText(null, null);
                businessProfileFieldView2.setSubText(null);
                businessProfileFieldView2.setIcon(R.drawable.ic_business_link);
                businessProfileFieldView2.setText(str2, null);
                A00(r2, businessProfileFieldView2, r22, r23, num, C15380n4.A03(C15370n3.A05(r3)), 0, r3.A0J(), this.A01, this.A00);
            }
            i4 = i6;
        }
        if (this.A0K) {
            int i7 = 0;
            for (BusinessProfileFieldView businessProfileFieldView3 : this.A0I) {
                int i8 = i7 + 1;
                List list2 = r26.A0F;
                if (i7 < list2.size()) {
                    str = C12960it.A0g(list2, i7);
                } else {
                    str = null;
                }
                if (A01(str) && (infoCard = this.A05) != null) {
                    businessProfileFieldView3.setText(null, null);
                    businessProfileFieldView3.setSubText(null);
                    businessProfileFieldView3.setIcon(R.drawable.ic_business_link);
                    infoCard.setVisibility(0);
                    businessProfileFieldView3.setText(str, null);
                    A00(r2, businessProfileFieldView3, r22, r23, num, C15380n4.A03(C15370n3.A05(r3)), 0, r3.A0J(), this.A01, this.A00);
                }
                i7 = i8;
            }
        }
        String str7 = r26.A0A;
        BusinessProfileFieldView businessProfileFieldView4 = this.A09;
        businessProfileFieldView4.setText(str7, null);
        A00(r2, businessProfileFieldView4, r22, r23, num, C15380n4.A03(C15370n3.A05(r3)), 1, r3.A0J(), this.A01, this.A00);
        BusinessHoursView businessHoursView = this.A07;
        C30201Wm r112 = r26.A00;
        String rawString = r26.A04.getRawString();
        boolean z2 = this.A01;
        boolean z3 = this.A00;
        int i9 = 8;
        if (r112 != null) {
            View A0D2 = AnonymousClass028.A0D(businessHoursView, R.id.business_hours_icon);
            AnonymousClass018 r14 = businessHoursView.A04;
            int i10 = Calendar.getInstance().get(7);
            int i11 = 0;
            while (true) {
                iArr = AnonymousClass4G3.A00;
                length = iArr.length;
                if (i11 >= length) {
                    i11 = length - 1;
                    break;
                }
                if (iArr[i11] == i10) {
                    break;
                }
                i11++;
            }
            HashMap hashMap = new HashMap(7);
            for (C30191Wl r43 : r112.A02) {
                Integer valueOf = Integer.valueOf(r43.A00);
                if (!hashMap.containsKey(valueOf)) {
                    hashMap.put(valueOf, C12960it.A0l());
                }
                ((List) hashMap.get(valueOf)).add(r43);
            }
            ArrayList A0l = C12960it.A0l();
            for (int i12 = i11; i12 < length + i11; i12++) {
                int i13 = iArr[i12 % length];
                List list3 = (List) hashMap.get(Integer.valueOf(i13));
                switch (i13) {
                    case 1:
                        i = 204;
                        break;
                    case 2:
                        i = 202;
                        break;
                    case 3:
                        i = 206;
                        break;
                    case 4:
                        i = 207;
                        break;
                    case 5:
                        i = 205;
                        break;
                    case 6:
                        i = 201;
                        break;
                    case 7:
                        i = 203;
                        break;
                    default:
                        throw new AssertionError("Unreachable code");
                }
                String A08 = r14.A08(i);
                if ("titlecase-firstword".equals(r14.A08(272))) {
                    A08 = AnonymousClass1MY.A05(C12970iu.A14(r14), A08);
                }
                if (list3 == null) {
                    join = r14.A09(R.string.business_hours_day_closed);
                } else {
                    if (list3.size() > 1) {
                        C12980iv.A1S(list3, 10);
                    }
                    ArrayList A0l2 = C12960it.A0l();
                    Iterator it = list3.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            C30191Wl r32 = (C30191Wl) it.next();
                            int i14 = r32.A01;
                            if (i14 == 0) {
                                Integer num2 = r32.A03;
                                AnonymousClass009.A05(num2);
                                int intValue = num2.intValue();
                                Integer num3 = r32.A02;
                                AnonymousClass009.A05(num3);
                                int intValue2 = num3.intValue();
                                Locale A14 = C12970iu.A14(r14);
                                Calendar instance = Calendar.getInstance(A14);
                                instance.set(11, intValue / 60);
                                instance.set(12, intValue % 60);
                                instance.set(13, 0);
                                Calendar instance2 = Calendar.getInstance(A14);
                                instance2.set(11, intValue2 / 60);
                                instance2.set(12, intValue2 % 60);
                                instance2.set(13, 0);
                                A0l2.add(AnonymousClass3JK.A04(r14, instance, instance2));
                            } else if (i14 == 1) {
                                join = AbstractC27291Gt.A07(C12970iu.A14(r14), r14.A09(R.string.business_hours_day_mode_open_24h));
                            } else if (i14 == 2) {
                                join = r14.A09(R.string.business_hours_day_mode_appointment_only);
                            }
                        } else {
                            join = TextUtils.join("\n", A0l2);
                        }
                    }
                }
                A0l.add(C12990iw.A0L(A08, join));
            }
            if (A0l.size() != 0) {
                A0D2.setVisibility(8);
                businessHoursView.setPadding(C12960it.A09(businessHoursView).getDimensionPixelSize(R.dimen.info_screen_padding), businessHoursView.getPaddingTop(), C12960it.A09(businessHoursView).getDimensionPixelSize(R.dimen.info_screen_padding), businessHoursView.getPaddingBottom());
                businessHoursView.A01.setupWithOpenNow(A0l, businessHoursView.A03.A00(), r112);
                businessHoursView.setOnClickListener(new View.OnClickListener(num, rawString, z2, z3) { // from class: X.3lf
                    public final /* synthetic */ Integer A01;
                    public final /* synthetic */ String A02;
                    public final /* synthetic */ boolean A03 = true;
                    public final /* synthetic */ boolean A04;
                    public final /* synthetic */ boolean A05;

                    {
                        this.A02 = r4;
                        this.A01 = r3;
                        this.A04 = r5;
                        this.A05 = r6;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        BusinessHoursView businessHoursView2 = BusinessHoursView.this;
                        boolean z4 = this.A03;
                        String str8 = this.A02;
                        Integer num4 = this.A01;
                        boolean z5 = this.A04;
                        boolean z6 = this.A05;
                        if (z4 && !businessHoursView2.A06) {
                            businessHoursView2.A02.A05(null, num4, str8, 4, z5, z6);
                        }
                        businessHoursView2.A06 = !businessHoursView2.A06;
                        businessHoursView2.A02();
                    }
                });
                businessHoursView.A02();
                i9 = 0;
            }
            r4 = r26.A02;
            boolean z4 = true;
            if (r4 != null || (r4.A00 == null && r4.A01 == null)) {
                z = false;
            } else {
                z = true;
            }
            this.A01 = z;
            r1 = r26.A01;
            if (r1 != null || TextUtils.isEmpty(r1.A00)) {
                z4 = false;
            }
            this.A00 = z4;
        }
        businessHoursView.setVisibility(i9);
        r4 = r26.A02;
        boolean z4 = true;
        if (r4 != null) {
        }
        z = false;
        this.A01 = z;
        r1 = r26.A01;
        if (r1 != null) {
        }
        z4 = false;
        this.A00 = z4;
    }
}
