package X;

import com.whatsapp.contact.picker.PhoneContactsSelector;
import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.38Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38Z extends AbstractC16350or {
    public final C22680zT A00;
    public final C14650lo A01;
    public final C15550nR A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;
    public final AbstractC14640lm A05;
    public final WeakReference A06;
    public final List A07;

    public AnonymousClass38Z(C22680zT r2, C14650lo r3, C15550nR r4, PhoneContactsSelector phoneContactsSelector, C16590pI r6, AnonymousClass018 r7, AbstractC14640lm r8, List list) {
        this.A03 = r6;
        this.A02 = r4;
        this.A04 = r7;
        this.A00 = r2;
        this.A01 = r3;
        this.A06 = C12970iu.A10(phoneContactsSelector);
        this.A05 = r8;
        this.A07 = list;
    }
}
