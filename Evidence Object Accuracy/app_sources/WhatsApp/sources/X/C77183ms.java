package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.Log;
import android.util.SparseArray;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3ms  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77183ms extends AbstractC76783mE {
    public final C95524ds A00;

    public C77183ms(List list) {
        super("DvbDecoder");
        C95304dT r0 = new C95304dT(C72463ee.A0b(list, 0));
        this.A00 = new C95524ds(r0.A0F(), r0.A0F());
    }

    @Override // X.AbstractC76783mE
    public AbstractC116805Wy A07(byte[] bArr, int i, boolean z) {
        List unmodifiableList;
        int[] iArr;
        int i2;
        Paint paint;
        int[] iArr2;
        C91964Tx r3;
        int i3;
        int i4;
        int i5;
        int i6;
        if (z) {
            C91914Ts r1 = this.A00.A06;
            r1.A08.clear();
            r1.A06.clear();
            r1.A07.clear();
            r1.A04.clear();
            r1.A05.clear();
            r1.A00 = null;
            r1.A01 = null;
        }
        C95524ds r2 = this.A00;
        C95054d0 r0 = new C95054d0(bArr, i);
        while (true) {
            int i7 = r0.A01;
            if (((i7 - r0.A02) << 3) - r0.A00 >= 48 && r0.A04(8) == 15) {
                C91914Ts r12 = r2.A06;
                int A04 = r0.A04(8);
                int A042 = r0.A04(16);
                int A043 = r0.A04(16);
                C95314dV.A04(C12960it.A1T(r0.A00));
                int i8 = r0.A02;
                int i9 = i8 + A043;
                if ((A043 << 3) > ((i7 - i8) << 3) - r0.A00) {
                    Log.w("DvbParser", "Data field length exceeds limit");
                    r0.A08(C95054d0.A00(r0));
                } else {
                    switch (A04) {
                        case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                            if (A042 == r12.A03) {
                                AnonymousClass4P9 r11 = r12.A01;
                                r0.A04(8);
                                int A044 = r0.A04(4);
                                int A045 = r0.A04(2);
                                r0.A08(2);
                                int i10 = A043 - 2;
                                SparseArray sparseArray = new SparseArray();
                                while (i10 > 0) {
                                    int A046 = r0.A04(8);
                                    r0.A08(8);
                                    i10 -= 6;
                                    sparseArray.put(A046, new AnonymousClass4M3(r0.A04(16), r0.A04(16)));
                                }
                                AnonymousClass4P9 r5 = new AnonymousClass4P9(sparseArray, A044, A045);
                                if (r5.A00 == 0) {
                                    if (!(r11 == null || r11.A01 == r5.A01)) {
                                        r12.A01 = r5;
                                        break;
                                    }
                                } else {
                                    r12.A01 = r5;
                                    r12.A08.clear();
                                    r12.A06.clear();
                                    r12.A07.clear();
                                    break;
                                }
                            }
                            break;
                        case 17:
                            AnonymousClass4P9 r4 = r12.A01;
                            if (A042 == r12.A03 && r4 != null) {
                                int A047 = r0.A04(8);
                                r0.A08(4);
                                boolean A0C = r0.A0C();
                                r0.A08(3);
                                int i11 = 16;
                                int A048 = r0.A04(16);
                                int A049 = r0.A04(16);
                                r0.A04(3);
                                int A0410 = r0.A04(3);
                                int i12 = 2;
                                int A01 = C95054d0.A01(r0, 2, 8);
                                int A0411 = r0.A04(8);
                                int A0412 = r0.A04(4);
                                int A0413 = r0.A04(2);
                                r0.A08(2);
                                int i13 = A043 - 10;
                                SparseArray sparseArray2 = new SparseArray();
                                while (i13 > 0) {
                                    int A0414 = r0.A04(i11);
                                    int A0415 = r0.A04(i12);
                                    r0.A04(i12);
                                    int A0416 = r0.A04(12);
                                    int A012 = C95054d0.A01(r0, 4, 12);
                                    i13 -= 6;
                                    if (A0415 == 1 || A0415 == 2) {
                                        r0.A04(8);
                                        r0.A04(8);
                                        i13 -= 2;
                                    }
                                    sparseArray2.put(A0414, new AnonymousClass4M4(A0416, A012));
                                    i12 = 2;
                                    i11 = 16;
                                }
                                C91964Tx r10 = new C91964Tx(sparseArray2, A047, A048, A049, A0410, A01, A0411, A0412, A0413, A0C);
                                if (r4.A00 == 0 && (r3 = (C91964Tx) r12.A08.get(r10.A03)) != null) {
                                    SparseArray sparseArray3 = r3.A08;
                                    for (int i14 = 0; i14 < sparseArray3.size(); i14++) {
                                        r10.A08.put(sparseArray3.keyAt(i14), sparseArray3.valueAt(i14));
                                    }
                                }
                                r12.A08.put(r10.A03, r10);
                                break;
                            }
                            break;
                        case 18:
                            if (A042 != r12.A03) {
                                if (A042 == r12.A02) {
                                    AnonymousClass4R3 A00 = C95524ds.A00(r0, A043);
                                    r12.A04.put(A00.A00, A00);
                                    break;
                                }
                            } else {
                                AnonymousClass4R3 A002 = C95524ds.A00(r0, A043);
                                r12.A06.put(A002.A00, A002);
                                break;
                            }
                            break;
                        case 19:
                            if (A042 != r12.A03) {
                                if (A042 == r12.A02) {
                                    AnonymousClass4R4 A013 = C95524ds.A01(r0);
                                    r12.A05.put(A013.A00, A013);
                                    break;
                                }
                            } else {
                                AnonymousClass4R4 A014 = C95524ds.A01(r0);
                                r12.A07.put(A014.A00, A014);
                                break;
                            }
                            break;
                        case C43951xu.A01:
                            if (A042 == r12.A03) {
                                r0.A08(4);
                                boolean A0C2 = r0.A0C();
                                r0.A08(3);
                                int A0417 = r0.A04(16);
                                int A0418 = r0.A04(16);
                                if (A0C2) {
                                    i5 = r0.A04(16);
                                    i3 = r0.A04(16);
                                    i6 = r0.A04(16);
                                    i4 = r0.A04(16);
                                } else {
                                    i3 = A0417;
                                    i4 = A0418;
                                    i5 = 0;
                                    i6 = 0;
                                }
                                r12.A00 = new AnonymousClass4T5(A0417, A0418, i5, i3, i6, i4);
                                break;
                            }
                            break;
                    }
                    C95314dV.A04(C12960it.A1T(r0.A00));
                    r0.A09(i9 - r0.A02);
                }
            }
        }
        C91914Ts r13 = r2.A06;
        AnonymousClass4P9 r42 = r13.A01;
        if (r42 == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            AnonymousClass4T5 r122 = r13.A00;
            if (r122 == null) {
                r122 = r2.A05;
            }
            Bitmap bitmap = r2.A00;
            if (!(bitmap != null && r122.A05 + 1 == bitmap.getWidth() && r122.A00 + 1 == r2.A00.getHeight())) {
                Bitmap createBitmap = Bitmap.createBitmap(r122.A05 + 1, r122.A00 + 1, Bitmap.Config.ARGB_8888);
                r2.A00 = createBitmap;
                r2.A01.setBitmap(createBitmap);
            }
            ArrayList A0l = C12960it.A0l();
            SparseArray sparseArray4 = r42.A02;
            for (int i15 = 0; i15 < sparseArray4.size(); i15++) {
                Canvas canvas = r2.A01;
                canvas.save();
                AnonymousClass4M3 r32 = (AnonymousClass4M3) sparseArray4.valueAt(i15);
                C91964Tx r9 = (C91964Tx) r13.A08.get(sparseArray4.keyAt(i15));
                int i16 = r32.A00 + r122.A02;
                int i17 = r32.A01 + r122.A04;
                canvas.clipRect(i16, i17, Math.min(r9.A07 + i16, r122.A01), Math.min(r9.A02 + i17, r122.A03));
                AnonymousClass4R3 r6 = (AnonymousClass4R3) r13.A06.get(r9.A00);
                if (r6 == null && (r6 = (AnonymousClass4R3) r13.A04.get(r9.A00)) == null) {
                    r6 = r2.A04;
                }
                SparseArray sparseArray5 = r9.A08;
                for (int i18 = 0; i18 < sparseArray5.size(); i18++) {
                    int keyAt = sparseArray5.keyAt(i18);
                    AnonymousClass4M4 r14 = (AnonymousClass4M4) sparseArray5.valueAt(i18);
                    AnonymousClass4R4 r142 = (AnonymousClass4R4) r13.A07.get(keyAt);
                    if (r142 != null || (r142 = (AnonymousClass4R4) r13.A05.get(keyAt)) != null) {
                        if (r142.A01) {
                            paint = null;
                        } else {
                            paint = r2.A02;
                        }
                        int i19 = r9.A01;
                        int i20 = r14.A00 + i16;
                        int i21 = i17 + r14.A01;
                        if (i19 == 3) {
                            iArr2 = r6.A03;
                        } else if (i19 == 2) {
                            iArr2 = r6.A02;
                        } else {
                            iArr2 = r6.A01;
                        }
                        C95524ds.A02(canvas, paint, r142.A03, iArr2, i19, i20, i21);
                        C95524ds.A02(canvas, paint, r142.A02, iArr2, i19, i20, i21 + 1);
                    }
                }
                if (r9.A09) {
                    int i22 = r9.A01;
                    if (i22 == 3) {
                        iArr = r6.A03;
                        i2 = r9.A06;
                    } else if (i22 == 2) {
                        iArr = r6.A02;
                        i2 = r9.A05;
                    } else {
                        iArr = r6.A01;
                        i2 = r9.A04;
                    }
                    int i23 = iArr[i2];
                    Paint paint2 = r2.A03;
                    paint2.setColor(i23);
                    canvas.drawRect((float) i16, (float) i17, (float) (r9.A07 + i16), (float) (r9.A02 + i17), paint2);
                }
                C94144bK r52 = new C94144bK();
                Bitmap bitmap2 = r2.A00;
                int i24 = r9.A07;
                int i25 = r9.A02;
                r52.A0C = Bitmap.createBitmap(bitmap2, i16, i17, i24, i25);
                float f = (float) r122.A05;
                r52.A02 = ((float) i16) / f;
                r52.A08 = 0;
                float f2 = (float) r122.A00;
                r52.A01 = ((float) i17) / f2;
                r52.A07 = 0;
                r52.A06 = 0;
                r52.A04 = ((float) i24) / f;
                r52.A00 = ((float) i25) / f2;
                A0l.add(r52.A00());
                canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                canvas.restore();
            }
            unmodifiableList = Collections.unmodifiableList(A0l);
        }
        return new C107704xo(unmodifiableList);
    }
}
