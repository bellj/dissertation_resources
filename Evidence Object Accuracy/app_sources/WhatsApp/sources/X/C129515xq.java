package X;

import android.text.TextUtils;
import java.math.BigDecimal;

/* renamed from: X.5xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C129515xq {
    public final AnonymousClass102 A00;
    public final C14850m9 A01;

    public C129515xq(AnonymousClass102 r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public C30821Yy A00(String str, String str2, String str3) {
        BigDecimal bigDecimal;
        AbstractC30791Yv A02 = this.A00.A02("INR");
        if (!TextUtils.isEmpty(str)) {
            bigDecimal = new BigDecimal(str);
        } else if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3)) {
            return A02.AEA();
        } else {
            bigDecimal = new BigDecimal(str3);
        }
        return C117295Zj.A0D(A02, bigDecimal);
    }

    public boolean A01(AbstractC28901Pl r4, String str) {
        AnonymousClass1ZY r1 = r4.A08;
        if (!(r1 instanceof C119755f3)) {
            return false;
        }
        boolean equals = "p2m".equals(str);
        if ((!"OD_UNSECURED".equals(((C119755f3) r1).A0B)) || equals) {
            return false;
        }
        return this.A01.A07(1677);
    }
}
