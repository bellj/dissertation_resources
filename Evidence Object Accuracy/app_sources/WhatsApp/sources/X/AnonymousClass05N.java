package X;

import java.util.HashMap;

/* renamed from: X.05N  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05N extends AnonymousClass03E {
    public HashMap A00 = new HashMap();

    @Override // X.AnonymousClass03E
    public AnonymousClass05U A00(Object obj) {
        return (AnonymousClass05U) this.A00.get(obj);
    }

    @Override // X.AnonymousClass03E
    public Object A01(Object obj) {
        Object A01 = super.A01(obj);
        this.A00.remove(obj);
        return A01;
    }

    @Override // X.AnonymousClass03E
    public Object A02(Object obj, Object obj2) {
        AnonymousClass05U A00 = A00(obj);
        if (A00 != null) {
            return A00.A03;
        }
        HashMap hashMap = this.A00;
        AnonymousClass05U r1 = new AnonymousClass05U(obj, obj2);
        super.A00++;
        AnonymousClass05U r0 = this.A01;
        if (r0 == null) {
            this.A02 = r1;
        } else {
            r0.A00 = r1;
            r1.A01 = r0;
        }
        this.A01 = r1;
        hashMap.put(obj, r1);
        return null;
    }
}
