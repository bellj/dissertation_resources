package X;

/* renamed from: X.31B  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31B extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;

    public AnonymousClass31B() {
        super(3292, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A06);
        r3.Abe(2, this.A07);
        r3.Abe(3, this.A08);
        r3.Abe(4, this.A01);
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(7, this.A00);
        r3.Abe(8, this.A09);
        r3.Abe(9, this.A05);
        r3.Abe(10, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCatalogCategoryView {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogCategoryId", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogOwnerJid", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "catalogSessionId", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "categoryBrowsingEntryPoint", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "categoryItemIndex", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "categoryLevel", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "isLastLevel", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "productId", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sequenceNumber", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "viewAction", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
