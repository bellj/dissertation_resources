package X;

import android.content.Context;
import android.widget.ImageView;

/* renamed from: X.3hb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74213hb extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public C74213hb(Context context) {
        super(context, null);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int defaultSize = ImageView.getDefaultSize(getSuggestedMinimumWidth(), i);
        setMeasuredDimension(defaultSize, (int) (((double) ((float) defaultSize)) * 1.5d));
    }
}
