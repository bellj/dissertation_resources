package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98754jC implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        Bundle bundle = null;
        C78603pB[] r1 = null;
        C56422kr r2 = null;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                bundle = C95664e9.A05(parcel, readInt);
            } else if (c == 2) {
                r1 = (C78603pB[]) C95664e9.A0K(parcel, C78603pB.CREATOR, readInt);
            } else if (c == 3) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c != 4) {
                C95664e9.A0D(parcel, readInt);
            } else {
                r2 = (C56422kr) C95664e9.A07(parcel, C56422kr.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56472kw(bundle, r2, r1, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C56472kw[i];
    }
}
