package X;

import android.view.LayoutInflater;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.35R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35R extends AbstractC33641ei {
    public final View A00;
    public final C92784Xk A01;

    public AnonymousClass35R(C14900mE r11, AnonymousClass18U r12, AnonymousClass01d r13, AnonymousClass018 r14, AnonymousClass1CH r15, C92784Xk r16, C48242Fd r17) {
        super(r12, r11, r13, r14, r15, r17);
        this.A01 = r16;
        View A0O = C12980iv.A0O(LayoutInflater.from(A02()), R.layout.status_playback_deleting);
        this.A00 = A0O;
        C12960it.A0I(A0O, R.id.status_playback_deleting_subtitle).setText(R.string.deleting_your_status);
    }

    @Override // X.AbstractC33641ei
    public long A01() {
        return this.A01.A03;
    }

    @Override // X.AbstractC33641ei
    public void A07() {
        AbstractC33641ei.A00(this.A01, this);
    }

    @Override // X.AbstractC33641ei
    public void A08() {
        this.A01.A02();
    }
}
