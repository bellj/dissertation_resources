package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.3wX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83003wX extends AbstractC94534c0 {
    public final Object A00;
    public final boolean A01 = false;

    public C83003wX(CharSequence charSequence) {
        this.A00 = charSequence.toString();
    }

    public C83003wX(Object obj) {
        this.A00 = obj;
    }

    public static AbstractC94534c0 A00(AbstractC94534c0 r1) {
        C83003wX A03 = r1.A03();
        if (!(A03.A08() instanceof List)) {
            return AbstractC116885Xh.A03;
        }
        return new C83023wZ(Collections.unmodifiableList((List) A03.A08()));
    }

    public Object A08() {
        try {
            if (this.A01) {
                return this.A00;
            }
            C94764cV r1 = new C94764cV(-1);
            return new AnonymousClass5LL(r1.A00).A0A(this.A00.toString(), AnonymousClass4ZZ.A02.A00);
        } catch (AnonymousClass4CH e) {
            throw new IllegalArgumentException(e);
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C83003wX)) {
                return false;
            }
            Object obj2 = this.A00;
            Object obj3 = ((C83003wX) obj).A00;
            if (obj2 != null) {
                if (!obj2.equals(obj3)) {
                    return false;
                }
            } else if (obj3 != null) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        return this.A00.toString();
    }
}
