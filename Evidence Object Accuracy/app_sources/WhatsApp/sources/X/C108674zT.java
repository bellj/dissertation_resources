package X;

/* renamed from: X.4zT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108674zT implements AbstractC115175Qm {
    public static final AbstractC116375Ve A01 = new C108594zL();
    public final AbstractC116375Ve A00;

    public C108674zT() {
        AbstractC116375Ve r1;
        AbstractC116375Ve[] r2 = new AbstractC116375Ve[2];
        r2[0] = C108614zN.A00;
        try {
            r1 = (AbstractC116375Ve) C72453ed.A0k(Class.forName("com.google.protobuf.DescriptorMessageInfoFactory"), "getInstance");
        } catch (Exception unused) {
            r1 = A01;
        }
        r2[1] = r1;
        this.A00 = new C108604zM(r2);
    }
}
