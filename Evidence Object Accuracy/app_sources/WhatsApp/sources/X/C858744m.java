package X;

import com.whatsapp.profile.ViewProfilePhoto;
import java.util.Set;

/* renamed from: X.44m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C858744m extends AbstractC33331dp {
    public final /* synthetic */ ViewProfilePhoto A00;

    public C858744m(ViewProfilePhoto viewProfilePhoto) {
        this.A00 = viewProfilePhoto;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        ViewProfilePhoto.A02(this.A00);
    }
}
