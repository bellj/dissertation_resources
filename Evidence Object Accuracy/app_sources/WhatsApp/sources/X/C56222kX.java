package X;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.2kX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56222kX extends AbstractC64703Go {
    public final List A00 = C12960it.A0l();
    public final List A01 = C12960it.A0l();
    public final Map A02 = C12970iu.A11();

    public final String toString() {
        HashMap A11 = C12970iu.A11();
        List list = this.A00;
        if (!list.isEmpty()) {
            A11.put("products", list);
        }
        List list2 = this.A01;
        if (!list2.isEmpty()) {
            A11.put("promotions", list2);
        }
        Map map = this.A02;
        if (!map.isEmpty()) {
            A11.put("impressions", map);
        }
        return AbstractC64703Go.A01("productAction", null, A11);
    }
}
