package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.whatsapp.InteractiveAnnotation;
import com.whatsapp.R;
import com.whatsapp.SerializablePoint;
import com.whatsapp.mediaview.PhotoView;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.2VE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VE {
    public Context A00;
    public View A01;
    public PopupWindow A02;
    public C244415n A03;

    public AnonymousClass2VE(Context context, ViewGroup viewGroup, C244415n r6) {
        this.A02 = new PopupWindow(context);
        this.A00 = context;
        this.A03 = r6;
        LayoutInflater A01 = AnonymousClass01d.A01(context);
        AnonymousClass009.A05(A01);
        this.A01 = A01.inflate(R.layout.tool_tip_view, viewGroup, false);
    }

    public boolean A00(PopupWindow.OnDismissListener onDismissListener, InteractiveAnnotation interactiveAnnotation, PhotoView photoView) {
        double d;
        int i;
        Bitmap photo = photoView.getPhoto();
        if (photo == null) {
            return false;
        }
        float height = (float) photo.getHeight();
        float[] fArr = {(float) photo.getWidth(), height};
        SerializablePoint[] serializablePointArr = interactiveAnnotation.polygonVertices;
        SerializablePoint serializablePoint = serializablePointArr[0];
        double d2 = serializablePoint.x;
        double d3 = (double) fArr[0];
        double d4 = d2 * d3;
        double d5 = serializablePoint.y;
        double d6 = (double) height;
        double d7 = d5 * d6;
        SerializablePoint serializablePoint2 = serializablePointArr[1];
        double d8 = serializablePoint2.x * d3;
        double d9 = serializablePoint2.y * d6;
        SerializablePoint serializablePoint3 = serializablePointArr[2];
        double d10 = serializablePoint3.x * d3;
        double d11 = serializablePoint3.y * d6;
        SerializablePoint serializablePoint4 = serializablePointArr[3];
        double d12 = serializablePoint4.x * d3;
        double d13 = serializablePoint4.y * d6;
        double d14 = (d4 + d10) / 2.0d;
        double d15 = (d7 + d11) / 2.0d;
        if (d8 <= d14 && d14 <= d10) {
            double d16 = d8 - d10;
            if (d16 != 0.0d) {
                d = d9 - (((d9 - d11) * (d8 - d14)) / d16);
                i = 2;
                float[] fArr2 = new float[i];
                fArr2[0] = (float) d14;
                fArr2[1] = (float) d;
                photoView.getImageMatrix().mapPoints(fArr2);
                fArr2[0] = fArr2[0] + ((float) photoView.getLeft());
                fArr2[1] = fArr2[1] + ((float) photoView.getTop());
                View rootView = photoView.getRootView();
                int i2 = (int) fArr2[0];
                int i3 = (int) fArr2[1];
                PopupWindow popupWindow = this.A02;
                popupWindow.setHeight(-2);
                popupWindow.setWidth(-2);
                popupWindow.setOutsideTouchable(true);
                popupWindow.setTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                View view = this.A01;
                popupWindow.setContentView(view);
                view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                C27531Hw.A06((TextView) view.findViewById(R.id.tooltip_text));
                view.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(interactiveAnnotation, 24, this));
                popupWindow.setOnDismissListener(onDismissListener);
                popupWindow.setAnimationStyle(R.style.location_sticker_popup);
                popupWindow.showAtLocation(rootView, 0, i2 - (view.getMeasuredWidth() >> 1), (int) (((float) i3) - (((float) view.getMeasuredHeight()) * 0.82f)));
                return true;
            }
            d = d15;
            i = 2;
            float[] fArr2 = new float[i];
            fArr2[0] = (float) d14;
            fArr2[1] = (float) d;
            photoView.getImageMatrix().mapPoints(fArr2);
            fArr2[0] = fArr2[0] + ((float) photoView.getLeft());
            fArr2[1] = fArr2[1] + ((float) photoView.getTop());
            View rootView = photoView.getRootView();
            int i2 = (int) fArr2[0];
            int i3 = (int) fArr2[1];
            PopupWindow popupWindow = this.A02;
            popupWindow.setHeight(-2);
            popupWindow.setWidth(-2);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setTouchable(true);
            popupWindow.setFocusable(true);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            View view = this.A01;
            popupWindow.setContentView(view);
            view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            C27531Hw.A06((TextView) view.findViewById(R.id.tooltip_text));
            view.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(interactiveAnnotation, 24, this));
            popupWindow.setOnDismissListener(onDismissListener);
            popupWindow.setAnimationStyle(R.style.location_sticker_popup);
            popupWindow.showAtLocation(rootView, 0, i2 - (view.getMeasuredWidth() >> 1), (int) (((float) i3) - (((float) view.getMeasuredHeight()) * 0.82f)));
            return true;
        } else if (d10 > d14 || d14 > d12) {
            i = 2;
            if (d12 > d14 || d14 > d4) {
                double d17 = d4 - d8;
                if (d17 != 0.0d) {
                    d = d7 - (((d7 - d9) * (d4 - d14)) / d17);
                }
                d = d15;
            } else {
                double d18 = d12 - d4;
                if (d18 != 0.0d) {
                    d = d13 - (((d13 - d7) * (d12 - d14)) / d18);
                }
                d = d15;
            }
            float[] fArr2 = new float[i];
            fArr2[0] = (float) d14;
            fArr2[1] = (float) d;
            photoView.getImageMatrix().mapPoints(fArr2);
            fArr2[0] = fArr2[0] + ((float) photoView.getLeft());
            fArr2[1] = fArr2[1] + ((float) photoView.getTop());
            View rootView = photoView.getRootView();
            int i2 = (int) fArr2[0];
            int i3 = (int) fArr2[1];
            PopupWindow popupWindow = this.A02;
            popupWindow.setHeight(-2);
            popupWindow.setWidth(-2);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setTouchable(true);
            popupWindow.setFocusable(true);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            View view = this.A01;
            popupWindow.setContentView(view);
            view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            C27531Hw.A06((TextView) view.findViewById(R.id.tooltip_text));
            view.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(interactiveAnnotation, 24, this));
            popupWindow.setOnDismissListener(onDismissListener);
            popupWindow.setAnimationStyle(R.style.location_sticker_popup);
            popupWindow.showAtLocation(rootView, 0, i2 - (view.getMeasuredWidth() >> 1), (int) (((float) i3) - (((float) view.getMeasuredHeight()) * 0.82f)));
            return true;
        } else {
            double d19 = d10 - d12;
            if (d19 != 0.0d) {
                d = d11 - (((d11 - d13) * (d10 - d14)) / d19);
                i = 2;
                float[] fArr2 = new float[i];
                fArr2[0] = (float) d14;
                fArr2[1] = (float) d;
                photoView.getImageMatrix().mapPoints(fArr2);
                fArr2[0] = fArr2[0] + ((float) photoView.getLeft());
                fArr2[1] = fArr2[1] + ((float) photoView.getTop());
                View rootView = photoView.getRootView();
                int i2 = (int) fArr2[0];
                int i3 = (int) fArr2[1];
                PopupWindow popupWindow = this.A02;
                popupWindow.setHeight(-2);
                popupWindow.setWidth(-2);
                popupWindow.setOutsideTouchable(true);
                popupWindow.setTouchable(true);
                popupWindow.setFocusable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                View view = this.A01;
                popupWindow.setContentView(view);
                view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                C27531Hw.A06((TextView) view.findViewById(R.id.tooltip_text));
                view.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(interactiveAnnotation, 24, this));
                popupWindow.setOnDismissListener(onDismissListener);
                popupWindow.setAnimationStyle(R.style.location_sticker_popup);
                popupWindow.showAtLocation(rootView, 0, i2 - (view.getMeasuredWidth() >> 1), (int) (((float) i3) - (((float) view.getMeasuredHeight()) * 0.82f)));
                return true;
            }
            d = d15;
            i = 2;
            float[] fArr2 = new float[i];
            fArr2[0] = (float) d14;
            fArr2[1] = (float) d;
            photoView.getImageMatrix().mapPoints(fArr2);
            fArr2[0] = fArr2[0] + ((float) photoView.getLeft());
            fArr2[1] = fArr2[1] + ((float) photoView.getTop());
            View rootView = photoView.getRootView();
            int i2 = (int) fArr2[0];
            int i3 = (int) fArr2[1];
            PopupWindow popupWindow = this.A02;
            popupWindow.setHeight(-2);
            popupWindow.setWidth(-2);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setTouchable(true);
            popupWindow.setFocusable(true);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            View view = this.A01;
            popupWindow.setContentView(view);
            view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            C27531Hw.A06((TextView) view.findViewById(R.id.tooltip_text));
            view.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(interactiveAnnotation, 24, this));
            popupWindow.setOnDismissListener(onDismissListener);
            popupWindow.setAnimationStyle(R.style.location_sticker_popup);
            popupWindow.showAtLocation(rootView, 0, i2 - (view.getMeasuredWidth() >> 1), (int) (((float) i3) - (((float) view.getMeasuredHeight()) * 0.82f)));
            return true;
        }
    }
}
