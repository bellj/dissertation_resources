package X;

/* renamed from: X.1yp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC44381yp extends AnonymousClass19V {
    public final C19930uu A00;
    public final String A01;

    public AbstractC44381yp(C18790t3 r14, C14820m6 r15, C14850m9 r16, C19930uu r17, AnonymousClass01H r18, String str, AnonymousClass01N r20, AnonymousClass01N r21, long j) {
        super(r14, r15, r16, null, r18, null, null, r20, r21, j);
        this.A00 = r17;
        this.A01 = str;
    }
}
