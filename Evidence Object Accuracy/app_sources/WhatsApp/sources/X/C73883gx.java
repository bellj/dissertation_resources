package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3gx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73883gx extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass2Zf A02;

    public C73883gx(AnonymousClass2Zf r1, int i, int i2) {
        this.A02 = r1;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        if (f != 1.0f) {
            AnonymousClass2Zf r2 = this.A02;
            int i = this.A01;
            r2.A00 = i + ((int) (((float) (this.A00 - i)) * f));
            r2.invalidateSelf();
        }
    }
}
