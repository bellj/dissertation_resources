package X;

import android.content.Context;
import android.content.res.Resources;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.bloks.BloksCameraOverlay;
import com.whatsapp.util.Log;

/* renamed from: X.675  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass675 implements AbstractC20990we {
    public boolean A00;
    public final C14900mE A01;
    public final C128485wB A02;
    public final AbstractC14440lR A03;

    public AnonymousClass675(C14900mE r1, C128485wB r2, AbstractC14440lR r3) {
        this.A01 = r1;
        this.A03 = r3;
        this.A02 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x009d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(android.view.SurfaceView r4, X.AnonymousClass4N0 r5, X.AnonymousClass674 r6, X.C128485wB r7) {
        /*
        // Method dump skipped, instructions count: 268
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass675.A00(android.view.SurfaceView, X.4N0, X.674, X.5wB):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (r0 == false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.AnonymousClass4N0 r6, X.AnonymousClass674 r7, X.C128485wB r8) {
        /*
            java.util.HashMap r3 = X.C12970iu.A11()
            java.lang.String r2 = r7.A08
            int r1 = r2.hashCode()
            r0 = 100313435(0x5faa95b, float:2.3572098E-35)
            r5 = 1
            if (r1 == r0) goto L_0x0081
            r0 = 112202875(0x6b0147b, float:6.6233935E-35)
            if (r1 == r0) goto L_0x0079
            r0 = 124969519(0x772e22f, float:1.8272526E-34)
            if (r1 != r0) goto L_0x0023
            java.lang.String r0 = "image_and_video"
            boolean r0 = r2.equals(r0)
            r4 = 1
        L_0x0021:
            if (r0 != 0) goto L_0x0024
        L_0x0023:
            r4 = -1
        L_0x0024:
            java.lang.String r2 = "video_file_name"
            if (r4 == 0) goto L_0x0073
            java.lang.String r1 = "image_file_name"
            if (r4 == r5) goto L_0x0052
            java.lang.String r0 = r7.A07
            r3.put(r1, r0)
        L_0x0031:
            X.28D r5 = r6.A01
            r0 = 44
            X.0l1 r4 = r5.A0G(r0)
            if (r4 == 0) goto L_0x0051
            X.0l2 r2 = new X.0l2
            r2.<init>()
            r1 = 0
            java.util.Map r0 = X.AnonymousClass3GC.A01(r3)
            r2.A05(r0, r1)
            X.0l3 r1 = r2.A03()
            X.0l7 r0 = r6.A00
            X.C28701Oq.A01(r0, r5, r1, r4)
        L_0x0051:
            return
        L_0x0052:
            boolean r0 = r7.A0C
            if (r0 != 0) goto L_0x0063
            boolean r0 = r7.A0A
            if (r0 == 0) goto L_0x0063
            X.5eV r0 = r7.A06
            X.AnonymousClass009.A03(r0)
            A00(r0, r6, r7, r8)
            return
        L_0x0063:
            java.lang.String r0 = r7.A07
            r3.put(r1, r0)
            java.lang.String r0 = r7.A09
            r3.put(r2, r0)
            r0 = 0
            r7.A0C = r0
            r7.A0A = r0
            goto L_0x0031
        L_0x0073:
            java.lang.String r0 = r7.A09
            r3.put(r2, r0)
            goto L_0x0031
        L_0x0079:
            java.lang.String r0 = "video"
            boolean r0 = r2.equals(r0)
            r4 = 0
            goto L_0x0021
        L_0x0081:
            java.lang.String r0 = "image"
            boolean r0 = r2.equals(r0)
            r4 = 2
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass675.A01(X.4N0, X.674, X.5wB):void");
    }

    public static void A02(AnonymousClass674 r1) {
        MediaRecorder mediaRecorder = r1.A04;
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            r1.A04.release();
            r1.A04 = null;
            Camera camera = r1.A03;
            if (camera != null) {
                camera.lock();
            }
        }
    }

    @Override // X.AbstractC20990we
    public void A6Y(Context context, View view, AbstractC11740gm r14, AnonymousClass4N0 r15, String str, String str2, String str3, String str4, String str5) {
        int i;
        Camera camera;
        String str6 = str5;
        this.A00 = false;
        AnonymousClass5RJ r8 = (AnonymousClass5RJ) AnonymousClass3JV.A04(r15.A00, r15.A01);
        AnonymousClass009.A05(r8);
        AnonymousClass674 r82 = (AnonymousClass674) r8;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        r82.A01 = displayMetrics.heightPixels;
        r82.A02 = displayMetrics.widthPixels;
        ViewGroup A04 = C117315Zl.A04(view, R.id.bloks_camera_preview);
        if (TextUtils.equals(str, "front")) {
            i = 1;
            r82.A00 = 1;
        } else {
            i = 0;
            r82.A00 = 0;
        }
        if (Camera.getNumberOfCameras() < i + 1) {
            i = 0;
        }
        try {
            camera = Camera.open(i);
        } catch (Exception e) {
            Log.e(C12960it.A0b("CAMERA EXPECTION", e));
            camera = null;
        }
        r82.A03 = camera;
        r82.A08 = str2;
        r82.A07 = str3;
        r82.A09 = str4;
        r82.A0C = false;
        r82.A0A = false;
        r82.A05 = (BloksCameraOverlay) view.findViewById(R.id.camera_overlay);
        Camera camera2 = r82.A03;
        int i2 = r82.A00;
        int i3 = r82.A02;
        int i4 = r82.A01;
        SurfaceHolder$CallbackC119475eV r0 = new SurfaceHolder$CallbackC119475eV(context);
        r0.A02 = camera2;
        r0.A01 = i3;
        r0.A00 = i4;
        ((AnonymousClass27X) r0).A00 = i2;
        r82.A06 = r0;
        BloksCameraOverlay bloksCameraOverlay = r82.A05;
        AnonymousClass009.A03(bloksCameraOverlay);
        if (str6 == null) {
            str6 = "original";
        }
        bloksCameraOverlay.A00(str6);
        A04.removeAllViews();
        A04.addView(r82.A06);
        SurfaceHolder$CallbackC119475eV r02 = r82.A06;
        AnonymousClass009.A03(r02);
        r02.getDisplayOrientation();
        AnonymousClass028.A0D(view, R.id.shutter).setOnClickListener(new View.OnClickListener(new Camera.PictureCallback(r14, r82, this) { // from class: X.63D
            public final /* synthetic */ AbstractC11740gm A00;
            public final /* synthetic */ AnonymousClass674 A01;
            public final /* synthetic */ AnonymousClass675 A02;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.hardware.Camera.PictureCallback
            public final void onPictureTaken(byte[] bArr, Camera camera3) {
                AnonymousClass675 r5 = this.A02;
                AnonymousClass674 r4 = this.A01;
                AbstractC11740gm r6 = this.A00;
                if (bArr == null) {
                    r5.A01.A0H(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0011: INVOKE  
                          (wrap: X.0mE : 0x000a: IGET  (r1v6 X.0mE A[REMOVE]) = (r5v0 'r5' X.675) X.675.A01 X.0mE)
                          (wrap: X.6FL : 0x000e: CONSTRUCTOR  (r0v8 X.6FL A[REMOVE]) = (r5v0 'r5' X.675) call: X.6FL.<init>(X.675):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.63D.onPictureTaken(byte[], android.hardware.Camera):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000e: CONSTRUCTOR  (r0v8 X.6FL A[REMOVE]) = (r5v0 'r5' X.675) call: X.6FL.<init>(X.675):void type: CONSTRUCTOR in method: X.63D.onPictureTaken(byte[], android.hardware.Camera):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6FL, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                        this = this;
                        X.675 r5 = r15.A02
                        X.674 r4 = r15.A01
                        X.0gm r6 = r15.A00
                        r11 = r16
                        if (r16 != 0) goto L_0x0015
                        X.0mE r1 = r5.A01
                        X.6FL r0 = new X.6FL
                        r0.<init>(r5)
                        r1.A0H(r0)
                        return
                    L_0x0015:
                        X.5wB r8 = r5.A02
                        java.lang.String r1 = r4.A07
                        r3 = 0
                        boolean r0 = android.text.TextUtils.isEmpty(r1)
                        if (r0 == 0) goto L_0x003f
                        java.lang.String r0 = "yyyyMMdd_HHmmss"
                        java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
                        r1.<init>(r0)
                        java.util.Date r0 = new java.util.Date
                        r0.<init>()
                        java.lang.String r2 = r1.format(r0)
                        java.lang.String r0 = "IMG_"
                        java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
                        r1.append(r2)
                        java.lang.String r0 = ".png"
                        java.lang.String r1 = X.C12960it.A0d(r0, r1)
                    L_0x003f:
                        java.io.File r9 = r8.A00(r1)
                        android.hardware.Camera$CameraInfo r2 = new android.hardware.Camera$CameraInfo
                        r2.<init>()
                        int r0 = r4.A00
                        android.hardware.Camera.getCameraInfo(r0, r2)
                        X.0lR r1 = r5.A03
                        int r2 = r2.facing
                        r0 = 1
                        boolean r14 = X.C12960it.A1V(r2, r0)
                        java.lang.String r10 = r4.A07
                        int r12 = r4.A01
                        int r13 = r4.A02
                        com.whatsapp.bloks.BloksCameraOverlay r7 = r4.A05
                        X.AnonymousClass009.A03(r7)
                        X.5ol r5 = new X.5ol
                        r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14)
                        java.lang.Void[] r0 = new java.lang.Void[r3]
                        r1.Ab5(r5, r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass63D.onPictureTaken(byte[], android.hardware.Camera):void");
                }
            }, r15, r82, this, str2) { // from class: X.64d
                public final /* synthetic */ Camera.PictureCallback A00;
                public final /* synthetic */ AnonymousClass4N0 A01;
                public final /* synthetic */ AnonymousClass674 A02;
                public final /* synthetic */ AnonymousClass675 A03;
                public final /* synthetic */ String A04;

                {
                    this.A03 = r4;
                    this.A04 = r5;
                    this.A01 = r2;
                    this.A02 = r3;
                    this.A00 = r1;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    AnonymousClass675 r6 = this.A03;
                    String str7 = this.A04;
                    AnonymousClass4N0 r4 = this.A01;
                    AnonymousClass674 r3 = this.A02;
                    Camera.PictureCallback pictureCallback = this.A00;
                    if (!r6.A00) {
                        r6.A00 = true;
                        int hashCode = str7.hashCode();
                        if (hashCode == 100313435 || hashCode != 112202875 || !str7.equals("video")) {
                            r3.A03.takePicture(null, null, pictureCallback);
                            return;
                        }
                        SurfaceHolder$CallbackC119475eV r1 = r3.A06;
                        AnonymousClass009.A03(r1);
                        AnonymousClass675.A00(r1, r4, r3, r6.A02);
                    }
                }
            });
        }

        @Override // X.AbstractC20990we
        public AnonymousClass5RJ A8C() {
            return new AnonymousClass674();
        }

        @Override // X.AbstractC20990we
        public AbstractC12070hK AKT() {
            return new AbstractC12070hK() { // from class: X.65x
                @Override // X.AbstractC12070hK
                public final AnonymousClass643 A8N(Context context) {
                    return C124825qB.A00(context, new TextureView(context));
                }
            };
        }

        @Override // X.AbstractC20990we
        public void Af9(AnonymousClass5RJ r2) {
            AnonymousClass674 r22 = (AnonymousClass674) r2;
            A02(r22);
            Camera camera = r22.A03;
            if (camera != null) {
                camera.release();
                r22.A03 = null;
            }
        }
    }
