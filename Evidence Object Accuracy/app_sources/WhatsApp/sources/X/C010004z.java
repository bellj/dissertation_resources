package X;

import X.AbstractC001200n;
import X.AnonymousClass058;
import X.AnonymousClass074;
import android.os.Bundle;
import androidx.savedstate.Recreator;
import java.util.Map;

/* renamed from: X.04z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C010004z {
    public final AnonymousClass058 A00 = new AnonymousClass058();
    public final AbstractC001500q A01;

    public C010004z(AbstractC001500q r2) {
        this.A01 = r2;
    }

    public void A00(Bundle bundle) {
        AbstractC001500q r3 = this.A01;
        AbstractC009904y ADr = r3.ADr();
        if (((C009804x) ADr).A02 == AnonymousClass05I.INITIALIZED) {
            ADr.A00(new Recreator(r3));
            AnonymousClass058 r1 = this.A00;
            if (!r1.A04) {
                if (bundle != null) {
                    r1.A00 = bundle.getBundle("androidx.lifecycle.BundlableSavedStateRegistry.key");
                }
                ADr.A00(new AnonymousClass054() { // from class: androidx.savedstate.SavedStateRegistry$1
                    @Override // X.AnonymousClass054
                    public void AWQ(AnonymousClass074 r32, AbstractC001200n r4) {
                        AnonymousClass058 r12;
                        boolean z;
                        if (r32 == AnonymousClass074.ON_START) {
                            r12 = AnonymousClass058.this;
                            z = true;
                        } else if (r32 == AnonymousClass074.ON_STOP) {
                            r12 = AnonymousClass058.this;
                            z = false;
                        } else {
                            return;
                        }
                        r12.A03 = z;
                    }
                });
                r1.A04 = true;
                return;
            }
            throw new IllegalStateException("SavedStateRegistry was already restored.");
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage");
    }

    public void A01(Bundle bundle) {
        AnonymousClass058 r1 = this.A00;
        Bundle bundle2 = new Bundle();
        Bundle bundle3 = r1.A00;
        if (bundle3 != null) {
            bundle2.putAll(bundle3);
        }
        AnonymousClass03E r0 = r1.A01;
        AnonymousClass075 r2 = new AnonymousClass075(r0);
        r0.A03.put(r2, Boolean.FALSE);
        while (r2.hasNext()) {
            Map.Entry entry = (Map.Entry) r2.next();
            bundle2.putBundle((String) entry.getKey(), ((AnonymousClass05A) entry.getValue()).AbH());
        }
        bundle.putBundle("androidx.lifecycle.BundlableSavedStateRegistry.key", bundle2);
    }
}
