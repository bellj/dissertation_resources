package X;

import com.whatsapp.net.tls13.WtCachedPsk;
import com.whatsapp.watls13.WtPersistentSession;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* renamed from: X.1D8  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1D8 implements AbstractC16990q5 {
    public final AnonymousClass14J A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public AnonymousClass1D8(AnonymousClass14J r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        File[] listFiles;
        LinkedHashSet linkedHashSet;
        long j;
        AnonymousClass14J r8 = this.A00;
        synchronized (r8) {
            File A01 = r8.A01();
            if (!(A01 == null || (listFiles = A01.listFiles()) == null)) {
                for (File file : listFiles) {
                    WtPersistentSession A00 = r8.A00(file);
                    if (!(A00 == null || (linkedHashSet = A00.A03) == null)) {
                        Iterator it = linkedHashSet.iterator();
                        while (it.hasNext()) {
                            WtCachedPsk wtCachedPsk = (WtCachedPsk) it.next();
                            if (wtCachedPsk.useTestTime) {
                                j = 3600000;
                            } else {
                                j = System.currentTimeMillis();
                            }
                            if (j - wtCachedPsk.ticketIssuedTime <= wtCachedPsk.ticketLifetime) {
                                break;
                            }
                        }
                    }
                    file.delete();
                    file.getAbsolutePath();
                }
            }
        }
    }
}
