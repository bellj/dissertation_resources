package X;

import android.text.TextUtils;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5yT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129895yT {
    public final AnonymousClass619 A00;
    public final AnonymousClass619 A01;
    public final C1316263m A02;
    public final List A03;
    public final List A04;

    public C129895yT(AnonymousClass619 r2, AnonymousClass619 r3, C1316263m r4, List list, List list2) {
        this.A04 = list;
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = Collections.unmodifiableList(list2);
    }

    public boolean A00() {
        Iterator it = this.A04.iterator();
        boolean z = false;
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (A0x.equals("READ_DISABLED") || A0x.equals("WRITE_DISABLED")) {
                return false;
            }
            z = true;
        }
        return z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C129895yT)) {
            return false;
        }
        C129895yT r4 = (C129895yT) obj;
        if (!this.A04.equals(r4.A04) || this.A03.size() != r4.A03.size() || !TextUtils.equals(this.A01.A00, r4.A01.A00) || !TextUtils.equals(this.A00.A00, r4.A00.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A04.hashCode();
    }
}
