package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.20d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C450920d extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C18610sj A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C450920d(Context context, C14900mE r2, AnonymousClass1FK r3, C18650sn r4, C18610sj r5) {
        super(context, r2, r4);
        this.A01 = r5;
        this.A00 = r3;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AV3(r2);
        }
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AVA(r2);
        }
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r5) {
        try {
            C452120p A00 = C452120p.A00(r5.A0F("account"));
            if (A00 != null) {
                this.A00.AVA(A00);
                return;
            }
            AnonymousClass56H r2 = null;
            AnonymousClass1FK r0 = this.A00;
            if (r0 != null) {
                r2 = new AbstractC451720l() { // from class: X.56H
                    @Override // X.AbstractC451720l
                    public final void AM7(List list) {
                        AnonymousClass1FK.this.AVB(new AnonymousClass46N());
                    }
                };
            }
            this.A01.A06(r2, r5, true);
        } catch (AnonymousClass1V9 e) {
            C30931Zj r22 = this.A01.A0I;
            StringBuilder sb = new StringBuilder("removePaymentMethod/onResponseSuccess/corrupt stream exception: ");
            sb.append(e);
            r22.A05(sb.toString());
            this.A00.AVA(new C452120p(500));
        }
    }
}
