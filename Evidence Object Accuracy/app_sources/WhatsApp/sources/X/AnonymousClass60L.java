package X;

import java.util.Arrays;

/* renamed from: X.60L  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60L {
    public final int A00;
    public final int A01;
    public final int A02;

    public AnonymousClass60L(int i, int i2, int i3) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
    }

    public static AnonymousClass60L A00(AnonymousClass1V8 r4) {
        return new AnonymousClass60L(r4.A0F("day").A04("value"), r4.A0F("month").A04("value"), r4.A0F("year").A04("value"));
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass60L r5 = (AnonymousClass60L) obj;
            if (!(this.A00 == r5.A00 && this.A01 == r5.A01 && this.A02 == r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        C12960it.A1O(objArr, this.A00);
        objArr[1] = Integer.valueOf(this.A01);
        objArr[2] = Integer.valueOf(this.A02);
        return Arrays.hashCode(objArr);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("KycDate{day=");
        A0k.append(this.A00);
        A0k.append(", month=");
        A0k.append(this.A01);
        A0k.append(", year=");
        A0k.append(this.A02);
        return C12970iu.A0v(A0k);
    }
}
