package X;

import android.text.TextUtils;

/* renamed from: X.1XN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XN extends AnonymousClass1XO implements AbstractC28871Pi {
    public C28891Pk A00;

    public AnonymousClass1XN(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 30, j);
    }

    public AnonymousClass1XN(AnonymousClass1IS r9, AnonymousClass1XN r10, long j) {
        super(r9, r10, r10.A0y, j, true);
        this.A00 = r10.A00.A00();
    }

    @Override // X.AbstractC28871Pi
    public String ADA() {
        if (TextUtils.isEmpty(this.A00.A02)) {
            return this.A00.A01;
        }
        StringBuilder sb = new StringBuilder();
        C28891Pk r1 = this.A00;
        sb.append(r1.A01);
        sb.append(" ");
        sb.append(r1.A02);
        return sb.toString();
    }

    @Override // X.AbstractC28871Pi
    public String AEh(AnonymousClass018 r3) {
        StringBuilder sb = new StringBuilder("📌 ");
        sb.append(this.A00.A01);
        return sb.toString();
    }

    @Override // X.AbstractC28871Pi
    public String AFs() {
        return this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public String AG2() {
        return this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public C28891Pk AH7() {
        return this.A00;
    }

    @Override // X.AbstractC28871Pi
    public void Acz(C28891Pk r1) {
        this.A00 = r1;
    }
}
