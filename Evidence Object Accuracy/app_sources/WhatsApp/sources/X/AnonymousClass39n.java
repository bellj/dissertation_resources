package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.39n  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass39n extends Enum {
    public static final /* synthetic */ AnonymousClass39n[] A00;
    public static final AnonymousClass39n A01;
    public static final AnonymousClass39n A02;
    public static final AnonymousClass39n A03;
    public static final AnonymousClass39n A04;
    public static final AnonymousClass39n A05;
    public static final AnonymousClass39n A06;
    public static final AnonymousClass39n A07;
    public static final AnonymousClass39n A08;
    public static final AnonymousClass39n A09;
    public static final AnonymousClass39n A0A;
    public static final AnonymousClass39n A0B;

    static {
        AnonymousClass39n r14 = new AnonymousClass39n("START_ARRAY", 0);
        A09 = r14;
        AnonymousClass39n r12 = new AnonymousClass39n("END_ARRAY", 1);
        A02 = r12;
        AnonymousClass39n r11 = new AnonymousClass39n("START_OBJECT", 2);
        A0A = r11;
        AnonymousClass39n r10 = new AnonymousClass39n("END_OBJECT", 3);
        A04 = r10;
        AnonymousClass39n r9 = new AnonymousClass39n("NAME", 4);
        A06 = r9;
        AnonymousClass39n r8 = new AnonymousClass39n("STRING", 5);
        A0B = r8;
        AnonymousClass39n r7 = new AnonymousClass39n("EXPRESSION", 6);
        A05 = r7;
        AnonymousClass39n r6 = new AnonymousClass39n("NUMBER", 7);
        A08 = r6;
        AnonymousClass39n r5 = new AnonymousClass39n("BOOLEAN", 8);
        A01 = r5;
        AnonymousClass39n r4 = new AnonymousClass39n("NULL", 9);
        A07 = r4;
        AnonymousClass39n r2 = new AnonymousClass39n("END_DOCUMENT", 10);
        A03 = r2;
        AnonymousClass39n[] r1 = new AnonymousClass39n[11];
        r1[0] = r14;
        r1[1] = r12;
        C12980iv.A1P(r11, r10, r9, r1);
        C12970iu.A1R(r8, r7, r6, r5, r1);
        r1[9] = r4;
        r1[10] = r2;
        A00 = r1;
    }

    public AnonymousClass39n(String str, int i) {
    }

    public static AnonymousClass39n valueOf(String str) {
        return (AnonymousClass39n) Enum.valueOf(AnonymousClass39n.class, str);
    }

    public static AnonymousClass39n[] values() {
        return (AnonymousClass39n[]) A00.clone();
    }
}
