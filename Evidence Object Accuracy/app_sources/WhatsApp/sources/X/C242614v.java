package X;

import android.database.Cursor;

/* renamed from: X.14v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242614v {
    public final C16490p7 A00;

    public C242614v(C16490p7 r1) {
        this.A00 = r1;
    }

    public void A00(AbstractC15340mz r9) {
        C16310on A01 = this.A00.get();
        try {
            int i = 1;
            Cursor A09 = A01.A03.A09("SELECT forward_score FROM message_forwarded WHERE message_row_id = ?", new String[]{Long.toString(r9.A11)});
            if (A09 != null && A09.moveToNext()) {
                i = A09.getInt(A09.getColumnIndexOrThrow("forward_score"));
            }
            r9.A05 = i;
            if (A09 != null) {
                A09.close();
            }
            A01.close();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
