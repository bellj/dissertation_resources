package X;

/* renamed from: X.1H2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1H2 extends AbstractC250017s {
    public final C250217u A00;
    public final C250417w A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C19990v2 A04;

    public AnonymousClass1H2(C250217u r1, C250417w r2, C14830m7 r3, C16590pI r4, C19990v2 r5, C233511i r6) {
        super(r6);
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
    }

    public final void A08(C34521gH r6) {
        AnonymousClass1PE A06;
        AbstractC14640lm r4 = r6.A01;
        C19990v2 r3 = this.A04;
        if (r3.A06(r4) != null) {
            int A00 = C34361g1.A00(this.A00.A04(r4, false), r6.A00);
            if (A00 != 0) {
                if (A00 == 1 || A00 == 2) {
                    A09(r6);
                } else if (A00 != 3) {
                    StringBuilder sb = new StringBuilder("mark-chat-as-read-handler/applyMutation RangeEnclosedState is not recognized or not used = ");
                    sb.append(A00);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            if (r6.A02 && (A06 = r3.A06(r4)) != null && ((long) A06.A06) == -1) {
                A09(r6);
            }
            A05(r6);
            return;
        }
        A06(r6);
    }

    public final void A09(C34521gH r5) {
        boolean z = r5.A02;
        C250417w r2 = this.A01;
        AbstractC14640lm r1 = r5.A01;
        if (z) {
            r2.A02(r1, false, true);
        } else {
            r2.A01(r1, false);
        }
    }
}
