package X;

import android.view.View;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.3b7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70333b7 implements AbstractC41521tf {
    public final /* synthetic */ MediaViewFragment A00;
    public final /* synthetic */ PhotoView A01;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70333b7(MediaViewFragment mediaViewFragment, PhotoView photoView) {
        this.A00 = mediaViewFragment;
        this.A01 = photoView;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.A1S.A03(this.A01.getContext());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
        if (r1 != 43) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0048, code lost:
        if (r12 != null) goto L_0x004b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC41521tf
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Adg(android.graphics.Bitmap r12, android.view.View r13, X.AbstractC15340mz r14) {
        /*
            r11 = this;
            com.whatsapp.mediaview.MediaViewFragment r4 = r11.A00
            android.content.Context r0 = r4.A0p()
            if (r0 == 0) goto L_0x0071
            X.0oV r14 = (X.AbstractC16130oV) r14
            r8 = 0
            r7 = 43
            r6 = 42
            r9 = 13
            r5 = 9
            r2 = 3
            r3 = 1
            if (r12 == 0) goto L_0x008d
            com.whatsapp.mediaview.PhotoView r10 = r11.A01
            android.content.res.Resources r1 = r4.A02()
            X.2ZM r0 = new X.2ZM
            r0.<init>(r1, r12, r14)
        L_0x0022:
            r10.A06(r0)
        L_0x0025:
            byte r1 = r14.A0y
            if (r1 == r3) goto L_0x0083
            if (r1 == r2) goto L_0x0077
            if (r1 == r5) goto L_0x0072
            if (r1 == r9) goto L_0x0077
            r0 = 28
            if (r1 == r0) goto L_0x0077
            r0 = 29
            if (r1 == r0) goto L_0x0077
            if (r1 == r6) goto L_0x0083
            if (r1 == r7) goto L_0x0077
        L_0x003b:
            X.0oX r1 = X.AbstractC15340mz.A00(r14)
            int r0 = r1.A08
            if (r0 == 0) goto L_0x004a
            int r0 = r1.A06
            if (r0 == 0) goto L_0x004a
            r2 = 1
            if (r12 != 0) goto L_0x004b
        L_0x004a:
            r2 = 0
        L_0x004b:
            boolean r0 = X.AbstractC454421p.A00
            if (r0 == 0) goto L_0x0068
            X.0oV r0 = r4.A1I
            if (r0 == 0) goto L_0x0068
            X.1IS r1 = r14.A0z
            X.1IS r0 = r0.A0z
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0068
            if (r2 != 0) goto L_0x0063
            com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0 r0 = r4.A0C
            if (r0 != 0) goto L_0x0068
        L_0x0063:
            r4.A1X = r3
            com.whatsapp.mediaview.MediaViewFragment.A04(r4)
        L_0x0068:
            com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0 r1 = r4.A0C
            if (r1 == 0) goto L_0x0071
            com.whatsapp.mediaview.PhotoView r0 = r11.A01
            r1.A00(r0, r14)
        L_0x0071:
            return
        L_0x0072:
            com.whatsapp.mediaview.PhotoView r0 = r11.A01
            r0.A0M = r8
            goto L_0x003b
        L_0x0077:
            com.whatsapp.mediaview.PhotoView r2 = r11.A01
            r1 = 20
            com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1 r0 = new com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1
            r0.<init>(r11, r1, r14)
            r2.A0M = r0
            goto L_0x003b
        L_0x0083:
            com.whatsapp.mediaview.PhotoView r1 = r11.A01
            X.45S r0 = new X.45S
            r0.<init>(r11, r14)
            r1.A0M = r0
            goto L_0x003b
        L_0x008d:
            byte r0 = r14.A0y
            if (r0 == r3) goto L_0x00ac
            if (r0 == r2) goto L_0x00a2
            if (r0 == r5) goto L_0x00b6
            if (r0 == r9) goto L_0x00a2
            if (r0 == r6) goto L_0x009c
            if (r0 == r7) goto L_0x009c
            goto L_0x0025
        L_0x009c:
            com.whatsapp.mediaview.PhotoView r0 = r11.A01
            r0.A06(r8)
            goto L_0x0025
        L_0x00a2:
            com.whatsapp.mediaview.PhotoView r10 = r11.A01
            android.content.Context r1 = r4.A01()
            r0 = 2131231112(0x7f080188, float:1.8078296E38)
            goto L_0x00bf
        L_0x00ac:
            com.whatsapp.mediaview.PhotoView r10 = r11.A01
            android.content.Context r1 = r4.A01()
            r0 = 2131231110(0x7f080186, float:1.8078292E38)
            goto L_0x00bf
        L_0x00b6:
            com.whatsapp.mediaview.PhotoView r10 = r11.A01
            android.content.Context r1 = r4.A01()
            r0 = 2131232265(0x7f080609, float:1.8080634E38)
        L_0x00bf:
            android.graphics.drawable.Drawable r0 = X.AnonymousClass00T.A04(r1, r0)
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C70333b7.Adg(android.graphics.Bitmap, android.view.View, X.0mz):void");
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        PhotoView photoView = this.A01;
        photoView.A0J = null;
        photoView.A04 = 0.0f;
    }
}
