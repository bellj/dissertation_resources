package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.12d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C235612d {
    public final AnonymousClass1KZ A00(List list) {
        C37471mS[] r15;
        ArrayList arrayList = new ArrayList(C16760pi.A0D(list));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C65943Lr r3 = (C65943Lr) it.next();
            C16700pc.A0E(r3, 0);
            AnonymousClass1KS r1 = new AnonymousClass1KS();
            r1.A03 = r3.A02;
            r1.A02 = r3.A01;
            r1.A0F = r3.A06;
            r1.A00 = r3.A00;
            r1.A0B = r3.A05;
            r1.A06 = r3.A03;
            r1.A0C = r3.A04;
            arrayList.add(r1);
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            AnonymousClass1KS r6 = (AnonymousClass1KS) it2.next();
            r6.A0E = "meta-avatar";
            r6.A0G = true;
            String str = r6.A06;
            if (str == null) {
                r15 = null;
            } else {
                if (!str.contains(" ")) {
                    r15 = new C37471mS[]{new C37471mS(str)};
                } else {
                    String[] split = TextUtils.split(str, " ");
                    int length = split.length;
                    r15 = new C37471mS[length];
                    for (int i = 0; i < length; i++) {
                        r15[i] = new C37471mS(split[i]);
                    }
                }
            }
            r6.A04 = new AnonymousClass1KB("meta-avatar", "Avatar", "Meta", null, null, r15, true, false, true);
        }
        C37451mQ r4 = new C37451mQ();
        r4.A0B = "meta-avatar";
        r4.A0D = "Avatar";
        r4.A0F = "Meta";
        r4.A02 = "Get your own sticker pack that represents who you are.";
        r4.A0C = "-1";
        r4.A0G = "-1";
        r4.A0H = "-1";
        int i2 = 0;
        r4.A0K = false;
        r4.A0L = true;
        Iterator it3 = arrayList.iterator();
        while (it3.hasNext()) {
            i2 += ((AnonymousClass1KS) it3.next()).A00;
        }
        r4.A01 = (long) i2;
        r4.A0J = arrayList;
        r4.A0N = true;
        return r4.A00();
    }
}
