package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* renamed from: X.46z  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass46z extends AbstractC74143hO {
    public ViewGroup A00 = ((ViewGroup) AnonymousClass028.A0D(this, R.id.search_message_attachment_container_content));
    public ViewGroup A01 = ((ViewGroup) AnonymousClass028.A0D(this, R.id.search_message_attachment_container_icon));

    public abstract View A01();

    public abstract View A02();

    public AnonymousClass46z(Context context) {
        super(context);
        FrameLayout.inflate(getContext(), R.layout.search_message_attachment_container, this);
    }

    public void A03() {
        View A02 = A02();
        if (A02 != null) {
            this.A01.addView(A02);
        }
        View A01 = A01();
        if (A01 != null) {
            this.A00.addView(A01);
        }
    }
}
