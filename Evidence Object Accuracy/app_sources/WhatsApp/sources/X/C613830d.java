package X;

/* renamed from: X.30d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C613830d extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Long A02;
    public Long A03;

    public C613830d() {
        super(2588, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A01);
        r3.Abe(4, this.A02);
        r3.Abe(3, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamThirdPartyPackImport {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "animated", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "importSuccess", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerCount", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "totalSizeIn10Kb", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
