package X;

import android.app.SharedElementCallback;
import android.view.View;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.2Ym  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class SharedElementCallbackC51742Ym extends SharedElementCallback {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ActivityC000800j A01;

    public SharedElementCallbackC51742Ym(View view, ActivityC000800j r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.app.SharedElementCallback
    public void onMapSharedElements(List list, Map map) {
        View A06;
        super.onMapSharedElements(list, map);
        ActivityC000800j r3 = this.A01;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (!map.containsKey(A0x) && (A06 = AbstractC454421p.A06(C12970iu.A0G(r3), A0x)) != null) {
                map.put(A0x, A06);
            }
        }
    }

    @Override // android.app.SharedElementCallback
    public void onSharedElementEnd(List list, List list2, List list3) {
        super.onSharedElementEnd(list, list2, list3);
        ActivityC000800j r2 = this.A01;
        r2.setExitSharedElementCallback(null);
        AnonymousClass028.A0k(this.A00, null);
        View findViewById = r2.findViewById(16908335);
        if (findViewById != null) {
            AnonymousClass028.A0k(findViewById, null);
        }
    }
}
