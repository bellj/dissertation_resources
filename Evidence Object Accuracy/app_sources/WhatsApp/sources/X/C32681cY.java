package X;

/* renamed from: X.1cY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32681cY extends AbstractC32691cZ {
    public int A00;
    public final int A01;
    public final CharSequence A02;

    public C32681cY(CharSequence charSequence) {
        this.A02 = charSequence;
        this.A01 = charSequence.length();
    }

    public int A01(int i, long j) {
        if (j != -1) {
            return A02(i, j);
        }
        return Character.charCount(Character.codePointAt(this.A02, i));
    }

    public int A02(int i, long j) {
        int codePointAt;
        int i2 = (int) (j & 15);
        int i3 = 0;
        int i4 = 0;
        while (i3 < i2) {
            int codePointAt2 = Character.codePointAt(this.A02, i + i4);
            i4 += Character.charCount(codePointAt2);
            if (codePointAt2 == 65039) {
                i3--;
            }
            i3++;
        }
        int i5 = i + i4;
        return (i5 >= this.A01 || (codePointAt = Character.codePointAt(this.A02, i5)) != 65039) ? i4 : i4 + Character.charCount(codePointAt);
    }
}
