package X;

/* renamed from: X.3Da  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63813Da {
    public final C89004Ih A00 = new C89004Ih(AnonymousClass3HZ.A0E);

    public static void A00(StringBuilder sb, int i) {
        sb.append(AnonymousClass4Z9.A00(i));
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014 A[Catch: 3vo | 3vq -> 0x0075, TryCatch #0 {3vo | 3vq -> 0x0075, blocks: (B:8:0x0010, B:10:0x0014, B:15:0x0029, B:17:0x002f, B:18:0x0032, B:19:0x0035, B:20:0x0038, B:21:0x0047, B:23:0x004d, B:24:0x0050, B:26:0x0054, B:28:0x005e, B:29:0x0064, B:31:0x0069), top: B:36:0x0010 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004d A[Catch: 3vo | 3vq -> 0x0075, TryCatch #0 {3vo | 3vq -> 0x0075, blocks: (B:8:0x0010, B:10:0x0014, B:15:0x0029, B:17:0x002f, B:18:0x0032, B:19:0x0035, B:20:0x0038, B:21:0x0047, B:23:0x004d, B:24:0x0050, B:26:0x0054, B:28:0x005e, B:29:0x0064, B:31:0x0069), top: B:36:0x0010 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0069 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass4TS A01(X.C71183cW r10, java.util.Map r11) {
        /*
            r9 = this;
            X.3FD r6 = new X.3FD
            r6.<init>(r10)
            r8 = 0
            X.4TS r0 = r9.A02(r6, r11)     // Catch: 3vq -> 0x000d, 3vo -> 0x000b
            return r0
        L_0x000b:
            r7 = move-exception
            goto L_0x0010
        L_0x000d:
            r0 = move-exception
            r7 = r8
            r8 = r0
        L_0x0010:
            X.3IQ r0 = r6.A00     // Catch: 3vq | 3vo -> 0x0075
            if (r0 == 0) goto L_0x0038
            X.4BN[] r1 = X.AnonymousClass4BN.values()     // Catch: 3vq | 3vo -> 0x0075
            X.3IQ r0 = r6.A00     // Catch: 3vq | 3vo -> 0x0075
            byte r0 = r0.A00     // Catch: 3vq | 3vo -> 0x0075
            r5 = r1[r0]     // Catch: 3vq | 3vo -> 0x0075
            X.3cW r4 = r6.A03     // Catch: 3vq | 3vo -> 0x0075
            int r3 = r4.A00     // Catch: 3vq | 3vo -> 0x0075
            r2 = 0
        L_0x0023:
            r1 = 0
            if (r2 < r3) goto L_0x0027
            goto L_0x0038
        L_0x0027:
            if (r1 >= r3) goto L_0x0035
            boolean r0 = r5.A00(r2, r1)     // Catch: 3vq | 3vo -> 0x0075
            if (r0 == 0) goto L_0x0032
            r4.A00(r1, r2)     // Catch: 3vq | 3vo -> 0x0075
        L_0x0032:
            int r1 = r1 + 1
            goto L_0x0027
        L_0x0035:
            int r2 = r2 + 1
            goto L_0x0023
        L_0x0038:
            r1 = 1
            r0 = 0
            r6.A01 = r0     // Catch: 3vq | 3vo -> 0x0075
            r6.A00 = r0     // Catch: 3vq | 3vo -> 0x0075
            r6.A02 = r1     // Catch: 3vq | 3vo -> 0x0075
            r6.A02()     // Catch: 3vq | 3vo -> 0x0075
            r6.A01()     // Catch: 3vq | 3vo -> 0x0075
            r5 = 0
        L_0x0047:
            X.3cW r4 = r6.A03     // Catch: 3vq | 3vo -> 0x0075
            int r0 = r4.A02     // Catch: 3vq | 3vo -> 0x0075
            if (r5 >= r0) goto L_0x0069
            int r3 = r5 + 1
            r2 = r3
        L_0x0050:
            int r0 = r4.A00     // Catch: 3vq | 3vo -> 0x0075
            if (r2 >= r0) goto L_0x0067
            boolean r1 = r4.A03(r5, r2)     // Catch: 3vq | 3vo -> 0x0075
            boolean r0 = r4.A03(r2, r5)     // Catch: 3vq | 3vo -> 0x0075
            if (r1 == r0) goto L_0x0064
            r4.A00(r2, r5)     // Catch: 3vq | 3vo -> 0x0075
            r4.A00(r5, r2)     // Catch: 3vq | 3vo -> 0x0075
        L_0x0064:
            int r2 = r2 + 1
            goto L_0x0050
        L_0x0067:
            r5 = r3
            goto L_0x0047
        L_0x0069:
            X.4TS r1 = r9.A02(r6, r11)     // Catch: 3vq | 3vo -> 0x0075
            X.4Ii r0 = new X.4Ii     // Catch: 3vq | 3vo -> 0x0075
            r0.<init>()     // Catch: 3vq | 3vo -> 0x0075
            r1.A00 = r0     // Catch: 3vq | 3vo -> 0x0075
            return r1
        L_0x0075:
            if (r8 == 0) goto L_0x0078
            throw r8
        L_0x0078:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C63813Da.A01(X.3cW, java.util.Map):X.4TS");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x0448, code lost:
        if (r8[2] != -65) goto L_0x044a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass4TS A02(X.AnonymousClass3FD r37, java.util.Map r38) {
        /*
        // Method dump skipped, instructions count: 1944
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C63813Da.A02(X.3FD, java.util.Map):X.4TS");
    }
}
