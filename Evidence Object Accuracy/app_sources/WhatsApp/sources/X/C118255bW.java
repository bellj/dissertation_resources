package X;

/* renamed from: X.5bW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118255bW extends AnonymousClass0Yo {
    public final /* synthetic */ C128375w0 A00;

    public C118255bW(C128375w0 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123365n2.class)) {
            C128375w0 r0 = this.A00;
            C14850m9 r2 = r0.A0J;
            return new C123365n2(r0.A0B, r2, r0.A0d, r0.A0f, r0.A0h);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviPayHubSecurityViewModel");
    }
}
