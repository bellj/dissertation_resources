package X;

/* renamed from: X.5Fz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113095Fz implements AnonymousClass20J {
    public final AnonymousClass5GB A00;

    public C113095Fz(AnonymousClass5GB r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass20J
    public int AE2() {
        return 16;
    }

    @Override // X.AnonymousClass20J
    public void update(byte[] bArr, int i, int i2) {
        this.A00.AZX(bArr, i, i2);
    }

    @Override // X.AnonymousClass20J
    public int A97(byte[] bArr, int i) {
        try {
            return this.A00.A97(bArr, i);
        } catch (C114965Nt e) {
            throw C12960it.A0U(e.toString());
        }
    }

    @Override // X.AnonymousClass20J
    public void AIc(AnonymousClass20L r8) {
        if (r8 instanceof C113075Fx) {
            C113075Fx r82 = (C113075Fx) r8;
            this.A00.AIf(new C113035Ft((AnonymousClass20K) r82.A00, r82.A01, null, 128), true);
            return;
        }
        throw C12970iu.A0f("GMAC requires ParametersWithIV");
    }

    @Override // X.AnonymousClass20J
    public void AfG(byte b) {
        AnonymousClass5GB r4 = this.A00;
        r4.A06();
        byte[] bArr = r4.A0H;
        int i = r4.A00;
        bArr[i] = b;
        int i2 = i + 1;
        r4.A00 = i2;
        if (i2 == 16) {
            byte[] bArr2 = r4.A0F;
            AnonymousClass4F4.A00(bArr2, bArr);
            r4.A09.A00(bArr2);
            r4.A00 = 0;
            r4.A04 += 16;
        }
    }

    @Override // X.AnonymousClass20J
    public void reset() {
        this.A00.A08(true);
    }
}
