package X;

import android.text.style.ForegroundColorSpan;

/* renamed from: X.3gP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C73543gP extends ForegroundColorSpan implements AbstractC115425Rm {
    public final C73533gO A00;
    public final String A01;

    public C73543gP(C73533gO r1, String str, int i) {
        super(i);
        this.A01 = str;
        this.A00 = r1;
    }
}
