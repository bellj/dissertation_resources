package X;

import android.util.SparseIntArray;

/* renamed from: X.3ki  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75923ki extends AbstractC105924uq implements AbstractC12890ij {
    public final int[] A00;

    public C75923ki(AbstractC11580gW r5, C93604aR r6, AbstractC115035Pl r7) {
        super(r5, r6, r7);
        SparseIntArray sparseIntArray = r6.A03;
        this.A00 = new int[sparseIntArray.size()];
        for (int i = 0; i < sparseIntArray.size(); i++) {
            this.A00[i] = sparseIntArray.keyAt(i);
        }
    }
}
