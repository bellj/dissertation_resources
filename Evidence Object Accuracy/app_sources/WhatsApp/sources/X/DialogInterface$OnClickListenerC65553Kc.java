package X;

import android.content.DialogInterface;
import android.content.Intent;
import com.whatsapp.account.delete.DeleteAccountFeedback;

/* renamed from: X.3Kc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class DialogInterface$OnClickListenerC65553Kc implements DialogInterface.OnClickListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ DeleteAccountFeedback.ChangeNumberMessageDialogFragment A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ DialogInterface$OnClickListenerC65553Kc(DeleteAccountFeedback.ChangeNumberMessageDialogFragment changeNumberMessageDialogFragment, String str, int i) {
        this.A01 = changeNumberMessageDialogFragment;
        this.A00 = i;
        this.A02 = str;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        DeleteAccountFeedback.ChangeNumberMessageDialogFragment changeNumberMessageDialogFragment = this.A01;
        int i2 = this.A00;
        String str = this.A02;
        ActivityC000900k A0B = changeNumberMessageDialogFragment.A0B();
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(A0B.getPackageName(), "com.whatsapp.account.delete.DeleteAccountConfirmation");
        A0A.putExtra("deleteReason", i2);
        A0A.putExtra("additionalComments", str);
        changeNumberMessageDialogFragment.A0v(A0A);
    }
}
