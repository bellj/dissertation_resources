package X;

/* renamed from: X.4WG  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4WG {
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AnonymousClass4WG.class == obj.getClass() && Float.compare(0.0f, 0.0f) == 0) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return 16337 + Float.floatToIntBits(0.0f);
    }
}
