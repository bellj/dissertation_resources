package X;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.util.ViewOnClickCListenerShape2S0101000_I1;
import java.util.Map;

/* renamed from: X.2gU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54292gU extends AnonymousClass02M {
    public final Map A00 = C12970iu.A11();
    public final /* synthetic */ CatalogCarouselDetailImageView A01;

    public /* synthetic */ C54292gU(CatalogCarouselDetailImageView catalogCarouselDetailImageView) {
        this.A01 = catalogCarouselDetailImageView;
    }

    @Override // X.AnonymousClass02M
    public void A0A(AnonymousClass03U r3) {
        ThumbnailButton thumbnailButton = ((C55142hr) r3).A01;
        thumbnailButton.setImageBitmap(null);
        thumbnailButton.setOnClickListener(null);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.A02.A06.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r17, int i) {
        ThumbnailButton thumbnailButton;
        C55142hr r4 = (C55142hr) r17;
        Map map = this.A00;
        CatalogCarouselDetailImageView catalogCarouselDetailImageView = r4.A02;
        if (!catalogCarouselDetailImageView.A02.A06.isEmpty()) {
            C44691zO r2 = catalogCarouselDetailImageView.A02;
            if (!r2.A02()) {
                boolean z = true;
                if (r2.A06.size() <= 1) {
                    z = false;
                }
                if (catalogCarouselDetailImageView.A02.A06.get(i) != null) {
                    C44741zT r12 = (C44741zT) catalogCarouselDetailImageView.A02.A06.get(i);
                    int i2 = r12.A03;
                    int i3 = r12.A02;
                    if (!(i2 == 0 || i3 == 0)) {
                        r4.A08(i2, i3, z);
                    }
                    String A00 = AnonymousClass19N.A00(i, catalogCarouselDetailImageView.A02.A0D);
                    thumbnailButton = r4.A01;
                    if (!A00.equals(thumbnailButton.getTag())) {
                        thumbnailButton.setImageResource(R.color.light_gray);
                    }
                    thumbnailButton.setTag(A00);
                    catalogCarouselDetailImageView.A04.A02(thumbnailButton, r12, null, new AnonymousClass2E5(A00, map, i2, i3, z) { // from class: X.3VC
                        public final /* synthetic */ int A00;
                        public final /* synthetic */ int A01;
                        public final /* synthetic */ String A03;
                        public final /* synthetic */ Map A04;
                        public final /* synthetic */ boolean A05;

                        {
                            this.A04 = r3;
                            this.A03 = r2;
                            this.A05 = r6;
                            this.A00 = r4;
                            this.A01 = r5;
                        }

                        @Override // X.AnonymousClass2E5
                        public final void AS6(Bitmap bitmap, C68203Um r19, boolean z2) {
                            C55142hr r22 = C55142hr.this;
                            Map map2 = this.A04;
                            String str = this.A03;
                            boolean z3 = this.A05;
                            int i4 = this.A00;
                            int i5 = this.A01;
                            ThumbnailButton thumbnailButton2 = r22.A01;
                            if (thumbnailButton2.getTag().equals(str)) {
                                if (i4 == 0 || i5 == 0) {
                                    i4 = bitmap.getWidth();
                                    i5 = bitmap.getHeight();
                                    r22.A08(i4, i5, z3);
                                }
                                boolean A1Y = C12990iw.A1Y(i4, i5);
                                if (i4 == i5 || (z3 && A1Y)) {
                                    thumbnailButton2.setImageBitmap(bitmap);
                                    return;
                                }
                                CatalogCarouselDetailImageView catalogCarouselDetailImageView2 = r22.A02;
                                int A002 = AnonymousClass00T.A00(catalogCarouselDetailImageView2.getContext(), R.color.catalog_detail_image_default_background_color);
                                C90144Mt r122 = (C90144Mt) map2.get(str);
                                if (r122 != null) {
                                    catalogCarouselDetailImageView2.setImageAndGradient(r122, A1Y, thumbnailButton2, bitmap, r22.A00);
                                    return;
                                }
                                C12990iw.A1N(new AnonymousClass380(bitmap, r22.A00, catalogCarouselDetailImageView2, thumbnailButton2, str, map2, A002), catalogCarouselDetailImageView2.A09);
                            }
                        }
                    }, 1);
                } else {
                    thumbnailButton = r4.A01;
                    thumbnailButton.setImageResource(R.color.light_gray);
                }
                if (catalogCarouselDetailImageView.A0C) {
                    int i4 = 2;
                    if (!z) {
                        i4 = 1;
                    }
                    thumbnailButton.setOnClickListener(new ViewOnClickCListenerShape2S0101000_I1(r4, i, i4));
                    return;
                }
                return;
            }
        }
        AnonymousClass4Dz.A00(r4.A01);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        CatalogCarouselDetailImageView catalogCarouselDetailImageView = this.A01;
        return new C55142hr(C12960it.A0F(C12960it.A0E(catalogCarouselDetailImageView), viewGroup, R.layout.product_catalog_detail_image), catalogCarouselDetailImageView);
    }
}
