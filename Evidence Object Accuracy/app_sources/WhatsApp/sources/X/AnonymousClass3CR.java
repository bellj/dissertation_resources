package X;

import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3CR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CR {
    public final Map A00 = C12970iu.A11();

    public final String A00() {
        StringBuilder A0k = C12960it.A0k("{\"server_params\":{");
        Map map = this.A00;
        Iterator A0n = C12960it.A0n(map);
        int i = 0;
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            String A0r = C12990iw.A0r(A15);
            A0k.append("\"");
            A0k.append(A0r);
            C12960it.A1L("\":\"", (String) A15.getValue(), "\"", A0k);
            if (i < map.size() - 1) {
                A0k.append(",");
            }
            i++;
        }
        String A0d = C12960it.A0d("}}", A0k);
        C16700pc.A0B(A0d);
        return A0d;
    }
}
