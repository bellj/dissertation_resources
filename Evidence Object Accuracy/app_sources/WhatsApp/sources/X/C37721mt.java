package X;

/* renamed from: X.1mt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37721mt implements AbstractC37731mu {
    public int A00;
    public final long A01;
    public final C28481Nj A02;

    public C37721mt(String str, long j) {
        this.A02 = new C28481Nj(str, str, "", null, null, 0, false);
        this.A01 = j;
    }

    @Override // X.AbstractC37731mu
    public long AAo() {
        return this.A01;
    }

    @Override // X.AbstractC37731mu
    public C28481Nj ACB() {
        if (this.A00 < 4) {
            return this.A02;
        }
        return null;
    }

    @Override // X.AbstractC37731mu
    public void APu(boolean z, int i) {
        this.A00++;
    }
}
