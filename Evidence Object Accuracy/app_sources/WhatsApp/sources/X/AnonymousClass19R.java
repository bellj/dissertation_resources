package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

/* renamed from: X.19R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19R {
    public final AbstractC15710nm A00;
    public final C14850m9 A01;

    public AnonymousClass19R(AbstractC15710nm r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final Matcher A00(String str) {
        if (!TextUtils.isEmpty(str)) {
            String A03 = this.A01.A03(265);
            if (!TextUtils.isEmpty(A03)) {
                try {
                    AnonymousClass009.A05(A03);
                    Pattern compile = Pattern.compile(new JSONObject(A03).getJSONArray("url").getJSONObject(0).getString("regex"));
                    AnonymousClass009.A05(str);
                    return compile.matcher(str);
                } catch (Exception e) {
                    Log.e(e);
                    throw new AnonymousClass4CC(e);
                }
            } else {
                throw new AnonymousClass4CC("SHOPS_STOREFRONT_URLS_CONFIG_CODE was null/empty");
            }
        } else {
            throw new AnonymousClass4CC("Shop url was null");
        }
    }

    public boolean A01(String str) {
        try {
            return A00(str).matches();
        } catch (AnonymousClass4CC e) {
            Log.e(e);
            this.A00.AaV("ShopUtils/isShopUrl", str, true);
            return false;
        }
    }
}
