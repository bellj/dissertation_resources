package X;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.27v  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27v {
    public static final Set A00 = new HashSet(Arrays.asList("audio", "image", "video", "kyc-id", "sticker", "document", "ptt", "gif", "md-app-state", "md-msg-hist", "ppic"));

    public static Set A00(AnonymousClass1V8 r6, Set set) {
        if (r6 == null) {
            return null;
        }
        HashSet hashSet = new HashSet();
        AnonymousClass1V8[] r4 = r6.A03;
        if (r4 != null) {
            for (AnonymousClass1V8 r1 : r4) {
                if (set == null || set.contains(r1.A00)) {
                    hashSet.add(r1.A00);
                }
            }
        }
        return hashSet;
    }
}
