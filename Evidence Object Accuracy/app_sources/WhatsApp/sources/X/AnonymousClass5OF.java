package X;

/* renamed from: X.5OF  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5OF implements AnonymousClass5WS, AbstractC117275Zf {
    public int A00;
    public long A01;
    public final byte[] A02;

    public AnonymousClass5OF() {
        this.A02 = new byte[4];
        this.A00 = 0;
    }

    public AnonymousClass5OF(AnonymousClass5OF r2) {
        this.A02 = new byte[4];
        A0M(r2);
    }

    public void A0M(AnonymousClass5OF r5) {
        byte[] bArr = r5.A02;
        System.arraycopy(bArr, 0, this.A02, 0, bArr.length);
        this.A00 = r5.A00;
        this.A01 = r5.A01;
    }

    @Override // X.AbstractC117275Zf
    public int AB4() {
        return 64;
    }

    @Override // X.AnonymousClass5XI
    public void AfG(byte b) {
        byte[] bArr = this.A02;
        int i = this.A00;
        int i2 = i + 1;
        this.A00 = i2;
        bArr[i] = b;
        if (i2 == bArr.length) {
            A0N(bArr, 0);
            this.A00 = 0;
        }
        this.A01++;
    }

    @Override // X.AnonymousClass5XI
    public void reset() {
        this.A01 = 0;
        this.A00 = 0;
        int i = 0;
        while (true) {
            byte[] bArr = this.A02;
            if (i < bArr.length) {
                bArr[i] = 0;
                i++;
            } else {
                return;
            }
        }
    }

    public static int A08(int i) {
        return (i >>> 22) | (i << 10);
    }

    public static int A09(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 18) | (i4 << 14)) + i3;
    }

    public static int A0A(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 20) | (i4 << 12)) + i3;
    }

    public static int A0B(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 21) | (i4 << 11)) + i3;
    }

    public static int A0C(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 23) | (i4 << 9)) + i3;
    }

    public static int A0D(int i, int i2, int i3, int i4) {
        return i4 + (((i2 ^ -1) & i3) | (i & i2));
    }

    public static int A0E(int i, int i2, int i3, int i4) {
        return i4 + ((i ^ i2) ^ i3);
    }

    public static int A0F(int i, int i2, int i3, int i4) {
        return ((i >>> i2) | i3) + i4;
    }

    public static int A0G(int i, int i2, int i3, int i4, int i5) {
        return i4 + ((i & i2) | (i3 & (i2 ^ -1))) + i5;
    }

    public static int A0H(int i, int i2, int i3, int i4, int i5) {
        return i4 + ((i2 | (i ^ -1)) ^ i3) + i5;
    }

    public static int A0I(byte[] bArr, int[] iArr, int i, int i2) {
        iArr[i2] = ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
        return 16;
    }

    public static int A0J(int[] iArr, int i) {
        int i2 = iArr[i - 2];
        int i3 = ((i2 >>> 10) ^ (((i2 >>> 17) | (i2 << 15)) ^ ((i2 >>> 19) | (i2 << 13)))) + iArr[i - 7];
        int i4 = iArr[i - 15];
        iArr[i] = i3 + ((i4 >>> 3) ^ (((i4 >>> 7) | (i4 << 25)) ^ ((i4 >>> 18) | (i4 << 14)))) + iArr[i - 16];
        return i + 1;
    }

    public void A0K() {
        int i;
        char c;
        int[] iArr;
        int[] iArr2;
        char c2;
        char c3;
        long j = this.A01 << 3;
        byte b = Byte.MIN_VALUE;
        while (true) {
            AfG(b);
            if (this.A00 == 0) {
                break;
            }
            b = 0;
        }
        if (!(this instanceof AnonymousClass5O9)) {
            if (this instanceof AnonymousClass5OE) {
                AnonymousClass5OE r8 = (AnonymousClass5OE) this;
                c = 14;
                if (r8.A08 > 14) {
                    r8.A0L();
                }
                iArr = r8.A09;
            } else if (!(this instanceof AnonymousClass5OD)) {
                if (!(this instanceof AnonymousClass5OB)) {
                    if (!(this instanceof AnonymousClass5OA)) {
                        AnonymousClass5OC r82 = (AnonymousClass5OC) this;
                        c3 = 14;
                        if (r82.A04 > 14) {
                            r82.A0L();
                        }
                        iArr2 = r82.A05;
                    } else {
                        AnonymousClass5OA r83 = (AnonymousClass5OA) this;
                        c3 = 14;
                        if (r83.A05 > 14) {
                            r83.A0L();
                        }
                        iArr2 = r83.A06;
                    }
                    iArr2[c3] = (int) (-1 & j);
                    c2 = 15;
                    j >>>= 32;
                } else {
                    AnonymousClass5OB r84 = (AnonymousClass5OB) this;
                    if (r84.A05 > 14) {
                        r84.A0L();
                    }
                    iArr2 = r84.A06;
                    iArr2[14] = (int) (j >>> 32);
                    c2 = 15;
                }
                iArr2[c2] = (int) j;
            } else {
                AnonymousClass5OD r85 = (AnonymousClass5OD) this;
                c = 14;
                if (r85.A08 > 14) {
                    r85.A0L();
                }
                iArr = r85.A09;
            }
            iArr[c] = (int) (j >>> 32);
            iArr[15] = (int) (j & -1);
        } else {
            AnonymousClass5O9 r86 = (AnonymousClass5O9) this;
            int i2 = r86.A00;
            if (i2 > 14) {
                r86.A03[i2] = 0;
                r86.A00 = i2 + 1;
                r86.A0L();
            }
            while (true) {
                i = r86.A00;
                if (i >= 14) {
                    break;
                }
                r86.A03[i] = 0;
                r86.A00 = i + 1;
            }
            int[] iArr3 = r86.A03;
            int i3 = i + 1;
            r86.A00 = i3;
            iArr3[i] = (int) (j >>> 32);
            r86.A00 = i3 + 1;
            iArr3[i3] = (int) j;
        }
        A0L();
    }

    public void A0L() {
        int[] iArr;
        int[] iArr2;
        int i;
        int i2;
        int i3;
        int i4;
        AnonymousClass5O9 r4 = (AnonymousClass5O9) this;
        int i5 = 0;
        do {
            iArr = r4.A02;
            iArr[i5] = r4.A03[i5];
            i5++;
        } while (i5 < 16);
        int i6 = 16;
        do {
            int i7 = iArr[i6 - 3];
            int i8 = (i7 >>> 17) | (i7 << 15);
            int i9 = iArr[i6 - 13];
            int i10 = i8 ^ (iArr[i6 - 16] ^ iArr[i6 - 9]);
            iArr[i6] = (((i10 ^ ((i10 << 15) | (i10 >>> 17))) ^ ((i10 << 23) | (i10 >>> 9))) ^ ((i9 >>> 25) | (i9 << 7))) ^ iArr[i6 - 6];
            i6++;
        } while (i6 < 68);
        int[] iArr3 = r4.A01;
        int i11 = iArr3[0];
        int i12 = iArr3[1];
        int i13 = iArr3[2];
        int i14 = iArr3[3];
        int i15 = iArr3[4];
        int i16 = iArr3[5];
        int i17 = iArr3[6];
        int i18 = iArr3[7];
        int i19 = 0;
        do {
            int i20 = (i11 << 12) | (i11 >>> 20);
            iArr2 = AnonymousClass5O9.A04;
            int i21 = i20 + i15 + iArr2[i19];
            int i22 = (i21 << 7) | (i21 >>> 25);
            int i23 = iArr[i19];
            i = ((i11 ^ i12) ^ i13) + i14 + (i20 ^ i22) + (iArr[i19 + 4] ^ i23);
            int i24 = ((i15 ^ i16) ^ i17) + i18 + i22 + i23;
            i13 = (i12 << 9) | (i12 >>> 23);
            i17 = (i16 << 19) | (i16 >>> 13);
            i2 = (i24 ^ ((i24 << 9) | (i24 >>> 23))) ^ ((i24 << 17) | (i24 >>> 15));
            i19++;
            i16 = i15;
            i15 = i2;
            i14 = i13;
            i18 = i17;
            i12 = i11;
            i11 = i;
        } while (i19 < 16);
        int i25 = 16;
        do {
            int i26 = (i << 12) | (i >>> 20);
            int i27 = i26 + i2 + iArr2[i25];
            int i28 = (i27 << 7) | (i27 >>> 25);
            int i29 = i28 ^ i26;
            int i30 = iArr[i25];
            i = ((i & i13) | (i & i12) | (i12 & i13)) + i14 + i29 + (i30 ^ iArr[i25 + 4]);
            int i31 = (((i2 ^ -1) & i17) | (i16 & i2)) + i18 + i28 + i30;
            i3 = (i12 >>> 23) | (i12 << 9);
            i4 = (i16 << 19) | (i16 >>> 13);
            i2 = (i31 ^ ((i31 << 9) | (i31 >>> 23))) ^ ((i31 << 17) | (i31 >>> 15));
            i25++;
            i16 = i15;
            i15 = i2;
            i14 = i13;
            i13 = i3;
            i12 = i11;
            i11 = i;
            i18 = i17;
            i17 = i4;
        } while (i25 < 64);
        iArr3[0] = i ^ i11;
        iArr3[1] = iArr3[1] ^ i12;
        iArr3[2] = iArr3[2] ^ i3;
        iArr3[3] = iArr3[3] ^ i14;
        iArr3[4] = iArr3[4] ^ i2;
        iArr3[5] = iArr3[5] ^ i16;
        iArr3[6] = i4 ^ iArr3[6];
        iArr3[7] = iArr3[7] ^ i18;
        r4.A00 = 0;
    }

    public void A0N(byte[] bArr, int i) {
        if (this instanceof AnonymousClass5O9) {
            AnonymousClass5O9 r4 = (AnonymousClass5O9) this;
            int i2 = i + 1;
            int A0P = C72453ed.A0P(bArr, (bArr[i] & 255) << 24, (bArr[i2] & 255) << 16, i2);
            int[] iArr = r4.A03;
            int i3 = r4.A00;
            iArr[i3] = A0P;
            int i4 = i3 + 1;
            r4.A00 = i4;
            if (i4 >= 16) {
                r4.A0L();
            }
        } else if (this instanceof AnonymousClass5OE) {
            AnonymousClass5OE r42 = (AnonymousClass5OE) this;
            int i5 = i + 1;
            int A0P2 = C72453ed.A0P(bArr, bArr[i] << 24, (bArr[i5] & 255) << 16, i5);
            int[] iArr2 = r42.A09;
            int i6 = r42.A08;
            iArr2[i6] = A0P2;
            int i7 = i6 + 1;
            r42.A08 = i7;
            if (i7 == 16) {
                r42.A0L();
            }
        } else if (this instanceof AnonymousClass5OD) {
            AnonymousClass5OD r43 = (AnonymousClass5OD) this;
            int i8 = i + 1;
            int A0P3 = C72453ed.A0P(bArr, bArr[i] << 24, (bArr[i8] & 255) << 16, i8);
            int[] iArr3 = r43.A09;
            int i9 = r43.A08;
            iArr3[i9] = A0P3;
            int i10 = i9 + 1;
            r43.A08 = i10;
            if (i10 == 16) {
                r43.A0L();
            }
        } else if (this instanceof AnonymousClass5OB) {
            AnonymousClass5OB r44 = (AnonymousClass5OB) this;
            int i11 = i + 1;
            int A0P4 = C72453ed.A0P(bArr, bArr[i] << 24, (bArr[i11] & 255) << 16, i11);
            int[] iArr4 = r44.A06;
            int i12 = r44.A05;
            iArr4[i12] = A0P4;
            int i13 = i12 + 1;
            r44.A05 = i13;
            if (i13 == 16) {
                r44.A0L();
            }
        } else if (!(this instanceof AnonymousClass5OA)) {
            AnonymousClass5OC r3 = (AnonymousClass5OC) this;
            int[] iArr5 = r3.A05;
            int i14 = r3.A04;
            int i15 = i14 + 1;
            r3.A04 = i15;
            if (i15 == A0I(bArr, iArr5, i, i14)) {
                r3.A0L();
            }
        } else {
            AnonymousClass5OA r32 = (AnonymousClass5OA) this;
            int[] iArr6 = r32.A06;
            int i16 = r32.A05;
            int i17 = i16 + 1;
            r32.A05 = i17;
            if (i17 == A0I(bArr, iArr6, i, i16)) {
                r32.A0L();
            }
        }
    }

    @Override // X.AnonymousClass5XI
    public void update(byte[] bArr, int i, int i2) {
        int i3 = 0;
        int max = Math.max(0, i2);
        int i4 = this.A00;
        int i5 = 0;
        if (i4 != 0) {
            while (true) {
                if (i5 >= max) {
                    i3 = i5;
                    break;
                }
                byte[] bArr2 = this.A02;
                i4++;
                this.A00 = i4;
                int i6 = i5 + 1;
                C72463ee.A0X(bArr, bArr2, i5 + i, i4);
                if (i4 == 4) {
                    A0N(bArr2, 0);
                    this.A00 = 0;
                    i4 = 0;
                    i3 = i6;
                    break;
                }
                i5 = i6;
            }
        }
        int i7 = ((max - i3) & -4) + i3;
        while (i3 < i7) {
            A0N(bArr, i + i3);
            i3 += 4;
        }
        while (i3 < max) {
            byte[] bArr3 = this.A02;
            i4++;
            this.A00 = i4;
            C72463ee.A0X(bArr, bArr3, i3 + i, i4);
            i3++;
        }
        this.A01 += (long) max;
    }
}
