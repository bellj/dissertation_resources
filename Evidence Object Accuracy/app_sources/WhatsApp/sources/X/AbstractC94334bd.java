package X;

import android.graphics.BitmapFactory;
import android.os.Build;
import com.facebook.imagepipeline.platform.PreverificationHelper;
import java.nio.ByteBuffer;

/* renamed from: X.4bd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94334bd {
    public static final byte[] A03 = {-1, -39};
    public final AnonymousClass0DQ A00;
    public final AnonymousClass5YZ A01;
    public final PreverificationHelper A02;

    public abstract int A00(BitmapFactory.Options options, int i, int i2);

    public AbstractC94334bd(AnonymousClass0DQ r4, AnonymousClass5YZ r5, int i) {
        PreverificationHelper preverificationHelper;
        if (Build.VERSION.SDK_INT >= 26) {
            preverificationHelper = new PreverificationHelper();
        } else {
            preverificationHelper = null;
        }
        this.A02 = preverificationHelper;
        this.A01 = r5;
        this.A00 = r4;
        for (int i2 = 0; i2 < i; i2++) {
            this.A00.Aa6(ByteBuffer.allocate(16384));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0095, code lost:
        if (r4 == false) goto L_0x0097;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C08870bz A01(android.graphics.Bitmap.Config r13, X.AnonymousClass5B9 r14, int r15) {
        /*
        // Method dump skipped, instructions count: 310
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC94334bd.A01(android.graphics.Bitmap$Config, X.5B9, int):X.0bz");
    }
}
