package X;

import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.concurrent.ExecutionException;

/* renamed from: X.1oL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38581oL extends AbstractC38591oM implements AbstractC28771Oy, AbstractC38621oP {
    public final AbstractC15710nm A00;
    public final C002701f A01;
    public final C22370yy A02;
    public final AnonymousClass1KS A03;
    public final AnonymousClass1VC A04 = new AnonymousClass1VC();
    public final File A05;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public C38581oL(AbstractC15710nm r10, C14900mE r11, C002701f r12, C15450nH r13, C18790t3 r14, C14950mJ r15, C14850m9 r16, C20110vE r17, C22370yy r18, C22600zL r19, AnonymousClass1KS r20, File file) {
        super(r13, r14, r15, r16, r17, r19, r11.A06);
        this.A03 = r20;
        this.A00 = r10;
        this.A05 = file;
        this.A02 = r18;
        this.A01 = r12;
    }

    @Override // X.AbstractC38621oP
    public AnonymousClass1RN A9B() {
        C22370yy r1 = this.A02;
        C1108757f r2 = new C1108757f(this);
        AnonymousClass1KS r0 = this.A03;
        if (r1.A0D(r2, this, null, null, r0.A0C, false)) {
            try {
                return (AnonymousClass1RN) this.A04.get();
            } catch (InterruptedException | ExecutionException e) {
                Log.e("DuplicateStickerDownloadListener/waitForResult ", e);
                return new AnonymousClass1RN(1);
            }
        } else {
            A5g(this);
            AnonymousClass1RN r22 = A01().A00;
            if (r22.A00 == 0) {
                File file = this.A05;
                AnonymousClass1KB A00 = AnonymousClass1KB.A00(WebpUtils.fetchWebpMetadata(file.getAbsolutePath()));
                if (!(A00 == null && (A00 = r0.A04) == null)) {
                    WebpUtils.A01(file, new AnonymousClass1KB(A00.A01, A00.A02, A00.A04, A00.A03, A00.A00, A00.A08, A00.A06, A00.A07, true).A01());
                }
                r0.A09 = WebpUtils.A00(file);
            }
            return r22;
        }
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r1, C28781Oz r2) {
    }
}
