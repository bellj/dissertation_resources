package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0oB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15940oB {
    public static C31291aJ A00() {
        C90604On A01 = C32001bS.A00().A01();
        return new C31291aJ(new C32051bX(A01.A00, (byte) 5), new AnonymousClass1PA(A01.A01, (byte) 5));
    }

    public static AnonymousClass1PA A01(byte[] bArr) {
        if (bArr.length >= 33) {
            int i = bArr[0] & 255;
            if (i == 5) {
                byte[] bArr2 = new byte[32];
                System.arraycopy(bArr, 1, bArr2, 0, 32);
                return new AnonymousClass1PA(bArr2, (byte) 5);
            }
            StringBuilder sb = new StringBuilder("Bad key type: ");
            sb.append(i);
            throw new AnonymousClass1RY(sb.toString());
        }
        throw new AnonymousClass1RY("Invalid byte array");
    }

    public static C15950oC A02(DeviceJid deviceJid) {
        AnonymousClass009.A06(deviceJid, "Provided jid must not be null");
        String str = deviceJid.user;
        AnonymousClass009.A06(str, "User part of provided jid must not be null");
        int i = 0;
        if (deviceJid instanceof AnonymousClass1MX) {
            i = 1;
        }
        return new C15950oC(i, str, deviceJid.device);
    }

    public static DeviceJid A03(C15950oC r3) {
        UserJid A02;
        try {
            boolean z = false;
            if (r3.A01 == 0) {
                z = true;
            }
            String str = r3.A02;
            if (z) {
                A02 = C27631Ih.A02(str);
            } else {
                A02 = UserJid.JID_FACTORY.A02(str, "lid");
                if (!(A02 instanceof AnonymousClass1MU)) {
                    StringBuilder sb = new StringBuilder("invalid lid: ");
                    sb.append(str);
                    throw new AnonymousClass1MW(sb.toString());
                }
            }
            return DeviceJid.getFromUserJidAndDeviceId(A02, r3.A00);
        } catch (AnonymousClass1MW unused) {
            StringBuilder sb2 = new StringBuilder("Invalid signal protocol address: ");
            sb2.append(r3);
            Log.e(sb2.toString());
            return null;
        }
    }

    public static List A04(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            DeviceJid A03 = A03((C15950oC) it.next());
            if (A03 != null) {
                arrayList.add(A03);
            }
        }
        return arrayList;
    }

    public static byte[] A05(C32051bX r4, byte[] bArr) {
        if (r4.A00 == 5) {
            C32001bS A00 = C32001bS.A00();
            byte[] bArr2 = r4.A01;
            if (bArr2 == null || bArr2.length != 32) {
                throw new IllegalArgumentException("Invalid private key length!");
            }
            AnonymousClass5XJ r1 = A00.A00;
            return r1.calculateSignature(r1.AG4(64), bArr2, bArr);
        }
        throw new AssertionError("PrivateKey type is invalid");
    }
}
