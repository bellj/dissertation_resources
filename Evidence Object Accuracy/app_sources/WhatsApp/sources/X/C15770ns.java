package X;

import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.0ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15770ns {
    public final AbstractC15710nm A00;
    public final C15750nq A01;
    public final C15730no A02;
    public final C15760nr A03;
    public final C15740np A04;
    public final Set A05 = new HashSet();
    public final Executor A06;
    public final AtomicLong A07 = new AtomicLong();

    public C15770ns(AbstractC15710nm r3, C15750nq r4, C15730no r5, C15760nr r6, C15740np r7, AbstractC14440lR r8) {
        this.A00 = r3;
        this.A01 = r4;
        this.A04 = r7;
        this.A02 = r5;
        this.A03 = r6;
        this.A06 = new ExecutorC27271Gr(r8, false);
    }

    public static Bundle A00() {
        Bundle bundle = new Bundle();
        bundle.putString("iconUri", null);
        return bundle;
    }

    public static Bundle A01() {
        Bundle bundle = new Bundle();
        bundle.putString("name", "WhatsApp");
        return bundle;
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x0076 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A02(android.os.CancellationSignal r6, android.os.ParcelFileDescriptor r7, X.C15770ns r8, java.io.File r9, javax.crypto.Cipher r10) {
        /*
            r0 = 32768(0x8000, float:4.5918E-41)
            byte[] r0 = new byte[r0]     // Catch: all -> 0x0080
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch: OperationCanceledException -> 0x0061, IOException -> 0x0059, all -> 0x0080
            r3.<init>(r9)     // Catch: OperationCanceledException -> 0x0061, IOException -> 0x0059, all -> 0x0080
            if (r10 != 0) goto L_0x000e
            r4 = r3
            goto L_0x0013
        L_0x000e:
            javax.crypto.CipherInputStream r4 = new javax.crypto.CipherInputStream     // Catch: all -> 0x0054
            r4.<init>(r3, r10)     // Catch: all -> 0x0054
        L_0x0013:
            android.os.ParcelFileDescriptor$AutoCloseOutputStream r5 = new android.os.ParcelFileDescriptor$AutoCloseOutputStream     // Catch: all -> 0x004f
            r5.<init>(r7)     // Catch: all -> 0x004f
            X.C15740np.A01(r6, r4, r5, r0)     // Catch: IOException -> 0x001c, all -> 0x004a
            goto L_0x0040
        L_0x001c:
            r6 = move-exception
            java.lang.String r0 = "ExportMigrationApi/Failed while writing to a remote stream "
            com.whatsapp.util.Log.e(r0, r6)     // Catch: all -> 0x004a
            X.0nm r2 = r8.A00     // Catch: all -> 0x004a
            java.lang.String r1 = "xpm-export-api-remote-write"
            java.lang.String r0 = r6.getMessage()     // Catch: all -> 0x004a
            r2.A02(r1, r0, r6)     // Catch: all -> 0x004a
            java.lang.String r2 = "Failed to write data."
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch: IOException -> 0x003a, all -> 0x004a
            r0 = 19
            if (r1 < r0) goto L_0x0040
            r7.closeWithError(r2)     // Catch: IOException -> 0x003a, all -> 0x004a
            goto L_0x0040
        L_0x003a:
            r1 = move-exception
            java.lang.String r0 = "ExportMigrationApi/Failed to close the pipe after an error."
            com.whatsapp.util.Log.e(r0, r1)     // Catch: all -> 0x004a
        L_0x0040:
            r5.close()     // Catch: all -> 0x004f
            r4.close()     // Catch: all -> 0x0054
            r3.close()     // Catch: OperationCanceledException -> 0x0061, IOException -> 0x0059, all -> 0x0080
            goto L_0x0075
        L_0x004a:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x004e
        L_0x004e:
            throw r0     // Catch: all -> 0x004f
        L_0x004f:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0053
        L_0x0053:
            throw r0     // Catch: all -> 0x0054
        L_0x0054:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0058
        L_0x0058:
            throw r0     // Catch: OperationCanceledException -> 0x0061, IOException -> 0x0059, all -> 0x0080
        L_0x0059:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0080
            r1.<init>()     // Catch: all -> 0x0080
            java.lang.String r0 = "ExportMigrationApi/Failed to close stream for "
            goto L_0x0068
        L_0x0061:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0080
            r1.<init>()     // Catch: all -> 0x0080
            java.lang.String r0 = "ExportMigrationApi/Cancelled by remote peer while streaming "
        L_0x0068:
            r1.append(r0)     // Catch: all -> 0x0080
            r1.append(r9)     // Catch: all -> 0x0080
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x0080
            com.whatsapp.util.Log.w(r0)     // Catch: all -> 0x0080
        L_0x0075:
            monitor-enter(r8)
            java.util.Set r0 = r8.A05     // Catch: all -> 0x007d
            r0.remove(r7)     // Catch: all -> 0x007d
            monitor-exit(r8)     // Catch: all -> 0x007d
            return
        L_0x007d:
            r0 = move-exception
            monitor-exit(r8)     // Catch: all -> 0x007d
            throw r0
        L_0x0080:
            r1 = move-exception
            monitor-enter(r8)
            java.util.Set r0 = r8.A05     // Catch: all -> 0x0089
            r0.remove(r7)     // Catch: all -> 0x0089
            monitor-exit(r8)     // Catch: all -> 0x0089
            throw r1
        L_0x0089:
            r0 = move-exception
            monitor-exit(r8)     // Catch: all -> 0x0089
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15770ns.A02(android.os.CancellationSignal, android.os.ParcelFileDescriptor, X.0ns, java.io.File, javax.crypto.Cipher):void");
    }

    public ParcelFileDescriptor A03(CancellationSignal cancellationSignal, long j) {
        AnonymousClass2FQ r6;
        ParcelFileDescriptor[] createPipe;
        Cipher cipher;
        CancellationSignal cancellationSignal2 = cancellationSignal;
        C16310on A00 = this.A03.A01.A00.A00();
        try {
            Cursor A09 = A00.A03.A09("SELECT   f._id, f.local_path, f.exported_path, f.file_size, f.required, f.encryption_iv FROM exported_files_metadata AS f WHERE f._id = ?", new String[]{Long.toString(j)});
            if (!A09.moveToFirst()) {
                r6 = null;
            } else {
                r6 = C19530uG.A00(A09);
            }
            A09.close();
            A00.close();
            if (r6 != null) {
                File file = r6.A02;
                if (file.exists()) {
                    if (file.length() == 0) {
                        StringBuilder sb = new StringBuilder("Exporting EMPTY file: path=");
                        sb.append(file);
                        Log.i(sb.toString());
                    }
                    long length = file.length();
                    long j2 = r6.A01;
                    if (length != j2) {
                        StringBuilder sb2 = new StringBuilder("Exporting MISMATCHED SIZE file: path=");
                        sb2.append(file);
                        sb2.append(", on-disk=");
                        sb2.append(file.length());
                        sb2.append(", on-record=");
                        sb2.append(j2);
                        Log.i(sb2.toString());
                    }
                    if (this.A07.getAndSet(j) == j) {
                        StringBuilder sb3 = new StringBuilder("RETRY DETECTED for path=");
                        sb3.append(file);
                        sb3.append(" with size on-disk=");
                        sb3.append(file.length());
                        sb3.append(", on-record=");
                        sb3.append(j2);
                        Log.i(sb3.toString());
                    }
                    if (cancellationSignal == null) {
                        cancellationSignal2 = new CancellationSignal();
                    }
                    try {
                        if (Build.VERSION.SDK_INT > 19) {
                            createPipe = ParcelFileDescriptor.createReliablePipe();
                        } else {
                            createPipe = ParcelFileDescriptor.createPipe();
                        }
                        ParcelFileDescriptor parcelFileDescriptor = createPipe[0];
                        ParcelFileDescriptor parcelFileDescriptor2 = createPipe[1];
                        synchronized (this) {
                            Set<ParcelFileDescriptor> set = this.A05;
                            if (!set.isEmpty()) {
                                this.A00.AaV("xpm-export-api-leaked-fd", Integer.toString(set.size()), false);
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("ExportMigrationApi/force closing pending file descriptors (");
                                sb4.append(set.size());
                                sb4.append(")");
                                Log.e(sb4.toString());
                                for (ParcelFileDescriptor parcelFileDescriptor3 : set) {
                                    try {
                                        if (Build.VERSION.SDK_INT >= 19) {
                                            parcelFileDescriptor3.closeWithError("Force closing, concurrent streaming not supported.");
                                        }
                                    } catch (IOException e) {
                                        Log.e("ExportMigrationApi/Failed to close the pipe after an error.", e);
                                    }
                                }
                                set.clear();
                            }
                            set.add(parcelFileDescriptor2);
                        }
                        String str = r6.A03;
                        if (TextUtils.isEmpty(str)) {
                            cipher = null;
                        } else {
                            AnonymousClass2F8 A002 = this.A01.A00();
                            if (A002 != null) {
                                byte[] decode = Base64.decode(A002.A03, 2);
                                byte[] decode2 = Base64.decode(str, 2);
                                try {
                                    cipher = Cipher.getInstance("AES/GCM/NoPadding");
                                    cipher.init(1, new SecretKeySpec(decode, "AES"), new IvParameterSpec(decode2));
                                } catch (GeneralSecurityException e2) {
                                    throw new IOException("Failed to initiate encrypting cipher.", e2);
                                }
                            } else {
                                throw new IOException("Failed to initiate encryption, key is missing.");
                            }
                        }
                        try {
                            this.A06.execute(new Runnable(cancellationSignal2, parcelFileDescriptor2, this, file, cipher, r6.A00) { // from class: X.3lP
                                public final /* synthetic */ long A00;
                                public final /* synthetic */ CancellationSignal A01;
                                public final /* synthetic */ ParcelFileDescriptor A02;
                                public final /* synthetic */ C15770ns A03;
                                public final /* synthetic */ File A04;
                                public final /* synthetic */ Cipher A05;

                                {
                                    this.A03 = r3;
                                    this.A00 = r6;
                                    this.A04 = r4;
                                    this.A05 = r5;
                                    this.A02 = r2;
                                    this.A01 = r1;
                                }

                                @Override // java.lang.Runnable
                                public final void run() {
                                    C15770ns r4 = this.A03;
                                    File file2 = this.A04;
                                    Cipher cipher2 = this.A05;
                                    C15770ns.A02(this.A01, this.A02, r4, file2, cipher2);
                                }
                            });
                            return parcelFileDescriptor;
                        } catch (RejectedExecutionException e3) {
                            parcelFileDescriptor.close();
                            parcelFileDescriptor2.close();
                            throw new IOException("Failed to initiate streaming.", e3);
                        }
                    } catch (FileNotFoundException e4) {
                        throw e4;
                    } catch (IOException e5) {
                        throw new FileNotFoundException(e5.toString());
                    }
                } else {
                    this.A00.AaV("xpm-export-missing-file-type", C14350lI.A07(file.getAbsolutePath()), false);
                    StringBuilder sb5 = new StringBuilder("File no longer exists: ");
                    sb5.append(j);
                    throw new FileNotFoundException(sb5.toString());
                }
            } else {
                StringBuilder sb6 = new StringBuilder("Unknown entry: ");
                sb6.append(j);
                throw new FileNotFoundException(sb6.toString());
            }
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
