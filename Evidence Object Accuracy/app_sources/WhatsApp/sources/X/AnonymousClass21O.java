package X;

/* renamed from: X.21O  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass21O extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;

    public AnonymousClass21O() {
        super(3314, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 2, 113760892);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(5, this.A04);
        r3.Abe(6, this.A05);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamPsUserActivitySessionSummary {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psUserActivityDuration", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psUserActivityForeground", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psUserActivitySessionsLength", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psUserActivityStartTime", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psUserActivityTimeChange", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psUserSessionSummarySequence", this.A05);
        sb.append("}");
        return sb.toString();
    }
}
