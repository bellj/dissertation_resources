package X;

import com.whatsapp.payments.ui.IndiaUpiBankAccountDetailsActivity;

/* renamed from: X.5od  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124115od extends AbstractC16350or {
    public final /* synthetic */ int A00;
    public final /* synthetic */ IndiaUpiBankAccountDetailsActivity A01;
    public final /* synthetic */ Runnable A02;

    public C124115od(IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity, Runnable runnable, int i) {
        this.A01 = indiaUpiBankAccountDetailsActivity;
        this.A00 = i;
        this.A02 = runnable;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity = this.A01;
        C17070qD r0 = ((AbstractView$OnClickListenerC121765jx) indiaUpiBankAccountDetailsActivity).A0D;
        r0.A03();
        C20370ve r5 = r0.A08;
        boolean z = false;
        Integer[] numArr = {417, 418};
        Integer[] numArr2 = new Integer[1];
        C12960it.A1P(numArr2, 40, 0);
        if (r5.A0W(indiaUpiBankAccountDetailsActivity.A00.A0A, numArr, numArr2, 1).size() > 0) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        if (C12970iu.A1Y(obj)) {
            C36021jC.A01(this.A01, this.A00);
        } else {
            this.A02.run();
        }
    }
}
