package X;

import com.whatsapp.catalogcategory.view.fragment.CatalogAllCategoryFragment;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogAllCategoryViewModel;

/* renamed from: X.3dj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71923dj extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogAllCategoryFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71923dj(CatalogAllCategoryFragment catalogAllCategoryFragment) {
        super(0);
        this.this$0 = catalogAllCategoryFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return C13000ix.A02(this.this$0).A00(CatalogAllCategoryViewModel.class);
    }
}
