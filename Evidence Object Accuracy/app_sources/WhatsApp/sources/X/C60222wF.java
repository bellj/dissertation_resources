package X;

import java.util.ArrayList;

/* renamed from: X.2wF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60222wF extends C71313cj {
    public final C15610nY A00;
    public final ArrayList A01;

    public C60222wF(C15450nH r1, C15550nR r2, C15610nY r3, ArrayList arrayList) {
        super(r1, r2, r3);
        this.A00 = r3;
        this.A01 = arrayList;
    }

    @Override // X.C71313cj
    public int A00(AnonymousClass1YV r6, AnonymousClass1YV r7) {
        C15550nR r2 = super.A01;
        C15370n3 A0B = r2.A0B(r6.A02);
        C15370n3 A0B2 = r2.A0B(r7.A02);
        C15610nY r3 = this.A00;
        ArrayList arrayList = this.A01;
        boolean A0M = r3.A0M(A0B, arrayList, true);
        if (A0M != r3.A0M(A0B2, arrayList, true)) {
            return A0M ? -1 : 1;
        }
        return super.compare(r6, r7);
    }
}
