package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.5I4  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5I4<K, V> extends AbstractMap<K, V> implements Serializable {
    public static final Object NOT_FOUND = C12970iu.A0l();
    public transient int[] entries;
    public transient Set entrySetView;
    public transient Set keySetView;
    public transient Object[] keys;
    public transient int metadata;
    public transient int size;
    public transient Object table;
    public transient Object[] values;
    public transient Collection valuesView;

    public int adjustAfterRemove(int i, int i2) {
        return i - 1;
    }

    public AnonymousClass5I4() {
        init(3);
    }

    public static /* synthetic */ int access$1210(AnonymousClass5I4 r2) {
        int i = r2.size;
        r2.size = i - 1;
        return i;
    }

    public int allocArrays() {
        C28291Mn.A05("Arrays already allocated", needsAllocArrays());
        int i = this.metadata;
        int tableSize = C95584e0.tableSize(i);
        this.table = C95584e0.createTable(tableSize);
        setHashTableMask(tableSize - 1);
        this.entries = new int[i];
        this.keys = new Object[i];
        this.values = new Object[i];
        return i;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        if (!needsAllocArrays()) {
            incrementModCount();
            Map delegateOrNull = delegateOrNull();
            if (delegateOrNull != null) {
                this.metadata = Math.min(Math.max(size(), 3), 1073741823);
                delegateOrNull.clear();
                this.table = null;
            } else {
                Arrays.fill(requireKeys(), 0, this.size, (Object) null);
                Arrays.fill(requireValues(), 0, this.size, (Object) null);
                C95584e0.tableClear(requireTable());
                Arrays.fill(requireEntries(), 0, this.size, 0);
            }
            this.size = 0;
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.containsKey(obj);
        }
        return C12980iv.A1V(indexOf(obj), -1);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsValue(Object obj) {
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.containsValue(obj);
        }
        for (int i = 0; i < this.size; i++) {
            if (AnonymousClass28V.A00(obj, value(i))) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.Map */
    /* JADX WARN: Multi-variable type inference failed */
    public Map convertToHashFloodingResistantImplementation() {
        Map createHashFloodingResistantDelegate = createHashFloodingResistantDelegate(hashTableMask() + 1);
        int firstEntryIndex = firstEntryIndex();
        while (firstEntryIndex >= 0) {
            createHashFloodingResistantDelegate.put(key(firstEntryIndex), value(firstEntryIndex));
            firstEntryIndex = getSuccessor(firstEntryIndex);
        }
        this.table = createHashFloodingResistantDelegate;
        this.entries = null;
        this.keys = null;
        this.values = null;
        incrementModCount();
        return createHashFloodingResistantDelegate;
    }

    public static AnonymousClass5I4 create() {
        return new AnonymousClass5I4();
    }

    public Set createEntrySet() {
        return new AnonymousClass5I6(this);
    }

    public Map createHashFloodingResistantDelegate(int i) {
        return new LinkedHashMap(i, 1.0f);
    }

    public Set createKeySet() {
        return new AnonymousClass5I7(this);
    }

    public Collection createValues() {
        return new C113495Hu(this);
    }

    public Map delegateOrNull() {
        Object obj = this.table;
        if (obj instanceof Map) {
            return (Map) obj;
        }
        return null;
    }

    private int entry(int i) {
        return requireEntries()[i];
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set entrySet() {
        Set set = this.entrySetView;
        if (set != null) {
            return set;
        }
        Set createEntrySet = createEntrySet();
        this.entrySetView = createEntrySet;
        return createEntrySet;
    }

    public Iterator entrySetIterator() {
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return C12960it.A0n(delegateOrNull);
        }
        return new C80983tH(this);
    }

    public int firstEntryIndex() {
        return isEmpty() ? -1 : 0;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Object get(Object obj) {
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.get(obj);
        }
        int indexOf = indexOf(obj);
        if (indexOf == -1) {
            return null;
        }
        return value(indexOf);
    }

    public int getSuccessor(int i) {
        int i2 = i + 1;
        if (i2 >= this.size) {
            return -1;
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public int hashTableMask() {
        return (1 << (this.metadata & 31)) - 1;
    }

    public void incrementModCount() {
        this.metadata += 32;
    }

    /* access modifiers changed from: private */
    public int indexOf(Object obj) {
        if (!needsAllocArrays()) {
            int smearedHash = C28301Mo.smearedHash(obj);
            int hashTableMask = hashTableMask();
            int tableGet = C95584e0.tableGet(requireTable(), smearedHash & hashTableMask);
            if (tableGet != 0) {
                int hashPrefix = C95584e0.getHashPrefix(smearedHash, hashTableMask);
                do {
                    int i = tableGet - 1;
                    int entry = entry(i);
                    if (C95584e0.getHashPrefix(entry, hashTableMask) == hashPrefix && AnonymousClass28V.A00(obj, key(i))) {
                        return i;
                    }
                    tableGet = C95584e0.getNext(entry, hashTableMask);
                } while (tableGet != 0);
            }
        }
        return -1;
    }

    public void init(int i) {
        this.metadata = Math.min(Math.max(i, 1), 1073741823);
    }

    public void insertEntry(int i, Object obj, Object obj2, int i2, int i3) {
        setEntry(i, C95584e0.maskCombine(i2, 0, i3));
        setKey(i, obj);
        setValue(i, obj2);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean isEmpty() {
        return C12960it.A1T(size());
    }

    /* access modifiers changed from: private */
    public Object key(int i) {
        return requireKeys()[i];
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set keySet() {
        Set set = this.keySetView;
        if (set != null) {
            return set;
        }
        Set createKeySet = createKeySet();
        this.keySetView = createKeySet;
        return createKeySet;
    }

    public Iterator keySetIterator() {
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return C72453ed.A10(delegateOrNull);
        }
        return new C80963tF(this);
    }

    public void moveLastEntry(int i, int i2) {
        int i3;
        int i4;
        Object requireTable = requireTable();
        int[] requireEntries = requireEntries();
        Object[] requireKeys = requireKeys();
        Object[] requireValues = requireValues();
        int size = size() - 1;
        if (i < size) {
            Object obj = requireKeys[size];
            requireKeys[i] = obj;
            requireValues[i] = requireValues[size];
            requireKeys[size] = null;
            requireValues[size] = null;
            requireEntries[i] = requireEntries[size];
            requireEntries[size] = 0;
            int smearedHash = C28301Mo.smearedHash(obj) & i2;
            int tableGet = C95584e0.tableGet(requireTable, smearedHash);
            int i5 = size + 1;
            if (tableGet == i5) {
                C95584e0.tableSet(requireTable, smearedHash, i + 1);
                return;
            }
            do {
                i3 = tableGet - 1;
                i4 = requireEntries[i3];
                tableGet = C95584e0.getNext(i4, i2);
            } while (tableGet != i5);
            requireEntries[i3] = C95584e0.maskCombine(i4, i + 1, i2);
            return;
        }
        requireKeys[i] = null;
        requireValues[i] = null;
        requireEntries[i] = 0;
    }

    public boolean needsAllocArrays() {
        return C12980iv.A1X(this.table);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Object put(Object obj, Object obj2) {
        int i;
        int i2;
        if (needsAllocArrays()) {
            allocArrays();
        }
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull == null) {
            int[] requireEntries = requireEntries();
            Object[] requireKeys = requireKeys();
            Object[] requireValues = requireValues();
            int i3 = this.size;
            int i4 = i3 + 1;
            int smearedHash = C28301Mo.smearedHash(obj);
            int hashTableMask = hashTableMask();
            int i5 = smearedHash & hashTableMask;
            Object requireTable = requireTable();
            int tableGet = C95584e0.tableGet(requireTable, i5);
            if (tableGet == 0) {
                if (i4 <= hashTableMask) {
                    C95584e0.tableSet(requireTable, i5, i4);
                }
                hashTableMask = resizeTable(hashTableMask, C95584e0.newCapacity(hashTableMask), smearedHash, i3);
            } else {
                int hashPrefix = C95584e0.getHashPrefix(smearedHash, hashTableMask);
                int i6 = 0;
                do {
                    i = tableGet - 1;
                    i2 = requireEntries[i];
                    if (C95584e0.getHashPrefix(i2, hashTableMask) != hashPrefix || !AnonymousClass28V.A00(obj, requireKeys[i])) {
                        tableGet = C95584e0.getNext(i2, hashTableMask);
                        i6++;
                    } else {
                        Object obj3 = requireValues[i];
                        requireValues[i] = obj2;
                        return obj3;
                    }
                } while (tableGet != 0);
                if (i6 >= 9) {
                    delegateOrNull = convertToHashFloodingResistantImplementation();
                } else {
                    if (i4 <= hashTableMask) {
                        requireEntries[i] = C95584e0.maskCombine(i2, i4, hashTableMask);
                    }
                    hashTableMask = resizeTable(hashTableMask, C95584e0.newCapacity(hashTableMask), smearedHash, i3);
                }
            }
            resizeMeMaybe(i4);
            insertEntry(i3, obj, obj2, smearedHash, hashTableMask);
            this.size = i4;
            incrementModCount();
            return null;
        }
        return delegateOrNull.put(obj, obj2);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: X.5I4<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            init(readInt);
            for (int i = 0; i < readInt; i++) {
                put(objectInputStream.readObject(), objectInputStream.readObject());
            }
            return;
        }
        throw new InvalidObjectException(C12960it.A0e("Invalid size: ", C12980iv.A0t(25), readInt));
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Object remove(Object obj) {
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.remove(obj);
        }
        Object removeHelper = removeHelper(obj);
        if (removeHelper == NOT_FOUND) {
            return null;
        }
        return removeHelper;
    }

    /* access modifiers changed from: private */
    public Object removeHelper(Object obj) {
        int hashTableMask;
        int remove;
        if (needsAllocArrays() || (remove = C95584e0.remove(obj, null, (hashTableMask = hashTableMask()), requireTable(), requireEntries(), requireKeys(), null)) == -1) {
            return NOT_FOUND;
        }
        Object value = value(remove);
        moveLastEntry(remove, hashTableMask);
        this.size--;
        incrementModCount();
        return value;
    }

    /* access modifiers changed from: private */
    public int[] requireEntries() {
        return this.entries;
    }

    /* access modifiers changed from: private */
    public Object[] requireKeys() {
        return this.keys;
    }

    /* access modifiers changed from: private */
    public Object requireTable() {
        return this.table;
    }

    /* access modifiers changed from: private */
    public Object[] requireValues() {
        return this.values;
    }

    public void resizeEntries(int i) {
        this.entries = Arrays.copyOf(requireEntries(), i);
        this.keys = Arrays.copyOf(requireKeys(), i);
        this.values = Arrays.copyOf(requireValues(), i);
    }

    private void resizeMeMaybe(int i) {
        int min;
        int length = requireEntries().length;
        if (i > length && (min = Math.min(1073741823, (Math.max(1, length >>> 1) + length) | 1)) != length) {
            resizeEntries(min);
        }
    }

    private int resizeTable(int i, int i2, int i3, int i4) {
        Object createTable = C95584e0.createTable(i2);
        int i5 = i2 - 1;
        if (i4 != 0) {
            C95584e0.tableSet(createTable, i3 & i5, i4 + 1);
        }
        Object requireTable = requireTable();
        int[] requireEntries = requireEntries();
        for (int i6 = 0; i6 <= i; i6++) {
            int tableGet = C95584e0.tableGet(requireTable, i6);
            while (tableGet != 0) {
                int i7 = tableGet - 1;
                int i8 = requireEntries[i7];
                int hashPrefix = C95584e0.getHashPrefix(i8, i) | i6;
                int i9 = hashPrefix & i5;
                int tableGet2 = C95584e0.tableGet(createTable, i9);
                C95584e0.tableSet(createTable, i9, tableGet);
                requireEntries[i7] = C95584e0.maskCombine(hashPrefix, tableGet2, i5);
                tableGet = C95584e0.getNext(i8, i);
            }
        }
        this.table = createTable;
        setHashTableMask(i5);
        return i5;
    }

    private void setEntry(int i, int i2) {
        requireEntries()[i] = i2;
    }

    private void setHashTableMask(int i) {
        this.metadata = C95584e0.maskCombine(this.metadata, 32 - Integer.numberOfLeadingZeros(i), 31);
    }

    private void setKey(int i, Object obj) {
        requireKeys()[i] = obj;
    }

    /* access modifiers changed from: private */
    public void setValue(int i, Object obj) {
        requireValues()[i] = obj;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        Map delegateOrNull = delegateOrNull();
        return delegateOrNull != null ? delegateOrNull.size() : this.size;
    }

    /* access modifiers changed from: private */
    public Object value(int i) {
        return requireValues()[i];
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Collection values() {
        Collection collection = this.valuesView;
        if (collection != null) {
            return collection;
        }
        Collection createValues = createValues();
        this.valuesView = createValues;
        return createValues;
    }

    public Iterator valuesIterator() {
        Map delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return C12960it.A0o(delegateOrNull);
        }
        return new C80973tG(this);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(size());
        Iterator entrySetIterator = entrySetIterator();
        while (entrySetIterator.hasNext()) {
            Map.Entry A15 = C12970iu.A15(entrySetIterator);
            objectOutputStream.writeObject(A15.getKey());
            objectOutputStream.writeObject(A15.getValue());
        }
    }
}
