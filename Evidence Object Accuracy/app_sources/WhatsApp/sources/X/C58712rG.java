package X;

/* renamed from: X.2rG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58712rG extends AbstractC83923y7 {
    public final Integer A00;
    public final boolean A01;

    public C58712rG() {
        this(null, false);
    }

    public C58712rG(Integer num, boolean z) {
        this.A01 = z;
        this.A00 = num;
    }

    @Override // X.AbstractC83923y7
    public boolean A00() {
        return this.A01;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C58712rG) {
                C58712rG r5 = (C58712rG) obj;
                if (this.A01 != r5.A01 || !C16700pc.A0O(this.A00, r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        boolean z = this.A01;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = i * 31;
        Integer num = this.A00;
        return i4 + (num == null ? 0 : num.hashCode());
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Loading(isSelected=");
        A0k.append(this.A01);
        A0k.append(", ringColor=");
        return C12960it.A0a(this.A00, A0k);
    }
}
