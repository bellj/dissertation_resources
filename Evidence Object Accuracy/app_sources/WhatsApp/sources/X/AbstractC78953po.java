package X;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Iterator;

/* renamed from: X.3po  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC78953po extends AbstractC95014cu implements SafeParcelable {
    @Override // android.os.Parcelable
    public final int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj != null) {
            if (this != obj) {
                if (getClass().isInstance(obj)) {
                    AbstractC95014cu r7 = (AbstractC95014cu) obj;
                    Iterator A0o = C12960it.A0o(A05());
                    while (A0o.hasNext()) {
                        C78633pE r2 = (C78633pE) A0o.next();
                        boolean A06 = A06(r2);
                        boolean A062 = r7.A06(r2);
                        if (A06) {
                            if (A062 && C13300jT.A00(A04(r2), r7.A04(r2))) {
                            }
                        } else if (A062) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Iterator A0o = C12960it.A0o(A05());
        int i = 0;
        while (A0o.hasNext()) {
            C78633pE r2 = (C78633pE) A0o.next();
            if (A06(r2)) {
                Object A04 = A04(r2);
                C13020j0.A01(A04);
                i = C12990iw.A08(A04, i * 31);
            }
        }
        return i;
    }
}
