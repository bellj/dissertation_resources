package X;

/* renamed from: X.2Dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C48012Dv {
    public final AbstractC14640lm A00;
    public final AbstractC14640lm A01;
    public final AbstractC14640lm A02;
    public final String A03;
    public final boolean A04;
    public final byte[] A05;
    public final byte[] A06;

    public C48012Dv(AbstractC14640lm r1, AbstractC14640lm r2, AbstractC14640lm r3, String str, byte[] bArr, byte[] bArr2, boolean z) {
        this.A02 = r1;
        this.A01 = r2;
        this.A00 = r3;
        this.A03 = str;
        this.A05 = bArr;
        this.A06 = bArr2;
        this.A04 = z;
    }
}
