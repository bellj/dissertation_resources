package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.3mj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77093mj extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(17);
    public final int A00;
    public final int A01;
    public final int A02;
    public final int[] A03;
    public final int[] A04;

    public C77093mj(Parcel parcel) {
        super("MLLT");
        this.A02 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A01 = parcel.readInt();
        this.A03 = parcel.createIntArray();
        this.A04 = parcel.createIntArray();
    }

    public C77093mj(int[] iArr, int[] iArr2, int i, int i2, int i3) {
        super("MLLT");
        this.A02 = i;
        this.A00 = i2;
        this.A01 = i3;
        this.A03 = iArr;
        this.A04 = iArr2;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77093mj.class != obj.getClass()) {
                return false;
            }
            C77093mj r5 = (C77093mj) obj;
            if (!(this.A02 == r5.A02 && this.A00 == r5.A00 && this.A01 == r5.A01 && Arrays.equals(this.A03, r5.A03) && Arrays.equals(this.A04, r5.A04))) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((((((C72453ed.A05(this.A02) + this.A00) * 31) + this.A01) * 31) + Arrays.hashCode(this.A03)) * 31) + Arrays.hashCode(this.A04);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A01);
        parcel.writeIntArray(this.A03);
        parcel.writeIntArray(this.A04);
    }
}
