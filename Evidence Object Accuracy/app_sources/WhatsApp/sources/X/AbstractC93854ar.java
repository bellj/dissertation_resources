package X;

/* renamed from: X.4ar  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC93854ar {
    public static final C114065Kb A00 = new C114065Kb();
    public static final AbstractC93854ar A01;

    static {
        AbstractC93854ar r0;
        if (!(AnonymousClass4GX.A00 instanceof C113705Iq)) {
            r0 = new AnonymousClass5KZ();
        } else {
            r0 = new AnonymousClass5KY();
        }
        A01 = r0;
    }

    public int A00() {
        if (!(this instanceof C114065Kb)) {
            return ((AbstractC114055Ka) this).A01().nextInt();
        }
        return A01.A00();
    }
}
