package X;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;

/* renamed from: X.63A  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63A implements Camera.FaceDetectionListener {
    public final /* synthetic */ AnonymousClass661 A00;

    public AnonymousClass63A(AnonymousClass661 r1) {
        this.A00 = r1;
    }

    public static void A00(Matrix matrix, Point point, float[] fArr) {
        if (point != null) {
            fArr[0] = (float) point.x;
            fArr[1] = (float) point.y;
            matrix.mapPoints(fArr);
            point.set((int) fArr[0], (int) fArr[1]);
        }
    }

    @Override // android.hardware.Camera.FaceDetectionListener
    public void onFaceDetection(Camera.Face[] faceArr, Camera camera) {
        C127555ug[] r5;
        if (faceArr != null) {
            int length = faceArr.length;
            r5 = new C127555ug[length];
            for (int i = 0; i < length; i++) {
                Camera.Face face = faceArr[i];
                r5[i] = new C127555ug(face.leftEye, face.rightEye, face.mouth, face.rect);
                C127555ug r7 = r5[i];
                Matrix matrix = this.A00.A03;
                RectF rectF = new RectF();
                float[] fArr = new float[2];
                Rect rect = r7.A03;
                rectF.set(rect);
                matrix.mapRect(rectF);
                rect.set((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
                A00(matrix, r7.A00, fArr);
                A00(matrix, r7.A02, fArr);
                A00(matrix, r7.A01, fArr);
            }
        } else {
            r5 = null;
        }
        AnonymousClass61K.A00(new RunnableC135116Hf(this, r5));
    }
}
