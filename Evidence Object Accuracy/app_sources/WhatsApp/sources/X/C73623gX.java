package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ScrollView;

/* renamed from: X.3gX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73623gX extends GestureDetector.SimpleOnGestureListener {
    public int A00 = -1;
    public final /* synthetic */ ScrollView A01;
    public final /* synthetic */ AbstractActivityC37221lk A02;

    public C73623gX(ScrollView scrollView, AbstractActivityC37221lk r3) {
        this.A02 = r3;
        this.A01 = scrollView;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        this.A01.requestDisallowInterceptTouchEvent(true);
        return super.onDown(motionEvent);
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int i = this.A00;
        AbstractActivityC37221lk r5 = this.A02;
        this.A00 = r5.A01.getScrollY();
        r5.A01.scrollBy((int) (f + 0.5f), (int) (0.5f + f2));
        if ((f2 >= 0.0f || r5.A01.getScrollY() > 0) && (f2 <= 0.0f || i != this.A00)) {
            return true;
        }
        this.A01.requestDisallowInterceptTouchEvent(false);
        this.A00 = -1;
        return true;
    }
}
