package X;

import android.content.Context;
import android.text.SpannableString;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.payments.ui.BrazilOrderDetailsActivity;
import com.whatsapp.payments.ui.BrazilPaymentActivity;
import com.whatsapp.payments.ui.ConfirmPaymentFragment;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.6CL  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CL implements AbstractC124835qC {
    public final /* synthetic */ AbstractC30791Yv A00;
    public final /* synthetic */ C30821Yy A01;
    public final /* synthetic */ AbstractC28901Pl A02;
    public final /* synthetic */ AnonymousClass2S0 A03;
    public final /* synthetic */ BrazilPaymentActivity A04;
    public final /* synthetic */ ConfirmPaymentFragment A05;

    @Override // X.AbstractC124835qC
    public String ACJ(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public String ACK(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public String AEO(AbstractC28901Pl r2) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public void AMM(ViewGroup viewGroup) {
    }

    @Override // X.AbstractC124835qC
    public void AMP(ViewGroup viewGroup) {
    }

    @Override // X.AbstractC124835qC
    public boolean AdU(AbstractC28901Pl r2) {
        return false;
    }

    @Override // X.AbstractC124835qC
    public boolean AdV() {
        return true;
    }

    @Override // X.AbstractC124835qC
    public boolean Adt() {
        return true;
    }

    public AnonymousClass6CL(AbstractC30791Yv r1, C30821Yy r2, AbstractC28901Pl r3, AnonymousClass2S0 r4, BrazilPaymentActivity brazilPaymentActivity, ConfirmPaymentFragment confirmPaymentFragment) {
        this.A04 = brazilPaymentActivity;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = r1;
        this.A05 = confirmPaymentFragment;
    }

    @Override // X.AbstractC124835qC
    public void A6F(ViewGroup viewGroup) {
        C50942Ry r7;
        AnonymousClass2S0 r0 = this.A03;
        if (r0 != null && (r7 = r0.A01) != null) {
            BrazilPaymentActivity brazilPaymentActivity = this.A04;
            C117745aX r3 = new C117745aX(brazilPaymentActivity, brazilPaymentActivity.A04, this.A01, r7, ((AbstractActivityC121685jC) brazilPaymentActivity).A01, true);
            int i = ((AbstractActivityC121685jC) brazilPaymentActivity).A01;
            if (i != 0) {
                if (i != 1) {
                    if (!(i == 2 || i == 3)) {
                        if (i != 4) {
                            if (!(i == 5 || i == 7)) {
                                return;
                            }
                        } else if (r7.A01 == 0) {
                            viewGroup.addView(r3);
                            ((AbstractActivityC121685jC) brazilPaymentActivity).A0T.A05(-1, 1);
                            return;
                        } else {
                            return;
                        }
                    }
                } else if (r7.A00 == 0) {
                    viewGroup.addView(r3);
                    ((AbstractActivityC121685jC) brazilPaymentActivity).A0T.A05(1, -1);
                    return;
                } else {
                    return;
                }
            }
            viewGroup.addView(r3);
        }
    }

    @Override // X.AbstractC124835qC
    public String ABX(AbstractC28901Pl r8, int i) {
        BrazilPaymentActivity brazilPaymentActivity = this.A04;
        if (BrazilPaymentActivity.A03(r8, i)) {
            return brazilPaymentActivity.A01.getString(R.string.payment_use_another_card);
        }
        AnonymousClass1ZY r0 = this.A02.A08;
        AnonymousClass009.A05(r0);
        if (!r0.A0A()) {
            return brazilPaymentActivity.getString(R.string.confirm_payment_bottom_sheet_confirm_unverified_button);
        }
        return C12960it.A0X(brazilPaymentActivity, this.A00.AAA(brazilPaymentActivity.A04, this.A01, 0), C12970iu.A1b(), 0, R.string.confirm_payment_bottom_sheet_confirm_amount_button);
    }

    @Override // X.AbstractC124835qC
    public String ACf(AbstractC28901Pl r4, int i) {
        Context context;
        int i2;
        AbstractC30871Zd r1 = (AbstractC30871Zd) r4.A08;
        if (r1 == null) {
            return null;
        }
        BrazilPaymentActivity brazilPaymentActivity = this.A04;
        if (BrazilPaymentActivity.A03(r4, i)) {
            if (!"ACTIVE".equals(r1.A0I)) {
                context = brazilPaymentActivity.A01;
                i2 = R.string.card_state_no_longer_active_hint;
            } else {
                boolean A08 = ((AbstractActivityC121685jC) brazilPaymentActivity).A0O.A08();
                context = brazilPaymentActivity.A01;
                i2 = R.string.brazil_credit_card_education_hint_p2p_only;
                if (A08) {
                    i2 = R.string.brazil_credit_card_education_hint;
                }
            }
        } else if (r1.A0A()) {
            return null;
        } else {
            context = brazilPaymentActivity.A01;
            i2 = R.string.verify_payment_card_message;
        }
        return context.getString(i2);
    }

    @Override // X.AbstractC124835qC
    public void AMN(ViewGroup viewGroup) {
        String str;
        BrazilPaymentActivity brazilPaymentActivity = this.A04;
        C12960it.A0J(brazilPaymentActivity.getLayoutInflater().inflate(R.layout.confirm_dialog_title, viewGroup, true), R.id.text).setText(C12960it.A0X(brazilPaymentActivity.A01, brazilPaymentActivity.A03.A04(((AbstractActivityC121685jC) brazilPaymentActivity).A08.A01(((AbstractActivityC121685jC) brazilPaymentActivity).A0G)), new Object[1], 0, R.string.confirm_payment_title));
        if (!(brazilPaymentActivity instanceof BrazilOrderDetailsActivity)) {
            str = "new_payment";
        } else {
            str = "order_details";
        }
        AnonymousClass61I.A03(AnonymousClass61I.A00(((ActivityC13790kL) brazilPaymentActivity).A05, this.A01, this.A03, null, true), brazilPaymentActivity.A0K, "payment_confirm_prompt", str);
    }

    @Override // X.AbstractC124835qC
    public void AQh(ViewGroup viewGroup, AbstractC28901Pl r9) {
        BrazilPaymentActivity brazilPaymentActivity = this.A04;
        TextEmojiLabel A0T = C12970iu.A0T(brazilPaymentActivity.getLayoutInflater().inflate(R.layout.confirm_payment_footer_row, viewGroup, true), R.id.footer_text);
        boolean A1p = AbstractActivityC121685jC.A1p(brazilPaymentActivity);
        int i = R.string.brazil_ecosystem_name;
        if (A1p) {
            i = R.string.facebook_pay;
        }
        Context context = brazilPaymentActivity.A01;
        AbstractC28491Nn.A05(A0T, ((ActivityC13810kN) brazilPaymentActivity).A08, new SpannableString(C12960it.A0X(context, context.getString(i), new Object[1], 0, R.string.confirm_payment_bottom_sheet_processor)));
    }

    @Override // X.AbstractC124835qC
    public boolean AdO(AbstractC28901Pl r2, int i) {
        return BrazilPaymentActivity.A03(r2, i);
    }

    @Override // X.AbstractC124835qC
    public void Adj(AbstractC28901Pl r2, PaymentMethodRow paymentMethodRow) {
        if (C1311161i.A0B(r2) && !this.A05.A0X) {
            this.A04.A0R.A02(r2, paymentMethodRow);
        }
    }
}
