package X;

/* renamed from: X.5M9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5M9 extends AnonymousClass5NS {
    public AnonymousClass5M9(byte[] bArr, int i) {
        super(bArr, i);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int length = this.A01.length;
        return AnonymousClass1TQ.A00(length + 1) + 1 + length + 1;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }
}
