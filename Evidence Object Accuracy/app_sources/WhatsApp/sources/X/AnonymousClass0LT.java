package X;

import android.content.Context;
import android.text.SpannableStringBuilder;

/* renamed from: X.0LT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0LT {
    public static void A00(Context context, SpannableStringBuilder spannableStringBuilder, float f, int i, int i2) {
        spannableStringBuilder.setSpan(new C73553gQ(f, context.getResources().getDisplayMetrics().scaledDensity), i, i2, 0);
    }
}
