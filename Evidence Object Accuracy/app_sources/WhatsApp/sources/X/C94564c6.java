package X;

/* renamed from: X.4c6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94564c6 {
    public static void A00(Object obj) {
        if (obj != null) {
            if (obj instanceof AnonymousClass1J8) {
                if (obj instanceof AnonymousClass1WJ) {
                    if (((AnonymousClass1WJ) obj).AAk() == 2) {
                        return;
                    }
                } else if (!(obj instanceof AnonymousClass1WK) && !(obj instanceof AnonymousClass1J7) && (obj instanceof AnonymousClass5ZQ)) {
                    return;
                }
            }
            A01(obj, C12960it.A0W(2, "kotlin.jvm.functions.Function"));
            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
        }
    }

    public static void A01(Object obj, String str) {
        StringBuilder A0j = C12960it.A0j(C12980iv.A0s(obj));
        A0j.append(" cannot be cast to ");
        ClassCastException classCastException = new ClassCastException(C12960it.A0d(str, A0j));
        C16700pc.A0L(C94564c6.class.getName(), classCastException);
        throw classCastException;
    }
}
