package X;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.46p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C862946p extends AnonymousClass4V6 {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;

    public C862946p(int i) {
        super(i);
    }

    @Override // X.AnonymousClass4V6
    public JSONObject A00() {
        JSONObject A00 = super.A00();
        try {
            Boolean bool = this.A00;
            if (bool != null) {
                A00.put("flash_call_end_success", bool);
            }
            Boolean bool2 = this.A02;
            if (bool2 != null) {
                A00.put("no_flash_call_id_received", bool2);
            }
            Boolean bool3 = this.A01;
            if (bool3 != null) {
                A00.put("invalid_flash_call_received", bool3);
            }
        } catch (JSONException unused) {
        }
        return A00;
    }
}
