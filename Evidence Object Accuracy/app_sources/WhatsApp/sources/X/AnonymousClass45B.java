package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.45B  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45B extends C14510lY {
    public final AbstractC14640lm A00;
    public final Jid A01;
    public final UserJid A02;
    public final boolean A03;
    public final boolean A04;

    @Override // X.C14510lY
    public int A00() {
        return 3;
    }

    public AnonymousClass45B(AbstractC14640lm r1, Jid jid, UserJid userJid, AnonymousClass1KC r4, C14380lL r5, boolean z, boolean z2) {
        super(r4, r5);
        this.A01 = jid;
        this.A00 = r1;
        this.A03 = z;
        this.A02 = userJid;
        this.A04 = z2;
    }
}
