package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Layout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import com.facebook.redex.IDxCListenerShape8S0100000_1_I1;
import com.facebook.redex.IDxCListenerShape9S0100000_2_I1;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.chatinfo.view.custom.CollapsingProfilePhotoView;
import com.whatsapp.components.TextAndDateLayout;
import com.whatsapp.mediacomposer.doodle.ImagePreviewContentLayout;
import com.whatsapp.settings.SettingsJidNotificationFragment;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0iu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C12970iu {
    public static double A00(AtomicLong atomicLong, AtomicLong atomicLong2) {
        return (((double) atomicLong.get()) * 100.0d) / ((double) atomicLong2.get());
    }

    public static int A01(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getInt(str, 0);
    }

    public static int A02(Layout layout, TextAndDateLayout textAndDateLayout) {
        int i = textAndDateLayout.A00;
        if (i == 0) {
            return layout.getLineCount() - 1;
        }
        return Math.min(i - 1, layout.getLineCount() - 1);
    }

    public static int A03(View view, float f) {
        return (int) TypedValue.applyDimension(1, f, view.getResources().getDisplayMetrics());
    }

    public static int A04(View view, View view2, int i) {
        return i + view.getMeasuredWidth() + view2.getPaddingLeft() + view2.getPaddingRight();
    }

    public static int A05(CollapsingProfilePhotoView collapsingProfilePhotoView) {
        collapsingProfilePhotoView.A05 = 0;
        collapsingProfilePhotoView.A03 = 0;
        collapsingProfilePhotoView.A02 = 0;
        collapsingProfilePhotoView.A0C = true;
        return collapsingProfilePhotoView.getResources().getDimensionPixelSize(R.dimen.space_tight);
    }

    public static int A06(C28531Ny r1, int i, int i2, int i3) {
        r1.A03 = Integer.valueOf(i);
        r1.A01 = Integer.valueOf(i2);
        if (i3 == 0) {
            return 2;
        }
        return i3;
    }

    public static int A07(Object obj) {
        return String.valueOf(obj).length();
    }

    public static int A08(Object obj, Object[] objArr) {
        objArr[0] = obj;
        return Arrays.hashCode(objArr);
    }

    public static int A09(List list) {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public static Intent A0A() {
        return new Intent();
    }

    public static Intent A0B(Uri uri) {
        return new Intent("android.intent.action.VIEW", uri);
    }

    public static Drawable A0C(Context context, int i) {
        Drawable A04 = AnonymousClass00T.A04(context, i);
        AnonymousClass009.A05(A04);
        return A04;
    }

    public static Bundle A0D() {
        return new Bundle();
    }

    public static Handler A0E() {
        return new Handler(Looper.getMainLooper());
    }

    public static Parcelable A0F(Parcel parcel, Parcelable.Creator creator) {
        if (parcel.readInt() == 0) {
            return null;
        }
        return (Parcelable) creator.createFromParcel(parcel);
    }

    public static View A0G(Activity activity) {
        return activity.getWindow().getDecorView();
    }

    public static ViewGroup.MarginLayoutParams A0H(View view) {
        return (ViewGroup.MarginLayoutParams) view.getLayoutParams();
    }

    public static ViewPropertyAnimator A0I(View view, long j) {
        return view.animate().setDuration(j).scaleX(1.0f).scaleY(1.0f).translationX(0.0f).translationY(0.0f);
    }

    public static AlphaAnimation A0J() {
        return new AlphaAnimation(0.0f, 1.0f);
    }

    public static ImageView A0K(View view, int i) {
        return (ImageView) AnonymousClass028.A0D(view, i);
    }

    public static ImageView A0L(View view, int i) {
        return (ImageView) view.findViewById(i);
    }

    public static TextView A0M(ActivityC000800j r0, int i) {
        return (TextView) r0.findViewById(i);
    }

    public static AbstractC005102i A0N(ActivityC000800j r0) {
        AbstractC005102i A1U = r0.A1U();
        AnonymousClass009.A05(A1U);
        return A1U;
    }

    public static C004802e A0O(AnonymousClass01E r1) {
        return new C004802e(r1.A0B());
    }

    public static C004902f A0P(ActivityC000900k r1) {
        return new C004902f(r1.A0V());
    }

    public static AbstractC15710nm A0Q(AnonymousClass01J r0) {
        return (AbstractC15710nm) r0.A4o.get();
    }

    public static C14900mE A0R(AnonymousClass01J r0) {
        return (C14900mE) r0.A8X.get();
    }

    public static C15570nT A0S(AnonymousClass01J r0) {
        return (C15570nT) r0.AAr.get();
    }

    public static TextEmojiLabel A0T(View view, int i) {
        return (TextEmojiLabel) AnonymousClass028.A0D(view, i);
    }

    public static TextEmojiLabel A0U(View view, int i) {
        return (TextEmojiLabel) view.findViewById(i);
    }

    public static C251118d A0V(AnonymousClass01J r0) {
        return (C251118d) r0.A2D.get();
    }

    public static C21270x9 A0W(AnonymousClass01J r0) {
        return (C21270x9) r0.A4A.get();
    }

    public static C16590pI A0X(AnonymousClass01J r0) {
        return (C16590pI) r0.AMg.get();
    }

    public static C15890o4 A0Y(AnonymousClass01J r0) {
        return (C15890o4) r0.AN1.get();
    }

    public static C14820m6 A0Z(AnonymousClass01J r0) {
        return (C14820m6) r0.AN3.get();
    }

    public static C15370n3 A0a(Iterator it) {
        return (C15370n3) it.next();
    }

    public static C16120oU A0b(AnonymousClass01J r0) {
        return (C16120oU) r0.ANE.get();
    }

    public static AbstractC14640lm A0c(Activity activity) {
        return AbstractC14640lm.A01(activity.getIntent().getStringExtra("jid"));
    }

    public static C15860o1 A0d(Preference preference, SettingsJidNotificationFragment settingsJidNotificationFragment, Object obj) {
        ListPreference listPreference = (ListPreference) preference;
        preference.A0I(listPreference.A03[listPreference.A0S((String) obj)].toString());
        return settingsJidNotificationFragment.A05;
    }

    public static BanAppealViewModel A0e(AnonymousClass01E r1) {
        return (BanAppealViewModel) new AnonymousClass02A(r1.A0C()).A00(BanAppealViewModel.class);
    }

    public static IllegalArgumentException A0f(String str) {
        return new IllegalArgumentException(str);
    }

    public static Integer A0g() {
        return 2;
    }

    public static Integer A0h() {
        return 3;
    }

    public static Integer A0i(Object obj, AbstractMap abstractMap, int i) {
        Integer valueOf = Integer.valueOf(i);
        abstractMap.put(obj, valueOf);
        return valueOf;
    }

    public static Long A0j() {
        return -9007199254740991L;
    }

    public static Long A0k() {
        return 9007199254740991L;
    }

    public static Object A0l() {
        return new Object();
    }

    public static SecurityException A0m(Object obj, Object obj2, Locale locale, Object[] objArr) {
        objArr[2] = obj;
        objArr[3] = obj2;
        return new SecurityException(String.format(locale, "java uri \"%s\" not equal to android uri \"%s\". Debug info: %s. Original uri: %s", objArr));
    }

    public static String A0n(Cursor cursor, String str) {
        return cursor.getString(cursor.getColumnIndexOrThrow(str));
    }

    public static String A0o(Uri uri, URI uri2) {
        return String.format(Locale.US, "javaUri scheme: \"%s\". androidUri scheme: \"%s\".", uri2.getScheme(), uri.getScheme());
    }

    public static String A0p(EditText editText) {
        return editText.getText().toString();
    }

    public static String A0q(AnonymousClass01E r0, Object obj, Object[] objArr, int i, int i2) {
        objArr[i] = obj;
        return r0.A0J(i2, objArr);
    }

    public static String A0r(AnonymousClass018 r4, int i) {
        return r4.A0K().format(((double) i) / 100.0d);
    }

    public static String A0s(Object obj, StringBuilder sb) {
        sb.append(obj);
        return sb.toString();
    }

    public static String A0t(Object obj, Map map) {
        return (String) map.get(obj);
    }

    public static String A0u(StringBuilder sb) {
        sb.append(')');
        return sb.toString();
    }

    public static String A0v(StringBuilder sb) {
        sb.append('}');
        return sb.toString();
    }

    public static String A0w(StringBuilder sb, long j) {
        sb.append(j);
        return sb.toString();
    }

    public static String A0x(Iterator it) {
        return (String) it.next();
    }

    public static String A0y(List list, int i) {
        return C87904Dm.A00(list.get(i));
    }

    public static UnsupportedOperationException A0z() {
        return new UnsupportedOperationException();
    }

    public static WeakReference A10(Object obj) {
        return new WeakReference(obj);
    }

    public static HashMap A11() {
        return new HashMap();
    }

    public static HashSet A12() {
        return new HashSet();
    }

    public static HashSet A13(Object obj, Object[] objArr, int i) {
        objArr[i] = obj;
        return new HashSet(Arrays.asList(objArr));
    }

    public static Locale A14(AnonymousClass018 r0) {
        return AnonymousClass018.A00(r0.A00);
    }

    public static Map.Entry A15(Iterator it) {
        return (Map.Entry) it.next();
    }

    public static void A16(int i, Paint paint) {
        paint.setColor(i);
        paint.setStyle(Paint.Style.FILL);
    }

    public static void A17(Activity activity, Point point) {
        activity.getWindowManager().getDefaultDisplay().getSize(point);
    }

    public static void A18(Context context, View view, int i) {
        view.setBackgroundColor(AnonymousClass00T.A00(context, i));
    }

    public static void A19(Context context, TextView textView, int i) {
        textView.setText(context.getString(i));
    }

    public static void A1A(Context context, ImagePreviewContentLayout imagePreviewContentLayout) {
        imagePreviewContentLayout.A01();
        imagePreviewContentLayout.A01 = new RectF();
        imagePreviewContentLayout.A00 = new Rect();
        LayoutInflater.from(context).inflate(R.layout.image_preview_content, (ViewGroup) imagePreviewContentLayout, true);
        imagePreviewContentLayout.setWillNotDraw(false);
        AnonymousClass3MP r0 = new AnonymousClass3MP(imagePreviewContentLayout.getContext(), imagePreviewContentLayout, new C63853De(imagePreviewContentLayout));
        imagePreviewContentLayout.A05 = r0;
        r0.A0P = true;
    }

    public static void A1B(SharedPreferences.Editor editor, String str, int i) {
        editor.putInt(str, i).apply();
    }

    public static void A1C(SharedPreferences.Editor editor, String str, long j) {
        editor.putLong(str, j).apply();
    }

    public static void A1D(SharedPreferences.Editor editor, String str, String str2) {
        editor.putString(str, str2).apply();
    }

    public static void A1E(Rect rect) {
        rect.left >>= 1;
        rect.right >>= 1;
        rect.top >>= 1;
        rect.bottom >>= 1;
    }

    public static void A1F(View view) {
        view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    public static void A1G(View view) {
        if (view != null) {
            view.setVisibility(8);
        }
    }

    public static void A1H(ImageView imageView, int i, boolean z) {
        imageView.setImageResource(i);
        C016307r.A00(AnonymousClass00T.A03(imageView.getContext(), R.color.selector_emoji_icons), imageView);
        imageView.setSelected(z);
    }

    public static void A1I(C004802e r2) {
        r2.setPositiveButton(R.string.ok, null);
    }

    public static void A1J(C004802e r0) {
        r0.create().show();
    }

    public static void A1K(C004802e r1, Object obj, int i, int i2) {
        r1.setNegativeButton(i2, new IDxCListenerShape9S0100000_2_I1(obj, i));
    }

    public static void A1L(C004802e r1, Object obj, int i, int i2) {
        r1.setPositiveButton(i2, new IDxCListenerShape9S0100000_2_I1(obj, i));
    }

    public static void A1M(C004802e r1, Object obj, int i, int i2) {
        r1.setPositiveButton(i2, new IDxCListenerShape8S0100000_1_I1(obj, i));
    }

    public static void A1N(ActivityC000800j r0, int i, int i2) {
        r0.findViewById(i).setVisibility(i2);
    }

    public static void A1O(AnonymousClass04Z r2, CharSequence charSequence) {
        r2.A09(new C007804a(16, charSequence));
    }

    public static void A1P(AbstractC001200n r1, AnonymousClass017 r2, Object obj, int i) {
        r2.A05(r1, new IDxObserverShape4S0100000_2_I1(obj, i));
    }

    public static void A1Q(AnonymousClass017 r1, int i) {
        r1.A0A(Integer.valueOf(i));
    }

    public static void A1R(Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        objArr[5] = obj;
        objArr[6] = obj2;
        objArr[7] = obj3;
        objArr[8] = obj4;
    }

    public static void A1S(Object obj, Object obj2, Object obj3, Object[] objArr) {
        objArr[29] = obj;
        objArr[30] = obj2;
        objArr[31] = obj3;
    }

    public static void A1T(Object obj, Object obj2, AbstractCollection abstractCollection) {
        abstractCollection.add(new AnonymousClass01T(obj, obj2));
    }

    public static void A1U(Object obj, Object obj2, Object[] objArr) {
        objArr[0] = obj;
        objArr[1] = obj2;
    }

    public static void A1V(Object obj, StringBuilder sb) {
        sb.append(obj.toString());
    }

    public static boolean A1W(int i) {
        return i == 1;
    }

    public static boolean A1X(C28501No r2, AbstractCollection abstractCollection) {
        r2.A0F = Long.valueOf((long) abstractCollection.size());
        return abstractCollection.isEmpty();
    }

    public static boolean A1Y(Object obj) {
        return ((Boolean) obj).booleanValue();
    }

    public static boolean A1Z(Object obj, Object obj2) {
        return obj == obj2;
    }

    public static int[] A1a(int i, int i2) {
        return new int[]{i, i2};
    }

    public static Object[] A1b() {
        return new Object[1];
    }
}
