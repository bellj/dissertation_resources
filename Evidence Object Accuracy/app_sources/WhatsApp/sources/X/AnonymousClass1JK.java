package X;

import android.app.Notification;
import android.os.Build;
import com.whatsapp.util.Log;

/* renamed from: X.1JK  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1JK extends AnonymousClass1JL {
    public int A00 = -1;
    public C22900zp A01;
    public boolean A02;
    public final String A03;
    public final boolean A04;

    public AnonymousClass1JK(String str, boolean z) {
        this.A03 = str;
        this.A04 = z;
    }

    public void A01(int i, Notification notification, int i2) {
        this.A00 = i;
        try {
            startForeground(i2, notification);
            if (!this.A02) {
                this.A02 = true;
                if (Build.VERSION.SDK_INT >= 26) {
                    this.A01.A02(this);
                }
            }
        } catch (IllegalStateException e) {
            StringBuilder sb = new StringBuilder("Failed to start foreground service ");
            sb.append(this.A03);
            Log.w(sb.toString(), e);
            A02();
        }
    }

    public boolean A02() {
        int i;
        if (this.A04) {
            i = -1;
        } else {
            i = this.A00;
        }
        boolean stopSelfResult = stopSelfResult(i);
        StringBuilder sb = new StringBuilder();
        sb.append(this.A03);
        sb.append("/Stop service success:");
        sb.append(stopSelfResult);
        Log.i(sb.toString());
        return stopSelfResult;
    }

    @Override // X.AnonymousClass1JL, android.app.Service
    public void onCreate() {
        this.A02 = false;
        super.onCreate();
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        this.A02 = false;
        this.A00 = -1;
    }
}
