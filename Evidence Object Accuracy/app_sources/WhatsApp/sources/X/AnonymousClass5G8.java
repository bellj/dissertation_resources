package X;

/* renamed from: X.5G8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G8 implements AbstractC117285Zg {
    public int A00;
    public int A01;
    public AnonymousClass5XE A02;
    public AnonymousClass20L A03;
    public C866848k A04 = new C866848k(this);
    public C866848k A05 = new C866848k(this);
    public boolean A06;
    public byte[] A07;
    public byte[] A08;
    public byte[] A09;

    @Override // X.AnonymousClass5XQ
    public byte[] AE1() {
        int i = this.A01;
        byte[] bArr = new byte[i];
        System.arraycopy(this.A08, 0, bArr, 0, i);
        return bArr;
    }

    @Override // X.AnonymousClass5XQ
    public int AEp(int i) {
        int size = i + this.A05.size();
        boolean z = this.A06;
        int i2 = this.A01;
        if (z) {
            return size + i2;
        }
        int i3 = size - i2;
        if (size < i2) {
            return 0;
        }
        return i3;
    }

    @Override // X.AbstractC117285Zg
    public AnonymousClass5XE AHO() {
        return this.A02;
    }

    @Override // X.AnonymousClass5XQ
    public int AHQ(int i) {
        return 0;
    }

    @Override // X.AnonymousClass5XQ
    public void AZX(byte[] bArr, int i, int i2) {
        this.A04.write(bArr, i, i2);
    }

    @Override // X.AnonymousClass5XQ
    public int AZZ(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        if (bArr.length >= i + i2) {
            this.A05.write(bArr, i, i2);
            return 0;
        }
        throw new AnonymousClass5O2("Input buffer too short");
    }

    public AnonymousClass5G8(AnonymousClass5XE r3) {
        this.A02 = r3;
        int AAt = r3.AAt();
        this.A00 = AAt;
        this.A08 = new byte[AAt];
        if (AAt != 16) {
            throw C12970iu.A0f("cipher required with a block size of 16.");
        }
    }

    public final void A00(byte[] bArr, byte[] bArr2, int i, int i2) {
        int length;
        AnonymousClass5G2 r5 = new AnonymousClass5G2(this.A02, this.A01 << 3);
        r5.AIc(this.A03);
        byte[] bArr3 = new byte[16];
        C866848k r3 = this.A04;
        int size = r3.size();
        byte[] bArr4 = this.A07;
        if (bArr4 == null) {
            length = 0;
        } else {
            length = bArr4.length;
        }
        if (C12960it.A1U(size + length)) {
            bArr3[0] = (byte) (bArr3[0] | 64);
        }
        int i3 = 2;
        byte b = (byte) (bArr3[0] | ((((r5.A01 - 2) / 2) & 7) << 3));
        bArr3[0] = b;
        byte[] bArr5 = this.A09;
        int length2 = bArr5.length;
        C72463ee.A0O(b, bArr3, ((15 - length2) - 1) & 7, 0);
        System.arraycopy(bArr5, 0, bArr3, 1, length2);
        int i4 = i2;
        int i5 = 1;
        while (i4 > 0) {
            C72463ee.A0V(bArr3, i4, 16 - i5);
            i4 >>>= 8;
            i5++;
        }
        r5.update(bArr3, 0, 16);
        int size2 = r3.size();
        byte[] bArr6 = this.A07;
        if (size2 + (bArr6 == null ? 0 : bArr6.length) > 0) {
            int size3 = r3.size();
            byte[] bArr7 = this.A07;
            int length3 = size3 + (bArr7 == null ? 0 : bArr7.length);
            if (length3 < 65280) {
                r5.AfG((byte) (length3 >> 8));
                r5.AfG((byte) length3);
            } else {
                r5.AfG((byte) -1);
                r5.AfG((byte) -2);
                r5.AfG((byte) (length3 >> 24));
                r5.AfG((byte) (length3 >> 16));
                r5.AfG((byte) (length3 >> 8));
                r5.AfG((byte) length3);
                i3 = 6;
            }
            byte[] bArr8 = this.A07;
            if (bArr8 != null) {
                r5.update(bArr8, 0, bArr8.length);
            }
            if (r3.size() > 0) {
                r5.update(r3.A00(), 0, r3.size());
            }
            int i6 = (i3 + length3) % 16;
            if (i6 != 0) {
                while (i6 != 16) {
                    r5.AfG((byte) 0);
                    i6++;
                }
            }
        }
        r5.update(bArr, i, i2);
        r5.A97(bArr2, 0);
    }

    @Override // X.AnonymousClass5XQ
    public int A97(byte[] bArr, int i) {
        int i2;
        int i3;
        C866848k r6 = this.A05;
        byte[] A00 = r6.A00();
        int size = r6.size();
        if (this.A03 != null) {
            byte[] bArr2 = this.A09;
            int length = bArr2.length;
            int i4 = 15 - length;
            if (i4 >= 4 || size < (1 << (i4 << 3))) {
                int i5 = this.A00;
                byte[] bArr3 = new byte[i5];
                bArr3[0] = (byte) ((i4 - 1) & 7);
                System.arraycopy(bArr2, 0, bArr3, 1, length);
                AnonymousClass5XE r7 = this.A02;
                AnonymousClass5O8 r5 = new AnonymousClass5O8(r7);
                r5.AIf(new C113075Fx(this.A03, bArr3), this.A06);
                boolean z = this.A06;
                int i6 = this.A01;
                if (z) {
                    i2 = i6 + size;
                    if (bArr.length >= i2 + i) {
                        byte[] bArr4 = this.A08;
                        A00(A00, bArr4, 0, size);
                        byte[] bArr5 = new byte[i5];
                        r5.AZY(bArr4, bArr5, 0, 0);
                        int i7 = 0;
                        int i8 = i;
                        while (true) {
                            i3 = 0 + size;
                            if (i7 >= i3 - i5) {
                                break;
                            }
                            r5.AZY(A00, bArr, i7, i8);
                            i8 += i5;
                            i7 += i5;
                        }
                        byte[] bArr6 = new byte[i5];
                        int i9 = i3 - i7;
                        System.arraycopy(A00, i7, bArr6, 0, i9);
                        r5.AZY(bArr6, bArr6, 0, 0);
                        System.arraycopy(bArr6, 0, bArr, i8, i9);
                        System.arraycopy(bArr5, 0, bArr, i + size, this.A01);
                    } else {
                        throw new C114975Nu("Output buffer too short.");
                    }
                } else if (size >= i6) {
                    int i10 = size - i6;
                    if (bArr.length >= i10 + i) {
                        int i11 = 0 + i10;
                        byte[] bArr7 = this.A08;
                        System.arraycopy(A00, i11, bArr7, 0, i6);
                        r5.AZY(bArr7, bArr7, 0, 0);
                        for (int i12 = this.A01; i12 != bArr7.length; i12++) {
                            bArr7[i12] = 0;
                        }
                        int i13 = 0;
                        int i14 = i;
                        while (i13 < i11 - i5) {
                            r5.AZY(A00, bArr, i13, i14);
                            i14 += i5;
                            i13 += i5;
                        }
                        byte[] bArr8 = new byte[i5];
                        int i15 = i10 - (i13 - 0);
                        System.arraycopy(A00, i13, bArr8, 0, i15);
                        r5.AZY(bArr8, bArr8, 0, 0);
                        System.arraycopy(bArr8, 0, bArr, i14, i15);
                        byte[] bArr9 = new byte[i5];
                        A00(bArr, bArr9, i, i10);
                        if (AnonymousClass1TT.A01(bArr7, bArr9)) {
                            i2 = i10;
                        } else {
                            throw new C114965Nt("mac check in CCM failed");
                        }
                    } else {
                        throw new C114975Nu("Output buffer too short.");
                    }
                } else {
                    throw new C114965Nt("data too short");
                }
                r7.reset();
                this.A04.reset();
                r6.reset();
                return i2;
            }
            throw C12960it.A0U("CCM packet too large for choice of q.");
        }
        throw C12960it.A0U("CCM cipher unitialized.");
    }

    @Override // X.AnonymousClass5XQ
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A02);
        return C12960it.A0d("/CCM", A0h);
    }

    @Override // X.AnonymousClass5XQ
    public void AIf(AnonymousClass20L r3, boolean z) {
        AnonymousClass20L r0;
        int length;
        this.A06 = z;
        if (r3 instanceof C113035Ft) {
            C113035Ft r32 = (C113035Ft) r3;
            this.A09 = AnonymousClass1TT.A02(r32.A03);
            this.A07 = AnonymousClass1TT.A02(r32.A02);
            int i = r32.A00;
            if (!z || (i >= 32 && i <= 128 && (i & 15) == 0)) {
                this.A01 = i >>> 3;
                r0 = r32.A01;
            } else {
                throw C12970iu.A0f("tag length in octets must be one of {4,6,8,10,12,14,16}");
            }
        } else if (r3 instanceof C113075Fx) {
            C113075Fx r33 = (C113075Fx) r3;
            this.A09 = r33.A01;
            this.A07 = null;
            this.A01 = 8;
            r0 = r33.A00;
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(r3), C12960it.A0k("invalid parameters passed to CCM: ")));
        }
        if (r0 != null) {
            this.A03 = r0;
        }
        byte[] bArr = this.A09;
        if (bArr == null || (length = bArr.length) < 7 || length > 13) {
            throw C12970iu.A0f("nonce must have length from 7 to 13 octets");
        }
        this.A02.reset();
        this.A04.reset();
        this.A05.reset();
    }
}
