package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextPaint;

/* renamed from: X.3Ff  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64353Ff {
    public Typeface A00;
    public boolean A01 = false;
    public final float A02;
    public final float A03;
    public final float A04;
    public final float A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final ColorStateList A09;
    public final ColorStateList A0A;
    public final ColorStateList A0B;
    public final ColorStateList A0C;
    public final String A0D;

    public C64353Ff(Context context, int i) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, C50572Qb.A0F);
        this.A05 = obtainStyledAttributes.getDimension(0, 0.0f);
        this.A0A = C50592Qd.A00(context, obtainStyledAttributes, 3);
        this.A0B = C50592Qd.A00(context, obtainStyledAttributes, 4);
        this.A0C = C50592Qd.A00(context, obtainStyledAttributes, 5);
        this.A07 = obtainStyledAttributes.getInt(2, 0);
        this.A08 = obtainStyledAttributes.getInt(1, 1);
        int i2 = obtainStyledAttributes.hasValue(12) ? 12 : 10;
        this.A06 = obtainStyledAttributes.getResourceId(i2, 0);
        this.A0D = obtainStyledAttributes.getString(i2);
        obtainStyledAttributes.getBoolean(14, false);
        this.A09 = C50592Qd.A00(context, obtainStyledAttributes, 6);
        this.A02 = obtainStyledAttributes.getFloat(7, 0.0f);
        this.A03 = obtainStyledAttributes.getFloat(8, 0.0f);
        this.A04 = obtainStyledAttributes.getFloat(9, 0.0f);
        obtainStyledAttributes.recycle();
    }

    public final void A00() {
        Typeface typeface;
        if (this.A00 == null) {
            String str = this.A0D;
            int i = this.A07;
            Typeface create = Typeface.create(str, i);
            this.A00 = create;
            if (create == null) {
                int i2 = this.A08;
                if (i2 == 1) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i2 == 2) {
                    typeface = Typeface.SERIF;
                } else if (i2 != 3) {
                    typeface = Typeface.DEFAULT;
                } else {
                    typeface = Typeface.MONOSPACE;
                }
                this.A00 = typeface;
                if (typeface != null) {
                    this.A00 = Typeface.create(typeface, i);
                }
            }
        }
    }

    public void A01(Context context, TextPaint textPaint, AnonymousClass08K r9) {
        int i;
        int i2;
        A02(context, textPaint, r9);
        ColorStateList colorStateList = this.A0A;
        if (colorStateList != null) {
            i = colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor());
        } else {
            i = -16777216;
        }
        textPaint.setColor(i);
        float f = this.A04;
        float f2 = this.A02;
        float f3 = this.A03;
        ColorStateList colorStateList2 = this.A09;
        if (colorStateList2 != null) {
            i2 = colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor());
        } else {
            i2 = 0;
        }
        textPaint.setShadowLayer(f, f2, f3, i2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(android.content.Context r8, android.text.TextPaint r9, X.AnonymousClass08K r10) {
        /*
            r7 = this;
            boolean r0 = r7.A01
            if (r0 != 0) goto L_0x0011
            r7.A00()
            r1 = r8
            boolean r0 = r8.isRestricted()
            if (r0 == 0) goto L_0x0020
            r0 = 1
            r7.A01 = r0
        L_0x0011:
            android.graphics.Typeface r0 = r7.A00
            r7.A03(r0, r9)
        L_0x0016:
            boolean r0 = r7.A01
            if (r0 != 0) goto L_0x001f
            android.graphics.Typeface r0 = r7.A00
            r7.A03(r0, r9)
        L_0x001f:
            return
        L_0x0020:
            int r4 = r7.A06     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            X.3hk r3 = new X.3hk     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            r3.<init>(r9, r10, r7)     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            boolean r0 = r8.isRestricted()     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            if (r0 == 0) goto L_0x0032
            r0 = -4
            r3.A00(r0)     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            goto L_0x0016
        L_0x0032:
            android.util.TypedValue r2 = new android.util.TypedValue     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            r2.<init>()     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            r5 = 0
            r6 = 0
            X.AnonymousClass00X.A03(r1, r2, r3, r4, r5, r6)     // Catch: UnsupportedOperationException | NotFoundException -> 0x0016, Exception -> 0x003d
            goto L_0x0016
        L_0x003d:
            r2 = move-exception
            java.lang.String r0 = "Error loading font "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = r7.A0D
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            java.lang.String r0 = "TextAppearance"
            android.util.Log.d(r0, r1, r2)
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64353Ff.A02(android.content.Context, android.text.TextPaint, X.08K):void");
    }

    public void A03(Typeface typeface, TextPaint textPaint) {
        textPaint.setTypeface(typeface);
        int style = (typeface.getStyle() ^ -1) & this.A07;
        textPaint.setFakeBoldText(C12960it.A1S(style & 1));
        float f = 0.0f;
        if ((style & 2) != 0) {
            f = -0.25f;
        }
        textPaint.setTextSkewX(f);
        textPaint.setTextSize(this.A05);
    }
}
