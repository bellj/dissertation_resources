package X;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.fragment.NoviAddDebitCardSheet;
import java.util.List;

/* renamed from: X.5nD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123475nD extends AbstractC118095bG {
    public C30881Ze A00;
    public final C14900mE A01;
    public final C129705yA A02;
    public final C27691It A03 = C13000ix.A03();

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C123475nD(X.C14900mE r16, X.C16590pI r17, X.AnonymousClass018 r18, X.C17070qD r19, X.C129865yQ r20, X.AnonymousClass60Y r21, X.AnonymousClass61F r22, X.C130105yo r23, X.C129705yA r24, X.C129685y8 r25, X.C129675y7 r26) {
        /*
            r15 = this;
            r2 = r17
            android.content.Context r1 = r2.A00
            r0 = 2131889760(0x7f120e60, float:1.9414193E38)
            java.lang.String r11 = r1.getString(r0)
            r0 = 2131889759(0x7f120e5f, float:1.941419E38)
            java.lang.String r12 = r1.getString(r0)
            r0 = 2131889756(0x7f120e5c, float:1.9414185E38)
            java.lang.String r13 = r1.getString(r0)
            java.lang.String r14 = "ADD_MONEY"
            r1 = r15
            r5 = r20
            r4 = r19
            r3 = r18
            r6 = r21
            r9 = r25
            r10 = r26
            r8 = r23
            r7 = r22
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            X.1It r0 = X.C13000ix.A03()
            r15.A03 = r0
            r0 = r16
            r15.A01 = r0
            r0 = r24
            r15.A02 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123475nD.<init>(X.0mE, X.0pI, X.018, X.0qD, X.5yQ, X.60Y, X.61F, X.5yo, X.5yA, X.5y8, X.5y7):void");
    }

    public static /* synthetic */ void A01(AbstractC001200n r2, C123475nD r3, List list) {
        r3.A09.A0B(Boolean.FALSE);
        AbstractC28901Pl A01 = AnonymousClass61F.A01(list);
        if (A01 instanceof C30881Ze) {
            r3.A00 = (C30881Ze) A01;
            super.A08(r2);
            return;
        }
        r3.A0A.A0B(new C129525xr(new AnonymousClass6MC() { // from class: X.6DD
            @Override // X.AnonymousClass6MC
            public final DialogFragment ANO(Activity activity) {
                PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
                paymentBottomSheet.A01 = new NoviAddDebitCardSheet();
                return paymentBottomSheet;
            }
        }));
    }

    @Override // X.AbstractC118095bG
    public Bundle A04(boolean z) {
        Bundle A04 = super.A04(z);
        A04.putBoolean("display-footer", true);
        return A04;
    }

    @Override // X.AbstractC118095bG
    public void A08(AbstractC001200n r4) {
        if (this.A00 != null) {
            super.A08(r4);
            return;
        }
        C117315Zl.A0Q(this.A09);
        C117315Zl.A0R(this.A01, ((AbstractC118095bG) this).A00, new AbstractC14590lg(r4, this) { // from class: X.6EZ
            public final /* synthetic */ AbstractC001200n A00;
            public final /* synthetic */ C123475nD A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C123475nD.A01(this.A00, this.A01, (List) obj);
            }
        });
    }
}
