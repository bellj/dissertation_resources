package X;

import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.util.Log;
import android.view.Surface;

/* renamed from: X.0Tr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06450Tr {
    public EGLConfig A00;
    public EGLContext A01;
    public EGLDisplay A02;

    public C06450Tr() {
        this(null, 0);
    }

    public C06450Tr(EGLContext eGLContext, int i) {
        EGLDisplay eGLDisplay = EGL14.EGL_NO_DISPLAY;
        this.A02 = eGLDisplay;
        EGLContext eGLContext2 = EGL14.EGL_NO_CONTEXT;
        this.A01 = eGLContext2;
        this.A00 = null;
        if (eGLDisplay == eGLDisplay) {
            EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
            this.A02 = eglGetDisplay;
            if (eglGetDisplay != EGL14.EGL_NO_DISPLAY) {
                int[] iArr = new int[2];
                if (EGL14.eglInitialize(eglGetDisplay, iArr, 0, iArr, 1)) {
                    EGLContext eGLContext3 = this.A01;
                    if (eGLContext3 == EGL14.EGL_NO_CONTEXT) {
                        int[] iArr2 = new int[13];
                        iArr2[0] = 12324;
                        iArr2[1] = 8;
                        iArr2[2] = 12323;
                        iArr2[3] = 8;
                        iArr2[4] = 12322;
                        iArr2[5] = 8;
                        iArr2[6] = 12321;
                        iArr2[7] = 8;
                        iArr2[8] = 12352;
                        iArr2[9] = 4;
                        iArr2[10] = 12344;
                        iArr2[11] = 0;
                        iArr2[12] = 12344;
                        iArr2[10] = 12610;
                        iArr2[11] = 1;
                        EGLConfig[] eGLConfigArr = new EGLConfig[1];
                        if (!EGL14.eglChooseConfig(this.A02, iArr2, 0, eGLConfigArr, 0, 1, new int[1], 0)) {
                            StringBuilder sb = new StringBuilder("unable to find RGB8888 / ");
                            sb.append(2);
                            sb.append(" EGLConfig");
                            Log.w("Grafika", sb.toString());
                        } else {
                            EGLConfig eGLConfig = eGLConfigArr[0];
                            if (eGLConfig != null) {
                                eGLContext3 = EGL14.eglCreateContext(this.A02, eGLConfig, eGLContext2, new int[]{12440, 2, 12344}, 0);
                                A00("eglCreateContext");
                                this.A00 = eGLConfig;
                                this.A01 = eGLContext3;
                            }
                        }
                        throw new RuntimeException("Unable to find a suitable EGLConfig");
                    }
                    int[] iArr3 = new int[1];
                    EGL14.eglQueryContext(this.A02, eGLContext3, 12440, iArr3, 0);
                    StringBuilder sb2 = new StringBuilder("EGLContext created, client version ");
                    sb2.append(iArr3[0]);
                    Log.d("Grafika", sb2.toString());
                    return;
                }
                this.A02 = null;
                throw new RuntimeException("unable to initialize EGL14");
            }
            throw new RuntimeException("unable to get EGL14 display");
        }
        throw new RuntimeException("EGL already set up");
    }

    public static final void A00(String str) {
        int eglGetError = EGL14.eglGetError();
        if (eglGetError != 12288) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": EGL error: 0x");
            sb.append(Integer.toHexString(eglGetError));
            throw new RuntimeException(sb.toString());
        }
    }

    public int A01(EGLSurface eGLSurface, int i) {
        int[] iArr = new int[1];
        EGL14.eglQuerySurface(this.A02, eGLSurface, i, iArr, 0);
        return iArr[0];
    }

    public EGLSurface A02(Object obj) {
        if ((obj instanceof Surface) || (obj instanceof SurfaceTexture)) {
            EGLSurface eglCreateWindowSurface = EGL14.eglCreateWindowSurface(this.A02, this.A00, obj, new int[]{12344}, 0);
            A00("eglCreateWindowSurface");
            if (eglCreateWindowSurface != null) {
                return eglCreateWindowSurface;
            }
            throw new RuntimeException("surface was null");
        }
        StringBuilder sb = new StringBuilder("invalid surface: ");
        sb.append(obj);
        throw new RuntimeException(sb.toString());
    }

    public void A03() {
        EGLDisplay eGLDisplay = this.A02;
        if (eGLDisplay != EGL14.EGL_NO_DISPLAY) {
            EGLSurface eGLSurface = EGL14.EGL_NO_SURFACE;
            EGL14.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, EGL14.EGL_NO_CONTEXT);
            EGL14.eglDestroyContext(this.A02, this.A01);
            EGL14.eglReleaseThread();
            EGL14.eglTerminate(this.A02);
        }
        this.A02 = EGL14.EGL_NO_DISPLAY;
        this.A01 = EGL14.EGL_NO_CONTEXT;
        this.A00 = null;
    }

    public void A04(EGLSurface eGLSurface) {
        if (this.A02 == EGL14.EGL_NO_DISPLAY) {
            Log.d("Grafika", "NOTE: makeCurrent w/o display");
        }
        if (!EGL14.eglMakeCurrent(this.A02, eGLSurface, eGLSurface, this.A01)) {
            throw new RuntimeException("eglMakeCurrent failed");
        }
    }

    public void A05(EGLSurface eGLSurface) {
        EGL14.eglDestroySurface(this.A02, eGLSurface);
    }

    public boolean A06(EGLSurface eGLSurface) {
        return EGL14.eglSwapBuffers(this.A02, eGLSurface);
    }

    public void finalize() {
        try {
            if (this.A02 != EGL14.EGL_NO_DISPLAY) {
                Log.w("Grafika", "WARNING: EglCore was not explicitly released -- state may be leaked");
                A03();
            }
        } finally {
            super.finalize();
        }
    }
}
