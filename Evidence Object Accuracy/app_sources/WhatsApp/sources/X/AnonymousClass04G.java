package X;

import android.view.Choreographer;

/* renamed from: X.04G  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass04G {
    public long A00 = -1;
    public long A01 = -1;
    public C04670Mp A02;
    public boolean A03 = false;
    public final Choreographer.FrameCallback A04;
    public final Choreographer A05;

    public AnonymousClass04G(Choreographer choreographer, C04670Mp r4) {
        this.A05 = choreographer;
        this.A02 = r4;
        this.A04 = new AnonymousClass0Vy(this);
    }
}
