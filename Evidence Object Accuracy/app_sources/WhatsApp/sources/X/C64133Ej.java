package X;

/* renamed from: X.3Ej  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64133Ej {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        String str = this.A05;
        String str2 = ((C64133Ej) obj).A05;
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return C12970iu.A08(this.A05, C12970iu.A1b());
    }
}
