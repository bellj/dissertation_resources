package X;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.5x0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128995x0 {
    public final AnonymousClass6MN A00;
    public final AtomicBoolean A01 = new AtomicBoolean(false);
    public final AtomicInteger A02 = new AtomicInteger(2);
    public final String[] A03 = new String[2];

    public C128995x0(AnonymousClass6MN r4) {
        this.A00 = r4;
    }

    public void A00(int i, String str) {
        String[] strArr = this.A03;
        strArr[i] = str;
        if (this.A02.decrementAndGet() == 0 && this.A01.compareAndSet(false, true)) {
            this.A00.AX8(strArr);
        }
    }
}
