package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import java.util.List;

/* renamed from: X.34X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34X extends AnonymousClass46v {
    public WaImageView A00;
    public C53202d5 A01;
    public boolean A02;
    public final AnonymousClass018 A03;

    public AnonymousClass34X(Context context, AnonymousClass018 r2) {
        super(context);
        A00();
        this.A03 = r2;
        A03();
    }

    @Override // X.AbstractC74143hO
    public void A00() {
        if (!this.A02) {
            this.A02 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass46z
    public View A01() {
        this.A01 = new C53202d5(getContext());
        FrameLayout.LayoutParams A0M = C12990iw.A0M();
        int A06 = C12980iv.A06(this);
        C42941w9.A0A(this.A01, this.A03, 0, 0, A06, 0);
        this.A01.setLayoutParams(A0M);
        return this.A01;
    }

    @Override // X.AnonymousClass46z
    public View A02() {
        this.A00 = new WaImageView(getContext());
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.search_attachment_icon_size);
        int A06 = C12980iv.A06(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize);
        layoutParams.setMargins(A06, A06, A06, A06);
        this.A00.setLayoutParams(layoutParams);
        this.A00.setScaleType(ImageView.ScaleType.FIT_CENTER);
        return this.A00;
    }

    public void setMessage(C16440p1 r11, List list) {
        String string;
        if (!TextUtils.isEmpty(r11.A16())) {
            string = r11.A16();
        } else {
            string = getContext().getString(R.string.untitled_document);
        }
        AnonymousClass018 r2 = this.A03;
        String A03 = C44891zj.A03(r2, ((AbstractC16130oV) r11).A01);
        String A01 = AbstractC15340mz.A01(r11);
        this.A01.setTitleAndDescription(string, null, list);
        boolean A012 = C28141Kv.A01(r2);
        C53202d5 r3 = this.A01;
        Context context = getContext();
        Object[] objArr = new Object[2];
        if (A012) {
            objArr[0] = A03;
            objArr[1] = A01;
        } else {
            objArr[0] = A01;
            objArr[1] = A03;
        }
        r3.setSubText(context.getString(R.string.file_attachment_size_and_ext, objArr), null);
        this.A00.setImageDrawable(C26511Dt.A02(getContext(), r11));
    }
}
