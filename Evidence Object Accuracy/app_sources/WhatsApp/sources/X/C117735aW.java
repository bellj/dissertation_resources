package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5aW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117735aW extends LinearLayout implements AnonymousClass004 {
    public View A00;
    public ImageView A01;
    public ImageView A02;
    public LinearLayout A03;
    public LinearLayout A04;
    public LinearLayout A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public AnonymousClass1J1 A09;
    public C21270x9 A0A;
    public AnonymousClass018 A0B;
    public AnonymousClass2P7 A0C;
    public boolean A0D;

    public C117735aW(Context context) {
        super(context);
        if (!this.A0D) {
            this.A0D = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A0A = C12970iu.A0W(A00);
            this.A0B = C12960it.A0R(A00);
        }
        C117305Zk.A14(C12960it.A0E(this), this, R.layout.india_payment_setting_header_row);
        this.A02 = C12970iu.A0L(this, R.id.profile_image);
        this.A08 = C12960it.A0J(this, R.id.profile_payment_name);
        this.A07 = C12960it.A0J(this, R.id.profile_payment_handler);
        this.A09 = this.A0A.A03(getContext(), "india-upi-payment-settings-header-row");
        this.A03 = (LinearLayout) findViewById(R.id.profile_container);
        this.A05 = (LinearLayout) findViewById(R.id.send_payment_container);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.scan_qr_container);
        this.A04 = linearLayout;
        this.A06 = C12960it.A0I(linearLayout, R.id.scan_qr);
        this.A00 = findViewById(R.id.divider_above_send_payment);
        this.A01 = C12970iu.A0K(this, R.id.profile_details_icon);
    }

    public void A00(C15370n3 r6, String str, String str2) {
        this.A03.setVisibility(0);
        this.A00.setVisibility(0);
        this.A09.A06(this.A02, r6);
        this.A08.setText(str);
        this.A07.setText(C12990iw.A0o(getResources(), str2, C12970iu.A1b(), 0, R.string.vpa_prefix));
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0C;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A0C = r0;
        }
        return r0.generatedComponent();
    }

    public LinearLayout getProfileContainer() {
        return this.A03;
    }

    public LinearLayout getScanQrContainer() {
        return this.A04;
    }

    public LinearLayout getSendPaymentContainer() {
        return this.A05;
    }

    public void setScanQrText(int i) {
        this.A06.setText(i);
    }
}
