package X;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.Me;
import com.whatsapp.util.Log;
import java.util.Date;

/* renamed from: X.0w5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20640w5 {
    public Date A00;
    public boolean A01;
    public final C15570nT A02;
    public final C18360sK A03;
    public final C14820m6 A04;

    public C20640w5(C15570nT r1, C18360sK r2, C14820m6 r3) {
        this.A02 = r1;
        this.A04 = r3;
        this.A03 = r2;
    }

    public static boolean A00() {
        return "chromium".equals(Build.MANUFACTURER) && "chromium".equals(Build.BRAND);
    }

    public Date A01() {
        SharedPreferences sharedPreferences = this.A04.A00;
        long j = sharedPreferences.getLong("software_forced_expiration", 0);
        long j2 = 0;
        if (j <= 0) {
            j = sharedPreferences.getLong("client_expiration_time", 0);
            if (j <= 0) {
                C15570nT r0 = this.A02;
                r0.A08();
                Me me = r0.A00;
                int i = -1;
                if (me != null) {
                    try {
                        if (!TextUtils.isEmpty(me.number)) {
                            i = (int) (Long.valueOf(me.number).longValue() % 14);
                        }
                    } catch (NumberFormatException e) {
                        StringBuilder sb = new StringBuilder("number format not valid: ");
                        sb.append(me.number);
                        Log.w(sb.toString(), e);
                    }
                }
                if (i >= 0 && i <= 13) {
                    j2 = (long) (i - 6);
                }
                return new Date(1659716253000L + ((180 + j2) * 86400000));
            }
        }
        return new Date(j);
    }

    public boolean A02() {
        if (this.A01) {
            return true;
        }
        boolean after = new Date().after(A01());
        this.A01 = after;
        return after;
    }

    public boolean A03() {
        boolean z = true;
        if (this.A00 == null) {
            Date date = new Date();
            if (date.before(new Date(1659543453000L)) || (z = date.after(new Date(A01().getTime() + 31536000000L)))) {
                this.A00 = date;
            }
        }
        return z;
    }
}
