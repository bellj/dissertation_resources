package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.util.Log;

/* renamed from: X.2Ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49222Ju implements AbstractC21730xt {
    public AnonymousClass2KP A00;
    public C41911uM A01;
    public boolean A02 = true;
    public boolean A03;
    public final AbstractC15710nm A04;
    public final C14900mE A05;
    public final AnonymousClass2KO A06;
    public final C14830m7 A07;
    public final C17220qS A08;

    public C49222Ju(AbstractC15710nm r2, C14900mE r3, AnonymousClass2KO r4, C14830m7 r5, C17220qS r6) {
        this.A07 = r5;
        this.A05 = r3;
        this.A04 = r2;
        this.A08 = r6;
        this.A06 = r4;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.i("devicePairRequest/onDeliveryFailure");
        this.A05.A0I(new RunnableBRunnable0Shape4S0100000_I0_4(this, 8));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        int A00 = C41151sz.A00(r5);
        StringBuilder sb = new StringBuilder("devicePairRequest/onError :");
        sb.append(A00);
        Log.i(sb.toString());
        this.A05.A0I(new RunnableBRunnable0Shape0S0101000_I0(this, A00, 7));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004b, code lost:
        if (r8 != null) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006e, code lost:
        if (r1.A04 == false) goto L_0x0070;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0087 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r20, java.lang.String r21) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C49222Ju.AX9(X.1V8, java.lang.String):void");
    }
}
