package X;

/* renamed from: X.1Q6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Q6 {
    public int A00;
    public boolean A01 = false;
    public boolean A02 = true;
    public boolean A03 = false;
    public boolean A04 = false;
    public final int A05;

    public AnonymousClass1Q6(int i) {
        this.A05 = i;
    }

    public boolean A00() {
        return this.A03 && this.A05 > 0;
    }
}
