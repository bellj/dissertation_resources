package X;

import java.util.List;

/* renamed from: X.3mC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC76773mC extends AbstractC76693m3 implements AbstractC116805Wy {
    public long A00;
    public AbstractC116805Wy A01;

    @Override // X.AbstractC116805Wy
    public List ABx(long j) {
        return this.A01.ABx(j - this.A00);
    }

    @Override // X.AbstractC116805Wy
    public long ACn(int i) {
        return this.A01.ACn(i) + this.A00;
    }

    @Override // X.AbstractC116805Wy
    public int ACo() {
        return this.A01.ACo();
    }

    @Override // X.AbstractC116805Wy
    public int AEd(long j) {
        return this.A01.AEd(j - this.A00);
    }

    @Override // X.AnonymousClass4YO
    public void clear() {
        this.flags = 0;
        this.A01 = null;
    }
}
