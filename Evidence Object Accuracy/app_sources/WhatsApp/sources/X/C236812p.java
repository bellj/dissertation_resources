package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.12p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C236812p {
    public C246616j A00;
    public final C18460sU A01;
    public final C16490p7 A02;
    public final AbstractC14440lR A03;

    public C236812p(C18460sU r1, C246616j r2, C16490p7 r3, AbstractC14440lR r4) {
        this.A01 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = r2;
    }

    public final ContentValues A00(AnonymousClass1YS r4) {
        long j;
        ContentValues contentValues = new ContentValues();
        contentValues.put("call_log_row_id", Long.valueOf(r4.A00));
        contentValues.put("call_id", r4.A03);
        contentValues.put("joinable_video_call", Boolean.valueOf(r4.A04));
        GroupJid groupJid = r4.A01;
        if (groupJid != null) {
            j = this.A01.A01(groupJid);
        } else {
            j = 0;
        }
        contentValues.put("group_jid_row_id", Long.valueOf(j));
        return contentValues;
    }

    public AnonymousClass1YS A01(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("call_log_row_id");
        if (columnIndex == -1 || cursor.isNull(columnIndex)) {
            return null;
        }
        long j = cursor.getLong(columnIndex);
        String string = cursor.getString(cursor.getColumnIndexOrThrow("call_id"));
        boolean z = false;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("joinable_video_call")) > 0) {
            z = true;
        }
        return new AnonymousClass1YS(GroupJid.of(this.A01.A03(cursor.getLong(cursor.getColumnIndexOrThrow("group_jid_row_id")))), string, j, z);
    }

    public AnonymousClass1YS A02(GroupJid groupJid) {
        AnonymousClass1YS r0;
        HashMap hashMap = this.A00.A01;
        synchronized (hashMap) {
            r0 = (AnonymousClass1YS) hashMap.get(groupJid);
        }
        return r0;
    }

    public AnonymousClass1YS A03(GroupJid groupJid) {
        boolean containsKey;
        AnonymousClass1YS r0;
        AnonymousClass1YS r02;
        C246616j r8 = this.A00;
        HashMap hashMap = r8.A01;
        synchronized (hashMap) {
            containsKey = hashMap.containsKey(groupJid);
        }
        if (containsKey) {
            synchronized (hashMap) {
                r02 = (AnonymousClass1YS) hashMap.get(groupJid);
            }
            return r02;
        }
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT call_id, call_log_row_id, joinable_video_call, group_jid_row_id FROM joinable_call_log WHERE group_jid_row_id = ? ", new String[]{String.valueOf(this.A01.A01(groupJid))});
            if (!A09.moveToLast() || (r0 = A01(A09)) == null) {
                synchronized (hashMap) {
                    r0 = null;
                    hashMap.put(groupJid, null);
                }
            } else {
                r8.A00(r0);
            }
            A09.close();
            A01.close();
            return r0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public AnonymousClass1YS A04(String str) {
        boolean containsKey;
        AnonymousClass1YS r0;
        AnonymousClass1YS r02;
        C246616j r6 = this.A00;
        HashMap hashMap = r6.A00;
        synchronized (hashMap) {
            containsKey = hashMap.containsKey(str);
        }
        if (containsKey) {
            synchronized (hashMap) {
                r02 = (AnonymousClass1YS) hashMap.get(str);
            }
            return r02;
        }
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT call_id, call_log_row_id, joinable_video_call, group_jid_row_id FROM joinable_call_log WHERE call_id = ? ", new String[]{str});
            if (!A09.moveToLast() || (r0 = A01(A09)) == null) {
                synchronized (hashMap) {
                    r0 = null;
                    hashMap.put(str, null);
                }
            } else {
                r6.A00(r0);
            }
            A09.close();
            A01.close();
            return r0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A05() {
        ArrayList arrayList = new ArrayList();
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT call_id, call_log_row_id, joinable_video_call, group_jid_row_id FROM joinable_call_log", null);
            while (A09.moveToNext()) {
                arrayList.add(AnonymousClass1SF.A0A(A09.getString(A09.getColumnIndexOrThrow("call_id"))));
            }
            A09.close();
            A01.close();
            return arrayList;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A06(AnonymousClass1YS r6) {
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A03(A00(r6), "joinable_call_log");
            this.A00.A00(r6);
            r6.A02 = false;
            A00.A00();
            StringBuilder sb = new StringBuilder();
            sb.append("JoinableCallLogStore/insertOnCurrentThread/inserted; joinableCallLog.callId=");
            sb.append(r6.A03);
            Log.i(sb.toString());
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A07(GroupJid groupJid) {
        boolean containsKey;
        HashMap hashMap = this.A00.A01;
        synchronized (hashMap) {
            containsKey = hashMap.containsKey(groupJid);
        }
        return containsKey;
    }
}
