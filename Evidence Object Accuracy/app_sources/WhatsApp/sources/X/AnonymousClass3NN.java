package X;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.chatinfo.view.custom.BusinessChatInfoLayout;
import java.io.File;

/* renamed from: X.3NN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NN implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass2VA A00;

    public AnonymousClass3NN(AnonymousClass2VA r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C30181Wk r3;
        String str;
        AnonymousClass2VA r5 = this.A00;
        AnonymousClass2Ew r6 = r5.A0Z;
        C12980iv.A1F(r6, this);
        C30141Wg r0 = r5.A0H;
        int measuredWidth = r6.getMeasuredWidth();
        if (r0 != null && (r3 = r0.A01) != null && (str = r3.A00) != null && r5.A0h.A07(601)) {
            C68193Ul r02 = r5.A0J;
            if (r02 != null) {
                if (!C29941Vi.A00(r02.A03.A00, str)) {
                    C63393Bk r03 = r5.A0l;
                    C68193Ul r1 = r5.A0J;
                    C58962tj r04 = r03.A00;
                    if (r04 != null) {
                        r04.A00(r1);
                    }
                } else {
                    return;
                }
            }
            C68193Ul r12 = new C68193Ul(new AnonymousClass4JU(r5), r3, measuredWidth, r6.A03(measuredWidth));
            r5.A0J = r12;
            C63393Bk r2 = r5.A0l;
            C58962tj r32 = r2.A00;
            if (r32 == null) {
                File file = new File(r2.A04.A00.getCacheDir(), "cover_photos");
                C68173Uj r7 = new C68173Uj();
                C14850m9 r8 = r2.A05;
                r32 = new C58962tj(r2.A01, r2.A02, r2.A03, r7, r8, r2.A06, r2.A07, r2.A08, file, "cover-photo-loader", 1);
                r2.A00 = r32;
            }
            r32.A01(r12, true);
        } else if (r6 instanceof BusinessChatInfoLayout) {
            ((ImageView) AnonymousClass028.A0D(r6, R.id.picture)).setImageDrawable(null);
            r6.A06();
        }
    }
}
