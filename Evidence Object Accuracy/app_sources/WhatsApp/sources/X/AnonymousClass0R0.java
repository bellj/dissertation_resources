package X;

import android.widget.PopupWindow;

/* renamed from: X.0R0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0R0 {
    public static void A00(PopupWindow popupWindow, int i) {
        popupWindow.setWindowLayoutType(i);
    }

    public static void A01(PopupWindow popupWindow, boolean z) {
        popupWindow.setOverlapAnchor(z);
    }
}
