package X;

import android.graphics.Paint;
import android.graphics.RectF;

/* renamed from: X.4Rw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91474Rw {
    public final float A00;
    public final float A01;
    public final Paint A02;
    public final RectF A03;

    public C91474Rw(Paint paint, float f, float f2, float f3, float f4, float f5, float f6) {
        this.A03 = new RectF(f, f2, f3, f4);
        this.A00 = f5;
        this.A01 = f6;
        this.A02 = paint;
    }
}
