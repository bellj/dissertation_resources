package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;
import com.whatsapp.util.Log;
import java.util.Set;

/* renamed from: X.1vS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42581vS {
    public final Context A00;
    public final C14900mE A01;
    public final C15450nH A02;
    public final C18850tA A03;
    public final C15550nR A04;
    public final AnonymousClass5U3 A05;
    public final C14830m7 A06;
    public final C20650w6 A07;
    public final C15600nX A08;
    public final C242114q A09;
    public final AnonymousClass132 A0A;
    public final C20710wC A0B;
    public final C22230yk A0C;
    public final C15860o1 A0D;
    public final C255719x A0E;
    public final AbstractC14440lR A0F;

    public C42581vS(Context context, C14900mE r3, C15450nH r4, C18850tA r5, C15550nR r6, AnonymousClass5U3 r7, C14830m7 r8, C20650w6 r9, C15600nX r10, C242114q r11, AnonymousClass132 r12, C20710wC r13, C22230yk r14, C15860o1 r15, C255719x r16, AbstractC14440lR r17) {
        this.A00 = context;
        this.A06 = r8;
        this.A01 = r3;
        this.A0F = r17;
        this.A07 = r9;
        this.A02 = r4;
        this.A03 = r5;
        this.A04 = r6;
        this.A0C = r14;
        this.A0B = r13;
        this.A0D = r15;
        this.A0A = r12;
        this.A09 = r11;
        this.A0E = r16;
        this.A08 = r10;
        this.A05 = r7;
    }

    public static /* synthetic */ void A00(AnonymousClass01F r7, C42581vS r8, AbstractC14640lm r9, boolean z) {
        C15370n3 A0B = r8.A04.A0B(r9);
        if (r7 == null) {
            return;
        }
        if (A0B.A0K()) {
            StringBuilder sb = new StringBuilder("conversations/delete/group:");
            sb.append(A0B);
            Log.i(sb.toString());
            if (r8.A0B.A0z) {
                r8.A01.A07(R.string.group_updating, 0);
                return;
            }
            if (!C15380n4.A0O(r9)) {
                C15600nX r1 = r8.A08;
                C15580nU A02 = C15580nU.A02(r9);
                AnonymousClass009.A05(A02);
                if (r1.A0C(A02)) {
                    r8.A0F.Aaz(new AnonymousClass392(new LeaveGroupsDialogFragment(), r7, r8.A0A, A0B, z), new Object[0]);
                    return;
                }
            }
            ConversationsFragment.DeleteGroupDialogFragment deleteGroupDialogFragment = new ConversationsFragment.DeleteGroupDialogFragment();
            AnonymousClass3GF.A00(deleteGroupDialogFragment, A0B);
            deleteGroupDialogFragment.A03().putBoolean("chatContainsStarredMessages", z);
            deleteGroupDialogFragment.A1F(r7, null);
        } else if (C15380n4.A0F(A0B.A0D)) {
            ConversationsFragment.DeleteBroadcastListDialogFragment deleteBroadcastListDialogFragment = new ConversationsFragment.DeleteBroadcastListDialogFragment();
            AnonymousClass3GF.A00(deleteBroadcastListDialogFragment, A0B);
            deleteBroadcastListDialogFragment.A03().putBoolean("chatContainsStarredMessages", z);
            deleteBroadcastListDialogFragment.A1F(r7, null);
        } else {
            r8.A0F.Aaz(new AnonymousClass392(new ConversationsFragment.DeleteContactDialogFragment(), r7, r8.A0A, A0B, z), new Object[0]);
        }
    }

    public void A01(AbstractC14640lm r4, long j) {
        if (!C15380n4.A0O(r4)) {
            C18850tA r2 = this.A03;
            Set A06 = r2.A06(r4, true);
            if (this.A0D.A09(r4, j) != null) {
                r2.A0O(A06);
                this.A0C.A0C(new C34081fY(r4, 11, j), 0);
                return;
            }
            r2.A0N(A06);
        }
    }
}
