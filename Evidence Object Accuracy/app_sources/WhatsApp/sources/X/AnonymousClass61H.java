package X;

import android.content.Context;
import android.os.Build;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;

/* renamed from: X.61H  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61H {
    public static C05000Nw A00(String str, String str2) {
        C05130Oj r1 = new C05130Oj();
        r1.A03 = str;
        r1.A01 = str2;
        r1.A04 = false;
        return r1.A00();
    }

    public static C05500Pu A01(ActivityC000800j r2, AnonymousClass0PW r3) {
        return new C05500Pu(r3, r2, AnonymousClass00T.A07(r2));
    }

    public static FingerprintBottomSheet A02() {
        return FingerprintBottomSheet.A01(R.string.novi_fingerprint_start, R.string.fingerprint_bottom_sheet_negative_button, 0, R.layout.novi_header, R.layout.novi_finger_print, 2131951985);
    }

    public static void A03(FingerprintBottomSheet fingerprintBottomSheet) {
        View view = ((AnonymousClass01E) fingerprintBottomSheet).A0A;
        if (view != null) {
            AnonymousClass028.A0D(view, R.id.fingerprint_icon).setVisibility(4);
            AnonymousClass028.A0D(view, R.id.fingerprint_button_container).setVisibility(4);
            AnonymousClass028.A0D(view, R.id.fingerprint_progressbar).setVisibility(0);
            C12960it.A0I(view, R.id.fingerprint_prompt).setText(R.string.fingerprint_recognized);
        }
    }

    public static boolean A04(Context context, C14850m9 r3) {
        return Build.VERSION.SDK_INT >= 23 && r3.A07(935) && new C06150Sj(new AnonymousClass0XW(context)).A03(15) == 0;
    }
}
