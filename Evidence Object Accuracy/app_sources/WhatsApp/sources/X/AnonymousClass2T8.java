package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.camera.bottomsheet.CameraMediaPickerFragment;
import com.whatsapp.util.Log;

/* renamed from: X.2T8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2T8 extends BroadcastReceiver {
    public final /* synthetic */ CameraMediaPickerFragment A00;

    public AnonymousClass2T8(CameraMediaPickerFragment cameraMediaPickerFragment) {
        this.A00 = cameraMediaPickerFragment;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        CameraMediaPickerFragment cameraMediaPickerFragment;
        boolean z;
        String str;
        String str2;
        String action = intent.getAction();
        if (action != null) {
            switch (action.hashCode()) {
                case -1514214344:
                    if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        Log.i("mediapickerfragment/receivemediabroadcast/ACTION_MEDIA_MOUNTED");
                        return;
                    }
                    return;
                case -1142424621:
                    if (action.equals("android.intent.action.MEDIA_SCANNER_FINISHED")) {
                        str = "mediapickerfragment/receivemediabroadcast/ACTION_MEDIA_SCANNER_FINISHED";
                        Log.i(str);
                        cameraMediaPickerFragment = this.A00;
                        z = false;
                        cameraMediaPickerFragment.A1H(z);
                        return;
                    }
                    return;
                case -963871873:
                    if (action.equals("android.intent.action.MEDIA_UNMOUNTED")) {
                        str2 = "mediapickerfragment/receivemediabroadcast/ACTION_MEDIA_UNMOUNTED";
                        Log.i(str2);
                        cameraMediaPickerFragment = this.A00;
                        z = true;
                        cameraMediaPickerFragment.A1H(z);
                        return;
                    }
                    return;
                case -625887599:
                    if (action.equals("android.intent.action.MEDIA_EJECT")) {
                        str2 = "mediapickerfragment/receivemediabroadcast/ACTION_MEDIA_EJECT";
                        Log.i(str2);
                        cameraMediaPickerFragment = this.A00;
                        z = true;
                        cameraMediaPickerFragment.A1H(z);
                        return;
                    }
                    return;
                case 1412829408:
                    if (action.equals("android.intent.action.MEDIA_SCANNER_STARTED")) {
                        str = "mediapickerfragment/receivemediabroadcast/ACTION_MEDIA_SCANNER_STARTED";
                        Log.i(str);
                        cameraMediaPickerFragment = this.A00;
                        z = false;
                        cameraMediaPickerFragment.A1H(z);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
