package X;

import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0100002_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;

/* renamed from: X.2JX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JX implements AbstractC467527b {
    public final /* synthetic */ AnonymousClass1s8 A00;

    public AnonymousClass2JX(AnonymousClass1s8 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC467527b
    public void AMh(float f, float f2) {
        this.A00.A0b.A0H(new RunnableBRunnable0Shape0S0100002_I0(this, f, f2, 0));
    }

    @Override // X.AbstractC467527b
    public void AMi(boolean z) {
        this.A00.A0b.A0H(new RunnableBRunnable0Shape0S0110000_I0(this, 3, z));
    }

    @Override // X.AbstractC467527b
    public void ANb(int i) {
        String str;
        AnonymousClass1s8 r3 = this.A00;
        r3.A0b.A0H(new RunnableBRunnable0Shape0S0101000_I0(this, i, 6));
        AnonymousClass1AX r5 = r3.A0e;
        boolean A03 = r3.A0s.A03();
        if (r5.A0A) {
            AbstractC21180x0 r1 = r5.A08;
            if (A03) {
                str = "in_call";
            } else {
                str = i != 2 ? i != 3 ? i != 4 ? "unknown" : "video" : "photo" : "evicted";
            }
            r1.AKw(554251647, "error_message", str);
        }
        r5.A05(4);
    }

    @Override // X.AbstractC467527b
    public void AUF() {
        long j;
        AnonymousClass1s8 r4 = this.A00;
        AnonymousClass1AX r3 = r4.A0e;
        AnonymousClass1s9 r2 = r4.A0A;
        int cameraType = r2.getCameraType();
        int i = !r2.AJS();
        Integer valueOf = Integer.valueOf(r2.getCameraApi());
        String flashMode = r2.getFlashMode();
        boolean z = r3.A0A;
        if (z) {
            r3.A02(554251647, "onPreviewReady");
            r3.A03(valueOf, 554251647, cameraType);
            r3.A00(554251647, i);
            r3.A08.AKw(554251647, "flash_mode", flashMode);
        }
        if (r4.A0V) {
            AnonymousClass1s9 r1 = r4.A0A;
            Integer valueOf2 = Integer.valueOf(r1.getCameraApi());
            int cameraType2 = r1.getCameraType();
            AnonymousClass30M r12 = new AnonymousClass30M();
            r12.A02 = Long.valueOf(SystemClock.elapsedRealtime() - r3.A03);
            r12.A01 = Integer.valueOf(cameraType2);
            r12.A00 = valueOf2;
            if (r3.A09) {
                r3.A07.A07(r12);
            }
            if (z) {
                r3.A03(valueOf2, 554250848, cameraType2);
                r3.A08.AL7(554250848, 2);
            }
            r4.A0V = false;
        } else {
            boolean z2 = r4.A0U;
            AnonymousClass1s9 r13 = r4.A0A;
            Integer valueOf3 = Integer.valueOf(r13.getCameraApi());
            int cameraType3 = r13.getCameraType();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (z2) {
                j = r3.A01;
            } else {
                j = r3.A02;
            }
            r3.A01 = 0;
            r3.A02 = 0;
            if (j != 0) {
                int intValue = valueOf3.intValue();
                int i2 = !z2 ? 1 : 0;
                AnonymousClass30Y r14 = new AnonymousClass30Y();
                r14.A02 = Integer.valueOf(i2);
                r14.A03 = Long.valueOf(elapsedRealtime - j);
                r14.A01 = Integer.valueOf(cameraType3);
                r14.A00 = Integer.valueOf(intValue);
                if (r3.A09) {
                    r3.A07.A07(r14);
                }
            }
        }
        r4.A0U = false;
        r4.A0b.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(this, 30));
        r3.A01(554251647, "onPreviewReady");
        r3.A05(2);
    }

    @Override // X.AbstractC467527b
    public void AUS(C49262Kb r5) {
        this.A00.A0b.A0H(new RunnableBRunnable0Shape0S1100000_I0(18, r5.A02, this));
    }

    @Override // X.AbstractC467527b
    public void AYD() {
        AnonymousClass1AX r4 = this.A00.A0e;
        r4.A04 = SystemClock.elapsedRealtime() - r4.A05;
        if (r4.A0A) {
            r4.A01(554249147, "start_video_capture");
            r4.A02(554249147, "video_record");
        }
    }
}
