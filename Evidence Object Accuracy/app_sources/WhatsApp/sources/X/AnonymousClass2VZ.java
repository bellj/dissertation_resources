package X;

import android.os.Build;
import android.view.View;
import android.widget.Button;
import com.whatsapp.R;

/* renamed from: X.2VZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VZ extends AnonymousClass04v {
    public final /* synthetic */ View A00;
    public final /* synthetic */ String A01 = "Button";

    public AnonymousClass2VZ(View view) {
        this.A00 = view;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        String str = this.A01;
        if (str.equals("Button")) {
            String name = Button.class.getName();
            if (Build.VERSION.SDK_INT < 23) {
                r6.A0F(this.A00.getContext().getString(R.string.accessibility_role_button));
            }
            r6.A02.setClassName(name);
            return;
        }
        StringBuilder sb = new StringBuilder("AccessibilityUtils/setRole/invalid role: ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
}
