package X;

import android.location.Address;
import android.location.Geocoder;
import com.facebook.redex.EmptyBaseRunnable0;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2if  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55512if extends EmptyBaseRunnable0 implements Runnable {
    public double A00;
    public double A01;
    public final /* synthetic */ AbstractC36001jA A02;

    public RunnableC55512if(AbstractC36001jA r1, double d, double d2) {
        this.A02 = r1;
        this.A00 = d;
        this.A01 = d2;
    }

    @Override // java.lang.Runnable
    public void run() {
        String str;
        List<Address> fromLocation;
        AbstractC36001jA r2 = this.A02;
        Address address = null;
        try {
            fromLocation = new Geocoder(r2.A0X.getApplicationContext(), C12970iu.A14(r2.A1A)).getFromLocation(this.A00, this.A01, 1);
        } catch (Exception unused) {
        }
        if (fromLocation != null && !fromLocation.isEmpty()) {
            address = fromLocation.get(0);
            StringBuilder A0h = C12960it.A0h();
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                if (i != 0) {
                    A0h.append(", ");
                }
                A0h.append(address.getAddressLine(i));
            }
            str = A0h.toString();
            r2.A0y.A0H(new RunnableBRunnable0Shape1S1200000_I1(address, this, str, 15));
        }
        str = r2.A0X.getString(R.string.location_no_address);
        r2.A0y.A0H(new RunnableBRunnable0Shape1S1200000_I1(address, this, str, 15));
    }
}
