package X;

import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.1f2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33761f2 {
    public static int A00(byte b, int i, boolean z) {
        if (b != 1) {
            int i2 = 5;
            if (b != 2) {
                if (b != 3) {
                    if (b == 4) {
                        return 7;
                    }
                    if (b == 5) {
                        return 6;
                    }
                    if (b == 9) {
                        return 8;
                    }
                    if (b == 16) {
                        return 14;
                    }
                    i2 = 20;
                    if (b == 20) {
                        return 16;
                    }
                    if (b == 23) {
                        return 18;
                    }
                    if (b == 35) {
                        return 21;
                    }
                    if (b == 49) {
                        return 28;
                    }
                    if (b == 52) {
                        return 29;
                    }
                    if (b != 57) {
                        if (b != 71) {
                            if (b != 62) {
                                if (b == 63) {
                                    return 8;
                                }
                                switch (b) {
                                    case 12:
                                        return 12;
                                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                        return 11;
                                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                        return 13;
                                    default:
                                        switch (b) {
                                            case 42:
                                                break;
                                            case 43:
                                                break;
                                            case 44:
                                                return 24;
                                            case 45:
                                                return 25;
                                            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                                                return 26;
                                            default:
                                                return z ? 9 : 1;
                                        }
                                }
                            }
                        }
                    }
                }
                return 3;
            } else if (i != 1) {
                return 4;
            }
            return i2;
        }
        return 2;
    }

    public static int A01(int i) {
        switch (i) {
            case 0:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return 1;
            case 1:
            case 10:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
            default:
                return 2;
            case 2:
                return 4;
            case 3:
                return 3;
            case 4:
                return 5;
            case 5:
                return 6;
            case 6:
                return 7;
            case 7:
                return 8;
            case 8:
                return 9;
            case 9:
                return 10;
            case 11:
                return 17;
            case 12:
                return 18;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return 11;
            case 18:
                return 20;
            case 19:
                return 21;
        }
    }

    public static Long A02(Long l, boolean z) {
        long j;
        double d;
        double d2;
        if (l == null) {
            return null;
        }
        if (!z) {
            return l;
        }
        long longValue = l.longValue();
        if (longValue < 100) {
            d = (double) longValue;
            d2 = 50.0d;
        } else if (longValue < 1000) {
            d = (double) longValue;
            d2 = 100.0d;
        } else if (longValue < 10000) {
            d = (double) longValue;
            d2 = 1000.0d;
        } else if (longValue <= 20000) {
            d = (double) longValue;
            d2 = 5000.0d;
        } else {
            j = C26061Bw.A0L;
            return Long.valueOf(j);
        }
        j = (long) (Math.ceil(d / d2) * d2);
        return Long.valueOf(j);
    }
}
