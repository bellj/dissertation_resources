package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.27S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27S implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ Runnable A01;

    public AnonymousClass27S(View view, Runnable runnable) {
        this.A00 = view;
        this.A01 = runnable;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        this.A00.getViewTreeObserver().removeOnPreDrawListener(this);
        this.A01.run();
        return true;
    }
}
