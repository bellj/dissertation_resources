package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.view.View;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.47u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C865347u extends AnonymousClass21T {
    public final VideoSurfaceView A00;

    @Override // X.AnonymousClass21T
    public Bitmap A03() {
        return null;
    }

    @Override // X.AnonymousClass21T
    public boolean A0D() {
        return false;
    }

    public C865347u(Context context, String str, boolean z) {
        C865447v r1 = new C865447v(context, this);
        this.A00 = r1;
        r1.setVideoPath(str);
        r1.A0A = new MediaPlayer.OnErrorListener() { // from class: X.3LW
            @Override // android.media.MediaPlayer.OnErrorListener
            public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                C865347u r3 = C865347u.this;
                StringBuilder A0k = C12960it.A0k("VideoPlayerOnSurfaceView/error ");
                A0k.append(i);
                Log.e(C12960it.A0e(" ", A0k, i2));
                AnonymousClass5V3 r0 = r3.A02;
                if (r0 == null) {
                    return false;
                }
                r0.APt(null, true);
                return false;
            }
        };
        r1.A09 = new MediaPlayer.OnCompletionListener() { // from class: X.4i2
            @Override // android.media.MediaPlayer.OnCompletionListener
            public final void onCompletion(MediaPlayer mediaPlayer) {
                C865347u r12 = C865347u.this;
                AnonymousClass5V2 r0 = r12.A01;
                if (r0 != null) {
                    r0.AOT(r12);
                }
            }
        };
        r1.setLooping(z);
    }

    @Override // X.AnonymousClass21T
    public int A01() {
        return this.A00.getCurrentPosition();
    }

    @Override // X.AnonymousClass21T
    public int A02() {
        return this.A00.getDuration();
    }

    @Override // X.AnonymousClass21T
    public View A04() {
        return this.A00;
    }

    @Override // X.AnonymousClass21T
    public void A05() {
        this.A00.pause();
    }

    @Override // X.AnonymousClass21T
    public void A07() {
        this.A00.start();
    }

    @Override // X.AnonymousClass21T
    public void A08() {
        this.A00.A00();
    }

    @Override // X.AnonymousClass21T
    public void A09(int i) {
        this.A00.seekTo(i);
    }

    @Override // X.AnonymousClass21T
    public void A0A(boolean z) {
        this.A00.setMute(z);
    }

    @Override // X.AnonymousClass21T
    public boolean A0B() {
        return this.A00.isPlaying();
    }

    @Override // X.AnonymousClass21T
    public boolean A0C() {
        return C72463ee.A0Y(this.A00.getCurrentPosition(), 50);
    }
}
