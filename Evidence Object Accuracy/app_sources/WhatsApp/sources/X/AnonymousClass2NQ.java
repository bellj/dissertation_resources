package X;

import android.text.TextUtils;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.2NQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2NQ {
    public final int A00;
    public final int A01;
    public final AnonymousClass2NR A02;
    public final List A03;
    public final boolean A04;
    public final int[] A05;

    public AnonymousClass2NQ(AnonymousClass2NR r1, List list, int[] iArr, int i, int i2, boolean z) {
        this.A00 = i;
        this.A04 = z;
        this.A01 = i2;
        this.A05 = iArr;
        this.A02 = r1;
        this.A03 = list;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0069, code lost:
        if (r0.intValue() == 0) goto L_0x006b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass2NQ A00(X.AnonymousClass01d r13, int r14) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2NQ.A00(X.01d, int):X.2NQ");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("json version: 1, api version: ");
        sb.append(this.A00);
        sb.append(", front camera: ");
        sb.append(this.A04);
        sb.append(", orientation: ");
        sb.append(this.A01);
        sb.append(", formats: ");
        sb.append(Arrays.toString(this.A05));
        sb.append(", preferred size: ");
        sb.append(this.A02);
        sb.append(", sizes: ");
        List list = this.A03;
        sb.append(list != null ? TextUtils.join(", ", list) : "null");
        return sb.toString();
    }
}
