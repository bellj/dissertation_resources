package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.calling.CallDetailsLayout;

/* renamed from: X.3f9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72773f9 extends AnimatorListenerAdapter {
    public final /* synthetic */ CallDetailsLayout A00;
    public final /* synthetic */ boolean A01;

    public C72773f9(CallDetailsLayout callDetailsLayout, boolean z) {
        this.A00 = callDetailsLayout;
        this.A01 = z;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        super.onAnimationCancel(animator);
        CallDetailsLayout callDetailsLayout = this.A00;
        callDetailsLayout.A00 = 0;
        callDetailsLayout.clearAnimation();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        CallDetailsLayout callDetailsLayout = this.A00;
        callDetailsLayout.A00 = 2;
        if (this.A01) {
            callDetailsLayout.setVisibility(8);
        } else {
            callDetailsLayout.A04.setVisibility(8);
        }
        callDetailsLayout.clearAnimation();
    }
}
