package X;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilMerchantDetailsListActivity;

/* renamed from: X.5jQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121695jQ extends AbstractActivityC121785jz {
    public C17070qD A00;
    public C118035bA A01;

    @Override // X.ActivityC121715je
    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        if (i == 300) {
            return new C122455lU(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.merchant_detail_payout_bank_view));
        }
        if (i == 301) {
            return new C122355lK(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.merchant_detail_card_payout_method_view));
        }
        if (i == 303) {
            return new C122255lA(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_expandable_listview));
        }
        if (i != 305) {
            return super.A2e(viewGroup, i);
        }
        return new C122425lR(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.merchant_warning_info_view));
    }

    @Override // X.ActivityC121715je, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        BrazilMerchantDetailsListActivity brazilMerchantDetailsListActivity = (BrazilMerchantDetailsListActivity) this;
        C118035bA r3 = (C118035bA) C117315Zl.A06(new C118275bY(brazilMerchantDetailsListActivity, brazilMerchantDetailsListActivity.A09), brazilMerchantDetailsListActivity).A00(C118035bA.class);
        brazilMerchantDetailsListActivity.A08 = r3;
        r3.A03.A05(r3.A07, C117305Zk.A0B(brazilMerchantDetailsListActivity, 13));
        C118035bA r32 = brazilMerchantDetailsListActivity.A08;
        this.A01 = r32;
        r32.A00.A05(r32.A07, C117305Zk.A0B(this, 70));
        C118035bA r33 = this.A01;
        r33.A04.A05(r33.A07, C117305Zk.A0B(this, 69));
        this.A01.A05(new C127105tx(0));
        ((ActivityC121715je) this).A01.setLockIconVisibility(false);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        CharSequence charSequence;
        boolean z = false;
        if (i == 200) {
            charSequence = getString(R.string.delete_seller_account_dialog_title);
        } else if (i != 201) {
            return super.onCreateDialog(i);
        } else {
            C17070qD r0 = this.A00;
            r0.A03();
            z = true;
            int size = r0.A08.A0T(1).size();
            int i2 = R.string.delete_seller_account_dialog_title;
            if (size > 0) {
                i2 = R.string.delete_seller_account_dialog_title_with_warning;
            }
            charSequence = AbstractC36671kL.A05(this, ((ActivityC13810kN) this).A0B, getString(i2));
        }
        String string = getString(R.string.remove);
        int i3 = 200;
        if (z) {
            i3 = 201;
        }
        C004802e A0S = C12980iv.A0S(this);
        A0S.A0A(charSequence);
        A0S.A0B(true);
        A0S.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(i3) { // from class: X.62a
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                C36021jC.A00(AbstractActivityC121695jQ.this, this.A00);
            }
        });
        A0S.A03(new DialogInterface.OnClickListener(i3, z) { // from class: X.62k
            public final /* synthetic */ int A00;
            public final /* synthetic */ boolean A02;

            {
                this.A00 = r2;
                this.A02 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i4) {
                AbstractActivityC121695jQ r3 = AbstractActivityC121695jQ.this;
                int i5 = this.A00;
                boolean z2 = this.A02;
                C36021jC.A00(r3, i5);
                C127105tx r1 = new C127105tx(2);
                r1.A01 = z2;
                r3.A01.A05(r1);
            }
        }, string);
        A0S.A08(new DialogInterface.OnCancelListener(i3) { // from class: X.62B
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                C36021jC.A00(AbstractActivityC121695jQ.this, this.A00);
            }
        });
        return A0S.create();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menuitem_remove_payment_method, 0, getString(R.string.remove_account));
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menuitem_remove_payment_method) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.A01.A05(new C127105tx(1));
        return true;
    }
}
