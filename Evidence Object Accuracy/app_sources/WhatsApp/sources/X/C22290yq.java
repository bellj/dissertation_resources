package X;

import com.whatsapp.util.Log;
import java.util.List;
import java.util.Random;

/* renamed from: X.0yq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22290yq {
    public final C16370ot A00;
    public final C16510p9 A01;
    public final C19990v2 A02;
    public final AnonymousClass10Y A03;
    public final C20050v8 A04;
    public final C16490p7 A05;
    public final C242214r A06;
    public final AnonymousClass134 A07;
    public final C242114q A08;

    public C22290yq(C16370ot r1, C16510p9 r2, C19990v2 r3, AnonymousClass10Y r4, C20050v8 r5, C16490p7 r6, C242214r r7, AnonymousClass134 r8, C242114q r9) {
        this.A01 = r2;
        this.A02 = r3;
        this.A07 = r8;
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = r1;
        this.A05 = r6;
        this.A08 = r9;
        this.A06 = r7;
    }

    public int A00(AbstractC14640lm r6) {
        AnonymousClass1PE A06 = this.A02.A06(r6);
        if (A06 == null) {
            return -1;
        }
        int nextInt = new Random().nextInt(999999) + 1;
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A06.A0A = nextInt;
            boolean A0I = this.A01.A0I(A06);
            A00.A00();
            A00.close();
            A02.close();
            if (!A0I) {
                StringBuilder sb = new StringBuilder("msgStore/updateWebModTag/none/");
                sb.append(r6);
                Log.e(sb.toString());
            }
            return nextInt;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public List A01(AbstractC14640lm r10, AnonymousClass1IS r11, Integer num, String str) {
        long A02;
        if (r11 == null) {
            A02 = this.A07.A05(r10);
        } else {
            A02 = C30041Vv.A02(this.A00.A03(r11));
        }
        if (A02 != Long.MIN_VALUE) {
            return A02(r10, num, str, A02);
        }
        StringBuilder sb = new StringBuilder("msgstore/get/newer no id for ");
        sb.append(r11);
        Log.i(sb.toString());
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (X.C15380n4.A0J(r9) != false) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A02(X.AbstractC14640lm r9, java.lang.Integer r10, java.lang.String r11, long r12) {
        /*
            r8 = this;
            boolean r0 = X.C15380n4.A0F(r9)
            r4 = 1
            r3 = 0
            if (r0 != 0) goto L_0x000f
            boolean r0 = X.C15380n4.A0J(r9)
            r2 = 1
            if (r0 == 0) goto L_0x0010
        L_0x000f:
            r2 = 0
        L_0x0010:
            X.0p7 r0 = r8.A05
            X.0on r5 = r0.get()
            r0.A04()     // Catch: all -> 0x0097
            X.1To r0 = r0.A05     // Catch: all -> 0x0097
            X.0op r6 = r5.A03     // Catch: all -> 0x0097
            java.lang.Boolean r0 = r0.A06(r6)     // Catch: all -> 0x0097
            boolean r0 = r0.booleanValue()     // Catch: all -> 0x0097
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x0097
            r1.<init>()     // Catch: all -> 0x0097
            X.C242214r.A01(r11, r1, r2, r0)     // Catch: all -> 0x0097
            java.lang.String r0 = " "
            r1.append(r0)     // Catch: all -> 0x0097
            X.C242214r.A02(r1, r3)     // Catch: all -> 0x0097
            if (r10 == 0) goto L_0x003f
            java.lang.String r0 = " LIMIT "
            r1.append(r0)     // Catch: all -> 0x0097
            r1.append(r10)     // Catch: all -> 0x0097
        L_0x003f:
            java.lang.String r7 = r1.toString()     // Catch: all -> 0x0097
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x0097
            X.0p9 r0 = r8.A01     // Catch: all -> 0x0097
            long r0 = r0.A02(r9)     // Catch: all -> 0x0097
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x0097
            r2[r3] = r0     // Catch: all -> 0x0097
            java.lang.String r0 = java.lang.String.valueOf(r12)     // Catch: all -> 0x0097
            r2[r4] = r0     // Catch: all -> 0x0097
            android.database.Cursor r2 = r6.A09(r7, r2)     // Catch: all -> 0x0097
            r5.close()
            if (r2 != 0) goto L_0x0073
            java.lang.String r1 = "msgstore/get/newer no newer messages for "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r12)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            r2 = 0
        L_0x0073:
            if (r2 != 0) goto L_0x0077
            r0 = 0
            return r0
        L_0x0077:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch: all -> 0x0092
            r1.<init>()     // Catch: all -> 0x0092
        L_0x007c:
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x0092
            if (r0 == 0) goto L_0x008e
            X.0ot r0 = r8.A00     // Catch: all -> 0x0092
            X.0mz r0 = r0.A02(r2, r9, r3, r4)     // Catch: all -> 0x0092
            if (r0 == 0) goto L_0x007c
            r1.add(r0)     // Catch: all -> 0x0092
            goto L_0x007c
        L_0x008e:
            r2.close()
            return r1
        L_0x0092:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0096
        L_0x0096:
            throw r0
        L_0x0097:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x009b
        L_0x009b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22290yq.A02(X.0lm, java.lang.Integer, java.lang.String, long):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        if (X.C15380n4.A0J(r7) != false) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A03(X.AnonymousClass1IS r14, java.lang.String r15, int r16) {
        /*
            r13 = this;
            X.0ot r8 = r13.A00
            X.0mz r0 = r8.A03(r14)
            long r11 = X.C30041Vv.A01(r0)
            r5 = 0
            r1 = 1
            int r0 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0023
            java.lang.String r1 = "msgstore/get/previous no id for "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r14)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            return r5
        L_0x0023:
            X.0lm r7 = r14.A00
            boolean r0 = X.C15380n4.A0F(r7)
            r6 = 1
            r4 = 0
            if (r0 != 0) goto L_0x0034
            boolean r0 = X.C15380n4.A0J(r7)
            r2 = 1
            if (r0 == 0) goto L_0x0035
        L_0x0034:
            r2 = 0
        L_0x0035:
            X.0p7 r0 = r13.A05
            X.0on r3 = r0.get()
            r0.A04()     // Catch: all -> 0x00c3
            X.1To r0 = r0.A05     // Catch: all -> 0x00c3
            X.0op r9 = r3.A03     // Catch: all -> 0x00c3
            java.lang.Boolean r0 = r0.A06(r9)     // Catch: all -> 0x00c3
            boolean r0 = r0.booleanValue()     // Catch: all -> 0x00c3
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x00c3
            r1.<init>()     // Catch: all -> 0x00c3
            X.C242214r.A01(r15, r1, r2, r0)     // Catch: all -> 0x00c3
            java.lang.String r0 = " "
            r1.append(r0)     // Catch: all -> 0x00c3
            java.lang.String r0 = " AND _id < ? "
            r1.append(r0)     // Catch: all -> 0x00c3
            java.lang.String r0 = " ORDER BY _id DESC"
            r1.append(r0)     // Catch: all -> 0x00c3
            java.lang.String r0 = " LIMIT "
            r1.append(r0)     // Catch: all -> 0x00c3
            r0 = r16
            r1.append(r0)     // Catch: all -> 0x00c3
            java.lang.String r10 = r1.toString()     // Catch: all -> 0x00c3
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x00c3
            X.0p9 r0 = r13.A01     // Catch: all -> 0x00c3
            long r0 = r0.A02(r7)     // Catch: all -> 0x00c3
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x00c3
            r2[r4] = r0     // Catch: all -> 0x00c3
            java.lang.String r0 = java.lang.String.valueOf(r11)     // Catch: all -> 0x00c3
            r2[r6] = r0     // Catch: all -> 0x00c3
            android.database.Cursor r2 = r9.A09(r10, r2)     // Catch: all -> 0x00c3
            if (r2 == 0) goto L_0x009f
            java.util.LinkedList r5 = new java.util.LinkedList     // Catch: all -> 0x00bc
            r5.<init>()     // Catch: all -> 0x00bc
        L_0x008f:
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x00bc
            if (r0 == 0) goto L_0x00b3
            X.0mz r0 = r8.A02(r2, r7, r4, r6)     // Catch: all -> 0x00bc
            if (r0 == 0) goto L_0x008f
            r5.addFirst(r0)     // Catch: all -> 0x00bc
            goto L_0x008f
        L_0x009f:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x00bc
            r1.<init>()     // Catch: all -> 0x00bc
            java.lang.String r0 = "msgstore/get/previous cursor null "
            r1.append(r0)     // Catch: all -> 0x00bc
            r1.append(r14)     // Catch: all -> 0x00bc
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x00bc
            com.whatsapp.util.Log.i(r0)     // Catch: all -> 0x00bc
        L_0x00b3:
            if (r2 == 0) goto L_0x00b8
            r2.close()     // Catch: all -> 0x00c3
        L_0x00b8:
            r3.close()
            return r5
        L_0x00bc:
            r0 = move-exception
            if (r2 == 0) goto L_0x00c2
            r2.close()     // Catch: all -> 0x00c2
        L_0x00c2:
            throw r0     // Catch: all -> 0x00c3
        L_0x00c3:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x00c7
        L_0x00c7:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22290yq.A03(X.1IS, java.lang.String, int):java.util.List");
    }
}
