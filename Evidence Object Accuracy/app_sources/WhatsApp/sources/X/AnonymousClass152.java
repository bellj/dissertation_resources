package X;

import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.152  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass152 {
    public static final C37831n5 A01 = new C37831n5(0, 0, true);
    public static final C37831n5 A02 = new C37831n5(0, 7, true);
    public static final C37841n6 A03 = new C37841n6(0, 0, 0, true);
    public static final C37841n6 A04 = new C37841n6(0, 5, 7, true);
    public static final byte[] A05 = {35, 33, 65, 77, 82, 10};
    public static final byte[] A06 = {35, 33, 65, 77, 82, 45, 87, 66, 10};
    public static final byte[] A07 = {79, 103, 103, 83};
    public static final byte[] A08 = {79, 112, 117, 115, 72, 101, 97, 100};
    public static final byte[] A09 = {82, 73, 70, 70};
    public static final byte[] A0A = {100, 97, 116, 97};
    public static final byte[] A0B = {102, 109, 116, 32};
    public static final byte[] A0C = {73, 68, 51};
    public static final byte[] A0D = {102, 116, 121, 112};
    public static final byte[] A0E = {51, 103};
    public static final byte[] A0F = {113, 116, 32, 32};
    public static final byte[] A0G = {87, 65, 86, 69};
    public static final int[] A0H = {1633973356, 1668637984, 1684108385, 1717658484, 1718449184, 1768846196, 1818321516, 1819572340, 1852798053, 1886155636, 1936552044};
    public final AbstractC15710nm A00;

    public static byte[] A0A(int i) {
        return new byte[]{(byte) ((i >> 24) & 255), (byte) ((i >> 16) & 255), (byte) ((i >> 8) & 255), (byte) (i & 255)};
    }

    public AnonymousClass152(AbstractC15710nm r1) {
        this.A00 = r1;
    }

    public static float A00(long j) {
        long j2 = j >> ((int) 16);
        float pow = (float) Math.pow(2.0d, (double) 16);
        return ((float) j2) + (((float) (j & (((long) pow) - 1))) / pow);
    }

    public static int A01(C37851n7 r11, InputStream inputStream) {
        try {
            int i = (int) (r11.A01 - r11.A02);
            byte[] bArr = new byte[i];
            if (A02(inputStream, bArr, 0, i) != i) {
                return 7;
            }
            byte b = bArr[4];
            if (b != 3) {
                StringBuilder sb = new StringBuilder();
                sb.append("Did not find esds description tag - found: ");
                sb.append((int) b);
                Log.i(sb.toString());
                return 7;
            }
            int[] A0C2 = A0C(bArr, 4, i);
            if (A0C2 == null) {
                Log.i("Did not find esds description details");
                return 7;
            }
            int i2 = A0C2[0] + 3;
            byte b2 = bArr[i2];
            int i3 = 1;
            int i4 = i2 + 1;
            if ((b2 & 128) == 128) {
                i4 += 2;
            }
            if ((b2 & 64) == 64) {
                i4 += bArr[i4] + 1;
            }
            if ((b2 & 32) == 32) {
                i4 += 2;
            }
            byte b3 = bArr[i4];
            if (b3 != 4) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Did not find esds config description tag - found: ");
                sb2.append((int) b3);
                Log.i(sb2.toString());
                return 7;
            }
            int[] A0C3 = A0C(bArr, i4, i);
            if (A0C3 == null) {
                Log.i("Did not find esds config details");
                return 7;
            }
            int i5 = A0C3[0];
            byte b4 = bArr[i5 + 1];
            if (b4 != 64) {
                if (b4 != 107) {
                    switch (b4) {
                        case 102:
                        case 103:
                        case 104:
                            break;
                        default:
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Did not find esds supported type - found: ");
                            sb3.append((int) b4);
                            Log.i(sb3.toString());
                            return 7;
                        case 105:
                            i3 = 2;
                            break;
                    }
                }
                i3 = 2;
            }
            byte b5 = bArr[i5 + 2];
            if (((b5 & 252) >> 2) == 5) {
                return i3;
            }
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Did not find stream type - found: ");
            sb4.append((int) b5);
            Log.i(sb4.toString());
            return 7;
        } catch (Exception e) {
            Log.i("Exception processing esds box: ", e);
            return 7;
        }
    }

    public static int A02(InputStream inputStream, byte[] bArr, int i, int i2) {
        int min = Math.min(i2, bArr.length - i);
        int i3 = 0;
        while (i3 < min) {
            int read = inputStream.read(bArr, i + i3, min - i3);
            if (read == -1) {
                break;
            }
            i3 += read;
        }
        return i3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:108:0x02f6, code lost:
        if (A09(r4, r12, 0) != false) goto L_0x02f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x0074, code lost:
        continue;
     */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0441 A[Catch: all -> 0x0479, TryCatch #1 {all -> 0x0479, blocks: (B:3:0x0009, B:5:0x0016, B:7:0x0025, B:9:0x002f, B:12:0x0061, B:13:0x0066, B:14:0x0074, B:16:0x007a, B:18:0x008c, B:19:0x0094, B:21:0x009a, B:24:0x00b2, B:27:0x00d0, B:30:0x00eb, B:38:0x0119, B:41:0x0135, B:44:0x0155, B:47:0x0175, B:50:0x01a4, B:52:0x01a8, B:53:0x01c1, B:56:0x01ef, B:57:0x01f4, B:58:0x020e, B:59:0x0217, B:65:0x0224, B:66:0x022c, B:68:0x0235, B:71:0x0242, B:72:0x024d, B:74:0x025a, B:78:0x026c, B:80:0x0272, B:82:0x027d, B:84:0x0285, B:88:0x0296, B:90:0x029e, B:93:0x02c7, B:95:0x02cd, B:97:0x02d1, B:99:0x02d8, B:101:0x02df, B:103:0x02e5, B:104:0x02ea, B:105:0x02ef, B:107:0x02f2, B:109:0x02f8, B:111:0x030c, B:112:0x0314, B:114:0x031c, B:115:0x0329, B:116:0x032c, B:117:0x0330, B:118:0x0335, B:120:0x033f, B:121:0x034d, B:129:0x036e, B:130:0x0379, B:131:0x039d, B:132:0x03a0, B:133:0x03a3, B:136:0x03ad, B:138:0x03ce, B:140:0x03d7, B:143:0x03e4, B:144:0x0401, B:146:0x0406, B:150:0x0414, B:151:0x0422, B:155:0x0428, B:157:0x042f, B:160:0x0438, B:162:0x0441, B:164:0x0445, B:166:0x044c, B:168:0x044f, B:170:0x0457, B:173:0x0461, B:176:0x046a, B:177:0x0470, B:178:0x0473), top: B:187:0x0009, inners: #0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C37831n5 A03(java.io.File r27) {
        /*
        // Method dump skipped, instructions count: 1150
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass152.A03(java.io.File):X.1n5");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:194:0x048b, code lost:
        r0 = "multiple hldr audio tracks found - not dolby";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x0263, code lost:
        continue;
     */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x03e7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x020c  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0213  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C37841n6 A04(java.io.File r34, boolean r35) {
        /*
        // Method dump skipped, instructions count: 1180
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass152.A04(java.io.File, boolean):X.1n6");
    }

    public static C37851n7 A05(InputStream inputStream, int[] iArr, long j, long j2) {
        long j3;
        long j4 = j2;
        new String(A0A(iArr[0]));
        long j5 = 0;
        if (j > 0) {
            if (j2 <= 0 || j <= j2) {
                A08(inputStream, j);
                if (j2 == -1) {
                    j4 = -1;
                } else {
                    j4 = j2 - j;
                }
            } else {
                throw new IOException("Not enough bytes to skip");
            }
        }
        int i = 8;
        byte[] bArr = new byte[8];
        while (true) {
            if (j4 != -1 && j4 <= j5) {
                return null;
            }
            int A022 = A02(inputStream, bArr, 0, i);
            if (A022 >= i) {
                long j6 = (long) A022;
                if (j4 == -1) {
                    j3 = -1;
                } else {
                    j3 = j4 - j6;
                }
                for (int i2 = 4; i2 < i; i2++) {
                    byte b = bArr[i2];
                    if (b < 32) {
                        StringBuilder sb = new StringBuilder("Found non character data in box type ");
                        sb.append((int) b);
                        Log.i(sb.toString());
                        return null;
                    }
                }
                int i3 = ((bArr[0] & 255) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255);
                new String(bArr, 4, 4);
                if (i3 == 0) {
                    i3 = -1;
                } else if (i3 == 1) {
                    byte[] bArr2 = new byte[i];
                    A022 += A02(inputStream, bArr2, 0, i);
                    if (A022 == 16) {
                        long j7 = (long) A022;
                        if (j3 == -1) {
                            j3 = -1;
                        } else {
                            j3 -= j7;
                        }
                        long j8 = ((long) (((bArr2[4] & 255) << 24) | ((bArr2[5] & 255) << 16) | ((bArr2[6] & 255) << 8) | (bArr2[7] & 255))) | (((long) (((((bArr2[0] & 255) << 24) | ((bArr2[1] & 255) << 16)) | ((bArr2[2] & 255) << 8)) | (bArr2[3] & 255))) << 32);
                        if (j8 <= 2147483647L) {
                            i3 = (int) j8;
                        } else {
                            StringBuilder sb2 = new StringBuilder("Length of box too long to be processed: ");
                            sb2.append(j8);
                            throw new IOException(sb2.toString());
                        }
                    } else {
                        throw new IOException("End of file looking for wide box length");
                    }
                }
                if (j3 <= 0 || j3 >= ((long) (i3 - A022))) {
                    int i4 = ((bArr[4] & 255) << 24) | ((bArr[5] & 255) << 16) | ((bArr[6] & 255) << 8) | (bArr[7] & 255);
                    boolean A032 = C37871n9.A03(iArr, i4);
                    byte[] A0A2 = A0A(i4);
                    if (A032) {
                        new String(A0A2);
                        return new C37851n7(i3, i4, A022);
                    }
                    new String(A0A2);
                    long j9 = (long) (i3 - A022);
                    A08(inputStream, j9);
                    j4 = j3 == -1 ? -1 : j3 - j9;
                    j5 = 0;
                    i = 8;
                } else {
                    StringBuilder sb3 = new StringBuilder("Length of box too long to be in current input: ");
                    sb3.append(i3);
                    sb3.append('>');
                    sb3.append(j3);
                    throw new IOException(sb3.toString());
                }
            } else {
                throw new IOException("End of file looking for box header");
            }
        }
    }

    public static String A06(C37831n5 r3) {
        switch (r3.A00) {
            case 0:
            case 4:
            case 7:
            case 8:
                Log.w("unsupported audio type; returning null mime type");
                StringBuilder sb = new StringBuilder("Audio type not supported: ");
                sb.append(r3.A00);
                throw new C37881nA(sb.toString());
            case 1:
                if (r3.A01 != 2) {
                    return "audio/aac";
                }
                return "audio/mp4";
            case 2:
                if (r3.A01 != 2) {
                    return "audio/mpeg";
                }
                return "audio/mp4";
            case 3:
                return "audio/amr";
            case 5:
                return "audio/ogg; codecs=opus";
            case 6:
            default:
                StringBuilder sb2 = new StringBuilder("invalid audio type returned; ");
                sb2.append(r3);
                throw new AssertionError(sb2.toString());
        }
    }

    public static String A07(String str, boolean z) {
        String obj;
        if (TextUtils.isEmpty(str)) {
            if (z) {
                obj = "empty audio mime type";
                Log.w(obj);
            }
            return null;
        } else if ("audio/aac".equals(str)) {
            return "aac";
        } else {
            if ("audio/mp4".equals(str)) {
                return "m4a";
            }
            if ("audio/amr".equals(str)) {
                return "amr";
            }
            if ("audio/mpeg".equals(str)) {
                return "mp3";
            }
            if ("audio/ogg; codecs=opus".equals(str)) {
                return "opus";
            }
            if (z) {
                StringBuilder sb = new StringBuilder("unrecognized audio mime type; mimeType=");
                sb.append(str);
                obj = sb.toString();
                Log.w(obj);
            }
            return null;
        }
    }

    public static void A08(InputStream inputStream, long j) {
        byte[] bArr = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
        while (j > 0) {
            long j2 = (long) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
            if (A02(inputStream, bArr, 0, (int) Math.min(j2, j)) != -1) {
                j -= j2;
            } else {
                StringBuilder sb = new StringBuilder("Unexpected EOF skipping ");
                sb.append(j);
                throw new IOException(sb.toString());
            }
        }
    }

    public static boolean A09(byte[] bArr, byte[] bArr2, int i) {
        int length = bArr.length - i;
        int length2 = bArr2.length;
        if (length >= length2) {
            for (int i2 = 0; i2 < length2; i2++) {
                if (bArr[i + i2] == bArr2[i2]) {
                }
            }
            return true;
        }
        return false;
    }

    public static int[] A0B(InputStream inputStream) {
        int[] iArr;
        int i;
        byte[] bArr = new byte[2];
        if (A02(inputStream, bArr, 0, 2) == 2) {
            int i2 = ((bArr[0] & 255) << 8) | (bArr[1] & 255);
            StringBuilder sb = new StringBuilder("mp4a box version: ");
            sb.append(i2);
            Log.i(sb.toString());
            if (i2 == 0) {
                iArr = new int[2];
                iArr[0] = 0;
                i = 18;
            } else if (i2 == 1) {
                iArr = new int[2];
                iArr[0] = 1;
                i = 34;
            } else if (i2 == 2) {
                iArr = new int[2];
                iArr[0] = 2;
                i = 54;
            } else {
                throw new IOException("Unexpected result getting mp4a version");
            }
            iArr[1] = i;
            return iArr;
        }
        throw new IOException("Unexpected eof getting mp4a version");
    }

    public static int[] A0C(byte[] bArr, int i, int i2) {
        try {
            int min = Math.min(i2, i + 4);
            int i3 = i;
            int i4 = 0;
            do {
                i3++;
                i4 = (i4 << 7) + (bArr[i3] & Byte.MAX_VALUE);
                if (i >= min) {
                    break;
                }
            } while ((bArr[i3] & 128) == 128);
            if (i4 == 0) {
                return null;
            }
            return new int[]{i3, i4};
        } catch (Exception e) {
            Log.i("Exception processing esds box: ", e);
            return null;
        }
    }

    public boolean A0D(File file) {
        C37831n5 A032 = A03(file);
        switch (A032.A00) {
            case 0:
            case 4:
            case 7:
            case 8:
                return false;
            case 1:
            case 2:
            case 3:
            case 5:
                return true;
            case 6:
            default:
                StringBuilder sb = new StringBuilder("invalid audio file type returned; ");
                sb.append(A032);
                throw new AssertionError(sb.toString());
        }
    }

    public boolean A0E(File file) {
        C37841n6 A042 = A04(file, false);
        int i = A042.A01;
        if (i != 2 && i != 3) {
            return false;
        }
        int i2 = A042.A00;
        if (i2 != 4 && i2 != 2 && i2 != 1 && i2 != 0 && i2 != 8) {
            return false;
        }
        int i3 = A042.A02;
        return i3 == 2 || i3 == 1 || i3 == 3;
    }
}
