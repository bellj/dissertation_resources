package X;

import java.util.List;

/* renamed from: X.0dc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09850dc implements Runnable {
    public final /* synthetic */ AnonymousClass0EJ A00;
    public final /* synthetic */ AnonymousClass0Q9 A01;
    public final /* synthetic */ List A02;

    public RunnableC09850dc(AnonymousClass0EJ r1, AnonymousClass0Q9 r2, List list) {
        this.A00 = r1;
        this.A02 = list;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        List list = this.A02;
        AnonymousClass0Q9 r2 = this.A01;
        if (list.contains(r2)) {
            list.remove(r2);
            r2.A01.A02(r2.A04.A0A);
        }
    }
}
