package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1BG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BG {
    public final C15570nT A00;
    public final C22640zP A01;
    public final C19990v2 A02;
    public final C15600nX A03;
    public final C18770sz A04;

    public AnonymousClass1BG(C15570nT r1, C22640zP r2, C19990v2 r3, C15600nX r4, C18770sz r5) {
        this.A00 = r1;
        this.A02 = r3;
        this.A04 = r5;
        this.A01 = r2;
        this.A03 = r4;
    }

    public AnonymousClass1YM A00(C15580nU r10) {
        AnonymousClass1YM r3 = new AnonymousClass1YM(r10);
        C15580nU A01 = A01(r10);
        if (A01 != null) {
            C15600nX r1 = this.A03;
            if (r1.A0D(r10)) {
                Iterator it = r1.A02(A01).A07().iterator();
                while (it.hasNext()) {
                    AnonymousClass1YO r12 = (AnonymousClass1YO) it.next();
                    r3.A03(r12.A03, new HashSet(AnonymousClass1JO.A00(r12.A04.keySet()).A00), 0, r12.A02, true);
                }
            }
        }
        C15600nX r2 = this.A03;
        r3.A0F(r2.A02(r10).A05(new AnonymousClass03D() { // from class: X.4rf
            @Override // X.AnonymousClass03D
            public final boolean Aek(Object obj) {
                return C12960it.A1S(((AnonymousClass1YO) obj).A01);
            }
        }).A02.values());
        if (r2.A0D(r10)) {
            return r3;
        }
        C15570nT r0 = this.A00;
        r0.A08();
        C27631Ih r4 = r0.A05;
        if (!r3.A0H(r0) && r4 != null) {
            r3.A03(r4, this.A04.A0D(r4), 0, false, true);
        }
        return r3.A05(new AnonymousClass03D() { // from class: X.4rh
            @Override // X.AnonymousClass03D
            public final boolean Aek(Object obj) {
                AnonymousClass1BG r13 = AnonymousClass1BG.this;
                AnonymousClass1YO r32 = (AnonymousClass1YO) obj;
                if (r32 == null) {
                    return false;
                }
                if (r32.A01 != 0 || r13.A00.A0F(r32.A03)) {
                    return true;
                }
                return false;
            }
        });
    }

    public final C15580nU A01(C15580nU r5) {
        for (AnonymousClass1OU r2 : this.A01.A0D.A01(r5)) {
            if (r2.A00 == 3) {
                return C15580nU.A02(r2.A02);
            }
        }
        return null;
    }

    public Set A02(C15580nU r4) {
        C15580nU A01;
        HashSet hashSet = new HashSet();
        if (this.A02.A02(r4) == 1) {
            hashSet.add(r4);
            if (this.A03.A0D(r4) && (A01 = A01(r4)) != null) {
                hashSet.add(A01);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }
}
