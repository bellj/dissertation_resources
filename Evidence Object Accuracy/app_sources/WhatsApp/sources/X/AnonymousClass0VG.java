package X;

import android.os.Handler;
import android.os.Message;

/* renamed from: X.0VG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VG implements Handler.Callback {
    public final /* synthetic */ C08770bp A00;

    public /* synthetic */ AnonymousClass0VG(C08770bp r1) {
        this.A00 = r1;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (!AnonymousClass3J3.A03()) {
            AnonymousClass0AT r2 = this.A00.A02;
            r2.A00(r2.obtainMessage(message.what, message.obj));
            return true;
        }
        int i = message.what;
        if (i == 1) {
            this.A00.A0A((AnonymousClass0HM) message.obj);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            this.A00.A09((AnonymousClass0HL) message.obj);
            return true;
        }
    }
}
