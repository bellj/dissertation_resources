package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.1uO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41931uO implements AbstractC21730xt {
    public final long A00;
    public final long A01;
    public final AnonymousClass10B A02;
    public final C17220qS A03;
    public final boolean A04;

    public C41931uO(AnonymousClass10B r1, C17220qS r2, long j, long j2, boolean z) {
        this.A03 = r2;
        this.A04 = z;
        this.A00 = j;
        this.A01 = j2;
        this.A02 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A02.A04.A00.edit().putBoolean("adv_key_index_list_require_update", true).apply();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        AnonymousClass1V8 A0E = r4.A0E("error");
        int i = -1;
        if (A0E != null) {
            i = A0E.A05("code", -1);
        }
        this.A02.A01(i);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r12, String str) {
        long j;
        AnonymousClass1V8 A0E = r12.A0E("retry-ts");
        if (A0E != null) {
            String A0I = A0E.A0I("ts", null);
            if (!TextUtils.isEmpty(A0I)) {
                j = C28421Nd.A01(A0I, -1);
            } else {
                j = -1;
            }
            if (this.A04 || j == -1) {
                this.A02.A01(-1);
                return;
            }
            AnonymousClass10B r2 = this.A02;
            long j2 = this.A01;
            StringBuilder sb = new StringBuilder("DeviceKeyIndexListUpdateHandler/onRetry advTs=");
            sb.append(j);
            sb.append(" serverTs=");
            sb.append(j2);
            Log.e(sb.toString());
            r2.A02(j, j2, true);
            return;
        }
        AnonymousClass10B r5 = this.A02;
        long j3 = this.A00;
        long j4 = this.A01;
        r5.A02.A05(j3);
        C22100yW r10 = r5.A06;
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1JU r7 : r10.A07()) {
            long j5 = r7.A01;
            if (j5 > 0 && j5 < j4) {
                arrayList.add(r7.A05);
            }
        }
        r10.A0I.A03(AnonymousClass1JO.A00(arrayList));
    }
}
