package X;

import java.util.concurrent.Callable;

/* renamed from: X.6KR  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KR implements Callable {
    public final /* synthetic */ C128425w5 A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KR(C128425w5 r1, AnonymousClass661 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C128965wx r2 = this.A01.A0L;
        C128425w5 r1 = this.A00;
        if (r2.A00.A00()) {
            AnonymousClass61K.A00(new RunnableC135156Hj(r1, r2));
        }
        r2.A01.A01(r1);
        return null;
    }
}
