package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3B4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3B4 {
    public static final Map A00;

    static {
        HashMap A11 = C12970iu.A11();
        A00 = A11;
        A11.put(C17930rd.A0F, Collections.emptySet());
        C17930rd r1 = C17930rd.A0E;
        A11.put(r1, Collections.singleton(r1));
        C17930rd r12 = C17930rd.A0D;
        A11.put(r12, Collections.singleton(r12));
        C17930rd r5 = C468427x.A02;
        C17930rd[] r0 = new C17930rd[2];
        r0[0] = r5;
        C17930rd r2 = C468427x.A01;
        A11.put(r5, C12970iu.A13(r2, r0, 1));
        C17930rd[] r02 = new C17930rd[2];
        r02[0] = r5;
        A11.put(r2, C12970iu.A13(r2, r02, 1));
    }
}
