package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1Jk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27831Jk extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C27831Jk A0R;
    public static volatile AnonymousClass255 A0S;
    public int A00;
    public long A01;
    public AnonymousClass25Z A02;
    public C34431g8 A03;
    public C34621gR A04;
    public C34661gV A05;
    public C34491gE A06;
    public C34341fz A07;
    public C34401g5 A08;
    public C34451gA A09;
    public AnonymousClass25Y A0A;
    public AnonymousClass25X A0B;
    public C34701gZ A0C;
    public C34511gG A0D;
    public C34601gP A0E;
    public C34641gT A0F;
    public C34681gX A0G;
    public C34561gL A0H;
    public AnonymousClass25W A0I;
    public AnonymousClass25V A0J;
    public AnonymousClass25U A0K;
    public C34581gN A0L;
    public C57642nO A0M;
    public AnonymousClass25T A0N;
    public C34541gJ A0O;
    public C34471gC A0P;
    public AnonymousClass25R A0Q;

    static {
        C27831Jk r0 = new C27831Jk();
        A0R = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r12, Object obj, Object obj2) {
        C82473vg r1;
        AnonymousClass26M r13;
        AnonymousClass26O r14;
        AnonymousClass26J r15;
        C464725z r16;
        AnonymousClass260 r17;
        AnonymousClass26D r18;
        AnonymousClass26N r19;
        AnonymousClass264 r110;
        AnonymousClass262 r111;
        AnonymousClass26A r112;
        AnonymousClass266 r113;
        AnonymousClass265 r114;
        AnonymousClass261 r115;
        AnonymousClass269 r116;
        AnonymousClass267 r117;
        AnonymousClass268 r118;
        AnonymousClass26G r119;
        AnonymousClass26F r120;
        AnonymousClass26E r121;
        AnonymousClass26H r122;
        AnonymousClass26C r123;
        AnonymousClass26B r124;
        AnonymousClass263 r125;
        AnonymousClass26I r126;
        switch (r12.ordinal()) {
            case 0:
                return A0R;
            case 1:
                AbstractC462925h r4 = (AbstractC462925h) obj;
                C27831Jk r142 = (C27831Jk) obj2;
                this.A01 = r4.Afs(this.A01, r142.A01, A0b(), r142.A0b());
                this.A0L = (C34581gN) r4.Aft(this.A0L, r142.A0L);
                this.A06 = (C34491gE) r4.Aft(this.A06, r142.A06);
                this.A0E = (C34601gP) r4.Aft(this.A0E, r142.A0E);
                this.A0F = (C34641gT) r4.Aft(this.A0F, r142.A0F);
                this.A0K = (AnonymousClass25U) r4.Aft(this.A0K, r142.A0K);
                this.A0H = (C34561gL) r4.Aft(this.A0H, r142.A0H);
                this.A0I = (AnonymousClass25W) r4.Aft(this.A0I, r142.A0I);
                this.A0J = (AnonymousClass25V) r4.Aft(this.A0J, r142.A0J);
                this.A0B = (AnonymousClass25X) r4.Aft(this.A0B, r142.A0B);
                this.A0A = (AnonymousClass25Y) r4.Aft(this.A0A, r142.A0A);
                this.A0C = (C34701gZ) r4.Aft(this.A0C, r142.A0C);
                this.A04 = (C34621gR) r4.Aft(this.A04, r142.A04);
                this.A08 = (C34401g5) r4.Aft(this.A08, r142.A08);
                this.A09 = (C34451gA) r4.Aft(this.A09, r142.A09);
                this.A0D = (C34511gG) r4.Aft(this.A0D, r142.A0D);
                this.A05 = (C34661gV) r4.Aft(this.A05, r142.A05);
                this.A07 = (C34341fz) r4.Aft(this.A07, r142.A07);
                this.A0P = (C34471gC) r4.Aft(this.A0P, r142.A0P);
                this.A0G = (C34681gX) r4.Aft(this.A0G, r142.A0G);
                this.A03 = (C34431g8) r4.Aft(this.A03, r142.A03);
                this.A02 = (AnonymousClass25Z) r4.Aft(this.A02, r142.A02);
                this.A0N = (AnonymousClass25T) r4.Aft(this.A0N, r142.A0N);
                this.A0Q = (AnonymousClass25R) r4.Aft(this.A0Q, r142.A0Q);
                this.A0O = (C34541gJ) r4.Aft(this.A0O, r142.A0O);
                this.A0M = (C57642nO) r4.Aft(this.A0M, r142.A0M);
                if (r4 == C463025i.A00) {
                    this.A00 |= r142.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r42 = (AnonymousClass253) obj;
                AnonymousClass254 r143 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r42.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 8:
                                    this.A00 |= 1;
                                    this.A01 = r42.A06();
                                    break;
                                case 18:
                                    if ((this.A00 & 2) == 2) {
                                        r126 = (AnonymousClass26I) this.A0L.A0T();
                                    } else {
                                        r126 = null;
                                    }
                                    C34581gN r0 = (C34581gN) r42.A09(r143, C34581gN.A02.A0U());
                                    this.A0L = r0;
                                    if (r126 != null) {
                                        r126.A04(r0);
                                        this.A0L = (C34581gN) r126.A01();
                                    }
                                    this.A00 |= 2;
                                    break;
                                case 26:
                                    if ((this.A00 & 4) == 4) {
                                        r125 = (AnonymousClass263) this.A06.A0T();
                                    } else {
                                        r125 = null;
                                    }
                                    C34491gE r02 = (C34491gE) r42.A09(r143, C34491gE.A03.A0U());
                                    this.A06 = r02;
                                    if (r125 != null) {
                                        r125.A04(r02);
                                        this.A06 = (C34491gE) r125.A01();
                                    }
                                    this.A00 |= 4;
                                    break;
                                case 34:
                                    if ((this.A00 & 8) == 8) {
                                        r124 = (AnonymousClass26B) this.A0E.A0T();
                                    } else {
                                        r124 = null;
                                    }
                                    C34601gP r03 = (C34601gP) r42.A09(r143, C34601gP.A03.A0U());
                                    this.A0E = r03;
                                    if (r124 != null) {
                                        r124.A04(r03);
                                        this.A0E = (C34601gP) r124.A01();
                                    }
                                    this.A00 |= 8;
                                    break;
                                case 42:
                                    if ((this.A00 & 16) == 16) {
                                        r123 = (AnonymousClass26C) this.A0F.A0T();
                                    } else {
                                        r123 = null;
                                    }
                                    C34641gT r04 = (C34641gT) r42.A09(r143, C34641gT.A02.A0U());
                                    this.A0F = r04;
                                    if (r123 != null) {
                                        r123.A04(r04);
                                        this.A0F = (C34641gT) r123.A01();
                                    }
                                    this.A00 |= 16;
                                    break;
                                case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                    if ((this.A00 & 32) == 32) {
                                        r122 = (AnonymousClass26H) this.A0K.A0T();
                                    } else {
                                        r122 = null;
                                    }
                                    AnonymousClass25U r05 = (AnonymousClass25U) r42.A09(r143, AnonymousClass25U.A02.A0U());
                                    this.A0K = r05;
                                    if (r122 != null) {
                                        r122.A04(r05);
                                        this.A0K = (AnonymousClass25U) r122.A01();
                                    }
                                    this.A00 |= 32;
                                    break;
                                case 58:
                                    if ((this.A00 & 64) == 64) {
                                        r121 = (AnonymousClass26E) this.A0H.A0T();
                                    } else {
                                        r121 = null;
                                    }
                                    C34561gL r06 = (C34561gL) r42.A09(r143, C34561gL.A02.A0U());
                                    this.A0H = r06;
                                    if (r121 != null) {
                                        r121.A04(r06);
                                        this.A0H = (C34561gL) r121.A01();
                                    }
                                    this.A00 |= 64;
                                    break;
                                case 66:
                                    if ((this.A00 & 128) == 128) {
                                        r120 = (AnonymousClass26F) this.A0I.A0T();
                                    } else {
                                        r120 = null;
                                    }
                                    AnonymousClass25W r07 = (AnonymousClass25W) r42.A09(r143, AnonymousClass25W.A06.A0U());
                                    this.A0I = r07;
                                    if (r120 != null) {
                                        r120.A04(r07);
                                        this.A0I = (AnonymousClass25W) r120.A01();
                                    }
                                    this.A00 |= 128;
                                    break;
                                case 90:
                                    if ((this.A00 & 256) == 256) {
                                        r119 = (AnonymousClass26G) this.A0J.A0T();
                                    } else {
                                        r119 = null;
                                    }
                                    AnonymousClass25V r08 = (AnonymousClass25V) r42.A09(r143, AnonymousClass25V.A01.A0U());
                                    this.A0J = r08;
                                    if (r119 != null) {
                                        r119.A04(r08);
                                        this.A0J = (AnonymousClass25V) r119.A01();
                                    }
                                    this.A00 |= 256;
                                    break;
                                case 114:
                                    if ((this.A00 & 512) == 512) {
                                        r118 = (AnonymousClass268) this.A0B.A0T();
                                    } else {
                                        r118 = null;
                                    }
                                    AnonymousClass25X r09 = (AnonymousClass25X) r42.A09(r143, AnonymousClass25X.A05.A0U());
                                    this.A0B = r09;
                                    if (r118 != null) {
                                        r118.A04(r09);
                                        this.A0B = (AnonymousClass25X) r118.A01();
                                    }
                                    this.A00 |= 512;
                                    break;
                                case 122:
                                    if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                                        r117 = (AnonymousClass267) this.A0A.A0T();
                                    } else {
                                        r117 = null;
                                    }
                                    AnonymousClass25Y r010 = (AnonymousClass25Y) r42.A09(r143, AnonymousClass25Y.A02.A0U());
                                    this.A0A = r010;
                                    if (r117 != null) {
                                        r117.A04(r010);
                                        this.A0A = (AnonymousClass25Y) r117.A01();
                                    }
                                    this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    break;
                                case 130:
                                    if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                                        r116 = (AnonymousClass269) this.A0C.A0T();
                                    } else {
                                        r116 = null;
                                    }
                                    C34701gZ r011 = (C34701gZ) r42.A09(r143, C34701gZ.A02.A0U());
                                    this.A0C = r011;
                                    if (r116 != null) {
                                        r116.A04(r011);
                                        this.A0C = (C34701gZ) r116.A01();
                                    }
                                    this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    break;
                                case 138:
                                    if ((this.A00 & 4096) == 4096) {
                                        r115 = (AnonymousClass261) this.A04.A0T();
                                    } else {
                                        r115 = null;
                                    }
                                    C34621gR r012 = (C34621gR) r42.A09(r143, C34621gR.A03.A0U());
                                    this.A04 = r012;
                                    if (r115 != null) {
                                        r115.A04(r012);
                                        this.A04 = (C34621gR) r115.A01();
                                    }
                                    this.A00 |= 4096;
                                    break;
                                case 146:
                                    if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                                        r114 = (AnonymousClass265) this.A08.A0T();
                                    } else {
                                        r114 = null;
                                    }
                                    C34401g5 r013 = (C34401g5) r42.A09(r143, C34401g5.A03.A0U());
                                    this.A08 = r013;
                                    if (r114 != null) {
                                        r114.A04(r013);
                                        this.A08 = (C34401g5) r114.A01();
                                    }
                                    this.A00 |= DefaultCrypto.BUFFER_SIZE;
                                    break;
                                case 154:
                                    if ((this.A00 & 16384) == 16384) {
                                        r113 = (AnonymousClass266) this.A09.A0T();
                                    } else {
                                        r113 = null;
                                    }
                                    C34451gA r014 = (C34451gA) r42.A09(r143, C34451gA.A02.A0U());
                                    this.A09 = r014;
                                    if (r113 != null) {
                                        r113.A04(r014);
                                        this.A09 = (C34451gA) r113.A01();
                                    }
                                    this.A00 |= 16384;
                                    break;
                                case 162:
                                    if ((this.A00 & 32768) == 32768) {
                                        r112 = (AnonymousClass26A) this.A0D.A0T();
                                    } else {
                                        r112 = null;
                                    }
                                    C34511gG r015 = (C34511gG) r42.A09(r143, C34511gG.A03.A0U());
                                    this.A0D = r015;
                                    if (r112 != null) {
                                        r112.A04(r015);
                                        this.A0D = (C34511gG) r112.A01();
                                    }
                                    this.A00 |= 32768;
                                    break;
                                case 170:
                                    if ((this.A00 & 65536) == 65536) {
                                        r111 = (AnonymousClass262) this.A05.A0T();
                                    } else {
                                        r111 = null;
                                    }
                                    C34661gV r016 = (C34661gV) r42.A09(r143, C34661gV.A02.A0U());
                                    this.A05 = r016;
                                    if (r111 != null) {
                                        r111.A04(r016);
                                        this.A05 = (C34661gV) r111.A01();
                                    }
                                    this.A00 |= 65536;
                                    break;
                                case 178:
                                    if ((this.A00 & C25981Bo.A0F) == 131072) {
                                        r110 = (AnonymousClass264) this.A07.A0T();
                                    } else {
                                        r110 = null;
                                    }
                                    C34341fz r017 = (C34341fz) r42.A09(r143, C34341fz.A02.A0U());
                                    this.A07 = r017;
                                    if (r110 != null) {
                                        r110.A04(r017);
                                        this.A07 = (C34341fz) r110.A01();
                                    }
                                    this.A00 |= C25981Bo.A0F;
                                    break;
                                case 186:
                                    if ((this.A00 & 262144) == 262144) {
                                        r19 = (AnonymousClass26N) this.A0P.A0T();
                                    } else {
                                        r19 = null;
                                    }
                                    C34471gC r018 = (C34471gC) r42.A09(r143, C34471gC.A02.A0U());
                                    this.A0P = r018;
                                    if (r19 != null) {
                                        r19.A04(r018);
                                        this.A0P = (C34471gC) r19.A01();
                                    }
                                    this.A00 |= 262144;
                                    break;
                                case 194:
                                    if ((this.A00 & 524288) == 524288) {
                                        r18 = (AnonymousClass26D) this.A0G.A0T();
                                    } else {
                                        r18 = null;
                                    }
                                    C34681gX r019 = (C34681gX) r42.A09(r143, C34681gX.A01.A0U());
                                    this.A0G = r019;
                                    if (r18 != null) {
                                        r18.A04(r019);
                                        this.A0G = (C34681gX) r18.A01();
                                    }
                                    this.A00 |= 524288;
                                    break;
                                case 210:
                                    if ((this.A00 & 1048576) == 1048576) {
                                        r17 = (AnonymousClass260) this.A03.A0T();
                                    } else {
                                        r17 = null;
                                    }
                                    C34431g8 r020 = (C34431g8) r42.A09(r143, C34431g8.A02.A0U());
                                    this.A03 = r020;
                                    if (r17 != null) {
                                        r17.A04(r020);
                                        this.A03 = (C34431g8) r17.A01();
                                    }
                                    this.A00 |= 1048576;
                                    break;
                                case 218:
                                    if ((this.A00 & 2097152) == 2097152) {
                                        r16 = (C464725z) this.A02.A0T();
                                    } else {
                                        r16 = null;
                                    }
                                    AnonymousClass25Z r021 = (AnonymousClass25Z) r42.A09(r143, AnonymousClass25Z.A04.A0U());
                                    this.A02 = r021;
                                    if (r16 != null) {
                                        r16.A04(r021);
                                        this.A02 = (AnonymousClass25Z) r16.A01();
                                    }
                                    this.A00 |= 2097152;
                                    break;
                                case 226:
                                    if ((this.A00 & 4194304) == 4194304) {
                                        r15 = (AnonymousClass26J) this.A0N.A0T();
                                    } else {
                                        r15 = null;
                                    }
                                    AnonymousClass25T r022 = (AnonymousClass25T) r42.A09(r143, AnonymousClass25T.A04.A0U());
                                    this.A0N = r022;
                                    if (r15 != null) {
                                        r15.A04(r022);
                                        this.A0N = (AnonymousClass25T) r15.A01();
                                    }
                                    this.A00 |= 4194304;
                                    break;
                                case 234:
                                    if ((this.A00 & 8388608) == 8388608) {
                                        r14 = (AnonymousClass26O) this.A0Q.A0T();
                                    } else {
                                        r14 = null;
                                    }
                                    AnonymousClass25R r023 = (AnonymousClass25R) r42.A09(r143, AnonymousClass25R.A02.A0U());
                                    this.A0Q = r023;
                                    if (r14 != null) {
                                        r14.A04(r023);
                                        this.A0Q = (AnonymousClass25R) r14.A01();
                                    }
                                    this.A00 |= 8388608;
                                    break;
                                case 242:
                                    if ((this.A00 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                                        r13 = (AnonymousClass26M) this.A0O.A0T();
                                    } else {
                                        r13 = null;
                                    }
                                    C34541gJ r024 = (C34541gJ) r42.A09(r143, C34541gJ.A02.A0U());
                                    this.A0O = r024;
                                    if (r13 != null) {
                                        r13.A04(r024);
                                        this.A0O = (C34541gJ) r13.A01();
                                    }
                                    this.A00 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
                                    break;
                                case 266:
                                    if ((this.A00 & 33554432) == 33554432) {
                                        r1 = (C82473vg) this.A0M.A0T();
                                    } else {
                                        r1 = null;
                                    }
                                    C57642nO r025 = (C57642nO) r42.A09(r143, C57642nO.A0B.A0U());
                                    this.A0M = r025;
                                    if (r1 != null) {
                                        r1.A04(r025);
                                        this.A0M = (C57642nO) r1.A01();
                                    }
                                    this.A00 |= 33554432;
                                    break;
                                default:
                                    if (A0a(r42, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (C28971Pt e) {
                            e.unfinishedMessage = this;
                            throw new RuntimeException(e);
                        }
                    } catch (IOException e2) {
                        C28971Pt r127 = new C28971Pt(e2.getMessage());
                        r127.unfinishedMessage = this;
                        throw new RuntimeException(r127);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C27831Jk();
            case 5:
                return new AnonymousClass271();
            case 6:
                break;
            case 7:
                if (A0S == null) {
                    synchronized (C27831Jk.class) {
                        if (A0S == null) {
                            A0S = new AnonymousClass255(A0R);
                        }
                    }
                }
                return A0S;
            default:
                throw new UnsupportedOperationException();
        }
        return A0R;
    }

    public boolean A0b() {
        return (this.A00 & 1) == 1;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A05(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            C34581gN r0 = this.A0L;
            if (r0 == null) {
                r0 = C34581gN.A02;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C34491gE r02 = this.A06;
            if (r02 == null) {
                r02 = C34491gE.A03;
            }
            i2 += CodedOutputStream.A0A(r02, 3);
        }
        if ((this.A00 & 8) == 8) {
            C34601gP r03 = this.A0E;
            if (r03 == null) {
                r03 = C34601gP.A03;
            }
            i2 += CodedOutputStream.A0A(r03, 4);
        }
        if ((this.A00 & 16) == 16) {
            C34641gT r04 = this.A0F;
            if (r04 == null) {
                r04 = C34641gT.A02;
            }
            i2 += CodedOutputStream.A0A(r04, 5);
        }
        if ((this.A00 & 32) == 32) {
            AnonymousClass25U r05 = this.A0K;
            if (r05 == null) {
                r05 = AnonymousClass25U.A02;
            }
            i2 += CodedOutputStream.A0A(r05, 6);
        }
        if ((this.A00 & 64) == 64) {
            C34561gL r06 = this.A0H;
            if (r06 == null) {
                r06 = C34561gL.A02;
            }
            i2 += CodedOutputStream.A0A(r06, 7);
        }
        if ((this.A00 & 128) == 128) {
            AnonymousClass25W r07 = this.A0I;
            if (r07 == null) {
                r07 = AnonymousClass25W.A06;
            }
            i2 += CodedOutputStream.A0A(r07, 8);
        }
        if ((this.A00 & 256) == 256) {
            AnonymousClass25V r08 = this.A0J;
            if (r08 == null) {
                r08 = AnonymousClass25V.A01;
            }
            i2 += CodedOutputStream.A0A(r08, 11);
        }
        if ((this.A00 & 512) == 512) {
            AnonymousClass25X r09 = this.A0B;
            if (r09 == null) {
                r09 = AnonymousClass25X.A05;
            }
            i2 += CodedOutputStream.A0A(r09, 14);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            AnonymousClass25Y r010 = this.A0A;
            if (r010 == null) {
                r010 = AnonymousClass25Y.A02;
            }
            i2 += CodedOutputStream.A0A(r010, 15);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C34701gZ r011 = this.A0C;
            if (r011 == null) {
                r011 = C34701gZ.A02;
            }
            i2 += CodedOutputStream.A0A(r011, 16);
        }
        if ((this.A00 & 4096) == 4096) {
            C34621gR r012 = this.A04;
            if (r012 == null) {
                r012 = C34621gR.A03;
            }
            i2 += CodedOutputStream.A0A(r012, 17);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            C34401g5 r013 = this.A08;
            if (r013 == null) {
                r013 = C34401g5.A03;
            }
            i2 += CodedOutputStream.A0A(r013, 18);
        }
        if ((this.A00 & 16384) == 16384) {
            C34451gA r014 = this.A09;
            if (r014 == null) {
                r014 = C34451gA.A02;
            }
            i2 += CodedOutputStream.A0A(r014, 19);
        }
        if ((this.A00 & 32768) == 32768) {
            C34511gG r015 = this.A0D;
            if (r015 == null) {
                r015 = C34511gG.A03;
            }
            i2 += CodedOutputStream.A0A(r015, 20);
        }
        if ((this.A00 & 65536) == 65536) {
            C34661gV r016 = this.A05;
            if (r016 == null) {
                r016 = C34661gV.A02;
            }
            i2 += CodedOutputStream.A0A(r016, 21);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            C34341fz r017 = this.A07;
            if (r017 == null) {
                r017 = C34341fz.A02;
            }
            i2 += CodedOutputStream.A0A(r017, 22);
        }
        if ((this.A00 & 262144) == 262144) {
            C34471gC r018 = this.A0P;
            if (r018 == null) {
                r018 = C34471gC.A02;
            }
            i2 += CodedOutputStream.A0A(r018, 23);
        }
        if ((this.A00 & 524288) == 524288) {
            C34681gX r019 = this.A0G;
            if (r019 == null) {
                r019 = C34681gX.A01;
            }
            i2 += CodedOutputStream.A0A(r019, 24);
        }
        if ((this.A00 & 1048576) == 1048576) {
            C34431g8 r020 = this.A03;
            if (r020 == null) {
                r020 = C34431g8.A02;
            }
            i2 += CodedOutputStream.A0A(r020, 26);
        }
        if ((this.A00 & 2097152) == 2097152) {
            AnonymousClass25Z r021 = this.A02;
            if (r021 == null) {
                r021 = AnonymousClass25Z.A04;
            }
            i2 += CodedOutputStream.A0A(r021, 27);
        }
        if ((this.A00 & 4194304) == 4194304) {
            AnonymousClass25T r022 = this.A0N;
            if (r022 == null) {
                r022 = AnonymousClass25T.A04;
            }
            i2 += CodedOutputStream.A0A(r022, 28);
        }
        if ((this.A00 & 8388608) == 8388608) {
            AnonymousClass25R r023 = this.A0Q;
            if (r023 == null) {
                r023 = AnonymousClass25R.A02;
            }
            i2 += CodedOutputStream.A0A(r023, 29);
        }
        if ((this.A00 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            C34541gJ r024 = this.A0O;
            if (r024 == null) {
                r024 = C34541gJ.A02;
            }
            i2 += CodedOutputStream.A0A(r024, 30);
        }
        if ((this.A00 & 33554432) == 33554432) {
            C57642nO r025 = this.A0M;
            if (r025 == null) {
                r025 = C57642nO.A0B;
            }
            i2 += CodedOutputStream.A0A(r025, 33);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0H(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            C34581gN r0 = this.A0L;
            if (r0 == null) {
                r0 = C34581gN.A02;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C34491gE r02 = this.A06;
            if (r02 == null) {
                r02 = C34491gE.A03;
            }
            codedOutputStream.A0L(r02, 3);
        }
        if ((this.A00 & 8) == 8) {
            C34601gP r03 = this.A0E;
            if (r03 == null) {
                r03 = C34601gP.A03;
            }
            codedOutputStream.A0L(r03, 4);
        }
        if ((this.A00 & 16) == 16) {
            C34641gT r04 = this.A0F;
            if (r04 == null) {
                r04 = C34641gT.A02;
            }
            codedOutputStream.A0L(r04, 5);
        }
        if ((this.A00 & 32) == 32) {
            AnonymousClass25U r05 = this.A0K;
            if (r05 == null) {
                r05 = AnonymousClass25U.A02;
            }
            codedOutputStream.A0L(r05, 6);
        }
        if ((this.A00 & 64) == 64) {
            C34561gL r06 = this.A0H;
            if (r06 == null) {
                r06 = C34561gL.A02;
            }
            codedOutputStream.A0L(r06, 7);
        }
        if ((this.A00 & 128) == 128) {
            AnonymousClass25W r07 = this.A0I;
            if (r07 == null) {
                r07 = AnonymousClass25W.A06;
            }
            codedOutputStream.A0L(r07, 8);
        }
        if ((this.A00 & 256) == 256) {
            AnonymousClass25V r08 = this.A0J;
            if (r08 == null) {
                r08 = AnonymousClass25V.A01;
            }
            codedOutputStream.A0L(r08, 11);
        }
        if ((this.A00 & 512) == 512) {
            AnonymousClass25X r09 = this.A0B;
            if (r09 == null) {
                r09 = AnonymousClass25X.A05;
            }
            codedOutputStream.A0L(r09, 14);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            AnonymousClass25Y r010 = this.A0A;
            if (r010 == null) {
                r010 = AnonymousClass25Y.A02;
            }
            codedOutputStream.A0L(r010, 15);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C34701gZ r011 = this.A0C;
            if (r011 == null) {
                r011 = C34701gZ.A02;
            }
            codedOutputStream.A0L(r011, 16);
        }
        if ((this.A00 & 4096) == 4096) {
            C34621gR r012 = this.A04;
            if (r012 == null) {
                r012 = C34621gR.A03;
            }
            codedOutputStream.A0L(r012, 17);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            C34401g5 r013 = this.A08;
            if (r013 == null) {
                r013 = C34401g5.A03;
            }
            codedOutputStream.A0L(r013, 18);
        }
        if ((this.A00 & 16384) == 16384) {
            C34451gA r014 = this.A09;
            if (r014 == null) {
                r014 = C34451gA.A02;
            }
            codedOutputStream.A0L(r014, 19);
        }
        if ((this.A00 & 32768) == 32768) {
            C34511gG r015 = this.A0D;
            if (r015 == null) {
                r015 = C34511gG.A03;
            }
            codedOutputStream.A0L(r015, 20);
        }
        if ((this.A00 & 65536) == 65536) {
            C34661gV r016 = this.A05;
            if (r016 == null) {
                r016 = C34661gV.A02;
            }
            codedOutputStream.A0L(r016, 21);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            C34341fz r017 = this.A07;
            if (r017 == null) {
                r017 = C34341fz.A02;
            }
            codedOutputStream.A0L(r017, 22);
        }
        if ((this.A00 & 262144) == 262144) {
            C34471gC r018 = this.A0P;
            if (r018 == null) {
                r018 = C34471gC.A02;
            }
            codedOutputStream.A0L(r018, 23);
        }
        if ((this.A00 & 524288) == 524288) {
            C34681gX r019 = this.A0G;
            if (r019 == null) {
                r019 = C34681gX.A01;
            }
            codedOutputStream.A0L(r019, 24);
        }
        if ((this.A00 & 1048576) == 1048576) {
            C34431g8 r020 = this.A03;
            if (r020 == null) {
                r020 = C34431g8.A02;
            }
            codedOutputStream.A0L(r020, 26);
        }
        if ((this.A00 & 2097152) == 2097152) {
            AnonymousClass25Z r021 = this.A02;
            if (r021 == null) {
                r021 = AnonymousClass25Z.A04;
            }
            codedOutputStream.A0L(r021, 27);
        }
        if ((this.A00 & 4194304) == 4194304) {
            AnonymousClass25T r022 = this.A0N;
            if (r022 == null) {
                r022 = AnonymousClass25T.A04;
            }
            codedOutputStream.A0L(r022, 28);
        }
        if ((this.A00 & 8388608) == 8388608) {
            AnonymousClass25R r023 = this.A0Q;
            if (r023 == null) {
                r023 = AnonymousClass25R.A02;
            }
            codedOutputStream.A0L(r023, 29);
        }
        if ((this.A00 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            C34541gJ r024 = this.A0O;
            if (r024 == null) {
                r024 = C34541gJ.A02;
            }
            codedOutputStream.A0L(r024, 30);
        }
        if ((this.A00 & 33554432) == 33554432) {
            C57642nO r025 = this.A0M;
            if (r025 == null) {
                r025 = C57642nO.A0B;
            }
            codedOutputStream.A0L(r025, 33);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
