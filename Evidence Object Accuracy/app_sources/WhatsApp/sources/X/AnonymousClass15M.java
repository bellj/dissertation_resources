package X;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.15M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15M extends AbstractC16280ok {
    public final C16590pI A00;
    public final C231410n A01;

    public AnonymousClass15M(AbstractC15710nm r8, C16590pI r9, C231410n r10) {
        super(r9.A00, r8, "location.db", null, 2, true);
        this.A00 = r9;
        this.A01 = r10;
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteDatabaseCorruptException e) {
            Log.w("LocationDbHelper/getReadableDatabase/Locations database is corrupt. Removing...", e);
            A04();
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteException e2) {
            String obj = e2.toString();
            if (obj.contains("file is encrypted")) {
                Log.w("LocationDbHelper/getReadableDatabase/Locations database is encrypted. Removing...", e2);
                A04();
                return AnonymousClass1Tx.A01(super.A00(), this.A01);
            } else if (obj.contains("upgrade read-only database")) {
                Log.w("LocationDbHelper/getReadableDatabase/Client actually opened database as read-only and can't upgrade. Switching to writable...", e2);
                return AnonymousClass1Tx.A01(super.A00(), this.A01);
            } else {
                throw e2;
            }
        } catch (StackOverflowError e3) {
            Log.w("LocationDbHelper/getReadableDatabase/StackOverflowError during db init check");
            for (StackTraceElement stackTraceElement : e3.getStackTrace()) {
                if (stackTraceElement.getMethodName().equals("onCorruption")) {
                    Log.w("LocationDbHelper/getReadableDatabase/Locations database is corrupt. Found via StackOverflowError. Removing...");
                    A04();
                    return AnonymousClass1Tx.A01(super.A00(), this.A01);
                }
            }
            throw e3;
        }
    }

    public void A04() {
        synchronized (this) {
            close();
            Log.i("LocationDbHelper/deleteDatabaseFiles");
            File databasePath = this.A00.A00.getDatabasePath("location.db");
            boolean delete = databasePath.delete();
            StringBuilder sb = new StringBuilder();
            sb.append("LocationDbHelper/deleteDatabaseFiles/deleted location database; databaseDeleted=");
            sb.append(delete);
            Log.i(sb.toString());
            AnonymousClass1Tx.A04(databasePath, "LocationDbHelper");
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Log.i("LocationDbHelper/onCreate; version=2");
        Log.i("LocationSharerTable/createTable/version=2");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS location_sharer");
        sQLiteDatabase.execSQL("CREATE TABLE location_sharer (_id INTEGER PRIMARY KEY AUTOINCREMENT, remote_jid TEXT NOT NULL, from_me BOOLEAN NOT NULL, remote_resource TEXT NOT NULL, expires INTEGER NOT NULL, message_id TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS location_sharer_index ON location_sharer(remote_jid, from_me, remote_resource, message_id)");
        Log.i("LocationKeyDistributionTable/createTable/version=2");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS location_key_distribution");
        sQLiteDatabase.execSQL("CREATE TABLE location_key_distribution (_id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT NOT NULL, sent_to_server BOOLEAN NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS location_key_distribution_index ON location_key_distribution(jid)");
        Log.i("LocationCacheTable/createTable/version=2");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS location_cache");
        sQLiteDatabase.execSQL("CREATE TABLE location_cache (_id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT NOT NULL, latitude REAL NOT NULL, longitude REAL NOT NULL, accuracy INTEGER NOT NULL, speed REAL NOT NULL, bearing INTEGER NOT NULL, location_ts INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS user_location_index ON location_cache(jid)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("LocationDbHelper/onDowngrade; oldVersion=");
        sb.append(i);
        sb.append("; newVersion=");
        sb.append(i2);
        Log.w(sb.toString());
        onCreate(sQLiteDatabase);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("LocationDbHelper/onUpgrade; oldVersion=");
        sb.append(i);
        sb.append("; newVersion=");
        sb.append(i2);
        Log.i(sb.toString());
        if (i2 != 2) {
            StringBuilder sb2 = new StringBuilder("LocationDbHelper/Unknown upgrade destination version: ");
            sb2.append(i);
            sb2.append(" -> ");
            sb2.append(i2);
            throw new SQLiteException(sb2.toString());
        } else if (i != 1) {
            Log.i("LocationDbHelper/onUpgrade/unknown old version");
            onCreate(sQLiteDatabase);
        } else {
            sQLiteDatabase.execSQL("DROP INDEX IF EXISTS location_sharer_index");
            sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS location_sharer_index ON location_sharer(remote_jid, from_me, remote_resource, message_id)");
        }
    }
}
