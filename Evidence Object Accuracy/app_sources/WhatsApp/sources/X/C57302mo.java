package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57302mo extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57302mo A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public AnonymousClass1K6 A01 = AnonymousClass277.A01;
    public C43261wh A02;
    public String A03 = "";

    static {
        C57302mo r0 = new C57302mo();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        C81603uH r1;
        switch (r5.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C57302mo r7 = (C57302mo) obj2;
                this.A03 = r6.Afy(this.A03, r7.A03, C12960it.A1R(this.A00), C12960it.A1R(r7.A00));
                this.A01 = r6.Afr(this.A01, r7.A01);
                this.A02 = (C43261wh) r6.Aft(this.A02, r7.A02);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r62.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r62.A0A();
                            this.A00 = 1 | this.A00;
                            this.A03 = A0A;
                        } else if (A03 == 18) {
                            AnonymousClass1K6 r12 = this.A01;
                            if (!((AnonymousClass1K7) r12).A00) {
                                r12 = AbstractC27091Fz.A0G(r12);
                                this.A01 = r12;
                            }
                            r12.add((C57292mn) AbstractC27091Fz.A0H(r62, r72, C57292mn.A04));
                        } else if (A03 == 138) {
                            if ((this.A00 & 2) == 2) {
                                r1 = (C81603uH) this.A02.A0T();
                            } else {
                                r1 = null;
                            }
                            C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r62, r72, C43261wh.A0O);
                            this.A02 = r0;
                            if (r1 != null) {
                                this.A02 = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 2;
                        } else if (!A0a(r62, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                AbstractC27091Fz.A0R(this.A01);
                return null;
            case 4:
                return new C57302mo();
            case 5:
                return new C81843uf();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57302mo.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A03) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A01.size(); i3++) {
            i = AbstractC27091Fz.A08((AnonymousClass1G1) this.A01.get(i3), 2, i);
        }
        if ((this.A00 & 2) == 2) {
            C43261wh r0 = this.A02;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i = AbstractC27091Fz.A08(r0, 17, i);
        }
        return AbstractC27091Fz.A07(this, i);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        int i = 0;
        while (i < this.A01.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A01, i, 2);
        }
        if ((this.A00 & 2) == 2) {
            C43261wh r0 = this.A02;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
