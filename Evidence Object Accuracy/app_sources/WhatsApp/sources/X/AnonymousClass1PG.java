package X;

import java.io.Serializable;

/* renamed from: X.1PG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1PG implements Serializable {
    public static final long serialVersionUID = 1;
    public final int disappearingMessagesInitiator;
    public final long ephemeralSettingTimestamp;
    public final int expiration;

    public AnonymousClass1PG(int i, long j, int i2) {
        this.expiration = i;
        this.ephemeralSettingTimestamp = j;
        this.disappearingMessagesInitiator = i2;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass1PG r7 = (AnonymousClass1PG) obj;
            if (this.expiration == r7.expiration && this.disappearingMessagesInitiator == r7.disappearingMessagesInitiator && this.ephemeralSettingTimestamp == r7.ephemeralSettingTimestamp) {
                return true;
            }
            return false;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        long j = this.ephemeralSettingTimestamp;
        return (((this.expiration * 31) + ((int) (j ^ (j >>> 32)))) * 31) + this.disappearingMessagesInitiator;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("EphemeralInfo{expiration=");
        sb.append(this.expiration);
        sb.append(", ephemeralSettingTimestamp=");
        sb.append(this.ephemeralSettingTimestamp);
        sb.append(", disappearingMessagesInitiator=");
        sb.append(this.disappearingMessagesInitiator);
        sb.append('}');
        return sb.toString();
    }
}
