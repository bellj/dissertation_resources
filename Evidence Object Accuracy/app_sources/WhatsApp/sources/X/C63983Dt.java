package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;

/* renamed from: X.3Dt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63983Dt {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3HJ A01;

    public C63983Dt(AbstractC15710nm r5, AnonymousClass1V8 r6) {
        AnonymousClass1V8.A01(r6, "pay");
        IDxNFunctionShape17S0100000_2_I1 iDxNFunctionShape17S0100000_2_I1 = new IDxNFunctionShape17S0100000_2_I1(r5, 0);
        String[] A08 = C13000ix.A08();
        A08[0] = "merchant";
        this.A01 = (AnonymousClass3HJ) AnonymousClass3JT.A05(r6, iDxNFunctionShape17S0100000_2_I1, A08);
        this.A00 = r6;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C63983Dt.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((C63983Dt) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
