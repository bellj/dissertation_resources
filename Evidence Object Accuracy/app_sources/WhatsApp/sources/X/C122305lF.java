package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.NoviPayHubBalanceView;

/* renamed from: X.5lF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122305lF extends AbstractC118825cR {
    public NoviPayHubBalanceView A00;

    public C122305lF(View view) {
        super(view);
        this.A00 = (NoviPayHubBalanceView) AnonymousClass028.A0D(view, R.id.balance_section);
    }
}
