package X;

/* renamed from: X.1Nc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28411Nc extends RuntimeException {
    public C28411Nc(String str) {
        super(str);
    }

    public C28411Nc(String str, Throwable th) {
        super(str);
        setStackTrace(th.getStackTrace());
    }
}
