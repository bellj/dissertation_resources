package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.biz.order.view.fragment.OrderDetailFragment;
import java.math.BigDecimal;

/* renamed from: X.2u0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59122u0 extends AbstractC75653kC {
    public final ImageView A00;
    public final TextView A01;
    public final TextView A02;
    public final C37071lG A03;
    public final AnonymousClass018 A04;

    public C59122u0(View view, AbstractC37061lF r4, C37071lG r5, OrderDetailFragment orderDetailFragment, AnonymousClass018 r7) {
        super(view);
        this.A04 = r7;
        this.A03 = r5;
        this.A02 = C12960it.A0I(view, R.id.cart_item_title);
        this.A01 = C12960it.A0I(view, R.id.cart_item_subtitle);
        this.A00 = C12970iu.A0K(view, R.id.cart_item_thumbnail);
        AnonymousClass028.A0D(view, R.id.cart_item_quantity_container).setVisibility(8);
        AbstractView$OnClickListenerC34281fs.A03(view, this, r4, orderDetailFragment, 3);
    }

    @Override // X.AbstractC75653kC
    public void A08(AnonymousClass4JV r9) {
        Context context;
        int i;
        Object[] objArr;
        C30711Yn r1;
        AnonymousClass3M5 r6 = ((C84483zN) r9).A00;
        this.A02.setText(r6.A05);
        BigDecimal bigDecimal = r6.A03;
        if (bigDecimal == null || (r1 = r6.A02) == null) {
            context = this.A0H.getContext();
            i = R.string.order_item_quantity_in_list;
            objArr = new Object[1];
            C12960it.A1P(objArr, r6.A00, 0);
        } else {
            String A03 = r1.A03(this.A04, bigDecimal, true);
            context = this.A0H.getContext();
            i = R.string.order_item_price_quantity;
            objArr = C12980iv.A1a();
            objArr[0] = A03;
            C12960it.A1P(objArr, r6.A00, 1);
        }
        this.A01.setText(context.getString(i, objArr));
        AnonymousClass3AK.A00(this.A00, this.A03, r6.A01);
    }
}
