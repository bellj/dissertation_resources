package X;

import android.net.Uri;
import java.util.Collections;
import java.util.Map;

/* renamed from: X.3D9  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3D9 {
    public int A00;
    public int A01 = 1;
    public long A02 = -1;
    public long A03;
    public Uri A04;
    public Map A05 = Collections.emptyMap();

    public AnonymousClass3H3 A00() {
        Uri uri = this.A04;
        if (uri != null) {
            int i = this.A01;
            return new AnonymousClass3H3(uri, null, this.A05, null, i, this.A00, 0, this.A03, this.A02);
        }
        throw C12960it.A0U(String.valueOf("The uri must be set."));
    }
}
