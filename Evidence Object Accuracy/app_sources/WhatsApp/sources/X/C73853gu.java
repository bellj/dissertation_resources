package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3gu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73853gu extends Animation {
    public final /* synthetic */ AnonymousClass3FR A00;

    public /* synthetic */ C73853gu(AnonymousClass3FR r1) {
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        AnonymousClass3FR r1 = this.A00;
        r1.A00 = 1.0f - f;
        r1.A0A.invalidate();
    }
}
