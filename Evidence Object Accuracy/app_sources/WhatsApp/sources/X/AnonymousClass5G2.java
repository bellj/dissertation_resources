package X;

/* renamed from: X.5G2  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G2 implements AnonymousClass20J {
    public int A00;
    public int A01;
    public AnonymousClass5XE A02;
    public byte[] A03;
    public byte[] A04;

    @Override // X.AnonymousClass20J
    public int A97(byte[] bArr, int i) {
        AnonymousClass5XE r4 = this.A02;
        int AAt = r4.AAt();
        while (true) {
            int i2 = this.A00;
            byte[] bArr2 = this.A03;
            if (i2 < AAt) {
                bArr2[i2] = 0;
                this.A00 = i2 + 1;
            } else {
                byte[] bArr3 = this.A04;
                r4.AZY(bArr2, bArr3, 0, 0);
                int i3 = this.A01;
                System.arraycopy(bArr3, 0, bArr, i, i3);
                reset();
                return i3;
            }
        }
    }

    @Override // X.AnonymousClass20J
    public int AE2() {
        return this.A01;
    }

    @Override // X.AnonymousClass20J
    public void AIc(AnonymousClass20L r3) {
        reset();
        this.A02.AIf(r3, true);
    }

    @Override // X.AnonymousClass20J
    public void AfG(byte b) {
        int i = this.A00;
        byte[] bArr = this.A03;
        if (i == bArr.length) {
            this.A02.AZY(bArr, this.A04, 0, 0);
            this.A00 = 0;
            i = 0;
        }
        this.A00 = i + 1;
        bArr[i] = b;
    }

    @Override // X.AnonymousClass20J
    public void reset() {
        int i = 0;
        while (true) {
            byte[] bArr = this.A03;
            if (i < bArr.length) {
                bArr[i] = 0;
                i++;
            } else {
                this.A00 = 0;
                this.A02.reset();
                return;
            }
        }
    }

    public AnonymousClass5G2(AnonymousClass5XE r3, int i) {
        if (i % 8 == 0) {
            this.A02 = new C112955Fl(r3);
            this.A01 = i >> 3;
            int AAt = r3.AAt();
            this.A04 = new byte[AAt];
            this.A03 = new byte[AAt];
            this.A00 = 0;
            return;
        }
        throw C12970iu.A0f("MAC size must be multiple of 8");
    }

    @Override // X.AnonymousClass20J
    public void update(byte[] bArr, int i, int i2) {
        if (i2 >= 0) {
            AnonymousClass5XE r5 = this.A02;
            int AAt = r5.AAt();
            int i3 = this.A00;
            int i4 = AAt - i3;
            if (i2 > i4) {
                byte[] bArr2 = this.A03;
                System.arraycopy(bArr, i, bArr2, i3, i4);
                byte[] bArr3 = this.A04;
                r5.AZY(bArr2, bArr3, 0, 0);
                this.A00 = 0;
                i2 -= i4;
                i += i4;
                while (i2 > AAt) {
                    r5.AZY(bArr, bArr3, i, 0);
                    i2 -= AAt;
                    i += AAt;
                }
            }
            System.arraycopy(bArr, i, this.A03, this.A00, i2);
            this.A00 += i2;
            return;
        }
        throw C12970iu.A0f("Can't have a negative input length!");
    }
}
