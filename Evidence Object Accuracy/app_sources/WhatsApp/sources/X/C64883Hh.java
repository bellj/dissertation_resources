package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.3Hh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64883Hh {
    public static C64883Hh A04;
    public AnonymousClass4PT A00;
    public AnonymousClass4PT A01;
    public final Handler A02 = new Handler(Looper.getMainLooper(), new C98214iK(this));
    public final Object A03 = C12970iu.A0l();

    public static C64883Hh A00() {
        C64883Hh r0 = A04;
        if (r0 != null) {
            return r0;
        }
        C64883Hh r02 = new C64883Hh();
        A04 = r02;
        return r02;
    }

    public final void A01() {
        AnonymousClass4PT r0 = this.A01;
        if (r0 != null) {
            this.A00 = r0;
            this.A01 = null;
            AnonymousClass5R8 r02 = (AnonymousClass5R8) r0.A02.get();
            if (r02 != null) {
                Handler handler = AbstractC15160mf.A08;
                handler.sendMessage(handler.obtainMessage(0, ((AnonymousClass51C) r02).A00));
                return;
            }
            this.A00 = null;
        }
    }

    public void A02(AnonymousClass5R8 r4) {
        synchronized (this.A03) {
            if (A05(r4)) {
                AnonymousClass4PT r1 = this.A00;
                if (!r1.A01) {
                    r1.A01 = true;
                    this.A02.removeCallbacksAndMessages(r1);
                }
            }
        }
    }

    public void A03(AnonymousClass5R8 r4) {
        synchronized (this.A03) {
            if (A05(r4)) {
                AnonymousClass4PT r1 = this.A00;
                if (r1.A01) {
                    r1.A01 = false;
                    A04(r1);
                }
            }
        }
    }

    public final void A04(AnonymousClass4PT r6) {
        int i = r6.A00;
        if (i != -2) {
            if (i <= 0) {
                i = 2750;
                if (i == -1) {
                    i = 1500;
                }
            }
            Handler handler = this.A02;
            handler.removeCallbacksAndMessages(r6);
            handler.sendMessageDelayed(Message.obtain(handler, 0, r6), (long) i);
        }
    }

    public final boolean A05(AnonymousClass5R8 r3) {
        AnonymousClass4PT r0 = this.A00;
        if (r0 == null || r3 == null || r0.A02.get() != r3) {
            return false;
        }
        return true;
    }

    public final boolean A06(AnonymousClass4PT r5, int i) {
        AnonymousClass5R8 r1 = (AnonymousClass5R8) r5.A02.get();
        if (r1 == null) {
            return false;
        }
        this.A02.removeCallbacksAndMessages(r5);
        Handler handler = AbstractC15160mf.A08;
        handler.sendMessage(handler.obtainMessage(1, i, 0, ((AnonymousClass51C) r1).A00));
        return true;
    }
}
