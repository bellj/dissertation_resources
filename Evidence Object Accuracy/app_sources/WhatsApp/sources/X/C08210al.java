package X;

/* renamed from: X.0al  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08210al implements AbstractC12040hH {
    public final AnonymousClass0JU A00;
    public final String A01;
    public final boolean A02;

    public C08210al(AnonymousClass0JU r1, String str, boolean z) {
        this.A01 = str;
        this.A00 = r1;
        this.A02 = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        if (r2.A0A) {
            return new C07960aM(this);
        }
        AnonymousClass0R5.A00("Animation contains merge paths but they are disabled.");
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("MergePaths{mode=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
