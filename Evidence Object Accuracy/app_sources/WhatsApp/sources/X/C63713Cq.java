package X;

import java.util.Set;

/* renamed from: X.3Cq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63713Cq {
    public String A00;
    public Set A01;
    public final C14160kx A02;
    public volatile Boolean A03;

    public C63713Cq(C14160kx r1) {
        this.A02 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0014, code lost:
        if (r0.equals(r5) == false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Set A00() {
        /*
            r6 = this;
            X.4Vl r0 = X.C88904Hw.A01
            java.lang.Object r5 = r0.A00()
            java.lang.String r5 = (java.lang.String) r5
            java.util.Set r4 = r6.A01
            if (r4 == 0) goto L_0x0016
            java.lang.String r0 = r6.A00
            if (r0 == 0) goto L_0x0016
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x0034
        L_0x0016:
            java.lang.String r0 = ","
            java.lang.String[] r3 = android.text.TextUtils.split(r5, r0)
            java.util.HashSet r4 = X.C12970iu.A12()
            int r2 = r3.length
            r1 = 0
        L_0x0022:
            if (r1 >= r2) goto L_0x0030
            r0 = r3[r1]
            int r0 = java.lang.Integer.parseInt(r0)     // Catch: NumberFormatException -> 0x002d
            X.C12980iv.A1R(r4, r0)     // Catch: NumberFormatException -> 0x002d
        L_0x002d:
            int r1 = r1 + 1
            goto L_0x0022
        L_0x0030:
            r6.A00 = r5
            r6.A01 = r4
        L_0x0034:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C63713Cq.A00():java.util.Set");
    }
}
