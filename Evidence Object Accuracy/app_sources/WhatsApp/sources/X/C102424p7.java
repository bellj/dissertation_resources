package X;

import android.widget.CompoundButton;
import com.google.android.material.chip.ChipGroup;

/* renamed from: X.4p7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102424p7 implements CompoundButton.OnCheckedChangeListener {
    public final /* synthetic */ ChipGroup A00;

    public /* synthetic */ C102424p7(ChipGroup chipGroup) {
        this.A00 = chipGroup;
    }

    @Override // android.widget.CompoundButton.OnCheckedChangeListener
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        ChipGroup chipGroup = this.A00;
        if (!chipGroup.A05) {
            int id = compoundButton.getId();
            int i = chipGroup.A00;
            if (z) {
                if (!(i == -1 || i == id || !chipGroup.A06)) {
                    chipGroup.A00(i, false);
                }
                chipGroup.A00 = id;
            } else if (i == id) {
                chipGroup.A00 = -1;
            }
        }
    }
}
