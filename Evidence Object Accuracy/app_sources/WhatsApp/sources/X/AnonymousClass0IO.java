package X;

import java.util.ArrayList;

/* renamed from: X.0IO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IO extends AnonymousClass0eL {
    public final /* synthetic */ AnonymousClass0IQ A00;
    public final /* synthetic */ C06440Tp A01;
    public final /* synthetic */ boolean A02;

    public AnonymousClass0IO(AnonymousClass0IQ r1, C06440Tp r2, boolean z) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = z;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        AnonymousClass0IQ r6 = this.A00;
        AnonymousClass0IT r5 = r6.A04;
        int i = ((AnonymousClass03S) r5).A09.A0E.A0G;
        C06440Tp r1 = this.A01;
        if (r1 != null) {
            r5.A0A.A02(r1);
            if (!this.A02 && r6.A03 <= i + 1) {
                r5.A01();
                ArrayList arrayList = AnonymousClass0IT.A0C;
                if (!arrayList.isEmpty()) {
                    int size = arrayList.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        ((AnonymousClass03S) arrayList.get(i2)).A01();
                    }
                    arrayList.clear();
                    return;
                }
                return;
            }
            return;
        }
        int i3 = r6.A00;
        if (i3 > 0) {
            int i4 = r6.A03;
            if (i4 == -1 || i4 == i) {
                r5.A0B(r6.A01, r6.A02, i4, i3 - 1);
            }
        }
    }
}
