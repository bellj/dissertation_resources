package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.44I  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44I extends AbstractC51682Vy {
    public final SearchViewModel A00;
    public final AnonymousClass34d A01;

    public AnonymousClass44I(SearchViewModel searchViewModel, AnonymousClass34d r2) {
        super(r2);
        this.A01 = r2;
        this.A00 = searchViewModel;
    }
}
