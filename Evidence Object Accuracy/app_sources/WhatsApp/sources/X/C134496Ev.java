package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeDialogFragment;
import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.6Ev  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134496Ev implements AnonymousClass18V {
    public final AnonymousClass01H A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;
    public final AnonymousClass01H A03;
    public final List A04;

    public C134496Ev(AnonymousClass01H r2, AnonymousClass01H r3, AnonymousClass01H r4, AnonymousClass01H r5, AnonymousClass01H r6) {
        this.A02 = r2;
        this.A00 = r4;
        this.A01 = r5;
        this.A03 = r6;
        this.A04 = ((C127795v4) r3.get()).A03;
    }

    public final void A00(Context context, Intent intent) {
        if (((AnonymousClass18X) this.A01.get()).AKA()) {
            ((AnonymousClass12P) this.A00.get()).A09(AnonymousClass12P.A00(context), intent, 475);
            return;
        }
        this.A02.get();
        Activity A01 = AnonymousClass12P.A01(context, ActivityC000900k.class);
        AnonymousClass009.A05(A01);
        new PrivacyNoticeDialogFragment().A1F(((ActivityC000900k) A01).A0V(), "TOSFragmentOuter");
    }

    @Override // X.AnonymousClass18V
    public boolean AI1(Context context, Uri uri) {
        if (context == null) {
            return false;
        }
        List list = this.A04;
        if (list.isEmpty()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            Intent AYx = ((AnonymousClass6MG) list.get(i)).AYx(context, uri);
            if (AYx != null) {
                ((C17120qI) this.A03.get()).A01(context).A00(new AbstractC50172Ok(AYx, this, C12970iu.A10(context)) { // from class: X.6E2
                    public final /* synthetic */ Intent A00;
                    public final /* synthetic */ C134496Ev A01;
                    public final /* synthetic */ WeakReference A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC50172Ok
                    public final void APz(Object obj) {
                        C134496Ev r5 = this.A01;
                        WeakReference weakReference = this.A02;
                        Intent intent = this.A00;
                        C50142Oh r7 = (C50142Oh) obj;
                        Context context2 = (Context) weakReference.get();
                        if (context2 != null) {
                            int i2 = r7.A00;
                            if (2 == i2) {
                                ((AnonymousClass12P) r5.A00.get()).A06(context2, intent);
                            } else if (i2 == 0) {
                                C12970iu.A1B(((C14820m6) ((C134526Ey) ((AnonymousClass18X) r5.A01.get())).A01.get()).A00.edit(), "shops_privacy_notice", -1);
                                r5.A00(context2, intent);
                            }
                        }
                    }
                }, C50142Oh.class, this);
                A00(context, AYx);
                return true;
            }
        }
        return false;
    }
}
