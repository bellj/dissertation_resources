package X;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Pair;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1Kr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28111Kr {
    public static final boolean A00;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 23) {
            z = true;
        }
        A00 = z;
    }

    public static C38811ok A00(C18720su r13, AnonymousClass148 r14, AnonymousClass1KS r15) {
        String str;
        C38461o8 A002;
        if (r15.A08 == null || r15.A01 == 1 || (A002 = r14.A00(r15.A0C)) == null) {
            int i = r15.A00;
            if (i == 0 && (str = r15.A08) != null) {
                i = (int) new File(str).length();
            }
            String str2 = r15.A0F;
            String str3 = r15.A07;
            String str4 = r15.A05;
            String str5 = r15.A0C;
            C38811ok r3 = new C38811ok(str2, str3, str4, str5, r15.A0B, r15.A0A, i, r15.A03, r15.A02);
            synchronized (r13.A0K) {
                C006202y r1 = r13.A01;
                if (r1 == null) {
                    r1 = new C006202y(60);
                    r13.A01 = r1;
                }
                r1.A08(str5, r15);
            }
            return r3;
        }
        String str6 = null;
        byte[] bArr = A002.A00;
        if (bArr != null && bArr.length > 0) {
            str6 = Base64.encodeToString(bArr, 3);
        }
        return new C38811ok(A002.A09, A002.A05, A002.A04, A002.A06, A002.A08, str6, A002.A01, A002.A03, A002.A02);
    }

    public static String A01(Context context, AnonymousClass1KS r5) {
        if (TextUtils.isEmpty(r5.A06)) {
            return context.getString(R.string.sticker_message_content_description);
        }
        return context.getString(R.string.sticker_message_content_description_with_emojis, r5.A06);
    }

    public static String A02(List list) {
        if (list.size() == 0) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1KS r0 = (AnonymousClass1KS) it.next();
                if (r0 != null) {
                    String str = r0.A0C;
                    String str2 = r0.A08;
                    if (str != null) {
                        instance.update(str.getBytes());
                    } else if (str2 != null) {
                        instance.update(str2.getBytes());
                    }
                }
            }
            return AnonymousClass1US.A0A(Base64.encodeToString(instance.digest(), 2));
        } catch (NoSuchAlgorithmException unused) {
            Log.e("StickerUtils/handleSHA256ErrorForStickersChecksum/could not get MD5 message digest");
            int size = list.size();
            String[] strArr = new String[size];
            for (int i = 0; i < size; i++) {
                AnonymousClass1KS r02 = (AnonymousClass1KS) list.get(i);
                if (r02 != null) {
                    String str3 = r02.A0C;
                    String str4 = r02.A08;
                    if (str3 != null) {
                        strArr[i] = str3;
                    } else if (str4 != null) {
                        strArr[i] = str4;
                    }
                }
            }
            return String.valueOf(Arrays.hashCode(strArr));
        }
    }

    public static String A03(List list) {
        if (list.size() == 0) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Pair pair = (Pair) it.next();
                if (pair != null) {
                    Object obj = pair.first;
                    if (obj != null) {
                        AnonymousClass1KS r0 = (AnonymousClass1KS) obj;
                        String str = r0.A0C;
                        String str2 = r0.A08;
                        if (str != null) {
                            instance.update(str.getBytes());
                        } else if (str2 != null) {
                            instance.update(str2.getBytes());
                        }
                    }
                    Object obj2 = pair.second;
                    if (obj2 != null) {
                        instance.update(String.valueOf(obj2).getBytes());
                    }
                }
            }
            return AnonymousClass1US.A0A(Base64.encodeToString(instance.digest(), 2));
        } catch (NoSuchAlgorithmException unused) {
            Log.e("StickerUtils/handleSHA256ErrorForWeightedStickersChecksum/could not get MD5 message digest");
            int size = list.size() << 1;
            String[] strArr = new String[size];
            for (int i = 0; i < size; i += 2) {
                Pair pair2 = (Pair) list.get(i >> 1);
                if (pair2 != null) {
                    Object obj3 = pair2.first;
                    if (obj3 != null) {
                        String str3 = ((AnonymousClass1KS) obj3).A0C;
                        String str4 = ((AnonymousClass1KS) pair2.first).A08;
                        if (str3 != null) {
                            strArr[i] = str3;
                        } else if (str4 != null) {
                            strArr[i] = str4;
                        }
                    }
                    Object obj4 = pair2.second;
                    if (obj4 != null) {
                        strArr[i + 1] = String.valueOf(obj4);
                    }
                }
            }
            return String.valueOf(Arrays.hashCode(strArr));
        }
    }
}
