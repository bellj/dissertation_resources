package X;

/* renamed from: X.0Ph  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05370Ph {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ AnonymousClass28D A01;
    public final /* synthetic */ AnonymousClass28D A02;

    public C05370Ph(C14260l7 r1, AnonymousClass28D r2, AnonymousClass28D r3) {
        this.A02 = r2;
        this.A00 = r1;
        this.A01 = r3;
    }

    public void A00() {
        AnonymousClass28D r1 = this.A02;
        AbstractC14200l1 A0G = r1.A0G(42);
        if (A0G != null) {
            AnonymousClass28D r3 = this.A01;
            C14210l2 r2 = new C14210l2();
            r2.A04(r3, 0);
            C14260l7 r12 = this.A00;
            r2.A04(r12, 1);
            C28701Oq.A01(r12, r3, r2.A03(), A0G);
            return;
        }
        AbstractC14200l1 A0G2 = r1.A0G(35);
        if (A0G2 != null) {
            C28701Oq.A01(this.A00, this.A01, new C14210l2().A03(), A0G2);
        }
    }

    public void A01(int i) {
        AbstractC14200l1 A0G = this.A02.A0G(38);
        if (A0G != null) {
            C14260l7 r5 = this.A00;
            AnonymousClass28D r3 = this.A01;
            C14210l2 r2 = new C14210l2();
            r2.A04(r3, 0);
            r2.A04(r5, 1);
            r2.A04(Integer.valueOf((int) (((float) i) / r5.A00().getResources().getDisplayMetrics().density)), 2);
            C28701Oq.A01(r5, r3, r2.A03(), A0G);
        }
    }

    public void A02(int i) {
        AnonymousClass28D r1 = this.A02;
        AbstractC14200l1 A0G = r1.A0G(43);
        if (A0G != null) {
            C14260l7 r5 = this.A00;
            AnonymousClass28D r3 = this.A01;
            C14210l2 r2 = new C14210l2();
            r2.A04(r3, 0);
            r2.A04(r5, 1);
            r2.A04(Integer.valueOf((int) (((float) i) / r5.A00().getResources().getDisplayMetrics().density)), 2);
            C28701Oq.A01(r5, r3, r2.A03(), A0G);
            return;
        }
        AbstractC14200l1 A0G2 = r1.A0G(36);
        if (A0G2 != null) {
            C28701Oq.A01(this.A00, this.A01, new C14210l2().A03(), A0G2);
        }
    }
}
