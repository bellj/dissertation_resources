package X;

/* renamed from: X.1d8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC33011d8 extends ActivityC13770kJ {
    public boolean A00 = false;

    public AbstractActivityC33011d8() {
        A0R(new C102894ps(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            AbstractActivityC33001d7 r1 = (AbstractActivityC33001d7) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) r1).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) r1).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) r1).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) r1).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) r1).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) r1).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) r1).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) r1).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) r1).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) r1).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) r1).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) r1).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) r1).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) r1).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) r1).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) r1).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) r1).A09 = r3.A06();
            ((ActivityC13790kL) r1).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) r1).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) r1).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) r1).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) r1).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) r1).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) r1).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) r1).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) r1).A08 = (C249317l) r2.A8B.get();
            r1.A0L = (AnonymousClass14X) r2.AFM.get();
            r1.A09 = (C19990v2) r2.A3M.get();
            r1.A01 = (C16170oZ) r2.AM4.get();
            r1.A0A = (C15650ng) r2.A4m.get();
            r1.A0P = (AnonymousClass19O) r2.ACO.get();
            r1.A06 = (C15550nR) r2.A45.get();
            r1.A02 = (C19850um) r2.A2v.get();
            r1.A08 = (AnonymousClass018) r2.ANb.get();
            r1.A0K = (C17070qD) r2.AFC.get();
            r1.A04 = (C243915i) r2.A37.get();
            r1.A0H = (C20710wC) r2.A8m.get();
            r1.A0D = (C20000v3) r2.AAG.get();
            r1.A0E = (C20050v8) r2.AAV.get();
            r1.A0F = (C15660nh) r2.ABE.get();
            r1.A0N = (C15860o1) r2.A3H.get();
            r1.A0I = (C17900ra) r2.AF5.get();
            r1.A03 = (AnonymousClass19Q) r2.A2u.get();
            r1.A07 = (C15890o4) r2.AN1.get();
            r1.A0B = (AnonymousClass1BB) r2.A68.get();
            r1.A0J = (C22710zW) r2.AF7.get();
            r1.A0O = (C255719x) r2.A5X.get();
            r1.A0C = (C15600nX) r2.A8x.get();
            r1.A0G = (AnonymousClass150) r2.A63.get();
        }
    }
}
