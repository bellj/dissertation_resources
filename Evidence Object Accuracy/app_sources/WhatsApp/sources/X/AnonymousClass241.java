package X;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape2S0400000_I0;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.241  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass241 extends AnonymousClass242 implements AnonymousClass1KU {
    public C58372oi A00;
    public AbstractC116245Ur A01;
    public C622235r A02;
    public AnonymousClass35q A03;
    public List A04 = new ArrayList();
    public boolean A05 = false;
    public final int A06 = 8;
    public final int A07;
    public final Activity A08;
    public final LayoutInflater A09;
    public final C14900mE A0A;
    public final C14850m9 A0B;
    public final C22210yi A0C;
    public final AnonymousClass1AB A0D;
    public final C235512c A0E;
    public final AbstractC116245Ur A0F;
    public final C64773Gv A0G;
    public final AnonymousClass1KT A0H;
    public final C69223Yk A0I;
    public final AbstractC14440lR A0J;
    public final String[] A0K = {"recents", "starred", "id_all", "id_love", "id_haha", "id_sad", "id_reaction", "id_celebrate"};

    @Override // X.AnonymousClass1KU
    public void AWh(String str, HashMap hashMap, HashMap hashMap2, HashSet hashSet, List list, int i) {
    }

    public AnonymousClass241(Activity activity, ViewGroup viewGroup, AbstractC05270Ox r27, C14900mE r28, AnonymousClass018 r29, C14850m9 r30, C18120rw r31, C235812f r32, C22210yi r33, AnonymousClass1AB r34, C235512c r35, AnonymousClass1KT r36, AbstractC14440lR r37) {
        super(activity, viewGroup, r27, r29, R.id.avatar_sticker_pager);
        C1113459a r2 = new C1113459a(this);
        this.A0F = r2;
        this.A0B = r30;
        this.A08 = activity;
        this.A0A = r28;
        this.A0J = r37;
        this.A0H = r36;
        LayoutInflater from = LayoutInflater.from(activity);
        this.A09 = from;
        this.A0E = r35;
        this.A0C = r33;
        this.A0D = r34;
        this.A0G = new C64773Gv();
        int dimensionPixelSize = super.A0A.getContext().getResources().getDimensionPixelSize(R.dimen.sticker_picker_item);
        this.A07 = dimensionPixelSize;
        Context context = super.A07;
        this.A02 = new C622235r(context, from, r30, this, r33, r34, r2, dimensionPixelSize);
        this.A03 = new AnonymousClass35q(context, from, r30, r34, r35, r2, dimensionPixelSize, true);
        C58372oi r1 = new C58372oi(r29, new AnonymousClass5WC[0]);
        this.A00 = r1;
        A03(r1);
        C69223Yk r5 = new C69223Yk(context, viewGroup, r29);
        this.A0I = r5;
        AbstractC116745Wq r22 = super.A04;
        if (r22 != null) {
            r22.Abx(null);
        }
        super.A04 = r5;
        r5.Abx(this);
        viewGroup.findViewById(R.id.avatar_edit_button).setOnClickListener(new ViewOnClickCListenerShape2S0400000_I0(this, r32, activity, r31, 1));
        r36.A0B.A03(r36.A0A);
        r32.A02(12);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass47H(0, "1", R.drawable.emoji_recent_focus));
        arrayList.add(new AnonymousClass47H(1, "2", R.drawable.sticker_favorites_focus));
        arrayList.add(new AnonymousClass47I(2, context.getString(R.string.avatar_stickers_category_all), "3"));
        arrayList.add(new AnonymousClass47I(3, context.getString(R.string.avatar_stickers_category_love), "4"));
        arrayList.add(new AnonymousClass47I(4, context.getString(R.string.avatar_stickers_category_happy), "5"));
        arrayList.add(new AnonymousClass47I(5, context.getString(R.string.avatar_stickers_category_sad), "6"));
        arrayList.add(new AnonymousClass47I(6, context.getString(R.string.avatar_stickers_category_reaction), "7"));
        arrayList.add(new AnonymousClass47I(7, context.getString(R.string.avatar_stickers_category_celebrate), "8"));
        C69223Yk r23 = this.A0I;
        ArrayList arrayList2 = r23.A08;
        arrayList2.clear();
        arrayList2.addAll(arrayList);
        C54342gZ r4 = r23.A07;
        List list = r4.A00;
        AnonymousClass0SZ A00 = AnonymousClass0RD.A00(new C74573iK(list, arrayList));
        list.clear();
        list.addAll(arrayList);
        A00.A02(r4);
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(this.A02);
        arrayList3.add(this.A03);
        for (int i = 2; i < this.A06; i++) {
            String str = this.A0K[i];
            C37451mQ r6 = new C37451mQ();
            r6.A0B = str;
            r6.A0D = "";
            r6.A0F = "";
            ArrayList arrayList4 = new ArrayList();
            int i2 = 0;
            do {
                AnonymousClass1KS r24 = new AnonymousClass1KS();
                r24.A0C = "fileHash";
                arrayList4.add(r24);
                i2++;
            } while (i2 < 16);
            r6.A0J = arrayList4;
            r6.A0I = new LinkedList();
            r6.A0P = false;
            r6.A0N = true;
            r6.A0O = false;
            AnonymousClass1KZ r8 = new AnonymousClass1KZ(r6);
            arrayList3.add(new C622335s(context, this.A09, this.A0B, this.A0D, r8, this.A0E, this.A0F, this.A07));
        }
        this.A04 = arrayList3;
        AnonymousClass5WC[] r25 = (AnonymousClass5WC[]) arrayList3.toArray(new AnonymousClass5WC[arrayList3.size()]);
        C58372oi r0 = this.A00;
        if (r0 == null) {
            C58372oi r02 = new C58372oi(super.A0B, r25);
            this.A00 = r02;
            A03(r02);
            return;
        }
        r0.A0L(r25);
        r0.A06();
    }

    public static final AnonymousClass1KZ A00(AnonymousClass1KZ r2, String str, String str2, List list) {
        C37451mQ r1 = new C37451mQ();
        r1.A0J = list;
        r1.A0B = str;
        r1.A0D = str2;
        r1.A0F = r2.A0H;
        r1.A0I = new LinkedList();
        r1.A0L = r2.A0L;
        r1.A0P = r2.A0O;
        r1.A0O = r2.A05;
        return new AnonymousClass1KZ(r1);
    }

    public void A04(String str) {
        List<AnonymousClass5WC> list = this.A04;
        for (AnonymousClass5WC r1 : list) {
            if (str.equals(r1.getId())) {
                A02(list.indexOf(r1), true);
                return;
            }
        }
    }

    @Override // X.AnonymousClass1KU
    public void AQC() {
        this.A03.A01();
        if (this.A05) {
            A04("starred");
        }
    }

    @Override // X.AnonymousClass1KU
    public void AUj() {
        this.A02.A01();
    }

    @Override // X.AnonymousClass1KU
    public void AWk(AnonymousClass1KZ r4) {
        if (r4.A0N) {
            this.A0J.Ab6(new RunnableBRunnable0Shape8S0200000_I0_8(this, 13, r4));
        }
    }

    @Override // X.AnonymousClass1KU
    public void Acu(String str, HashMap hashMap, HashMap hashMap2, HashSet hashSet, List list) {
        String str2;
        if (list.size() == 0) {
            str2 = "AvatarStickerPicker/setStickerPacks Avatar sticker pack is empty";
        } else {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1KZ r3 = (AnonymousClass1KZ) it.next();
                if (r3.A0N) {
                    this.A0J.Ab6(new RunnableBRunnable0Shape8S0200000_I0_8(this, 13, r3));
                    return;
                }
            }
            str2 = "AvatarStickerPicker/setStickerPacks got sticker packs, but no Avatar sticker pack";
        }
        Log.e(str2);
    }
}
