package X;

/* renamed from: X.59q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1115059q implements AnonymousClass1Kp {
    public final /* synthetic */ AbstractC36781kZ A00;

    public C1115059q(AbstractC36781kZ r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1Kp
    public void A7o() {
        C36021jC.A01(this.A00.A01, 0);
    }

    @Override // X.AnonymousClass1Kp
    public void AHv(boolean z) {
        ActivityC000800j r1 = this.A00.A01;
        int i = 20;
        if (z) {
            i = 0;
        }
        C36021jC.A01(r1, i);
    }
}
