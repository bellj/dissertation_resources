package X;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.inappsupport.ui.SupportTopicsActivity;
import com.whatsapp.support.faq.FaqItemActivity;
import com.whatsapp.support.faq.SearchFAQ;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: X.5ov  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124295ov extends AbstractC16350or {
    public ProgressDialog A00;
    public final Bundle A01;
    public final AnonymousClass19Y A02;
    public final C15450nH A03;
    public final C18790t3 A04;
    public final AnonymousClass018 A05;
    public final AbstractC28901Pl A06;
    public final AnonymousClass1IR A07;
    public final C18810t5 A08;
    public final C17900ra A09;
    public final C30931Zj A0A = C117305Zk.A0V("PaymentSupportTask", "payment-settings");
    public final String A0B;
    public final WeakReference A0C;

    public C124295ov(Bundle bundle, ActivityC13810kN r4, AnonymousClass19Y r5, C15450nH r6, C18790t3 r7, AnonymousClass018 r8, AbstractC28901Pl r9, AnonymousClass1IR r10, C18810t5 r11, C17900ra r12, String str) {
        this.A0C = C12970iu.A10(r4);
        this.A04 = r7;
        this.A03 = r6;
        this.A02 = r5;
        this.A05 = r8;
        this.A08 = r11;
        this.A09 = r12;
        this.A0B = str;
        this.A06 = r9;
        this.A07 = r10;
        this.A01 = bundle;
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x017c A[Catch: all -> 0x01c9, TryCatch #4 {all -> 0x01ce, blocks: (B:26:0x00d4, B:55:0x01c2, B:27:0x00ee, B:30:0x00fb, B:31:0x0103, B:34:0x011f, B:36:0x0129, B:37:0x012d, B:39:0x0135, B:41:0x013f, B:42:0x0144, B:44:0x014a, B:47:0x0174, B:49:0x017c, B:50:0x0184, B:52:0x018c, B:53:0x01ba), top: B:68:0x00d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x018c A[Catch: all -> 0x01c9, TryCatch #4 {all -> 0x01ce, blocks: (B:26:0x00d4, B:55:0x01c2, B:27:0x00ee, B:30:0x00fb, B:31:0x0103, B:34:0x011f, B:36:0x0129, B:37:0x012d, B:39:0x0135, B:41:0x013f, B:42:0x0144, B:44:0x014a, B:47:0x0174, B:49:0x017c, B:50:0x0184, B:52:0x018c, B:53:0x01ba), top: B:68:0x00d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c0  */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r18) {
        /*
        // Method dump skipped, instructions count: 488
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124295ov.A05(java.lang.Object[]):java.lang.Object");
    }

    @Override // X.AbstractC16350or
    public void A06() {
        Context context = (Context) this.A0C.get();
        if (context != null) {
            if (this.A00 == null) {
                ProgressDialog progressDialog = new ProgressDialog(context);
                this.A00 = progressDialog;
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: X.61y
                    @Override // android.content.DialogInterface.OnCancelListener
                    public final void onCancel(DialogInterface dialogInterface) {
                        C124295ov.this.A03(true);
                    }
                });
                this.A00.setCanceledOnTouchOutside(false);
            }
            if (!this.A00.isShowing()) {
                this.A00.setMessage(context.getString(R.string.help_loading_progress_label));
                this.A00.setIndeterminate(true);
                this.A00.show();
            }
        }
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        ProgressDialog progressDialog;
        Intent A02;
        C127405uR r8 = (C127405uR) obj;
        ActivityC13810kN r9 = (ActivityC13810kN) this.A0C.get();
        if (r9 != null) {
            if (r8 != null) {
                C127855vA r3 = r8.A00;
                if (r3 != null) {
                    Intent A0D = C12990iw.A0D(r9, FaqItemActivity.class);
                    A0D.putExtra("title", r3.A02);
                    A0D.putExtra("content", r3.A00);
                    A0D.putExtra("url", r3.A03);
                    A0D.putExtra("article_id", r3.A01);
                    boolean z = r3.A04;
                    A0D.putExtra("show_contact_support_button", z);
                    A0D.putExtra("contact_us_context", this.A0B);
                    if (z) {
                        A0D.putParcelableArrayListExtra("payments_support_topics", r8.A02);
                    }
                    Bundle bundle = this.A01;
                    bundle.putInt("com.whatsapp.support.DescribeProblemActivity.type", 3);
                    A0D.putExtra("describe_problem_fields", bundle);
                    r9.A2E(A0D, 48);
                    r9.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    ArrayList arrayList = r8.A01;
                    if (arrayList == null || arrayList.isEmpty()) {
                        ArrayList arrayList2 = r8.A02;
                        if (arrayList2 != null && !arrayList2.isEmpty()) {
                            A02 = SupportTopicsActivity.A02(r9, this.A01, arrayList2);
                        }
                    } else {
                        String str = this.A0B;
                        Bundle bundle2 = this.A01;
                        ArrayList arrayList3 = r8.A02;
                        A02 = C12990iw.A0D(r9, SearchFAQ.class);
                        A02.putExtra("com.whatsapp.support.faq.SearchFAQ.from", str);
                        A02.putExtra("com.whatsapp.support.faq.SearchFAQ.count", arrayList.size());
                        A02.putExtra("describe_problem_bundle", bundle2);
                        A02.putExtra("payments_support_faqs", arrayList);
                        A02.putExtra("payments_support_topics", arrayList3);
                        A02.putExtra("com.whatsapp.support.faq.SearchFAQ.usePaymentsFlow", true);
                    }
                    r9.A2E(A02, 48);
                }
                progressDialog = this.A00;
                if (progressDialog != null && progressDialog.isShowing()) {
                    this.A00.cancel();
                    return;
                }
            }
            if (this.A03.A05(AbstractC15460nI.A0q)) {
                r9.Ado(R.string.payments_cs_email_disabled);
            } else {
                AnonymousClass19Y r82 = this.A02;
                String str2 = this.A0B;
                Integer A0h = C12970iu.A0h();
                Bundle bundle3 = this.A01;
                Intent A00 = r82.A00(r9, bundle3, null, A0h, str2, null, null, null, false);
                A00.putExtras(bundle3);
                r9.A2E(A00, 48);
            }
            progressDialog = this.A00;
            if (progressDialog != null) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r2.getBoolean("children_skippable") == false) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008c, code lost:
        if (r15 == 2) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.ArrayList A08(org.json.JSONArray r14, int r15) {
        /*
            r13 = this;
            int r0 = r14.length()
            r10 = 0
            if (r0 != 0) goto L_0x0008
            return r10
        L_0x0008:
            java.util.ArrayList r4 = X.C12960it.A0l()
            r3 = 0
        L_0x000d:
            int r0 = r14.length()
            if (r3 >= r0) goto L_0x00a4
            org.json.JSONObject r2 = r14.getJSONObject(r3)
            java.lang.String r0 = "id"
            java.lang.String r7 = r2.getString(r0)
            java.lang.String r0 = "title"
            java.lang.String r8 = r2.getString(r0)
            java.lang.String r1 = "children"
            boolean r0 = r2.has(r1)
            if (r0 == 0) goto L_0x00a2
            org.json.JSONArray r0 = r2.getJSONArray(r1)
            java.util.ArrayList r11 = r13.A08(r0, r15)
        L_0x0033:
            java.lang.String r1 = "children_skippable"
            boolean r0 = r2.has(r1)
            if (r0 == 0) goto L_0x0042
            boolean r0 = r2.getBoolean(r1)
            r12 = 1
            if (r0 != 0) goto L_0x0043
        L_0x0042:
            r12 = 0
        L_0x0043:
            r5 = 2
            if (r15 != r5) goto L_0x008b
            java.lang.String r1 = "description"
            boolean r0 = r2.has(r1)
            if (r0 == 0) goto L_0x008b
            java.lang.String r9 = r2.getString(r1)
        L_0x0052:
            java.lang.String r1 = "chat_support"
            boolean r0 = r2.has(r1)
            if (r0 == 0) goto L_0x008f
            org.json.JSONObject r6 = r2.getJSONObject(r1)
            java.lang.String r0 = "auth_required"
            boolean r5 = r6.getBoolean(r0)
            java.lang.String r1 = "required_data"
            boolean r0 = r6.has(r1)
            if (r0 == 0) goto L_0x0091
            java.util.ArrayList r2 = X.C12960it.A0l()
            org.json.JSONArray r6 = r6.getJSONArray(r1)
            r1 = 0
        L_0x0075:
            int r0 = r6.length()
            if (r1 >= r0) goto L_0x0085
            java.lang.String r0 = r6.getString(r1)
            r2.add(r0)
            int r1 = r1 + 1
            goto L_0x0075
        L_0x0085:
            X.4m2 r6 = new X.4m2
            r6.<init>(r2, r5)
            goto L_0x0096
        L_0x008b:
            r9 = r10
            if (r15 != r5) goto L_0x008f
            goto L_0x0052
        L_0x008f:
            r6 = r10
            goto L_0x0096
        L_0x0091:
            X.4m2 r6 = new X.4m2
            r6.<init>(r10, r5)
        L_0x0096:
            X.3M6 r5 = new X.3M6
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            r4.add(r5)
            int r3 = r3 + 1
            goto L_0x000d
        L_0x00a2:
            r11 = r10
            goto L_0x0033
        L_0x00a4:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C124295ov.A08(org.json.JSONArray, int):java.util.ArrayList");
    }
}
