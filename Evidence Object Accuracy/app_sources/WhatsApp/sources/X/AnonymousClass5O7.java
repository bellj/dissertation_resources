package X;

/* renamed from: X.5O7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5O7 extends AbstractC112995Fp {
    public static final byte[] A04 = {105, 0, 114, 34, 100, -55, 4, 35, -115, 58, -37, -106, 70, -23, 42, -60, 24, -2, -84, -108, 0, -19, 7, 18, -64, -122, -36, -62, -17, 76, -87, 43};
    public long A00 = 0;
    public AnonymousClass20K A01;
    public boolean A02;
    public final AnonymousClass5O5 A03;

    public AnonymousClass5O7(AnonymousClass5XE r3) {
        super(r3);
        this.A03 = new AnonymousClass5O5(r3, r3.AAt() << 3);
    }

    @Override // X.AnonymousClass5XE
    public void reset() {
        this.A00 = 0;
        this.A03.reset();
    }

    @Override // X.AnonymousClass5XE
    public String AAf() {
        String AAf = this.A03.AAf();
        StringBuilder A0h = C12960it.A0h();
        int indexOf = AAf.indexOf(47);
        C72453ed.A1O(AAf, A0h, 0, indexOf);
        A0h.append("/G");
        return C12960it.A0d(AAf.substring(indexOf + 1), A0h);
    }

    @Override // X.AnonymousClass5XE
    public int AAt() {
        return this.A03.A00;
    }

    @Override // X.AnonymousClass5XE
    public void AIf(AnonymousClass20L r3, boolean z) {
        this.A00 = 0;
        this.A03.AIf(r3, z);
        this.A02 = z;
        if (r3 instanceof C113075Fx) {
            r3 = ((C113075Fx) r3).A00;
        }
        if (r3 instanceof C113025Fs) {
            r3 = ((C113025Fs) r3).A01;
        }
        if (r3 instanceof C113005Fq) {
            r3 = null;
        }
        this.A01 = (AnonymousClass20K) r3;
    }

    @Override // X.AnonymousClass5XE
    public int AZY(byte[] bArr, byte[] bArr2, int i, int i2) {
        int i3 = this.A03.A00;
        A01(bArr, bArr2, i, i3, i2);
        return i3;
    }
}
