package X;

import org.json.JSONObject;

/* renamed from: X.2SA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SA {
    public final int A00;
    public final boolean A01;
    public final /* synthetic */ C50942Ry A02;

    public AnonymousClass2SA(C50942Ry r3, AnonymousClass1V8 r4) {
        boolean z;
        this.A02 = r3;
        if (r4 != null) {
            this.A00 = r4.A05("max_from_sender", -1);
            z = "1".equals(r4.A0I("usync_pay_eligible_offers_includes_current_offer_id", "0"));
        } else {
            this.A00 = -1;
            z = false;
        }
        this.A01 = z;
    }

    public AnonymousClass2SA(C50942Ry r3, String str) {
        this.A02 = r3;
        JSONObject jSONObject = new JSONObject(str);
        this.A00 = jSONObject.getInt("max_from_sender");
        this.A01 = jSONObject.getBoolean("usync_pay_eligible_offers_includes_current_offer_id");
    }
}
