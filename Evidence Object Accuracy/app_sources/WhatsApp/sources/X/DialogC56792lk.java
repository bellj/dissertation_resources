package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.IDxObserverShape4S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S0301000_I1;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2lk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC56792lk extends DialogC53322dr {
    public View A00;
    public AnonymousClass2KT A01;
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final C18790t3 A03;
    public final C22640zP A04;
    public final AnonymousClass2IA A05;
    public final C32701cb A06;
    public final C254319j A07;
    public final AnonymousClass4KX A08;
    public final C54182gJ A09 = new C54182gJ(new C74633iQ());
    public final AbstractC14640lm A0A;
    public final AnonymousClass1JF A0B;

    public DialogC56792lk(Context context, C18790t3 r4, C22640zP r5, AnonymousClass2IA r6, C32701cb r7, C254319j r8, AnonymousClass4KX r9, AbstractC14640lm r10, AnonymousClass1JF r11) {
        super(context, R.style.RoundedBottomSheetDialogTheme);
        this.A0A = r10;
        this.A03 = r4;
        this.A07 = r8;
        this.A0B = r11;
        this.A08 = r9;
        this.A06 = r7;
        this.A04 = r5;
        this.A05 = r6;
    }

    @Override // X.DialogC53322dr, X.AnonymousClass04T, android.app.Dialog
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.conversation_icebreaker_bottom_sheet_view);
        RecyclerView recyclerView = (RecyclerView) AnonymousClass0KS.A00(this, R.id.questions_view);
        getContext();
        C12990iw.A1K(recyclerView);
        C54182gJ r6 = this.A09;
        recyclerView.setAdapter(r6);
        ArrayList A0l = C12960it.A0l();
        AnonymousClass1JF r2 = this.A0B;
        List<C92664Wv> list = r2.A06;
        if (list != null) {
            for (C92664Wv r3 : list) {
                A0l.add(new AnonymousClass4QG(this.A02, r3));
            }
        }
        C71203cY r7 = new C71203cY(A0l);
        C91574Sg r5 = r6.A00;
        int i = r5.A00 + 1;
        r5.A00 = i;
        C71203cY r62 = r5.A01;
        if (r7 != r62) {
            if (r62 == null) {
                r5.A01 = r7;
                r5.A03.ARP(0, r7.A00.size());
            } else {
                r5.A02.A01.execute(new RunnableBRunnable0Shape1S0301000_I1(r5, r62, r7, i, 0));
            }
        }
        View A00 = AnonymousClass0KS.A00(this, R.id.send_button);
        this.A00 = A00;
        C12960it.A0x(A00, this, 4);
        C12960it.A0x(AnonymousClass0KS.A00(this, R.id.close), this, 5);
        this.A01 = new AnonymousClass2KT(this.A03, this.A05.A01(this.A06, r2));
        WebPagePreviewView webPagePreviewView = (WebPagePreviewView) AnonymousClass0KS.A00(this, R.id.web_page_preview);
        webPagePreviewView.A0A(this.A01, null, false, this.A04.A07());
        View findViewById = webPagePreviewView.findViewById(R.id.link_preview_content);
        if (findViewById != null) {
            findViewById.setBackgroundResource(R.drawable.round_corner_icebreaker_link_preview_background);
        }
        Drawable A03 = C015607k.A03(C12970iu.A0C(getContext(), R.drawable.balloon_incoming_frame).mutate());
        C015607k.A0A(A03, AnonymousClass00T.A00(getContext(), R.color.attach_popup_background));
        webPagePreviewView.setForeground(A03);
        this.A02.A08(new IDxObserverShape4S0100000_2_I1(this, 40));
        View A002 = AnonymousClass0KS.A00(this, R.id.design_bottom_sheet);
        BottomSheetBehavior A003 = BottomSheetBehavior.A00(A002);
        A003.A0M(3);
        A003.A0N = true;
        A003.A0L(A002.getHeight());
        this.A07.A00(3, this.A0A.getRawString(), true);
    }
}
