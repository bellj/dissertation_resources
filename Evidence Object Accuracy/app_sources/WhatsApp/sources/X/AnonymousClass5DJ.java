package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.5DJ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5DJ implements Iterator {
    public Collection collection;
    public Object key;
    public final Iterator keyIterator;
    public final /* synthetic */ AbstractC80933tC this$0;
    public Iterator valueIterator;

    public Object output(Object obj, Object obj2) {
        return obj2;
    }

    public AnonymousClass5DJ(AbstractC80933tC r2) {
        this(r2, 0);
    }

    public AnonymousClass5DJ(AbstractC80933tC r2, int i) {
        this.this$0 = r2;
        this.keyIterator = C12960it.A0n(r2.map);
        this.key = null;
        this.collection = null;
        this.valueIterator = AnonymousClass1I4.emptyModifiableIterator();
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.keyIterator.hasNext() || this.valueIterator.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        if (!this.valueIterator.hasNext()) {
            Map.Entry A15 = C12970iu.A15(this.keyIterator);
            this.key = A15.getKey();
            Collection collection = (Collection) A15.getValue();
            this.collection = collection;
            this.valueIterator = collection.iterator();
        }
        return output(this.key, this.valueIterator.next());
    }

    @Override // java.util.Iterator
    public void remove() {
        this.valueIterator.remove();
        if (this.collection.isEmpty()) {
            this.keyIterator.remove();
        }
        AbstractC80933tC.access$210(this.this$0);
    }
}
