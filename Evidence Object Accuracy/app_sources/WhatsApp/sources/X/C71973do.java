package X;

import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;

/* renamed from: X.3do  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71973do extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogSearchFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71973do(CatalogSearchFragment catalogSearchFragment) {
        super(0);
        this.this$0 = catalogSearchFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        CatalogSearchFragment catalogSearchFragment = this.this$0;
        AnonymousClass3EX r2 = (AnonymousClass3EX) catalogSearchFragment.A0W.getValue();
        AnonymousClass1CR r1 = this.this$0.A0J;
        if (r1 != null) {
            return new AnonymousClass02A(new C105604uK(r1, r2), catalogSearchFragment).A00(AnonymousClass2fT.class);
        }
        throw C16700pc.A06("productSearchLogger");
    }
}
