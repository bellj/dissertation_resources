package X;

/* renamed from: X.0Ym  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ym implements AnonymousClass02B {
    public boolean A00 = false;
    public final AnonymousClass03M A01;
    public final AnonymousClass0QL A02;

    public AnonymousClass0Ym(AnonymousClass03M r2, AnonymousClass0QL r3) {
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass02B
    public void ANq(Object obj) {
        this.A01.AS2(this.A02, obj);
        this.A00 = true;
    }

    public String toString() {
        return this.A01.toString();
    }
}
