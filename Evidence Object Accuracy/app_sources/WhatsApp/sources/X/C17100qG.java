package X;

/* renamed from: X.0qG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17100qG {
    public final C14330lG A00;
    public final AnonymousClass1D6 A01;
    public final C15810nw A02;
    public final C14830m7 A03;
    public final C15880o3 A04;
    public final C21600xg A05;
    public final C33461e7 A06;
    public final C33481e9 A07;
    public final AbstractC14440lR A08;

    public C17100qG(C14330lG r8, AnonymousClass1D6 r9, C15810nw r10, C14830m7 r11, C15650ng r12, C20120vF r13, C15660nh r14, C15880o3 r15, C16490p7 r16, C21600xg r17, AnonymousClass19O r18, AbstractC14440lR r19, C21560xc r20) {
        this.A03 = r11;
        this.A08 = r19;
        this.A00 = r8;
        this.A02 = r10;
        this.A04 = r15;
        this.A01 = r9;
        this.A05 = r17;
        C33461e7 r5 = new C33461e7(r11, r20);
        this.A06 = r5;
        this.A07 = new C33481e9(r12, r13, r14, r16, r5, r18);
    }

    public final boolean A00(Long l) {
        return l == null || this.A03.A00() - l.longValue() > 1296000000;
    }
}
