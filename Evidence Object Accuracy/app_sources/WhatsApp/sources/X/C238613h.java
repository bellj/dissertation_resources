package X;

import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.Mp4Ops;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/* renamed from: X.13h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238613h {
    public final C15650ng A00;
    public final C240113w A01;
    public final C238113c A02;
    public final C240213x A03;
    public final C239113m A04;
    public final C22370yy A05;
    public final C240013v A06;
    public final C22170ye A07;
    public final C17230qT A08;

    public C238613h(C15650ng r1, C240113w r2, C238113c r3, C240213x r4, C239113m r5, C22370yy r6, C240013v r7, C22170ye r8, C17230qT r9) {
        this.A07 = r8;
        this.A00 = r1;
        this.A02 = r3;
        this.A06 = r7;
        this.A08 = r9;
        this.A05 = r6;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:199:0x0017 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [X.0mz, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r0v7, types: [X.0ng] */
    /* JADX WARN: Type inference failed for: r0v38, types: [X.0ye] */
    /* JADX WARN: Type inference failed for: r1v25, types: [X.0mz] */
    /* JADX WARN: Type inference failed for: r0v51, types: [X.0ye] */
    /* JADX WARN: Type inference failed for: r1v26, types: [X.0mz, X.1gl] */
    /* JADX WARN: Type inference failed for: r0v53, types: [X.0ye] */
    /* JADX WARN: Type inference failed for: r0v78, types: [X.0ye] */
    public final void A00(AbstractC15340mz r84, C28941Pp r85) {
        C20870wS r5;
        int i;
        C16310on A02;
        AnonymousClass1JU A06;
        C34831gm r1 = r84;
        if ("peer".equals(r85.A0R)) {
            C240013v r4 = this.A06;
            if (r1 instanceof C34821gl) {
                try {
                    r1 = (C34821gl) r1;
                    Log.i("PeerMessageHandler/handleKeyShareMessage");
                    try {
                        C233311g r9 = r4.A03;
                        DeviceJid deviceJid = r1.A17;
                        boolean z = false;
                        if (deviceJid != null) {
                            HashMap A14 = r1.A14();
                            HashSet hashSet = new HashSet(A14.values());
                            hashSet.remove(null);
                            r9.A09(hashSet);
                            Set keySet = A14.keySet();
                            StringBuilder sb = new StringBuilder("SyncdKeyManager/handleAppStateSyncKeyShareMessage syncdKeyIds = ");
                            sb.append(keySet);
                            Log.i(sb.toString());
                            if (keySet.size() == hashSet.size()) {
                                z = true;
                            }
                            C22090yV r6 = r9.A07;
                            List<C34831gm> A04 = r6.A04((byte) 39);
                            ArrayList arrayList = new ArrayList();
                            for (C34831gm r2 : A04) {
                                if (z || deviceJid.equals(((AbstractC30271Wt) r2).A00)) {
                                    if (keySet.equals(Collections.unmodifiableSet(r2.A00))) {
                                        arrayList.add(Long.valueOf(r2.A11));
                                    }
                                }
                            }
                            r6.A06(arrayList);
                            if (!r9.A0B()) {
                                Log.i("PeerMessageHandler/handleKeyShareMessage trySync");
                                r4.A05.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r4.A02, 38));
                            } else {
                                throw new AnonymousClass1K1(30, null);
                            }
                        }
                    } catch (AnonymousClass1K1 e) {
                        r4.A02.A0K(Integer.valueOf(e.errorCode));
                    }
                } finally {
                    r4.A04.A04(r1);
                }
            } else if (r1 instanceof C34831gm) {
                r1 = (C34831gm) r1;
                Log.i("PeerMessageHandler/handleKeyRequestMessage");
                C233311g r7 = r4.A03;
                DeviceJid deviceJid2 = r1.A17;
                if (deviceJid2 != null) {
                    HashMap A03 = r7.A03(Collections.unmodifiableSet(r1.A00));
                    StringBuilder sb2 = new StringBuilder("SyncdKeyManager/handleAppStateSyncKeyRequestMessage syncdKeyMap = ");
                    sb2.append(A03);
                    Log.i(sb2.toString());
                    r7.A08(deviceJid2, A03, false);
                }
            } else if (r1 instanceof C34841gn) {
                r1 = (C34841gn) r1;
                Set set = r1.A01;
                Log.i(String.format("PeerMessageHandler/handleFatalExceptionNotificationMessage: time = %s; collectionNames=%s", Long.valueOf(r1.A00), set));
                C18850tA r52 = r4.A02;
                synchronized (r52) {
                    DeviceJid deviceJid3 = r1.A17;
                    if (!(deviceJid3 == null || (A06 = r52.A0V.A06(deviceJid3.device)) == null)) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("sync-manager/handleFatalExceptionOnCompanion companion: ");
                        sb3.append((int) A06.A05.device);
                        sb3.append("; [");
                        sb3.append(A06.A06);
                        sb3.append('(');
                        sb3.append(A06.A07);
                        sb3.append(")]");
                        Log.i(sb3.toString());
                    }
                    r52.A03.AaV("app-sate-sync-handle-fatal-exception-on-companion", set.toString(), false);
                    r52.A0K(null);
                }
            } else if (r1 instanceof C34811gk) {
                Log.i("PeerMessageHandler/handleInitialSecurityNotificationSettingSyncMessage");
                C232410x r0 = r4.A01;
                synchronized (r0.A03) {
                    r0.A00.A08();
                }
            } else if (r1 instanceof C34771gg) {
                Log.i("PeerMessageHandler/handleHistorySyncNoticationMessage");
                r4.A00.A00.A08();
                Log.e("ReceiveHistorySyncManager/ Received history sync as primary device");
            } else if (r84 == null) {
            }
        } else {
            AnonymousClass1V4 A00 = this.A08.A00(0, r85.A06);
            if (A00 != null) {
                A00.A02(4);
            }
            if (r84 == null) {
                C240113w r22 = this.A01;
                A02 = r22.A01.A02();
                try {
                    C15650ng r62 = r22.A00;
                    AnonymousClass1IS r12 = r85.A0C;
                    if (r12 == null) {
                        r12 = r85.A0i;
                    }
                    if (r62.A0K.A03(r12) == null) {
                        C20320vZ r53 = r22.A02;
                        AnonymousClass1IS r3 = r85.A0C;
                        if (r3 == null) {
                            r3 = r85.A0i;
                        }
                        AbstractC15340mz A01 = r53.A01(r3, (byte) 11, r85.A0f);
                        r85.A04(A01);
                        if (A01 instanceof C30371Xd) {
                            ((C30371Xd) A01).A00 = r85.A04;
                        }
                        r62.A0p(A01);
                    }
                } finally {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                }
            } else if (C30041Vv.A0m(r1)) {
                C238113c r23 = this.A02;
                AnonymousClass009.A05(r1);
                r23.A03((C30331Wz) r1, true);
            } else {
                if (r1 instanceof AnonymousClass1XK) {
                    C240213x r63 = this.A03;
                    AnonymousClass1XK r54 = (AnonymousClass1XK) r1;
                    C241814n r72 = r63.A01;
                    if (r72.A01(r54.A00)) {
                        AnonymousClass1PE A012 = r63.A01(r54);
                        if (A012 != null) {
                            AnonymousClass1PG r92 = A012.A0Y;
                            AbstractC14640lm r8 = r54.A0z.A00;
                            long j = r54.A0I;
                            if (!r72.A03(r8, r92, Long.valueOf(j), r54.A00, j) && r54.A0B == 0) {
                                A02 = r63.A04.A02();
                                try {
                                    AnonymousClass1Lx A002 = A02.A00();
                                    r63.A04(A012, r54);
                                    A002.A00();
                                    A002.close();
                                    A02.close();
                                } finally {
                                }
                            }
                        }
                    }
                    this.A07.A04(r1);
                    return;
                }
                if (r1 instanceof AnonymousClass1Iv) {
                    C239113m r42 = this.A04;
                    r42.A05.A01(new RunnableBRunnable0Shape4S0200000_I0_4(r42, 35, r1), 52);
                } else if (r1 instanceof C30291Wv) {
                    C30291Wv r13 = (C30291Wv) r1;
                    String str = r13.A03;
                    String str2 = r13.A01;
                    String str3 = r13.A02;
                    long j2 = r13.A00;
                    if (str == null || str2 == null || str3 == null) {
                        Log.e("DecryptMessageHandler/handleMediaNotifyMessage wrong data in medianotify message");
                    } else {
                        C22370yy r02 = this.A05;
                        C22600zL r64 = r02.A0e;
                        synchronized (r64.A0H) {
                            if (r64.A00 != null) {
                                Uri parse = Uri.parse(str);
                                for (AnonymousClass1n2 r14 : r64.A00.A0A) {
                                    if (r14.A04.equals(parse.getHost())) {
                                        AnonymousClass12J r15 = r02.A0U;
                                        C14370lK r47 = C14370lK.A08;
                                        if (!r15.A02(r47, 0, j2, true, false, false, false)) {
                                            StringBuilder sb4 = new StringBuilder("mediadownloadmanager/queueexpresspathdownload auto download not enabled, ignore ep download for enc file hash: ");
                                            sb4.append(str2);
                                            Log.i(sb4.toString());
                                            r5 = r02.A07;
                                            i = 24;
                                        } else {
                                            HashMap hashMap = r02.A0r;
                                            synchronized (hashMap) {
                                                if (hashMap.containsKey(str3)) {
                                                    StringBuilder sb5 = new StringBuilder();
                                                    sb5.append("MediaDownloadManager/isExpressPathDownloadDuplicated Existing regular download job ");
                                                    sb5.append(hashMap.get(str3));
                                                    Log.i(sb5.toString());
                                                } else {
                                                    HashMap hashMap2 = r02.A0s;
                                                    synchronized (hashMap2) {
                                                        if (hashMap2.containsKey(str2)) {
                                                            StringBuilder sb6 = new StringBuilder();
                                                            sb6.append("MediaDownloadManager/isExpressPathDownloadDuplicated Existing express path download job ");
                                                            sb6.append(hashMap2.get(str2));
                                                            Log.i(sb6.toString());
                                                        } else {
                                                            C14330lG r10 = r02.A04;
                                                            C41471ta r32 = new C41471ta(null, r47, null, null, r10.A0C(r47, str2, str3, str, false, false), str, str3, null, UUID.randomUUID().toString(), str2, "application/octet-stream", null, null, null, null, null, 0, 0, 2, 8, 1, 3, j2, 0, false, false, false, false, false, false, false, false, false, false);
                                                            C14830m7 r55 = r02.A0E;
                                                            Mp4Ops mp4Ops = r02.A08;
                                                            C14850m9 r56 = r02.A0M;
                                                            C14900mE r57 = r02.A05;
                                                            C16590pI r58 = r02.A0F;
                                                            AbstractC15710nm r59 = r02.A03;
                                                            AbstractC14440lR r93 = r02.A0m;
                                                            C18790t3 r510 = r02.A0B;
                                                            C21040wk r511 = r02.A02;
                                                            C15450nH r512 = r02.A0A;
                                                            C26501Ds r513 = r02.A0o;
                                                            C14410lO r514 = r02.A0P;
                                                            C14950mJ r515 = r02.A0G;
                                                            C26511Dt r516 = r02.A0i;
                                                            C20870wS r517 = r02.A07;
                                                            C239713s r518 = r02.A0Z;
                                                            C26521Du r519 = r02.A0l;
                                                            C22590zK r520 = r02.A0h;
                                                            C21710xr r521 = r02.A0p;
                                                            AnonymousClass155 r522 = r02.A0R;
                                                            C15860o1 r523 = r02.A0g;
                                                            C15660nh r152 = r02.A0I;
                                                            C14420lP r142 = r02.A0T;
                                                            C28921Pn r524 = new C28921Pn(r02.A01, r511, r59, r10, r57, r517, mp4Ops, r512, r510, r55, r58, r515, r152, r56, r02.A0N, r02.A0O, r514, r522, r02.A0S, r142, r15, r32, r518, r02.A0b, r64, r523, r520, r516, r02.A0k, r519, r93, r513, r521, 1, 1, -1, true);
                                                            r524.A5g(new C50682Qn(r524, r02, str2));
                                                            synchronized (hashMap2) {
                                                                if (!hashMap2.containsKey(str2)) {
                                                                    StringBuilder sb7 = new StringBuilder();
                                                                    sb7.append("mediadownloadmanager/queueexpresspathdownload enqueue media job: ");
                                                                    sb7.append(r524);
                                                                    sb7.append(" enc hash: ");
                                                                    sb7.append(str2);
                                                                    Log.i(sb7.toString());
                                                                    hashMap2.put(str2, r524);
                                                                    r93.Ab2(r524);
                                                                } else {
                                                                    StringBuilder sb8 = new StringBuilder();
                                                                    sb8.append("mediadownloadmanager/queueexpresspathdownload media job: ");
                                                                    sb8.append(r524);
                                                                    sb8.append(" already exists enc hash: ");
                                                                    sb8.append(str2);
                                                                    Log.i(sb8.toString());
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            r5 = r02.A07;
                            i = 23;
                        }
                        Integer valueOf = Integer.valueOf(i);
                        AnonymousClass31V r16 = new AnonymousClass31V();
                        r16.A0B = 4;
                        r16.A0E = valueOf;
                        AnonymousClass009.A05(4);
                        AnonymousClass009.A05(valueOf);
                        r5.A0B.A07(r16);
                    }
                    this.A07.A09(r85, "medianotify", null);
                } else {
                    this.A00.A0p(r1);
                }
            }
        }
    }
}
