package X;

import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchFragment;

/* renamed from: X.2vB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59662vB extends AbstractC75133jM {
    public final /* synthetic */ BusinessDirectorySearchFragment A00;

    public C59662vB(BusinessDirectorySearchFragment businessDirectorySearchFragment) {
        this.A00 = businessDirectorySearchFragment;
    }

    @Override // X.AbstractC75133jM
    public void A02() {
        this.A00.A09.A0A();
    }

    @Override // X.AbstractC75133jM
    public boolean A03() {
        C53862fQ r3 = this.A00.A09;
        int i = r3.A01;
        boolean z = true;
        if (!(i == 1 || i == 4)) {
            z = false;
        }
        if (z && r3.A02 != null) {
            if (!r3.A0Q()) {
                return false;
            }
            C63363Bh r0 = (C63363Bh) r3.A0H.A04.A01();
            if (r0 != null && !r0.A07) {
                return false;
            }
        }
        return true;
    }
}
