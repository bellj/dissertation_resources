package X;

import android.app.AppOpsManager;
import android.content.Context;
import android.os.Binder;
import android.os.Build;

/* renamed from: X.0T0  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0T0 {
    public static int A00(Context context, String str, String str2) {
        if (Build.VERSION.SDK_INT >= 23) {
            return C06300Sy.A00((AppOpsManager) C06300Sy.A01(context, AppOpsManager.class), str, str2);
        }
        return 1;
    }

    public static int A01(Context context, String str, String str2, int i) {
        if (Build.VERSION.SDK_INT < 29) {
            return A00(context, str, str2);
        }
        AppOpsManager A01 = C06310Sz.A01(context);
        int A00 = C06310Sz.A00(A01, str, str2, Binder.getCallingUid());
        if (A00 == 0) {
            return C06310Sz.A00(A01, str, C06310Sz.A02(context), i);
        }
        return A00;
    }

    public static String A02(String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return C06300Sy.A02(str);
        }
        return null;
    }
}
