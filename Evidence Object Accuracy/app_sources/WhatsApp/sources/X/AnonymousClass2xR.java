package X;

import android.view.View;

/* renamed from: X.2xR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xR extends AbstractC104124rr {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AnonymousClass0QQ A02;
    public final /* synthetic */ AnonymousClass03U A03;
    public final /* synthetic */ C55262i6 A04;

    public AnonymousClass2xR(AnonymousClass0QQ r1, AnonymousClass03U r2, C55262i6 r3, int i, int i2) {
        this.A04 = r3;
        this.A03 = r2;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = r1;
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMB(View view) {
        if (this.A00 != 0) {
            view.setTranslationX(0.0f);
        }
        if (this.A01 != 0) {
            view.setTranslationY(0.0f);
        }
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMC(View view) {
        this.A02.A09(null);
        C55262i6 r2 = this.A04;
        AnonymousClass03U r1 = this.A03;
        r2.A03(r1);
        C12980iv.A1K(r2, r1, r2.A04);
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMD(View view) {
    }
}
