package X;

import android.graphics.Paint;

/* renamed from: X.5Jp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113945Jp extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ AnonymousClass4WD this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113945Jp(AnonymousClass4WD r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        Paint paint = new Paint(1);
        AnonymousClass4WD r2 = this.this$0;
        paint.setStyle(Paint.Style.STROKE);
        paint.setDither(true);
        paint.setStrokeWidth(r2.A05.A01);
        paint.setColor(AnonymousClass00T.A00(r2.A0A, r2.A06.statusColor));
        return paint;
    }
}
