package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124595pn extends Enum implements AnonymousClass5UX {
    public static final /* synthetic */ EnumC124595pn[] A00;
    public static final EnumC124595pn A01;
    public final String fieldName = "payment_device_id";

    public static EnumC124595pn valueOf(String str) {
        return (EnumC124595pn) Enum.valueOf(EnumC124595pn.class, str);
    }

    public static EnumC124595pn[] values() {
        return (EnumC124595pn[]) A00.clone();
    }

    static {
        EnumC124595pn r1 = new EnumC124595pn();
        A01 = r1;
        A00 = new EnumC124595pn[]{r1};
    }

    @Override // X.AnonymousClass5UX
    public String ACu() {
        return this.fieldName;
    }
}
