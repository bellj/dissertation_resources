package X;

import java.util.Iterator;

/* renamed from: X.28f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC469228f implements Iterator {
    public final Iterator backingIterator;

    public abstract Object transform(Object obj);

    public AbstractC469228f(Iterator it) {
        this.backingIterator = it;
    }

    @Override // java.util.Iterator
    public final boolean hasNext() {
        return this.backingIterator.hasNext();
    }

    @Override // java.util.Iterator
    public final Object next() {
        return transform(this.backingIterator.next());
    }

    @Override // java.util.Iterator
    public final void remove() {
        this.backingIterator.remove();
    }
}
