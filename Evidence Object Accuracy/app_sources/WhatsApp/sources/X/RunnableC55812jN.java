package X;

import android.graphics.Bitmap;
import android.util.SparseArray;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2jN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55812jN extends EmptyBaseRunnable0 implements Runnable {
    public final int A00;
    public final int A01;
    public final AbstractC117045Ya A02;
    public final AnonymousClass5XK A03;
    public final /* synthetic */ AnonymousClass4SI A04;

    public RunnableC55812jN(AbstractC117045Ya r1, AnonymousClass5XK r2, AnonymousClass4SI r3, int i, int i2) {
        this.A04 = r3;
        this.A02 = r1;
        this.A03 = r2;
        this.A00 = i;
        this.A01 = i2;
    }

    public final boolean A00(int i, int i2) {
        boolean z;
        int i3 = 2;
        C08870bz r4 = null;
        try {
            if (i2 != 1) {
                if (i2 == 2) {
                    try {
                        AnonymousClass4SI r5 = this.A04;
                        AnonymousClass4UM r3 = r5.A03;
                        AbstractC117045Ya r0 = this.A02;
                        r4 = r3.A00(r5.A00, r0.ADY(), r0.ADX());
                        i3 = -1;
                    } catch (RuntimeException e) {
                        AbstractC12710iN r1 = AnonymousClass0UN.A00;
                        if (r1.AJi(5)) {
                            r1.Ag1(AnonymousClass4SI.class.getSimpleName(), "Failed to create frame bitmap", e);
                        }
                    }
                }
                return false;
            }
            AnonymousClass5XK r2 = this.A03;
            AbstractC117045Ya r02 = this.A02;
            r4 = r2.AAr(i, r02.ADY(), r02.ADX());
            if (C08870bz.A01(r4)) {
                AnonymousClass4SI r7 = this.A04;
                if (r7.A02.A00((Bitmap) r4.A04(), i)) {
                    int i4 = this.A00;
                    AnonymousClass0UN.A02(AnonymousClass4SI.class, Integer.valueOf(i4), "Frame %d ready.");
                    synchronized (r7.A01) {
                        this.A03.AQo(r4, i4, i2);
                    }
                    z = true;
                    if (z) {
                        return z;
                    }
                    return i3 != -1 ? A00(i, i3) : z;
                }
            }
            z = false;
        } finally {
            if (r4 != null) {
                r4.close();
            }
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            AnonymousClass5XK r0 = this.A03;
            int i = this.A00;
            if (r0.A7c(i)) {
                AnonymousClass0UN.A02(AnonymousClass4SI.class, Integer.valueOf(i), "Frame %d is cached already.");
                SparseArray sparseArray = this.A04.A01;
                synchronized (sparseArray) {
                    try {
                        sparseArray.remove(this.A01);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return;
            }
            if (A00(i, 1)) {
                AnonymousClass0UN.A02(AnonymousClass4SI.class, Integer.valueOf(i), "Prepared frame frame %d.");
            } else {
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, i, 0);
                AbstractC12710iN r2 = AnonymousClass0UN.A00;
                if (r2.AJi(6)) {
                    r2.A9G(AnonymousClass4SI.class.getSimpleName(), String.format(null, "Could not prepare frame %d.", objArr));
                }
            }
            SparseArray sparseArray2 = this.A04.A01;
            synchronized (sparseArray2) {
                try {
                    sparseArray2.remove(this.A01);
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        } catch (Throwable th3) {
            SparseArray sparseArray3 = this.A04.A01;
            synchronized (sparseArray3) {
                try {
                    sparseArray3.remove(this.A01);
                    throw th3;
                } catch (Throwable th4) {
                    throw th4;
                }
            }
        }
    }
}
