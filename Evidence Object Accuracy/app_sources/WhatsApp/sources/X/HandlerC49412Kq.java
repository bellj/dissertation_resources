package X;

import android.os.Handler;
import android.os.Message;
import com.whatsapp.util.Log;

/* renamed from: X.2Kq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC49412Kq extends Handler {
    public final /* synthetic */ HandlerThreadC26611Ed A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC49412Kq(HandlerThreadC26611Ed r2) {
        super(r2.getLooper());
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            HandlerThreadC26611Ed r4 = this.A00;
            if (C21280xA.A00()) {
                Log.i("xmpp/connection/logout/timeout/skip-voip-active");
                r4.A04.sendEmptyMessageDelayed(0, 10000);
                AbstractC43471x5 r3 = r4.A05;
                Message obtain = Message.obtain(null, 0, 22, 0);
                obtain.what = 2;
                ((Handler) r3).sendMessage(obtain);
                return;
            }
            Log.i("xmpp/connection/logout/timeout/close-socket");
            r4.A04();
        } else if (i == 1) {
            AnonymousClass1IS r5 = (AnonymousClass1IS) message.obj;
            HandlerThreadC26611Ed r32 = this.A00;
            AbstractC15340mz A03 = r32.A0S.A0K.A03(r5);
            if (A03 != null) {
                int i2 = A03.A0C;
                if (C37381mH.A00(i2, 4) < 0 && i2 != 20) {
                    StringBuilder sb = new StringBuilder("message receipt timeout fired; messageKey=");
                    sb.append(r5);
                    sb.append("; fMessage.status=");
                    sb.append(A03.A0C);
                    Log.w(sb.toString());
                    removeMessages(1);
                    r32.A06(true);
                }
            }
        } else if (i == 2) {
            Log.w("connection active timeout fired");
            removeMessages(2);
            this.A00.A06(true);
        }
    }
}
