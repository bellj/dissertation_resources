package X;

import android.app.Application;
import android.os.Build;
import com.whatsapp.util.Log;
import java.lang.reflect.Field;

/* renamed from: X.17z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C250717z {
    public boolean A00 = false;
    public final Application A01;
    public final AbstractC15710nm A02;

    public C250717z(Application application, AbstractC15710nm r3) {
        this.A02 = r3;
        this.A01 = application;
    }

    public void A00() {
        if (!this.A00 && "samsung".equalsIgnoreCase(Build.MANUFACTURER) && Build.VERSION.SDK_INT == 24) {
            try {
                Class<?> cls = Class.forName("com.samsung.android.emergencymode.SemEmergencyManager");
                Field declaredField = cls.getDeclaredField("sInstance");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(null);
                Field declaredField2 = cls.getDeclaredField("mContext");
                declaredField2.setAccessible(true);
                declaredField2.set(obj, this.A01);
            } catch (Exception e) {
                this.A00 = true;
                Log.e("SemEmergencyManagerLeakFix/apply", e);
                this.A02.A02("leak-fix-v2", "SemEmergencyManagerLeakFix", e);
            }
        }
    }
}
