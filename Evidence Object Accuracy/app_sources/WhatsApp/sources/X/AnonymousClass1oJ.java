package X;

import android.os.Message;
import com.whatsapp.util.Log;
import java.io.InputStream;

/* renamed from: X.1oJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1oJ extends InputStream {
    public final C18790t3 A00;
    public final InputStream A01;
    public final Integer A02;
    public final Integer A03;

    public AnonymousClass1oJ(C18790t3 r1, InputStream inputStream, Integer num, Integer num2) {
        this.A01 = inputStream;
        this.A02 = num;
        this.A00 = r1;
        this.A03 = num2;
    }

    public final void A00(int i) {
        AnonymousClass1N0 r1;
        Integer num = this.A02;
        if (num != null) {
            C18790t3 r3 = this.A00;
            int intValue = num.intValue();
            AnonymousClass1N1 r12 = r3.A00;
            boolean z = false;
            if (r12 != null) {
                z = true;
            }
            AnonymousClass009.A0F(z);
            Message.obtain(r12, 3, intValue, i).sendToTarget();
            r3.A02();
        }
        C18790t3 r13 = this.A00;
        int intValue2 = this.A03.intValue();
        AnonymousClass16B r5 = r13.A06;
        if (((long) i) >= 0 && (r1 = r5.A00) != null) {
            AnonymousClass009.A0F(true);
            Message.obtain(r1, 3, intValue2, i).sendToTarget();
            r5.A00();
        }
    }

    @Override // java.io.InputStream
    public int available() {
        return this.A01.available();
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A01.close();
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        Log.w("mark called in MessageInputStream");
    }

    @Override // java.io.InputStream
    public int read() {
        int read = this.A01.read();
        if (read != -1) {
            A00(1);
        }
        return read;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        int read = this.A01.read(bArr);
        if (read > 0) {
            A00(read);
        }
        return read;
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        int read = this.A01.read(bArr, i, i2);
        if (read > 0) {
            A00(read);
        }
        return read;
    }

    @Override // java.io.InputStream
    public synchronized void reset() {
        Log.w("reset called in MessageInputStream");
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        long skip = this.A01.skip(j);
        Integer num = this.A02;
        if (num != null) {
            this.A00.A03(skip, num.intValue());
        }
        C18790t3 r4 = this.A00;
        r4.A06.A02(skip, this.A03.intValue());
        return skip;
    }
}
