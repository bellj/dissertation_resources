package X;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import com.whatsapp.R;

/* renamed from: X.2xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60402xb extends AnonymousClass1PZ {
    public final AnonymousClass2JT A00;

    public C60402xb(ActivityC000800j r30, C253519b r31, C14900mE r32, AnonymousClass2TT r33, AnonymousClass10A r34, C22330yu r35, AnonymousClass130 r36, AnonymousClass10S r37, C15610nY r38, AnonymousClass1J1 r39, AnonymousClass131 r40, AnonymousClass018 r41, C19990v2 r42, C20830wO r43, C15600nX r44, C15370n3 r45, AnonymousClass19M r46, C14850m9 r47, C20710wC r48, C244215l r49, C29901Ve r50, AnonymousClass12F r51, AbstractC14440lR r52) {
        super(r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r41, null, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52);
        this.A00 = new AnonymousClass2JT(r38, new AnonymousClass2JU() { // from class: X.557
            @Override // X.AnonymousClass2JU
            public final void AR6() {
                C60402xb.this.A03();
            }
        }, r50, r52);
    }

    @Override // X.AnonymousClass1PZ
    public void A02() {
        super.A02();
        this.A00.A00();
    }

    @Override // X.AnonymousClass1PZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        super.onActivityCreated(activity, bundle);
        this.A08.setText(R.string.tap_for_list_info);
        C12960it.A13(this.A03, this, activity, 35);
        AnonymousClass23N.A02(this.A03, R.string.accessibility_action_click_conversation_title_for_broadcast_list_info);
    }

    @Override // X.AnonymousClass1PZ, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        super.onActivityDestroyed(activity);
        AnonymousClass2JT r3 = this.A00;
        AnonymousClass2JS r2 = r3.A00;
        if (r2 != null) {
            r2.A03(false);
            Handler handler = r2.A00;
            if (handler != null) {
                handler.removeCallbacks(r2.A01);
            }
            r2.A00 = null;
            r2.A01 = null;
            r3.A00 = null;
        }
    }
}
