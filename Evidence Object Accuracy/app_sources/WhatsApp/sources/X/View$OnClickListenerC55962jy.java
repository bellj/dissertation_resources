package X;

import android.app.Activity;
import android.view.View;
import android.widget.ImageButton;
import com.facebook.redex.EmptyBaseViewOnClick0CListener;
import java.lang.ref.WeakReference;

/* renamed from: X.2jy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class View$OnClickListenerC55962jy extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public final View A00;
    public final C15570nT A01;
    public final C15550nR A02;
    public final C15610nY A03;
    public final AnonymousClass1J1 A04;
    public final AnonymousClass19D A05;
    public final AnonymousClass11P A06;
    public final C14820m6 A07;
    public final AnonymousClass018 A08;
    public final WeakReference A09;

    public View$OnClickListenerC55962jy(Activity activity, View view, C15570nT r4, C15550nR r5, C15610nY r6, AnonymousClass1J1 r7, AnonymousClass19D r8, AnonymousClass11P r9, C14820m6 r10, AnonymousClass018 r11) {
        this.A00 = view;
        this.A05 = r8;
        this.A06 = r9;
        this.A09 = C12970iu.A10(activity);
        this.A08 = r11;
        this.A04 = r7;
        this.A01 = r4;
        this.A02 = r5;
        this.A03 = r6;
        this.A07 = r10;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r2 == 10) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.C30421Xi r11) {
        /*
            r10 = this;
            java.lang.ref.WeakReference r0 = r10.A09
            java.lang.Object r3 = r0.get()
            android.app.Activity r3 = (android.app.Activity) r3
            if (r11 == 0) goto L_0x0050
            if (r3 == 0) goto L_0x0050
            X.19D r2 = r10.A05
            r4 = 0
            r1 = 0
            r0 = 1
            X.1hP r3 = r2.A01(r3, r1, r0)
            r3.A0O = r11
            r3.A0A = r0
            int r2 = r11.A0C
            r0 = 9
            if (r2 == r0) goto L_0x0024
            r1 = 10
            r0 = 0
            if (r2 != r1) goto L_0x0025
        L_0x0024:
            r0 = 1
        L_0x0025:
            r3.A0T = r0
            r3.A0J = r4
            r0 = 0
            r2 = 1
            r3.A0A(r0, r2, r2)
            X.11P r8 = r10.A06
            X.018 r9 = r10.A08
            android.view.View r3 = r10.A00
            X.1J1 r7 = r10.A04
            X.0nT r4 = r10.A01
            X.0nR r5 = r10.A02
            X.0nY r6 = r10.A03
            X.C40691s6.A01(r3, r4, r5, r6, r7, r8, r9)
            X.0m6 r1 = r10.A07
            X.1IS r0 = r11.A0z
            X.0lm r0 = r0.A00
            X.C40691s6.A0B(r1, r0)
            X.1hP r0 = r8.A00()
            if (r0 == 0) goto L_0x0050
            r0.A0X = r2
        L_0x0050:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.View$OnClickListenerC55962jy.A00(X.1Xi):void");
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        AnonymousClass11P r3 = this.A06;
        boolean A0C = r3.A0C();
        boolean A0B = r3.A0B();
        if (!A0C) {
            if (A0B) {
                r3.A04();
            }
            C30421Xi A01 = r3.A01();
            synchronized (r3) {
                r3.A02 = null;
            }
            A00(A01);
        } else if (A0B) {
            C35191hP A00 = r3.A00();
            if (A00 != null) {
                A00.A06++;
            }
            r3.A04();
            if (view instanceof ImageButton) {
                C40691s6.A06((ImageButton) view);
            }
        } else {
            C35191hP A002 = r3.A00();
            C30421Xi A012 = r3.A01();
            if (A002 != null) {
                A002.A0A(0, true, true);
                if (view instanceof ImageButton) {
                    C40691s6.A05((ImageButton) view);
                }
            } else if (A012 != null) {
                A00(A012);
            }
        }
    }
}
