package X;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.ActionBarContextView;
import java.lang.ref.WeakReference;

/* renamed from: X.0CF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CF extends AbstractC009504t implements AbstractC011605p {
    public AnonymousClass02Q A00;
    public WeakReference A01;
    public final Context A02;
    public final AnonymousClass07H A03;
    public final /* synthetic */ AnonymousClass0C4 A04;

    public AnonymousClass0CF(Context context, AnonymousClass0C4 r4, AnonymousClass02Q r5) {
        this.A04 = r4;
        this.A02 = context;
        this.A00 = r5;
        AnonymousClass07H r1 = new AnonymousClass07H(context);
        r1.A00 = 1;
        this.A03 = r1;
        r1.A0C(this);
    }

    @Override // X.AbstractC009504t
    public Menu A00() {
        return this.A03;
    }

    @Override // X.AbstractC009504t
    public MenuInflater A01() {
        return new AnonymousClass0Ax(this.A02);
    }

    @Override // X.AbstractC009504t
    public View A02() {
        WeakReference weakReference = this.A01;
        if (weakReference != null) {
            return (View) weakReference.get();
        }
        return null;
    }

    @Override // X.AbstractC009504t
    public CharSequence A03() {
        return this.A04.A0A.A0D;
    }

    @Override // X.AbstractC009504t
    public CharSequence A04() {
        return this.A04.A0A.A0E;
    }

    @Override // X.AbstractC009504t
    public void A05() {
        AnonymousClass0C4 r3 = this.A04;
        if (r3.A05 == this) {
            boolean z = r3.A0H;
            boolean z2 = r3.A0I;
            if (z || z2) {
                r3.A07 = this;
                r3.A06 = this.A00;
            } else {
                this.A00.AP3(this);
            }
            this.A00 = null;
            r3.A0Z(false);
            ActionBarContextView actionBarContextView = r3.A0A;
            if (actionBarContextView.A04 == null) {
                actionBarContextView.A03();
            }
            ((C017408d) r3.A0C).A09.sendAccessibilityEvent(32);
            r3.A0B.setHideOnContentScrollEnabled(r3.A0J);
            r3.A05 = null;
        }
    }

    @Override // X.AbstractC009504t
    public void A06() {
        if (this.A04.A05 == this) {
            AnonymousClass07H r1 = this.A03;
            r1.A07();
            try {
                this.A00.AU7(r1, this);
            } finally {
                r1.A06();
            }
        }
    }

    @Override // X.AbstractC009504t
    public void A07(int i) {
        A0A(this.A04.A02.getResources().getString(i));
    }

    @Override // X.AbstractC009504t
    public void A08(int i) {
        A0B(this.A04.A02.getResources().getString(i));
    }

    @Override // X.AbstractC009504t
    public void A09(View view) {
        this.A04.A0A.setCustomView(view);
        this.A01 = new WeakReference(view);
    }

    @Override // X.AbstractC009504t
    public void A0A(CharSequence charSequence) {
        this.A04.A0A.setSubtitle(charSequence);
    }

    @Override // X.AbstractC009504t
    public void A0B(CharSequence charSequence) {
        this.A04.A0A.setTitle(charSequence);
    }

    @Override // X.AbstractC009504t
    public void A0C(boolean z) {
        super.A01 = z;
        this.A04.A0A.setTitleOptional(z);
    }

    @Override // X.AbstractC009504t
    public boolean A0D() {
        return this.A04.A0A.A0H;
    }

    @Override // X.AbstractC011605p
    public boolean ASh(MenuItem menuItem, AnonymousClass07H r3) {
        AnonymousClass02Q r0 = this.A00;
        if (r0 != null) {
            return r0.ALr(menuItem, this);
        }
        return false;
    }

    @Override // X.AbstractC011605p
    public void ASi(AnonymousClass07H r2) {
        if (this.A00 != null) {
            A06();
            AnonymousClass0XQ r0 = this.A04.A0A.A0A;
            if (r0 != null) {
                r0.A03();
            }
        }
    }
}
