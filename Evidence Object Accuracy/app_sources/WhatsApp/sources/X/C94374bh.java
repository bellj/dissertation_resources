package X;

import java.util.Arrays;

/* renamed from: X.4bh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94374bh {
    public static final C94374bh A04 = new C94374bh(new long[0]);
    public final int A00;
    public final long A01 = -9223372036854775807L;
    public final long[] A02;
    public final AnonymousClass4XE[] A03;

    public C94374bh(long[] jArr) {
        this.A02 = jArr;
        int length = jArr.length;
        this.A00 = length;
        AnonymousClass4XE[] r2 = new AnonymousClass4XE[length];
        for (int i = 0; i < length; i++) {
            r2[i] = new AnonymousClass4XE();
        }
        this.A03 = r2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C94374bh.class != obj.getClass()) {
                return false;
            }
            C94374bh r7 = (C94374bh) obj;
            if (!AnonymousClass3JZ.A0H(null, null) || this.A00 != r7.A00 || this.A01 != r7.A01 || !Arrays.equals(this.A02, r7.A02) || !Arrays.equals(this.A03, r7.A03)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (((((((((this.A00 * 31) + 0) * 31) + ((int) 0)) * 31) + ((int) this.A01)) * 31) + Arrays.hashCode(this.A02)) * 31) + Arrays.hashCode(this.A03);
    }

    public String toString() {
        char c;
        StringBuilder A0k = C12960it.A0k("AdPlaybackState(adsId=");
        A0k.append((Object) null);
        A0k.append(", adResumePositionUs=");
        A0k.append(0L);
        A0k.append(", adGroups=[");
        int i = 0;
        while (true) {
            AnonymousClass4XE[] r6 = this.A03;
            int length = r6.length;
            if (i >= length) {
                return C12960it.A0d("])", A0k);
            }
            A0k.append("adGroup(timeUs=");
            A0k.append(this.A02[i]);
            A0k.append(", ads=[");
            for (int i2 = 0; i2 < r6[i].A01.length; i2++) {
                A0k.append("ad(state=");
                int i3 = r6[i].A01[i2];
                if (i3 != 0) {
                    c = 'R';
                    if (i3 != 1) {
                        c = 'S';
                        if (i3 != 2) {
                            c = 'P';
                            if (i3 != 3) {
                                c = '!';
                                if (i3 != 4) {
                                    c = '?';
                                }
                            }
                        }
                    }
                } else {
                    c = '_';
                }
                A0k.append(c);
                A0k.append(", durationUs=");
                AnonymousClass4XE r2 = r6[i];
                A0k.append(r2.A02[i2]);
                A0k.append(')');
                if (i2 < r2.A01.length - 1) {
                    A0k.append(", ");
                }
            }
            A0k.append("])");
            if (i < length - 1) {
                A0k.append(", ");
            }
            i++;
        }
    }
}
