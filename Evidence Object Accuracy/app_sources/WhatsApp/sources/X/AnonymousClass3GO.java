package X;

import android.text.TextUtils;

/* renamed from: X.3GO  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3GO {
    public static void A00(AbstractC15340mz r16, C39971qq r17) {
        C16470p4 ABf;
        AnonymousClass4B6 r4;
        AnonymousClass1ZC r7;
        AnonymousClass1G3 r2 = r17.A03;
        C57742nY r0 = ((C27081Fy) r2.A00).A0N;
        if (r0 == null) {
            r0 = C57742nY.A09;
        }
        AnonymousClass1G4 A0T = r0.A0T();
        if ((r16 instanceof AbstractC16390ow) && (ABf = ((AbstractC16390ow) r16).ABf()) != null) {
            int i = ABf.A00;
            if (i == 1) {
                r4 = AnonymousClass4B6.A02;
            } else if (i != 2) {
                r4 = AnonymousClass4B6.A03;
            } else {
                r4 = AnonymousClass4B6.A01;
            }
            C57742nY r1 = (C57742nY) AnonymousClass1G4.A00(A0T);
            r1.A00 |= 8;
            r1.A01 = r4.value;
            AnonymousClass1Z6 r02 = ABf.A02;
            if (r02 != null) {
                String str = r02.A01;
                if (!TextUtils.isEmpty(str)) {
                    C57742nY r12 = (C57742nY) AnonymousClass1G4.A00(A0T);
                    r12.A00 |= 1;
                    r12.A08 = str;
                }
            }
            String str2 = ABf.A08;
            if (!TextUtils.isEmpty(str2)) {
                C57742nY r13 = (C57742nY) AnonymousClass1G4.A00(A0T);
                r13.A00 |= 32;
                r13.A07 = str2;
            }
            String str3 = ABf.A07;
            C57742nY r14 = (C57742nY) AnonymousClass1G4.A00(A0T);
            r14.A00 |= 2;
            r14.A06 = str3;
            String str4 = ABf.A06;
            C57742nY r15 = (C57742nY) AnonymousClass1G4.A00(A0T);
            r15.A00 |= 4;
            r15.A05 = str4;
            int i2 = ABf.A00;
            if (i2 == 1) {
                for (AnonymousClass1ZA r5 : ABf.A09) {
                    AnonymousClass1G4 A0T2 = C57202me.A03.A0T();
                    String str5 = r5.A00;
                    if (!TextUtils.isEmpty(str5)) {
                        C57202me r18 = (C57202me) AnonymousClass1G4.A00(A0T2);
                        r18.A00 |= 1;
                        r18.A02 = str5;
                    }
                    for (AnonymousClass1ZB r6 : r5.A01) {
                        AnonymousClass1G4 A0T3 = C57372mv.A04.A0T();
                        String str6 = r6.A02;
                        C57372mv r19 = (C57372mv) AnonymousClass1G4.A00(A0T3);
                        r19.A00 |= 1;
                        r19.A03 = str6;
                        String str7 = r6.A01;
                        C57372mv r110 = (C57372mv) AnonymousClass1G4.A00(A0T3);
                        r110.A00 |= 4;
                        r110.A02 = str7;
                        String str8 = r6.A00;
                        if (!TextUtils.isEmpty(str8)) {
                            C57372mv r111 = (C57372mv) AnonymousClass1G4.A00(A0T3);
                            r111.A00 |= 2;
                            r111.A01 = str8;
                        }
                        AbstractC27091Fz A02 = A0T3.A02();
                        C57202me r42 = (C57202me) AnonymousClass1G4.A00(A0T2);
                        AnonymousClass1K6 r112 = r42.A01;
                        if (!((AnonymousClass1K7) r112).A00) {
                            r112 = AbstractC27091Fz.A0G(r112);
                            r42.A01 = r112;
                        }
                        r112.add(A02);
                    }
                    AbstractC27091Fz A022 = A0T2.A02();
                    C57742nY r43 = (C57742nY) AnonymousClass1G4.A00(A0T);
                    AnonymousClass1K6 r113 = r43.A02;
                    if (!((AnonymousClass1K7) r113).A00) {
                        r113 = AbstractC27091Fz.A0G(r113);
                        r43.A02 = r113;
                    }
                    r113.add(A022);
                }
            } else if (i2 == 2 && (r7 = ABf.A04) != null) {
                AnonymousClass1G4 A0T4 = C57362mu.A04.A0T();
                for (AnonymousClass1ZF r52 : r7.A02) {
                    AnonymousClass1G4 A0T5 = C57192md.A03.A0T();
                    String str9 = r52.A00;
                    if (!TextUtils.isEmpty(str9)) {
                        C57192md r114 = (C57192md) AnonymousClass1G4.A00(A0T5);
                        r114.A00 |= 1;
                        r114.A02 = str9;
                    }
                    for (AnonymousClass1ZG r115 : r52.A01) {
                        AnonymousClass1G4 A0T6 = C57022mL.A02.A0T();
                        String str10 = r115.A00;
                        if (!TextUtils.isEmpty(str10)) {
                            C57022mL r116 = (C57022mL) AnonymousClass1G4.A00(A0T6);
                            r116.A00 |= 1;
                            r116.A01 = str10;
                            AbstractC27091Fz A023 = A0T6.A02();
                            C57192md r44 = (C57192md) AnonymousClass1G4.A00(A0T5);
                            AnonymousClass1K6 r117 = r44.A01;
                            if (!((AnonymousClass1K7) r117).A00) {
                                r117 = AbstractC27091Fz.A0G(r117);
                                r44.A01 = r117;
                            }
                            r117.add(A023);
                        }
                    }
                    AbstractC27091Fz A024 = A0T5.A02();
                    C57362mu r45 = (C57362mu) AnonymousClass1G4.A00(A0T4);
                    AnonymousClass1K6 r118 = r45.A01;
                    if (!((AnonymousClass1K7) r118).A00) {
                        r118 = AbstractC27091Fz.A0G(r118);
                        r45.A01 = r118;
                    }
                    r118.add(A024);
                    String rawString = r7.A00.getRawString();
                    C57362mu r119 = (C57362mu) AnonymousClass1G4.A00(A0T4);
                    r119.A00 |= 2;
                    r119.A03 = rawString;
                }
                AnonymousClass1G4 A0T7 = C57182mc.A03.A0T();
                AnonymousClass1ZH r53 = r7.A01;
                byte[] bArr = r53.A02;
                if (bArr != null) {
                    AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
                    C57182mc r120 = (C57182mc) AnonymousClass1G4.A00(A0T7);
                    r120.A00 |= 2;
                    r120.A01 = A01;
                }
                String str11 = r53.A01;
                C57182mc r121 = (C57182mc) AnonymousClass1G4.A00(A0T7);
                r121.A00 |= 1;
                r121.A02 = str11;
                C57362mu r122 = (C57362mu) AnonymousClass1G4.A00(A0T4);
                r122.A02 = (C57182mc) A0T7.A02();
                r122.A00 |= 1;
                C57742nY r123 = (C57742nY) AnonymousClass1G4.A00(A0T);
                r123.A04 = (C57362mu) A0T4.A02();
                r123.A00 |= 16;
            }
            AnonymousClass1PG r142 = r17.A04;
            byte[] bArr2 = r17.A09;
            if (C32411c7.A0U(r142, r16, bArr2)) {
                C43261wh A0P = C32411c7.A0P(r17.A00, r17.A02, r142, r16, bArr2, r17.A06);
                C57742nY r124 = (C57742nY) AnonymousClass1G4.A00(A0T);
                r124.A03 = A0P;
                r124.A00 |= 64;
            }
            C27081Fy r22 = (C27081Fy) AnonymousClass1G4.A00(r2);
            r22.A0N = (C57742nY) A0T.A02();
            r22.A00 |= 134217728;
        }
    }

    public static byte[] A01(C16380ov r3) {
        byte[] bArr;
        C16470p4 r32 = r3.A00;
        if (r32 == null) {
            return null;
        }
        AnonymousClass1ZC r2 = r32.A04;
        if (r2 != null && r32.A00 == 2) {
            return r2.A01.A02;
        }
        AnonymousClass1ZD r22 = r32.A01;
        if (r22 != null && r32.A00 == 3) {
            return r22.A0C;
        }
        AnonymousClass1Z6 r0 = r32.A02;
        if (r0 == null || (bArr = r0.A02) == null) {
            return null;
        }
        return bArr;
    }
}
