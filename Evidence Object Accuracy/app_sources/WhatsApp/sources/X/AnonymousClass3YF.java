package X;

import com.whatsapp.mediacomposer.doodle.ColorPickerView;

/* renamed from: X.3YF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YF implements AnonymousClass5W4 {
    public final /* synthetic */ DialogC51702Ye A00;

    public AnonymousClass3YF(DialogC51702Ye r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5W4
    public void AOJ(float f, int i) {
        DialogC51702Ye r2 = this.A00;
        r2.A0D.A00 = i;
        r2.A0E.A00((int) f, i);
        r2.A07.A00(f, i);
        r2.A07.A04 = r2.A06.A05.A0B;
    }

    @Override // X.AnonymousClass5W4
    public void AY2() {
        DialogC51702Ye r3 = this.A00;
        C48792Hu r1 = r3.A0D;
        ColorPickerView colorPickerView = r3.A06.A05;
        int i = colorPickerView.A02;
        r1.A00 = i;
        r3.A0E.A00((int) colorPickerView.A00, i);
        AnonymousClass2ZW r2 = r3.A07;
        ColorPickerView colorPickerView2 = r3.A06.A05;
        r2.A00(colorPickerView2.A00, colorPickerView2.A02);
        r3.A07.A04 = r3.A06.A05.A0B;
    }
}
