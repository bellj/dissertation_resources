package X;

import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.1oa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38711oa extends AbstractC16350or {
    public AbstractC33511eG A00;
    public final int A01;
    public final C235512c A02;

    public C38711oa(C235512c r2) {
        this.A02 = r2;
        this.A01 = 0;
    }

    public C38711oa(C235512c r1, int i) {
        this.A02 = r1;
        this.A01 = i;
    }

    public C38711oa(C235512c r1, AbstractC33511eG r2, int i) {
        this.A02 = r1;
        this.A01 = i;
        this.A00 = r2;
    }

    public Void A08(Void... voidArr) {
        AsyncTaskC16360os r8 = super.A02;
        if (!r8.isCancelled()) {
            C235512c r3 = this.A02;
            int i = this.A01;
            List A07 = r3.A07.A07(i);
            if (A07 == null) {
                C15830ny r0 = r3.A0X;
                AnonymousClass009.A00();
                if (i == 0) {
                    A07 = r0.A0C.A00("SELECT * FROM installed_sticker_packs LEFT JOIN downloadable_sticker_packs ON (installed_id = id)", null);
                } else {
                    AnonymousClass15B r4 = r0.A0C;
                    int i2 = 1;
                    if (i == 1) {
                        i2 = 0;
                    }
                    A07 = r4.A00("SELECT * FROM installed_sticker_packs LEFT JOIN downloadable_sticker_packs ON (installed_id = id) WHERE installed_is_avatar_pack = ?", new String[]{String.valueOf(i2)});
                }
                r3.A0M(A07, i);
            }
            if (!r8.isCancelled()) {
                List<AnonymousClass1KZ> A0A = r3.A0A();
                if (!r8.isCancelled()) {
                    r8.A01(new C71733dQ(A07, A0A));
                    if (!r8.isCancelled()) {
                        List<AnonymousClass1KZ> A0E = r3.A0E(new C38691oY(this));
                        HashSet hashSet = new HashSet();
                        for (AnonymousClass1KZ r02 : A0E) {
                            hashSet.add(r02.A0D);
                        }
                        for (AnonymousClass1KZ r32 : A0A) {
                            String str = r32.A0D;
                            if (!hashSet.contains(str)) {
                                StringBuilder sb = new StringBuilder("LoadStickerPickerPacksAsyncTask/doInBackground failed to load pack ");
                                sb.append(str);
                                Log.e(sb.toString());
                                r8.A01(r32);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
