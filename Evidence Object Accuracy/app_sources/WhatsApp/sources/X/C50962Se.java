package X;

import com.whatsapp.gallery.GalleryTabHostFragment;

/* renamed from: X.2Se  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50962Se extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ GalleryTabHostFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C50962Se(GalleryTabHostFragment galleryTabHostFragment) {
        super(0);
        this.this$0 = galleryTabHostFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        GalleryTabHostFragment galleryTabHostFragment = this.this$0;
        C18720su r4 = galleryTabHostFragment.A06;
        if (r4 != null) {
            return new C457522x(galleryTabHostFragment.A1B().A00.getContentResolver(), this.this$0.A0B, r4, "tabbed-gallery-ui");
        }
        C16700pc.A0K("caches");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }
}
