package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

/* renamed from: X.06m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C013906m extends Drawable.ConstantState {
    public int A00;
    public int A01;
    public ColorStateList A02;
    public ColorStateList A03;
    public Bitmap A04;
    public Paint A05;
    public PorterDuff.Mode A06;
    public PorterDuff.Mode A07;
    public C014006n A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;

    public C013906m() {
        this.A03 = null;
        this.A07 = C013606j.A09;
        this.A08 = new C014006n();
    }

    public C013906m(C013906m r4) {
        this.A03 = null;
        this.A07 = C013606j.A09;
        if (r4 != null) {
            this.A01 = r4.A01;
            C014006n r2 = new C014006n(r4.A08);
            this.A08 = r2;
            Paint paint = r4.A08.A06;
            if (paint != null) {
                r2.A06 = new Paint(paint);
            }
            Paint paint2 = r4.A08.A07;
            if (paint2 != null) {
                this.A08.A07 = new Paint(paint2);
            }
            this.A03 = r4.A03;
            this.A07 = r4.A07;
            this.A09 = r4.A09;
        }
    }

    public void A00(int i, int i2) {
        this.A04.eraseColor(0);
        Canvas canvas = new Canvas(this.A04);
        C014006n r0 = this.A08;
        r0.A00(canvas, C014006n.A0G, r0.A0F, i, i2);
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public int getChangingConfigurations() {
        return this.A01;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable() {
        return new C013606j(this);
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable(Resources resources) {
        return new C013606j(this);
    }
}
