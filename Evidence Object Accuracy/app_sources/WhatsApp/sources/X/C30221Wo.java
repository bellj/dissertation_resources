package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Wo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30221Wo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99704kj();
    public final int A00;
    public final String A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30221Wo(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A02 = readString;
        String readString2 = parcel.readString();
        AnonymousClass009.A05(readString2);
        this.A01 = readString2;
        this.A00 = parcel.readInt();
    }

    public C30221Wo(String str, String str2, int i) {
        this.A02 = str;
        this.A01 = str2;
        this.A00 = i;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof C30221Wo)) {
            return false;
        }
        C30221Wo r4 = (C30221Wo) obj;
        if (!this.A02.equals(r4.A02) || !this.A01.equals(r4.A01) || this.A00 != r4.A00) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((this.A02.hashCode() * 31) + this.A01.hashCode()) * 31) + this.A00;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("ConnectedAccount:{'id'='");
        sb.append(this.A02);
        sb.append("', 'name'='");
        sb.append(this.A01);
        sb.append("', 'likes'='");
        sb.append(this.A00);
        sb.append("'}");
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeInt(this.A00);
    }
}
