package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.2I9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2I9 {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2I9(C48302Fl r1) {
        this.A00 = r1;
    }

    public C14680lr A00(Context context, View view) {
        AnonymousClass01J r1 = this.A00.A03;
        return new C14680lr(context, view, (C14900mE) r1.A8X.get(), (AnonymousClass018) r1.ANb.get(), (C14850m9) r1.A04.get(), (AbstractC14440lR) r1.ANe.get());
    }
}
