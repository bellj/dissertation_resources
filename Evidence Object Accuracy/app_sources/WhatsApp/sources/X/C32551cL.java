package X;

import java.util.Arrays;

/* renamed from: X.1cL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32551cL {
    public final AbstractC14640lm A00;
    public final AbstractC14640lm A01;
    public final Long[] A02;
    public final String[] A03;

    public C32551cL(AbstractC14640lm r1, AbstractC14640lm r2, Long[] lArr, String[] strArr) {
        this.A01 = r1;
        this.A00 = r2;
        this.A02 = lArr;
        this.A03 = strArr;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("toJid=");
        sb.append(this.A01);
        sb.append("; participant=");
        sb.append(this.A00);
        sb.append("; rowIds=");
        sb.append(Arrays.toString(this.A02));
        sb.append("; ids=");
        sb.append(Arrays.toString(this.A03));
        return sb.toString();
    }
}
