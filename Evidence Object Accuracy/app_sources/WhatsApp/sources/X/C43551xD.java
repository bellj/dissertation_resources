package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1xD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C43551xD extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C43551xD A07;
    public static volatile AnonymousClass255 A08;
    public int A00;
    public int A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public C57422n1 A03;
    public String A04 = "";
    public String A05 = "";
    public String A06 = "";

    static {
        C43551xD r0 = new C43551xD();
        A07 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        C81483u5 r1;
        switch (r8.ordinal()) {
            case 0:
                return A07;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                C43551xD r10 = (C43551xD) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A06;
                int i2 = r10.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A06 = r9.Afy(str, r10.A06, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A05;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A05 = r9.Afy(str2, r10.A05, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                String str3 = this.A04;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A04 = r9.Afy(str3, r10.A04, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                int i3 = this.A01;
                boolean z8 = false;
                if ((i2 & 8) == 8) {
                    z8 = true;
                }
                this.A01 = r9.Afp(i3, r10.A01, z7, z8);
                this.A02 = r9.Afr(this.A02, r10.A02);
                this.A03 = (C57422n1) r9.Aft(this.A03, r10.A03);
                if (r9 == C463025i.A00) {
                    this.A00 |= r10.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r92.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r92.A0A();
                            this.A00 = 1 | this.A00;
                            this.A06 = A0A;
                        } else if (A03 == 18) {
                            String A0A2 = r92.A0A();
                            this.A00 |= 2;
                            this.A05 = A0A2;
                        } else if (A03 == 26) {
                            String A0A3 = r92.A0A();
                            this.A00 |= 4;
                            this.A04 = A0A3;
                        } else if (A03 == 32) {
                            this.A00 |= 8;
                            this.A01 = r92.A02();
                        } else if (A03 == 42) {
                            AnonymousClass1K6 r12 = this.A02;
                            if (!((AnonymousClass1K7) r12).A00) {
                                r12 = AbstractC27091Fz.A0G(r12);
                                this.A02 = r12;
                            }
                            r12.add((AnonymousClass22G) r92.A09(r102, AnonymousClass22G.A08.A0U()));
                        } else if (A03 == 50) {
                            if ((this.A00 & 16) == 16) {
                                r1 = (C81483u5) this.A03.A0T();
                            } else {
                                r1 = null;
                            }
                            C57422n1 r0 = (C57422n1) r92.A09(r102, C57422n1.A05.A0U());
                            this.A03 = r0;
                            if (r1 != null) {
                                r1.A04(r0);
                                this.A03 = (C57422n1) r1.A01();
                            }
                            this.A00 |= 16;
                        } else if (!A0a(r92, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r13 = new C28971Pt(e2.getMessage());
                        r13.unfinishedMessage = this;
                        throw new RuntimeException(r13);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A02).A00 = false;
                return null;
            case 4:
                return new C43551xD();
            case 5:
                return new C43561xE();
            case 6:
                break;
            case 7:
                if (A08 == null) {
                    synchronized (C43551xD.class) {
                        if (A08 == null) {
                            A08 = new AnonymousClass255(A07);
                        }
                    }
                }
                return A08;
            default:
                throw new UnsupportedOperationException();
        }
        return A07;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            i = CodedOutputStream.A07(1, this.A06) + 0;
        } else {
            i = 0;
        }
        if ((this.A00 & 2) == 2) {
            i += CodedOutputStream.A07(2, this.A05);
        }
        if ((this.A00 & 4) == 4) {
            i += CodedOutputStream.A07(3, this.A04);
        }
        if ((this.A00 & 8) == 8) {
            i += CodedOutputStream.A04(4, this.A01);
        }
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A02.get(i3), 5);
        }
        if ((this.A00 & 16) == 16) {
            C57422n1 r0 = this.A03;
            if (r0 == null) {
                r0 = C57422n1.A05;
            }
            i += CodedOutputStream.A0A(r0, 6);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A06);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A05);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A04);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A01);
        }
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A02.get(i), 5);
        }
        if ((this.A00 & 16) == 16) {
            C57422n1 r0 = this.A03;
            if (r0 == null) {
                r0 = C57422n1.A05;
            }
            codedOutputStream.A0L(r0, 6);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
