package X;

import android.view.View;
import android.view.ViewParent;
import com.whatsapp.R;

/* renamed from: X.2CM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CM extends AnonymousClass04X {
    public final /* synthetic */ ScaleGestureDetector$OnScaleGestureListenerC52942c4 A00;

    @Override // X.AnonymousClass04X
    public int A03(View view, int i, int i2) {
        return i;
    }

    @Override // X.AnonymousClass04X
    public int A04(View view, int i, int i2) {
        return i;
    }

    public /* synthetic */ AnonymousClass2CM(ScaleGestureDetector$OnScaleGestureListenerC52942c4 r1) {
        this.A00 = r1;
    }

    public static /* synthetic */ void A00(AnonymousClass2CM r2) {
        AbstractC115435Rn r1;
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r22 = r2.A00;
        View view = r22.A0E;
        if (view != null && r22.A0C == view && (r1 = r22.A0G) != null) {
            r22.A0E = null;
            ((AnonymousClass5AR) r1).A00.A7N();
        }
    }

    @Override // X.AnonymousClass04X
    public int A01(View view) {
        return view.getWidth();
    }

    @Override // X.AnonymousClass04X
    public int A02(View view) {
        return view.getHeight();
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass04X
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(android.view.View r12, float r13, float r14) {
        /*
        // Method dump skipped, instructions count: 369
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2CM.A05(android.view.View, float, float):void");
    }

    @Override // X.AnonymousClass04X
    public void A06(View view, int i) {
        AnonymousClass2G9 r2;
        ViewParent parent = view.getParent();
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r3 = this.A00;
        r3.A07 = r3.A04;
        r3.A08 = r3.A05;
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        if (r3.A0N && (r2 = r3.A0I) != null) {
            r2.A04(r3.getResources().getColor(R.color.black), 0);
            r3.A0I.setPlayerElevation(0);
            r3.A0I.setVisibility(8);
            AnonymousClass21T r0 = ((AnonymousClass39B) r3.A0I).A06;
            if (r0 != null && r0.A0B()) {
                r3.A0I.A02();
            }
        }
    }

    @Override // X.AnonymousClass04X
    public void A07(View view, int i, int i2, int i3, int i4) {
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r2 = this.A00;
        r2.A0F.A01(view);
        if (r2.A0U.A03 == 1 && !r2.A0P) {
            r2.A05 = view.getTop();
            r2.A04 = view.getLeft();
        }
    }

    @Override // X.AnonymousClass04X
    public boolean A08(View view, int i) {
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r1 = this.A00;
        View view2 = r1.A0C;
        return view2 != null && view == view2 && !r1.A0Q;
    }
}
