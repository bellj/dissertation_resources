package X;

import android.os.AsyncTask;

/* renamed from: X.0AK  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0AK extends AsyncTask {
    public final /* synthetic */ AbstractServiceC003801r A00;

    public AnonymousClass0AK(AbstractServiceC003801r r1) {
        this.A00 = r1;
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        while (true) {
            AbstractServiceC003801r r2 = this.A00;
            AbstractC12330hk A01 = r2.A01();
            if (A01 == null) {
                return null;
            }
            r2.A05(A01.getIntent());
            A01.A7T();
        }
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onCancelled(Object obj) {
        this.A00.A02();
    }

    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        this.A00.A02();
    }
}
