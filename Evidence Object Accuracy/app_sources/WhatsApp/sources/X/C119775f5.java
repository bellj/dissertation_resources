package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5f5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119775f5 extends AbstractC30871Zd {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(11);
    public int A00 = 0;
    public int A01;
    public int A02 = 1;
    public String A03 = null;
    public String A04;
    public String A05;
    public String A06 = null;
    public boolean A07;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0122, code lost:
        if (X.C117295Zj.A1T(r4, "default-eligible-p2p", null, "1") != false) goto L_0x0124;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0091, code lost:
        if (X.C117295Zj.A1T(r8, "default-debit", null, "1") != false) goto L_0x0093;
     */
    @Override // X.AnonymousClass1ZP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass102 r7, X.AnonymousClass1V8 r8, int r9) {
        /*
        // Method dump skipped, instructions count: 467
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119775f5.A01(X.102, X.1V8, int):void");
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        throw C12980iv.A0u("PAY: BrazilCardMethodData toNetwork unsupported");
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        try {
            JSONObject A0C = A0C();
            A0C.put("v", this.A02);
            A0C.put("paymentRails", super.A03);
            A0C.put("needsDeviceBinding", this.A07);
            String str = this.A03;
            if (str != null) {
                A0C.put("bindingType", str);
            }
            String str2 = this.A06;
            if (str2 != null) {
                A0C.put("tokenId", str2);
            }
            String str3 = this.A0C;
            if (str3 != null) {
                A0C.put("cardImageContentId", str3);
            }
            String str4 = this.A0E;
            if (str4 != null) {
                A0C.put("cardImageUrl", str4);
            }
            String str5 = this.A0D;
            if (str5 != null) {
                A0C.put("cardImageLabelColor", str5);
            }
            String str6 = this.A0J;
            if (str6 != null) {
                A0C.put("lastFour", str6);
            }
            Long l = this.A09;
            if (l != null) {
                A0C.put("cardDataUpdatedTimeMillis", l);
            }
            A0C.put("notificationType", this.A05);
            A0C.put("cardState", this.A0F);
            A0C.put("p2pEligible", this.A0Y);
            A0C.put("p2mEligible", this.A0U);
            A0C.put("verificationStatus", this.A01);
            return A0C.toString();
        } catch (JSONException e) {
            Log.w(C12960it.A0b("PAY: BrazilCardMethodData toDBString threw: ", e));
            return null;
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A04(String str) {
        if (str != null) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                A0D(A05);
                this.A02 = A05.optInt("v", 1);
                super.A03 = A05.optInt("paymentRails", 0);
                this.A0Y = A05.optBoolean("p2pEligible", false);
                this.A0U = A05.optBoolean("p2mEligible", false);
                this.A07 = A05.optBoolean("needsDeviceBinding", false);
                this.A03 = A05.optString("bindingType", null);
                this.A06 = A05.optString("tokenId", null);
                this.A0C = A05.optString("cardImageContentId", null);
                this.A0E = A05.optString("cardImageUrl", null);
                this.A0D = A05.optString("cardImageLabelColor", null);
                this.A0J = A05.optString("lastFour", null);
                this.A09 = Long.valueOf(A05.optLong("cardDataUpdatedTimeMillis", 0));
                this.A05 = A05.optString("notificationType", null);
                this.A0F = A05.optString("cardState", "UNSET");
                this.A01 = A05.optInt("verificationStatus");
            } catch (JSONException e) {
                Log.w(C12960it.A0b("PAY: BrazilCardMethodData fromDBString threw: ", e));
            }
        }
    }

    @Override // X.AnonymousClass1ZY
    public AbstractC28901Pl A05() {
        C17930rd A00 = C17930rd.A00("BR");
        String str = this.A0H;
        int i = super.A00;
        int A002 = C117305Zk.A00(this.A0W ? 1 : 0);
        int A003 = C117305Zk.A00(this.A0V ? 1 : 0);
        int A004 = C117305Zk.A00(this.A0S ? 1 : 0);
        int A005 = C117305Zk.A00(this.A0R ? 1 : 0);
        C30881Ze A06 = C30881Ze.A06(A00, this, str, this.A0J, i, A002, A003, A004, A005, super.A01, super.A05);
        A06.A04 = this.A01;
        return A06;
    }

    @Override // X.AnonymousClass1ZY
    public LinkedHashSet A09() {
        return new LinkedHashSet(Collections.singletonList(C30771Yt.A04));
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ verified: ");
        A0k.append(this.A0a);
        A0k.append(" accountType: ");
        A0k.append(super.A00);
        A0k.append(" bankName: ");
        A0k.append(this.A08);
        A0k.append(" bankPhoneNumber: ");
        A0k.append(this.A0B);
        A0k.append(" bankLogoUrl: ");
        A0k.append(this.A0A);
        A0k.append(" verificationType: ");
        A0k.append(this.A0O);
        A0k.append(" otpNumberMatch: ");
        A0k.append(this.A0Q);
        A0k.append(" paymentRails: ");
        A0k.append(super.A03);
        A0k.append(" p2pEligible: ");
        A0k.append(this.A0Y);
        A0k.append(" p2mEligible: ");
        A0k.append(this.A0U);
        A0k.append(" timeLastAdded: ");
        A0k.append(super.A06);
        A0k.append(" needsDeviceBinding: ");
        A0k.append(this.A07);
        A0k.append(" bindingType: ");
        A0k.append(this.A03);
        A0k.append(" cardImageContentId: ");
        A0k.append(this.A0C);
        A0k.append(" cardImageUrl: ");
        A0k.append(this.A0E);
        A0k.append(" cardImageLabelColor: ");
        A0k.append(this.A0D);
        A0k.append(" notificationType: ");
        A0k.append(this.A05);
        A0k.append(" lastFour: ");
        A0k.append(this.A0J);
        A0k.append("payoutVerificationStatus: ");
        A0k.append(this.A01);
        A0k.append(" displayState: ");
        A0k.append(this.A0I);
        StringBuilder A0k2 = C12960it.A0k(" capabilities { editable: ");
        A0k2.append(this.A0P);
        A0k2.append(", verifiable: ");
        A0k2.append(this.A0Z);
        A0k2.append(", p2pDefaultEligible: ");
        A0k2.append(this.A0X);
        A0k2.append(", p2mDefaultEligible: ");
        A0k2.append(this.A0T);
        A0k2.append(", p2pSend: ");
        A0k2.append(this.A0N);
        A0k2.append(", p2pReceive: ");
        A0k2.append(this.A0M);
        A0k2.append(", p2mSend: ");
        A0k2.append(this.A0L);
        A0k2.append(", p2mReceive: ");
        A0k2.append(this.A0K);
        A0k.append(C12960it.A0d("}", A0k2));
        return C12960it.A0d(" ]", A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.A0a ? (byte) 1 : 0);
        parcel.writeParcelable(this.A08, i);
        parcel.writeString(this.A0B);
        parcel.writeString(this.A0A);
        parcel.writeString(this.A0O);
        parcel.writeByte(this.A0Q ? (byte) 1 : 0);
        parcel.writeInt(super.A03);
        parcel.writeByte(this.A0Y ? (byte) 1 : 0);
        parcel.writeByte(this.A0U ? (byte) 1 : 0);
        parcel.writeLong(super.A06);
        parcel.writeInt(super.A04);
        parcel.writeString(this.A0G);
        parcel.writeString(this.A0H);
        parcel.writeInt(super.A00);
        parcel.writeByte(this.A0W ? (byte) 1 : 0);
        parcel.writeByte(this.A0V ? (byte) 1 : 0);
        parcel.writeByte(this.A0S ? (byte) 1 : 0);
        parcel.writeByte(this.A0R ? (byte) 1 : 0);
        parcel.writeString(this.A0J);
        parcel.writeLong(super.A05);
        parcel.writeInt(super.A01);
        parcel.writeByte(this.A07 ? (byte) 1 : 0);
        parcel.writeString(this.A03);
        parcel.writeString(this.A06);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A04);
        parcel.writeInt(this.A01);
        parcel.writeString(this.A0C);
        parcel.writeString(this.A0E);
        parcel.writeString(this.A0D);
        parcel.writeLong(this.A09.longValue());
        parcel.writeString(this.A05);
        parcel.writeString(this.A0F);
        parcel.writeString(this.A0I);
        parcel.writeByte(this.A0P ? (byte) 1 : 0);
        parcel.writeByte(this.A0Z ? (byte) 1 : 0);
        parcel.writeByte(this.A0X ? (byte) 1 : 0);
        parcel.writeByte(this.A0T ? (byte) 1 : 0);
        parcel.writeString(this.A0N);
        parcel.writeString(this.A0M);
        parcel.writeString(this.A0L);
        parcel.writeString(this.A0K);
    }
}
