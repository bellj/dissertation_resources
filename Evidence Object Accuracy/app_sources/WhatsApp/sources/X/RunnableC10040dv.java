package X;

import java.util.List;

/* renamed from: X.0dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10040dv implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C06040Ry A01;
    public final /* synthetic */ List A02;
    public final /* synthetic */ List A03;

    public RunnableC10040dv(C06040Ry r1, List list, List list2, int i) {
        this.A01 = r1;
        this.A03 = list;
        this.A02 = list2;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A01.A05.execute(new AnonymousClass0d8(this, AnonymousClass0RD.A00(new AnonymousClass0Es(this))));
    }
}
