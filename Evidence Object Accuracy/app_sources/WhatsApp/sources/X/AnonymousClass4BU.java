package X;

/* renamed from: X.4BU  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BU {
    A01(0),
    A03(1),
    A04(2),
    A02(3);
    
    public final int value;

    AnonymousClass4BU(int i) {
        this.value = i;
    }

    public static AnonymousClass4BU A00(int i) {
        if (i == 0) {
            return A01;
        }
        if (i == 1) {
            return A03;
        }
        if (i == 2) {
            return A04;
        }
        if (i != 3) {
            return null;
        }
        return A02;
    }
}
