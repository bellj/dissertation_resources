package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2Gv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48582Gv extends LayoutInflater {
    public static final String[] A01 = {"android.widget.", "android.webkit."};
    public final AnonymousClass018 A00;

    public C48582Gv(Context context, LayoutInflater layoutInflater, AnonymousClass018 r3) {
        super(layoutInflater, context);
        this.A00 = r3;
    }

    @Override // android.view.LayoutInflater
    public LayoutInflater cloneInContext(Context context) {
        return new C48582Gv(context, this, this.A00);
    }

    @Override // android.view.LayoutInflater
    public View inflate(int i, ViewGroup viewGroup, boolean z) {
        View inflate = super.inflate(i, viewGroup, z);
        AnonymousClass018 r4 = this.A00;
        if (!(inflate instanceof AnonymousClass2H4)) {
            if (!z || inflate.getTag(R.id.bidilayout_ignore) == null) {
                C42941w9.A06(inflate, r4);
            } else if (inflate instanceof ViewGroup) {
                ViewGroup viewGroup2 = (ViewGroup) inflate;
                int childCount = viewGroup2.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    C42941w9.A06(viewGroup2.getChildAt(i2), r4);
                }
            }
            inflate.setTag(R.id.bidilayout_ignore, C42941w9.A00);
        }
        return inflate;
    }

    @Override // android.view.LayoutInflater
    public View onCreateView(String str, AttributeSet attributeSet) {
        View createView;
        for (String str2 : A01) {
            try {
                createView = createView(str, str2, attributeSet);
            } catch (ClassNotFoundException unused) {
            }
            if (createView != null) {
                return createView;
            }
        }
        return super.onCreateView(str, attributeSet);
    }
}
