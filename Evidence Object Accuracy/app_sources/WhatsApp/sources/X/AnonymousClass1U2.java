package X;

import android.location.LocationListener;
import java.lang.ref.WeakReference;

/* renamed from: X.1U2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1U2 implements AnonymousClass1U3 {
    public final float A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final WeakReference A04;

    public AnonymousClass1U2(LocationListener locationListener, float f, int i, long j, long j2) {
        this.A04 = new WeakReference(locationListener);
        this.A03 = j;
        this.A02 = j2;
        this.A00 = f;
        this.A01 = i;
    }
}
