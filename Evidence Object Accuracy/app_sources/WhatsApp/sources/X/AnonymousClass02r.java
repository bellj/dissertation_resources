package X;

import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Bundle;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.02r  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass02r {
    public static ServiceConnectionC006102x A02;
    public static String A03;
    public static Set A04 = new HashSet();
    public static final Object A05 = new Object();
    public static final Object A06 = new Object();
    public final NotificationManager A00;
    public final Context A01;

    public AnonymousClass02r(Context context) {
        this.A01 = context;
        this.A00 = (NotificationManager) context.getSystemService("notification");
    }

    public final void A00(AbstractC005802u r4) {
        synchronized (A06) {
            ServiceConnectionC006102x r1 = A02;
            if (r1 == null) {
                r1 = new ServiceConnectionC006102x(this.A01.getApplicationContext());
                A02 = r1;
            }
            r1.A02.obtainMessage(0, r4).sendToTarget();
        }
    }

    public void A01(String str, int i) {
        this.A00.cancel(str, i);
        if (Build.VERSION.SDK_INT <= 19) {
            A00(new C005702t(this.A01.getPackageName(), str, i));
        }
    }

    public void A02(String str, int i, Notification notification) {
        Bundle A00 = C005902v.A00(notification);
        if (A00 == null || !A00.getBoolean("android.support.useSideChannel")) {
            this.A00.notify(str, i, notification);
            return;
        }
        A00(new C006002w(notification, this.A01.getPackageName(), str, i));
        this.A00.cancel(str, i);
    }

    public boolean A03() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            return this.A00.areNotificationsEnabled();
        }
        if (i < 19) {
            return true;
        }
        Context context = this.A01;
        Object systemService = context.getSystemService("appops");
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        String packageName = context.getApplicationContext().getPackageName();
        int i2 = applicationInfo.uid;
        try {
            Class<?> cls = Class.forName(AppOpsManager.class.getName());
            Class<?> cls2 = Integer.TYPE;
            if (((Integer) cls.getMethod("checkOpNoThrow", cls2, cls2, String.class).invoke(systemService, Integer.valueOf(((Integer) cls.getDeclaredField("OP_POST_NOTIFICATION").get(Integer.class)).intValue()), Integer.valueOf(i2), packageName)).intValue() != 0) {
                return false;
            }
            return true;
        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException | NoSuchMethodException | RuntimeException | InvocationTargetException unused) {
            return true;
        }
    }
}
