package X;

import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.12t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C237212t {
    public Set A00;
    public final C14900mE A01;
    public final C14820m6 A02;
    public final C21320xE A03;
    public final C236812p A04;

    public C237212t(C14900mE r1, C14820m6 r2, C21320xE r3, C236812p r4) {
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
        this.A03 = r3;
    }

    public final void A00() {
        HashSet hashSet;
        synchronized (this) {
            if (this.A00 == null) {
                long j = this.A02.A00.getLong("first_unseen_joinable_call", 0);
                if (j > 0) {
                    hashSet = new HashSet(this.A04.A05());
                } else {
                    hashSet = new HashSet();
                }
                this.A00 = hashSet;
                StringBuilder sb = new StringBuilder();
                sb.append("UnseenJoinableCallsBadge/init count:");
                sb.append(hashSet.size());
                sb.append(" timestamp:");
                sb.append(j);
                Log.i(sb.toString());
            }
        }
    }
}
