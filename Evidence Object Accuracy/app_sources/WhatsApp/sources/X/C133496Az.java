package X;

/* renamed from: X.6Az  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133496Az implements AnonymousClass6MW {
    public final /* synthetic */ C117985b5 A00;

    public C133496Az(C117985b5 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MW
    public void APo(C452120p r4) {
        C117985b5 r2 = this.A00;
        C127115ty.A01(r2.A00);
        C128145vd r1 = new C128145vd(2);
        r1.A02 = r4;
        r2.A01.A0B(r1);
    }

    @Override // X.AnonymousClass6MW
    public void AX6(String str, String str2) {
        C128145vd r1 = new C128145vd(3);
        r1.A07 = str;
        r1.A03 = str2;
        this.A00.A01.A0B(r1);
    }
}
