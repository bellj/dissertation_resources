package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.TextUtils;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: X.0mK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14960mK {
    public static Intent A00(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.registration.ChangeNumberOverview");
        return intent;
    }

    public static Intent A01(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.registration.EULA");
        intent.setFlags(268468224);
        return intent;
    }

    public static Intent A02(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.HomeActivity");
        return intent;
    }

    public static Intent A03(Context context) {
        return new Intent().setClassName(context.getPackageName(), "com.whatsapp.HomeActivity").setAction("com.whatsapp.intent.action.CHATS");
    }

    public static Intent A04(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.Main");
        return intent;
    }

    public static Intent A05(Context context) {
        return new Intent().setClassName(context.getPackageName(), "com.whatsapp.registration.RegisterPhone");
    }

    public static Intent A06(Context context, int i) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.dmsetting.ChangeDMSettingActivity");
        intent.putExtra("entry_point", i);
        return intent;
    }

    public static Intent A07(Context context, int i) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.backup.encryptedbackup.EncBackupMainActivity");
        intent.putExtra("user_action", i);
        return intent;
    }

    public static Intent A08(Context context, int i, long j, long j2, boolean z, boolean z2) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.registration.VerifyPhoneNumber");
        className.putExtra("changenumber", z);
        className.putExtra("sms_retry_time", j);
        className.putExtra("voice_retry_time", j2);
        className.putExtra("use_sms_retriever", z2);
        className.putExtra("code_verification_mode", i);
        return className;
    }

    public static Intent A09(Context context, long j) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.insufficientstoragespace.InsufficientStorageSpaceActivity");
        intent.putExtra("allowSkipKey", false);
        intent.putExtra("spaceNeededInBytes", j);
        return intent;
    }

    public static Intent A0A(Context context, long j, long j2, boolean z) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.registration.PrimaryFlashCallEducationScreen");
        intent.putExtra("sms_retry_time", j);
        intent.putExtra("voice_retry_time", j2);
        intent.putExtra("change_number", z);
        return intent;
    }

    public static Intent A0B(Context context, Bundle bundle, AbstractC14640lm r6, C15580nU r7, ArrayList arrayList, boolean z) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity");
        className.putExtra("edit_mode", true);
        className.putExtra("jid", C15380n4.A03(r6));
        className.putExtra("quoted_message", bundle);
        className.putExtra("quoted_group_jid", C15380n4.A03(r7));
        className.putExtra("has_number_from_url", z);
        className.putStringArrayListExtra("vcard_array", arrayList);
        return className;
    }

    public static Intent A0C(Context context, AbstractC14640lm r4) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.gallery.MediaGalleryActivity");
        intent.putExtra("jid", C15380n4.A03(r4));
        return intent;
    }

    public static Intent A0D(Context context, AbstractC14640lm r6, int i) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.gallery.MediaGalleryActivity");
        intent.putExtra("pos", -1);
        intent.putExtra("jid", C15380n4.A03(r6));
        intent.putExtra("alert", true);
        intent.putExtra("key", i);
        return intent;
    }

    public static Intent A0E(Context context, AbstractC14640lm r4, int i, int i2) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.ephemeral.ChangeEphemeralSettingActivity");
        className.putExtra("jid", r4.getRawString());
        className.putExtra("current_setting", i);
        className.putExtra("entry_point", i2);
        return className;
    }

    public static Intent A0F(Context context, AbstractC14640lm r4, Boolean bool) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.status.playback.StatusPlaybackActivity");
        intent.putExtra("jid", C15380n4.A03(r4));
        intent.putExtra("single_contact_update", bool);
        return intent;
    }

    public static Intent A0G(Context context, AbstractC14640lm r4, String str, int i, int i2, long j) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.storage.StorageUsageGalleryActivity");
        className.putExtra("gallery_type", i);
        className.putExtra("jid", C15380n4.A03(r4));
        className.putExtra("memory_size", j);
        className.putExtra("session_id", str);
        className.putExtra("entry_point", i2);
        return className;
    }

    public static Intent A0H(Context context, GroupJid groupJid) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.community.CommunityHomeActivity");
        intent.putExtra("parent_group_jid", groupJid.getRawString());
        StringBuilder sb = new StringBuilder("whatsapp://community/");
        sb.append(groupJid.hashCode());
        intent.setData(Uri.parse(sb.toString()));
        intent.setFlags(603979776);
        return intent;
    }

    public static Intent A0I(Context context, GroupJid groupJid) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.group.GroupSettingsActivity");
        intent.putExtra("gid", groupJid.getRawString());
        return intent;
    }

    public static Intent A0J(Context context, GroupJid groupJid) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.community.ManageGroupsInCommunityActivity");
        intent.putExtra("parent_group_jid", groupJid.getRawString());
        return intent;
    }

    public static Intent A0K(Context context, Jid jid) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.chatinfo.ListChatInfo");
        intent.putExtra("gid", C15380n4.A03(jid));
        intent.putExtra("circular_transition", true);
        return intent;
    }

    public static Intent A0L(Context context, Jid jid) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity");
        intent.putExtra("jid", C15380n4.A03(jid));
        return intent;
    }

    public static Intent A0M(Context context, Jid jid, Integer num, int i) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.biz.catalog.view.activity.CatalogListActivity");
        intent.putExtra("cache_jid", jid.getRawString());
        intent.putExtra("source", num);
        intent.putExtra("entry_point", i);
        return intent;
    }

    public static Intent A0N(Context context, Jid jid, String str, float f, int i, int i2, int i3, int i4, boolean z) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.profile.ViewProfilePhoto");
        intent.putExtra("jid", jid.getRawString());
        intent.putExtra("circular_transition", z);
        if (str != null) {
            intent.putExtra("circular_return_name", str);
        }
        intent.putExtra("start_transition_alpha", f);
        intent.putExtra("start_transition_status_bar_color", i);
        intent.putExtra("return_transition_status_bar_color", i2);
        intent.putExtra("start_transition_navigation_bar_color", i3);
        intent.putExtra("return_transition_navigation_bar_color", i4);
        return intent;
    }

    public static Intent A0O(Context context, Jid jid, boolean z, boolean z2, boolean z3) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.group.GroupChatInfo");
        intent.putExtra("gid", C15380n4.A03(jid));
        intent.putExtra("circular_transition", z);
        intent.putExtra("show_description", z2);
        intent.putExtra("show_chat_action", z3);
        return intent;
    }

    public static Intent A0P(Context context, UserJid userJid) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.identity.IdentityVerificationActivity");
        intent.putExtra("jid", userJid.getRawString());
        return intent;
    }

    public static Intent A0Q(Context context, UserJid userJid, Integer num, Integer num2, String str, String str2, String str3) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.businesscollection.view.activity.CollectionProductListActivity");
        intent.putExtra("collection_id", str);
        intent.putExtra("collection_name", str2);
        intent.putExtra("cache_jid", userJid.getRawString());
        intent.putExtra("collection_index", str3);
        if (num != null) {
            intent.putExtra("category_browsing_entry_point", num);
        }
        if (num2 != null) {
            intent.putExtra("category_level", num2);
        }
        return intent;
    }

    public static Intent A0R(Context context, UserJid userJid, Integer num, boolean z) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.chatinfo.ContactInfoActivity");
        className.putExtra("jid", userJid.getRawString());
        className.putExtra("circular_transition", true);
        className.putExtra("should_show_chat_action", z);
        className.putExtra("profile_entry_point", num);
        return className;
    }

    public static Intent A0S(Context context, UserJid userJid, String str, boolean z, boolean z2, boolean z3) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.blockbusiness.BlockBusinessActivity");
        intent.putExtra("jid_extra", userJid.getRawString());
        intent.putExtra("entry_point_extra", str);
        intent.putExtra("show_success_toast_extra", z);
        intent.putExtra("from_spam_panel_extra", z2);
        intent.putExtra("show_report_upsell", z3);
        return intent;
    }

    public static Intent A0T(Context context, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, List list) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.voipcalling.VoipActivityV2");
        if (list != null) {
            className.putStringArrayListExtra("jid", C15380n4.A06(list));
        }
        if (bool != null) {
            className.putExtra("isTaskRoot", bool);
        }
        if (bool4 != null) {
            className.putExtra("newCall", bool4);
        }
        if (bool2 != null) {
            className.putExtra("video_call", bool2);
        }
        if (bool3 != null || AbstractC35731ia.A00(context) == null) {
            className.setFlags(268435456);
        }
        return className;
    }

    public static Intent A0U(Context context, String str) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.backup.google.GoogleBackupService");
        if (!TextUtils.isEmpty(str)) {
            intent.setAction(str);
        }
        return intent;
    }

    public static Intent A0V(Context context, String str) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.identity.IdentityVerificationActivity");
        intent.putExtra("jid", str);
        return intent;
    }

    public static Intent A0W(Context context, String str, int i) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.storage.StorageUsageActivity");
        className.putExtra("session_id", str);
        className.putExtra("entry_point", i);
        return className;
    }

    public static Intent A0X(Context context, String str, int i, long j, long j2, boolean z, boolean z2, boolean z3, boolean z4) {
        Intent className = new Intent().setClassName(context.getPackageName(), "com.whatsapp.registration.VerifyPhoneNumber");
        className.putExtra("sms_retry_time", j);
        className.putExtra("voice_retry_time", j2);
        className.putExtra("use_sms_retriever", z);
        className.putExtra("show_request_sms_code_progress", z2);
        className.putExtra("changenumber", z3);
        className.putExtra("should_request_flash_call", z4);
        className.putExtra("server_start_message", str);
        className.putExtra("flash_type", i);
        className.putExtra("code_verification_mode", 0);
        return className;
    }

    public static Intent A0Y(Context context, String str, String str2) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.stickers.StickerStorePackPreviewActivity");
        intent.putExtra("sticker_pack_id", str);
        intent.putExtra("sticker_pack_preview_source", str2);
        return intent;
    }

    public static Intent A0Z(Context context, String str, String str2, String str3, List list, int i, int i2, boolean z, boolean z2) {
        return new Intent().setClassName(context.getPackageName(), "com.whatsapp.gifvideopreview.GifVideoPreviewActivity").putExtra("preview_media_url", str).putExtra("media_url", str2).putExtra("static_preview_url", str3).putExtra("jids", C15380n4.A06(list)).putExtra("send", z2).putExtra("provider", i).putExtra("number_from_url", z).putExtra("origin", i2);
    }

    public static Intent A0a(Context context, String str, String str2, boolean z, boolean z2) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.WaInAppBrowsingActivity");
        AnonymousClass009.A05(str);
        intent.putExtra("webview_url", str);
        intent.putExtra("webview_javascript_enabled", true);
        if (!TextUtils.isEmpty(str2)) {
            intent.putExtra("webview_callback", str2);
        }
        intent.putExtra("webview_hide_url", z);
        intent.putExtra("webview_javascript_enabled", z2);
        return intent;
    }

    public static Intent A0b(Context context, Collection collection, int i) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.group.GroupMembersSelector");
        intent.putExtra("entry_point", i);
        if (collection != null && !collection.isEmpty()) {
            intent.putExtra("selected", new ArrayList(collection));
        }
        return intent;
    }

    public static Intent A0c(Context context, boolean z) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.biz.product.view.activity.ProductDetailActivity");
        intent.putExtra("is_from_product_detail_screen", z);
        return intent;
    }

    public static void A0d(AnonymousClass01E r2) {
        r2.A0v(A02(r2.A0p()).addFlags(603979776));
    }

    public Intent A0e(Context context, Bundle bundle, Parcelable parcelable, String str, String str2) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.inappsupport.ui.ContactUsActivity");
        intent.putExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.from", str);
        intent.putExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.serverStatus", str2);
        if (bundle != null) {
            Bundle bundle2 = new Bundle();
            String string = bundle.getString("com.whatsapp.support.DescribeProblemActivity.from");
            String string2 = bundle.getString("com.whatsapp.support.DescribeProblemActivity.serverstatus");
            String string3 = bundle.getString("com.whatsapp.support.DescribeProblemActivity.emailAddress");
            String string4 = bundle.getString("com.whatsapp.support.DescribeProblemActivity.description");
            if (!TextUtils.isEmpty(string)) {
                bundle2.putString("com.whatsapp.inappsupport.ui.ContactUsActivity.from", string);
            }
            if (!TextUtils.isEmpty(string2)) {
                bundle2.putString("com.whatsapp.inappsupport.ui.ContactUsActivity.serverStatus", string2);
            }
            if (!TextUtils.isEmpty(string3)) {
                bundle2.putString("com.whatsapp.inappsupport.ui.ContactUsActivity.emailAddress", string3);
            }
            if (!TextUtils.isEmpty(string4)) {
                bundle2.putString("com.whatsapp.inappsupport.ui.ContactUsActivity.description", string4);
            }
            intent.putExtras(bundle2);
        }
        if (parcelable != null) {
            intent.putExtra("com.whatsapp.inappsupport.ui.ContactUsActivity.supportUserContext", parcelable);
        }
        return intent;
    }

    @Deprecated
    public Intent A0f(Context context, C15370n3 r5) {
        Jid A0B = r5.A0B(UserJid.class);
        AnonymousClass009.A05(A0B);
        return A0R(context, (UserJid) A0B, null, true);
    }

    public Intent A0g(Context context, C15370n3 r3) {
        return A0i(context, (AbstractC14640lm) r3.A0B(AbstractC14640lm.class));
    }

    public Intent A0h(Context context, C15370n3 r4, Integer num) {
        Jid A0B = r4.A0B(UserJid.class);
        AnonymousClass009.A05(A0B);
        return A0R(context, (UserJid) A0B, num, true);
    }

    public Intent A0i(Context context, AbstractC14640lm r4) {
        Intent A0j = A0j(context, r4);
        A0j.addFlags(335544320);
        return A0j;
    }

    public Intent A0j(Context context, AbstractC14640lm r5) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.Conversation");
        intent.putExtra("jid", C15380n4.A03(r5));
        return intent;
    }

    public Intent A0k(Context context, AbstractC14640lm r6, String str) {
        Intent A0i = A0i(context, r6);
        A0i.putExtra("wa_type", (byte) 0);
        A0i.putExtra("share_msg", str);
        A0i.putExtra("has_share", true);
        A0i.putExtra("confirm", true);
        A0i.putExtra("text_from_url", true);
        A0i.putExtra("number_from_url", true);
        AnonymousClass2KJ.A00(context, A0i);
        return A0i;
    }

    public Intent A0l(Context context, UserJid userJid, Boolean bool) {
        ArrayList arrayList = new ArrayList();
        if (userJid != null) {
            arrayList.add(userJid);
        }
        return A0T(context, bool, null, null, null, arrayList);
    }

    public Intent A0m(Context context, AbstractC15340mz r7, String str) {
        AnonymousClass1IS r4 = r7.A0z;
        Intent putExtra = A0i(context, r4.A00).putExtra("start_t", SystemClock.uptimeMillis()).putExtra("row_id", r7.A11).putExtra("sort_id", r7.A12);
        C38211ni.A00(putExtra, r4);
        return putExtra.putExtra("query", str);
    }

    public Intent A0n(Context context, String str, int i, boolean z) {
        Intent A0T = A0T(context, Boolean.valueOf(z), null, null, null, null);
        A0T.setAction("com.whatsapp.intent.action.ACCEPT_CALL");
        A0T.putExtra("call_ui_action", i);
        A0T.putExtra("call_id", str);
        A0T.putExtra("isTaskRoot", z);
        return A0T;
    }
}
