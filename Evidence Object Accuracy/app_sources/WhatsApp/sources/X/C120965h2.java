package X;

import org.json.JSONObject;

/* renamed from: X.5h2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120965h2 extends C1309860t {
    public final String A00;

    public C120965h2(AnonymousClass1V8 r2) {
        super(r2);
        this.A00 = r2.A0H("type");
    }

    public C120965h2(JSONObject jSONObject) {
        super(jSONObject);
        this.A00 = jSONObject.getString("type");
    }

    @Override // X.C1309860t
    public JSONObject A00() {
        return super.A00().put("type", this.A00);
    }
}
