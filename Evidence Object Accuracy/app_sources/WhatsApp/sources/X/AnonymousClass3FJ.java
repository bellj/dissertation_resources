package X;

import android.content.Context;
import java.util.Map;

/* renamed from: X.3FJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FJ {
    public final Context A00;
    public final C16590pI A01;
    public final AbstractC19700uX A02;
    public final C17120qI A03;
    public final String A04;

    public AnonymousClass3FJ(C16590pI r2, AbstractC19700uX r3, C17120qI r4, String str) {
        C16700pc.A0G(r2, r4);
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = str;
        this.A02 = r3;
        Context context = r2.A00;
        C16700pc.A0B(context);
        this.A00 = context;
    }

    public final void A00(AnonymousClass01E r3, String str) {
        this.A03.A02(this.A04).A01(new AnonymousClass6EB(r3, str));
    }

    public final void A01(AnonymousClass3BP r23, String str, Map map, Map map2, int i, boolean z) {
        if (r23.A01 == AnonymousClass4A4.A01) {
            if (!z) {
                C17120qI r6 = this.A03;
                String str2 = this.A04;
                C50132Og A02 = r6.A02(str2);
                Context context = this.A00;
                A02.A00(new AbstractC50172Ok(str, map, map2, i) { // from class: X.5A0
                    public final /* synthetic */ int A00;
                    public final /* synthetic */ String A02;
                    public final /* synthetic */ Map A03;
                    public final /* synthetic */ Map A04;

                    {
                        this.A02 = r2;
                        this.A00 = r5;
                        this.A03 = r3;
                        this.A04 = r4;
                    }

                    @Override // X.AbstractC50172Ok
                    public final void APz(Object obj) {
                        AnonymousClass3FJ r1 = AnonymousClass3FJ.this;
                        String str3 = this.A02;
                        int i2 = this.A00;
                        Map map3 = this.A03;
                        Map map4 = this.A04;
                        C16700pc.A0F(r1, str3);
                        C16700pc.A0E(map3, 3);
                        C16700pc.A0E(map4, 4);
                        r1.A00(r1.A02.AEX(str3, r1.A04, map3, map4, i2), str3);
                    }
                }, AnonymousClass6E4.class, context);
                r6.A02(str2).A00(new AbstractC50172Ok() { // from class: X.59z
                    @Override // X.AbstractC50172Ok
                    public final void APz(Object obj) {
                        AnonymousClass3FJ r1 = AnonymousClass3FJ.this;
                        C16700pc.A0E(r1, 0);
                        r1.A02.A6h();
                    }
                }, AnonymousClass6E3.class, context);
                this.A02.AYh(r23.A00, r23.A03, r23.A02.name(), str, str2, map, map2, i);
                r6.A02(str2).A01(new AnonymousClass5A2());
                return;
            }
        } else if (!z) {
            C17120qI r0 = this.A03;
            String str3 = this.A04;
            r0.A02(str3).A00(new AbstractC50172Ok(str, map, map2, i) { // from class: X.5A1
                public final /* synthetic */ int A00;
                public final /* synthetic */ String A02;
                public final /* synthetic */ Map A03;
                public final /* synthetic */ Map A04;

                {
                    this.A02 = r2;
                    this.A00 = r5;
                    this.A03 = r3;
                    this.A04 = r4;
                }

                @Override // X.AbstractC50172Ok
                public final void APz(Object obj) {
                    AnonymousClass3FJ r1 = AnonymousClass3FJ.this;
                    String str4 = this.A02;
                    int i2 = this.A00;
                    Map map3 = this.A03;
                    Map map4 = this.A04;
                    C16700pc.A0F(r1, str4);
                    C16700pc.A0E(map3, 3);
                    C16700pc.A0E(map4, 4);
                    r1.A00(r1.A02.AEX(str4, r1.A04, map3, map4, i2), str4);
                }
            }, AnonymousClass6E4.class, this.A00);
            this.A02.AYb(r23.A03, r23.A02.name(), str, str3, map, map2, i);
            return;
        }
        AbstractC19700uX r10 = this.A02;
        String str4 = this.A04;
        A00(r10.AEX(str, str4, map, map2, i), str);
        this.A03.A02(str4).A01(new AnonymousClass6EC(r23.A03, r23.A02.name()));
    }
}
