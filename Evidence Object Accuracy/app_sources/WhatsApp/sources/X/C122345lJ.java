package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;

/* renamed from: X.5lJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122345lJ extends AbstractC118825cR {
    public final TextEmojiLabel A00;
    public final WaTextView A01;

    public C122345lJ(View view) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.title);
        this.A00 = C12970iu.A0T(view, R.id.desc);
    }
}
