package X;

import android.view.KeyEvent;
import android.view.View;
import android.widget.ViewSwitcher;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.commonlibrary.ATMPinFragment;
import org.npci.commonlibrary.GetCredential;
import org.npci.commonlibrary.NPCIFragment;
import org.npci.commonlibrary.PinFragment;
import org.npci.commonlibrary.widget.Keypad;

/* renamed from: X.64E  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass64E implements View.OnClickListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Keypad A01;

    public AnonymousClass64E(Keypad keypad, int i) {
        this.A01 = keypad;
        this.A00 = i;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        NPCIFragment nPCIFragment;
        C117795ag A0s;
        int i;
        String inputValue;
        C117795ag A0s2;
        int i2;
        String inputValue2;
        View A01;
        int i3;
        AnonymousClass6MH r1 = this.A01.A04;
        if (r1 != null) {
            int i4 = this.A00 + 7;
            GetCredential getCredential = ((C136066Kw) r1).A00;
            getCredential.dispatchKeyEvent(new KeyEvent(0, i4));
            if (i4 == 66 && (nPCIFragment = getCredential.A0D) != null) {
                if (!(nPCIFragment instanceof PinFragment)) {
                    ATMPinFragment aTMPinFragment = (ATMPinFragment) nPCIFragment;
                    int i5 = aTMPinFragment.A00;
                    if (i5 == 0) {
                        C117325Zm.A02(((NPCIFragment) aTMPinFragment).A0B, 1).AA5();
                        aTMPinFragment.A00++;
                        return;
                    }
                    if (i5 == 1) {
                        ArrayList arrayList = ((NPCIFragment) aTMPinFragment).A0B;
                        if (C117305Zk.A0s(arrayList, 0).A00 != C117325Zm.A02(arrayList, 0).getInputValue().length()) {
                            A01 = C117325Zm.A01(arrayList, 0);
                            i3 = R.string.npci_otp_title;
                        } else if (C117305Zk.A0s(arrayList, 1).A00 != C117325Zm.A02(arrayList, 1).getInputValue().length()) {
                            A01 = C117325Zm.A01(arrayList, 1);
                            i3 = R.string.npci_atm_title;
                        } else {
                            ViewSwitcher viewSwitcher = aTMPinFragment.A01;
                            if (viewSwitcher != null) {
                                viewSwitcher.showNext();
                                aTMPinFragment.A00 = 2;
                                return;
                            }
                        }
                        aTMPinFragment.A1A(A01, aTMPinFragment.A0I(i3));
                        return;
                    }
                    int i6 = aTMPinFragment.A00;
                    if (i6 != 2 || C117325Zm.A02(((NPCIFragment) aTMPinFragment).A0B, i6).AA5()) {
                        int i7 = ((NPCIFragment) aTMPinFragment).A00;
                        if (i7 != -1) {
                            ArrayList arrayList2 = ((NPCIFragment) aTMPinFragment).A0B;
                            if ((arrayList2.get(i7) instanceof C117795ag) && ((inputValue2 = (A0s2 = C117305Zk.A0s(arrayList2, i7)).getInputValue()) == null || inputValue2.length() != A0s2.A00)) {
                                i2 = R.string.npci_invalid_otp;
                                aTMPinFragment.A1A(A0s2, aTMPinFragment.A0I(i2));
                                return;
                            }
                        }
                        int i8 = 0;
                        while (true) {
                            ArrayList arrayList3 = ((NPCIFragment) aTMPinFragment).A0B;
                            if (i8 < arrayList3.size()) {
                                if (arrayList3.get(i8) instanceof C117795ag) {
                                    A0s2 = C117305Zk.A0s(arrayList3, i8);
                                    if (A0s2.getInputValue().length() != A0s2.A00) {
                                        i2 = R.string.npci_component_message;
                                        break;
                                    }
                                }
                                i8++;
                            } else if (!aTMPinFragment.A03) {
                                aTMPinFragment.A03 = true;
                                for (int i9 = 0; i9 < arrayList3.size(); i9++) {
                                    JSONObject jSONObject = (JSONObject) C117325Zm.A02(arrayList3, i9).getFormDataTag();
                                    try {
                                        String string = jSONObject.getString("type");
                                        String string2 = jSONObject.getString("subtype");
                                        String A03 = AbstractActivityC117815ak.A03(arrayList3, aTMPinFragment, i9);
                                        String A00 = aTMPinFragment.A02.A00();
                                        C128835wk A09 = AbstractActivityC117815ak.A09(aTMPinFragment);
                                        aTMPinFragment.A0p();
                                        AnonymousClass6F0 A002 = A09.A00(A03, string, string2, A00, ((NPCIFragment) aTMPinFragment).A09);
                                        if (A002 != null) {
                                            HashMap hashMap = aTMPinFragment.A04;
                                            StringBuilder A0h = C12960it.A0h();
                                            C125155qn.A00(A002, A0h);
                                            hashMap.put(string2, A0h.toString());
                                        }
                                    } catch (JSONException e) {
                                        throw C117315Zl.A0J(e);
                                    }
                                }
                                C117325Zm.A03(C12970iu.A0A(), aTMPinFragment.A04, aTMPinFragment);
                                return;
                            } else {
                                return;
                            }
                        }
                    }
                } else {
                    PinFragment pinFragment = (PinFragment) nPCIFragment;
                    int i10 = pinFragment.A00;
                    ArrayList arrayList4 = ((NPCIFragment) pinFragment).A0B;
                    if (i10 < arrayList4.size() - 1) {
                        if (C117325Zm.A02(arrayList4, i10 + 1).AA5()) {
                            int i11 = pinFragment.A00 + 1;
                            pinFragment.A00 = i11;
                            if (i11 < arrayList4.size() - 1) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    int i12 = ((NPCIFragment) pinFragment).A00;
                    if (i12 == -1 || !(arrayList4.get(i12) instanceof C117795ag) || ((inputValue = (A0s = C117305Zk.A0s(arrayList4, i12)).getInputValue()) != null && inputValue.length() == A0s.A00)) {
                        for (int i13 = 0; i13 < arrayList4.size(); i13++) {
                            if (arrayList4.get(i13) instanceof C117795ag) {
                                A0s = C117305Zk.A0s(arrayList4, i13);
                                if (A0s.getInputValue().length() != A0s.A00) {
                                    i = R.string.npci_component_message;
                                }
                            }
                        }
                        if (!pinFragment.A03) {
                            pinFragment.A03 = true;
                            for (int i14 = 0; i14 < arrayList4.size(); i14++) {
                                try {
                                    JSONObject jSONObject2 = (JSONObject) C117325Zm.A02(arrayList4, i14).getFormDataTag();
                                    String string3 = jSONObject2.getString("type");
                                    String string4 = jSONObject2.getString("subtype");
                                    String A032 = AbstractActivityC117815ak.A03(arrayList4, pinFragment, i14);
                                    String A003 = pinFragment.A02.A00();
                                    C128835wk A092 = AbstractActivityC117815ak.A09(pinFragment);
                                    pinFragment.A0p();
                                    AnonymousClass6F0 A004 = A092.A00(A032, string3, string4, A003, ((NPCIFragment) pinFragment).A09);
                                    if (A004 != null) {
                                        HashMap hashMap2 = pinFragment.A04;
                                        StringBuilder A0h2 = C12960it.A0h();
                                        C125155qn.A00(A004, A0h2);
                                        hashMap2.put(string4, A0h2.toString());
                                    }
                                } catch (JSONException e2) {
                                    throw C117315Zl.A0J(e2);
                                }
                            }
                            C117325Zm.A03(C12970iu.A0A(), pinFragment.A04, pinFragment);
                            return;
                        }
                        return;
                    }
                    i = R.string.npci_invalid_otp;
                    pinFragment.A1A(A0s, pinFragment.A0I(i));
                }
            }
        }
    }
}
