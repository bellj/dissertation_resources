package X;

import android.text.ParcelableSpan;

/* renamed from: X.4S7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4S7 {
    public int A00;
    public int A01;
    public final int A02;
    public final ParcelableSpan A03;

    public AnonymousClass4S7(ParcelableSpan parcelableSpan, int i, int i2, int i3) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A03 = parcelableSpan;
    }
}
