package X;

import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.whatsapp.Conversation;

/* renamed from: X.2bO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52612bO extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Drawable A01;
    public final /* synthetic */ ViewTreeObserver$OnGlobalLayoutListenerC66323Nd A02;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return false;
    }

    public C52612bO(Drawable drawable, ViewTreeObserver$OnGlobalLayoutListenerC66323Nd r2, int i) {
        this.A02 = r2;
        this.A00 = i;
        this.A01 = drawable;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i = this.A00;
        int i2 = i - ((int) (((float) i) * f));
        Conversation conversation = this.A02.A03;
        Drawable background = conversation.A0A.getBackground();
        if (!(background instanceof AnonymousClass2Zg)) {
            return;
        }
        if (f == 1.0f) {
            AnonymousClass2Zg.A00(this.A01, conversation.A0A);
            return;
        }
        AnonymousClass2Zg r1 = (AnonymousClass2Zg) background;
        r1.A00 = i2;
        r1.invalidateSelf();
    }
}
