package X;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/* renamed from: X.1tS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41391tS {
    public static final Pattern A00 = Pattern.compile("\r?\n");
    public static final Pattern A01 = Pattern.compile("(?<=\nEND:VCARD)\\s*\r?\n", 2);

    public static List A00(String str) {
        List asList = Arrays.asList(A01.split(str, 258));
        int i = 1;
        if (asList.size() <= 0 || !((String) asList.get(asList.size() - 1)).isEmpty()) {
            i = 0;
        }
        int min = Math.min(asList.size(), 257) - i;
        if (min < asList.size()) {
            return asList.subList(0, min);
        }
        return asList;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:90|(1:92)|368|97) */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x04df, code lost:
        throw new X.C41341tN("File ended during parsing BASE64 binary");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0080, code lost:
        r13.A0B += java.lang.System.currentTimeMillis() - r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x0590, code lost:
        if (r1 != ',') goto L_0x05bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:294:0x05df, code lost:
        if (r0 != false) goto L_0x0609;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x05e1, code lost:
        r5 = java.lang.System.currentTimeMillis();
        r2.A06.A02.add(r2.A05);
        r13.A00 += java.lang.System.currentTimeMillis() - r5;
        r4 = java.lang.System.currentTimeMillis();
        r2.A05 = new X.AnonymousClass3FO();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:0x079b, code lost:
        throw new X.C41341tN(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:365:0x07d5, code lost:
        throw new X.C41341tN(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x023f, code lost:
        if (r0 != null) goto L_0x0241;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0286, code lost:
        r6[1] = 0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x02e5  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0305  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0311  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0335  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x054d  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0555  */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x0598  */
    /* JADX WARNING: Removed duplicated region for block: B:296:0x0605 A[LOOP:4: B:41:0x010f->B:296:0x0605, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:397:0x05df A[EDGE_INSN: B:397:0x05df->B:294:0x05df ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x025a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(java.lang.String r30, X.C41401tT r31) {
        /*
        // Method dump skipped, instructions count: 2007
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41391tS.A01(java.lang.String, X.1tT):void");
    }
}
