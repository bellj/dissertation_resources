package X;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0F0  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0F0 extends AbstractC06220Sq {
    public AnonymousClass0F0(AnonymousClass02H r1) {
        super(r1);
    }

    @Override // X.AbstractC06220Sq
    public int A01() {
        return this.A02.A00;
    }

    @Override // X.AbstractC06220Sq
    public int A02() {
        AnonymousClass02H r0 = this.A02;
        return r0.A00 - r0.A08();
    }

    @Override // X.AbstractC06220Sq
    public int A03() {
        return this.A02.A08();
    }

    @Override // X.AbstractC06220Sq
    public int A04() {
        return this.A02.A01;
    }

    @Override // X.AbstractC06220Sq
    public int A05() {
        return this.A02.A04;
    }

    @Override // X.AbstractC06220Sq
    public int A06() {
        return this.A02.A0B();
    }

    @Override // X.AbstractC06220Sq
    public int A07() {
        AnonymousClass02H r2 = this.A02;
        return (r2.A00 - r2.A0B()) - r2.A08();
    }

    @Override // X.AbstractC06220Sq
    public int A08(View view) {
        return view.getBottom() + ((AnonymousClass0B6) view.getLayoutParams()).A03.bottom + ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).bottomMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A09(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        Rect rect = ((AnonymousClass0B6) view.getLayoutParams()).A03;
        return view.getMeasuredHeight() + rect.top + rect.bottom + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A0A(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        Rect rect = ((AnonymousClass0B6) view.getLayoutParams()).A03;
        return view.getMeasuredWidth() + rect.left + rect.right + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A0B(View view) {
        return (view.getTop() - ((AnonymousClass0B6) view.getLayoutParams()).A03.top) - ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin;
    }

    @Override // X.AbstractC06220Sq
    public int A0C(View view) {
        AnonymousClass02H r1 = this.A02;
        Rect rect = this.A01;
        r1.A0H(rect, view);
        return rect.bottom;
    }

    @Override // X.AbstractC06220Sq
    public int A0D(View view) {
        AnonymousClass02H r1 = this.A02;
        Rect rect = this.A01;
        r1.A0H(rect, view);
        return rect.top;
    }

    @Override // X.AbstractC06220Sq
    public void A0E(int i) {
        this.A02.A0m(i);
    }
}
