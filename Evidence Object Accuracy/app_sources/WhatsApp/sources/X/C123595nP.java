package X;

import com.whatsapp.util.Log;

/* renamed from: X.5nP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123595nP extends AbstractC118055bC {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final AnonymousClass016 A03 = C12980iv.A0T();
    public final C14900mE A04;
    public final C14850m9 A05;
    public final C129965ya A06;
    public final C129215xM A07;
    public final AnonymousClass60U A08;
    public final AbstractC14440lR A09;
    public final String A0A;
    public final String A0B;

    public C123595nP(C14900mE r8, C14830m7 r9, C14850m9 r10, C129965ya r11, C129215xM r12, AnonymousClass60T r13, AnonymousClass60U r14, AnonymousClass61E r15, AnonymousClass605 r16, C130015yf r17, AbstractC14440lR r18, String str, String str2) {
        super(r9, r13, r15, r16, r17);
        this.A05 = r10;
        this.A04 = r8;
        this.A09 = r18;
        this.A08 = r14;
        this.A0B = str;
        this.A0A = str2;
        this.A06 = r11;
        this.A07 = r12;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        Log.i("DyiViewModel/on-cleared");
        AnonymousClass60U r3 = this.A08;
        String str = this.A0A;
        if (r3.A02(str) == 3) {
            synchronized (r3) {
                C120035fV r0 = r3.A00;
                if (r0 != null) {
                    r0.A03();
                }
                r3.A0A.A0B(2, str);
            }
        }
    }

    public final void A07(int i) {
        if (!this.A04.A0M()) {
            Log.i("DyiViewModel/check-internet :: no internet connection aborting the action");
            C117305Zk.A1B(this.A03, Integer.valueOf(i), new C452120p(7));
        }
    }

    public final void A08(AnonymousClass6B7 r9, C129115xC r10, AnonymousClass1V8 r11, String str) {
        try {
            this.A06.A01(r10, r11, str, this.A0A, r9.A00.A9N(C117315Zl.A0a(this.A0B), C003501n.A0D(16)));
        } catch (Exception unused) {
            Log.e("DyiViewModel/request-report/sendDyiReportRequestWithPassword");
            r10.A00(C117305Zk.A0L());
        }
    }

    public void A09(C129115xC r9, AnonymousClass1V8 r10, String str) {
        Log.i("DyiViewModel/request-report");
        A07(0);
        AnonymousClass60U r1 = this.A08;
        long A00 = super.A04.A00();
        String str2 = this.A0A;
        synchronized (r1) {
            Log.i("dyiReportManager/on-report-requested");
            r1.A0A.A0J(str2, A00);
        }
        this.A02.A0A(C12960it.A0V());
        if (str2.equals("business")) {
            this.A06.A01(r9, null, str, "business", null);
        } else if (!str2.equals("personal")) {
            Log.e("PAY: DyiReportViewModel/requestReport - this account type is not supported");
        } else if (this.A05.A07(1214)) {
            AnonymousClass009.A05(r10);
            AnonymousClass6B7 A0C = C117315Zl.A0C(super.A05, "FB", "DYI-REPORT");
            if (A0C != null) {
                A08(A0C, r9, r10, str);
            } else {
                this.A07.A00(new C133466Aw(r9, this, r10, str), "FB");
            }
        } else {
            C129965ya r2 = this.A06;
            AnonymousClass009.A05(r10);
            r2.A01(r9, r10, str, "personal", null);
        }
    }
}
