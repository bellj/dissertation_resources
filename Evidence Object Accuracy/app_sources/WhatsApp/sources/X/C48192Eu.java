package X;

import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;

/* renamed from: X.2Eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48192Eu {
    public long A00;
    public C49322Kh A01;
    public C48182Et A02;
    public boolean A03;
    public final int A04;
    public final int A05;
    public final C22260yn A06;
    public final C14900mE A07;
    public final C15550nR A08;
    public final C22700zV A09;
    public final C253318z A0A;
    public final C18640sm A0B;
    public final C14830m7 A0C;
    public final C17170qN A0D;
    public final C22610zM A0E;
    public final C14850m9 A0F;
    public final C16120oU A0G;
    public final C17220qS A0H;
    public final C22410z2 A0I;
    public final AbstractC14440lR A0J;
    public final String A0K;
    public final WeakReference A0L;
    public final boolean A0M;
    public final boolean A0N;

    public C48192Eu(C22260yn r2, C14900mE r3, C15550nR r4, C22700zV r5, C253318z r6, C18640sm r7, C14830m7 r8, C17170qN r9, C22610zM r10, C14850m9 r11, C16120oU r12, C17220qS r13, C22410z2 r14, AnonymousClass2JY r15, AbstractC14440lR r16, String str, int i, int i2, boolean z) {
        this.A0C = r8;
        this.A0F = r11;
        this.A07 = r3;
        this.A0J = r16;
        this.A0G = r12;
        this.A0H = r13;
        this.A08 = r4;
        this.A06 = r2;
        this.A0A = r6;
        this.A05 = i;
        this.A0D = r9;
        this.A0I = r14;
        this.A0E = r10;
        this.A0K = str;
        this.A0M = z;
        this.A09 = r5;
        this.A0B = r7;
        this.A0L = new WeakReference(r15);
        this.A04 = i2;
        this.A0N = r15.A02;
    }

    public void A00() {
        this.A03 = true;
        C49322Kh r1 = this.A01;
        if (r1 != null) {
            r1.A03(false);
            this.A01 = null;
        }
        AnonymousClass2JY r12 = (AnonymousClass2JY) this.A0L.get();
        if (r12 != null) {
            r12.A0Z = false;
        }
    }

    public void A01(C42351v4 r8) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.A00;
        this.A07.A0J(new RunnableBRunnable0Shape11S0200000_I1_1(this, 44, r8), elapsedRealtime < 500 ? 500 - elapsedRealtime : 0);
    }

    public void A02(C48182Et r18) {
        int i;
        String str;
        String str2;
        if (!this.A03) {
            this.A02 = r18;
            UserJid userJid = r18.A02;
            int i2 = r18.A01;
            if (i2 == -1 || (i = this.A05) != i2 || userJid == null) {
                A01(null);
                return;
            }
            C14850m9 r3 = this.A0F;
            if (r3.A07(508)) {
                if (i == 2) {
                    str = "message_short_link";
                } else {
                    str = "qr_code";
                }
                if (r3.A07(1669) && i == 2) {
                    String str3 = this.A0K;
                    if (C22180yf.A03(r3, str3)) {
                        str = "custom_qr_code_link";
                    } else if (C22180yf.A02(r3, str3)) {
                        str = "custom_link";
                    }
                }
                if (this.A0M) {
                    str2 = "whatsapp";
                } else {
                    str2 = null;
                }
                this.A0E.A00(new C40511ri(new C40521rj(userJid, str, str2, System.currentTimeMillis(), System.currentTimeMillis())));
            }
            C49322Kh r2 = new C49322Kh(this.A06, this.A08, this.A0A, userJid, this.A0I, this, r18.A04);
            this.A01 = r2;
            this.A0J.Aaz(r2, new Void[0]);
        }
    }
}
