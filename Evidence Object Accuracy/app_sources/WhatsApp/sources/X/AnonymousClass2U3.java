package X;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;
import com.whatsapp.CircleWaImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import java.util.HashMap;

/* renamed from: X.2U3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2U3 {
    public static final HashMap A0G;
    public static final HashMap A0H;
    public int A00;
    public Animation A01;
    public Animation A02;
    public final View A03;
    public final View A04;
    public final View A05;
    public final TextView A06;
    public final TextView A07;
    public final CircleWaImageView A08;
    public final WaImageView A09;
    public final WaImageView A0A;
    public final WaImageView A0B;
    public final WaImageView A0C;
    public final AnonymousClass1s9 A0D;
    public final boolean A0E;
    public final boolean A0F;

    static {
        HashMap hashMap = new HashMap();
        A0H = hashMap;
        HashMap hashMap2 = new HashMap();
        A0G = hashMap2;
        Integer valueOf = Integer.valueOf((int) R.drawable.flash_off);
        hashMap.put("off", valueOf);
        hashMap.put("on", Integer.valueOf((int) R.drawable.flash_on));
        hashMap.put("auto", Integer.valueOf((int) R.drawable.flash_auto));
        hashMap.put("torch", valueOf);
        Integer valueOf2 = Integer.valueOf((int) R.string.flash_off);
        hashMap2.put("off", valueOf2);
        hashMap2.put("on", Integer.valueOf((int) R.string.flash_on));
        hashMap2.put("auto", Integer.valueOf((int) R.string.flash_auto));
        hashMap2.put("torch", valueOf2);
    }

    public AnonymousClass2U3(View view, AnonymousClass1s9 r24, boolean z) {
        this.A03 = view;
        this.A04 = AnonymousClass028.A0D(view, R.id.fake_flash);
        WaImageView waImageView = (WaImageView) AnonymousClass028.A0D(view, R.id.flash_btn);
        this.A0A = waImageView;
        WaImageView waImageView2 = (WaImageView) AnonymousClass028.A0D(view, R.id.switch_camera_btn);
        this.A0C = waImageView2;
        this.A0B = (WaImageView) AnonymousClass028.A0D(view, R.id.shutter);
        WaImageView waImageView3 = null;
        this.A08 = z ? (CircleWaImageView) AnonymousClass028.A0D(view, R.id.gallery_btn) : null;
        this.A09 = z ? (WaImageView) AnonymousClass028.A0D(view, R.id.close_camera_btn) : waImageView3;
        this.A07 = (TextView) AnonymousClass028.A0D(view, R.id.recording_hint);
        this.A05 = AnonymousClass028.A0D(view, R.id.select_multiple);
        this.A06 = (TextView) AnonymousClass028.A0D(view, R.id.selected_count);
        this.A0E = z;
        this.A0D = r24;
        boolean z2 = r24.getNumberOfCameras() > 1;
        this.A0F = z2;
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        this.A01 = scaleAnimation;
        scaleAnimation.setDuration(200);
        ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.A02 = scaleAnimation2;
        scaleAnimation2.setDuration(200);
        int i = 0;
        waImageView2.setVisibility(z2 ? 0 : 8);
        waImageView.setVisibility(r24.getStoredFlashModeCount() <= 1 ? 8 : i);
        AnonymousClass1s9 r4 = this.A0D;
        this.A0C.setContentDescription(this.A03.getContext().getResources().getString(r4.AJS() ? R.string.switch_to_back_camera : R.string.switch_to_front_camera));
        this.A0B.setEnabled(false);
        this.A0C.setEnabled(false);
        this.A0A.setEnabled(false);
        A02(r4.getFlashMode());
    }

    public void A00() {
        WaImageView waImageView = this.A0C;
        Animation animation = this.A02;
        waImageView.startAnimation(animation);
        int i = 8;
        waImageView.setVisibility(8);
        WaImageView waImageView2 = this.A0A;
        waImageView2.startAnimation(animation);
        waImageView2.setVisibility(8);
        CircleWaImageView circleWaImageView = this.A08;
        if (circleWaImageView != null) {
            circleWaImageView.startAnimation(animation);
            circleWaImageView.setVisibility(8);
        }
        WaImageView waImageView3 = this.A09;
        if (waImageView3 != null) {
            waImageView3.startAnimation(animation);
            waImageView3.setVisibility(8);
        }
        TextView textView = this.A07;
        if (this.A0E) {
            i = 4;
        }
        textView.setVisibility(i);
    }

    public final void A01() {
        AnonymousClass1s9 r3 = this.A0D;
        int size = r3.getFlashModes().size();
        WaImageView waImageView = this.A0A;
        if (size <= 1) {
            waImageView.setVisibility(8);
            return;
        }
        waImageView.setVisibility(0);
        A02(r3.getFlashMode());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0085, code lost:
        if (r0 == false) goto L_0x0087;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(java.lang.String r7) {
        /*
            r6 = this;
            java.util.HashMap r0 = X.AnonymousClass2U3.A0H
            java.lang.Object r0 = r0.get(r7)
            java.lang.Number r0 = (java.lang.Number) r0
            int r3 = r0.intValue()
            java.util.HashMap r0 = X.AnonymousClass2U3.A0G
            java.lang.Object r0 = r0.get(r7)
            java.lang.Number r0 = (java.lang.Number) r0
            int r5 = r0.intValue()
            X.1s9 r0 = r6.A0D
            java.util.List r2 = r0.getFlashModes()
            boolean r0 = r2.isEmpty()
            if (r0 == 0) goto L_0x008b
            r1 = 0
        L_0x0025:
            java.lang.String r0 = "off"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0087
            java.lang.String r0 = "on"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x007c
            r1 = 2131888315(0x7f1208bb, float:1.9411262E38)
        L_0x0038:
            com.whatsapp.WaImageView r4 = r6.A0A
            X.AnonymousClass23N.A02(r4, r1)
            android.view.View r0 = r6.A03
            android.content.Context r2 = r0.getContext()
            java.lang.String r0 = r2.getString(r5)
            r4.setContentDescription(r0)
            int r0 = r6.A00
            if (r0 == r3) goto L_0x0078
            if (r0 == 0) goto L_0x0078
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r2, r0)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass00T.A04(r2, r3)
            X.2Zh r2 = new X.2Zh
            r2.<init>(r1, r0)
            int r1 = r2.getIntrinsicHeight()
            int r0 = r4.getPaddingTop()
            int r1 = r1 + r0
            r0 = 120(0x78, float:1.68E-43)
            r2.A00 = r0
            r2.A01 = r1
            r0 = 0
            r2.A02 = r0
            r2.invalidateSelf()
            r4.setImageDrawable(r2)
        L_0x0075:
            r6.A00 = r3
            return
        L_0x0078:
            r4.setImageResource(r3)
            goto L_0x0075
        L_0x007c:
            java.lang.String r0 = "auto"
            boolean r0 = r0.equals(r1)
            r1 = 2131888310(0x7f1208b6, float:1.9411252E38)
            if (r0 != 0) goto L_0x0038
        L_0x0087:
            r1 = 2131888313(0x7f1208b9, float:1.9411258E38)
            goto L_0x0038
        L_0x008b:
            int r0 = r2.indexOf(r7)
            int r1 = r0 + 1
            int r0 = r2.size()
            int r1 = r1 % r0
            java.lang.Object r1 = r2.get(r1)
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2U3.A02(java.lang.String):void");
    }

    public void A03(boolean z, int i) {
        View view = this.A05;
        int visibility = view.getVisibility();
        if (z) {
            if (visibility != 0) {
                view.setVisibility(0);
                view.startAnimation(this.A01);
            }
            TextView textView = this.A06;
            textView.setText(String.valueOf(i));
            textView.setContentDescription(textView.getResources().getQuantityString(R.plurals.n_items_selected, i, Integer.valueOf(i)));
        } else if (visibility != 8) {
            view.setVisibility(8);
            view.startAnimation(this.A02);
        }
    }
}
