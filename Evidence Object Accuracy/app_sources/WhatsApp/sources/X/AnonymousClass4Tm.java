package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4Tm  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Tm {
    public final int A00;
    public final int A01;
    public final int A02 = 3;
    public final int A03;
    public final AnonymousClass1VY A04;
    public final UserJid A05;
    public final String A06;
    public final String A07;

    public AnonymousClass4Tm(AnonymousClass1VY r2, UserJid userJid, String str, String str2, int i, int i2, int i3) {
        this.A05 = userJid;
        this.A04 = r2;
        this.A06 = str2;
        this.A00 = i3;
        this.A03 = i;
        this.A01 = i2;
        this.A07 = str;
    }
}
