package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1Ih  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27631Ih extends UserJid implements Parcelable {
    public static final UserJid A00;
    public static final Parcelable.Creator CREATOR = new AnonymousClass27E();

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getServer() {
        return "s.whatsapp.net";
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public int getType() {
        return 0;
    }

    static {
        try {
            A00 = A02("16505361212");
        } catch (AnonymousClass1MW e) {
            throw new IllegalStateException(e);
        }
    }

    public C27631Ih(Parcel parcel) {
        super(parcel);
    }

    public C27631Ih(String str) {
        super(str);
        if (!A03(str)) {
            StringBuilder sb = new StringBuilder("Invalid user: ");
            sb.append(str);
            throw new AnonymousClass1MW(sb.toString());
        }
    }

    public static C27631Ih A02(String str) {
        UserJid A02 = UserJid.JID_FACTORY.A02(str, "s.whatsapp.net");
        if (A02 instanceof C27631Ih) {
            return (C27631Ih) A02;
        }
        StringBuilder sb = new StringBuilder("invalid phone number: ");
        sb.append(str);
        throw new AnonymousClass1MW(sb.toString());
    }

    public static boolean A03(String str) {
        int length = str.length();
        if (length >= 5 && length <= 20 && !str.startsWith("10") && !str.startsWith("0")) {
            char[] charArray = str.toCharArray();
            for (char c : charArray) {
                if (c >= '0' && c <= '9') {
                }
            }
            return true;
        }
        return false;
    }
}
