package X;

import android.content.Context;
import android.content.Intent;
import android.view.View;

/* renamed from: X.2oL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58222oL extends AbstractC52172aN {
    public final /* synthetic */ C64433Fn A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58222oL(Context context, C64433Fn r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        ActivityC000800j r4 = this.A00.A04;
        Context applicationContext = r4.getApplicationContext();
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(applicationContext.getPackageName(), "com.whatsapp.settings.SettingsPrivacy");
        A0A.putExtra("target_setting", "privacy_groupadd");
        r4.startActivity(A0A);
    }
}
