package X;

import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;

/* renamed from: X.0KJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KJ {
    public static void A00(Drawable drawable) {
        if (drawable instanceof AnimatedVectorDrawable) {
            ((AnimatedVectorDrawable) drawable).start();
        }
    }
}
