package X;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.text.TextUtils;
import androidx.core.app.NotificationCompat$BigTextStyle;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.0vg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20390vg {
    public final C16590pI A00;
    public final C18360sK A01;
    public final AnonymousClass018 A02;
    public final C16490p7 A03;
    public final C21390xL A04;
    public final C241414j A05;
    public final C22710zW A06;
    public final C17070qD A07;
    public final C30931Zj A08 = C30931Zj.A00("PaymentMethodUpdateNotification", "notification", "COMMON");
    public final C15860o1 A09;
    public final AbstractC14440lR A0A;

    public C20390vg(C16590pI r4, C18360sK r5, AnonymousClass018 r6, C16490p7 r7, C21390xL r8, C241414j r9, C22710zW r10, C17070qD r11, C15860o1 r12, AbstractC14440lR r13) {
        this.A00 = r4;
        this.A0A = r13;
        this.A05 = r9;
        this.A02 = r6;
        this.A07 = r11;
        this.A09 = r12;
        this.A04 = r8;
        this.A03 = r7;
        this.A06 = r10;
        this.A01 = r5;
    }

    public void A00() {
        C21390xL r2 = this.A04;
        if (!TextUtils.isEmpty(r2.A02("unread_payment_method_credential_ids"))) {
            r2.A06("unread_payment_method_credential_ids", "");
            this.A01.A04(22, null);
        }
    }

    public void A01() {
        this.A0A.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(this, 15));
    }

    public final void A02() {
        C21390xL r1;
        ArrayList arrayList;
        C30931Zj r12;
        String str;
        String str2;
        if (this.A06.A03()) {
            C16490p7 r0 = this.A03;
            r0.A04();
            if (!r0.A01) {
                r12 = this.A08;
                str = "message store not yet ready";
            } else {
                synchronized (this) {
                    r1 = this.A04;
                    String A02 = r1.A02("unread_payment_method_credential_ids");
                    if (TextUtils.isEmpty(A02)) {
                        arrayList = new ArrayList();
                    } else {
                        List<String> asList = Arrays.asList(TextUtils.split(A02, ";"));
                        ArrayList arrayList2 = new ArrayList();
                        for (String str3 : asList) {
                            arrayList2.add(TextUtils.split(str3, ":")[0]);
                        }
                        if (!TextUtils.isEmpty(A02)) {
                            C241414j r4 = this.A05;
                            if (r4.A07) {
                                StringBuilder sb = new StringBuilder("credential_id IN (\"");
                                sb.append(TextUtils.join("\",\"", arrayList2));
                                sb.append("\")");
                                String obj = sb.toString();
                                C16310on A01 = r4.A00.get();
                                Cursor A08 = A01.A03.A08("methods", obj, null, "100", AnonymousClass1ZV.A00, null);
                                if (A08 != null) {
                                    try {
                                        arrayList = new ArrayList(A08.getCount());
                                        while (A08.moveToNext()) {
                                            arrayList.add(r4.A07(A08));
                                        }
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("PAY: PaymentTransactionStore readPaymentMethodByCredentialIds returned: ");
                                        sb2.append(arrayList.size());
                                        Log.i(sb2.toString());
                                        A08.close();
                                        A01.close();
                                    } catch (Throwable th) {
                                        try {
                                            A08.close();
                                        } catch (Throwable unused) {
                                        }
                                        throw th;
                                    }
                                } else {
                                    A01.close();
                                    arrayList = new ArrayList();
                                }
                            }
                        }
                        arrayList = new ArrayList();
                    }
                }
                Context context = this.A00.A00;
                AnonymousClass24Y AFQ = this.A07.A02().AFQ();
                if (arrayList.isEmpty() || AFQ == null) {
                    this.A01.A04(22, null);
                    r12 = this.A08;
                    str = "no unread payment notifications";
                } else {
                    C005602s A00 = C22630zO.A00(context);
                    A00.A0I = "status";
                    A00.A03 = 1;
                    A00.A0D(true);
                    A00.A02(4);
                    C18360sK.A01(A00, R.drawable.notifybar);
                    if (arrayList.size() == 1) {
                        AbstractC28901Pl r42 = (AbstractC28901Pl) arrayList.get(0);
                        String str4 = r42.A0A;
                        String A022 = r1.A02("unread_payment_method_credential_ids");
                        if (!TextUtils.isEmpty(A022)) {
                            for (String str5 : Arrays.asList(TextUtils.split(A022, ";"))) {
                                String[] split = TextUtils.split(str5, ":");
                                if (TextUtils.equals(split[0], str4)) {
                                    str2 = split[1];
                                    break;
                                }
                            }
                        }
                        str2 = null;
                        String A03 = AFQ.A03(r42, str2);
                        String A023 = AFQ.A02(r42, str2);
                        if (TextUtils.isEmpty(A03)) {
                            this.A08.A06("no available payment notification text");
                            A03(r42.A0A);
                            return;
                        }
                        A00.A0B(A03);
                        A00.A09(A03);
                        NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
                        notificationCompat$BigTextStyle.A09(A03);
                        A00.A08(notificationCompat$BigTextStyle);
                        A00.A09 = AFQ.A00(context, r42, str2);
                        if (!TextUtils.isEmpty(A023)) {
                            A00.A04(R.drawable.ic_fab_check, A023, AFQ.A00(context, r42, str2));
                        }
                    } else {
                        int size = arrayList.size();
                        A00.A09(this.A02.A0I(new Object[]{Integer.valueOf(size)}, R.plurals.notification_new_payment_method_update, (long) size));
                        A00.A09 = AFQ.A00(context, null, null);
                    }
                    int i = Build.VERSION.SDK_INT;
                    if (i >= 21) {
                        C005602s A002 = C22630zO.A00(context);
                        A002.A0I = "status";
                        A002.A03 = 1;
                        A002.A09(this.A02.A0I(new Object[]{Integer.valueOf(arrayList.size())}, R.plurals.notification_new_payment_method_update, (long) arrayList.size()));
                        A002.A04(R.drawable.ic_fab_check, AFQ.A02(null, null), AFQ.A00(context, null, null));
                        A002.A08 = A002.A01();
                        C18360sK.A01(A002, R.drawable.notifybar);
                    }
                    A00.A07.deleteIntent = AnonymousClass1UY.A01(context, 22, new Intent(context, AnonymousClass24Z.class), 134217728);
                    if (i >= 26) {
                        A00.A0J = ((C33271dj) this.A09.A05()).A0C();
                    }
                    Notification A012 = A00.A01();
                    try {
                        this.A08.A06("NotificationManager/notify");
                        this.A01.A03(22, A012);
                        return;
                    } catch (SecurityException e) {
                        if (!C38241nl.A0B(e.toString())) {
                            throw e;
                        }
                        return;
                    }
                }
            }
            r12.A06(str);
        }
    }

    public final synchronized void A03(String str) {
        if (TextUtils.isEmpty(str)) {
            this.A08.A05("removeUnreadPaymentMethodUpdate empty credentialId");
        } else {
            C21390xL r8 = this.A04;
            String A02 = r8.A02("unread_payment_method_credential_ids");
            if (A02 == null) {
                A02 = "";
            }
            String[] split = TextUtils.split(A02, ";");
            HashSet hashSet = new HashSet();
            for (String str2 : split) {
                if (!TextUtils.equals(TextUtils.split(str2, ":")[0], str)) {
                    hashSet.add(str2);
                } else {
                    C30931Zj r2 = this.A08;
                    StringBuilder sb = new StringBuilder();
                    sb.append("removeUnreadPaymentMethodUpdate/removed credentialId:");
                    sb.append(str);
                    r2.A06(sb.toString());
                }
            }
            r8.A06("unread_payment_method_credential_ids", TextUtils.join(";", hashSet));
        }
    }
}
