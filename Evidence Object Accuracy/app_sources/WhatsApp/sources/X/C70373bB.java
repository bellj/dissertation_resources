package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

/* renamed from: X.3bB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70373bB implements AbstractC41521tf {
    public final /* synthetic */ ImageView A00;
    public final /* synthetic */ AnonymousClass19P A01;
    public final /* synthetic */ AbstractC15340mz A02;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70373bB(ImageView imageView, AnonymousClass19P r2, AbstractC15340mz r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = imageView;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A01.A0I.A03(this.A00.getContext());
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        if (bitmap != null && bitmap.getWidth() > 0 && bitmap.getHeight() > 0) {
            AbstractC15340mz r1 = this.A02;
            if (!(r1 instanceof C30341Xa) && !(r1 instanceof C30061Vy)) {
                ImageView imageView = this.A00;
                imageView.setVisibility(0);
                imageView.setImageBitmap(bitmap);
                return;
            }
        }
        this.A00.setVisibility(8);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C12990iw.A1D(this.A00);
    }
}
