package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1I4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1I4 {
    public static boolean addAll(Collection collection, Iterator it) {
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    public static boolean any(Iterator it, AnonymousClass28S r2) {
        return indexOf(it, r2) != -1;
    }

    public static void clear(Iterator it) {
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    public static Iterator concat(Iterator it) {
        return new AnonymousClass28T(it);
    }

    public static Iterator concat(Iterator it, Iterator it2) {
        return concat(consumingForArray(it, it2));
    }

    public static Iterator consumingForArray(Iterator... itArr) {
        return new AnonymousClass28U(itArr);
    }

    public static boolean elementsEqual(Iterator it, Iterator it2) {
        do {
            boolean hasNext = it.hasNext();
            boolean hasNext2 = it2.hasNext();
            if (hasNext) {
                if (!hasNext2) {
                    break;
                }
            } else {
                return !hasNext2;
            }
        } while (AnonymousClass28V.A00(it.next(), it2.next()));
        return false;
    }

    public static AnonymousClass1I5 emptyIterator() {
        return emptyListIterator();
    }

    public static AnonymousClass28W emptyListIterator() {
        return AnonymousClass28X.EMPTY;
    }

    public static Iterator emptyModifiableIterator() {
        return AnonymousClass28Z.INSTANCE;
    }

    public static AnonymousClass1I5 filter(Iterator it, AnonymousClass28S r2) {
        return new C468728a(it, r2);
    }

    public static Object getLast(Iterator it) {
        Object next;
        do {
            next = it.next();
        } while (it.hasNext());
        return next;
    }

    public static Object getNext(Iterator it, Object obj) {
        return it.hasNext() ? it.next() : obj;
    }

    public static int indexOf(Iterator it, AnonymousClass28S r3) {
        C28291Mn.A04(r3, "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (r3.A66(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static Object pollNext(Iterator it) {
        if (!it.hasNext()) {
            return null;
        }
        Object next = it.next();
        it.remove();
        return next;
    }

    public static boolean removeAll(Iterator it, Collection collection) {
        boolean z = false;
        while (it.hasNext()) {
            if (collection.contains(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    public static boolean removeIf(Iterator it, AnonymousClass28S r3) {
        boolean z = false;
        while (it.hasNext()) {
            if (r3.A66(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    public static AnonymousClass1I5 singletonIterator(Object obj) {
        return new C468928c(obj);
    }

    public static String toString(Iterator it) {
        StringBuilder sb = new StringBuilder("[");
        boolean z = true;
        while (it.hasNext()) {
            if (!z) {
                sb.append(", ");
            }
            z = false;
            sb.append(it.next());
        }
        sb.append(']');
        return sb.toString();
    }

    public static Iterator transform(Iterator it, AbstractC469028d r2) {
        return new C469128e(it, r2);
    }

    public static AnonymousClass1I5 unmodifiableIterator(Iterator it) {
        return new C469328g(it);
    }
}
