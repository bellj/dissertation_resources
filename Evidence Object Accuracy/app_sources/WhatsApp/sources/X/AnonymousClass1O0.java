package X;

import android.content.Context;
import java.util.regex.Pattern;

/* renamed from: X.1O0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1O0 {
    public static final Pattern A01 = Pattern.compile("dump\\.hprof.*");
    public final Context A00;

    public AnonymousClass1O0(Context context) {
        this.A00 = context;
    }
}
