package X;

import android.content.res.Resources;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.WaTextView;
import com.whatsapp.polls.PollCreatorViewModel;
import java.util.List;

/* renamed from: X.349  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass349 extends AbstractC75183jR {
    public int A00 = -1;
    public final WaEditText A01;
    public final WaTextView A02;
    public final WaTextView A03;

    public AnonymousClass349(View view, C14850m9 r5, PollCreatorViewModel pollCreatorViewModel) {
        super(view);
        this.A03 = C12960it.A0N(view, R.id.poll_option_label);
        this.A02 = C12960it.A0N(view, R.id.duplicated_option);
        WaEditText waEditText = (WaEditText) AnonymousClass028.A0D(view, R.id.poll_option_edit_text);
        this.A01 = waEditText;
        waEditText.setOnFocusChangeListener(new View.OnFocusChangeListener(pollCreatorViewModel) { // from class: X.3Ma
            public final /* synthetic */ PollCreatorViewModel A01;

            {
                this.A01 = r2;
            }

            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view2, boolean z) {
                String trim;
                AnonymousClass349 r52 = AnonymousClass349.this;
                PollCreatorViewModel pollCreatorViewModel2 = this.A01;
                if (!z && (view2 instanceof WaEditText) && r52.A00() != -1) {
                    EditText editText = (EditText) view2;
                    if (editText.getText() == null) {
                        trim = "";
                    } else {
                        trim = C12970iu.A0p(editText).trim();
                    }
                    int A00 = r52.A00() - 1;
                    if (trim.length() > 0) {
                        pollCreatorViewModel2.A05(trim, A00);
                    } else {
                        List list = pollCreatorViewModel2.A0F;
                        if (list.size() > 2 && C12980iv.A0C(list) != A00) {
                            list.remove(A00);
                            pollCreatorViewModel2.A04();
                        }
                    }
                }
                WaTextView waTextView = r52.A03;
                Resources resources = waTextView.getResources();
                int i = R.color.secondary_text;
                if (z) {
                    i = R.color.action_text;
                }
                C12980iv.A14(resources, waTextView, i);
            }
        });
        C12990iw.A1I(waEditText, new InputFilter[1], r5.A02(1407));
        waEditText.addTextChangedListener(new AnonymousClass365(this, pollCreatorViewModel));
    }
}
