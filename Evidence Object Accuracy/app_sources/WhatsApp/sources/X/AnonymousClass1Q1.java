package X;

/* renamed from: X.1Q1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Q1 {
    public static final AnonymousClass1Q1 A02;
    public static final AbstractC28621Oh[] A03;
    public final AbstractC28621Oh[] A00;
    public final AbstractC28621Oh[] A01;

    static {
        AbstractC28621Oh[] r1 = new AbstractC28621Oh[0];
        A03 = r1;
        A02 = new AnonymousClass1Q1(r1, r1);
    }

    public AnonymousClass1Q1(AbstractC28621Oh[] r1, AbstractC28621Oh[] r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
