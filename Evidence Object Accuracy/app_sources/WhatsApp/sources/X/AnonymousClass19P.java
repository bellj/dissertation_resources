package X;

import java.util.List;

/* renamed from: X.19P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19P {
    public final AnonymousClass11L A00;
    public final C14650lo A01;
    public final AnonymousClass19N A02;
    public final AnonymousClass130 A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final AnonymousClass01d A06;
    public final C16590pI A07;
    public final AnonymousClass018 A08;
    public final C15600nX A09;
    public final AnonymousClass19M A0A;
    public final C14850m9 A0B;
    public final C20710wC A0C;
    public final AnonymousClass13H A0D;
    public final C22460z7 A0E;
    public final AnonymousClass14X A0F;
    public final C16630pM A0G;
    public final AnonymousClass12F A0H;
    public final AnonymousClass19O A0I;

    public AnonymousClass19P(AnonymousClass11L r2, C14650lo r3, AnonymousClass19N r4, AnonymousClass130 r5, C15550nR r6, C15610nY r7, AnonymousClass01d r8, C16590pI r9, AnonymousClass018 r10, C15600nX r11, AnonymousClass19M r12, C14850m9 r13, C20710wC r14, AnonymousClass13H r15, C22460z7 r16, AnonymousClass14X r17, C16630pM r18, AnonymousClass12F r19, AnonymousClass19O r20) {
        this.A07 = r9;
        this.A0B = r13;
        this.A0D = r15;
        this.A0A = r12;
        this.A02 = r4;
        this.A0F = r17;
        this.A03 = r5;
        this.A04 = r6;
        this.A06 = r8;
        this.A05 = r7;
        this.A08 = r10;
        this.A0C = r14;
        this.A0H = r19;
        this.A0I = r20;
        this.A01 = r3;
        this.A09 = r11;
        this.A0G = r18;
        this.A00 = r2;
        this.A0E = r16;
    }

    public final CharSequence A00(CharSequence charSequence, List list) {
        return this.A0D.A00(C42971wC.A03(this.A06, this.A0G, charSequence), list);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0138, code lost:
        if (r15.equals(r31.A07.A00.getString(com.whatsapp.R.string.group_subject_unknown)) != false) goto L_0x013a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:339:0x097e  */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x0996  */
    /* JADX WARNING: Removed duplicated region for block: B:344:0x099e  */
    /* JADX WARNING: Removed duplicated region for block: B:364:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01f2  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x023b  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0251  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0259  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0261  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.view.View r32, X.AnonymousClass1J1 r33, X.AbstractC14640lm r34, X.AbstractC15340mz r35, X.AnonymousClass1AB r36, boolean r37) {
        /*
        // Method dump skipped, instructions count: 2679
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19P.A01(android.view.View, X.1J1, X.0lm, X.0mz, X.1AB, boolean):void");
    }
}
