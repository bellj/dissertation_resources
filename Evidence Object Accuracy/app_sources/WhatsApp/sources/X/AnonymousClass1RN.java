package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import org.chromium.net.UrlRequest;

/* renamed from: X.1RN  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1RN {
    public final int A00;
    public final File A01;
    public final String A02;
    public final boolean A03;

    public static String A00(int i) {
        switch (i) {
            case 0:
                return "success";
            case 1:
                return "failed_generic";
            case 2:
                return "dns_failure";
            case 3:
                return "timeout";
            case 4:
                return "disk_full";
            case 5:
                return "too_old";
            case 6:
                return "resume_failed";
            case 7:
                return "hash_mismatch";
            case 8:
                return "invalid_url";
            case 9:
                return "output_stream_fail";
            case 10:
            default:
                return "invalid";
            case 11:
                return "no_route";
            case 12:
                return "throttle";
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return "user_cancel";
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return "prefetch_end";
            case 15:
                return "watls_error";
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return "server_error";
            case 17:
                return "wamsys_failed";
            case 18:
                return "failed_network";
            case 19:
                return "failed_connect";
        }
    }

    public static boolean A01(int i) {
        return i == 1 || i == 2 || i == 3 || i == 11 || i == 15 || i == 16 || i == 18 || i == 19;
    }

    public AnonymousClass1RN(int i) {
        this(i, null, false);
    }

    public AnonymousClass1RN(int i, String str, boolean z) {
        this(null, str, i, z);
    }

    public AnonymousClass1RN(File file, String str, int i, boolean z) {
        this.A00 = i;
        this.A02 = str;
        this.A03 = z;
        this.A01 = file;
    }

    public boolean A02() {
        return this.A00 == 0;
    }

    public String toString() {
        return A00(this.A00);
    }
}
