package X;

import android.content.ClipData;
import android.view.ContentInfo;

/* renamed from: X.0Y1  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Y1 implements AbstractC12630iE {
    public final ContentInfo A00;

    public AnonymousClass0Y1(ContentInfo contentInfo) {
        this.A00 = contentInfo;
    }

    @Override // X.AbstractC12630iE
    public ClipData ABQ() {
        return this.A00.getClip();
    }

    @Override // X.AbstractC12630iE
    public int AD4() {
        return this.A00.getFlags();
    }

    @Override // X.AbstractC12630iE
    public int AGp() {
        return this.A00.getSource();
    }

    @Override // X.AbstractC12630iE
    public ContentInfo AHo() {
        return this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ContentInfoCompat{");
        sb.append(this.A00);
        sb.append("}");
        return sb.toString();
    }
}
