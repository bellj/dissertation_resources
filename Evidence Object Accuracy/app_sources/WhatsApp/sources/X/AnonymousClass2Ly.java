package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.2Ly  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2Ly extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2Ly A07;
    public static volatile AnonymousClass255 A08;
    public int A00;
    public int A01 = 0;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public Object A03;
    public String A04 = "";
    public String A05 = "";
    public String A06 = "";

    static {
        AnonymousClass2Ly r0 = new AnonymousClass2Ly();
        A07 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a9, code lost:
        if (r15.A01 == 1) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bc, code lost:
        if (r15.A01 == 3) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c1, code lost:
        if (r15.A01 == 4) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c6, code lost:
        if (r15.A01 == 5) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00c8, code lost:
        r9 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00c9, code lost:
        r0 = r8.Afv(r15.A03, r7.A03, r9);
     */
    @Override // X.AbstractC27091Fz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A0V(X.AnonymousClass25B r16, java.lang.Object r17, java.lang.Object r18) {
        /*
        // Method dump skipped, instructions count: 636
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Ly.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public EnumC630139s A0b() {
        int i = this.A01;
        if (i == 0) {
            return EnumC630139s.A05;
        }
        if (i == 1) {
            return EnumC630139s.A01;
        }
        if (i == 2) {
            return EnumC630139s.A02;
        }
        if (i == 3) {
            return EnumC630139s.A03;
        }
        if (i == 4) {
            return EnumC630139s.A06;
        }
        if (i != 5) {
            return null;
        }
        return EnumC630139s.A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if (this.A01 == 1) {
            i = CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 1) + 0;
        } else {
            i = 0;
        }
        if (this.A01 == 2) {
            i += CodedOutputStream.A07(2, (String) this.A03);
        }
        if (this.A01 == 3) {
            i += CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 3);
        }
        if (this.A01 == 4) {
            i += CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 4);
        }
        if (this.A01 == 5) {
            i += CodedOutputStream.A0A((AnonymousClass1G0) this.A03, 5);
        }
        if ((this.A00 & 32) == 32) {
            i += CodedOutputStream.A07(6, this.A04);
        }
        if ((this.A00 & 64) == 64) {
            i += CodedOutputStream.A07(7, this.A05);
        }
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A02.get(i3), 8);
        }
        if ((this.A00 & 128) == 128) {
            i += CodedOutputStream.A07(9, this.A06);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if (this.A01 == 1) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 1);
        }
        if (this.A01 == 2) {
            codedOutputStream.A0I(2, (String) this.A03);
        }
        if (this.A01 == 3) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 3);
        }
        if (this.A01 == 4) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 4);
        }
        if (this.A01 == 5) {
            codedOutputStream.A0L((AnonymousClass1G0) this.A03, 5);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0I(6, this.A04);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A05);
        }
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A02.get(i), 8);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(9, this.A06);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
