package X;

import java.util.Arrays;

/* renamed from: X.1xt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43941xt {
    public final int A00;
    public final int A01;
    public final long A02;

    public C43941xt(int i, long j, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = j;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C43941xt)) {
            return false;
        }
        C43941xt r7 = (C43941xt) obj;
        if (this.A00 == r7.A00 && this.A01 == r7.A01 && this.A02 == r7.A02) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.A00), Integer.valueOf(this.A01), Long.valueOf(this.A02)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FutureStageTiming{stage=");
        sb.append(this.A00);
        sb.append(", type=");
        sb.append(this.A01);
        sb.append(", timeInMillis=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
