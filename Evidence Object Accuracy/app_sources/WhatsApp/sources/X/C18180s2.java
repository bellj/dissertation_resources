package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import java.util.ArrayList;

/* renamed from: X.0s2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18180s2 implements AbstractC16890pv {
    public final C14900mE A00;
    public final C14850m9 A01;
    public final AnonymousClass01H A02;
    public final AnonymousClass01H A03;
    public final AnonymousClass01H A04;

    public C18180s2(C14900mE r2, C14850m9 r3, AnonymousClass01H r4, AnonymousClass01H r5, AnonymousClass01H r6) {
        C16700pc.A0E(r3, 1);
        C16700pc.A0E(r2, 2);
        C16700pc.A0E(r4, 3);
        C16700pc.A0E(r5, 4);
        C16700pc.A0E(r6, 5);
        this.A01 = r3;
        this.A00 = r2;
        this.A04 = r4;
        this.A03 = r5;
        this.A02 = r6;
    }

    @Override // X.AbstractC16890pv
    public void AMJ() {
        if (!(!this.A01.A07(1396)) && ((AnonymousClass12V) this.A02.get()).A01() && ((C235512c) this.A04.get()).A03("meta-avatar") == null) {
            this.A00.A0H(new RunnableBRunnable0Shape0S0200000_I0(this, 37, ((C235612d) this.A03.get()).A00(new ArrayList())));
        }
    }
}
