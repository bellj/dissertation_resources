package X;

import android.util.Base64;

/* renamed from: X.3ZA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZA implements AbstractC21730xt {
    public final /* synthetic */ C251518h A00;
    public final /* synthetic */ AnonymousClass3BI A01;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r1, String str) {
    }

    public AnonymousClass3ZA(C251518h r1, AnonymousClass3BI r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        AnonymousClass3BI r2 = this.A01;
        C251518h r4 = this.A00;
        AnonymousClass3E7 r0 = new AnonymousClass3EN(r4.A00, r6, r2).A02;
        if (r0 != null) {
            C14820m6 r3 = r4.A02;
            C12970iu.A1D(C12960it.A08(r3), "pref_client_auth_token", Base64.encodeToString(r0.A01, 3));
            if (r4.A03.A07(1689)) {
                r4.A01.A00();
            }
        }
    }
}
