package X;

import com.whatsapp.util.Log;

/* renamed from: X.68F  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68F implements AnonymousClass2DH {
    public final /* synthetic */ C119865fE A00;

    public AnonymousClass68F(C119865fE r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
        Log.e("PAY:PaymentErrorMapAssetManager/triggerBackgroundFetch/onAbort");
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        Log.e("PAY:PaymentErrorMapAssetManager/triggerBackgroundFetch/onError");
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        C119865fE r5 = this.A00;
        C18600si r4 = r5.A04;
        C12970iu.A1C(C117295Zj.A05(r4), "payments_error_map_last_sync_time_millis", r4.A01.A00());
        StringBuilder A0j = C12960it.A0j(r5.A03.ABp());
        A0j.append("_");
        A0j.append(r5.A01.A06());
        A0j.append("_");
        C12970iu.A1D(C117295Zj.A05(r4), "error_map_key", C12960it.A0d("1", A0j));
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        Log.e("PAY:PaymentErrorMapAssetManager/triggerBackgroundFetch/onTimeOut");
    }
}
