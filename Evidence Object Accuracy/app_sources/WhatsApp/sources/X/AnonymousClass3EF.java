package X;

/* renamed from: X.3EF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EF {
    public final AnonymousClass1V8 A00;
    public final String A01;
    public final String A02;

    public AnonymousClass3EF(AnonymousClass1V8 r11) {
        AnonymousClass1V8.A01(r11, "fds");
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        this.A02 = (String) AnonymousClass3JT.A03(null, r11, String.class, A0j, A0k, null, new String[]{"state"}, false);
        this.A01 = (String) AnonymousClass3JT.A03(null, r11, String.class, A0j, A0k, null, new String[]{"params"}, false);
        this.A00 = r11;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EF.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EF r5 = (AnonymousClass3EF) obj;
            if (!C29941Vi.A00(this.A02, r5.A02) || !C29941Vi.A00(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A02;
        return C12960it.A06(this.A01, A1a);
    }
}
