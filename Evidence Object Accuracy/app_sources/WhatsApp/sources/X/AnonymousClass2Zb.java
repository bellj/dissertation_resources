package X;

import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import com.facebook.drawee.drawable.DrawableProperties;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;

/* renamed from: X.2Zb  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Zb extends Drawable implements Animatable {
    public static final C87744Ct A0F = new C87744Ct();
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public DrawableProperties A08;
    public AbstractC117045Ya A09;
    public AnonymousClass4VC A0A;
    public final Runnable A0B;
    public volatile AnonymousClass5Pj A0C;
    public volatile C87744Ct A0D;
    public volatile boolean A0E;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public AnonymousClass2Zb() {
        this(null);
    }

    public AnonymousClass2Zb(AbstractC117045Ya r3) {
        this.A03 = 8;
        this.A0D = A0F;
        this.A0B = new RunnableBRunnable0Shape14S0100000_I1(this, 1);
        this.A09 = r3;
        this.A0A = new AnonymousClass4VC(r3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a5  */
    @Override // android.graphics.drawable.Drawable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r20) {
        /*
        // Method dump skipped, instructions count: 243
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Zb.draw(android.graphics.Canvas):void");
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        AbstractC117045Ya r0 = this.A09;
        if (r0 == null) {
            return super.getIntrinsicHeight();
        }
        return r0.ADX();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        AbstractC117045Ya r0 = this.A09;
        if (r0 == null) {
            return super.getIntrinsicWidth();
        }
        return r0.ADY();
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.A0E;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        AbstractC117045Ya r0 = this.A09;
        if (r0 != null) {
            r0.Abp(rect);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        if (!this.A0E) {
            long j = (long) i;
            if (this.A04 != j) {
                this.A04 = j;
                invalidateSelf();
                return true;
            }
        }
        return false;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        DrawableProperties drawableProperties = this.A08;
        if (drawableProperties == null) {
            drawableProperties = new DrawableProperties();
            this.A08 = drawableProperties;
        }
        drawableProperties.setAlpha(i);
        AbstractC117045Ya r0 = this.A09;
        if (r0 != null) {
            r0.Abj(i);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        DrawableProperties drawableProperties = this.A08;
        if (drawableProperties == null) {
            drawableProperties = new DrawableProperties();
            this.A08 = drawableProperties;
        }
        drawableProperties.setColorFilter(colorFilter);
        AbstractC117045Ya r0 = this.A09;
        if (r0 != null) {
            r0.Abu(colorFilter);
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        AbstractC117045Ya r0;
        if (!this.A0E && (r0 = this.A09) != null && r0.getFrameCount() > 1) {
            this.A0E = true;
            long uptimeMillis = SystemClock.uptimeMillis();
            this.A07 = uptimeMillis - this.A06;
            this.A04 = uptimeMillis - this.A05;
            this.A01 = this.A02;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        if (this.A0E) {
            long uptimeMillis = SystemClock.uptimeMillis();
            this.A06 = uptimeMillis - this.A07;
            this.A05 = uptimeMillis - this.A04;
            this.A02 = this.A01;
            this.A0E = false;
            this.A07 = 0;
            this.A04 = -1;
            this.A01 = -1;
            unscheduleSelf(this.A0B);
        }
    }
}
