package X;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/* renamed from: X.5Ks  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class RunnableC114235Ks extends AbstractC11120fm implements Runnable {
    public static final long A00;
    public static final RunnableC114235Ks A01;
    public static volatile Thread _thread;
    public static volatile int debugStatus;

    static {
        Long l;
        RunnableC114235Ks r4 = new RunnableC114235Ks();
        A01 = r4;
        r4.A00++;
        r4.A02 = true;
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        try {
            l = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000);
        } catch (SecurityException unused) {
            l = 1000L;
        }
        A00 = timeUnit.toNanos(l.longValue());
    }

    @Override // X.AbstractC11120fm, X.AbstractC114165Kl
    public void A07() {
        debugStatus = 4;
        super.A07();
    }

    @Override // X.AbstractC114245Kt
    public Thread A0B() {
        Thread thread = _thread;
        if (thread == null) {
            synchronized (this) {
                thread = _thread;
                if (thread == null) {
                    thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
                    _thread = thread;
                    thread.setDaemon(true);
                    thread.start();
                }
            }
        }
        return thread;
    }

    @Override // X.AbstractC11120fm
    public void A0H(Runnable runnable) {
        if (debugStatus == 4) {
            throw new RejectedExecutionException("DefaultExecutor was shut down. This error indicates that Dispatchers.shutdown() was invoked prior to completion of exiting coroutines, leaving coroutines in incomplete state. Please refer to Dispatchers.shutdown documentation for more details");
        }
        super.A0H(runnable);
    }

    public final synchronized void A0K() {
        int i = debugStatus;
        if (i == 2 || i == 3) {
            debugStatus = 3;
            this._queue = null;
            this._delayed = null;
            notifyAll();
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean z;
        C94654cI.A00.set(this);
        try {
            synchronized (this) {
                int i = debugStatus;
                if (i == 2 || i == 3) {
                    z = false;
                } else {
                    z = true;
                    debugStatus = 1;
                    notifyAll();
                }
            }
            if (z) {
                long j = Long.MAX_VALUE;
                while (true) {
                    Thread.interrupted();
                    long A0D = A0D();
                    if (A0D == Long.MAX_VALUE) {
                        long nanoTime = System.nanoTime();
                        if (j == Long.MAX_VALUE) {
                            j = A00 + nanoTime;
                        }
                        long j2 = j - nanoTime;
                        if (j2 <= 0) {
                            break;
                        } else if (A0D > j2) {
                            A0D = j2;
                        }
                    } else {
                        j = Long.MAX_VALUE;
                    }
                    if (A0D > 0) {
                        int i2 = debugStatus;
                        if (i2 == 2 || i2 == 3) {
                            break;
                        }
                        LockSupport.parkNanos(this, A0D);
                    }
                }
            }
        } finally {
            _thread = null;
            A0K();
            if (!A0I()) {
                A0B();
            }
        }
    }
}
