package X;

import android.content.Context;
import android.view.MotionEvent;
import android.webkit.WebView;

/* renamed from: X.3hF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74063hF extends WebView {
    public final /* synthetic */ C74103hK A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74063hF(Context context, C74103hK r3) {
        super(context, null);
        this.A00 = r3;
    }

    @Override // android.webkit.WebView, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return false;
    }

    @Override // android.view.View
    public boolean performClick() {
        super.performClick();
        return false;
    }
}
