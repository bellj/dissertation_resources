package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.1ED  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ED {
    public final C16510p9 A00;
    public final C18460sU A01;
    public final C16490p7 A02;
    public final C20320vZ A03;

    public AnonymousClass1ED(C16510p9 r1, C18460sU r2, C16490p7 r3, C20320vZ r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static Cursor A00(C16310on r4, byte b, long j) {
        C16330op r42 = r4.A03;
        StringBuilder sb = new StringBuilder();
        sb.append(C40471re.A00(b));
        sb.append(" WHERE message_add_on._id = ? AND message_add_on.message_add_on_type = ?");
        return r42.A09(sb.toString(), new String[]{String.valueOf(j), String.valueOf((int) b)});
    }

    public static Cursor A01(C16310on r4, byte b, long j) {
        String[] strArr = {String.valueOf(j), String.valueOf((int) b)};
        C16330op r2 = r4.A03;
        StringBuilder sb = new StringBuilder();
        sb.append(C40471re.A00(b));
        sb.append(" WHERE message_add_on.parent_message_row_id = ? AND message_add_on.message_add_on_type = ?");
        return r2.A09(sb.toString(), strArr);
    }

    public static Cursor A02(C16310on r5, byte b, long j, long j2, boolean z) {
        String str;
        String[] strArr = new String[4];
        strArr[0] = String.valueOf(j);
        strArr[1] = String.valueOf((int) b);
        strArr[2] = String.valueOf(j2);
        if (z) {
            str = "1";
        } else {
            str = "0";
        }
        strArr[3] = str;
        C16330op r52 = r5.A03;
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(C40471re.A00(b));
        sb2.append(" WHERE message_add_on.parent_message_row_id = ? AND message_add_on.message_add_on_type = ?");
        sb.append(sb2.toString());
        sb.append(" AND ");
        sb.append("message_add_on");
        sb.append(".");
        sb.append("sender_jid_row_id");
        sb.append(" = ? AND ");
        sb.append("message_add_on");
        sb.append(".");
        sb.append("from_me");
        sb.append(" = ? ");
        return r52.A09(sb.toString(), strArr);
    }

    public long A03(AnonymousClass1Iv r8) {
        long j;
        C16310on A02 = this.A02.A02();
        try {
            ContentValues contentValues = new ContentValues(12);
            C16510p9 r1 = this.A00;
            AnonymousClass1IS r3 = r8.A0z;
            AbstractC14640lm r0 = r3.A00;
            AnonymousClass009.A05(r0);
            contentValues.put("chat_row_id", Long.valueOf(r1.A02(r0)));
            contentValues.put("from_me", Boolean.valueOf(r3.A02));
            contentValues.put("key_id", r3.A01);
            AbstractC14640lm A0B = r8.A0B();
            if (A0B != null) {
                j = this.A01.A01(A0B);
            } else {
                j = -1;
            }
            contentValues.put("sender_jid_row_id", Long.valueOf(j));
            contentValues.put("parent_message_row_id", Long.valueOf(r8.A00));
            contentValues.put("timestamp", Long.valueOf(r8.A0I));
            contentValues.put("status", Integer.valueOf(r8.A0C));
            contentValues.put("message_add_on_type", Byte.valueOf(r8.A0y));
            long A03 = A02.A03.A03(contentValues, "message_add_on");
            r8.A11 = A03;
            A02.close();
            return A03;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public AnonymousClass1Iv A04(Cursor cursor, HashMap hashMap) {
        String str;
        int A00 = AnonymousClass1Tx.A00("from_me", hashMap);
        int A002 = AnonymousClass1Tx.A00("key_id", hashMap);
        int A003 = AnonymousClass1Tx.A00("chat_row_id", hashMap);
        boolean z = true;
        if (cursor.getInt(A00) != 1) {
            z = false;
        }
        String string = cursor.getString(A002);
        AbstractC14640lm A05 = this.A00.A05(cursor.getLong(A003));
        if (A05 == null) {
            str = "MessageAddOnStore/createFMessageFromCursor unexpected jid for MessageAddOn";
        } else {
            AnonymousClass1IS r4 = new AnonymousClass1IS(A05, string, z);
            int A004 = AnonymousClass1Tx.A00("timestamp", hashMap);
            int A005 = AnonymousClass1Tx.A00("message_add_on_type", hashMap);
            long j = cursor.getLong(A004);
            AbstractC15340mz A01 = this.A03.A01(r4, (byte) cursor.getInt(A005), j);
            if (A01 instanceof AnonymousClass1Iv) {
                return (AnonymousClass1Iv) A01;
            }
            str = "MessageAddOnStore/createFMessageFromCursor read fMessage with not supported messageAddOnType";
        }
        Log.e(str);
        return null;
    }

    public void A05(long j) {
        C16310on A02 = this.A02.A02();
        try {
            A02.A03.A01("message_add_on", "_id = ?", new String[]{String.valueOf(j)});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A06(Set set, int i) {
        String str;
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", Integer.valueOf(i));
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                AnonymousClass1IS r7 = (AnonymousClass1IS) it.next();
                String[] strArr = new String[3];
                C16510p9 r1 = this.A00;
                AbstractC14640lm r0 = r7.A00;
                AnonymousClass009.A05(r0);
                strArr[0] = String.valueOf(r1.A02(r0));
                if (r7.A02) {
                    str = "1";
                } else {
                    str = "0";
                }
                strArr[1] = str;
                strArr[2] = r7.A01;
                A02.A03.A00("message_add_on", contentValues, "chat_row_id = ? AND from_me = ? AND key_id = ?", strArr);
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
