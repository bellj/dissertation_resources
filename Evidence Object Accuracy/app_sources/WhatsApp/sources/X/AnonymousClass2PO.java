package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2PO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PO extends AnonymousClass2PA {
    public final String A00;

    public AnonymousClass2PO(Jid jid, String str, String str2, long j) {
        super(jid, str, j);
        this.A00 = str2;
    }
}
