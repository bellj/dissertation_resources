package X;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.gallerypicker.MediaPickerFragment;
import java.util.HashSet;

/* renamed from: X.2G6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2G6 implements AnonymousClass02Q {
    public MenuItem A00;
    public Runnable A01;
    public final Context A02;
    public final TextView A03;
    public final /* synthetic */ MediaPickerFragment A04;

    public AnonymousClass2G6(Context context, MediaPickerFragment mediaPickerFragment) {
        this.A04 = mediaPickerFragment;
        this.A02 = context;
        TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.action_mode_marque_text, (ViewGroup) null);
        this.A03 = textView;
        AnonymousClass028.A0g(textView, new C53612ej(this));
    }

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r4) {
        if (menuItem.getItemId() != 0) {
            return false;
        }
        MediaPickerFragment mediaPickerFragment = this.A04;
        mediaPickerFragment.A1O(mediaPickerFragment.A0E);
        return false;
    }

    @Override // X.AnonymousClass02Q
    public final boolean AOg(Menu menu, AbstractC009504t r6) {
        int i;
        TextView textView = this.A03;
        r6.A09(textView);
        MediaPickerFragment mediaPickerFragment = this.A04;
        if (!((MediaGalleryFragmentBase) mediaPickerFragment).A0Q) {
            MenuItem add = menu.add(0, 0, 0, mediaPickerFragment.A0I(R.string.ok));
            this.A00 = add;
            add.setShowAsAction(2);
        }
        if (Build.VERSION.SDK_INT < 21) {
            return true;
        }
        if (((MediaGalleryFragmentBase) mediaPickerFragment).A0Q) {
            textView.setTextColor(AnonymousClass00T.A00(this.A02, R.color.gallery_toolbar_text));
            i = R.color.gallery_toolbar_background;
        } else {
            i = R.color.primary_dark;
        }
        mediaPickerFragment.A0C().getWindow().setStatusBarColor(AnonymousClass00T.A00(this.A02, i));
        return true;
    }

    @Override // X.AnonymousClass02Q
    public final void AP3(AbstractC009504t r4) {
        Runnable runnable = this.A01;
        if (runnable != null) {
            this.A03.removeCallbacks(runnable);
        }
        MediaPickerFragment mediaPickerFragment = this.A04;
        if (mediaPickerFragment.A0B) {
            mediaPickerFragment.A0C().finish();
        }
        mediaPickerFragment.A05 = null;
        mediaPickerFragment.A1M();
        if (Build.VERSION.SDK_INT >= 21) {
            mediaPickerFragment.A0C().getWindow().setStatusBarColor(AnonymousClass00T.A00(this.A02, R.color.black));
        }
    }

    @Override // X.AnonymousClass02Q
    public boolean AU7(Menu menu, AbstractC009504t r10) {
        String quantityString;
        MediaPickerFragment mediaPickerFragment = this.A04;
        HashSet hashSet = mediaPickerFragment.A0E;
        if (hashSet.isEmpty()) {
            quantityString = mediaPickerFragment.A0I(R.string.select_multiple_title);
        } else {
            quantityString = mediaPickerFragment.A02().getQuantityString(R.plurals.n_photos_selected, hashSet.size(), Integer.valueOf(hashSet.size()));
        }
        TextView textView = this.A03;
        textView.setText(quantityString);
        if (this.A01 == null && !textView.isSelected()) {
            RunnableBRunnable0Shape16S0100000_I1_2 runnableBRunnable0Shape16S0100000_I1_2 = new RunnableBRunnable0Shape16S0100000_I1_2(this, 15);
            this.A01 = runnableBRunnable0Shape16S0100000_I1_2;
            textView.postDelayed(runnableBRunnable0Shape16S0100000_I1_2, 1000);
        }
        MenuItem menuItem = this.A00;
        if (menuItem != null) {
            menuItem.setVisible(!hashSet.isEmpty());
        }
        return true;
    }
}
