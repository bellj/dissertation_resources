package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.1Fv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27051Fv {
    public SharedPreferences A00;
    public final C16630pM A01;

    public C27051Fv(C16630pM r1) {
        this.A01 = r1;
    }

    public final SharedPreferences A00() {
        SharedPreferences sharedPreferences = this.A00;
        if (sharedPreferences != null) {
            return sharedPreferences;
        }
        SharedPreferences A01 = this.A01.A01(AnonymousClass01V.A07);
        this.A00 = A01;
        return A01;
    }

    public void A01(String str) {
        ArrayList arrayList = new ArrayList();
        for (String str2 : A00().getAll().keySet()) {
            if (str2 != null) {
                if (!str2.startsWith("ResumableUrl-")) {
                    StringBuilder sb = new StringBuilder("gdrive-ResumableUrl-");
                    sb.append(str);
                    if (!str2.startsWith(sb.toString())) {
                        StringBuilder sb2 = new StringBuilder("gbackup-ResumableUrl-");
                        sb2.append(str);
                        if (str2.startsWith(sb2.toString())) {
                        }
                    }
                }
                arrayList.add(str2);
            }
        }
        SharedPreferences.Editor edit = A00().edit();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            edit.remove((String) it.next());
        }
        if (!edit.commit()) {
            Log.w("gdrive-api/remove-all-resumable-uris unable to commit after cleanup to shared prefs.");
        }
    }

    public void A02(String str, String str2) {
        SharedPreferences.Editor edit = A00().edit();
        StringBuilder sb = new StringBuilder("gbackup-ResumableUrl-");
        sb.append(str);
        sb.append("-");
        sb.append(str2);
        edit.remove(sb.toString());
        if (!edit.commit()) {
            Log.w("gdrive-api/remove-resumable-uri unable to commit resumable uri to shared prefs.");
        }
    }
}
