package X;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.37a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C623837a extends AbstractC16350or {
    public final AnonymousClass02N A00;
    public final AnonymousClass5U5 A01;
    public final C15660nh A02;
    public final HashMap A03 = C12970iu.A11();

    public C623837a(AnonymousClass02N r5, AnonymousClass5U5 r6, C15660nh r7, Collection collection) {
        File file;
        this.A02 = r7;
        this.A00 = r5;
        this.A01 = r6;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC15340mz A0f = C12980iv.A0f(it);
            if (A0f instanceof AbstractC16130oV) {
                AbstractC16130oV r1 = (AbstractC16130oV) A0f;
                String str = r1.A05;
                C16150oX r0 = r1.A02;
                if (r0 != null) {
                    file = r0.A0F;
                } else {
                    file = null;
                }
                if (!(str == null || file == null)) {
                    this.A03.put(str, file);
                }
            }
        }
    }
}
