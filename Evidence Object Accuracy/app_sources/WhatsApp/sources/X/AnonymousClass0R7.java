package X;

import android.content.Context;
import androidx.fragment.app.DialogFragment;

/* renamed from: X.0R7  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0R7 {
    public static AnonymousClass0OS A00(Context context, DialogFragment dialogFragment, AnonymousClass0PT r6, AnonymousClass3JI r7, AnonymousClass4E1 r8, String str) {
        AnonymousClass0OS r3 = new AnonymousClass0OS(new C55982k5(context), str);
        A01(context, dialogFragment, r3, r6, r7, r8);
        return r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00bc, code lost:
        if (r9 == -1) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.content.Context r12, androidx.fragment.app.DialogFragment r13, X.AnonymousClass0OS r14, X.AnonymousClass0PT r15, X.AnonymousClass3JI r16, X.AnonymousClass4E1 r17) {
        /*
            android.util.SparseArray r6 = new android.util.SparseArray
            r6.<init>()
            r0 = 2131362610(0x7f0a0332, float:1.8345005E38)
            r6.put(r0, r14)
            r0 = r13
            X.0ii r0 = (X.AbstractC12880ii) r0
            X.3En r5 = r0.A8A()
            java.util.Map r4 = r15.A0B
            int r1 = r15.A03
            r7 = 2
            r3 = -1
            r11 = 1
            r10 = 0
            if (r1 != r3) goto L_0x00c0
            int r0 = r15.A00
            if (r0 != r3) goto L_0x00c0
            android.content.res.Resources r0 = r12.getResources()
            android.util.DisplayMetrics r8 = r0.getDisplayMetrics()
            android.content.res.Resources r9 = r12.getResources()
            java.lang.String r2 = "status_bar_height"
            java.lang.String r1 = "dimen"
            java.lang.String r0 = "android"
            int r0 = r9.getIdentifier(r2, r1, r0)
            if (r0 > 0) goto L_0x00b8
            r9 = -1
        L_0x0039:
            android.app.Activity r1 = X.AnonymousClass0RG.A00(r12)
            if (r1 == 0) goto L_0x0057
            android.view.Window r0 = r1.getWindow()
            if (r0 == 0) goto L_0x0057
            android.view.Window r0 = r1.getWindow()
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            android.view.View r0 = r0.getDecorView()
            r0.getWindowVisibleDisplayFrame(r1)
            int r9 = r1.top
        L_0x0057:
            int[] r2 = new int[r7]
            int r0 = r8.widthPixels
            r2[r10] = r0
            int r0 = r8.heightPixels
            int r0 = r0 - r9
            r2[r11] = r0
            X.0Mi r0 = X.AnonymousClass0RG.A00
            r9 = r2[r10]
            java.util.concurrent.atomic.AtomicReference r8 = r0.A00
            java.lang.Object r1 = r8.get()
            int[] r1 = (int[]) r1
            if (r1 == 0) goto L_0x00cf
            int r0 = r1.length
            if (r0 != r7) goto L_0x00cf
            r0 = r1[r10]
            if (r0 == r3) goto L_0x0078
            r9 = r0
        L_0x0078:
            r2 = r2[r11]
            java.lang.Object r1 = r8.get()
            int[] r1 = (int[]) r1
            if (r1 == 0) goto L_0x00c7
            int r0 = r1.length
            if (r0 != r7) goto L_0x00c7
            r0 = r1[r11]
            if (r0 == r3) goto L_0x008a
            r2 = r0
        L_0x008a:
            int[] r3 = new int[r7]
            r3[r10] = r9
            r3[r11] = r2
            int[] r2 = new int[r7]
            r0 = r3[r10]
            r1 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r2[r10] = r0
            r0 = r3[r11]
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
        L_0x00a2:
            r2[r11] = r0
            X.0Rw r0 = X.C06020Rw.A00(r12, r6, r5, r4, r2)
            r2 = r16
            r1 = r17
            X.0UA r0 = X.AnonymousClass0UA.A00(r0, r15, r2, r1)
            X.0hM r13 = (X.AbstractC12090hM) r13
            r0.A07(r12, r13)
            r14.A00 = r0
            return
        L_0x00b8:
            int r9 = r9.getDimensionPixelSize(r0)
            if (r9 != r3) goto L_0x0057
            goto L_0x0039
        L_0x00c0:
            int[] r2 = new int[r7]
            r2[r10] = r1
            int r0 = r15.A00
            goto L_0x00a2
        L_0x00c7:
            java.lang.String r1 = "Screen size cache is corrupted"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x00cf:
            java.lang.String r1 = "Screen size cache is corrupted"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0R7.A01(android.content.Context, androidx.fragment.app.DialogFragment, X.0OS, X.0PT, X.3JI, X.4E1):void");
    }
}
