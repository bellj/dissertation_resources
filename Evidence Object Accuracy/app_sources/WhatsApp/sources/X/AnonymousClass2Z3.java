package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import com.whatsapp.R;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.util.regex.Matcher;

/* renamed from: X.2Z3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Z3 extends BroadcastReceiver {
    public boolean A00;
    public final C14820m6 A01;
    public final Object A02 = C12970iu.A0l();
    public final WeakReference A03;
    public volatile boolean A04 = false;

    public AnonymousClass2Z3(C14820m6 r2, VerifyPhoneNumber verifyPhoneNumber) {
        this.A03 = C12970iu.A10(verifyPhoneNumber);
        this.A01 = r2;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        SmsMessage smsMessage;
        String str;
        if (!this.A04) {
            synchronized (this.A02) {
                if (!this.A04) {
                    AnonymousClass22D.A00(context);
                    this.A04 = true;
                }
            }
        }
        Log.i("receivedtextreceiver/text/intent");
        if (this.A00) {
            str = "receivedtextreceiver/already received";
        } else {
            VerifyPhoneNumber verifyPhoneNumber = (VerifyPhoneNumber) this.A03.get();
            if (verifyPhoneNumber == null) {
                str = "receivedtextreceiver/activity is null";
            } else if (verifyPhoneNumber.AJN()) {
                str = "receivedtextreceiver/destroyed";
            } else {
                Bundle extras = intent.getExtras();
                if (extras == null) {
                    str = "receivedtextreceiver/bundle-null";
                } else {
                    Object[] objArr = (Object[]) extras.get("pdus");
                    if (objArr == null) {
                        str = "receivedtextreceiver/pdus-null";
                    } else {
                        StringBuilder A0k = C12960it.A0k("receivedtextreceiver/pdus-length/");
                        int length = objArr.length;
                        A0k.append(length);
                        C12960it.A1F(A0k);
                        AnonymousClass4L0 r4 = new AnonymousClass4L0(verifyPhoneNumber.getString(R.string.localized_app_name));
                        for (Object obj : objArr) {
                            String str2 = null;
                            try {
                                if (C28391Mz.A03()) {
                                    smsMessage = SmsMessage.createFromPdu((byte[]) obj, extras.getString("format"));
                                } else {
                                    smsMessage = SmsMessage.createFromPdu((byte[]) obj);
                                }
                                if (smsMessage != null) {
                                    try {
                                        str2 = smsMessage.getMessageBody();
                                        StringBuilder A0h = C12960it.A0h();
                                        A0h.append("verifysms/getMessageBody ");
                                        Log.i(C12960it.A0d(str2, A0h));
                                        StringBuilder A0h2 = C12960it.A0h();
                                        A0h2.append("verifysms/displayMessageBody ");
                                        Log.i(C12960it.A0d(smsMessage.getDisplayMessageBody(), A0h2));
                                        StringBuilder A0h3 = C12960it.A0h();
                                        A0h3.append("verifysms/displayOriginatingAddress ");
                                        Log.i(C12960it.A0d(smsMessage.getDisplayOriginatingAddress(), A0h3));
                                        StringBuilder A0h4 = C12960it.A0h();
                                        A0h4.append("verifysms/emailBody ");
                                        Log.i(C12960it.A0d(smsMessage.getEmailBody(), A0h4));
                                        StringBuilder A0h5 = C12960it.A0h();
                                        A0h5.append("verifysms/emailFrom ");
                                        Log.i(C12960it.A0d(smsMessage.getEmailFrom(), A0h5));
                                        StringBuilder A0h6 = C12960it.A0h();
                                        A0h6.append("verifysms/getOriginatingAddress ");
                                        Log.i(C12960it.A0d(smsMessage.getOriginatingAddress(), A0h6));
                                        StringBuilder A0h7 = C12960it.A0h();
                                        A0h7.append("verifysms/getPseudoSubject ");
                                        Log.i(C12960it.A0d(smsMessage.getPseudoSubject(), A0h7));
                                        StringBuilder A0h8 = C12960it.A0h();
                                        A0h8.append("verifysms/getServiceCenterAddress ");
                                        Log.i(C12960it.A0d(smsMessage.getServiceCenterAddress(), A0h8));
                                    } catch (NullPointerException e) {
                                        Log.e("verifysms", e);
                                    }
                                    if (str2 == null) {
                                        Log.i("receivedtextreceiver/message-null");
                                    } else {
                                        Log.i(C12960it.A0d(str2, C12960it.A0k("verifysms/text-receiver/")));
                                        Matcher matcher = r4.A00.matcher(str2);
                                        if (matcher.find()) {
                                            StringBuilder A0h9 = C12960it.A0h();
                                            A0h9.append(matcher.group(1));
                                            String A0d = C12960it.A0d(matcher.group(2), A0h9);
                                            if (A0d != null) {
                                                if (C28421Nd.A00(A0d, -1) != -1) {
                                                    this.A00 = true;
                                                    abortBroadcast();
                                                    verifyPhoneNumber.A3X(A0d);
                                                } else {
                                                    Log.w("verifysms/text-receiver/no-code");
                                                    AnonymousClass23M.A0J(this.A01, "server-send-mismatch-empty");
                                                }
                                            }
                                        }
                                        Log.w("verifysms/text-receiver/not_sms_verification");
                                    }
                                }
                            } catch (OutOfMemoryError e2) {
                                Log.e("verifysms/text/out-of-memory ", e2);
                            }
                        }
                        return;
                    }
                }
            }
        }
        Log.i(str);
    }
}
