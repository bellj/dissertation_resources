package X;

import java.io.IOException;

/* renamed from: X.68T  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68T implements AbstractC44401yr {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass4VZ A01;
    public final /* synthetic */ C91064Qh A02;
    public final /* synthetic */ C65963Lt A03;
    public final /* synthetic */ AnonymousClass5WN A04;
    public final /* synthetic */ C124395pK A05;
    public final /* synthetic */ Boolean A06;
    public final /* synthetic */ String A07;
    public final /* synthetic */ String A08;
    public final /* synthetic */ String A09;
    public final /* synthetic */ boolean A0A;

    public AnonymousClass68T(AnonymousClass4VZ r1, C91064Qh r2, C65963Lt r3, AnonymousClass5WN r4, C124395pK r5, Boolean bool, String str, String str2, String str3, int i, boolean z) {
        this.A05 = r5;
        this.A08 = str;
        this.A03 = r3;
        this.A09 = str2;
        this.A06 = bool;
        this.A02 = r2;
        this.A0A = z;
        this.A04 = r4;
        this.A00 = i;
        this.A01 = r1;
        this.A07 = str3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0108 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC44401yr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6t(X.AnonymousClass23Z r19) {
        /*
        // Method dump skipped, instructions count: 366
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass68T.A6t(X.23Z):void");
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        C91064Qh r3 = this.A02;
        r3.A02 = iOException;
        r3.A00 = 7;
        C124395pK r2 = this.A05;
        r2.A01(r3, this.A04, iOException.getLocalizedMessage());
        C124395pK.A00(r2, this.A00);
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        C91064Qh r3 = this.A02;
        r3.A02 = exc;
        r3.A00 = 4;
        C124395pK r2 = this.A05;
        r2.A01(r3, this.A04, exc.getLocalizedMessage());
        C124395pK.A00(r2, this.A00);
    }
}
