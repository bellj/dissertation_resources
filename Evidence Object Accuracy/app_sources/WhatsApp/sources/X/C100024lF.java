package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100024lF implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C15960oD(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C15960oD[i];
    }
}
