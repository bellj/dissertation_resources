package X;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import com.whatsapp.R;
import com.whatsapp.payments.ui.NoviCreateClaimActivity;

/* renamed from: X.63w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1317263w implements TextWatcher {
    public final /* synthetic */ NoviCreateClaimActivity A00;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C1317263w(NoviCreateClaimActivity noviCreateClaimActivity) {
        this.A00 = noviCreateClaimActivity;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        View view;
        float dimension;
        int length = charSequence.toString().trim().length();
        NoviCreateClaimActivity noviCreateClaimActivity = this.A00;
        Button button = noviCreateClaimActivity.A01;
        if (length == 0) {
            button.setEnabled(false);
            if (C28391Mz.A02()) {
                view = noviCreateClaimActivity.A00;
                dimension = 0.0f;
            } else {
                return;
            }
        } else {
            button.setEnabled(true);
            if (C28391Mz.A02()) {
                view = noviCreateClaimActivity.A00;
                dimension = noviCreateClaimActivity.getResources().getDimension(R.dimen.novi_pay_button_elevation);
            } else {
                return;
            }
        }
        view.setElevation(dimension);
    }
}
