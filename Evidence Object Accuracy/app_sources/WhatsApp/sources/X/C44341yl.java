package X;

import android.app.Application;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.avatar.home.AvatarHomeViewModel;
import com.whatsapp.avatar.profilephoto.AvatarProfilePhotoViewModel;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.backup.google.viewmodel.GoogleDriveNewUserSetupViewModel;
import com.whatsapp.backup.google.viewmodel.RestoreFromBackupViewModel;
import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;
import com.whatsapp.biz.SmbViewModel;
import com.whatsapp.biz.order.viewmodel.OrderInfoViewModel;
import com.whatsapp.biz.product.viewmodel.ComplianceInfoViewModel;
import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListViewModel;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectoryActivityViewModel;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.businessdirectory.viewmodel.DirectorySearchHistoryViewModel;
import com.whatsapp.businessdirectory.viewmodel.DirectorySetNeighborhoodViewModel;
import com.whatsapp.businessdirectory.viewmodel.LocationOptionPickerViewModel;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;
import com.whatsapp.calling.callgrid.viewmodel.InCallBannerViewModel;
import com.whatsapp.calling.callgrid.viewmodel.MenuBottomSheetViewModel;
import com.whatsapp.calling.callgrid.viewmodel.OrientationViewModel;
import com.whatsapp.calling.callheader.viewmodel.CallHeaderViewModel;
import com.whatsapp.calling.calllink.viewmodel.CallLinkViewModel;
import com.whatsapp.calling.controls.viewmodel.BottomSheetViewModel;
import com.whatsapp.calling.controls.viewmodel.CallControlButtonsViewModel;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogAllCategoryViewModel;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupsViewModel;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryTabsViewModel;
import com.whatsapp.catalogsearch.view.viewmodel.CatalogSearchViewModel;
import com.whatsapp.community.AddGroupsToCommunityViewModel;
import com.whatsapp.community.CommunityTabViewModel;
import com.whatsapp.community.ConversationCommunityViewModel;
import com.whatsapp.companiondevice.LinkedDevicesSharedViewModel;
import com.whatsapp.companiondevice.LinkedDevicesViewModel;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsViewModel;
import com.whatsapp.conversation.conversationrow.messagerating.MessageRatingViewModel;
import com.whatsapp.conversationslist.ArchiveHeaderViewModel;
import com.whatsapp.countrygating.viewmodel.CountryGatingViewModel;
import com.whatsapp.group.GroupSettingsViewModel;
import com.whatsapp.migration.android.view.GoogleMigrateImporterViewModel;
import com.whatsapp.migration.export.ui.ExportMigrationViewModel;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperLinkViewModel;
import com.whatsapp.payments.ui.viewmodel.BusinessHubViewModel;
import com.whatsapp.polls.PollCreatorViewModel;
import com.whatsapp.polls.PollResultsViewModel;
import com.whatsapp.polls.PollVoterViewModel;
import com.whatsapp.qrcode.DevicePairQrScannerViewModel;
import com.whatsapp.reactions.ReactionsTrayViewModel;
import com.whatsapp.registration.report.BanReportViewModel;
import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.settings.SettingsChatViewModel;
import com.whatsapp.settings.SettingsDataUsageViewModel;
import com.whatsapp.shops.ShopsBkLayoutViewModel;
import com.whatsapp.shops.ShopsProductPreviewFragmentViewModel;
import com.whatsapp.tosgating.viewmodel.ToSGatingViewModel;
import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.camera.VoipCameraManager;
import com.whatsapp.wabloks.base.GenericBkLayoutViewModel;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel;
import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeFragmentViewModel;
import org.chromium.net.UrlRequest;

/* renamed from: X.1yl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44341yl implements AnonymousClass01N {
    public final int A00;
    public final C48722Hj A01;
    public final C44351ym A02;
    public final AnonymousClass01J A03;

    public C44341yl(C48722Hj r1, C44351ym r2, AnonymousClass01J r3, int i) {
        this.A03 = r3;
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = i;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        int i = this.A00;
        switch (i) {
            case 0:
                AnonymousClass01J r1 = this.A03;
                AnonymousClass10V r3 = (AnonymousClass10V) r1.A48.get();
                C20710wC r5 = (C20710wC) r1.A8m.get();
                AnonymousClass10Z r6 = (AnonymousClass10Z) r1.AGM.get();
                return new AddGroupsToCommunityViewModel((AbstractC15710nm) r1.A4o.get(), r3, (C20830wO) r1.A4W.get(), r5, r6, (AbstractC14440lR) r1.ANe.get());
            case 1:
                AnonymousClass01J r12 = this.A03;
                C19990v2 r4 = (C19990v2) r12.A3M.get();
                AnonymousClass018 r32 = (AnonymousClass018) r12.ANb.get();
                return new ArchiveHeaderViewModel((C14820m6) r12.AN3.get(), r32, r4, (C15680nj) r12.A4e.get(), (C14850m9) r12.A04.get());
            case 2:
                AnonymousClass01J r2 = this.A03;
                AnonymousClass12V r33 = (AnonymousClass12V) r2.A0p.get();
                C20980wd r22 = (C20980wd) r2.A0t.get();
                return new AvatarHomeViewModel(r33, C44351ym.A01(this.A02), r22, (C235812f) r2.A13.get());
            case 3:
                return new AnonymousClass4JL(this);
            case 4:
                return new AnonymousClass44N();
            case 5:
                return new AnonymousClass328();
            case 6:
                return new C69243Ym(this);
            case 7:
                return new AnonymousClass4JM(this);
            case 8:
                return new AnonymousClass44O();
            case 9:
                return new AnonymousClass329();
            case 10:
                return new C69253Yn(this);
            case 11:
                AnonymousClass01J r23 = this.A03;
                C14900mE r34 = (C14900mE) r23.A8X.get();
                C15570nT r42 = (C15570nT) r23.AAr.get();
                C235812f r122 = (C235812f) r23.A13.get();
                C18170s1 A41 = r23.A41();
                AnonymousClass10Z r13 = (AnonymousClass10Z) r23.AGM.get();
                AnonymousClass12V r8 = (AnonymousClass12V) r23.A0p.get();
                C20980wd r11 = (C20980wd) r23.A0t.get();
                C44351ym r14 = this.A02;
                AnonymousClass01J r24 = r14.A1O;
                AnonymousClass4JR r62 = new AnonymousClass4JR((C16590pI) r24.AMg.get());
                return new AvatarProfilePhotoViewModel(r34, r42, new AnonymousClass4JQ((C14330lG) r24.A7B.get()), r62, (C14830m7) r23.ALb.get(), r8, C44351ym.A01(r14), A41, r11, r122, r13, (AbstractC14440lR) r23.ANe.get());
            case 12:
                AnonymousClass01J r15 = this.A03;
                C18350sJ r52 = (C18350sJ) r15.AHX.get();
                return new BanAppealViewModel((AnonymousClass19Y) r15.AI6.get(), (C252818u) r15.AMy.get(), (C18360sK) r15.AN0.get(), r52, (C15510nN) r15.AHZ.get(), (C252018m) r15.A7g.get(), (C18340sI) r15.A1K.get());
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                AnonymousClass01J r25 = this.A03;
                C44351ym r16 = this.A02;
                AnonymousClass4JN r35 = (AnonymousClass4JN) r16.A0l.get();
                C20330va r63 = (C20330va) r25.A7w.get();
                return new BanReportViewModel(r35, (AnonymousClass4JO) r16.A0m.get(), (C44331yk) r16.A0n.get(), r63, (C26891Ff) r25.A29.get(), (AbstractC14440lR) r25.ANe.get());
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new AnonymousClass4JN(this);
            case 15:
                return new AnonymousClass32A();
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new AnonymousClass44P();
            case 17:
                return new AnonymousClass4JO(this);
            case 18:
                return new AnonymousClass44M();
            case 19:
                return new C44331yk(this);
            case C43951xu.A01:
                AnonymousClass01J r17 = this.A03;
                C22160yd r7 = (C22160yd) r17.AC4.get();
                C16120oU r9 = (C16120oU) r17.ANE.get();
                C16170oZ r36 = (C16170oZ) r17.AM4.get();
                C15550nR r53 = (C15550nR) r17.A45.get();
                AnonymousClass10Y r82 = (AnonymousClass10Y) r17.AAR.get();
                return new BlockReasonListViewModel(AbstractC250617y.A00(r17.AO3), r36, (C238013b) r17.A1Z.get(), r53, (C254119h) r17.AJi.get(), r7, r82, r9, (AbstractC14440lR) r17.ANe.get());
            case 21:
                return new BottomSheetViewModel((C48782Ht) this.A01.A00.get(), (AnonymousClass01d) this.A03.ALI.get());
            case 22:
                AnonymousClass01J r26 = this.A03;
                r26.AMg.get();
                C14820m6 r92 = (C14820m6) r26.AN3.get();
                C26891Ff r10 = (C26891Ff) r26.A29.get();
                C26871Fd r112 = (C26871Fd) r26.A2A.get();
                AnonymousClass01J r64 = this.A02.A1O;
                C14900mE r43 = (C14900mE) r64.A8X.get();
                C44101yE r132 = new C44101yE((AbstractC15710nm) r64.A4o.get(), r43, (C15570nT) r64.AAr.get(), (C18640sm) r64.A3u.get(), (C14830m7) r64.ALb.get(), (C17220qS) r64.ABt.get());
                C48332Fp r123 = new C48332Fp((AbstractC15710nm) r64.A4o.get(), (C14900mE) r64.A8X.get(), (C15570nT) r64.AAr.get(), (C18640sm) r64.A3u.get(), (C17220qS) r64.ABt.get());
                AbstractC15710nm r44 = (AbstractC15710nm) r64.A4o.get();
                AnonymousClass018 r18 = (AnonymousClass018) r64.ANb.get();
                return new BusinessActivityReportViewModel(AbstractC250617y.A00(r26.AO3), (C14900mE) r26.A8X.get(), r92, r10, r112, r123, r132, new C44241ya(r44, (C14900mE) r64.A8X.get(), (C15570nT) r64.AAr.get(), (C18640sm) r64.A3u.get(), r18, (C17220qS) r64.ABt.get()), (AbstractC14440lR) r26.ANe.get());
            case 23:
                return new BusinessDirectoryActivityViewModel((AnonymousClass1B0) this.A03.A2K.get());
            case 24:
                AnonymousClass01J r19 = this.A03;
                C14900mE r93 = (C14900mE) r19.A8X.get();
                Application A00 = AbstractC250617y.A00(r19.AO3);
                C44351ym r27 = this.A02;
                AnonymousClass07E r142 = r27.A1L;
                C251118d r113 = (C251118d) r19.A2D.get();
                AnonymousClass01J r0 = r27.A1O;
                C68553Vv r133 = new C68553Vv(r27.A02(), (C14850m9) r0.A04.get());
                AnonymousClass2K0 A02 = r27.A02();
                AbstractC115915Tk r83 = (AbstractC115915Tk) r27.A0e.get();
                C92174Uv r72 = new C92174Uv((C251118d) r0.A2D.get());
                AnonymousClass1B3 r65 = (AnonymousClass1B3) r19.AI0.get();
                AbstractC115925Tl r54 = (AbstractC115925Tl) r27.A0g.get();
                AnonymousClass4N8 r29 = new AnonymousClass4N8();
                C16430p0 r45 = (C16430p0) r19.A5y.get();
                AbstractC115855Te r37 = (AbstractC115855Te) r27.A0h.get();
                return new BusinessDirectorySearchQueryViewModel(A00, r142, r93, r113, (AnonymousClass1B2) r19.A2G.get(), r45, A02, r65, r72, r37, new AnonymousClass3E8((C26211Cl) r0.A5x.get(), (AbstractC14440lR) r0.ANe.get()), r83, r133, r29, r54, (C14830m7) r19.ALb.get());
            case 25:
                return new C68543Vu(this);
            case 26:
                return new C68453Vl(this);
            case 27:
                return new C68443Vk(this);
            case 28:
                return new C68513Vr(this);
            case 29:
                return new C68523Vs(this);
            case C25991Bp.A0S:
                return new AnonymousClass3W4(this);
            case 31:
                return new AnonymousClass3W2(this);
            case 32:
                return new AnonymousClass3W6(this);
            case 33:
                return new C68533Vt(this);
            case 34:
                return new C68593Vz(this);
            case 35:
                AnonymousClass01J r110 = this.A03;
                C17070qD r55 = (C17070qD) r110.AFC.get();
                AbstractC16870pt r66 = (AbstractC16870pt) r110.A20.get();
                C248217a r28 = (C248217a) r110.AE5.get();
                C25871Bd r73 = (C25871Bd) r110.ABZ.get();
                return new BusinessHubViewModel(r28, (C18660so) r110.AEf.get(), (C25861Bc) r110.AEh.get(), r55, r66, r73, (AbstractC14440lR) r110.ANe.get());
            case 36:
                return new CallControlButtonsViewModel((C48782Ht) this.A01.A00.get());
            case 37:
                AnonymousClass01J r210 = this.A03;
                return new CallGridViewModel((C48782Ht) this.A01.A00.get(), (C15550nR) r210.A45.get(), (AnonymousClass018) r210.ANb.get(), (C15600nX) r210.A8x.get(), (C14850m9) r210.A04.get(), (VoipCameraManager) r210.AMV.get());
            case 38:
                AnonymousClass01J r111 = this.A03;
                return new CallHeaderViewModel((C15570nT) r111.AAr.get(), (C48782Ht) this.A01.A00.get(), (C15550nR) r111.A45.get(), (C15610nY) r111.AMe.get());
            case 39:
                C44351ym r114 = this.A02;
                AnonymousClass07E r46 = r114.A1L;
                AnonymousClass01J r115 = r114.A1O;
                return new CallLinkViewModel(r46, new C90974Py((C236512m) r115.A2f.get(), r115.A2X()), (C18640sm) this.A03.A3u.get());
            case 40:
                AnonymousClass01J r47 = this.A03;
                AnonymousClass2ET A002 = C44351ym.A00(this.A02);
                return new CatalogAllCategoryViewModel((C25811Ax) r47.A2w.get(), A002, (AbstractC14440lR) r47.ANe.get());
            case 41:
                AnonymousClass01J r48 = this.A03;
                AnonymousClass2ET A003 = C44351ym.A00(this.A02);
                return new CatalogCategoryGroupsViewModel((C25811Ax) r48.A2w.get(), A003, (AbstractC14440lR) r48.ANe.get());
            case 42:
                AnonymousClass01J r49 = this.A03;
                AnonymousClass2ET A004 = C44351ym.A00(this.A02);
                return new CatalogCategoryTabsViewModel((C25811Ax) r49.A2w.get(), A004, (AbstractC14440lR) r49.ANe.get());
            case 43:
                AnonymousClass01J r116 = this.A02.A1O;
                C20100vD A2N = r116.A2N();
                AnonymousClass4JX r84 = new AnonymousClass4JX(AbstractC250617y.A00(r116.AO3), R.dimen.product_catalog_list_thumb_size);
                AnonymousClass4K9 r94 = new AnonymousClass4K9((AnonymousClass19T) r116.A2y.get());
                C14650lo r56 = (C14650lo) r116.A2V.get();
                C246816l r67 = (C246816l) r116.A5q.get();
                AnonymousClass3DH r38 = new AnonymousClass3DH((AbstractC15710nm) r116.A4o.get(), r56, r67, (AnonymousClass19Q) r116.A2u.get(), r84, r94, (AbstractC14440lR) r116.ANe.get());
                AnonymousClass3IN r57 = new AnonymousClass3IN(new AnonymousClass28G((C14900mE) r116.A8X.get(), A2N, new AnonymousClass28I(), r38, (C18640sm) r116.A3u.get()), new AnonymousClass4KA(new AnonymousClass4EC()));
                AnonymousClass4Q2 r410 = new AnonymousClass4Q2(new AnonymousClass4ED());
                return new CatalogSearchViewModel(new C90094Mo((C14650lo) r116.A2V.get(), (C14850m9) r116.A04.get()), (AnonymousClass1CR) this.A03.AGK.get(), r410, r57);
            case 44:
                AnonymousClass01J r411 = this.A03;
                C14900mE r58 = (C14900mE) r411.A8X.get();
                C15570nT r68 = (C15570nT) r411.AAr.get();
                C19990v2 r95 = (C19990v2) r411.A3M.get();
                AnonymousClass10Y r134 = (AnonymousClass10Y) r411.AAR.get();
                AnonymousClass12H r143 = (AnonymousClass12H) r411.AC5.get();
                AnonymousClass1E5 r211 = (AnonymousClass1E5) r411.AKs.get();
                AnonymousClass1EC r85 = (AnonymousClass1EC) r411.A3C.get();
                C22640zP r74 = (C22640zP) r411.A3Z.get();
                C15680nj r117 = (C15680nj) r411.A4e.get();
                AnonymousClass1E8 r118 = (AnonymousClass1E8) r411.ADy.get();
                return new CommunityTabViewModel(r58, r68, r74, r85, r95, (C21320xE) r411.A4Y.get(), r117, (C15600nX) r411.A8x.get(), r134, r143, (C14850m9) r411.A04.get(), (C244215l) r411.A8y.get(), r118, r211, (AbstractC14440lR) r411.ANe.get());
            case 45:
                AnonymousClass01J r119 = this.A03;
                AnonymousClass19T r39 = (AnonymousClass19T) r119.A2y.get();
                AnonymousClass19Q r212 = (AnonymousClass19Q) r119.A2u.get();
                return new ComplianceInfoViewModel((C19850um) r119.A2v.get(), r212, r39, (AbstractC14440lR) r119.ANe.get());
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                AnonymousClass01J r120 = this.A03;
                return new ConversationCommunityViewModel((C22640zP) r120.A3Z.get(), (C15600nX) r120.A8x.get(), (AbstractC14440lR) r120.ANe.get());
            case 47:
                AnonymousClass01J r121 = this.A03;
                return new CountryGatingViewModel((C22700zV) r121.AMN.get(), (C14850m9) r121.A04.get());
            case 48:
                AnonymousClass01J r124 = this.A03;
                Application A005 = AbstractC250617y.A00(r124.AO3);
                AnonymousClass1E2 r412 = (AnonymousClass1E2) r124.A0N.get();
                return new DevicePairQrScannerViewModel(A005, (C245716a) r124.AJR.get(), r412, (C26221Cm) r124.AJ1.get(), (AbstractC14440lR) r124.ANe.get());
            case 49:
                AnonymousClass01J r310 = this.A03;
                Application A006 = AbstractC250617y.A00(r310.AO3);
                C15550nR r86 = (C15550nR) r310.A45.get();
                AnonymousClass1B3 r69 = (AnonymousClass1B3) r310.AI0.get();
                AnonymousClass01J r213 = this.A02.A1O;
                AnonymousClass3E8 r75 = new AnonymousClass3E8((C26211Cl) r213.A5x.get(), (AbstractC14440lR) r213.ANe.get());
                return new DirectorySearchHistoryViewModel(A006, (C16430p0) r310.A5y.get(), r69, r75, r86, (C14830m7) r310.ALb.get());
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                AnonymousClass01J r125 = this.A03;
                AnonymousClass018 r610 = (AnonymousClass018) r125.ANb.get();
                C16430p0 r214 = (C16430p0) r125.A5y.get();
                AnonymousClass1B3 r59 = (AnonymousClass1B3) r125.AI0.get();
                return new DirectorySetNeighborhoodViewModel(r214, (AnonymousClass1CO) r125.AHw.get(), (AnonymousClass1B0) r125.A2K.get(), r59, r610, (AbstractC14440lR) r125.ANe.get());
            case 51:
                AnonymousClass01J r126 = this.A03;
                C16120oU r611 = (C16120oU) r126.ANE.get();
                C17220qS r76 = (C17220qS) r126.ABt.get();
                AnonymousClass01d r311 = (AnonymousClass01d) r126.ALI.get();
                AnonymousClass018 r510 = (AnonymousClass018) r126.ANb.get();
                C15820nx r215 = (C15820nx) r126.A6U.get();
                C18350sJ r87 = (C18350sJ) r126.AHX.get();
                return new EncBackupViewModel(r215, r311, (C14820m6) r126.AN3.get(), r510, r611, r76, r87, (AbstractC14440lR) r126.ANe.get());
            case 52:
                AnonymousClass01J r127 = this.A03;
                return new ExportMigrationViewModel((C14850m9) r127.A04.get(), (C20170vK) r127.ACS.get());
            case 53:
                AnonymousClass01J r128 = this.A03;
                return new GenericBkLayoutViewModel((C18640sm) r128.A3u.get(), C18000rk.A00(r128.A1j));
            case 54:
                AnonymousClass01J r129 = this.A03;
                return new GoogleDriveNewUserSetupViewModel((C26551Dx) r129.A8Z.get(), (AnonymousClass1D6) r129.A1G.get(), (C14820m6) r129.AN3.get());
            case 55:
                AnonymousClass01J r130 = this.A03;
                AnonymousClass116 r216 = (AnonymousClass116) r130.A3z.get();
                C15890o4 r511 = (C15890o4) r130.AN1.get();
                C18350sJ r88 = (C18350sJ) r130.AHX.get();
                C26031Bt r77 = (C26031Bt) r130.A8g.get();
                return new GoogleMigrateImporterViewModel(r216, (C18640sm) r130.A3u.get(), (C16590pI) r130.AMg.get(), r511, (C26061Bw) r130.A8e.get(), r77, r88, (C25661Ag) r130.A8G.get(), (C22900zp) r130.A7t.get());
            case 56:
                AnonymousClass01J r131 = this.A03;
                return new GroupSettingsViewModel((C15550nR) r131.A45.get(), (AbstractC14440lR) r131.ANe.get());
            case 57:
                AnonymousClass01J r217 = this.A03;
                C15550nR r413 = (C15550nR) r217.A45.get();
                C15610nY r512 = (C15610nY) r217.AMe.get();
                return new InCallBannerViewModel((C48782Ht) this.A01.A00.get(), r413, r512, (C14820m6) r217.AN3.get(), (C14850m9) r217.A04.get());
            case 58:
                AnonymousClass01J r135 = this.A03;
                C15570nT r1110 = (C15570nT) r135.AAr.get();
                AnonymousClass01J r218 = this.A02.A1O;
                C14900mE r312 = (C14900mE) r218.A8X.get();
                C1308460e r513 = (C1308460e) r218.A9c.get();
                C18610sj r78 = (C18610sj) r218.AF0.get();
                AnonymousClass6BE r89 = (AnonymousClass6BE) r218.A9X.get();
                return new IndiaUpiMapperLinkViewModel(AbstractC250617y.A00(r135.AO3), r1110, (C1329668y) r135.A9d.get(), new C120535gL(r312, (C16590pI) r218.AMg.get(), r513, (C18650sn) r218.AEe.get(), r78, r89, (C18590sh) r218.AES.get()), (AnonymousClass18O) r135.A9Y.get());
            case 59:
                AnonymousClass01J r02 = this.A03;
                Application A007 = AbstractC250617y.A00(r02.AO3);
                C15450nH r1210 = (C15450nH) r02.AII.get();
                C233411h r1111 = (C233411h) r02.AKz.get();
                C14820m6 r102 = (C14820m6) r02.AN3.get();
                C14840m8 r96 = (C14840m8) r02.ACi.get();
                C18640sm r810 = (C18640sm) r02.A3u.get();
                C22100yW r612 = (C22100yW) r02.A3g.get();
                AnonymousClass139 r514 = (AnonymousClass139) r02.A9o.get();
                C238813j r414 = (C238813j) r02.ABy.get();
                C18850tA r313 = (C18850tA) r02.AKx.get();
                C14890mD r219 = (C14890mD) r02.ANL.get();
                C22230yk r136 = (C22230yk) r02.ANT.get();
                return new LinkedDevicesSharedViewModel(A007, (C14900mE) r02.A8X.get(), r1210, r313, r1111, r810, r102, r612, (C245916c) r02.A5j.get(), r414, r514, r96, r136, (AbstractC14440lR) r02.ANe.get(), r219, (C14860mA) r02.ANU.get());
            case 60:
                AnonymousClass01J r137 = this.A03;
                C14840m8 r613 = (C14840m8) r137.ACi.get();
                return new LinkedDevicesViewModel(AbstractC250617y.A00(r137.AO3), (C14900mE) r137.A8X.get(), (C22100yW) r137.A3g.get(), (AnonymousClass139) r137.A9o.get(), r613, (AbstractC14440lR) r137.ANe.get(), (C14860mA) r137.ANU.get());
            case 61:
                AnonymousClass01J r138 = this.A03;
                Application A008 = AbstractC250617y.A00(r138.AO3);
                C251118d r314 = (C251118d) r138.A2D.get();
                C16430p0 r415 = (C16430p0) r138.A5y.get();
                AnonymousClass1B3 r614 = (AnonymousClass1B3) r138.AI0.get();
                return new LocationOptionPickerViewModel(A008, r314, r415, (AnonymousClass1B0) r138.A2K.get(), r614, (C16590pI) r138.AMg.get(), (C15890o4) r138.AN1.get());
            case 62:
                AnonymousClass01J r139 = this.A03;
                return new MenuBottomSheetViewModel((C15550nR) r139.A45.get(), (C15610nY) r139.AMe.get(), (C14850m9) r139.A04.get());
            case 63:
                AnonymousClass01J r140 = this.A03;
                return new MessageDetailsViewModel(AbstractC250617y.A00(r140.AO3), (C242014p) r140.A0O.get(), (C26221Cm) r140.AJ1.get(), (AbstractC14440lR) r140.ANe.get());
            case 64:
                AnonymousClass01J r141 = this.A03;
                C15650ng r315 = (C15650ng) r141.A4m.get();
                C26231Cn r220 = (C26231Cn) r141.AC8.get();
                return new MessageRatingViewModel((C26241Co) r141.AC7.get(), r315, r220, (AbstractC14440lR) r141.ANe.get());
            case 65:
                AnonymousClass01J r144 = this.A03;
                return new OrderInfoViewModel(AbstractC250617y.A00(r144.AO3), (C15570nT) r144.AAr.get(), (AnonymousClass018) r144.ANb.get());
            case 66:
                AnonymousClass01J r145 = this.A03;
                return new OrientationViewModel((C16590pI) r145.AMg.get(), (C17140qK) r145.AMZ.get(), r145.AGf);
            case 67:
                AnonymousClass01J r221 = this.A03;
                return new ParticipantsListViewModel((AbstractC15710nm) r221.A4o.get(), (C48782Ht) this.A01.A00.get(), (C15550nR) r221.A45.get(), (C15610nY) r221.AMe.get(), (C21250x7) r221.AJh.get(), (C20710wC) r221.A8m.get(), (C17140qK) r221.AMZ.get());
            case 68:
                AnonymousClass01J r146 = this.A03;
                C14900mE r222 = (C14900mE) r146.A8X.get();
                C15570nT r316 = (C15570nT) r146.AAr.get();
                C16170oZ r416 = (C16170oZ) r146.AM4.get();
                C20320vZ r811 = (C20320vZ) r146.A7A.get();
                return new PollCreatorViewModel(r222, r316, r416, (C14830m7) r146.ALb.get(), (C15650ng) r146.A4m.get(), (C14850m9) r146.A04.get(), r811, (AbstractC14440lR) r146.ANe.get());
            case 69:
                AnonymousClass01J r147 = this.A03;
                return new PollResultsViewModel((C15550nR) r147.A45.get(), (C15610nY) r147.AMe.get(), (C16590pI) r147.AMg.get());
            case 70:
                AnonymousClass01J r148 = this.A03;
                return new PollVoterViewModel((C16170oZ) r148.AM4.get(), (C15550nR) r148.A45.get(), (C15610nY) r148.AMe.get(), (C14830m7) r148.ALb.get(), (C16590pI) r148.AMg.get(), (AnonymousClass018) r148.ANb.get());
            case 71:
                AnonymousClass01J r149 = this.A03;
                return new PrivacyNoticeFragmentViewModel((C18640sm) r149.A3u.get(), C18000rk.A00(r149.A1j));
            case C43951xu.A02:
                AnonymousClass01J r150 = this.A03;
                return new ReactionsTrayViewModel((C15570nT) r150.AAr.get(), (C14850m9) r150.A04.get(), (C16630pM) r150.AIc.get(), (C26661Ei) r150.AHE.get());
            case 73:
                AnonymousClass01J r151 = this.A03;
                C14330lG r223 = (C14330lG) r151.A7B.get();
                C26251Cp r417 = (C26251Cp) r151.AHc.get();
                C15880o3 r515 = (C15880o3) r151.ACF.get();
                return new RestoreFromBackupViewModel(r223, (C25721Am) r151.A1B.get(), r417, r515, (AbstractC14440lR) r151.ANe.get());
            case 74:
                AnonymousClass01J r152 = this.A03;
                return new SettingsChatViewModel((C15880o3) r152.ACF.get(), (AbstractC14440lR) r152.ANe.get());
            case 75:
                AnonymousClass01J r153 = this.A03;
                return new SettingsDataUsageViewModel((C15810nw) r153.A73.get(), (C14850m9) r153.A04.get(), (AbstractC14440lR) r153.ANe.get());
            case 76:
                AnonymousClass01J r79 = this.A03;
                C14900mE r97 = (C14900mE) r79.A8X.get();
                C14330lG r812 = (C14330lG) r79.A7B.get();
                C15820nx r103 = (C15820nx) r79.A6U.get();
                AnonymousClass1D6 r418 = (AnonymousClass1D6) r79.A1G.get();
                C15880o3 r317 = (C15880o3) r79.ACF.get();
                C25721Am r1112 = (C25721Am) r79.A1B.get();
                C26551Dx r1410 = (C26551Dx) r79.A8Z.get();
                C14820m6 r224 = (C14820m6) r79.AN3.get();
                AnonymousClass10I r1211 = (AnonymousClass10I) r79.A1C.get();
                C18640sm r154 = (C18640sm) r79.A3u.get();
                C22730zY r1310 = (C22730zY) r79.A8Y.get();
                AnonymousClass10K r03 = (AnonymousClass10K) r79.A8c.get();
                return new SettingsGoogleDriveViewModel(r812, r97, r103, r1112, r1211, r1310, r1410, (AnonymousClass10J) r79.A8b.get(), r03, r418, r154, r224, r317, (C14850m9) r79.A04.get(), (AbstractC14440lR) r79.ANe.get());
            case 77:
                AnonymousClass01J r155 = this.A03;
                return new ShopsBkLayoutViewModel((C18640sm) r155.A3u.get(), C18000rk.A00(r155.A1j));
            case 78:
                AnonymousClass01J r156 = this.A03;
                return new ShopsProductPreviewFragmentViewModel((C16120oU) r156.ANE.get(), (AnonymousClass18X) r156.AIo.get());
            case 79:
                AnonymousClass01J r157 = this.A03;
                C20540vv r225 = (C20540vv) r157.AGI.get();
                return new SmbViewModel((C14820m6) r157.AN3.get(), r225, (C14850m9) r157.A04.get(), (AbstractC14440lR) r157.ANe.get());
            case 80:
                AnonymousClass01J r158 = this.A03;
                C15570nT r226 = (C15570nT) r158.AAr.get();
                AnonymousClass10Y r419 = (AnonymousClass10Y) r158.AAR.get();
                return new ToSGatingViewModel(r226, (C22700zV) r158.AMN.get(), r419, (C14850m9) r158.A04.get(), (AnonymousClass163) r158.ALf.get(), (C23000zz) r158.ALg.get());
            case 81:
                AnonymousClass01J r159 = this.A03;
                return new WaBkGalaxyLayoutViewModel((C18640sm) r159.A3u.get(), C18000rk.A00(r159.A1j));
            case 82:
                return new WaGalaxyNavBarViewModel((C130795zz) this.A03.A1i.get());
            default:
                throw new AssertionError(i);
        }
    }
}
