package X;

/* renamed from: X.2SL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2SL {
    public final C14820m6 A00;
    public final C16120oU A01;

    public AnonymousClass2SL(C14820m6 r1, C16120oU r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public final void A00(int i) {
        AnonymousClass30C r1 = new AnonymousClass30C();
        r1.A00 = Integer.valueOf(i);
        r1.A01 = 13;
        this.A01.A06(r1);
    }

    public final void A01(int i) {
        this.A00.A00.edit().putInt("biz_app_cross_sell_banner_consecutive_days", i).apply();
    }

    public final void A02(int i) {
        this.A00.A00.edit().putInt("biz_app_cross_sell_banner_cool_off_days", i).apply();
    }

    public void A03(Integer num, long j) {
        A00(4);
        C14820m6 r3 = this.A00;
        r3.A00.edit().putInt("biz_app_cross_sell_banner_expiry_days", num.intValue()).apply();
        r3.A0q("biz_app_cross_sell_banner_notif_time", j);
    }
}
