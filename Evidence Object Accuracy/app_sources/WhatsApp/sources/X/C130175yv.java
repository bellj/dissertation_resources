package X;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/* renamed from: X.5yv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130175yv {
    public Map A00 = Collections.emptyMap();
    public final CameraManager A01;
    public final C130055yj A02;
    public final C1308560f A03;
    public volatile C127565uh[] A04 = null;

    public C130175yv(CameraManager cameraManager, C130055yj r3, C1308560f r4) {
        this.A01 = cameraManager;
        this.A03 = r4;
        this.A02 = r3;
    }

    public int A00(int i) {
        try {
            return Integer.parseInt(A05(i).A03);
        } catch (CameraAccessException unused) {
            AnonymousClass616.A01("CameraInventory", "Failed to load CameraInfo to obtain camera id");
            return 0;
        }
    }

    public int A01(int i) {
        try {
            return A05(i).A02;
        } catch (CameraAccessException unused) {
            throw C12990iw.A0m("Could not get camera info, for orientation");
        }
    }

    public final int A02(int i) {
        if (this.A04 == null) {
            A07();
        }
        if (!(this.A04 == null || this.A04.length == 0)) {
            for (int i2 = 0; i2 < this.A04.length; i2++) {
                if (this.A04[i2].A00 == i) {
                    return i2;
                }
            }
            AnonymousClass616.A01("CameraInventory", C12960it.A0W(i, "Could not get CameraInfo for CameraFacing id: "));
        }
        return -1;
    }

    public int A03(int i, int i2) {
        if (i2 != -1) {
            try {
                C127565uh A05 = A05(i);
                int i3 = ((i2 + 45) / 90) * 90;
                if (A05.A01 == 0) {
                    return ((A05.A02 - i3) + 360) % 360;
                }
                return (A05.A02 + i3) % 360;
            } catch (CameraAccessException e) {
                AnonymousClass616.A01("CameraInventory", C12960it.A0d(e.getMessage(), C12960it.A0k("Failed to get info to calculate media rotation: ")));
            }
        }
        return 0;
    }

    public int A04(String str) {
        if (this.A04 == null) {
            A07();
        }
        int length = this.A04.length;
        for (int i = 0; i < length; i++) {
            C127565uh r1 = this.A04[i];
            if (r1.A03.equals(str)) {
                return r1.A00;
            }
        }
        AnonymousClass616.A01("CameraInventory", C12960it.A0d(str, C12960it.A0k("Failed to find camera facing for id: ")));
        return 0;
    }

    public final C127565uh A05(int i) {
        if (this.A04 == null) {
            A07();
        }
        int A02 = A02(i);
        if (A02 != -1) {
            return this.A04[A02];
        }
        throw C12970iu.A0f("Camera facing did not resolve to a camera info instance");
    }

    public String A06(int i) {
        try {
            return A05(i).A03;
        } catch (CameraAccessException e) {
            throw new RuntimeException("Failed to get camera info", e);
        }
    }

    public final void A07() {
        if (this.A04 == null) {
            C1308560f r2 = this.A03;
            if (r2.A09()) {
                A08();
                return;
            }
            try {
                r2.A01(new C118905cZ(), new IDxCallableShape15S0100000_3_I1(this, 13)).get();
            } catch (InterruptedException | ExecutionException e) {
                AnonymousClass616.A01("CameraInventory", C12960it.A0d(e.getMessage(), C12960it.A0k("failed to load camera infos: ")));
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: X.5uh[] */
    /* JADX WARN: Multi-variable type inference failed */
    public final void A08() {
        CameraManager cameraManager = this.A01;
        String[] cameraIdList = cameraManager.getCameraIdList();
        HashMap A11 = C12970iu.A11();
        int i = 0;
        for (String str : cameraIdList) {
            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(str);
            int A05 = C12960it.A05(cameraCharacteristics.get(CameraCharacteristics.LENS_FACING));
            int i2 = 0;
            if (A05 != 1) {
                i2 = 1;
            }
            Map map = this.A00;
            Integer valueOf = Integer.valueOf(i2);
            if ((map.containsKey(valueOf) && C12970iu.A0t(valueOf, this.A00).equals(str)) || !A11.containsKey(valueOf)) {
                A11.put(valueOf, new C127565uh(str, i2, A05, C12960it.A05(cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION))));
            }
        }
        C127565uh[] r3 = new C127565uh[A11.size()];
        Iterator A0s = C12990iw.A0s(A11);
        while (A0s.hasNext()) {
            r3[i] = C12970iu.A15(A0s).getValue();
            i++;
        }
        this.A04 = r3;
    }

    public boolean A09(Integer num) {
        if (this.A04 == null) {
            A07();
        }
        if (this.A04 == null) {
            AnonymousClass616.A01("CameraInventory", "Failed to detect camera, cameraInfos was null");
        } else {
            int i = 1;
            if (num.intValue() == 1) {
                i = 0;
            }
            if (A02(i) != -1) {
                return true;
            }
        }
        return false;
    }
}
