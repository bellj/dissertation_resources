package X;

import android.view.View;
import android.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.0dA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09570dA implements Runnable {
    public final /* synthetic */ AnonymousClass0FH A00;
    public final /* synthetic */ ArrayList A01;

    public RunnableC09570dA(AnonymousClass0FH r1, ArrayList arrayList) {
        this.A00 = r1;
        this.A01 = arrayList;
    }

    @Override // java.lang.Runnable
    public void run() {
        View view;
        ArrayList arrayList = this.A01;
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C05140Ok r6 = (C05140Ok) it.next();
            AnonymousClass0FH r5 = this.A00;
            AnonymousClass03U r0 = r6.A05;
            View view2 = null;
            if (r0 == null) {
                view = null;
            } else {
                view = r0.A0H;
            }
            AnonymousClass03U r02 = r6.A04;
            if (r02 != null) {
                view2 = r02.A0H;
            }
            if (view != null) {
                ViewPropertyAnimator duration = view.animate().setDuration(r5.A05());
                r5.A02.add(r6.A05);
                duration.translationX((float) (r6.A02 - r6.A00));
                duration.translationY((float) (r6.A03 - r6.A01));
                duration.alpha(0.0f).setListener(new AnonymousClass09H(view, duration, r6, r5)).start();
            }
            if (view2 != null) {
                ViewPropertyAnimator animate = view2.animate();
                r5.A02.add(r6.A04);
                animate.translationX(0.0f).translationY(0.0f).setDuration(r5.A05()).alpha(1.0f).setListener(new AnonymousClass09I(view2, animate, r6, r5)).start();
            }
        }
        arrayList.clear();
        this.A00.A03.remove(arrayList);
    }
}
