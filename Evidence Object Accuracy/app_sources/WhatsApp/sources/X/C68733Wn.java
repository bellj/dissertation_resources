package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import org.json.JSONException;

/* renamed from: X.3Wn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68733Wn implements AnonymousClass2DH {
    public final /* synthetic */ C17730rI A00;
    public final /* synthetic */ C65963Lt A01;
    public final /* synthetic */ C129625y2 A02;
    public final /* synthetic */ String A03;

    public C68733Wn(C17730rI r1, C65963Lt r2, C129625y2 r3, String str) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = str;
    }

    public static /* synthetic */ void A00(C17730rI r7, C65963Lt r8, C129625y2 r9, String str) {
        C16700pc.A0F(r7, r9);
        C16700pc.A0E(str, 3);
        C20440vl r3 = r7.A02;
        String str2 = r3.A00;
        if (str2 == null) {
            throw C16700pc.A06("appId");
        }
        String A08 = C16700pc.A08(str2, "_layout.json");
        StringBuilder A0k = C12960it.A0k("phoenix_bloks_layout");
        A0k.append((Object) File.separator);
        File A00 = r3.A00(C12960it.A0d(A08, A0k));
        if (A00 != null) {
            try {
                byte[] A002 = AnonymousClass1NM.A00(A00);
                C16700pc.A0B(A002);
                try {
                    String string = C13000ix.A05(new String(A002, C88844Hi.A05)).getJSONObject("data").getJSONObject("whatsapp_pmtd_bloks_getprivatelayout").getString("payload");
                    if (string != null) {
                        r9.A01(string);
                        if (r8 != null) {
                            C18840t8 r2 = r7.A04;
                            String str3 = r8.A01;
                            StringBuilder A0j = C12960it.A0j(str);
                            A0j.append(":8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666:");
                            r2.A03(string, str3, C12970iu.A0s(C12970iu.A14(r7.A01), A0j), Long.valueOf(r8.A00).longValue(), r8.A02);
                            return;
                        }
                        return;
                    }
                } catch (JSONException e) {
                    Log.e("PhoenixCustomBloksRequest/processRawJsonString", e);
                }
            } catch (IOException e2) {
                Log.e("PhoenixBloksAssetDownloader/loadBloksLayoutJsonString", e2);
            }
        }
        r9.A00(C12960it.A0U("Failed to load a Phoenix Bloks layout"));
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
        this.A02.A00(C12960it.A0U("Failed to load a Phoenix Bloks layout"));
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        this.A02.A00(C12960it.A0U("Failed to load a Phoenix Bloks layout"));
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        C17730rI r5 = this.A00;
        r5.A03.Ab2(new Runnable(this.A01, this.A02, this.A03) { // from class: X.3lE
            public final /* synthetic */ C65963Lt A01;
            public final /* synthetic */ C129625y2 A02;
            public final /* synthetic */ String A03;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A03 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C68733Wn.A00(C17730rI.this, this.A01, this.A02, this.A03);
            }
        });
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        this.A02.A00(C12960it.A0U("Failed to load a Phoenix Bloks layout"));
    }
}
