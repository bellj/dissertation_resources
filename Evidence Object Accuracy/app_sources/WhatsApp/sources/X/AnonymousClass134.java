package X;

import android.database.Cursor;
import android.os.SystemClock;
import com.whatsapp.util.Log;

/* renamed from: X.134  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass134 {
    public final C14830m7 A00;
    public final C16510p9 A01;
    public final C19990v2 A02;
    public final C20290vW A03;
    public final C16490p7 A04;

    public AnonymousClass134(C14830m7 r1, C16510p9 r2, C19990v2 r3, C20290vW r4, C16490p7 r5) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
    }

    public int A00(AbstractC14640lm r10, long j) {
        long uptimeMillis = SystemClock.uptimeMillis();
        C16310on A01 = this.A04.get();
        try {
            int i = 0;
            Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') AND sort_id > ?", new String[]{String.valueOf(this.A01.A02(r10)), String.valueOf(j)});
            if (A09.moveToNext()) {
                i = A09.getInt(0);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("msgstore/getnewercount/db no message for ");
                sb.append(r10);
                Log.i(sb.toString());
            }
            A09.close();
            A01.close();
            this.A03.A00("SortIdStore/getMessagesNewerThanCount", SystemClock.uptimeMillis() - uptimeMillis);
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public int A01(AbstractC14640lm r8, long j) {
        C16310on A01 = this.A04.get();
        try {
            int i = 0;
            Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') AND (message_type = '10') AND sort_id > ?", new String[]{String.valueOf(this.A01.A02(r8)), String.valueOf(j)});
            if (A09.moveToNext()) {
                i = A09.getInt(0);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("msgstore/getnewercount/db no message for ");
                sb.append(r8);
                Log.i(sb.toString());
            }
            A09.close();
            A01.close();
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public int A02(AbstractC14640lm r6, long j, long j2) {
        int i = 0;
        String[] strArr = {String.valueOf(this.A01.A02(r6)), Long.toString(j), Long.toString(j2)};
        C16310on A01 = this.A04.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT COUNT(*) FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') AND sort_id > ? AND sort_id <= ?", strArr);
            if (A09.moveToNext()) {
                i = A09.getInt(0);
            }
            A09.close();
            A01.close();
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        if (r2 != null) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A03(long r6) {
        /*
            r5 = this;
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]
            java.lang.String r0 = java.lang.String.valueOf(r6)
            r4 = 0
            r2[r4] = r0
            X.0p7 r0 = r5.A04
            X.0on r3 = r0.get()
            X.0op r1 = r3.A03     // Catch: all -> 0x0035
            java.lang.String r0 = "SELECT sort_id FROM message_view WHERE _id = ?"
            android.database.Cursor r2 = r1.A09(r0, r2)     // Catch: all -> 0x0035
            if (r2 == 0) goto L_0x002a
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x0025
            if (r0 == 0) goto L_0x002a
            long r0 = r2.getLong(r4)     // Catch: all -> 0x0025
            goto L_0x002e
        L_0x0025:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0029
        L_0x0029:
            throw r0     // Catch: all -> 0x0035
        L_0x002a:
            r0 = -9223372036854775808
            if (r2 == 0) goto L_0x0031
        L_0x002e:
            r2.close()     // Catch: all -> 0x0035
        L_0x0031:
            r3.close()
            return r0
        L_0x0035:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0039
        L_0x0039:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass134.A03(long):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        if (r2 != null) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A04(long r7) {
        /*
            r6 = this;
            X.0p7 r0 = r6.A04
            X.0on r5 = r0.get()
            X.0op r4 = r5.A03     // Catch: all -> 0x0035
            java.lang.String r3 = "SELECT sort_id FROM available_message_view WHERE timestamp <= ? ORDER BY sort_id DESC LIMIT 1"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch: all -> 0x0035
            java.lang.String r0 = java.lang.Long.toString(r7)     // Catch: all -> 0x0035
            r1 = 0
            r2[r1] = r0     // Catch: all -> 0x0035
            android.database.Cursor r2 = r4.A09(r3, r2)     // Catch: all -> 0x0035
            if (r2 == 0) goto L_0x002a
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x0025
            if (r0 == 0) goto L_0x002a
            long r0 = r2.getLong(r1)     // Catch: all -> 0x0025
            goto L_0x002e
        L_0x0025:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0029
        L_0x0029:
            throw r0     // Catch: all -> 0x0035
        L_0x002a:
            r0 = 0
            if (r2 == 0) goto L_0x0031
        L_0x002e:
            r2.close()     // Catch: all -> 0x0035
        L_0x0031:
            r5.close()
            return r0
        L_0x0035:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x0039
        L_0x0039:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass134.A04(long):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
        if (r2 != null) goto L_0x0057;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A05(X.AbstractC14640lm r7) {
        /*
            r6 = this;
            X.0v2 r0 = r6.A02
            X.1PE r5 = r0.A06(r7)
            r3 = -9223372036854775808
            if (r5 != 0) goto L_0x000b
            return r3
        L_0x000b:
            long r1 = r5.A0G
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x006a
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]
            X.0p9 r0 = r6.A01
            long r0 = r0.A02(r7)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4 = 0
            r2[r4] = r0
            X.0p7 r0 = r6.A04
            X.0on r3 = r0.get()
            X.0op r1 = r3.A03     // Catch: all -> 0x0062
            java.lang.String r0 = "   SELECT sort_id FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') ORDER BY sort_id ASC LIMIT 1"
            android.database.Cursor r2 = r1.A09(r0, r2)     // Catch: all -> 0x0062
            if (r2 != 0) goto L_0x0034
            java.lang.String r0 = "msgstore/getfirstsortref/cursor is null"
            goto L_0x0052
        L_0x0034:
            boolean r0 = r2.moveToFirst()     // Catch: all -> 0x005b
            if (r0 == 0) goto L_0x0041
            long r0 = r2.getLong(r4)     // Catch: all -> 0x005b
            r5.A0G = r0     // Catch: all -> 0x005b
            goto L_0x0057
        L_0x0041:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x005b
            r1.<init>()     // Catch: all -> 0x005b
            java.lang.String r0 = "msgstore/getfirstsortref can't get value for "
            r1.append(r0)     // Catch: all -> 0x005b
            r1.append(r7)     // Catch: all -> 0x005b
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x005b
        L_0x0052:
            com.whatsapp.util.Log.w(r0)     // Catch: all -> 0x005b
            if (r2 == 0) goto L_0x0067
        L_0x0057:
            r2.close()     // Catch: all -> 0x0062
            goto L_0x0067
        L_0x005b:
            r0 = move-exception
            if (r2 == 0) goto L_0x0061
            r2.close()     // Catch: all -> 0x0061
        L_0x0061:
            throw r0     // Catch: all -> 0x0062
        L_0x0062:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0066
        L_0x0066:
            throw r0
        L_0x0067:
            r3.close()
        L_0x006a:
            long r0 = r5.A0G
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass134.A05(X.0lm):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        if (r2 != null) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A06(X.AbstractC14640lm r6) {
        /*
            r5 = this;
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]
            X.0p9 r0 = r5.A01
            long r0 = r0.A02(r6)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r4 = 0
            r2[r4] = r0
            X.0p7 r0 = r5.A04
            X.0on r3 = r0.get()
            X.0op r1 = r3.A03     // Catch: all -> 0x003b
            java.lang.String r0 = "   SELECT sort_id FROM available_message_view WHERE chat_row_id = ? ORDER BY sort_id DESC LIMIT 1"
            android.database.Cursor r2 = r1.A09(r0, r2)     // Catch: all -> 0x003b
            if (r2 == 0) goto L_0x0030
            boolean r0 = r2.moveToNext()     // Catch: all -> 0x002b
            if (r0 == 0) goto L_0x0030
            long r0 = r2.getLong(r4)     // Catch: all -> 0x002b
            goto L_0x0034
        L_0x002b:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x002f
        L_0x002f:
            throw r0     // Catch: all -> 0x003b
        L_0x0030:
            r0 = -9223372036854775808
            if (r2 == 0) goto L_0x0037
        L_0x0034:
            r2.close()     // Catch: all -> 0x003b
        L_0x0037:
            r3.close()
            return r0
        L_0x003b:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x003f
        L_0x003f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass134.A06(X.0lm):long");
    }

    public boolean A07(AbstractC14640lm r6, long j) {
        AnonymousClass009.A05(r6);
        String[] strArr = {String.valueOf(this.A01.A02(r6))};
        C16310on A01 = this.A04.get();
        try {
            Cursor A09 = A01.A03.A09("   SELECT sort_id FROM available_message_view WHERE chat_row_id = ? AND (message_type != '8') ORDER BY sort_id ASC LIMIT 1", strArr);
            if (A09.moveToFirst()) {
                long j2 = A09.getLong(0);
                A09.close();
                A01.close();
                if (j2 != Long.MIN_VALUE && j2 < j) {
                    return true;
                }
            } else {
                A09.close();
                A01.close();
                StringBuilder sb = new StringBuilder("SortIdStore/ getFirstSortId can't get value for ");
                sb.append(r6);
                Log.w(sb.toString());
            }
            return false;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
