package X;

import android.graphics.PointF;
import android.util.Property;
import android.view.View;

/* renamed from: X.0Ak  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02200Ak extends Property {
    public C02200Ak() {
        super(PointF.class, "topLeft");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return null;
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        View view = (View) obj;
        PointF pointF = (PointF) obj2;
        AnonymousClass0U3.A04.A06(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
    }
}
