package X;

/* renamed from: X.33q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C620433q extends AbstractC1112458q {
    public final /* synthetic */ AnonymousClass5WA A00;
    public final /* synthetic */ AnonymousClass17Q A01;
    public final /* synthetic */ AnonymousClass3FB A02;
    public final /* synthetic */ AnonymousClass3BH A03;

    public C620433q(AnonymousClass5WA r1, AnonymousClass17Q r2, AnonymousClass3FB r3, AnonymousClass3BH r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // X.AbstractC1112458q, X.AbstractC21730xt
    public void AP1(String str) {
        C16700pc.A0E(str, 0);
        AnonymousClass17Q r1 = this.A01;
        r1.A06.A02.A05(this.A02.hashCode(), 105);
        this.A00.AQE(new AnonymousClass3FA("XMPP not connected", null, 1));
    }

    @Override // X.AbstractC1112458q, X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r7, String str) {
        C16700pc.A0F(str, r7);
        AnonymousClass3BH r2 = this.A03;
        AnonymousClass17Q r5 = this.A01;
        AnonymousClass3EW r0 = new AnonymousClass3EJ(r5.A02, r7, r2).A01;
        C16700pc.A0B(r0);
        AnonymousClass3FA A00 = C63163An.A00(r0);
        AnonymousClass3FB r3 = this.A02;
        int hashCode = r3.hashCode();
        Long valueOf = Long.valueOf(A00.A00);
        AnonymousClass17L r02 = r5.A06;
        AnonymousClass17Q.A00(r02, r5, valueOf, hashCode);
        r02.A02.A05(r3.hashCode(), 467);
        this.A00.AQE(A00);
    }

    @Override // X.AbstractC1112458q, X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r7, String str) {
        C16700pc.A0E(r7, 1);
        AnonymousClass17Q r3 = this.A01;
        AnonymousClass3FB r5 = this.A02;
        int hashCode = r5.hashCode();
        AnonymousClass1Q5 r4 = r3.A06.A02;
        r4.A03(hashCode, "iqResponse");
        AnonymousClass3EI r2 = new AnonymousClass3E6(r3.A02, r7, this.A03).A01;
        C16700pc.A0B(r2);
        r4.A05(r5.hashCode(), 467);
        AnonymousClass3E2 r0 = r2.A01.A03;
        if (r0 != null) {
            for (AnonymousClass3EH r02 : r0.A01.A01) {
                C16700pc.A0B(r02);
                AnonymousClass17Q.A01(r3, r02);
            }
        }
        this.A00.AQF(r2);
    }
}
