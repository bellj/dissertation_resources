package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.stickers.StickerStoreMyTabFragment;
import com.whatsapp.stickers.StickerStoreTabFragment;
import java.util.Collections;

/* renamed from: X.2g9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54082g9 extends AbstractC06200So {
    public final /* synthetic */ StickerStoreMyTabFragment A00;

    @Override // X.AbstractC06200So
    public boolean A04() {
        return false;
    }

    @Override // X.AbstractC06200So
    public boolean A05() {
        return false;
    }

    public C54082g9(StickerStoreMyTabFragment stickerStoreMyTabFragment) {
        this.A00 = stickerStoreMyTabFragment;
    }

    @Override // X.AbstractC06200So
    public int A01(AnonymousClass03U r5, RecyclerView recyclerView) {
        return (3 << 16) | (48 << 8) | (51 << 0);
    }

    @Override // X.AbstractC06200So
    public boolean A07(AnonymousClass03U r8, AnonymousClass03U r9, RecyclerView recyclerView) {
        int A00 = r8.A00();
        int A002 = r9.A00();
        StickerStoreMyTabFragment stickerStoreMyTabFragment = this.A00;
        if (A002 >= ((StickerStoreTabFragment) stickerStoreMyTabFragment).A0E.size() || A002 < 0 || A00 >= ((StickerStoreTabFragment) stickerStoreMyTabFragment).A0E.size() || A00 < 0) {
            return false;
        }
        if (A002 == 0) {
            AnonymousClass1KZ r1 = (AnonymousClass1KZ) ((StickerStoreTabFragment) stickerStoreMyTabFragment).A0E.get(A002);
            if (r1.A0N || "meta-avatar".equals(r1.A0D)) {
                A002 = 1;
            }
        }
        int i = A00;
        if (A00 < A002) {
            while (i < A002) {
                int i2 = i + 1;
                Collections.swap(((StickerStoreTabFragment) stickerStoreMyTabFragment).A0E, i, i2);
                i = i2;
            }
        } else {
            while (i > A002) {
                int i3 = i - 1;
                Collections.swap(((StickerStoreTabFragment) stickerStoreMyTabFragment).A0E, i, i3);
                i = i3;
            }
        }
        stickerStoreMyTabFragment.A05 = true;
        ((AnonymousClass02M) ((StickerStoreTabFragment) stickerStoreMyTabFragment).A0D).A01.A01(A00, A002);
        return true;
    }
}
