package X;

import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0wU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20890wU {
    public final C16590pI A00;

    public C20890wU(C16590pI r1) {
        this.A00 = r1;
    }

    public static File A00(C16590pI r2, String str) {
        File cacheDir = r2.A00.getCacheDir();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".health");
        return new File(cacheDir, sb.toString());
    }

    public List A01() {
        ArrayList arrayList = new ArrayList();
        if (!A04("breakpad")) {
            arrayList.add("breakpad");
        }
        if (!A04("anr_detector")) {
            arrayList.add("anr_detector");
        }
        if (!A04("abort_hook")) {
            arrayList.add("abort_hook");
        }
        return arrayList;
    }

    public void A02(Runnable runnable, String str) {
        if (!A04(str)) {
            StringBuilder sb = new StringBuilder("Skipping module ");
            sb.append(str);
            sb.append(" since its unhealthy");
            Log.w(sb.toString());
            return;
        }
        StringBuilder sb2 = new StringBuilder("Loading module: ");
        sb2.append(str);
        Log.i(sb2.toString());
        try {
            A00(this.A00, str).createNewFile();
        } catch (IOException e) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Error creating health file for ");
            sb3.append(str);
            Log.e(sb3.toString(), e);
        }
        long elapsedRealtime = SystemClock.elapsedRealtime();
        runnable.run();
        long elapsedRealtime2 = SystemClock.elapsedRealtime();
        A03(str);
        StringBuilder sb4 = new StringBuilder("Module loaded: ");
        sb4.append(str);
        sb4.append(" load time: ");
        sb4.append(elapsedRealtime2 - elapsedRealtime);
        Log.i(sb4.toString());
    }

    public void A03(String str) {
        boolean delete = A00(this.A00, str).delete();
        StringBuilder sb = new StringBuilder();
        sb.append("Module ");
        sb.append(str);
        sb.append(" health file deleted: ");
        sb.append(delete);
        Log.i(sb.toString());
    }

    public boolean A04(String str) {
        return !A00(this.A00, str).exists();
    }
}
