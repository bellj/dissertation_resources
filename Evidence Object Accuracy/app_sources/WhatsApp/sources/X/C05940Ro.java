package X;

import java.util.concurrent.Executor;

/* renamed from: X.0Ro  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05940Ro {
    public static final C05940Ro A03 = new C05940Ro(null, null);
    public C05940Ro A00;
    public final Runnable A01;
    public final Executor A02;

    public C05940Ro(Runnable runnable, Executor executor) {
        this.A01 = runnable;
        this.A02 = executor;
    }
}
