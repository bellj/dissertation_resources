package X;

/* renamed from: X.388  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass388 extends AbstractC16350or {
    public C16520pA A00;
    public C16520pA A01;
    public boolean A02;
    public boolean A03;
    public final C15650ng A04;
    public final C15660nh A05;
    public final AbstractC14640lm A06;
    public final AnonymousClass2Am A07;
    public final AbstractC16130oV A08;

    public AnonymousClass388(C15650ng r1, C15660nh r2, AbstractC14640lm r3, AnonymousClass2Am r4, AbstractC16130oV r5) {
        this.A06 = r3;
        this.A08 = r5;
        this.A07 = r4;
        this.A04 = r1;
        this.A05 = r2;
    }
}
