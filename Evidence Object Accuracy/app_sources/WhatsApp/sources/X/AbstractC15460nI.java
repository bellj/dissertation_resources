package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0nI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15460nI {
    public static int A06;
    public static long A07;
    public static long A08;
    public static C16140oW A09 = A01(Integer.MIN_VALUE, 172800, "privatestats_token_first_delay_seconds", "privatestats_token_first_delay_seconds", 21600);
    public static C16140oW A0A = A01(43200, Integer.MAX_VALUE, "privatestats_token_max_expiry_seconds", "privatestats_token_max_expiry_seconds", 86400);
    public static C16140oW A0B = A01(1, 43200, "privatestats_token_prec_lead_seconds", "privatestats_token_prec_lead_seconds", 7200);
    public static C16140oW A0C = A01(2, Integer.MAX_VALUE, "privatestats_token_pre_redeem_count", "privatestats_token_pre_redeem_count", 388);
    public static C16140oW A0D = A01(0, 150, "privatestats_upload_jitter_secs", "privatestats_upload_jitter_secs", 35);
    public static String A0E;
    public static String A0F;
    public static final C15470nJ A0G = A00("abprops_encryption", false);
    public static final C15470nJ A0H = A00("abprops_prefs_file_rollback_usage_enabled", false);
    public static final C15470nJ A0I = A00("abprops_rollback_enabled", false);
    public static final C15470nJ A0J = A00("async_init_fts_migration", false);
    public static final C15470nJ A0K = A00("async_init_md_migrations", true);
    public static final C15470nJ A0L = A00("audio_data_for_notification", true);
    public static final C15470nJ A0M = A00("bsp_system_message_enabled", false);
    public static final C15470nJ A0N = A00("ctwa_first_business_reply_logging", false);
    public static final C15470nJ A0O = A00("delete_bad_gcm_token", false);
    public static final C15470nJ A0P = A00("enhanced_storage_mgmt_sort_fw_score", false);
    public static final C15470nJ A0Q;
    public static final C15470nJ A0R = A00("fbns_disabled_for_secondary", false);
    public static final C15470nJ A0S = A00("fbns_enabled", false);
    public static final C15470nJ A0T = A00("force_long_connect", false);
    public static final C15470nJ A0U = A00("frequently_forwarded_group_setting", false);
    public static final C15470nJ A0V = A00("gif_search_v2", false);
    public static final C15470nJ A0W = A00("google_drive_enabled", true);
    public static final C15470nJ A0X = A00("group_message_notification_use_jid_instead_of_from_me", true);
    public static final C15470nJ A0Y = A00("grp_uii_cleanup", false);
    public static final C15470nJ A0Z = A00("disable_hfm_autodownload", false);
    public static final C15470nJ A0a = A00("instrument_spam_report_enabled", true);
    public static final C15470nJ A0b = A00("linked_devices_re_auth_enabled", false);
    public static final C15470nJ A0c = A00("linked_devices_title_enabled", false);
    public static final C15470nJ A0d = A00("linked_devices_ui_enabled", false);
    public static final C15470nJ A0e = A00("syncd_android_unsupported_mutation_enabled", false);
    public static final C15470nJ A0f = A00("syncd_clear_chat_delete_chat_enabled", false);
    public static final C15470nJ A0g = A00("syncd_one_time_cleanup_for_non_md_user", false);
    public static final C15470nJ A0h = A00("syncd_patch_device_index_included", false);
    public static final C15470nJ A0i = A00("md_pin_chat_enabled", false);
    public static final C15470nJ A0j = A00("md_voip_enabled", false);
    public static final C15470nJ A0k = A00("mms_forward_uploading_media_enabled", false);
    public static final C15470nJ A0l = A00("mms_vcache_aggregation_enabled", false);
    public static final C15470nJ A0m = A00("mute_always", false);
    public static final C15470nJ A0n = A00("notif_ch_override_off", false);
    public static final C15470nJ A0o = A00("p2m_pay", false);
    public static final C15470nJ A0p = A00("p2p_pay", false);
    public static final C15470nJ A0q = A00("payments_cs_email_disabled", false);
    public static final C15470nJ A0r = A00("payments_deeplink_signup_enabled", false);
    public static final C15470nJ A0s = A00("payments_disable_switch_psp", false);
    public static final C15470nJ A0t = A00("novi_p2p", false);
    public static final C15470nJ A0u = A00("payments_request_messages", true);
    public static final C15470nJ A0v = A00("payments_upi_qr_signing", false);
    public static final C15470nJ A0w = A00("payments_upi_settings_privacy_banner_enabled", true);
    public static final C15470nJ A0x = A00("payments_upi_enable_sim_swap_detection", false);
    public static final C15470nJ A0y = A00("payments_upi_view_in_inbox", false);
    public static final C15470nJ A0z = A00("payment_history_fts_enabled", false);
    public static final C15470nJ A10;
    public static final C15470nJ A11 = A00("profilo_enabled", false);
    public static final C15470nJ A12 = A00("qr_scanning", false);
    public static final C15470nJ A13 = A00("quick_message_search_enabled", false);
    public static final C15470nJ A14 = A00("receipt_processing_dedup", true);
    public static final C15470nJ A15 = A00("receipt_processing_thread", false);
    public static final C15470nJ A16 = A00("reg_log_advertiser_id", false);
    public static final C15470nJ A17 = A00("smb_upsell_chat_banner_enabled", false);
    public static final C15470nJ A18 = A00("stella_interop_enabled", false);
    public static final C15470nJ A19;
    public static final C15470nJ A1A = A00("track_battery_metrics", false);
    public static final C15470nJ A1B = A00("wa_msys_crypto", true);
    public static final C15470nJ A1C = A00("wa_msys_fingerprint", false);
    public static final AnonymousClass1NJ A1D;
    public static final C16140oW A1E = A01(null, null, "abprops_revert_bg_crash_count", "abprops_revert_bg_crash_count", 10);
    public static final C16140oW A1F = A01(null, null, "abprops_revert_fg_crash_count", "abprops_revert_fg_crash_count", 5);
    public static final C16140oW A1G = A01(null, null, "announcement_toggle_time_hours", "announcement_toggle_time_hours", 72);
    public static final C16140oW A1H = A01(null, null, "biz_block_reasons_version", "biz_block_reasons_version", 0);
    public static final C16140oW A1I = A01(256, null, "broadcast_list_size_limit", "max_list_recipients", 256);
    public static final C16140oW A1J = A01(null, null, "biz_profile_options", "biz_profile_options", 4);
    public static final C16140oW A1K = A01(null, null, "max_keys", "max_keys", 812);
    public static final C16140oW A1L = A01(null, null, "direct_db_migration_timeout_in_secs", "direct_db_migration_timeout_in_secs", 180);
    public static final C16140oW A1M = A01(null, null, "gif_provider", "gif_provider", -1);
    public static final C16140oW A1N = A01(null, null, "group_call_callee_ring_duration", "group_call_callee_ring_duration", 23);
    public static final C16140oW A1O = A01(null, null, "group_call_max_participants", "group_call_max_participants", 4);
    public static final C16140oW A1P = A01(null, null, "group_description_length", "group_description_length", 0);
    public static final C16140oW A1Q = A01(null, null, "hq_image_bw_threshold", "hq_image_bw_threshold", 75);
    public static final C16140oW A1R = A01(null, null, "hq_image_max_edge", "hq_image_max_edge", 0);
    public static final C16140oW A1S = A01(null, null, "hq_image_quality", "hq_image_quality", 0);
    public static final C16140oW A1T = A01(null, null, "image_max_edge", "image_max_edge", 1600);
    public static final C16140oW A1U = A01(Integer.valueOf((int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), null, "image_max_kbytes", "image_max_kbytes", EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    public static final C16140oW A1V = A01(null, null, "image_quality", "image_quality", 80);
    public static final C16140oW A1W = A01(null, null, "linked_devices_max_count", "linked_devices_max_count", 4);
    public static final C16140oW A1X = A01(null, null, "md_mhfs_days", "md_mhfs_days", 732);
    public static final C16140oW A1Y = A01(null, null, "md_mhfs_limit", "md_mhfs_limit", 200);
    public static final C16140oW A1Z = A01(0, 100, "md_mhfs_start_progress", "md_mhfs_start_progress", 70);
    public static final C16140oW A1a = A01(null, null, "md_mhrs_days", "md_mhrs_days", 90);
    public static final C16140oW A1b = A01(1, 5, "syncd_additional_mutations_for_key_catch_up", "syncd_additional_mutations_for_key_catch_up", 1);
    public static final C16140oW A1c = A01(100, 5000, "syncd_critical_contacts_limit", "syncd_critical_contacts_limit", 1000);
    public static final C16140oW A1d = A01(0, 7, "syncd_fatal_error_timeout_days", "syncd_fatal_error_timeout_days", 0);
    public static final C16140oW A1e = A01(100, 2000, "syncd_inline_mutations_max_count", "syncd_inline_mutations_max_count", 100);
    public static final C16140oW A1f = A01(0, 43200, "syncd_keep_alive_mins", "syncd_keep_alive_mins", 1440);
    public static final C16140oW A1g = A01(1, 90, "syncd_key_max_use_days", "syncd_key_max_use_days", 30);
    public static final C16140oW A1h = A01(15, 90, "syncd_key_stale_days", "syncd_key_stale_days", 31);
    public static final C16140oW A1i = A01(10, 100, "syncd_patch_protobuf_max_size", "syncd_patch_protobuf_max_size", 10);
    public static final C16140oW A1j = A01(0, 20, "syncd_sentinel_timeout_seconds", "syncd_sentinel_timeout_seconds", 3);
    public static final C16140oW A1k = A01(1, 15, "syncd_wait_for_key_timeout_days", "syncd_wait_for_key_timeout_days", 7);
    public static final C16140oW A1l = A01(10, 300, "critical_payload_download_and_applied_timeout_secs", "critical_payload_download_and_applied_timeout_secs", 120);
    public static final C16140oW A1m = A01(10, 300, "critical_payload_upload_timeout_secs", "critical_payload_upload_timeout_secs", 60);
    public static final C16140oW A1n = A01(0, null, "media_limit_auto_download_mb", "media_max_autodownload", 32);
    public static final C16140oW A1o = A01(null, null, "media_limit_auto_download_wifi_mb", "media_max_autodownload_wifi_mb", 100);
    public static final C16140oW A1p = A01(16, null, "media_limit_mb", "media", 16);
    public static final C16140oW A1q = A01(null, null, "mms_hot_content_time_span", "mms_hot_content_timespan_in_seconds", 0);
    public static final C16140oW A1r = A01(null, null, "mms_vcard_autodownload_size_kb", "mms_vcard_autodownload_size_kb", EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    public static final C16140oW A1s = A01(null, null, "one_tap_calling_in_group_chat_size", "one_tap_calling_in_group_chat_size", 4);
    public static final C16140oW A1t = A01(0, 300, "overnight_alarms_jitter_limit_in_sec", "overnight_alarms_jitter_limit_in_sec", 0);
    public static final C16140oW A1u = A01(null, null, "partial_pjpeg_bw_threshold", "partial_pjpeg_bw_threshold", 125);
    public static final C16140oW A1v = A01(257, null, "participants_size_limit", "max_participants", 257);
    public static final C16140oW A1w = A01(null, null, "payments_br_transaction_limit", "payments_transaction_limit", 1000);
    public static final C16140oW A1x = A01(null, null, "payments_upi_first_day_max_transaction_limit", "payments_upi_first_day_max_transaction_limit", 5000);
    public static final C16140oW A1y = A01(null, null, "payments_upi_generate_qr_amount_limit", "payments_upi_generate_qr_amount_limit", 5000);
    public static final C16140oW A1z = A01(null, null, "payments_upi_intent_transaction_limit", "payments_upi_intent_transaction_limit", 2000);
    public static final C16140oW A20 = A01(null, null, "payments_upi_transaction_limit", "payments_upi_transaction_limit", 5000);
    public static final C16140oW A21 = A01(null, null, "payments_upi_transaction_limit_request", "payments_upi_transaction_limit_request", 5000);
    public static final C16140oW A22 = A01(null, null, "ping_timeout_s", "ping_timeout_s", 32);
    public static final C16140oW A23 = A01(30, 180, "prekey_expiration_days", "prekey_expiration_days", 60);
    public static final C16140oW A24 = A01(null, null, "ptt_playback_speed_hide_delay", "ptt_playback_speed_hide_delay", 1500);
    public static final C16140oW A25 = A01(null, null, "shops_required_tos_version", "shops_required_tos_version", 0);
    public static final C16140oW A26 = A01(null, null, "status_image_max_edge", "status_image_max_edge", 1280);
    public static final C16140oW A27 = A01(null, null, "status_image_quality", "status_image_quality", 50);
    public static final C16140oW A28 = A01(null, null, "status_video_max_duration", "status_video_max_duration", 45);
    public static final C16140oW A29 = A01(0, 20, "stella_addressbook_restriction_type", "stella_addressbook_restriction_type", 10);
    public static final C16140oW A2A = A01(25, null, "subject_length_limit", "max_subject", 25);
    public static final C16140oW A2B = A01(null, null, "sync_wifi_threshold_kb", "sync_wifi_threshold_kb", 5000);
    public static final C16140oW A2C = A01(null, null, "template_doc_mime_types_int", "template_doc_mime_types", 0);
    public static final C16140oW A2D = A01(null, null, "vcard_as_document_size_kb", "vcard_as_document_size_kb", 0);
    public static final C16140oW A2E = A01(null, null, "vcard_max_size_kb", "vcard_max_size_kb", EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    public static final C16140oW A2F = A01(null, null, "video_max_bitrate", "video_max_bitrate", 5000);
    public static final C16140oW A2G = A01(null, null, "vname_cert_staleness_threshold", "vname_cert_staleness_threshold", 43200);
    public static final C16140oW A2H = A01(0, 500, "web_max_size_kb", "web_max_size_kb", 100);
    public static final C16140oW A2I = A01(null, null, "web_service_delay", "web_service_delay", 120);
    public static final AnonymousClass1ML A2J;
    public static final AnonymousClass1ML A2K;
    public static final AnonymousClass1ML A2L;
    public static final AnonymousClass1ML A2M;
    public static final AnonymousClass1ML A2N;
    public static final List A2O = new ArrayList();
    public static final List A2P = new ArrayList();
    public final SharedPreferences A00;
    public final C22780zd A01;
    public final AnonymousClass16N A02;
    public final AnonymousClass16M A03;
    public final C16590pI A04;
    public final AnonymousClass15Y A05;

    static {
        AnonymousClass1ML r1 = new AnonymousClass1ML("payments_cs_brazil_jid", "551130421648@s.whatsapp.net");
        A2P.add(r1);
        A2K = r1;
        AnonymousClass1ML r12 = new AnonymousClass1ML("payments_cs_faq_url", "https://faq.whatsapp.com/payments");
        A2P.add(r12);
        A2M = r12;
        AnonymousClass1ML r13 = new AnonymousClass1ML("payments_cs_email_address", null);
        A2P.add(r13);
        A2L = r13;
        AnonymousClass1ML r14 = new AnonymousClass1ML("payments_cs_phone_number", null);
        A2P.add(r14);
        A2N = r14;
        C15470nJ r15 = new C15470nJ("storage_migration_enabled", "ssm_enabled", false);
        A2P.add(r15);
        A19 = r15;
        AnonymousClass1ML r16 = new AnonymousClass1ML("ephemeral_messages_allowed_values", "604800");
        A2P.add(r16);
        A2J = r16;
        C15470nJ r17 = new C15470nJ("portal_optimization_enabled", "p_opt", false);
        A2P.add(r17);
        A10 = r17;
        C15470nJ r18 = new C15470nJ("enable_export", "enable_export", true);
        A2O.add(r18);
        A0Q = r18;
        AnonymousClass1NJ r19 = new AnonymousClass1NJ();
        A2P.add(r19);
        A1D = r19;
    }

    public AbstractC15460nI(C22780zd r6, AnonymousClass16N r7, AnonymousClass16M r8, C16590pI r9, AnonymousClass15Y r10, C16630pM r11) {
        SharedPreferences A02 = r11.A02("server_prop_preferences");
        this.A04 = r9;
        this.A03 = r8;
        this.A05 = r10;
        this.A00 = A02;
        this.A02 = r7;
        this.A01 = r6;
        synchronized (AbstractC15460nI.class) {
            A06 = A02.getInt("server_props:last_version", 0);
            A08 = A02.getLong("groups_server_props_last_refresh_time", 0);
            A0F = A02.getString("server_props:config_hash", null);
            A0E = A02.getString("server_props:config_key", null);
            A07 = A02.getLong("server_props:refresh", 86400000);
        }
    }

    public static C15470nJ A00(String str, boolean z) {
        C15470nJ r1 = new C15470nJ(str, str, z);
        A2P.add(r1);
        return r1;
    }

    public static C16140oW A01(Integer num, Integer num2, String str, String str2, int i) {
        C16140oW r1 = new C16140oW(num, num2, str, str2, i);
        A2P.add(r1);
        return r1;
    }

    public int A02(C16140oW r5) {
        int i;
        synchronized (AbstractC15460nI.class) {
            i = this.A00.getInt(((AbstractC15480nK) r5).A00, r5.A00);
        }
        return i;
    }

    public String A03(AnonymousClass1ML r5) {
        String string;
        synchronized (AbstractC15460nI.class) {
            string = this.A00.getString(((AbstractC15480nK) r5).A00, r5.A00);
        }
        return string;
    }

    public void A04(C14830m7 r10, String str, String str2, Map map, int i, int i2, long j) {
        boolean z;
        ArrayList arrayList;
        SharedPreferences sharedPreferences = this.A00;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        synchronized (AbstractC15460nI.class) {
            A08 = r10.A00();
            A06 = i;
            edit.putInt("server_props:last_version", i);
            long max = Math.max(j, 600000L);
            A07 = max;
            edit.putLong("server_props:refresh", max);
            z = false;
            if (i2 == 1 || !TextUtils.isEmpty(str)) {
                Iterator it = this.A02.A01().iterator();
                while (it.hasNext()) {
                    it.next();
                }
                for (AbstractC15480nK r0 : A2P) {
                    r0.A00(edit, map);
                }
                AnonymousClass16M r1 = this.A03;
                SharedPreferences.Editor edit2 = r1.A00.edit();
                synchronized (r1.A01) {
                    arrayList = new ArrayList(AnonymousClass16M.A02.values());
                }
                Iterator it2 = arrayList.iterator();
                if (it2.hasNext()) {
                    it2.next();
                    throw new NullPointerException("remoteKey");
                }
                edit2.apply();
                if (sharedPreferences.getBoolean("server_props:one_time_unlocked", true)) {
                    for (AbstractC15480nK r02 : A2O) {
                        r02.A00(edit, map);
                    }
                    edit.putBoolean("server_props:one_time_unlocked", false);
                }
                z = true;
            }
            if (i2 == 2) {
                A0E = str2;
                edit.putString("server_props:config_key", str2);
                AnonymousClass15Y r2 = this.A05;
                r2.A02(str2, 2141, 0);
                r2.A02(str2, 2141, 1);
                if (!TextUtils.isEmpty(str)) {
                    A0F = str;
                    edit.putString("server_props:config_hash", str);
                }
            } else {
                A0E = null;
                edit.remove("server_props:config_key");
                AnonymousClass15Y r22 = this.A05;
                r22.A02(null, 2141, 0);
                r22.A02(null, 2141, 1);
                A0F = null;
                edit.remove("server_props:config_hash");
            }
            edit.putLong("groups_server_props_last_refresh_time", A08);
            edit.apply();
        }
        C22780zd r12 = this.A01;
        ((SharedPreferences) r12.A01.get()).edit().putString("server_props:hash", AnonymousClass123.A00(((SharedPreferences) r12.A02.get()).getAll())).apply();
        if (z) {
            for (AnonymousClass1GI r3 : this.A02.A01()) {
                r3.A00.A0e.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(r3, 21));
            }
        }
    }

    public boolean A05(C15470nJ r5) {
        boolean z;
        synchronized (AbstractC15460nI.class) {
            z = this.A00.getBoolean(((AbstractC15480nK) r5).A00, r5.A00);
        }
        return z;
    }
}
