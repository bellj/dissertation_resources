package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3Ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63073Ad {
    public static List A00(Long l, int[] iArr) {
        long longValue;
        ArrayList A0l = C12960it.A0l();
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        int i = 0;
        while (true) {
            int length = iArr.length;
            if (i >= length) {
                break;
            }
            j += (long) iArr[i];
            if (i != length - 1 || l == null) {
                if (j > j2) {
                    int i2 = (int) ((((j - j2) + 15) / 16) * 16);
                    C12980iv.A1R(A0l, i2);
                    j2 = ((long) i2) + j2;
                    j3 = j2;
                }
                i++;
            } else {
                if (j > j2) {
                    longValue = l.longValue() - j2;
                } else {
                    A0l.remove(A0l.size() - 1);
                    longValue = l.longValue() - j3;
                }
                C12980iv.A1R(A0l, (int) longValue);
            }
        }
        return A0l;
    }
}
