package X;

/* renamed from: X.1kb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36801kb extends AnonymousClass015 {
    public C15370n3 A00;
    public final C27131Gd A01;
    public final AnonymousClass10S A02;
    public final C20830wO A03;
    public final AnonymousClass1E5 A04;
    public final C27691It A05 = new C27691It();

    public C36801kb(AnonymousClass10S r2, C20830wO r3, C15370n3 r4, AnonymousClass1E5 r5) {
        C36721kR r0 = new C36721kR(this);
        this.A01 = r0;
        this.A00 = r4;
        this.A02 = r2;
        this.A04 = r5;
        this.A03 = r3;
        r2.A03(r0);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A02.A04(this.A01);
    }
}
