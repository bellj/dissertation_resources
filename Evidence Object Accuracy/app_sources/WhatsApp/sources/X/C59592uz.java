package X;

/* renamed from: X.2uz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59592uz extends C37171lc {
    public final AbstractView$OnClickListenerC34281fs A00;
    public final String A01;
    public final String A02;

    public C59592uz(AbstractView$OnClickListenerC34281fs r2, String str, String str2) {
        super(AnonymousClass39o.A0b);
        this.A02 = str;
        this.A00 = r2;
        this.A01 = str2;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass() || !super.equals(obj) || !this.A01.equals(((C59592uz) obj).A01)) {
                return false;
            }
            String str = this.A02;
            if (!str.equals(str)) {
                return false;
            }
        }
        return true;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A01.hashCode();
    }
}
