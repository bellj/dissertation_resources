package X;

import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import com.whatsapp.jid.GroupJid;
import java.util.Locale;

/* renamed from: X.1da  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33181da {
    public int A00 = 0;
    public long A01;
    public long A02;
    public long A03;
    public C33191db A04;
    public C33191db A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public final C15450nH A0J;
    public final AnonymousClass01d A0K;
    public final C14830m7 A0L;
    public final C15890o4 A0M;
    public final C15860o1 A0N;

    public C33181da(C15450nH r2, AnonymousClass01d r3, C14830m7 r4, C15890o4 r5, C15860o1 r6) {
        this.A0L = r4;
        this.A0J = r2;
        this.A0K = r3;
        this.A0N = r6;
        this.A0M = r5;
    }

    public long A00() {
        return A01();
    }

    public long A01() {
        long j = this.A02;
        if (j == -1) {
            return j;
        }
        if (j > this.A0L.A00()) {
            return this.A02;
        }
        return 0;
    }

    public C33181da A02() {
        String str = this.A0C;
        if ("group_chat_defaults".equals(str) || "individual_chat_defaults".equals(str)) {
            return this;
        }
        GroupJid nullable = GroupJid.getNullable(str);
        C15860o1 r0 = this.A0N;
        if (nullable != null) {
            return r0.A04();
        }
        return r0.A05();
    }

    public String A03() {
        String str;
        if ((this.A0I && (str = this.A06) != null) || (str = A02().A06) != null) {
            if (C14350lI.A0I(Uri.parse(str), this.A0K, this.A0M, false)) {
                return str;
            }
        }
        return Settings.System.DEFAULT_NOTIFICATION_URI.toString();
    }

    public String A04() {
        if (!this.A0I || TextUtils.isEmpty(this.A07)) {
            return A02().A07;
        }
        return this.A07;
    }

    public String A05() {
        if (!this.A0I || TextUtils.isEmpty(this.A08)) {
            return A02().A08;
        }
        return this.A08;
    }

    public String A06() {
        if (Build.VERSION.SDK_INT >= 29) {
            return Integer.toString(0);
        }
        if (!this.A0I || TextUtils.isEmpty(this.A09)) {
            return A02().A09;
        }
        return this.A09;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (X.C14350lI.A0I(android.net.Uri.parse(r4), r5.A0K, r5.A0M, false) != false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A07() {
        /*
            r5 = this;
            boolean r0 = r5.A0I
            if (r0 == 0) goto L_0x0008
            java.lang.String r4 = r5.A0A
            if (r4 != 0) goto L_0x0010
        L_0x0008:
            X.1da r0 = r5.A02()
            java.lang.String r4 = r0.A0A
            if (r4 == 0) goto L_0x0026
        L_0x0010:
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 >= r0) goto L_0x0025
            X.01d r3 = r5.A0K
            X.0o4 r2 = r5.A0M
            android.net.Uri r1 = android.net.Uri.parse(r4)
            r0 = 0
            boolean r0 = X.C14350lI.A0I(r1, r3, r2, r0)
            if (r0 == 0) goto L_0x0026
        L_0x0025:
            return r4
        L_0x0026:
            android.net.Uri r0 = android.provider.Settings.System.DEFAULT_NOTIFICATION_URI
            java.lang.String r4 = r0.toString()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33181da.A07():java.lang.String");
    }

    public String A08() {
        if (!this.A0I || TextUtils.isEmpty(this.A0B)) {
            return A02().A0B;
        }
        return this.A0B;
    }

    public boolean A09() {
        return A00() != 0;
    }

    public boolean A0A() {
        return this.A0G || !A09();
    }

    public boolean A0B() {
        if (!this.A0I) {
            return A02().A0D;
        }
        return this.A0D;
    }

    public String toString() {
        return String.format(Locale.ENGLISH, "jid: %s deleted:%d muteEndTime:%d showNotificationWhenMuted:%b useCustomNotification:%b messageTone:%s messageVibrate:%s messagePopup:%s messageLight:%s callTone:%s callVibrate:%s statusMuted:%b pinned:%b pinned_time:%d lowPriorityNotifications:%b mediaVisibility:%d muteReactions:%b", C15380n4.A04(this.A0C), Long.valueOf(this.A01), Long.valueOf(this.A02), Boolean.valueOf(this.A0G), Boolean.valueOf(this.A0I), this.A0A, this.A0B, this.A09, this.A08, this.A06, this.A07, Boolean.valueOf(this.A0H), Boolean.valueOf(this.A0F), Long.valueOf(this.A03), Boolean.valueOf(this.A0D), Integer.valueOf(this.A00), Boolean.valueOf(this.A0E));
    }
}
