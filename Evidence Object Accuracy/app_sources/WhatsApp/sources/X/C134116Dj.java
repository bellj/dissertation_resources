package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.6Dj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134116Dj implements AnonymousClass5Wu {
    public View A00;
    public TextView A01;
    public TextView A02;
    public TextView A03;
    public TextView A04;
    public ThumbnailButton A05;

    /* renamed from: A00 */
    public void A6Q(AnonymousClass4OZ r6) {
        if (r6 != null) {
            int i = r6.A00;
            if (!(i == -2 || i == -1)) {
                if (i == 0) {
                    this.A00.setVisibility(8);
                    return;
                } else if (!(i == 1 || i == 2)) {
                    return;
                }
            }
            this.A00.setVisibility(0);
            C1315363d r2 = (C1315363d) r6.A01;
            if (r2 != null) {
                this.A04.setText(r2.A03);
                this.A01.setText(r2.A00);
                CharSequence charSequence = r2.A01;
                TextView textView = this.A02;
                if (charSequence == null) {
                    textView.setVisibility(8);
                    this.A03.setVisibility(8);
                    return;
                }
                textView.setVisibility(0);
                this.A02.setText(charSequence);
                this.A03.setText(r2.A02);
            }
        }
    }

    @Override // X.AnonymousClass5Wu
    public int ADq() {
        return R.layout.novi_withdraw_review_amount;
    }

    @Override // X.AnonymousClass5Wu
    public void AYL(View view) {
        this.A00 = view;
        this.A05 = (ThumbnailButton) AnonymousClass028.A0D(view, R.id.novi_withdraw_review_profile_photo);
        this.A04 = C12960it.A0I(view, R.id.novi_withdraw_review_title);
        this.A01 = C12960it.A0I(view, R.id.novi_withdraw_review_amount);
        this.A02 = C12960it.A0I(view, R.id.novi_withdraw_review_amount_crypto);
        this.A03 = C12960it.A0I(view, R.id.novi_withdraw_review_amount_rate);
    }
}
