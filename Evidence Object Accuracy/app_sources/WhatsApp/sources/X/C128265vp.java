package X;

import android.content.Context;

/* renamed from: X.5vp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128265vp {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final AnonymousClass102 A03;
    public final C17220qS A04;
    public final C18650sn A05;
    public final C18610sj A06;
    public final C129385xd A07;
    public final String A08;
    public final String A09;
    public final String A0A;

    public C128265vp(Context context, C14900mE r2, C18640sm r3, AnonymousClass102 r4, C17220qS r5, C18650sn r6, C18610sj r7, C129385xd r8, String str, String str2, String str3) {
        this.A00 = context;
        this.A01 = r2;
        this.A04 = r5;
        this.A07 = r8;
        this.A06 = r7;
        this.A03 = r4;
        this.A02 = r3;
        this.A05 = r6;
        this.A0A = str;
        this.A08 = str2;
        this.A09 = str3;
    }
}
