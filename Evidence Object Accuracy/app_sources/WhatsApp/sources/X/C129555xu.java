package X;

/* renamed from: X.5xu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129555xu {
    public String A00;
    public String A01;
    public String A02;

    public int hashCode() {
        return -1;
    }

    public C129555xu(String str, String str2, String str3) {
        this.A02 = str;
        this.A01 = str2;
        this.A00 = str3;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C129555xu r5 = (C129555xu) obj;
            if (!this.A02.equals(r5.A02) || !this.A01.equals(r5.A01)) {
                return false;
            }
            String str = r5.A00;
            String str2 = this.A00;
            if (str == null) {
                if (str2 != null) {
                    return false;
                }
            } else if (str.equals(str2)) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
