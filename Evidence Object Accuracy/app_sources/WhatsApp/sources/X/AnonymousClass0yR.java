package X;

import android.content.SharedPreferences;
import java.util.Calendar;
import java.util.TimeZone;

/* renamed from: X.0yR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass0yR implements AbstractC22060yS, AbstractC18270sB {
    public long A00;
    public long A01;
    public long A02;
    public final C20640w5 A03;
    public final C14830m7 A04;
    public final C22050yP A05;
    public final C16100oS A06;
    public final AnonymousClass0yQ A07;

    public AnonymousClass0yR(C20640w5 r1, C14830m7 r2, C22050yP r3, C16100oS r4, AnonymousClass0yQ r5) {
        this.A04 = r2;
        this.A03 = r1;
        this.A05 = r3;
        this.A06 = r4;
        this.A07 = r5;
    }

    public void A00() {
        long A00 = this.A04.A00() / 1000;
        long j = this.A00;
        if (A00 != j) {
            boolean z = false;
            if (A00 < j) {
                z = true;
            }
            long max = Math.max(j, A00);
            if (A00 - j > 30 || z) {
                A02();
                if (A05(A00) || z) {
                    this.A07.A00().edit().putLong("timespent_end_time", max).apply();
                    A04(z);
                    A03(A00);
                }
                this.A02 = A00;
            }
            this.A00 = A00;
        }
    }

    public void A01() {
        long j;
        long j2;
        AnonymousClass0yQ r4 = this.A07;
        if (r4.A00().getLong("timespent_saved_start_time", 0) > 0) {
            long j3 = r4.A00().getLong("timespent_summary_sequence", 0);
            C22050yP r12 = this.A05;
            long j4 = r4.A00().getLong("timespent_saved_start_time", 0);
            long j5 = r4.A00().getLong("timespent_saved_duration", 0);
            long j6 = r4.A00().getLong("timespent_saved_session_total", 0);
            long j7 = r4.A00().getLong("timespent_saved_foreground_count", 0);
            boolean z = r4.A00().getBoolean("timespent_saved_time_altered", false);
            AnonymousClass21N r1 = new AnonymousClass21N();
            r1.A03 = Long.valueOf(j4);
            r1.A00 = Long.valueOf(j5);
            r1.A02 = Long.valueOf(j6);
            Long valueOf = Long.valueOf(j3);
            r1.A05 = valueOf;
            r1.A01 = Long.valueOf(j7);
            if (z) {
                j = 1;
            } else {
                j = 0;
            }
            r1.A04 = Long.valueOf(j);
            r12.A0G.A06(r1);
            C16100oS r5 = this.A06;
            long j8 = r4.A00().getLong("timespent_saved_start_time", 0);
            long j9 = r4.A00().getLong("timespent_saved_duration", 0);
            long j10 = r4.A00().getLong("timespent_saved_session_total", 0);
            long j11 = r4.A00().getLong("timespent_saved_foreground_count", 0);
            boolean z2 = r4.A00().getBoolean("timespent_saved_time_altered", false);
            AnonymousClass21O r122 = new AnonymousClass21O();
            r122.A03 = Long.valueOf(j8);
            r122.A00 = Long.valueOf(j9);
            r122.A02 = Long.valueOf(j10);
            r122.A05 = valueOf;
            r122.A01 = Long.valueOf(j11);
            if (z2) {
                j2 = 1;
            } else {
                j2 = 0;
            }
            r122.A04 = Long.valueOf(j2);
            r5.A02.A06(r122);
            r4.A00().edit().putLong("timespent_saved_start_time", 0).putLong("timespent_saved_duration", 0).putLong("timespent_saved_session_total", 0).putLong("timespent_saved_foreground_count", 0).putBoolean("timespent_saved_time_altered", false).putLong("timespent_summary_sequence", (j3 % 9999) + 1).apply();
        }
    }

    public final void A02() {
        long j = this.A02;
        if (j > 0) {
            long j2 = this.A00;
            long j3 = ((j2 - j) + 1) - this.A01;
            AnonymousClass0yQ r2 = this.A07;
            r2.A00().edit().putLong("timespent_last_activity_time", j2).putLong("timespent_session_total", r2.A00().getLong("timespent_session_total", 0) + j3).apply();
            this.A02 = 0;
            this.A00 = 0;
            this.A01 = 0;
        }
    }

    public final void A03(long j) {
        long time = this.A03.A01().getTime() / 1000;
        if (1659716253 <= j && time >= j) {
            AnonymousClass0yQ r6 = this.A07;
            if (r6.A00().getLong("timespent_start_time", 0) == 0) {
                r6.A00().edit().putLong("timespent_start_time", j).apply();
            }
        }
    }

    public final void A04(boolean z) {
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("PST8PDT"));
        instance.add(6, 1);
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        AnonymousClass0yQ r11 = this.A07;
        r11.A00().edit().putLong("timespent_rollover_time", instance.getTimeInMillis() / 1000).apply();
        A01();
        SharedPreferences.Editor edit = r11.A00().edit();
        long j = r11.A00().getLong("timespent_start_time", 0);
        if (j > 0) {
            edit.putLong("timespent_saved_start_time", j).putLong("timespent_saved_duration", (r11.A00().getLong("timespent_end_time", 0) - j) + 1).putLong("timespent_saved_session_total", r11.A00().getLong("timespent_session_total", 0)).putLong("timespent_saved_foreground_count", r11.A00().getLong("timespent_foreground_count", 0)).putBoolean("timespent_saved_time_altered", z);
        }
        edit.putLong("timespent_start_time", 0).putLong("timespent_session_total", 0).putLong("timespent_end_time", 0).putLong("timespent_foreground_count", 0).apply();
    }

    public final boolean A05(long j) {
        return this.A07.A00().getLong("timespent_rollover_time", 0) < j || j < this.A00;
    }

    @Override // X.AbstractC22060yS
    public void AMF() {
        long A00 = this.A04.A00() / 1000;
        long j = this.A00;
        boolean z = false;
        if (A00 < j) {
            z = true;
        }
        long max = Math.max(j, A00);
        A02();
        this.A07.A00().edit().putLong("timespent_end_time", max).apply();
        if (A05(A00) || z) {
            A04(z);
        }
    }

    @Override // X.AbstractC22060yS
    public void AMG() {
        long j;
        long A00 = this.A04.A00() / 1000;
        AnonymousClass0yQ r6 = this.A07;
        long j2 = r6.A00().getLong("timespent_last_activity_time", 0);
        this.A00 = j2;
        int i = (A00 > j2 ? 1 : (A00 == j2 ? 0 : -1));
        boolean z = false;
        if (i < 0) {
            z = true;
        }
        if (A05(A00) || z) {
            A04(z);
        }
        A03(A00);
        if (A00 == this.A00) {
            j = 1;
        } else {
            j = 0;
        }
        this.A01 = j;
        this.A02 = A00;
        this.A00 = A00;
        r6.A00().edit().putLong("timespent_foreground_count", r6.A00().getLong("timespent_foreground_count", 0) + 1).apply();
    }

    @Override // X.AbstractC18270sB
    public void ARF() {
        A01();
    }
}
