package X;

import android.hardware.camera2.CameraDevice;
import java.util.List;
import java.util.UUID;

/* renamed from: X.5Zx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117435Zx extends CameraDevice.StateCallback implements AnonymousClass6MJ {
    public CameraDevice A00;
    public C128445w7 A01;
    public C125415rD A02;
    public AnonymousClass6L0 A03;
    public Boolean A04;
    public final C129875yR A05;

    public C117435Zx(C128445w7 r4, C125415rD r5) {
        this.A01 = r4;
        this.A02 = r5;
        C129875yR r2 = new C129875yR();
        this.A05 = r2;
        r2.A02(0);
    }

    /* renamed from: A00 */
    public CameraDevice AGH() {
        Boolean bool = this.A04;
        if (bool == null) {
            throw C12960it.A0U("Open Camera operation hasn't completed yet.");
        } else if (bool.booleanValue()) {
            return this.A00;
        } else {
            throw this.A03;
        }
    }

    @Override // X.AnonymousClass6MJ
    public void A6e() {
        this.A05.A00();
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onClosed(CameraDevice cameraDevice) {
        super.onClosed(cameraDevice);
        this.A00 = null;
        C128445w7 r0 = this.A01;
        if (r0 != null) {
            r0.A00(cameraDevice);
        }
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onDisconnected(CameraDevice cameraDevice) {
        if (this.A00 == null) {
            this.A04 = Boolean.FALSE;
            this.A03 = new AnonymousClass6L0("Could not open camera. Operation disconnected.");
            this.A05.A01();
            return;
        }
        C125415rD r0 = this.A02;
        if (r0 != null) {
            AnonymousClass662 r4 = r0.A00;
            List list = r4.A0a.A00;
            UUID uuid = r4.A0c.A03;
            r4.A0d.A05(new RunnableC135736Jp(r4, list, uuid), uuid);
        }
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onError(CameraDevice cameraDevice, int i) {
        if (this.A00 == null) {
            this.A04 = Boolean.FALSE;
            this.A03 = new AnonymousClass6L0(C12960it.A0W(i, "Could not open camera. Operation error: "));
            this.A05.A01();
            return;
        }
        C125415rD r0 = this.A02;
        if (r0 != null) {
            AnonymousClass662 r4 = r0.A00;
            List list = r4.A0a.A00;
            UUID uuid = r4.A0c.A03;
            r4.A0d.A05(new RunnableC135736Jp(r4, list, uuid), uuid);
        }
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onOpened(CameraDevice cameraDevice) {
        this.A04 = Boolean.TRUE;
        this.A00 = cameraDevice;
        this.A05.A01();
    }
}
