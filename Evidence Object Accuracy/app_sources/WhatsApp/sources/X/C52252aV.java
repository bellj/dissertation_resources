package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import java.lang.ref.WeakReference;

/* renamed from: X.2aV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52252aV extends ImageSpan {
    public WeakReference A00;

    public C52252aV(Drawable drawable) {
        super(drawable);
    }

    public static CharSequence A00(Paint paint, Drawable drawable, CharSequence charSequence) {
        int indexOf = TextUtils.indexOf(charSequence, "%s");
        SpannableStringBuilder A0J = C12990iw.A0J(charSequence);
        A02(paint, drawable, A0J, -1, indexOf, "%s".length() + indexOf);
        return A0J;
    }

    public static CharSequence A01(Paint paint, Drawable drawable, CharSequence charSequence) {
        SpannableStringBuilder A0J = C12990iw.A0J(C12960it.A0b("  ", charSequence));
        A02(paint, drawable, A0J, -1, 0, 1);
        return A0J;
    }

    public static void A02(Paint paint, Drawable drawable, SpannableStringBuilder spannableStringBuilder, int i, int i2, int i3) {
        if (i <= 0) {
            i = (int) paint.getTextSize();
        }
        drawable.setBounds(0, 0, (drawable.getIntrinsicWidth() * i) / drawable.getIntrinsicHeight(), i);
        spannableStringBuilder.setSpan(new C52252aV(drawable), i2, i3, 33);
    }

    public Drawable A03() {
        Drawable drawable;
        WeakReference weakReference = this.A00;
        if (weakReference != null && (drawable = (Drawable) weakReference.get()) != null) {
            return drawable;
        }
        Drawable drawable2 = getDrawable();
        this.A00 = C12970iu.A10(drawable2);
        return drawable2;
    }

    @Override // android.text.style.DynamicDrawableSpan, android.text.style.ReplacementSpan
    public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        Drawable A03 = A03();
        canvas.save();
        Rect bounds = A03.getBounds();
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        canvas.translate(f, ((float) i4) + (((fontMetrics.ascent + fontMetrics.descent) - ((float) bounds.height())) / 2.0f));
        A03.draw(canvas);
        canvas.restore();
    }
}
