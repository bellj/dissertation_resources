package X;

import java.util.HashMap;

/* renamed from: X.0fO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10900fO extends HashMap<String, Object> {
    public final /* synthetic */ AnonymousClass0IU this$0;
    public final /* synthetic */ String val$surface = null;

    public C10900fO(AnonymousClass0IU r3) {
        this.this$0 = r3;
        put("surface", null);
        put("bytes_downloaded", Long.valueOf(((AbstractC08590bS) r3).A01.get()));
        put("cache_hit_count", Long.valueOf(r3.A00.get()));
        put("cache_miss_count", Long.valueOf(r3.A01.get()));
    }
}
