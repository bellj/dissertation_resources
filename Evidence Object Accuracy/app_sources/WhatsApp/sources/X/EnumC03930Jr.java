package X;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0Jr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03930Jr {
    REMOVED,
    VISIBLE,
    GONE,
    INVISIBLE;

    public static EnumC03930Jr A00(int i) {
        if (i == 0) {
            return VISIBLE;
        }
        if (i == 4) {
            return INVISIBLE;
        }
        if (i == 8) {
            return GONE;
        }
        StringBuilder sb = new StringBuilder("Unknown visibility ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public static EnumC03930Jr A01(View view) {
        if (view.getAlpha() == 0.0f && view.getVisibility() == 0) {
            return INVISIBLE;
        }
        return A00(view.getVisibility());
    }

    public void A02(View view) {
        int i;
        int[] iArr = AnonymousClass0MB.A01;
        int ordinal = ordinal();
        int i2 = iArr[ordinal];
        if (ordinal != 0) {
            if (i2 == 2) {
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("SpecialEffectsController: Setting view ");
                    sb.append(view);
                    sb.append(" to VISIBLE");
                    Log.v("FragmentManager", sb.toString());
                }
                i = 0;
            } else if (i2 == 3) {
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("SpecialEffectsController: Setting view ");
                    sb2.append(view);
                    sb2.append(" to GONE");
                    Log.v("FragmentManager", sb2.toString());
                }
                i = 8;
            } else if (i2 == 4) {
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: Setting view ");
                    sb3.append(view);
                    sb3.append(" to INVISIBLE");
                    Log.v("FragmentManager", sb3.toString());
                }
                view.setVisibility(4);
                return;
            } else {
                return;
            }
            view.setVisibility(i);
            return;
        }
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb4 = new StringBuilder("SpecialEffectsController: Removing view ");
                sb4.append(view);
                sb4.append(" from container ");
                sb4.append(viewGroup);
                Log.v("FragmentManager", sb4.toString());
            }
            viewGroup.removeView(view);
        }
    }
}
