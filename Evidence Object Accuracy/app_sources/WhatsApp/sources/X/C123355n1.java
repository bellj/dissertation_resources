package X;

/* renamed from: X.5n1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123355n1 extends AbstractC118045bB {
    public C127645up A00;
    public final C20920wX A01;
    public final AnonymousClass12P A02;
    public final C15570nT A03;
    public final C14830m7 A04;
    public final C16590pI A05;
    public final AnonymousClass102 A06;
    public final C14850m9 A07;
    public final AnonymousClass61M A08;
    public final C130155yt A09;
    public final C130125yq A0A;
    public final AnonymousClass61F A0B;
    public final AnonymousClass61C A0C;

    public C123355n1(C20920wX r1, AnonymousClass12P r2, C15570nT r3, C14830m7 r4, C16590pI r5, AnonymousClass102 r6, C14850m9 r7, AnonymousClass61M r8, C130155yt r9, C130125yq r10, AnonymousClass61F r11, AnonymousClass61C r12) {
        super(r11);
        this.A05 = r5;
        this.A04 = r4;
        this.A07 = r7;
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r1;
        this.A09 = r9;
        this.A0B = r11;
        this.A0A = r10;
        this.A06 = r6;
        this.A08 = r8;
        this.A0C = r12;
    }
}
