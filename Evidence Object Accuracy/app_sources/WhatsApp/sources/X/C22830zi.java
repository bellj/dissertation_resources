package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0zi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22830zi {
    public final C16370ot A00;
    public final C245415x A01;
    public final C20600w1 A02;
    public final C16490p7 A03;

    public C22830zi(C16370ot r1, C245415x r2, C20600w1 r3, C16490p7 r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    public Set A00(AnonymousClass1IS r3) {
        AbstractC20610w2 r1;
        C32611cR A00;
        if (this.A00.A03(r3) == null) {
            r1 = this.A01;
        } else {
            r1 = this.A02;
        }
        if (!(r1 instanceof C20600w1)) {
            A00 = ((C245415x) r1).A05(r3);
        } else {
            C20600w1 r12 = (C20600w1) r1;
            AbstractC15340mz A03 = r12.A01.A03(r3);
            if (A03 == null) {
                return new HashSet();
            }
            A00 = r12.A00(A03);
        }
        return new HashSet(A00.A00.keySet());
    }

    public void A01(DeviceJid deviceJid, AbstractC15340mz r18, long j) {
        AbstractC20610w2 r4;
        String str;
        String str2;
        String str3;
        if (r18 instanceof AnonymousClass1Iv) {
            r4 = this.A01;
        } else {
            r4 = this.A02;
        }
        if (!r18.A13 && r18.A11 != -1) {
            C32611cR A00 = r4.A00(r18);
            StringBuilder sb = new StringBuilder();
            boolean z = r4 instanceof C20600w1;
            if (!z) {
                str = "MessageAddOnReceiptDeviceStore/";
            } else {
                str = "MessageReceiptDeviceStore/";
            }
            sb.append(str);
            sb.append("updateDeviceReceiptsForMessage/key.jid=");
            AnonymousClass1IS r8 = r18.A0z;
            sb.append(r8.A00);
            sb.append("; deviceJid=");
            sb.append(deviceJid);
            sb.append("; receipt=");
            ConcurrentHashMap concurrentHashMap = A00.A00;
            sb.append(concurrentHashMap.get(deviceJid));
            sb.append("; timestamp=");
            sb.append(j);
            sb.append("; rowId=");
            sb.append(r18.A11);
            Log.i(sb.toString());
            if (j > 0) {
                C32621cS r10 = (C32621cS) concurrentHashMap.get(deviceJid);
                if (r10 == null) {
                    concurrentHashMap.put(deviceJid, new C32621cS(j));
                } else {
                    long j2 = r10.A00;
                    if (j2 <= 0 || j2 > j) {
                        r10.A00 = j;
                    } else {
                        return;
                    }
                }
                long A01 = r4.A02.A01(deviceJid);
                ContentValues contentValues = new ContentValues(3);
                if (!z) {
                    str2 = "message_add_on_row_id";
                } else {
                    str2 = "message_row_id";
                }
                contentValues.put(str2, Long.valueOf(r18.A11));
                contentValues.put("receipt_device_timestamp", Long.valueOf(j));
                contentValues.put("receipt_device_jid_row_id", Long.valueOf(A01));
                try {
                    C16310on A02 = r4.A04.A02();
                    C16330op r2 = A02.A03;
                    if (!z) {
                        str3 = "message_add_on_receipt_device";
                    } else {
                        str3 = "receipt_device";
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append("=? AND ");
                    sb2.append("receipt_device_jid_row_id");
                    sb2.append("=?");
                    String obj = sb2.toString();
                    String[] strArr = {String.valueOf(r18.A11), String.valueOf(A01)};
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("writeDeviceReceipt/UPDATE_RECEIPT_DEVICE");
                    sb3.toString();
                    if (((long) r2.A00(str3, contentValues, obj, strArr)) == 0) {
                        contentValues.put("primary_device_version", r4.A05.A00(deviceJid.getUserJid()));
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append(str);
                        sb4.append("writeDeviceReceipt/INSERT_RECEIPT_DEVICE");
                        sb4.toString();
                        if (r2.A02(contentValues, str3) == -1) {
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append(str);
                            sb5.append("writedevicereceipt/replace/failed ");
                            sb5.append(r8);
                            sb5.append(" ");
                            sb5.append(deviceJid);
                            Log.e(sb5.toString());
                            AbstractC15710nm r3 = r4.A01;
                            StringBuilder sb6 = new StringBuilder();
                            sb6.append("key=");
                            sb6.append(r8);
                            sb6.append(" device=");
                            sb6.append(deviceJid);
                            r3.AaV("ReceiptsMessageStore: replace failed", sb6.toString(), true);
                        }
                    }
                    A02.close();
                } catch (SQLiteDatabaseCorruptException e) {
                    Log.e(e);
                    r4.A03.A02();
                }
            }
        }
    }

    public void A02(AbstractC15340mz r4, Set set) {
        AbstractC20610w2 r1;
        if (r4 instanceof AnonymousClass1Iv) {
            r1 = this.A01;
        } else {
            r1 = this.A02;
        }
        AnonymousClass1IS r2 = r4.A0z;
        set.size();
        try {
            r1.A02(r4, set, false);
        } catch (SQLiteConstraintException e) {
            Log.e(e);
            StringBuilder sb = new StringBuilder("BaseReceiptDeviceStore: Tried to add message twice: Message id:");
            sb.append(r2.A01);
            throw new IllegalStateException(sb.toString());
        }
    }

    public void A03(Set set) {
        C16310on A02 = this.A03.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            this.A02.A03(set);
            this.A01.A03(set);
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
