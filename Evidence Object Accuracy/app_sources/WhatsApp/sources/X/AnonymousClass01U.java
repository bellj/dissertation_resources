package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import com.whatsapp.util.Log;

/* renamed from: X.01U  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass01U {
    public static long A00(Context context, String str) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return -1;
            }
            PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
            if (packageInfo == null) {
                return -1;
            }
            if (Build.VERSION.SDK_INT >= 28) {
                return packageInfo.getLongVersionCode();
            }
            return (long) packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException | RuntimeException e) {
            Log.e("Failed to get package info", e);
            return -1;
        }
    }

    public static void A01(Context context, Class cls, boolean z) {
        PackageManager packageManager = context.getPackageManager();
        try {
            ComponentName componentName = new ComponentName(context, cls);
            int i = 2;
            if (z) {
                i = 1;
            }
            packageManager.setComponentEnabledSetting(componentName, i, 1);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("PackageManagerUtils/setActivityRegisteredState/error: ");
            sb.append(e);
            Log.e(sb.toString());
        }
    }

    public static Signature[] A02(Context context) {
        String packageName = context.getPackageName();
        if (context.getPackageManager() == null) {
            return null;
        }
        try {
            return context.getPackageManager().getPackageInfo(packageName, 64).signatures;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }
}
