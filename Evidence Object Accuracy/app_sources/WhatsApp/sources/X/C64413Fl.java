package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.3Fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64413Fl {
    public File A00;
    public final C15820nx A01;
    public final AnonymousClass3FI A02;
    public final C17050qB A03;
    public final File A04;
    public final Object A05 = C12970iu.A0l();
    public final String A06;
    public final byte[] A07;

    public C64413Fl(C14330lG r12, C15820nx r13, AnonymousClass28Q r14, C15810nw r15, C17050qB r16, C16590pI r17, C15890o4 r18, File file, String str, String str2) {
        byte[] bArr;
        this.A04 = file;
        this.A01 = r13;
        this.A03 = r16;
        if (r13.A04() && str != null && str.startsWith(r14.A05)) {
            try {
                String A09 = C44771zW.A09(r15, r18, file, file.length());
                if (A09 != null) {
                    AnonymousClass3FI r3 = new AnonymousClass3FI(r13, str2, A09, file.length(), file.lastModified());
                    this.A02 = r3;
                    String str3 = r3.A03;
                    byte[] A03 = r13.A01.A03();
                    try {
                        Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
                        C12990iw.A1S(DefaultCrypto.HMAC_SHA256, instance, A03);
                        byte[] bytes = str2.getBytes();
                        try {
                            MessageDigest instance2 = MessageDigest.getInstance("SHA-256");
                            instance2.update(bytes);
                            instance.update(instance2.digest());
                            bArr = instance.doFinal(AnonymousClass1US.A0E(str3));
                        } catch (NoSuchAlgorithmException e) {
                            throw new AssertionError(e);
                        }
                    } catch (InvalidKeyException | NoSuchAlgorithmException e2) {
                        Log.e("EncBackupManager/getMediaDecryptionHash failed", e2);
                        bArr = null;
                    }
                    this.A07 = bArr;
                    if (bArr != null) {
                        File file2 = r12.A04().A0A;
                        C14330lG.A03(file2, false);
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append(C003501n.A03(bArr));
                        String A08 = C44771zW.A08(r17.A00, r15, new File(file2, C12960it.A0d(".mcrypt1", A0h)));
                        if (A08 != null) {
                            this.A06 = A08;
                            return;
                        }
                        throw C12970iu.A0f("Filed to get a new uploadPath");
                    }
                    throw C12960it.A0U("Filed to get media decryption hash");
                }
            } catch (AnonymousClass2AP e3) {
                Log.w("gdrive/local-file/calcMd5() failed", e3);
            }
        }
        this.A06 = str2;
    }

    public long A00() {
        if (!this.A01.A04() || this.A07 == null) {
            return this.A04.length();
        }
        return this.A04.length() + 16;
    }

    public File A01() {
        byte[] bArr;
        File file;
        C15820nx r6 = this.A01;
        if (!r6.A04() || (bArr = this.A07) == null) {
            return this.A04;
        }
        synchronized (this.A05) {
            File file2 = this.A00;
            if (file2 == null || !file2.exists()) {
                C27421Hj r0 = ((C32861cr) this.A03.A05.get()).A02;
                r0.A01();
                File file3 = new File(r0.A03, new File(this.A06).getName());
                this.A00 = file3;
                File file4 = this.A04;
                byte[] A03 = r6.A01.A03();
                if (A03 != null) {
                    byte[] A00 = C32891cu.A00(A03, bArr, 48);
                    byte[] bArr2 = new byte[32];
                    System.arraycopy(A00, 0, bArr2, 0, 32);
                    byte[] bArr3 = new byte[16];
                    System.arraycopy(A00, 32, bArr3, 0, 16);
                    try {
                        Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                        instance.init(1, new SecretKeySpec(bArr2, "AES"), new IvParameterSpec(bArr3));
                        try {
                            FileInputStream fileInputStream = new FileInputStream(file4);
                            try {
                                CipherOutputStream cipherOutputStream = new CipherOutputStream(new FileOutputStream(file3), instance);
                                C14350lI.A0G(fileInputStream, cipherOutputStream);
                                cipherOutputStream.close();
                                fileInputStream.close();
                            } catch (Throwable th) {
                                try {
                                    fileInputStream.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        } catch (IOException e) {
                            Log.w("EncBackupManager/encrypt media failed", e);
                        }
                    } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e2) {
                        Log.w("EncBackupManager/encrypt media failed", e2);
                    }
                }
            }
            file = this.A00;
        }
        return file;
    }

    public void A02() {
        synchronized (this.A05) {
            File file = this.A00;
            if (file != null && file.exists() && !this.A00.delete()) {
                Log.w("local-file/cleanup/failed to delete a file");
            }
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C64413Fl.class != obj.getClass()) {
                return false;
            }
            C64413Fl r5 = (C64413Fl) obj;
            if (!this.A04.equals(r5.A04) || !C29941Vi.A00(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A04;
        return C12960it.A06(this.A02, A1a);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("LocalFile{file=");
        A0k.append(this.A04);
        A0k.append(", metadata=");
        A0k.append(this.A02);
        return C12970iu.A0v(A0k);
    }
}
