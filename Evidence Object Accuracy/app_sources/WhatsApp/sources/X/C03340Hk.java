package X;

/* renamed from: X.0Hk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03340Hk extends AbstractC08890c1 {
    public AbstractC08890c1 A00;
    public String A01;

    public C03340Hk(AbstractC08890c1 r1, String str) {
        this.A01 = str;
        this.A00 = r1;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01);
        sb.append(" ");
        sb.append(this.A00);
        return sb.toString();
    }
}
