package X;

import android.text.TextUtils;
import android.util.Base64;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.17E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass17E {
    public final AbstractC15710nm A00;
    public final AnonymousClass122 A01;
    public final AnonymousClass124 A02;
    public final AnonymousClass17D A03;
    public final Set A04;

    public AnonymousClass17E(AbstractC15710nm r1, AnonymousClass122 r2, AnonymousClass124 r3, AnonymousClass17D r4, Set set) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = set;
    }

    public final Map A00() {
        String string = this.A03.A00().getString("pref_fb_user_certs_encrypted", null);
        if (string != null) {
            C31091Zz A00 = AnonymousClass122.A00(new JSONArray(string));
            if (A00 == null) {
                AnonymousClass009.A07("null key data");
            } else {
                byte[] A01 = this.A02.A01(A00, AnonymousClass029.A0M);
                if (A01 != null) {
                    HashMap hashMap = new HashMap();
                    JSONObject jSONObject = new JSONObject(new String(A01, AnonymousClass01V.A0A));
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        hashMap.put(new C16820po(next), new C64663Gk(jSONObject.getString(next)));
                    }
                    return hashMap;
                }
            }
            AnonymousClass009.A07("null decrypt result");
        }
        return new HashMap();
    }

    public final void A01(Map map) {
        byte[] A01;
        String str;
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            String str2 = ((C16820po) entry.getKey()).A00;
            C64663Gk r6 = (C64663Gk) entry.getValue();
            hashMap.put(str2, new JSONObject().put("e_cert", Base64.encodeToString(r6.A04.getEncoded(), 3)).put("s_cert", Base64.encodeToString(r6.A05.getEncoded(), 3)).put("ttl", r6.A00).put("ts", r6.A01).put("ppk", r6.A03).put("ppk_id", r6.A02).toString());
        }
        String obj = new JSONObject(hashMap).toString();
        AnonymousClass124 r4 = this.A02;
        Charset charset = AnonymousClass01V.A0A;
        byte[] bytes = obj.getBytes(charset);
        String str3 = AnonymousClass029.A0M;
        C31091Zz A00 = r4.A00(str3, bytes);
        if (A00 == null) {
            str = "null keyData";
        } else {
            String A002 = A00.A00();
            if (TextUtils.isEmpty(A002)) {
                str = "empty result";
            } else {
                C31091Zz A003 = AnonymousClass122.A00(new JSONArray(A002));
                if (A003 == null) {
                    AnonymousClass009.A07("null key data");
                    A01 = null;
                } else {
                    A01 = r4.A01(A003, str3);
                }
                if (!new String(A01, charset).equals(obj)) {
                    AnonymousClass009.A07("decrypted does not match original");
                    this.A00.AaV("FbUserEntityCertificateCache/encryptAndStoreMap", "Failed to encrypt cert", true);
                    return;
                }
                this.A03.A00().edit().putString("pref_fb_user_certs_encrypted", A002).apply();
                return;
            }
        }
        AnonymousClass009.A07(str);
    }
}
