package X;

import android.view.MenuItem;

/* renamed from: X.0XG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XG implements AbstractC011605p {
    public final /* synthetic */ AnonymousClass07N A00;

    @Override // X.AbstractC011605p
    public void ASi(AnonymousClass07H r1) {
    }

    public AnonymousClass0XG(AnonymousClass07N r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC011605p
    public boolean ASh(MenuItem menuItem, AnonymousClass07H r3) {
        AbstractC11650gd r0 = this.A00.A01;
        if (r0 != null) {
            return r0.onMenuItemClick(menuItem);
        }
        return false;
    }
}
