package X;

/* renamed from: X.0sW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18480sW {
    public final AbstractC15710nm A00;
    public final C15450nH A01;
    public final C18280sC A02;
    public final C14830m7 A03;
    public final C14950mJ A04;
    public final C16510p9 A05;
    public final C20820wN A06;
    public final C16490p7 A07;
    public final C21390xL A08;
    public final C21670xn A09;
    public final C21650xl A0A;
    public final C21660xm A0B;
    public final C21630xj A0C;
    public final C21640xk A0D;
    public final C16120oU A0E;

    public C18480sW(AbstractC15710nm r1, C15450nH r2, C18280sC r3, C14830m7 r4, C14950mJ r5, C16510p9 r6, C20820wN r7, C16490p7 r8, C21390xL r9, C21670xn r10, C21650xl r11, C21660xm r12, C21630xj r13, C21640xk r14, C16120oU r15) {
        this.A03 = r4;
        this.A05 = r6;
        this.A00 = r1;
        this.A0E = r15;
        this.A01 = r2;
        this.A04 = r5;
        this.A0C = r13;
        this.A08 = r9;
        this.A0D = r14;
        this.A07 = r8;
        this.A0A = r11;
        this.A02 = r3;
        this.A0B = r12;
        this.A09 = r10;
        this.A06 = r7;
    }
}
