package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.10q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C231710q implements AbstractC231810r {
    public boolean A00;
    public final AbstractC15710nm A01;
    public final AnonymousClass212 A02;
    public final C231510o A03;
    public final AnonymousClass211 A04 = new AnonymousClass211(EnumC452920z.values());
    public final C231610p A05;

    public C231710q(AbstractC15710nm r3, C16590pI r4, C231410n r5, C231510o r6, C231610p r7, C14850m9 r8) {
        this.A01 = r3;
        this.A03 = r6;
        this.A05 = r7;
        this.A02 = new AnonymousClass212(r4.A00, r3, r5, r8);
    }

    public static final LinkedHashSet A00(HashSet hashSet, List list) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        if (!hashSet.isEmpty()) {
            for (Object obj : list) {
                if (hashSet.contains(obj)) {
                    linkedHashSet.add(obj);
                    hashSet.remove(obj);
                }
            }
        }
        return linkedHashSet;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A01(java.lang.String r19, java.util.List r20, java.util.List r21, X.C37471mS[] r22, int r23, boolean r24) {
        /*
            r18 = this;
            java.lang.String r0 = r19.trim()
            java.lang.String r8 = X.AnonymousClass1US.A08(r0)
            java.util.LinkedHashSet r1 = new java.util.LinkedHashSet
            r1.<init>()
            r7 = r18
            r10 = r21
            r9 = r20
            r11 = r23
            if (r24 == 0) goto L_0x0089
            r12 = 1
            java.util.Set r0 = r7.A03(r8, r9, r10, r11, r12)
        L_0x001c:
            r1.addAll(r0)
        L_0x001f:
            int r0 = r1.size()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>(r0)
            java.util.Iterator r10 = r1.iterator()
        L_0x002c:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x00bd
            java.lang.Object r6 = r10.next()
            X.1mS r6 = (X.C37471mS) r6
            int r0 = r5.size()
            if (r0 >= r11) goto L_0x00bd
            r0 = r22
            if (r22 == 0) goto L_0x0049
            boolean r0 = X.C37871n9.A01(r6, r0)
            if (r0 == 0) goto L_0x0049
            goto L_0x002c
        L_0x0049:
            int[] r9 = r6.A00
            int r0 = r9.length
            r8 = 0
            r2 = 1
            if (r0 != r2) goto L_0x0076
            r0 = 2
            int[] r1 = new int[r0]
            r0 = r9[r8]
            r1[r8] = r0
            r0 = 65039(0xfe0f, float:9.1139E-41)
            r1[r2] = r0
            X.1mS r7 = new X.1mS
            r7.<init>(r1)
            int[] r1 = r7.A00
            X.1q1 r0 = new X.1q1
            r0.<init>(r1)
            long r3 = com.whatsapp.emoji.EmojiDescriptor.A00(r0, r2)
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0076
            r5.add(r7)
            goto L_0x002c
        L_0x0076:
            X.1q1 r0 = new X.1q1
            r0.<init>(r9)
            long r3 = com.whatsapp.emoji.EmojiDescriptor.A00(r0, r8)
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x002c
            r5.add(r6)
            goto L_0x002c
        L_0x0089:
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0096
            r1.addAll(r9)
            r1.addAll(r10)
            goto L_0x001f
        L_0x0096:
            int r0 = r1.size()
            int r16 = r23 - r0
            r17 = 1
            r12 = r7
            r13 = r8
            r14 = r9
            r15 = r10
            java.util.Set r0 = r12.A03(r13, r14, r15, r16, r17)
            r1.addAll(r0)
            int r0 = r1.size()
            if (r0 >= r11) goto L_0x001f
            int r0 = r1.size()
            int r16 = r23 - r0
            r17 = 0
            java.util.Set r0 = r12.A03(r13, r14, r15, r16, r17)
            goto L_0x001c
        L_0x00bd:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C231710q.A01(java.lang.String, java.util.List, java.util.List, X.1mS[], int, boolean):java.util.List");
    }

    public List A02(String str, C37471mS[] r14, int i, boolean z) {
        List list;
        ArrayList arrayList = new ArrayList();
        for (int[] iArr : this.A03.A02()) {
            if (iArr != null) {
                C37471mS r1 = new C37471mS(iArr);
                if (r14 == null || !C37871n9.A01(r1, r14)) {
                    arrayList.add(r1);
                }
            }
        }
        C231610p r3 = this.A05;
        synchronized (r3) {
            List list2 = r3.A00;
            if (list2 != null) {
                list = new ArrayList(list2);
            } else {
                ArrayList arrayList2 = new ArrayList();
                String string = r3.A01.A00.getString("top_emojis", null);
                if (string != null) {
                    try {
                        JSONArray jSONArray = new JSONArray(string);
                        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                            arrayList2.add(C38491oB.A00(jSONArray.getString(i2)));
                        }
                        r3.A00 = arrayList2;
                        list = new ArrayList(arrayList2);
                    } catch (JSONException e) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("topemojisstore/get-top-emojis/failed ");
                        sb.append(e);
                        Log.e(sb.toString());
                        list = new ArrayList(C231610p.A02);
                    }
                } else {
                    list = new ArrayList(C231610p.A02);
                }
            }
        }
        if (r14 != null) {
            list.removeAll(Arrays.asList(r14));
        }
        return A01(str, arrayList, list, r14, i, z);
    }

    public final Set A03(String str, List list, List list2, int i, boolean z) {
        String str2;
        String obj;
        HashSet hashSet = new HashSet();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        if (linkedHashSet.size() < i) {
            if (!this.A00) {
                this.A01.AaV("emoji dictionary is not prepared yet", null, false);
                return linkedHashSet;
            }
            hashSet = new HashSet();
            StringBuilder sb = new StringBuilder("SELECT DISTINCT symbol FROM emoji_search_tag WHERE type=? AND (tag");
            if (z) {
                str2 = " = ";
            } else {
                str2 = " LIKE ";
            }
            sb.append(str2);
            sb.append("? OR ");
            sb.append("symbol");
            sb.append("=?) ORDER BY _id ASC LIMIT ?");
            String obj2 = sb.toString();
            String[] strArr = new String[4];
            strArr[0] = "1";
            if (z) {
                obj = str;
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append("%");
                obj = sb2.toString();
            }
            strArr[1] = obj;
            strArr[2] = str;
            strArr[3] = String.valueOf(256);
            AnonymousClass01T r6 = new AnonymousClass01T(obj2, strArr);
            try {
                C16310on A01 = this.A02.get();
                Cursor A09 = A01.A03.A09((String) r6.A00, (String[]) r6.A01);
                while (A09.moveToNext()) {
                    try {
                        hashSet.add(C38491oB.A00(A09.getString(0)));
                    } catch (Throwable th) {
                        if (A09 != null) {
                            try {
                                A09.close();
                            } catch (Throwable unused) {
                            }
                        }
                        throw th;
                    }
                }
                A09.close();
                A01.close();
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e(e);
            }
        }
        if (linkedHashSet.size() < i) {
            linkedHashSet.addAll(A00(hashSet, list));
        }
        if (linkedHashSet.size() < i) {
            linkedHashSet.addAll(A00(hashSet, list2));
        }
        if (linkedHashSet.size() < i && hashSet.size() > 0) {
            ArrayList arrayList = new ArrayList(hashSet.size());
            arrayList.addAll(hashSet);
            Collections.sort(arrayList, this.A04);
            linkedHashSet.addAll(arrayList);
            return linkedHashSet;
        }
        return linkedHashSet;
    }

    @Override // X.AbstractC231810r
    public void A7B() {
        C16310on A02 = this.A02.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("emoji_search_tag", "type=?", new String[]{String.valueOf(1)});
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Override // X.AbstractC231810r
    public /* bridge */ /* synthetic */ Collection A9t(String str, Object[] objArr, int i, boolean z, boolean z2) {
        return A02(str, (C37471mS[]) objArr, i, false);
    }

    @Override // X.AbstractC231810r
    public void AcF(boolean z) {
        StringBuilder sb = new StringBuilder("emojidictionarystore/setIsFetched:");
        sb.append(z);
        Log.i(sb.toString());
        this.A00 = z;
    }

    @Override // X.AbstractC231810r
    public int getCount() {
        C16310on A01 = this.A02.get();
        try {
            int i = 0;
            Cursor A09 = A01.A03.A09("SELECT count(*) FROM emoji_search_tag WHERE type=?", new String[]{String.valueOf(1)});
            if (A09.moveToNext()) {
                i = A09.getInt(0);
            }
            A09.close();
            A01.close();
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
