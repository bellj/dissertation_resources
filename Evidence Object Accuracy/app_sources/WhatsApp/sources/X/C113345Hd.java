package X;

import java.security.cert.CertPathBuilderException;

/* renamed from: X.5Hd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113345Hd extends CertPathBuilderException {
    public Throwable cause;

    public C113345Hd(Throwable th) {
        super("Error finding target certificate.");
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
