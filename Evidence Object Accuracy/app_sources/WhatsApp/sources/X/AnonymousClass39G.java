package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.whatsapp.WaImageView;
import java.io.File;

/* renamed from: X.39G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39G extends AnonymousClass21T {
    public Bitmap A00;
    public final AnonymousClass2Zb A01;
    public final WaImageView A02;
    public final C38911ou A03;

    public AnonymousClass39G(Context context, File file) {
        C38911ou A00 = C38911ou.A00(ParcelFileDescriptor.open(file, 268435456), false);
        this.A03 = A00;
        AnonymousClass2Zb A05 = A00.A05(context);
        this.A01 = A05;
        WaImageView waImageView = new WaImageView(context);
        this.A02 = waImageView;
        waImageView.setImageDrawable(A05);
    }
}
