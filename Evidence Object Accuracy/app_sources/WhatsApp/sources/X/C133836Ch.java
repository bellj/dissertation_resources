package X;

import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.6Ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133836Ch implements AbstractC1311061h {
    public final /* synthetic */ AbstractActivityC121685jC A00;
    public final /* synthetic */ PaymentView A01;

    public C133836Ch(AbstractActivityC121685jC r1, PaymentView paymentView) {
        this.A00 = r1;
        this.A01 = paymentView;
    }

    @Override // X.AbstractC1311061h
    public void A9z() {
        this.A00.A2g(1);
    }

    @Override // X.AbstractC136476Mr
    public void AaH() {
        this.A01.A04();
    }

    @Override // X.AbstractC136476Mr
    public void AaN() {
        this.A00.AaN();
    }

    @Override // X.AbstractC136476Mr
    public void AaQ() {
        this.A01.A05();
    }
}
