package X;

import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.view.View;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Nu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04980Nu {
    public AnonymousClass0K0 A00 = null;
    public final AssetManager A01;
    public final C05320Pc A02 = new C05320Pc();
    public final Map A03 = new HashMap();
    public final Map A04 = new HashMap();

    public C04980Nu(Drawable.Callback callback) {
        AssetManager assets;
        if (!(callback instanceof View)) {
            AnonymousClass0R5.A00("LottieDrawable must be inside of a view for images to work.");
            assets = null;
        } else {
            assets = ((View) callback).getContext().getAssets();
        }
        this.A01 = assets;
    }
}
