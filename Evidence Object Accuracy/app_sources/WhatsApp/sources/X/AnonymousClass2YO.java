package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.ClearableEditText;
import com.whatsapp.R;

/* renamed from: X.2YO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YO extends AnimatorListenerAdapter {
    public final /* synthetic */ C470928x A00;

    public AnonymousClass2YO(C470928x r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        C470928x r2 = this.A00;
        r2.A04.setClickable(true);
        r2.A06.setVisibility(0);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        C470928x r3 = this.A00;
        ClearableEditText clearableEditText = r3.A0A;
        clearableEditText.clearFocus();
        r3.A07.setImageResource(R.drawable.ic_shape_picker_search);
        r3.A04.setVisibility(0);
        clearableEditText.setVisibility(4);
    }
}
