package X;

import android.icu.text.DisplayContext;
import android.icu.text.SimpleDateFormat;
import java.text.Format;

/* renamed from: X.55y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1105455y implements AbstractC51202Tg {
    @Override // X.AbstractC51202Tg
    public Format AD6(AnonymousClass018 r4) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("LLLL yyyy", AnonymousClass018.A00(r4.A00));
        simpleDateFormat.setContext(DisplayContext.CAPITALIZATION_FOR_BEGINNING_OF_SENTENCE);
        return simpleDateFormat;
    }
}
