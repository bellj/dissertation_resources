package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.community.SubgroupPileView;

/* renamed from: X.3WF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WF implements AnonymousClass28F {
    public final /* synthetic */ SubgroupPileView A00;
    public final /* synthetic */ C15370n3 A01;

    public AnonymousClass3WF(SubgroupPileView subgroupPileView, C15370n3 r2) {
        this.A00 = subgroupPileView;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            Adv(imageView);
        }
    }

    @Override // X.AnonymousClass28F
    public void Adv(ImageView imageView) {
        imageView.setImageDrawable(C12970iu.A0C(imageView.getContext(), this.A00.A03.A01(this.A01)));
    }
}
