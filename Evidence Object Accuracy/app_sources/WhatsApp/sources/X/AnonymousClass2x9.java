package X;

import java.util.List;

/* renamed from: X.2x9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2x9 extends AnonymousClass36m {
    public final C15450nH A00;
    public final C15550nR A01;

    public AnonymousClass2x9(C15450nH r1, C15550nR r2, C15610nY r3, AbstractActivityC36611kC r4, AnonymousClass018 r5, List list) {
        super(r3, r4, r5, list);
        this.A00 = r1;
        this.A01 = r2;
    }
}
