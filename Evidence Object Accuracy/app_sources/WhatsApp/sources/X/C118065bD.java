package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5bD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118065bD extends AnonymousClass015 {
    public AbstractC001200n A00;
    public AnonymousClass02P A01 = new AnonymousClass02P();
    public AnonymousClass02P A02 = new AnonymousClass02P();
    public AnonymousClass016 A03 = C12980iv.A0T();
    public final C14900mE A04;
    public final C15450nH A05;
    public final C18640sm A06;
    public final C14830m7 A07;
    public final C16590pI A08;
    public final C14820m6 A09;
    public final C17220qS A0A;
    public final C1329668y A0B;
    public final C18650sn A0C;
    public final C18610sj A0D;
    public final C17900ra A0E;

    public C118065bD(AbstractC001200n r5, C14900mE r6, C15450nH r7, C18640sm r8, C14830m7 r9, C16590pI r10, C14820m6 r11, C17220qS r12, C1329668y r13, C18650sn r14, C18610sj r15, C17900ra r16) {
        this.A00 = r5;
        this.A07 = r9;
        this.A04 = r6;
        this.A08 = r10;
        this.A05 = r7;
        this.A0A = r12;
        this.A09 = r11;
        this.A0D = r15;
        this.A0E = r16;
        this.A06 = r8;
        this.A0C = r14;
        this.A0B = r13;
        this.A02.A0B(new C127175u4(0, -1));
        this.A01.A0B(new C1310661b());
        this.A01.A0D(this.A03, C117305Zk.A0B(this, 161));
    }

    public C1310661b A04() {
        Object A01 = this.A01.A01();
        AnonymousClass009.A05(A01);
        return (C1310661b) A01;
    }

    public final void A05(int i) {
        if (this.A05.A05(AbstractC15460nI.A0v)) {
            this.A02.A0B(new C127175u4(2, -1));
            C1329668y r5 = this.A0B;
            synchronized (r5) {
                String[] strArr = {"signedQrCode", "signedQrCodeTs"};
                try {
                    C18600si r4 = r5.A03;
                    String A04 = r4.A04();
                    if (!TextUtils.isEmpty(A04)) {
                        JSONObject A05 = C13000ix.A05(A04);
                        int i2 = 0;
                        do {
                            A05.remove(strArr[i2]);
                            i2++;
                        } while (i2 < 2);
                        C117295Zj.A1E(r4, A05);
                    }
                } catch (JSONException e) {
                    Log.w("PAY: IndiaUpiPaymentSharedPrefs deleteFromPaymentInfo for keys threw: ", e);
                }
            }
            C1310661b A042 = A04();
            A042.A09 = null;
            A042.A01 = "02";
            this.A01.A0B(A042);
            C120375g5 r52 = new C120375g5(this.A08.A00, this.A04, this.A0A, this.A0C, new C64513Fv(), this.A0D);
            String A052 = A04().A05();
            C128815wi r12 = new C128815wi(this, i);
            C17220qS r3 = r52.A02;
            String A01 = r3.A01();
            C126555t4 r1 = new C126555t4(new AnonymousClass3CS(A01), A052);
            C64513Fv r10 = ((C126705tJ) r52).A00;
            if (r10 != null) {
                r10.A04("upi-sign-qr-code");
            }
            C117325Zm.A06(r3, new C120785gk(r52.A00, r52.A01, r52.A03, r10, r52, r12), r1.A00, A01);
            return;
        }
        this.A02.A0B(new C127175u4(0, i));
    }

    public void A06(C126155sQ r9, int i) {
        C127175u4 r1;
        String[] A0P;
        String str;
        if (i == 0) {
            C1329668y r3 = this.A0B;
            synchronized (r3) {
                A0P = r3.A0P("signedQrCode", "signedQrCodeTs");
            }
            if (!this.A05.A05(AbstractC15460nI.A0v) || (!TextUtils.isEmpty(A0P[0]) && !TextUtils.isEmpty(A0P[1]) && this.A07.A00() - Long.parseLong(A0P[1]) <= 259200000)) {
                r1 = new C127175u4(0, -1);
                this.A02.A0B(r1);
            }
            A05(-1);
        } else if (i != 1) {
            if (!(i == 2 || i == 3)) {
                if (i == 4) {
                    if (r9 != null) {
                        str = r9.A00;
                    } else {
                        str = "";
                    }
                    A08(str, 0);
                    return;
                }
                return;
            }
            A05(-1);
        } else {
            r1 = new C127175u4(1, -1);
            this.A02.A0B(r1);
        }
    }

    public void A07(String str) {
        String[] A0P;
        C1310661b A00;
        String trim;
        C1329668y r3 = this.A0B;
        synchronized (r3) {
            A0P = r3.A0P("signedQrCode", "signedQrCodeTs");
        }
        if (TextUtils.isEmpty(A0P[0])) {
            String A002 = C1329668y.A00(r3);
            if (TextUtils.isEmpty(A002)) {
                this.A02.A0B(new C127175u4(-1, -1));
                return;
            }
            if (TextUtils.isEmpty(str)) {
                trim = this.A09.A00.getString("push_name", "");
                this.A0D.A08(null, 1);
            } else {
                trim = str.trim();
            }
            C1310661b A04 = A04();
            A04.A04 = trim;
            A04.A0C = A002;
            A04.A01 = "01";
            this.A01.A0B(A04);
            return;
        }
        AnonymousClass02P r4 = this.A01;
        String str2 = A0P[0];
        C1310661b r1 = null;
        if (!(str2 == null || (A00 = C1310661b.A00(Uri.parse(str2), "SCANNED_QR_CODE")) == null)) {
            A00.A03 = str2;
            r1 = A00;
        }
        r4.A0B(r1);
    }

    public final void A08(String str, int i) {
        C127175u4 r1;
        AnonymousClass02P r5 = this.A01;
        C1310661b r4 = (C1310661b) r5.A01();
        if (str.equals(r4.A05)) {
            r1 = new C127175u4(3, i);
        } else {
            C17900ra r12 = this.A0E;
            C30821Yy AEV = r12.A00().AEV();
            C30821Yy A0E = C117305Zk.A0E(r12.A00(), str);
            if (A0E == null || A0E.A00.compareTo(AEV.A00) < 0) {
                r4.A05 = null;
                r5.A0B(r4);
                r1 = new C127175u4(0, i);
            } else {
                r4.A05 = str;
                r5.A0B(r4);
                A05(i);
                return;
            }
        }
        this.A02.A0B(r1);
    }
}
