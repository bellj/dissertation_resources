package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.webpagepreview.WebPagePreviewView;

/* renamed from: X.3bA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70363bA implements AbstractC41521tf {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C64343Fe A01;
    public final /* synthetic */ Bitmap[] A02;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70363bA(C64343Fe r1, Bitmap[] bitmapArr, int i) {
        this.A01 = r1;
        this.A02 = bitmapArr;
        this.A00 = i;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r6) {
        C64343Fe r0 = this.A01;
        WebPagePreviewView webPagePreviewView = r0.A0A;
        if (bitmap != null) {
            webPagePreviewView.setVideoLargeThumbWithBitmap(bitmap);
            this.A02[0] = bitmap;
            return;
        }
        webPagePreviewView.setVideoLargeThumbWithBackground(AnonymousClass00T.A00(r0.A03, R.color.primary_surface));
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A01.A0A.setVideoLargeThumbWithBackground(-7829368);
    }
}
