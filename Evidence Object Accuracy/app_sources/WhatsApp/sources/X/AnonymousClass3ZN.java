package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3ZN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZN implements AbstractC21730xt {
    public final C14900mE A00;
    public final C90064Ml A01;
    public final C17170qN A02;
    public final C17220qS A03;

    public AnonymousClass3ZN(C14900mE r1, C90064Ml r2, C17170qN r3, C17220qS r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
    }

    public final void A00() {
        this.A00.A0I(new RunnableBRunnable0Shape10S0200000_I1(this, 46, new C89214Jc(null)));
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("GetCustomUrlsByJidProtocol/onDeliveryFailure");
        A00();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        Log.e("GetCustomUrlsByJidProtocol/onError");
        C41151sz.A00(r2);
        A00();
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        AnonymousClass1V8 A0E;
        String A0G;
        Log.e("GetCustomUrlsByJidProtocol/onSuccess");
        AnonymousClass1V8 A0E2 = r6.A0E("custom_urls");
        if (A0E2 != null) {
            List<AnonymousClass1V8> A0J = A0E2.A0J("custom_url");
            ArrayList A0l = C12960it.A0l();
            for (AnonymousClass1V8 r1 : A0J) {
                if (!(r1 == null || (A0E = r1.A0E("path")) == null || (A0G = A0E.A0G()) == null || A0G.isEmpty())) {
                    A0l.add(A0G);
                }
            }
            this.A00.A0I(new RunnableBRunnable0Shape10S0200000_I1(this, 46, new C89214Jc(A0l)));
            return;
        }
        A00();
    }
}
