package X;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.3Gv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64773Gv {
    public static final int[][] A01;
    public static final int[][] A02 = {new int[]{128512}, new int[]{128515}, new int[]{128516}, new int[]{128513}, new int[]{128518}, new int[]{128517}, new int[]{128514}, new int[]{129315}, new int[]{128578}, new int[]{128539}, new int[]{128541}, new int[]{128540}, new int[]{129322}, new int[]{129303}, new int[]{128570}, new int[]{128568}, new int[]{128569}, new int[]{9786}};
    public static final int[][] A03;
    public static final int[][] A04;
    public static final int[][] A05;
    public HashMap A00;

    static {
        int[][] iArr = new int[18];
        iArr[4] = C12960it.A1a(iArr);
        int[] iArr2 = new int[1];
        iArr2[0] = 128576;
        iArr[5] = iArr2;
        int[] iArr3 = new int[1];
        iArr3[0] = 128561;
        iArr[6] = iArr3;
        int[] iArr4 = new int[1];
        iArr4[0] = 129327;
        iArr[7] = iArr4;
        int[] iArr5 = new int[1];
        iArr5[0] = 128563;
        iArr[8] = iArr5;
        int[] iArr6 = new int[1];
        iArr6[0] = 128576;
        iArr[9] = iArr6;
        int[] iArr7 = new int[1];
        iArr7[0] = 10071;
        iArr[10] = iArr7;
        int[] iArr8 = new int[1];
        iArr8[0] = 10069;
        iArr[11] = iArr8;
        int[] iArr9 = new int[1];
        iArr9[0] = 129325;
        iArr[12] = iArr9;
        int[] iArr10 = new int[1];
        iArr10[0] = 128077;
        iArr[13] = iArr10;
        int[] iArr11 = new int[1];
        iArr11[0] = 128078;
        iArr[14] = iArr11;
        int[] iArr12 = new int[1];
        iArr12[0] = 128064;
        iArr[15] = iArr12;
        int[] iArr13 = new int[1];
        iArr13[0] = 129488;
        iArr[16] = iArr13;
        int[] iArr14 = new int[1];
        iArr14[0] = 129327;
        iArr[17] = iArr14;
        A04 = iArr;
        int[][] A1b = C12960it.A1b();
        int[] iArr15 = new int[1];
        iArr15[0] = 128546;
        A1b[5] = iArr15;
        int[] A1Z = C12980iv.A1Z(A1b, 6, 7);
        A1Z[0] = 128532;
        A1b[8] = A1Z;
        int[] iArr16 = new int[1];
        iArr16[0] = 128543;
        A1b[9] = iArr16;
        int[] iArr17 = new int[1];
        iArr17[0] = 128533;
        A1b[10] = iArr17;
        int[] iArr18 = new int[1];
        iArr18[0] = 128548;
        A1b[11] = iArr18;
        int[] iArr19 = new int[1];
        iArr19[0] = 128544;
        A1b[12] = iArr19;
        int[] iArr20 = new int[1];
        iArr20[0] = 128549;
        A1b[13] = iArr20;
        int[] iArr21 = new int[1];
        iArr21[0] = 128560;
        A1b[14] = iArr21;
        int[] iArr22 = new int[1];
        iArr22[0] = 128552;
        A1b[15] = iArr22;
        int[] iArr23 = new int[1];
        iArr23[0] = 128575;
        A1b[16] = iArr23;
        int[] iArr24 = new int[1];
        iArr24[0] = 128574;
        A1b[17] = iArr24;
        int[] iArr25 = new int[1];
        iArr25[0] = 128531;
        A1b[18] = iArr25;
        A1b[19] = new int[]{128589, 8205, 9794};
        A1b[20] = new int[]{128589, 8205, 9792};
        A05 = A1b;
        int[][] iArr26 = new int[20];
        iArr26[4] = C12960it.A1Z(iArr26);
        iArr26[5] = new int[]{128111, 8205, 9794, 65039};
        iArr26[6] = new int[]{128111, 8205, 9792, 65039};
        int[] iArr27 = new int[1];
        iArr27[0] = 128131;
        iArr26[7] = iArr27;
        int[] iArr28 = new int[1];
        iArr28[0] = 128378;
        iArr26[8] = iArr28;
        int[] iArr29 = new int[1];
        iArr29[0] = 128293;
        iArr26[9] = iArr29;
        int[] iArr30 = new int[1];
        iArr30[0] = 11088;
        iArr26[10] = iArr30;
        int[] iArr31 = new int[1];
        iArr31[0] = 10024;
        iArr26[11] = iArr31;
        int[] iArr32 = new int[1];
        iArr32[0] = 128171;
        iArr26[12] = iArr32;
        int[] iArr33 = new int[1];
        iArr33[0] = 127879;
        iArr26[13] = iArr33;
        int[] iArr34 = new int[1];
        iArr34[0] = 127878;
        iArr26[14] = iArr34;
        int[] iArr35 = new int[1];
        iArr35[0] = 127867;
        iArr26[15] = iArr35;
        int[] iArr36 = new int[1];
        iArr36[0] = 129346;
        iArr26[16] = iArr36;
        int[] iArr37 = new int[1];
        iArr37[0] = 127870;
        iArr26[17] = iArr37;
        int[] iArr38 = new int[1];
        iArr38[0] = 127874;
        iArr26[18] = iArr38;
        int[] iArr39 = new int[1];
        iArr39[0] = 127856;
        iArr26[19] = iArr39;
        A01 = iArr26;
        int[][] iArr40 = new int[31];
        int[] iArr41 = new int[1];
        iArr41[0] = 10084;
        iArr40[0] = iArr41;
        int[] iArr42 = new int[1];
        iArr42[0] = 128525;
        iArr40[1] = iArr42;
        int[] iArr43 = new int[1];
        iArr43[0] = 128536;
        iArr40[2] = iArr43;
        int[] iArr44 = new int[1];
        iArr44[0] = 128149;
        iArr40[3] = iArr44;
        int[] iArr45 = new int[1];
        iArr45[0] = 128571;
        iArr40[4] = iArr45;
        int[] iArr46 = new int[1];
        iArr46[0] = 128145;
        iArr40[5] = iArr46;
        iArr40[6] = new int[]{128105, 8205, 10084, 8205, 128105};
        iArr40[7] = new int[]{128104, 8205, 10084, 8205, 128104};
        int[] iArr47 = new int[1];
        iArr47[0] = 128143;
        iArr40[8] = iArr47;
        iArr40[9] = new int[]{128105, 8205, 10084, 8205, 128139, 8205, 128105};
        iArr40[10] = new int[]{128104, 8205, 10084, 8205, 128139, 8205, 128104};
        int[] iArr48 = new int[1];
        iArr48[0] = 10084;
        iArr40[11] = iArr48;
        int[] iArr49 = new int[1];
        iArr49[0] = 129505;
        iArr40[12] = iArr49;
        int[] iArr50 = new int[1];
        iArr50[0] = 128155;
        iArr40[13] = iArr50;
        int[] iArr51 = new int[1];
        iArr51[0] = 128154;
        iArr40[14] = iArr51;
        int[] iArr52 = new int[1];
        iArr52[0] = 128153;
        iArr40[15] = iArr52;
        int[] iArr53 = new int[1];
        iArr53[0] = 128156;
        iArr40[16] = iArr53;
        int[] iArr54 = new int[1];
        iArr54[0] = 128420;
        iArr40[17] = iArr54;
        int[] iArr55 = new int[1];
        iArr55[0] = 128148;
        iArr40[18] = iArr55;
        int[] iArr56 = new int[1];
        iArr56[0] = 10083;
        iArr40[19] = iArr56;
        C12960it.A1Q(iArr40, new int[]{128149});
        A03 = iArr40;
    }

    public C64773Gv() {
        HashMap hashMap = this.A00;
        if (hashMap == null) {
            this.A00 = C12970iu.A11();
        } else {
            hashMap.clear();
        }
        A00(A02, 1);
        A00(A04, 2);
        A00(A05, 3);
        A00(A01, 4);
        A00(A03, 5);
    }

    public final void A00(int[][] iArr, int i) {
        for (int[] iArr2 : iArr) {
            C37471mS r2 = new C37471mS(iArr2);
            HashMap hashMap = this.A00;
            if (hashMap.containsKey(r2)) {
                ((Set) hashMap.get(r2)).add(Integer.valueOf(i));
            } else {
                HashSet A12 = C12970iu.A12();
                C12980iv.A1R(A12, i);
                hashMap.put(r2, A12);
            }
        }
    }
}
