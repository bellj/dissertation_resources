package X;

import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.689  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass689 implements AnonymousClass5W2 {
    public final C14900mE A00;
    public final C18790t3 A01;
    public final C14830m7 A02;
    public final C248217a A03;
    public final C21860y6 A04;
    public final C18660so A05;
    public final C17070qD A06;
    public final C129945yY A07;

    public AnonymousClass689(C14900mE r1, C18790t3 r2, C14830m7 r3, C248217a r4, C21860y6 r5, C18660so r6, C17070qD r7, C129945yY r8) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A06 = r7;
        this.A04 = r5;
        this.A07 = r8;
        this.A03 = r4;
        this.A05 = r6;
    }

    @Override // X.AnonymousClass5W2
    public void A5s(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl A0H = C117305Zk.A0H(it);
            int A04 = A0H.A04();
            if (!(A04 == 1 || A04 == 4)) {
                if (A04 == 5) {
                    C18660so r1 = this.A05;
                    r1.A06(r1.A01("add_business"));
                } else if (!(A04 == 6 || A04 == 7)) {
                    Log.w(C12960it.A0b("PAY: Not supported method type for Brazil: ", A0H));
                }
            }
            C21860y6 r12 = this.A04;
            r12.A06(r12.A01("add_card"));
        }
        this.A00.A0I(new RunnableBRunnable0Shape8S0100000_I0_8(this.A03, 25));
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0173  */
    @Override // X.AnonymousClass5W2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC28901Pl A6M(X.AbstractC28901Pl r6) {
        /*
        // Method dump skipped, instructions count: 405
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass689.A6M(X.1Pl):X.1Pl");
    }
}
