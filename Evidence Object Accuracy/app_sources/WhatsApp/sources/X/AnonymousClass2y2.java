package X;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.reactions.ReactionsBottomSheetDialogFragment;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2y2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2y2 extends AbstractC60742yY {
    public int A00 = 0;
    public AnonymousClass4KQ A01 = new AnonymousClass4KQ(this);
    public AnonymousClass109 A02;
    public C22370yy A03;
    public C26161Cg A04;
    public List A05;
    public boolean A06;
    public boolean A07 = false;
    public boolean A08 = false;
    public final View A09;
    public final View A0A;
    public final ImageView A0B;
    public final TextView A0C;
    public final TextView A0D;
    public final TextView A0E = C12960it.A0J(this, R.id.more);
    public final CircularProgressBar A0F;
    public final AbstractC41521tf A0G = new C70203au(this);
    public final AbstractView$OnClickListenerC34281fs A0H = new ViewOnClickCListenerShape18S0100000_I1_1(this, 8);
    public final AbstractView$OnClickListenerC34281fs A0I = new ViewOnClickCListenerShape18S0100000_I1_1(this, 10);
    public final AbstractView$OnClickListenerC34281fs A0J = new ViewOnClickCListenerShape18S0100000_I1_1(this, 9);
    public final ArrayList A0K;

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return 0;
    }

    @Override // X.AbstractC60742yY
    public int getMaxAlbumSize() {
        return 102;
    }

    @Override // X.AbstractC60742yY
    public int getMinAlbumSize() {
        return 4;
    }

    public AnonymousClass2y2(Context context, AbstractC13890kV r8, AbstractC16130oV r9) {
        super(context, r8, r9);
        TextView textView;
        A0Z();
        ArrayList A0l = C12960it.A0l();
        this.A0K = A0l;
        A0l.add(new AnonymousClass3DB(findViewById(R.id.thumb_0), this, 0));
        A0l.add(new AnonymousClass3DB(findViewById(R.id.thumb_1), this, 1));
        A0l.add(new AnonymousClass3DB(findViewById(R.id.thumb_2), this, 2));
        A0l.add(new AnonymousClass3DB(findViewById(R.id.thumb_3), this, 3));
        AnonymousClass009.A0A("wrong number of views", C12960it.A1V(4, A0l.size()));
        this.A09 = findViewById(R.id.control_btn);
        CircularProgressBar circularProgressBar = (CircularProgressBar) findViewById(R.id.progress_bar);
        this.A0F = circularProgressBar;
        circularProgressBar.A0B = 0;
        this.A0B = C12970iu.A0L(this, R.id.cancel_download);
        View findViewById = findViewById(R.id.control_frame);
        this.A0A = findViewById;
        boolean z = r9.A0z.A02;
        if (!z) {
            this.A0D = C12960it.A0J(this, R.id.download_size);
            textView = C12960it.A0J(this, R.id.download_item_count);
        } else {
            textView = null;
            this.A0D = null;
        }
        this.A0C = textView;
        findViewById.setBackground(new C49742Mi(AnonymousClass00T.A00(getContext(), z ? R.color.bubble_color_outgoing : R.color.bubble_color_incoming)));
        A0X(true);
    }

    private void A0X(boolean z) {
        ArrayList arrayList;
        TextView textView;
        AnonymousClass018 r7;
        int i;
        AbstractView$OnClickListenerC34281fs r1;
        ImageView imageView;
        int i2;
        ImageView imageView2;
        TextView textView2;
        if (this.A05 != null) {
            if (z && (textView2 = this.A0D) != null) {
                textView2.setTag(null);
            }
            int i3 = 0;
            do {
                arrayList = this.A0K;
                AnonymousClass3DB r5 = (AnonymousClass3DB) arrayList.get(i3);
                AbstractC15340mz r11 = (AbstractC15340mz) this.A05.get(i3);
                if (r11.A0v && r5.A00 == null) {
                    AnonymousClass2y2 r8 = r5.A05;
                    ImageView imageView3 = new ImageView(r8.getContext());
                    r5.A00 = imageView3;
                    int dimensionPixelSize = r8.getResources().getDimensionPixelSize(R.dimen.star_padding);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    imageView3.setLayoutParams(layoutParams);
                    C12960it.A0r(imageView3.getContext(), imageView3, R.string.starred);
                    C42941w9.A08(imageView3, ((AbstractC28551Oa) r8).A0K, 0, dimensionPixelSize);
                    C12990iw.A0x(r8.getContext(), r5.A00, R.drawable.message_star_media);
                    ViewGroup viewGroup = r5.A01;
                    viewGroup.addView(r5.A00, 0);
                    viewGroup.setClipChildren(false);
                }
                if (!r11.A0v || (imageView2 = r5.A00) == null) {
                    ImageView imageView4 = r5.A00;
                    if (imageView4 != null) {
                        imageView4.setVisibility(8);
                    }
                } else {
                    imageView2.setVisibility(0);
                }
                TextView textView3 = r5.A04;
                AnonymousClass2y2 r6 = r5.A05;
                textView3.setText(AnonymousClass3JK.A00(((AbstractC28551Oa) r6).A0K, r6.A0k.A02(r11.A0I)));
                r6.A15(textView3, r11, R.drawable.broadcast_status_icon_onmedia);
                AnonymousClass19O r9 = r6.A1O;
                ImageView imageView5 = r5.A02;
                AbstractC41521tf r12 = r6.A0G;
                StringBuilder A0k = C12960it.A0k("album-");
                AnonymousClass1IS r2 = r11.A0z;
                r9.A0A(imageView5, r11, r12, C12970iu.A0s(r2, A0k), 100, false, false);
                if (r2.A02 && (imageView = r5.A03) != null) {
                    int i4 = r11.A0C;
                    if (C37381mH.A00(i4, 13) >= 0) {
                        i2 = R.drawable.message_got_read_receipt_from_target_onmedia;
                    } else if (C37381mH.A00(i4, 5) >= 0) {
                        i2 = R.drawable.message_got_receipt_from_target_onmedia;
                    } else {
                        i2 = R.drawable.message_unsent_onmedia;
                        if (i4 == 4) {
                            i2 = R.drawable.message_got_receipt_from_server_onmedia;
                        }
                    }
                    imageView.setImageResource(i2);
                }
                AnonymousClass028.A0k(imageView5, AbstractC28561Ob.A0W(r2));
                AnonymousClass028.A0k(textView3, AbstractC42671vd.A0Y(r11));
                ImageView imageView6 = r5.A03;
                if (imageView6 != null) {
                    StringBuilder sb = new StringBuilder("status-transition-");
                    sb.append(r2);
                    AnonymousClass028.A0k(imageView6, sb.toString());
                }
                i3++;
            } while (i3 < 4);
            AnonymousClass3DB r22 = (AnonymousClass3DB) arrayList.get(3);
            if (this.A05.size() > arrayList.size()) {
                TextView textView4 = this.A0E;
                textView4.setVisibility(0);
                Context context = getContext();
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, (this.A05.size() - arrayList.size()) + 1, 0);
                textView4.setText(context.getString(R.string.plus_n, objArr));
                r22.A04.setVisibility(8);
                ImageView imageView7 = r22.A03;
                if (imageView7 != null) {
                    imageView7.setVisibility(8);
                }
                int A09 = C12990iw.A09(this.A05, 4);
                ImageView imageView8 = r22.A02;
                AnonymousClass23N.A02(imageView8, R.string.action_open_photo_album);
                Object[] objArr2 = new Object[1];
                C12960it.A1P(objArr2, A09, 0);
                imageView8.setContentDescription(((AbstractC28551Oa) this).A0K.A0I(objArr2, R.plurals.number_of_photos, (long) A09));
            } else {
                this.A0E.setVisibility(8);
                r22.A04.setVisibility(0);
                ImageView imageView9 = r22.A03;
                if (imageView9 != null) {
                    imageView9.setVisibility(0);
                }
                ImageView imageView10 = r22.A02;
                C12960it.A0r(getContext(), imageView10, R.string.action_open_image);
                AnonymousClass028.A0g(imageView10, new AnonymousClass04v());
            }
            List<AbstractC16130oV> list = this.A05;
            if (list != null) {
                for (AbstractC16130oV r13 : list) {
                    if (AbstractC15340mz.A00(r13).A0a) {
                        View view = this.A0A;
                        view.setVisibility(0);
                        CircularProgressBar circularProgressBar = this.A0F;
                        ImageView imageView11 = this.A0B;
                        View view2 = this.A09;
                        AbstractC42671vd.A0a(view, circularProgressBar, view2, imageView11, true, !z, false);
                        AbstractView$OnClickListenerC34281fs r14 = this.A0H;
                        imageView11.setOnClickListener(r14);
                        view2.setOnClickListener(r14);
                        circularProgressBar.setOnClickListener(r14);
                        break;
                    }
                }
            }
            List<AbstractC16130oV> list2 = this.A05;
            if (list2 != null) {
                for (AbstractC16130oV r15 : list2) {
                    if (!AbstractC15340mz.A00(r15).A0P) {
                        View view3 = this.A0A;
                        view3.setVisibility(0);
                        CircularProgressBar circularProgressBar2 = this.A0F;
                        ImageView imageView12 = this.A0B;
                        View view4 = this.A09;
                        int i5 = 0;
                        AbstractC42671vd.A0a(view3, circularProgressBar2, view4, imageView12, false, !z, false);
                        int i6 = 0;
                        for (AbstractC16130oV r4 : this.A05) {
                            C16150oX A00 = AbstractC15340mz.A00(r4);
                            if (!A00.A0P && !A00.A0a) {
                                if (C30041Vv.A11(r4)) {
                                    i5++;
                                }
                                i6++;
                            }
                        }
                        if (!((AbstractC28551Oa) this).A0O.A0z.A02 || i6 == i5) {
                            this.A00 = 0;
                            this.A08 = false;
                            this.A07 = false;
                            long j = 0;
                            ArrayList A0l = C12960it.A0l();
                            for (AbstractC16130oV r72 : this.A05) {
                                C16150oX A002 = AbstractC15340mz.A00(r72);
                                if (!A002.A0P && !A002.A0a) {
                                    A0l.add(r72);
                                    this.A00++;
                                    j += r72.A01;
                                    boolean z2 = this.A07;
                                    byte b = r72.A0y;
                                    this.A07 = z2 | C12960it.A1V(b, 1);
                                    this.A08 = C12960it.A1V(b, 3) | this.A08;
                                }
                            }
                            TextView textView5 = this.A0D;
                            if (textView5 != null && (textView = this.A0C) != null) {
                                textView5.setTag(A0l);
                                A16(textView5, A0l, j);
                                if (this.A00 == 0) {
                                    textView.setVisibility(8);
                                } else {
                                    textView.setVisibility(0);
                                    if (!this.A07) {
                                        r7 = ((AbstractC28551Oa) this).A0K;
                                        i = R.plurals.number_of_videos;
                                    } else {
                                        boolean z3 = this.A08;
                                        r7 = ((AbstractC28551Oa) this).A0K;
                                        i = R.plurals.number_of_items;
                                        if (!z3) {
                                            i = R.plurals.number_of_photos;
                                        }
                                    }
                                    int i7 = this.A00;
                                    Object[] objArr3 = new Object[1];
                                    C12960it.A1P(objArr3, i7, 0);
                                    textView.setText(r7.A0I(objArr3, i, (long) i7));
                                }
                                r1 = this.A0I;
                            }
                            A0w();
                        }
                        if (view4 instanceof TextView) {
                            TextView textView6 = (TextView) view4;
                            textView6.setText(R.string.retry);
                            textView6.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_upload_dark, 0, 0, 0);
                        }
                        r1 = this.A0J;
                        view4.setOnClickListener(r1);
                        A0w();
                    }
                }
            }
            View view5 = this.A0A;
            view5.setVisibility(8);
            AbstractC42671vd.A0a(view5, this.A0F, this.A09, this.A0B, false, false, false);
            A0w();
        }
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A06) {
            this.A06 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A03 = (C22370yy) A08.AB0.get();
            this.A04 = (C26161Cg) A08.AB4.get();
            this.A02 = (AnonymousClass109) A08.AI8.get();
        }
    }

    @Override // X.AbstractC28551Oa
    public void A0e(AnonymousClass1IS r4) {
        super.A0e(r4);
        List list = this.A05;
        if (list != null) {
            int i = 0;
            Iterator it = list.iterator();
            while (it.hasNext() && !r4.equals(C12980iv.A0f(it).A0z)) {
                i++;
            }
            Intent A1N = A1N();
            if (AbstractC454421p.A00) {
                A1N.putExtra("start_index", i);
            }
            C12990iw.A10(A1N, this);
        }
    }

    @Override // X.AbstractC28551Oa
    public boolean A0h() {
        return C30041Vv.A0t(((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public boolean A0l(AbstractC15340mz r5) {
        List list = this.A05;
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                if ((C12980iv.A0f(it).A07 & 1) == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // X.AnonymousClass1OY
    public ReactionsBottomSheetDialogFragment A0p(AbstractC14640lm r4, C40481rf r5) {
        AnonymousClass009.A05(r4);
        AnonymousClass4KQ r2 = this.A01;
        ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment = new ReactionsBottomSheetDialogFragment();
        reactionsBottomSheetDialogFragment.A0D = r4;
        reactionsBottomSheetDialogFragment.A0E = r5;
        reactionsBottomSheetDialogFragment.A0H = C12960it.A1W(r2);
        reactionsBottomSheetDialogFragment.A0A = r2;
        return reactionsBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass1OY
    public void A0r() {
        if (A1J()) {
            AbstractC15340mz r0 = ((AbstractC28551Oa) this).A0O;
            AbstractC13890kV r2 = ((AbstractC28551Oa) this).A0a;
            if (r2 != null) {
                r2.AeE(r0);
                r2.Acq(this.A05, true);
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A0X(false);
        A1H(false);
    }

    @Override // X.AnonymousClass1OY
    public void A0w() {
        List list = this.A05;
        boolean z = false;
        if (!(list == null || list.isEmpty())) {
            int size = this.A05.size();
            int i = 0;
            for (AbstractC16130oV r6 : this.A05) {
                C16150oX A00 = AbstractC15340mz.A00(r6);
                if (A00.A0a && !A00.A0Y) {
                    int i2 = (int) A00.A0C;
                    if (this.A02.A06(r6)) {
                        i2 >>= 1;
                        if (this.A02.A07(r6)) {
                            i2 += 50;
                        }
                    }
                    i += i2;
                } else if (A00.A0P) {
                    i += 100;
                }
            }
            if (size != 0) {
                int i3 = i / size;
                CircularProgressBar circularProgressBar = this.A0F;
                if (i3 == 0 || i3 == 100) {
                    z = true;
                }
                circularProgressBar.setIndeterminate(z);
                circularProgressBar.setProgress(i3);
                Context context = getContext();
                int i4 = R.color.album_progress_determinate;
                if (i3 == 0) {
                    i4 = R.color.album_progress_indeterminate;
                }
                circularProgressBar.A0C = AnonymousClass00T.A00(context, i4);
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1B(AbstractC15340mz r4) {
        AbstractC13890kV r2;
        if (A1J() && (r2 = ((AbstractC28551Oa) this).A0a) != null) {
            boolean z = !r2.AJm(r4);
            r2.Acq(this.A05, z);
            this.A0e.setRowSelected(z);
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        super.A1D(((AbstractC28551Oa) this).A0O, z);
        if (z) {
            A0X(false);
        }
    }

    @Override // X.AbstractC60742yY
    public void A1M(List list, boolean z) {
        boolean A1X = C12960it.A1X(((AbstractC28551Oa) this).A0O, list.get(0));
        if (!z) {
            List list2 = this.A05;
            if (list2 != null && list2.size() == list.size()) {
                for (int i = 0; i < list.size(); i++) {
                    if (this.A05.get(i) == list.get(i)) {
                    }
                }
            }
            z = true;
            break;
        }
        this.A05 = list;
        super.A1D((AbstractC15340mz) list.get(0), z);
        if (A1X || z) {
            A0X(A1X);
        }
    }

    public final Intent A1N() {
        AbstractC14640lm r3;
        long[] jArr = new long[this.A05.size()];
        for (int i = 0; i < this.A05.size(); i++) {
            jArr[i] = ((AbstractC15340mz) this.A05.get(i)).A11;
        }
        AbstractC15340mz r2 = ((AbstractC28551Oa) this).A0O;
        AnonymousClass1IS r1 = r2.A0z;
        if (!r1.A02) {
            AbstractC14640lm r12 = r1.A00;
            if (!C15380n4.A0J(r12) || (r3 = r2.A0B()) == null) {
                r3 = r12;
            }
        } else {
            r3 = null;
        }
        Context context = getContext();
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(context.getPackageName(), "com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity");
        A0A.putExtra("message_ids", jArr);
        A0A.putExtra("jid", C15380n4.A03(r3));
        return A0A;
    }

    @Override // X.AbstractC28551Oa
    public AbstractC16130oV getFMessage() {
        return (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_album_left;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        return AnonymousClass3GD.A02(this);
    }

    @Override // X.AbstractC60742yY, X.AnonymousClass1OY
    public int getMessageCount() {
        return C12970iu.A09(this.A05);
    }

    @Override // X.AnonymousClass1OY
    public C40481rf getMessageReactions() {
        List list = this.A05;
        if (list == null) {
            return null;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC15340mz A0f = C12980iv.A0f(it);
            C26701Em r3 = this.A1H;
            if (C26701Em.A00(A0f, (byte) 56)) {
                r3.A06.execute(new RunnableBRunnable0Shape0S0201000_I0(A0f, r3));
            } else {
                r3.A03(A0f, null, (byte) 56, false, false);
            }
        }
        C15570nT r2 = ((AnonymousClass1OY) this).A0L;
        List list2 = this.A05;
        C40481rf r32 = new C40481rf(r2, Collections.emptyList());
        Iterator it2 = list2.iterator();
        while (it2.hasNext()) {
            C40481rf r0 = C12980iv.A0f(it2).A0V;
            if (r0 != null) {
                for (AnonymousClass1Iv r02 : r0.A02()) {
                    r32.A04(r02);
                }
            }
        }
        return r32;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_album_right;
    }

    @Override // X.AbstractC28551Oa
    public int getReactionsViewVerticalOverlap() {
        return getResources().getDimensionPixelOffset(R.dimen.space_tight_halfStep);
    }

    @Override // X.AnonymousClass1OY
    public int getTopAttributeTextAnchorId() {
        return R.id.media_container;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AbstractC16130oV);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
