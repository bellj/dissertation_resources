package X;

import android.graphics.Rect;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.2ef  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53572ef extends AnonymousClass04v {
    public final /* synthetic */ View A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ boolean A03;

    public C53572ef(View view, String str, String str2, boolean z) {
        this.A02 = str;
        this.A01 = str2;
        this.A00 = view;
        this.A03 = z;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r10) {
        super.A06(view, r10);
        r10.A0F("Button");
        AccessibilityNodeInfo accessibilityNodeInfo = r10.A02;
        accessibilityNodeInfo.setSelected(false);
        accessibilityNodeInfo.setContentDescription(this.A02);
        String str = this.A01;
        if (str != null) {
            C12970iu.A1O(r10, str);
        }
        View view2 = (View) this.A00.getParent();
        if (this.A03 && view2 != null) {
            int[] A07 = C13000ix.A07();
            view2.getLocationOnScreen(A07);
            int i = A07[0];
            accessibilityNodeInfo.setBoundsInScreen(new Rect(i, A07[1], i + view2.getWidth(), A07[1] + view2.getHeight()));
        }
    }
}
