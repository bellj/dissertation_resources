package X;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.0BG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BG extends Animation {
    public final /* synthetic */ AnonymousClass0BD A00;

    public AnonymousClass0BG(AnonymousClass0BD r1) {
        this.A00 = r1;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        this.A00.setAnimationProgress(1.0f - f);
    }
}
