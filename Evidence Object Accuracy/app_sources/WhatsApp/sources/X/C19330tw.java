package X;

import android.os.PowerManager;
import com.whatsapp.util.Log;

/* renamed from: X.0tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19330tw implements AbstractC16890pv {
    public final AnonymousClass01d A00;

    public C19330tw(AnonymousClass01d r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC16890pv
    public void AMJ() {
        PowerManager A0I = this.A00.A0I();
        if (A0I == null) {
            Log.w("app-init/main pm=null");
        } else {
            Log.i(C16700pc.A08("app-init/async/screen/", A0I.isScreenOn() ? "on" : "off"));
        }
    }
}
