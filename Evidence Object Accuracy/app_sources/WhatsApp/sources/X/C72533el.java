package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.util.FloatingChildLayout;

/* renamed from: X.3el  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72533el extends AnimatorListenerAdapter {
    public final /* synthetic */ FloatingChildLayout A00;

    public C72533el(FloatingChildLayout floatingChildLayout) {
        this.A00 = floatingChildLayout;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        this.A00.A09.setVisibility(4);
    }
}
