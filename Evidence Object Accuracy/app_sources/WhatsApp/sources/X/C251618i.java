package X;

/* renamed from: X.18i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C251618i {
    public final C19380u1 A00;
    public final C20970wc A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C14820m6 A04;
    public final C14850m9 A05;
    public final C16120oU A06;
    public final C16030oK A07;
    public final C251218e A08;
    public final C19890uq A09;
    public final C251318f A0A;
    public final C22270yo A0B;
    public final C18350sJ A0C;
    public final C251418g A0D;
    public final C231210l A0E;
    public final C251518h A0F;
    public final C18370sL A0G;

    public C251618i(C19380u1 r2, C20970wc r3, C14830m7 r4, C16590pI r5, C14820m6 r6, C14850m9 r7, C16120oU r8, C16030oK r9, C251218e r10, C19890uq r11, C251318f r12, C22270yo r13, C18350sJ r14, C251418g r15, C231210l r16, C251518h r17, C18370sL r18) {
        this.A02 = r4;
        this.A05 = r7;
        this.A03 = r5;
        this.A0G = r18;
        this.A06 = r8;
        this.A09 = r11;
        this.A00 = r2;
        this.A08 = r10;
        this.A0B = r13;
        this.A0A = r12;
        this.A0D = r15;
        this.A0C = r14;
        this.A01 = r3;
        this.A04 = r6;
        this.A0F = r17;
        this.A07 = r9;
        this.A0E = r16;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02a6, code lost:
        if (r47 != false) goto L_0x02a8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A00(java.lang.Integer r34, java.lang.Integer r35, java.lang.String r36, java.lang.String r37, java.lang.String r38, java.lang.String r39, java.lang.String r40, java.lang.String r41, java.lang.String r42, java.lang.String r43, java.lang.String r44, java.lang.String r45, int r46, boolean r47) {
        /*
        // Method dump skipped, instructions count: 1239
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C251618i.A00(java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, boolean):void");
    }
}
