package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.18n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252118n extends AbstractC15850o0 {
    public Drawable A00;
    public boolean A01;
    public boolean A02;
    public final C14330lG A03;
    public final C14900mE A04;
    public final C15570nT A05;
    public final AnonymousClass15E A06;
    public final C252318p A07;
    public final C22190yg A08;

    public C252118n(C14330lG r13, C14900mE r14, C15570nT r15, C15820nx r16, C15810nw r17, C17050qB r18, C16590pI r19, C19490uC r20, C21780xy r21, AnonymousClass15E r22, C252318p r23, AbstractC15870o2 r24, C16600pJ r25, AnonymousClass15D r26, C22190yg r27) {
        super(r15, r16, r17, r18, r19, r20, r21, r24, r25, r26);
        this.A04 = r14;
        this.A05 = r15;
        this.A03 = r13;
        this.A08 = r27;
        this.A06 = r22;
        this.A07 = r23;
    }

    public AnonymousClass2JQ A0A(Context context, boolean z) {
        int i;
        int i2;
        Drawable A00;
        OutOfMemoryError e;
        FileInputStream openFileInput;
        String str;
        boolean A08 = C41691tw.A08(context);
        if ((this.A01 && A08 != this.A02) || z) {
            this.A00 = null;
        }
        int i3 = 0;
        if (this.A00 == null) {
            this.A01 = false;
            Resources resources = context.getResources();
            Drawable A01 = AnonymousClass2JP.A01(context, resources, new File(context.getFilesDir(), "wallpaper.jpg"));
            this.A00 = A01;
            i = 2;
            if (A01 != null) {
                i = 5;
            } else {
                try {
                    openFileInput = context.openFileInput("wallpaper.jpg");
                    try {
                        i = openFileInput.read();
                        i2 = i == 4 ? openFileInput.read() : 0;
                    } finally {
                    }
                } catch (IOException unused) {
                    i2 = 0;
                } catch (OutOfMemoryError e2) {
                    e = e2;
                    i2 = 0;
                }
                try {
                    openFileInput.close();
                } catch (IOException unused2) {
                } catch (OutOfMemoryError e3) {
                    e = e3;
                    Log.i(e);
                }
                if (i == 2 || i == 1) {
                    A00 = AnonymousClass2JP.A00(context, resources);
                } else {
                    if (i == 4) {
                        int[] intArray = context.getResources().getIntArray(R.array.solid_color_wallpaper_colors);
                        Bitmap createBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
                        createBitmap.setPixel(0, 0, intArray[i2]);
                        A00 = new BitmapDrawable(context.getResources(), createBitmap);
                    }
                    i3 = i2;
                }
                this.A00 = A00;
                this.A02 = C41691tw.A08(context);
                this.A01 = true;
                i3 = i2;
            }
            Drawable drawable = this.A00;
            if (drawable != null) {
                StringBuilder sb = new StringBuilder("wallpaper/get ");
                sb.append(drawable.getIntrinsicWidth());
                sb.append("x");
                sb.append(this.A00.getIntrinsicHeight());
                sb.append(" ");
                sb.append(((BitmapDrawable) this.A00).getBitmap().getByteCount());
                str = sb.toString();
            } else {
                str = "wallpaper/get null";
            }
            Log.i(str);
        } else {
            i = 2;
        }
        String str2 = "DEFAULT";
        if (!(i == 1 || i == 2)) {
            if (i == 4) {
                str2 = "COLOR_ONLY";
            } else if (i == 5) {
                str2 = "DOWNLOADED";
            } else if (this.A00 == null) {
                str2 = "NONE";
            }
        }
        return new AnonymousClass2JQ(this.A00, Integer.valueOf(i3), str2);
    }

    public final void A0B(Context context, Drawable drawable) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput("wallpaper.jpg", 0);
            ((BitmapDrawable) drawable).getBitmap().compress(Bitmap.CompressFormat.JPEG, 90, openFileOutput);
            if (openFileOutput != null) {
                openFileOutput.close();
            }
        } catch (IOException e) {
            Log.e(e);
        }
        this.A07.A6K();
    }
}
