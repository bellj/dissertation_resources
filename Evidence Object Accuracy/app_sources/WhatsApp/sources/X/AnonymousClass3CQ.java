package X;

import android.app.Application;
import com.whatsapp.R;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperLinkViewModel;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.3CQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3CQ {
    public final /* synthetic */ IndiaUpiMapperLinkViewModel A00;

    public /* synthetic */ AnonymousClass3CQ(IndiaUpiMapperLinkViewModel indiaUpiMapperLinkViewModel) {
        this.A00 = indiaUpiMapperLinkViewModel;
    }

    public final void A00(AnonymousClass2SO r8, C452120p r9) {
        Object obj;
        IndiaUpiMapperLinkViewModel indiaUpiMapperLinkViewModel = this.A00;
        C16700pc.A0E(indiaUpiMapperLinkViewModel, 0);
        if (r9 != null) {
            int i = r9.A00;
            if (i == 21176) {
                indiaUpiMapperLinkViewModel.A05.A0A(AnonymousClass46W.A00);
                return;
            }
            C27691It r4 = indiaUpiMapperLinkViewModel.A05;
            Application application = indiaUpiMapperLinkViewModel.A02;
            if (i == 21138) {
                r4.A0A(new C620533w(i, application.getString(R.string.mapper_adding_upi_number_error_title), application.getString(R.string.mapper_adding_upi_number_error_desc)));
            } else {
                r4.A0A(new C620533w(i, "", application.getString(R.string.payments_upi_something_went_wrong)));
            }
        } else {
            AnonymousClass18O r42 = indiaUpiMapperLinkViewModel.A04;
            C16700pc.A0B(r8);
            synchronized (r42) {
                Set<AnonymousClass2SO> set = r42.A01;
                Iterator it = set.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (C16700pc.A0O(((AnonymousClass2SO) obj).A01, r8.A01)) {
                        break;
                    }
                }
                if (((AnonymousClass2SO) obj) != null) {
                    r42.A01(r8);
                } else if (set.size() < 3 && !set.contains(r8)) {
                    if (!(set instanceof Collection) || !set.isEmpty()) {
                        for (AnonymousClass2SO r0 : set) {
                            if (C16700pc.A0O(r0.A03, "mobile_number")) {
                                break;
                            }
                        }
                    }
                    LinkedHashSet linkedHashSet = new LinkedHashSet(C17540qy.A03(set.size() + 1));
                    linkedHashSet.addAll(set);
                    linkedHashSet.add(r8);
                    if (r42.A02(linkedHashSet)) {
                        set.add(r8);
                    }
                }
            }
            String str = r8.A02;
            if (str != null && str.equalsIgnoreCase("active_pending")) {
                indiaUpiMapperLinkViewModel.A05.A0A(AnonymousClass46Y.A00);
            } else if (str != null && str.equalsIgnoreCase("active")) {
                indiaUpiMapperLinkViewModel.A05.A0A(AnonymousClass46X.A00);
            }
        }
    }
}
