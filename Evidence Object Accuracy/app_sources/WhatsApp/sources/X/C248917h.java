package X;

import android.telephony.PhoneNumberUtils;
import com.whatsapp.util.Log;
import java.util.regex.Pattern;

/* renamed from: X.17h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C248917h {
    public static Pattern A00;
    public static Pattern A01;
    public static Pattern A02;

    public static String A00(C20920wX r9, String str) {
        String str2;
        StringBuilder sb;
        String str3;
        StringBuilder sb2 = new StringBuilder("+");
        sb2.append(str);
        String obj = sb2.toString();
        try {
            C71133cR A0E = r9.A0E(obj, "ZZ");
            int i = A0E.countryCode_;
            String valueOf = String.valueOf(A0E.nationalNumber_);
            if (52 == i) {
                if (valueOf.length() == 11 && valueOf.charAt(0) == '1') {
                    sb = new StringBuilder();
                    sb.append("+");
                    sb.append(i);
                    sb.append(valueOf.substring(1));
                    str2 = sb.toString();
                }
                str2 = null;
            } else {
                if (225 == i) {
                    Pattern pattern = A00;
                    if (pattern == null) {
                        pattern = Pattern.compile("((?:0[1-3]|[457][0-3])\\d{6})");
                        A00 = pattern;
                    }
                    if (pattern.matcher(valueOf).matches()) {
                        sb = new StringBuilder();
                        sb.append("+");
                        sb.append(i);
                        str3 = "01";
                    } else {
                        Pattern pattern2 = A01;
                        if (pattern2 == null) {
                            pattern2 = Pattern.compile("([04-9][4-6]\\d{6})");
                            A01 = pattern2;
                        }
                        if (pattern2.matcher(valueOf).matches()) {
                            sb = new StringBuilder();
                            sb.append("+");
                            sb.append(i);
                            str3 = "05";
                        } else {
                            Pattern pattern3 = A02;
                            if (pattern3 == null) {
                                pattern3 = Pattern.compile("((?:[04-8][7-9]|9[78])\\d{6})");
                                A02 = pattern3;
                            }
                            if (pattern3.matcher(valueOf).matches()) {
                                sb = new StringBuilder();
                                sb.append("+");
                                sb.append(i);
                                str3 = "07";
                            }
                        }
                    }
                    sb.append(str3);
                    sb.append(valueOf);
                    str2 = sb.toString();
                }
                str2 = null;
            }
            if (str2 != null) {
                A0E = r9.A0E(str2, "ZZ");
            }
            obj = r9.A0G(EnumC868649g.INTERNATIONAL, A0E);
            return obj;
        } catch (Exception e) {
            StringBuilder sb3 = new StringBuilder("contact/formatter-exception num:");
            sb3.append(obj);
            sb3.append(" ");
            sb3.append(e.getMessage());
            Log.e(sb3.toString(), e);
            return obj;
        } catch (ExceptionInInitializerError e2) {
            StringBuilder sb4 = new StringBuilder("contact/formatter-init-exception num:");
            sb4.append(obj);
            sb4.append(" ");
            sb4.append(e2.getMessage());
            Log.e(sb4.toString(), e2);
            return obj;
        }
    }

    public static String A01(C15370n3 r1) {
        return A04((AbstractC14640lm) r1.A0B(AbstractC14640lm.class));
    }

    public static String A02(AbstractC14640lm r2) {
        String replaceAll;
        String A04 = A04(r2);
        if (A04 == null || (replaceAll = A04.replaceAll("\\D", "")) == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder("+");
        sb.append(replaceAll);
        return sb.toString();
    }

    public static String A03(AbstractC14640lm r4) {
        String str;
        int indexOf;
        if (r4 instanceof C27631Ih) {
            return r4.user;
        }
        if (!C15380n4.A0J(r4) || r4 == null || (str = r4.user) == null || (indexOf = str.indexOf("-")) == -1) {
            return null;
        }
        return str.substring(0, indexOf);
    }

    public static String A04(AbstractC14640lm r3) {
        C20920wX A002 = C20920wX.A00();
        if (r3 == null) {
            Log.w("contact/phonenumber/jid/null");
            return null;
        } else if (C15380n4.A0M(r3)) {
            StringBuilder sb = new StringBuilder("+");
            sb.append(r3.user);
            return sb.toString();
        } else {
            String A03 = A03(r3);
            if (!PhoneNumberUtils.isGlobalPhoneNumber(A03)) {
                return A03;
            }
            AnonymousClass009.A05(A03);
            return A00(A002, A03);
        }
    }
}
