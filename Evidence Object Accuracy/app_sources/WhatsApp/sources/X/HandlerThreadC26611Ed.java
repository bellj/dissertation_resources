package X;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.util.SparseIntArray;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.util.List;
import java.util.Random;
import java.util.Set;

/* renamed from: X.1Ed  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerThreadC26611Ed extends HandlerThread {
    public long A00;
    public AnonymousClass2L2 A01;
    public AbstractC43311wo A02;
    public AnonymousClass2L3 A03;
    public HandlerC49412Kq A04;
    public AbstractC43471x5 A05;
    public HandlerThreadC43491x7 A06;
    public AnonymousClass2L8 A07;
    public C51322Tx A08;
    public final C17200qQ A09;
    public final C22690zU A0A;
    public final AbstractC15710nm A0B;
    public final C233111e A0C;
    public final C15570nT A0D;
    public final C20870wS A0E;
    public final C22920zr A0F;
    public final C20640w5 A0G;
    public final C15450nH A0H;
    public final C18790t3 A0I;
    public final C17210qR A0J;
    public final C26561Dy A0K;
    public final C231010j A0L;
    public final AnonymousClass1EC A0M;
    public final C22160yd A0N;
    public final C14830m7 A0O;
    public final C16590pI A0P;
    public final C17170qN A0Q;
    public final C14820m6 A0R;
    public final C15650ng A0S;
    public final C16490p7 A0T;
    public final C22350yw A0U;
    public final C20250vS A0V;
    public final C14850m9 A0W;
    public final C21680xo A0X;
    public final AnonymousClass15Y A0Y;
    public final C16120oU A0Z;
    public final C20710wC A0a;
    public final C22960zv A0b;
    public final AnonymousClass13N A0c;
    public final C16030oK A0d;
    public final C26581Ea A0e;
    public final C251218e A0f;
    public final C49402Kp A0g;
    public final C43321wp A0h = new C43321wp("connection_thread/logged_flag/connected", false);
    public final C43321wp A0i = new C43321wp("connection_thread/logged_flag/disconnecting", false);
    public final C43321wp A0j = new C43321wp("connection_thread/logged_flag/quit", false);
    public final C17220qS A0k;
    public final C22360yx A0l;
    public final C14840m8 A0m;
    public final C238213d A0n;
    public final AnonymousClass1EW A0o;
    public final C230710g A0p;
    public final C26591Eb A0q;
    public final C20160vJ A0r;
    public final C17230qT A0s;
    public final C230810h A0t;
    public final AnonymousClass11H A0u;
    public final C18610sj A0v;
    public final C17070qD A0w;
    public final AnonymousClass1EX A0x;
    public final C22940zt A0y;
    public final AnonymousClass18L A0z;
    public final C22980zx A10;
    public final AnonymousClass1EZ A11;
    public final C20320vZ A12;
    public final C22660zR A13;
    public final C15510nN A14;
    public final C251418g A15;
    public final C18910tG A16;
    public final AnonymousClass1EY A17;
    public final AnonymousClass100 A18;
    public final C18800t4 A19;
    public final C23000zz A1A;
    public final C26601Ec A1B;
    public final AbstractC14440lR A1C;
    public final C21280xA A1D;
    public final JniBridge A1E;
    public final C14890mD A1F;
    public final Random A1G;

    public HandlerThreadC26611Ed(C17200qQ r4, C22690zU r5, AbstractC15710nm r6, C233111e r7, C15570nT r8, C20870wS r9, C22920zr r10, C20640w5 r11, C15450nH r12, C18790t3 r13, C17210qR r14, C26561Dy r15, C231010j r16, AnonymousClass1EC r17, C22160yd r18, C14830m7 r19, C16590pI r20, C17170qN r21, C14820m6 r22, C15650ng r23, C16490p7 r24, C22350yw r25, C20250vS r26, C14850m9 r27, C21680xo r28, AnonymousClass15Y r29, C16120oU r30, C20710wC r31, C22960zv r32, AnonymousClass13N r33, C16030oK r34, C26581Ea r35, C251218e r36, C17220qS r37, C22360yx r38, C14840m8 r39, C238213d r40, AnonymousClass1EW r41, C230710g r42, C26591Eb r43, C20160vJ r44, C17230qT r45, C230810h r46, AnonymousClass11H r47, C18610sj r48, C17070qD r49, AnonymousClass1EX r50, C22940zt r51, AnonymousClass18L r52, C22980zx r53, AnonymousClass1EZ r54, C20320vZ r55, C22660zR r56, C15510nN r57, C251418g r58, C18910tG r59, AnonymousClass1EY r60, AnonymousClass100 r61, C18800t4 r62, C23000zz r63, C26601Ec r64, AbstractC14440lR r65, C21280xA r66, JniBridge jniBridge, C14890mD r68) {
        super("ConnectionThread", 1);
        this.A0P = r20;
        this.A0O = r19;
        this.A0W = r27;
        this.A0V = r26;
        this.A0B = r6;
        this.A0D = r8;
        this.A1C = r65;
        this.A1E = jniBridge;
        this.A0N = r18;
        this.A0G = r11;
        this.A0I = r13;
        this.A0Z = r30;
        this.A1F = r68;
        this.A0H = r12;
        this.A09 = r4;
        this.A13 = r56;
        this.A0k = r37;
        this.A1D = r66;
        this.A0o = r41;
        this.A0X = r28;
        this.A0E = r9;
        this.A0f = r36;
        this.A12 = r55;
        this.A0w = r49;
        this.A0x = r50;
        this.A17 = r60;
        this.A0S = r23;
        this.A0r = r44;
        this.A0a = r31;
        this.A0F = r10;
        this.A0n = r40;
        this.A0c = r33;
        this.A0m = r39;
        this.A15 = r58;
        this.A0y = r51;
        this.A19 = r62;
        this.A0Y = r29;
        this.A0U = r25;
        this.A0b = r32;
        this.A0A = r5;
        this.A0s = r45;
        this.A0l = r38;
        this.A0T = r24;
        this.A10 = r53;
        this.A1A = r63;
        this.A0R = r22;
        this.A11 = r54;
        this.A0M = r17;
        this.A0e = r35;
        this.A18 = r61;
        this.A0q = r43;
        this.A0t = r46;
        this.A0u = r47;
        this.A0v = r48;
        this.A0J = r14;
        this.A0d = r34;
        this.A14 = r57;
        this.A0p = r42;
        this.A0z = r52;
        this.A0K = r15;
        this.A1B = r64;
        this.A0Q = r21;
        this.A0L = r16;
        this.A0C = r7;
        this.A16 = r59;
        this.A1G = new Random();
        this.A0g = new C49402Kp(r62, r65);
    }

    public static /* synthetic */ void A00(Message message, HandlerThreadC26611Ed r5) {
        Handler handler;
        Message message2;
        int i;
        int i2 = message.arg1;
        if (i2 != 4) {
            if (i2 == 87) {
                r5.A04.removeMessages(2);
            } else if (i2 == 205) {
                Object obj = message.obj;
                AbstractC43471x5 r3 = r5.A05;
                Message obtain = Message.obtain(null, 0, 76, 0, obj);
                obtain.what = 2;
                ((Handler) r3).sendMessage(obtain);
                return;
            }
            C230710g r2 = r5.A0p;
            int i3 = r2.A04.get(i2, -1);
            if (i3 < 0 || i3 >= r2.A0C.size()) {
                AbstractC43311wo r22 = r5.A02;
                message2 = Message.obtain(message);
                handler = (Handler) r22;
                i = 5;
            } else {
                Message obtain2 = Message.obtain(message);
                obtain2.what = 1;
                obtain2.arg1 = i2;
                r2.A09.sendMessage(obtain2);
                return;
            }
        } else {
            r5.A0O.A04(message.getData().getLong("timestamp") * 1000, System.currentTimeMillis());
            AbstractC43471x5 r23 = r5.A05;
            message2 = Message.obtain(null, 0, 0, 0);
            handler = (Handler) r23;
            i = 2;
        }
        message2.what = i;
        handler.sendMessage(message2);
    }

    public static /* synthetic */ void A01(Message message, HandlerThreadC26611Ed r4) {
        String string = message.getData().getString("iqId");
        if (!r4.A0p.A05((AnonymousClass1V8) message.obj, string)) {
            ((Handler) r4.A02).obtainMessage(8, string).sendToTarget();
        }
    }

    public static void A02(C14820m6 r4, AnonymousClass15Y r5, AnonymousClass1V8 r6) {
        SharedPreferences.Editor putString;
        String A0I = r6.A0I("location", null);
        if (TextUtils.isEmpty(A0I) || A0I.length() < 40) {
            r5.A02(A0I, 2795, 0);
            r5.A02(A0I, 2795, 1);
            boolean isEmpty = TextUtils.isEmpty(A0I);
            SharedPreferences.Editor edit = r4.A00.edit();
            if (isEmpty) {
                putString = edit.remove("last_datacenter");
            } else {
                putString = edit.putString("last_datacenter", A0I);
            }
            putString.apply();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void A03(X.AbstractC49422Kr r5, java.util.Map r6) {
        /*
            boolean r0 = r5 instanceof X.C49432Ks
            if (r0 != 0) goto L_0x007c
            boolean r0 = r5 instanceof X.C49442Kt
            if (r0 != 0) goto L_0x0075
            boolean r0 = r5 instanceof X.C49452Ku
            if (r0 != 0) goto L_0x006e
            boolean r0 = r5 instanceof X.C49462Kv
            if (r0 != 0) goto L_0x007c
            boolean r0 = r5 instanceof X.C49472Kw
            if (r0 != 0) goto L_0x0067
            boolean r0 = r5 instanceof X.C49482Kx
            if (r0 != 0) goto L_0x0060
            boolean r0 = r5 instanceof X.C49492Ky
            if (r0 != 0) goto L_0x0059
            boolean r0 = r5 instanceof X.C49502Kz
            if (r0 != 0) goto L_0x004c
            boolean r0 = r5 instanceof X.AnonymousClass2L0
            if (r0 != 0) goto L_0x0045
            boolean r2 = r5 instanceof X.AnonymousClass2L1
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            if (r2 != 0) goto L_0x0042
            java.lang.String r0 = "ack"
        L_0x002e:
            r4[r1] = r0
            int r3 = r4.length
            r2 = 0
        L_0x0032:
            if (r2 >= r3) goto L_0x008b
            r1 = r4[r2]
            boolean r0 = r6.containsKey(r1)
            if (r0 != 0) goto L_0x0083
            r6.put(r1, r5)
            int r2 = r2 + 1
            goto L_0x0032
        L_0x0042:
            java.lang.String r0 = "call"
            goto L_0x002e
        L_0x0045:
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "chatstate"
            goto L_0x002e
        L_0x004c:
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "stream:error"
            r4[r1] = r0
            r1 = 1
            java.lang.String r0 = "error"
            goto L_0x002e
        L_0x0059:
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "ib"
            goto L_0x002e
        L_0x0060:
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "iq"
            goto L_0x002e
        L_0x0067:
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "message"
            goto L_0x002e
        L_0x006e:
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "presence"
            goto L_0x002e
        L_0x0075:
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "receipt"
            goto L_0x002e
        L_0x007c:
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]
            r1 = 0
            java.lang.String r0 = "notification"
            goto L_0x002e
        L_0x0083:
            java.lang.String r1 = "ConnectionReader/addStanzaHandler this stanza is already handled"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x008b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerThreadC26611Ed.A03(X.2Kr, java.util.Map):void");
    }

    public final void A04() {
        Log.i("ConnectionThread/closeSocket");
        this.A01.A00();
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00d6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(int r14, int r15, int r16, int r17, long r18, long r20, boolean r22) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: X.HandlerThreadC26611Ed.A05(int, int, int, int, long, long, boolean):void");
    }

    public final void A06(boolean z) {
        boolean hasMessages = this.A04.hasMessages(0);
        this.A04.removeCallbacksAndMessages(null);
        C43321wp r3 = this.A0h;
        if (r3.A00) {
            C43321wp r5 = this.A0i;
            if (!r5.A00) {
                if (z) {
                    Log.i("xmpp/connection/forced_disconnect/reader_thread/mark_finished");
                    AnonymousClass2L3 r0 = this.A03;
                    if (r0 != null) {
                        r0.A00 = true;
                    }
                    A04();
                    ((Handler) this.A05).sendEmptyMessage(1);
                    ((Handler) this.A02).obtainMessage(4, 1, 0).sendToTarget();
                    r3.A00(false);
                    if (this.A0j.A00) {
                        Log.i("xmpp/connection/quit during forced disconnect");
                        this.A06.quit();
                        quit();
                        return;
                    }
                    return;
                }
                this.A04.sendEmptyMessageDelayed(0, 10000);
                AbstractC43471x5 r32 = this.A05;
                Message obtain = Message.obtain(null, 0, 13, 0);
                obtain.what = 2;
                ((Handler) r32).sendMessage(obtain);
                r5.A00(true);
                return;
            }
        }
        if (hasMessages && z) {
            HandlerC49412Kq r1 = this.A04;
            Log.w("xmpp/connection/fire-logout-timeout");
            r1.sendEmptyMessage(0);
        }
    }

    public final void A07(boolean z) {
        this.A0h.A00(false);
        this.A0i.A00(false);
        if (!this.A0j.A00) {
            this.A04.removeMessages(0);
            ((Handler) this.A05).sendEmptyMessage(1);
            ((Handler) this.A02).obtainMessage(4, z ? 1 : 0, 0).sendToTarget();
        } else {
            Log.i("xmpp/connection/quit");
            this.A06.quit();
            quit();
        }
        this.A04.removeCallbacksAndMessages(null);
    }

    @Override // android.os.HandlerThread
    public void onLooperPrepared() {
        this.A04 = new HandlerC49412Kq(this);
        AnonymousClass2L4 r6 = new AnonymousClass2L4(this);
        HandlerThreadC43491x7 r0 = new HandlerThreadC43491x7(this.A0B, this.A0E, this.A0I, this.A0M, this.A0n, r6);
        this.A06 = r0;
        r0.start();
        C26591Eb r7 = this.A0q;
        synchronized (r7) {
            if (!r7.A00) {
                for (AbstractC15920o8 r1 : (Set) r7.A02.get()) {
                    C230710g r9 = r7.A01;
                    int[] ADF = r1.ADF();
                    List list = r9.A0C;
                    int size = list.size();
                    list.add(r1);
                    for (int i : ADF) {
                        SparseIntArray sparseIntArray = r9.A04;
                        if (sparseIntArray.get(i, -1) == -1) {
                            sparseIntArray.put(i, size);
                        } else {
                            StringBuilder sb = new StringBuilder("Already have registered handler for recv message type:");
                            sb.append(i);
                            throw new IllegalArgumentException(sb.toString());
                        }
                    }
                }
                r7.A00 = true;
            }
        }
        this.A0p.A02 = this.A0k;
    }

    @Override // java.lang.Thread
    public synchronized void start() {
        boolean z = false;
        if (this.A02 != null) {
            z = true;
        }
        AnonymousClass009.A0C("callback must be set", z);
        super.start();
    }
}
