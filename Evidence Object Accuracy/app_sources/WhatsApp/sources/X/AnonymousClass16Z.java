package X;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.16Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16Z {
    public final ConcurrentHashMap A00 = new ConcurrentHashMap();
    public final ConcurrentHashMap A01 = new ConcurrentHashMap();
    public final boolean A02;

    public AnonymousClass16Z(C14850m9 r3) {
        boolean A07 = r3.A07(1885);
        this.A02 = A07;
    }

    public Lock A00(C15950oC r4) {
        ConcurrentHashMap concurrentHashMap = this.A00;
        StringBuilder sb = new StringBuilder();
        sb.append(r4.A02);
        sb.append(".");
        sb.append(r4.A01);
        return A02(sb.toString(), concurrentHashMap, 512);
    }

    public Lock A01(C15980oF r4) {
        return A02(r4.A01, this.A01, 32);
    }

    public final Lock A02(String str, ConcurrentHashMap concurrentHashMap, int i) {
        if (!this.A02) {
            return null;
        }
        Integer valueOf = Integer.valueOf(Math.abs(str.hashCode()) % i);
        if (!concurrentHashMap.containsKey(valueOf)) {
            concurrentHashMap.putIfAbsent(valueOf, new ReentrantLock());
        }
        Object obj = concurrentHashMap.get(valueOf);
        AnonymousClass009.A05(obj);
        return (Lock) obj;
    }
}
