package X;

import java.math.BigDecimal;

/* renamed from: X.3wY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83013wY extends AbstractC94534c0 {
    public static C83013wY A01 = new C83013wY((BigDecimal) null);
    public final BigDecimal A00;

    public C83013wY(CharSequence charSequence) {
        this.A00 = new BigDecimal(charSequence.toString());
    }

    public C83013wY(BigDecimal bigDecimal) {
        this.A00 = bigDecimal;
    }

    public boolean equals(Object obj) {
        C83013wY A04;
        if (this == obj) {
            return true;
        }
        if (((obj instanceof C83013wY) || (obj instanceof C82973wU)) && (A04 = ((AbstractC94534c0) obj).A04()) != A01 && this.A00.compareTo(A04.A00) == 0) {
            return true;
        }
        return false;
    }

    public String toString() {
        return this.A00.toString();
    }
}
