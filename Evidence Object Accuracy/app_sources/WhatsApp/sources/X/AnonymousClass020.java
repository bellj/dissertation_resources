package X;

import java.util.Set;
import java.util.UUID;

/* renamed from: X.020  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass020 {
    public C004401z A00;
    public Set A01;
    public UUID A02;

    public AnonymousClass020(C004401z r1, Set set, UUID uuid) {
        this.A02 = uuid;
        this.A00 = r1;
        this.A01 = set;
    }
}
