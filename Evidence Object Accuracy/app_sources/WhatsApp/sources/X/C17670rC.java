package X;

import com.whatsapp.jid.UserJid;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0rC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17670rC {
    public Object A00(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("brj");
            return new C455922g(UserJid.get(string), jSONObject.getString("ap"), jSONObject.getString("s"), jSONObject.getLong("ct"));
        } catch (AnonymousClass1MW e) {
            throw new C456222j("CTWA: AdsEntryPointTransformer/fromData/InvalidJidException", e);
        } catch (JSONException e2) {
            throw new C456222j("CTWA: AdsEntryPointTransformer/fromData/JSONException", e2);
        }
    }
}
