package X;

/* renamed from: X.5OA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OA extends AnonymousClass5OF {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int[] A06;

    public AnonymousClass5OA() {
        this.A06 = new int[16];
        reset();
    }

    public AnonymousClass5OA(AnonymousClass5OA r2) {
        super(r2);
        this.A06 = new int[16];
        A0O(r2);
    }

    public final void A0O(AnonymousClass5OA r5) {
        super.A0M(r5);
        this.A00 = r5.A00;
        this.A01 = r5.A01;
        this.A02 = r5.A02;
        this.A03 = r5.A03;
        this.A04 = r5.A04;
        int[] iArr = r5.A06;
        System.arraycopy(iArr, 0, this.A06, 0, iArr.length);
        this.A05 = r5.A05;
    }

    @Override // X.AnonymousClass5WS
    public AnonymousClass5WS A7l() {
        return new AnonymousClass5OA(this);
    }

    @Override // X.AnonymousClass5XI
    public int A97(byte[] bArr, int i) {
        A0K();
        A07(bArr, this.A00, i);
        A07(bArr, this.A01, i + 4);
        A07(bArr, this.A02, i + 8);
        A07(bArr, this.A03, i + 12);
        A07(bArr, this.A04, i + 16);
        reset();
        return 20;
    }

    @Override // X.AnonymousClass5XI
    public String AAf() {
        return "RIPEMD160";
    }

    @Override // X.AnonymousClass5XI
    public int ACZ() {
        return 20;
    }

    @Override // X.AnonymousClass5WS
    public void Aag(AnonymousClass5WS r1) {
        A0O((AnonymousClass5OA) r1);
    }

    @Override // X.AnonymousClass5OF, X.AnonymousClass5XI
    public void reset() {
        super.reset();
        this.A00 = 1732584193;
        this.A01 = -271733879;
        this.A02 = -1732584194;
        this.A03 = 271733878;
        this.A04 = -1009589776;
        this.A05 = 0;
        int i = 0;
        while (true) {
            int[] iArr = this.A06;
            if (i != iArr.length) {
                iArr[i] = 0;
                i++;
            } else {
                return;
            }
        }
    }

    public static int A00(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 26) | (i4 << 6)) + i3;
    }

    public static int A01(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 25) | (i4 << 7)) + i3;
    }

    public static int A02(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 24) | (i4 << 8)) + i3;
    }

    public static int A03(int i, int i2, int i3) {
        int i4 = i + i2;
        return ((i4 >>> 17) | (i4 << 15)) + i3;
    }

    public static int A04(int i, int i2, int i3, int i4) {
        int i5 = i + i2 + i3;
        return ((i5 >>> 27) | (i5 << 5)) + i4;
    }

    public static int A05(int i, int i2, int i3, int i4, int i5) {
        int i6 = i + i2 + i3 + i4;
        return ((i6 >>> 19) | (i6 << 13)) + i5;
    }

    public static int A06(int i, int i2, int i3, int i4, int i5) {
        return i4 + (i3 ^ (i2 | (i ^ -1))) + i5;
    }

    public static final void A07(byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) i;
        C72463ee.A0W(bArr, i, i2 + 1);
        bArr[i2 + 2] = (byte) (i >>> 16);
        bArr[i2 + 3] = (byte) (i >>> 24);
    }

    @Override // X.AnonymousClass5OF
    public void A0L() {
        int i = this.A00;
        int i2 = this.A01;
        int i3 = this.A02;
        int i4 = this.A03;
        int i5 = this.A04;
        int i6 = ((i2 ^ i3) ^ i4) + i;
        int[] iArr = this.A06;
        int i7 = iArr[0];
        int i8 = i6 + i7;
        int A0F = AnonymousClass5OF.A0F(i8, 21, i8 << 11, i5);
        int A08 = AnonymousClass5OF.A08(i3);
        int i9 = iArr[1];
        int i10 = ((A0F ^ i2) ^ A08) + i5 + i9;
        int A0F2 = AnonymousClass5OF.A0F(i10, 18, i10 << 14, i4);
        int A082 = AnonymousClass5OF.A08(i2);
        int i11 = iArr[2];
        int A03 = A03(((A0F2 ^ A0F) ^ A082) + i4, i11, A08);
        int A083 = AnonymousClass5OF.A08(A0F);
        int A0E = AnonymousClass5OF.A0E(A03, A0F2, A083, A08);
        int i12 = iArr[3];
        int i13 = A0E + i12;
        int A0F3 = AnonymousClass5OF.A0F(i13, 20, i13 << 12, A082);
        int A084 = AnonymousClass5OF.A08(A0F2);
        int A0E2 = AnonymousClass5OF.A0E(A0F3, A03, A084, A082);
        int i14 = iArr[4];
        int i15 = A0E2 + i14;
        int A0F4 = AnonymousClass5OF.A0F(i15, 27, i15 << 5, A083);
        int A085 = AnonymousClass5OF.A08(A03);
        int A0E3 = AnonymousClass5OF.A0E(A0F4, A0F3, A085, A083);
        int i16 = iArr[5];
        int i17 = A0E3 + i16;
        int A0F5 = AnonymousClass5OF.A0F(i17, 24, i17 << 8, A084);
        int A086 = AnonymousClass5OF.A08(A0F3);
        int A0E4 = AnonymousClass5OF.A0E(A0F5, A0F4, A086, A084);
        int i18 = iArr[6];
        int i19 = A0E4 + i18;
        int A0F6 = AnonymousClass5OF.A0F(i19, 25, i19 << 7, A085);
        int A087 = AnonymousClass5OF.A08(A0F4);
        int A0E5 = AnonymousClass5OF.A0E(A0F6, A0F5, A087, A085);
        int i20 = iArr[7];
        int i21 = A0E5 + i20;
        int A0F7 = AnonymousClass5OF.A0F(i21, 23, i21 << 9, A086);
        int A088 = AnonymousClass5OF.A08(A0F5);
        int A0E6 = AnonymousClass5OF.A0E(A0F7, A0F6, A088, A086);
        int i22 = iArr[8];
        int A0B = AnonymousClass5OF.A0B(A0E6, i22, A087);
        int A089 = AnonymousClass5OF.A08(A0F6);
        int A0E7 = AnonymousClass5OF.A0E(A0B, A0F7, A089, A087);
        int i23 = iArr[9];
        int i24 = A0E7 + i23;
        int A0F8 = AnonymousClass5OF.A0F(i24, 19, i24 << 13, A088);
        int A0810 = AnonymousClass5OF.A08(A0F7);
        int A0E8 = AnonymousClass5OF.A0E(A0F8, A0B, A0810, A088);
        int i25 = iArr[10];
        int i26 = A0E8 + i25;
        int A0F9 = AnonymousClass5OF.A0F(i26, 18, i26 << 14, A089);
        int A0811 = AnonymousClass5OF.A08(A0B);
        int A0E9 = AnonymousClass5OF.A0E(A0F9, A0F8, A0811, A089);
        int i27 = iArr[11];
        int A032 = A03(A0E9, i27, A0810);
        int A0812 = AnonymousClass5OF.A08(A0F8);
        int A0E10 = AnonymousClass5OF.A0E(A032, A0F9, A0812, A0810);
        int i28 = iArr[12];
        int A00 = A00(A0E10, i28, A0811);
        int A0813 = AnonymousClass5OF.A08(A0F9);
        int A0E11 = AnonymousClass5OF.A0E(A00, A032, A0813, A0811);
        int i29 = iArr[13];
        int A01 = A01(A0E11, i29, A0812);
        int A0814 = AnonymousClass5OF.A08(A032);
        int A0E12 = AnonymousClass5OF.A0E(A01, A00, A0814, A0812);
        int i30 = iArr[14];
        int A0C = AnonymousClass5OF.A0C(A0E12, i30, A0813);
        int A0815 = AnonymousClass5OF.A08(A00);
        int A0E13 = AnonymousClass5OF.A0E(A0C, A01, A0815, A0813);
        int i31 = iArr[15];
        int i32 = A0E13 + i31;
        int A0F10 = AnonymousClass5OF.A0F(i32, 24, i32 << 8, A0814);
        int A0816 = AnonymousClass5OF.A08(A01);
        int A06 = A06(i4, i3, i2, i, i16) + 1352829926;
        int A0F11 = AnonymousClass5OF.A0F(A06, 24, A06 << 8, i5);
        int A0C2 = AnonymousClass5OF.A0C(A06(A08, i2, A0F11, i5, i30), 1352829926, i4);
        int A0C3 = AnonymousClass5OF.A0C(A06(A082, A0F11, A0C2, i4, i20), 1352829926, A08);
        int A0817 = AnonymousClass5OF.A08(A0F11);
        int A0B2 = AnonymousClass5OF.A0B(A08 + (((A0817 ^ -1) | A0C2) ^ A0C3) + i7, 1352829926, A082);
        int A0818 = AnonymousClass5OF.A08(A0C2);
        int A05 = A05(A082, A0B2 ^ ((A0818 ^ -1) | A0C3), i23, 1352829926, A0817);
        int A0819 = AnonymousClass5OF.A08(A0C3);
        int A062 = A06(A0819, A0B2, A05, A0817, i11) + 1352829926;
        int A0F12 = AnonymousClass5OF.A0F(A062, 17, A062 << 15, A0818);
        int A0820 = AnonymousClass5OF.A08(A0B2);
        int A063 = A06(A0820, A05, A0F12, A0818, i27) + 1352829926;
        int A0F13 = AnonymousClass5OF.A0F(A063, 17, A063 << 15, A0819);
        int A0821 = AnonymousClass5OF.A08(A05);
        int A04 = A04(A0819 + (((A0821 ^ -1) | A0F12) ^ A0F13), i14, 1352829926, A0820);
        int A0822 = AnonymousClass5OF.A08(A0F12);
        int A012 = A01(A06(A0822, A0F13, A04, A0820, i29), 1352829926, A0821);
        int A0823 = AnonymousClass5OF.A08(A0F13);
        int A013 = A01(A06(A0823, A04, A012, A0821, i18), 1352829926, A0822);
        int A0824 = AnonymousClass5OF.A08(A04);
        int A02 = A02(A06(A0824, A012, A013, A0822, i31), 1352829926, A0823);
        int A0825 = AnonymousClass5OF.A08(A012);
        int A0B3 = AnonymousClass5OF.A0B(A06(A0825, A013, A02, A0823, i22), 1352829926, A0824);
        int A0826 = AnonymousClass5OF.A08(A013);
        int A09 = AnonymousClass5OF.A09(A06(A0826, A02, A0B3, A0824, i9), 1352829926, A0825);
        int A0827 = AnonymousClass5OF.A08(A02);
        int A092 = AnonymousClass5OF.A09(A06(A0827, A0B3, A09, A0825, i25), 1352829926, A0826);
        int A0828 = AnonymousClass5OF.A08(A0B3);
        int A0A = AnonymousClass5OF.A0A(A06(A0828, A09, A092, A0826, i12), 1352829926, A0827);
        int A0829 = AnonymousClass5OF.A08(A09);
        int A002 = A00(A06(A0829, A092, A0A, A0827, i28), 1352829926, A0828);
        int A0830 = AnonymousClass5OF.A08(A092);
        int A014 = A01(AnonymousClass5OF.A0D(A0C, A0F10, A0816, A0814) + i20, 1518500249, A0815);
        int A0831 = AnonymousClass5OF.A08(A0C);
        int A003 = A00(AnonymousClass5OF.A0D(A0F10, A014, A0831, A0815) + i14, 1518500249, A0816);
        int A0832 = AnonymousClass5OF.A08(A0F10);
        int A022 = A02(AnonymousClass5OF.A0D(A014, A003, A0832, A0816) + i29, 1518500249, A0831);
        int A0833 = AnonymousClass5OF.A08(A014);
        int A052 = A05(A0831, ((A022 ^ -1) & A0833) | (A003 & A022), i9, 1518500249, A0832);
        int A0834 = AnonymousClass5OF.A08(A003);
        int A0B4 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0D(A022, A052, A0834, A0832) + i25, 1518500249, A0833);
        int A0835 = AnonymousClass5OF.A08(A022);
        int A0C4 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0D(A052, A0B4, A0835, A0833) + i18, 1518500249, A0834);
        int A0836 = AnonymousClass5OF.A08(A052);
        int A015 = A01(AnonymousClass5OF.A0D(A0B4, A0C4, A0836, A0834) + i31, 1518500249, A0835);
        int A0837 = AnonymousClass5OF.A08(A0B4);
        int A0D = AnonymousClass5OF.A0D(A0C4, A015, A0837, A0835) + i12 + 1518500249;
        int A0F14 = AnonymousClass5OF.A0F(A0D, 17, A0D << 15, A0836);
        int A0838 = AnonymousClass5OF.A08(A0C4);
        int A016 = A01(AnonymousClass5OF.A0D(A015, A0F14, A0838, A0836) + i28, 1518500249, A0837);
        int A0839 = AnonymousClass5OF.A08(A015);
        int A0A2 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0D(A0F14, A016, A0839, A0837) + i7, 1518500249, A0838);
        int A0840 = AnonymousClass5OF.A08(A0F14);
        int A0D2 = AnonymousClass5OF.A0D(A016, A0A2, A0840, A0838) + i23 + 1518500249;
        int A0F15 = AnonymousClass5OF.A0F(A0D2, 17, A0D2 << 15, A0839);
        int A0841 = AnonymousClass5OF.A08(A016);
        int A0C5 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0D(A0A2, A0F15, A0841, A0839) + i16, 1518500249, A0840);
        int A0842 = AnonymousClass5OF.A08(A0A2);
        int A0B5 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0D(A0F15, A0C5, A0842, A0840) + i11, 1518500249, A0841);
        int A0843 = AnonymousClass5OF.A08(A0F15);
        int A017 = A01(AnonymousClass5OF.A0D(A0C5, A0B5, A0843, A0841) + i30, 1518500249, A0842);
        int A0844 = AnonymousClass5OF.A08(A0C5);
        int A053 = A05(A0842, ((A017 ^ -1) & A0844) | (A0B5 & A017), i27, 1518500249, A0843);
        int A0845 = AnonymousClass5OF.A08(A0B5);
        int i33 = A053 ^ -1;
        int A0A3 = AnonymousClass5OF.A0A(A0843 + ((i33 & A0845) | (A017 & A053)) + i22, 1518500249, A0844);
        int A0846 = AnonymousClass5OF.A08(A017);
        int A0C6 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0G(A002, A0830, A0A, A0828, i18), 1548603684, A0829);
        int A0847 = AnonymousClass5OF.A08(A0A);
        int A0G = AnonymousClass5OF.A0G(A0C6, A0847, A002, A0829, i27) + 1548603684;
        int A0F16 = AnonymousClass5OF.A0F(A0G, 19, A0G << 13, A0830);
        int A0848 = AnonymousClass5OF.A08(A002);
        int A033 = A03(AnonymousClass5OF.A0G(A0F16, A0848, A0C6, A0830, i12), 1548603684, A0847);
        int A0849 = AnonymousClass5OF.A08(A0C6);
        int A018 = A01(AnonymousClass5OF.A0G(A033, A0849, A0F16, A0847, i20), 1548603684, A0848);
        int A0850 = AnonymousClass5OF.A08(A0F16);
        int A0A4 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0G(A018, A0850, A033, A0848, i7), 1548603684, A0849);
        int A0851 = AnonymousClass5OF.A08(A033);
        int A023 = A02(AnonymousClass5OF.A0G(A0A4, A0851, A018, A0849, i29), 1548603684, A0850);
        int A0852 = AnonymousClass5OF.A08(A018);
        int A0C7 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0G(A023, A0852, A0A4, A0850, i16), 1548603684, A0851);
        int A0853 = AnonymousClass5OF.A08(A0A4);
        int A0B6 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0G(A0C7, A0853, A023, A0851, i25), 1548603684, A0852);
        int A0854 = AnonymousClass5OF.A08(A023);
        int A019 = A01(AnonymousClass5OF.A0G(A0B6, A0854, A0C7, A0852, i30), 1548603684, A0853);
        int A0855 = AnonymousClass5OF.A08(A0C7);
        int A0110 = A01(AnonymousClass5OF.A0G(A019, A0855, A0B6, A0853, i31), 1548603684, A0854);
        int A0856 = AnonymousClass5OF.A08(A0B6);
        int A0A5 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0G(A0110, A0856, A019, A0854, i22), 1548603684, A0855);
        int A0857 = AnonymousClass5OF.A08(A019);
        int A0111 = A01(AnonymousClass5OF.A0G(A0A5, A0857, A0110, A0855, i28), 1548603684, A0856);
        int A0858 = AnonymousClass5OF.A08(A0110);
        int A004 = A00(AnonymousClass5OF.A0G(A0111, A0858, A0A5, A0856, i14), 1548603684, A0857);
        int A0859 = AnonymousClass5OF.A08(A0A5);
        int A034 = A03(AnonymousClass5OF.A0G(A004, A0859, A0111, A0857, i23), 1548603684, A0858);
        int A0860 = AnonymousClass5OF.A08(A0111);
        int A0G2 = AnonymousClass5OF.A0G(A034, A0860, A004, A0858, i9) + 1548603684;
        int A0F17 = AnonymousClass5OF.A0F(A0G2, 19, A0G2 << 13, A0859);
        int A0861 = AnonymousClass5OF.A08(A004);
        int A0B7 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0G(A0F17, A0861, A034, A0859, i11), 1548603684, A0860);
        int A0862 = AnonymousClass5OF.A08(A034);
        int A0B8 = AnonymousClass5OF.A0B(A0844 + ((A0A3 | i33) ^ A0846) + i12, 1859775393, A0845);
        int A0863 = AnonymousClass5OF.A08(A053);
        int A054 = A05(A0845, (A0B8 | (A0A3 ^ -1)) ^ A0863, i25, 1859775393, A0846);
        int A0864 = AnonymousClass5OF.A08(A0A3);
        int A005 = A00(AnonymousClass5OF.A0H(A0B8, A054, A0864, A0846, i30), 1859775393, A0863);
        int A0865 = AnonymousClass5OF.A08(A0B8);
        int A0112 = A01(AnonymousClass5OF.A0H(A054, A005, A0865, A0863, i14), 1859775393, A0864);
        int A0866 = AnonymousClass5OF.A08(A054);
        int A093 = AnonymousClass5OF.A09(AnonymousClass5OF.A0H(A005, A0112, A0866, A0864, i23), 1859775393, A0865);
        int A0867 = AnonymousClass5OF.A08(A005);
        int A0C8 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0H(A0112, A093, A0867, A0865, i31), 1859775393, A0866);
        int A0868 = AnonymousClass5OF.A08(A0112);
        int A055 = A05(A0866, (A0C8 | (A093 ^ -1)) ^ A0868, i22, 1859775393, A0867);
        int A0869 = AnonymousClass5OF.A08(A093);
        int A035 = A03(AnonymousClass5OF.A0H(A0C8, A055, A0869, A0867, i9), 1859775393, A0868);
        int A0870 = AnonymousClass5OF.A08(A0C8);
        int A094 = AnonymousClass5OF.A09(AnonymousClass5OF.A0H(A055, A035, A0870, A0868, i11), 1859775393, A0869);
        int A0871 = AnonymousClass5OF.A08(A055);
        int A024 = A02(AnonymousClass5OF.A0H(A035, A094, A0871, A0869, i20), 1859775393, A0870);
        int A0872 = AnonymousClass5OF.A08(A035);
        int A056 = A05(A0870, (A024 | (A094 ^ -1)) ^ A0872, i7, 1859775393, A0871);
        int A0873 = AnonymousClass5OF.A08(A094);
        int A006 = A00(AnonymousClass5OF.A0H(A024, A056, A0873, A0871, i18), 1859775393, A0872);
        int A0874 = AnonymousClass5OF.A08(A024);
        int A042 = A04(A0872 + (((A056 ^ -1) | A006) ^ A0874), i29, 1859775393, A0873);
        int A0875 = AnonymousClass5OF.A08(A056);
        int A0A6 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0H(A006, A042, A0875, A0873, i27), 1859775393, A0874);
        int A0876 = AnonymousClass5OF.A08(A006);
        int A0113 = A01(AnonymousClass5OF.A0H(A042, A0A6, A0876, A0874, i16), 1859775393, A0875);
        int A0877 = AnonymousClass5OF.A08(A042);
        int A043 = A04(A0875 + (((A0A6 ^ -1) | A0113) ^ A0877), i28, 1859775393, A0876);
        int A0878 = AnonymousClass5OF.A08(A0A6);
        int A0C9 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0H(A0F17, A0B7, A0862, A0860, i31), 1836072691, A0861);
        int A0879 = AnonymousClass5OF.A08(A0F17);
        int A0114 = A01(AnonymousClass5OF.A0H(A0B7, A0C9, A0879, A0861, i16), 1836072691, A0862);
        int A0880 = AnonymousClass5OF.A08(A0B7);
        int A036 = A03(AnonymousClass5OF.A0H(A0C9, A0114, A0880, A0862, i9), 1836072691, A0879);
        int A0881 = AnonymousClass5OF.A08(A0C9);
        int A0B9 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0H(A0114, A036, A0881, A0879, i12), 1836072691, A0880);
        int A0882 = AnonymousClass5OF.A08(A0114);
        int A025 = A02(AnonymousClass5OF.A0H(A036, A0B9, A0882, A0880, i20), 1836072691, A0881);
        int A0883 = AnonymousClass5OF.A08(A036);
        int A007 = A00(AnonymousClass5OF.A0H(A0B9, A025, A0883, A0881, i30), 1836072691, A0882);
        int A0884 = AnonymousClass5OF.A08(A0B9);
        int A008 = A00(AnonymousClass5OF.A0H(A025, A007, A0884, A0882, i18), 1836072691, A0883);
        int A0885 = AnonymousClass5OF.A08(A025);
        int A095 = AnonymousClass5OF.A09(AnonymousClass5OF.A0H(A007, A008, A0885, A0883, i23), 1836072691, A0884);
        int A0886 = AnonymousClass5OF.A08(A007);
        int A0A7 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0H(A008, A095, A0886, A0884, i27), 1836072691, A0885);
        int A0887 = AnonymousClass5OF.A08(A008);
        int A057 = A05(A0885, (A0A7 | (A095 ^ -1)) ^ A0887, i22, 1836072691, A0886);
        int A0888 = AnonymousClass5OF.A08(A095);
        int A044 = A04(A0886 + (((A0A7 ^ -1) | A057) ^ A0888), i28, 1836072691, A0887);
        int A0889 = AnonymousClass5OF.A08(A0A7);
        int A096 = AnonymousClass5OF.A09(AnonymousClass5OF.A0H(A057, A044, A0889, A0887, i11), 1836072691, A0888);
        int A0890 = AnonymousClass5OF.A08(A057);
        int A058 = A05(A0888, (A096 | (A044 ^ -1)) ^ A0890, i25, 1836072691, A0889);
        int A0891 = AnonymousClass5OF.A08(A044);
        int A059 = A05(A0889, ((A096 ^ -1) | A058) ^ A0891, i7, 1836072691, A0890);
        int A0892 = AnonymousClass5OF.A08(A096);
        int A0115 = A01(AnonymousClass5OF.A0H(A058, A059, A0892, A0890, i14), 1836072691, A0891);
        int A0893 = AnonymousClass5OF.A08(A058);
        int A045 = A04(A0891 + (((A059 ^ -1) | A0115) ^ A0893), i29, 1836072691, A0892);
        int A0894 = AnonymousClass5OF.A08(A059);
        int A0B10 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0G(A043, A0878, A0113, A0876, i9), -1894007588, A0877);
        int A0895 = AnonymousClass5OF.A08(A0113);
        int A0A8 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0G(A0B10, A0895, A043, A0877, i23), -1894007588, A0878);
        int A0896 = AnonymousClass5OF.A08(A043);
        int A097 = AnonymousClass5OF.A09(AnonymousClass5OF.A0G(A0A8, A0896, A0B10, A0878, i27), -1894007588, A0895);
        int A0897 = AnonymousClass5OF.A08(A0B10);
        int A037 = A03(AnonymousClass5OF.A0G(A097, A0897, A0A8, A0895, i25), -1894007588, A0896);
        int A0898 = AnonymousClass5OF.A08(A0A8);
        int A098 = AnonymousClass5OF.A09(AnonymousClass5OF.A0G(A037, A0898, A097, A0896, i7), -1894007588, A0897);
        int A0899 = AnonymousClass5OF.A08(A097);
        int A038 = A03(AnonymousClass5OF.A0G(A098, A0899, A037, A0897, i22), -1894007588, A0898);
        int A08100 = AnonymousClass5OF.A08(A037);
        int A0C10 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0G(A038, A08100, A098, A0898, i28), -1894007588, A0899);
        int A08101 = AnonymousClass5OF.A08(A098);
        int A026 = A02(AnonymousClass5OF.A0G(A0C10, A08101, A038, A0899, i14), -1894007588, A08100);
        int A08102 = AnonymousClass5OF.A08(A038);
        int A0C11 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0G(A026, A08102, A0C10, A08100, i29), -1894007588, A08101);
        int A08103 = AnonymousClass5OF.A08(A0C10);
        int A099 = AnonymousClass5OF.A09(AnonymousClass5OF.A0G(A0C11, A08103, A026, A08101, i12), -1894007588, A08102);
        int A08104 = AnonymousClass5OF.A08(A026);
        int A0G3 = AnonymousClass5OF.A0G(A099, A08104, A0C11, A08102, i20) - 1894007588;
        int A0F18 = AnonymousClass5OF.A0F(A0G3, 27, A0G3 << 5, A08103);
        int A08105 = AnonymousClass5OF.A08(A0C11);
        int A009 = A00(AnonymousClass5OF.A0G(A0F18, A08105, A099, A08103, i31), -1894007588, A08104);
        int A08106 = AnonymousClass5OF.A08(A099);
        int A027 = A02(AnonymousClass5OF.A0G(A009, A08106, A0F18, A08104, i30), -1894007588, A08105);
        int A08107 = AnonymousClass5OF.A08(A0F18);
        int A0010 = A00(AnonymousClass5OF.A0G(A027, A08107, A009, A08105, i16), -1894007588, A08106);
        int A08108 = AnonymousClass5OF.A08(A009);
        int A0G4 = AnonymousClass5OF.A0G(A0010, A08108, A027, A08106, i18) - 1894007588;
        int A0F19 = AnonymousClass5OF.A0F(A0G4, 27, A0G4 << 5, A08107);
        int A08109 = AnonymousClass5OF.A08(A027);
        int A0A9 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0G(A0F19, A08109, A0010, A08107, i11), -1894007588, A08108);
        int A08110 = AnonymousClass5OF.A08(A0010);
        int A039 = A03(AnonymousClass5OF.A0D(A0115, A045, A0894, A0892) + i22, 2053994217, A0893);
        int A08111 = AnonymousClass5OF.A08(A0115);
        int A046 = A04(AnonymousClass5OF.A0D(A045, A039, A08111, A0893), i18, 2053994217, A0894);
        int A08112 = AnonymousClass5OF.A08(A045);
        int A028 = A02(AnonymousClass5OF.A0D(A039, A046, A08112, A0894) + i14, 2053994217, A08111);
        int A08113 = AnonymousClass5OF.A08(A039);
        int A0B11 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0D(A046, A028, A08113, A08111) + i9, 2053994217, A08112);
        int A08114 = AnonymousClass5OF.A08(A046);
        int A0910 = AnonymousClass5OF.A09(AnonymousClass5OF.A0D(A028, A0B11, A08114, A08112) + i12, 2053994217, A08113);
        int A08115 = AnonymousClass5OF.A08(A028);
        int A0911 = AnonymousClass5OF.A09(AnonymousClass5OF.A0D(A0B11, A0910, A08115, A08113) + i27, 2053994217, A08114);
        int A08116 = AnonymousClass5OF.A08(A0B11);
        int A0011 = A00(AnonymousClass5OF.A0D(A0910, A0911, A08116, A08114) + i31, 2053994217, A08115);
        int A08117 = AnonymousClass5OF.A08(A0910);
        int A0912 = AnonymousClass5OF.A09(AnonymousClass5OF.A0D(A0911, A0011, A08117, A08115) + i7, 2053994217, A08116);
        int A08118 = AnonymousClass5OF.A08(A0911);
        int A0012 = A00(AnonymousClass5OF.A0D(A0011, A0912, A08118, A08116) + i16, 2053994217, A08117);
        int A08119 = AnonymousClass5OF.A08(A0011);
        int A0C12 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0D(A0912, A0012, A08119, A08117) + i28, 2053994217, A08118);
        int A08120 = AnonymousClass5OF.A08(A0912);
        int A0A10 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0D(A0012, A0C12, A08120, A08118) + i11, 2053994217, A08119);
        int A08121 = AnonymousClass5OF.A08(A0012);
        int A0C13 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0D(A0C12, A0A10, A08121, A08119) + i29, 2053994217, A08120);
        int A08122 = AnonymousClass5OF.A08(A0C12);
        int A0A11 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0D(A0A10, A0C13, A08122, A08120) + i23, 2053994217, A08121);
        int A08123 = AnonymousClass5OF.A08(A0A10);
        int A047 = A04(AnonymousClass5OF.A0D(A0C13, A0A11, A08123, A08121), i20, 2053994217, A08122);
        int A08124 = AnonymousClass5OF.A08(A0C13);
        int A0310 = A03(AnonymousClass5OF.A0D(A0A11, A047, A08124, A08122) + i25, 2053994217, A08123);
        int A08125 = AnonymousClass5OF.A08(A0A11);
        int A029 = A02(AnonymousClass5OF.A0D(A047, A0310, A08125, A08123) + i30, 2053994217, A08124);
        int A08126 = AnonymousClass5OF.A08(A047);
        int A0C14 = AnonymousClass5OF.A0C(A06(A08110, A0F19, A0A9, A08108, i14), -1454113458, A08109);
        int A08127 = AnonymousClass5OF.A08(A0F19);
        int A0311 = A03(A06(A08127, A0A9, A0C14, A08109, i7), -1454113458, A08110);
        int A08128 = AnonymousClass5OF.A08(A0A9);
        int A048 = A04(A08110 + (((A08128 ^ -1) | A0C14) ^ A0311), i16, -1454113458, A08127);
        int A08129 = AnonymousClass5OF.A08(A0C14);
        int A0B12 = AnonymousClass5OF.A0B(A06(A08129, A0311, A048, A08127, i23), -1454113458, A08128);
        int A08130 = AnonymousClass5OF.A08(A0311);
        int A0013 = A00(A06(A08130, A048, A0B12, A08128, i20), -1454113458, A08129);
        int A08131 = AnonymousClass5OF.A08(A048);
        int A0210 = A02(A06(A08131, A0B12, A0013, A08129, i28), -1454113458, A08130);
        int A08132 = AnonymousClass5OF.A08(A0B12);
        int A0510 = A05(A08130, A0210 ^ ((A08132 ^ -1) | A0013), i11, -1454113458, A08131);
        int A08133 = AnonymousClass5OF.A08(A0013);
        int A0A12 = AnonymousClass5OF.A0A(A06(A08133, A0210, A0510, A08131, i25), -1454113458, A08132);
        int A08134 = AnonymousClass5OF.A08(A0210);
        int A049 = A04(A08132 + (((A08134 ^ -1) | A0510) ^ A0A12), i30, -1454113458, A08133);
        int A08135 = AnonymousClass5OF.A08(A0510);
        int A0A13 = AnonymousClass5OF.A0A(A06(A08135, A0A12, A049, A08133, i9), -1454113458, A08134);
        int A08136 = AnonymousClass5OF.A08(A0A12);
        int A0511 = A05(A08134, ((A08136 ^ -1) | A049) ^ A0A13, i12, -1454113458, A08135);
        int A08137 = AnonymousClass5OF.A08(A049);
        int A0913 = AnonymousClass5OF.A09(A06(A08137, A0A13, A0511, A08135, i22), -1454113458, A08136);
        int A08138 = AnonymousClass5OF.A08(A0A13);
        int A0B13 = AnonymousClass5OF.A0B(A06(A08138, A0511, A0913, A08136, i27), -1454113458, A08137);
        int A08139 = AnonymousClass5OF.A08(A0511);
        int A0211 = A02(A06(A08139, A0913, A0B13, A08137, i18), -1454113458, A08138);
        int A08140 = AnonymousClass5OF.A08(A0913);
        int A0410 = A04(A08138 + (((A08140 ^ -1) | A0B13) ^ A0211), i31, -1454113458, A08139);
        int A08141 = AnonymousClass5OF.A08(A0B13);
        int A0014 = A00(A06(A08141, A0211, A0410, A08139, i29), -1454113458, A08140);
        int A08142 = AnonymousClass5OF.A08(A0211);
        int A0212 = A02(AnonymousClass5OF.A0E(A029, A0310, A08126, A08124), i28, A08125);
        int A08143 = AnonymousClass5OF.A08(A0310);
        int A0411 = A04(A08125, (A0212 ^ A029) ^ A08143, i31, A08126);
        int A08144 = AnonymousClass5OF.A08(A029);
        int A0A14 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0E(A0411, A0212, A08144, A08126), i25, A08143);
        int A08145 = AnonymousClass5OF.A08(A0212);
        int A0C15 = AnonymousClass5OF.A0C(AnonymousClass5OF.A0E(A0A14, A0411, A08145, A08143), i14, A08144);
        int A08146 = AnonymousClass5OF.A08(A0411);
        int A0A15 = AnonymousClass5OF.A0A(AnonymousClass5OF.A0E(A0C15, A0A14, A08146, A08144), i9, A08145);
        int A08147 = AnonymousClass5OF.A08(A0A14);
        int A0412 = A04(A08145, (A0A15 ^ A0C15) ^ A08147, i16, A08146);
        int A08148 = AnonymousClass5OF.A08(A0C15);
        int A0914 = AnonymousClass5OF.A09(AnonymousClass5OF.A0E(A0412, A0A15, A08148, A08146), i22, A08147);
        int A08149 = AnonymousClass5OF.A08(A0A15);
        int A0015 = A00(AnonymousClass5OF.A0E(A0914, A0412, A08149, A08147), i20, A08148);
        int A08150 = AnonymousClass5OF.A08(A0412);
        int A0213 = A02(AnonymousClass5OF.A0E(A0015, A0914, A08150, A08148), i18, A08149);
        int A08151 = AnonymousClass5OF.A08(A0914);
        int A0E14 = AnonymousClass5OF.A0E(A0213, A0015, A08151, A08149) + i11;
        int A0F20 = AnonymousClass5OF.A0F(A0E14, 19, A0E14 << 13, A08150);
        int A08152 = AnonymousClass5OF.A08(A0015);
        int A0016 = A00(AnonymousClass5OF.A0E(A0F20, A0213, A08152, A08150), i29, A08151);
        int A08153 = AnonymousClass5OF.A08(A0213);
        int A0413 = A04(A08151, (A0016 ^ A0F20) ^ A08153, i30, A08152);
        int A08154 = AnonymousClass5OF.A08(A0F20);
        int A0312 = A03(AnonymousClass5OF.A0E(A0413, A0016, A08154, A08152), i7, A08153);
        int A08155 = AnonymousClass5OF.A08(A0016);
        int A0E15 = AnonymousClass5OF.A0E(A0312, A0413, A08155, A08153) + i12;
        int A0F21 = AnonymousClass5OF.A0F(A0E15, 19, A0E15 << 13, A08154);
        int A08156 = AnonymousClass5OF.A08(A0413);
        int A0B14 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0E(A0F21, A0312, A08156, A08154), i23, A08155);
        int A08157 = AnonymousClass5OF.A08(A0312);
        int A0B15 = AnonymousClass5OF.A0B(AnonymousClass5OF.A0E(A0B14, A0F21, A08157, A08155), i27, A08156);
        this.A01 = i3 + A08142 + A08157;
        this.A02 = i4 + A08141 + A08156;
        this.A03 = i5 + A08140 + A0B15;
        this.A04 = i + A0014 + A0B14;
        this.A00 = AnonymousClass5OF.A08(A0F21) + A0410 + i2;
        this.A05 = 0;
        for (int i34 = 0; i34 != iArr.length; i34++) {
            iArr[i34] = 0;
        }
    }
}
