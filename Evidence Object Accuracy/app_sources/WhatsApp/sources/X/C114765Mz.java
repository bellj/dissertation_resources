package X;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

/* renamed from: X.5Mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114765Mz extends AnonymousClass1TM implements AbstractC115545Ry {
    public AnonymousClass1TL A00;

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    public String toString() {
        return A03();
    }

    public C114765Mz(AnonymousClass1TL r2) {
        if ((r2 instanceof AnonymousClass5NA) || (r2 instanceof AnonymousClass5NE)) {
            this.A00 = r2;
            return;
        }
        throw C12970iu.A0f("unknown object passed to Time");
    }

    public static C114765Mz A00(Object obj) {
        if (obj == null || (obj instanceof C114765Mz)) {
            return (C114765Mz) obj;
        }
        if ((obj instanceof AnonymousClass5NA) || (obj instanceof AnonymousClass5NE)) {
            return new C114765Mz((AnonymousClass1TL) obj);
        }
        throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("unknown object in factory: ")));
    }

    public String A03() {
        String str;
        AnonymousClass1TL r1 = this.A00;
        if (!(r1 instanceof AnonymousClass5NA)) {
            return ((AnonymousClass5NE) r1).A0B();
        }
        String A0B = ((AnonymousClass5NA) r1).A0B();
        char charAt = A0B.charAt(0);
        StringBuilder A0h = C12960it.A0h();
        if (charAt < '5') {
            str = "20";
        } else {
            str = "19";
        }
        A0h.append(str);
        return C12960it.A0d(A0B, A0h);
    }

    public Date A04() {
        StringBuilder A0h;
        String str;
        try {
            AnonymousClass1TL r2 = this.A00;
            if (!(r2 instanceof AnonymousClass5NA)) {
                return ((AnonymousClass5NE) r2).A0D();
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssz");
            simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
            String A0B = ((AnonymousClass5NA) r2).A0B();
            if (A0B.charAt(0) < '5') {
                A0h = C12960it.A0h();
                str = "20";
            } else {
                A0h = C12960it.A0h();
                str = "19";
            }
            A0h.append(str);
            return AnonymousClass3GX.A00(simpleDateFormat.parse(C12960it.A0d(A0B, A0h)));
        } catch (ParseException e) {
            throw C12960it.A0U(C12960it.A0d(e.getMessage(), C12960it.A0k("invalid date string: ")));
        }
    }
}
