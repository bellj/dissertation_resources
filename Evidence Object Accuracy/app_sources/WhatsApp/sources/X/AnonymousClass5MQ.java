package X;

/* renamed from: X.5MQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MQ extends AnonymousClass1TM {
    public AnonymousClass5MA A00;

    public AnonymousClass5MQ(AnonymousClass5MA r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    public String toString() {
        int i;
        byte[] A0B = this.A00.A0B();
        int length = A0B.length;
        StringBuilder A0h = C12960it.A0h();
        if (length == 1) {
            A0h.append("KeyUsage: 0x");
            i = A0B[0] & 255;
        } else {
            A0h.append("KeyUsage: 0x");
            i = (A0B[0] & 255) | ((A0B[1] & 255) << 8);
        }
        return C12960it.A0d(Integer.toHexString(i), A0h);
    }
}
