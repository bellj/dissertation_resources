package X;

import android.net.Uri;

/* renamed from: X.1ms  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37711ms implements AbstractC37701mr {
    public final String A00;
    public final String A01;

    public C37711ms(String str) {
        this.A01 = str;
        String authority = Uri.parse(str).getAuthority();
        AnonymousClass009.A05(authority);
        this.A00 = authority;
    }

    @Override // X.AbstractC37701mr
    public String AAJ(C15450nH r2, C28481Nj r3, boolean z) {
        return this.A01;
    }
}
