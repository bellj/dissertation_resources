package X;

import android.content.SharedPreferences;

/* renamed from: X.0zX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22720zX {
    public final C22780zd A00;
    public final C22770zc A01;
    public final C15510nN A02;

    public C22720zX(C22780zd r1, C22770zc r2, C15510nN r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0065 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A00() {
        /*
            r5 = this;
            X.0zc r4 = r5.A01
            X.01H r0 = r4.A08
            java.lang.Object r2 = r0.get()
            android.content.SharedPreferences r2 = (android.content.SharedPreferences) r2
            java.lang.String r1 = "ab_props:hash"
            r0 = 0
            java.lang.String r1 = r2.getString(r1, r0)
            if (r1 == 0) goto L_0x0036
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch: JSONException -> 0x0030
            r0.<init>(r1)     // Catch: JSONException -> 0x0030
            X.1Zz r2 = X.AnonymousClass122.A00(r0)     // Catch: JSONException -> 0x0030
            if (r2 == 0) goto L_0x0036
            X.124 r1 = r4.A02     // Catch: JSONException -> 0x0030
            java.lang.String r0 = X.AnonymousClass029.A00     // Catch: JSONException -> 0x0030
            byte[] r1 = r1.A01(r2, r0)     // Catch: JSONException -> 0x0030
            if (r1 == 0) goto L_0x0036
            java.nio.charset.Charset r0 = X.C22770zc.A0A     // Catch: JSONException -> 0x0030
            java.lang.String r3 = new java.lang.String     // Catch: JSONException -> 0x0030
            r3.<init>(r1, r0)     // Catch: JSONException -> 0x0030
            goto L_0x0038
        L_0x0030:
            r1 = move-exception
            java.lang.String r0 = "AB Props Hash couldn't be decrypted"
            com.whatsapp.util.Log.e(r0, r1)
        L_0x0036:
            r1 = 0
            goto L_0x005d
        L_0x0038:
            X.01H r0 = r4.A07
            java.lang.Object r0 = r0.get()
            android.content.SharedPreferences r0 = (android.content.SharedPreferences) r0
            java.util.Map r2 = r0.getAll()
            java.util.Set r1 = r2.keySet()
            X.01H r0 = r4.A06
            java.lang.Object r0 = r0.get()
            java.util.Collection r0 = (java.util.Collection) r0
            r1.retainAll(r0)
            java.lang.String r0 = X.AnonymousClass123.A00(r2)
            boolean r0 = r3.equals(r0)
            r1 = r0 ^ 1
        L_0x005d:
            X.0nN r0 = r5.A02
            boolean r0 = r0.A01()
            if (r0 != 0) goto L_0x0069
            java.lang.String r0 = "unregistered"
            return r0
        L_0x0069:
            if (r1 == 0) goto L_0x006f
            java.lang.String r0 = "true"
            return r0
        L_0x006f:
            java.lang.String r0 = "false"
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22720zX.A00():java.lang.String");
    }

    public String A01() {
        boolean z;
        C22780zd r3 = this.A00;
        String string = ((SharedPreferences) r3.A01.get()).getString("server_props:hash", null);
        if (string != null) {
            z = !string.equals(AnonymousClass123.A00(((SharedPreferences) r3.A02.get()).getAll()));
        } else {
            z = false;
        }
        if (!this.A02.A01()) {
            return "unregistered";
        }
        return z ? "true" : "false";
    }
}
