package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipErrorDialogFragment;

/* renamed from: X.2wC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60192wC extends AbstractC75743kL {
    public final WaTextView A00;

    public C60192wC(View view, ParticipantsListViewModel participantsListViewModel) {
        super(view, participantsListViewModel);
        this.A00 = C12960it.A0N(view, R.id.button_text);
    }

    @Override // X.AbstractC75743kL
    public void A09(C92194Ux r3) {
        WaTextView waTextView;
        int i;
        int i2 = r3.A00;
        if (i2 == 2) {
            View view = this.A0H;
            view.setClickable(true);
            view.setOnClickListener(new View.OnClickListener() { // from class: X.2ja
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    try {
                        C12960it.A16(VoipErrorDialogFragment.A01(new C50102Ob(), 9), (ActivityC000900k) AnonymousClass12P.A01(view2.getContext(), ActivityC000900k.class));
                    } catch (IllegalStateException e) {
                        Log.w(C12960it.A0d(e.getMessage(), C12960it.A0k("CallInfoButtonViewHolder/showCallIsFullDialog/Context not an activity: ")));
                    }
                }
            });
            waTextView = this.A00;
            i = R.string.voip_joinable_call_is_fall_info;
        } else if (i2 != 3) {
            Log.w(C12960it.A0W(i2, "CallInfoButtonViewHolder/bind/Unsupported item type: "));
            return;
        } else {
            View view2 = this.A0H;
            view2.setClickable(false);
            view2.setOnClickListener(null);
            waTextView = this.A00;
            i = R.string.voip_joinable_call_inline_education;
        }
        waTextView.setText(i);
    }
}
