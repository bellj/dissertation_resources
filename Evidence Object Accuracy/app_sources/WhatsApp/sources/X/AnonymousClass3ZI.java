package X;

import android.os.SystemClock;
import com.whatsapp.jid.GroupJid;
import java.net.MalformedURLException;
import java.net.URL;

/* renamed from: X.3ZI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZI implements AbstractC21730xt {
    public final long A00 = SystemClock.elapsedRealtime();
    public final GroupJid A01;
    public final AnonymousClass5WE A02;
    public final String A03;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    public /* synthetic */ AnonymousClass3ZI(GroupJid groupJid, AnonymousClass5WE r5, String str) {
        this.A02 = r5;
        if (groupJid == null) {
            this.A01 = null;
        } else {
            this.A01 = groupJid;
        }
        this.A03 = str;
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r7, String str) {
        this.A02.AUJ(this.A01, this.A03, C41151sz.A00(r7), this.A00);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r13, String str) {
        int parseInt;
        AnonymousClass1V8 A0E = r13.A0E("picture");
        if (A0E != null) {
            String A0I = A0E.A0I("id", null);
            String A0I2 = A0E.A0I("type", null);
            String A0I3 = A0E.A0I("linked_group_jid", null);
            URL url = null;
            String A0I4 = A0E.A0I("url", null);
            String A0I5 = A0E.A0I("direct_path", null);
            if (A0I4 != null) {
                try {
                    url = new URL(A0I4);
                } catch (MalformedURLException unused) {
                    throw new AnonymousClass1V9("Malformed picture url");
                }
            }
            byte[] bArr = A0E.A01;
            C15580nU A04 = C15580nU.A04(A0I3);
            if (A0I == null) {
                parseInt = -1;
            } else {
                try {
                    parseInt = Integer.parseInt(A0I);
                } catch (NumberFormatException unused2) {
                    throw new AnonymousClass1V9(C12960it.A0d(A0I, C12960it.A0k("Malformed photo id=")));
                }
            }
            if (A0I2 != null) {
                this.A02.AUK(new C41831uE(A04, A0I5, url, bArr, parseInt, C12980iv.A03("preview".equals(A0I2) ? 1 : 0)), this.A00);
            }
        }
    }
}
