package X;

/* renamed from: X.51z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1095151z implements AnonymousClass5T7 {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if ((r8 instanceof X.C82933wQ) == false) goto L_0x0011;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        if ((r7 instanceof X.C82933wQ) == false) goto L_0x0021;
     */
    @Override // X.AnonymousClass5T7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A9i(X.AbstractC94534c0 r7, X.AbstractC94534c0 r8, X.AnonymousClass4RG r9) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof X.C83003wX
            boolean r0 = X.C72473ef.A00(r0)
            r5 = 0
            if (r0 == 0) goto L_0x0011
            X.4c0 r8 = X.C83003wX.A00(r8)
            boolean r0 = r8 instanceof X.C82933wQ
            if (r0 != 0) goto L_0x0047
        L_0x0011:
            X.3wZ r4 = r8.A07()
            boolean r0 = r7 instanceof X.C83003wX
            if (r0 == 0) goto L_0x0021
            X.4c0 r7 = X.C83003wX.A00(r7)
            boolean r0 = r7 instanceof X.C82933wQ
            if (r0 != 0) goto L_0x0047
        L_0x0021:
            X.3wZ r0 = r7.A07()
            java.util.Iterator r3 = r0.iterator()
        L_0x0029:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0048
            java.lang.Object r2 = r3.next()
            java.util.Iterator r1 = r4.iterator()
        L_0x0037:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0029
            java.lang.Object r0 = r1.next()
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0037
        L_0x0047:
            return r5
        L_0x0048:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1095151z.A9i(X.4c0, X.4c0, X.4RG):boolean");
    }
}
