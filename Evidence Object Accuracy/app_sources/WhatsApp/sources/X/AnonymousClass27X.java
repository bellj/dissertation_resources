package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.facebook.redex.RunnableBRunnable0Shape0S0100002_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.27X */
/* loaded from: classes2.dex */
public class AnonymousClass27X extends AnonymousClass27Y implements AnonymousClass1s9, SurfaceHolder.Callback {
    public static final String[] A0a = {"GT-I9195", "GT-I9190", "GT-I9192"};
    public static final String[] A0b = {"GT-I9505", "GT-I9506", "GT-I9505G", "SGH-I337", "SGH-M919", "SCH-I545", "SPH-L720", "SCH-R970", "GT-I9508", "SGH-N045", "SC-04E"};
    public int A00;
    public int A01;
    public int A02;
    public SurfaceTexture A03;
    public Camera.Size A04;
    public Camera.Size A05;
    public Camera.Size A06;
    public Camera A07;
    public MediaRecorder A08;
    public Handler A09;
    public HandlerThread A0A;
    public C06450Tr A0B;
    public AnonymousClass0ON A0C;
    public AnonymousClass0QH A0D;
    public AnonymousClass0QH A0E;
    public AbstractC467527b A0F;
    public AnonymousClass01d A0G;
    public C15890o4 A0H;
    public C21200x2 A0I;
    public C16630pM A0J;
    public String A0K;
    public List A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public byte[] A0S;
    public final Handler A0T;
    public final Display A0U;
    public final SurfaceHolder A0V;
    public final AnonymousClass3WB A0W;
    public final C73643gZ A0X;
    public final C64113Eh A0Y;
    public final float[] A0Z;

    public static int A00(float f) {
        if (f < -995.0f) {
            f = -995.0f;
        } else if (f > 995.0f) {
            f = 995.0f;
        }
        return (int) f;
    }

    @Override // X.AnonymousClass1s9
    public void Aar() {
    }

    @Override // X.AnonymousClass1s9
    public int getCameraApi() {
        return 0;
    }

    @Override // X.AnonymousClass1s9
    public int getCameraType() {
        return 0;
    }

    @Override // X.AnonymousClass1s9
    public void pause() {
    }

    public AnonymousClass27X(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AnonymousClass27X(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0T = new Handler(Looper.getMainLooper());
        this.A0Z = new float[16];
        AnonymousClass3WB r3 = new AnonymousClass3WB(this);
        this.A0W = r3;
        SharedPreferences sharedPreferences = getSharedPreferences();
        this.A00 = sharedPreferences.getInt("camera_index", 0);
        this.A0K = sharedPreferences.getString("flash_mode", "off");
        SurfaceHolder holder = getHolder();
        this.A0V = holder;
        holder.addCallback(this);
        holder.setType(3);
        this.A0U = AnonymousClass01d.A02(context).getDefaultDisplay();
        this.A0X = new C73643gZ(context, this);
        this.A0Y = new C64113Eh(new AbstractC115965Tp() { // from class: X.54n
            @Override // X.AbstractC115965Tp
            public final void AT1(C49262Kb r2) {
                AbstractC467527b r0 = AnonymousClass27X.this.A0F;
                if (r0 != null) {
                    r0.AUS(r2);
                }
            }
        }, r3, 5);
    }

    public static Camera.Size A01(List list, int i, int i2) {
        double d = (double) i;
        double d2 = d / ((double) i2);
        Camera.Size size = null;
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Camera.Size size2 = (Camera.Size) it.next();
                int i3 = size2.width;
                double d3 = (double) i3;
                int i4 = size2.height;
                double d4 = d3 / ((double) i4);
                double d5 = d3 / d;
                if (i3 * i4 >= 153600 && d5 <= 1.5d && Math.abs(d4 - d2) <= 0.1d && A04(size2, size, i, i2)) {
                    size = size2;
                }
            }
            if (size == null) {
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    Camera.Size size3 = (Camera.Size) it2.next();
                    if (((double) size3.width) / d <= 1.5d && A04(size3, size, i, i2)) {
                        size = size3;
                    }
                }
                if (size == null) {
                    Iterator it3 = list.iterator();
                    while (it3.hasNext()) {
                        Camera.Size size4 = (Camera.Size) it3.next();
                        if (A04(size4, size, i, i2)) {
                            size = size4;
                        }
                    }
                }
            }
        }
        return size;
    }

    public static String A02(List list) {
        StringBuilder sb = new StringBuilder();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Camera.Size size = (Camera.Size) it.next();
            sb.append(size.width);
            sb.append('x');
            sb.append(size.height);
            sb.append(", ");
        }
        if (sb.length() > 1) {
            sb.deleteCharAt(sb.length() - 2);
        }
        return sb.toString();
    }

    public static /* synthetic */ void A03(AnonymousClass27X r4) {
        synchronized (r4) {
            Camera camera = r4.A07;
            if (camera == null) {
                try {
                    if (r4.A00 >= Camera.getNumberOfCameras()) {
                        r4.A00 = Camera.getNumberOfCameras() - 1;
                    }
                    Camera open = Camera.open(r4.A00);
                    r4.A07 = open;
                    open.setErrorCallback(new Camera.ErrorCallback() { // from class: X.3LM
                        @Override // android.hardware.Camera.ErrorCallback
                        public final void onError(int i, Camera camera2) {
                            AnonymousClass27X r2 = AnonymousClass27X.this;
                            synchronized (r2) {
                                StringBuilder A0h = C12960it.A0h();
                                A0h.append("cameraview/start-camera camera error:");
                                A0h.append(i);
                                A0h.append(" takingpicture:");
                                A0h.append(r2.A0R);
                                A0h.append(" recording:");
                                A0h.append(r2.A0P);
                                A0h.append(" inpreview:");
                                A0h.append(r2.A0M);
                                Log.w(A0h.toString());
                                if (i == 100) {
                                    r2.A09();
                                    r2.A05();
                                } else if (i == 2) {
                                    Camera camera3 = r2.A07;
                                    if (camera3 != null) {
                                        camera3.release();
                                    }
                                    r2.A07 = null;
                                    Log.i(C12960it.A0W(2, "cameraview/on-error "));
                                    AbstractC467527b r0 = r2.A0F;
                                    if (r0 != null) {
                                        r0.ANb(2);
                                    }
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    Camera camera2 = r4.A07;
                    if (camera2 != null) {
                        camera2.release();
                    }
                    r4.A07 = null;
                    Log.e("cameraview/start-camera error opening camera", e);
                    if (r4.A00 != 0) {
                        r4.getSharedPreferences().edit().putInt("camera_index", 0).apply();
                    }
                    r4.A06();
                }
                Camera camera3 = r4.A07;
                if (camera3 != null) {
                    try {
                        camera3.setPreviewDisplay(r4.A0V);
                        r4.A08();
                    } catch (IOException | RuntimeException e2) {
                        r4.A07.release();
                        r4.A07 = null;
                        Log.e("cameraview/start-camera", e2);
                        if (r4.A00 != 0) {
                            r4.getSharedPreferences().edit().putInt("camera_index", 0).apply();
                        }
                        r4.A06();
                    }
                }
            } else {
                try {
                    camera.reconnect();
                } catch (IOException e3) {
                    r4.A07.release();
                    r4.A07 = null;
                    Log.e("cameraview/start-camera error reconnecting camera", e3);
                    r4.A06();
                }
            }
        }
    }

    public static boolean A04(Camera.Size size, Camera.Size size2, int i, int i2) {
        if (size2 == null) {
            return true;
        }
        int abs = Math.abs(size2.height - i2);
        if ((Math.abs(size.height - i2) * i) + (Math.abs(size.width - i) * i2) >= (abs * i) + (Math.abs(size2.width - i) * i2)) {
            return false;
        }
        return true;
    }

    public void A05() {
        this.A09.post(new RunnableBRunnable0Shape3S0100000_I0_3(this, 33));
    }

    public final void A06() {
        StringBuilder sb = new StringBuilder("cameraview/on-error ");
        sb.append(1);
        Log.i(sb.toString());
        AbstractC467527b r0 = this.A0F;
        if (r0 != null) {
            r0.ANb(1);
        }
    }

    public final void A07() {
        MediaRecorder mediaRecorder = this.A08;
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            this.A08.release();
            this.A08 = null;
            this.A0P = false;
            Camera camera = this.A07;
            if (camera != null) {
                try {
                    camera.lock();
                    Camera.Parameters parameters = this.A07.getParameters();
                    parameters.getFlashMode();
                    if ("torch".equals(parameters.getFlashMode())) {
                        parameters.setFlashMode("off");
                        this.A07.setParameters(parameters);
                        this.A07.stopPreview();
                        this.A0M = false;
                    }
                } catch (RuntimeException e) {
                    Log.e("failed to lock the camera, it's in use by another process or WhatsApp video call.", e);
                }
            }
        }
        SurfaceTexture surfaceTexture = this.A03;
        if (surfaceTexture != null) {
            surfaceTexture.release();
            this.A03 = null;
        }
        AnonymousClass0QH r2 = this.A0D;
        if (r2 != null) {
            r2.A03();
            Surface surface = r2.A03;
            if (surface != null) {
                if (r2.A05) {
                    surface.release();
                }
                r2.A03 = null;
            }
            this.A0D = null;
        }
        AnonymousClass0ON r1 = this.A0C;
        if (r1 != null) {
            if (r1.A00 != null) {
                r1.A00 = null;
            }
            this.A0C = null;
        }
        AnonymousClass0QH r22 = this.A0E;
        if (r22 != null) {
            r22.A03();
            Surface surface2 = r22.A03;
            if (surface2 != null) {
                if (r22.A05) {
                    surface2.release();
                }
                r22.A03 = null;
            }
            this.A0E = null;
        }
        C06450Tr r0 = this.A0B;
        if (r0 != null) {
            r0.A03();
            this.A0B = null;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(12:29|(11:31|(2:33|(1:35)(1:40))(1:41)|(2:39|42)(1:43)|44|128|45|48|(2:50|(3:52|55|(12:57|(2:59|(1:61)(2:82|(2:84|85)))|62|(3:64|(4:67|(1:136)(3:130|69|(2:74|(3:132|76|(2:78|138)(1:134))(1:137))(2:133|73))|135|65)|131)(3:86|(4:89|(3:141|92|145)|142|87)|140)|93|(2:97|(3:99|(1:101)(1:115)|102))|103|(1:114)(3:107|(1:111)|112)|113|(1:117)|118|(1:120))(13:79|(2:81|85)|62|(0)(0)|93|(3:95|97|(0))|103|(1:105)|114|113|(0)|118|(0))))(1:53)|54|55|(0)(0))|37|(0)(0)|44|128|45|48|(0)(0)|54|55|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0045, code lost:
        if (r9 == 2) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0146, code lost:
        if (r9 != 3) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x01ab, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x01ac, code lost:
        com.whatsapp.util.Log.e("cameraview/start-preview/setdisplayorientation ", r1);
     */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x03db A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x03f0 A[Catch: all -> 0x03fd, TRY_LEAVE, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0163 A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01c2 A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01eb A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01f7 A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0278 A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02bf A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02ed A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0326 A[Catch: all -> 0x03fd, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x002e, B:7:0x0038, B:12:0x0048, B:14:0x005a, B:18:0x0066, B:20:0x0072, B:21:0x007a, B:23:0x009c, B:26:0x00c6, B:28:0x00f3, B:29:0x0137, B:42:0x0152, B:43:0x0163, B:44:0x016d, B:45:0x01a1, B:47:0x01ac, B:48:0x01b1, B:50:0x01c2, B:52:0x01e6, B:53:0x01eb, B:55:0x01f1, B:57:0x01f7, B:59:0x0222, B:61:0x0228, B:62:0x0247, B:64:0x0278, B:65:0x0289, B:67:0x028f, B:71:0x02a1, B:74:0x02a8, B:79:0x02bf, B:82:0x02d5, B:84:0x02e3, B:85:0x02e7, B:86:0x02ed, B:87:0x02f1, B:89:0x02f7, B:93:0x030c, B:95:0x0312, B:97:0x031c, B:99:0x0326, B:101:0x0339, B:102:0x033f, B:103:0x0342, B:105:0x036c, B:107:0x0372, B:109:0x0397, B:111:0x039a, B:112:0x03a0, B:113:0x03a7, B:114:0x03af, B:115:0x03bc, B:117:0x03db, B:118:0x03e7, B:120:0x03f0, B:123:0x03f5, B:124:0x03fc), top: B:127:0x0003, inners: #1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void A08() {
        /*
        // Method dump skipped, instructions count: 1024
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass27X.A08():void");
    }

    public final synchronized void A09() {
        Log.i("cameraview/stop-camera");
        Camera camera = this.A07;
        if (camera != null) {
            try {
                camera.stopPreview();
                this.A0M = false;
            } catch (Exception e) {
                Log.w("cameraview/stop-camera error stopping camera preview", e);
            }
            try {
                this.A07.release();
            } catch (Exception e2) {
                Log.w("cameraview/stop-camera error releasing camera", e2);
            }
            this.A07 = null;
        }
        Log.i("cameraview/stop-camera-end");
    }

    public void A0A(SurfaceHolder surfaceHolder) {
        this.A09.post(new RunnableBRunnable0Shape1S0200000_I0_1(this, 45, surfaceHolder));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(35:71|79|111|(1:113)(1:114)|115|(3:117|(1:119)|121)(1:123)|122|124|(1:126)|127|(1:129)|130|(1:136)|137|(4:139|218|140|143)|144|(15:146|(2:150|(2:152|(1:154)))|155|157|(5:176|(1:178)(1:184)|179|(1:181)(1:183)|182)(5:161|(1:163)(1:175)|164|(1:166)(1:174)|167)|168|(1:172)|173|214|185|(9:189|222|190|225|191|196|216|197|200)|220|201|(1:203)|208)|156|157|(1:159)|176|(0)(0)|179|(0)(0)|182|168|(2:170|172)|173|214|185|(10:187|189|222|190|225|191|196|216|197|200)|220|201|(0)|208) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:189|(2:222|190)|(2:225|191)|196|216|197|200) */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x031c, code lost:
        if (r2.contains(r1) != false) goto L_0x031e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x058f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0590, code lost:
        com.whatsapp.util.Log.e("cameraview/prepare-video error setting preview texture", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x05af, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x05b0, code lost:
        com.whatsapp.util.Log.e("cameraview/start-video-capture failed", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x05b7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x05b8, code lost:
        r0.getMessage();
        A07();
        A06();
     */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02af A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x02b4 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x02f5 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0344 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0354  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0359 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0379 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0392 A[Catch: all -> 0x05cd, TRY_LEAVE, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x03ba A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x03e8 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0420 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x046b A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x0481 A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x049c A[Catch: all -> 0x05cd, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x04c0 A[Catch: all -> 0x05cd, TRY_LEAVE, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x04cb A[Catch: all -> 0x05cd, TRY_ENTER, TryCatch #5 {, blocks: (B:4:0x0003, B:6:0x002a, B:7:0x002f, B:9:0x0037, B:10:0x003c, B:13:0x0045, B:15:0x00a1, B:16:0x00a5, B:19:0x00ad, B:22:0x00b5, B:23:0x00b8, B:24:0x00bd, B:25:0x00c5, B:27:0x00ce, B:29:0x00ef, B:31:0x00f5, B:32:0x00f8, B:34:0x012e, B:35:0x0143, B:37:0x0149, B:41:0x0159, B:46:0x0164, B:48:0x016d, B:50:0x017d, B:51:0x0182, B:52:0x0189, B:55:0x0191, B:56:0x0195, B:58:0x019b, B:60:0x01a9, B:62:0x01b1, B:67:0x01bc, B:69:0x01c6, B:71:0x01d0, B:72:0x01e7, B:74:0x01eb, B:76:0x01f5, B:78:0x01ff, B:82:0x0219, B:83:0x0226, B:85:0x022c, B:87:0x023a, B:89:0x0240, B:91:0x0244, B:93:0x0249, B:97:0x025f, B:98:0x0268, B:100:0x026e, B:102:0x027c, B:104:0x0281, B:108:0x0297, B:111:0x02a3, B:113:0x02af, B:114:0x02b4, B:115:0x02b8, B:117:0x02f5, B:119:0x0316, B:121:0x031e, B:122:0x0321, B:123:0x0344, B:127:0x0355, B:129:0x0359, B:130:0x036f, B:132:0x0379, B:134:0x037f, B:136:0x0388, B:137:0x038b, B:139:0x0392, B:140:0x0397, B:142:0x03a0, B:143:0x03a5, B:144:0x03b0, B:146:0x03ba, B:148:0x03c0, B:150:0x03ca, B:152:0x03d4, B:159:0x03e8, B:161:0x03ec, B:163:0x03f4, B:164:0x03fa, B:166:0x040a, B:167:0x040d, B:168:0x0416, B:170:0x0420, B:172:0x0424, B:173:0x0428, B:174:0x042f, B:175:0x0453, B:176:0x0457, B:178:0x046b, B:179:0x0471, B:181:0x0481, B:182:0x0484, B:183:0x049c, B:184:0x04c0, B:185:0x04c4, B:187:0x04cb, B:189:0x04cf, B:190:0x04d4, B:191:0x04d7, B:195:0x04df, B:196:0x04e4, B:197:0x0587, B:199:0x0590, B:200:0x0595, B:201:0x059a, B:203:0x05ab, B:205:0x05b0, B:207:0x05b8, B:210:0x05c5, B:211:0x05cc), top: B:224:0x0003, inners: #0, #1, #2, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x05ab A[Catch: RuntimeException -> 0x05af, all -> 0x05cd, TRY_LEAVE, TryCatch #3 {RuntimeException -> 0x05af, blocks: (B:201:0x059a, B:203:0x05ab), top: B:220:0x059a, outer: #5 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A0B(java.io.File r25, java.lang.Integer r26, int r27) {
        /*
        // Method dump skipped, instructions count: 1488
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass27X.A0B(java.io.File, java.lang.Integer, int):void");
    }

    @Override // X.AnonymousClass1s9
    public void A7E() {
        C64113Eh r1 = this.A0Y;
        synchronized (r1) {
            r1.A00 = null;
        }
    }

    @Override // X.AnonymousClass1s9
    public void AA4(float f, float f2) {
        this.A09.post(new RunnableBRunnable0Shape0S0100002_I0(this, f, f2, 1));
    }

    @Override // X.AnonymousClass1s9
    public boolean AJS() {
        return this.A0N;
    }

    @Override // X.AnonymousClass1s9
    public boolean AJV() {
        return this.A0M;
    }

    @Override // X.AnonymousClass1s9
    public boolean AJy() {
        return this.A0P;
    }

    @Override // X.AnonymousClass1s9
    public boolean AK9() {
        Camera camera = this.A07;
        if (camera == null || !this.A0Q) {
            return false;
        }
        return "torch".equals(camera.getParameters().getFlashMode());
    }

    @Override // X.AnonymousClass1s9
    public boolean ALa() {
        Camera camera;
        if (!this.A0N || !"on".equals(this.A0K) || (camera = this.A07) == null) {
            return false;
        }
        List<String> supportedFlashModes = camera.getParameters().getSupportedFlashModes();
        if (supportedFlashModes == null || !supportedFlashModes.contains(this.A0K)) {
            return true;
        }
        return false;
    }

    @Override // X.AnonymousClass1s9
    public synchronized void ALf() {
        Log.i("cameraview/next-camera");
        if (this.A07 != null) {
            boolean z = true;
            if (Camera.getNumberOfCameras() > 1) {
                this.A00 = (this.A00 + 1) % Camera.getNumberOfCameras();
                if (getCameraInfo().facing != 1) {
                    z = false;
                }
                this.A0N = z;
                A09();
                A05();
                getSharedPreferences().edit().putInt("camera_index", this.A00).apply();
            }
        }
    }

    @Override // X.AnonymousClass1s9
    public synchronized String ALg() {
        String str;
        if (this.A07 == null) {
            str = "off";
        } else {
            List flashModes = getFlashModes();
            if (flashModes.isEmpty()) {
                str = "off";
            } else {
                Camera.Parameters parameters = this.A07.getParameters();
                int indexOf = flashModes.indexOf(this.A0K);
                if (indexOf >= 0 || (indexOf = flashModes.indexOf((str = "off"))) >= 0) {
                    String str2 = (String) flashModes.get((indexOf + 1) % flashModes.size());
                    this.A0K = str2;
                    StringBuilder sb = new StringBuilder();
                    sb.append("cameraview/next flash mode:");
                    sb.append(str2);
                    Log.i(sb.toString());
                    List<String> supportedFlashModes = parameters.getSupportedFlashModes();
                    if (supportedFlashModes != null && supportedFlashModes.contains(this.A0K)) {
                        parameters.setFlashMode(this.A0K);
                        this.A07.setParameters(parameters);
                    }
                    getSharedPreferences().edit().putString("flash_mode", this.A0K).apply();
                    str = this.A0K;
                }
            }
        }
        return str;
    }

    @Override // X.AnonymousClass1s9
    public void Aap() {
        A0A(this.A0V);
    }

    @Override // X.AnonymousClass1s9
    public synchronized int AdD(int i) {
        Camera camera = this.A07;
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.isZoomSupported() && i <= parameters.getMaxZoom()) {
                if (parameters.getZoom() != i) {
                    parameters.setZoom(i);
                    this.A07.setParameters(parameters);
                }
                List<Integer> zoomRatios = parameters.getZoomRatios();
                if (zoomRatios != null && zoomRatios.size() >= i) {
                    return zoomRatios.get(i).intValue();
                }
            }
        }
        return 0;
    }

    @Override // X.AnonymousClass1s9
    public synchronized void AeK(File file, int i) {
        A0B(file, null, i);
    }

    @Override // X.AnonymousClass1s9
    public synchronized void AeT() {
        try {
            this.A08.stop();
        } catch (RuntimeException e) {
            Log.w("cameraview/stop-video-capture ", e);
        }
        A07();
        Camera camera = this.A07;
        if (camera != null) {
            camera.lock();
        }
        this.A0P = false;
        this.A05 = null;
    }

    @Override // X.AnonymousClass1s9
    public boolean Aee() {
        return this.A0Q;
    }

    @Override // X.AnonymousClass1s9
    public synchronized void Aei(AnonymousClass2AQ r6, boolean z) {
        if (this.A07 == null) {
            Log.e("cameraview/take-picture camera is null");
        } else if (this.A0R) {
            Log.e("cameraview/take-picture already taking a picture");
        } else {
            this.A0M = false;
            this.A0R = true;
            Log.i("cameraview/take-picture/start");
            Camera.Parameters parameters = this.A07.getParameters();
            parameters.setRotation(getRequiredCameraRotation());
            parameters.setJpegQuality(80);
            this.A07.setParameters(parameters);
            try {
                AnonymousClass3LN r3 = new Camera.PictureCallback(r6, this) { // from class: X.3LN
                    public final /* synthetic */ AnonymousClass2AQ A00;
                    public final /* synthetic */ AnonymousClass27X A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // android.hardware.Camera.PictureCallback
                    public final void onPictureTaken(byte[] bArr, Camera camera) {
                        AnonymousClass27X r4 = this.A01;
                        AnonymousClass2AQ r32 = this.A00;
                        StringBuilder A0k = C12960it.A0k("cameraview/take-picture taken ");
                        A0k.append(r4.A0N);
                        C12960it.A1F(A0k);
                        try {
                            r4.A07.stopPreview();
                            r4.A0M = false;
                        } catch (Exception e) {
                            Log.w("cameraview/take-picture error stopping camera preview", e);
                        }
                        r4.A0R = false;
                        r4.A0T.post(new RunnableBRunnable0Shape0S0300000_I0(r4, r32, bArr, 20));
                    }
                };
                this.A07.takePicture(new Camera.ShutterCallback() { // from class: X.4hq
                    @Override // android.hardware.Camera.ShutterCallback
                    public final void onShutter() {
                        AnonymousClass2AQ.this.onShutter();
                    }
                }, null, r3);
            } catch (Exception e) {
                this.A0R = false;
                Log.e("cameraview/take-picture failed", e);
            }
        }
        A06();
    }

    @Override // X.AnonymousClass1s9
    public void Af2() {
        String str;
        Camera camera = this.A07;
        if (camera != null && this.A0Q) {
            Camera.Parameters parameters = camera.getParameters();
            if (AK9()) {
                str = "off";
            } else {
                str = "torch";
            }
            parameters.setFlashMode(str);
            camera.setParameters(parameters);
        }
    }

    private Camera.CameraInfo getCameraInfo() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(this.A00, cameraInfo);
        return cameraInfo;
    }

    private List getFallbackSupportedPreviewSizes() {
        Log.i("cameraview/fallback-supported-preview-sizes");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Camera.Size(this.A07, 640, 480));
        return arrayList;
    }

    @Override // X.AnonymousClass1s9
    public String getFlashMode() {
        return this.A0K;
    }

    @Override // X.AnonymousClass1s9
    public synchronized List getFlashModes() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        Camera camera = this.A07;
        if (camera != null) {
            try {
                List<String> supportedFlashModes = camera.getParameters().getSupportedFlashModes();
                if (supportedFlashModes != null) {
                    if (supportedFlashModes.contains("off")) {
                        arrayList.add("off");
                    }
                    if (supportedFlashModes.contains("on")) {
                        arrayList.add("on");
                    }
                    if (supportedFlashModes.contains("auto")) {
                        arrayList.add("auto");
                    }
                }
                if (this.A0N) {
                    if (!arrayList.contains("off")) {
                        arrayList.add("off");
                    }
                    if (!arrayList.contains("on")) {
                        arrayList.add("on");
                    }
                }
                if (getStoredFlashModeCount() != arrayList.size()) {
                    SharedPreferences.Editor edit = getSharedPreferences().edit();
                    StringBuilder sb = new StringBuilder();
                    sb.append("flash_mode_count");
                    sb.append(this.A00);
                    edit.putInt(sb.toString(), arrayList.size()).apply();
                }
            } catch (RuntimeException e) {
                Log.e("cameraview/getFlashModes ", e);
            }
        }
        return arrayList;
    }

    @Override // X.AnonymousClass1s9
    public synchronized int getMaxZoom() {
        int i;
        i = 0;
        Camera camera = this.A07;
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters.isZoomSupported()) {
                i = parameters.getMaxZoom();
            }
        }
        return i;
    }

    @Override // X.AnonymousClass1s9
    public int getNumberOfCameras() {
        return Camera.getNumberOfCameras();
    }

    @Override // X.AnonymousClass1s9
    public synchronized long getPictureResolution() {
        long j;
        Camera.Size pictureSize;
        Camera camera = this.A07;
        j = 0;
        if (!(camera == null || (pictureSize = camera.getParameters().getPictureSize()) == null)) {
            j = (long) (pictureSize.width * pictureSize.height);
        }
        return j;
    }

    private int getRequiredCameraRotation() {
        int rotation = this.A0U.getRotation();
        Camera.CameraInfo cameraInfo = getCameraInfo();
        int i = 0;
        boolean z = false;
        if (cameraInfo.facing == 1) {
            z = true;
        }
        this.A0N = z;
        int i2 = cameraInfo.orientation;
        if (rotation != 0) {
            if (rotation == 1) {
                i = 90;
            } else if (rotation == 2) {
                i = 180;
            } else if (rotation == 3) {
                i = 270;
            }
        }
        int i3 = i2 - i;
        if (z) {
            i3 = i2 + i;
        }
        int i4 = (i3 + 360) % 360;
        StringBuilder sb = new StringBuilder("cameraview/orientation display:");
        sb.append(i);
        sb.append(" camera:");
        sb.append(i2);
        sb.append(" rotate:");
        sb.append(i4);
        Log.i(sb.toString());
        return i4;
    }

    private SharedPreferences getSharedPreferences() {
        return this.A0J.A01(AnonymousClass01V.A07);
    }

    @Override // X.AnonymousClass1s9
    public int getStoredFlashModeCount() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        StringBuilder sb = new StringBuilder("flash_mode_count");
        sb.append(this.A00);
        return sharedPreferences.getInt(sb.toString(), 0);
    }

    @Override // X.AnonymousClass1s9
    public synchronized long getVideoResolution() {
        Camera.Size size;
        size = this.A06;
        return size != null ? (long) (size.width * size.height) : 0;
    }

    @Override // android.view.SurfaceView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A0X.enable();
        HandlerThread handlerThread = new HandlerThread("Camera");
        this.A0A = handlerThread;
        handlerThread.start();
        this.A09 = new Handler(this.A0A.getLooper());
        if (this.A0O) {
            this.A0Y.A01();
        }
    }

    @Override // android.view.SurfaceView, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A0X.disable();
        HandlerThread handlerThread = this.A0A;
        if (handlerThread != null) {
            handlerThread.quit();
            this.A0A = null;
        }
        this.A0Y.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r1 == 2) goto L_0x0034;
     */
    @Override // android.view.SurfaceView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r18, int r19) {
        /*
        // Method dump skipped, instructions count: 309
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass27X.onMeasure(int, int):void");
    }

    @Override // X.AnonymousClass1s9
    public void setCameraCallback(AbstractC467527b r1) {
        this.A0F = r1;
    }

    @Override // X.AnonymousClass1s9
    public void setQrDecodeHints(Map map) {
        this.A0Y.A02 = map;
    }

    @Override // X.AnonymousClass1s9
    public void setQrScanningEnabled(boolean z) {
        this.A0O = z;
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (this.A07 == null) {
            return;
        }
        if (this.A0V.getSurface() == null) {
            Log.e("cameraview/surface-changed: no surface");
            A06();
        } else if (!this.A0P) {
            A0A(surfaceHolder);
        }
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        A05();
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.A09.post(new RunnableBRunnable0Shape3S0100000_I0_3(this, 32));
        A07();
    }
}
