package X;

/* renamed from: X.3ET  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ET {
    public final int A00;
    public final C30031Vu A01;
    public final AbstractC15340mz A02;
    public final boolean A03;

    public AnonymousClass3ET(C30031Vu r1, AbstractC15340mz r2, int i, boolean z) {
        this.A01 = r1;
        this.A03 = z;
        this.A02 = r2;
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3ET r5 = (AnonymousClass3ET) obj;
            if (!C29941Vi.A00(this.A01, r5.A01) || !C29941Vi.A00(Boolean.valueOf(this.A03), Boolean.valueOf(r5.A03)) || !C29941Vi.A00(Integer.valueOf(this.A00), Integer.valueOf(r5.A00)) || !C29941Vi.A00(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[4];
        objArr[0] = this.A01;
        objArr[1] = Boolean.valueOf(this.A03);
        C12990iw.A1V(objArr, this.A00);
        return C12980iv.A0B(this.A02, objArr, 3);
    }
}
