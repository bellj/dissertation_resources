package X;

import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import org.json.JSONException;

/* renamed from: X.3HM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HM {
    public static final Pair A0F = Pair.create(null, null);
    public static final Pattern A0G = Pattern.compile("bytes=0-(\\d*)");
    public static final boolean A0H;
    public final int A00;
    public final AbstractC15710nm A01;
    public final C15820nx A02;
    public final AnonymousClass5TJ A03;
    public final C27051Fv A04;
    public final AbstractC44761zV A05;
    public final C65113Ie A06;
    public final C44791zY A07;
    public final C64413Fl A08;
    public final C18640sm A09;
    public final C15810nw A0A;
    public final C15890o4 A0B;
    public final C14850m9 A0C;
    public final AbstractC14440lR A0D;
    public final String A0E;

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT <= 20) {
            z = true;
        }
        A0H = z;
    }

    public AnonymousClass3HM(AbstractC15710nm r2, C15820nx r3, AnonymousClass5TJ r4, C27051Fv r5, AbstractC44761zV r6, C65113Ie r7, C44791zY r8, C64413Fl r9, C18640sm r10, C15810nw r11, C15890o4 r12, C14850m9 r13, AbstractC14440lR r14, int i) {
        this.A0C = r13;
        this.A01 = r2;
        this.A0A = r11;
        this.A02 = r3;
        this.A04 = r5;
        this.A0B = r12;
        this.A06 = r7;
        this.A08 = r9;
        this.A00 = i;
        this.A03 = r4;
        this.A05 = r6;
        this.A07 = r8;
        this.A0E = r8.A0E;
        this.A09 = r10;
        this.A0D = r14;
    }

    public static void A00(AnonymousClass3HM r4, AtomicLong atomicLong, long j) {
        r4.A03.AOt((atomicLong.get() + j) * -1);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:134:0x0592 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:138:0x05a8 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:153:0x05d2 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r18v4 */
    /* JADX WARN: Type inference failed for: r8v2, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r5v20, types: [X.1zY] */
    /* JADX WARN: Type inference failed for: r7v12 */
    /* JADX WARN: Type inference failed for: r8v4, types: [android.os.Handler] */
    /* JADX WARN: Type inference failed for: r7v13, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r13v6, types: [X.1zY] */
    /* JADX WARN: Type inference failed for: r8v5, types: [android.os.Handler] */
    /* JADX WARN: Type inference failed for: r7v17, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r7v18 */
    /* JADX WARN: Type inference failed for: r7v19, types: [java.lang.Object, X.55l] */
    /* JADX WARN: Type inference failed for: r18v6 */
    /* JADX WARN: Type inference failed for: r18v7 */
    /* JADX WARN: Type inference failed for: r7v24 */
    /* JADX WARN: Type inference failed for: r7v25 */
    /* JADX WARN: Type inference failed for: r8v11 */
    /* JADX WARN: Type inference failed for: r8v12 */
    /* JADX WARN: Type inference failed for: r18v14 */
    /* JADX WARN: Type inference failed for: r18v15 */
    /* JADX WARNING: Unknown variable types count: 5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass3HV A01() {
        /*
        // Method dump skipped, instructions count: 2321
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HM.A01():X.3HV");
    }

    public final AnonymousClass3HV A02(String str, String str2, long j) {
        if (TextUtils.isEmpty(str)) {
            Log.e("gdrive-api-v2/upload-file/unexpected-response/file-uploaded-but-no-entity-in-response");
            return null;
        }
        try {
            AnonymousClass3HV A00 = AnonymousClass3HV.A00(this.A02, null, str2, C13000ix.A05(str), j);
            if (A00 == null) {
                Log.e("gdrive-api-v2/upload-file/some attributes are missing");
            }
            return A00;
        } catch (JSONException e) {
            Log.e(C12960it.A0d(str, C12960it.A0k("gdrive-api-v2/upload-file/malformed-json-response/")), e);
            return null;
        }
    }
}
