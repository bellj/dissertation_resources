package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3om  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78353om extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99504kP();
    public final int A00;
    public final int A01;
    public final byte[] A02;

    public C78353om(byte[] bArr, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = bArr;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A07(parcel, 2, this.A01);
        C95654e8.A0G(parcel, this.A02, 3, false);
        C95654e8.A06(parcel, A00);
    }
}
