package X;

import android.content.Context;
import android.os.Looper;
import android.util.SparseArray;
import com.facebook.rendercore.RootHostView;
import com.whatsapp.R;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0bp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08770bp implements AnonymousClass5T4 {
    public RootHostView A00 = null;
    public C64893Hi A01 = null;
    public final AnonymousClass0AT A02 = new AnonymousClass0AT(Looper.getMainLooper(), new AnonymousClass0VG(this));
    public final C06020Rw A03;
    public final AbstractC92144Us A04;
    public final Set A05 = new HashSet();
    public final AtomicBoolean A06 = new AtomicBoolean(false);
    public final AtomicReference A07 = new AtomicReference();
    public volatile boolean A08 = false;

    public C08770bp(C06020Rw r5, AnonymousClass3JI r6) {
        C03580Ii r2 = null;
        this.A03 = r5;
        this.A04 = r6 != null ? new C03580Ii(r6, C94004b6.A02) : r2;
    }

    public static void A03(C28671On r2, C14260l7 r3, C14220l3 r4, Map map) {
        C14250l6.A00(C28701Oq.A00(r2, r3, null, map), r4, r2.A01);
    }

    public AnonymousClass01T A04(Context context) {
        boolean z;
        RootHostView rootHostView = new RootHostView(context);
        this.A00 = rootHostView;
        C64893Hi r1 = this.A01;
        if (r1 != null) {
            z = true;
            r1.A05(rootHostView);
        } else {
            z = false;
            A07();
        }
        return new AnonymousClass01T(this.A00, Boolean.valueOf(z));
    }

    public final C64893Hi A05(C03580Ii r8, Runnable runnable) {
        C1093551j r5 = C1093551j.A00;
        r5.A6N("BloksSurface_create_bloks_hosting_component");
        try {
            C06020Rw r6 = this.A03;
            SparseArray clone = r6.A01.clone();
            clone.put(R.id.bloks_surface_on_data_rendered_runnable, runnable);
            AnonymousClass4Y0 A00 = C64893Hi.A00(r6.A00, r8.A01(), r6.A02);
            Map A03 = r8.A03();
            HashMap hashMap = new HashMap(r6.A03);
            hashMap.putAll(A03);
            A00.A02(hashMap);
            A00.A01(clone);
            A00.A03(Collections.emptyMap());
            return A00.A00();
        } finally {
            r5.A9T();
        }
    }

    public void A06() {
        C64893Hi r0 = this.A01;
        if (r0 != null) {
            r0.A03();
        }
        this.A01 = null;
        this.A07.set(null);
    }

    public void A07() {
        if (this.A06.compareAndSet(false, true)) {
            AbstractC92144Us r2 = this.A04;
            if (r2 == null) {
                A08(0);
                return;
            }
            A08(1);
            A0C((C03580Ii) r2, 2, 3);
        }
    }

    public final void A08(int i) {
        AbstractC12090hM r3 = (AbstractC12090hM) this.A07.get();
        if (r3 != null && i != -1) {
            C1093551j r2 = C1093551j.A00;
            StringBuilder sb = new StringBuilder("BloksSurface_notify_on_render_surface_");
            sb.append(i);
            r2.A6N(sb.toString());
            try {
                r3.AUv(i);
            } finally {
                r2.A9T();
            }
        }
    }

    public final void A09(AnonymousClass0HL r11) {
        C64893Hi r2 = this.A01;
        if (r2 != null) {
            C06020Rw r0 = this.A03;
            Map map = r11.A02;
            HashMap hashMap = new HashMap(r0.A03);
            hashMap.putAll(map);
            Set set = this.A05;
            HashSet hashSet = new HashSet(set);
            try {
                C14260l7 A02 = r2.A02();
                List<C28671On> list = r11.A01;
                for (C28671On r3 : list) {
                    if (!hashSet.contains(r3)) {
                        Map A04 = AnonymousClass3JI.A04(r3.A00(), hashMap);
                        C14210l2 r1 = new C14210l2();
                        r1.A05(A02, 0);
                        A03(r3, A02, r1.A03(), A04);
                        hashSet.add(r3);
                    }
                }
                set.addAll(list);
                A08(r11.A00);
            } catch (Throwable th) {
                set.addAll(r11.A01);
                A08(r11.A00);
                throw th;
            }
        }
    }

    public final void A0A(AnonymousClass0HM r3) {
        C64893Hi r0;
        try {
            C64893Hi r02 = this.A01;
            if (r02 != null) {
                r02.A04();
            }
            RootHostView rootHostView = this.A00;
            if (rootHostView == null) {
                A08(8);
                r0 = r3.A00();
            } else {
                r0 = r3.A00();
                r0.A05(rootHostView);
            }
            this.A01 = r0;
            A08(r3.A00);
        } catch (Throwable th) {
            this.A01 = r3.A00();
            A08(r3.A00);
            throw th;
        }
    }

    public void A0B(AbstractC12090hM r2) {
        this.A07.set(r2);
    }

    public final void A0C(C03580Ii r4, int i, int i2) {
        A08(10);
        AnonymousClass0HM r2 = new AnonymousClass0HM(A05(r4, new RunnableC09740dR(this, i2)), i);
        AnonymousClass0AT r1 = this.A02;
        r1.A00(r1.obtainMessage(1, r2));
    }

    @Override // X.AnonymousClass5T4
    public void AV7(AbstractC92144Us r7) {
        C94004b6 A00 = r7.A00();
        int i = A00.A00;
        int i2 = 4;
        if (i != 4 && i != 5) {
            boolean z = false;
            if (i == 3) {
                z = true;
                i2 = 5;
            }
            A08(i2);
            if (!z) {
                A08(6);
                int i3 = -1;
                if (A00.A00()) {
                    i3 = 9;
                }
                if (r7 instanceof C03580Ii) {
                    C03580Ii r72 = (C03580Ii) r7;
                    if (this.A08) {
                        List A02 = r72.A02();
                        Map A03 = r72.A03();
                        if (!A02.isEmpty()) {
                            A08(12);
                            AnonymousClass0AT r2 = this.A02;
                            r2.A00(r2.obtainMessage(2, new AnonymousClass0HL(A02, A03)));
                            return;
                        }
                        return;
                    }
                    A0C(r72, 7, i3);
                    List A022 = r72.A02();
                    Map A032 = r72.A03();
                    if (!A022.isEmpty()) {
                        A08(12);
                        AnonymousClass0AT r22 = this.A02;
                        r22.A00(r22.obtainMessage(2, new AnonymousClass0HL(A022, A032)));
                    }
                    this.A08 = true;
                }
            }
        }
    }
}
