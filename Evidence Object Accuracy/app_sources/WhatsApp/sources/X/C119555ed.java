package X;

import android.view.View;

/* renamed from: X.5ed  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119555ed extends AbstractC17880rY {
    public C119555ed() {
        super(13538);
    }

    @Override // X.AbstractC17880rY
    public Object A00(C14260l7 r2, AnonymousClass28D r3) {
        return new C127275uE();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005d, code lost:
        if (r2 == 8) goto L_0x005f;
     */
    @Override // X.AbstractC17880rY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.view.View r7, X.C14260l7 r8, X.AnonymousClass28D r9, X.AnonymousClass28D r10) {
        /*
            r6 = this;
            r0 = 35
            X.0l1 r5 = r9.A0G(r0)
            r0 = 36
            X.0l1 r4 = r9.A0G(r0)
            if (r5 != 0) goto L_0x0011
            if (r4 != 0) goto L_0x0011
            return
        L_0x0011:
            java.lang.Object r2 = X.AnonymousClass3JV.A04(r8, r9)
            X.5uE r2 = (X.C127275uE) r2
            r0 = 38
            java.lang.String r1 = r9.A0I(r0)
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.A02
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x0038
            X.5ui r1 = new X.5ui
            r1.<init>(r8, r10, r5, r4)
            java.util.ArrayList r0 = X.C12960it.A0l()
            r0.add(r1)
            java.lang.String r0 = "register"
            java.lang.NullPointerException r0 = X.C12980iv.A0n(r0)
            throw r0
        L_0x0038:
            X.653 r3 = r2.A01
            r3.A02 = r10
            r3.A00 = r7
            r3.A01 = r8
            r3.A05 = r1
            r3.A03 = r5
            r3.A04 = r4
            android.view.ViewTreeObserver r0 = r7.getViewTreeObserver()
            r0.addOnGlobalLayoutListener(r3)
            android.view.ViewParent r0 = r7.getParent()
            if (r0 == 0) goto L_0x005f
            int r2 = r7.getVisibility()
            r0 = 4
            if (r2 == r0) goto L_0x005f
            r1 = 8
            r0 = 1
            if (r2 != r1) goto L_0x0060
        L_0x005f:
            r0 = 0
        L_0x0060:
            r3.A06 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119555ed.A01(android.view.View, X.0l7, X.28D, X.28D):void");
    }

    @Override // X.AbstractC17880rY
    public void A02(View view, C14260l7 r5, AnonymousClass28D r6, AnonymousClass28D r7) {
        int visibility;
        AbstractC14200l1 A0G = r6.A0G(35);
        AbstractC14200l1 A0G2 = r6.A0G(36);
        if (A0G != null || A0G2 != null) {
            C127275uE r1 = (C127275uE) AnonymousClass3JV.A04(r5, r6);
            r6.A0I(38);
            if (r1.A02.get()) {
                throw C12980iv.A0n("register");
            }
            AnonymousClass653 r2 = r1.A01;
            View view2 = r2.A00;
            if (view2 != null) {
                if (r2.A06 && (view2.getParent() == null || (visibility = view2.getVisibility()) == 4 || visibility == 8)) {
                    r2.A00();
                }
                r2.A00.getViewTreeObserver().removeOnGlobalLayoutListener(r2);
                r2.A00 = null;
                r2.A01 = null;
                r2.A03 = null;
                r2.A04 = null;
                r2.A06 = false;
            }
        }
    }
}
