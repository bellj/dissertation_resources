package X;

import android.content.Context;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import java.util.List;

/* renamed from: X.2xx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60492xx extends AnonymousClass1OY {
    public AnonymousClass130 A00;
    public C39271pZ A01;
    public boolean A02;
    public final LinearLayout A03;
    public final TextView A04 = C12960it.A0J(this, R.id.vcard_text);
    public final TextView A05;
    public final AnonymousClass1J1 A06;
    public final C39091pH A07;
    public final ImageView[] A08;

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: android.widget.ImageView[] */
    /* JADX WARN: Multi-variable type inference failed */
    public C60492xx(Context context, AnonymousClass1J1 r5, AbstractC13890kV r6, AbstractC15340mz r7, C39091pH r8) {
        super(context, r6, r7);
        A0Z();
        this.A08 = r2;
        this.A06 = r5;
        this.A07 = r8;
        ImageView[] imageViewArr = {findViewById(R.id.picture), findViewById(R.id.picture2), findViewById(R.id.picture3)};
        this.A05 = C12960it.A0J(this, R.id.view_contacts_btn);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.contact_card);
        this.A03 = linearLayout;
        linearLayout.setOnClickListener(new ViewOnClickCListenerShape18S0100000_I1_1(this));
        linearLayout.setOnLongClickListener(this.A1a);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A00 = C12990iw.A0Y(A08);
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, getFMessage());
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    public final void A1M() {
        Runnable A01;
        boolean z;
        C15370n3 A00;
        boolean z2;
        AbstractC15340mz fMessage = getFMessage();
        int A002 = C47962Dl.A00(fMessage);
        TextView textView = this.A04;
        AnonymousClass1IS r5 = fMessage.A0z;
        textView.setTag(r5);
        C39271pZ r1 = this.A01;
        if (r1 != null) {
            this.A07.A03(r1);
        }
        C39091pH r12 = this.A07;
        synchronized (r12) {
            A01 = r12.A01(fMessage, null);
        }
        C39271pZ r2 = (C39271pZ) A01;
        this.A01 = r2;
        r2.A01(new AbstractC14590lg() { // from class: X.3bL
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                String A08;
                C60492xx r3 = C60492xx.this;
                AnonymousClass4S8 r52 = (AnonymousClass4S8) obj;
                if (r52.A01.A0z.equals(r3.A04.getTag())) {
                    int i = r52.A00;
                    C30721Yo r0 = r52.A03;
                    if (r0 == null) {
                        A08 = null;
                    } else {
                        A08 = r0.A08();
                    }
                    r3.A1N(r52.A02, i, A08);
                }
            }
        }, ((AnonymousClass1OY) this).A0J.A06);
        A1N(null, A002, null);
        ImageView imageView = this.A08[2];
        if (A002 == 2) {
            imageView.setVisibility(4);
        } else {
            imageView.setVisibility(0);
        }
        if (!r5.A02) {
            AbstractC14640lm r3 = r5.A00;
            boolean A0J = C15380n4.A0J(r3);
            boolean z3 = true;
            C15550nR r13 = ((AnonymousClass1OY) this).A0X;
            if (A0J) {
                A00 = C15550nR.A00(r13, fMessage.A0B());
                z2 = (!this.A11.A0X.A0F((C15580nU) r3)) & this.A0v.A04(r3) & true;
            } else {
                A00 = C15550nR.A00(r13, r3);
                z2 = true;
            }
            if (A00.A0C != null) {
                z3 = false;
            }
            z = z2 & z3 & this.A0v.A04(C15370n3.A01(A00));
        } else {
            z = false;
        }
        View findViewById = findViewById(R.id.button_div);
        TextView textView2 = this.A05;
        if (!z) {
            textView2.setVisibility(0);
            textView2.setOnClickListener(new ViewOnClickCListenerShape18S0100000_I1_1(this));
            return;
        }
        textView2.setVisibility(8);
        findViewById.setVisibility(8);
    }

    public final void A1N(List list, int i, String str) {
        CharSequence A02;
        int i2 = 0;
        do {
            if (list == null || i2 >= list.size()) {
                this.A00.A05(this.A08[i2], R.drawable.avatar_contact);
            } else {
                this.A06.A08(this.A08[i2], (C30721Yo) list.get(i2));
            }
            i2++;
        } while (i2 < 3);
        if (!TextUtils.isEmpty(str)) {
            int i3 = i - 1;
            String A04 = AnonymousClass1US.A04(50, str);
            Object[] A1a = C12980iv.A1a();
            A1a[0] = A04;
            C12960it.A1P(A1a, i3, 1);
            String A0I = ((AbstractC28551Oa) this).A0K.A0I(A1a, R.plurals.contacts_array_title, (long) i3);
            Context context = getContext();
            TextView textView = this.A04;
            TextPaint paint = textView.getPaint();
            AnonymousClass19M r1 = this.A10;
            if (A0I == null) {
                A02 = null;
            } else {
                A02 = AbstractC36671kL.A02(context, paint, r1, new AnonymousClass56Y(), A0I);
            }
            textView.setText(A0q(A02));
            return;
        }
        TextView textView2 = this.A04;
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, i, 0);
        textView2.setText(((AbstractC28551Oa) this).A0K.A0I(objArr, R.plurals.n_contacts_message_title, (long) i));
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_contacts_array_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_contacts_array_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_contacts_array_right;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (X.C30041Vv.A0r(r3) != false) goto L_0x000b;
     */
    @Override // X.AbstractC28551Oa
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setFMessage(X.AbstractC15340mz r3) {
        /*
            r2 = this;
            boolean r0 = r3 instanceof X.C30351Xb
            if (r0 != 0) goto L_0x000b
            boolean r1 = X.C30041Vv.A0r(r3)
            r0 = 0
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            r0 = 1
        L_0x000c:
            X.AnonymousClass009.A0F(r0)
            r2.A0O = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60492xx.setFMessage(X.0mz):void");
    }
}
