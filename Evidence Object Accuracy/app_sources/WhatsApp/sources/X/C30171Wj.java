package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Wj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30171Wj implements Parcelable {
    public static final C30171Wj A04 = new C30171Wj(C30161Wi.A04, null, null);
    public static final Parcelable.Creator CREATOR = new C99724kl();
    public final C30161Wi A00;
    public final String A01;
    public final String A02;
    public final String A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C30171Wj(C30161Wi r2, String str, String str2) {
        this.A01 = "";
        this.A03 = str == null ? "" : str;
        this.A02 = str2 == null ? "" : str2;
        this.A00 = r2;
    }

    public C30171Wj(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A01 = readString;
        String readString2 = parcel.readString();
        AnonymousClass009.A05(readString2);
        this.A03 = readString2;
        String readString3 = parcel.readString();
        AnonymousClass009.A05(readString3);
        this.A02 = readString3;
        Parcelable readParcelable = parcel.readParcelable(C30161Wi.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        this.A00 = (C30161Wi) readParcelable;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C30171Wj r4 = (C30171Wj) obj;
            if (this.A01.equals(r4.A01) && this.A03.equals(r4.A03) && this.A02.equals(r4.A02)) {
                return this.A00.equals(r4.A00);
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((((this.A01.hashCode() * 31) + this.A03.hashCode()) * 31) + this.A02.hashCode()) * 31) + this.A00.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A03);
        sb.append(" ");
        sb.append(this.A02);
        sb.append(" ");
        sb.append(this.A00);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A03);
        parcel.writeString(this.A02);
        parcel.writeParcelable(this.A00, i);
    }
}
