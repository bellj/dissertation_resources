package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.4G1  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4G1 {
    public static final Map A00;

    static {
        HashMap A11 = C12970iu.A11();
        A11.put("avg", C83063wd.class);
        A11.put("stddev", C83073we.class);
        A11.put("sum", C83053wc.class);
        A11.put("min", C83043wb.class);
        A11.put("max", C83033wa.class);
        A11.put("concat", AnonymousClass52B.class);
        A11.put("length", AnonymousClass52C.class);
        A11.put("size", AnonymousClass52C.class);
        A11.put("append", AnonymousClass529.class);
        A11.put("keys", AnonymousClass52A.class);
        A00 = Collections.unmodifiableMap(A11);
    }
}
