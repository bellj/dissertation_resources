package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Process;
import android.util.Pair;

/* renamed from: X.0nM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15500nM {
    public final C16590pI A00;
    public final C248717f A01;

    public C15500nM(C16590pI r1, C248717f r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public C15430nF A00() {
        int length;
        int callingUid = Binder.getCallingUid();
        if (callingUid != Process.myUid()) {
            Context context = this.A00.A00;
            String[] packagesForUid = context.getPackageManager().getPackagesForUid(callingUid);
            if (packagesForUid == null || (length = packagesForUid.length) == 0) {
                StringBuilder sb = new StringBuilder("No packages associated with uid: ");
                sb.append(callingUid);
                throw new SecurityException(sb.toString());
            } else if (length == 1) {
                String str = packagesForUid[0];
                String A01 = C45051zz.A01(C45051zz.A00(context.getPackageManager(), str));
                boolean z = false;
                if (this.A01.A01.contains(Pair.create(str, A01))) {
                    z = true;
                }
                return new C15430nF(str, A01, callingUid, z);
            } else {
                StringBuilder sb2 = new StringBuilder("Multiple packages per uid are not supported, uid: ");
                sb2.append(callingUid);
                throw new SecurityException(sb2.toString());
            }
        } else {
            throw new IllegalStateException("This method should be called on behalf of an IPC transaction from binder thread");
        }
    }

    public C15430nF A01(String str) {
        Context context = this.A00.A00;
        int i = context.getPackageManager().getPackageInfo(str, 0).applicationInfo.uid;
        String A01 = C45051zz.A01(C45051zz.A00(context.getPackageManager(), str));
        boolean z = false;
        if (this.A01.A01.contains(Pair.create(str, A01))) {
            z = true;
        }
        return new C15430nF(str, A01, i, z);
    }

    public C15430nF A02(String str) {
        try {
            C15430nF A01 = A01(str);
            A01.A00();
            return A01;
        } catch (PackageManager.NameNotFoundException e) {
            StringBuilder sb = new StringBuilder("Package not found: ");
            sb.append(str);
            throw new SecurityException(sb.toString(), e);
        }
    }
}
