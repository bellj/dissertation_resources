package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1ws  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43351ws extends BroadcastReceiver {
    public final /* synthetic */ C19890uq A00;

    public C43351ws(C19890uq r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("com.whatsapp.MessageHandler.LOGOUT_ACTION".equals(intent.getAction())) {
            Log.i("xmpp/handler/logout-timer/timeout");
            C19890uq r1 = this.A00;
            if (!r1.A0g.A00()) {
                r1.A08();
                r1.A09();
            } else if (C21280xA.A00() || r1.A0s.get()) {
                r1.A06();
                r1.A0X.A00();
            } else {
                AnonymousClass1VL r12 = r1.A09;
                if (r12 != null) {
                    r12.Abb(false);
                } else {
                    Log.i("xmpp/handler/logout-timer ignoring due to null sending channel");
                }
            }
        } else {
            StringBuilder sb = new StringBuilder("unknown intent received in logout receiver ");
            sb.append(intent);
            Log.w(sb.toString());
        }
    }
}
