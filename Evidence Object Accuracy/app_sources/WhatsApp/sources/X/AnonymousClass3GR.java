package X;

import android.net.Uri;

/* renamed from: X.3GR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GR {
    public static final Uri A00;
    public static final AnonymousClass3GR A01 = new AnonymousClass3GR();

    static {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(AnonymousClass4FX.A00);
        A00 = C65283Ix.A00(C12960it.A0d(C12960it.A0d(".identity.feo2.api", A0h), C12960it.A0k("content://")));
    }
}
