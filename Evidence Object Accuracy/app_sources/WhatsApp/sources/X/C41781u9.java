package X;

/* renamed from: X.1u9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41781u9 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Double A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;

    public C41781u9() {
        super(458, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(7, this.A05);
        r3.Abe(8, this.A06);
        r3.Abe(5, this.A07);
        r3.Abe(4, this.A00);
        r3.Abe(9, this.A08);
        r3.Abe(1, this.A03);
        r3.Abe(3, this.A02);
        r3.Abe(2, this.A04);
        r3.Abe(6, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamPtt {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttDraftPlayCnt", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttDraftSeekCnt", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttDuration", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttLock", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttPauseCnt", this.A08);
        Integer num = this.A03;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttResult", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttSize", this.A02);
        Integer num2 = this.A04;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttSource", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "pttStop", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
