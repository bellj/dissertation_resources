package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57492n9 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57492n9 A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public long A02;
    public AbstractC27881Jp A03 = AbstractC27881Jp.A01;
    public String A04 = "";
    public String A05 = "";

    static {
        C57492n9 r0 = new C57492n9();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57492n9 r4 = (C57492n9) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A01;
                int i3 = r4.A00;
                this.A01 = r7.Afp(i2, r4.A01, A1R, C12960it.A1R(i3));
                this.A04 = r7.Afy(this.A04, r4.A04, C12960it.A1V(i & 2, 2), C12960it.A1V(i3 & 2, 2));
                this.A02 = r7.Afs(this.A02, r4.A02, C12960it.A1V(i & 4, 4), C12960it.A1V(i3 & 4, 4));
                this.A05 = r7.Afy(this.A05, r4.A05, C12960it.A1V(i & 8, 8), C12960it.A1V(i3 & 8, 8));
                this.A03 = r7.Afm(this.A03, r4.A03, C12960it.A1V(i & 16, 16), C12960it.A1V(i3 & 16, 16));
                if (r7 == C463025i.A00) {
                    this.A00 |= r4.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 8) {
                            this.A00 |= 1;
                            this.A01 = r72.A02();
                        } else if (A03 == 18) {
                            String A0A = r72.A0A();
                            this.A00 |= 2;
                            this.A04 = A0A;
                        } else if (A03 == 24) {
                            this.A00 |= 4;
                            this.A02 = r72.A06();
                        } else if (A03 == 34) {
                            String A0A2 = r72.A0A();
                            this.A00 |= 8;
                            this.A05 = A0A2;
                        } else if (A03 == 42) {
                            this.A00 |= 16;
                            this.A03 = r72.A08();
                        } else if (!A0a(r72, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57492n9();
            case 5:
                return new C81573uE();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C57492n9.class) {
                        if (A07 == null) {
                            A07 = AbstractC27091Fz.A09(A06);
                        }
                    }
                }
                return A07;
            default:
                throw C12970iu.A0z();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A02(1, this.A01, 0);
        }
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A04, i2);
        }
        int i4 = this.A00;
        if ((i4 & 4) == 4) {
            i2 += CodedOutputStream.A06(3, this.A02);
        }
        if ((i4 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(4, this.A05, i2);
        }
        if ((this.A00 & 16) == 16) {
            i2 = AbstractC27091Fz.A05(this.A03, 5, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0H(3, this.A02);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A05);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0K(this.A03, 5);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
