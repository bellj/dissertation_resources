package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1rq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40591rq {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;

    public C40591rq(Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7, boolean z, boolean z2, boolean z3) {
        this.A06 = l;
        this.A07 = z;
        this.A09 = z2;
        this.A03 = l2;
        this.A08 = z3;
        this.A05 = l3;
        this.A04 = l4;
        this.A02 = l5;
        this.A01 = l6;
        this.A00 = l7;
    }

    public static C40591rq A00(String str) {
        boolean z;
        boolean z2;
        boolean z3;
        String[] split = str.split(",");
        Long A03 = C40601rr.A03(split, 0);
        int length = split.length;
        if (length > 1) {
            z = Boolean.parseBoolean(split[1]);
        } else {
            z = false;
        }
        if (length > 2) {
            z2 = Boolean.parseBoolean(split[2]);
        } else {
            z2 = false;
        }
        Long A032 = C40601rr.A03(split, 3);
        if (length > 4) {
            z3 = Boolean.parseBoolean(split[4]);
        } else {
            z3 = false;
        }
        return new C40591rq(A03, A032, C40601rr.A03(split, 5), C40601rr.A03(split, 6), C40601rr.A03(split, 7), C40601rr.A03(split, 8), C40601rr.A03(split, 9), z, z2, z3);
    }

    public String toString() {
        return TextUtils.join(",", Arrays.asList(this.A06, Boolean.valueOf(this.A07), Boolean.valueOf(this.A09), this.A03, Boolean.valueOf(this.A08), this.A05, this.A04, this.A02, this.A01, this.A00));
    }
}
