package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;

/* renamed from: X.1uJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41881uJ implements AbstractC21730xt {
    public boolean A00;
    public final AbstractC14640lm A01;
    public final C17220qS A02;
    public final C27611If A03;
    public final AnonymousClass1P4 A04;

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
    }

    public C41881uJ(AbstractC14640lm r1, C17220qS r2, C27611If r3, AnonymousClass1P4 r4) {
        this.A01 = r1;
        this.A04 = r4;
        this.A02 = r2;
        this.A03 = r3;
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        C14900mE r2;
        int i;
        C27611If r4 = this.A03;
        int A00 = C41151sz.A00(r6);
        StringBuilder sb = new StringBuilder("profilephotohandler/request failed : ");
        sb.append(A00);
        sb.append(" | ");
        AbstractC14640lm r22 = r4.A0I;
        sb.append(r22);
        Log.i(sb.toString());
        r4.A01(3);
        r4.A03 = true;
        r4.A01.cancel();
        C27611If.A0O.remove(r4.A02.toString());
        if (!r4.A04) {
            C15370n3 A0B = r4.A09.A0B(r22);
            if (A00 != 401 || !A0B.A0K() || r4.A0F.A0C((GroupJid) A0B.A0B(GroupJid.class))) {
                r2 = r4.A07;
                i = 14;
            } else {
                r2 = r4.A07;
                i = 15;
            }
            r2.A0H(new RunnableBRunnable0Shape0S0101000_I0(r4, A00, i));
        }
        AnonymousClass1P4 r0 = r4.A0K;
        if (r0 != null) {
            r4.A0M.A0H(r0.A01, A00);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e4, code lost:
        if (r7 != false) goto L_0x00eb;
     */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r12, java.lang.String r13) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41881uJ.AX9(X.1V8, java.lang.String):void");
    }
}
