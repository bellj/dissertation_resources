package X;

/* renamed from: X.0LP  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0LP {
    public static int A00(int i, float f) {
        return (i & 16777215) | (Math.max(0, Math.min((int) (f * 255.0f), 255)) << 24);
    }
}
