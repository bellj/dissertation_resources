package X;

import android.content.Context;
import com.whatsapp.R;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.2WL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2WL extends GregorianCalendar {
    public final Context context;
    public int count;
    public final int id;
    public final AnonymousClass018 whatsAppLocale;

    public AnonymousClass2WL(Context context, AnonymousClass018 r3, AnonymousClass2WL r4) {
        this.id = r4.id;
        this.context = context;
        this.count = r4.count;
        setTime(r4.getTime());
        this.whatsAppLocale = r3;
    }

    public AnonymousClass2WL(Context context, AnonymousClass018 r3, Calendar calendar, int i) {
        this.id = i;
        this.context = context;
        setTime(calendar.getTime());
        this.whatsAppLocale = r3;
    }

    @Override // java.util.Calendar, java.lang.Object
    public String toString() {
        AnonymousClass018 r2;
        Locale locale;
        int i;
        int i2 = this.id;
        if (i2 == 1) {
            return this.context.getString(R.string.recent);
        }
        if (i2 == 2) {
            r2 = this.whatsAppLocale;
            locale = AnonymousClass018.A00(r2.A00);
            i = 232;
        } else if (i2 != 3) {
            AnonymousClass018 r5 = this.whatsAppLocale;
            long timeInMillis = getTimeInMillis();
            if (i2 != 4) {
                return new SimpleDateFormat(r5.A08(MediaCodecVideoEncoder.MIN_ENCODER_WIDTH), AnonymousClass018.A00(r5.A00)).format(new Date(timeInMillis));
            }
            Calendar instance = Calendar.getInstance(AnonymousClass018.A00(r5.A00));
            instance.setTimeInMillis(timeInMillis);
            return AbstractC28191Mb.A00(r5)[instance.get(2)];
        } else {
            r2 = this.whatsAppLocale;
            locale = AnonymousClass018.A00(r2.A00);
            i = 231;
        }
        return AnonymousClass1MY.A05(locale, r2.A08(i));
    }
}
