package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;

/* renamed from: X.1FJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FJ implements AnonymousClass1FK {
    public int A00;
    public int A01;
    public final C14830m7 A02;
    public final C20370ve A03;
    public final C21860y6 A04;
    public final C18600si A05;
    public final C22710zW A06;
    public final C17070qD A07;
    public final C30931Zj A08 = C30931Zj.A00("PaymentUnfinishedTransactionsSyncer", "network", "COMMON");
    public final AbstractC14440lR A09;

    public AnonymousClass1FJ(C14830m7 r4, C20370ve r5, C21860y6 r6, C18600si r7, C22710zW r8, C17070qD r9, AbstractC14440lR r10) {
        this.A02 = r4;
        this.A09 = r10;
        this.A07 = r9;
        this.A05 = r7;
        this.A04 = r6;
        this.A06 = r8;
        this.A03 = r5;
    }

    public synchronized void A00(AbstractC452320r r4) {
        if (!this.A06.A03() || !this.A04.A0C()) {
            this.A08.A06("skipped as account setup is incomplete.");
        } else {
            this.A09.Ab2(new RunnableBRunnable0Shape6S0200000_I0_6(this, 35, r4));
        }
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        C30931Zj r2 = this.A08;
        StringBuilder sb = new StringBuilder("onRequestError: ");
        sb.append(r4);
        r2.A05(sb.toString());
        AbstractC16870pt ACx = this.A07.A02().ACx();
        if (ACx != null) {
            ACx.AKa(r4, 10);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        C30931Zj r2 = this.A08;
        StringBuilder sb = new StringBuilder("onResponseError: ");
        sb.append(r4);
        r2.A05(sb.toString());
        AbstractC16870pt ACx = this.A07.A02().ACx();
        if (ACx != null) {
            ACx.AKa(r4, 10);
        }
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r6) {
        AbstractC16870pt ACx = this.A07.A02().ACx();
        if (ACx != null) {
            ACx.AKa(null, 10);
        }
        if (r6.A02) {
            int i = this.A00 + 1;
            this.A00 = i;
            C30931Zj r4 = this.A08;
            StringBuilder sb = new StringBuilder("finished syncing ");
            sb.append(i);
            sb.append(" transactions; total to sync: ");
            sb.append(this.A01);
            r4.A06(sb.toString());
            if (this.A01 == this.A00) {
                long A00 = this.A02.A00();
                this.A05.A01().edit().putLong("payments_pending_transactions_last_sync_time", A00).apply();
                StringBuilder sb2 = new StringBuilder("updatePendingTransactionsLastSyncTimeMilli to: ");
                sb2.append(A00);
                r4.A06(sb2.toString());
            }
        }
    }
}
