package X;

import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0vL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20180vL {
    public static final int A0G = 10;
    public static final int A0H = 5000;
    public static final long A0I = 64092211200L;
    public static final String A0J = "xpm-messages-exporter-";
    public long A00;
    public int A01;
    public long A02;
    public final AbstractC15710nm A03;
    public final C14830m7 A04;
    public final C17170qN A05;
    public final C14820m6 A06;
    public final C19990v2 A07;
    public final C16490p7 A08;
    public final C18680sq A09;
    public final C20140vH A0A;
    public final C20160vJ A0B;
    public final C20170vK A0C;
    public final C15760nr A0D;
    public final C15860o1 A0E;
    public final AbstractC15870o2 A0F;

    public static /* synthetic */ void A03() {
    }

    public C20180vL(C14830m7 r1, AbstractC15710nm r2, C19990v2 r3, C20140vH r4, C20160vJ r5, C15860o1 r6, C16490p7 r7, C14820m6 r8, C18680sq r9, C15760nr r10, AbstractC15870o2 r11, C20170vK r12, C17170qN r13) {
        this.A04 = r1;
        this.A03 = r2;
        this.A07 = r3;
        this.A0A = r4;
        this.A0B = r5;
        this.A0E = r6;
        this.A06 = r8;
        this.A08 = r7;
        this.A09 = r9;
        this.A0D = r10;
        this.A0F = r11;
        this.A0C = r12;
        this.A05 = r13;
    }

    public static /* synthetic */ int A00(Pair pair, Pair pair2) {
        return -((Long) pair.second).compareTo((Long) pair2.second);
    }

    public static int A01(String str) {
        return str.hashCode() & Integer.MAX_VALUE;
    }

    public static C462525d A02(int i) {
        if (i == 0) {
            return null;
        }
        AnonymousClass1G4 A0T = C462525d.A05.A0T();
        if ((i & 1) > 0) {
            A0T.A03();
            C462525d r2 = (C462525d) A0T.A00;
            r2.A00 |= 1;
            r2.A03 = true;
        }
        if ((i & 2) > 0) {
            A0T.A03();
            C462525d r22 = (C462525d) A0T.A00;
            r22.A00 |= 2;
            r22.A01 = true;
        }
        if ((i & 4) > 0) {
            A0T.A03();
            C462525d r23 = (C462525d) A0T.A00;
            r23.A00 |= 4;
            r23.A04 = true;
        }
        if ((i & 8) > 0) {
            A0T.A03();
            C462525d r24 = (C462525d) A0T.A00;
            r24.A00 |= 8;
            r24.A02 = true;
        }
        return (C462525d) A0T.A02();
    }

    public static void A04(C91784Tc r8, OutputStream outputStream) {
        try {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("creation_date", r8.A00);
            jSONObject2.put("os", r8.A05);
            jSONObject2.put("os_version", r8.A06);
            jSONObject2.put("app_name", r8.A02);
            jSONObject2.put("app_version", r8.A03);
            jSONObject2.put("format_version", r8.A04);
            jSONObject.put("header", jSONObject2);
            if (r8.A01 != null) {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject.put("messages", jSONObject3);
                jSONObject3.put("filename", r8.A01.A00);
                jSONObject3.put("format", r8.A01.A01);
                JSONArray jSONArray = new JSONArray();
                jSONObject3.put("chunks", jSONArray);
                List<C90454Ny> list = r8.A01.A02;
                if (list != null) {
                    for (C90454Ny r2 : list) {
                        JSONObject jSONObject4 = new JSONObject();
                        jSONObject4.put("chunk_number", r2.A00);
                        jSONObject4.put("messages_count", r2.A01);
                        jSONArray.put(jSONObject4);
                    }
                }
            }
            outputStream.write(jSONObject.toString(4).getBytes(DefaultCrypto.UTF_8));
        } catch (IOException | JSONException e) {
            Log.e("Failed to write header information.", e);
            throw e;
        }
    }

    public Cursor A07(int i, long j, long j2, long j3) {
        String[] strArr = {String.valueOf(j), String.valueOf(j2), String.valueOf(j3), String.valueOf(i)};
        C16310on A01 = this.A08.get();
        try {
            Cursor A09 = A01.A03.A09(C32301bw.A0J, strArr);
            A01.close();
            return A09;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public C91784Tc A08() {
        C91784Tc r2 = new C91784Tc();
        r2.A00 = System.currentTimeMillis();
        r2.A05 = "android";
        r2.A06 = String.valueOf(Build.VERSION.SDK_INT);
        r2.A02 = "consumer";
        r2.A03 = "2.22.17.70";
        r2.A04 = "1.0";
        return r2;
    }

    public AnonymousClass1P8 A09(Cursor cursor, CancellationSignal cancellationSignal, Map map) {
        Map A03 = this.A0B.A03(cursor, new C40881sU(this));
        if (A03.size() == 0) {
            return null;
        }
        AnonymousClass1P8 r3 = (AnonymousClass1P8) AnonymousClass1P7.A0D.A0T();
        r3.A06(AnonymousClass1P9.A01);
        for (Map.Entry entry : A03.entrySet()) {
            cancellationSignal.throwIfCanceled();
            AbstractC14640lm r2 = (AbstractC14640lm) entry.getKey();
            AnonymousClass1PC r4 = (AnonymousClass1PC) entry.getValue();
            if (this.A07.A06(AbstractC14640lm.A01(((AnonymousClass1PB) r4.A00).A0O)) != null) {
                if (r2 instanceof AbstractC15590nW) {
                    this.A0B.A04((AbstractC15590nW) r2, r4);
                }
                A0H(r2, r4, map);
                r3.A03();
                AnonymousClass1P7 r22 = (AnonymousClass1P7) r3.A00;
                AnonymousClass1K6 r1 = r22.A07;
                if (!((AnonymousClass1K7) r1).A00) {
                    r1 = AbstractC27091Fz.A0G(r1);
                    r22.A07 = r1;
                }
                r1.add(r4.A02());
            }
        }
        return r3;
    }

    public C463525n A0A(AbstractC14640lm r6, boolean z) {
        String str;
        C33191db AHh = this.A0F.AHh(r6, z);
        if (AHh == null || !AHh.A01.equals("USER_PROVIDED") || (str = AHh.A02) == null || TextUtils.isEmpty(str)) {
            return null;
        }
        Uri parse = Uri.parse(str);
        if (parse.getScheme() != null) {
            str = parse.getPath();
        }
        String A0B = A0B(new File(str));
        if (A0B == null) {
            return null;
        }
        C463525n r3 = (C463525n) C462425c.A03.A0T();
        r3.A03();
        C462425c r1 = (C462425c) r3.A00;
        r1.A00 |= 1;
        r1.A02 = A0B;
        int intValue = AHh.A00.intValue();
        r3.A03();
        C462425c r12 = (C462425c) r3.A00;
        r12.A00 |= 2;
        r12.A01 = intValue;
        return r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0099, code lost:
        r8 = r0.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009c, code lost:
        r9 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009d, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b0, code lost:
        r0 = new java.lang.StringBuilder("MessagesExporter/exportMediaFile/IOException during file registration. Local path: ");
        r0.append(r7);
        com.whatsapp.util.Log.e(r0.toString(), r9);
        r14.A03.AaV("xpm-messages-exporter-exportMediaFile/IOException", r9.getMessage(), true);
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e9 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A0B(java.io.File r15) {
        /*
        // Method dump skipped, instructions count: 269
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20180vL.A0B(java.io.File):java.lang.String");
    }

    public List A0C(CancellationSignal cancellationSignal, AnonymousClass5UQ r19, OutputStream outputStream) {
        ArrayList arrayList = new ArrayList();
        long A01 = this.A0A.A01() + 1;
        this.A00 = (long) this.A0A.A00(1, A01);
        this.A02 = 0;
        Map A0D = A0D();
        long j = 1;
        int i = 0;
        while (j < A01) {
            cancellationSignal.throwIfCanceled();
            C90454Ny r2 = new C90454Ny();
            Cursor ACG = r19.ACG(5000, j, A01, System.currentTimeMillis());
            if (ACG == null) {
                break;
            }
            try {
                r2.A01 = (long) ACG.getCount();
                if (ACG.moveToLast()) {
                    j = ACG.getLong(ACG.getColumnIndexOrThrow("_id"));
                    ACG.moveToFirst();
                    ACG.move(-1);
                    AnonymousClass1P8 A09 = A09(ACG, cancellationSignal, A0D);
                    if (A09 != null) {
                        A09.A03();
                        AnonymousClass1P7 r1 = (AnonymousClass1P7) A09.A00;
                        r1.A01 |= 2;
                        r1.A02 = i;
                        if (i == 0) {
                            A0I(A09);
                        }
                        ((AnonymousClass1P7) A09.A02()).A01(outputStream);
                        r2.A00 = i;
                        arrayList.add(r2);
                        i++;
                        ACG.close();
                    }
                }
                ACG.close();
                return arrayList;
            } catch (Throwable th) {
                try {
                    ACG.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return arrayList;
    }

    public Map A0D() {
        HashMap hashMap = new HashMap();
        Set<AbstractC14640lm> A0D = this.A0E.A0D();
        if (A0D.size() != 0) {
            ArrayList arrayList = new ArrayList(A0D.size());
            for (AbstractC14640lm r2 : A0D) {
                arrayList.add(new Pair(r2, Long.valueOf(this.A0E.A02(r2))));
            }
            Collections.sort(arrayList, new Comparator() { // from class: X.5CM
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return C20180vL.A00((Pair) obj, (Pair) obj2);
                }
            });
            int i = 0;
            while (i < arrayList.size()) {
                i++;
                hashMap.put(((Pair) arrayList.get(i)).first, Integer.valueOf(i));
            }
        }
        return hashMap;
    }

    public void A0E(long j) {
        this.A00 = j;
    }

    public void A0F(CancellationSignal cancellationSignal, AnonymousClass5UQ r13, File file) {
        C28181Ma r8 = new C28181Ma(false);
        r8.A03();
        this.A01 = 0;
        for (AnonymousClass2F5 r0 : this.A0C.A01()) {
            r0.AUL(0);
        }
        C91784Tc A08 = A08();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
            zipOutputStream.putNextEntry(new ZipEntry("messages.bin"));
            file.getParentFile();
            List A0C = A0C(cancellationSignal, r13, zipOutputStream);
            zipOutputStream.closeEntry();
            AnonymousClass4QT r1 = new AnonymousClass4QT();
            r1.A00 = "messages.bin";
            r1.A01 = "protobuf";
            r1.A02 = A0C;
            A08.A01 = r1;
            zipOutputStream.putNextEntry(new ZipEntry("header.json"));
            A04(A08, zipOutputStream);
            zipOutputStream.closeEntry();
            zipOutputStream.close();
            fileOutputStream.close();
            long A00 = r8.A00();
            Locale locale = Locale.US;
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            String format = String.format(locale, "%02d:%02d:%02d", Long.valueOf(timeUnit.toHours(A00)), Long.valueOf(timeUnit.toMinutes(A00) - TimeUnit.HOURS.toMinutes(timeUnit.toHours(A00))), Long.valueOf(timeUnit.toSeconds(A00) - TimeUnit.MINUTES.toSeconds(timeUnit.toMinutes(A00))));
            StringBuilder sb = new StringBuilder("exportMessages - messages exporting is completed, consumed time: ");
            sb.append(format);
            Log.i(sb.toString());
        } catch (JSONException e) {
            this.A0C.A05(1);
            Log.e("Failed to write metadata; data export is not completed.");
            file.delete();
            throw new IOException("Failed to write metadata; data export is not completed.", e);
        }
    }

    public void A0G(CancellationSignal cancellationSignal, File file) {
        A0F(cancellationSignal, new AnonymousClass5UQ() { // from class: X.582
            @Override // X.AnonymousClass5UQ
            public final Cursor ACG(int i, long j, long j2, long j3) {
                return C20180vL.this.A07(5000, j, j2, j3);
            }
        }, file);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0035, code lost:
        if (r1 != 0) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0H(X.AbstractC14640lm r9, X.AnonymousClass1PC r10, java.util.Map r11) {
        /*
            r8 = this;
            java.lang.Object r0 = r11.get(r9)
            java.lang.Number r0 = (java.lang.Number) r0
            if (r0 == 0) goto L_0x001c
            int r3 = r0.intValue()
            r10.A03()
            X.1Fz r2 = r10.A00
            X.1PB r2 = (X.AnonymousClass1PB) r2
            int r1 = r2.A01
            r0 = 2097152(0x200000, float:2.938736E-39)
            r1 = r1 | r0
            r2.A01 = r1
            r2.A06 = r3
        L_0x001c:
            X.0o1 r1 = r8.A0E
            java.lang.String r0 = r9.getRawString()
            X.1da r7 = r1.A08(r0)
            long r1 = r7.A00()
            r5 = 0
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x008a
            r3 = 1000(0x3e8, double:4.94E-321)
            long r1 = r1 / r3
        L_0x0033:
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0047
        L_0x0037:
            r10.A03()
            X.1Fz r4 = r10.A00
            X.1PB r4 = (X.AnonymousClass1PB) r4
            int r3 = r4.A01
            r0 = 4194304(0x400000, float:5.877472E-39)
            r3 = r3 | r0
            r4.A01 = r3
            r4.A0D = r1
        L_0x0047:
            int r1 = r7.A00
            r4 = 1
            if (r1 != r4) goto L_0x0084
            X.4BT r3 = X.AnonymousClass4BT.A02
        L_0x004e:
            r10.A03()
            X.1Fz r2 = r10.A00
            X.1PB r2 = (X.AnonymousClass1PB) r2
            int r1 = r2.A01
            r0 = 16777216(0x1000000, float:2.3509887E-38)
            r1 = r1 | r0
            r2.A01 = r1
            int r0 = r3.value
            r2.A05 = r0
        L_0x0060:
            r0 = 0
            X.25n r0 = r8.A0A(r9, r0)
            if (r0 != 0) goto L_0x006d
            X.25n r0 = r8.A0A(r9, r4)
            if (r0 == 0) goto L_0x0083
        L_0x006d:
            r10.A03()
            X.1Fz r2 = r10.A00
            X.1PB r2 = (X.AnonymousClass1PB) r2
            X.1Fz r0 = r0.A02()
            X.25c r0 = (X.C462425c) r0
            r2.A0L = r0
            int r1 = r2.A01
            r0 = 8388608(0x800000, float:1.17549435E-38)
            r1 = r1 | r0
            r2.A01 = r1
        L_0x0083:
            return
        L_0x0084:
            r0 = 2
            if (r1 != r0) goto L_0x0060
            X.4BT r3 = X.AnonymousClass4BT.A03
            goto L_0x004e
        L_0x008a:
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0033
            r1 = 64092211200(0xeec318800, double:3.166575972E-313)
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20180vL.A0H(X.0lm, X.1PC, java.util.Map):void");
    }

    public void A0I(AnonymousClass1P8 r6) {
        AnonymousClass4BT A00;
        AnonymousClass1G4 A0T = C57632nN.A0B.A0T();
        C463525n A0A = A0A(null, false);
        if (A0A != null) {
            A0T.A03();
            C57632nN r1 = (C57632nN) A0T.A00;
            r1.A08 = (C462425c) A0A.A02();
            r1.A00 |= 1;
        }
        C463525n A0A2 = A0A(null, true);
        if (A0A2 != null) {
            A0T.A03();
            C57632nN r12 = (C57632nN) A0T.A00;
            r12.A07 = (C462425c) A0A2.A02();
            r12.A00 |= 4;
        }
        int i = this.A0E.A05().A00;
        if (i == 0) {
            A00 = AnonymousClass4BT.A01;
        } else {
            A00 = AnonymousClass4BT.A00(i);
        }
        A0T.A03();
        C57632nN r13 = (C57632nN) A0T.A00;
        r13.A00 |= 2;
        r13.A02 = A00.value;
        C462525d A02 = A02(this.A06.A00.getInt("autodownload_wifi_mask", 15));
        if (A02 != null) {
            A0T.A03();
            C57632nN r14 = (C57632nN) A0T.A00;
            r14.A06 = A02;
            r14.A00 |= 8;
        }
        C462525d A022 = A02(this.A06.A00.getInt("autodownload_cellular_mask", 1));
        if (A022 != null) {
            A0T.A03();
            C57632nN r15 = (C57632nN) A0T.A00;
            r15.A04 = A022;
            r15.A00 |= 16;
        }
        C462525d A023 = A02(this.A06.A00.getInt("autodownload_roaming_mask", 0));
        if (A023 != null) {
            A0T.A03();
            C57632nN r16 = (C57632nN) A0T.A00;
            r16.A05 = A023;
            r16.A00 |= 32;
        }
        this.A0E.A05();
        A0T.A03();
        C57632nN r17 = (C57632nN) A0T.A00;
        r17.A00 |= 64;
        r17.A0A = !this.A0E.A05().A0B();
        this.A0E.A04();
        A0T.A03();
        C57632nN r18 = (C57632nN) A0T.A00;
        r18.A00 |= 128;
        r18.A09 = !this.A0E.A04().A0B();
        r6.A03();
        AnonymousClass1P7 r19 = (AnonymousClass1P7) r6.A00;
        r19.A0C = (C57632nN) A0T.A02();
        r19.A01 |= 8;
    }

    public void A0J(AbstractC15340mz r7) {
        C16150oX r2;
        String A0B;
        if (!(!(r7 instanceof AbstractC16130oV) || (r2 = ((AbstractC16130oV) r7).A02) == null || (A0B = A0B(r2.A0F)) == null)) {
            r2.A0F = new File(A0B);
            long j = this.A02 + 1;
            this.A02 = j;
            int i = (int) ((((double) j) * 100.0d) / ((double) this.A00));
            if (this.A01 != i) {
                this.A01 = i;
                for (AnonymousClass2F5 r0 : this.A0C.A01()) {
                    r0.AUL(i);
                }
            }
        }
    }
}
