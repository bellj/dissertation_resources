package X;

import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.1jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36371jm extends AbstractC16350or {
    public final C14900mE A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C20710wC A03;
    public final C15580nU A04;
    public final C20660w7 A05;
    public final WeakReference A06;
    public final List A07;
    public final List A08;

    public C36371jm(ActivityC13810kN r2, C14900mE r3, C15550nR r4, C15610nY r5, C20710wC r6, C15580nU r7, C20660w7 r8, List list, List list2) {
        this.A00 = r3;
        this.A05 = r8;
        this.A01 = r4;
        this.A02 = r5;
        this.A03 = r6;
        this.A06 = new WeakReference(r2);
        this.A04 = r7;
        this.A08 = list;
        this.A07 = list2;
    }
}
