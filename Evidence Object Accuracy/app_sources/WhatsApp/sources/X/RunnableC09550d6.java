package X;

import java.util.ArrayList;

/* renamed from: X.0d6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09550d6 implements Runnable {
    public final /* synthetic */ AnonymousClass0Q9 A00;
    public final /* synthetic */ AbstractC06180Sm A01;

    public RunnableC09550d6(AnonymousClass0Q9 r1, AbstractC06180Sm r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        ArrayList arrayList = this.A01.A03;
        AnonymousClass0Q9 r2 = this.A00;
        if (arrayList.contains(r2)) {
            r2.A01.A02(r2.A04.A0A);
        }
    }
}
