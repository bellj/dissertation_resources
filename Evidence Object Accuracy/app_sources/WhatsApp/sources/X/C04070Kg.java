package X;

import android.os.Build;
import java.util.Locale;

/* renamed from: X.0Kg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04070Kg {
    @Deprecated
    public static boolean A00() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 31) {
            return true;
        }
        if (i < 30) {
            return false;
        }
        String str = Build.VERSION.CODENAME;
        if ("REL".equals(str)) {
            return false;
        }
        Locale locale = Locale.ROOT;
        return str.toUpperCase(locale).compareTo("S".toUpperCase(locale)) >= 0;
    }
}
