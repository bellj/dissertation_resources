package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2PA  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2PA {
    public final long A00;
    public final Jid A01;
    public final String A02;

    public AnonymousClass2PA(Jid jid, String str, long j) {
        this.A01 = jid;
        this.A02 = str;
        this.A00 = j;
    }
}
