package X;

import com.facebook.redex.RunnableBRunnable0Shape0S2310000_I0;
import org.json.JSONObject;

/* renamed from: X.4Vy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C92454Vy {
    public final /* synthetic */ C63933Dm A00;
    public final /* synthetic */ AnonymousClass1B4 A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ JSONObject A04;
    public final /* synthetic */ boolean A05 = true;

    public /* synthetic */ C92454Vy(C63933Dm r2, AnonymousClass1B4 r3, String str, String str2, JSONObject jSONObject) {
        this.A01 = r3;
        this.A02 = str;
        this.A04 = jSONObject;
        this.A03 = str2;
        this.A00 = r2;
    }

    public final void A00() {
        AnonymousClass1B4 r2 = this.A01;
        String str = this.A02;
        JSONObject jSONObject = this.A04;
        String str2 = this.A03;
        r2.A07.Ab2(new RunnableBRunnable0Shape0S2310000_I0(r2, this.A00, jSONObject, str2, str, 0, this.A05));
    }
}
