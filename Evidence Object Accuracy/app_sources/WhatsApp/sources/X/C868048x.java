package X;

import java.io.IOException;

/* renamed from: X.48x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C868048x extends IOException {
    public AbstractC117125Yn zzkw = null;

    public C868048x(String str) {
        super(str);
    }

    public static C868048x A00() {
        return new C868048x("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }
}
