package X;

import java.io.Serializable;

/* renamed from: X.0qw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17520qw implements Serializable {
    public final Object first;
    public final Object second;

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C17520qw) {
                C17520qw r5 = (C17520qw) obj;
                if (!C16700pc.A0O(this.first, r5.first) || !C16700pc.A0O(this.second, r5.second)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object obj = this.first;
        int i = 0;
        int hashCode = (obj == null ? 0 : obj.hashCode()) * 31;
        Object obj2 = this.second;
        if (obj2 != null) {
            i = obj2.hashCode();
        }
        return hashCode + i;
    }

    public C17520qw(Object obj, Object obj2) {
        this.first = obj;
        this.second = obj2;
    }

    public static void A00(Object obj, Object obj2, Object[] objArr, int i) {
        objArr[i] = new C17520qw(obj, obj2);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("(");
        sb.append(this.first);
        sb.append(", ");
        sb.append(this.second);
        sb.append(')');
        return sb.toString();
    }
}
