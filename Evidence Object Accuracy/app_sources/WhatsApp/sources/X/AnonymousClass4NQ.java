package X;

import java.util.List;
import java.util.Set;

/* renamed from: X.4NQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4NQ {
    public final List A00;
    public final Set A01;

    public AnonymousClass4NQ(List list, Set set) {
        this.A00 = list;
        this.A01 = set;
    }
}
