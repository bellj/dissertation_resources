package X;

/* renamed from: X.0FC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FC extends AbstractC05270Ox {
    public final /* synthetic */ AnonymousClass0F7 A00;

    public AnonymousClass0FC(AnonymousClass0F7 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002d, code lost:
        if (r5 < r4.A0H) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if (r9 < r4.A0H) goto L_0x001d;
     */
    @Override // X.AbstractC05270Ox
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(androidx.recyclerview.widget.RecyclerView r13, int r14, int r15) {
        /*
            r12 = this;
            X.0F7 r4 = r12.A00
            int r10 = r13.computeHorizontalScrollOffset()
            int r2 = r13.computeVerticalScrollOffset()
            androidx.recyclerview.widget.RecyclerView r6 = r4.A0B
            int r11 = r6.computeVerticalScrollRange()
            int r9 = r4.A06
            int r0 = r11 - r9
            r1 = 0
            r3 = 1
            if (r0 <= 0) goto L_0x001d
            int r5 = r4.A0H
            r0 = 1
            if (r9 >= r5) goto L_0x001e
        L_0x001d:
            r0 = 0
        L_0x001e:
            r4.A0D = r0
            int r6 = r6.computeHorizontalScrollRange()
            int r5 = r4.A07
            int r0 = r6 - r5
            if (r0 <= 0) goto L_0x002f
            int r0 = r4.A0H
            r8 = 1
            if (r5 >= r0) goto L_0x0030
        L_0x002f:
            r8 = 0
        L_0x0030:
            r4.A0C = r8
            boolean r0 = r4.A0D
            if (r0 != 0) goto L_0x0040
            if (r8 != 0) goto L_0x0040
            int r0 = r4.A08
            if (r0 == 0) goto L_0x003f
            r4.A04(r1)
        L_0x003f:
            return
        L_0x0040:
            r7 = 1073741824(0x40000000, float:2.0)
            if (r0 == 0) goto L_0x0058
            float r2 = (float) r2
            float r1 = (float) r9
            float r0 = r1 / r7
            float r2 = r2 + r0
            float r1 = r1 * r2
            float r0 = (float) r11
            float r1 = r1 / r0
            int r0 = (int) r1
            r4.A09 = r0
            int r0 = r9 * r9
            int r0 = r0 / r11
            int r0 = java.lang.Math.min(r9, r0)
            r4.A0A = r0
        L_0x0058:
            if (r8 == 0) goto L_0x006e
            float r2 = (float) r10
            float r1 = (float) r5
            float r0 = r1 / r7
            float r2 = r2 + r0
            float r1 = r1 * r2
            float r0 = (float) r6
            float r1 = r1 / r0
            int r0 = (int) r1
            r4.A04 = r0
            int r0 = r5 * r5
            int r0 = r0 / r6
            int r0 = java.lang.Math.min(r5, r0)
            r4.A05 = r0
        L_0x006e:
            int r0 = r4.A08
            if (r0 == 0) goto L_0x0074
            if (r0 != r3) goto L_0x003f
        L_0x0074:
            r4.A04(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0FC.A01(androidx.recyclerview.widget.RecyclerView, int, int):void");
    }
}
