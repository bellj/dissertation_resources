package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.5m6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122785m6 extends C122625ll {
    public final WaImageView A00;

    public C122785m6(View view) {
        super(view);
        this.A00 = C12980iv.A0X(view, R.id.asset_id);
    }

    @Override // X.C122625ll, X.AbstractC118835cS
    public void A08(AbstractC125975s7 r5, int i) {
        C123345n0 r3 = (C123345n0) r5;
        int i2 = r3.A00;
        if (i2 != -1) {
            WaImageView waImageView = this.A00;
            waImageView.setImageResource(i2);
            waImageView.setOnClickListener(r3.A01);
        }
        super.A08(r5, i);
    }
}
