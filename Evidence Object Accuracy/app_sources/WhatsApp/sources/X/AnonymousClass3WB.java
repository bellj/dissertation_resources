package X;

import android.hardware.Camera;

/* renamed from: X.3WB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WB implements AbstractC115975Tq {
    public final AnonymousClass4Q1 A00 = new AnonymousClass4Q1();
    public final /* synthetic */ AnonymousClass27X A01;

    public /* synthetic */ AnonymousClass3WB(AnonymousClass27X r2) {
        this.A01 = r2;
    }

    @Override // X.AbstractC115975Tq
    public synchronized AnonymousClass4Q1 AAP() {
        AnonymousClass4Q1 r4;
        r4 = this.A00;
        byte[] bArr = r4.A02;
        if (bArr != null) {
            AnonymousClass27X r2 = this.A01;
            Camera camera = r2.A07;
            if (camera != null && !r2.A0R && bArr == r2.A0S) {
                camera.addCallbackBuffer(bArr);
            }
            r4.A02 = null;
        }
        while (r4.A02 == null) {
            wait();
        }
        return r4;
    }
}
