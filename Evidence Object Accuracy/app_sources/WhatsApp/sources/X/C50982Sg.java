package X;

import android.content.res.Resources;
import com.whatsapp.R;
import com.whatsapp.gallery.GalleryRecentsFragment;
import com.whatsapp.gallery.GalleryTabHostFragment;
import java.util.Collection;

/* renamed from: X.2Sg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50982Sg extends AnonymousClass019 implements AbstractC48852Ic {
    public final int A00;
    public final Resources A01;
    public final AnonymousClass1s8 A02;
    public final GalleryTabHostFragment A03;
    public final AbstractC16710pd A04 = AnonymousClass4Yq.A00(new C72003dr(this));
    public final AbstractC16710pd A05 = AnonymousClass4Yq.A00(new C72013ds(this));

    @Override // X.AnonymousClass01A
    public int A01() {
        return 2;
    }

    public C50982Sg(Resources resources, AnonymousClass01F r3, AnonymousClass1s8 r4, GalleryTabHostFragment galleryTabHostFragment, int i) {
        super(r3, 0);
        this.A03 = galleryTabHostFragment;
        this.A01 = resources;
        this.A00 = i;
        this.A02 = r4;
    }

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        Resources resources;
        int i2;
        if (i == 0) {
            resources = this.A01;
            i2 = R.string.gallery_tab_title_recents;
        } else if (i == 1) {
            resources = this.A01;
            i2 = R.string.gallery_tab_title_folders;
        } else {
            throw new IllegalArgumentException(C16700pc.A08("Invalid item position: ", Integer.valueOf(i)));
        }
        String string = resources.getString(i2);
        C16700pc.A0B(string);
        return string;
    }

    @Override // X.AnonymousClass019
    public AnonymousClass01E A0G(int i) {
        AbstractC16710pd r0;
        if (i == 0) {
            r0 = this.A05;
        } else if (i == 1) {
            r0 = this.A04;
        } else {
            throw new IllegalArgumentException(C16700pc.A08("Invalid item position: ", Integer.valueOf(i)));
        }
        return (AnonymousClass01E) r0.getValue();
    }

    @Override // X.AbstractC48852Ic
    public void AGY(C453421e r2, Collection collection) {
        C16700pc.A0E(collection, 0);
        C16700pc.A0E(r2, 1);
        ((GalleryRecentsFragment) this.A05.getValue()).AGY(r2, collection);
    }

    @Override // X.AbstractC48852Ic
    public void AZy() {
        ((GalleryRecentsFragment) this.A05.getValue()).AZy();
    }

    @Override // X.AbstractC48852Ic
    public void Acp(C453421e r2, Collection collection, Collection collection2) {
        C16700pc.A0E(collection, 0);
        C16700pc.A0E(collection2, 1);
        C16700pc.A0E(r2, 2);
        ((GalleryRecentsFragment) this.A05.getValue()).Acp(r2, collection, collection2);
    }
}
