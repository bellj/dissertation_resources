package X;

import android.content.Context;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.3w2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82693w2 extends AnonymousClass2k7 {
    @Override // X.AnonymousClass2k7
    public void A07(View view, C14260l7 r2, AnonymousClass28D r3, Object obj) {
    }

    public C82693w2(C14260l7 r1, AnonymousClass28D r2) {
        super(r1, r2);
    }

    @Override // X.AnonymousClass2k7
    public void A06(View view, C14260l7 r5, AnonymousClass28D r6, Object obj) {
        AbstractC14200l1 A0G = r6.A0G(35);
        if (A0G != null) {
            view.post(new RunnableBRunnable0Shape3S0300000_I1(r6, A0G, r5, 12));
        }
    }

    @Override // X.AnonymousClass2k7, X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        View view = new View(context);
        view.setVisibility(8);
        return view;
    }
}
