package X;

/* renamed from: X.0dN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class RunnableC09700dN implements Runnable {
    public final AnonymousClass041 A00;
    public final AbstractFutureC44231yX A01;

    public RunnableC09700dN(AnonymousClass041 r1, AbstractFutureC44231yX r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass041 r2 = this.A00;
        if (r2.value == this) {
            if (AnonymousClass041.A00.A04(r2, this, AnonymousClass041.A01(this.A01))) {
                AnonymousClass041.A04(r2);
            }
        }
    }
}
