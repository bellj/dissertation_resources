package X;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

/* renamed from: X.0HE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HE extends AbstractC08070aX {
    public AnonymousClass0QR A00;
    public final Paint A01;
    public final Path A02;
    public final RectF A03 = new RectF();
    public final AnonymousClass0PU A04;
    public final float[] A05;

    public AnonymousClass0HE(AnonymousClass0AA r3, AnonymousClass0PU r4) {
        super(r3, r4);
        C020609t r1 = new C020609t();
        this.A01 = r1;
        this.A05 = new float[8];
        this.A02 = new Path();
        this.A04 = r4;
        r1.setAlpha(0);
        r1.setStyle(Paint.Style.FILL);
        r1.setColor(r4.A04);
    }

    @Override // X.AbstractC08070aX
    public void A06(Canvas canvas, Matrix matrix, int i) {
        int intValue;
        AnonymousClass0PU r2 = this.A04;
        int alpha = Color.alpha(r2.A04);
        if (alpha != 0) {
            AnonymousClass0QR r0 = this.A0L.A02;
            if (r0 == null) {
                intValue = 100;
            } else {
                intValue = ((Number) r0.A03()).intValue();
            }
            int i2 = (int) ((((float) i) / 255.0f) * (((((float) alpha) / 255.0f) * ((float) intValue)) / 100.0f) * 255.0f);
            Paint paint = this.A01;
            paint.setAlpha(i2);
            AnonymousClass0QR r02 = this.A00;
            if (r02 != null) {
                paint.setColorFilter((ColorFilter) r02.A03());
            }
            if (i2 > 0) {
                float[] fArr = this.A05;
                fArr[0] = 0.0f;
                fArr[1] = 0.0f;
                float f = (float) r2.A06;
                fArr[2] = f;
                fArr[3] = 0.0f;
                fArr[4] = f;
                float f2 = (float) r2.A05;
                fArr[5] = f2;
                fArr[6] = 0.0f;
                fArr[7] = f2;
                matrix.mapPoints(fArr);
                Path path = this.A02;
                path.reset();
                path.moveTo(fArr[0], fArr[1]);
                path.lineTo(fArr[2], fArr[3]);
                path.lineTo(fArr[4], fArr[5]);
                path.lineTo(fArr[6], fArr[7]);
                path.lineTo(fArr[0], fArr[1]);
                path.close();
                canvas.drawPath(path, paint);
            }
        }
    }

    @Override // X.AbstractC08070aX, X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r3, Object obj) {
        AnonymousClass0Gu r1;
        super.A5q(r3, obj);
        if (obj == AbstractC12810iX.A00) {
            if (r3 == null) {
                r1 = null;
            } else {
                r1 = new AnonymousClass0Gu(r3, null);
            }
            this.A00 = r1;
        }
    }

    @Override // X.AbstractC08070aX, X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        super.AAy(matrix, rectF, z);
        RectF rectF2 = this.A03;
        AnonymousClass0PU r1 = this.A04;
        rectF2.set(0.0f, 0.0f, (float) r1.A06, (float) r1.A05);
        this.A08.mapRect(rectF2);
        rectF.set(rectF2);
    }
}
