package X;

/* renamed from: X.0vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20470vo {
    public final AnonymousClass01d A00;
    public final C16590pI A01;
    public final C14850m9 A02;
    public final C21680xo A03;
    public final C16630pM A04;
    public final AbstractC20460vn A05;
    public final AnonymousClass15R A06;

    public C20470vo(AnonymousClass01d r1, C16590pI r2, C14850m9 r3, C21680xo r4, C16630pM r5, AbstractC20460vn r6, AnonymousClass15R r7) {
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A03 = r4;
        this.A00 = r1;
        this.A05 = r6;
        this.A04 = r5;
    }
}
