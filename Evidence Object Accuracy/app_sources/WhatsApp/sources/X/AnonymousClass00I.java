package X;

import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/* renamed from: X.00I  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass00I implements Closeable {
    public final long A00;
    public final File A01;
    public final File A02;
    public final RandomAccessFile A03;
    public final FileChannel A04;
    public final FileLock A05;

    public AnonymousClass00I(File file, File file2) {
        StringBuilder sb = new StringBuilder("MultiDexExtractor(");
        sb.append(file.getPath());
        sb.append(", ");
        sb.append(file2.getPath());
        sb.append(")");
        Log.i("MultiDex", sb.toString());
        this.A02 = file;
        this.A01 = file2;
        this.A00 = A00(file);
        File file3 = new File(file2, "MultiDex.lock");
        RandomAccessFile randomAccessFile = new RandomAccessFile(file3, "rw");
        this.A03 = randomAccessFile;
        try {
            FileChannel channel = randomAccessFile.getChannel();
            this.A04 = channel;
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Blocking on lock ");
                sb2.append(file3.getPath());
                Log.i("MultiDex", sb2.toString());
                this.A05 = channel.lock();
                StringBuilder sb3 = new StringBuilder();
                sb3.append(file3.getPath());
                sb3.append(" locked");
                Log.i("MultiDex", sb3.toString());
            } catch (IOException | Error | RuntimeException e) {
                A01(this.A04);
                throw e;
            }
        } catch (IOException | Error | RuntimeException e2) {
            A01(this.A03);
            throw e2;
        }
    }

    /* JADX INFO: finally extract failed */
    public static long A00(File file) {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        try {
            long length = randomAccessFile.length() - 22;
            long j = 0;
            if (length >= 0) {
                long j2 = length - 65536;
                if (j2 >= 0) {
                    j = j2;
                }
                int reverseBytes = Integer.reverseBytes(101010256);
                do {
                    randomAccessFile.seek(length);
                    if (randomAccessFile.readInt() == reverseBytes) {
                        randomAccessFile.skipBytes(2);
                        randomAccessFile.skipBytes(2);
                        randomAccessFile.skipBytes(2);
                        randomAccessFile.skipBytes(2);
                        AnonymousClass00K r4 = new AnonymousClass00K();
                        r4.A01 = ((long) Integer.reverseBytes(randomAccessFile.readInt())) & 4294967295L;
                        r4.A00 = ((long) Integer.reverseBytes(randomAccessFile.readInt())) & 4294967295L;
                        CRC32 crc32 = new CRC32();
                        long j3 = r4.A01;
                        randomAccessFile.seek(r4.A00);
                        int min = (int) Math.min(16384L, j3);
                        byte[] bArr = new byte[16384];
                        while (true) {
                            int read = randomAccessFile.read(bArr, 0, min);
                            if (read == -1) {
                                break;
                            }
                            crc32.update(bArr, 0, read);
                            j3 -= (long) read;
                            if (j3 == 0) {
                                break;
                            }
                            min = (int) Math.min(16384L, j3);
                        }
                        long value = crc32.getValue();
                        randomAccessFile.close();
                        return value == -1 ? value - 1 : value;
                    }
                    length--;
                } while (length >= j);
                throw new ZipException("End Of Central Directory signature not found");
            }
            StringBuilder sb = new StringBuilder("File too short to be a zip file: ");
            sb.append(randomAccessFile.length());
            throw new ZipException(sb.toString());
        } catch (Throwable th) {
            randomAccessFile.close();
            throw th;
        }
    }

    public static void A01(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e) {
            Log.w("MultiDex", "Failed to close resource", e);
        }
    }

    public final List A02() {
        StringBuilder sb = new StringBuilder();
        File file = this.A02;
        sb.append(file.getName());
        sb.append(".classes");
        String obj = sb.toString();
        File file2 = this.A01;
        File[] listFiles = file2.listFiles(new C08880c0(this));
        if (listFiles == null) {
            StringBuilder sb2 = new StringBuilder("Failed to list secondary dex dir content (");
            sb2.append(file2.getPath());
            sb2.append(").");
            Log.w("MultiDex", sb2.toString());
        } else {
            for (File file3 : listFiles) {
                StringBuilder sb3 = new StringBuilder("Trying to delete old file ");
                sb3.append(file3.getPath());
                sb3.append(" of size ");
                sb3.append(file3.length());
                Log.i("MultiDex", sb3.toString());
                if (!file3.delete()) {
                    StringBuilder sb4 = new StringBuilder("Failed to delete old file ");
                    sb4.append(file3.getPath());
                    Log.w("MultiDex", sb4.toString());
                } else {
                    StringBuilder sb5 = new StringBuilder("Deleted old file ");
                    sb5.append(file3.getPath());
                    Log.i("MultiDex", sb5.toString());
                }
            }
        }
        ArrayList arrayList = new ArrayList();
        ZipFile zipFile = new ZipFile(file);
        try {
            StringBuilder sb6 = new StringBuilder();
            sb6.append("classes");
            sb6.append(2);
            sb6.append(".dex");
            ZipEntry entry = zipFile.getEntry(sb6.toString());
            int i = 2;
            while (entry != null) {
                StringBuilder sb7 = new StringBuilder();
                sb7.append(obj);
                sb7.append(i);
                sb7.append(".zip");
                AnonymousClass00L r3 = new AnonymousClass00L(file2, sb7.toString());
                arrayList.add(r3);
                StringBuilder sb8 = new StringBuilder();
                sb8.append("Extraction is needed for file ");
                sb8.append(r3);
                Log.i("MultiDex", sb8.toString());
                int i2 = 0;
                boolean z = false;
                while (i2 < 3 && !z) {
                    i2++;
                    InputStream inputStream = zipFile.getInputStream(entry);
                    StringBuilder sb9 = new StringBuilder("tmp-");
                    sb9.append(obj);
                    File createTempFile = File.createTempFile(sb9.toString(), ".zip", r3.getParentFile());
                    StringBuilder sb10 = new StringBuilder("Extracting ");
                    sb10.append(createTempFile.getPath());
                    Log.i("MultiDex", sb10.toString());
                    ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(createTempFile)));
                    try {
                        ZipEntry zipEntry = new ZipEntry("classes.dex");
                        zipEntry.setTime(entry.getTime());
                        zipOutputStream.putNextEntry(zipEntry);
                        byte[] bArr = new byte[16384];
                        for (int read = inputStream.read(bArr); read != -1; read = inputStream.read(bArr)) {
                            zipOutputStream.write(bArr, 0, read);
                        }
                        zipOutputStream.closeEntry();
                        zipOutputStream.close();
                        if (createTempFile.setReadOnly()) {
                            StringBuilder sb11 = new StringBuilder();
                            sb11.append("Renaming to ");
                            sb11.append(r3.getPath());
                            Log.i("MultiDex", sb11.toString());
                            if (createTempFile.renameTo(r3)) {
                                A01(inputStream);
                                createTempFile.delete();
                                try {
                                    r3.crc = A00(r3);
                                    z = true;
                                } catch (IOException e) {
                                    StringBuilder sb12 = new StringBuilder();
                                    sb12.append("Failed to read crc from ");
                                    sb12.append(r3.getAbsolutePath());
                                    Log.w("MultiDex", sb12.toString(), e);
                                    z = false;
                                }
                                StringBuilder sb13 = new StringBuilder();
                                sb13.append("Extraction ");
                                sb13.append(z ? "succeeded" : "failed");
                                sb13.append(" '");
                                sb13.append(r3.getAbsolutePath());
                                sb13.append("': length ");
                                sb13.append(r3.length());
                                sb13.append(" - crc: ");
                                sb13.append(r3.crc);
                                Log.i("MultiDex", sb13.toString());
                                if (!z) {
                                    r3.delete();
                                    if (r3.exists()) {
                                        StringBuilder sb14 = new StringBuilder();
                                        sb14.append("Failed to delete corrupted secondary dex '");
                                        sb14.append(r3.getPath());
                                        sb14.append("'");
                                        Log.w("MultiDex", sb14.toString());
                                    }
                                }
                            } else {
                                StringBuilder sb15 = new StringBuilder();
                                sb15.append("Failed to rename \"");
                                sb15.append(createTempFile.getAbsolutePath());
                                sb15.append("\" to \"");
                                sb15.append(r3.getAbsolutePath());
                                sb15.append("\"");
                                throw new IOException(sb15.toString());
                            }
                        } else {
                            StringBuilder sb16 = new StringBuilder();
                            sb16.append("Failed to mark readonly \"");
                            sb16.append(createTempFile.getAbsolutePath());
                            sb16.append("\" (tmp of \"");
                            sb16.append(r3.getAbsolutePath());
                            sb16.append("\")");
                            throw new IOException(sb16.toString());
                        }
                    } catch (Throwable th) {
                        zipOutputStream.close();
                        throw th;
                    }
                }
                if (z) {
                    i++;
                    StringBuilder sb17 = new StringBuilder();
                    sb17.append("classes");
                    sb17.append(i);
                    sb17.append(".dex");
                    entry = zipFile.getEntry(sb17.toString());
                } else {
                    StringBuilder sb18 = new StringBuilder();
                    sb18.append("Could not create zip file ");
                    sb18.append(r3.getAbsolutePath());
                    sb18.append(" for secondary dex (");
                    sb18.append(i);
                    sb18.append(")");
                    throw new IOException(sb18.toString());
                }
            }
            try {
                zipFile.close();
                return arrayList;
            } catch (IOException e2) {
                Log.w("MultiDex", "Failed to close resource", e2);
                return arrayList;
            }
        } catch (Throwable th2) {
            try {
                zipFile.close();
                throw th2;
            } catch (IOException e3) {
                Log.w("MultiDex", "Failed to close resource", e3);
                throw th2;
            }
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:52:0x026e */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r8v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r8v4, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r8v5 */
    /* JADX WARN: Type inference failed for: r8v6 */
    /* JADX WARN: Type inference failed for: r8v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x008b, code lost:
        if (r6.getLong(r3.toString(), -1) != r18) goto L_0x008d;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A03(android.content.Context r23, boolean r24) {
        /*
        // Method dump skipped, instructions count: 657
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass00I.A03(android.content.Context, boolean):java.util.List");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A05.release();
        this.A04.close();
        this.A03.close();
    }
}
