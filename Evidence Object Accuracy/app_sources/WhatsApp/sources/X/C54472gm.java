package X;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.stickers.RemoveStickerFromFavoritesDialogFragment;
import com.whatsapp.stickers.StarStickerFromPickerDialogFragment;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;
import com.whatsapp.stickers.StickerView;
import java.util.List;
import java.util.Set;

/* renamed from: X.2gm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54472gm extends AnonymousClass02M {
    public int A00;
    public int A01;
    public View A02;
    public RecyclerView A03;
    public C91014Qc A04;
    public AnonymousClass4LF A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final AbstractC05270Ox A09 = new C75073jG(this);
    public final AnonymousClass1AB A0A;
    public final StickerView A0B;
    public final C38721ob A0C;
    public final boolean A0D;
    public final boolean A0E;

    public C54472gm(AnonymousClass1AB r3, StickerView stickerView, C38721ob r5, int i, int i2, boolean z, boolean z2) {
        this.A0A = r3;
        this.A0C = r5;
        this.A07 = i;
        this.A06 = i2;
        this.A00 = 0;
        this.A08 = R.drawable.sticker_store_error;
        this.A0D = z;
        this.A0E = z2;
        this.A0B = stickerView;
        if (stickerView != null) {
            C12960it.A11(stickerView, this, 27);
            stickerView.setOnLongClickListener(new View.OnLongClickListener() { // from class: X.4n2
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    C54472gm r1 = C54472gm.this;
                    return r1.A0H(r1.A01);
                }
            });
        }
    }

    @Override // X.AnonymousClass02M
    public void A0B(RecyclerView recyclerView) {
        this.A03 = recyclerView;
        recyclerView.A0n(this.A09);
    }

    @Override // X.AnonymousClass02M
    public void A0C(RecyclerView recyclerView) {
        recyclerView.A0o(this.A09);
        this.A03 = null;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        List list;
        AnonymousClass1KZ r1 = this.A04.A02;
        if (r1 == null) {
            return 0;
        }
        if (r1.A0O || (r1.A0E == null && !r1.A04.isEmpty())) {
            list = r1.A04;
        } else {
            list = r1.A03;
        }
        int size = list.size();
        int i = this.A00;
        if (i > 0) {
            return Math.min(size, i);
        }
        return size;
    }

    public void A0E() {
        AnonymousClass009.A03(this.A03);
        StickerView stickerView = this.A0B;
        if (stickerView != null && stickerView.getVisibility() == 0) {
            this.A02.setVisibility(0);
            stickerView.setVisibility(4);
            this.A03.setAlpha(1.0f);
            stickerView.A03();
        }
    }

    public final void A0F() {
        RecyclerView recyclerView = this.A03;
        AnonymousClass009.A03(recyclerView);
        StickerView stickerView = this.A0B;
        if (stickerView != null) {
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(recyclerView);
            int i = A0H.leftMargin;
            int i2 = A0H.rightMargin;
            int width = this.A03.getWidth();
            int height = this.A03.getHeight();
            AnonymousClass03U A0C = this.A03.A0C(this.A01);
            if (A0C == null) {
                A0E();
                return;
            }
            View view = A0C.A0H;
            this.A02 = view;
            float x = view.getX() + ((float) i) + (C12990iw.A02(this.A02) / 2.0f);
            float y = this.A02.getY() + (C12990iw.A03(this.A02) / 2.0f);
            float max = Math.max(x - (C12990iw.A02(stickerView) / 2.0f), 0.0f);
            float max2 = Math.max(y - (C12990iw.A03(stickerView) / 2.0f), 0.0f);
            stickerView.setX(max - Math.max(((C12990iw.A02(stickerView) + max) - ((float) width)) - ((float) i2), 0.0f));
            stickerView.setY(max2 - Math.max((C12990iw.A03(stickerView) + max2) - ((float) height), 0.0f));
        }
    }

    public void A0G(AnonymousClass1KS r11, AnonymousClass1KZ r12, int i) {
        RecyclerView recyclerView = this.A03;
        AnonymousClass009.A03(recyclerView);
        AnonymousClass03U A0C = recyclerView.A0C(i);
        if (A0C == null) {
            A0E();
            return;
        }
        View view = A0C.A0H;
        this.A02 = view;
        ImageView A0L = C12970iu.A0L(view, R.id.sticker_preview);
        this.A01 = i;
        A0F();
        StickerView stickerView = this.A0B;
        if (stickerView != null) {
            if (r11 == null || r11.A08 == null || (this.A0E ? !r12.A0M : r12.A01() || !r12.A0K)) {
                stickerView.setImageDrawable(A0L.getDrawable());
            } else {
                this.A0A.A04(stickerView, r11, new AbstractC39661qJ() { // from class: X.59S
                    @Override // X.AbstractC39661qJ
                    public final void AWe(boolean z) {
                        C54472gm.this.A0B.A02();
                    }
                }, 1, stickerView.getWidth(), stickerView.getHeight(), true, false);
            }
            AnonymousClass009.A03(this.A03);
            this.A02.setVisibility(4);
            stickerView.setVisibility(0);
            this.A03.setAlpha(0.2f);
        }
    }

    public final boolean A0H(int i) {
        DialogFragment starStickerFromPickerDialogFragment;
        if (this.A05 == null) {
            return true;
        }
        SparseBooleanArray sparseBooleanArray = this.A04.A01;
        if (sparseBooleanArray != null && sparseBooleanArray.get(i)) {
            return true;
        }
        StickerView stickerView = this.A0B;
        if (stickerView != null && stickerView.getVisibility() == 0 && this.A01 != i) {
            return true;
        }
        AnonymousClass1KZ r2 = this.A04.A02;
        AnonymousClass009.A05(r2);
        if (r2.A04.size() <= i || i < 0) {
            return false;
        }
        AnonymousClass4LF r1 = this.A05;
        AnonymousClass1KS r4 = (AnonymousClass1KS) r2.A04.get(i);
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = r1.A00;
        Set set = stickerStorePackPreviewActivity.A0U;
        if (set == null) {
            return true;
        }
        if (set.contains(r4.A0C)) {
            starStickerFromPickerDialogFragment = new RemoveStickerFromFavoritesDialogFragment();
            Bundle A0D = C12970iu.A0D();
            A0D.putParcelable("sticker", r4);
            starStickerFromPickerDialogFragment.A0U(A0D);
        } else {
            starStickerFromPickerDialogFragment = new StarStickerFromPickerDialogFragment();
            Bundle A0D2 = C12970iu.A0D();
            A0D2.putParcelable("sticker", r4);
            starStickerFromPickerDialogFragment.A0U(A0D2);
        }
        stickerStorePackPreviewActivity.Adm(starStickerFromPickerDialogFragment);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r1.get(r14) == false) goto L_0x0018;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r13, int r14) {
        /*
            r12 = this;
            X.3ju r13 = (X.C75473ju) r13
            android.widget.ImageView r4 = r13.A01
            int r0 = r12.A08
            r4.setImageResource(r0)
            X.4Qc r1 = r12.A04
            X.1KZ r0 = r1.A02
            android.util.SparseBooleanArray r1 = r1.A01
            if (r1 == 0) goto L_0x0018
            boolean r1 = r1.get(r14)
            r3 = 1
            if (r1 != 0) goto L_0x0019
        L_0x0018:
            r3 = 0
        L_0x0019:
            android.view.View r2 = r13.A00
            int r1 = X.C12990iw.A0A(r3)
            r2.setVisibility(r1)
            r1 = 1065353216(0x3f800000, float:1.0)
            if (r3 == 0) goto L_0x0028
            r1 = 1056964608(0x3f000000, float:0.5)
        L_0x0028:
            r4.setAlpha(r1)
            if (r0 == 0) goto L_0x0086
            java.util.List r1 = r0.A04
            int r1 = r1.size()
            if (r1 <= r14) goto L_0x00a5
            java.util.List r1 = r0.A04
            java.lang.Object r5 = r1.get(r14)
            X.1KS r5 = (X.AnonymousClass1KS) r5
        L_0x003d:
            boolean r1 = r0.A0O
            if (r1 != 0) goto L_0x005d
            java.lang.String r1 = r0.A0E
            if (r1 != 0) goto L_0x004d
            java.util.List r1 = r0.A04
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x005d
        L_0x004d:
            if (r5 == 0) goto L_0x0087
            java.lang.String r1 = r5.A08
            if (r1 == 0) goto L_0x0087
            boolean r1 = r12.A0E
            if (r1 != 0) goto L_0x005d
            boolean r1 = r0.A01()
            if (r1 != 0) goto L_0x0087
        L_0x005d:
            X.1AB r3 = r12.A0A
            X.AnonymousClass009.A05(r5)
            int r8 = r12.A07
            X.59U r6 = new X.59U
            r6.<init>(r14)
            r10 = 0
            r7 = 1
            r11 = 0
            r9 = r8
            r3.A04(r4, r5, r6, r7, r8, r9, r10, r11)
        L_0x0070:
            boolean r1 = r12.A0D
            if (r1 == 0) goto L_0x0086
            android.view.View r2 = r13.A0H
            X.3Mz r1 = new X.3Mz
            r1.<init>(r0, r12, r14)
            r2.setOnLongClickListener(r1)
            X.2jo r1 = new X.2jo
            r1.<init>(r5, r0, r12, r14)
            r2.setOnClickListener(r1)
        L_0x0086:
            return
        L_0x0087:
            X.1ob r6 = r12.A0C
            java.lang.String r1 = "https://static.whatsapp.net/sticker?img="
            java.lang.StringBuilder r2 = X.C12960it.A0k(r1)
            java.util.List r1 = r0.A03
            java.lang.String r1 = X.C12960it.A0g(r1, r14)
            java.lang.String r11 = X.C12960it.A0d(r1, r2)
            X.59i r10 = new X.59i
            r10.<init>(r12, r14)
            r7 = 0
            r8 = r7
            r9 = r4
            r6.A00(r7, r8, r9, r10, r11)
            goto L_0x0070
        L_0x00a5:
            r5 = 0
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54472gm.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C75473ju r3 = new C75473ju(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.sticker_store_preview));
        ImageView imageView = r3.A01;
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        int i2 = this.A07;
        layoutParams.height = i2;
        layoutParams.width = i2;
        imageView.setLayoutParams(layoutParams);
        int i3 = this.A06;
        imageView.setPadding(i3, i3, i3, i3);
        return r3;
    }
}
