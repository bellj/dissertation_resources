package X;

import java.io.IOException;
import java.util.Arrays;

/* renamed from: X.5NB  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5NB extends AnonymousClass1TL {
    public final int A00;
    public final boolean A01;
    public final byte[] A02;

    public AnonymousClass5NB(byte[] bArr, int i, boolean z) {
        this.A01 = z;
        this.A00 = i;
        this.A02 = AnonymousClass1TT.A02(bArr);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int A01 = AnonymousClass1TQ.A01(this.A00);
        int length = this.A02.length;
        return A01 + AnonymousClass1TQ.A00(length) + length;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return this.A01;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        boolean z = this.A01;
        return ((z ? 1 : 0) ^ this.A00) ^ AnonymousClass1TT.A00(this.A02);
    }

    public static AnonymousClass5NB A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NB)) {
            return (AnonymousClass5NB) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return A00(AnonymousClass1TL.A03((byte[]) obj));
            } catch (IOException e) {
                throw C12970iu.A0f(C12960it.A0d(e.getMessage(), C12960it.A0k("Failed to construct object from byte[]: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("unknown object in getInstance: ")));
        }
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r4) {
        if (!(r4 instanceof AnonymousClass5NB)) {
            return false;
        }
        AnonymousClass5NB r42 = (AnonymousClass5NB) r4;
        return this.A01 == r42.A01 && this.A00 == r42.A00 && Arrays.equals(this.A02, r42.A02);
    }

    public String toString() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        if (this.A01) {
            stringBuffer.append("CONSTRUCTED ");
        }
        stringBuffer.append("APPLICATION ");
        stringBuffer.append(Integer.toString(this.A00));
        stringBuffer.append("]");
        byte[] bArr = this.A02;
        if (bArr != null) {
            stringBuffer.append(" #");
            str = C72463ee.A0I(bArr, bArr.length);
        } else {
            str = " #null";
        }
        stringBuffer.append(str);
        stringBuffer.append(" ");
        return stringBuffer.toString();
    }
}
