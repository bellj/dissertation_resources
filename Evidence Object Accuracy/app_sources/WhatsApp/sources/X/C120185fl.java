package X;

import android.content.Context;

/* renamed from: X.5fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120185fl extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass1FK A00;
    public final /* synthetic */ C129965ya A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120185fl(Context context, C14900mE r2, AnonymousClass1FK r3, C18650sn r4, C129965ya r5) {
        super(context, r2, r4);
        this.A01 = r5;
        this.A00 = r3;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        AbstractC16870pt A0U = C117305Zk.A0U(this.A01.A08);
        if (A0U != null) {
            A0U.reset();
        }
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AV3(r2);
        }
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        AbstractC16870pt A0U = C117305Zk.A0U(this.A01.A08);
        if (A0U != null) {
            A0U.reset();
        }
        AnonymousClass1FK r0 = this.A00;
        if (r0 != null) {
            r0.AVA(r2);
        }
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r5) {
        C129965ya r0 = this.A01;
        C12960it.A1E(new AnonymousClass5oQ(r0.A03, new Runnable() { // from class: X.6FT
            @Override // java.lang.Runnable
            public final void run() {
                C129965ya r2 = C120185fl.this.A01;
                C18600si r52 = r2.A05;
                long A0E = C12980iv.A0E(r52.A01(), "payments_enabled_till");
                if (!(r2 instanceof C120905gw)) {
                    r2.A07.A01(false, false);
                } else {
                    r2.A07.A01(true, false);
                }
                r52.A0D(A0E);
            }
        }), r0.A0A);
        AnonymousClass1FK r1 = this.A00;
        if (r1 != null) {
            r1.AVB(new AnonymousClass46N());
        }
    }
}
