package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.concurrent.Executor;

/* renamed from: X.515  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass515 implements AbstractC13670k8 {
    public AbstractC13640k3 A00;
    public final Object A01 = C12970iu.A0l();
    public final Executor A02;

    public AnonymousClass515(AbstractC13640k3 r2, Executor executor) {
        this.A02 = executor;
        this.A00 = r2;
    }

    @Override // X.AbstractC13670k8
    public final void Agv(C13600jz r4) {
        if (r4.A0A()) {
            synchronized (this.A01) {
                if (this.A00 != null) {
                    this.A02.execute(new RunnableBRunnable0Shape10S0200000_I1(r4, 24, this));
                }
            }
        }
    }
}
