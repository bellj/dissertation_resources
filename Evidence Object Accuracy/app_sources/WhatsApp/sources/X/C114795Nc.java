package X;

/* renamed from: X.5Nc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114795Nc extends AnonymousClass5NV {
    public C114795Nc() {
    }

    public C114795Nc(AnonymousClass1TN r1) {
        super(r1);
    }

    public C114795Nc(C94954co r2) {
        super(r2, false);
    }

    public C114795Nc(AnonymousClass1TN[] r2) {
        super(r2, false);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        AnonymousClass1TN[] r4 = this.A01;
        int length = r4.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i += C72463ee.A0N(r4, i2).A05();
        }
        return i + 2 + 2;
    }
}
