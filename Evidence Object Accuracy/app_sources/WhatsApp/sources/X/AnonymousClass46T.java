package X;

import java.util.Map;

/* renamed from: X.46T  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass46T extends AnonymousClass4VP {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final Map A05;
    public final Map A06;

    public AnonymousClass46T(String str, String str2, String str3, String str4, String str5, String str6, String str7, Map map, Map map2) {
        super(str, str6);
        this.A03 = str2;
        this.A00 = str3;
        this.A05 = map;
        this.A06 = map2;
        this.A04 = str4;
        this.A02 = str5;
        this.A01 = str7;
    }

    public static Object A00(Object obj, String str) {
        C16700pc.A0E(str, 1);
        C89104Ir r1 = new C89104Ir();
        C95094d8.A03(obj, "json object can not be null");
        return new C93934az(r1.A00, obj).A01(str, new AnonymousClass5T6[0]);
    }

    public final Map A01(Map map) {
        Object A00;
        if (map == null) {
            map = C71373cp.A00;
        }
        Object A002 = A00(map, this.A00);
        if (A002 != null) {
            Map map2 = (Map) A002;
            Map map3 = this.A05;
            C16700pc.A0E(map2, 0);
            AnonymousClass4O5 r0 = new AnonymousClass4O5(map2);
            if (map3 == null) {
                A00 = r0.A01;
            } else {
                A00 = C95334dX.A00(r0, map3);
                if (A00 == null) {
                    throw C12980iv.A0n("null cannot be cast to non-null type kotlin.Any");
                }
            }
            return (Map) A00;
        }
        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
    }

    public final Map A02(Map map, Map map2) {
        Object A00;
        if (map != null) {
            String str = this.A04;
            if (map2 == null) {
                map2 = C71373cp.A00;
            }
            Map map3 = this.A06;
            AnonymousClass4O5 r0 = new AnonymousClass4O5(map2);
            if (map3 == null) {
                A00 = r0.A01;
            } else {
                A00 = C95334dX.A00(r0, map3);
                if (A00 == null) {
                    throw C12980iv.A0n("null cannot be cast to non-null type kotlin.Any");
                }
            }
            Object A01 = C95334dX.A01(A00, str, map);
            if (A01 != null) {
                return (Map) A00(A01, this.A02);
            }
            throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
        }
        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.collections.Map<kotlin.String, kotlin.Any?>");
    }
}
