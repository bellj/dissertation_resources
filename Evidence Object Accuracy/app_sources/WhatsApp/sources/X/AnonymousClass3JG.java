package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.search.views.itemviews.AudioPlayerView;
import java.io.File;

/* renamed from: X.3JG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JG {
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0056, code lost:
        if (r1.A0F == null) goto L_0x0058;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(android.view.View.OnClickListener r4, android.view.View.OnClickListener r5, android.view.View.OnClickListener r6, android.view.View.OnClickListener r7, X.C30421Xi r8, X.AbstractC116155Ui r9, com.whatsapp.search.views.itemviews.AudioPlayerView r10) {
        /*
            X.0oX r1 = X.AbstractC15340mz.A00(r8)
            boolean r0 = X.C30041Vv.A12(r8)
            r3 = 0
            if (r0 == 0) goto L_0x001a
            r0 = 4
            r10.setPlayButtonState(r0)
            r10.setOnControlButtonClickListener(r4)
            r10.setSeekbarProgress(r3)
            r0 = 1
            r9.AWP(r0)
            return
        L_0x001a:
            boolean r0 = X.C30041Vv.A13(r8)
            if (r0 == 0) goto L_0x0049
            java.lang.String r0 = r8.A16()
            boolean r0 = X.AnonymousClass1US.A0C(r0)
            if (r0 == 0) goto L_0x0034
            java.io.File r0 = r1.A0F
            if (r0 == 0) goto L_0x0034
            java.lang.String r0 = r0.getName()
            r8.A07 = r0
        L_0x0034:
            android.content.Context r1 = r10.getContext()
            r0 = 2131100485(0x7f060345, float:1.7813353E38)
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            r10.setSeekbarColor(r0)
            r10.setOnControlButtonClickListener(r7)
        L_0x0045:
            r9.AWP(r3)
            return
        L_0x0049:
            X.0oX r1 = r8.A02
            X.1IS r0 = r8.A0z
            boolean r0 = r0.A02
            if (r0 == 0) goto L_0x0058
            if (r1 == 0) goto L_0x0058
            java.io.File r1 = r1.A0F
            r0 = 1
            if (r1 != 0) goto L_0x0059
        L_0x0058:
            r0 = 0
        L_0x0059:
            r2 = 2
            r3 = 3
            if (r0 == 0) goto L_0x0072
            r10.setPlayButtonState(r2)
            r10.setOnControlButtonClickListener(r5)
            android.content.Context r1 = r10.getContext()
            r0 = 2131100485(0x7f060345, float:1.7813353E38)
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            r10.setSeekbarColor(r0)
            goto L_0x0045
        L_0x0072:
            r10.setPlayButtonState(r3)
            r10.setOnControlButtonClickListener(r6)
            android.content.Context r1 = r10.getContext()
            r0 = 2131100485(0x7f060345, float:1.7813353E38)
            int r0 = X.AnonymousClass00T.A00(r1, r0)
            r10.setSeekbarColor(r0)
            r9.AWP(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JG.A00(android.view.View$OnClickListener, android.view.View$OnClickListener, android.view.View$OnClickListener, android.view.View$OnClickListener, X.1Xi, X.5Ui, com.whatsapp.search.views.itemviews.AudioPlayerView):void");
    }

    public static void A01(AnonymousClass2MF r4, AnonymousClass11P r5, AnonymousClass018 r6, C14850m9 r7, C30421Xi r8, AbstractC116135Ug r9, AudioPlayerView audioPlayerView) {
        if (!r5.A0D(r8) || (r7.A07(931) && r5.A0C())) {
            A02(r6, r8, r9, audioPlayerView);
            return;
        }
        C35191hP A00 = r5.A00();
        if (A00 != null) {
            if (!A00.A0I()) {
                A02(r6, r8, r9, audioPlayerView);
            } else {
                audioPlayerView.setPlayButtonState(1);
                r9.ATp(0, C38131nZ.A04(r6, (long) (A00.A02() / 1000)));
            }
            audioPlayerView.setSeekbarContentDescription((long) A00.A02());
            A00.A0K = r4;
        }
    }

    public static void A02(AnonymousClass018 r3, C30421Xi r4, AbstractC116135Ug r5, AudioPlayerView audioPlayerView) {
        int intValue;
        Number number = (Number) C35191hP.A0y.get(r4.A0z);
        if (number == null) {
            intValue = 0;
        } else {
            intValue = number.intValue();
        }
        Integer valueOf = Integer.valueOf(intValue);
        audioPlayerView.setPlayButtonState(0);
        audioPlayerView.setSeekbarMax(((AbstractC16130oV) r4).A00 * 1000);
        int intValue2 = valueOf.intValue();
        audioPlayerView.setSeekbarProgress(intValue2);
        audioPlayerView.setSeekbarContentDescription((long) intValue2);
        r5.ATp(1, C38131nZ.A04(r3, (long) ((AbstractC16130oV) r4).A00));
    }

    public static boolean A03(Context context, C14900mE r6, C30421Xi r7, AbstractC116145Uh r8, AnonymousClass1CY r9, AnonymousClass19O r10) {
        int i;
        File file;
        C16150oX r0;
        C16150oX A00 = AbstractC15340mz.A00(r7);
        AnonymousClass009.A05(A00);
        if (A00.A0a) {
            i = 0;
        } else {
            i = 1;
            if (A00.A07 != 1) {
                if (A00.A0P && (file = A00.A0F) != null) {
                    File A0h = C12980iv.A0h(file);
                    if (!A0h.exists() || !A0h.canRead()) {
                        i = 2;
                    }
                }
                i = 3;
            }
        }
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    File A0h2 = C12980iv.A0h(A00.A0F);
                    if (!A0h2.exists() || !A0h2.canRead()) {
                        if (r7.A0z.A02 || (r0 = ((AbstractC16130oV) r7).A02) == null) {
                            ActivityC13810kN r02 = (ActivityC13810kN) AbstractC35731ia.A01(context, ActivityC13810kN.class);
                            if (r02 != null) {
                                r9.A01(r02);
                                return false;
                            }
                        } else {
                            r0.A0X = true;
                            r10.A0D(r7);
                            r8.AQU();
                            return false;
                        }
                    }
                } else if (i != 3) {
                    throw C12960it.A0U(C12960it.A0W(i, "Please add a case for a new playability state: "));
                }
                return true;
            }
            r6.A05(R.string.gallery_unsafe_audio_removed, 1);
        }
        return false;
    }
}
