package X;

/* renamed from: X.3kb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75873kb extends AbstractC08840bw {
    public C88924Hz A00;

    public C75873kb(C88924Hz r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC08840bw
    public synchronized int A00() {
        return A01() ? 0 : this.A00.A00.getSizeInBytes();
    }

    @Override // X.AbstractC08840bw
    public synchronized boolean A01() {
        return C12980iv.A1X(this.A00);
    }

    @Override // X.AbstractC08840bw, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        synchronized (this) {
            C88924Hz r1 = this.A00;
            if (r1 != null) {
                this.A00 = null;
                synchronized (r1) {
                }
            }
        }
    }
}
