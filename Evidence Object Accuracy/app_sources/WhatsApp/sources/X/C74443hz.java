package X;

import android.app.Application;

/* renamed from: X.3hz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74443hz extends AnonymousClass014 {
    public final AnonymousClass016 A00;
    public final AnonymousClass3EX A01;

    public C74443hz(Application application, AnonymousClass3EX r3) {
        super(application);
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A00 = A0T;
        this.A01 = r3;
        r3.A00 = A0T;
    }
}
