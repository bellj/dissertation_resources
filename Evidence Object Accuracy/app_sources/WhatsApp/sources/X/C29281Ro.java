package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1Ro  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29281Ro {
    public final NativeHolder A00;

    public C29281Ro(NativeHolder nativeHolder) {
        this.A00 = nativeHolder;
    }

    public C29281Ro(byte[] bArr, long j) {
        JniBridge.getInstance();
        this.A00 = new C29281Ro((NativeHolder) JniBridge.jvidispatchOIO(19, j, bArr)).A00;
    }
}
