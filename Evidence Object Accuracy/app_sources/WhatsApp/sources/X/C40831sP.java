package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.1sP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40831sP extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40831sP A0L;
    public static volatile AnonymousClass255 A0M;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;
    public AbstractC27881Jp A09;
    public AbstractC27881Jp A0A;
    public AbstractC27881Jp A0B;
    public C43261wh A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G = "";
    public String A0H;
    public String A0I = "";
    public String A0J = "";
    public boolean A0K;

    static {
        C40831sP r0 = new C40831sP();
        A0L = r0;
        r0.A0W();
    }

    public C40831sP() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A07 = r0;
        this.A09 = r0;
        this.A0F = "";
        this.A06 = r0;
        this.A0E = "";
        this.A0H = "";
        this.A0B = r0;
        this.A0A = r0;
        this.A08 = r0;
        this.A0D = "";
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r16, Object obj, Object obj2) {
        C81603uH r2;
        switch (r16.ordinal()) {
            case 0:
                return A0L;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C40831sP r1 = (C40831sP) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                String str = this.A0J;
                int i2 = r1.A00;
                boolean z2 = true;
                if ((i2 & 1) != 1) {
                    z2 = false;
                }
                this.A0J = r8.Afy(str, r1.A0J, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A0G;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A0G = r8.Afy(str2, r1.A0G, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                String str3 = this.A0I;
                boolean z6 = false;
                if ((i2 & 4) == 4) {
                    z6 = true;
                }
                this.A0I = r8.Afy(str3, r1.A0I, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                AbstractC27881Jp r3 = this.A07;
                boolean z8 = false;
                if ((i2 & 8) == 8) {
                    z8 = true;
                }
                this.A07 = r8.Afm(r3, r1.A07, z7, z8);
                int i3 = this.A00;
                boolean z9 = false;
                if ((i3 & 16) == 16) {
                    z9 = true;
                }
                long j = this.A04;
                int i4 = r1.A00;
                boolean z10 = false;
                if ((i4 & 16) == 16) {
                    z10 = true;
                }
                this.A04 = r8.Afs(j, r1.A04, z9, z10);
                boolean z11 = false;
                if ((i3 & 32) == 32) {
                    z11 = true;
                }
                int i5 = this.A01;
                boolean z12 = false;
                if ((i4 & 32) == 32) {
                    z12 = true;
                }
                this.A01 = r8.Afp(i5, r1.A01, z11, z12);
                boolean z13 = false;
                if ((i3 & 64) == 64) {
                    z13 = true;
                }
                AbstractC27881Jp r32 = this.A09;
                boolean z14 = false;
                if ((i4 & 64) == 64) {
                    z14 = true;
                }
                this.A09 = r8.Afm(r32, r1.A09, z13, z14);
                int i6 = this.A00;
                boolean z15 = false;
                if ((i6 & 128) == 128) {
                    z15 = true;
                }
                String str4 = this.A0F;
                int i7 = r1.A00;
                boolean z16 = false;
                if ((i7 & 128) == 128) {
                    z16 = true;
                }
                this.A0F = r8.Afy(str4, r1.A0F, z15, z16);
                boolean z17 = false;
                if ((i6 & 256) == 256) {
                    z17 = true;
                }
                AbstractC27881Jp r33 = this.A06;
                boolean z18 = false;
                if ((i7 & 256) == 256) {
                    z18 = true;
                }
                this.A06 = r8.Afm(r33, r1.A06, z17, z18);
                int i8 = this.A00;
                boolean z19 = false;
                if ((i8 & 512) == 512) {
                    z19 = true;
                }
                String str5 = this.A0E;
                int i9 = r1.A00;
                boolean z20 = false;
                if ((i9 & 512) == 512) {
                    z20 = true;
                }
                this.A0E = r8.Afy(str5, r1.A0E, z19, z20);
                boolean z21 = false;
                if ((i8 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z21 = true;
                }
                long j2 = this.A05;
                boolean z22 = false;
                if ((i9 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z22 = true;
                }
                this.A05 = r8.Afs(j2, r1.A05, z21, z22);
                boolean z23 = false;
                if ((i8 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z23 = true;
                }
                boolean z24 = this.A0K;
                boolean z25 = false;
                if ((i9 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z25 = true;
                }
                this.A0K = r8.Afl(z23, z24, z25, r1.A0K);
                boolean z26 = false;
                if ((i8 & 4096) == 4096) {
                    z26 = true;
                }
                String str6 = this.A0H;
                boolean z27 = false;
                if ((i9 & 4096) == 4096) {
                    z27 = true;
                }
                this.A0H = r8.Afy(str6, r1.A0H, z26, z27);
                boolean z28 = false;
                if ((i8 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z28 = true;
                }
                AbstractC27881Jp r34 = this.A0B;
                boolean z29 = false;
                if ((i9 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z29 = true;
                }
                this.A0B = r8.Afm(r34, r1.A0B, z28, z29);
                boolean z30 = false;
                if ((this.A00 & 16384) == 16384) {
                    z30 = true;
                }
                AbstractC27881Jp r4 = this.A0A;
                boolean z31 = false;
                if ((r1.A00 & 16384) == 16384) {
                    z31 = true;
                }
                this.A0A = r8.Afm(r4, r1.A0A, z30, z31);
                boolean z32 = false;
                if ((this.A00 & 32768) == 32768) {
                    z32 = true;
                }
                AbstractC27881Jp r42 = this.A08;
                boolean z33 = false;
                if ((r1.A00 & 32768) == 32768) {
                    z33 = true;
                }
                this.A08 = r8.Afm(r42, r1.A08, z32, z33);
                this.A0C = (C43261wh) r8.Aft(this.A0C, r1.A0C);
                int i10 = this.A00;
                boolean z34 = false;
                if ((i10 & C25981Bo.A0F) == 131072) {
                    z34 = true;
                }
                int i11 = this.A02;
                int i12 = r1.A00;
                boolean z35 = false;
                if ((i12 & C25981Bo.A0F) == 131072) {
                    z35 = true;
                }
                this.A02 = r8.Afp(i11, r1.A02, z34, z35);
                boolean z36 = false;
                if ((i10 & 262144) == 262144) {
                    z36 = true;
                }
                int i13 = this.A03;
                boolean z37 = false;
                if ((i12 & 262144) == 262144) {
                    z37 = true;
                }
                this.A03 = r8.Afp(i13, r1.A03, z36, z37);
                boolean z38 = false;
                if ((i10 & 524288) == 524288) {
                    z38 = true;
                }
                String str7 = this.A0D;
                boolean z39 = false;
                if ((i12 & 524288) == 524288) {
                    z39 = true;
                }
                this.A0D = r8.Afy(str7, r1.A0D, z38, z39);
                if (r8 == C463025i.A00) {
                    this.A00 = i10 | i12;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                String A0A = r82.A0A();
                                this.A00 = 1 | this.A00;
                                this.A0J = A0A;
                                break;
                            case 18:
                                String A0A2 = r82.A0A();
                                this.A00 |= 2;
                                this.A0G = A0A2;
                                break;
                            case 26:
                                String A0A3 = r82.A0A();
                                this.A00 |= 4;
                                this.A0I = A0A3;
                                break;
                            case 34:
                                this.A00 |= 8;
                                this.A07 = r82.A08();
                                break;
                            case 40:
                                this.A00 |= 16;
                                this.A04 = r82.A06();
                                break;
                            case 48:
                                this.A00 |= 32;
                                this.A01 = r82.A02();
                                break;
                            case 58:
                                this.A00 |= 64;
                                this.A09 = r82.A08();
                                break;
                            case 66:
                                String A0A4 = r82.A0A();
                                this.A00 |= 128;
                                this.A0F = A0A4;
                                break;
                            case 74:
                                this.A00 |= 256;
                                this.A06 = r82.A08();
                                break;
                            case 82:
                                String A0A5 = r82.A0A();
                                this.A00 |= 512;
                                this.A0E = A0A5;
                                break;
                            case 88:
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A05 = r82.A06();
                                break;
                            case 96:
                                this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                this.A0K = r82.A0F();
                                break;
                            case 106:
                                String A0A6 = r82.A0A();
                                this.A00 |= 4096;
                                this.A0H = A0A6;
                                break;
                            case 114:
                                this.A00 |= DefaultCrypto.BUFFER_SIZE;
                                this.A0B = r82.A08();
                                break;
                            case 122:
                                this.A00 |= 16384;
                                this.A0A = r82.A08();
                                break;
                            case 130:
                                this.A00 |= 32768;
                                this.A08 = r82.A08();
                                break;
                            case 138:
                                if ((this.A00 & 65536) == 65536) {
                                    r2 = (C81603uH) this.A0C.A0T();
                                } else {
                                    r2 = null;
                                }
                                C43261wh r0 = (C43261wh) r82.A09(r12, C43261wh.A0O.A0U());
                                this.A0C = r0;
                                if (r2 != null) {
                                    r2.A04(r0);
                                    this.A0C = (C43261wh) r2.A01();
                                }
                                this.A00 |= 65536;
                                break;
                            case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                                this.A00 |= C25981Bo.A0F;
                                this.A02 = r82.A02();
                                break;
                            case 152:
                                this.A00 |= 262144;
                                this.A03 = r82.A02();
                                break;
                            case 162:
                                String A0A7 = r82.A0A();
                                this.A00 |= 524288;
                                this.A0D = A0A7;
                                break;
                            default:
                                if (A0a(r82, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r13 = new C28971Pt(e2.getMessage());
                        r13.unfinishedMessage = this;
                        throw new RuntimeException(r13);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C40831sP();
            case 5:
                return new C81873ui();
            case 6:
                break;
            case 7:
                if (A0M == null) {
                    synchronized (C40831sP.class) {
                        if (A0M == null) {
                            A0M = new AnonymousClass255(A0L);
                        }
                    }
                }
                return A0M;
            default:
                throw new UnsupportedOperationException();
        }
        return A0L;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A0J);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A0G);
        }
        if ((this.A00 & 4) == 4) {
            i2 += CodedOutputStream.A07(3, this.A0I);
        }
        int i3 = this.A00;
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A09(this.A07, 4);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A06(5, this.A04);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A04(6, this.A01);
        }
        if ((i3 & 64) == 64) {
            i2 += CodedOutputStream.A09(this.A09, 7);
        }
        if ((i3 & 128) == 128) {
            i2 += CodedOutputStream.A07(8, this.A0F);
        }
        int i4 = this.A00;
        if ((i4 & 256) == 256) {
            i2 += CodedOutputStream.A09(this.A06, 9);
        }
        if ((i4 & 512) == 512) {
            i2 += CodedOutputStream.A07(10, this.A0E);
        }
        int i5 = this.A00;
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A05(11, this.A05);
        }
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A00(12);
        }
        if ((i5 & 4096) == 4096) {
            i2 += CodedOutputStream.A07(13, this.A0H);
        }
        int i6 = this.A00;
        if ((i6 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i2 += CodedOutputStream.A09(this.A0B, 14);
        }
        if ((i6 & 16384) == 16384) {
            i2 += CodedOutputStream.A09(this.A0A, 15);
        }
        if ((i6 & 32768) == 32768) {
            i2 += CodedOutputStream.A09(this.A08, 16);
        }
        if ((i6 & 65536) == 65536) {
            C43261wh r0 = this.A0C;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 += CodedOutputStream.A0A(r0, 17);
        }
        int i7 = this.A00;
        if ((i7 & C25981Bo.A0F) == 131072) {
            i2 += CodedOutputStream.A04(18, this.A02);
        }
        if ((i7 & 262144) == 262144) {
            i2 += CodedOutputStream.A04(19, this.A03);
        }
        if ((i7 & 524288) == 524288) {
            i2 += CodedOutputStream.A07(20, this.A0D);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0J);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A0G);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A0I);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A07, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0H(5, this.A04);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0F(6, this.A01);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0K(this.A09, 7);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A0F);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0K(this.A06, 9);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0I(10, this.A0E);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0H(11, this.A05);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0J(12, this.A0K);
        }
        if ((this.A00 & 4096) == 4096) {
            codedOutputStream.A0I(13, this.A0H);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0K(this.A0B, 14);
        }
        if ((this.A00 & 16384) == 16384) {
            codedOutputStream.A0K(this.A0A, 15);
        }
        if ((this.A00 & 32768) == 32768) {
            codedOutputStream.A0K(this.A08, 16);
        }
        if ((this.A00 & 65536) == 65536) {
            C43261wh r0 = this.A0C;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0F(18, this.A02);
        }
        if ((this.A00 & 262144) == 262144) {
            codedOutputStream.A0F(19, this.A03);
        }
        if ((this.A00 & 524288) == 524288) {
            codedOutputStream.A0I(20, this.A0D);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
