package X;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.13O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13O implements AnonymousClass13P, AnonymousClass13Q {
    public final C16240og A00;
    public final C14830m7 A01;
    public final C16590pI A02;
    public final AnonymousClass13N A03;
    public final C16030oK A04;
    public final C17220qS A05;
    public final C22230yk A06;
    public final Object A07 = new Object();
    public final Set A08 = new HashSet();
    public final Set A09 = new HashSet();

    @Override // X.AnonymousClass13P
    public void AUg(C30751Yr r1) {
    }

    @Override // X.AnonymousClass13P
    public void AUh(AbstractC14640lm r1, UserJid userJid) {
    }

    public AnonymousClass13O(C16240og r2, C14830m7 r3, C16590pI r4, AnonymousClass13N r5, C16030oK r6, C17220qS r7, C22230yk r8) {
        this.A02 = r4;
        this.A01 = r3;
        this.A05 = r7;
        this.A00 = r2;
        this.A03 = r5;
        this.A06 = r8;
        this.A04 = r6;
    }

    public void A00(AbstractC14640lm r6, AnonymousClass1P4 r7) {
        synchronized (this.A07) {
            Set set = this.A09;
            set.remove(r6);
            if (set.isEmpty()) {
                C16030oK r1 = this.A04;
                r1.A0c.remove(this);
                r1.A0b.remove(this);
            }
            if (!this.A08.contains(r6)) {
                A02(new AnonymousClass2NK(r6, r7));
            }
            C16030oK r2 = this.A04;
            if (r2.A0f(r6)) {
                Iterator it = set.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (r2.A0f((AbstractC14640lm) it.next())) {
                            break;
                        }
                    } else {
                        Context context = this.A02.A00;
                        AnonymousClass1Tv.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.ACTION_STOP_LOCATION_UPDATES_FOR_WEB"));
                        break;
                    }
                }
            }
        }
    }

    public void A01(C36261ja r5) {
        if (this.A00.A06) {
            StringBuilder sb = new StringBuilder("sendmethods/sendSubscribeLocations/");
            sb.append(r5.A00);
            sb.append("/");
            sb.append(r5.A01);
            Log.i(sb.toString());
            this.A05.A08(Message.obtain(null, 0, 82, 0, r5), false);
        }
    }

    public void A02(AnonymousClass2NK r5) {
        if (this.A00.A06) {
            StringBuilder sb = new StringBuilder("sendmethods/sendUnsubscribeLocations/");
            sb.append(r5.A00);
            Log.i(sb.toString());
            this.A05.A08(Message.obtain(null, 0, 83, 0, r5), false);
        }
    }

    @Override // X.AnonymousClass13P
    public void AUi(AbstractC14640lm r7, UserJid userJid) {
        synchronized (this.A07) {
            if (this.A09.contains(r7)) {
                C22230yk r1 = this.A06;
                if (r1.A0G.A02() && r7 != null) {
                    r1.A0C.A08(Message.obtain(null, 0, 173, 0, new C49722Mg(r7, userJid)), false);
                }
            }
        }
    }

    @Override // X.AnonymousClass13Q
    public void AWN(AbstractC14640lm r7) {
        synchronized (this.A07) {
            if (this.A09.contains(r7)) {
                Context context = this.A02.A00;
                LocationSharingService.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.ACTION_START_LOCATION_UPDATES_FOR_WEB").putExtra("duration", 42000L));
            }
        }
    }

    @Override // X.AnonymousClass13Q
    public void AWr(AbstractC14640lm r5) {
        synchronized (this.A07) {
            Set set = this.A09;
            if (set.contains(r5)) {
                Iterator it = set.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        Context context = this.A02.A00;
                        AnonymousClass1Tv.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.ACTION_STOP_LOCATION_UPDATES_FOR_WEB"));
                        break;
                    }
                    if (this.A04.A0f((AbstractC14640lm) it.next())) {
                        break;
                    }
                }
            }
        }
    }
}
