package X;

import android.hardware.biometrics.BiometricPrompt;

/* renamed from: X.0OP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OP {
    public BiometricPrompt.AuthenticationCallback A00;
    public AnonymousClass04A A01;
    public final AnonymousClass0PV A02;

    public AnonymousClass0OP(AnonymousClass0PV r1) {
        this.A02 = r1;
    }

    public BiometricPrompt.AuthenticationCallback A00() {
        BiometricPrompt.AuthenticationCallback authenticationCallback = this.A00;
        if (authenticationCallback != null) {
            return authenticationCallback;
        }
        BiometricPrompt.AuthenticationCallback A00 = AnonymousClass0KE.A00(this.A02);
        this.A00 = A00;
        return A00;
    }
}
