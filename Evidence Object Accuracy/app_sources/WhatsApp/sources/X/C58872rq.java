package X;

import java.io.File;

/* renamed from: X.2rq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58872rq extends AbstractC84023yH {
    public int A00 = 0;
    public final /* synthetic */ AnonymousClass10N A01;
    public final /* synthetic */ AnonymousClass3HV A02;
    public final /* synthetic */ C63533By A03;
    public final /* synthetic */ File A04;

    public C58872rq(AnonymousClass10N r2, AnonymousClass3HV r3, C63533By r4, File file) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = file;
    }

    @Override // X.AbstractC84023yH, X.AnonymousClass4UU
    public /* bridge */ /* synthetic */ Object A00(int i) {
        C68123Ue r5 = new C68123Ue(this);
        Boolean bool = null;
        try {
            C63533By r0 = this.A03;
            C44791zY r3 = r0.A08;
            File file = this.A04;
            if (r3.A0C(r5, r0.A07, this.A02, file)) {
                bool = Boolean.TRUE;
                return bool;
            }
        } catch (C84153yV e) {
            int i2 = this.A00;
            if (i2 <= 5) {
                this.A00 = i2 + 1;
            } else {
                throw new C84193yZ(e);
            }
        }
        return bool;
    }
}
