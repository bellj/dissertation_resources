package X;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import com.whatsapp.R;
import com.whatsapp.reactions.ReactionsTrayViewModel;

/* renamed from: X.2dY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53242dY extends PopupWindow {
    public final AnonymousClass1OY A00;
    public final AbstractActivityC13750kH A01;
    public final C53232dO A02;
    public final AnonymousClass018 A03;

    public C53242dY(AnonymousClass1OY r12, AbstractActivityC13750kH r13, AnonymousClass018 r14, ReactionsTrayViewModel reactionsTrayViewModel) {
        int i;
        this.A03 = r14;
        this.A01 = r13;
        this.A00 = r12;
        Context context = r12.getContext();
        AbstractC15340mz fMessage = r12.getFMessage();
        C53232dO r7 = new C53232dO(context, reactionsTrayViewModel);
        this.A02 = r7;
        FrameLayout frameLayout = new FrameLayout(context);
        int dimensionPixelOffset = context.getResources().getDimensionPixelOffset(R.dimen.space_tight);
        int i2 = !((AbstractC28551Oa) r12).A0R ? fMessage.A0z.A02 ? 8388613 : 8388611 : 8388611;
        reactionsTrayViewModel.A01 = C12980iv.A0H(r13).orientation;
        Rect A0J = C12980iv.A0J();
        C12970iu.A0G(r13).getWindowVisibleDisplayFrame(A0J);
        int width = C12970iu.A0G(r13).getWidth();
        boolean z = false;
        if (reactionsTrayViewModel.A01 == 2) {
            i = width - (A0J.right - A0J.left);
        } else {
            i = 0;
        }
        frameLayout.setPadding(dimensionPixelOffset, 0, i + dimensionPixelOffset, 0);
        frameLayout.setClipToPadding(false);
        frameLayout.addView(r7, new FrameLayout.LayoutParams(-2, -2, i2));
        setContentView(frameLayout);
        setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.transparent)));
        setTouchable(true);
        AccessibilityManager A0P = ((ActivityC13810kN) r13).A08.A0P();
        if (A0P != null && A0P.isTouchExplorationEnabled()) {
            z = true;
        }
        setFocusable(z);
        setOutsideTouchable(true);
        setWidth(-1);
        setHeight(-2);
        setInputMethodMode(2);
        setTouchInterceptor(new View.OnTouchListener(frameLayout, this) { // from class: X.3N7
            public final /* synthetic */ FrameLayout A00;
            public final /* synthetic */ C53242dY A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                C53242dY r2 = this.A01;
                FrameLayout frameLayout2 = this.A00;
                if (motionEvent.getAction() != 0) {
                    return false;
                }
                float x = motionEvent.getX();
                C53232dO r3 = r2.A02;
                if (x >= ((float) r3.getLeft()) && motionEvent.getX() <= ((float) r3.getRight()) && motionEvent.getY() >= ((float) frameLayout2.getTop()) && motionEvent.getY() <= ((float) frameLayout2.getBottom())) {
                    return false;
                }
                r2.dismiss();
                return true;
            }
        });
    }
}
