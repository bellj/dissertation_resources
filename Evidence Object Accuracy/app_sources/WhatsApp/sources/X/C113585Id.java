package X;

import java.util.Set;

/* renamed from: X.5Id  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113585Id extends C17630r8 {
    public static final Set A01() {
        return C16900pw.A00;
    }

    public static final Set A02(Set set) {
        int size = set.size();
        if (size == 0) {
            return C16900pw.A00;
        }
        if (size == 1) {
            return C17630r8.A00(set.iterator().next());
        }
        return set;
    }
}
