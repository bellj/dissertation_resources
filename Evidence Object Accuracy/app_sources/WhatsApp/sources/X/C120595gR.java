package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.5gR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120595gR extends C120895gv {
    public final /* synthetic */ C120545gM A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120595gR(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120545gM r11) {
        super(context, r8, r9, r10, "upi-batch");
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r2) {
        super.A02(r2);
        AnonymousClass6MP r0 = this.A00.A00;
        if (r0 != null) {
            r0.ANA(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r2) {
        super.A03(r2);
        AnonymousClass6MP r0 = this.A00.A00;
        if (r0 != null) {
            r0.ANA(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r11) {
        super.A04(r11);
        C120545gM r3 = this.A00;
        AbstractC43531xB AEz = r3.A08.A02().AEz();
        AnonymousClass009.A05(AEz);
        ArrayList AYv = AEz.AYv(r3.A03, r11);
        C1308460e r9 = r3.A05;
        C64513Fv r5 = ((C126705tJ) r3).A00;
        C127295uG A03 = r9.A03(r5, AYv);
        ArrayList arrayList = A03.A01;
        ArrayList arrayList2 = A03.A02;
        C119715ez r6 = A03.A00;
        C118025b9 r2 = r3.A09;
        if (r2 != null) {
            r2.A05.Ab2(new AnonymousClass6HV(r2));
        }
        if (C120545gM.A00(r6, r3.A06, arrayList, arrayList2)) {
            r9.A0A(r6, arrayList, arrayList2);
            r5.A05("upi-get-banks");
            AnonymousClass6MP r1 = r3.A00;
            if (r1 != null) {
                r1.AN9(r6, null, arrayList, arrayList2);
            }
        } else {
            StringBuilder A0k = C12960it.A0k("PAY: received invalid objects from batch: banks: ");
            A0k.append(arrayList);
            A0k.append(" psps: ");
            A0k.append(arrayList2);
            A0k.append(" pspRouting: ");
            A0k.append(r6);
            Log.w(C12960it.A0d(" , try get bank list directly.", A0k));
            r3.A01();
        }
        ArrayList arrayList3 = r5.A05;
        if (!arrayList3.contains("upi-list-keys")) {
            r5.A06("upi-list-keys", 500);
        }
        if (!arrayList3.contains("upi-get-banks")) {
            r5.A06("upi-get-banks", 500);
        }
    }
}
