package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

/* renamed from: X.0mS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15040mS extends C15050mT {
    public boolean A00;

    public AbstractC15040mS(C14160kx r1) {
        super(r1);
    }

    public void A0F() {
        String str;
        if (this instanceof C15070mW) {
            C15070mW r3 = (C15070mW) this;
            r3.A01 = ((C15050mT) r3).A00.A00.getSharedPreferences("com.google.android.gms.analytics.prefs", 0);
        } else if (this instanceof C56622lJ) {
            C56622lJ r2 = (C56622lJ) this;
            r2.A0D("Network initialized. User agent", r2.A01);
        } else if (this instanceof C56582lF) {
            C56582lF r0 = (C56582lF) this;
            synchronized (C56582lF.class) {
                C56582lF.A00 = r0;
            }
        } else if (this instanceof C56602lH) {
            C56602lH r4 = (C56602lH) this;
            try {
                r4.A0H();
                if (((Number) C88904Hw.A0U.A00()).longValue() > 0) {
                    Context context = ((C15050mT) r4).A00.A00;
                    ActivityInfo receiverInfo = context.getPackageManager().getReceiverInfo(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsReceiver"), 0);
                    if (receiverInfo != null && receiverInfo.enabled) {
                        r4.A09("Receiver registered for local dispatch.");
                        r4.A00 = true;
                    }
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        } else if (this instanceof C56592lG) {
        } else {
            if (this instanceof C15030mR) {
                C15030mR r22 = (C15030mR) this;
                C15120mb r02 = r22.A04;
                r02.A0F();
                ((AbstractC15040mS) r02).A00 = true;
                C56622lJ r03 = r22.A06;
                r03.A0F();
                ((AbstractC15040mS) r03).A00 = true;
                C15100mZ r04 = r22.A03;
                r04.A0F();
                ((AbstractC15040mS) r04).A00 = true;
            } else if (this instanceof C56552lC) {
                C56552lC r8 = (C56552lC) this;
                C14160kx r7 = ((C15050mT) r8).A00;
                C14170ky r6 = r7.A03;
                C13020j0.A01(r6);
                if (r6.A05 == null) {
                    synchronized (r6) {
                        if (r6.A05 == null) {
                            C56252ka r9 = new C56252ka();
                            Context context2 = r6.A01;
                            PackageManager packageManager = context2.getPackageManager();
                            String packageName = context2.getPackageName();
                            r9.A02 = packageName;
                            r9.A03 = packageManager.getInstallerPackageName(packageName);
                            String str2 = null;
                            try {
                                PackageInfo packageInfo = packageManager.getPackageInfo(context2.getPackageName(), 0);
                                if (packageInfo != null) {
                                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                                    if (!TextUtils.isEmpty(applicationLabel)) {
                                        packageName = applicationLabel.toString();
                                    }
                                    str2 = packageInfo.versionName;
                                }
                            } catch (PackageManager.NameNotFoundException unused2) {
                                String valueOf = String.valueOf(packageName);
                                if (valueOf.length() != 0) {
                                    str = "Error retrieving package info: appName set to ".concat(valueOf);
                                } else {
                                    str = new String("Error retrieving package info: appName set to ");
                                }
                                Log.e("GAv4", str);
                            }
                            r9.A00 = packageName;
                            r9.A01 = str2;
                            r6.A05 = r9;
                        }
                    }
                }
                C56252ka r05 = r6.A05;
                C56252ka r23 = r8.A00;
                r05.A03(r23);
                C56562lD r1 = r7.A0E;
                C14160kx.A01(r1);
                r1.A0G();
                String str3 = r1.A02;
                if (str3 != null) {
                    r23.A00 = str3;
                }
                r1.A0G();
                String str4 = r1.A01;
                if (str4 != null) {
                    r23.A01 = str4;
                }
            } else if (!(this instanceof C15120mb) && !(this instanceof C15100mZ) && (this instanceof C56572lE)) {
                C15030mR r12 = ((C56572lE) this).A00;
                r12.A0F();
                ((AbstractC15040mS) r12).A00 = true;
            }
        }
    }

    public final void A0G() {
        if (!this.A00) {
            throw new IllegalStateException("Not initialized");
        }
    }
}
