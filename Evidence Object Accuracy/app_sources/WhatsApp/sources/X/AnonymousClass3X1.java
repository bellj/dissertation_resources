package X;

import android.content.ContentUris;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;

/* renamed from: X.3X1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X1 implements AnonymousClass23D {
    public final long A00;
    public final /* synthetic */ C64393Fj A01;

    public AnonymousClass3X1(C64393Fj r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        return Long.toString(this.A00);
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        byte[] bArr = null;
        try {
            C38941ox r4 = new C38941ox();
            try {
                r4.setDataSource(this.A01.A0C.getApplicationContext(), ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, this.A00));
                bArr = r4.getEmbeddedPicture();
                r4.close();
            } catch (Throwable th) {
                try {
                    r4.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (Exception | OutOfMemoryError unused2) {
        }
        if (bArr == null) {
            return C64843Hc.A05;
        }
        try {
            return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
        } catch (OutOfMemoryError unused3) {
            return C64843Hc.A05;
        }
    }
}
