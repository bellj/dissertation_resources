package X;

import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendWebForwardJob;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0yk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22230yk implements AbstractC22240yl, AbstractC22250ym {
    public static final AbstractC33941fK A0I = new AbstractC33941fK() { // from class: X.1fO
        @Override // X.AbstractC33941fK
        public final int AHj(byte b) {
            return b == 1 ? 100 : 1;
        }
    };
    public static final AbstractC33941fK A0J = new AbstractC33941fK() { // from class: X.1fN
        @Override // X.AbstractC33941fK
        public final int AHj(byte b) {
            return b == 3 ? 100 : 1;
        }
    };
    public final AbstractC15710nm A00;
    public final C22930zs A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final C20670w8 A04;
    public final C16240og A05;
    public final C18280sC A06;
    public final AnonymousClass143 A07;
    public final C19780uf A08;
    public final C15600nX A09;
    public final C18470sV A0A;
    public final C244915s A0B;
    public final C17220qS A0C;
    public final C22170ye A0D;
    public final ExecutorC27271Gr A0E;
    public final AbstractC14440lR A0F;
    public final C14890mD A0G;
    public final C14860mA A0H;

    public C22230yk(AbstractC15710nm r4, C22930zs r5, C15570nT r6, C15450nH r7, C20670w8 r8, C16240og r9, C18280sC r10, AnonymousClass143 r11, C19780uf r12, C15600nX r13, C18470sV r14, C244915s r15, C17220qS r16, C22170ye r17, AbstractC14440lR r18, C14890mD r19, C14860mA r20) {
        this.A00 = r4;
        this.A02 = r6;
        this.A0F = r18;
        this.A0G = r19;
        this.A0H = r20;
        this.A03 = r7;
        this.A0A = r14;
        this.A0D = r17;
        this.A04 = r8;
        this.A0C = r16;
        this.A0B = r15;
        this.A05 = r9;
        this.A01 = r5;
        this.A06 = r10;
        this.A08 = r12;
        this.A09 = r13;
        this.A07 = r11;
        this.A0E = new ExecutorC27271Gr(r18, false);
    }

    public void A00(int i, int i2, boolean z) {
        if (this.A0G.A02()) {
            this.A0C.A07(Message.obtain(null, 0, 257, 0, new C34251fp(i, i2, z)), "clear-ga-banner", false);
        }
    }

    public void A01(int i, String str, String str2) {
        if (this.A05.A06 && str != null) {
            this.A0C.A08(Message.obtain(null, 0, 49, 0, new C34131fd(i, str, str2)), false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0022, code lost:
        if (r2 == Integer.MIN_VALUE) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(X.C32441cA r6, boolean r7) {
        /*
            r5 = this;
            X.0og r0 = r5.A05
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x0049
            X.0mD r0 = r5.A0G
            boolean r0 = r0.A02()
            if (r0 == 0) goto L_0x0049
            double r0 = r6.A00()
            boolean r2 = java.lang.Double.isNaN(r0)
            if (r2 != 0) goto L_0x0049
            X.0mA r4 = r5.A0H
            int r3 = (int) r0
            int r2 = r6.A02
            if (r2 == 0) goto L_0x0024
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r0 = 1
            if (r2 != r1) goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            X.1fV r1 = new X.1fV
            r1.<init>(r3, r0, r7)
            java.util.concurrent.atomic.AtomicReference r0 = r4.A0T
            java.lang.Object r0 = r0.getAndSet(r1)
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0049
            X.0qS r4 = r5.A0C
            X.1fW r3 = new X.1fW
            r3.<init>(r1)
            r2 = 0
            r1 = 0
            r0 = 56
            android.os.Message r1 = android.os.Message.obtain(r2, r1, r0, r1, r3)
            r0 = 0
            r4.A08(r1, r0)
        L_0x0049:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22230yk.A02(X.1cA, boolean):void");
    }

    public void A03(C29901Ve r9, boolean z) {
        C14890mD r3 = this.A0G;
        if ((r3.A02() || z) && C15380n4.A0F(r9)) {
            ArrayList arrayList = new ArrayList(new HashSet(this.A09.A02(r9).A06().A00));
            C15570nT r0 = this.A02;
            r0.A08();
            arrayList.remove(r0.A05);
            C34001fQ r2 = new C34001fQ(r9, this, z);
            ((AbstractC34011fR) r2).A00 = r3.A00().A03;
            C14860mA r02 = this.A0H;
            C34021fS r1 = new C34021fS(r2, r02);
            String A02 = r02.A02();
            this.A04.A00(new SendWebForwardJob(Message.obtain(null, 0, 51, 0, new C34041fU(r9, r1, A02, arrayList)), A02, r3.A00().A03));
        }
    }

    public void A04(AbstractC14640lm r8, int i, int i2, long j) {
        if (!C15380n4.A0O(r8)) {
            A0C(new C34081fY(r8, i, j, 0), i2);
        }
    }

    public void A05(AbstractC14640lm r12, Collection collection, int i) {
        C14890mD r3 = this.A0G;
        if (r3.A02() && r12 != null && collection != null && collection.size() != 0) {
            C34111fb r1 = new C34111fb(r12, this, collection, i);
            ((AbstractC34011fR) r1).A00 = r3.A00().A03;
            C14860mA r2 = this.A0H;
            C34021fS r8 = new C34021fS(r1, r2);
            ArrayList arrayList = new ArrayList(collection.size());
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                arrayList.add(((AbstractC15340mz) it.next()).A0z);
            }
            String A02 = r2.A02();
            C20670w8 r4 = this.A04;
            String str = r3.A00().A03;
            C34081fY r7 = new C34081fY(r12, 2);
            r7.A00 = i;
            r4.A00(new SendWebForwardJob(Message.obtain(null, 0, 54, 0, new C34121fc(r12, r7, r8, A02, arrayList)), A02, str));
        }
    }

    public void A06(AbstractC14640lm r8, boolean z) {
        if (r8 != null && !C15380n4.A0O(r8)) {
            C14890mD r3 = this.A0G;
            if (r3.A02()) {
                C34211fl r2 = new C34211fl(r8, this, z);
                ((AbstractC34011fR) r2).A00 = r3.A00().A03;
                C14860mA r0 = this.A0H;
                C34021fS r1 = new C34021fS(r2, r0);
                String A02 = r0.A02();
                this.A04.A00(new SendWebForwardJob(Message.obtain(null, 0, 48, 0, new C34221fm(r8, r1, A02, z)), A02, r3.A00().A03));
            }
        }
    }

    public void A07(Jid jid, String str, String str2, String str3, String str4, long j, boolean z) {
        C14860mA r5;
        C14880mC r0;
        StringBuilder sb = new StringBuilder("app/xmpp/recv/qr_terminate recv: ");
        sb.append(str2);
        sb.append(" local: ");
        C14890mD r6 = this.A0G;
        sb.append(r6.A00().A03);
        sb.append(" clear: ");
        sb.append(z);
        Log.i(sb.toString());
        if (!r6.A02() || !r6.A00().A03.equals(str2)) {
            r5 = this.A0H;
            r5.A0I(str3, j);
        } else {
            Handler handler = this.A01.A00;
            handler.removeMessages(5);
            handler.removeMessages(3);
            handler.removeMessages(4);
            r5 = this.A0H;
            r5.A09 = false;
            r5.A0E();
            r5.A0I(r5.A0N.A00().A00, j);
            r5.A0L(z);
        }
        if (str2 != null) {
            if (!str2.equals(r6.A00().A03) && str3 != null && z && r5.A05().containsKey(str3)) {
                r5.A0K(str3, false);
                r5.A0D();
            }
        } else if (!(str4 == null || str3 == null || !z || (r0 = (C14880mC) r5.A05().get(str3)) == null)) {
            byte[] decode = Base64.decode(r0.A0A, 0);
            byte[] bArr = new byte[32];
            System.arraycopy(decode, 0, bArr, 0, 32);
            byte[] bArr2 = new byte[32];
            System.arraycopy(decode, 32, bArr2, 0, 32);
            byte[] A01 = C33991fP.A01(bArr2, bArr);
            if (A01 != null && Base64.encodeToString(A01, 2).equals(str4)) {
                r5.A0K(str3, false);
                r5.A0D();
            }
        }
        this.A0D.A03(jid, str, "web", 0);
    }

    public void A08(UserJid userJid) {
        if (this.A05.A06 && this.A0G.A02() && userJid != null) {
            this.A0E.execute(new RunnableBRunnable0Shape6S0200000_I0_6(this, 21, userJid));
        }
    }

    public void A09(UserJid userJid, C27081Fy r7, long j) {
        if (this.A0G.A02() && r7 != null && userJid != null) {
            this.A0C.A08(Message.obtain(null, 0, 155, 0, new C34151ff(userJid, r7.A02(), j)), false);
        }
    }

    public void A0A(AbstractC15340mz r12, String str) {
        if (r12 != null && str != null) {
            C14890mD r2 = this.A0G;
            if (r2.A02() && (r12.A0B() instanceof UserJid)) {
                C34181fi r1 = new C34181fi(this, r12, str);
                ((AbstractC34011fR) r1).A00 = r2.A00().A03;
                C14860mA r0 = this.A0H;
                C34021fS r8 = new C34021fS(r1, r0);
                String A02 = r0.A02();
                this.A04.A00(new SendWebForwardJob(Message.obtain(null, 0, 127, 0, new C34231fn((UserJid) r12.A0B(), r12.A0z, r8, A02, str)), A02, r2.A00().A03));
            }
        }
    }

    public void A0B(AnonymousClass1IS r8, int i) {
        C14890mD r3 = this.A0G;
        if (!r3.A02()) {
            return;
        }
        if (i == 0 || i == 5 || i == 13 || i == 7 || i == 8) {
            C34161fg r2 = new C34161fg(this, r8, i);
            ((AbstractC34011fR) r2).A00 = r3.A00().A03;
            C14860mA r0 = this.A0H;
            C34021fS r1 = new C34021fS(r2, r0);
            String A02 = r0.A02();
            this.A04.A00(new SendWebForwardJob(Message.obtain(null, 0, 47, 0, new C34171fh(r8, r1, A02, i)), A02, r3.A00().A03));
            return;
        }
        Log.e("app/xmpp/send/qr_msg_status invalid status");
    }

    public void A0C(C34081fY r3, int i) {
        if (!C15380n4.A0O(r3.A06)) {
            A0E(Integer.valueOf(i), Collections.singletonList(r3));
        }
    }

    public void A0D(C30331Wz r14) {
        C20670w8 r0;
        SendWebForwardJob sendWebForwardJob;
        C14890mD r1 = this.A0G;
        if (r1.A02()) {
            AnonymousClass1IS r8 = r14.A0z;
            AbstractC14640lm r7 = r8.A00;
            if (C15380n4.A0N(r7)) {
                String A00 = C18470sV.A00(this.A0A.A05());
                C34181fi r2 = new C34181fi(this, r14, A00);
                ((AbstractC34011fR) r2).A00 = r1.A00().A03;
                C14860mA r02 = this.A0H;
                C34021fS r9 = new C34021fS(r2, r02);
                String A02 = r02.A02();
                if (r14.A0B() instanceof UserJid) {
                    r0 = this.A04;
                    sendWebForwardJob = new SendWebForwardJob(Message.obtain(null, 0, 154, 0, new C34191fj((UserJid) r14.A0B(), r8, r9, A02, A00, r14.A01)), r8.A01, r1.A00().A03);
                } else {
                    return;
                }
            } else {
                r0 = this.A04;
                String str = r8.A01;
                sendWebForwardJob = new SendWebForwardJob(Message.obtain(null, 0, 128, 0, new C34201fk(r7, r14.A0B(), str, r14.A01, r8.A02)), str, r1.A00().A03);
            }
            r0.A00(sendWebForwardJob);
        }
    }

    public void A0E(Integer num, List list) {
        C14890mD r3 = this.A0G;
        if (r3.A02()) {
            ArrayList arrayList = new ArrayList();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C34081fY r1 = (C34081fY) it.next();
                if (!C15380n4.A0O(r1.A06)) {
                    if (num != null) {
                        r1.A00 = num.intValue();
                    }
                    arrayList.add(r1);
                }
            }
            if (!arrayList.isEmpty()) {
                C34091fZ r2 = new C34091fZ(this, arrayList);
                ((AbstractC34011fR) r2).A00 = r3.A00().A03;
                C14860mA r0 = this.A0H;
                C34021fS r12 = new C34021fS(r2, r0);
                String A02 = r0.A02();
                this.A04.A00(new SendWebForwardJob(Message.obtain(null, 0, 52, 0, new C34101fa(r12, A02, arrayList)), A02, r3.A00().A03));
            }
        }
    }

    public void A0F(String str, int i) {
        if (this.A05.A06 && this.A0G.A02() && str != null) {
            this.A0C.A08(Message.obtain(null, 0, 57, 0, new C34071fX(str, i)), false);
        }
    }

    public void A0G(String str, String str2) {
        if (this.A0G.A02()) {
            AnonymousClass009.A04(str2);
            this.A0C.A08(Message.obtain(null, 0, 199, 0, new C34241fo(str, "delete", str2)), false);
        }
    }

    public void A0H(String str, String str2) {
        if (this.A0G.A02()) {
            AnonymousClass009.A0A("sendWebStickerPacksUpdate should not handle delete event, use sendWebStickerPacksDelete for that", !"delete".equals(str2));
            this.A0C.A08(Message.obtain(null, 0, 199, 0, new C34241fo(str, str2, null)), false);
        }
    }

    public void A0I(boolean z, boolean z2) {
        if (this.A05.A06) {
            C14890mD r4 = this.A0G;
            if (r4.A02()) {
                this.A0C.A08(Message.obtain(null, 0, 44, 0, new C34141fe(z, z2)), false);
                A07(null, null, r4.A00().A03, r4.A00().A00, null, 0, z);
            }
        }
    }

    public boolean A0J(String str) {
        C14860mA r1 = this.A0H;
        Number number = (Number) r1.A06(true).get(str);
        if (number == null) {
            r1.A0H(str, -1);
            return false;
        }
        int intValue = number.intValue();
        if (intValue < 0) {
            return true;
        }
        A0F(str, intValue);
        return true;
    }

    @Override // X.AbstractC22250ym
    public void AMm(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            if (deviceJid != null) {
                A08(deviceJid.getUserJid());
            }
        }
    }

    @Override // X.AbstractC22240yl
    public void ANB(C32441cA r2) {
        A02(r2, this.A07.A06());
    }
}
