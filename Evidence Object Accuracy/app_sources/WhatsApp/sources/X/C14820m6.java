package X;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0m6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14820m6 {
    public static final String A05;
    public final SharedPreferences A00;
    public final C16620pL A01;
    public final Object A02 = new Object();
    public final Object A03 = new Object();
    public final Object A04 = new Object();

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass01V.A07);
        sb.append("_light");
        A05 = sb.toString();
    }

    public C14820m6(C16620pL r2, C16630pM r3) {
        this.A00 = r3.A02(A05);
        this.A01 = r2;
    }

    public static List A00() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass01T("security_notifications", 1));
        arrayList.add(new AnonymousClass01T("input_enter_send", 1));
        arrayList.add(new AnonymousClass01T("interface_font_size", 2));
        arrayList.add(new AnonymousClass01T("settings_language", 2));
        arrayList.add(new AnonymousClass01T("conversation_sound", 1));
        arrayList.add(new AnonymousClass01T("autodownload_wifi_mask", 0));
        arrayList.add(new AnonymousClass01T("autodownload_cellular_mask", 0));
        arrayList.add(new AnonymousClass01T("autodownload_roaming_mask", 0));
        arrayList.add(new AnonymousClass01T("voip_low_data_usage", 1));
        arrayList.add(new AnonymousClass01T("gdrive_backup_filters", 3));
        return arrayList;
    }

    public int A01() {
        try {
            return Integer.parseInt(this.A00.getString("interface_gdrive_backup_frequency", String.valueOf(0)));
        } catch (NumberFormatException e) {
            Log.e("wa-shared-preferences/get-backup-freq", e);
            return 0;
        }
    }

    public int A02() {
        try {
            return Integer.parseInt(this.A00.getString("interface_gdrive_backup_network_setting", String.valueOf(0)));
        } catch (NumberFormatException e) {
            Log.w("wa-shared-preferences/get-backup-network-settings", e);
            return 0;
        }
    }

    public int A03() {
        return this.A00.getInt("gdrive_error_code", 10);
    }

    public int A04() {
        SharedPreferences sharedPreferences = this.A00;
        int i = 1;
        if (Build.VERSION.SDK_INT >= 29) {
            i = -1;
        }
        return sharedPreferences.getInt("night_mode", i);
    }

    public int A05() {
        SharedPreferences sharedPreferences = this.A00;
        int i = sharedPreferences.getInt("reg_attempts_generate_code", 0) + 1;
        sharedPreferences.edit().putInt("reg_attempts_generate_code", i).apply();
        return i;
    }

    public int A06(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        SharedPreferences sharedPreferences = this.A00;
        StringBuilder sb = new StringBuilder("gdrive_old_media_encryption_status:");
        sb.append(str);
        return sharedPreferences.getInt(sb.toString(), 0);
    }

    public long A07(String str) {
        if (str == null) {
            Log.w("wa-shared-preferences/get-backup-timestamp accountName passed is null.");
            return 0;
        }
        SharedPreferences sharedPreferences = this.A00;
        StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_timestamp:");
        sb.append(str);
        return sharedPreferences.getLong(sb.toString(), 0);
    }

    public long A08(String str) {
        SharedPreferences sharedPreferences = this.A00;
        StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_total_size:");
        sb.append(str);
        return sharedPreferences.getLong(sb.toString(), -1);
    }

    public String A09() {
        return this.A00.getString("gdrive_account_name", null);
    }

    public String A0A() {
        SharedPreferences sharedPreferences = this.A00;
        String string = sharedPreferences.getString("perf_device_id", null);
        if (string != null) {
            return string;
        }
        String obj = UUID.randomUUID().toString();
        sharedPreferences.edit().putString("perf_device_id", obj).apply();
        return obj;
    }

    public String A0B() {
        return this.A00.getString("cc", "");
    }

    public String A0C() {
        return this.A00.getString("ph", "");
    }

    public String A0D(String str) {
        SharedPreferences sharedPreferences = this.A00;
        StringBuilder sb = new StringBuilder("dc_user_postcode_");
        sb.append(str);
        return sharedPreferences.getString(sb.toString(), null);
    }

    public Map A0E() {
        List<AnonymousClass01T> A00 = A00();
        HashMap hashMap = new HashMap();
        for (AnonymousClass01T r0 : A00) {
            Object obj = r0.A00;
            AnonymousClass009.A05(obj);
            String str = (String) obj;
            Object obj2 = r0.A01;
            AnonymousClass009.A05(obj2);
            int intValue = ((Number) obj2).intValue();
            SharedPreferences sharedPreferences = this.A00;
            if (sharedPreferences.contains(str)) {
                if (intValue == 0) {
                    hashMap.put(str, Integer.valueOf(sharedPreferences.getInt(str, 0)));
                } else if (intValue == 1) {
                    hashMap.put(str, Boolean.valueOf(sharedPreferences.getBoolean(str, false)));
                } else if (intValue == 2) {
                    String string = sharedPreferences.getString(str, null);
                    if (string != null) {
                        hashMap.put(str, string);
                    }
                } else if (intValue == 3) {
                    Set<String> stringSet = sharedPreferences.getStringSet(str, new HashSet());
                    if (stringSet != null && !stringSet.isEmpty()) {
                        hashMap.put(str, stringSet);
                    }
                } else {
                    StringBuilder sb = new StringBuilder("Unexpected key type: ");
                    sb.append(str);
                    sb.append(" ");
                    sb.append(intValue);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
        return hashMap;
    }

    public void A0F() {
        this.A00.edit().remove("gdrive_already_downloaded_bytes").remove("gdrive_restore_overwrite_local_files").remove("gdrive_restore_start_timestamp").remove("gdrive_media_restore_network_setting").remove("gdrive_approx_media_download_size").apply();
    }

    public void A0G() {
        Log.i("wa-shared-preferences/cleangcmregsettings");
        this.A00.edit().remove("c2dm_reg_id").remove("c2dm_app_vers").remove("saved_gcm_token_server_unreg").apply();
    }

    public void A0H() {
        this.A00.edit().remove("pref_fail_too_many").remove("pref_no_route_sms").remove("pref_no_route_voice").remove("pref_fail_too_many_attempts").remove("pref_fail_too_many_guesses").apply();
    }

    public void A0I() {
        this.A00.edit().remove("forced_language").apply();
    }

    public void A0J() {
        SharedPreferences sharedPreferences = this.A00;
        sharedPreferences.edit().remove("registration_wipe_type").remove("registration_wipe_token").remove("registration_wipe_wait").remove("registration_wipe_expiry").remove("registration_wipe_server_time").apply();
        sharedPreferences.edit().remove("registration_wipe_info_timestamp").apply();
    }

    public void A0K() {
        this.A00.edit().remove("business_activity_report_expiration_timestamp").remove("business_activity_report_size").remove("business_activity_report_name").remove("business_activity_report_url").remove("business_activity_report_direct_url").remove("business_activity_report_media_key").remove("business_activity_report_file_sha").remove("business_activity_report_file_enc_sha").remove("business_activity_report_timestamp").remove("business_activity_report_state").apply();
    }

    public void A0L() {
        this.A00.edit().remove("gdrive_backup_quota_warning_visibility").remove("backup_quota_user_notice_period_end_timestamp").remove("backup_quota_imposed_timestamp").remove("backup_quota_media_cutoff_timestamp").apply();
    }

    public void A0M() {
        this.A00.edit().remove("gdpr_report_expiration_timestamp").remove("gdpr_report_timestamp").remove("gdpr_report_state").apply();
    }

    public void A0N() {
        this.A00.edit().putInt("video_transcode_compliance_v5", Build.VERSION.SDK_INT).apply();
    }

    public void A0O(int i) {
        this.A00.edit().putInt("business_activity_report_state", i).apply();
    }

    public void A0P(int i) {
        this.A00.edit().putInt("education_banner_count", i).apply();
    }

    public void A0Q(int i) {
        this.A00.edit().putInt("external_dir_migration_stage", i).commit();
    }

    public void A0R(int i) {
        this.A00.edit().putInt("gdrive_backup_quota_warning_visibility", i).apply();
    }

    public void A0S(int i) {
        this.A00.edit().putInt("gdrive_error_code", i).apply();
    }

    public void A0T(int i) {
        this.A00.edit().putInt("gdpr_report_state", i).apply();
    }

    public void A0U(int i) {
        StringBuilder sb = new StringBuilder("wa-shared-preferences/getgroupsparams ");
        sb.append(i);
        Log.i(sb.toString());
        this.A00.edit().remove("need_to_get_groups").putInt("get_groups_params", i).apply();
    }

    public void A0V(int i) {
        StringBuilder sb = new StringBuilder("wa-shared-preferences/set-gdrive-state/");
        sb.append(i);
        Log.i(sb.toString());
        this.A00.edit().putInt("gdrive_state", i).apply();
    }

    public void A0W(int i) {
        this.A00.edit().putInt("logins_with_messages", i).apply();
    }

    public void A0X(int i) {
        this.A00.edit().putInt("encrypted_backup_fleet_migration_state", i).apply();
    }

    public void A0Y(long j) {
        StringBuilder sb = new StringBuilder("wa-shared-prefs/save-gdrive-user-prompt-again-timestamp/");
        sb.append(j);
        sb.append(" ");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        sb.append(simpleDateFormat.format(instance.getTime()));
        Log.i(sb.toString());
        A0q("gdrive_next_prompt_for_setup_timestamp", j);
    }

    public void A0Z(String str) {
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("smb_business_direct_connection_public_key_");
        sb.append(str);
        edit.remove(sb.toString()).apply();
    }

    public void A0a(String str) {
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("galaxy_biz_public_key_");
        sb.append(str);
        edit.remove(sb.toString()).apply();
    }

    public void A0b(String str) {
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_timestamp:");
        sb.append(str);
        SharedPreferences.Editor remove = edit.remove(sb.toString());
        StringBuilder sb2 = new StringBuilder("gdrive_last_successful_backup_total_size:");
        sb2.append(str);
        SharedPreferences.Editor remove2 = remove.remove(sb2.toString());
        StringBuilder sb3 = new StringBuilder("gdrive_last_successful_backup_media_size:");
        sb3.append(str);
        SharedPreferences.Editor remove3 = remove2.remove(sb3.toString());
        StringBuilder sb4 = new StringBuilder("gdrive_last_successful_backup_video_size:");
        sb4.append(str);
        SharedPreferences.Editor remove4 = remove3.remove(sb4.toString());
        StringBuilder sb5 = new StringBuilder("gdrive_last_successful_backup_encrypted:");
        sb5.append(str);
        SharedPreferences.Editor remove5 = remove4.remove(sb5.toString());
        StringBuilder sb6 = new StringBuilder("gdrive_backup_quota_warning_visibility:");
        sb6.append(str);
        remove5.remove(sb6.toString()).remove("gdrive_backup_quota_warning_visibility").remove("backup_quota_user_notice_period_end_timestamp").remove("backup_quota_imposed_timestamp").remove("backup_quota_media_cutoff_timestamp").remove("gdrive_restore_start_timestamp").remove("gdrive_backup_start_timestamp").remove("gdrive_next_prompt_for_setup_timestamp").remove("gdrive_error_code").remove("interface_gdrive_backup_frequency").remove("gdrive_already_downloaded_bytes").remove("gdrive_restore_overwrite_local_files").remove("gdrive_media_restore_network_setting").remove("gdrive_old_media_encryption_status").remove("gdrive_old_media_encryption_start_time").remove("gdrive_last_restore_file_is_encrypted").remove("gdrive_successive_backup_failed_count").remove("gdrive_already_uploaded_bytes").remove("gdrive_user_initiated_backup").remove("gdrive_state").remove("gdrive_activity_state").remove("gdrive_activity_msgstore_init_key").remove("interface_gdrive_backup_network_setting").remove("gdrive_include_videos_in_backup").remove("gdrive_estimated_backup_size").remove("gdrive_backup_filters").remove("gdrive_approx_media_download_size").remove("gdrive_account_name").remove("gdrive_setup_user_prompted_count").commit();
    }

    public void A0c(String str) {
        this.A00.edit().putString("pref_autoconf_status", str).apply();
    }

    public void A0d(String str) {
        this.A00.edit().putString("block_list_v2_dhash", str).apply();
    }

    public void A0e(String str) {
        this.A00.edit().putString("forced_language", str).apply();
    }

    public void A0f(String str) {
        this.A00.edit().putString("gdrive_account_name", str).apply();
    }

    public void A0g(String str) {
        this.A00.edit().putString("pref_primary_flash_call_status", str).apply();
    }

    public void A0h(String str) {
        SharedPreferences.Editor remove;
        SharedPreferences.Editor edit = this.A00.edit();
        if (str != null) {
            remove = edit.putString("private_stats_id", str);
        } else {
            remove = edit.remove("private_stats_id");
        }
        remove.apply();
    }

    public void A0i(String str) {
        this.A00.edit().putString("registration_code", str).apply();
    }

    public void A0j(String str) {
        this.A00.edit().putString("pref_secondary_flash_call_status", str).apply();
    }

    public void A0k(String str) {
        A0q(str, System.currentTimeMillis());
    }

    public void A0l(String str, int i) {
        SharedPreferences sharedPreferences = this.A00;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        StringBuilder sb = new StringBuilder("gdrive_old_media_encryption_status:");
        sb.append(str);
        edit.putInt(sb.toString(), i).apply();
        if (i == 1) {
            SharedPreferences.Editor edit2 = sharedPreferences.edit();
            StringBuilder sb2 = new StringBuilder("gdrive_old_media_encryption_start_time:");
            sb2.append(str);
            edit2.putLong(sb2.toString(), System.currentTimeMillis()).apply();
        }
    }

    public void A0m(String str, long j) {
        if (str == null) {
            Log.e("wa-shared-preferences/set-backup-media-size account name is null");
            return;
        }
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_media_size:");
        sb.append(str);
        edit.putLong(sb.toString(), j).apply();
    }

    public void A0n(String str, long j) {
        if (str == null) {
            StringBuilder sb = new StringBuilder("wa-shared-preferences/set-backup-timestamp last successful backup timestamp is set to ");
            sb.append(j);
            sb.append(" but accountName associated is null, ignoring.");
            Log.e(sb.toString());
            return;
        }
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb2 = new StringBuilder("gdrive_last_successful_backup_timestamp:");
        sb2.append(str);
        edit.putLong(sb2.toString(), j).apply();
    }

    public void A0o(String str, long j) {
        if (str == null) {
            Log.e("wa-shared-preferences/set-total-backup-size account name is null");
            return;
        }
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_total_size:");
        sb.append(str);
        edit.putLong(sb.toString(), j).apply();
    }

    public void A0p(String str, long j) {
        if (str == null) {
            Log.e("wa-shared-preferences/set-backup-video-size account name is null");
            return;
        }
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_video_size:");
        sb.append(str);
        edit.putLong(sb.toString(), j).apply();
    }

    public void A0q(String str, long j) {
        this.A00.edit().putLong(str, j).apply();
    }

    public void A0r(String str, String str2) {
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("dc_location_name_");
        sb.append(str);
        edit.putString(sb.toString(), str2).apply();
    }

    public void A0s(String str, String str2) {
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("dc_user_postcode_");
        sb.append(str);
        edit.putString(sb.toString(), str2).apply();
    }

    public void A0t(String str, String str2) {
        SharedPreferences.Editor edit = this.A00.edit();
        if (str == null) {
            edit.remove("my_current_status");
        } else {
            edit.putString("my_current_status", str);
            if (!TextUtils.isEmpty(str2)) {
                edit.putString("my_current_status_hash", str2);
                edit.apply();
            }
        }
        edit.remove("my_current_status_hash");
        edit.apply();
    }

    public void A0u(String str, String str2) {
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb = new StringBuilder("downloadable_category_local_info_json_");
        sb.append(str);
        edit.putString(sb.toString(), str2).apply();
    }

    public void A0v(String str, String str2) {
        this.A00.edit().putString("cc", str).putString("ph", str2).apply();
    }

    public void A0w(String str, String str2, long j, long j2, long j3, long j4) {
        this.A00.edit().putString("registration_wipe_type", str).putString("registration_wipe_token", str2).putLong("registration_wipe_wait", j).putLong("registration_wipe_expiry", j2).putLong("registration_wipe_server_time", j3).apply();
        A0q("registration_wipe_info_timestamp", j4);
    }

    public void A0x(String str, boolean z) {
        if (str == null) {
            StringBuilder sb = new StringBuilder("wa-shared-preferences/set-encrypted is set to ");
            sb.append(z);
            sb.append(" but accountName associated is null, ignoring.");
            Log.e(sb.toString());
            return;
        }
        SharedPreferences.Editor edit = this.A00.edit();
        StringBuilder sb2 = new StringBuilder("gdrive_last_successful_backup_encrypted:");
        sb2.append(str);
        edit.putBoolean(sb2.toString(), z).apply();
    }

    public void A0y(JSONObject jSONObject) {
        StringBuilder sb;
        SharedPreferences sharedPreferences = this.A00;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        for (AnonymousClass01T r0 : A00()) {
            Object obj = r0.A00;
            AnonymousClass009.A05(obj);
            String str = (String) obj;
            Object obj2 = r0.A01;
            AnonymousClass009.A05(obj2);
            int intValue = ((Number) obj2).intValue();
            if (jSONObject.has(str)) {
                if (intValue == 0) {
                    try {
                        edit.putInt(str, jSONObject.getInt(str));
                    } catch (JSONException e) {
                        e = e;
                        sb = new StringBuilder();
                        sb.append("wa-shared-preferences/set-local-settings/error-while-inserting ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(sharedPreferences.getInt(str, 0));
                        Log.e(sb.toString(), e);
                    }
                } else if (intValue == 1) {
                    try {
                        edit.putBoolean(str, jSONObject.getBoolean(str));
                    } catch (JSONException e2) {
                        e = e2;
                        sb = new StringBuilder();
                        sb.append("wa-shared-preferences/set-local-settings/error-while-inserting ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(sharedPreferences.getBoolean(str, false));
                        Log.e(sb.toString(), e);
                    }
                } else if (intValue == 2) {
                    try {
                        edit.putString(str, jSONObject.getString(str));
                    } catch (JSONException e3) {
                        e = e3;
                        sb = new StringBuilder();
                        sb.append("wa-shared-preferences/set-local-settings/error-while-inserting ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(sharedPreferences.getString(str, null));
                        Log.e(sb.toString(), e);
                    }
                } else if (intValue == 3) {
                    try {
                        JSONArray jSONArray = jSONObject.getJSONArray(str);
                        HashSet hashSet = new HashSet(jSONArray.length());
                        for (int i = 0; i < jSONArray.length(); i++) {
                            hashSet.add(jSONArray.get(i));
                        }
                        edit.putStringSet(str, hashSet);
                    } catch (JSONException e4) {
                        e = e4;
                        sb = new StringBuilder();
                        sb.append("wa-shared-preferences/set-local-settings/error-while-inserting ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(sharedPreferences.getStringSet(str, null));
                        Log.e(sb.toString(), e);
                    }
                }
            }
        }
        edit.apply();
    }

    public void A0z(boolean z) {
        SharedPreferences.Editor remove = this.A00.edit().remove("external_dir_migration_attempt_n").remove("ext_dir_migration_rescan_time").remove("ext_dir_migration_move_time").remove("ext_dir_migration_start_time");
        if (!z) {
            remove.remove("external_dir_migration_stage");
        }
        remove.commit();
    }

    public void A10(boolean z) {
        this.A00.edit().putBoolean("gdrive_restore_overwrite_local_files", z).apply();
    }

    public void A11(boolean z) {
        this.A00.edit().putBoolean("gdrive_include_videos_in_backup", z).apply();
    }

    public void A12(boolean z) {
        this.A00.edit().putBoolean("new_jid", z).apply();
    }

    public void A13(boolean z) {
        this.A00.edit().putBoolean("live_location_is_new_user", z).apply();
    }

    public void A14(boolean z) {
        this.A00.edit().putBoolean("encrypted_backup_enabled", z).apply();
    }

    public void A15(boolean z) {
        this.A00.edit().putBoolean("encrypted_backup_using_encryption_key", z).apply();
    }

    public void A16(boolean z) {
        this.A00.edit().putBoolean("encrypted_backup_show_forced_reg_after_logout", z).apply();
    }

    public void A17(boolean z) {
        this.A00.edit().putBoolean("privacy_fingerprint_enabled", z).apply();
    }

    public void A18(boolean z) {
        this.A00.edit().putBoolean("seamless_migration_in_progress", z).apply();
    }

    public void A19(boolean z) {
        this.A00.edit().putBoolean("payment_background_batch_require_fetch", z).apply();
    }

    public void A1A(boolean z) {
        StringBuilder sb = new StringBuilder("wa-shared-prefs/setshouldgetprekeydigest/");
        sb.append(z);
        Log.i(sb.toString());
        synchronized (this.A02) {
            this.A00.edit().putBoolean("need_to_get_pre_key_digest", z).apply();
        }
    }

    public void A1B(boolean z) {
        SharedPreferences.Editor edit = this.A00.edit();
        if (z) {
            edit.putBoolean("show_pre_reg_do_not_share_code_warning", true);
        } else {
            edit.remove("show_pre_reg_do_not_share_code_warning");
        }
        edit.apply();
    }

    public void A1C(boolean z) {
        StringBuilder sb = new StringBuilder("wa-shared-prefs/setsignalprotocolstoreisnew/");
        sb.append(z);
        Log.i(sb.toString());
        synchronized (this.A03) {
            this.A00.edit().putBoolean("signal_protocol_store_is_new", z).apply();
        }
    }

    public void A1D(boolean z) {
        this.A00.edit().putBoolean("wam_is_current_buffer_real_time", z).apply();
    }

    public void A1E(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.A00.edit().putBoolean("pref_fail_too_many", z).putBoolean("pref_no_route_sms", z2).putBoolean("pref_no_route_voice", z3).putBoolean("pref_fail_too_many_attempts", z4).putBoolean("pref_fail_too_many_guesses", z5).apply();
    }

    public boolean A1F() {
        return Build.VERSION.SDK_INT == this.A00.getInt("video_transcode_compliance_v5", -1);
    }

    public boolean A1G() {
        boolean z;
        synchronized (this.A03) {
            z = this.A00.getBoolean("signal_protocol_store_is_new", false);
        }
        return z;
    }

    public boolean A1H(int i) {
        if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4) {
            this.A00.edit().putString("interface_gdrive_backup_frequency", String.valueOf(i)).apply();
            return true;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("wa-shared-preferences/set-backup-freq/");
        sb.append(i);
        Log.e(sb.toString());
        return false;
    }

    public boolean A1I(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        SharedPreferences sharedPreferences = this.A00;
        StringBuilder sb = new StringBuilder("gdrive_last_successful_backup_encrypted:");
        sb.append(str);
        return sharedPreferences.getBoolean(sb.toString(), false);
    }

    public boolean A1J(String str, long j) {
        long j2 = this.A00.getLong(str, -1);
        if (j2 == -1 || System.currentTimeMillis() > j2 + j) {
            return true;
        }
        return false;
    }
}
