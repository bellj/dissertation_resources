package X;

import android.os.ConditionVariable;
import android.os.Message;
import com.whatsapp.jobqueue.job.SendWebForwardJob;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.0z8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22470z8 {
    public final C20670w8 A00;
    public final C14850m9 A01;
    public final AnonymousClass16O A02;
    public final AbstractC14440lR A03;
    public final C14890mD A04;
    public final C14860mA A05;
    public final AtomicInteger A06 = new AtomicInteger();
    public final AtomicInteger A07 = new AtomicInteger();
    public final Condition A08;
    public final ReentrantLock A09;

    public C22470z8(C20670w8 r2, C14850m9 r3, AnonymousClass16O r4, AbstractC14440lR r5, C14890mD r6, C14860mA r7) {
        ReentrantLock reentrantLock = new ReentrantLock();
        this.A09 = reentrantLock;
        this.A08 = reentrantLock.newCondition();
        this.A01 = r3;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r7;
        this.A00 = r2;
        this.A02 = r4;
    }

    public final void A00(ConditionVariable conditionVariable, ConditionVariable conditionVariable2, AnonymousClass1IS r14, String str, List list, Map map, int i, int i2, boolean z, boolean z2) {
        int i3;
        if (3 == i) {
            i3 = this.A06.getAndIncrement();
        } else {
            i3 = -1;
        }
        AnonymousClass22J r2 = new AnonymousClass22J(conditionVariable, conditionVariable2, this, i, i3);
        if ((str != null || (list != null && (list.size() != 0 || i == 0))) && (this.A04.A02() || z2)) {
            AnonymousClass22K r1 = new Runnable(r2, this, r14, str, list, map, i, i2, z2) { // from class: X.22K
                public final /* synthetic */ int A00;
                public final /* synthetic */ int A01;
                public final /* synthetic */ AnonymousClass22J A02;
                public final /* synthetic */ C22470z8 A03;
                public final /* synthetic */ AnonymousClass1IS A04;
                public final /* synthetic */ String A05;
                public final /* synthetic */ List A06;
                public final /* synthetic */ Map A07;
                public final /* synthetic */ boolean A08;

                {
                    this.A03 = r2;
                    this.A06 = r5;
                    this.A04 = r3;
                    this.A05 = r4;
                    this.A00 = r7;
                    this.A02 = r1;
                    this.A08 = r9;
                    this.A01 = r8;
                    this.A07 = r6;
                }

                /* JADX WARNING: Code restructure failed: missing block: B:11:0x0034, code lost:
                    if (3 != r9) goto L_0x0036;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:33:0x007c, code lost:
                    r1 = new java.lang.StringBuilder();
                    r1.append("app/xmpp/send/qr_msgs await timeout ");
                    r1.append(r12.get());
                    r1.append(' ');
                    r1.append(r3);
                    com.whatsapp.util.Log.e(r1.toString());
                 */
                @Override // java.lang.Runnable
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void run() {
                    /*
                    // Method dump skipped, instructions count: 267
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass22K.run():void");
                }
            };
            if (z) {
                r1.run();
                return;
            }
            try {
                this.A03.Ab2(r1);
            } catch (Exception e) {
                Log.e("app/xmpp/send/qr_msgs dispatch error ", e);
                r2.A00();
            }
        } else {
            r2.A00();
        }
    }

    public void A01(AbstractC14640lm r19, Collection collection, int i, boolean z) {
        ArrayList arrayList;
        C14890mD r2 = this.A04;
        if (r2.A02() && r19 != null && collection != null && collection.size() != 0) {
            AnonymousClass22L r12 = new AnonymousClass22L(r19, this, collection, i, z);
            ((AbstractC34011fR) r12).A00 = r2.A00().A03;
            C14860mA r4 = this.A05;
            C34021fS r10 = new C34021fS(r12, r4);
            ArrayList arrayList2 = null;
            ArrayList arrayList3 = new ArrayList(collection.size());
            Iterator it = collection.iterator();
            if (z) {
                while (it.hasNext()) {
                    AnonymousClass1JX A00 = this.A02.A00((AbstractC15340mz) it.next(), null, false, false);
                    if (A00 != null) {
                        arrayList3.add(A00);
                    }
                }
                arrayList = null;
                arrayList2 = arrayList3;
            } else {
                while (it.hasNext()) {
                    arrayList3.add(((AbstractC15340mz) it.next()).A0z);
                }
                arrayList = arrayList3;
            }
            String A02 = r4.A02();
            C20670w8 r42 = this.A00;
            String str = r2.A00().A03;
            int i2 = 8;
            if (z) {
                i2 = 7;
            }
            C34081fY r9 = new C34081fY(r19, i2);
            r9.A00 = i;
            r42.A00(new SendWebForwardJob(Message.obtain(null, 0, 55, 0, new AnonymousClass22M(r19, r9, r10, A02, arrayList, arrayList2)), A02, str));
        }
    }

    public void A02(AbstractC15340mz r13) {
        if (!C30051Vw.A17(r13)) {
            throw new IllegalArgumentException("message thumb not loaded");
        } else if (this.A04.A02() && A03()) {
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(r13);
            A00(null, null, null, null, arrayList, null, 4, 4, false, false);
        }
    }

    public final boolean A03() {
        C14880mC r2;
        C14860mA r22 = this.A05;
        String str = this.A04.A00().A00;
        if (str == null || (r2 = (C14880mC) r22.A05().get(str)) == null || !r2.A0E.A05(AbstractC15460nI.A10) || !r2.A0C) {
            return true;
        }
        return false;
    }
}
