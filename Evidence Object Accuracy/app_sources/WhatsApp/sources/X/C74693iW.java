package X;

import androidx.recyclerview.widget.GridLayoutManager;
import com.whatsapp.StickyHeadersRecyclerView;

/* renamed from: X.3iW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74693iW extends AbstractC05290Oz {
    public final /* synthetic */ GridLayoutManager A00;
    public final /* synthetic */ StickyHeadersRecyclerView A01;

    public C74693iW(GridLayoutManager gridLayoutManager, StickyHeadersRecyclerView stickyHeadersRecyclerView) {
        this.A01 = stickyHeadersRecyclerView;
        this.A00 = gridLayoutManager;
    }

    @Override // X.AbstractC05290Oz
    public int A00(int i) {
        if ((((C54412gg) this.A01.A0N).A0E(i) & 4294967295L) == 4294967295L) {
            return this.A00.A00;
        }
        return 1;
    }
}
