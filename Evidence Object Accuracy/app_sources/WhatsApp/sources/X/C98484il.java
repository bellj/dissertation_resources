package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4il  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98484il implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C50692Qo(parcel, null);
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return new C50692Qo(parcel, classLoader);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C50692Qo[i];
    }
}
