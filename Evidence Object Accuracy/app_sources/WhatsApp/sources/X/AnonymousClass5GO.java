package X;

import java.security.Provider;
import java.security.Signature;

/* renamed from: X.5GO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GO implements AnonymousClass5VR {
    public final /* synthetic */ Provider A00;
    public final /* synthetic */ AbstractC113435Ho A01;

    public AnonymousClass5GO(Provider provider, AbstractC113435Ho r2) {
        this.A01 = r2;
        this.A00 = provider;
    }

    @Override // X.AnonymousClass5VR
    public Signature A8Z(String str) {
        Provider provider = this.A00;
        String str2 = this.A01.A00;
        return provider != null ? Signature.getInstance(str2, provider) : Signature.getInstance(str2);
    }
}
