package X;

import android.os.Environment;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.5wO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128615wO {
    public final C14330lG A00;

    public C128615wO(C14330lG r1) {
        this.A00 = r1;
    }

    public final File A00(String str) {
        File A0M;
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            Log.e("PAY: NoviSelfieMediaUtil/getCapturedMediaDirectory external storage is not writable");
            A0M = null;
        } else {
            A0M = this.A00.A0M("novi_selfie_media");
        }
        if (A0M != null) {
            if (A0M.exists() || A0M.mkdirs()) {
                return new File(A0M.getPath(), str);
            }
            Log.e("PAY: NoviSelfieMediaUtil/getMediaFile: failed to create media directory");
        }
        return null;
    }
}
