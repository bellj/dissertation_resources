package X;

import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import java.util.Calendar;
import java.util.HashMap;

/* renamed from: X.19b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C253519b {
    public final C15550nR A00;
    public final C15610nY A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;
    public final C22280yp A05;

    public C253519b(C15550nR r1, C15610nY r2, C14830m7 r3, C16590pI r4, AnonymousClass018 r5, C22280yp r6) {
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A05 = r6;
    }

    public String A00(C15370n3 r10) {
        int i;
        String A01 = A01(r10);
        if (A01 != null) {
            return A01;
        }
        C22280yp r2 = this.A05;
        Jid A0B = r10.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        C33911fH r0 = (C33911fH) r2.A06.get(A0B);
        if (r0 == null) {
            return "";
        }
        long j = r0.A04;
        if (j == 0) {
            return "";
        }
        if (j == 1) {
            return this.A03.A00.getString(R.string.conversation_contact_online);
        }
        toString();
        C14830m7 r02 = this.A02;
        long A02 = r02.A02(j);
        int A00 = C38121nY.A00(r02.A00(), A02);
        if (A00 <= 6) {
            if (A00 != 0) {
                if (A00 != 1) {
                    Calendar instance = Calendar.getInstance();
                    instance.setTimeInMillis(A02);
                    switch (instance.get(7)) {
                        case 1:
                            i = R.string.conversation_last_seen_sun_with_placeholder;
                            break;
                        case 2:
                            i = R.string.conversation_last_seen_mon_with_placeholder;
                            break;
                        case 3:
                            i = R.string.conversation_last_seen_tue_with_placeholder;
                            break;
                        case 4:
                            i = R.string.conversation_last_seen_wed_with_placeholder;
                            break;
                        case 5:
                            i = R.string.conversation_last_seen_thu_with_placeholder;
                            break;
                        case 6:
                            i = R.string.conversation_last_seen_fri_with_placeholder;
                            break;
                        case 7:
                            i = R.string.conversation_last_seen_sat_with_placeholder;
                            break;
                        default:
                            i = 0;
                            break;
                    }
                } else {
                    i = R.string.conversation_last_seen_yesterday_with_placeholder;
                }
            } else {
                i = R.string.conversation_last_seen_today_with_placeholder;
            }
            AnonymousClass018 r5 = this.A04;
            return AnonymousClass3JK.A01(r5, this.A03.A00.getString(i, AnonymousClass3JK.A00(r5, A02)), A02);
        }
        return this.A03.A00.getString(R.string.conversation_last_seen_with_placeholder, C38131nZ.A03(this.A04, A00, A02));
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A01(X.C15370n3 r12) {
        /*
            r11 = this;
            com.whatsapp.jid.Jid r0 = r12.A0D
            boolean r0 = X.C15380n4.A0F(r0)
            r3 = 0
            if (r0 != 0) goto L_0x00e1
            boolean r1 = r12.A0K()
            r6 = 1
            X.0yp r4 = r11.A05
            java.lang.Class<X.0lm> r0 = X.AbstractC14640lm.class
            com.whatsapp.jid.Jid r2 = r12.A0B(r0)
            X.AnonymousClass009.A05(r2)
            X.0lm r2 = (X.AbstractC14640lm) r2
            if (r1 == 0) goto L_0x00c5
            java.util.HashMap r0 = r4.A06
            java.lang.Object r1 = r0.get(r2)
            X.1fH r1 = (X.C33911fH) r1
            r8 = 0
            if (r1 == 0) goto L_0x00e1
            boolean r0 = X.C15380n4.A0J(r2)
            r7 = -1
            if (r0 == 0) goto L_0x006a
            java.util.HashMap r0 = r1.A05
            if (r0 == 0) goto L_0x00e1
            r4 = 0
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r10 = r0.iterator()
        L_0x003d:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x0076
            java.lang.Object r9 = r10.next()
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9
            java.lang.Object r0 = r9.getValue()
            X.1xI r0 = (X.C43601xI) r0
            long r0 = r0.A01
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x003d
            java.lang.Object r8 = r9.getKey()
            java.lang.Object r0 = r9.getValue()
            X.1xI r0 = (X.C43601xI) r0
            long r4 = r0.A01
            java.lang.Object r0 = r9.getValue()
            X.1xI r0 = (X.C43601xI) r0
            int r7 = r0.A00
            goto L_0x003d
        L_0x006a:
            int r0 = r4.A00(r2, r8)
            if (r0 == r7) goto L_0x00e1
            X.4O7 r4 = new X.4O7
            r4.<init>(r2, r0)
            goto L_0x0091
        L_0x0076:
            r1 = 0
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x00e1
            r0 = 25000(0x61a8, double:1.23516E-319)
            long r4 = r4 + r0
            long r1 = android.os.SystemClock.elapsedRealtime()
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00e1
            X.AnonymousClass009.A05(r8)
            X.0lm r8 = (X.AbstractC14640lm) r8
            X.4O7 r4 = new X.4O7
            r4.<init>(r8, r7)
        L_0x0091:
            X.0nR r1 = r11.A00
            X.0lm r0 = r4.A01
            X.0n3 r2 = r1.A0B(r0)
            X.0nY r1 = r11.A01
            r0 = -1
            java.lang.String r5 = r1.A0A(r2, r0)
            int r0 = r4.A00
            r4 = 0
            if (r0 != 0) goto L_0x00bb
            X.0pI r0 = r11.A03
            android.content.Context r3 = r0.A00
            r2 = 2131887491(0x7f120583, float:1.940959E38)
        L_0x00ac:
            java.lang.Object[] r1 = new java.lang.Object[r6]
            X.018 r0 = r11.A04
            java.lang.String r0 = r0.A0F(r5)
            r1[r4] = r0
            java.lang.String r0 = r3.getString(r2, r1)
            return r0
        L_0x00bb:
            if (r0 != r6) goto L_0x00e1
            X.0pI r0 = r11.A03
            android.content.Context r3 = r0.A00
            r2 = 2131887492(0x7f120584, float:1.9409593E38)
            goto L_0x00ac
        L_0x00c5:
            int r0 = r4.A00(r2, r3)
            if (r0 != 0) goto L_0x00d7
            X.0pI r0 = r11.A03
            android.content.Context r1 = r0.A00
            r0 = 2131887477(0x7f120575, float:1.9409562E38)
        L_0x00d2:
            java.lang.String r0 = r1.getString(r0)
            return r0
        L_0x00d7:
            if (r0 != r6) goto L_0x00e1
            X.0pI r0 = r11.A03
            android.content.Context r1 = r0.A00
            r0 = 2131887478(0x7f120576, float:1.9409564E38)
            goto L_0x00d2
        L_0x00e1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C253519b.A01(X.0n3):java.lang.String");
    }

    public String A02(C15370n3 r8) {
        long j;
        if (A01(r8) != null) {
            return null;
        }
        C22280yp r1 = this.A05;
        Jid A0B = r8.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        HashMap hashMap = r1.A06;
        C33911fH r0 = (C33911fH) hashMap.get(A0B);
        if (r0 == null) {
            j = 0;
        } else {
            j = r0.A04;
        }
        Jid A0B2 = r8.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B2);
        C33911fH r02 = (C33911fH) hashMap.get(A0B2);
        if (r02 != null && 1 == r02.A04) {
            return null;
        }
        C14830m7 r03 = this.A02;
        long A02 = r03.A02(j);
        int A00 = C38121nY.A00(r03.A00(), A02);
        if (A00 > 6) {
            return C38131nZ.A03(this.A04, A00, A02);
        }
        if (!(A00 == 0 || A00 == 1)) {
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(A02);
            instance.get(7);
        }
        AnonymousClass018 r12 = this.A04;
        return AnonymousClass3JK.A01(r12, AnonymousClass3JK.A00(r12, A02), A02);
    }
}
