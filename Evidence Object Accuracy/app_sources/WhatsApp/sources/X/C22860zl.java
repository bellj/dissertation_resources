package X;

import android.database.Cursor;
import com.whatsapp.data.device.DeviceChangeManager;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22860zl {
    public final C22820zh A00;
    public final C15570nT A01;
    public final C20870wS A02;
    public final C20670w8 A03;
    public final C22700zV A04;
    public final C14830m7 A05;
    public final C14820m6 A06;
    public final C15990oG A07;
    public final C15680nj A08;
    public final C15650ng A09;
    public final C15600nX A0A;
    public final C22830zi A0B;
    public final C22810zg A0C;
    public final DeviceChangeManager A0D;
    public final C22840zj A0E;
    public final C18770sz A0F;
    public final C14850m9 A0G;
    public final C16030oK A0H;
    public final C22850zk A0I;
    public final C22140ya A0J;

    public C22860zl(C22820zh r2, C15570nT r3, C20870wS r4, C20670w8 r5, C22700zV r6, C14830m7 r7, C14820m6 r8, C15990oG r9, C15680nj r10, C15650ng r11, C15600nX r12, C22830zi r13, C22810zg r14, DeviceChangeManager deviceChangeManager, C22840zj r16, C18770sz r17, C14850m9 r18, C16030oK r19, C22850zk r20, C22140ya r21) {
        this.A05 = r7;
        this.A0G = r18;
        this.A01 = r3;
        this.A03 = r5;
        this.A02 = r4;
        this.A0C = r14;
        this.A09 = r11;
        this.A0F = r17;
        this.A00 = r2;
        this.A07 = r9;
        this.A0D = deviceChangeManager;
        this.A04 = r6;
        this.A06 = r8;
        this.A08 = r10;
        this.A0B = r13;
        this.A0J = r21;
        this.A0H = r19;
        this.A0A = r12;
        this.A0E = r16;
        this.A0I = r20;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0071, code lost:
        if (X.AnonymousClass23Y.A00(r4.hostStorage, r4.actualActors) == 1) goto L_0x0073;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x033e  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x034c  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x038d  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x03a8 A[Catch: all -> 0x0564, TRY_LEAVE, TryCatch #23 {all -> 0x0569, blocks: (B:118:0x039c, B:149:0x0449, B:119:0x03a0, B:121:0x03a8, B:131:0x03ee, B:132:0x03f1, B:134:0x03f9, B:147:0x0443, B:148:0x0446, B:122:0x03bf, B:130:0x03eb, B:185:0x052d, B:135:0x0402, B:146:0x0440, B:188:0x0537), top: B:231:0x039c }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x03f9 A[Catch: all -> 0x0564, TRY_LEAVE, TryCatch #23 {all -> 0x0569, blocks: (B:118:0x039c, B:149:0x0449, B:119:0x03a0, B:121:0x03a8, B:131:0x03ee, B:132:0x03f1, B:134:0x03f9, B:147:0x0443, B:148:0x0446, B:122:0x03bf, B:130:0x03eb, B:185:0x052d, B:135:0x0402, B:146:0x0440, B:188:0x0537), top: B:231:0x039c }] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0453  */
    /* JADX WARNING: Removed duplicated region for block: B:273:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0158 A[Catch: all -> 0x027f, LOOP:1: B:44:0x0152->B:46:0x0158, LOOP_END, TRY_LEAVE, TryCatch #24 {all -> 0x0286, blocks: (B:42:0x0135, B:47:0x0164, B:43:0x014c, B:44:0x0152, B:46:0x0158), top: B:223:0x0135 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x019d A[Catch: all -> 0x027a, TRY_LEAVE, TryCatch #19 {all -> 0x027a, blocks: (B:49:0x0193, B:50:0x0197, B:52:0x019d, B:57:0x01e5, B:53:0x01cf, B:54:0x01d3, B:56:0x01d9), top: B:219:0x0193 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0223 A[Catch: all -> 0x0270, LOOP:4: B:61:0x021d->B:63:0x0223, LOOP_END, TryCatch #15 {all -> 0x0275, blocks: (B:59:0x01fb, B:65:0x026c, B:60:0x01ff, B:61:0x021d, B:63:0x0223, B:64:0x025f), top: B:213:0x01fb }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x02dd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(X.C22860zl r17, com.whatsapp.jid.DeviceJid r18, boolean r19) {
        /*
        // Method dump skipped, instructions count: 1395
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22860zl.A00(X.0zl, com.whatsapp.jid.DeviceJid, boolean):void");
    }

    public final Set A01(DeviceJid deviceJid, UserJid userJid) {
        boolean z = false;
        if (deviceJid.device == 0) {
            z = true;
        }
        boolean z2 = !z;
        C15600nX r1 = this.A0A;
        if (z2) {
            return r1.A06(userJid, Collections.singleton(deviceJid));
        }
        C18680sq r7 = r1.A09;
        if (!r7.A0B()) {
            return r1.A08.A02(userJid);
        }
        HashSet hashSet = new HashSet();
        C16310on A01 = r7.A08.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT DISTINCT(group_jid_row_id) FROM group_participant_user AS user JOIN group_participant_device AS device ON  user._id =  device.group_participant_row_id WHERE user_jid_row_id = ? AND sent_sender_key = 1", new String[]{String.valueOf(r7.A01(userJid))});
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("group_jid_row_id");
            HashSet hashSet2 = new HashSet();
            while (A09.moveToNext()) {
                hashSet2.add(Long.valueOf(A09.getLong(columnIndexOrThrow)));
            }
            for (AbstractC15590nW r0 : r7.A07.A09(AbstractC15590nW.class, hashSet2).values()) {
                if (r0 != null) {
                    hashSet.add(r0);
                }
            }
            A09.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
