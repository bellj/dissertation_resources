package X;

import android.hardware.display.DisplayManager;
import com.whatsapp.calling.callgrid.viewmodel.OrientationViewModel;

/* renamed from: X.4ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C97944ht implements DisplayManager.DisplayListener {
    public final /* synthetic */ OrientationViewModel A00;

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayAdded(int i) {
    }

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayRemoved(int i) {
    }

    public C97944ht(OrientationViewModel orientationViewModel) {
        this.A00 = orientationViewModel;
    }

    @Override // android.hardware.display.DisplayManager.DisplayListener
    public void onDisplayChanged(int i) {
        OrientationViewModel.A00(this.A00);
    }
}
