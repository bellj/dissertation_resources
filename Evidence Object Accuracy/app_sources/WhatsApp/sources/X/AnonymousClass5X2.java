package X;

import android.content.Context;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5X2  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5X2 {
    List AF3();

    boolean AIL();

    void AcT(String str, String str2, String str3, String str4, ArrayList arrayList);

    View buildPaymentHelpSupportSection(Context context, AbstractC28901Pl v, String str);
}
