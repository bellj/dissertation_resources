package X;

import android.view.View;

/* renamed from: X.1th  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41541th extends AbstractC41531tg {
    public final /* synthetic */ int A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AbstractC15340mz A02;
    public final /* synthetic */ AbstractC41521tf A03;
    public final /* synthetic */ AnonymousClass19O A04;
    public final /* synthetic */ Object A05;
    public final /* synthetic */ boolean A06;
    public final /* synthetic */ boolean A07;

    public C41541th(View view, AbstractC15340mz r2, AbstractC41521tf r3, AnonymousClass19O r4, Object obj, int i, boolean z, boolean z2) {
        this.A04 = r4;
        this.A02 = r2;
        this.A07 = z;
        this.A00 = i;
        this.A01 = view;
        this.A03 = r3;
        this.A05 = obj;
        this.A06 = z2;
    }
}
