package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;

/* renamed from: X.2nT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57692nT extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57692nT A0D;
    public static volatile AnonymousClass255 A0E;
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public AnonymousClass1PH A04;
    public C57112mV A05;
    public AnonymousClass2m9 A06;
    public C34871gq A07;
    public C57612nL A08;
    public C56982mH A09;
    public C57232mh A0A;
    public C57402my A0B;
    public AnonymousClass1G8 A0C;

    static {
        C57692nT r0 = new C57692nT();
        A0D = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C82303vP r1;
        C82293vO r12;
        AnonymousClass1PI r13;
        C81693uQ r14;
        C81973us r16;
        C81743uV r17;
        C81753uW r18;
        C81953uq r19;
        AnonymousClass1G9 r110;
        switch (r15.ordinal()) {
            case 0:
                return A0D;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57692nT r3 = (C57692nT) obj2;
                this.A0C = (AnonymousClass1G8) r7.Aft(this.A0C, r3.A0C);
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                int i2 = this.A02;
                int i3 = r3.A00;
                this.A02 = r7.Afp(i2, r3.A02, A1V, C12960it.A1V(i3 & 2, 2));
                this.A01 = r7.Afp(this.A01, r3.A01, C12960it.A1V(i & 4, 4), C12960it.A1V(i3 & 4, 4));
                this.A03 = r7.Afs(this.A03, r3.A03, C12960it.A1V(i & 8, 8), C12960it.A1V(i3 & 8, 8));
                this.A08 = (C57612nL) r7.Aft(this.A08, r3.A08);
                this.A07 = (C34871gq) r7.Aft(this.A07, r3.A07);
                this.A06 = (AnonymousClass2m9) r7.Aft(this.A06, r3.A06);
                this.A09 = (C56982mH) r7.Aft(this.A09, r3.A09);
                this.A05 = (C57112mV) r7.Aft(this.A05, r3.A05);
                this.A04 = (AnonymousClass1PH) r7.Aft(this.A04, r3.A04);
                this.A0A = (C57232mh) r7.Aft(this.A0A, r3.A0A);
                this.A0B = (C57402my) r7.Aft(this.A0B, r3.A0B);
                if (r7 == C463025i.A00) {
                    this.A00 |= r3.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r32 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                if ((this.A00 & 1) == 1) {
                                    r110 = (AnonymousClass1G9) this.A0C.A0T();
                                } else {
                                    r110 = null;
                                }
                                AnonymousClass1G8 r0 = (AnonymousClass1G8) AbstractC27091Fz.A0H(r72, r32, AnonymousClass1G8.A05);
                                this.A0C = r0;
                                if (r110 != null) {
                                    this.A0C = (AnonymousClass1G8) AbstractC27091Fz.A0C(r110, r0);
                                }
                                this.A00 |= 1;
                                break;
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                int A02 = r72.A02();
                                if (EnumC87324Bb.A00(A02) != null) {
                                    this.A00 |= 2;
                                    this.A02 = A02;
                                    break;
                                } else {
                                    super.A0X(2, A02);
                                    break;
                                }
                            case 32:
                                this.A00 |= 4;
                                this.A01 = r72.A02();
                                break;
                            case 40:
                                this.A00 |= 8;
                                this.A03 = r72.A06();
                                break;
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                if ((this.A00 & 16) == 16) {
                                    r19 = (C81953uq) this.A08.A0T();
                                } else {
                                    r19 = null;
                                }
                                C57612nL r02 = (C57612nL) AbstractC27091Fz.A0H(r72, r32, C57612nL.A0A);
                                this.A08 = r02;
                                if (r19 != null) {
                                    this.A08 = (C57612nL) AbstractC27091Fz.A0C(r19, r02);
                                }
                                this.A00 |= 16;
                                break;
                            case 58:
                                if ((this.A00 & 32) == 32) {
                                    r18 = (C81753uW) this.A07.A0T();
                                } else {
                                    r18 = null;
                                }
                                C34871gq r03 = (C34871gq) AbstractC27091Fz.A0H(r72, r32, C34871gq.A01);
                                this.A07 = r03;
                                if (r18 != null) {
                                    this.A07 = (C34871gq) AbstractC27091Fz.A0C(r18, r03);
                                }
                                this.A00 |= 32;
                                break;
                            case 66:
                                if ((this.A00 & 64) == 64) {
                                    r17 = (C81743uV) this.A06.A0T();
                                } else {
                                    r17 = null;
                                }
                                AnonymousClass2m9 r04 = (AnonymousClass2m9) AbstractC27091Fz.A0H(r72, r32, AnonymousClass2m9.A01);
                                this.A06 = r04;
                                if (r17 != null) {
                                    this.A06 = (AnonymousClass2m9) AbstractC27091Fz.A0C(r17, r04);
                                }
                                this.A00 |= 64;
                                break;
                            case 74:
                                if ((this.A00 & 128) == 128) {
                                    r16 = (C81973us) this.A09.A0T();
                                } else {
                                    r16 = null;
                                }
                                C56982mH r05 = (C56982mH) AbstractC27091Fz.A0H(r72, r32, C56982mH.A02);
                                this.A09 = r05;
                                if (r16 != null) {
                                    this.A09 = (C56982mH) AbstractC27091Fz.A0C(r16, r05);
                                }
                                this.A00 |= 128;
                                break;
                            case 82:
                                if ((this.A00 & 256) == 256) {
                                    r14 = (C81693uQ) this.A05.A0T();
                                } else {
                                    r14 = null;
                                }
                                C57112mV r06 = (C57112mV) AbstractC27091Fz.A0H(r72, r32, C57112mV.A03);
                                this.A05 = r06;
                                if (r14 != null) {
                                    this.A05 = (C57112mV) AbstractC27091Fz.A0C(r14, r06);
                                }
                                this.A00 |= 256;
                                break;
                            case 90:
                                if ((this.A00 & 512) == 512) {
                                    r13 = (AnonymousClass1PI) this.A04.A0T();
                                } else {
                                    r13 = null;
                                }
                                AnonymousClass1PH r07 = (AnonymousClass1PH) AbstractC27091Fz.A0H(r72, r32, AnonymousClass1PH.A02);
                                this.A04 = r07;
                                if (r13 != null) {
                                    this.A04 = (AnonymousClass1PH) AbstractC27091Fz.A0C(r13, r07);
                                }
                                this.A00 |= 512;
                                break;
                            case 98:
                                if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                                    r12 = (C82293vO) this.A0A.A0T();
                                } else {
                                    r12 = null;
                                }
                                C57232mh r08 = (C57232mh) AbstractC27091Fz.A0H(r72, r32, C57232mh.A03);
                                this.A0A = r08;
                                if (r12 != null) {
                                    this.A0A = (C57232mh) AbstractC27091Fz.A0C(r12, r08);
                                }
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                break;
                            case 106:
                                if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                                    r1 = (C82303vP) this.A0B.A0T();
                                } else {
                                    r1 = null;
                                }
                                C57402my r09 = (C57402my) AbstractC27091Fz.A0H(r72, r32, C57402my.A04);
                                this.A0B = r09;
                                if (r1 != null) {
                                    this.A0B = (C57402my) AbstractC27091Fz.A0C(r1, r09);
                                }
                                this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                break;
                            default:
                                if (A0a(r72, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57692nT();
            case 5:
                return new C56882m6();
            case 6:
                break;
            case 7:
                if (A0E == null) {
                    synchronized (C57692nT.class) {
                        if (A0E == null) {
                            A0E = AbstractC27091Fz.A09(A0D);
                        }
                    }
                }
                return A0E;
            default:
                throw C12970iu.A0z();
        }
        return A0D;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A0C;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A03(2, this.A02, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A02(4, this.A01, i2);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A05(5, this.A03);
        }
        if ((i3 & 16) == 16) {
            C57612nL r02 = this.A08;
            if (r02 == null) {
                r02 = C57612nL.A0A;
            }
            i2 = AbstractC27091Fz.A08(r02, 6, i2);
        }
        if ((this.A00 & 32) == 32) {
            C34871gq r03 = this.A07;
            if (r03 == null) {
                r03 = C34871gq.A01;
            }
            i2 = AbstractC27091Fz.A08(r03, 7, i2);
        }
        if ((this.A00 & 64) == 64) {
            AnonymousClass2m9 r04 = this.A06;
            if (r04 == null) {
                r04 = AnonymousClass2m9.A01;
            }
            i2 = AbstractC27091Fz.A08(r04, 8, i2);
        }
        if ((this.A00 & 128) == 128) {
            C56982mH r05 = this.A09;
            if (r05 == null) {
                r05 = C56982mH.A02;
            }
            i2 = AbstractC27091Fz.A08(r05, 9, i2);
        }
        if ((this.A00 & 256) == 256) {
            C57112mV r06 = this.A05;
            if (r06 == null) {
                r06 = C57112mV.A03;
            }
            i2 = AbstractC27091Fz.A08(r06, 10, i2);
        }
        if ((this.A00 & 512) == 512) {
            AnonymousClass1PH r07 = this.A04;
            if (r07 == null) {
                r07 = AnonymousClass1PH.A02;
            }
            i2 = AbstractC27091Fz.A08(r07, 11, i2);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C57232mh r08 = this.A0A;
            if (r08 == null) {
                r08 = C57232mh.A03;
            }
            i2 = AbstractC27091Fz.A08(r08, 12, i2);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C57402my r09 = this.A0B;
            if (r09 == null) {
                r09 = C57402my.A04;
            }
            i2 = AbstractC27091Fz.A08(r09, 13, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1G8 r0 = this.A0C;
            if (r0 == null) {
                r0 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0E(2, this.A02);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(4, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(5, this.A03);
        }
        if ((this.A00 & 16) == 16) {
            C57612nL r02 = this.A08;
            if (r02 == null) {
                r02 = C57612nL.A0A;
            }
            codedOutputStream.A0L(r02, 6);
        }
        if ((this.A00 & 32) == 32) {
            C34871gq r03 = this.A07;
            if (r03 == null) {
                r03 = C34871gq.A01;
            }
            codedOutputStream.A0L(r03, 7);
        }
        if ((this.A00 & 64) == 64) {
            AnonymousClass2m9 r04 = this.A06;
            if (r04 == null) {
                r04 = AnonymousClass2m9.A01;
            }
            codedOutputStream.A0L(r04, 8);
        }
        if ((this.A00 & 128) == 128) {
            C56982mH r05 = this.A09;
            if (r05 == null) {
                r05 = C56982mH.A02;
            }
            codedOutputStream.A0L(r05, 9);
        }
        if ((this.A00 & 256) == 256) {
            C57112mV r06 = this.A05;
            if (r06 == null) {
                r06 = C57112mV.A03;
            }
            codedOutputStream.A0L(r06, 10);
        }
        if ((this.A00 & 512) == 512) {
            AnonymousClass1PH r07 = this.A04;
            if (r07 == null) {
                r07 = AnonymousClass1PH.A02;
            }
            codedOutputStream.A0L(r07, 11);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C57232mh r08 = this.A0A;
            if (r08 == null) {
                r08 = C57232mh.A03;
            }
            codedOutputStream.A0L(r08, 12);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C57402my r09 = this.A0B;
            if (r09 == null) {
                r09 = C57402my.A04;
            }
            codedOutputStream.A0L(r09, 13);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
