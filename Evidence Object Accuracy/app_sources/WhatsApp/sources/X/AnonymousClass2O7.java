package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.protocol.VoipStanzaChildNode;

/* renamed from: X.2O7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2O7 extends AnonymousClass2MM {
    public final long A00;
    public final long A01;
    public final long A02;
    public final DeviceJid A03;
    public final String A04;
    public final String A05;
    public final boolean A06;

    public AnonymousClass2O7(DeviceJid deviceJid, Jid jid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, String str3, String str4, long j, long j2, long j3, boolean z) {
        super(jid, voipStanzaChildNode, str, str2, false);
        this.A03 = deviceJid;
        this.A05 = str3;
        this.A04 = str4;
        this.A01 = j;
        this.A00 = j2;
        this.A06 = z;
        this.A02 = j3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0009 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A00(java.lang.String r3) {
        /*
            int r0 = r3.hashCode()
            r2 = 1
            r1 = 0
            switch(r0) {
                case -1423461112: goto L_0x0017;
                case -934710369: goto L_0x0014;
                case 105650780: goto L_0x0011;
                case 1063018407: goto L_0x000e;
                case 2035990113: goto L_0x000a;
                default: goto L_0x0009;
            }
        L_0x0009:
            return r2
        L_0x000a:
            java.lang.String r0 = "terminate"
            goto L_0x0019
        L_0x000e:
            java.lang.String r0 = "enc_rekey"
            goto L_0x0019
        L_0x0011:
            java.lang.String r0 = "offer"
            goto L_0x0019
        L_0x0014:
            java.lang.String r0 = "reject"
            goto L_0x0019
        L_0x0017:
            java.lang.String r0 = "accept"
        L_0x0019:
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0009
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2O7.A00(java.lang.String):boolean");
    }
}
