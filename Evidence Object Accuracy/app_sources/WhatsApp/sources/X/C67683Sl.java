package X;

import java.io.IOException;

/* renamed from: X.3Sl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67683Sl implements AbstractC14080kp, AbstractC14130ku {
    public long A00 = -9223372036854775807L;
    public AbstractC14130ku A01;
    public AbstractC14080kp A02;
    public AnonymousClass2CD A03;
    public final long A04;
    public final C28741Ov A05;
    public final AnonymousClass5VZ A06;

    public C67683Sl(C28741Ov r3, AnonymousClass5VZ r4, long j) {
        this.A05 = r3;
        this.A06 = r4;
        this.A04 = j;
    }

    public void A00(C28741Ov r9) {
        long j = this.A04;
        long j2 = this.A00;
        if (j2 != -9223372036854775807L) {
            j = j2;
        }
        AbstractC14080kp A8S = this.A03.A8S(r9, this.A06, j);
        this.A02 = A8S;
        if (this.A01 != null) {
            A8S.AZS(this, j);
        }
    }

    @Override // X.AbstractC14080kp
    public boolean A7e(long j) {
        AbstractC14080kp r0 = this.A02;
        return r0 != null && r0.A7e(j);
    }

    @Override // X.AbstractC14080kp
    public void A8x(long j, boolean z) {
        this.A02.A8x(j, false);
    }

    @Override // X.AbstractC14080kp
    public long AAe(C94224bS r3, long j) {
        return this.A02.AAe(r3, j);
    }

    @Override // X.AbstractC14080kp
    public long AB2() {
        return this.A02.AB2();
    }

    @Override // X.AbstractC14080kp
    public long AEf() {
        return this.A02.AEf();
    }

    @Override // X.AbstractC14080kp
    public C100564m7 AHH() {
        return this.A02.AHH();
    }

    @Override // X.AbstractC14080kp
    public boolean AJh() {
        AbstractC14080kp r0 = this.A02;
        return r0 != null && r0.AJh();
    }

    @Override // X.AbstractC14080kp
    public void ALS() {
        try {
            AbstractC14080kp r0 = this.A02;
            if (r0 != null) {
                r0.ALS();
                return;
            }
            AnonymousClass2CD r02 = this.A03;
            if (r02 != null) {
                r02.ALT();
            }
        } catch (IOException e) {
            throw e;
        }
    }

    @Override // X.AbstractC14140kv
    public /* bridge */ /* synthetic */ void AOe(AbstractC14090kq r2) {
        this.A01.AOe(this);
    }

    @Override // X.AbstractC14130ku
    public void AUB(AbstractC14080kp r2) {
        this.A01.AUB(this);
    }

    @Override // X.AbstractC14080kp
    public void AZS(AbstractC14130ku r9, long j) {
        this.A01 = r9;
        AbstractC14080kp r7 = this.A02;
        if (r7 != null) {
            long j2 = this.A04;
            long j3 = this.A00;
            if (j3 != -9223372036854775807L) {
                j2 = j3;
            }
            r7.AZS(this, j2);
        }
    }

    @Override // X.AbstractC14080kp
    public long AZt() {
        return this.A02.AZt();
    }

    @Override // X.AbstractC14080kp
    public long AbT(long j) {
        return this.A02.AbT(j);
    }

    @Override // X.AbstractC14080kp
    public long AbW(AbstractC116795Wx[] r8, AbstractC117085Ye[] r9, boolean[] zArr, boolean[] zArr2, long j) {
        long j2 = this.A00;
        if (j2 == -9223372036854775807L || j != this.A04) {
            j2 = j;
        } else {
            this.A00 = -9223372036854775807L;
        }
        return this.A02.AbW(r8, r9, zArr, zArr2, j2);
    }
}
