package X;

/* renamed from: X.0GP  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0GP extends AnonymousClass0LM {
    public final Throwable A00;

    public AnonymousClass0GP(Throwable th) {
        this.A00 = th;
    }

    public String toString() {
        return String.format("FAILURE (%s)", this.A00.getMessage());
    }
}
