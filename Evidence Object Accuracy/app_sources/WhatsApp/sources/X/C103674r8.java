package X;

import android.content.Context;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.4r8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103674r8 implements AbstractC009204q {
    public final /* synthetic */ VoipActivityV2 A00;

    public C103674r8(VoipActivityV2 voipActivityV2) {
        this.A00 = voipActivityV2;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
