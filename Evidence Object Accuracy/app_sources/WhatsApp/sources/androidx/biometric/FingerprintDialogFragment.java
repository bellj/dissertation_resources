package androidx.biometric;

import X.ActivityC000900k;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass02A;
import X.AnonymousClass04S;
import X.AnonymousClass0EP;
import X.AnonymousClass0KK;
import X.AnonymousClass0QX;
import X.AnonymousClass0V5;
import X.C004802e;
import X.C05000Nw;
import X.C07480Yh;
import X.C07490Yi;
import X.RunnableC09190cV;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class FingerprintDialogFragment extends DialogFragment {
    public int A00;
    public int A01;
    public ImageView A02;
    public TextView A03;
    public AnonymousClass0EP A04;
    public final Handler A05 = new Handler(Looper.getMainLooper());
    public final Runnable A06 = new RunnableC09190cV(this);

    @Override // X.AnonymousClass01E
    public void A0r() {
        this.A0V = true;
        this.A05.removeCallbacksAndMessages(null);
    }

    @Override // X.AnonymousClass01E
    public void A13() {
        this.A0V = true;
        AnonymousClass0EP r1 = this.A04;
        r1.A01 = 0;
        r1.A04(1);
        this.A04.A05(A0I(R.string.fingerprint_dialog_touch_sensor));
    }

    @Override // androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        int i;
        super.A16(bundle);
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            AnonymousClass0EP r0 = (AnonymousClass0EP) new AnonymousClass02A(A0B).A00(AnonymousClass0EP.class);
            this.A04 = r0;
            AnonymousClass016 r1 = r0.A0C;
            if (r1 == null) {
                r1 = new AnonymousClass016();
                r0.A0C = r1;
            }
            r1.A05(this, new C07480Yh(this));
            AnonymousClass0EP r02 = this.A04;
            AnonymousClass016 r12 = r02.A0B;
            if (r12 == null) {
                r12 = new AnonymousClass016();
                r02.A0B = r12;
            }
            r12.A05(this, new C07490Yi(this));
        }
        if (Build.VERSION.SDK_INT >= 26) {
            i = A1I(AnonymousClass0KK.A00());
        } else {
            Context A0p = A0p();
            i = 0;
            if (A0p != null) {
                i = AnonymousClass00T.A00(A0p, R.color.biometric_error_color);
            }
        }
        this.A00 = i;
        this.A01 = A1I(16842808);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        CharSequence charSequence;
        CharSequence charSequence2;
        CharSequence charSequence3;
        C004802e r6 = new C004802e(A01());
        C05000Nw r0 = this.A04.A06;
        if (r0 != null) {
            charSequence = r0.A03;
        } else {
            charSequence = null;
        }
        r6.setTitle(charSequence);
        View inflate = LayoutInflater.from(r6.A01.A0O).inflate(R.layout.fingerprint_dialog_layout, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.fingerprint_subtitle);
        if (textView != null) {
            C05000Nw r02 = this.A04.A06;
            if (r02 != null) {
                charSequence3 = r02.A02;
            } else {
                charSequence3 = null;
            }
            if (TextUtils.isEmpty(charSequence3)) {
                textView.setVisibility(8);
            } else {
                textView.setVisibility(0);
                textView.setText(charSequence3);
            }
        }
        TextView textView2 = (TextView) inflate.findViewById(R.id.fingerprint_description);
        if (textView2 != null) {
            if (TextUtils.isEmpty(null)) {
                textView2.setVisibility(8);
            } else {
                textView2.setVisibility(0);
                textView2.setText((CharSequence) null);
            }
        }
        this.A02 = (ImageView) inflate.findViewById(R.id.fingerprint_icon);
        this.A03 = (TextView) inflate.findViewById(R.id.fingerprint_error);
        AnonymousClass0EP r3 = this.A04;
        C05000Nw r2 = r3.A06;
        if (r2 == null || (AnonymousClass0QX.A00(r3.A05, r2) & 32768) == 0) {
            charSequence2 = r3.A0G;
            if (charSequence2 == null) {
                if (r2 != null) {
                    charSequence2 = r2.A01;
                    if (charSequence2 == null) {
                        charSequence2 = "";
                    }
                } else {
                    charSequence2 = null;
                }
            }
        } else {
            charSequence2 = A0I(R.string.confirm_device_credential_password);
        }
        r6.A01(new AnonymousClass0V5(this), charSequence2);
        r6.setView(inflate);
        AnonymousClass04S create = r6.create();
        create.setCanceledOnTouchOutside(false);
        return create;
    }

    public final int A1I(int i) {
        Context A0p = A0p();
        ActivityC000900k A0B = A0B();
        if (A0p == null || A0B == null) {
            Log.w("FingerprintFragment", "Unable to get themed color. Context or activity is null.");
            return 0;
        }
        TypedValue typedValue = new TypedValue();
        A0p.getTheme().resolveAttribute(i, typedValue, true);
        TypedArray obtainStyledAttributes = A0B.obtainStyledAttributes(typedValue.data, new int[]{i});
        int color = obtainStyledAttributes.getColor(0, 0);
        obtainStyledAttributes.recycle();
        return color;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        if (r6 != 2) goto L_0x0027;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1J(int r6) {
        /*
            r5 = this;
            android.widget.ImageView r0 = r5.A02
            if (r0 == 0) goto L_0x001b
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            if (r1 < r0) goto L_0x001b
            X.0EP r0 = r5.A04
            int r4 = r0.A01
            android.content.Context r3 = r5.A0p()
            if (r3 != 0) goto L_0x001c
            java.lang.String r1 = "FingerprintFragment"
            java.lang.String r0 = "Unable to get asset. Context is null."
            android.util.Log.w(r1, r0)
        L_0x001b:
            return
        L_0x001c:
            r2 = 1
            if (r4 == 0) goto L_0x0051
            r0 = 2
            if (r4 != r2) goto L_0x004c
            r1 = 2131231531(0x7f08032b, float:1.8079146E38)
            if (r6 == r0) goto L_0x002d
        L_0x0027:
            r0 = 3
            if (r6 != r0) goto L_0x001b
        L_0x002a:
            r1 = 2131231532(0x7f08032c, float:1.8079148E38)
        L_0x002d:
            android.graphics.drawable.Drawable r1 = X.AnonymousClass00T.A04(r3, r1)
            if (r1 == 0) goto L_0x001b
            android.widget.ImageView r0 = r5.A02
            r0.setImageDrawable(r1)
            if (r4 == 0) goto L_0x0042
            r0 = 2
            if (r4 != r2) goto L_0x0047
            if (r6 != r0) goto L_0x0042
        L_0x003f:
            X.AnonymousClass0KJ.A00(r1)
        L_0x0042:
            X.0EP r0 = r5.A04
            r0.A01 = r6
            return
        L_0x0047:
            if (r4 != r0) goto L_0x0042
            if (r6 != r2) goto L_0x0042
            goto L_0x003f
        L_0x004c:
            if (r4 == r0) goto L_0x0051
            if (r4 != r2) goto L_0x001b
            goto L_0x0027
        L_0x0051:
            if (r6 != r2) goto L_0x001b
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.biometric.FingerprintDialogFragment.A1J(int):void");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        AnonymousClass0EP r2 = this.A04;
        AnonymousClass016 r1 = r2.A0E;
        if (r1 == null) {
            r1 = new AnonymousClass016();
            r2.A0E = r1;
        }
        AnonymousClass0EP.A00(r1, true);
    }
}
