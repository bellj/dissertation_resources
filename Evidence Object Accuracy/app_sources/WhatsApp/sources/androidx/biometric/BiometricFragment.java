package androidx.biometric;

import X.ActivityC000900k;
import X.AnonymousClass016;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass02A;
import X.AnonymousClass02N;
import X.AnonymousClass049;
import X.AnonymousClass04A;
import X.AnonymousClass04B;
import X.AnonymousClass0DP;
import X.AnonymousClass0EP;
import X.AnonymousClass0KG;
import X.AnonymousClass0NI;
import X.AnonymousClass0OP;
import X.AnonymousClass0QX;
import X.AnonymousClass0U4;
import X.AnonymousClass0XW;
import X.AnonymousClass0Yd;
import X.AnonymousClass0Ye;
import X.AnonymousClass0Yf;
import X.C004902f;
import X.C02440Ch;
import X.C04700Ms;
import X.C05000Nw;
import X.C05560Qa;
import X.C05580Qc;
import X.C06150Sj;
import X.C07450Yb;
import X.C07460Yc;
import X.C07470Yg;
import X.ExecutorC10570eq;
import X.RunnableC09150cR;
import X.RunnableC09160cS;
import X.RunnableC09170cT;
import X.RunnableC09180cU;
import X.RunnableC09470cx;
import X.RunnableC09790dW;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;
import java.security.Signature;
import java.util.concurrent.Executor;
import javax.crypto.Cipher;
import javax.crypto.Mac;

/* loaded from: classes.dex */
public class BiometricFragment extends AnonymousClass01E {
    public Handler A00 = new Handler(Looper.getMainLooper());
    public AnonymousClass0EP A01;

    public static String A00(Context context, int i) {
        int i2;
        if (context == null) {
            return "";
        }
        if (i != 1) {
            if (i != 7) {
                switch (i) {
                    case 9:
                        break;
                    case 10:
                        i2 = R.string.fingerprint_error_user_canceled;
                        break;
                    case 11:
                        i2 = R.string.fingerprint_error_no_fingerprints;
                        break;
                    case 12:
                        i2 = R.string.fingerprint_error_hw_not_present;
                        break;
                    default:
                        StringBuilder sb = new StringBuilder("Unknown error code: ");
                        sb.append(i);
                        Log.e("BiometricUtils", sb.toString());
                        i2 = R.string.default_error_msg;
                        break;
                }
            }
            i2 = R.string.fingerprint_error_lockout;
        } else {
            i2 = R.string.fingerprint_error_hw_not_available;
        }
        return context.getString(i2);
    }

    public static boolean A01(Context context, String str) {
        if (Build.VERSION.SDK_INT != 28 || str == null) {
            return false;
        }
        for (String str2 : context.getResources().getStringArray(R.array.hide_fingerprint_instantly_prefixes)) {
            if (str.startsWith(str2)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        AnonymousClass0EP r4;
        C05000Nw r1;
        this.A0V = true;
        if (Build.VERSION.SDK_INT == 29 && (r1 = (r4 = this.A01).A06) != null && (AnonymousClass0QX.A00(r4.A05, r1) & 32768) != 0) {
            r4.A0M = true;
            this.A00.postDelayed(new RunnableC09180cU(r4), 250);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0t(int i, int i2, Intent intent) {
        super.A0t(i, i2, intent);
        if (i == 1) {
            this.A01.A0J = false;
            if (i2 == -1) {
                A1H(new C04700Ms(null, 1));
                return;
            }
            A1G(10, A0I(R.string.generic_error_user_canceled));
            A18();
        }
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        this.A0V = true;
        if (Build.VERSION.SDK_INT < 29 && !this.A01.A0J) {
            ActivityC000900k A0B = A0B();
            if (A0B == null || !A0B.isChangingConfigurations()) {
                A1E(0);
            }
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            AnonymousClass0EP r0 = (AnonymousClass0EP) new AnonymousClass02A(A0B).A00(AnonymousClass0EP.class);
            this.A01 = r0;
            AnonymousClass016 r1 = r0.A0A;
            if (r1 == null) {
                r1 = new AnonymousClass016();
                r0.A0A = r1;
            }
            r1.A05(this, new C07450Yb(this));
            AnonymousClass0EP r02 = this.A01;
            AnonymousClass016 r12 = r02.A08;
            if (r12 == null) {
                r12 = new AnonymousClass016();
                r02.A08 = r12;
            }
            r12.A05(this, new C07460Yc(this));
            AnonymousClass0EP r03 = this.A01;
            AnonymousClass016 r13 = r03.A09;
            if (r13 == null) {
                r13 = new AnonymousClass016();
                r03.A09 = r13;
            }
            r13.A05(this, new AnonymousClass0Yd(this));
            AnonymousClass0EP r04 = this.A01;
            AnonymousClass016 r14 = r04.A0D;
            if (r14 == null) {
                r14 = new AnonymousClass016();
                r04.A0D = r14;
            }
            r14.A05(this, new AnonymousClass0Ye(this));
            AnonymousClass0EP r05 = this.A01;
            AnonymousClass016 r15 = r05.A0F;
            if (r15 == null) {
                r15 = new AnonymousClass016();
                r05.A0F = r15;
            }
            r15.A05(this, new AnonymousClass0Yf(this));
            AnonymousClass0EP r06 = this.A01;
            AnonymousClass016 r16 = r06.A0E;
            if (r16 == null) {
                r16 = new AnonymousClass016();
                r06.A0E = r16;
            }
            r16.A05(this, new C07470Yg(this));
        }
    }

    public void A18() {
        this.A01.A0N = false;
        A1B();
        if (!this.A01.A0J && A0c()) {
            C004902f r1 = new C004902f(A0F());
            r1.A05(this);
            r1.A00(true);
        }
        Context A0p = A0p();
        if (A0p != null) {
            String str = Build.MODEL;
            if (Build.VERSION.SDK_INT == 29 && str != null) {
                for (String str2 : A0p.getResources().getStringArray(R.array.delay_showing_prompt_models)) {
                    if (str.equals(str2)) {
                        AnonymousClass0EP r12 = this.A01;
                        r12.A0K = true;
                        this.A00.postDelayed(new RunnableC09170cT(r12), 600);
                        return;
                    }
                }
            }
        }
    }

    public void A19() {
        if (Build.VERSION.SDK_INT < 21) {
            Log.e("BiometricFragment", "Failed to check device credential. Not supported prior to API 21.");
        } else {
            A1C();
        }
    }

    public void A1A() {
        int i;
        if (this.A01.A0N) {
            return;
        }
        if (A0p() == null) {
            Log.w("BiometricFragment", "Not showing biometric prompt. Context is null.");
            return;
        }
        AnonymousClass0EP r1 = this.A01;
        r1.A0N = true;
        r1.A0I = true;
        if (A1K()) {
            Context applicationContext = A01().getApplicationContext();
            AnonymousClass049 r5 = new AnonymousClass049(applicationContext);
            if (!r5.A06()) {
                i = 12;
            } else if (!r5.A05()) {
                i = 11;
            } else if (A0c()) {
                this.A01.A0L = true;
                if (!A01(applicationContext, Build.MODEL)) {
                    this.A00.postDelayed(new RunnableC09150cR(this), 500);
                    new FingerprintDialogFragment().A1F(A0F(), "androidx.biometric.FingerprintDialogFragment");
                }
                AnonymousClass0EP r12 = this.A01;
                r12.A00 = 0;
                AnonymousClass0U4 r2 = r12.A05;
                AnonymousClass04B r4 = null;
                if (r2 != null) {
                    Cipher cipher = r2.A02;
                    if (cipher != null) {
                        r4 = new AnonymousClass04B(cipher);
                    } else {
                        Signature signature = r2.A01;
                        if (signature != null) {
                            r4 = new AnonymousClass04B(signature);
                        } else {
                            Mac mac = r2.A03;
                            if (mac != null) {
                                r4 = new AnonymousClass04B(mac);
                            } else if (Build.VERSION.SDK_INT >= 30 && r2.A00() != null) {
                                Log.e("CryptoObjectUtils", "Identity credential is not supported by FingerprintManager.");
                            }
                        }
                    }
                }
                AnonymousClass0EP r6 = this.A01;
                AnonymousClass0NI r0 = r6.A07;
                if (r0 == null) {
                    r0 = new AnonymousClass0NI();
                    r6.A07 = r0;
                }
                AnonymousClass02N r22 = r0.A01;
                if (r22 == null) {
                    r22 = new AnonymousClass02N();
                    r0.A01 = r22;
                }
                AnonymousClass0OP r13 = r6.A03;
                if (r13 == null) {
                    r13 = new AnonymousClass0OP(new C02440Ch(r6));
                    r6.A03 = r13;
                }
                AnonymousClass04A r02 = r13.A01;
                if (r02 == null) {
                    r02 = new AnonymousClass0DP(r13);
                    r13.A01 = r02;
                }
                try {
                    r5.A04(r02, r4, r22);
                    return;
                } catch (NullPointerException e) {
                    Log.e("BiometricFragment", "Got NPE while authenticating with fingerprint.", e);
                    A1G(1, A00(applicationContext, 1));
                    A18();
                    return;
                }
            } else {
                return;
            }
            A1G(i, A00(applicationContext, i));
            A18();
            return;
        }
        A1D();
    }

    public final void A1B() {
        this.A01.A0N = false;
        if (A0c()) {
            AnonymousClass01F A0F = A0F();
            DialogFragment dialogFragment = (DialogFragment) A0F.A0A("androidx.biometric.FingerprintDialogFragment");
            if (dialogFragment == null) {
                return;
            }
            if (dialogFragment.A0c()) {
                dialogFragment.A1C();
                return;
            }
            C004902f r1 = new C004902f(A0F);
            r1.A05(dialogFragment);
            r1.A00(true);
        }
    }

    public final void A1C() {
        CharSequence charSequence;
        CharSequence charSequence2;
        int i;
        int i2;
        ActivityC000900k A0B = A0B();
        if (A0B == null) {
            Log.e("BiometricFragment", "Failed to check device credential. Client FragmentActivity not found.");
            return;
        }
        KeyguardManager A00 = C05580Qc.A00(A0B);
        if (A00 == null) {
            i = 12;
            i2 = R.string.generic_error_no_keyguard;
        } else {
            C05000Nw r0 = this.A01.A06;
            if (r0 != null) {
                charSequence = r0.A03;
                charSequence2 = r0.A02;
            } else {
                charSequence = null;
                charSequence2 = null;
            }
            if (charSequence2 == null) {
                charSequence2 = null;
            }
            Intent A002 = AnonymousClass0KG.A00(A00, charSequence, charSequence2);
            if (A002 == null) {
                i = 14;
                i2 = R.string.generic_error_no_device_credential;
            } else {
                this.A01.A0J = true;
                if (A1K()) {
                    A1B();
                }
                A002.setFlags(134742016);
                startActivityForResult(A002, 1);
                return;
            }
        }
        A1G(i, A0I(i2));
        A18();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0060, code lost:
        if (r1 != false) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1D() {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.biometric.BiometricFragment.A1D():void");
    }

    public void A1E(int i) {
        if (i == 3 || !this.A01.A0M) {
            if (A1K()) {
                this.A01.A00 = i;
                if (i == 1) {
                    A1G(10, A00(A0p(), 10));
                }
            }
            AnonymousClass0EP r0 = this.A01;
            AnonymousClass0NI r4 = r0.A07;
            if (r4 == null) {
                r4 = new AnonymousClass0NI();
                r0.A07 = r4;
            }
            CancellationSignal cancellationSignal = r4.A00;
            if (cancellationSignal != null) {
                try {
                    cancellationSignal.cancel();
                } catch (NullPointerException e) {
                    Log.e("CancelSignalProvider", "Got NPE while canceling biometric authentication.", e);
                }
                r4.A00 = null;
            }
            AnonymousClass02N r02 = r4.A01;
            if (r02 != null) {
                try {
                    r02.A01();
                } catch (NullPointerException e2) {
                    Log.e("CancelSignalProvider", "Got NPE while canceling fingerprint authentication.", e2);
                }
                r4.A01 = null;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0052, code lost:
        if (r1 != 3) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b2, code lost:
        if (A01(r1, android.os.Build.MODEL) == false) goto L_0x00b4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1F(int r5, java.lang.CharSequence r6) {
        /*
            r4 = this;
            switch(r5) {
                case 1: goto L_0x0005;
                case 2: goto L_0x0005;
                case 3: goto L_0x0005;
                case 4: goto L_0x0005;
                case 5: goto L_0x0005;
                case 6: goto L_0x0003;
                case 7: goto L_0x0005;
                case 8: goto L_0x0005;
                case 9: goto L_0x0005;
                case 10: goto L_0x0005;
                case 11: goto L_0x0005;
                case 12: goto L_0x0005;
                case 13: goto L_0x0005;
                case 14: goto L_0x0005;
                case 15: goto L_0x0005;
                default: goto L_0x0003;
            }
        L_0x0003:
            r5 = 8
        L_0x0005:
            android.content.Context r2 = r4.A0p()
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0038
            r0 = 29
            if (r1 >= r0) goto L_0x0038
            r0 = 7
            if (r5 == r0) goto L_0x001a
            r0 = 9
            if (r5 != r0) goto L_0x0038
        L_0x001a:
            if (r2 == 0) goto L_0x0038
            boolean r0 = X.C05580Qc.A01(r2)
            if (r0 == 0) goto L_0x0038
            X.0EP r0 = r4.A01
            X.0Nw r1 = r0.A06
            if (r1 == 0) goto L_0x0038
            X.0U4 r0 = r0.A05
            int r1 = X.AnonymousClass0QX.A00(r0, r1)
            r0 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0038
            r4.A1C()
            return
        L_0x0038:
            boolean r0 = r4.A1K()
            if (r0 == 0) goto L_0x005b
            if (r6 != 0) goto L_0x0048
            android.content.Context r0 = r4.A0p()
            java.lang.String r6 = A00(r0, r5)
        L_0x0048:
            r1 = 5
            X.0EP r0 = r4.A01
            if (r5 != r1) goto L_0x0079
            int r1 = r0.A00
            if (r1 == 0) goto L_0x0054
            r0 = 3
            if (r1 != r0) goto L_0x0057
        L_0x0054:
            r4.A1G(r5, r6)
        L_0x0057:
            r4.A18()
            return
        L_0x005b:
            if (r6 != 0) goto L_0x0054
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r0 = 2131887624(0x7f120608, float:1.940986E38)
            java.lang.String r0 = r4.A0I(r0)
            r1.append(r0)
            java.lang.String r0 = " "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r6 = r1.toString()
            goto L_0x0054
        L_0x0079:
            boolean r0 = r0.A0L
            if (r0 == 0) goto L_0x0089
            r4.A1G(r5, r6)
            r4.A18()
        L_0x0083:
            X.0EP r1 = r4.A01
            r0 = 1
            r1.A0L = r0
            return
        L_0x0089:
            r2 = r6
            if (r6 != 0) goto L_0x0093
            r0 = 2131887624(0x7f120608, float:1.940986E38)
            java.lang.String r2 = r4.A0I(r0)
        L_0x0093:
            X.0EP r1 = r4.A01
            r0 = 2
            r1.A04(r0)
            X.0EP r0 = r4.A01
            r0.A05(r2)
            android.os.Handler r3 = r4.A00
            X.0dX r2 = new X.0dX
            r2.<init>(r4, r6, r5)
            android.content.Context r1 = r4.A0p()
            if (r1 == 0) goto L_0x00b4
            java.lang.String r0 = android.os.Build.MODEL
            boolean r1 = A01(r1, r0)
            r0 = 0
            if (r1 != 0) goto L_0x00b6
        L_0x00b4:
            r0 = 2000(0x7d0, float:2.803E-42)
        L_0x00b6:
            long r0 = (long) r0
            r3.postDelayed(r2, r0)
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.biometric.BiometricFragment.A1F(int, java.lang.CharSequence):void");
    }

    public final void A1G(int i, CharSequence charSequence) {
        AnonymousClass0EP r2 = this.A01;
        if (r2.A0J) {
            Log.v("BiometricFragment", "Error not sent to client. User is confirming their device credential.");
        } else if (!r2.A0I) {
            Log.w("BiometricFragment", "Error not sent to client. Client is not awaiting a result.");
        } else {
            r2.A0I = false;
            Executor executor = r2.A0H;
            if (executor == null) {
                executor = new ExecutorC10570eq();
            }
            executor.execute(new RunnableC09790dW(this, charSequence, i));
        }
    }

    public final void A1H(C04700Ms r3) {
        AnonymousClass0EP r1 = this.A01;
        if (!r1.A0I) {
            Log.w("BiometricFragment", "Success not sent to client. Client is not awaiting a result.");
        } else {
            r1.A0I = false;
            Executor executor = r1.A0H;
            if (executor == null) {
                executor = new ExecutorC10570eq();
            }
            executor.execute(new RunnableC09470cx(this, r3));
        }
        A18();
    }

    public void A1I(AnonymousClass0U4 r6, C05000Nw r7) {
        ActivityC000900k A0B = A0B();
        if (A0B == null) {
            Log.e("BiometricFragment", "Not launching prompt. Client activity was null.");
            return;
        }
        AnonymousClass0EP r2 = this.A01;
        r2.A06 = r7;
        int A00 = AnonymousClass0QX.A00(r6, r7);
        int i = Build.VERSION.SDK_INT;
        if (i >= 23 && i < 30 && A00 == 15 && r6 == null) {
            r6 = C05560Qa.A01();
        }
        r2.A05 = r6;
        boolean A1J = A1J();
        AnonymousClass0EP r1 = this.A01;
        String str = null;
        if (A1J) {
            str = A0I(R.string.confirm_device_credential_password);
        }
        r1.A0G = str;
        if (i >= 21 && A1J() && new C06150Sj(new AnonymousClass0XW(A0B)).A03(255) != 0) {
            this.A01.A0I = true;
            A1C();
        } else if (this.A01.A0K) {
            this.A00.postDelayed(new RunnableC09160cS(this), 600);
        } else {
            A1A();
        }
    }

    public boolean A1J() {
        AnonymousClass0EP r0;
        C05000Nw r1;
        return (Build.VERSION.SDK_INT > 28 || (r1 = (r0 = this.A01).A06) == null || (AnonymousClass0QX.A00(r0.A05, r1) & 32768) == 0) ? false : true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A1K() {
        /*
            r7 = this;
            int r2 = android.os.Build.VERSION.SDK_INT
            r1 = 28
            if (r2 < r1) goto L_0x005c
            X.00k r6 = r7.A0B()
            if (r6 == 0) goto L_0x0050
            X.0EP r0 = r7.A01
            X.0U4 r0 = r0.A05
            if (r0 == 0) goto L_0x0050
            java.lang.String r5 = android.os.Build.MANUFACTURER
            java.lang.String r4 = android.os.Build.MODEL
            if (r2 != r1) goto L_0x0050
            r1 = 2130903048(0x7f030008, float:1.7412903E38)
            if (r5 == 0) goto L_0x0034
            android.content.res.Resources r0 = r6.getResources()
            java.lang.String[] r3 = r0.getStringArray(r1)
            int r2 = r3.length
            r1 = 0
        L_0x0027:
            if (r1 >= r2) goto L_0x0034
            r0 = r3[r1]
            boolean r0 = r5.equalsIgnoreCase(r0)
            if (r0 != 0) goto L_0x005c
            int r1 = r1 + 1
            goto L_0x0027
        L_0x0034:
            r1 = 2130903047(0x7f030007, float:1.74129E38)
            if (r4 == 0) goto L_0x0052
            android.content.res.Resources r0 = r6.getResources()
            java.lang.String[] r3 = r0.getStringArray(r1)
            int r2 = r3.length
            r1 = 0
        L_0x0043:
            if (r1 >= r2) goto L_0x0052
            r0 = r3[r1]
            boolean r0 = r4.startsWith(r0)
            if (r0 != 0) goto L_0x005c
            int r1 = r1 + 1
            goto L_0x0043
        L_0x0050:
            if (r2 != r1) goto L_0x005e
        L_0x0052:
            android.content.Context r0 = r7.A0p()
            boolean r0 = X.AnonymousClass0KM.A00(r0)
            if (r0 != 0) goto L_0x005e
        L_0x005c:
            r0 = 1
            return r0
        L_0x005e:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.biometric.BiometricFragment.A1K():boolean");
    }
}
