package androidx.biometric;

import X.AnonymousClass03H;
import X.AnonymousClass074;
import androidx.lifecycle.OnLifecycleEvent;

/* loaded from: classes.dex */
public class BiometricPrompt$ResetCallbackObserver implements AnonymousClass03H {
    @OnLifecycleEvent(AnonymousClass074.ON_DESTROY)
    public void resetCallback() {
        throw new NullPointerException("get");
    }
}
