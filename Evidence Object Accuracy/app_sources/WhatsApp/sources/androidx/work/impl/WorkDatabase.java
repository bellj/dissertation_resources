package androidx.work.impl;

import X.AbstractC11470gL;
import X.AbstractC11480gM;
import X.AbstractC11980hB;
import X.AbstractC11990hC;
import X.AbstractC12460hx;
import X.AbstractC12580i9;
import X.AbstractC12700iM;
import X.AnonymousClass0QN;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public abstract class WorkDatabase extends AnonymousClass0QN {
    public static final long A00 = TimeUnit.DAYS.toMillis(1);

    public abstract AbstractC11980hB A06();

    public abstract AbstractC12460hx A07();

    public abstract AbstractC12580i9 A08();

    public abstract AbstractC11470gL A09();

    public abstract AbstractC11480gM A0A();

    public abstract AbstractC12700iM A0B();

    public abstract AbstractC11990hC A0C();
}
