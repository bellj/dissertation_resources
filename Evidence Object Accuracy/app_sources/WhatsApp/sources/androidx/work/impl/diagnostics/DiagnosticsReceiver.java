package androidx.work.impl.diagnostics;

import X.AnonymousClass022;
import X.C004201x;
import X.C06390Tk;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.work.impl.workers.DiagnosticsWorker;

/* loaded from: classes.dex */
public class DiagnosticsReceiver extends BroadcastReceiver {
    public static final String A00 = C06390Tk.A01("DiagnosticsRcvr");

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            C06390Tk A002 = C06390Tk.A00();
            String str = A00;
            A002.A02(str, "Requesting diagnostics", new Throwable[0]);
            try {
                AnonymousClass022.A00(context).A06(new C004201x(DiagnosticsWorker.class).A00());
            } catch (IllegalStateException e) {
                C06390Tk.A00().A03(str, "WorkManager is not initialized", e);
            }
        }
    }
}
