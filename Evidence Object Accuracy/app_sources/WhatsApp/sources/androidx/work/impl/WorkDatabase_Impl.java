package androidx.work.impl;

import X.AbstractC11460gK;
import X.AbstractC11470gL;
import X.AbstractC11480gM;
import X.AbstractC11980hB;
import X.AbstractC11990hC;
import X.AbstractC12460hx;
import X.AbstractC12580i9;
import X.AbstractC12700iM;
import X.C07690Zv;
import X.C07700Zw;
import X.C07710Zx;
import X.C07720Zy;
import X.C07730Zz;
import X.C07740a0;
import X.C07750a1;

/* loaded from: classes.dex */
public final class WorkDatabase_Impl extends WorkDatabase {
    public volatile AbstractC11980hB A00;
    public volatile AbstractC12460hx A01;
    public volatile AbstractC11460gK A02;
    public volatile AbstractC12580i9 A03;
    public volatile AbstractC11470gL A04;
    public volatile AbstractC11480gM A05;
    public volatile AbstractC12700iM A06;
    public volatile AbstractC11990hC A07;

    @Override // androidx.work.impl.WorkDatabase
    public AbstractC11980hB A06() {
        AbstractC11980hB r0;
        if (this.A00 != null) {
            return this.A00;
        }
        synchronized (this) {
            if (this.A00 == null) {
                this.A00 = new C07690Zv(this);
            }
            r0 = this.A00;
        }
        return r0;
    }

    @Override // androidx.work.impl.WorkDatabase
    public AbstractC12460hx A07() {
        AbstractC12460hx r0;
        if (this.A01 != null) {
            return this.A01;
        }
        synchronized (this) {
            if (this.A01 == null) {
                this.A01 = new C07700Zw(this);
            }
            r0 = this.A01;
        }
        return r0;
    }

    @Override // androidx.work.impl.WorkDatabase
    public AbstractC12580i9 A08() {
        AbstractC12580i9 r0;
        if (this.A03 != null) {
            return this.A03;
        }
        synchronized (this) {
            if (this.A03 == null) {
                this.A03 = new C07710Zx(this);
            }
            r0 = this.A03;
        }
        return r0;
    }

    @Override // androidx.work.impl.WorkDatabase
    public AbstractC11470gL A09() {
        AbstractC11470gL r0;
        if (this.A04 != null) {
            return this.A04;
        }
        synchronized (this) {
            if (this.A04 == null) {
                this.A04 = new C07720Zy(this);
            }
            r0 = this.A04;
        }
        return r0;
    }

    @Override // androidx.work.impl.WorkDatabase
    public AbstractC11480gM A0A() {
        AbstractC11480gM r0;
        if (this.A05 != null) {
            return this.A05;
        }
        synchronized (this) {
            if (this.A05 == null) {
                this.A05 = new C07730Zz(this);
            }
            r0 = this.A05;
        }
        return r0;
    }

    @Override // androidx.work.impl.WorkDatabase
    public AbstractC12700iM A0B() {
        AbstractC12700iM r0;
        if (this.A06 != null) {
            return this.A06;
        }
        synchronized (this) {
            if (this.A06 == null) {
                this.A06 = new C07740a0(this);
            }
            r0 = this.A06;
        }
        return r0;
    }

    @Override // androidx.work.impl.WorkDatabase
    public AbstractC11990hC A0C() {
        AbstractC11990hC r0;
        if (this.A07 != null) {
            return this.A07;
        }
        synchronized (this) {
            if (this.A07 == null) {
                this.A07 = new C07750a1(this);
            }
            r0 = this.A07;
        }
        return r0;
    }
}
