package androidx.work.impl.foreground;

import X.AbstractC11970hA;
import X.AnonymousClass09X;
import X.C06390Tk;
import X.C07630Zn;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

/* loaded from: classes.dex */
public class SystemForegroundService extends AnonymousClass09X implements AbstractC11970hA {
    public static SystemForegroundService A04;
    public static final String A05 = C06390Tk.A01("SystemFgService");
    public NotificationManager A00;
    public Handler A01;
    public C07630Zn A02;
    public boolean A03;

    public final void A00() {
        this.A01 = new Handler(Looper.getMainLooper());
        this.A00 = (NotificationManager) getApplicationContext().getSystemService("notification");
        C07630Zn r1 = new C07630Zn(getApplicationContext());
        this.A02 = r1;
        if (r1.A02 != null) {
            C06390Tk.A00().A03(C07630Zn.A0A, "A callback already exists.", new Throwable[0]);
        } else {
            r1.A02 = this;
        }
    }

    @Override // X.AnonymousClass09X, android.app.Service
    public void onCreate() {
        super.onCreate();
        A04 = this;
        A00();
    }

    @Override // X.AnonymousClass09X, android.app.Service
    public void onDestroy() {
        super.onDestroy();
        this.A02.A00();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        if (this.A03) {
            C06390Tk.A00().A04(A05, "Re-initializing SystemForegroundService after a request to shut-down.", new Throwable[0]);
            this.A02.A00();
            A00();
            this.A03 = false;
        }
        if (intent == null) {
            return 3;
        }
        this.A02.A01(intent);
        return 3;
    }

    @Override // X.AbstractC11970hA
    public void stop() {
        this.A03 = true;
        C06390Tk.A00().A02(A05, "All commands completed.", new Throwable[0]);
        if (Build.VERSION.SDK_INT >= 26) {
            stopForeground(true);
        }
        A04 = null;
        stopSelf();
    }
}
