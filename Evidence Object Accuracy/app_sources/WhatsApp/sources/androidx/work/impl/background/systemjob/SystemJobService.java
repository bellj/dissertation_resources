package androidx.work.impl.background.systemjob;

import X.AbstractC11960h9;
import X.AnonymousClass022;
import X.C06390Tk;
import android.app.Application;
import android.app.job.JobParameters;
import android.app.job.JobService;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes.dex */
public class SystemJobService extends JobService implements AbstractC11960h9 {
    public static final String A02 = C06390Tk.A01("SystemJobService");
    public AnonymousClass022 A00;
    public final Map A01 = new HashMap();

    @Override // X.AbstractC11960h9
    public void AQ1(String str, boolean z) {
        JobParameters jobParameters;
        C06390Tk.A00().A02(A02, String.format("%s executed on JobScheduler", str), new Throwable[0]);
        Map map = this.A01;
        synchronized (map) {
            jobParameters = (JobParameters) map.remove(str);
        }
        if (jobParameters != null) {
            jobFinished(jobParameters, z);
        }
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        try {
            AnonymousClass022 A00 = AnonymousClass022.A00(getApplicationContext());
            this.A00 = A00;
            A00.A03.A02(this);
        } catch (IllegalStateException unused) {
            if (Application.class.equals(getApplication().getClass())) {
                C06390Tk.A00().A05(A02, "Could not find WorkManager instance; this may be because an auto-backup is in progress. Ignoring JobScheduler commands for now. Please make sure that you are initializing WorkManager if you have manually disabled WorkManagerInitializer.", new Throwable[0]);
                return;
            }
            throw new IllegalStateException("WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().");
        }
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        AnonymousClass022 r0 = this.A00;
        if (r0 != null) {
            r0.A03.A03(this);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f  */
    @Override // android.app.job.JobService
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onStartJob(android.app.job.JobParameters r9) {
        /*
            r8 = this;
            X.022 r0 = r8.A00
            r4 = 1
            r7 = 0
            if (r0 != 0) goto L_0x0017
            X.0Tk r3 = X.C06390Tk.A00()
            java.lang.String r2 = androidx.work.impl.background.systemjob.SystemJobService.A02
            java.lang.String r1 = "WorkManager is not initialized; requesting retry."
            java.lang.Throwable[] r0 = new java.lang.Throwable[r7]
            r3.A02(r2, r1, r0)
            r8.jobFinished(r9, r4)
            return r7
        L_0x0017:
            java.lang.String r2 = "EXTRA_WORK_SPEC_ID"
            android.os.PersistableBundle r1 = r9.getExtras()     // Catch: NullPointerException -> 0x002a
            if (r1 == 0) goto L_0x002a
            boolean r0 = r1.containsKey(r2)     // Catch: NullPointerException -> 0x002a
            if (r0 == 0) goto L_0x002a
            java.lang.String r5 = r1.getString(r2)     // Catch: NullPointerException -> 0x002a
            goto L_0x002b
        L_0x002a:
            r5 = 0
        L_0x002b:
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 == 0) goto L_0x003f
            X.0Tk r3 = X.C06390Tk.A00()
            java.lang.String r2 = androidx.work.impl.background.systemjob.SystemJobService.A02
            java.lang.String r1 = "WorkSpec id not found!"
            java.lang.Throwable[] r0 = new java.lang.Throwable[r7]
            r3.A03(r2, r1, r0)
            return r7
        L_0x003f:
            java.util.Map r6 = r8.A01
            monitor-enter(r6)
            boolean r0 = r6.containsKey(r5)     // Catch: all -> 0x00bf
            if (r0 == 0) goto L_0x005f
            X.0Tk r3 = X.C06390Tk.A00()     // Catch: all -> 0x00bf
            java.lang.String r2 = androidx.work.impl.background.systemjob.SystemJobService.A02     // Catch: all -> 0x00bf
            java.lang.String r1 = "Job is already being executed by SystemJobService: %s"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch: all -> 0x00bf
            r0[r7] = r5     // Catch: all -> 0x00bf
            java.lang.String r1 = java.lang.String.format(r1, r0)     // Catch: all -> 0x00bf
            java.lang.Throwable[] r0 = new java.lang.Throwable[r7]     // Catch: all -> 0x00bf
            r3.A02(r2, r1, r0)     // Catch: all -> 0x00bf
            monitor-exit(r6)     // Catch: all -> 0x00bf
            return r7
        L_0x005f:
            X.0Tk r3 = X.C06390Tk.A00()     // Catch: all -> 0x00bf
            java.lang.String r2 = androidx.work.impl.background.systemjob.SystemJobService.A02     // Catch: all -> 0x00bf
            java.lang.String r1 = "onStartJob for %s"
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch: all -> 0x00bf
            r0[r7] = r5     // Catch: all -> 0x00bf
            java.lang.String r1 = java.lang.String.format(r1, r0)     // Catch: all -> 0x00bf
            java.lang.Throwable[] r0 = new java.lang.Throwable[r7]     // Catch: all -> 0x00bf
            r3.A02(r2, r1, r0)     // Catch: all -> 0x00bf
            r6.put(r5, r9)     // Catch: all -> 0x00bf
            monitor-exit(r6)     // Catch: all -> 0x00bf
            r3 = 0
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 24
            if (r1 < r0) goto L_0x00ae
            X.0NN r3 = new X.0NN
            r3.<init>()
            android.net.Uri[] r0 = r9.getTriggeredContentUris()
            if (r0 == 0) goto L_0x0094
            android.net.Uri[] r0 = r9.getTriggeredContentUris()
            java.util.List r0 = java.util.Arrays.asList(r0)
            r3.A02 = r0
        L_0x0094:
            java.lang.String[] r0 = r9.getTriggeredContentAuthorities()
            if (r0 == 0) goto L_0x00a4
            java.lang.String[] r0 = r9.getTriggeredContentAuthorities()
            java.util.List r0 = java.util.Arrays.asList(r0)
            r3.A01 = r0
        L_0x00a4:
            r0 = 28
            if (r1 < r0) goto L_0x00ae
            android.net.Network r0 = r9.getNetwork()
            r3.A00 = r0
        L_0x00ae:
            X.022 r2 = r8.A00
            X.0gO r0 = r2.A06
            X.0dp r1 = new X.0dp
            r1.<init>(r3, r2, r5)
            X.0a2 r0 = (X.C07760a2) r0
            X.0eu r0 = r0.A01
            r0.execute(r1)
            return r4
        L_0x00bf:
            r0 = move-exception
            monitor-exit(r6)     // Catch: all -> 0x00bf
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.background.systemjob.SystemJobService.onStartJob(android.app.job.JobParameters):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003c  */
    @Override // android.app.job.JobService
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onStopJob(android.app.job.JobParameters r8) {
        /*
            r7 = this;
            X.022 r0 = r7.A00
            r4 = 1
            r6 = 0
            if (r0 != 0) goto L_0x0014
            X.0Tk r3 = X.C06390Tk.A00()
            java.lang.String r2 = androidx.work.impl.background.systemjob.SystemJobService.A02
            java.lang.String r1 = "WorkManager is not initialized; requesting retry."
            java.lang.Throwable[] r0 = new java.lang.Throwable[r6]
            r3.A02(r2, r1, r0)
            return r4
        L_0x0014:
            java.lang.String r2 = "EXTRA_WORK_SPEC_ID"
            android.os.PersistableBundle r1 = r8.getExtras()     // Catch: NullPointerException -> 0x0027
            if (r1 == 0) goto L_0x0027
            boolean r0 = r1.containsKey(r2)     // Catch: NullPointerException -> 0x0027
            if (r0 == 0) goto L_0x0027
            java.lang.String r5 = r1.getString(r2)     // Catch: NullPointerException -> 0x0027
            goto L_0x0028
        L_0x0027:
            r5 = 0
        L_0x0028:
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            X.0Tk r3 = X.C06390Tk.A00()
            java.lang.String r2 = androidx.work.impl.background.systemjob.SystemJobService.A02
            if (r0 == 0) goto L_0x003c
            java.lang.String r1 = "WorkSpec id not found!"
            java.lang.Throwable[] r0 = new java.lang.Throwable[r6]
            r3.A03(r2, r1, r0)
            return r6
        L_0x003c:
            java.lang.String r1 = "onStopJob for %s"
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r6] = r5
            java.lang.String r1 = java.lang.String.format(r1, r0)
            java.lang.Throwable[] r0 = new java.lang.Throwable[r6]
            r3.A02(r2, r1, r0)
            java.util.Map r1 = r7.A01
            monitor-enter(r1)
            r1.remove(r5)     // Catch: all -> 0x006a
            monitor-exit(r1)     // Catch: all -> 0x006a
            X.022 r0 = r7.A00
            r0.A09(r5)
            X.022 r0 = r7.A00
            X.0Zp r0 = r0.A03
            java.lang.Object r1 = r0.A09
            monitor-enter(r1)
            java.util.Set r0 = r0.A08     // Catch: all -> 0x0067
            boolean r0 = r0.contains(r5)     // Catch: all -> 0x0067
            monitor-exit(r1)     // Catch: all -> 0x0067
            r0 = r0 ^ r4
            return r0
        L_0x0067:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0067
            throw r0
        L_0x006a:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x006a
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.work.impl.background.systemjob.SystemJobService.onStopJob(android.app.job.JobParameters):boolean");
    }
}
