package androidx.work.impl.background.systemalarm;

import X.AnonymousClass022;
import X.C06390Tk;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/* loaded from: classes.dex */
public class RescheduleReceiver extends BroadcastReceiver {
    public static final String A00 = C06390Tk.A01("RescheduleReceiver");

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        C06390Tk A002 = C06390Tk.A00();
        String str = A00;
        A002.A02(str, String.format("Received intent %s", intent), new Throwable[0]);
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                AnonymousClass022 A003 = AnonymousClass022.A00(context);
                BroadcastReceiver.PendingResult goAsync = goAsync();
                synchronized (AnonymousClass022.A0C) {
                    A003.A00 = goAsync;
                    if (A003.A08) {
                        goAsync.finish();
                        A003.A00 = null;
                    }
                }
            } catch (IllegalStateException e) {
                C06390Tk.A00().A03(str, "Cannot reschedule jobs. WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().", e);
            }
        } else {
            Intent intent2 = new Intent(context, SystemAlarmService.class);
            intent2.setAction("ACTION_RESCHEDULE");
            context.startService(intent2);
        }
    }
}
