package androidx.work.impl.background.systemalarm;

import X.AnonymousClass022;
import X.C06390Tk;
import X.C07760a2;
import X.RunnableC10050dw;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* loaded from: classes.dex */
public class ConstraintProxyUpdateReceiver extends BroadcastReceiver {
    public static final String A00 = C06390Tk.A01("ConstrntProxyUpdtRecvr");

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str;
        if (intent != null) {
            str = intent.getAction();
        } else {
            str = null;
        }
        if (!"androidx.work.impl.background.systemalarm.UpdateProxies".equals(str)) {
            C06390Tk.A00().A02(A00, String.format("Ignoring unknown action %s", str), new Throwable[0]);
            return;
        }
        BroadcastReceiver.PendingResult goAsync = goAsync();
        ((C07760a2) AnonymousClass022.A00(context).A06).A01.execute(new RunnableC10050dw(goAsync, context, intent, this));
    }
}
