package androidx.work.impl.background.systemalarm;

import X.AbstractC11420gG;
import X.AnonymousClass09X;
import X.C06390Tk;
import X.C07620Zm;
import android.content.Intent;

/* loaded from: classes.dex */
public class SystemAlarmService extends AnonymousClass09X implements AbstractC11420gG {
    public static final String A02 = C06390Tk.A01("SystemAlarmService");
    public C07620Zm A00;
    public boolean A01;

    public final void A00() {
        C07620Zm r1 = new C07620Zm(this);
        this.A00 = r1;
        if (r1.A01 != null) {
            C06390Tk.A00().A03(C07620Zm.A0A, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
        } else {
            r1.A01 = this;
        }
    }

    @Override // X.AnonymousClass09X, android.app.Service
    public void onCreate() {
        super.onCreate();
        A00();
        this.A01 = false;
    }

    @Override // X.AnonymousClass09X, android.app.Service
    public void onDestroy() {
        super.onDestroy();
        this.A01 = true;
        this.A00.A00();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        if (this.A01) {
            C06390Tk.A00().A04(A02, "Re-initializing SystemAlarmDispatcher after a request to shut-down.", new Throwable[0]);
            this.A00.A00();
            A00();
            this.A01 = false;
        }
        if (intent == null) {
            return 3;
        }
        this.A00.A03(intent, i2);
        return 3;
    }
}
