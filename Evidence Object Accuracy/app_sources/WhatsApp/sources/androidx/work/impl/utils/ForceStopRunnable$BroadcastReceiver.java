package androidx.work.impl.utils;

import X.C06390Tk;
import X.RunnableC10270eJ;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/* loaded from: classes.dex */
public class ForceStopRunnable$BroadcastReceiver extends BroadcastReceiver {
    public static final String A00 = C06390Tk.A01("ForceStopRunnable$Rcvr");

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent != null && "ACTION_FORCE_STOP_RESCHEDULE".equals(intent.getAction())) {
            C06390Tk A002 = C06390Tk.A00();
            String str = A00;
            if (A002.A00 <= 2) {
                Log.v(str, "Rescheduling alarm that keeps track of force-stops.");
            }
            RunnableC10270eJ.A00(context);
        }
    }
}
