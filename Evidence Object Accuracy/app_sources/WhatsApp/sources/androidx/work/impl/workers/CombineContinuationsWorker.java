package androidx.work.impl.workers;

import X.AnonymousClass043;
import X.AnonymousClass0GL;
import android.content.Context;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/* loaded from: classes.dex */
public class CombineContinuationsWorker extends Worker {
    public CombineContinuationsWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        return new AnonymousClass0GL(this.A01.A01);
    }
}
