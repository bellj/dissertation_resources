package androidx.work.impl.workers;

import X.AbstractC12450hw;
import X.AbstractC12700iM;
import X.AbstractFutureC44231yX;
import X.AnonymousClass022;
import X.AnonymousClass040;
import X.AnonymousClass042;
import X.AnonymousClass0GK;
import X.AnonymousClass0S6;
import X.AnonymousClass0Zu;
import X.C004401z;
import X.C06390Tk;
import X.RunnableC09360cm;
import X.RunnableC09710dO;
import android.content.Context;
import android.text.TextUtils;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/* loaded from: classes.dex */
public class ConstraintTrackingWorker extends ListenableWorker implements AbstractC12450hw {
    public static final String A05 = C06390Tk.A01("ConstraintTrkngWrkr");
    public ListenableWorker A00;
    public WorkerParameters A01;
    public AnonymousClass040 A02 = AnonymousClass040.A00();
    public final Object A03 = new Object();
    public volatile boolean A04 = false;

    @Override // X.AbstractC12450hw
    public void AM8(List list) {
    }

    public ConstraintTrackingWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.A01 = workerParameters;
    }

    @Override // androidx.work.ListenableWorker
    public AbstractFutureC44231yX A01() {
        super.A01.A09.execute(new RunnableC09360cm(this));
        return this.A02;
    }

    @Override // androidx.work.ListenableWorker
    public boolean A02() {
        ListenableWorker listenableWorker = this.A00;
        return listenableWorker != null && listenableWorker.A02();
    }

    @Override // androidx.work.ListenableWorker
    public void A03() {
        ListenableWorker listenableWorker = this.A00;
        if (listenableWorker != null && !listenableWorker.A04) {
            ListenableWorker listenableWorker2 = this.A00;
            listenableWorker2.A04 = true;
            listenableWorker2.A03();
        }
    }

    public void A04() {
        this.A02.A09(new AnonymousClass042());
    }

    public void A05() {
        WorkerParameters workerParameters = super.A01;
        String A03 = workerParameters.A01.A03("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME");
        if (TextUtils.isEmpty(A03)) {
            C06390Tk.A00().A03(A05, "No worker to delegate to.", new Throwable[0]);
        } else {
            AnonymousClass0S6 r1 = workerParameters.A04;
            Context context = super.A00;
            ListenableWorker A00 = r1.A00(context, this.A01, A03);
            this.A00 = A00;
            if (A00 == null) {
                C06390Tk.A00().A02(A05, "No worker to delegate to.", new Throwable[0]);
            } else {
                AbstractC12700iM A0B = AnonymousClass022.A00(context).A04.A0B();
                UUID uuid = workerParameters.A08;
                C004401z AHn = A0B.AHn(uuid.toString());
                if (AHn != null) {
                    AnonymousClass0Zu r12 = new AnonymousClass0Zu(context, this, AnonymousClass022.A00(context).A06);
                    r12.A01(Collections.singletonList(AHn));
                    boolean A02 = r12.A02(uuid.toString());
                    C06390Tk A002 = C06390Tk.A00();
                    String str = A05;
                    if (A02) {
                        A002.A02(str, String.format("Constraints met for delegate %s", A03), new Throwable[0]);
                        try {
                            AbstractFutureC44231yX A01 = this.A00.A01();
                            A01.A5i(new RunnableC09710dO(this, A01), workerParameters.A09);
                            return;
                        } catch (Throwable th) {
                            C06390Tk.A00().A02(str, String.format("Delegated worker %s threw exception in startWork.", A03), th);
                            synchronized (this.A03) {
                                try {
                                    if (this.A04) {
                                        C06390Tk.A00().A02(str, "Constraints were unmet, Retrying.", new Throwable[0]);
                                        A04();
                                    } else {
                                        this.A02.A09(new AnonymousClass0GK());
                                    }
                                    return;
                                } catch (Throwable th2) {
                                    throw th2;
                                }
                            }
                        }
                    } else {
                        A002.A02(str, String.format("Constraints not met for delegate %s. Requesting retry.", A03), new Throwable[0]);
                        A04();
                        return;
                    }
                }
            }
        }
        this.A02.A09(new AnonymousClass0GK());
    }

    @Override // X.AbstractC12450hw
    public void AM9(List list) {
        C06390Tk.A00().A02(A05, String.format("Constraints changed for %s", list), new Throwable[0]);
        synchronized (this.A03) {
            this.A04 = true;
        }
    }
}
