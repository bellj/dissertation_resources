package androidx.work.impl.workers;

import X.AbstractC11470gL;
import X.AbstractC11990hC;
import X.AbstractC12580i9;
import X.AbstractC12700iM;
import X.AnonymousClass022;
import X.AnonymousClass043;
import X.AnonymousClass0GL;
import X.AnonymousClass0LB;
import X.AnonymousClass0LC;
import X.AnonymousClass0PC;
import X.AnonymousClass0QN;
import X.AnonymousClass0UK;
import X.AnonymousClass0ZJ;
import X.C004101u;
import X.C004401z;
import X.C006503b;
import X.C06390Tk;
import X.C07720Zy;
import X.C07740a0;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class DiagnosticsWorker extends Worker {
    public static final String A00 = C06390Tk.A01("DiagnosticsWrkr");

    public DiagnosticsWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
    }

    /* JADX INFO: finally extract failed */
    public static String A00(AbstractC12580i9 r9, AbstractC11470gL r10, AbstractC11990hC r11, List list) {
        String str;
        StringBuilder sb = new StringBuilder();
        if (Build.VERSION.SDK_INT >= 23) {
            str = "Job Id";
        } else {
            str = "Alarm Id";
        }
        sb.append(String.format("\n Id \t Class Name\t %s\t State\t Unique Name\t Tags\t", str));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C004401z r6 = (C004401z) it.next();
            Integer num = null;
            AnonymousClass0PC AH2 = r9.AH2(r6.A0E);
            if (AH2 != null) {
                num = Integer.valueOf(AH2.A00);
            }
            String str2 = r6.A0E;
            C07720Zy r3 = (C07720Zy) r10;
            AnonymousClass0ZJ A002 = AnonymousClass0ZJ.A00("SELECT name FROM workname WHERE work_spec_id=?", 1);
            if (str2 == null) {
                A002.A6T(1);
            } else {
                A002.A6U(1, str2);
            }
            AnonymousClass0QN r0 = r3.A01;
            r0.A02();
            Cursor A003 = AnonymousClass0LC.A00(r0, A002, false);
            try {
                ArrayList arrayList = new ArrayList(A003.getCount());
                while (A003.moveToNext()) {
                    arrayList.add(A003.getString(0));
                }
                A003.close();
                A002.A01();
                sb.append(String.format("\n%s\t %s\t %s\t %s\t %s\t %s\t", r6.A0E, r6.A0G, num, r6.A0D.name(), TextUtils.join(",", arrayList), TextUtils.join(",", r11.AH6(r6.A0E))));
            } catch (Throwable th) {
                A003.close();
                A002.A01();
                throw th;
            }
        }
        return sb.toString();
    }

    /* JADX INFO: finally extract failed */
    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        WorkDatabase workDatabase = AnonymousClass022.A00(((ListenableWorker) this).A00).A04;
        AbstractC12700iM A0B = workDatabase.A0B();
        AbstractC11470gL A09 = workDatabase.A09();
        AbstractC11990hC A0C = workDatabase.A0C();
        AbstractC12580i9 A08 = workDatabase.A08();
        AnonymousClass0ZJ A002 = AnonymousClass0ZJ.A00("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE period_start_time >= ? AND state IN (2, 3, 5) ORDER BY period_start_time DESC", 1);
        A002.A6S(1, System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1));
        AnonymousClass0QN r1 = ((C07740a0) A0B).A01;
        r1.A02();
        Cursor A003 = AnonymousClass0LC.A00(r1, A002, false);
        try {
            int A004 = AnonymousClass0LB.A00(A003, "required_network_type");
            int A005 = AnonymousClass0LB.A00(A003, "requires_charging");
            int A006 = AnonymousClass0LB.A00(A003, "requires_device_idle");
            int A007 = AnonymousClass0LB.A00(A003, "requires_battery_not_low");
            int A008 = AnonymousClass0LB.A00(A003, "requires_storage_not_low");
            int A009 = AnonymousClass0LB.A00(A003, "trigger_content_update_delay");
            int A0010 = AnonymousClass0LB.A00(A003, "trigger_max_content_delay");
            int A0011 = AnonymousClass0LB.A00(A003, "content_uri_triggers");
            int A0012 = AnonymousClass0LB.A00(A003, "id");
            int A0013 = AnonymousClass0LB.A00(A003, "state");
            int A0014 = AnonymousClass0LB.A00(A003, "worker_class_name");
            int A0015 = AnonymousClass0LB.A00(A003, "input_merger_class_name");
            int A0016 = AnonymousClass0LB.A00(A003, "input");
            int A0017 = AnonymousClass0LB.A00(A003, "output");
            int A0018 = AnonymousClass0LB.A00(A003, "initial_delay");
            int A0019 = AnonymousClass0LB.A00(A003, "interval_duration");
            int A0020 = AnonymousClass0LB.A00(A003, "flex_duration");
            int A0021 = AnonymousClass0LB.A00(A003, "run_attempt_count");
            int A0022 = AnonymousClass0LB.A00(A003, "backoff_policy");
            int A0023 = AnonymousClass0LB.A00(A003, "backoff_delay_duration");
            int A0024 = AnonymousClass0LB.A00(A003, "period_start_time");
            int A0025 = AnonymousClass0LB.A00(A003, "minimum_retention_duration");
            int A0026 = AnonymousClass0LB.A00(A003, "schedule_requested_at");
            int A0027 = AnonymousClass0LB.A00(A003, "run_in_foreground");
            int A0028 = AnonymousClass0LB.A00(A003, "out_of_quota_policy");
            ArrayList arrayList = new ArrayList(A003.getCount());
            while (A003.moveToNext()) {
                String string = A003.getString(A0012);
                String string2 = A003.getString(A0014);
                C004101u r3 = new C004101u();
                r3.A03 = AnonymousClass0UK.A03(A003.getInt(A004));
                boolean z = false;
                if (A003.getInt(A005) != 0) {
                    z = true;
                }
                r3.A05 = z;
                boolean z2 = false;
                if (A003.getInt(A006) != 0) {
                    z2 = true;
                }
                r3.A02(z2);
                boolean z3 = false;
                if (A003.getInt(A007) != 0) {
                    z3 = true;
                }
                r3.A04 = z3;
                boolean z4 = false;
                if (A003.getInt(A008) != 0) {
                    z4 = true;
                }
                r3.A07 = z4;
                r3.A00 = A003.getLong(A009);
                r3.A01 = A003.getLong(A0010);
                r3.A01(AnonymousClass0UK.A02(A003.getBlob(A0011)));
                C004401z r2 = new C004401z(string, string2);
                r2.A0D = AnonymousClass0UK.A05(A003.getInt(A0013));
                r2.A0F = A003.getString(A0015);
                r2.A0A = C006503b.A00(A003.getBlob(A0016));
                r2.A0B = C006503b.A00(A003.getBlob(A0017));
                r2.A03 = A003.getLong(A0018);
                r2.A04 = A003.getLong(A0019);
                r2.A02 = A003.getLong(A0020);
                r2.A00 = A003.getInt(A0021);
                r2.A08 = AnonymousClass0UK.A01(A003.getInt(A0022));
                r2.A01 = A003.getLong(A0023);
                r2.A06 = A003.getLong(A0024);
                r2.A05 = A003.getLong(A0025);
                r2.A07 = A003.getLong(A0026);
                boolean z5 = false;
                if (A003.getInt(A0027) != 0) {
                    z5 = true;
                }
                r2.A0H = z5;
                r2.A0C = AnonymousClass0UK.A04(A003.getInt(A0028));
                r2.A09 = r3;
                arrayList.add(r2);
            }
            A003.close();
            A002.A01();
            List AGL = A0B.AGL();
            List AAg = A0B.AAg(200);
            if (!arrayList.isEmpty()) {
                C06390Tk A0029 = C06390Tk.A00();
                String str = A00;
                A0029.A04(str, "Recently completed work:\n\n", new Throwable[0]);
                C06390Tk.A00().A04(str, A00(A08, A09, A0C, arrayList), new Throwable[0]);
            }
            if (!AGL.isEmpty()) {
                C06390Tk A0030 = C06390Tk.A00();
                String str2 = A00;
                A0030.A04(str2, "Running work:\n\n", new Throwable[0]);
                C06390Tk.A00().A04(str2, A00(A08, A09, A0C, AGL), new Throwable[0]);
            }
            if (!AAg.isEmpty()) {
                C06390Tk A0031 = C06390Tk.A00();
                String str3 = A00;
                A0031.A04(str3, "Enqueued work:\n\n", new Throwable[0]);
                C06390Tk.A00().A04(str3, A00(A08, A09, A0C, AAg), new Throwable[0]);
            }
            return new AnonymousClass0GL(C006503b.A01);
        } catch (Throwable th) {
            A003.close();
            A002.A01();
            throw th;
        }
    }
}
