package androidx.work;

import X.AbstractFutureC44231yX;
import X.AnonymousClass040;
import X.AnonymousClass043;
import X.RunnableC09330cj;
import android.content.Context;

/* loaded from: classes.dex */
public abstract class Worker extends ListenableWorker {
    public AnonymousClass040 A00;

    public abstract AnonymousClass043 A04();

    public Worker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
    }

    @Override // androidx.work.ListenableWorker
    public final AbstractFutureC44231yX A01() {
        this.A00 = AnonymousClass040.A00();
        this.A01.A09.execute(new RunnableC09330cj(this));
        return this.A00;
    }
}
