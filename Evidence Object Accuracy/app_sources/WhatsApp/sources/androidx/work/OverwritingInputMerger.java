package androidx.work;

import X.AnonymousClass0S5;
import X.C006403a;
import X.C006503b;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes.dex */
public final class OverwritingInputMerger extends AnonymousClass0S5 {
    @Override // X.AnonymousClass0S5
    public C006503b A00(List list) {
        C006403a r3 = new C006403a();
        HashMap hashMap = new HashMap();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            hashMap.putAll(Collections.unmodifiableMap(((C006503b) it.next()).A00));
        }
        r3.A02(hashMap);
        return r3.A00();
    }
}
