package androidx.work;

import X.AbstractFutureC44231yX;
import X.AnonymousClass040;
import android.content.Context;

/* loaded from: classes.dex */
public abstract class ListenableWorker {
    public Context A00;
    public WorkerParameters A01;
    public boolean A02;
    public boolean A03;
    public volatile boolean A04;

    public abstract AbstractFutureC44231yX A01();

    public void A03() {
    }

    public ListenableWorker(Context context, WorkerParameters workerParameters) {
        if (context == null) {
            throw new IllegalArgumentException("Application Context is null");
        } else if (workerParameters != null) {
            this.A00 = context;
            this.A01 = workerParameters;
        } else {
            throw new IllegalArgumentException("WorkerParameters is null");
        }
    }

    public AbstractFutureC44231yX A00() {
        AnonymousClass040 A00 = AnonymousClass040.A00();
        A00.A0A(new IllegalStateException("Expedited WorkRequests require a ListenableWorker to provide an implementation for `getForegroundInfoAsync()`"));
        return A00;
    }

    public boolean A02() {
        return this.A02;
    }
}
