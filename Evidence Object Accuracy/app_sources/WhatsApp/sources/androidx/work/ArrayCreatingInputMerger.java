package androidx.work;

import X.AnonymousClass0S5;
import X.C006403a;
import X.C006503b;
import java.lang.reflect.Array;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public final class ArrayCreatingInputMerger extends AnonymousClass0S5 {
    @Override // X.AnonymousClass0S5
    public C006503b A00(List list) {
        Object newInstance;
        int i;
        C006403a r5 = new C006403a();
        HashMap hashMap = new HashMap();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            for (Map.Entry entry : Collections.unmodifiableMap(((C006503b) it.next()).A00).entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                Class<?> cls = value.getClass();
                Object obj = hashMap.get(key);
                if (obj != null) {
                    Class<?> cls2 = obj.getClass();
                    boolean equals = cls2.equals(cls);
                    boolean isArray = cls2.isArray();
                    if (equals) {
                        if (isArray) {
                            int length = Array.getLength(obj);
                            int length2 = Array.getLength(value);
                            newInstance = Array.newInstance(cls2.getComponentType(), length + length2);
                            System.arraycopy(obj, 0, newInstance, 0, length);
                            System.arraycopy(value, 0, newInstance, length, length2);
                        } else {
                            newInstance = Array.newInstance(cls2, 2);
                            Array.set(newInstance, 0, obj);
                            i = 1;
                        }
                    } else if (isArray && cls2.getComponentType().equals(cls)) {
                        int length3 = Array.getLength(obj);
                        newInstance = Array.newInstance(cls, length3 + 1);
                        System.arraycopy(obj, 0, newInstance, 0, length3);
                        Array.set(newInstance, length3, value);
                    } else if (!cls.isArray() || !cls.getComponentType().equals(cls2)) {
                        throw new IllegalArgumentException();
                    } else {
                        int length4 = Array.getLength(value);
                        newInstance = Array.newInstance(cls2, length4 + 1);
                        System.arraycopy(value, 0, newInstance, 0, length4);
                        Array.set(newInstance, length4, obj);
                    }
                    value = newInstance;
                    hashMap.put(key, value);
                } else if (!cls.isArray()) {
                    newInstance = Array.newInstance(cls, 1);
                    i = 0;
                } else {
                    hashMap.put(key, value);
                }
                Array.set(newInstance, i, value);
                value = newInstance;
                hashMap.put(key, value);
            }
        }
        r5.A02(hashMap);
        return r5.A00();
    }
}
