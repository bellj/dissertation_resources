package androidx.work;

import X.AbstractC11400gE;
import X.AbstractC11500gO;
import X.AbstractC11950h8;
import X.AnonymousClass0NN;
import X.AnonymousClass0S6;
import X.C006503b;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public final class WorkerParameters {
    public int A00;
    public C006503b A01;
    public AbstractC11950h8 A02;
    public AbstractC11400gE A03;
    public AnonymousClass0S6 A04;
    public AnonymousClass0NN A05;
    public AbstractC11500gO A06;
    public Set A07;
    public UUID A08;
    public Executor A09;

    public WorkerParameters(C006503b r2, AbstractC11950h8 r3, AbstractC11400gE r4, AnonymousClass0S6 r5, AnonymousClass0NN r6, AbstractC11500gO r7, Collection collection, UUID uuid, Executor executor, int i) {
        this.A08 = uuid;
        this.A01 = r2;
        this.A07 = new HashSet(collection);
        this.A05 = r6;
        this.A00 = i;
        this.A09 = executor;
        this.A06 = r7;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }
}
