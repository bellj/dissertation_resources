package androidx.recyclerview.widget;

import X.AbstractC015707l;
import X.AbstractC018308n;
import X.AbstractC03990Jy;
import X.AbstractC05270Ox;
import X.AbstractC05520Pw;
import X.AbstractC11340g8;
import X.AbstractC11350g9;
import X.AbstractC11360gA;
import X.AbstractC11880h1;
import X.AbstractC12560i7;
import X.AnonymousClass00N;
import X.AnonymousClass028;
import X.AnonymousClass02D;
import X.AnonymousClass02F;
import X.AnonymousClass02H;
import X.AnonymousClass02M;
import X.AnonymousClass036;
import X.AnonymousClass03U;
import X.AnonymousClass045;
import X.AnonymousClass04Y;
import X.AnonymousClass0B6;
import X.AnonymousClass0DV;
import X.AnonymousClass0E5;
import X.AnonymousClass0F2;
import X.AnonymousClass0F6;
import X.AnonymousClass0L9;
import X.AnonymousClass0LA;
import X.AnonymousClass0N0;
import X.AnonymousClass0OK;
import X.AnonymousClass0QG;
import X.AnonymousClass0QS;
import X.AnonymousClass0QT;
import X.AnonymousClass0UY;
import X.AnonymousClass0Z0;
import X.AnonymousClass0Z5;
import X.AnonymousClass0Z6;
import X.AnonymousClass0Z8;
import X.AnonymousClass0eK;
import X.C003401m;
import X.C04250Ky;
import X.C04810Nd;
import X.C05390Pj;
import X.C05480Ps;
import X.C05760Qv;
import X.C05920Rm;
import X.RunnableC10160e8;
import X.animation.InterpolatorC07060Wn;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.OverScroller;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class RecyclerView extends ViewGroup implements AnonymousClass02D, AnonymousClass02F {
    public static final Interpolator A1B = new animation.InterpolatorC07060Wn();
    public static final boolean A1C;
    public static final boolean A1D;
    public static final boolean A1E;
    public static final int[] A1F = {16842987};
    public static final int[] A1G = {16843830};
    public static final Class[] A1H;
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public VelocityTracker A0D;
    public EdgeEffect A0E;
    public EdgeEffect A0F;
    public EdgeEffect A0G;
    public EdgeEffect A0H;
    public AnonymousClass0UY A0I;
    public AnonymousClass0Z5 A0J;
    public AnonymousClass0QT A0K;
    public AnonymousClass0Z8 A0L;
    public AnonymousClass0eK A0M;
    public AnonymousClass02M A0N;
    public AbstractC11340g8 A0O;
    public AnonymousClass0L9 A0P;
    public AbstractC11350g9 A0Q;
    public AnonymousClass04Y A0R;
    public AnonymousClass02H A0S;
    public AnonymousClass0LA A0T;
    public AbstractC12560i7 A0U;
    public AbstractC05270Ox A0V;
    public AbstractC11880h1 A0W;
    public AnonymousClass0E5 A0X;
    public AnonymousClass0DV A0Y;
    public Runnable A0Z;
    public List A0a;
    public List A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;
    public boolean A0k;
    public boolean A0l;
    public boolean A0m;
    public boolean A0n;
    public boolean A0o;
    public boolean A0p;
    public final int A0q;
    public final int A0r;
    public final Rect A0s;
    public final Rect A0t;
    public final RectF A0u;
    public final AccessibilityManager A0v;
    public final AnonymousClass0QS A0w;
    public final AnonymousClass0F2 A0x;
    public final C05480Ps A0y;
    public final RunnableC10160e8 A0z;
    public final AbstractC11360gA A10;
    public final AnonymousClass0QG A11;
    public final Runnable A12;
    public final ArrayList A13;
    public final ArrayList A14;
    public final List A15;
    public final int[] A16;
    public final int[] A17;
    public final int[] A18;
    public final int[] A19;
    public final int[] A1A;

    public void A0X(int i) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0021, code lost:
        if (r2 == 20) goto L_0x0023;
     */
    static {
        /*
            r3 = 1
            int[] r1 = new int[r3]
            r0 = 16843830(0x1010436, float:2.369658E-38)
            r4 = 0
            r1[r4] = r0
            androidx.recyclerview.widget.RecyclerView.A1G = r1
            int[] r1 = new int[r3]
            r0 = 16842987(0x10100eb, float:2.3694217E-38)
            r1[r4] = r0
            androidx.recyclerview.widget.RecyclerView.A1F = r1
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 18
            if (r2 == r0) goto L_0x0023
            r0 = 19
            if (r2 == r0) goto L_0x0023
            r1 = 20
            r0 = 0
            if (r2 != r1) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            androidx.recyclerview.widget.RecyclerView.A1E = r0
            r1 = 23
            r0 = 0
            if (r2 < r1) goto L_0x002c
            r0 = 1
        L_0x002c:
            androidx.recyclerview.widget.RecyclerView.A1C = r0
            r1 = 21
            r0 = 0
            if (r2 < r1) goto L_0x0034
            r0 = 1
        L_0x0034:
            androidx.recyclerview.widget.RecyclerView.A1D = r0
            r0 = 4
            java.lang.Class[] r2 = new java.lang.Class[r0]
            java.lang.Class<android.content.Context> r0 = android.content.Context.class
            r2[r4] = r0
            java.lang.Class<android.util.AttributeSet> r0 = android.util.AttributeSet.class
            r2[r3] = r0
            r0 = 2
            java.lang.Class r1 = java.lang.Integer.TYPE
            r2[r0] = r1
            r0 = 3
            r2[r0] = r1
            androidx.recyclerview.widget.RecyclerView.A1H = r2
            X.0Wn r0 = new X.0Wn
            r0.<init>()
            androidx.recyclerview.widget.RecyclerView.A1B = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.<clinit>():void");
    }

    public RecyclerView(Context context) {
        this(context, null);
    }

    public RecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:(1:36)(9:38|(1:40)|82|43|(1:45)(1:47)|46|78|48|51)|82|43|(0)(0)|46|78|48|51) */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0239, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x023a, code lost:
        r12 = r10.getConstructor(new java.lang.Class[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x024e, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x024f, code lost:
        r2.initCause(r8);
        r1 = new java.lang.StringBuilder();
        r1.append(r20.getPositionDescription());
        r1.append(": Error creating LayoutManager ");
        r1.append(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x026f, code lost:
        throw new java.lang.IllegalStateException(r1.toString(), r2);
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0203 A[Catch: ClassNotFoundException -> 0x02e8, InvocationTargetException -> 0x02cb, InstantiationException -> 0x02ae, IllegalAccessException -> 0x028f, ClassCastException -> 0x0270, TryCatch #4 {ClassCastException -> 0x0270, ClassNotFoundException -> 0x02e8, IllegalAccessException -> 0x028f, InstantiationException -> 0x02ae, InvocationTargetException -> 0x02cb, blocks: (B:43:0x01fd, B:45:0x0203, B:46:0x020b, B:47:0x0217, B:48:0x021c, B:50:0x023a, B:51:0x0240, B:53:0x024f, B:54:0x026f), top: B:82:0x01fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0217 A[Catch: ClassNotFoundException -> 0x02e8, InvocationTargetException -> 0x02cb, InstantiationException -> 0x02ae, IllegalAccessException -> 0x028f, ClassCastException -> 0x0270, TRY_LEAVE, TryCatch #4 {ClassCastException -> 0x0270, ClassNotFoundException -> 0x02e8, IllegalAccessException -> 0x028f, InstantiationException -> 0x02ae, InvocationTargetException -> 0x02cb, blocks: (B:43:0x01fd, B:45:0x0203, B:46:0x020b, B:47:0x0217, B:48:0x021c, B:50:0x023a, B:51:0x0240, B:53:0x024f, B:54:0x026f), top: B:82:0x01fd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RecyclerView(android.content.Context r19, android.util.AttributeSet r20, int r21) {
        /*
        // Method dump skipped, instructions count: 826
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public static int A00(View view) {
        AnonymousClass03U A01 = A01(view);
        if (A01 != null) {
            return A01.A00();
        }
        return -1;
    }

    public static AnonymousClass03U A01(View view) {
        if (view == null) {
            return null;
        }
        return ((AnonymousClass0B6) view.getLayoutParams()).A00;
    }

    public static RecyclerView A02(View view) {
        if (view instanceof ViewGroup) {
            if (view instanceof RecyclerView) {
                return (RecyclerView) view;
            }
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                RecyclerView A02 = A02(viewGroup.getChildAt(i));
                if (A02 != null) {
                    return A02;
                }
            }
        }
        return null;
    }

    public static void A03(View view, Rect rect) {
        AnonymousClass0B6 r6 = (AnonymousClass0B6) view.getLayoutParams();
        Rect rect2 = r6.A03;
        rect.set((view.getLeft() - rect2.left) - ((ViewGroup.MarginLayoutParams) r6).leftMargin, (view.getTop() - rect2.top) - ((ViewGroup.MarginLayoutParams) r6).topMargin, view.getRight() + rect2.right + ((ViewGroup.MarginLayoutParams) r6).rightMargin, view.getBottom() + rect2.bottom + ((ViewGroup.MarginLayoutParams) r6).bottomMargin);
    }

    public static void A05(AnonymousClass03U r3) {
        WeakReference weakReference = r3.A0D;
        if (weakReference != null) {
            Object obj = weakReference.get();
            while (true) {
                for (View view = (View) obj; view != null; view = null) {
                    if (view != r3.A0H) {
                        obj = view.getParent();
                        if (!(obj instanceof View)) {
                        }
                    } else {
                        return;
                    }
                }
                r3.A0D = null;
                return;
            }
        }
    }

    public int A09(AnonymousClass03U r9) {
        if ((524 & r9.A00) == 0 && r9.A07()) {
            AnonymousClass0Z5 r0 = this.A0J;
            int i = r9.A05;
            ArrayList arrayList = r0.A04;
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                C05390Pj r7 = (C05390Pj) arrayList.get(i2);
                int i3 = r7.A00;
                if (i3 != 1) {
                    if (i3 == 2) {
                        int i4 = r7.A02;
                        if (i4 <= i) {
                            int i5 = r7.A01;
                            i -= i5;
                            if (i4 + i5 > i) {
                            }
                        } else {
                            continue;
                        }
                    } else if (i3 == 8) {
                        int i6 = r7.A02;
                        if (i6 == i) {
                            i = r7.A01;
                        } else {
                            if (i6 < i) {
                                i--;
                            }
                            if (r7.A01 <= i) {
                                i++;
                            }
                        }
                    }
                } else if (r7.A02 <= i) {
                    i += r7.A01;
                }
            }
            return i;
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        if ((r1 & 4) == 0) goto L_0x001f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Rect A0A(android.view.View r11) {
        /*
            r10 = this;
            android.view.ViewGroup$LayoutParams r8 = r11.getLayoutParams()
            X.0B6 r8 = (X.AnonymousClass0B6) r8
            boolean r0 = r8.A01
            if (r0 == 0) goto L_0x001c
            X.0Ps r7 = r10.A0y
            boolean r0 = r7.A08
            if (r0 == 0) goto L_0x001f
            X.03U r0 = r8.A00
            int r1 = r0.A00
            r0 = r1 & 2
            if (r0 != 0) goto L_0x001c
            r0 = r1 & 4
            if (r0 == 0) goto L_0x001f
        L_0x001c:
            android.graphics.Rect r0 = r8.A03
            return r0
        L_0x001f:
            android.graphics.Rect r6 = r8.A03
            r5 = 0
            r6.set(r5, r5, r5, r5)
            java.util.ArrayList r4 = r10.A13
            int r3 = r4.size()
            r2 = 0
        L_0x002c:
            if (r2 >= r3) goto L_0x005b
            android.graphics.Rect r9 = r10.A0s
            r9.set(r5, r5, r5, r5)
            java.lang.Object r0 = r4.get(r2)
            X.08n r0 = (X.AbstractC018308n) r0
            r0.A01(r9, r11, r7, r10)
            int r1 = r6.left
            int r0 = r9.left
            int r1 = r1 + r0
            r6.left = r1
            int r1 = r6.top
            int r0 = r9.top
            int r1 = r1 + r0
            r6.top = r1
            int r1 = r6.right
            int r0 = r9.right
            int r1 = r1 + r0
            r6.right = r1
            int r1 = r6.bottom
            int r0 = r9.bottom
            int r1 = r1 + r0
            r6.bottom = r1
            int r2 = r2 + 1
            goto L_0x002c
        L_0x005b:
            r8.A01 = r5
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0A(android.view.View):android.graphics.Rect");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A0B(android.view.View r3) {
        /*
            r2 = this;
        L_0x0000:
            android.view.ViewParent r1 = r3.getParent()
            if (r1 == 0) goto L_0x0010
            if (r1 == r2) goto L_0x0010
            boolean r0 = r1 instanceof android.view.View
            if (r0 == 0) goto L_0x0010
            r3 = r1
            android.view.View r3 = (android.view.View) r3
            goto L_0x0000
        L_0x0010:
            if (r1 == r2) goto L_0x0013
            r3 = 0
        L_0x0013:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0B(android.view.View):android.view.View");
    }

    public AnonymousClass03U A0C(int i) {
        AnonymousClass03U r1 = null;
        if (!this.A0e) {
            AnonymousClass0QT r5 = this.A0K;
            int A01 = r5.A01();
            for (int i2 = 0; i2 < A01; i2++) {
                AnonymousClass03U A012 = A01(r5.A04(i2));
                if (A012 != null && (A012.A00 & 8) == 0 && A09(A012) == i) {
                    if (!r5.A02.contains(A012.A0H)) {
                        return A012;
                    }
                    r1 = A012;
                }
            }
        }
        return r1;
    }

    public AnonymousClass03U A0D(int i, boolean z) {
        int i2;
        AnonymousClass0QT r6 = this.A0K;
        int A01 = r6.A01();
        AnonymousClass03U r4 = null;
        for (int i3 = 0; i3 < A01; i3++) {
            AnonymousClass03U A012 = A01(r6.A04(i3));
            if (A012 != null && (A012.A00 & 8) == 0) {
                if (z || (i2 = A012.A06) == -1) {
                    i2 = A012.A05;
                }
                if (i2 != i) {
                    continue;
                } else if (!r6.A02.contains(A012.A0H)) {
                    return A012;
                } else {
                    r4 = A012;
                }
            }
        }
        return r4;
    }

    public AnonymousClass03U A0E(View view) {
        ViewParent parent = view.getParent();
        if (parent == null || parent == this) {
            return A01(view);
        }
        StringBuilder sb = new StringBuilder("View ");
        sb.append(view);
        sb.append(" is not a direct child of ");
        sb.append(this);
        throw new IllegalArgumentException(sb.toString());
    }

    public String A0F() {
        StringBuilder sb = new StringBuilder(" ");
        sb.append(super.toString());
        sb.append(", adapter:");
        sb.append(this.A0N);
        sb.append(", layout:");
        sb.append(this.A0S);
        sb.append(", context:");
        sb.append(getContext());
        return sb.toString();
    }

    public void A0G() {
        if (this.A0g && !this.A0e) {
            AnonymousClass0Z5 r5 = this.A0J;
            ArrayList arrayList = r5.A04;
            if (arrayList.size() > 0) {
                int i = r5.A00;
                if ((4 & i) != 0 && (11 & i) == 0) {
                    C003401m.A01("RV PartialInvalidate");
                    A0Q();
                    this.A09++;
                    r5.A04();
                    if (!this.A0n) {
                        AnonymousClass0QT r4 = this.A0K;
                        int A00 = r4.A00();
                        int i2 = 0;
                        while (true) {
                            if (i2 < A00) {
                                AnonymousClass03U A01 = A01(r4.A03(i2));
                                if (A01 != null && !A01.A06() && (A01.A00 & 2) != 0) {
                                    A0H();
                                    break;
                                }
                                i2++;
                            } else {
                                r5.A02();
                                break;
                            }
                        }
                    }
                    A0s(true);
                    A0O();
                    C003401m.A00();
                } else if (arrayList.size() <= 0) {
                    return;
                }
            } else {
                return;
            }
        }
        C003401m.A01("RV FullInvalidate");
        A0H();
        C003401m.A00();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:175:0x03df, code lost:
        if (r22.A0K.A02.contains(getFocusedChild()) == false) goto L_0x03c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x041f, code lost:
        if (r11 != null) goto L_0x0421;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x042f, code lost:
        if (r6.hasFocusable() != false) goto L_0x046d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0279  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0366  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x036d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x03d3  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x03fa  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x042b  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x0439  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x0288 A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x041e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0H() {
        /*
        // Method dump skipped, instructions count: 1158
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0H():void");
    }

    public void A0I() {
        int measuredHeight;
        if (this.A0E == null) {
            EdgeEffect edgeEffect = new EdgeEffect(getContext());
            this.A0E = edgeEffect;
            boolean z = this.A0d;
            int measuredWidth = getMeasuredWidth();
            if (z) {
                measuredWidth = (measuredWidth - getPaddingLeft()) - getPaddingRight();
                measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
            } else {
                measuredHeight = getMeasuredHeight();
            }
            edgeEffect.setSize(measuredWidth, measuredHeight);
        }
    }

    public void A0J() {
        int measuredWidth;
        if (this.A0F == null) {
            EdgeEffect edgeEffect = new EdgeEffect(getContext());
            this.A0F = edgeEffect;
            boolean z = this.A0d;
            int measuredHeight = getMeasuredHeight();
            if (z) {
                measuredHeight = (measuredHeight - getPaddingTop()) - getPaddingBottom();
                measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            } else {
                measuredWidth = getMeasuredWidth();
            }
            edgeEffect.setSize(measuredHeight, measuredWidth);
        }
    }

    public void A0K() {
        int measuredWidth;
        if (this.A0G == null) {
            EdgeEffect edgeEffect = new EdgeEffect(getContext());
            this.A0G = edgeEffect;
            boolean z = this.A0d;
            int measuredHeight = getMeasuredHeight();
            if (z) {
                measuredHeight = (measuredHeight - getPaddingTop()) - getPaddingBottom();
                measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
            } else {
                measuredWidth = getMeasuredWidth();
            }
            edgeEffect.setSize(measuredHeight, measuredWidth);
        }
    }

    public void A0L() {
        int measuredHeight;
        if (this.A0H == null) {
            EdgeEffect edgeEffect = new EdgeEffect(getContext());
            this.A0H = edgeEffect;
            boolean z = this.A0d;
            int measuredWidth = getMeasuredWidth();
            if (z) {
                measuredWidth = (measuredWidth - getPaddingLeft()) - getPaddingRight();
                measuredHeight = (getMeasuredHeight() - getPaddingTop()) - getPaddingBottom();
            } else {
                measuredHeight = getMeasuredHeight();
            }
            edgeEffect.setSize(measuredWidth, measuredHeight);
        }
    }

    public void A0M() {
        if (this.A13.size() != 0) {
            AnonymousClass02H r1 = this.A0S;
            if (r1 != null) {
                r1.A13("Cannot invalidate item decorations during a scroll or layout");
            }
            A0N();
            requestLayout();
        }
    }

    public void A0N() {
        AnonymousClass0QT r4 = this.A0K;
        int A01 = r4.A01();
        for (int i = 0; i < A01; i++) {
            ((AnonymousClass0B6) r4.A04(i).getLayoutParams()).A01 = true;
        }
        ArrayList arrayList = this.A0w.A06;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            AnonymousClass0B6 r1 = (AnonymousClass0B6) ((AnonymousClass03U) arrayList.get(i2)).A0H.getLayoutParams();
            if (r1 != null) {
                r1.A01 = true;
            }
        }
    }

    public void A0O() {
        int i;
        AccessibilityManager accessibilityManager;
        int i2 = this.A09 - 1;
        this.A09 = i2;
        if (i2 < 1) {
            this.A09 = 0;
            int i3 = this.A03;
            this.A03 = 0;
            if (!(i3 == 0 || (accessibilityManager = this.A0v) == null || !accessibilityManager.isEnabled())) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain();
                obtain.setEventType(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
                C04250Ky.A00(obtain, i3);
                sendAccessibilityEventUnchecked(obtain);
            }
            List list = this.A15;
            int size = list.size();
            while (true) {
                size--;
                if (size >= 0) {
                    AnonymousClass03U r3 = (AnonymousClass03U) list.get(size);
                    if (r3.A0H.getParent() == this && !r3.A06() && (i = r3.A04) != -1) {
                        AnonymousClass028.A0a(r3.A0H, i);
                        r3.A04 = -1;
                    }
                } else {
                    list.clear();
                    return;
                }
            }
        }
    }

    public void A0P() {
        AnonymousClass04Y r0 = this.A0R;
        if (r0 != null) {
            r0.A08();
        }
        AnonymousClass02H r02 = this.A0S;
        if (r02 != null) {
            AnonymousClass0QS r1 = this.A0w;
            r02.A0O(r1);
            this.A0S.A0P(r1);
        }
        AnonymousClass0QS r12 = this.A0w;
        r12.A05.clear();
        r12.A02();
    }

    public void A0Q() {
        int i = this.A06 + 1;
        this.A06 = i;
        if (i == 1 && !this.A0m) {
            this.A0n = false;
        }
    }

    public void A0R() {
        AbstractC05520Pw r0;
        setScrollState(0);
        RunnableC10160e8 r1 = this.A0z;
        r1.A06.removeCallbacks(r1);
        r1.A03.abortAnimation();
        AnonymousClass02H r02 = this.A0S;
        if (r02 != null && (r0 = r02.A06) != null) {
            r0.A01();
        }
    }

    public final void A0S() {
        C05920Rm r0;
        long j;
        View focusedChild;
        View A0B;
        AnonymousClass03U A0E;
        long j2;
        int A00;
        int id;
        C05480Ps r6 = this.A0y;
        boolean z = true;
        r6.A01(1);
        if (this.A0B == 2) {
            OverScroller overScroller = this.A0z.A03;
            overScroller.getFinalX();
            overScroller.getCurrX();
            overScroller.getFinalY();
            overScroller.getCurrY();
        }
        r6.A09 = false;
        A0Q();
        AnonymousClass0QG r3 = this.A11;
        AnonymousClass00N r5 = r3.A00;
        r5.clear();
        AnonymousClass036 r2 = r3.A01;
        r2.A05();
        this.A09++;
        A0U();
        if (!this.A0p || !hasFocus() || this.A0N == null || (focusedChild = getFocusedChild()) == null || (A0B = A0B(focusedChild)) == null || (A0E = A0E(A0B)) == null) {
            r6.A07 = -1;
            r6.A01 = -1;
            r6.A02 = -1;
        } else {
            if (this.A0N.A00) {
                j2 = A0E.A08;
            } else {
                j2 = -1;
            }
            r6.A07 = j2;
            if (this.A0e) {
                A00 = -1;
            } else if ((A0E.A00 & 8) != 0) {
                A00 = A0E.A03;
            } else {
                A00 = A0E.A00();
            }
            r6.A01 = A00;
            View view = A0E.A0H;
            loop0: while (true) {
                id = view.getId();
                while (!view.isFocused() && (view instanceof ViewGroup) && view.hasFocus()) {
                    view = ((ViewGroup) view).getFocusedChild();
                    if (view.getId() != -1) {
                        break;
                    }
                }
            }
            r6.A02 = id;
        }
        if (!r6.A0B || !this.A0l) {
            z = false;
        }
        r6.A0D = z;
        this.A0l = false;
        this.A0k = false;
        r6.A08 = r6.A0A;
        r6.A03 = this.A0N.A0D();
        A0t(this.A16);
        if (r6.A0B) {
            AnonymousClass0QT r10 = this.A0K;
            int A002 = r10.A00();
            for (int i = 0; i < A002; i++) {
                AnonymousClass03U A01 = A01(r10.A03(i));
                if (!A01.A06() && ((A01.A00 & 4) == 0 || this.A0N.A00)) {
                    AnonymousClass04Y r02 = this.A0R;
                    AnonymousClass04Y.A00(A01);
                    A01.A01();
                    r3.A02(r02.A01(A01), A01);
                    if (r6.A0D && (A01.A00 & 2) != 0 && (A01.A00 & 8) == 0 && !A01.A06() && (A01.A00 & 4) == 0) {
                        if (this.A0N.A00) {
                            j = A01.A08;
                        } else {
                            j = (long) A01.A05;
                        }
                        r2.A09(j, A01);
                    }
                }
            }
        }
        if (r6.A0A) {
            AnonymousClass0QT r4 = this.A0K;
            int A012 = r4.A01();
            for (int i2 = 0; i2 < A012; i2++) {
                AnonymousClass03U A013 = A01(r4.A04(i2));
                if (!A013.A06() && A013.A03 == -1) {
                    A013.A03 = A013.A05;
                }
            }
            boolean z2 = r6.A0C;
            r6.A0C = false;
            this.A0S.A0u(this.A0w, r6);
            r6.A0C = z2;
            for (int i3 = 0; i3 < r4.A00(); i3++) {
                AnonymousClass03U A014 = A01(r4.A03(i3));
                if (!A014.A06() && ((r0 = (C05920Rm) r5.get(A014)) == null || (r0.A00 & 4) == 0)) {
                    AnonymousClass04Y.A00(A014);
                    boolean z3 = false;
                    if ((8192 & A014.A00) != 0) {
                        z3 = true;
                    }
                    AnonymousClass04Y r03 = this.A0R;
                    A014.A01();
                    AnonymousClass0N0 A015 = r03.A01(A014);
                    if (z3) {
                        A0k(A015, A014);
                    } else {
                        C05920Rm r1 = (C05920Rm) r5.get(A014);
                        if (r1 == null) {
                            r1 = (C05920Rm) C05920Rm.A03.A5a();
                            if (r1 == null) {
                                r1 = new C05920Rm();
                            }
                            r5.put(A014, r1);
                        }
                        r1.A00 |= 2;
                        r1.A02 = A015;
                    }
                }
            }
        }
        AnonymousClass0QT r42 = this.A0K;
        int A016 = r42.A01();
        for (int i4 = 0; i4 < A016; i4++) {
            AnonymousClass03U A017 = A01(r42.A04(i4));
            if (!A017.A06()) {
                A017.A03 = -1;
                A017.A06 = -1;
            }
        }
        AnonymousClass0QS r8 = this.A0w;
        ArrayList arrayList = r8.A06;
        int size = arrayList.size();
        for (int i5 = 0; i5 < size; i5++) {
            AnonymousClass03U r12 = (AnonymousClass03U) arrayList.get(i5);
            r12.A03 = -1;
            r12.A06 = -1;
        }
        ArrayList arrayList2 = r8.A05;
        int size2 = arrayList2.size();
        for (int i6 = 0; i6 < size2; i6++) {
            AnonymousClass03U r13 = (AnonymousClass03U) arrayList2.get(i6);
            r13.A03 = -1;
            r13.A06 = -1;
        }
        ArrayList arrayList3 = r8.A04;
        if (arrayList3 != null) {
            int size3 = arrayList3.size();
            for (int i7 = 0; i7 < size3; i7++) {
                AnonymousClass03U r14 = (AnonymousClass03U) arrayList3.get(i7);
                r14.A03 = -1;
                r14.A06 = -1;
            }
        }
        A0O();
        A0s(false);
        r6.A04 = 2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0034, code lost:
        if (r4.A0R == null) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0T() {
        /*
            r4 = this;
            r4.A0Q()
            int r0 = r4.A09
            int r0 = r0 + 1
            r4.A09 = r0
            X.0Ps r3 = r4.A0y
            r0 = 6
            r3.A01(r0)
            X.0Z5 r0 = r4.A0J
            r0.A03()
            X.02M r0 = r4.A0N
            int r0 = r0.A0D()
            r3.A03 = r0
            r2 = 0
            r3.A00 = r2
            r3.A08 = r2
            X.02H r1 = r4.A0S
            X.0QS r0 = r4.A0w
            r1.A0u(r0, r3)
            r3.A0C = r2
            r0 = 0
            r4.A0X = r0
            boolean r0 = r3.A0B
            if (r0 == 0) goto L_0x0036
            X.04Y r1 = r4.A0R
            r0 = 1
            if (r1 != 0) goto L_0x0037
        L_0x0036:
            r0 = 0
        L_0x0037:
            r3.A0B = r0
            r0 = 4
            r3.A04 = r0
            r4.A0O()
            r4.A0s(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0T():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        if (r4.A0S.A17() == false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0039, code lost:
        if (r4.A0l != false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0050, code lost:
        if (r0 != false) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0U() {
        /*
            r4 = this;
            boolean r0 = r4.A0e
            if (r0 == 0) goto L_0x001c
            X.0Z5 r1 = r4.A0J
            java.util.ArrayList r0 = r1.A04
            r1.A08(r0)
            java.util.ArrayList r0 = r1.A05
            r1.A08(r0)
            r0 = 0
            r1.A00 = r0
            boolean r0 = r4.A0f
            if (r0 == 0) goto L_0x001c
            X.02H r0 = r4.A0S
            r0.A0y(r4)
        L_0x001c:
            X.04Y r0 = r4.A0R
            if (r0 == 0) goto L_0x0029
            X.02H r0 = r4.A0S
            boolean r0 = r0.A17()
            r1 = 1
            if (r0 != 0) goto L_0x002a
        L_0x0029:
            r1 = 0
        L_0x002a:
            X.0Z5 r0 = r4.A0J
            if (r1 == 0) goto L_0x0074
            r0.A04()
        L_0x0031:
            boolean r0 = r4.A0k
            r3 = 0
            if (r0 != 0) goto L_0x003b
            boolean r0 = r4.A0l
            r2 = 0
            if (r0 == 0) goto L_0x003c
        L_0x003b:
            r2 = 1
        L_0x003c:
            X.0Ps r1 = r4.A0y
            boolean r0 = r4.A0g
            if (r0 == 0) goto L_0x0072
            X.04Y r0 = r4.A0R
            if (r0 == 0) goto L_0x0072
            boolean r0 = r4.A0e
            if (r0 != 0) goto L_0x006d
            if (r2 != 0) goto L_0x0052
            X.02H r0 = r4.A0S
            boolean r0 = r0.A0D
        L_0x0050:
            if (r0 == 0) goto L_0x0072
        L_0x0052:
            r0 = 1
        L_0x0053:
            r1.A0B = r0
            if (r0 == 0) goto L_0x006a
            if (r2 == 0) goto L_0x006a
            boolean r0 = r4.A0e
            if (r0 != 0) goto L_0x006a
            X.04Y r0 = r4.A0R
            if (r0 == 0) goto L_0x006a
            X.02H r0 = r4.A0S
            boolean r0 = r0.A17()
            if (r0 == 0) goto L_0x006a
            r3 = 1
        L_0x006a:
            r1.A0A = r3
            return
        L_0x006d:
            X.02M r0 = r4.A0N
            boolean r0 = r0.A00
            goto L_0x0050
        L_0x0072:
            r0 = 0
            goto L_0x0053
        L_0x0074:
            r0.A03()
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0U():void");
    }

    public final void A0V() {
        boolean z;
        VelocityTracker velocityTracker = this.A0D;
        if (velocityTracker != null) {
            velocityTracker.clear();
        }
        AeR(0);
        EdgeEffect edgeEffect = this.A0F;
        if (edgeEffect != null) {
            edgeEffect.onRelease();
            z = this.A0F.isFinished();
        } else {
            z = false;
        }
        EdgeEffect edgeEffect2 = this.A0H;
        if (edgeEffect2 != null) {
            edgeEffect2.onRelease();
            z |= this.A0H.isFinished();
        }
        EdgeEffect edgeEffect3 = this.A0G;
        if (edgeEffect3 != null) {
            edgeEffect3.onRelease();
            z |= this.A0G.isFinished();
        }
        EdgeEffect edgeEffect4 = this.A0E;
        if (edgeEffect4 != null) {
            edgeEffect4.onRelease();
            z |= this.A0E.isFinished();
        }
        if (z) {
            postInvalidateOnAnimation();
        }
    }

    public void A0W(int i) {
        AnonymousClass02H r0 = this.A0S;
        if (r0 != null) {
            r0.A0o(i);
            awakenScrollBars();
        }
    }

    public void A0Y(int i) {
        if (!this.A0m) {
            A0R();
            AnonymousClass02H r0 = this.A0S;
            if (r0 == null) {
                Log.e("RecyclerView", "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument.");
                return;
            }
            r0.A0o(i);
            awakenScrollBars();
        }
    }

    public void A0Z(int i) {
        if (!this.A0m) {
            AnonymousClass02H r1 = this.A0S;
            if (r1 == null) {
                Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
            } else {
                r1.A0x(this.A0y, this, i);
            }
        }
    }

    public void A0a(int i, int i2) {
        boolean z;
        EdgeEffect edgeEffect = this.A0F;
        if (edgeEffect == null || edgeEffect.isFinished() || i <= 0) {
            z = false;
        } else {
            this.A0F.onRelease();
            z = this.A0F.isFinished();
        }
        EdgeEffect edgeEffect2 = this.A0G;
        if (edgeEffect2 != null && !edgeEffect2.isFinished() && i < 0) {
            this.A0G.onRelease();
            z |= this.A0G.isFinished();
        }
        EdgeEffect edgeEffect3 = this.A0H;
        if (edgeEffect3 != null && !edgeEffect3.isFinished() && i2 > 0) {
            this.A0H.onRelease();
            z |= this.A0H.isFinished();
        }
        EdgeEffect edgeEffect4 = this.A0E;
        if (edgeEffect4 != null && !edgeEffect4.isFinished() && i2 < 0) {
            this.A0E.onRelease();
            z |= this.A0E.isFinished();
        }
        if (z) {
            postInvalidateOnAnimation();
        }
    }

    public void A0b(int i, int i2) {
        setMeasuredDimension(AnonymousClass02H.A00(i, getPaddingLeft() + getPaddingRight(), getMinimumWidth()), AnonymousClass02H.A00(i2, getPaddingTop() + getPaddingBottom(), getMinimumHeight()));
    }

    public void A0c(int i, int i2) {
        this.A02++;
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        onScrollChanged(scrollX, scrollY, scrollX, scrollY);
        AbstractC05270Ox r0 = this.A0V;
        if (r0 != null) {
            r0.A01(this, i, i2);
        }
        List list = this.A0b;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                ((AbstractC05270Ox) this.A0b.get(size)).A01(this, i, i2);
            }
        }
        this.A02--;
    }

    public void A0d(int i, int i2) {
        AnonymousClass02H r1 = this.A0S;
        if (r1 == null) {
            Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.A0m) {
            if (!r1.A14()) {
                i = 0;
            }
            if (!r1.A15()) {
                i2 = 0;
            }
            if (i != 0 || i2 != 0) {
                RunnableC10160e8 r2 = this.A0z;
                r2.A02(A1B, i, i2, r2.A00(i, i2));
            }
        }
    }

    public void A0e(int i, int i2, boolean z) {
        int i3 = i + i2;
        AnonymousClass0QT r8 = this.A0K;
        int A01 = r8.A01();
        for (int i4 = 0; i4 < A01; i4++) {
            AnonymousClass03U A012 = A01(r8.A04(i4));
            if (A012 != null && !A012.A06()) {
                int i5 = A012.A05;
                if (i5 >= i3) {
                    A012.A04(-i2, z);
                } else if (i5 >= i) {
                    A012.A00 = 8 | A012.A00;
                    A012.A04(-i2, z);
                    A012.A05 = i - 1;
                }
                this.A0y.A0C = true;
            }
        }
        AnonymousClass0QS r5 = this.A0w;
        ArrayList arrayList = r5.A06;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size >= 0) {
                AnonymousClass03U r2 = (AnonymousClass03U) arrayList.get(size);
                if (r2 != null) {
                    int i6 = r2.A05;
                    if (i6 >= i3) {
                        r2.A04(-i2, z);
                    } else if (i6 >= i) {
                        r2.A00 = 8 | r2.A00;
                        r5.A04(size);
                    }
                }
            } else {
                requestLayout();
                return;
            }
        }
    }

    public void A0f(int i, int[] iArr, int i2) {
        int i3;
        int i4;
        AnonymousClass03U r0;
        A0Q();
        this.A09++;
        C003401m.A01("RV Scroll");
        C05480Ps r2 = this.A0y;
        if (this.A0B == 2) {
            OverScroller overScroller = this.A0z.A03;
            overScroller.getFinalX();
            overScroller.getCurrX();
            overScroller.getFinalY();
            overScroller.getCurrY();
        }
        if (i != 0) {
            i3 = this.A0S.A0Y(this.A0w, r2, i);
        } else {
            i3 = 0;
        }
        if (i2 != 0) {
            i4 = this.A0S.A0Z(this.A0w, r2, i2);
        } else {
            i4 = 0;
        }
        C003401m.A00();
        AnonymousClass0QT r8 = this.A0K;
        int A00 = r8.A00();
        for (int i5 = 0; i5 < A00; i5++) {
            View A03 = r8.A03(i5);
            AnonymousClass03U A0E = A0E(A03);
            if (!(A0E == null || (r0 = A0E.A0B) == null)) {
                View view = r0.A0H;
                int left = A03.getLeft();
                int top = A03.getTop();
                if (left != view.getLeft() || top != view.getTop()) {
                    view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
                }
            }
        }
        A0O();
        A0s(false);
        if (iArr != null) {
            iArr[0] = i3;
            iArr[1] = i4;
        }
    }

    public final void A0g(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.A0A) {
            int i = 0;
            if (actionIndex == 0) {
                i = 1;
            }
            this.A0A = motionEvent.getPointerId(i);
            int x = (int) (motionEvent.getX(i) + 0.5f);
            this.A07 = x;
            this.A04 = x;
            int y = (int) (motionEvent.getY(i) + 0.5f);
            this.A08 = y;
            this.A05 = y;
        }
    }

    public void A0h(View view) {
        AnonymousClass03U A01 = A01(view);
        AnonymousClass02M r0 = this.A0N;
        if (!(r0 == null || A01 == null)) {
            r0.A09(A01);
        }
        List list = this.A0a;
        if (list != null) {
            int size = list.size();
            while (true) {
                size--;
                if (size >= 0) {
                    ((AnonymousClass045) this.A0a.get(size)).ANw(view);
                } else {
                    return;
                }
            }
        }
    }

    public final void A0i(View view, View view2) {
        View view3 = view;
        if (view2 != null) {
            view3 = view2;
        }
        Rect rect = this.A0s;
        rect.set(0, 0, view3.getWidth(), view3.getHeight());
        ViewGroup.LayoutParams layoutParams = view3.getLayoutParams();
        if (layoutParams instanceof AnonymousClass0B6) {
            AnonymousClass0B6 r1 = (AnonymousClass0B6) layoutParams;
            if (!r1.A01) {
                Rect rect2 = r1.A03;
                rect.left -= rect2.left;
                rect.right += rect2.right;
                rect.top -= rect2.top;
                rect.bottom += rect2.bottom;
            }
        }
        if (view2 != null) {
            offsetDescendantRectToMyCoords(view2, rect);
            offsetRectIntoDescendantCoords(view, rect);
        }
        AnonymousClass02H r2 = this.A0S;
        boolean z = !this.A0g;
        boolean z2 = false;
        if (view2 == null) {
            z2 = true;
        }
        r2.A0U(rect, view, this, z, z2);
    }

    public final void A0j(AnonymousClass02M r6, boolean z, boolean z2) {
        AnonymousClass02M r0 = this.A0N;
        if (r0 != null) {
            r0.A01.unregisterObserver(this.A0x);
            this.A0N.A0C(this);
        }
        if (!z || z2) {
            A0P();
        }
        AnonymousClass0Z5 r1 = this.A0J;
        r1.A08(r1.A04);
        r1.A08(r1.A05);
        r1.A00 = 0;
        AnonymousClass02M r2 = this.A0N;
        this.A0N = r6;
        if (r6 != null) {
            r6.A01.registerObserver(this.A0x);
            r6.A0B(this);
        }
        AnonymousClass0QS r12 = this.A0w;
        AnonymousClass02M r4 = this.A0N;
        r12.A05.clear();
        r12.A02();
        AnonymousClass0OK r3 = r12.A02;
        if (r3 == null) {
            r3 = new AnonymousClass0OK();
            r12.A02 = r3;
        }
        if (r2 != null) {
            r3.A00--;
        }
        if (!z && r3.A00 == 0) {
            int i = 0;
            while (true) {
                SparseArray sparseArray = r3.A01;
                if (i >= sparseArray.size()) {
                    break;
                }
                ((C04810Nd) sparseArray.valueAt(i)).A03.clear();
                i++;
            }
        }
        if (r4 != null) {
            r3.A00++;
        }
        this.A0y.A0C = true;
    }

    public void A0k(AnonymousClass0N0 r4, AnonymousClass03U r5) {
        long j;
        r5.A03(0, DefaultCrypto.BUFFER_SIZE);
        if (this.A0y.A0D) {
            int i = r5.A00;
            if ((i & 2) != 0 && (i & 8) == 0 && !r5.A06()) {
                if (this.A0N.A00) {
                    j = r5.A08;
                } else {
                    j = (long) r5.A05;
                }
                this.A11.A01.A09(j, r5);
            }
        }
        this.A11.A02(r4, r5);
    }

    public void A0l(AbstractC018308n r3) {
        AnonymousClass02H r1 = this.A0S;
        if (r1 != null) {
            r1.A13("Cannot add item decoration during a scroll  or layout");
        }
        ArrayList arrayList = this.A13;
        if (arrayList.isEmpty()) {
            setWillNotDraw(false);
        }
        arrayList.add(r3);
        A0N();
        requestLayout();
    }

    public void A0m(AbstractC018308n r4) {
        AnonymousClass02H r1 = this.A0S;
        if (r1 != null) {
            r1.A13("Cannot remove item decoration during a scroll  or layout");
        }
        ArrayList arrayList = this.A13;
        arrayList.remove(r4);
        if (arrayList.isEmpty()) {
            boolean z = false;
            if (getOverScrollMode() == 2) {
                z = true;
            }
            setWillNotDraw(z);
        }
        A0N();
        requestLayout();
    }

    public void A0n(AbstractC05270Ox r2) {
        List list = this.A0b;
        if (list == null) {
            list = new ArrayList();
            this.A0b = list;
        }
        list.add(r2);
    }

    public void A0o(AbstractC05270Ox r2) {
        List list = this.A0b;
        if (list != null) {
            list.remove(r2);
        }
    }

    public final void A0p(AnonymousClass03U r7) {
        View view = r7.A0H;
        boolean z = false;
        if (view.getParent() == this) {
            z = true;
        }
        this.A0w.A09(A0E(view));
        boolean z2 = false;
        if ((r7.A00 & 256) != 0) {
            z2 = true;
        }
        AnonymousClass0QT r2 = this.A0K;
        if (z2) {
            r2.A0A(view, view.getLayoutParams(), -1, true);
        } else if (!z) {
            r2.A09(view, -1, true);
        } else {
            int indexOfChild = ((AnonymousClass0Z0) r2.A01).A00.indexOfChild(view);
            if (indexOfChild >= 0) {
                r2.A00.A04(indexOfChild);
                r2.A07(view);
                return;
            }
            StringBuilder sb = new StringBuilder("view is not a child, cannot hide ");
            sb.append(view);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public void A0q(String str) {
        if (this.A09 > 0) {
            if (str == null) {
                StringBuilder sb = new StringBuilder("Cannot call this method while RecyclerView is computing a layout or scrolling");
                sb.append(A0F());
                throw new IllegalStateException(sb.toString());
            }
            throw new IllegalStateException(str);
        } else if (this.A02 > 0) {
            StringBuilder sb2 = new StringBuilder("");
            sb2.append(A0F());
            Log.w("RecyclerView", "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame.", new IllegalStateException(sb2.toString()));
        }
    }

    public void A0r(boolean z) {
        this.A0f = z | this.A0f;
        this.A0e = true;
        AnonymousClass0QT r5 = this.A0K;
        int A01 = r5.A01();
        for (int i = 0; i < A01; i++) {
            AnonymousClass03U A012 = A01(r5.A04(i));
            if (A012 != null && !A012.A06()) {
                A012.A00 = 6 | A012.A00;
            }
        }
        A0N();
        AnonymousClass0QS r6 = this.A0w;
        ArrayList arrayList = r6.A06;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            AnonymousClass03U r2 = (AnonymousClass03U) arrayList.get(i2);
            if (r2 != null) {
                int i3 = 6 | r2.A00;
                r2.A00 = i3;
                r2.A00 = 1024 | i3;
            }
        }
        AnonymousClass02M r0 = r6.A08.A0N;
        if (r0 == null || !r0.A00) {
            r6.A02();
        }
    }

    public void A0s(boolean z) {
        int i = this.A06;
        if (i < 1) {
            this.A06 = 1;
            i = 1;
        }
        if (!z && !this.A0m) {
            this.A0n = false;
        }
        if (i == 1) {
            if (z && this.A0n && !this.A0m && this.A0S != null && this.A0N != null) {
                A0H();
            }
            if (!this.A0m) {
                this.A0n = false;
            }
        }
        this.A06--;
    }

    public final void A0t(int[] iArr) {
        AnonymousClass0QT r9 = this.A0K;
        int A00 = r9.A00();
        if (A00 == 0) {
            iArr[0] = -1;
            iArr[1] = -1;
            return;
        }
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        for (int i3 = 0; i3 < A00; i3++) {
            AnonymousClass03U A01 = A01(r9.A03(i3));
            if (!A01.A06()) {
                int i4 = A01.A06;
                if (i4 == -1) {
                    i4 = A01.A05;
                }
                if (i4 < i) {
                    i = i4;
                }
                if (i4 > i2) {
                    i2 = i4;
                }
            }
        }
        iArr[0] = i;
        iArr[1] = i2;
    }

    public boolean A0u() {
        return getScrollingChildHelper().A00 != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ee, code lost:
        if (r4 == 0.0f) goto L_0x00c8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0v(android.view.MotionEvent r20, int r21, int r22) {
        /*
        // Method dump skipped, instructions count: 272
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.A0v(android.view.MotionEvent, int, int):boolean");
    }

    public boolean A0w(int[] iArr, int i, int i2, int i3, int i4, int i5) {
        return getScrollingChildHelper().A05(iArr, null, i, i2, i3, i4, i5);
    }

    public boolean A0x(int[] iArr, int[] iArr2, int i, int i2, int i3) {
        return getScrollingChildHelper().A04(iArr, iArr2, i, i2, i3);
    }

    @Override // X.AnonymousClass02D
    public void AeR(int i) {
        getScrollingChildHelper().A00(i);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void addFocusables(ArrayList arrayList, int i, int i2) {
        super.addFocusables(arrayList, i, i2);
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof AnonymousClass0B6) && this.A0S.A18((AnonymousClass0B6) layoutParams);
    }

    @Override // android.view.View
    public int computeHorizontalScrollExtent() {
        AnonymousClass02H r2 = this.A0S;
        if (r2 == null || !r2.A14()) {
            return 0;
        }
        return r2.A0a(this.A0y);
    }

    @Override // android.view.View
    public int computeHorizontalScrollOffset() {
        AnonymousClass02H r2 = this.A0S;
        if (r2 == null || !r2.A14()) {
            return 0;
        }
        return r2.A0b(this.A0y);
    }

    @Override // android.view.View
    public int computeHorizontalScrollRange() {
        AnonymousClass02H r2 = this.A0S;
        if (r2 == null || !r2.A14()) {
            return 0;
        }
        return r2.A0c(this.A0y);
    }

    @Override // android.view.View
    public int computeVerticalScrollExtent() {
        AnonymousClass02H r2 = this.A0S;
        if (r2 == null || !r2.A15()) {
            return 0;
        }
        return r2.A0d(this.A0y);
    }

    @Override // android.view.View
    public int computeVerticalScrollOffset() {
        AnonymousClass02H r2 = this.A0S;
        if (r2 == null || !r2.A15()) {
            return 0;
        }
        return r2.A0e(this.A0y);
    }

    @Override // android.view.View
    public int computeVerticalScrollRange() {
        AnonymousClass02H r2 = this.A0S;
        if (r2 == null || !r2.A15()) {
            return 0;
        }
        return r2.A0f(this.A0y);
    }

    @Override // android.view.View
    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return getScrollingChildHelper().A02(f, f2, z);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreFling(float f, float f2) {
        return getScrollingChildHelper().A01(f, f2);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return getScrollingChildHelper().A04(iArr, iArr2, i, i2, 0);
    }

    @Override // android.view.View
    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return getScrollingChildHelper().A05(iArr, null, i, i2, i3, i4, 0);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004a, code lost:
        if (r0.draw(r8) == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0078, code lost:
        if (r1 == false) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ae, code lost:
        if (r1 == false) goto L_0x00b0;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r8) {
        /*
        // Method dump skipped, instructions count: 281
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.draw(android.graphics.Canvas):void");
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        return super.drawChild(canvas, view, j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0146, code lost:
        if (r3 <= r2) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0032, code lost:
        if (r5.findNextFocus(r11, r12, r0) == null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0034, code lost:
        A0G();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003b, code lost:
        if (A0B(r12) == null) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003d, code lost:
        A0Q();
        r11.A0S.A0h(r12, r11.A0w, r11.A0y, r13);
        A0s(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0050, code lost:
        if (r5 == null) goto L_0x016f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0084, code lost:
        if (r5.findNextFocus(r11, r12, r0) == null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0121, code lost:
        if (r10 > 0) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0011, code lost:
        if (r11.A0m != false) goto L_0x0013;
     */
    @Override // android.view.ViewGroup, android.view.ViewParent
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View focusSearch(android.view.View r12, int r13) {
        /*
        // Method dump skipped, instructions count: 372
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.focusSearch(android.view.View, int):android.view.View");
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        AnonymousClass02H r0 = this.A0S;
        if (r0 != null) {
            return r0.A0i();
        }
        StringBuilder sb = new StringBuilder("RecyclerView has no LayoutManager");
        sb.append(A0F());
        throw new IllegalStateException(sb.toString());
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        AnonymousClass02H r1 = this.A0S;
        if (r1 != null) {
            return r1.A0j(getContext(), attributeSet);
        }
        StringBuilder sb = new StringBuilder("RecyclerView has no LayoutManager");
        sb.append(A0F());
        throw new IllegalStateException(sb.toString());
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        AnonymousClass02H r0 = this.A0S;
        if (r0 != null) {
            return r0.A0k(layoutParams);
        }
        StringBuilder sb = new StringBuilder("RecyclerView has no LayoutManager");
        sb.append(A0F());
        throw new IllegalStateException(sb.toString());
    }

    public AnonymousClass02M getAdapter() {
        return this.A0N;
    }

    @Override // android.view.View
    public int getBaseline() {
        if (this.A0S != null) {
            return -1;
        }
        return super.getBaseline();
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i, int i2) {
        AbstractC11340g8 r0 = this.A0O;
        if (r0 == null) {
            return super.getChildDrawingOrder(i, i2);
        }
        AnonymousClass0F6 r3 = ((AnonymousClass0Z6) r0).A00;
        View view = r3.A0G;
        if (view == null) {
            return i2;
        }
        int i3 = r3.A0A;
        if (i3 == -1) {
            i3 = r3.A0M.indexOfChild(view);
            r3.A0A = i3;
        }
        if (i2 == i - 1) {
            return i3;
        }
        if (i2 >= i3) {
            return i2 + 1;
        }
        return i2;
    }

    @Override // android.view.ViewGroup
    public boolean getClipToPadding() {
        return this.A0d;
    }

    public AnonymousClass0DV getCompatAccessibilityDelegate() {
        return this.A0Y;
    }

    public AnonymousClass0L9 getEdgeEffectFactory() {
        return this.A0P;
    }

    public AnonymousClass04Y getItemAnimator() {
        return this.A0R;
    }

    public int getItemDecorationCount() {
        return this.A13.size();
    }

    public AnonymousClass02H getLayoutManager() {
        return this.A0S;
    }

    public int getMaxFlingVelocity() {
        return this.A0q;
    }

    public int getMinFlingVelocity() {
        return this.A0r;
    }

    public long getNanoTime() {
        if (A1D) {
            return System.nanoTime();
        }
        return 0;
    }

    public AnonymousClass0LA getOnFlingListener() {
        return this.A0T;
    }

    public boolean getPreserveFocusAfterLayout() {
        return this.A0p;
    }

    public AnonymousClass0OK getRecycledViewPool() {
        AnonymousClass0QS r1 = this.A0w;
        AnonymousClass0OK r0 = r1.A02;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0OK r02 = new AnonymousClass0OK();
        r1.A02 = r02;
        return r02;
    }

    public int getScrollState() {
        return this.A0B;
    }

    private AnonymousClass0UY getScrollingChildHelper() {
        AnonymousClass0UY r0 = this.A0I;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0UY r02 = new AnonymousClass0UY(this);
        this.A0I = r02;
        return r02;
    }

    @Override // android.view.View
    public boolean hasNestedScrollingParent() {
        return getScrollingChildHelper().A01 != null;
    }

    @Override // android.view.View
    public boolean isAttachedToWindow() {
        return this.A0j;
    }

    @Override // android.view.View, X.AnonymousClass02E
    public boolean isNestedScrollingEnabled() {
        return getScrollingChildHelper().A02;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A09 = 0;
        boolean z = true;
        this.A0j = true;
        if (!this.A0g || isLayoutRequested()) {
            z = false;
        }
        this.A0g = z;
        AnonymousClass02H r1 = this.A0S;
        if (r1 != null) {
            r1.A0B = true;
        }
        this.A0o = false;
        if (A1D) {
            ThreadLocal threadLocal = AnonymousClass0eK.A05;
            AnonymousClass0eK r0 = (AnonymousClass0eK) threadLocal.get();
            this.A0M = r0;
            if (r0 == null) {
                this.A0M = new AnonymousClass0eK();
                Display A0B = AnonymousClass028.A0B(this);
                float f = 60.0f;
                if (!isInEditMode() && A0B != null) {
                    float refreshRate = A0B.getRefreshRate();
                    if (refreshRate >= 30.0f) {
                        f = refreshRate;
                    }
                }
                AnonymousClass0eK r2 = this.A0M;
                r2.A00 = (long) (1.0E9f / f);
                threadLocal.set(r2);
            }
            this.A0M.A02.add(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        AnonymousClass0eK r0;
        super.onDetachedFromWindow();
        AnonymousClass04Y r02 = this.A0R;
        if (r02 != null) {
            r02.A08();
        }
        A0R();
        this.A0j = false;
        AnonymousClass02H r1 = this.A0S;
        if (r1 != null) {
            AnonymousClass0QS r03 = this.A0w;
            r1.A0B = false;
            r1.A0v(r03, this);
        }
        this.A15.clear();
        removeCallbacks(this.A0Z);
        do {
        } while (C05920Rm.A03.A5a() != null);
        if (A1D && (r0 = this.A0M) != null) {
            r0.A02.remove(this);
            this.A0M = null;
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        ArrayList arrayList = this.A13;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((AbstractC018308n) arrayList.get(i)).A00(canvas, this.A0y, this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
        if (r3 != 0.0f) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003d, code lost:
        if (r2 != 0.0f) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
        A0v(r7, (int) (r2 * r6.A00), (int) (r3 * r6.A01));
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onGenericMotionEvent(android.view.MotionEvent r7) {
        /*
            r6 = this;
            X.02H r0 = r6.A0S
            r5 = 0
            if (r0 == 0) goto L_0x004a
            boolean r0 = r6.A0m
            if (r0 != 0) goto L_0x004a
            int r1 = r7.getAction()
            r0 = 8
            if (r1 != r0) goto L_0x004a
            int r0 = r7.getSource()
            r0 = r0 & 2
            r4 = 0
            if (r0 == 0) goto L_0x004d
            X.02H r0 = r6.A0S
            boolean r0 = r0.A15()
            if (r0 == 0) goto L_0x004b
            r0 = 9
            float r0 = r7.getAxisValue(r0)
            float r3 = -r0
        L_0x0029:
            X.02H r0 = r6.A0S
            boolean r0 = r0.A14()
            if (r0 == 0) goto L_0x0065
            r0 = 10
            float r2 = r7.getAxisValue(r0)
        L_0x0037:
            int r0 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x003f
        L_0x003b:
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x004a
        L_0x003f:
            float r0 = r6.A00
            float r2 = r2 * r0
            int r1 = (int) r2
            float r0 = r6.A01
            float r3 = r3 * r0
            int r0 = (int) r3
            r6.A0v(r7, r1, r0)
        L_0x004a:
            return r5
        L_0x004b:
            r3 = 0
            goto L_0x0029
        L_0x004d:
            int r1 = r7.getSource()
            r0 = 4194304(0x400000, float:5.877472E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x004a
            r0 = 26
            float r2 = r7.getAxisValue(r0)
            X.02H r1 = r6.A0S
            boolean r0 = r1.A15()
            if (r0 == 0) goto L_0x0067
            float r3 = -r2
        L_0x0065:
            r2 = 0
            goto L_0x0037
        L_0x0067:
            boolean r0 = r1.A14()
            if (r0 == 0) goto L_0x004a
            r3 = 0
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onGenericMotionEvent(android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ef, code lost:
        if (r4 != false) goto L_0x00eb;
     */
    @Override // android.view.ViewGroup
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r11) {
        /*
        // Method dump skipped, instructions count: 320
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        C003401m.A01("RV OnLayout");
        A0H();
        C003401m.A00();
        this.A0g = true;
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        C05480Ps r1;
        AnonymousClass02H r12 = this.A0S;
        if (r12 == null) {
            A0b(i, i2);
        } else if (r12.A16()) {
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            AnonymousClass02H r0 = this.A0S;
            C05480Ps r4 = this.A0y;
            r0.A07.A0b(i, i2);
            if ((mode != 1073741824 || mode2 != 1073741824) && this.A0N != null) {
                if (r4.A04 == 1) {
                    A0S();
                }
                this.A0S.A0F(i, i2);
                r4.A09 = true;
                A0T();
                this.A0S.A0G(i, i2);
                if (this.A0S.A0T()) {
                    this.A0S.A0F(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824));
                    r4.A09 = true;
                    A0T();
                    this.A0S.A0G(i, i2);
                }
            }
        } else if (this.A0h) {
            r12.A07.A0b(i, i2);
        } else {
            if (this.A0c) {
                A0Q();
                this.A09++;
                A0U();
                A0O();
                r1 = this.A0y;
                if (r1.A0A) {
                    r1.A08 = true;
                } else {
                    this.A0J.A03();
                    r1.A08 = false;
                }
                this.A0c = false;
                A0s(false);
            } else {
                r1 = this.A0y;
                if (r1.A0A) {
                    setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
                    return;
                }
            }
            AnonymousClass02M r02 = this.A0N;
            if (r02 != null) {
                r1.A03 = r02.A0D();
            } else {
                r1.A03 = 0;
            }
            A0Q();
            this.A0S.A07.A0b(i, i2);
            A0s(false);
            r1.A08 = false;
        }
    }

    @Override // android.view.ViewGroup
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        if (this.A09 > 0) {
            return false;
        }
        return super.onRequestFocusInDescendants(i, rect);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        if (!(parcelable instanceof AnonymousClass0E5)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        AnonymousClass0E5 r3 = (AnonymousClass0E5) parcelable;
        this.A0X = r3;
        super.onRestoreInstanceState(((AbstractC015707l) r3).A00);
        AnonymousClass02H r1 = this.A0S;
        if (r1 != null && (parcelable2 = this.A0X.A00) != null) {
            r1.A0q(parcelable2);
        }
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        Parcelable A0g;
        AnonymousClass0E5 r1 = new AnonymousClass0E5(super.onSaveInstanceState());
        AnonymousClass0E5 r0 = this.A0X;
        if (r0 != null) {
            A0g = r0.A00;
        } else {
            AnonymousClass02H r02 = this.A0S;
            A0g = r02 != null ? r02.A0g() : null;
        }
        r1.A00 = A0g;
        return r1;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3 || i2 != i4) {
            this.A0E = null;
            this.A0H = null;
            this.A0G = null;
            this.A0F = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0046, code lost:
        if (r2 != 1) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0194, code lost:
        if (r12 != false) goto L_0x0147;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004c  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r23) {
        /*
        // Method dump skipped, instructions count: 701
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.RecyclerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.ViewGroup
    public void removeDetachedView(View view, boolean z) {
        AnonymousClass03U A01 = A01(view);
        if (A01 != null) {
            int i = A01.A00;
            if ((i & 256) != 0) {
                A01.A00 = i & -257;
            } else if (!A01.A06()) {
                StringBuilder sb = new StringBuilder("Called removeDetachedView with a view which is not flagged as tmp detached.");
                sb.append(A01);
                sb.append(A0F());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        view.clearAnimation();
        A0h(view);
        super.removeDetachedView(view, z);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestChildFocus(View view, View view2) {
        AbstractC05520Pw r0 = this.A0S.A06;
        if ((r0 == null || !r0.A05) && this.A09 <= 0 && view2 != null) {
            A0i(view, view2);
        }
        super.requestChildFocus(view, view2);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        return this.A0S.A0U(rect, view, this, z, false);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestDisallowInterceptTouchEvent(boolean z) {
        ArrayList arrayList = this.A14;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((AbstractC12560i7) arrayList.get(i)).AV2(z);
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    @Override // android.view.ViewParent, android.view.View
    public void requestLayout() {
        if (this.A06 != 0 || this.A0m) {
            this.A0n = true;
        } else {
            super.requestLayout();
        }
    }

    @Override // android.view.View
    public void scrollBy(int i, int i2) {
        AnonymousClass02H r2 = this.A0S;
        if (r2 == null) {
            Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
        } else if (!this.A0m) {
            boolean A14 = r2.A14();
            boolean A15 = r2.A15();
            if (!A14) {
                if (A15) {
                    i = 0;
                } else {
                    return;
                }
            } else if (!A15) {
                i2 = 0;
            }
            A0v(null, i, i2);
        }
    }

    @Override // android.view.View
    public void scrollTo(int i, int i2) {
        Log.w("RecyclerView", "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead");
    }

    @Override // android.view.View, android.view.accessibility.AccessibilityEventSource
    public void sendAccessibilityEventUnchecked(AccessibilityEvent accessibilityEvent) {
        int A00;
        if (this.A09 > 0) {
            int i = 0;
            if (!(accessibilityEvent == null || Build.VERSION.SDK_INT < 19 || (A00 = C05760Qv.A00(accessibilityEvent)) == 0)) {
                i = A00;
            }
            this.A03 |= i;
            return;
        }
        super.sendAccessibilityEventUnchecked(accessibilityEvent);
    }

    public void setAccessibilityDelegateCompat(AnonymousClass0DV r1) {
        this.A0Y = r1;
        AnonymousClass028.A0g(this, r1);
    }

    public void setAdapter(AnonymousClass02M r3) {
        setLayoutFrozen(false);
        A0j(r3, false, true);
        A0r(false);
        requestLayout();
    }

    public void setChildDrawingOrderCallback(AbstractC11340g8 r2) {
        if (r2 != this.A0O) {
            this.A0O = r2;
            boolean z = false;
            if (r2 != null) {
                z = true;
            }
            setChildrenDrawingOrderEnabled(z);
        }
    }

    @Override // android.view.ViewGroup
    public void setClipToPadding(boolean z) {
        if (z != this.A0d) {
            this.A0E = null;
            this.A0H = null;
            this.A0G = null;
            this.A0F = null;
        }
        this.A0d = z;
        super.setClipToPadding(z);
        if (this.A0g) {
            requestLayout();
        }
    }

    public void setEdgeEffectFactory(AnonymousClass0L9 r2) {
        this.A0P = r2;
        this.A0E = null;
        this.A0H = null;
        this.A0G = null;
        this.A0F = null;
    }

    public void setHasFixedSize(boolean z) {
        this.A0h = z;
    }

    public void setItemAnimator(AnonymousClass04Y r3) {
        AnonymousClass04Y r0 = this.A0R;
        if (r0 != null) {
            r0.A08();
            this.A0R.A04 = null;
        }
        this.A0R = r3;
        if (r3 != null) {
            r3.A04 = this.A0Q;
        }
    }

    public void setItemViewCacheSize(int i) {
        AnonymousClass0QS r0 = this.A0w;
        r0.A00 = i;
        r0.A03();
    }

    public void setLayoutFrozen(boolean z) {
        if (z != this.A0m) {
            A0q("Do not setLayoutFrozen in layout or scroll");
            if (!z) {
                this.A0m = false;
                if (!(!this.A0n || this.A0S == null || this.A0N == null)) {
                    requestLayout();
                }
                this.A0n = false;
                return;
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            onTouchEvent(MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0));
            this.A0m = true;
            this.A0i = true;
            A0R();
        }
    }

    public void setLayoutManager(AnonymousClass02H r7) {
        AnonymousClass0QS r4;
        if (r7 != this.A0S) {
            A0R();
            if (this.A0S != null) {
                AnonymousClass04Y r0 = this.A0R;
                if (r0 != null) {
                    r0.A08();
                }
                AnonymousClass02H r02 = this.A0S;
                r4 = this.A0w;
                r02.A0O(r4);
                this.A0S.A0P(r4);
                r4.A05.clear();
                r4.A02();
                if (this.A0j) {
                    AnonymousClass02H r1 = this.A0S;
                    r1.A0B = false;
                    r1.A0v(r4, this);
                }
                AnonymousClass02H r2 = this.A0S;
                r2.A07 = null;
                r2.A05 = null;
                r2.A03 = 0;
                r2.A00 = 0;
                r2.A04 = 1073741824;
                r2.A01 = 1073741824;
                this.A0S = null;
            } else {
                r4 = this.A0w;
                r4.A05.clear();
                r4.A02();
            }
            AnonymousClass0QT r5 = this.A0K;
            r5.A00.A01();
            List list = r5.A02;
            int size = list.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                r5.A01.ARl((View) list.get(size));
                list.remove(size);
            }
            RecyclerView recyclerView = ((AnonymousClass0Z0) r5.A01).A00;
            int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                recyclerView.A0h(childAt);
                childAt.clearAnimation();
            }
            recyclerView.removeAllViews();
            this.A0S = r7;
            if (r7 != null) {
                if (r7.A07 == null) {
                    r7.A07 = this;
                    r7.A05 = r5;
                    r7.A03 = getWidth();
                    r7.A00 = getHeight();
                    r7.A04 = 1073741824;
                    r7.A01 = 1073741824;
                    if (this.A0j) {
                        this.A0S.A0B = true;
                    }
                } else {
                    StringBuilder sb = new StringBuilder("LayoutManager ");
                    sb.append(r7);
                    sb.append(" is already attached to a RecyclerView:");
                    sb.append(r7.A07.A0F());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            r4.A03();
            requestLayout();
        }
    }

    @Override // android.view.View, X.AnonymousClass02E
    public void setNestedScrollingEnabled(boolean z) {
        AnonymousClass0UY scrollingChildHelper = getScrollingChildHelper();
        if (scrollingChildHelper.A02) {
            AnonymousClass028.A0T(scrollingChildHelper.A04);
        }
        scrollingChildHelper.A02 = z;
    }

    public void setOnFlingListener(AnonymousClass0LA r1) {
        this.A0T = r1;
    }

    @Deprecated
    public void setOnScrollListener(AbstractC05270Ox r1) {
        this.A0V = r1;
    }

    public void setPreserveFocusAfterLayout(boolean z) {
        this.A0p = z;
    }

    public void setRecycledViewPool(AnonymousClass0OK r4) {
        AnonymousClass0QS r2 = this.A0w;
        AnonymousClass0OK r1 = r2.A02;
        if (r1 != null) {
            r1.A00--;
        }
        r2.A02 = r4;
        if (r4 != null && r2.A08.A0N != null) {
            r4.A00++;
        }
    }

    public void setRecyclerListener(AbstractC11880h1 r1) {
        this.A0W = r1;
    }

    public void setScrollState(int i) {
        AbstractC05520Pw r0;
        if (i != this.A0B) {
            this.A0B = i;
            if (i != 2) {
                RunnableC10160e8 r1 = this.A0z;
                r1.A06.removeCallbacks(r1);
                r1.A03.abortAnimation();
                AnonymousClass02H r02 = this.A0S;
                if (!(r02 == null || (r0 = r02.A06) == null)) {
                    r0.A01();
                }
            }
            AnonymousClass02H r03 = this.A0S;
            if (r03 != null) {
                r03.A0n(i);
            }
            A0X(i);
            AbstractC05270Ox r04 = this.A0V;
            if (r04 != null) {
                r04.A00(this, i);
            }
            List list = this.A0b;
            if (list != null) {
                int size = list.size();
                while (true) {
                    size--;
                    if (size >= 0) {
                        ((AbstractC05270Ox) this.A0b.get(size)).A00(this, i);
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void setScrollingTouchSlop(int i) {
        int i2;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        if (i != 0) {
            if (i != 1) {
                StringBuilder sb = new StringBuilder("setScrollingTouchSlop(): bad argument constant ");
                sb.append(i);
                sb.append("; using default value");
                Log.w("RecyclerView", sb.toString());
            } else {
                i2 = viewConfiguration.getScaledPagingTouchSlop();
                this.A0C = i2;
            }
        }
        i2 = viewConfiguration.getScaledTouchSlop();
        this.A0C = i2;
    }

    public void setViewCacheExtension(AbstractC03990Jy r2) {
        this.A0w.A03 = r2;
    }

    @Override // android.view.View
    public boolean startNestedScroll(int i) {
        return getScrollingChildHelper().A03(i, 0);
    }

    @Override // android.view.View, X.AnonymousClass02E
    public void stopNestedScroll() {
        getScrollingChildHelper().A00(0);
    }
}
