package androidx.recyclerview.widget;

import X.AbstractC05520Pw;
import X.AbstractC06220Sq;
import X.AbstractC11870h0;
import X.AnonymousClass028;
import X.AnonymousClass02H;
import X.AnonymousClass02I;
import X.AnonymousClass04Z;
import X.AnonymousClass0B6;
import X.AnonymousClass0FA;
import X.AnonymousClass0FE;
import X.AnonymousClass0QK;
import X.AnonymousClass0QS;
import X.AnonymousClass0QU;
import X.AnonymousClass0TD;
import X.C04800Nc;
import X.C05170On;
import X.C05210Or;
import X.C05480Ps;
import X.C06410Tm;
import X.C06840Vh;
import X.C06890Vm;
import X.RunnableC09320ci;
import android.content.Context;
import android.graphics.Rect;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.Arrays;
import java.util.BitSet;

/* loaded from: classes.dex */
public class StaggeredGridLayoutManager extends AnonymousClass02H implements AnonymousClass02I {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public AbstractC06220Sq A07;
    public AbstractC06220Sq A08;
    public AnonymousClass0QK A09;
    public C06890Vm A0A;
    public BitSet A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public int[] A0I;
    public AnonymousClass0QU[] A0J;
    public final Rect A0K;
    public final C05210Or A0L;
    public final C05170On A0M;
    public final Runnable A0N;

    public StaggeredGridLayoutManager(int i) {
        this.A06 = -1;
        this.A0F = false;
        this.A0G = false;
        this.A03 = -1;
        this.A04 = Integer.MIN_VALUE;
        this.A09 = new AnonymousClass0QK();
        this.A01 = 2;
        this.A0K = new Rect();
        this.A0M = new C05170On(this);
        this.A0C = false;
        this.A0H = true;
        this.A0N = new RunnableC09320ci(this);
        this.A02 = 1;
        A1K(i);
        this.A0L = new C05210Or();
        this.A07 = AbstractC06220Sq.A00(this, this.A02);
        this.A08 = AbstractC06220Sq.A00(this, 1 - this.A02);
    }

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        this.A06 = -1;
        this.A0F = false;
        this.A0G = false;
        this.A03 = -1;
        this.A04 = Integer.MIN_VALUE;
        this.A09 = new AnonymousClass0QK();
        this.A01 = 2;
        this.A0K = new Rect();
        this.A0M = new C05170On(this);
        this.A0C = false;
        this.A0H = true;
        this.A0N = new RunnableC09320ci(this);
        C04800Nc A03 = AnonymousClass02H.A03(context, attributeSet, i, i2);
        int i3 = A03.A00;
        if (i3 == 0 || i3 == 1) {
            A13(null);
            if (i3 != this.A02) {
                this.A02 = i3;
                AbstractC06220Sq r1 = this.A07;
                this.A07 = this.A08;
                this.A08 = r1;
                A0E();
            }
            A1K(A03.A01);
            boolean z = A03.A02;
            A13(null);
            C06890Vm r12 = this.A0A;
            if (!(r12 == null || r12.A07 == z)) {
                r12.A07 = z;
            }
            this.A0F = z;
            A0E();
            this.A0L = new C05210Or();
            this.A07 = AbstractC06220Sq.A00(this, this.A02);
            this.A08 = AbstractC06220Sq.A00(this, 1 - this.A02);
            return;
        }
        throw new IllegalArgumentException("invalid orientation.");
    }

    public static final int A06(int i, int i2, int i3) {
        int mode;
        if ((i2 != 0 || i3 != 0) && ((mode = View.MeasureSpec.getMode(i)) == Integer.MIN_VALUE || mode == 1073741824)) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i) - i2) - i3), mode);
        }
        return i;
    }

    @Override // X.AnonymousClass02H
    public int A0W(AnonymousClass0QS r3, C05480Ps r4) {
        if (this.A02 == 1) {
            return this.A06;
        }
        return super.A0W(r3, r4);
    }

    @Override // X.AnonymousClass02H
    public int A0X(AnonymousClass0QS r2, C05480Ps r3) {
        if (this.A02 == 0) {
            return this.A06;
        }
        return super.A0X(r2, r3);
    }

    @Override // X.AnonymousClass02H
    public int A0Y(AnonymousClass0QS r2, C05480Ps r3, int i) {
        return A1E(r2, r3, i);
    }

    @Override // X.AnonymousClass02H
    public int A0Z(AnonymousClass0QS r2, C05480Ps r3, int i) {
        return A1E(r2, r3, i);
    }

    @Override // X.AnonymousClass02H
    public int A0a(C05480Ps r8) {
        if (A06() == 0) {
            return 0;
        }
        AbstractC06220Sq r3 = this.A07;
        boolean z = this.A0H;
        boolean z2 = !z;
        return AnonymousClass0TD.A00(A1I(z2), A1H(z2), r3, this, r8, z);
    }

    @Override // X.AnonymousClass02H
    public int A0b(C05480Ps r2) {
        return A1F(r2);
    }

    @Override // X.AnonymousClass02H
    public int A0c(C05480Ps r8) {
        if (A06() == 0) {
            return 0;
        }
        AbstractC06220Sq r3 = this.A07;
        boolean z = this.A0H;
        boolean z2 = !z;
        return AnonymousClass0TD.A01(A1I(z2), A1H(z2), r3, this, r8, z);
    }

    @Override // X.AnonymousClass02H
    public int A0d(C05480Ps r8) {
        if (A06() == 0) {
            return 0;
        }
        AbstractC06220Sq r3 = this.A07;
        boolean z = this.A0H;
        boolean z2 = !z;
        return AnonymousClass0TD.A00(A1I(z2), A1H(z2), r3, this, r8, z);
    }

    @Override // X.AnonymousClass02H
    public int A0e(C05480Ps r2) {
        return A1F(r2);
    }

    @Override // X.AnonymousClass02H
    public int A0f(C05480Ps r8) {
        if (A06() == 0) {
            return 0;
        }
        AbstractC06220Sq r3 = this.A07;
        boolean z = this.A0H;
        boolean z2 = !z;
        return AnonymousClass0TD.A01(A1I(z2), A1H(z2), r3, this, r8, z);
    }

    @Override // X.AnonymousClass02H
    public Parcelable A0g() {
        int A19;
        View A1I;
        int A02;
        int A03;
        int A06;
        C06890Vm r0 = this.A0A;
        if (r0 != null) {
            return new C06890Vm(r0);
        }
        C06890Vm r4 = new C06890Vm();
        r4.A07 = this.A0F;
        r4.A05 = this.A0D;
        r4.A06 = this.A0E;
        AnonymousClass0QK r1 = this.A09;
        int[] iArr = r1.A01;
        if (iArr != null) {
            r4.A08 = iArr;
            r4.A01 = iArr.length;
            r4.A04 = r1.A00;
        } else {
            r4.A01 = 0;
        }
        if (A06() > 0) {
            if (this.A0D) {
                A19 = A1A();
            } else {
                A19 = A19();
            }
            r4.A00 = A19;
            if (this.A0G) {
                A1I = A1H(true);
            } else {
                A1I = A1I(true);
            }
            if (A1I == null) {
                A02 = -1;
            } else {
                A02 = AnonymousClass02H.A02(A1I);
            }
            r4.A03 = A02;
            int i = this.A06;
            r4.A02 = i;
            r4.A09 = new int[i];
            for (int i2 = 0; i2 < this.A06; i2++) {
                boolean z = this.A0D;
                AnonymousClass0QU r02 = this.A0J[i2];
                if (z) {
                    A03 = r02.A02(Integer.MIN_VALUE);
                    if (A03 != Integer.MIN_VALUE) {
                        A06 = this.A07.A02();
                        A03 -= A06;
                        r4.A09[i2] = A03;
                    } else {
                        r4.A09[i2] = A03;
                    }
                } else {
                    A03 = r02.A03(Integer.MIN_VALUE);
                    if (A03 != Integer.MIN_VALUE) {
                        A06 = this.A07.A06();
                        A03 -= A06;
                        r4.A09[i2] = A03;
                    } else {
                        r4.A09[i2] = A03;
                    }
                }
            }
            return r4;
        }
        r4.A00 = -1;
        r4.A03 = -1;
        r4.A02 = 0;
        return r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0088, code lost:
        if (r11.A02 == 0) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0093, code lost:
        if (A1U() != false) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009e, code lost:
        if (A1U() != false) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00a3, code lost:
        if (r11.A02 == 1) goto L_0x00a5;
     */
    @Override // X.AnonymousClass02H
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A0h(android.view.View r12, X.AnonymousClass0QS r13, X.C05480Ps r14, int r15) {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A0h(android.view.View, X.0QS, X.0Ps, int):android.view.View");
    }

    @Override // X.AnonymousClass02H
    public AnonymousClass0B6 A0i() {
        if (this.A02 == 0) {
            return new AnonymousClass0FA(-2, -1);
        }
        return new AnonymousClass0FA(-1, -2);
    }

    @Override // X.AnonymousClass02H
    public AnonymousClass0B6 A0j(Context context, AttributeSet attributeSet) {
        return new AnonymousClass0FA(context, attributeSet);
    }

    @Override // X.AnonymousClass02H
    public AnonymousClass0B6 A0k(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new AnonymousClass0FA((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new AnonymousClass0FA(layoutParams);
    }

    @Override // X.AnonymousClass02H
    public void A0l(int i) {
        super.A0l(i);
        for (int i2 = 0; i2 < this.A06; i2++) {
            AnonymousClass0QU r2 = this.A0J[i2];
            int i3 = r2.A01;
            if (i3 != Integer.MIN_VALUE) {
                r2.A01 = i3 + i;
            }
            int i4 = r2.A00;
            if (i4 != Integer.MIN_VALUE) {
                r2.A00 = i4 + i;
            }
        }
    }

    @Override // X.AnonymousClass02H
    public void A0m(int i) {
        super.A0m(i);
        for (int i2 = 0; i2 < this.A06; i2++) {
            AnonymousClass0QU r2 = this.A0J[i2];
            int i3 = r2.A01;
            if (i3 != Integer.MIN_VALUE) {
                r2.A01 = i3 + i;
            }
            int i4 = r2.A00;
            if (i4 != Integer.MIN_VALUE) {
                r2.A00 = i4 + i;
            }
        }
    }

    @Override // X.AnonymousClass02H
    public void A0n(int i) {
        if (i == 0) {
            A1T();
        }
    }

    @Override // X.AnonymousClass02H
    public void A0o(int i) {
        C06890Vm r1 = this.A0A;
        if (!(r1 == null || r1.A00 == i)) {
            r1.A09 = null;
            r1.A02 = 0;
            r1.A00 = -1;
            r1.A03 = -1;
        }
        this.A03 = i;
        this.A04 = Integer.MIN_VALUE;
        A0E();
    }

    @Override // X.AnonymousClass02H
    public void A0p(Rect rect, int i, int i2) {
        int A00;
        int A002;
        int A09 = A09() + A0A();
        int A0B = A0B() + A08();
        if (this.A02 == 1) {
            A002 = AnonymousClass02H.A00(i2, rect.height() + A0B, super.A07.getMinimumHeight());
            A00 = AnonymousClass02H.A00(i, (this.A05 * this.A06) + A09, super.A07.getMinimumWidth());
        } else {
            A00 = AnonymousClass02H.A00(i, rect.width() + A09, super.A07.getMinimumWidth());
            A002 = AnonymousClass02H.A00(i2, (this.A05 * this.A06) + A0B, super.A07.getMinimumHeight());
        }
        super.A07.setMeasuredDimension(A00, A002);
    }

    @Override // X.AnonymousClass02H
    public void A0q(Parcelable parcelable) {
        if (parcelable instanceof C06890Vm) {
            this.A0A = (C06890Vm) parcelable;
            A0E();
        }
    }

    @Override // X.AnonymousClass02H
    public void A0r(View view, AnonymousClass04Z r10, AnonymousClass0QS r11, C05480Ps r12) {
        int i;
        int i2;
        C06410Tm A01;
        int i3;
        int i4;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof AnonymousClass0FA)) {
            super.A0K(view, r10);
            return;
        }
        AnonymousClass0FA r1 = (AnonymousClass0FA) layoutParams;
        if (this.A02 == 0) {
            AnonymousClass0QU r0 = r1.A00;
            if (r0 == null) {
                i3 = -1;
            } else {
                i3 = r0.A04;
            }
            boolean z = r1.A01;
            if (z) {
                i4 = this.A06;
            } else {
                i4 = 1;
            }
            A01 = C06410Tm.A01(i3, i4, -1, -1, z, false);
        } else {
            AnonymousClass0QU r02 = r1.A00;
            if (r02 == null) {
                i = -1;
            } else {
                i = r02.A04;
            }
            boolean z2 = r1.A01;
            if (z2) {
                i2 = this.A06;
            } else {
                i2 = 1;
            }
            A01 = C06410Tm.A01(-1, -1, i, i2, z2, false);
        }
        r10.A0J(A01);
    }

    @Override // X.AnonymousClass02H
    public void A0s(AccessibilityEvent accessibilityEvent) {
        super.A0s(accessibilityEvent);
        if (A06() > 0) {
            View A1I = A1I(false);
            View A1H = A1H(false);
            if (A1I != null && A1H != null) {
                int A02 = AnonymousClass02H.A02(A1I);
                int A022 = AnonymousClass02H.A02(A1H);
                if (A02 < A022) {
                    accessibilityEvent.setFromIndex(A02);
                    accessibilityEvent.setToIndex(A022);
                    return;
                }
                accessibilityEvent.setFromIndex(A022);
                accessibilityEvent.setToIndex(A02);
            }
        }
    }

    @Override // X.AnonymousClass02H
    public void A0t(AbstractC11870h0 r7, C05480Ps r8, int i, int i2) {
        int A02;
        int i3;
        if (this.A02 != 0) {
            i = i2;
        }
        if (!(A06() == 0 || i == 0)) {
            A1Q(r8, i);
            int[] iArr = this.A0I;
            if (iArr == null || iArr.length < this.A06) {
                this.A0I = new int[this.A06];
            }
            int i4 = 0;
            for (int i5 = 0; i5 < this.A06; i5++) {
                C05210Or r5 = this.A0L;
                if (r5.A03 == -1) {
                    A02 = r5.A05;
                    i3 = this.A0J[i5].A03(A02);
                } else {
                    A02 = this.A0J[i5].A02(r5.A02);
                    i3 = r5.A02;
                }
                int i6 = A02 - i3;
                if (i6 >= 0) {
                    this.A0I[i4] = i6;
                    i4++;
                }
            }
            Arrays.sort(this.A0I, 0, i4);
            for (int i7 = 0; i7 < i4; i7++) {
                C05210Or r2 = this.A0L;
                int i8 = r2.A01;
                if (i8 >= 0 && i8 < r8.A00()) {
                    r7.A5o(i8, this.A0I[i7]);
                    r2.A01 += r2.A03;
                } else {
                    return;
                }
            }
        }
    }

    @Override // X.AnonymousClass02H
    public void A0u(AnonymousClass0QS r2, C05480Ps r3) {
        A1P(r2, r3, true);
    }

    @Override // X.AnonymousClass02H
    public void A0v(AnonymousClass0QS r3, RecyclerView recyclerView) {
        Runnable runnable = this.A0N;
        RecyclerView recyclerView2 = super.A07;
        if (recyclerView2 != null) {
            recyclerView2.removeCallbacks(runnable);
        }
        for (int i = 0; i < this.A06; i++) {
            this.A0J[i].A08();
        }
        recyclerView.requestLayout();
    }

    @Override // X.AnonymousClass02H
    public void A0w(C05480Ps r2) {
        this.A03 = -1;
        this.A04 = Integer.MIN_VALUE;
        this.A0A = null;
        this.A0M.A00();
    }

    @Override // X.AnonymousClass02H
    public void A0x(C05480Ps r3, RecyclerView recyclerView, int i) {
        AnonymousClass0FE r0 = new AnonymousClass0FE(recyclerView.getContext());
        ((AbstractC05520Pw) r0).A00 = i;
        A0R(r0);
    }

    @Override // X.AnonymousClass02H
    public void A0y(RecyclerView recyclerView) {
        AnonymousClass0QK r2 = this.A09;
        int[] iArr = r2.A01;
        if (iArr != null) {
            Arrays.fill(iArr, -1);
        }
        r2.A00 = null;
        A0E();
    }

    @Override // X.AnonymousClass02H
    public void A0z(RecyclerView recyclerView, int i, int i2) {
        A1M(i, i2, 1);
    }

    @Override // X.AnonymousClass02H
    public void A10(RecyclerView recyclerView, int i, int i2) {
        A1M(i, i2, 2);
    }

    @Override // X.AnonymousClass02H
    public void A11(RecyclerView recyclerView, int i, int i2, int i3) {
        A1M(i, i2, 8);
    }

    @Override // X.AnonymousClass02H
    public void A12(RecyclerView recyclerView, Object obj, int i, int i2) {
        A1M(i, i2, 4);
    }

    @Override // X.AnonymousClass02H
    public void A13(String str) {
        if (this.A0A == null) {
            super.A13(str);
        }
    }

    @Override // X.AnonymousClass02H
    public boolean A14() {
        return this.A02 == 0;
    }

    @Override // X.AnonymousClass02H
    public boolean A15() {
        return this.A02 == 1;
    }

    @Override // X.AnonymousClass02H
    public boolean A16() {
        return this.A01 != 0;
    }

    @Override // X.AnonymousClass02H
    public boolean A17() {
        return this.A0A == null;
    }

    @Override // X.AnonymousClass02H
    public boolean A18(AnonymousClass0B6 r2) {
        return r2 instanceof AnonymousClass0FA;
    }

    public int A19() {
        if (A06() != 0) {
            return AnonymousClass02H.A02(A0D(0));
        }
        return 0;
    }

    public int A1A() {
        int A06 = A06();
        if (A06 == 0) {
            return 0;
        }
        return AnonymousClass02H.A02(A0D(A06 - 1));
    }

    public final int A1B(int i) {
        int A02 = this.A0J[0].A02(i);
        for (int i2 = 1; i2 < this.A06; i2++) {
            int A022 = this.A0J[i2].A02(i);
            if (A022 > A02) {
                A02 = A022;
            }
        }
        return A02;
    }

    public final int A1C(int i) {
        int A03 = this.A0J[0].A03(i);
        for (int i2 = 1; i2 < this.A06; i2++) {
            int A032 = this.A0J[i2].A03(i);
            if (A032 < A03) {
                A03 = A032;
            }
        }
        return A03;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0063, code lost:
        if (r2 >= r28.A00()) goto L_0x0065;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x024a  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0278  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x027c  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0289  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0299  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x02a5  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02ab  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x02a8 A[ADDED_TO_REGION, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x01c6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0164  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01a3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A1D(X.C05210Or r26, X.AnonymousClass0QS r27, X.C05480Ps r28) {
        /*
        // Method dump skipped, instructions count: 920
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A1D(X.0Or, X.0QS, X.0Ps):int");
    }

    public int A1E(AnonymousClass0QS r5, C05480Ps r6, int i) {
        if (A06() == 0 || i == 0) {
            return 0;
        }
        A1Q(r6, i);
        C05210Or r2 = this.A0L;
        int A1D = A1D(r2, r5, r6);
        if (r2.A00 >= A1D) {
            i = A1D;
            if (i < 0) {
                i = -A1D;
            }
        }
        this.A07.A0E(-i);
        this.A0D = this.A0G;
        r2.A00 = 0;
        A1O(r2, r5);
        return i;
    }

    public final int A1F(C05480Ps r9) {
        if (A06() == 0) {
            return 0;
        }
        AbstractC06220Sq r3 = this.A07;
        boolean z = this.A0H;
        boolean z2 = !z;
        return AnonymousClass0TD.A02(A1I(z2), A1H(z2), r3, this, r9, z, this.A0G);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if (A1U() == false) goto L_0x001d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0073 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A1G() {
        /*
        // Method dump skipped, instructions count: 218
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A1G():android.view.View");
    }

    public View A1H(boolean z) {
        AbstractC06220Sq r7 = this.A07;
        int A06 = r7.A06();
        int A02 = r7.A02();
        View view = null;
        for (int A062 = A06() - 1; A062 >= 0; A062--) {
            View A0D = A0D(A062);
            int A0B = r7.A0B(A0D);
            int A08 = r7.A08(A0D);
            if (A08 > A06 && A0B < A02) {
                if (A08 <= A02 || !z) {
                    return A0D;
                }
                if (view == null) {
                    view = A0D;
                }
            }
        }
        return view;
    }

    public View A1I(boolean z) {
        AbstractC06220Sq r8 = this.A07;
        int A06 = r8.A06();
        int A02 = r8.A02();
        int A062 = A06();
        View view = null;
        for (int i = 0; i < A062; i++) {
            View A0D = A0D(i);
            int A0B = r8.A0B(A0D);
            if (r8.A08(A0D) > A06 && A0B < A02) {
                if (A0B >= A06 || !z) {
                    return A0D;
                }
                if (view == null) {
                    view = A0D;
                }
            }
        }
        return view;
    }

    public final void A1J() {
        boolean z;
        if (this.A02 == 1 || !A1U()) {
            z = this.A0F;
        } else {
            z = !this.A0F;
        }
        this.A0G = z;
    }

    public void A1K(int i) {
        A13(null);
        if (i != this.A06) {
            AnonymousClass0QK r2 = this.A09;
            int[] iArr = r2.A01;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            r2.A00 = null;
            A0E();
            this.A06 = i;
            this.A0B = new BitSet(i);
            AnonymousClass0QU[] r22 = new AnonymousClass0QU[i];
            this.A0J = r22;
            for (int i2 = 0; i2 < i; i2++) {
                r22[i2] = new AnonymousClass0QU(this, i2);
            }
            A0E();
        }
    }

    public final void A1L(int i) {
        C05210Or r4 = this.A0L;
        r4.A04 = i;
        boolean z = this.A0G;
        int i2 = 1;
        boolean z2 = false;
        if (i == -1) {
            z2 = true;
        }
        if (z != z2) {
            i2 = -1;
        }
        r4.A03 = i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M(int r8, int r9, int r10) {
        /*
            r7 = this;
            boolean r0 = r7.A0G
            if (r0 == 0) goto L_0x0047
            int r6 = r7.A1A()
        L_0x0008:
            r5 = 8
            if (r10 != r5) goto L_0x0043
            int r4 = r9 + 1
            if (r8 < r9) goto L_0x0045
            int r4 = r8 + 1
            r3 = r9
        L_0x0013:
            X.0QK r2 = r7.A09
            r2.A04(r3)
            r1 = 1
            if (r10 == r1) goto L_0x003f
            r0 = 2
            if (r10 == r0) goto L_0x003b
            if (r10 != r5) goto L_0x0026
            r2.A06(r8, r1)
            r2.A05(r9, r1)
        L_0x0026:
            if (r4 <= r6) goto L_0x0035
            boolean r0 = r7.A0G
            if (r0 == 0) goto L_0x0036
            int r0 = r7.A19()
        L_0x0030:
            if (r3 > r0) goto L_0x0035
            r7.A0E()
        L_0x0035:
            return
        L_0x0036:
            int r0 = r7.A1A()
            goto L_0x0030
        L_0x003b:
            r2.A06(r8, r9)
            goto L_0x0026
        L_0x003f:
            r2.A05(r8, r9)
            goto L_0x0026
        L_0x0043:
            int r4 = r8 + r9
        L_0x0045:
            r3 = r8
            goto L_0x0013
        L_0x0047:
            int r6 = r7.A19()
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A1M(int, int, int):void");
    }

    public final void A1N(View view, int i, int i2) {
        Rect rect = this.A0K;
        A0J(view, rect);
        AnonymousClass0B6 r4 = (AnonymousClass0B6) view.getLayoutParams();
        int A06 = A06(i, ((ViewGroup.MarginLayoutParams) r4).leftMargin + rect.left, ((ViewGroup.MarginLayoutParams) r4).rightMargin + rect.right);
        int A062 = A06(i2, ((ViewGroup.MarginLayoutParams) r4).topMargin + rect.top, ((ViewGroup.MarginLayoutParams) r4).bottomMargin + rect.bottom);
        if (A0V(view, r4, A06, A062)) {
            view.measure(A06, A062);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000f, code lost:
        if (r0 == -1) goto L_0x0011;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001b  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00cf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1O(X.C05210Or r8, X.AnonymousClass0QS r9) {
        /*
        // Method dump skipped, instructions count: 292
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A1O(X.0Or, X.0QS):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:139:0x01f0, code lost:
        if (r11.A0G != false) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x01f2, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x01f3, code lost:
        r9.A03 = r3;
        r0 = r9.A06.A07;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x01f9, code lost:
        if (r3 == false) goto L_0x0205;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x01fb, code lost:
        r0 = r0.A02();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0205, code lost:
        r0 = r0.A06();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0214, code lost:
        if (r2 != r11.A0G) goto L_0x01f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        if (r2 != null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x03b3, code lost:
        if (A1T() == false) goto L_0x03cf;
     */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x022b  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x026e  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x02cf  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x02ea  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0318  */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x0356  */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x03ac  */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x03b9  */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x03c8  */
    /* JADX WARNING: Removed duplicated region for block: B:304:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0113  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1P(X.AnonymousClass0QS r12, X.C05480Ps r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 1095
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A1P(X.0QS, X.0Ps, boolean):void");
    }

    public void A1Q(C05480Ps r5, int i) {
        int A19;
        int i2;
        if (i > 0) {
            A19 = A1A();
            i2 = 1;
        } else {
            A19 = A19();
            i2 = -1;
        }
        C05210Or r1 = this.A0L;
        r1.A07 = true;
        A1R(r5, A19);
        A1L(i2);
        r1.A01 = A19 + r1.A03;
        r1.A00 = Math.abs(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r1 == false) goto L_0x0010;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1R(X.C05480Ps r8, int r9) {
        /*
            r7 = this;
            X.0Or r5 = r7.A0L
            r4 = 0
            r5.A00 = r4
            r5.A01 = r9
            X.0Pw r0 = r7.A06
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0.A05
            r0 = 1
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            r6 = 1
            if (r0 == 0) goto L_0x005e
            int r3 = r8.A06
            r0 = -1
            if (r3 == r0) goto L_0x005e
            boolean r2 = r7.A0G
            r1 = 0
            if (r3 >= r9) goto L_0x001f
            r1 = 1
        L_0x001f:
            X.0Sq r0 = r7.A07
            int r3 = r0.A07()
            if (r2 == r1) goto L_0x005f
            r2 = r3
            r3 = 0
        L_0x0029:
            boolean r0 = r7.A0S()
            X.0Sq r1 = r7.A07
            if (r0 == 0) goto L_0x0053
            int r0 = r1.A06()
            int r0 = r0 - r2
            r5.A05 = r0
            int r0 = r1.A02()
            int r0 = r0 + r3
            r5.A02 = r0
        L_0x003f:
            r5.A08 = r4
            r5.A07 = r6
            int r0 = r1.A04()
            if (r0 != 0) goto L_0x0050
            int r0 = r1.A01()
            if (r0 != 0) goto L_0x0050
            r4 = 1
        L_0x0050:
            r5.A06 = r4
            return
        L_0x0053:
            int r0 = r1.A01()
            int r0 = r0 + r3
            r5.A02 = r0
            int r0 = -r2
            r5.A05 = r0
            goto L_0x003f
        L_0x005e:
            r3 = 0
        L_0x005f:
            r2 = 0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A1R(X.0Ps, int):void");
    }

    public final void A1S(AnonymousClass0QU r5, int i, int i2) {
        int i3 = r5.A02;
        if (i == -1) {
            int i4 = r5.A01;
            if (i4 == Integer.MIN_VALUE) {
                r5.A07();
                i4 = r5.A01;
            }
            if (i4 + i3 > i2) {
                return;
            }
        } else {
            int i5 = r5.A00;
            if (i5 == Integer.MIN_VALUE) {
                r5.A06();
                i5 = r5.A00;
            }
            if (i5 - i3 < i2) {
                return;
            }
        }
        this.A0B.set(r5.A04, false);
    }

    public boolean A1T() {
        int A19;
        int A1A;
        int i;
        if (!(A06() == 0 || this.A01 == 0 || !super.A0B)) {
            if (this.A0G) {
                A19 = A1A();
                A1A = A19();
            } else {
                A19 = A19();
                A1A = A1A();
            }
            if (A19 == 0 && A1G() != null) {
                AnonymousClass0QK r2 = this.A09;
                int[] iArr = r2.A01;
                if (iArr != null) {
                    Arrays.fill(iArr, -1);
                }
                r2.A00 = null;
            } else if (this.A0C) {
                int i2 = 1;
                if (this.A0G) {
                    i2 = -1;
                }
                AnonymousClass0QK r3 = this.A09;
                int i3 = A1A + 1;
                C06840Vh A01 = r3.A01(A19, i3, i2);
                if (A01 == null) {
                    this.A0C = false;
                    r3.A03(i3);
                    return false;
                }
                C06840Vh A012 = r3.A01(A19, A01.A01, -i2);
                if (A012 == null) {
                    i = A01.A01;
                } else {
                    i = A012.A01 + 1;
                }
                r3.A03(i);
            }
            super.A0D = true;
            A0E();
            return true;
        }
        return false;
    }

    public boolean A1U() {
        return AnonymousClass028.A05(super.A07) == 1;
    }

    public final boolean A1V(int i) {
        if (this.A02 == 0) {
            boolean z = false;
            if (i == -1) {
                z = true;
            }
            if (z != this.A0G) {
                return true;
            }
            return false;
        }
        boolean z2 = false;
        if (i == -1) {
            z2 = true;
        }
        boolean z3 = false;
        if (z2 == this.A0G) {
            z3 = true;
        }
        if (z3 == A1U()) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        if (r1 != r4.A0G) goto L_0x000c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r4.A0G != false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000b, code lost:
        r3 = 1;
     */
    @Override // X.AnonymousClass02I
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.PointF A7U(int r5) {
        /*
            r4 = this;
            int r0 = r4.A06()
            r3 = -1
            if (r0 != 0) goto L_0x001c
            boolean r0 = r4.A0G
            if (r0 == 0) goto L_0x000c
        L_0x000b:
            r3 = 1
        L_0x000c:
            android.graphics.PointF r2 = new android.graphics.PointF
            r2.<init>()
            int r0 = r4.A02
            r1 = 0
            if (r0 != 0) goto L_0x0029
            float r0 = (float) r3
            r2.x = r0
            r2.y = r1
            return r2
        L_0x001c:
            int r0 = r4.A19()
            r1 = 0
            if (r5 >= r0) goto L_0x0024
            r1 = 1
        L_0x0024:
            boolean r0 = r4.A0G
            if (r1 == r0) goto L_0x000b
            goto L_0x000c
        L_0x0029:
            r2.x = r1
            float r0 = (float) r3
            r2.y = r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.StaggeredGridLayoutManager.A7U(int):android.graphics.PointF");
    }
}
