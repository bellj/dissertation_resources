package androidx.recyclerview.widget;

import X.AbstractC05290Oz;
import X.AbstractC11870h0;
import X.AnonymousClass02H;
import X.AnonymousClass0B6;
import X.AnonymousClass0Et;
import X.AnonymousClass0F9;
import X.AnonymousClass0PR;
import X.AnonymousClass0QS;
import X.C05480Ps;
import X.C05510Pv;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;

/* loaded from: classes.dex */
public class GridLayoutManager extends LinearLayoutManager {
    public int A00 = -1;
    public AbstractC05290Oz A01 = new AnonymousClass0Et();
    public boolean A02 = false;
    public int[] A03;
    public View[] A04;
    public final Rect A05 = new Rect();
    public final SparseIntArray A06 = new SparseIntArray();
    public final SparseIntArray A07 = new SparseIntArray();

    public GridLayoutManager(int i) {
        super(1);
        A1h(i);
    }

    public GridLayoutManager(int i, int i2) {
        super(i2);
        A1h(i);
    }

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A1h(AnonymousClass02H.A03(context, attributeSet, i, i2).A01);
    }

    @Override // X.AnonymousClass02H
    public int A0W(AnonymousClass0QS r3, C05480Ps r4) {
        if (((LinearLayoutManager) this).A01 == 1) {
            return this.A00;
        }
        int A00 = r4.A00();
        if (A00 < 1) {
            return 0;
        }
        return A1c(r3, r4, A00 - 1) + 1;
    }

    @Override // X.AnonymousClass02H
    public int A0X(AnonymousClass0QS r3, C05480Ps r4) {
        if (((LinearLayoutManager) this).A01 == 0) {
            return this.A00;
        }
        int A00 = r4.A00();
        if (A00 < 1) {
            return 0;
        }
        return A1c(r3, r4, A00 - 1) + 1;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public int A0Y(AnonymousClass0QS r2, C05480Ps r3, int i) {
        A1g();
        A1f();
        return super.A0Y(r2, r3, i);
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public int A0Z(AnonymousClass0QS r2, C05480Ps r3, int i) {
        A1g();
        A1f();
        return super.A0Z(r2, r3, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005c, code lost:
        if (A1b() == false) goto L_0x005e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x00c3 A[SYNTHETIC] */
    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A0h(android.view.View r25, X.AnonymousClass0QS r26, X.C05480Ps r27, int r28) {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.A0h(android.view.View, X.0QS, X.0Ps, int):android.view.View");
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public AnonymousClass0B6 A0i() {
        if (((LinearLayoutManager) this).A01 == 0) {
            return new AnonymousClass0F9(-2, -1);
        }
        return new AnonymousClass0F9(-1, -2);
    }

    @Override // X.AnonymousClass02H
    public AnonymousClass0B6 A0j(Context context, AttributeSet attributeSet) {
        return new AnonymousClass0F9(context, attributeSet);
    }

    @Override // X.AnonymousClass02H
    public AnonymousClass0B6 A0k(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new AnonymousClass0F9((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new AnonymousClass0F9(layoutParams);
    }

    @Override // X.AnonymousClass02H
    public void A0p(Rect rect, int i, int i2) {
        int A00;
        int A002;
        if (this.A03 == null) {
            super.A0p(rect, i, i2);
        }
        int A09 = A09() + A0A();
        int A0B = A0B() + A08();
        if (((LinearLayoutManager) this).A01 == 1) {
            A002 = AnonymousClass02H.A00(i2, rect.height() + A0B, ((AnonymousClass02H) this).A07.getMinimumHeight());
            int[] iArr = this.A03;
            A00 = AnonymousClass02H.A00(i, iArr[iArr.length - 1] + A09, ((AnonymousClass02H) this).A07.getMinimumWidth());
        } else {
            A00 = AnonymousClass02H.A00(i, rect.width() + A09, ((AnonymousClass02H) this).A07.getMinimumWidth());
            int[] iArr2 = this.A03;
            A002 = AnonymousClass02H.A00(i2, iArr2[iArr2.length - 1] + A0B, ((AnonymousClass02H) this).A07.getMinimumHeight());
        }
        ((AnonymousClass02H) this).A07.setMeasuredDimension(A00, A002);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0024, code lost:
        if (r3 != r0) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        if (r7 != r0) goto L_0x003b;
     */
    @Override // X.AnonymousClass02H
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0r(android.view.View r11, X.AnonymousClass04Z r12, X.AnonymousClass0QS r13, X.C05480Ps r14) {
        /*
            r10 = this;
            android.view.ViewGroup$LayoutParams r1 = r11.getLayoutParams()
            boolean r0 = r1 instanceof X.AnonymousClass0F9
            if (r0 != 0) goto L_0x000c
            super.A0K(r11, r12)
            return
        L_0x000c:
            X.0F9 r1 = (X.AnonymousClass0F9) r1
            int r0 = r1.A00()
            int r4 = r10.A1c(r13, r14, r0)
            int r0 = r10.A01
            r5 = 1
            if (r0 != 0) goto L_0x0030
            int r2 = r1.A00
            int r3 = r1.A01
            int r0 = r10.A00
            if (r0 <= r5) goto L_0x0026
            r6 = 1
            if (r3 == r0) goto L_0x0027
        L_0x0026:
            r6 = 0
        L_0x0027:
            r7 = 0
            X.0Tm r0 = X.C06410Tm.A01(r2, r3, r4, r5, r6, r7)
        L_0x002c:
            r12.A0J(r0)
            return
        L_0x0030:
            int r6 = r1.A00
            int r7 = r1.A01
            int r0 = r10.A00
            if (r0 <= r5) goto L_0x003b
            r8 = 1
            if (r7 == r0) goto L_0x003c
        L_0x003b:
            r8 = 0
        L_0x003c:
            r9 = 0
            X.0Tm r0 = X.C06410Tm.A01(r4, r5, r6, r7, r8, r9)
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.A0r(android.view.View, X.04Z, X.0QS, X.0Ps):void");
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public void A0u(AnonymousClass0QS r7, C05480Ps r8) {
        if (r8.A08) {
            int A06 = A06();
            for (int i = 0; i < A06; i++) {
                AnonymousClass0F9 r3 = (AnonymousClass0F9) A0D(i).getLayoutParams();
                int A00 = r3.A00();
                this.A07.put(A00, r3.A01);
                this.A06.put(A00, r3.A00);
            }
        }
        super.A0u(r7, r8);
        this.A07.clear();
        this.A06.clear();
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public void A0w(C05480Ps r2) {
        super.A0w(r2);
        this.A02 = false;
    }

    @Override // X.AnonymousClass02H
    public void A0y(RecyclerView recyclerView) {
        this.A01.A00.clear();
    }

    @Override // X.AnonymousClass02H
    public void A0z(RecyclerView recyclerView, int i, int i2) {
        this.A01.A00.clear();
    }

    @Override // X.AnonymousClass02H
    public void A10(RecyclerView recyclerView, int i, int i2) {
        this.A01.A00.clear();
    }

    @Override // X.AnonymousClass02H
    public void A11(RecyclerView recyclerView, int i, int i2, int i3) {
        this.A01.A00.clear();
    }

    @Override // X.AnonymousClass02H
    public void A12(RecyclerView recyclerView, Object obj, int i, int i2) {
        this.A01.A00.clear();
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager, X.AnonymousClass02H
    public boolean A17() {
        return ((LinearLayoutManager) this).A05 == null && !this.A02;
    }

    @Override // X.AnonymousClass02H
    public boolean A18(AnonymousClass0B6 r2) {
        return r2 instanceof AnonymousClass0F9;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public View A1L(AnonymousClass0QS r8, C05480Ps r9, int i, int i2, int i3) {
        A1O();
        int A06 = ((LinearLayoutManager) this).A06.A06();
        int A02 = ((LinearLayoutManager) this).A06.A02();
        int i4 = -1;
        if (i2 > i) {
            i4 = 1;
        }
        View view = null;
        View view2 = null;
        while (i != i2) {
            View A0D = A0D(i);
            int A022 = AnonymousClass02H.A02(A0D);
            if (A022 >= 0 && A022 < i3 && A1d(r8, r9, A022) == 0) {
                if ((((AnonymousClass0B6) A0D.getLayoutParams()).A00.A00 & 8) != 0) {
                    if (view2 == null) {
                        view2 = A0D;
                    }
                } else if (((LinearLayoutManager) this).A06.A0B(A0D) < A02 && ((LinearLayoutManager) this).A06.A08(A0D) >= A06) {
                    return A0D;
                } else {
                    if (view == null) {
                        view = A0D;
                    }
                }
            }
            i += i4;
        }
        return view == null ? view2 : view;
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public void A1U(C05510Pv r6, AnonymousClass0QS r7, C05480Ps r8, int i) {
        A1g();
        if (r8.A00() > 0 && !r8.A08) {
            boolean z = false;
            if (i == 1) {
                z = true;
            }
            int A1d = A1d(r7, r8, r6.A01);
            if (z) {
                while (A1d > 0) {
                    int i2 = r6.A01;
                    if (i2 <= 0) {
                        break;
                    }
                    int i3 = i2 - 1;
                    r6.A01 = i3;
                    A1d = A1d(r7, r8, i3);
                }
            } else {
                int A00 = r8.A00() - 1;
                int i4 = r6.A01;
                while (i4 < A00) {
                    int i5 = i4 + 1;
                    int A1d2 = A1d(r7, r8, i5);
                    if (A1d2 <= A1d) {
                        break;
                    }
                    i4 = i5;
                    A1d = A1d2;
                }
                r6.A01 = i4;
            }
        }
        A1f();
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00c1  */
    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public void A1V(X.C04790Nb r19, X.AnonymousClass0PR r20, X.AnonymousClass0QS r21, X.C05480Ps r22) {
        /*
        // Method dump skipped, instructions count: 611
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.A1V(X.0Nb, X.0PR, X.0QS, X.0Ps):void");
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public void A1W(AnonymousClass0PR r6, AbstractC11870h0 r7, C05480Ps r8) {
        int i;
        int i2 = this.A00;
        for (int i3 = 0; i3 < this.A00 && (i = r6.A01) >= 0 && i < r8.A00() && i2 > 0; i3++) {
            r7.A5o(i, Math.max(0, r6.A07));
            i2 -= this.A01.A00(i);
            r6.A01 += r6.A03;
        }
    }

    @Override // androidx.recyclerview.widget.LinearLayoutManager
    public void A1a(boolean z) {
        if (!z) {
            super.A1a(false);
            return;
        }
        throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }

    public final int A1c(AnonymousClass0QS r9, C05480Ps r10, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (!r10.A08) {
            AbstractC05290Oz r2 = this.A01;
            i2 = this.A00;
            i3 = r2.A00(i);
            i4 = 0;
            i5 = 0;
            for (int i6 = 0; i6 < i; i6++) {
                int A00 = r2.A00(i6);
                i4 += A00;
                if (i4 == i2) {
                    i5++;
                    i4 = 0;
                } else if (i4 > i2) {
                    i5++;
                    i4 = A00;
                }
            }
        } else {
            int A002 = r9.A00(i);
            if (A002 == -1) {
                StringBuilder sb = new StringBuilder("Cannot find span size for pre layout position. ");
                sb.append(i);
                Log.w("GridLayoutManager", sb.toString());
                return 0;
            }
            AbstractC05290Oz r22 = this.A01;
            i2 = this.A00;
            i3 = r22.A00(A002);
            i4 = 0;
            i5 = 0;
            for (int i7 = 0; i7 < A002; i7++) {
                int A003 = r22.A00(i7);
                i4 += A003;
                if (i4 == i2) {
                    i5++;
                    i4 = 0;
                } else if (i4 > i2) {
                    i5++;
                    i4 = A003;
                }
            }
        }
        if (i4 + i3 > i2) {
            return i5 + 1;
        }
        return i5;
    }

    public final int A1d(AnonymousClass0QS r4, C05480Ps r5, int i) {
        if (!r5.A08) {
            return this.A01.A01(i, this.A00);
        }
        int i2 = this.A06.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int A00 = r4.A00(i);
        if (A00 != -1) {
            return this.A01.A01(A00, this.A00);
        }
        StringBuilder sb = new StringBuilder("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
        sb.append(i);
        Log.w("GridLayoutManager", sb.toString());
        return 0;
    }

    public final int A1e(AnonymousClass0QS r3, C05480Ps r4, int i) {
        if (!r4.A08) {
            return this.A01.A00(i);
        }
        int i2 = this.A07.get(i, -1);
        if (i2 != -1) {
            return i2;
        }
        int A00 = r3.A00(i);
        if (A00 != -1) {
            return this.A01.A00(A00);
        }
        StringBuilder sb = new StringBuilder("Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:");
        sb.append(i);
        Log.w("GridLayoutManager", sb.toString());
        return 1;
    }

    public final void A1f() {
        View[] viewArr = this.A04;
        if (viewArr == null || viewArr.length != this.A00) {
            this.A04 = new View[this.A00];
        }
    }

    public final void A1g() {
        int A08;
        int A0B;
        if (((LinearLayoutManager) this).A01 == 1) {
            A08 = ((AnonymousClass02H) this).A03 - A0A();
            A0B = A09();
        } else {
            A08 = ((AnonymousClass02H) this).A00 - A08();
            A0B = A0B();
        }
        A1i(A08 - A0B);
    }

    public void A1h(int i) {
        if (i != this.A00) {
            this.A02 = true;
            if (i >= 1) {
                this.A00 = i;
                this.A01.A00.clear();
                A0E();
                return;
            }
            StringBuilder sb = new StringBuilder("Span count should be at least 1. Provided ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public final void A1i(int i) {
        int i2;
        int length;
        int[] iArr = this.A03;
        int i3 = this.A00;
        if (!(iArr != null && (length = iArr.length) == i3 + 1 && iArr[length - 1] == i)) {
            iArr = new int[i3 + 1];
        }
        int i4 = 0;
        iArr[0] = 0;
        int i5 = i / i3;
        int i6 = i % i3;
        int i7 = 0;
        for (int i8 = 1; i8 <= i3; i8++) {
            i4 += i6;
            if (i4 <= 0 || i3 - i4 >= i6) {
                i2 = i5;
            } else {
                i2 = i5 + 1;
                i4 -= i3;
            }
            i7 += i2;
            iArr[i8] = i7;
        }
        this.A03 = iArr;
    }

    public final void A1j(View view, int i, boolean z) {
        int i2;
        int i3;
        int A01;
        int A012;
        AnonymousClass0F9 r6 = (AnonymousClass0F9) view.getLayoutParams();
        Rect rect = r6.A03;
        int i4 = rect.top + rect.bottom + ((ViewGroup.MarginLayoutParams) r6).topMargin + ((ViewGroup.MarginLayoutParams) r6).bottomMargin;
        int i5 = rect.left + rect.right + ((ViewGroup.MarginLayoutParams) r6).leftMargin + ((ViewGroup.MarginLayoutParams) r6).rightMargin;
        int i6 = r6.A00;
        int i7 = r6.A01;
        if (((LinearLayoutManager) this).A01 != 1 || !A1b()) {
            int[] iArr = this.A03;
            i2 = iArr[i7 + i6];
            i3 = iArr[i6];
        } else {
            int[] iArr2 = this.A03;
            int i8 = this.A00 - i6;
            i2 = iArr2[i8];
            i3 = iArr2[i8 - i7];
        }
        int i9 = i2 - i3;
        if (((LinearLayoutManager) this).A01 == 1) {
            A012 = AnonymousClass02H.A01(i9, i, i5, ((ViewGroup.MarginLayoutParams) r6).width, false);
            A01 = AnonymousClass02H.A01(((LinearLayoutManager) this).A06.A07(), ((AnonymousClass02H) this).A01, i4, ((ViewGroup.MarginLayoutParams) r6).height, true);
        } else {
            A01 = AnonymousClass02H.A01(i9, i, i4, ((ViewGroup.MarginLayoutParams) r6).height, false);
            A012 = AnonymousClass02H.A01(((LinearLayoutManager) this).A06.A07(), ((AnonymousClass02H) this).A04, i5, ((ViewGroup.MarginLayoutParams) r6).width, true);
        }
        AnonymousClass0B6 r2 = (AnonymousClass0B6) view.getLayoutParams();
        if (z) {
            if (AnonymousClass02H.A05(view.getMeasuredWidth(), A012, ((ViewGroup.MarginLayoutParams) r2).width) && AnonymousClass02H.A05(view.getMeasuredHeight(), A01, ((ViewGroup.MarginLayoutParams) r2).height)) {
                return;
            }
        } else if (!A0V(view, r2, A012, A01)) {
            return;
        }
        view.measure(A012, A01);
    }
}
