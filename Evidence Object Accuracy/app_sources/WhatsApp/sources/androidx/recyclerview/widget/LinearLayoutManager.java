package androidx.recyclerview.widget;

import X.AbstractC05520Pw;
import X.AbstractC06220Sq;
import X.AbstractC11870h0;
import X.AnonymousClass028;
import X.AnonymousClass02H;
import X.AnonymousClass02I;
import X.AnonymousClass02J;
import X.AnonymousClass0B6;
import X.AnonymousClass0FE;
import X.AnonymousClass0P8;
import X.AnonymousClass0PR;
import X.AnonymousClass0QS;
import X.AnonymousClass0TD;
import X.C04790Nb;
import X.C04800Nc;
import X.C05480Ps;
import X.C05510Pv;
import X.C06880Vl;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

/* loaded from: classes.dex */
public class LinearLayoutManager extends AnonymousClass02H implements AnonymousClass02I, AnonymousClass02J {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public AnonymousClass0PR A04;
    public C06880Vl A05;
    public AbstractC06220Sq A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final C05510Pv A0C;
    public final C04790Nb A0D;

    @Override // X.AnonymousClass02H
    public boolean A16() {
        return true;
    }

    public void A1U(C05510Pv r1, AnonymousClass0QS r2, C05480Ps r3, int i) {
    }

    public LinearLayoutManager() {
        this(1);
    }

    public LinearLayoutManager(int i) {
        this.A01 = 1;
        this.A08 = false;
        this.A09 = false;
        this.A0B = false;
        this.A0A = true;
        this.A02 = -1;
        this.A03 = Integer.MIN_VALUE;
        this.A05 = null;
        this.A0C = new C05510Pv();
        this.A0D = new C04790Nb();
        this.A00 = 2;
        A1Q(i);
        A1Z(false);
    }

    public LinearLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        this.A01 = 1;
        this.A08 = false;
        this.A09 = false;
        this.A0B = false;
        this.A0A = true;
        this.A02 = -1;
        this.A03 = Integer.MIN_VALUE;
        this.A05 = null;
        this.A0C = new C05510Pv();
        this.A0D = new C04790Nb();
        this.A00 = 2;
        C04800Nc A03 = AnonymousClass02H.A03(context, attributeSet, i, i2);
        A1Q(A03.A00);
        A1Z(A03.A02);
        A1a(A03.A03);
    }

    @Override // X.AnonymousClass02H
    public View A0C(int i) {
        int A06 = A06();
        if (A06 == 0) {
            return null;
        }
        int A02 = i - AnonymousClass02H.A02(A0D(0));
        if (A02 >= 0 && A02 < A06) {
            View A0D = A0D(A02);
            if (AnonymousClass02H.A02(A0D) == i) {
                return A0D;
            }
        }
        return super.A0C(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000c, code lost:
        if (r0 != false) goto L_0x000e;
     */
    @Override // X.AnonymousClass02H
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0M(X.AbstractC11870h0 r6, int r7) {
        /*
            r5 = this;
            X.0Vl r0 = r5.A05
            r4 = -1
            r3 = 0
            if (r0 == 0) goto L_0x001e
            int r2 = r0.A01
            if (r2 < 0) goto L_0x001e
            boolean r0 = r0.A02
        L_0x000c:
            if (r0 == 0) goto L_0x002d
        L_0x000e:
            r1 = 0
        L_0x000f:
            int r0 = r5.A00
            if (r1 >= r0) goto L_0x002f
            if (r2 < 0) goto L_0x002f
            if (r2 >= r7) goto L_0x002f
            r6.A5o(r2, r3)
            int r2 = r2 + r4
            int r1 = r1 + 1
            goto L_0x000f
        L_0x001e:
            r5.A1P()
            boolean r0 = r5.A09
            int r2 = r5.A02
            if (r2 != r4) goto L_0x000c
            r2 = 0
            if (r0 == 0) goto L_0x002d
            int r2 = r7 + -1
            goto L_0x000e
        L_0x002d:
            r4 = 1
            goto L_0x000e
        L_0x002f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.LinearLayoutManager.A0M(X.0h0, int):void");
    }

    @Override // X.AnonymousClass02H
    public boolean A0T() {
        if (super.A01 == 1073741824 || super.A04 == 1073741824) {
            return false;
        }
        int A06 = A06();
        for (int i = 0; i < A06; i++) {
            ViewGroup.LayoutParams layoutParams = A0D(i).getLayoutParams();
            if (layoutParams.width < 0 && layoutParams.height < 0) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AnonymousClass02H
    public int A0Y(AnonymousClass0QS r3, C05480Ps r4, int i) {
        if (this.A01 == 1) {
            return 0;
        }
        return A1F(r3, r4, i);
    }

    @Override // X.AnonymousClass02H
    public int A0Z(AnonymousClass0QS r2, C05480Ps r3, int i) {
        if (this.A01 == 0) {
            return 0;
        }
        return A1F(r2, r3, i);
    }

    @Override // X.AnonymousClass02H
    public int A0a(C05480Ps r2) {
        return A1G(r2);
    }

    @Override // X.AnonymousClass02H
    public int A0b(C05480Ps r2) {
        return A1H(r2);
    }

    @Override // X.AnonymousClass02H
    public int A0c(C05480Ps r2) {
        return A1I(r2);
    }

    @Override // X.AnonymousClass02H
    public int A0d(C05480Ps r2) {
        return A1G(r2);
    }

    @Override // X.AnonymousClass02H
    public int A0e(C05480Ps r2) {
        return A1H(r2);
    }

    @Override // X.AnonymousClass02H
    public int A0f(C05480Ps r2) {
        return A1I(r2);
    }

    @Override // X.AnonymousClass02H
    public Parcelable A0g() {
        int i;
        int i2;
        int A06;
        C06880Vl r0 = this.A05;
        if (r0 != null) {
            return new C06880Vl(r0);
        }
        C06880Vl r2 = new C06880Vl();
        if (A06() > 0) {
            A1O();
            boolean z = this.A07;
            boolean z2 = this.A09;
            boolean z3 = z ^ z2;
            r2.A02 = z3;
            if (z3) {
                if (z2) {
                    A06 = 0;
                } else {
                    A06 = A06() - 1;
                }
                View A0D = A0D(A06);
                r2.A00 = this.A06.A02() - this.A06.A08(A0D);
                i = AnonymousClass02H.A02(A0D);
            } else {
                if (z2) {
                    i2 = A06() - 1;
                } else {
                    i2 = 0;
                }
                View A0D2 = A0D(i2);
                r2.A01 = AnonymousClass02H.A02(A0D2);
                r2.A00 = this.A06.A0B(A0D2) - this.A06.A06();
                return r2;
            }
        } else {
            i = -1;
        }
        r2.A01 = i;
        return r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0045, code lost:
        if (r6.A09 != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0047, code lost:
        r0 = A06() - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004d, code lost:
        r1 = A0D(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0055, code lost:
        if (r1.hasFocusable() == false) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        if (r2 == null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0059, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006d, code lost:
        if (r6.A09 != false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006f, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0078, code lost:
        return r2;
     */
    @Override // X.AnonymousClass02H
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View A0h(android.view.View r7, X.AnonymousClass0QS r8, X.C05480Ps r9, int r10) {
        /*
            r6 = this;
            r6.A1P()
            int r0 = r6.A06()
            r5 = 0
            if (r0 == 0) goto L_0x0079
            int r4 = r6.A1D(r10)
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r4 == r3) goto L_0x0079
            r6.A1O()
            r6.A1O()
            r1 = 1051372203(0x3eaaaaab, float:0.33333334)
            X.0Sq r0 = r6.A06
            int r0 = r0.A07()
            float r0 = (float) r0
            float r0 = r0 * r1
            int r0 = (int) r0
            r2 = 0
            r6.A1Y(r9, r4, r0, r2)
            X.0PR r1 = r6.A04
            r1.A07 = r3
            r1.A0A = r2
            r0 = 1
            r6.A1E(r1, r8, r9, r0)
            r3 = -1
            boolean r1 = r6.A09
            if (r4 != r3) goto L_0x005f
            int r0 = r6.A06()
            if (r1 == 0) goto L_0x005a
            int r0 = r0 + -1
            android.view.View r2 = r6.A1J(r0, r3)
        L_0x0043:
            boolean r0 = r6.A09
            if (r0 == 0) goto L_0x006f
        L_0x0047:
            int r0 = r6.A06()
            int r0 = r0 + -1
        L_0x004d:
            android.view.View r1 = r6.A0D(r0)
            boolean r0 = r1.hasFocusable()
            if (r0 == 0) goto L_0x0078
            if (r2 == 0) goto L_0x0079
            return r1
        L_0x005a:
            android.view.View r2 = r6.A1J(r2, r0)
            goto L_0x0043
        L_0x005f:
            int r0 = r6.A06()
            if (r1 == 0) goto L_0x0071
            android.view.View r2 = r6.A1J(r2, r0)
        L_0x0069:
            if (r4 == r3) goto L_0x0043
            boolean r0 = r6.A09
            if (r0 == 0) goto L_0x0047
        L_0x006f:
            r0 = 0
            goto L_0x004d
        L_0x0071:
            int r0 = r0 + -1
            android.view.View r2 = r6.A1J(r0, r3)
            goto L_0x0069
        L_0x0078:
            return r2
        L_0x0079:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.LinearLayoutManager.A0h(android.view.View, X.0QS, X.0Ps, int):android.view.View");
    }

    @Override // X.AnonymousClass02H
    public AnonymousClass0B6 A0i() {
        return new AnonymousClass0B6(-2, -2);
    }

    @Override // X.AnonymousClass02H
    public void A0o(int i) {
        this.A02 = i;
        this.A03 = Integer.MIN_VALUE;
        C06880Vl r1 = this.A05;
        if (r1 != null) {
            r1.A01 = -1;
        }
        A0E();
    }

    @Override // X.AnonymousClass02H
    public void A0q(Parcelable parcelable) {
        if (parcelable instanceof C06880Vl) {
            this.A05 = (C06880Vl) parcelable;
            A0E();
        }
    }

    @Override // X.AnonymousClass02H
    public void A0s(AccessibilityEvent accessibilityEvent) {
        super.A0s(accessibilityEvent);
        if (A06() > 0) {
            accessibilityEvent.setFromIndex(A1A());
            accessibilityEvent.setToIndex(A1C());
        }
    }

    @Override // X.AnonymousClass02H
    public void A0t(AbstractC11870h0 r4, C05480Ps r5, int i, int i2) {
        if (this.A01 != 0) {
            i = i2;
        }
        if (A06() != 0 && i != 0) {
            A1O();
            int i3 = -1;
            if (i > 0) {
                i3 = 1;
            }
            A1Y(r5, i3, Math.abs(i), true);
            A1W(this.A04, r4, r5);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01f6, code lost:
        if (r0 <= 0) goto L_0x01f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01f8, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e0, code lost:
        if (r6.A01() != 0) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0164, code lost:
        if (r6 > 0) goto L_0x0166;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0166, code lost:
        r0 = -A1F(r20, r21, r6);
     */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x03e3  */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x03eb  */
    @Override // X.AnonymousClass02H
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0u(X.AnonymousClass0QS r20, X.C05480Ps r21) {
        /*
        // Method dump skipped, instructions count: 1153
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.LinearLayoutManager.A0u(X.0QS, X.0Ps):void");
    }

    @Override // X.AnonymousClass02H
    public void A0w(C05480Ps r2) {
        this.A05 = null;
        this.A02 = -1;
        this.A03 = Integer.MIN_VALUE;
        this.A0C.A00();
    }

    @Override // X.AnonymousClass02H
    public void A0x(C05480Ps r3, RecyclerView recyclerView, int i) {
        AnonymousClass0FE r0 = new AnonymousClass0FE(recyclerView.getContext());
        ((AbstractC05520Pw) r0).A00 = i;
        A0R(r0);
    }

    @Override // X.AnonymousClass02H
    public void A13(String str) {
        if (this.A05 == null) {
            super.A13(str);
        }
    }

    @Override // X.AnonymousClass02H
    public boolean A14() {
        return this.A01 == 0;
    }

    @Override // X.AnonymousClass02H
    public boolean A15() {
        return this.A01 == 1;
    }

    @Override // X.AnonymousClass02H
    public boolean A17() {
        return this.A05 == null && this.A07 == this.A0B;
    }

    public int A19() {
        View A1K = A1K(0, A06(), true, false);
        if (A1K == null) {
            return -1;
        }
        return AnonymousClass02H.A02(A1K);
    }

    public int A1A() {
        View A1K = A1K(0, A06(), false, true);
        if (A1K == null) {
            return -1;
        }
        return AnonymousClass02H.A02(A1K);
    }

    public int A1B() {
        View A1K = A1K(A06() - 1, -1, true, false);
        if (A1K != null) {
            return AnonymousClass02H.A02(A1K);
        }
        return -1;
    }

    public int A1C() {
        View A1K = A1K(A06() - 1, -1, false, true);
        if (A1K != null) {
            return AnonymousClass02H.A02(A1K);
        }
        return -1;
    }

    public int A1D(int i) {
        if (i != 1) {
            if (i != 2) {
                if (i != 17) {
                    if (i != 33) {
                        if (i != 66) {
                            if (i != 130 || this.A01 != 1) {
                                return Integer.MIN_VALUE;
                            }
                        } else if (this.A01 == 0) {
                            return 1;
                        } else {
                            return Integer.MIN_VALUE;
                        }
                    } else if (this.A01 != 1) {
                        return Integer.MIN_VALUE;
                    }
                } else if (this.A01 == 0) {
                    return -1;
                } else {
                    return Integer.MIN_VALUE;
                }
            } else if (this.A01 != 1 && A1b()) {
                return -1;
            }
            return 1;
        } else if (this.A01 != 1 && A1b()) {
            return 1;
        }
        return -1;
    }

    public int A1E(AnonymousClass0PR r8, AnonymousClass0QS r9, C05480Ps r10, boolean z) {
        int i;
        int i2 = r8.A00;
        int i3 = r8.A07;
        if (i3 != Integer.MIN_VALUE) {
            if (i2 < 0) {
                r8.A07 = i3 + i2;
            }
            A1X(r8, r9);
        }
        int i4 = r8.A00 + r8.A02;
        C04790Nb r2 = this.A0D;
        while (true) {
            if ((!r8.A09 && i4 <= 0) || (i = r8.A01) < 0 || i >= r10.A00()) {
                break;
            }
            r2.A00 = 0;
            r2.A01 = false;
            r2.A03 = false;
            r2.A02 = false;
            A1V(r2, r8, r9, r10);
            if (r2.A01) {
                break;
            }
            int i5 = r8.A06;
            int i6 = r2.A00;
            r8.A06 = i5 + (r8.A05 * i6);
            if (!r2.A03 || this.A04.A08 != null || !r10.A08) {
                r8.A00 -= i6;
                i4 -= i6;
            }
            int i7 = r8.A07;
            if (i7 != Integer.MIN_VALUE) {
                int i8 = i7 + i6;
                r8.A07 = i8;
                int i9 = r8.A00;
                if (i9 < 0) {
                    r8.A07 = i8 + i9;
                }
                A1X(r8, r9);
            }
            if (z && r2.A02) {
                break;
            }
        }
        return i2 - r8.A00;
    }

    public int A1F(AnonymousClass0QS r6, C05480Ps r7, int i) {
        if (!(A06() == 0 || i == 0)) {
            this.A04.A0A = true;
            A1O();
            int i2 = -1;
            if (i > 0) {
                i2 = 1;
            }
            int abs = Math.abs(i);
            A1Y(r7, i2, abs, true);
            AnonymousClass0PR r0 = this.A04;
            int A1E = r0.A07 + A1E(r0, r6, r7, false);
            if (A1E >= 0) {
                if (abs > A1E) {
                    i = i2 * A1E;
                }
                this.A06.A0E(-i);
                this.A04.A04 = i;
                return i;
            }
        }
        return 0;
    }

    public final int A1G(C05480Ps r8) {
        if (A06() == 0) {
            return 0;
        }
        A1O();
        AbstractC06220Sq r3 = this.A06;
        boolean z = this.A0A;
        boolean z2 = true ^ z;
        return AnonymousClass0TD.A00(A1N(z2), A1M(z2), r3, this, r8, z);
    }

    public final int A1H(C05480Ps r9) {
        if (A06() == 0) {
            return 0;
        }
        A1O();
        AbstractC06220Sq r3 = this.A06;
        boolean z = this.A0A;
        boolean z2 = true ^ z;
        return AnonymousClass0TD.A02(A1N(z2), A1M(z2), r3, this, r9, z, this.A09);
    }

    public final int A1I(C05480Ps r8) {
        if (A06() == 0) {
            return 0;
        }
        A1O();
        AbstractC06220Sq r3 = this.A06;
        boolean z = this.A0A;
        boolean z2 = true ^ z;
        return AnonymousClass0TD.A01(A1N(z2), A1M(z2), r3, this, r8, z);
    }

    public View A1J(int i, int i2) {
        AnonymousClass0P8 r0;
        A1O();
        if (i2 <= i && i2 >= i) {
            return A0D(i);
        }
        int i3 = 4161;
        int i4 = 4097;
        if (this.A06.A0B(A0D(i)) < this.A06.A06()) {
            i3 = 16644;
            i4 = 16388;
        }
        if (this.A01 == 0) {
            r0 = super.A08;
        } else {
            r0 = super.A09;
        }
        return r0.A00(i, i2, i3, i4);
    }

    public View A1K(int i, int i2, boolean z, boolean z2) {
        AnonymousClass0P8 r0;
        A1O();
        int i3 = 320;
        int i4 = 320;
        if (z) {
            i4 = 24579;
        }
        if (!z2) {
            i3 = 0;
        }
        if (this.A01 == 0) {
            r0 = super.A08;
        } else {
            r0 = super.A09;
        }
        return r0.A00(i, i2, i4, i3);
    }

    public View A1L(AnonymousClass0QS r8, C05480Ps r9, int i, int i2, int i3) {
        A1O();
        int A06 = this.A06.A06();
        int A02 = this.A06.A02();
        int i4 = -1;
        if (i2 > i) {
            i4 = 1;
        }
        View view = null;
        View view2 = null;
        while (i != i2) {
            View A0D = A0D(i);
            int A022 = AnonymousClass02H.A02(A0D);
            if (A022 >= 0 && A022 < i3) {
                if ((((AnonymousClass0B6) A0D.getLayoutParams()).A00.A00 & 8) != 0) {
                    if (view2 == null) {
                        view2 = A0D;
                    }
                } else if (this.A06.A0B(A0D) < A02 && this.A06.A08(A0D) >= A06) {
                    return A0D;
                } else {
                    if (view == null) {
                        view = A0D;
                    }
                }
            }
            i += i4;
        }
        return view == null ? view2 : view;
    }

    public final View A1M(boolean z) {
        if (this.A09) {
            return A1K(0, A06(), z, true);
        }
        return A1K(A06() - 1, -1, z, true);
    }

    public final View A1N(boolean z) {
        if (this.A09) {
            return A1K(A06() - 1, -1, z, true);
        }
        return A1K(0, A06(), z, true);
    }

    public void A1O() {
        if (this.A04 == null) {
            this.A04 = new AnonymousClass0PR();
        }
    }

    public final void A1P() {
        boolean z;
        if (this.A01 == 1 || !A1b()) {
            z = this.A08;
        } else {
            z = !this.A08;
        }
        this.A09 = z;
    }

    public void A1Q(int i) {
        if (i == 0 || i == 1) {
            A13(null);
            if (i != this.A01 || this.A06 == null) {
                AbstractC06220Sq A00 = AbstractC06220Sq.A00(this, i);
                this.A06 = A00;
                this.A0C.A02 = A00;
                this.A01 = i;
                A0E();
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder("invalid orientation:");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public void A1R(int i, int i2) {
        this.A02 = i;
        this.A03 = i2;
        C06880Vl r1 = this.A05;
        if (r1 != null) {
            r1.A01 = -1;
        }
        A0E();
    }

    public final void A1S(int i, int i2) {
        this.A04.A00 = this.A06.A02() - i2;
        AnonymousClass0PR r3 = this.A04;
        int i3 = 1;
        if (this.A09) {
            i3 = -1;
        }
        r3.A03 = i3;
        r3.A01 = i;
        r3.A05 = 1;
        r3.A06 = i2;
        r3.A07 = Integer.MIN_VALUE;
    }

    public final void A1T(int i, int i2) {
        this.A04.A00 = i2 - this.A06.A06();
        AnonymousClass0PR r3 = this.A04;
        r3.A01 = i;
        int i3 = -1;
        if (this.A09) {
            i3 = 1;
        }
        r3.A03 = i3;
        r3.A05 = -1;
        r3.A06 = i2;
        r3.A07 = Integer.MIN_VALUE;
    }

    public void A1V(C04790Nb r12, AnonymousClass0PR r13, AnonymousClass0QS r14, C05480Ps r15) {
        int A0B;
        int A0A;
        int i;
        int i2;
        View A00 = r13.A00(r14);
        if (A00 == null) {
            r12.A01 = true;
            return;
        }
        AnonymousClass0B6 r7 = (AnonymousClass0B6) A00.getLayoutParams();
        List list = r13.A08;
        boolean z = this.A09;
        int i3 = r13.A05;
        if (list == null) {
            boolean z2 = false;
            if (i3 == -1) {
                z2 = true;
            }
            if (z == z2) {
                A0I(A00, -1, false);
            } else {
                A0I(A00, 0, false);
            }
        } else {
            boolean z3 = false;
            if (i3 == -1) {
                z3 = true;
            }
            if (z == z3) {
                A0I(A00, -1, true);
            } else {
                A0I(A00, 0, true);
            }
        }
        AnonymousClass0B6 r8 = (AnonymousClass0B6) A00.getLayoutParams();
        Rect A0A2 = super.A07.A0A(A00);
        int i4 = 0 + A0A2.left + A0A2.right;
        int i5 = 0 + A0A2.top + A0A2.bottom;
        int A01 = AnonymousClass02H.A01(super.A03, super.A04, A09() + A0A() + ((ViewGroup.MarginLayoutParams) r8).leftMargin + ((ViewGroup.MarginLayoutParams) r8).rightMargin + i4, ((ViewGroup.MarginLayoutParams) r8).width, A14());
        int A012 = AnonymousClass02H.A01(super.A00, super.A01, A0B() + A08() + ((ViewGroup.MarginLayoutParams) r8).topMargin + ((ViewGroup.MarginLayoutParams) r8).bottomMargin + i5, ((ViewGroup.MarginLayoutParams) r8).height, A15());
        if (A0V(A00, r8, A01, A012)) {
            A00.measure(A01, A012);
        }
        r12.A00 = this.A06.A09(A00);
        if (this.A01 == 1) {
            if (A1b()) {
                i = super.A03 - A0A();
                i2 = i - this.A06.A0A(A00);
            } else {
                i2 = A09();
                i = this.A06.A0A(A00) + i2;
            }
            int i6 = r13.A05;
            A0A = r13.A06;
            int i7 = r12.A00;
            A0B = A0A - i7;
            if (i6 != -1) {
                A0A = i7 + A0A;
                A0B = A0A;
            }
        } else {
            A0B = A0B();
            A0A = this.A06.A0A(A00) + A0B;
            int i8 = r13.A05;
            int i9 = r13.A06;
            int i10 = r12.A00;
            if (i8 == -1) {
                i2 = i9 - i10;
                i = i9;
            } else {
                i = i10 + i9;
                i2 = i9;
            }
        }
        AnonymousClass02H.A04(A00, i2, A0B, i, A0A);
        int i11 = r7.A00.A00;
        if (!((i11 & 8) == 0 && (i11 & 2) == 0)) {
            r12.A03 = true;
        }
        r12.A02 = A00.hasFocusable();
    }

    public void A1W(AnonymousClass0PR r4, AbstractC11870h0 r5, C05480Ps r6) {
        int i = r4.A01;
        if (i >= 0 && i < r6.A00()) {
            r5.A5o(i, Math.max(0, r4.A07));
        }
    }

    public final void A1X(AnonymousClass0PR r8, AnonymousClass0QS r9) {
        int i;
        int i2;
        if (r8.A0A && !r8.A09) {
            int i3 = r8.A05;
            int i4 = r8.A07;
            if (i3 == -1) {
                int A06 = A06();
                if (i4 >= 0) {
                    int A01 = this.A06.A01() - i4;
                    i = A06 - 1;
                    i2 = i;
                    if (this.A09) {
                        i = 0;
                        i2 = 0;
                        while (i2 < A06) {
                            View A0D = A0D(i2);
                            if (this.A06.A0B(A0D) >= A01 && this.A06.A0D(A0D) >= A01) {
                                i2++;
                            }
                        }
                        return;
                    }
                    while (i2 >= 0) {
                        View A0D2 = A0D(i2);
                        if (this.A06.A0B(A0D2) >= A01 && this.A06.A0D(A0D2) >= A01) {
                            i2--;
                        }
                    }
                    return;
                }
                return;
            } else if (i4 >= 0) {
                int A062 = A06();
                i = 0;
                i2 = 0;
                if (this.A09) {
                    i = A062 - 1;
                    i2 = i;
                    while (i2 >= 0) {
                        View A0D3 = A0D(i2);
                        if (this.A06.A08(A0D3) <= i4 && this.A06.A0C(A0D3) <= i4) {
                            i2--;
                        }
                    }
                    return;
                }
                while (i2 < A062) {
                    View A0D4 = A0D(i2);
                    if (this.A06.A08(A0D4) <= i4 && this.A06.A0C(A0D4) <= i4) {
                        i2++;
                    }
                }
                return;
            } else {
                return;
            }
            if (i == i2) {
                return;
            }
            if (i2 > i) {
                while (true) {
                    i2--;
                    if (i2 >= i) {
                        A0Q(r9, i2);
                    } else {
                        return;
                    }
                }
            } else {
                while (i > i2) {
                    A0Q(r9, i);
                    i--;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r2.A01() != 0) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1Y(X.C05480Ps r7, int r8, int r9, boolean r10) {
        /*
            r6 = this;
            X.0PR r3 = r6.A04
            X.0Sq r2 = r6.A06
            int r0 = r2.A04()
            if (r0 != 0) goto L_0x0011
            int r1 = r2.A01()
            r0 = 1
            if (r1 == 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            r3.A09 = r0
            int r1 = r7.A06
            r0 = -1
            if (r1 == r0) goto L_0x00bf
            int r0 = r2.A07()
        L_0x001d:
            r3.A02 = r0
            X.0PR r2 = r6.A04
            r2.A05 = r8
            r5 = -1
            r0 = 1
            if (r8 != r0) goto L_0x0077
            int r1 = r2.A02
            X.0Sq r0 = r6.A06
            int r0 = r0.A03()
            int r1 = r1 + r0
            r2.A02 = r1
            boolean r0 = r6.A09
            if (r0 == 0) goto L_0x0070
            r0 = 0
        L_0x0037:
            android.view.View r4 = r6.A0D(r0)
            X.0PR r3 = r6.A04
            boolean r0 = r6.A09
            if (r0 != 0) goto L_0x0042
            r5 = 1
        L_0x0042:
            r3.A03 = r5
            int r2 = X.AnonymousClass02H.A02(r4)
            X.0PR r1 = r6.A04
            int r0 = r1.A03
            int r2 = r2 + r0
            r3.A01 = r2
            X.0Sq r0 = r6.A06
            int r0 = r0.A08(r4)
            r1.A06 = r0
            X.0Sq r0 = r6.A06
            int r1 = r0.A08(r4)
            X.0Sq r0 = r6.A06
            int r0 = r0.A02()
            int r1 = r1 - r0
        L_0x0064:
            X.0PR r0 = r6.A04
            r0.A00 = r9
            if (r10 == 0) goto L_0x006d
            int r9 = r9 - r1
            r0.A00 = r9
        L_0x006d:
            r0.A07 = r1
            return
        L_0x0070:
            int r0 = r6.A06()
            int r0 = r0 + -1
            goto L_0x0037
        L_0x0077:
            boolean r0 = r6.A09
            if (r0 == 0) goto L_0x00bd
            int r0 = r6.A06()
            int r0 = r0 + -1
        L_0x0081:
            android.view.View r4 = r6.A0D(r0)
            X.0PR r2 = r6.A04
            int r1 = r2.A02
            X.0Sq r0 = r6.A06
            int r0 = r0.A06()
            int r1 = r1 + r0
            r2.A02 = r1
            X.0PR r3 = r6.A04
            boolean r0 = r6.A09
            if (r0 == 0) goto L_0x0099
            r5 = 1
        L_0x0099:
            r3.A03 = r5
            int r2 = X.AnonymousClass02H.A02(r4)
            X.0PR r1 = r6.A04
            int r0 = r1.A03
            int r2 = r2 + r0
            r3.A01 = r2
            X.0Sq r0 = r6.A06
            int r0 = r0.A0B(r4)
            r1.A06 = r0
            X.0Sq r0 = r6.A06
            int r0 = r0.A0B(r4)
            int r1 = -r0
            X.0Sq r0 = r6.A06
            int r0 = r0.A06()
            int r1 = r1 + r0
            goto L_0x0064
        L_0x00bd:
            r0 = 0
            goto L_0x0081
        L_0x00bf:
            r0 = 0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.LinearLayoutManager.A1Y(X.0Ps, int, int, boolean):void");
    }

    public void A1Z(boolean z) {
        A13(null);
        if (z != this.A08) {
            this.A08 = z;
            A0E();
        }
    }

    public void A1a(boolean z) {
        A13(null);
        if (this.A0B != z) {
            this.A0B = z;
            A0E();
        }
    }

    public boolean A1b() {
        return AnonymousClass028.A05(super.A07) == 1;
    }

    @Override // X.AnonymousClass02I
    public PointF A7U(int i) {
        if (A06() == 0) {
            return null;
        }
        boolean z = false;
        int i2 = 1;
        if (i < AnonymousClass02H.A02(A0D(0))) {
            z = true;
        }
        if (z != this.A09) {
            i2 = -1;
        }
        float f = (float) i2;
        if (this.A01 == 0) {
            return new PointF(f, 0.0f);
        }
        return new PointF(0.0f, f);
    }
}
