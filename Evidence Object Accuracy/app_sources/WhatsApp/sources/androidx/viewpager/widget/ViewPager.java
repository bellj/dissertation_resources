package androidx.viewpager.widget;

import X.AbstractC015707l;
import X.AbstractC11910h4;
import X.AbstractC11920h5;
import X.AnonymousClass00T;
import X.AnonymousClass01A;
import X.AnonymousClass028;
import X.AnonymousClass070;
import X.AnonymousClass07R;
import X.AnonymousClass07S;
import X.AnonymousClass07T;
import X.AnonymousClass07U;
import X.AnonymousClass07V;
import X.AnonymousClass07W;
import X.AnonymousClass07Y;
import X.AnonymousClass08p;
import X.AnonymousClass0B3;
import X.AnonymousClass0E8;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* loaded from: classes.dex */
public class ViewPager extends ViewGroup {
    public static final Interpolator A0p = new AnonymousClass07S();
    public static final AnonymousClass07T A0q = new AnonymousClass07T();
    public static final Comparator A0r = new AnonymousClass07R();
    public static final int[] A0s = {16842931};
    public float A00 = -3.4028235E38f;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05 = Float.MAX_VALUE;
    public int A06 = -1;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public int A0H;
    public int A0I;
    public int A0J = 1;
    public int A0K;
    public int A0L;
    public int A0M = -1;
    public int A0N = 0;
    public int A0O;
    public int A0P;
    public Drawable A0Q;
    public Parcelable A0R = null;
    public VelocityTracker A0S;
    public EdgeEffect A0T;
    public EdgeEffect A0U;
    public AnonymousClass01A A0V;
    public AnonymousClass070 A0W;
    public AbstractC11920h5 A0X;
    public AnonymousClass08p A0Y;
    public ClassLoader A0Z = null;
    public ArrayList A0a;
    public List A0b;
    public List A0c;
    public boolean A0d;
    public boolean A0e = true;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;
    public boolean A0k;
    public final Rect A0l = new Rect();
    public final AnonymousClass07U A0m = new AnonymousClass07U();
    public final Runnable A0n = new AnonymousClass07V(this);
    public final ArrayList A0o = new ArrayList();
    public Scroller mScroller;

    /* loaded from: classes.dex */
    public @interface DecorView {
    }

    public ViewPager(Context context) {
        super(context);
        A07();
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A07();
    }

    private void A00(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.A06) {
            int i = 0;
            if (actionIndex == 0) {
                i = 1;
            }
            this.A03 = motionEvent.getX(i);
            this.A06 = motionEvent.getPointerId(i);
            VelocityTracker velocityTracker = this.A0S;
            if (velocityTracker != null) {
                velocityTracker.clear();
            }
        }
    }

    public final Rect A01(Rect rect, View view) {
        if (rect == null) {
            rect = new Rect();
        }
        if (view == null) {
            rect.set(0, 0, 0, 0);
        } else {
            rect.left = view.getLeft();
            rect.right = view.getRight();
            rect.top = view.getTop();
            rect.bottom = view.getBottom();
            ViewParent parent = view.getParent();
            while ((parent instanceof ViewGroup) && parent != this) {
                ViewGroup viewGroup = (ViewGroup) parent;
                rect.left += viewGroup.getLeft();
                rect.right += viewGroup.getRight();
                rect.top += viewGroup.getTop();
                rect.bottom += viewGroup.getBottom();
                parent = viewGroup.getParent();
            }
        }
        return rect;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0061, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass07U A02() {
        /*
            r12 = this;
            int r1 = r12.getClientWidth()
            r9 = 0
            if (r1 <= 0) goto L_0x005e
            int r0 = r12.getScrollX()
            float r7 = (float) r0
            float r1 = (float) r1
            float r7 = r7 / r1
            int r0 = r12.A0K
            float r6 = (float) r0
            float r6 = r6 / r1
        L_0x0012:
            r11 = 0
            r10 = 1
            r8 = 0
            r1 = -1
            r5 = 0
            r4 = 1
        L_0x0018:
            java.util.ArrayList r3 = r12.A0o
            int r0 = r3.size()
            if (r5 >= r0) goto L_0x0062
            java.lang.Object r2 = r3.get(r5)
            X.07U r2 = (X.AnonymousClass07U) r2
            if (r4 != 0) goto L_0x003f
            int r0 = r2.A02
            int r1 = r1 + r10
            if (r0 == r1) goto L_0x003f
            X.07U r2 = r12.A0m
            float r9 = r9 + r8
            float r9 = r9 + r6
            r2.A00 = r9
            r2.A02 = r1
            X.01A r0 = r12.A0V
            float r0 = r0.A00(r1)
            r2.A01 = r0
            int r5 = r5 + -1
        L_0x003f:
            float r9 = r2.A00
            float r8 = r2.A01
            float r1 = r8 + r9
            float r1 = r1 + r6
            if (r4 != 0) goto L_0x004c
            int r0 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r0 < 0) goto L_0x0062
        L_0x004c:
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0061
            int r0 = r3.size()
            int r0 = r0 - r10
            if (r5 == r0) goto L_0x0061
            int r1 = r2.A02
            int r5 = r5 + 1
            r11 = r2
            r4 = 0
            goto L_0x0018
        L_0x005e:
            r7 = 0
            r6 = 0
            goto L_0x0012
        L_0x0061:
            return r2
        L_0x0062:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A02():X.07U");
    }

    public AnonymousClass07U A03(int i) {
        int i2 = 0;
        while (true) {
            ArrayList arrayList = this.A0o;
            if (i2 >= arrayList.size()) {
                return null;
            }
            AnonymousClass07U r1 = (AnonymousClass07U) arrayList.get(i2);
            if (r1.A02 == i) {
                return r1;
            }
            i2++;
        }
    }

    public AnonymousClass07U A04(int i, int i2) {
        AnonymousClass07U r2 = new AnonymousClass07U();
        r2.A02 = i;
        r2.A03 = this.A0V.A05(this, i);
        r2.A01 = this.A0V.A00(i);
        if (i2 >= 0) {
            ArrayList arrayList = this.A0o;
            if (i2 < arrayList.size()) {
                arrayList.add(i2, r2);
                return r2;
            }
        }
        this.A0o.add(r2);
        return r2;
    }

    public AnonymousClass07U A05(View view) {
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A0o;
            if (i >= arrayList.size()) {
                return null;
            }
            AnonymousClass07U r2 = (AnonymousClass07U) arrayList.get(i);
            if (this.A0V.A0E(view, r2.A03)) {
                return r2;
            }
            i++;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (r3.size() >= r8) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
            r12 = this;
            X.01A r0 = r12.A0V
            int r8 = r0.A01()
            r12.A0E = r8
            java.util.ArrayList r3 = r12.A0o
            int r1 = r3.size()
            int r0 = r12.A0J
            int r0 = r0 << 1
            r5 = 1
            int r0 = r0 + r5
            r4 = 0
            if (r1 >= r0) goto L_0x001e
            int r0 = r3.size()
            r11 = 1
            if (r0 < r8) goto L_0x001f
        L_0x001e:
            r11 = 0
        L_0x001f:
            int r6 = r12.A0A
            r7 = 0
            r10 = 0
        L_0x0023:
            int r0 = r3.size()
            if (r7 >= r0) goto L_0x0072
            java.lang.Object r9 = r3.get(r7)
            X.07U r9 = (X.AnonymousClass07U) r9
            X.01A r1 = r12.A0V
            java.lang.Object r0 = r9.A03
            int r2 = r1.A02(r0)
            r0 = -1
            if (r2 == r0) goto L_0x0064
            r0 = -2
            if (r2 != r0) goto L_0x0066
            r3.remove(r7)
            int r7 = r7 + -1
            if (r10 != 0) goto L_0x004a
            X.01A r0 = r12.A0V
            r0.A0B(r12)
            r10 = 1
        L_0x004a:
            X.01A r2 = r12.A0V
            int r1 = r9.A02
            java.lang.Object r0 = r9.A03
            r2.A0D(r12, r0, r1)
            int r1 = r12.A0A
            int r0 = r9.A02
            if (r1 != r0) goto L_0x0063
            int r0 = r8 + -1
            int r0 = java.lang.Math.min(r1, r0)
            int r6 = java.lang.Math.max(r4, r0)
        L_0x0063:
            r11 = 1
        L_0x0064:
            int r7 = r7 + r5
            goto L_0x0023
        L_0x0066:
            int r1 = r9.A02
            if (r1 == r2) goto L_0x0064
            int r0 = r12.A0A
            if (r1 != r0) goto L_0x006f
            r6 = r2
        L_0x006f:
            r9.A02 = r2
            goto L_0x0063
        L_0x0072:
            if (r10 == 0) goto L_0x0079
            X.01A r0 = r12.A0V
            r0.A0A(r12)
        L_0x0079:
            java.util.Comparator r0 = androidx.viewpager.widget.ViewPager.A0r
            java.util.Collections.sort(r3, r0)
            if (r11 == 0) goto L_0x00a1
            int r3 = r12.getChildCount()
            r2 = 0
        L_0x0085:
            if (r2 >= r3) goto L_0x009b
            android.view.View r0 = r12.getChildAt(r2)
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            X.0B3 r1 = (X.AnonymousClass0B3) r1
            boolean r0 = r1.A04
            if (r0 != 0) goto L_0x0098
            r0 = 0
            r1.A00 = r0
        L_0x0098:
            int r2 = r2 + 1
            goto L_0x0085
        L_0x009b:
            r12.A0D(r6, r4, r4, r5)
            r12.requestLayout()
        L_0x00a1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A06():void");
    }

    public void A07() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.mScroller = new Scroller(context, A0p);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f = context.getResources().getDisplayMetrics().density;
        this.A0P = viewConfiguration.getScaledPagingTouchSlop();
        this.A0I = (int) (400.0f * f);
        this.A0H = viewConfiguration.getScaledMaximumFlingVelocity();
        this.A0T = new EdgeEffect(context);
        this.A0U = new EdgeEffect(context);
        this.A0F = (int) (25.0f * f);
        this.A09 = (int) (2.0f * f);
        this.A0C = (int) (f * 16.0f);
        AnonymousClass028.A0g(this, new AnonymousClass07W(this));
        if (getImportantForAccessibility() == 0) {
            AnonymousClass028.A0a(this, 1);
        }
        AnonymousClass028.A0h(this, new AnonymousClass07Y(this));
    }

    public final void A08() {
        if (this.A0D != 0) {
            ArrayList arrayList = this.A0a;
            if (arrayList == null) {
                this.A0a = new ArrayList();
            } else {
                arrayList.clear();
            }
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                this.A0a.add(getChildAt(i));
            }
            Collections.sort(this.A0a, A0q);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        if (r0 == r1) goto L_0x0054;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(int r19) {
        /*
        // Method dump skipped, instructions count: 816
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A09(int):void");
    }

    public final void A0A(int i) {
        AnonymousClass070 r0 = this.A0W;
        if (r0 != null) {
            r0.ATQ(i);
        }
        List list = this.A0c;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                AnonymousClass070 r02 = (AnonymousClass070) this.A0c.get(i2);
                if (r02 != null) {
                    r02.ATQ(i);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0B(int r12, float r13, int r14) {
        /*
            r11 = this;
            int r0 = r11.A0B
            r6 = 0
            r5 = 1
            if (r0 <= 0) goto L_0x0069
            int r10 = r11.getScrollX()
            int r4 = r11.getPaddingLeft()
            int r9 = r11.getPaddingRight()
            int r8 = r11.getWidth()
            int r3 = r11.getChildCount()
            r2 = 0
        L_0x001b:
            if (r2 >= r3) goto L_0x0069
            android.view.View r1 = r11.getChildAt(r2)
            android.view.ViewGroup$LayoutParams r7 = r1.getLayoutParams()
            X.0B3 r7 = (X.AnonymousClass0B3) r7
            boolean r0 = r7.A04
            if (r0 == 0) goto L_0x0044
            int r0 = r7.A02
            r7 = r0 & 7
            if (r7 == r5) goto L_0x005a
            r0 = 3
            if (r7 == r0) goto L_0x0054
            r0 = 5
            if (r7 == r0) goto L_0x0047
            r0 = r4
        L_0x0038:
            int r4 = r4 + r10
            int r7 = r1.getLeft()
            int r4 = r4 - r7
            if (r4 == 0) goto L_0x0043
            r1.offsetLeftAndRight(r4)
        L_0x0043:
            r4 = r0
        L_0x0044:
            int r2 = r2 + 1
            goto L_0x001b
        L_0x0047:
            int r7 = r8 - r9
            int r0 = r1.getMeasuredWidth()
            int r7 = r7 - r0
            int r0 = r1.getMeasuredWidth()
            int r9 = r9 + r0
            goto L_0x0066
        L_0x0054:
            int r0 = r1.getWidth()
            int r0 = r0 + r4
            goto L_0x0038
        L_0x005a:
            int r0 = r1.getMeasuredWidth()
            int r0 = r8 - r0
            int r0 = r0 >> 1
            int r7 = java.lang.Math.max(r0, r4)
        L_0x0066:
            r0 = r4
            r4 = r7
            goto L_0x0038
        L_0x0069:
            X.070 r0 = r11.A0W
            if (r0 == 0) goto L_0x0070
            r0.ATP(r12, r13, r14)
        L_0x0070:
            java.util.List r0 = r11.A0c
            if (r0 == 0) goto L_0x008b
            r2 = 0
            int r1 = r0.size()
        L_0x0079:
            if (r2 >= r1) goto L_0x008b
            java.util.List r0 = r11.A0c
            java.lang.Object r0 = r0.get(r2)
            X.070 r0 = (X.AnonymousClass070) r0
            if (r0 == 0) goto L_0x0088
            r0.ATP(r12, r13, r14)
        L_0x0088:
            int r2 = r2 + 1
            goto L_0x0079
        L_0x008b:
            X.0h5 r0 = r11.A0X
            if (r0 == 0) goto L_0x00bb
            int r4 = r11.getScrollX()
            int r3 = r11.getChildCount()
        L_0x0097:
            if (r6 >= r3) goto L_0x00bb
            android.view.View r2 = r11.getChildAt(r6)
            android.view.ViewGroup$LayoutParams r0 = r2.getLayoutParams()
            X.0B3 r0 = (X.AnonymousClass0B3) r0
            boolean r0 = r0.A04
            if (r0 != 0) goto L_0x00b8
            int r0 = r2.getLeft()
            int r0 = r0 - r4
            float r1 = (float) r0
            int r0 = r11.getClientWidth()
            float r0 = (float) r0
            float r1 = r1 / r0
            X.0h5 r0 = r11.A0X
            r0.Af5(r2, r1)
        L_0x00b8:
            int r6 = r6 + 1
            goto L_0x0097
        L_0x00bb:
            r11.A0d = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A0B(int, float, int):void");
    }

    public final void A0C(int i, int i2, int i3, int i4) {
        int i5;
        float f;
        if (i2 <= 0 || this.A0o.isEmpty()) {
            AnonymousClass07U A03 = A03(this.A0A);
            if (A03 != null) {
                f = Math.min(A03.A00, this.A05);
            } else {
                f = 0.0f;
            }
            i5 = (int) (f * ((float) ((i - getPaddingLeft()) - getPaddingRight())));
            if (i5 != getScrollX()) {
                A0I(false);
            } else {
                return;
            }
        } else if (!this.mScroller.isFinished()) {
            this.mScroller.setFinalX(getCurrentItem() * getClientWidth());
            return;
        } else {
            i5 = (int) ((((float) getScrollX()) / ((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4))) * ((float) (((i - getPaddingLeft()) - getPaddingRight()) + i3)));
        }
        scrollTo(i5, getScrollY());
    }

    public void A0D(int i, int i2, boolean z, boolean z2) {
        AnonymousClass01A r0 = this.A0V;
        boolean z3 = false;
        if (r0 == null || r0.A01() <= 0 || (!z2 && this.A0A == i && this.A0o.size() != 0)) {
            setScrollingCacheEnabled(false);
            return;
        }
        if (i < 0) {
            i = 0;
        } else if (i >= this.A0V.A01()) {
            i = this.A0V.A01() - 1;
        }
        int i3 = this.A0J;
        int i4 = this.A0A;
        if (i > i4 + i3 || i < i4 - i3) {
            int i5 = 0;
            while (true) {
                ArrayList arrayList = this.A0o;
                if (i5 >= arrayList.size()) {
                    break;
                }
                ((AnonymousClass07U) arrayList.get(i5)).A04 = true;
                i5++;
            }
        }
        if (i4 != i) {
            z3 = true;
        }
        if (this.A0e) {
            this.A0A = i;
            if (z3) {
                A0A(i);
            }
            requestLayout();
            return;
        }
        A09(i);
        A0E(i, i2, z, z3);
    }

    public final void A0E(int i, int i2, boolean z, boolean z2) {
        int i3;
        int scrollX;
        int A00;
        AnonymousClass07U A03 = A03(i);
        if (A03 != null) {
            i3 = (int) (((float) getClientWidth()) * Math.max(this.A00, Math.min(A03.A00, this.A05)));
        } else {
            i3 = 0;
        }
        if (z) {
            if (getChildCount() == 0) {
                setScrollingCacheEnabled(false);
            } else {
                Scroller scroller = this.mScroller;
                if (scroller == null || scroller.isFinished()) {
                    scrollX = getScrollX();
                } else {
                    boolean z3 = this.A0h;
                    Scroller scroller2 = this.mScroller;
                    if (z3) {
                        scrollX = scroller2.getCurrX();
                    } else {
                        scrollX = scroller2.getStartX();
                    }
                    this.mScroller.abortAnimation();
                    setScrollingCacheEnabled(false);
                }
                int scrollY = getScrollY();
                int i4 = i3 - scrollX;
                int i5 = 0 - scrollY;
                if (i4 == 0 && i5 == 0) {
                    A0I(false);
                    A09(this.A0A);
                    setScrollState(0);
                } else {
                    setScrollingCacheEnabled(true);
                    setScrollState(2);
                    int clientWidth = getClientWidth();
                    float abs = (float) Math.abs(i4);
                    float f = (float) clientWidth;
                    float f2 = (float) (clientWidth >> 1);
                    float sin = f2 + (((float) Math.sin((double) ((Math.min(1.0f, (abs * 1.0f) / f) - 0.5f) * 0.47123894f))) * f2);
                    int abs2 = Math.abs(i2);
                    if (abs2 > 0) {
                        A00 = Math.round(Math.abs(sin / ((float) abs2)) * 1000.0f) << 2;
                    } else {
                        A00 = (int) (((abs / ((f * this.A0V.A00(this.A0A)) + ((float) this.A0K))) + 1.0f) * 100.0f);
                    }
                    int min = Math.min(A00, 600);
                    this.A0h = false;
                    this.mScroller.startScroll(scrollX, scrollY, i4, i5, min);
                    postInvalidateOnAnimation();
                }
            }
            if (z2) {
                A0A(i);
                return;
            }
            return;
        }
        if (z2) {
            A0A(i);
        }
        A0I(false);
        scrollTo(i3, 0);
        A0M(i3);
    }

    public void A0F(int i, boolean z) {
        this.A0j = false;
        A0D(i, 0, z, false);
    }

    public void A0G(AnonymousClass070 r2) {
        List list = this.A0c;
        if (list == null) {
            list = new ArrayList();
            this.A0c = list;
        }
        list.add(r2);
    }

    public void A0H(AbstractC11920h5 r7, boolean z) {
        int i = 1;
        boolean z2 = false;
        if (r7 != null) {
            z2 = true;
        }
        boolean z3 = false;
        if (this.A0X != null) {
            z3 = true;
        }
        boolean z4 = false;
        if (z2 != z3) {
            z4 = true;
        }
        this.A0X = r7;
        setChildrenDrawingOrderEnabled(z2);
        if (z2) {
            if (z) {
                i = 2;
            }
            this.A0D = i;
            this.A0L = 2;
        } else {
            this.A0D = 0;
        }
        if (z4) {
            A09(this.A0A);
        }
    }

    public final void A0I(boolean z) {
        boolean z2 = false;
        if (this.A0N == 2) {
            z2 = true;
            setScrollingCacheEnabled(false);
            if (!this.mScroller.isFinished()) {
                this.mScroller.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.mScroller.getCurrX();
                int currY = this.mScroller.getCurrY();
                if (!(scrollX == currX && scrollY == currY)) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        A0M(currX);
                    }
                }
            }
        }
        this.A0j = false;
        int i = 0;
        while (true) {
            ArrayList arrayList = this.A0o;
            if (i >= arrayList.size()) {
                break;
            }
            AnonymousClass07U r1 = (AnonymousClass07U) arrayList.get(i);
            if (r1.A04) {
                r1.A04 = false;
                z2 = true;
            }
            i++;
        }
        if (z2) {
            Runnable runnable = this.A0n;
            if (z) {
                postOnAnimation(runnable);
            } else {
                runnable.run();
            }
        }
    }

    public final boolean A0J() {
        this.A06 = -1;
        this.A0g = false;
        this.A0i = false;
        VelocityTracker velocityTracker = this.A0S;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.A0S = null;
        }
        this.A0T.onRelease();
        this.A0U.onRelease();
        return this.A0T.isFinished() || this.A0U.isFinished();
    }

    public final boolean A0K(float f) {
        boolean z;
        boolean z2;
        this.A03 = f;
        float scrollX = ((float) getScrollX()) + (this.A03 - f);
        float clientWidth = (float) getClientWidth();
        float f2 = this.A00 * clientWidth;
        float f3 = this.A05 * clientWidth;
        ArrayList arrayList = this.A0o;
        boolean z3 = false;
        AnonymousClass07U r1 = (AnonymousClass07U) arrayList.get(0);
        AnonymousClass07U r8 = (AnonymousClass07U) arrayList.get(arrayList.size() - 1);
        if (r1.A02 != 0) {
            f2 = r1.A00 * clientWidth;
            z = false;
        } else {
            z = true;
        }
        if (r8.A02 != this.A0V.A01() - 1) {
            f3 = r8.A00 * clientWidth;
            z2 = false;
        } else {
            z2 = true;
        }
        if (scrollX < f2) {
            if (z) {
                this.A0T.onPull(Math.abs(f2 - scrollX) / clientWidth);
                z3 = true;
            }
            scrollX = f2;
        } else if (scrollX > f3) {
            if (z2) {
                this.A0U.onPull(Math.abs(scrollX - f3) / clientWidth);
                z3 = true;
            }
            scrollX = f3;
        }
        int i = (int) scrollX;
        this.A03 += scrollX - ((float) i);
        scrollTo(i, getScrollY());
        A0M(i);
        return z3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0081, code lost:
        if (r1 >= r0) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009b, code lost:
        if (r8 != 2) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b0, code lost:
        if (r1 <= r0) goto L_0x00b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c7, code lost:
        if (r6 != false) goto L_0x008c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0L(int r8) {
        /*
            r7 = this;
            android.view.View r4 = r7.findFocus()
            r3 = 1
            r6 = 0
            r5 = 0
            if (r4 == r7) goto L_0x005e
            if (r4 == 0) goto L_0x005f
            android.view.ViewParent r1 = r4.getParent()
        L_0x000f:
            boolean r0 = r1 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x001a
            if (r1 == r7) goto L_0x005f
            android.view.ViewParent r1 = r1.getParent()
            goto L_0x000f
        L_0x001a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r0 = r4.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r2.append(r0)
            android.view.ViewParent r1 = r4.getParent()
        L_0x002e:
            boolean r0 = r1 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x0047
            java.lang.String r0 = " => "
            r2.append(r0)
            java.lang.Class r0 = r1.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r2.append(r0)
            android.view.ViewParent r1 = r1.getParent()
            goto L_0x002e
        L_0x0047:
            java.lang.String r0 = "arrowScroll tried to find focus based on non-child current focused view "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r2.toString()
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "ViewPager"
            android.util.Log.e(r0, r1)
        L_0x005e:
            r4 = r5
        L_0x005f:
            android.view.FocusFinder r0 = android.view.FocusFinder.getInstance()
            android.view.View r5 = r0.findNextFocus(r7, r4, r8)
            r1 = 66
            r0 = 17
            if (r5 == 0) goto L_0x0094
            if (r5 == r4) goto L_0x0094
            if (r8 != r0) goto L_0x009e
            android.graphics.Rect r2 = r7.A0l
            android.graphics.Rect r0 = r7.A01(r2, r5)
            int r1 = r0.left
            android.graphics.Rect r0 = r7.A01(r2, r4)
            int r0 = r0.left
            if (r4 == 0) goto L_0x00c3
            if (r1 < r0) goto L_0x00c3
        L_0x0083:
            int r0 = r7.A0A
            if (r0 <= 0) goto L_0x0093
            int r0 = r0 - r3
        L_0x0088:
            r7.A0F(r0, r3)
            r6 = 1
        L_0x008c:
            int r0 = android.view.SoundEffectConstants.getContantForFocusDirection(r8)
            r7.playSoundEffect(r0)
        L_0x0093:
            return r6
        L_0x0094:
            if (r8 == r0) goto L_0x0083
            if (r8 == r3) goto L_0x0083
            if (r8 == r1) goto L_0x00b2
            r0 = 2
            if (r8 != r0) goto L_0x0093
            goto L_0x00b2
        L_0x009e:
            if (r8 != r1) goto L_0x0093
            android.graphics.Rect r2 = r7.A0l
            android.graphics.Rect r0 = r7.A01(r2, r5)
            int r1 = r0.left
            android.graphics.Rect r0 = r7.A01(r2, r4)
            int r0 = r0.left
            if (r4 == 0) goto L_0x00c3
            if (r1 > r0) goto L_0x00c3
        L_0x00b2:
            X.01A r0 = r7.A0V
            if (r0 == 0) goto L_0x0093
            int r1 = r7.A0A
            int r0 = r0.A01()
            int r0 = r0 - r3
            if (r1 >= r0) goto L_0x0093
            int r0 = r7.A0A
            int r0 = r0 + r3
            goto L_0x0088
        L_0x00c3:
            boolean r6 = r5.requestFocus()
            if (r6 == 0) goto L_0x0093
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.A0L(int):boolean");
    }

    public final boolean A0M(int i) {
        if (this.A0o.size() == 0) {
            if (!this.A0e) {
                this.A0d = false;
                A0B(0, 0.0f, 0);
                if (!this.A0d) {
                    throw new IllegalStateException("onPageScrolled did not call superclass implementation");
                }
            }
            return false;
        }
        AnonymousClass07U A02 = A02();
        int clientWidth = getClientWidth();
        int i2 = this.A0K;
        int i3 = clientWidth + i2;
        float f = (float) i2;
        float f2 = (float) clientWidth;
        int i4 = A02.A02;
        float f3 = ((((float) i) / f2) - A02.A00) / (A02.A01 + (f / f2));
        this.A0d = false;
        A0B(i4, f3, (int) (((float) i3) * f3));
        if (this.A0d) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    public boolean A0N(View view, int i, int i2, int i3, boolean z) {
        int i4;
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                int i5 = i2 + scrollX;
                if (i5 >= childAt.getLeft() && i5 < childAt.getRight() && (i4 = i3 + scrollY) >= childAt.getTop() && i4 < childAt.getBottom() && A0N(childAt, i, i5 - childAt.getLeft(), i4 - childAt.getTop(), true)) {
                    return true;
                }
            }
        }
        if (!z || !view.canScrollHorizontally(-i)) {
            return false;
        }
        return true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void addFocusables(ArrayList arrayList, int i, int i2) {
        AnonymousClass07U A05;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                if (childAt.getVisibility() == 0 && (A05 = A05(childAt)) != null && A05.A02 == this.A0A) {
                    childAt.addFocusables(arrayList, i, i2);
                }
            }
            if (descendantFocusability == 262144 && size != arrayList.size()) {
                return;
            }
        }
        if (!isFocusable()) {
            return;
        }
        if ((i2 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) {
            arrayList.add(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void addTouchables(ArrayList arrayList) {
        AnonymousClass07U A05;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (A05 = A05(childAt)) != null && A05.A02 == this.A0A) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (!checkLayoutParams(layoutParams)) {
            layoutParams = new AnonymousClass0B3();
        }
        AnonymousClass0B3 r3 = (AnonymousClass0B3) layoutParams;
        boolean z = r3.A04;
        boolean z2 = false;
        if (view.getClass().getAnnotation(DecorView.class) != null) {
            z2 = true;
        }
        boolean z3 = z | z2;
        r3.A04 = z3;
        if (!this.A0f) {
            super.addView(view, i, layoutParams);
        } else if (!z3) {
            r3.A05 = true;
            addViewInLayout(view, i, layoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    @Override // android.view.View
    public boolean canScrollHorizontally(int i) {
        if (this.A0V == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.A00))) {
                return false;
            }
        } else if (i <= 0 || scrollX >= ((int) (((float) clientWidth) * this.A05))) {
            return false;
        }
        return true;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof AnonymousClass0B3) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.view.View
    public void computeScroll() {
        this.A0h = true;
        if (this.mScroller.isFinished() || !this.mScroller.computeScrollOffset()) {
            A0I(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.mScroller.getCurrX();
        int currY = this.mScroller.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!A0M(currX)) {
                this.mScroller.abortAnimation();
                scrollTo(0, currY);
            }
        }
        postInvalidateOnAnimation();
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r4) {
        /*
            r3 = this;
            boolean r0 = super.dispatchKeyEvent(r4)
            if (r0 != 0) goto L_0x0030
            int r0 = r4.getAction()
            if (r0 != 0) goto L_0x0067
            int r2 = r4.getKeyCode()
            r0 = 21
            r1 = 2
            if (r2 == r0) goto L_0x004d
            r0 = 22
            if (r2 == r0) goto L_0x0032
            r0 = 61
            if (r2 != r0) goto L_0x0067
            boolean r0 = r4.hasNoModifiers()
            if (r0 != 0) goto L_0x002a
            r1 = 1
            boolean r0 = r4.hasModifiers(r1)
            if (r0 == 0) goto L_0x0067
        L_0x002a:
            boolean r0 = r3.A0L(r1)
        L_0x002e:
            if (r0 == 0) goto L_0x0067
        L_0x0030:
            r0 = 1
            return r0
        L_0x0032:
            boolean r0 = r4.hasModifiers(r1)
            if (r0 == 0) goto L_0x0060
            X.01A r0 = r3.A0V
            if (r0 == 0) goto L_0x0067
            int r2 = r3.A0A
            int r0 = r0.A01()
            r1 = 1
            int r0 = r0 - r1
            if (r2 >= r0) goto L_0x0067
            int r0 = r3.A0A
            int r0 = r0 + r1
            r3.A0F(r0, r1)
            goto L_0x0030
        L_0x004d:
            boolean r0 = r4.hasModifiers(r1)
            if (r0 == 0) goto L_0x005d
            int r1 = r3.A0A
            if (r1 <= 0) goto L_0x0067
            r0 = 1
            int r1 = r1 - r0
            r3.A0F(r1, r0)
            goto L_0x0030
        L_0x005d:
            r0 = 17
            goto L_0x0062
        L_0x0060:
            r0 = 66
        L_0x0062:
            boolean r0 = r3.A0L(r0)
            goto L_0x002e
        L_0x0067:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    @Override // android.view.View
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AnonymousClass07U A05;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (A05 = A05(childAt)) != null && A05.A02 == this.A0A && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        AnonymousClass01A r0;
        super.draw(canvas);
        int overScrollMode = getOverScrollMode();
        boolean z = false;
        if (overScrollMode == 0 || (overScrollMode == 1 && (r0 = this.A0V) != null && r0.A01() > 1)) {
            if (!this.A0T.isFinished()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.A00 * ((float) width));
                this.A0T.setSize(height, width);
                z = false | this.A0T.draw(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.A0U.isFinished()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.A05 + 1.0f)) * ((float) width2));
                this.A0U.setSize(height2, width2);
                z |= this.A0U.draw(canvas);
                canvas.restoreToCount(save2);
            }
            if (z) {
                postInvalidateOnAnimation();
                return;
            }
            return;
        }
        this.A0T.finish();
        this.A0U.finish();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.A0Q;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new AnonymousClass0B3();
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new AnonymousClass0B3(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new AnonymousClass0B3();
    }

    public AnonymousClass01A getAdapter() {
        return this.A0V;
    }

    @Override // android.view.ViewGroup
    public int getChildDrawingOrder(int i, int i2) {
        if (this.A0D == 2) {
            i2 = (i - 1) - i2;
        }
        return ((AnonymousClass0B3) ((View) this.A0a.get(i2)).getLayoutParams()).A01;
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    public int getCurrentItem() {
        return this.A0A;
    }

    public int getOffscreenPageLimit() {
        return this.A0J;
    }

    public int getPageMargin() {
        return this.A0K;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A0e = true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        removeCallbacks(this.A0n);
        Scroller scroller = this.mScroller;
        if (scroller != null && !scroller.isFinished()) {
            this.mScroller.abortAnimation();
        }
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int i;
        float f;
        super.onDraw(canvas);
        if (this.A0K > 0 && this.A0Q != null) {
            ArrayList arrayList = this.A0o;
            if (arrayList.size() > 0 && this.A0V != null) {
                int scrollX = getScrollX();
                int width = getWidth();
                float f2 = (float) width;
                float f3 = ((float) this.A0K) / f2;
                int i2 = 0;
                AnonymousClass07U r5 = (AnonymousClass07U) arrayList.get(0);
                float f4 = r5.A00;
                int size = arrayList.size();
                int i3 = ((AnonymousClass07U) arrayList.get(size - 1)).A02;
                for (int i4 = r5.A02; i4 < i3; i4++) {
                    while (true) {
                        i = r5.A02;
                        if (i4 <= i || i2 >= size) {
                            break;
                        }
                        i2++;
                        r5 = (AnonymousClass07U) arrayList.get(i2);
                    }
                    if (i4 == i) {
                        float f5 = r5.A00 + r5.A01;
                        f = f5 * f2;
                        f4 = f5 + f3;
                    } else {
                        float A00 = this.A0V.A00(i4);
                        f = (f4 + A00) * f2;
                        f4 += A00 + f3;
                    }
                    float f6 = ((float) this.A0K) + f;
                    if (f6 > ((float) scrollX)) {
                        this.A0Q.setBounds(Math.round(f), this.A0O, Math.round(f6), this.A07);
                        this.A0Q.draw(canvas);
                    }
                    if (f > ((float) (scrollX + width))) {
                        return;
                    }
                }
            }
        }
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        float f;
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            A0J();
            return false;
        }
        if (action != 0) {
            if (this.A0g) {
                return true;
            }
            if (this.A0i) {
                return false;
            }
        }
        if (action == 0) {
            float x = motionEvent.getX();
            this.A01 = x;
            this.A03 = x;
            float y = motionEvent.getY();
            this.A02 = y;
            this.A04 = y;
            this.A06 = motionEvent.getPointerId(0);
            this.A0i = false;
            this.A0h = true;
            this.mScroller.computeScrollOffset();
            if (this.A0N != 2 || Math.abs(this.mScroller.getFinalX() - this.mScroller.getCurrX()) <= this.A09) {
                A0I(false);
                this.A0g = false;
            } else {
                this.mScroller.abortAnimation();
                this.A0j = false;
                A09(this.A0A);
                this.A0g = true;
                ViewParent parent = getParent();
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true);
                }
                setScrollState(1);
            }
        } else if (action == 2) {
            int i = this.A06;
            if (i != -1) {
                int findPointerIndex = motionEvent.findPointerIndex(i);
                float x2 = motionEvent.getX(findPointerIndex);
                float f2 = x2 - this.A03;
                float abs = Math.abs(f2);
                float y2 = motionEvent.getY(findPointerIndex);
                float abs2 = Math.abs(y2 - this.A02);
                if (f2 != 0.0f) {
                    float f3 = this.A03;
                    if ((f3 >= ((float) this.A0G) || f2 <= 0.0f) && ((f3 <= ((float) (getWidth() - this.A0G)) || f2 >= 0.0f) && A0N(this, (int) f2, (int) x2, (int) y2, false))) {
                        this.A03 = x2;
                        this.A04 = y2;
                        this.A0i = true;
                        return false;
                    }
                }
                float f4 = (float) this.A0P;
                if (abs > f4 && abs * 0.5f > abs2) {
                    this.A0g = true;
                    ViewParent parent2 = getParent();
                    if (parent2 != null) {
                        parent2.requestDisallowInterceptTouchEvent(true);
                    }
                    setScrollState(1);
                    int i2 = (f2 > 0.0f ? 1 : (f2 == 0.0f ? 0 : -1));
                    float f5 = this.A01;
                    float f6 = (float) this.A0P;
                    if (i2 > 0) {
                        f = f5 + f6;
                    } else {
                        f = f5 - f6;
                    }
                    this.A03 = f;
                    this.A04 = y2;
                    setScrollingCacheEnabled(true);
                } else if (abs2 > f4) {
                    this.A0i = true;
                }
                if (this.A0g && A0K(x2)) {
                    postInvalidateOnAnimation();
                }
            }
        } else if (action == 6) {
            A00(motionEvent);
        }
        VelocityTracker velocityTracker = this.A0S;
        if (velocityTracker == null) {
            velocityTracker = VelocityTracker.obtain();
            this.A0S = velocityTracker;
        }
        velocityTracker.addMovement(motionEvent);
        return this.A0g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007b  */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r16, int r17, int r18, int r19, int r20) {
        /*
        // Method dump skipped, instructions count: 274
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onLayout(boolean, int, int, int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0068, code lost:
        if (r2 == 80) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b1, code lost:
        if (r12 == false) goto L_0x0079;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r18, int r19) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onMeasure(int, int):void");
    }

    @Override // android.view.ViewGroup
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        AnonymousClass07U A05;
        int childCount = getChildCount();
        int i2 = -1;
        int i3 = childCount - 1;
        int i4 = -1;
        if ((i & 2) != 0) {
            i2 = childCount;
            i3 = 0;
            i4 = 1;
        }
        while (i3 != i2) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (A05 = A05(childAt)) != null && A05.A02 == this.A0A && childAt.requestFocus(i, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof AnonymousClass0E8)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        AnonymousClass0E8 r4 = (AnonymousClass0E8) parcelable;
        super.onRestoreInstanceState(((AbstractC015707l) r4).A00);
        AnonymousClass01A r2 = this.A0V;
        if (r2 != null) {
            r2.A09(r4.A01, r4.A02);
            A0D(r4.A00, 0, false, true);
            return;
        }
        this.A0M = r4.A00;
        this.A0R = r4.A01;
        this.A0Z = r4.A02;
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        AnonymousClass0E8 r1 = new AnonymousClass0E8(super.onSaveInstanceState());
        r1.A00 = this.A0A;
        AnonymousClass01A r0 = this.A0V;
        if (r0 != null) {
            r1.A01 = r0.A03();
        }
        return r1;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            int i5 = this.A0K;
            A0C(i, i3, i5, i5);
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        AnonymousClass01A r0;
        boolean z;
        float f;
        int i = 0;
        if ((motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) || (r0 = this.A0V) == null || r0.A01() == 0) {
            return false;
        }
        VelocityTracker velocityTracker = this.A0S;
        if (velocityTracker == null) {
            velocityTracker = VelocityTracker.obtain();
            this.A0S = velocityTracker;
        }
        velocityTracker.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            if (action == 1) {
                if (this.A0g) {
                    VelocityTracker velocityTracker2 = this.A0S;
                    velocityTracker2.computeCurrentVelocity(1000, (float) this.A0H);
                    int xVelocity = (int) velocityTracker2.getXVelocity(this.A06);
                    this.A0j = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    AnonymousClass07U A02 = A02();
                    float f2 = (float) clientWidth;
                    int i2 = A02.A02;
                    float f3 = ((((float) scrollX) / f2) - A02.A00) / (A02.A01 + (((float) this.A0K) / f2));
                    if (Math.abs((int) (motionEvent.getX(motionEvent.findPointerIndex(this.A06)) - this.A01)) <= this.A0F || Math.abs(xVelocity) <= this.A0I) {
                        float f4 = 0.6f;
                        if (i2 >= this.A0A) {
                            f4 = 0.4f;
                        }
                        i2 += (int) (f3 + f4);
                    } else if (xVelocity <= 0) {
                        i2++;
                    }
                    ArrayList arrayList = this.A0o;
                    if (arrayList.size() > 0) {
                        i2 = Math.max(((AnonymousClass07U) arrayList.get(0)).A02, Math.min(i2, ((AnonymousClass07U) arrayList.get(arrayList.size() - 1)).A02));
                    }
                    A0D(i2, xVelocity, true, true);
                    z = A0J();
                }
                return true;
            } else if (action != 2) {
                if (action != 3) {
                    if (action == 5) {
                        i = motionEvent.getActionIndex();
                        this.A03 = motionEvent.getX(i);
                    } else if (action == 6) {
                        A00(motionEvent);
                        this.A03 = motionEvent.getX(motionEvent.findPointerIndex(this.A06));
                    }
                } else if (this.A0g) {
                    A0E(this.A0A, 0, true, false);
                    z = A0J();
                }
                return true;
            } else {
                if (!this.A0g) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.A06);
                    if (findPointerIndex != -1) {
                        float x = motionEvent.getX(findPointerIndex);
                        float abs = Math.abs(x - this.A03);
                        float y = motionEvent.getY(findPointerIndex);
                        float abs2 = Math.abs(y - this.A04);
                        if (abs > ((float) this.A0P) && abs > abs2) {
                            this.A0g = true;
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                            float f5 = this.A01;
                            int i3 = ((x - f5) > 0.0f ? 1 : ((x - f5) == 0.0f ? 0 : -1));
                            float f6 = (float) this.A0P;
                            if (i3 > 0) {
                                f = f5 + f6;
                            } else {
                                f = f5 - f6;
                            }
                            this.A03 = f;
                            this.A04 = y;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent2 = getParent();
                            if (parent2 != null) {
                                parent2.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                    z = A0J();
                }
                if (this.A0g) {
                    z = false | A0K(motionEvent.getX(motionEvent.findPointerIndex(this.A06)));
                }
                return true;
            }
            if (z) {
                postInvalidateOnAnimation();
                return true;
            }
            return true;
        }
        this.mScroller.abortAnimation();
        this.A0j = false;
        A09(this.A0A);
        float x2 = motionEvent.getX();
        this.A01 = x2;
        this.A03 = x2;
        float y2 = motionEvent.getY();
        this.A02 = y2;
        this.A04 = y2;
        this.A06 = motionEvent.getPointerId(i);
        return true;
    }

    @Override // android.view.ViewGroup, android.view.ViewManager
    public void removeView(View view) {
        if (this.A0f) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    public void setAdapter(AnonymousClass01A r8) {
        ArrayList arrayList;
        AnonymousClass01A r1 = this.A0V;
        if (r1 != null) {
            synchronized (r1) {
                r1.A00 = null;
            }
            this.A0V.A0B(this);
            int i = 0;
            while (true) {
                arrayList = this.A0o;
                if (i >= arrayList.size()) {
                    break;
                }
                AnonymousClass07U r0 = (AnonymousClass07U) arrayList.get(i);
                this.A0V.A0D(this, r0.A03, r0.A02);
                i++;
            }
            this.A0V.A0A(this);
            arrayList.clear();
            int i2 = 0;
            while (i2 < getChildCount()) {
                if (!((AnonymousClass0B3) getChildAt(i2).getLayoutParams()).A04) {
                    removeViewAt(i2);
                    i2--;
                }
                i2++;
            }
            this.A0A = 0;
            scrollTo(0, 0);
        }
        AnonymousClass01A r3 = this.A0V;
        this.A0V = r8;
        this.A0E = 0;
        if (r8 != null) {
            AnonymousClass08p r02 = this.A0Y;
            if (r02 == null) {
                r02 = new AnonymousClass08p(this);
                this.A0Y = r02;
            }
            AnonymousClass01A r12 = this.A0V;
            synchronized (r12) {
                r12.A00 = r02;
            }
            this.A0j = false;
            boolean z = this.A0e;
            this.A0e = true;
            this.A0E = this.A0V.A01();
            if (this.A0M >= 0) {
                this.A0V.A09(this.A0R, this.A0Z);
                A0D(this.A0M, 0, false, true);
                this.A0M = -1;
                this.A0R = null;
                this.A0Z = null;
            } else if (!z) {
                A09(this.A0A);
            } else {
                requestLayout();
            }
        }
        List list = this.A0b;
        if (!(list == null || list.isEmpty())) {
            int size = this.A0b.size();
            for (int i3 = 0; i3 < size; i3++) {
                ((AbstractC11910h4) this.A0b.get(i3)).ALv(r3, r8, this);
            }
        }
    }

    public void setCurrentItem(int i) {
        this.A0j = false;
        A0D(i, 0, !this.A0e, false);
    }

    public void setOffscreenPageLimit(int i) {
        if (i < 1) {
            StringBuilder sb = new StringBuilder("Requested offscreen page limit ");
            sb.append(i);
            sb.append(" too small; defaulting to ");
            sb.append(1);
            Log.w("ViewPager", sb.toString());
            i = 1;
        }
        if (i != this.A0J) {
            this.A0J = i;
            A09(this.A0A);
        }
    }

    @Deprecated
    public void setOnPageChangeListener(AnonymousClass070 r1) {
        this.A0W = r1;
    }

    public void setPageMargin(int i) {
        int i2 = this.A0K;
        this.A0K = i;
        int width = getWidth();
        A0C(width, width, i, i2);
        requestLayout();
    }

    public void setPageMarginDrawable(int i) {
        setPageMarginDrawable(AnonymousClass00T.A04(getContext(), i));
    }

    public void setPageMarginDrawable(Drawable drawable) {
        boolean z;
        this.A0Q = drawable;
        if (drawable != null) {
            refreshDrawableState();
            z = false;
        } else {
            z = true;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setScrollState(int i) {
        int i2;
        if (this.A0N != i) {
            this.A0N = i;
            if (this.A0X != null) {
                boolean z = false;
                if (i != 0) {
                    z = true;
                }
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    if (z) {
                        i2 = this.A0L;
                    } else {
                        i2 = 0;
                    }
                    getChildAt(i3).setLayerType(i2, null);
                }
            }
            AnonymousClass070 r0 = this.A0W;
            if (r0 != null) {
                r0.ATO(i);
            }
            List list = this.A0c;
            if (list != null) {
                int size = list.size();
                for (int i4 = 0; i4 < size; i4++) {
                    AnonymousClass070 r02 = (AnonymousClass070) this.A0c.get(i4);
                    if (r02 != null) {
                        r02.ATO(i);
                    }
                }
            }
        }
    }

    private void setScrollingCacheEnabled(boolean z) {
        if (this.A0k != z) {
            this.A0k = z;
        }
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.A0Q;
    }
}
