package androidx.window.java.layout;

import X.AbstractC112665Eg;
import X.AbstractC113665Im;
import X.AnonymousClass024;
import X.AnonymousClass1WZ;
import X.AnonymousClass5VF;
import X.AnonymousClass5VH;
import X.AnonymousClass5WO;
import X.AnonymousClass5ZQ;
import X.C10720f5;
import X.C113625Ii;
import X.C88174Eo;
import kotlin.coroutines.jvm.internal.DebugMetadata;

@DebugMetadata(c = "androidx.window.java.layout.WindowInfoTrackerCallbackAdapter$addListener$1$1", f = "WindowInfoTrackerCallbackAdapter.kt", i = {}, l = {96}, m = "invokeSuspend", n = {}, s = {})
/* loaded from: classes.dex */
public final class WindowInfoTrackerCallbackAdapter$addListener$1$1 extends AbstractC113665Im implements AnonymousClass5ZQ {
    public final /* synthetic */ AnonymousClass024 $consumer;
    public final /* synthetic */ AnonymousClass5VH $flow;
    public int label;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WindowInfoTrackerCallbackAdapter$addListener$1$1(AnonymousClass024 r1, AnonymousClass5WO r2, AnonymousClass5VH r3) {
        super(r2);
        this.$flow = r3;
        this.$consumer = r1;
    }

    @Override // X.AbstractC112665Eg
    public final AnonymousClass5WO A04(Object obj, AnonymousClass5WO r5) {
        return new WindowInfoTrackerCallbackAdapter$addListener$1$1(this.$consumer, r5, this.$flow);
    }

    /* renamed from: A05 */
    public final Object AJ5(AnonymousClass5WO r3, AnonymousClass5VF r4) {
        return ((AbstractC112665Eg) A04(r4, r3)).A03(AnonymousClass1WZ.A00);
    }

    @Override // X.AbstractC112665Eg
    public final Object A03(Object obj) {
        Object A01 = C113625Ii.A01();
        int i = this.label;
        if (i == 0) {
            C88174Eo.A00(obj);
            AnonymousClass5VH r2 = this.$flow;
            C10720f5 r0 = new C10720f5(this.$consumer);
            this.label = 1;
            if (r2.A7Q(this, r0) == A01) {
                return A01;
            }
        } else if (i == 1) {
            C88174Eo.A00(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return AnonymousClass1WZ.A00;
    }
}
