package androidx.window.layout;

import X.AbstractC112665Eg;
import X.AbstractC113665Im;
import X.AnonymousClass0PZ;
import X.AnonymousClass1WZ;
import X.AnonymousClass4A7;
import X.AnonymousClass5L6;
import X.AnonymousClass5VI;
import X.AnonymousClass5WO;
import X.AnonymousClass5ZQ;
import X.AnonymousClass5ZY;
import X.C07540Zc;
import X.C16700pc;
import android.app.Activity;
import kotlin.coroutines.jvm.internal.DebugMetadata;

@DebugMetadata(c = "androidx.window.layout.WindowInfoTrackerImpl$windowLayoutInfo$1", f = "WindowInfoTrackerImpl.kt", i = {0, 0, 1, 1}, l = {54, 55}, m = "invokeSuspend", n = {"$this$flow", "listener", "$this$flow", "listener"}, s = {"L$0", "L$1", "L$0", "L$1"})
/* loaded from: classes.dex */
public final class WindowInfoTrackerImpl$windowLayoutInfo$1 extends AbstractC113665Im implements AnonymousClass5ZQ {
    public final /* synthetic */ Activity $activity;
    public /* synthetic */ Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public final /* synthetic */ C07540Zc this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WindowInfoTrackerImpl$windowLayoutInfo$1(Activity activity, C07540Zc r2, AnonymousClass5WO r3) {
        super(r3);
        this.this$0 = r2;
        this.$activity = activity;
    }

    @Override // X.AbstractC112665Eg
    public final AnonymousClass5WO A04(Object obj, AnonymousClass5WO r5) {
        WindowInfoTrackerImpl$windowLayoutInfo$1 windowInfoTrackerImpl$windowLayoutInfo$1 = new WindowInfoTrackerImpl$windowLayoutInfo$1(this.$activity, this.this$0, r5);
        windowInfoTrackerImpl$windowLayoutInfo$1.L$0 = obj;
        return windowInfoTrackerImpl$windowLayoutInfo$1;
    }

    /* renamed from: A05 */
    public final Object AJ5(AnonymousClass5WO r3, AnonymousClass5VI r4) {
        return ((AbstractC112665Eg) A04(r4, r3)).A03(AnonymousClass1WZ.A00);
    }

    public static final AnonymousClass5ZY A00(AnonymousClass4A7 r1) {
        return new AnonymousClass5L6(r1);
    }

    public static final void A02(AnonymousClass0PZ r0, AnonymousClass5ZY r1) {
        C16700pc.A0B(r0);
        r1.Af7(r0);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006e A[Catch: all -> 0x008f, TryCatch #0 {all -> 0x008f, blocks: (B:7:0x001a, B:12:0x0050, B:13:0x0054, B:16:0x0063, B:17:0x0066, B:19:0x006e), top: B:29:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0085  */
    @Override // X.AbstractC112665Eg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A03(java.lang.Object r10) {
        /*
            r9 = this;
            java.lang.Object r8 = X.C113625Ii.A01()
            int r0 = r9.label
            r7 = 2
            r6 = 1
            if (r0 == 0) goto L_0x0033
            if (r0 == r6) goto L_0x0026
            if (r0 != r7) goto L_0x001e
            java.lang.Object r1 = r9.L$2
            X.0Sc r1 = (X.C06080Sc) r1
            java.lang.Object r4 = r9.L$1
            X.024 r4 = (X.AnonymousClass024) r4
            java.lang.Object r5 = r9.L$0
            X.5VI r5 = (X.AnonymousClass5VI) r5
            X.C88174Eo.A00(r10)     // Catch: all -> 0x008f
            goto L_0x0054
        L_0x001e:
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        L_0x0026:
            java.lang.Object r1 = r9.L$2
            X.0Sc r1 = (X.C06080Sc) r1
            java.lang.Object r4 = r9.L$1
            X.024 r4 = (X.AnonymousClass024) r4
            java.lang.Object r5 = r9.L$0
            X.5VI r5 = (X.AnonymousClass5VI) r5
            goto L_0x0063
        L_0x0033:
            X.C88174Eo.A00(r10)
            java.lang.Object r5 = r9.L$0
            X.5VI r5 = (X.AnonymousClass5VI) r5
            X.4A7 r0 = X.AnonymousClass4A7.A01
            X.5ZY r3 = A01(r0)
            X.0Xw r4 = new X.0Xw
            r4.<init>()
            X.0Zc r0 = r9.this$0
            X.0hv r2 = r0.A00
            android.app.Activity r1 = r9.$activity
            X.0ev r0 = X.ExecutorC10620ev.A00
            r2.Aa1(r1, r4, r0)
            X.0Sc r1 = r3.AKJ()     // Catch: all -> 0x008f
        L_0x0054:
            r9.L$0 = r5     // Catch: all -> 0x008f
            r9.L$1 = r4     // Catch: all -> 0x008f
            r9.L$2 = r1     // Catch: all -> 0x008f
            r9.label = r6     // Catch: all -> 0x008f
            java.lang.Object r10 = r1.A02(r9)     // Catch: all -> 0x008f
            if (r10 != r8) goto L_0x0066
            goto L_0x0083
        L_0x0063:
            X.C88174Eo.A00(r10)     // Catch: all -> 0x008f
        L_0x0066:
            java.lang.Boolean r10 = (java.lang.Boolean) r10     // Catch: all -> 0x008f
            boolean r0 = r10.booleanValue()     // Catch: all -> 0x008f
            if (r0 == 0) goto L_0x0085
            java.lang.Object r0 = r1.A01()     // Catch: all -> 0x008f
            X.0PZ r0 = (X.AnonymousClass0PZ) r0     // Catch: all -> 0x008f
            r9.L$0 = r5     // Catch: all -> 0x008f
            r9.L$1 = r4     // Catch: all -> 0x008f
            r9.L$2 = r1     // Catch: all -> 0x008f
            r9.label = r7     // Catch: all -> 0x008f
            java.lang.Object r0 = r5.A9I(r0, r9)     // Catch: all -> 0x008f
            if (r0 != r8) goto L_0x0054
            goto L_0x0084
        L_0x0083:
            return r8
        L_0x0084:
            return r8
        L_0x0085:
            X.0Zc r0 = r9.this$0
            X.0hv r0 = r0.A00
            r0.AfD(r4)
            X.1WZ r0 = X.AnonymousClass1WZ.A00
            return r0
        L_0x008f:
            r1 = move-exception
            X.0Zc r0 = r9.this$0
            X.0hv r0 = r0.A00
            r0.AfD(r4)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.window.layout.WindowInfoTrackerImpl$windowLayoutInfo$1.A03(java.lang.Object):java.lang.Object");
    }
}
