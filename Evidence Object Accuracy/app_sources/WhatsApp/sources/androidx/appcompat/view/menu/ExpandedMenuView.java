package androidx.appcompat.view.menu;

import X.AbstractC11620ga;
import X.AbstractC11630gb;
import X.AnonymousClass07H;
import X.C013406h;
import X.C07340Xp;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/* loaded from: classes.dex */
public final class ExpandedMenuView extends ListView implements AbstractC11620ga, AbstractC11630gb, AdapterView.OnItemClickListener {
    public static final int[] A01 = {16842964, 16843049};
    public AnonymousClass07H A00;

    public ExpandedMenuView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842868);
    }

    public ExpandedMenuView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        setOnItemClickListener(this);
        C013406h A00 = C013406h.A00(context, attributeSet, A01, i, 0);
        TypedArray typedArray = A00.A02;
        if (typedArray.hasValue(0)) {
            setBackgroundDrawable(A00.A02(0));
        }
        if (typedArray.hasValue(1)) {
            setDivider(A00.A02(1));
        }
        A00.A04();
    }

    @Override // X.AbstractC11630gb
    public void AIs(AnonymousClass07H r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC11620ga
    public boolean AJA(C07340Xp r4) {
        return this.A00.A0K(r4, null, 0);
    }

    public int getWindowAnimations() {
        return 0;
    }

    @Override // android.widget.ListView, android.widget.AbsListView, android.view.ViewGroup, android.view.View, android.widget.AdapterView
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        AJA((C07340Xp) getAdapter().getItem(i));
    }
}
