package androidx.appcompat.view.menu;

import X.AbstractC11620ga;
import X.AbstractC12290hg;
import X.AbstractC12300hh;
import X.AnonymousClass07H;
import X.AnonymousClass07O;
import X.AnonymousClass0CX;
import X.AnonymousClass0KC;
import X.AnonymousClass0KD;
import X.AnonymousClass0WW;
import X.C004602b;
import X.C07340Xp;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/* loaded from: classes.dex */
public class ActionMenuItemView extends C004602b implements View.OnClickListener, AbstractC12290hg, AbstractC12300hh {
    public int A00;
    public int A01;
    public int A02;
    public Drawable A03;
    public AnonymousClass0KC A04;
    public AbstractC11620ga A05;
    public C07340Xp A06;
    public AnonymousClass0WW A07;
    public CharSequence A08;
    public boolean A09;
    public boolean A0A;

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    public ActionMenuItemView(Context context) {
        this(context, null);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Resources resources = context.getResources();
        this.A09 = A01();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass07O.A02, i, 0);
        this.A01 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        obtainStyledAttributes.recycle();
        this.A00 = (int) ((resources.getDisplayMetrics().density * 32.0f) + 0.5f);
        setOnClickListener(this);
        this.A02 = -1;
        setSaveEnabled(false);
    }

    public final void A00() {
        CharSequence charSequence;
        boolean z = true;
        boolean z2 = !TextUtils.isEmpty(this.A08);
        if (this.A03 != null && ((this.A06.A06 & 4) != 4 || (!this.A09 && !this.A0A))) {
            z = false;
        }
        boolean z3 = z2 & z;
        CharSequence charSequence2 = null;
        if (z3) {
            charSequence = this.A08;
        } else {
            charSequence = null;
        }
        setText(charSequence);
        CharSequence contentDescription = this.A06.getContentDescription();
        if (TextUtils.isEmpty(contentDescription)) {
            if (z3) {
                contentDescription = null;
            } else {
                contentDescription = this.A06.getTitle();
            }
        }
        setContentDescription(contentDescription);
        CharSequence tooltipText = this.A06.getTooltipText();
        if (TextUtils.isEmpty(tooltipText)) {
            if (!z3) {
                charSequence2 = this.A06.getTitle();
            }
            AnonymousClass0KD.A00(this, charSequence2);
            return;
        }
        AnonymousClass0KD.A00(this, tooltipText);
    }

    public final boolean A01() {
        Configuration configuration = getContext().getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        return i >= 480 || (i >= 640 && configuration.screenHeightDp >= 480) || configuration.orientation == 2;
    }

    @Override // X.AbstractC12290hg
    public void AIt(C07340Xp r3, int i) {
        this.A06 = r3;
        setIcon(r3.getIcon());
        setTitle(r3.getTitleCondensed());
        setId(r3.getItemId());
        int i2 = 8;
        if (r3.isVisible()) {
            i2 = 0;
        }
        setVisibility(i2);
        setEnabled(r3.isEnabled());
        if (r3.hasSubMenu() && this.A07 == null) {
            this.A07 = new AnonymousClass0CX(this);
        }
    }

    @Override // X.AbstractC12300hh
    public boolean ALb() {
        return !TextUtils.isEmpty(getText());
    }

    @Override // X.AbstractC12300hh
    public boolean ALc() {
        return (TextUtils.isEmpty(getText()) ^ true) && this.A06.getIcon() == null;
    }

    @Override // X.AbstractC12290hg
    public C07340Xp getItemData() {
        return this.A06;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        AbstractC11620ga r1 = this.A05;
        if (r1 != null) {
            r1.AJA(this.A06);
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.A09 = A01();
        A00();
    }

    @Override // X.C004602b, android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        boolean z = !TextUtils.isEmpty(getText());
        if (z && (i5 = this.A02) >= 0) {
            super.setPadding(i5, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int measuredWidth = getMeasuredWidth();
        if (mode == Integer.MIN_VALUE) {
            i4 = this.A01;
            i3 = Math.min(size, i4);
        } else {
            i3 = this.A01;
            i4 = i3;
        }
        if (mode != 1073741824 && i4 > 0 && measuredWidth < i3) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i2);
        }
        if (!z && this.A03 != null) {
            super.setPadding((getMeasuredWidth() - this.A03.getBounds().width()) >> 1, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(null);
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        AnonymousClass0WW r0;
        if (!this.A06.hasSubMenu() || (r0 = this.A07) == null || !r0.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public void setExpandedFormat(boolean z) {
        if (this.A0A != z) {
            this.A0A = z;
            C07340Xp r0 = this.A06;
            if (r0 != null) {
                AnonymousClass07H r1 = r0.A0E;
                r1.A0D = true;
                r1.A0E(true);
            }
        }
    }

    public void setIcon(Drawable drawable) {
        this.A03 = drawable;
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            int i = this.A00;
            if (intrinsicWidth > i) {
                intrinsicHeight = (int) (((float) intrinsicHeight) * (((float) i) / ((float) intrinsicWidth)));
                intrinsicWidth = i;
            }
            if (intrinsicHeight > i) {
                intrinsicWidth = (int) (((float) intrinsicWidth) * (((float) i) / ((float) intrinsicHeight)));
            } else {
                i = intrinsicHeight;
            }
            drawable.setBounds(0, 0, intrinsicWidth, i);
        }
        setCompoundDrawables(drawable, null, null, null);
        A00();
    }

    public void setItemInvoker(AbstractC11620ga r1) {
        this.A05 = r1;
    }

    @Override // android.widget.TextView, android.view.View
    public void setPadding(int i, int i2, int i3, int i4) {
        this.A02 = i;
        super.setPadding(i, i2, i3, i4);
    }

    public void setPopupCallback(AnonymousClass0KC r1) {
        this.A04 = r1;
    }

    public void setTitle(CharSequence charSequence) {
        this.A08 = charSequence;
        A00();
    }
}
