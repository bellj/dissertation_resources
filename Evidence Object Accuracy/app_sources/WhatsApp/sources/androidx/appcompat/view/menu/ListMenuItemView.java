package androidx.appcompat.view.menu;

import X.AbstractC12290hg;
import X.AnonymousClass07O;
import X.C013406h;
import X.C07340Xp;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class ListMenuItemView extends LinearLayout implements AbstractC12290hg, AbsListView.SelectionBoundsAdjuster {
    public int A00;
    public Context A01;
    public Drawable A02;
    public Drawable A03;
    public LayoutInflater A04;
    public CheckBox A05;
    public ImageView A06;
    public ImageView A07;
    public ImageView A08;
    public LinearLayout A09;
    public RadioButton A0A;
    public TextView A0B;
    public TextView A0C;
    public C07340Xp A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.listMenuViewStyle);
    }

    public ListMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        C013406h A00 = C013406h.A00(getContext(), attributeSet, AnonymousClass07O.A0G, i, 0);
        this.A02 = A00.A02(5);
        TypedArray typedArray = A00.A02;
        this.A00 = typedArray.getResourceId(1, -1);
        this.A0G = typedArray.getBoolean(7, false);
        this.A01 = context;
        this.A03 = A00.A02(8);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(null, new int[]{16843049}, R.attr.dropDownListViewStyle, 0);
        this.A0F = obtainStyledAttributes.hasValue(0);
        A00.A04();
        obtainStyledAttributes.recycle();
    }

    public final void A00() {
        CheckBox checkBox = (CheckBox) getInflater().inflate(R.layout.abc_list_menu_item_checkbox, (ViewGroup) this, false);
        this.A05 = checkBox;
        LinearLayout linearLayout = this.A09;
        if (linearLayout != null) {
            linearLayout.addView(checkBox, -1);
        } else {
            addView(checkBox, -1);
        }
    }

    public final void A01() {
        RadioButton radioButton = (RadioButton) getInflater().inflate(R.layout.abc_list_menu_item_radio, (ViewGroup) this, false);
        this.A0A = radioButton;
        LinearLayout linearLayout = this.A09;
        if (linearLayout != null) {
            linearLayout.addView(radioButton, -1);
        } else {
            addView(radioButton, -1);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058  */
    @Override // X.AbstractC12290hg
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AIt(X.C07340Xp r10, int r11) {
        /*
        // Method dump skipped, instructions count: 309
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.view.menu.ListMenuItemView.AIt(X.0Xp, int):void");
    }

    @Override // android.widget.AbsListView.SelectionBoundsAdjuster
    public void adjustListItemSelectionBounds(Rect rect) {
        ImageView imageView = this.A06;
        if (imageView != null && imageView.getVisibility() == 0) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.A06.getLayoutParams();
            rect.top += this.A06.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
        }
    }

    private LayoutInflater getInflater() {
        LayoutInflater layoutInflater = this.A04;
        if (layoutInflater != null) {
            return layoutInflater;
        }
        LayoutInflater from = LayoutInflater.from(getContext());
        this.A04 = from;
        return from;
    }

    @Override // X.AbstractC12290hg
    public C07340Xp getItemData() {
        return this.A0D;
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        setBackground(this.A02);
        TextView textView = (TextView) findViewById(R.id.title);
        this.A0C = textView;
        int i = this.A00;
        if (i != -1) {
            textView.setTextAppearance(this.A01, i);
        }
        this.A0B = (TextView) findViewById(R.id.shortcut);
        ImageView imageView = (ImageView) findViewById(R.id.submenuarrow);
        this.A08 = imageView;
        if (imageView != null) {
            imageView.setImageDrawable(this.A03);
        }
        this.A06 = (ImageView) findViewById(R.id.group_divider);
        this.A09 = (LinearLayout) findViewById(R.id.content);
    }

    @Override // android.widget.LinearLayout, android.view.View
    public void onMeasure(int i, int i2) {
        if (this.A07 != null && this.A0G) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.A07.getLayoutParams();
            int i3 = layoutParams.height;
            if (i3 > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = i3;
            }
        }
        super.onMeasure(i, i2);
    }

    public void setCheckable(boolean z) {
        CheckBox checkBox;
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        if (z || this.A0A != null || this.A05 != null) {
            if ((this.A0D.A02 & 4) != 0) {
                if (this.A0A == null) {
                    A01();
                }
                CompoundButton compoundButton3 = this.A0A;
                CompoundButton compoundButton4 = this.A05;
                checkBox = compoundButton4;
                compoundButton2 = compoundButton4;
                compoundButton = compoundButton3;
            } else {
                if (this.A05 == null) {
                    A00();
                }
                CompoundButton compoundButton5 = this.A05;
                checkBox = compoundButton5;
                compoundButton2 = this.A0A;
                compoundButton = compoundButton5;
            }
            if (z) {
                compoundButton.setChecked(this.A0D.isChecked());
                if (compoundButton.getVisibility() != 0) {
                    compoundButton.setVisibility(0);
                }
                if (compoundButton2 != null && compoundButton2.getVisibility() != 8) {
                    compoundButton2.setVisibility(8);
                    return;
                }
                return;
            }
            if (checkBox != null) {
                checkBox.setVisibility(8);
            }
            RadioButton radioButton = this.A0A;
            if (radioButton != null) {
                radioButton.setVisibility(8);
            }
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if ((this.A0D.A02 & 4) != 0) {
            if (this.A0A == null) {
                A01();
            }
            compoundButton = this.A0A;
        } else {
            if (this.A05 == null) {
                A00();
            }
            compoundButton = this.A05;
        }
        compoundButton.setChecked(z);
    }

    public void setForceShowIcon(boolean z) {
        this.A0E = z;
        this.A0G = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0009, code lost:
        if (r3 == false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setGroupDividerEnabled(boolean r3) {
        /*
            r2 = this;
            android.widget.ImageView r1 = r2.A06
            if (r1 == 0) goto L_0x0010
            boolean r0 = r2.A0F
            if (r0 != 0) goto L_0x000b
            r0 = 0
            if (r3 != 0) goto L_0x000d
        L_0x000b:
            r0 = 8
        L_0x000d:
            r1.setVisibility(r0)
        L_0x0010:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.view.menu.ListMenuItemView.setGroupDividerEnabled(boolean):void");
    }

    public void setIcon(Drawable drawable) {
        boolean z;
        if (this.A0D.A0E.A0H || this.A0E) {
            z = true;
        } else {
            z = false;
            if (!this.A0G) {
                return;
            }
        }
        if (this.A07 == null) {
            if (drawable != null || this.A0G) {
                ImageView imageView = (ImageView) getInflater().inflate(R.layout.abc_list_menu_item_icon, (ViewGroup) this, false);
                this.A07 = imageView;
                LinearLayout linearLayout = this.A09;
                if (linearLayout != null) {
                    linearLayout.addView(imageView, 0);
                } else {
                    addView(imageView, 0);
                }
            } else {
                return;
            }
        }
        if (drawable != null || this.A0G) {
            ImageView imageView2 = this.A07;
            if (!z) {
                drawable = null;
            }
            imageView2.setImageDrawable(drawable);
            if (this.A07.getVisibility() != 0) {
                this.A07.setVisibility(0);
                return;
            }
            return;
        }
        this.A07.setVisibility(8);
    }

    private void setSubMenuArrowVisible(boolean z) {
        ImageView imageView = this.A08;
        if (imageView != null) {
            int i = 8;
            if (z) {
                i = 0;
            }
            imageView.setVisibility(i);
        }
    }

    public void setTitle(CharSequence charSequence) {
        int i;
        TextView textView;
        TextView textView2 = this.A0C;
        if (charSequence != null) {
            textView2.setText(charSequence);
            if (this.A0C.getVisibility() != 0) {
                textView = this.A0C;
                i = 0;
            } else {
                return;
            }
        } else {
            i = 8;
            if (textView2.getVisibility() != 8) {
                textView = this.A0C;
            } else {
                return;
            }
        }
        textView.setVisibility(i);
    }
}
