package androidx.appcompat.widget;

import X.AnonymousClass028;
import X.AnonymousClass07O;
import X.AnonymousClass0Bl;
import X.C013406h;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.LinearLayout;

/* loaded from: classes.dex */
public class LinearLayoutCompat extends ViewGroup {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public Drawable A0A;
    public boolean A0B;
    public boolean A0C;
    public int[] A0D;
    public int[] A0E;

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public LinearLayoutCompat(Context context) {
        this(context, null);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0B = true;
        this.A01 = -1;
        this.A02 = 0;
        this.A06 = 8388659;
        int[] iArr = AnonymousClass07O.A0C;
        C013406h A00 = C013406h.A00(context, attributeSet, iArr, i, 0);
        TypedArray typedArray = A00.A02;
        AnonymousClass028.A0L(context, typedArray, attributeSet, this, iArr, i);
        int i2 = typedArray.getInt(1, -1);
        if (i2 >= 0) {
            setOrientation(i2);
        }
        int i3 = typedArray.getInt(0, -1);
        if (i3 >= 0) {
            setGravity(i3);
        }
        if (!typedArray.getBoolean(2, true)) {
            this.A0B = false;
        }
        this.A00 = typedArray.getFloat(4, -1.0f);
        this.A01 = typedArray.getInt(3, -1);
        this.A0C = typedArray.getBoolean(7, false);
        setDividerDrawable(A00.A02(5));
        this.A08 = typedArray.getInt(8, 0);
        this.A04 = typedArray.getDimensionPixelSize(6, 0);
        A00.A04();
    }

    /* renamed from: A00 */
    public AnonymousClass0Bl generateDefaultLayoutParams() {
        int i = this.A07;
        int i2 = -2;
        if (i != 0) {
            if (i != 1) {
                return null;
            }
            i2 = -1;
        }
        return new AnonymousClass0Bl(i2);
    }

    /* renamed from: A01 */
    public AnonymousClass0Bl generateLayoutParams(AttributeSet attributeSet) {
        return new AnonymousClass0Bl(getContext(), attributeSet);
    }

    /* renamed from: A02 */
    public AnonymousClass0Bl generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new AnonymousClass0Bl(layoutParams);
    }

    public void A03(Canvas canvas, int i) {
        this.A0A.setBounds(getPaddingLeft() + this.A04, i, (getWidth() - getPaddingRight()) - this.A04, this.A03 + i);
        this.A0A.draw(canvas);
    }

    public void A04(Canvas canvas, int i) {
        this.A0A.setBounds(i, getPaddingTop() + this.A04, this.A05 + i, (getHeight() - getPaddingBottom()) - this.A04);
        this.A0A.draw(canvas);
    }

    public boolean A05(int i) {
        int i2;
        if (i == 0) {
            i2 = this.A08 & 1;
        } else {
            int childCount = getChildCount();
            int i3 = this.A08;
            if (i == childCount) {
                i2 = i3 & 4;
            } else if ((i3 & 2) == 0) {
                return false;
            } else {
                for (int i4 = i - 1; i4 >= 0; i4--) {
                    if (getChildAt(i4).getVisibility() == 8) {
                    }
                }
                return false;
            }
        }
        return i2 != 0;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof AnonymousClass0Bl;
    }

    @Override // android.view.View
    public int getBaseline() {
        int i;
        if (this.A01 < 0) {
            return super.getBaseline();
        }
        int childCount = getChildCount();
        int i2 = this.A01;
        if (childCount > i2) {
            View childAt = getChildAt(i2);
            int baseline = childAt.getBaseline();
            if (baseline != -1) {
                int i3 = this.A02;
                if (this.A07 == 1 && (i = this.A06 & 112) != 48) {
                    if (i == 16) {
                        i3 += ((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.A09) >> 1;
                    } else if (i == 80) {
                        i3 = ((getBottom() - getTop()) - getPaddingBottom()) - this.A09;
                    }
                }
                return i3 + ((LinearLayout.LayoutParams) childAt.getLayoutParams()).topMargin + baseline;
            } else if (this.A01 == 0) {
                return -1;
            } else {
                throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
            }
        } else {
            throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
        }
    }

    public int getBaselineAlignedChildIndex() {
        return this.A01;
    }

    public Drawable getDividerDrawable() {
        return this.A0A;
    }

    public int getDividerPadding() {
        return this.A04;
    }

    public int getDividerWidth() {
        return this.A05;
    }

    public int getGravity() {
        return this.A06;
    }

    public int getOrientation() {
        return this.A07;
    }

    public int getShowDividers() {
        return this.A08;
    }

    public int getVirtualChildCount() {
        return getChildCount();
    }

    public float getWeightSum() {
        return this.A00;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int i;
        int i2;
        int i3;
        int left;
        int bottom;
        if (this.A0A == null) {
            return;
        }
        if (this.A07 == 1) {
            int childCount = getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                if (!(childAt == null || childAt.getVisibility() == 8 || !A05(i4))) {
                    A03(canvas, (childAt.getTop() - ((LinearLayout.LayoutParams) childAt.getLayoutParams()).topMargin) - this.A03);
                }
            }
            if (A05(childCount)) {
                View childAt2 = getChildAt(childCount - 1);
                if (childAt2 == null) {
                    bottom = (getHeight() - getPaddingBottom()) - this.A03;
                } else {
                    bottom = childAt2.getBottom() + ((LinearLayout.LayoutParams) childAt2.getLayoutParams()).bottomMargin;
                }
                A03(canvas, bottom);
                return;
            }
            return;
        }
        int childCount2 = getChildCount();
        boolean z = true;
        if (AnonymousClass028.A05(this) != 1) {
            z = false;
        }
        for (int i5 = 0; i5 < childCount2; i5++) {
            View childAt3 = getChildAt(i5);
            if (!(childAt3 == null || childAt3.getVisibility() == 8 || !A05(i5))) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt3.getLayoutParams();
                if (z) {
                    left = childAt3.getRight() + layoutParams.rightMargin;
                } else {
                    left = (childAt3.getLeft() - layoutParams.leftMargin) - this.A05;
                }
                A04(canvas, left);
            }
        }
        if (A05(childCount2)) {
            View childAt4 = getChildAt(childCount2 - 1);
            if (childAt4 != null) {
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) childAt4.getLayoutParams();
                if (z) {
                    i = childAt4.getLeft();
                    i2 = layoutParams2.leftMargin;
                    i3 = (i - i2) - this.A05;
                } else {
                    i3 = childAt4.getRight() + layoutParams2.rightMargin;
                }
            } else if (z) {
                i3 = getPaddingLeft();
            } else {
                i = getWidth();
                i2 = getPaddingRight();
                i3 = (i - i2) - this.A05;
            }
            A04(canvas, i3);
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName("androidx.appcompat.widget.LinearLayoutCompat");
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName("androidx.appcompat.widget.LinearLayoutCompat");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0162  */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r23, int r24, int r25, int r26, int r27) {
        /*
        // Method dump skipped, instructions count: 424
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.LinearLayoutCompat.onLayout(boolean, int, int, int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0219, code lost:
        if (r11.width == -1) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0225, code lost:
        if (r11.width != r5) goto L_0x0227;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x03f4, code lost:
        if (r3.height != -1) goto L_0x03f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x05d8, code lost:
        if (r2.height != -1) goto L_0x05da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:0x05f7, code lost:
        if (r2.height != -1) goto L_0x05f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0107, code lost:
        if (r13.width != -1) goto L_0x0109;
     */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x03cc  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x03ef  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x03fe  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x040b  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r37, int r38) {
        /*
        // Method dump skipped, instructions count: 1844
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.LinearLayoutCompat.onMeasure(int, int):void");
    }

    public void setBaselineAligned(boolean z) {
        this.A0B = z;
    }

    public void setBaselineAlignedChildIndex(int i) {
        if (i < 0 || i >= getChildCount()) {
            StringBuilder sb = new StringBuilder("base aligned child index out of range (0, ");
            sb.append(getChildCount());
            sb.append(")");
            throw new IllegalArgumentException(sb.toString());
        }
        this.A01 = i;
    }

    public void setDividerDrawable(Drawable drawable) {
        if (drawable != this.A0A) {
            this.A0A = drawable;
            boolean z = false;
            if (drawable != null) {
                this.A05 = drawable.getIntrinsicWidth();
                this.A03 = drawable.getIntrinsicHeight();
            } else {
                this.A05 = 0;
                this.A03 = 0;
                z = true;
            }
            setWillNotDraw(z);
            requestLayout();
        }
    }

    public void setDividerPadding(int i) {
        this.A04 = i;
    }

    public void setGravity(int i) {
        if (this.A06 != i) {
            if ((8388615 & i) == 0) {
                i |= 8388611;
            }
            if ((i & 112) == 0) {
                i |= 48;
            }
            this.A06 = i;
            requestLayout();
        }
    }

    public void setHorizontalGravity(int i) {
        int i2 = i & 8388615;
        int i3 = this.A06;
        if ((8388615 & i3) != i2) {
            this.A06 = i2 | (-8388616 & i3);
            requestLayout();
        }
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.A0C = z;
    }

    public void setOrientation(int i) {
        if (this.A07 != i) {
            this.A07 = i;
            requestLayout();
        }
    }

    public void setShowDividers(int i) {
        if (i != this.A08) {
            requestLayout();
        }
        this.A08 = i;
    }

    public void setVerticalGravity(int i) {
        int i2 = i & 112;
        int i3 = this.A06;
        if ((i3 & 112) != i2) {
            this.A06 = i2 | (i3 & -113);
            requestLayout();
        }
    }

    public void setWeightSum(float f) {
        this.A00 = Math.max(0.0f, f);
    }
}
