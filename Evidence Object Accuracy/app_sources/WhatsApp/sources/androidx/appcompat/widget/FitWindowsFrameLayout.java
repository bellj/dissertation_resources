package androidx.appcompat.widget;

import X.AnonymousClass08Q;
import X.AnonymousClass08T;
import X.AnonymousClass08U;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/* loaded from: classes.dex */
public class FitWindowsFrameLayout extends FrameLayout implements AnonymousClass08Q {
    public AnonymousClass08U A00;

    public FitWindowsFrameLayout(Context context) {
        super(context);
    }

    public FitWindowsFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    public boolean fitSystemWindows(Rect rect) {
        AnonymousClass08U r0 = this.A00;
        if (r0 != null) {
            rect.top = ((AnonymousClass08T) r0).A00.A0J(rect, null);
        }
        return super.fitSystemWindows(rect);
    }

    @Override // X.AnonymousClass08Q
    public void setOnFitSystemWindowsListener(AnonymousClass08U r1) {
        this.A00 = r1;
    }
}
