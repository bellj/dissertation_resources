package androidx.appcompat.widget;

import X.AnonymousClass028;
import X.AnonymousClass07H;
import X.AnonymousClass07O;
import X.AnonymousClass0CO;
import X.AnonymousClass0QQ;
import X.AnonymousClass0XQ;
import X.AnonymousClass0YA;
import X.C013406h;
import X.C05280Oy;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class ActionBarContextView extends ViewGroup {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public View A04;
    public View A05;
    public View A06;
    public LinearLayout A07;
    public TextView A08;
    public TextView A09;
    public AnonymousClass0XQ A0A;
    public ActionMenuView A0B;
    public AnonymousClass0QQ A0C;
    public CharSequence A0D;
    public CharSequence A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public final Context A0I;
    public final AnonymousClass0YA A0J;

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2;
        this.A0J = new AnonymousClass0YA(this);
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(R.attr.actionBarPopupTheme, typedValue, true) || (i2 = typedValue.resourceId) == 0) {
            this.A0I = context;
        } else {
            this.A0I = new ContextThemeWrapper(context, i2);
        }
        C013406h A00 = C013406h.A00(context, attributeSet, AnonymousClass07O.A03, i, 0);
        setBackground(A00.A02(0));
        TypedArray typedArray = A00.A02;
        this.A03 = typedArray.getResourceId(5, 0);
        this.A02 = typedArray.getResourceId(4, 0);
        this.A01 = typedArray.getLayoutDimension(3, 0);
        this.A00 = typedArray.getResourceId(2, R.layout.abc_action_mode_close_item_material);
        A00.A04();
    }

    public static int A00(View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = i2 + ((i3 - measuredHeight) >> 1);
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
            return -measuredWidth;
        }
        view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        return measuredWidth;
    }

    public void A03() {
        removeAllViews();
        this.A06 = null;
        this.A0B = null;
        this.A0A = null;
        View view = this.A05;
        if (view != null) {
            view.setOnClickListener(null);
        }
    }

    public final void A04() {
        if (this.A07 == null) {
            LayoutInflater.from(getContext()).inflate(R.layout.abc_action_bar_title_item, this);
            LinearLayout linearLayout = (LinearLayout) getChildAt(getChildCount() - 1);
            this.A07 = linearLayout;
            this.A09 = (TextView) linearLayout.findViewById(R.id.action_bar_title);
            this.A08 = (TextView) this.A07.findViewById(R.id.action_bar_subtitle);
            int i = this.A03;
            if (i != 0) {
                this.A09.setTextAppearance(getContext(), i);
            }
            int i2 = this.A02;
            if (i2 != 0) {
                this.A08.setTextAppearance(getContext(), i2);
            }
        }
        this.A09.setText(this.A0E);
        this.A08.setText(this.A0D);
        boolean z = !TextUtils.isEmpty(this.A0E);
        boolean z2 = !TextUtils.isEmpty(this.A0D);
        TextView textView = this.A08;
        int i3 = 0;
        int i4 = 8;
        if (z2) {
            i4 = 0;
        }
        textView.setVisibility(i4);
        LinearLayout linearLayout2 = this.A07;
        if (!z && !z2) {
            i3 = 8;
        }
        linearLayout2.setVisibility(i3);
        if (this.A07.getParent() == null) {
            addView(this.A07);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.AbstractC009504t r7) {
        /*
            r6 = this;
            android.view.View r0 = r6.A04
            if (r0 != 0) goto L_0x0091
            android.content.Context r0 = r6.getContext()
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r0)
            int r1 = r6.A00
            r0 = 0
            android.view.View r0 = r2.inflate(r1, r6, r0)
            r6.A04 = r0
        L_0x0015:
            r6.addView(r0)
        L_0x0018:
            android.view.View r1 = r6.A04
            r0 = 2131361913(0x7f0a0079, float:1.8343592E38)
            android.view.View r1 = r1.findViewById(r0)
            r6.A05 = r1
            X.0WK r0 = new X.0WK
            r0.<init>(r7, r6)
            r1.setOnClickListener(r0)
            android.view.Menu r2 = r7.A00()
            X.07H r2 = (X.AnonymousClass07H) r2
            X.0XQ r0 = r6.A0A
            if (r0 == 0) goto L_0x003f
            r0.A01()
            X.0CO r0 = r0.A0D
            if (r0 == 0) goto L_0x003f
            r0.A01()
        L_0x003f:
            android.content.Context r0 = r6.getContext()
            X.0XQ r1 = new X.0XQ
            r1.<init>(r0)
            r6.A0A = r1
            r0 = 1
            r1.A0K = r0
            r1.A0L = r0
            r1 = -2
            r0 = -1
            android.view.ViewGroup$LayoutParams r5 = new android.view.ViewGroup$LayoutParams
            r5.<init>(r1, r0)
            X.0XQ r1 = r6.A0A
            android.content.Context r0 = r6.A0I
            r2.A08(r0, r1)
            X.0XQ r4 = r6.A0A
            X.0gb r3 = r4.A0C
            if (r3 != 0) goto L_0x0079
            android.view.LayoutInflater r2 = r4.A09
            int r1 = r4.A03
            r0 = 0
            android.view.View r1 = r2.inflate(r1, r6, r0)
            X.0gb r1 = (X.AbstractC11630gb) r1
            r4.A0C = r1
            X.07H r0 = r4.A0A
            r1.AIs(r0)
            r0 = 1
            r4.AfQ(r0)
        L_0x0079:
            X.0gb r1 = r4.A0C
            if (r3 == r1) goto L_0x0083
            r0 = r1
            androidx.appcompat.widget.ActionMenuView r0 = (androidx.appcompat.widget.ActionMenuView) r0
            r0.setPresenter(r4)
        L_0x0083:
            androidx.appcompat.widget.ActionMenuView r1 = (androidx.appcompat.widget.ActionMenuView) r1
            r6.A0B = r1
            r0 = 0
            r1.setBackground(r0)
            androidx.appcompat.widget.ActionMenuView r0 = r6.A0B
            r6.addView(r0, r5)
            return
        L_0x0091:
            android.view.ViewParent r0 = r0.getParent()
            if (r0 != 0) goto L_0x0018
            android.view.View r0 = r6.A04
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ActionBarContextView.A05(X.04t):void");
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    public int getAnimatedVisibility() {
        if (this.A0C != null) {
            return this.A0J.A00;
        }
        return getVisibility();
    }

    public int getContentHeight() {
        return this.A01;
    }

    public CharSequence getSubtitle() {
        return this.A0D;
    }

    public CharSequence getTitle() {
        return this.A0E;
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, AnonymousClass07O.A00, R.attr.actionBarStyle, 0);
        this.A01 = obtainStyledAttributes.getLayoutDimension(13, 0);
        obtainStyledAttributes.recycle();
        AnonymousClass0XQ r2 = this.A0A;
        if (r2 != null) {
            r2.A02 = new C05280Oy(r2.A05).A00();
            AnonymousClass07H r1 = r2.A0A;
            if (r1 != null) {
                r1.A0E(true);
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AnonymousClass0XQ r0 = this.A0A;
        if (r0 != null) {
            r0.A01();
            AnonymousClass0CO r02 = this.A0A.A0D;
            if (r02 != null) {
                r02.A01();
            }
        }
    }

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.A0F = false;
        }
        if (!this.A0F) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9) {
                if (!onHoverEvent) {
                    this.A0F = true;
                }
                return true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.A0F = false;
            return true;
        }
        return true;
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(getClass().getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.A0E);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int paddingLeft;
        int paddingRight;
        int i5;
        int i6;
        boolean z2 = true;
        if (AnonymousClass028.A05(this) == 1) {
            paddingLeft = (i3 - i) - getPaddingRight();
        } else {
            z2 = false;
            paddingLeft = getPaddingLeft();
        }
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i4 - i2) - getPaddingTop()) - getPaddingBottom();
        View view = this.A04;
        if (!(view == null || view.getVisibility() == 8)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A04.getLayoutParams();
            if (z2) {
                int i7 = marginLayoutParams.rightMargin;
                i5 = marginLayoutParams.leftMargin;
                i6 = paddingLeft - i7;
            } else {
                int i8 = marginLayoutParams.leftMargin;
                i5 = marginLayoutParams.rightMargin;
                i6 = paddingLeft + i8;
            }
            int A00 = i6 + A00(this.A04, i6, paddingTop, paddingTop2, z2);
            if (z2) {
                paddingLeft = A00 - i5;
            } else {
                paddingLeft = A00 + i5;
            }
        }
        LinearLayout linearLayout = this.A07;
        if (!(linearLayout == null || this.A06 != null || linearLayout.getVisibility() == 8)) {
            paddingLeft += A00(this.A07, paddingLeft, paddingTop, paddingTop2, z2);
        }
        View view2 = this.A06;
        if (view2 != null) {
            A00(view2, paddingLeft, paddingTop, paddingTop2, z2);
        }
        if (z2) {
            paddingRight = getPaddingLeft();
        } else {
            paddingRight = (i3 - i) - getPaddingRight();
        }
        ActionMenuView actionMenuView = this.A0B;
        if (actionMenuView != null) {
            A00(actionMenuView, paddingRight, paddingTop, paddingTop2, !z2);
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int i3 = 1073741824;
        if (View.MeasureSpec.getMode(i) != 1073741824) {
            StringBuilder sb = new StringBuilder();
            sb.append(getClass().getSimpleName());
            sb.append(" can only be used with android:layout_width=\"match_parent\" (or fill_parent)");
            throw new IllegalStateException(sb.toString());
        } else if (View.MeasureSpec.getMode(i2) != 0) {
            int size = View.MeasureSpec.getSize(i);
            int i4 = this.A01;
            if (i4 <= 0) {
                i4 = View.MeasureSpec.getSize(i2);
            }
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i5 = i4 - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE);
            View view = this.A04;
            if (view != null) {
                view.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, Integer.MIN_VALUE), makeMeasureSpec);
                int max = Math.max(0, (paddingLeft - view.getMeasuredWidth()) - 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A04.getLayoutParams();
                paddingLeft = max - (marginLayoutParams.leftMargin + marginLayoutParams.rightMargin);
            }
            ActionMenuView actionMenuView = this.A0B;
            if (actionMenuView != null && actionMenuView.getParent() == this) {
                ActionMenuView actionMenuView2 = this.A0B;
                actionMenuView2.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, Integer.MIN_VALUE), makeMeasureSpec);
                paddingLeft = Math.max(0, (paddingLeft - actionMenuView2.getMeasuredWidth()) - 0);
            }
            LinearLayout linearLayout = this.A07;
            if (linearLayout != null && this.A06 == null) {
                if (this.A0H) {
                    this.A07.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.A07.getMeasuredWidth();
                    boolean z = false;
                    if (measuredWidth <= paddingLeft) {
                        z = true;
                        paddingLeft -= measuredWidth;
                    }
                    LinearLayout linearLayout2 = this.A07;
                    int i6 = 8;
                    if (z) {
                        i6 = 0;
                    }
                    linearLayout2.setVisibility(i6);
                } else {
                    linearLayout.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, Integer.MIN_VALUE), makeMeasureSpec);
                    paddingLeft = Math.max(0, (paddingLeft - linearLayout.getMeasuredWidth()) - 0);
                }
            }
            View view2 = this.A06;
            if (view2 != null) {
                ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                int i7 = layoutParams.width;
                int i8 = Integer.MIN_VALUE;
                if (i7 != -2) {
                    i8 = 1073741824;
                    if (i7 >= 0) {
                        paddingLeft = Math.min(i7, paddingLeft);
                    }
                }
                int i9 = layoutParams.height;
                if (i9 == -2) {
                    i3 = Integer.MIN_VALUE;
                } else if (i9 >= 0) {
                    i5 = Math.min(i9, i5);
                }
                this.A06.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i8), View.MeasureSpec.makeMeasureSpec(i5, i3));
            }
            if (this.A01 <= 0) {
                int childCount = getChildCount();
                int i10 = 0;
                for (int i11 = 0; i11 < childCount; i11++) {
                    int measuredHeight = getChildAt(i11).getMeasuredHeight() + paddingTop;
                    if (measuredHeight > i10) {
                        i10 = measuredHeight;
                    }
                }
                setMeasuredDimension(size, i10);
                return;
            }
            setMeasuredDimension(size, i4);
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(getClass().getSimpleName());
            sb2.append(" can only be used with android:layout_height=\"wrap_content\"");
            throw new IllegalStateException(sb2.toString());
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.A0G = false;
        }
        if (!this.A0G) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0) {
                if (!onTouchEvent) {
                    this.A0G = true;
                }
                return true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.A0G = false;
            return true;
        }
        return true;
    }

    public void setContentHeight(int i) {
        this.A01 = i;
    }

    public void setCustomView(View view) {
        View view2 = this.A06;
        if (view2 != null) {
            removeView(view2);
        }
        this.A06 = view;
        if (view != null) {
            LinearLayout linearLayout = this.A07;
            if (linearLayout != null) {
                removeView(linearLayout);
                this.A07 = null;
            }
            addView(view);
        }
        requestLayout();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.A0D = charSequence;
        A04();
    }

    public void setTitle(CharSequence charSequence) {
        this.A0E = charSequence;
        A04();
    }

    public void setTitleOptional(boolean z) {
        if (z != this.A0H) {
            requestLayout();
        }
        this.A0H = z;
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        if (i != getVisibility()) {
            AnonymousClass0QQ r0 = this.A0C;
            if (r0 != null) {
                r0.A00();
            }
            super.setVisibility(i);
        }
    }
}
