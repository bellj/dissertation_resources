package androidx.appcompat.widget;

import X.AbstractC016407t;
import X.AbstractC016507u;
import X.AbstractC017508e;
import X.AbstractC11180fs;
import X.AbstractC12280hf;
import X.AbstractC12610iC;
import X.AbstractC12820ib;
import X.AnonymousClass028;
import X.AnonymousClass07H;
import X.AnonymousClass099;
import X.AnonymousClass0B4;
import X.AnonymousClass0C4;
import X.AnonymousClass0PN;
import X.AnonymousClass0PY;
import X.AnonymousClass0RW;
import X.AnonymousClass0U7;
import X.AnonymousClass0XQ;
import X.C012005t;
import X.C017408d;
import X.C018408o;
import X.C04740Mw;
import X.RunnableC09010cD;
import X.RunnableC09020cE;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.OverScroller;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class ActionBarOverlayLayout extends ViewGroup implements AbstractC12610iC, AbstractC016507u, AbstractC016407t, AbstractC12820ib {
    public static final int[] A0V = {R.attr.actionBarSize, 16842841};
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public Drawable A04;
    public ViewPropertyAnimator A05;
    public OverScroller A06;
    public ActionBarContainer A07;
    public AbstractC11180fs A08;
    public ContentFrameLayout A09;
    public AbstractC017508e A0A;
    public C018408o A0B;
    public C018408o A0C;
    public C018408o A0D;
    public C018408o A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public final AnimatorListenerAdapter A0K;
    public final Rect A0L;
    public final Rect A0M;
    public final Rect A0N;
    public final Rect A0O;
    public final Rect A0P;
    public final Rect A0Q;
    public final Rect A0R;
    public final C04740Mw A0S;
    public final Runnable A0T;
    public final Runnable A0U;

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedPreFling(View view, float f, float f2) {
        return false;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
    }

    public void setShowingForActionMode(boolean z) {
    }

    public void setUiOptions(int i) {
    }

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ActionBarOverlayLayout(Context context) {
        this(context, null);
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A03 = 0;
        this.A0L = new Rect();
        this.A0P = new Rect();
        this.A0N = new Rect();
        this.A0M = new Rect();
        this.A0Q = new Rect();
        this.A0O = new Rect();
        this.A0R = new Rect();
        C018408o r0 = C018408o.A01;
        this.A0B = r0;
        this.A0D = r0;
        this.A0C = r0;
        this.A0E = r0;
        this.A0K = new AnonymousClass099(this);
        this.A0U = new RunnableC09010cD(this);
        this.A0T = new RunnableC09020cE(this);
        A03(context);
        this.A0S = new C04740Mw();
    }

    public static final boolean A00(View view, Rect rect, boolean z) {
        boolean z2;
        int i;
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i2 = marginLayoutParams.leftMargin;
        int i3 = rect.left;
        if (i2 != i3) {
            marginLayoutParams.leftMargin = i3;
            z2 = true;
        } else {
            z2 = false;
        }
        int i4 = marginLayoutParams.topMargin;
        int i5 = rect.top;
        if (i4 != i5) {
            marginLayoutParams.topMargin = i5;
            z2 = true;
        }
        int i6 = marginLayoutParams.rightMargin;
        int i7 = rect.right;
        if (i6 != i7) {
            marginLayoutParams.rightMargin = i7;
            z2 = true;
        }
        if (!z || marginLayoutParams.bottomMargin == (i = rect.bottom)) {
            return z2;
        }
        marginLayoutParams.bottomMargin = i;
        return true;
    }

    public void A01() {
        removeCallbacks(this.A0U);
        removeCallbacks(this.A0T);
        ViewPropertyAnimator viewPropertyAnimator = this.A05;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
        }
    }

    public void A02() {
        AbstractC017508e wrapper;
        if (this.A09 == null) {
            this.A09 = (ContentFrameLayout) findViewById(R.id.action_bar_activity_content);
            this.A07 = (ActionBarContainer) findViewById(R.id.action_bar_container);
            View findViewById = findViewById(R.id.action_bar);
            if (findViewById instanceof AbstractC017508e) {
                wrapper = (AbstractC017508e) findViewById;
            } else if (findViewById instanceof Toolbar) {
                wrapper = ((Toolbar) findViewById).getWrapper();
            } else {
                StringBuilder sb = new StringBuilder("Can't make a decor toolbar out of ");
                sb.append(findViewById.getClass().getSimpleName());
                throw new IllegalStateException(sb.toString());
            }
            this.A0A = wrapper;
        }
    }

    public final void A03(Context context) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(A0V);
        boolean z = false;
        this.A00 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        Drawable drawable = obtainStyledAttributes.getDrawable(1);
        this.A04 = drawable;
        boolean z2 = false;
        if (drawable == null) {
            z2 = true;
        }
        setWillNotDraw(z2);
        obtainStyledAttributes.recycle();
        if (context.getApplicationInfo().targetSdkVersion < 19) {
            z = true;
        }
        this.A0I = z;
        this.A06 = new OverScroller(context);
    }

    @Override // X.AbstractC12610iC
    public boolean AJp() {
        AnonymousClass0XQ r0;
        A02();
        ActionMenuView actionMenuView = ((C017408d) this.A0A).A09.A0O;
        if (actionMenuView == null || (r0 = actionMenuView.A08) == null || !r0.A02()) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC016407t
    public void ASw(View view, int[] iArr, int i, int i2, int i3) {
    }

    @Override // X.AbstractC016407t
    public void ASx(View view, int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            onNestedScroll(view, i, i2, i3, i4);
        }
    }

    @Override // X.AbstractC12820ib
    public void ASy(View view, int[] iArr, int i, int i2, int i3, int i4, int i5) {
        ASx(view, i, i2, i3, i4, i5);
    }

    @Override // X.AbstractC016407t
    public void ASz(View view, View view2, int i, int i2) {
        if (i2 == 0) {
            onNestedScrollAccepted(view, view2, i);
        }
    }

    @Override // X.AbstractC016407t
    public boolean AWL(View view, View view2, int i, int i2) {
        return i2 == 0 && onStartNestedScroll(view, view2, i);
    }

    @Override // X.AbstractC016407t
    public void AWp(View view, int i) {
        if (i == 0) {
            onStopNestedScroll(view);
        }
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof AnonymousClass0B4;
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        int i;
        super.draw(canvas);
        if (this.A04 != null && !this.A0I) {
            if (this.A07.getVisibility() == 0) {
                i = (int) (((float) this.A07.getBottom()) + this.A07.getTranslationY() + 0.5f);
            } else {
                i = 0;
            }
            this.A04.setBounds(0, i, getWidth(), this.A04.getIntrinsicHeight() + i);
            this.A04.draw(canvas);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003b, code lost:
        if (r4 != false) goto L_0x0037;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean fitSystemWindows(android.graphics.Rect r7) {
        /*
            r6 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x000b
            boolean r0 = super.fitSystemWindows(r7)
            return r0
        L_0x000b:
            r6.A02()
            androidx.appcompat.widget.ActionBarContainer r1 = r6.A07
            r5 = 1
            r0 = 0
            boolean r4 = A00(r1, r7, r0)
            android.graphics.Rect r3 = r6.A0M
            r3.set(r7)
            android.graphics.Rect r2 = r6.A0L
            X.AnonymousClass0TH.A00(r3, r2, r6)
            android.graphics.Rect r1 = r6.A0Q
            boolean r0 = r1.equals(r3)
            if (r0 != 0) goto L_0x002c
            r1.set(r3)
            r4 = 1
        L_0x002c:
            android.graphics.Rect r1 = r6.A0P
            boolean r0 = r1.equals(r2)
            if (r0 != 0) goto L_0x003b
            r1.set(r2)
        L_0x0037:
            r6.requestLayout()
        L_0x003a:
            return r5
        L_0x003b:
            if (r4 == 0) goto L_0x003a
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ActionBarOverlayLayout.fitSystemWindows(android.graphics.Rect):boolean");
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new AnonymousClass0B4();
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new AnonymousClass0B4(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new AnonymousClass0B4(layoutParams);
    }

    public int getActionBarHideOffset() {
        ActionBarContainer actionBarContainer = this.A07;
        if (actionBarContainer != null) {
            return -((int) actionBarContainer.getTranslationY());
        }
        return 0;
    }

    @Override // android.view.ViewGroup
    public int getNestedScrollAxes() {
        C04740Mw r0 = this.A0S;
        return r0.A01 | r0.A00;
    }

    public CharSequence getTitle() {
        A02();
        return ((C017408d) this.A0A).A09.A0V;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0068, code lost:
        if (r6 != false) goto L_0x0050;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.WindowInsets onApplyWindowInsets(android.view.WindowInsets r9) {
        /*
            r8 = this;
            r8.A02()
            X.08o r7 = X.C018408o.A01(r8, r9)
            int r4 = r7.A04()
            int r3 = r7.A06()
            int r1 = r7.A05()
            int r0 = r7.A03()
            android.graphics.Rect r2 = new android.graphics.Rect
            r2.<init>(r4, r3, r1, r0)
            androidx.appcompat.widget.ActionBarContainer r1 = r8.A07
            r0 = 0
            boolean r6 = A00(r1, r2, r0)
            android.graphics.Rect r5 = r8.A0L
            X.AnonymousClass028.A0O(r5, r8, r7)
            int r4 = r5.left
            int r3 = r5.top
            int r1 = r5.right
            int r0 = r5.bottom
            X.0St r2 = r7.A00
            X.08o r1 = r2.A0A(r4, r3, r1, r0)
            r8.A0B = r1
            X.08o r0 = r8.A0D
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0045
            X.08o r0 = r8.A0B
            r8.A0D = r0
            r6 = 1
        L_0x0045:
            android.graphics.Rect r1 = r8.A0P
            boolean r0 = r1.equals(r5)
            if (r0 != 0) goto L_0x0068
            r1.set(r5)
        L_0x0050:
            r8.requestLayout()
        L_0x0053:
            X.08o r0 = r2.A07()
            X.0St r0 = r0.A00
            X.08o r0 = r0.A09()
            X.0St r0 = r0.A00
            X.08o r0 = r0.A08()
            android.view.WindowInsets r0 = r0.A07()
            return r0
        L_0x0068:
            if (r6 == 0) goto L_0x0053
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ActionBarOverlayLayout.onApplyWindowInsets(android.view.WindowInsets):android.view.WindowInsets");
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A03(getContext());
        AnonymousClass028.A0R(this);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        A01();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i6 = marginLayoutParams.leftMargin + paddingLeft;
                int i7 = marginLayoutParams.topMargin + paddingTop;
                childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
            }
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        boolean z;
        int measuredHeight;
        C018408o A00;
        A02();
        measureChildWithMargins(this.A07, i, 0, i2, 0);
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A07.getLayoutParams();
        int max = Math.max(0, this.A07.getMeasuredWidth() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin);
        int max2 = Math.max(0, this.A07.getMeasuredHeight() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin);
        int combineMeasuredStates = View.combineMeasuredStates(0, this.A07.getMeasuredState());
        if ((getWindowSystemUiVisibility() & 256) != 0) {
            z = true;
            measuredHeight = this.A00;
            if (this.A0G && this.A07.A06 != null) {
                measuredHeight += measuredHeight;
            }
        } else {
            z = false;
            measuredHeight = this.A07.getVisibility() != 8 ? this.A07.getMeasuredHeight() : 0;
        }
        Rect rect = this.A0N;
        rect.set(this.A0L);
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 21) {
            this.A0C = this.A0B;
        } else {
            this.A0O.set(this.A0M);
        }
        if (!this.A0J && !z) {
            rect.top += measuredHeight;
            rect.bottom += 0;
            if (i3 >= 21) {
                A00 = this.A0C.A00.A0A(0, measuredHeight, 0, 0);
                this.A0C = A00;
            }
        } else if (i3 >= 21) {
            AnonymousClass0U7 A002 = AnonymousClass0U7.A00(this.A0C.A04(), this.A0C.A06() + measuredHeight, this.A0C.A05(), this.A0C.A03() + 0);
            AnonymousClass0PY r0 = new AnonymousClass0RW(this.A0C).A00;
            r0.A02(A002);
            A00 = r0.A00();
            this.A0C = A00;
        } else {
            Rect rect2 = this.A0O;
            rect2.top += measuredHeight;
            rect2.bottom += 0;
        }
        A00(this.A09, rect, true);
        if (i3 >= 21 && !this.A0E.equals(this.A0C)) {
            C018408o r1 = this.A0C;
            this.A0E = r1;
            AnonymousClass028.A0H(this.A09, r1);
        } else if (i3 < 21) {
            Rect rect3 = this.A0R;
            Rect rect4 = this.A0O;
            if (!rect3.equals(rect4)) {
                rect3.set(rect4);
                this.A09.A00(rect4);
            }
        }
        measureChildWithMargins(this.A09, i, 0, i2, 0);
        ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) this.A09.getLayoutParams();
        int max3 = Math.max(max, this.A09.getMeasuredWidth() + marginLayoutParams2.leftMargin + marginLayoutParams2.rightMargin);
        int max4 = Math.max(max2, this.A09.getMeasuredHeight() + marginLayoutParams2.topMargin + marginLayoutParams2.bottomMargin);
        int combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates, this.A09.getMeasuredState());
        setMeasuredDimension(View.resolveSizeAndState(Math.max(max3 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i, combineMeasuredStates2), View.resolveSizeAndState(Math.max(max4 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i2, combineMeasuredStates2 << 16));
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        Runnable runnable;
        if (!this.A0H || !z) {
            return false;
        }
        this.A06.fling(0, 0, 0, (int) f2, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        if (this.A06.getFinalY() > this.A07.getHeight()) {
            A01();
            runnable = this.A0T;
        } else {
            A01();
            runnable = this.A0U;
        }
        runnable.run();
        this.A0F = true;
        return true;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        int i5 = this.A01 + i2;
        this.A01 = i5;
        setActionBarHideOffset(i5);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScrollAccepted(View view, View view2, int i) {
        AnonymousClass0C4 r1;
        AnonymousClass0PN r0;
        this.A0S.A01 = i;
        this.A01 = getActionBarHideOffset();
        A01();
        AbstractC11180fs r12 = this.A08;
        if (r12 != null && (r0 = (r1 = (AnonymousClass0C4) r12).A08) != null) {
            r0.A00();
            r1.A08 = null;
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onStartNestedScroll(View view, View view2, int i) {
        if ((i & 2) == 0 || this.A07.getVisibility() != 0) {
            return false;
        }
        return this.A0H;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onStopNestedScroll(View view) {
        Runnable runnable;
        if (this.A0H && !this.A0F) {
            if (this.A01 <= this.A07.getHeight()) {
                A01();
                runnable = this.A0U;
            } else {
                A01();
                runnable = this.A0T;
            }
            postDelayed(runnable, 600);
        }
    }

    @Override // android.view.View
    @Deprecated
    public void onWindowSystemUiVisibilityChanged(int i) {
        boolean z;
        super.onWindowSystemUiVisibilityChanged(i);
        A02();
        int i2 = this.A02 ^ i;
        this.A02 = i;
        boolean z2 = false;
        boolean z3 = false;
        if ((i & 4) == 0) {
            z3 = true;
        }
        if ((i & 256) != 0) {
            z2 = true;
        }
        AbstractC11180fs r1 = this.A08;
        if (r1 != null) {
            AnonymousClass0C4 r12 = (AnonymousClass0C4) r1;
            r12.A0F = !z2;
            if (z3 || !z2) {
                if (r12.A0I) {
                    r12.A0I = false;
                    z = true;
                    r12.A0a(z);
                }
            } else if (!r12.A0I) {
                z = true;
                r12.A0I = true;
                r12.A0a(z);
            }
        }
        if ((i2 & 256) != 0 && this.A08 != null) {
            AnonymousClass028.A0R(this);
        }
    }

    @Override // android.view.View
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        this.A03 = i;
        AbstractC11180fs r0 = this.A08;
        if (r0 != null) {
            ((AnonymousClass0C4) r0).A00 = i;
        }
    }

    public void setActionBarHideOffset(int i) {
        A01();
        this.A07.setTranslationY((float) (-Math.max(0, Math.min(i, this.A07.getHeight()))));
    }

    public void setActionBarVisibilityCallback(AbstractC11180fs r3) {
        this.A08 = r3;
        if (getWindowToken() != null) {
            ((AnonymousClass0C4) this.A08).A00 = this.A03;
            int i = this.A02;
            if (i != 0) {
                onWindowSystemUiVisibilityChanged(i);
                AnonymousClass028.A0R(this);
            }
        }
    }

    public void setHasNonEmbeddedTabs(boolean z) {
        this.A0G = z;
    }

    public void setHideOnContentScrollEnabled(boolean z) {
        if (z != this.A0H) {
            this.A0H = z;
            if (!z) {
                A01();
                setActionBarHideOffset(0);
            }
        }
    }

    public void setIcon(int i) {
        Drawable drawable;
        A02();
        C017408d r2 = (C017408d) this.A0A;
        if (i != 0) {
            drawable = C012005t.A01().A04(r2.A09.getContext(), i);
        } else {
            drawable = null;
        }
        r2.A03 = drawable;
        r2.A00();
    }

    public void setIcon(Drawable drawable) {
        A02();
        C017408d r0 = (C017408d) this.A0A;
        r0.A03 = drawable;
        r0.A00();
    }

    public void setLogo(int i) {
        Drawable drawable;
        A02();
        C017408d r2 = (C017408d) this.A0A;
        if (i != 0) {
            drawable = C012005t.A01().A04(r2.A09.getContext(), i);
        } else {
            drawable = null;
        }
        r2.A04 = drawable;
        r2.A00();
    }

    @Override // X.AbstractC12610iC
    public void setMenu(Menu menu, AbstractC12280hf r5) {
        A02();
        C017408d r2 = (C017408d) this.A0A;
        AnonymousClass0XQ r1 = r2.A08;
        if (r1 == null) {
            r1 = new AnonymousClass0XQ(r2.A09.getContext());
            r2.A08 = r1;
        }
        r1.A0B = r5;
        r2.A09.setMenu((AnonymousClass07H) menu, r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (getContext().getApplicationInfo().targetSdkVersion >= 19) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setOverlayMode(boolean r4) {
        /*
            r3 = this;
            r3.A0J = r4
            if (r4 == 0) goto L_0x0013
            android.content.Context r0 = r3.getContext()
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()
            int r2 = r0.targetSdkVersion
            r1 = 19
            r0 = 1
            if (r2 < r1) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            r3.A0I = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ActionBarOverlayLayout.setOverlayMode(boolean):void");
    }

    @Override // X.AbstractC12610iC
    public void setWindowCallback(Window.Callback callback) {
        A02();
        ((C017408d) this.A0A).A07 = callback;
    }

    @Override // X.AbstractC12610iC
    public void setWindowTitle(CharSequence charSequence) {
        A02();
        this.A0A.setWindowTitle(charSequence);
    }
}
