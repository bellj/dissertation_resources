package androidx.appcompat.widget;

import X.AbstractC011605p;
import X.AbstractC014907a;
import X.AbstractC11620ga;
import X.AbstractC11630gb;
import X.AbstractC12280hf;
import X.AbstractC12300hh;
import X.AnonymousClass028;
import X.AnonymousClass07H;
import X.AnonymousClass0Bl;
import X.AnonymousClass0CO;
import X.AnonymousClass0CR;
import X.AnonymousClass0XF;
import X.AnonymousClass0XH;
import X.AnonymousClass0XQ;
import X.C02390Ca;
import X.C07340Xp;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;

/* loaded from: classes.dex */
public class ActionMenuView extends LinearLayoutCompat implements AbstractC11620ga, AbstractC11630gb {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public Context A04;
    public AbstractC011605p A05;
    public AnonymousClass07H A06;
    public AbstractC12280hf A07;
    public AnonymousClass0XQ A08;
    public AbstractC014907a A09;
    public boolean A0A;
    public boolean A0B;

    @Override // android.view.View
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public int getWindowAnimations() {
        return 0;
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        super.A0B = false;
        float f = context.getResources().getDisplayMetrics().density;
        this.A02 = (int) (56.0f * f);
        this.A01 = (int) (f * 4.0f);
        this.A04 = context;
        this.A03 = 0;
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat
    public /* bridge */ /* synthetic */ AnonymousClass0Bl A00() {
        C02390Ca r1 = new C02390Ca();
        ((LinearLayout.LayoutParams) r1).gravity = 16;
        return r1;
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat
    public /* bridge */ /* synthetic */ AnonymousClass0Bl A01(AttributeSet attributeSet) {
        return new C02390Ca(getContext(), attributeSet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        if (((android.widget.LinearLayout.LayoutParams) r1).gravity <= 0) goto L_0x0011;
     */
    /* renamed from: A06 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C02390Ca generateLayoutParams(android.view.ViewGroup.LayoutParams r3) {
        /*
            r2 = this;
            if (r3 == 0) goto L_0x001c
            boolean r0 = r3 instanceof X.C02390Ca
            if (r0 == 0) goto L_0x0016
            X.0Ca r3 = (X.C02390Ca) r3
            X.0Ca r1 = new X.0Ca
            r1.<init>(r3)
        L_0x000d:
            int r0 = r1.gravity
            if (r0 > 0) goto L_0x0015
        L_0x0011:
            r0 = 16
            r1.gravity = r0
        L_0x0015:
            return r1
        L_0x0016:
            X.0Ca r1 = new X.0Ca
            r1.<init>(r3)
            goto L_0x000d
        L_0x001c:
            X.0Ca r1 = new X.0Ca
            r1.<init>()
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ActionMenuView.generateLayoutParams(android.view.ViewGroup$LayoutParams):X.0Ca");
    }

    public boolean A07(int i) {
        boolean z = false;
        if (i == 0) {
            return false;
        }
        View childAt = getChildAt(i - 1);
        View childAt2 = getChildAt(i);
        if (i < getChildCount() && (childAt instanceof AbstractC12300hh)) {
            z = false | ((AbstractC12300hh) childAt).ALb();
        }
        return (i <= 0 || !(childAt2 instanceof AbstractC12300hh)) ? z : z | ((AbstractC12300hh) childAt2).ALc();
    }

    @Override // X.AbstractC11630gb
    public void AIs(AnonymousClass07H r1) {
        this.A06 = r1;
    }

    @Override // X.AbstractC11620ga
    public boolean AJA(C07340Xp r4) {
        return this.A06.A0K(r4, null, 0);
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof C02390Ca;
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateDefaultLayoutParams() {
        C02390Ca r1 = new C02390Ca();
        ((LinearLayout.LayoutParams) r1).gravity = 16;
        return r1;
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C02390Ca(getContext(), attributeSet);
    }

    public Menu getMenu() {
        AnonymousClass07H r0 = this.A06;
        if (r0 != null) {
            return r0;
        }
        Context context = getContext();
        AnonymousClass07H r1 = new AnonymousClass07H(context);
        this.A06 = r1;
        r1.A0C(new AnonymousClass0XF(this));
        AnonymousClass0XQ r2 = new AnonymousClass0XQ(context);
        this.A08 = r2;
        r2.A0K = true;
        r2.A0L = true;
        AbstractC12280hf r02 = this.A07;
        if (r02 == null) {
            r02 = new AnonymousClass0XH();
        }
        r2.A0B = r02;
        this.A06.A08(this.A04, r2);
        AnonymousClass0XQ r03 = this.A08;
        r03.A0C = this;
        AnonymousClass07H r04 = r03.A0A;
        this.A06 = r04;
        return r04;
    }

    public Drawable getOverflowIcon() {
        getMenu();
        AnonymousClass0XQ r1 = this.A08;
        AnonymousClass0CR r0 = r1.A0G;
        if (r0 != null) {
            return r0.getDrawable();
        }
        if (r1.A0J) {
            return r1.A07;
        }
        return null;
    }

    public int getPopupTheme() {
        return this.A03;
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        AnonymousClass0XQ r1 = this.A08;
        if (r1 != null) {
            r1.AfQ(false);
            if (this.A08.A02()) {
                this.A08.A01();
                this.A08.A03();
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AnonymousClass0XQ r0 = this.A08;
        if (r0 != null) {
            r0.A01();
            AnonymousClass0CO r02 = r0.A0D;
            if (r02 != null) {
                r02.A01();
            }
        }
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int width;
        int i6;
        if (!this.A0A) {
            super.onLayout(z, i, i2, i3, i4);
            return;
        }
        int childCount = getChildCount();
        int i7 = (i4 - i2) >> 1;
        int i8 = super.A05;
        int i9 = i3 - i;
        int paddingRight = (i9 - getPaddingRight()) - getPaddingLeft();
        boolean z2 = true;
        if (AnonymousClass028.A05(this) != 1) {
            z2 = false;
        }
        int i10 = 0;
        int i11 = 0;
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt = getChildAt(i12);
            if (childAt.getVisibility() != 8) {
                C02390Ca r11 = (C02390Ca) childAt.getLayoutParams();
                if (r11.A04) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (A07(i12)) {
                        measuredWidth += i8;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (z2) {
                        i6 = getPaddingLeft() + ((LinearLayout.LayoutParams) r11).leftMargin;
                        width = i6 + measuredWidth;
                    } else {
                        width = (getWidth() - getPaddingRight()) - ((LinearLayout.LayoutParams) r11).rightMargin;
                        i6 = width - measuredWidth;
                    }
                    int i13 = i7 - (measuredHeight >> 1);
                    childAt.layout(i6, i13, width, measuredHeight + i13);
                    paddingRight -= measuredWidth;
                    i10 = 1;
                } else {
                    paddingRight -= (childAt.getMeasuredWidth() + ((LinearLayout.LayoutParams) r11).leftMargin) + ((LinearLayout.LayoutParams) r11).rightMargin;
                    A07(i12);
                    i11++;
                }
            }
        }
        if (childCount == 1 && i10 == 0) {
            View childAt2 = getChildAt(0);
            int measuredWidth2 = childAt2.getMeasuredWidth();
            int measuredHeight2 = childAt2.getMeasuredHeight();
            int i14 = (i9 >> 1) - (measuredWidth2 >> 1);
            int i15 = i7 - (measuredHeight2 >> 1);
            childAt2.layout(i14, i15, measuredWidth2 + i14, measuredHeight2 + i15);
            return;
        }
        int i16 = i11 - (i10 ^ 1);
        if (i16 > 0) {
            i5 = paddingRight / i16;
        } else {
            i5 = 0;
        }
        int max = Math.max(0, i5);
        if (z2) {
            int width2 = getWidth() - getPaddingRight();
            for (int i17 = 0; i17 < childCount; i17++) {
                View childAt3 = getChildAt(i17);
                C02390Ca r4 = (C02390Ca) childAt3.getLayoutParams();
                if (childAt3.getVisibility() != 8 && !r4.A04) {
                    int i18 = width2 - ((LinearLayout.LayoutParams) r4).rightMargin;
                    int measuredWidth3 = childAt3.getMeasuredWidth();
                    int measuredHeight3 = childAt3.getMeasuredHeight();
                    int i19 = i7 - (measuredHeight3 >> 1);
                    childAt3.layout(i18 - measuredWidth3, i19, i18, measuredHeight3 + i19);
                    width2 = i18 - ((measuredWidth3 + ((LinearLayout.LayoutParams) r4).leftMargin) + max);
                }
            }
            return;
        }
        int paddingLeft = getPaddingLeft();
        for (int i20 = 0; i20 < childCount; i20++) {
            View childAt4 = getChildAt(i20);
            C02390Ca r42 = (C02390Ca) childAt4.getLayoutParams();
            if (childAt4.getVisibility() != 8 && !r42.A04) {
                int i21 = paddingLeft + ((LinearLayout.LayoutParams) r42).leftMargin;
                int measuredWidth4 = childAt4.getMeasuredWidth();
                int measuredHeight4 = childAt4.getMeasuredHeight();
                int i22 = i7 - (measuredHeight4 >> 1);
                childAt4.layout(i21, i22, i21 + measuredWidth4, measuredHeight4 + i22);
                paddingLeft = i21 + measuredWidth4 + ((LinearLayout.LayoutParams) r42).rightMargin + max;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01e8, code lost:
        if (r9 != 1) goto L_0x01ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00bf, code lost:
        if ((!android.text.TextUtils.isEmpty(((X.C004602b) r12).getText())) == false) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00fa, code lost:
        if ((!android.text.TextUtils.isEmpty(r0.getText())) == false) goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x016c, code lost:
        if (r9 != 2) goto L_0x016e;
     */
    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r32, int r33) {
        /*
        // Method dump skipped, instructions count: 725
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.ActionMenuView.onMeasure(int, int):void");
    }

    public void setExpandedActionViewsExclusive(boolean z) {
        this.A08.A0I = z;
    }

    public void setMenuCallbacks(AbstractC12280hf r1, AbstractC011605p r2) {
        this.A07 = r1;
        this.A05 = r2;
    }

    public void setOnMenuItemClickListener(AbstractC014907a r1) {
        this.A09 = r1;
    }

    public void setOverflowIcon(Drawable drawable) {
        getMenu();
        AnonymousClass0XQ r1 = this.A08;
        AnonymousClass0CR r0 = r1.A0G;
        if (r0 != null) {
            r0.setImageDrawable(drawable);
            return;
        }
        r1.A0J = true;
        r1.A07 = drawable;
    }

    public void setOverflowReserved(boolean z) {
        this.A0B = z;
    }

    public void setPopupTheme(int i) {
        Context contextThemeWrapper;
        if (this.A03 != i) {
            this.A03 = i;
            if (i == 0) {
                contextThemeWrapper = getContext();
            } else {
                contextThemeWrapper = new ContextThemeWrapper(getContext(), i);
            }
            this.A04 = contextThemeWrapper;
        }
    }

    public void setPresenter(AnonymousClass0XQ r2) {
        this.A08 = r2;
        r2.A0C = this;
        this.A06 = r2.A0A;
    }
}
