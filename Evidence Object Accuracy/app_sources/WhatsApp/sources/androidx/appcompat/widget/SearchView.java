package androidx.appcompat.widget;

import X.AbstractC015707l;
import X.AbstractC11190ft;
import X.AbstractC11200fu;
import X.AnonymousClass07D;
import X.AnonymousClass07K;
import X.AnonymousClass07L;
import X.AnonymousClass07O;
import X.AnonymousClass0Ay;
import X.AnonymousClass0BR;
import X.AnonymousClass0E1;
import X.AnonymousClass0E7;
import X.AnonymousClass0KD;
import X.AnonymousClass0Vq;
import X.AnonymousClass0WE;
import X.AnonymousClass0WN;
import X.AnonymousClass0WO;
import X.AnonymousClass0WQ;
import X.AnonymousClass0X9;
import X.C013406h;
import X.C05880Ri;
import X.C07110Ws;
import X.C07170Wy;
import X.RunnableC09090cL;
import X.RunnableC09100cM;
import X.RunnableC09110cN;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

/* loaded from: classes.dex */
public class SearchView extends LinearLayoutCompat implements AnonymousClass07D {
    public static final C05880Ri A0o = (Build.VERSION.SDK_INT < 29 ? new C05880Ri() : null);
    public int A00;
    public int A01;
    public SearchableInfo A02;
    public Rect A03;
    public Rect A04;
    public Bundle A05;
    public TextWatcher A06;
    public View.OnClickListener A07;
    public View.OnFocusChangeListener A08;
    public View.OnKeyListener A09;
    public AbstractC11190ft A0A;
    public AnonymousClass07L A0B;
    public AbstractC11200fu A0C;
    public AnonymousClass0Ay A0D;
    public AnonymousClass0BR A0E;
    public CharSequence A0F;
    public CharSequence A0G;
    public CharSequence A0H;
    public Runnable A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public int[] A0Q;
    public int[] A0R;
    public final int A0S;
    public final int A0T;
    public final Intent A0U;
    public final Intent A0V;
    public final Drawable A0W;
    public final View.OnClickListener A0X;
    public final View A0Y;
    public final View A0Z;
    public final View A0a;
    public final View A0b;
    public final AdapterView.OnItemClickListener A0c;
    public final AdapterView.OnItemSelectedListener A0d;
    public final ImageView A0e;
    public final ImageView A0f;
    public final ImageView A0g;
    public final ImageView A0h;
    public final ImageView A0i;
    public final TextView.OnEditorActionListener A0j;
    public final SearchAutoComplete A0k;
    public final CharSequence A0l;
    public final Runnable A0m;
    public final WeakHashMap A0n;

    public SearchView(Context context) {
        this(context, null);
    }

    public SearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.searchViewStyle);
    }

    public SearchView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A03 = new Rect();
        this.A04 = new Rect();
        this.A0Q = new int[2];
        this.A0R = new int[2];
        this.A0m = new RunnableC09090cL(this);
        this.A0I = new RunnableC09100cM(this);
        this.A0n = new WeakHashMap();
        AnonymousClass0WE r12 = new AnonymousClass0WE(this);
        this.A0X = r12;
        this.A09 = new AnonymousClass0WO(this);
        AnonymousClass0X9 r11 = new AnonymousClass0X9(this);
        this.A0j = r11;
        C07110Ws r10 = new C07110Ws(this);
        this.A0c = r10;
        C07170Wy r9 = new C07170Wy(this);
        this.A0d = r9;
        this.A06 = new AnonymousClass0Vq(this);
        C013406h A00 = C013406h.A00(context, attributeSet, AnonymousClass07O.A0J, i, 0);
        LayoutInflater from = LayoutInflater.from(context);
        TypedArray typedArray = A00.A02;
        from.inflate(typedArray.getResourceId(9, R.layout.abc_search_view), (ViewGroup) this, true);
        SearchAutoComplete searchAutoComplete = (SearchAutoComplete) findViewById(R.id.search_src_text);
        this.A0k = searchAutoComplete;
        searchAutoComplete.A01 = this;
        this.A0Z = findViewById(R.id.search_edit_frame);
        View findViewById = findViewById(R.id.search_plate);
        this.A0a = findViewById;
        View findViewById2 = findViewById(R.id.submit_area);
        this.A0b = findViewById2;
        ImageView imageView = (ImageView) findViewById(R.id.search_button);
        this.A0h = imageView;
        ImageView imageView2 = (ImageView) findViewById(R.id.search_go_btn);
        this.A0g = imageView2;
        ImageView imageView3 = (ImageView) findViewById(R.id.search_close_btn);
        this.A0e = imageView3;
        ImageView imageView4 = (ImageView) findViewById(R.id.search_voice_btn);
        this.A0i = imageView4;
        ImageView imageView5 = (ImageView) findViewById(R.id.search_mag_icon);
        this.A0f = imageView5;
        findViewById.setBackground(A00.A02(10));
        findViewById2.setBackground(A00.A02(14));
        imageView.setImageDrawable(A00.A02(13));
        imageView2.setImageDrawable(A00.A02(7));
        imageView3.setImageDrawable(A00.A02(4));
        imageView4.setImageDrawable(A00.A02(16));
        imageView5.setImageDrawable(A00.A02(13));
        this.A0W = A00.A02(12);
        AnonymousClass0KD.A00(imageView, getResources().getString(R.string.abc_searchview_description_search));
        this.A0T = typedArray.getResourceId(15, R.layout.abc_search_dropdown_item_icons_2line);
        this.A0S = typedArray.getResourceId(5, 0);
        imageView.setOnClickListener(r12);
        imageView3.setOnClickListener(r12);
        imageView2.setOnClickListener(r12);
        imageView4.setOnClickListener(r12);
        searchAutoComplete.setOnClickListener(r12);
        searchAutoComplete.addTextChangedListener(this.A06);
        searchAutoComplete.setOnEditorActionListener(r11);
        searchAutoComplete.setOnItemClickListener(r10);
        searchAutoComplete.setOnItemSelectedListener(r9);
        searchAutoComplete.setOnKeyListener(this.A09);
        searchAutoComplete.setOnFocusChangeListener(new AnonymousClass0WN(this));
        setIconifiedByDefault(typedArray.getBoolean(8, true));
        int dimensionPixelSize = typedArray.getDimensionPixelSize(1, -1);
        if (dimensionPixelSize != -1) {
            setMaxWidth(dimensionPixelSize);
        }
        this.A0l = typedArray.getText(6);
        this.A0G = typedArray.getText(11);
        int i2 = typedArray.getInt(3, -1);
        if (i2 != -1) {
            setImeOptions(i2);
        }
        int i3 = typedArray.getInt(2, -1);
        if (i3 != -1) {
            setInputType(i3);
        }
        setFocusable(typedArray.getBoolean(0, true));
        A00.A04();
        Intent intent = new Intent("android.speech.action.WEB_SEARCH");
        this.A0V = intent;
        intent.addFlags(268435456);
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        Intent intent2 = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.A0U = intent2;
        intent2.addFlags(268435456);
        View findViewById3 = findViewById(searchAutoComplete.getDropDownAnchor());
        this.A0Y = findViewById3;
        if (findViewById3 != null) {
            findViewById3.addOnLayoutChangeListener(new AnonymousClass0WQ(this));
        }
        A0I(this.A0M);
        A0B();
    }

    public void A06() {
        if (Build.VERSION.SDK_INT >= 29) {
            this.A0k.refreshAutoCompleteResults();
            return;
        }
        C05880Ri r3 = A0o;
        SearchAutoComplete searchAutoComplete = this.A0k;
        C05880Ri.A00();
        Method method = r3.A01;
        if (method != null) {
            try {
                method.invoke(searchAutoComplete, new Object[0]);
            } catch (Exception unused) {
            }
        }
        C05880Ri.A00();
        Method method2 = r3.A00;
        if (method2 != null) {
            try {
                method2.invoke(searchAutoComplete, new Object[0]);
            } catch (Exception unused2) {
            }
        }
    }

    public void A07() {
        SearchAutoComplete searchAutoComplete = this.A0k;
        if (!TextUtils.isEmpty(searchAutoComplete.getText())) {
            searchAutoComplete.setText("");
            searchAutoComplete.requestFocus();
            searchAutoComplete.setImeVisibility(true);
        } else if (this.A0M && this.A0A == null) {
            clearFocus();
            A0I(true);
        }
    }

    public void A08() {
        SearchAutoComplete searchAutoComplete = this.A0k;
        Editable text = searchAutoComplete.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            AnonymousClass07L r1 = this.A0B;
            if (r1 == null || !r1.AUY(text.toString())) {
                if (this.A02 != null) {
                    A0G(text.toString());
                }
                searchAutoComplete.setImeVisibility(false);
                searchAutoComplete.dismissDropDown();
            }
        }
    }

    public void A09() {
        int[] iArr;
        if (this.A0k.hasFocus()) {
            iArr = ViewGroup.FOCUSED_STATE_SET;
        } else {
            iArr = ViewGroup.EMPTY_STATE_SET;
        }
        Drawable background = this.A0a.getBackground();
        if (background != null) {
            background.setState(iArr);
        }
        Drawable background2 = this.A0b.getBackground();
        if (background2 != null) {
            background2.setState(iArr);
        }
        invalidate();
    }

    public final void A0A() {
        boolean z = true;
        boolean z2 = !TextUtils.isEmpty(this.A0k.getText());
        int i = 0;
        if (!z2 && (!this.A0M || this.A0K)) {
            z = false;
        }
        ImageView imageView = this.A0e;
        if (!z) {
            i = 8;
        }
        imageView.setVisibility(i);
        Drawable drawable = imageView.getDrawable();
        if (drawable != null) {
            drawable.setState(z2 ? ViewGroup.ENABLED_STATE_SET : ViewGroup.EMPTY_STATE_SET);
        }
    }

    public final void A0B() {
        Drawable drawable;
        SpannableStringBuilder queryHint = getQueryHint();
        SearchAutoComplete searchAutoComplete = this.A0k;
        if (queryHint == null) {
            queryHint = "";
        }
        if (this.A0M && (drawable = this.A0W) != null) {
            int textSize = (int) (((double) searchAutoComplete.getTextSize()) * 1.25d);
            drawable.setBounds(0, 0, textSize, textSize);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
            spannableStringBuilder.setSpan(new ImageSpan(drawable), 1, 2, 33);
            spannableStringBuilder.append(queryHint);
            queryHint = spannableStringBuilder;
        }
        searchAutoComplete.setHint(queryHint);
    }

    public final void A0C() {
        int i;
        if ((this.A0O || this.A0P) && !A0J() && (this.A0g.getVisibility() == 0 || this.A0i.getVisibility() == 0)) {
            i = 0;
        } else {
            i = 8;
        }
        this.A0b.setVisibility(i);
    }

    public void A0D(int i) {
        CharSequence A7k;
        Editable text = this.A0k.getText();
        Cursor cursor = this.A0E.A02;
        if (cursor == null) {
            return;
        }
        if (!cursor.moveToPosition(i) || (A7k = this.A0E.A7k(cursor)) == null) {
            setQuery(text);
        } else {
            setQuery(A7k);
        }
    }

    public void A0E(CharSequence charSequence) {
        setQuery(charSequence);
    }

    public void A0F(CharSequence charSequence) {
        SearchAutoComplete searchAutoComplete = this.A0k;
        searchAutoComplete.setText(charSequence);
        if (charSequence != null) {
            searchAutoComplete.setSelection(searchAutoComplete.length());
            this.A0H = charSequence;
        }
    }

    public void A0G(String str) {
        Intent intent = new Intent("android.intent.action.SEARCH");
        intent.addFlags(268435456);
        intent.putExtra("user_query", this.A0H);
        if (str != null) {
            intent.putExtra("query", str);
        }
        Bundle bundle = this.A05;
        if (bundle != null) {
            intent.putExtra("app_data", bundle);
        }
        intent.setComponent(this.A02.getSearchActivity());
        getContext().startActivity(intent);
    }

    public final void A0H(boolean z) {
        int i;
        if (!this.A0O || A0J() || !hasFocus() || (!z && this.A0P)) {
            i = 8;
        } else {
            i = 0;
        }
        this.A0g.setVisibility(i);
    }

    public final void A0I(boolean z) {
        this.A0L = z;
        int i = 0;
        int i2 = 8;
        if (z) {
            i2 = 0;
        }
        boolean z2 = !TextUtils.isEmpty(this.A0k.getText());
        this.A0h.setVisibility(i2);
        A0H(z2);
        View view = this.A0Z;
        int i3 = 0;
        if (z) {
            i3 = 8;
        }
        view.setVisibility(i3);
        ImageView imageView = this.A0f;
        if (imageView.getDrawable() == null || this.A0M) {
            i = 8;
        }
        imageView.setVisibility(i);
        A0A();
        boolean z3 = !z2;
        int i4 = 8;
        if (this.A0P && !A0J() && z3) {
            this.A0g.setVisibility(8);
            i4 = 0;
        }
        this.A0i.setVisibility(i4);
        A0C();
    }

    public boolean A0J() {
        return this.A0L;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005c, code lost:
        if (r3 == null) goto L_0x005e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0K(int r10) {
        /*
            r9 = this;
            r4 = 0
            X.0BR r0 = r9.A0E
            android.database.Cursor r5 = r0.A02
            if (r5 == 0) goto L_0x00ea
            boolean r0 = r5.moveToPosition(r10)
            if (r0 == 0) goto L_0x00ea
            r7 = 0
            java.lang.String r0 = "suggest_intent_action"
            int r0 = r5.getColumnIndex(r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r8 = X.AnonymousClass0E1.A00(r5, r0)     // Catch: RuntimeException -> 0x00b6
            if (r8 != 0) goto L_0x0024
            android.app.SearchableInfo r0 = r9.A02     // Catch: RuntimeException -> 0x00b6
            java.lang.String r8 = r0.getSuggestIntentAction()     // Catch: RuntimeException -> 0x00b6
            if (r8 != 0) goto L_0x0024
            java.lang.String r8 = "android.intent.action.SEARCH"
        L_0x0024:
            java.lang.String r0 = "suggest_intent_data"
            int r0 = r5.getColumnIndex(r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r3 = X.AnonymousClass0E1.A00(r5, r0)     // Catch: RuntimeException -> 0x00b6
            if (r3 != 0) goto L_0x0038
            android.app.SearchableInfo r0 = r9.A02     // Catch: RuntimeException -> 0x00b6
            java.lang.String r3 = r0.getSuggestIntentData()     // Catch: RuntimeException -> 0x00b6
            if (r3 == 0) goto L_0x005e
        L_0x0038:
            java.lang.String r0 = "suggest_intent_data_id"
            int r0 = r5.getColumnIndex(r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r2 = X.AnonymousClass0E1.A00(r5, r0)     // Catch: RuntimeException -> 0x00b6
            if (r2 == 0) goto L_0x007d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: RuntimeException -> 0x00b6
            r1.<init>()     // Catch: RuntimeException -> 0x00b6
            r1.append(r3)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r0 = "/"
            r1.append(r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r0 = android.net.Uri.encode(r2)     // Catch: RuntimeException -> 0x00b6
            r1.append(r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r3 = r1.toString()     // Catch: RuntimeException -> 0x00b6
            if (r3 != 0) goto L_0x007d
        L_0x005e:
            java.lang.String r0 = "suggest_intent_query"
            int r0 = r5.getColumnIndex(r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r6 = X.AnonymousClass0E1.A00(r5, r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r0 = "suggest_intent_extra_data"
            int r0 = r5.getColumnIndex(r0)     // Catch: RuntimeException -> 0x00b6
            java.lang.String r2 = X.AnonymousClass0E1.A00(r5, r0)     // Catch: RuntimeException -> 0x00b6
            android.content.Intent r3 = new android.content.Intent     // Catch: RuntimeException -> 0x00b6
            r3.<init>(r8)     // Catch: RuntimeException -> 0x00b6
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r3.addFlags(r0)     // Catch: RuntimeException -> 0x00b6
            goto L_0x0082
        L_0x007d:
            android.net.Uri r7 = android.net.Uri.parse(r3)     // Catch: RuntimeException -> 0x00b6
            goto L_0x005e
        L_0x0082:
            if (r7 == 0) goto L_0x0087
            r3.setData(r7)     // Catch: RuntimeException -> 0x00b6
        L_0x0087:
            java.lang.CharSequence r1 = r9.A0H     // Catch: RuntimeException -> 0x00b6
            java.lang.String r0 = "user_query"
            r3.putExtra(r0, r1)     // Catch: RuntimeException -> 0x00b6
            if (r6 == 0) goto L_0x0095
            java.lang.String r0 = "query"
            r3.putExtra(r0, r6)     // Catch: RuntimeException -> 0x00b6
        L_0x0095:
            if (r2 == 0) goto L_0x009c
            java.lang.String r0 = "intent_extra_data_key"
            r3.putExtra(r0, r2)     // Catch: RuntimeException -> 0x00b6
        L_0x009c:
            android.os.Bundle r1 = r9.A05     // Catch: RuntimeException -> 0x00b6
            if (r1 == 0) goto L_0x00a5
            java.lang.String r0 = "app_data"
            r3.putExtra(r0, r1)     // Catch: RuntimeException -> 0x00b6
        L_0x00a5:
            android.app.SearchableInfo r0 = r9.A02     // Catch: RuntimeException -> 0x00b6
            android.content.ComponentName r0 = r0.getSearchActivity()     // Catch: RuntimeException -> 0x00b6
            r3.setComponent(r0)     // Catch: RuntimeException -> 0x00b6
            android.content.Context r0 = r9.getContext()     // Catch: RuntimeException -> 0x00d6
            r0.startActivity(r3)     // Catch: RuntimeException -> 0x00d6
            goto L_0x00ea
        L_0x00b6:
            r3 = move-exception
            int r2 = r5.getPosition()     // Catch: RuntimeException -> 0x00bc
            goto L_0x00bd
        L_0x00bc:
            r2 = -1
        L_0x00bd:
            java.lang.String r0 = "Search suggestions cursor at row "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r2)
            java.lang.String r0 = " returned exception."
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "SearchView"
            android.util.Log.w(r0, r1, r3)
            goto L_0x00ea
        L_0x00d6:
            r2 = move-exception
            java.lang.String r1 = "Failed launch activity: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r3)
            java.lang.String r1 = r0.toString()
            java.lang.String r0 = "SearchView"
            android.util.Log.e(r0, r1, r2)
        L_0x00ea:
            androidx.appcompat.widget.SearchView$SearchAutoComplete r0 = r9.A0k
            r0.setImeVisibility(r4)
            r0.dismissDropDown()
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.SearchView.A0K(int):boolean");
    }

    @Override // android.view.ViewGroup, android.view.View
    public void clearFocus() {
        this.A0J = true;
        super.clearFocus();
        SearchAutoComplete searchAutoComplete = this.A0k;
        searchAutoComplete.clearFocus();
        searchAutoComplete.setImeVisibility(false);
        this.A0J = false;
    }

    public int getImeOptions() {
        return this.A0k.getImeOptions();
    }

    public int getInputType() {
        return this.A0k.getInputType();
    }

    public int getMaxWidth() {
        return this.A01;
    }

    private int getPreferredHeight() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.abc_search_view_preferred_height);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.abc_search_view_preferred_width);
    }

    public CharSequence getQuery() {
        return this.A0k.getText();
    }

    public CharSequence getQueryHint() {
        CharSequence charSequence = this.A0G;
        if (charSequence != null) {
            return charSequence;
        }
        SearchableInfo searchableInfo = this.A02;
        if (searchableInfo == null || searchableInfo.getHintId() == 0) {
            return this.A0l;
        }
        return getContext().getText(this.A02.getHintId());
    }

    public int getSuggestionCommitIconResId() {
        return this.A0S;
    }

    public int getSuggestionRowLayout() {
        return this.A0T;
    }

    public AnonymousClass0BR getSuggestionsAdapter() {
        return this.A0E;
    }

    @Override // X.AnonymousClass07D
    public void onActionViewCollapsed() {
        A0F("");
        clearFocus();
        A0I(true);
        this.A0k.setImeOptions(this.A00);
        this.A0K = false;
    }

    @Override // X.AnonymousClass07D
    public void onActionViewExpanded() {
        if (!this.A0K) {
            this.A0K = true;
            SearchAutoComplete searchAutoComplete = this.A0k;
            int imeOptions = searchAutoComplete.getImeOptions();
            this.A00 = imeOptions;
            searchAutoComplete.setImeOptions(imeOptions | 33554432);
            searchAutoComplete.setText("");
            setIconified(false);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        removeCallbacks(this.A0m);
        post(this.A0I);
        super.onDetachedFromWindow();
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            SearchAutoComplete searchAutoComplete = this.A0k;
            Rect rect = this.A03;
            int[] iArr = this.A0Q;
            searchAutoComplete.getLocationInWindow(iArr);
            int[] iArr2 = this.A0R;
            getLocationInWindow(iArr2);
            int i5 = iArr[1] - iArr2[1];
            int i6 = iArr[0] - iArr2[0];
            rect.set(i6, i5, searchAutoComplete.getWidth() + i6, searchAutoComplete.getHeight() + i5);
            Rect rect2 = this.A04;
            rect2.set(rect.left, 0, rect.right, i4 - i2);
            AnonymousClass0Ay r2 = this.A0D;
            if (r2 == null) {
                AnonymousClass0Ay r0 = new AnonymousClass0Ay(rect2, rect, searchAutoComplete);
                this.A0D = r0;
                setTouchDelegate(r0);
                return;
            }
            r2.A04.set(rect2);
            Rect rect3 = r2.A03;
            rect3.set(rect2);
            int i7 = -r2.A01;
            rect3.inset(i7, i7);
            r2.A02.set(rect);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        if (r0 > 0) goto L_0x001c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003c  */
    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r6, int r7) {
        /*
            r5 = this;
            boolean r0 = r5.A0J()
            if (r0 != 0) goto L_0x0038
            int r0 = android.view.View.MeasureSpec.getMode(r6)
            int r4 = android.view.View.MeasureSpec.getSize(r6)
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 1073741824(0x40000000, float:2.0)
            if (r0 == r3) goto L_0x004e
            if (r0 == 0) goto L_0x0045
            if (r0 != r2) goto L_0x0020
            int r0 = r5.A01
            if (r0 <= 0) goto L_0x0020
        L_0x001c:
            int r4 = java.lang.Math.min(r0, r4)
        L_0x0020:
            int r0 = android.view.View.MeasureSpec.getMode(r7)
            int r1 = android.view.View.MeasureSpec.getSize(r7)
            if (r0 == r3) goto L_0x003c
            if (r0 != 0) goto L_0x0030
            int r1 = r5.getPreferredHeight()
        L_0x0030:
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r4, r2)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r2)
        L_0x0038:
            super.onMeasure(r6, r7)
            return
        L_0x003c:
            int r0 = r5.getPreferredHeight()
            int r1 = java.lang.Math.min(r0, r1)
            goto L_0x0030
        L_0x0045:
            int r4 = r5.A01
            if (r4 > 0) goto L_0x0020
            int r4 = r5.getPreferredWidth()
            goto L_0x0020
        L_0x004e:
            int r0 = r5.A01
            if (r0 > 0) goto L_0x001c
            int r0 = r5.getPreferredWidth()
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.SearchView.onMeasure(int, int):void");
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof AnonymousClass0E7)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        AnonymousClass0E7 r2 = (AnonymousClass0E7) parcelable;
        super.onRestoreInstanceState(((AbstractC015707l) r2).A00);
        A0I(r2.A00);
        requestLayout();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        AnonymousClass0E7 r1 = new AnonymousClass0E7(super.onSaveInstanceState());
        r1.A00 = A0J();
        return r1;
    }

    @Override // android.view.View
    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        post(this.A0m);
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean requestFocus(int i, Rect rect) {
        if (this.A0J || !isFocusable()) {
            return false;
        }
        if (A0J()) {
            return super.requestFocus(i, rect);
        }
        boolean requestFocus = this.A0k.requestFocus(i, rect);
        if (requestFocus) {
            A0I(false);
        }
        return requestFocus;
    }

    public void setAppSearchData(Bundle bundle) {
        this.A05 = bundle;
    }

    public void setIconified(boolean z) {
        if (z) {
            A07();
            return;
        }
        A0I(false);
        SearchAutoComplete searchAutoComplete = this.A0k;
        searchAutoComplete.requestFocus();
        searchAutoComplete.setImeVisibility(true);
        View.OnClickListener onClickListener = this.A07;
        if (onClickListener != null) {
            onClickListener.onClick(this);
        }
    }

    public void setIconifiedByDefault(boolean z) {
        if (this.A0M != z) {
            this.A0M = z;
            A0I(z);
            A0B();
        }
    }

    public void setImeOptions(int i) {
        this.A0k.setImeOptions(i);
    }

    public void setInputType(int i) {
        this.A0k.setInputType(i);
    }

    public void setMaxWidth(int i) {
        this.A01 = i;
        requestLayout();
    }

    public void setOnCloseListener(AbstractC11190ft r1) {
        this.A0A = r1;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.A08 = onFocusChangeListener;
    }

    public void setOnQueryTextListener(AnonymousClass07L r1) {
        this.A0B = r1;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.A07 = onClickListener;
    }

    public void setOnSuggestionListener(AbstractC11200fu r1) {
        this.A0C = r1;
    }

    private void setQuery(CharSequence charSequence) {
        SearchAutoComplete searchAutoComplete = this.A0k;
        searchAutoComplete.setText(charSequence);
        searchAutoComplete.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    public void setQueryHint(CharSequence charSequence) {
        this.A0G = charSequence;
        A0B();
    }

    public void setQueryRefinementEnabled(boolean z) {
        this.A0N = z;
        AnonymousClass0BR r1 = this.A0E;
        if (r1 instanceof AnonymousClass0E1) {
            AnonymousClass0E1 r12 = (AnonymousClass0E1) r1;
            int i = 1;
            if (z) {
                i = 2;
            }
            r12.A03 = i;
        }
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        Intent intent;
        this.A02 = searchableInfo;
        if (searchableInfo != null) {
            SearchAutoComplete searchAutoComplete = this.A0k;
            searchAutoComplete.setThreshold(searchableInfo.getSuggestThreshold());
            searchAutoComplete.setImeOptions(this.A02.getImeOptions());
            int inputType = this.A02.getInputType();
            int i = 1;
            if ((inputType & 15) == 1) {
                inputType &= -65537;
                if (this.A02.getSuggestAuthority() != null) {
                    inputType = inputType | 65536 | 524288;
                }
            }
            searchAutoComplete.setInputType(inputType);
            AnonymousClass0BR r1 = this.A0E;
            if (r1 != null) {
                r1.A77(null);
            }
            if (this.A02.getSuggestAuthority() != null) {
                AnonymousClass0E1 r0 = new AnonymousClass0E1(this.A02, getContext(), this, this.A0n);
                this.A0E = r0;
                searchAutoComplete.setAdapter(r0);
                AnonymousClass0E1 r12 = (AnonymousClass0E1) this.A0E;
                if (this.A0N) {
                    i = 2;
                }
                r12.A03 = i;
            }
            A0B();
        }
        SearchableInfo searchableInfo2 = this.A02;
        boolean z = false;
        if (searchableInfo2 != null && searchableInfo2.getVoiceSearchEnabled()) {
            if (this.A02.getVoiceSearchLaunchWebSearch()) {
                intent = this.A0V;
            } else if (this.A02.getVoiceSearchLaunchRecognizer()) {
                intent = this.A0U;
            }
            if (!(intent == null || getContext().getPackageManager().resolveActivity(intent, 65536) == null)) {
                z = true;
            }
        }
        this.A0P = z;
        if (z) {
            this.A0k.setPrivateImeOptions("nm");
        }
        A0I(A0J());
    }

    public void setSubmitButtonEnabled(boolean z) {
        this.A0O = z;
        A0I(A0J());
    }

    public void setSuggestionsAdapter(AnonymousClass0BR r2) {
        this.A0E = r2;
        this.A0k.setAdapter(r2);
    }

    /* loaded from: classes.dex */
    public class SearchAutoComplete extends AnonymousClass07K {
        public int A00;
        public SearchView A01;
        public boolean A02;
        public final Runnable A03;

        @Override // android.widget.AutoCompleteTextView
        public void performCompletion() {
        }

        @Override // android.widget.AutoCompleteTextView
        public void replaceText(CharSequence charSequence) {
        }

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, R.attr.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.A03 = new RunnableC09110cN(this);
            this.A00 = getThreshold();
        }

        public void A00() {
            if (Build.VERSION.SDK_INT >= 29) {
                setInputMethodMode(1);
                if (enoughToFilter()) {
                    showDropDown();
                    return;
                }
                return;
            }
            C05880Ri r0 = SearchView.A0o;
            C05880Ri.A00();
            Method method = r0.A02;
            if (method != null) {
                try {
                    method.invoke(this, Boolean.TRUE);
                } catch (Exception unused) {
                }
            }
        }

        @Override // android.widget.AutoCompleteTextView
        public boolean enoughToFilter() {
            return this.A00 <= 0 || super.enoughToFilter();
        }

        private int getSearchViewTextMinWidthDp() {
            Configuration configuration = getResources().getConfiguration();
            int i = configuration.screenWidthDp;
            int i2 = configuration.screenHeightDp;
            if (i < 960 || i2 < 720 || configuration.orientation != 2) {
                return (i >= 600 || (i >= 640 && i2 >= 480)) ? 192 : 160;
            }
            return 256;
        }

        @Override // X.AnonymousClass07K, android.widget.TextView, android.view.View
        public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
            InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
            if (this.A02) {
                Runnable runnable = this.A03;
                removeCallbacks(runnable);
                post(runnable);
            }
            return onCreateInputConnection;
        }

        @Override // android.view.View
        public void onFinishInflate() {
            super.onFinishInflate();
            setMinWidth((int) TypedValue.applyDimension(1, (float) getSearchViewTextMinWidthDp(), getResources().getDisplayMetrics()));
        }

        @Override // android.widget.AutoCompleteTextView, android.widget.TextView, android.view.View
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            SearchView searchView = this.A01;
            searchView.A0I(searchView.A0J());
            searchView.post(searchView.A0m);
            if (searchView.A0k.hasFocus()) {
                searchView.A06();
            }
        }

        @Override // android.widget.AutoCompleteTextView, android.widget.TextView, android.view.View
        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.startTracking(keyEvent, this);
                    }
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.A01.clearFocus();
                        setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }

        @Override // android.widget.AutoCompleteTextView, android.widget.TextView, android.view.View
        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.A01.hasFocus() && getVisibility() == 0) {
                this.A02 = true;
                if (getContext().getResources().getConfiguration().orientation == 2) {
                    A00();
                }
            }
        }

        public void setImeVisibility(boolean z) {
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
            if (!z) {
                this.A02 = false;
                removeCallbacks(this.A03);
                inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            } else if (inputMethodManager.isActive(this)) {
                this.A02 = false;
                removeCallbacks(this.A03);
                inputMethodManager.showSoftInput(this, 0);
            } else {
                this.A02 = true;
            }
        }

        public void setSearchView(SearchView searchView) {
            this.A01 = searchView;
        }

        @Override // android.widget.AutoCompleteTextView
        public void setThreshold(int i) {
            super.setThreshold(i);
            this.A00 = i;
        }
    }
}
