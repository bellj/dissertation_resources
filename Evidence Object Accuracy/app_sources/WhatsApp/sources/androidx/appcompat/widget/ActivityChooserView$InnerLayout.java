package androidx.appcompat.widget;

import X.C013406h;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/* loaded from: classes.dex */
public class ActivityChooserView$InnerLayout extends LinearLayout {
    public static final int[] A00 = {16842964};

    public ActivityChooserView$InnerLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        C013406h r1 = new C013406h(context, context.obtainStyledAttributes(attributeSet, A00));
        setBackgroundDrawable(r1.A02(0));
        r1.A04();
    }
}
