package androidx.appcompat.widget;

import X.AbstractC011605p;
import X.AbstractC014907a;
import X.AbstractC015707l;
import X.AbstractC017308c;
import X.AbstractC017508e;
import X.AbstractC12280hf;
import X.AnonymousClass028;
import X.AnonymousClass03X;
import X.AnonymousClass07H;
import X.AnonymousClass07O;
import X.AnonymousClass07Z;
import X.AnonymousClass07b;
import X.AnonymousClass07c;
import X.AnonymousClass08i;
import X.AnonymousClass0Ax;
import X.AnonymousClass0E6;
import X.AnonymousClass0WF;
import X.AnonymousClass0XN;
import X.AnonymousClass0XQ;
import X.C004602b;
import X.C009604u;
import X.C012005t;
import X.C013406h;
import X.C017408d;
import X.C017908j;
import X.C05670Qm;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes.dex */
public class Toolbar extends ViewGroup {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public Context A0C;
    public ColorStateList A0D;
    public ColorStateList A0E;
    public Drawable A0F;
    public View A0G;
    public ImageButton A0H;
    public ImageButton A0I;
    public ImageView A0J;
    public TextView A0K;
    public AbstractC011605p A0L;
    public AbstractC12280hf A0M;
    public AnonymousClass0XQ A0N;
    public ActionMenuView A0O;
    public AnonymousClass07c A0P;
    public AnonymousClass0XN A0Q;
    public AbstractC017308c A0R;
    public C017408d A0S;
    public CharSequence A0T;
    public CharSequence A0U;
    public CharSequence A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public final AbstractC014907a A0Z;
    public final Runnable A0a;
    public final ArrayList A0b;
    public final ArrayList A0c;
    public final int[] A0d;
    public TextView mTitleTextView;

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.toolbarStyle);
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A03 = 8388627;
        this.A0c = new ArrayList();
        this.A0b = new ArrayList();
        this.A0d = new int[2];
        this.A0Z = new AnonymousClass07Z(this);
        this.A0a = new AnonymousClass07b(this);
        Context context2 = getContext();
        int[] iArr = AnonymousClass07O.A0N;
        C013406h A00 = C013406h.A00(context2, attributeSet, iArr, i, 0);
        TypedArray typedArray = A00.A02;
        AnonymousClass028.A0L(context, typedArray, attributeSet, this, iArr, i);
        this.A0B = typedArray.getResourceId(28, 0);
        this.A06 = typedArray.getResourceId(19, 0);
        this.A03 = typedArray.getInteger(0, this.A03);
        this.A00 = typedArray.getInteger(2, 48);
        int dimensionPixelOffset = typedArray.getDimensionPixelOffset(22, 0);
        dimensionPixelOffset = typedArray.hasValue(27) ? typedArray.getDimensionPixelOffset(27, dimensionPixelOffset) : dimensionPixelOffset;
        this.A07 = dimensionPixelOffset;
        this.A0A = dimensionPixelOffset;
        this.A08 = dimensionPixelOffset;
        this.A09 = dimensionPixelOffset;
        int dimensionPixelOffset2 = typedArray.getDimensionPixelOffset(25, -1);
        if (dimensionPixelOffset2 >= 0) {
            this.A09 = dimensionPixelOffset2;
        }
        int dimensionPixelOffset3 = typedArray.getDimensionPixelOffset(24, -1);
        if (dimensionPixelOffset3 >= 0) {
            this.A08 = dimensionPixelOffset3;
        }
        int dimensionPixelOffset4 = typedArray.getDimensionPixelOffset(26, -1);
        if (dimensionPixelOffset4 >= 0) {
            this.A0A = dimensionPixelOffset4;
        }
        int dimensionPixelOffset5 = typedArray.getDimensionPixelOffset(23, -1);
        if (dimensionPixelOffset5 >= 0) {
            this.A07 = dimensionPixelOffset5;
        }
        this.A04 = typedArray.getDimensionPixelSize(13, -1);
        int dimensionPixelOffset6 = typedArray.getDimensionPixelOffset(9, Integer.MIN_VALUE);
        int dimensionPixelOffset7 = typedArray.getDimensionPixelOffset(5, Integer.MIN_VALUE);
        int dimensionPixelSize = typedArray.getDimensionPixelSize(7, 0);
        int dimensionPixelSize2 = typedArray.getDimensionPixelSize(8, 0);
        AnonymousClass07c r0 = this.A0P;
        if (r0 == null) {
            r0 = new AnonymousClass07c();
            this.A0P = r0;
        }
        r0.A06 = false;
        if (dimensionPixelSize != Integer.MIN_VALUE) {
            r0.A01 = dimensionPixelSize;
            r0.A03 = dimensionPixelSize;
        }
        if (dimensionPixelSize2 != Integer.MIN_VALUE) {
            r0.A02 = dimensionPixelSize2;
            r0.A04 = dimensionPixelSize2;
        }
        if (!(dimensionPixelOffset6 == Integer.MIN_VALUE && dimensionPixelOffset7 == Integer.MIN_VALUE)) {
            r0.A00(dimensionPixelOffset6, dimensionPixelOffset7);
        }
        this.A02 = typedArray.getDimensionPixelOffset(10, Integer.MIN_VALUE);
        this.A01 = typedArray.getDimensionPixelOffset(6, Integer.MIN_VALUE);
        this.A0F = A00.A02(4);
        this.A0T = typedArray.getText(3);
        CharSequence text = typedArray.getText(21);
        if (!TextUtils.isEmpty(text)) {
            setTitle(text);
        }
        CharSequence text2 = typedArray.getText(18);
        if (!TextUtils.isEmpty(text2)) {
            setSubtitle(text2);
        }
        this.A0C = getContext();
        setPopupTheme(typedArray.getResourceId(17, 0));
        Drawable A02 = A00.A02(16);
        if (A02 != null) {
            setNavigationIcon(A02);
        }
        CharSequence text3 = typedArray.getText(15);
        if (!TextUtils.isEmpty(text3)) {
            setNavigationContentDescription(text3);
        }
        Drawable A022 = A00.A02(11);
        if (A022 != null) {
            setLogo(A022);
        }
        CharSequence text4 = typedArray.getText(12);
        if (!TextUtils.isEmpty(text4)) {
            setLogoDescription(text4);
        }
        if (typedArray.hasValue(29)) {
            setTitleTextColor(A00.A01(29));
        }
        if (typedArray.hasValue(20)) {
            setSubtitleTextColor(A00.A01(20));
        }
        if (typedArray.hasValue(14)) {
            getMenuInflater().inflate(typedArray.getResourceId(14, 0), getMenu());
        }
        A00.A04();
    }

    public static final int A00(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
    }

    public static C017908j A01(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof C017908j) {
            return new C017908j((C017908j) layoutParams);
        }
        if (layoutParams instanceof C009604u) {
            return new C017908j((C009604u) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new C017908j((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new C017908j(layoutParams);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r1 != 80) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A02(android.view.View r9, int r10) {
        /*
            r8 = this;
            android.view.ViewGroup$LayoutParams r5 = r9.getLayoutParams()
            X.04u r5 = (X.C009604u) r5
            int r7 = r9.getMeasuredHeight()
            r6 = 0
            r3 = 0
            if (r10 <= 0) goto L_0x0012
            int r0 = r7 - r10
            int r3 = r0 >> 1
        L_0x0012:
            int r0 = r5.A00
            r1 = r0 & 112(0x70, float:1.57E-43)
            r0 = 16
            if (r1 == r0) goto L_0x002c
            r0 = 48
            if (r1 == r0) goto L_0x0063
            r2 = 80
            if (r1 == r2) goto L_0x0054
            int r0 = r8.A03
            r1 = r0 & 112(0x70, float:1.57E-43)
            r0 = 48
            if (r1 == r0) goto L_0x0063
            if (r1 == r2) goto L_0x0054
        L_0x002c:
            int r4 = r8.getPaddingTop()
            int r3 = r8.getPaddingBottom()
            int r2 = r8.getHeight()
            int r0 = r2 - r4
            int r0 = r0 - r3
            int r0 = r0 - r7
            int r1 = r0 >> 1
            int r0 = r5.topMargin
            if (r1 >= r0) goto L_0x0045
            r1 = r0
        L_0x0043:
            int r4 = r4 + r1
            return r4
        L_0x0045:
            int r2 = r2 - r3
            int r2 = r2 - r7
            int r2 = r2 - r1
            int r2 = r2 - r4
            int r0 = r5.bottomMargin
            if (r2 >= r0) goto L_0x0043
            int r0 = r0 - r2
            int r1 = r1 - r0
            int r1 = java.lang.Math.max(r6, r1)
            goto L_0x0043
        L_0x0054:
            int r1 = r8.getHeight()
            int r0 = r8.getPaddingBottom()
            int r1 = r1 - r0
            int r1 = r1 - r7
            int r0 = r5.bottomMargin
            int r1 = r1 - r0
            int r1 = r1 - r3
            return r1
        L_0x0063:
            int r0 = r8.getPaddingTop()
            int r0 = r0 - r3
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.A02(android.view.View, int):int");
    }

    public final int A03(View view, int[] iArr, int i, int i2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i3 = marginLayoutParams.leftMargin - iArr[0];
        int max = i + Math.max(0, i3);
        iArr[0] = Math.max(0, -i3);
        int A02 = A02(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, A02, max + measuredWidth, view.getMeasuredHeight() + A02);
        return max + measuredWidth + marginLayoutParams.rightMargin;
    }

    public final int A04(View view, int[] iArr, int i, int i2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i3 = marginLayoutParams.rightMargin - iArr[1];
        int max = i - Math.max(0, i3);
        iArr[1] = Math.max(0, -i3);
        int A02 = A02(view, i2);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, A02, max, view.getMeasuredHeight() + A02);
        return max - (measuredWidth + marginLayoutParams.leftMargin);
    }

    public final int A05(View view, int[] iArr, int i, int i2, int i3, int i4) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i5 = marginLayoutParams.leftMargin - iArr[0];
        int i6 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i5) + Math.max(0, i6);
        iArr[0] = Math.max(0, -i5);
        iArr[1] = Math.max(0, -i6);
        view.measure(ViewGroup.getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + max + i2, marginLayoutParams.width), ViewGroup.getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i4, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    public void A06() {
        if (this.A0H == null) {
            AnonymousClass08i r1 = new AnonymousClass08i(getContext(), null, R.attr.toolbarNavigationButtonStyle);
            this.A0H = r1;
            r1.setImageDrawable(this.A0F);
            this.A0H.setContentDescription(this.A0T);
            C017908j r2 = new C017908j();
            ((C009604u) r2).A00 = 8388611 | (this.A00 & 112);
            r2.A00 = 2;
            this.A0H.setLayoutParams(r2);
            this.A0H.setOnClickListener(new AnonymousClass0WF(this));
        }
    }

    public void A07() {
        AnonymousClass07c r1 = this.A0P;
        if (r1 == null) {
            r1 = new AnonymousClass07c();
            this.A0P = r1;
        }
        r1.A06 = false;
        r1.A01 = 0;
        r1.A03 = 0;
        r1.A02 = 0;
        r1.A04 = 0;
    }

    public final void A08() {
        A09();
        ActionMenuView actionMenuView = this.A0O;
        if (actionMenuView.A06 == null) {
            AnonymousClass07H r3 = (AnonymousClass07H) actionMenuView.getMenu();
            AnonymousClass0XN r2 = this.A0Q;
            if (r2 == null) {
                r2 = new AnonymousClass0XN(this);
                this.A0Q = r2;
            }
            this.A0O.setExpandedActionViewsExclusive(true);
            r3.A08(this.A0C, r2);
        }
    }

    public final void A09() {
        if (this.A0O == null) {
            ActionMenuView actionMenuView = new ActionMenuView(getContext(), null);
            this.A0O = actionMenuView;
            actionMenuView.setPopupTheme(this.A05);
            ActionMenuView actionMenuView2 = this.A0O;
            actionMenuView2.A09 = this.A0Z;
            actionMenuView2.setMenuCallbacks(this.A0M, this.A0L);
            C017908j r2 = new C017908j();
            ((C009604u) r2).A00 = 8388613 | (this.A00 & 112);
            this.A0O.setLayoutParams(r2);
            A0E(this.A0O, false);
        }
    }

    public final void A0A() {
        if (this.A0I == null) {
            this.A0I = new AnonymousClass08i(getContext(), null, R.attr.toolbarNavigationButtonStyle);
            C017908j r2 = new C017908j();
            ((C009604u) r2).A00 = 8388611 | (this.A00 & 112);
            this.A0I.setLayoutParams(r2);
        }
    }

    public void A0B(int i, int i2) {
        AnonymousClass07c r0 = this.A0P;
        if (r0 == null) {
            r0 = new AnonymousClass07c();
            this.A0P = r0;
        }
        r0.A00(i, i2);
    }

    public void A0C(Context context, int i) {
        this.A0B = i;
        TextView textView = this.mTitleTextView;
        if (textView != null) {
            textView.setTextAppearance(context, i);
        }
    }

    public final void A0D(View view, int i, int i2, int i3, int i4) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width);
        int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(i3, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + 0, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i4 >= 0) {
            if (mode != 0) {
                i4 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i4);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i4, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    public final void A0E(View view, boolean z) {
        C017908j r1;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            r1 = new C017908j();
        } else if (!checkLayoutParams(layoutParams)) {
            r1 = A01(layoutParams);
        } else {
            r1 = (C017908j) layoutParams;
        }
        r1.A00 = 1;
        if (!z || this.A0G == null) {
            addView(view, r1);
            return;
        }
        view.setLayoutParams(r1);
        this.A0b.add(view);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x004e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0085 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0F(java.util.List r10, int r11) {
        /*
            r9 = this;
            int r0 = X.AnonymousClass028.A05(r9)
            r7 = 0
            r6 = 1
            r1 = 0
            if (r0 != r6) goto L_0x000a
            r1 = 1
        L_0x000a:
            int r5 = r9.getChildCount()
            int r0 = X.AnonymousClass028.A05(r9)
            int r8 = X.C05660Ql.A00(r11, r0)
            r10.clear()
            if (r1 == 0) goto L_0x0053
            int r5 = r5 - r6
        L_0x001c:
            if (r5 < 0) goto L_0x008a
            android.view.View r4 = r9.getChildAt(r5)
            android.view.ViewGroup$LayoutParams r1 = r4.getLayoutParams()
            X.08j r1 = (X.C017908j) r1
            int r0 = r1.A00
            if (r0 != 0) goto L_0x004e
            boolean r0 = r9.A0H(r4)
            if (r0 == 0) goto L_0x004e
            int r0 = r1.A00
            int r3 = X.AnonymousClass028.A05(r9)
            int r0 = X.C05660Ql.A00(r0, r3)
            r2 = r0 & 7
            if (r2 == r6) goto L_0x0051
            r1 = 3
            if (r2 == r1) goto L_0x0051
            r0 = 5
            if (r2 == r0) goto L_0x0051
            if (r3 != r6) goto L_0x0049
            r1 = 5
        L_0x0049:
            if (r1 != r8) goto L_0x004e
            r10.add(r4)
        L_0x004e:
            int r5 = r5 + -1
            goto L_0x001c
        L_0x0051:
            r1 = r2
            goto L_0x0049
        L_0x0053:
            if (r7 >= r5) goto L_0x008a
            android.view.View r4 = r9.getChildAt(r7)
            android.view.ViewGroup$LayoutParams r1 = r4.getLayoutParams()
            X.08j r1 = (X.C017908j) r1
            int r0 = r1.A00
            if (r0 != 0) goto L_0x0085
            boolean r0 = r9.A0H(r4)
            if (r0 == 0) goto L_0x0085
            int r0 = r1.A00
            int r3 = X.AnonymousClass028.A05(r9)
            int r0 = X.C05660Ql.A00(r0, r3)
            r2 = r0 & 7
            if (r2 == r6) goto L_0x0088
            r1 = 3
            if (r2 == r1) goto L_0x0088
            r0 = 5
            if (r2 == r0) goto L_0x0088
            if (r3 != r6) goto L_0x0080
            r1 = 5
        L_0x0080:
            if (r1 != r8) goto L_0x0085
            r10.add(r4)
        L_0x0085:
            int r7 = r7 + 1
            goto L_0x0053
        L_0x0088:
            r1 = r2
            goto L_0x0080
        L_0x008a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.A0F(java.util.List, int):void");
    }

    public final boolean A0G(View view) {
        return view.getParent() == this || this.A0b.contains(view);
    }

    public final boolean A0H(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof C017908j);
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new C017908j();
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new C017908j(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return A01(layoutParams);
    }

    public CharSequence getCollapseContentDescription() {
        ImageButton imageButton = this.A0H;
        if (imageButton != null) {
            return imageButton.getContentDescription();
        }
        return null;
    }

    public Drawable getCollapseIcon() {
        ImageButton imageButton = this.A0H;
        if (imageButton != null) {
            return imageButton.getDrawable();
        }
        return null;
    }

    public int getContentInsetEnd() {
        AnonymousClass07c r1 = this.A0P;
        if (r1 == null) {
            return 0;
        }
        if (r1.A07) {
            return r1.A03;
        }
        return r1.A04;
    }

    public int getContentInsetEndWithActions() {
        int i = this.A01;
        return i == Integer.MIN_VALUE ? getContentInsetEnd() : i;
    }

    public int getContentInsetLeft() {
        AnonymousClass07c r0 = this.A0P;
        if (r0 != null) {
            return r0.A03;
        }
        return 0;
    }

    public int getContentInsetRight() {
        AnonymousClass07c r0 = this.A0P;
        if (r0 != null) {
            return r0.A04;
        }
        return 0;
    }

    public int getContentInsetStart() {
        AnonymousClass07c r1 = this.A0P;
        if (r1 == null) {
            return 0;
        }
        if (r1.A07) {
            return r1.A04;
        }
        return r1.A03;
    }

    public int getContentInsetStartWithNavigation() {
        int i = this.A02;
        return i == Integer.MIN_VALUE ? getContentInsetStart() : i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000e, code lost:
        if (r1 == false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getCurrentContentInsetEnd() {
        /*
            r3 = this;
            androidx.appcompat.widget.ActionMenuView r0 = r3.A0O
            r2 = 0
            if (r0 == 0) goto L_0x0010
            X.07H r0 = r0.A06
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0.hasVisibleItems()
            r0 = 1
            if (r1 != 0) goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            int r1 = r3.getContentInsetEnd()
            if (r0 == 0) goto L_0x0021
            int r0 = r3.A01
            int r0 = java.lang.Math.max(r0, r2)
            int r1 = java.lang.Math.max(r1, r0)
        L_0x0021:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.getCurrentContentInsetEnd():int");
    }

    public int getCurrentContentInsetLeft() {
        if (AnonymousClass028.A05(this) == 1) {
            return getCurrentContentInsetEnd();
        }
        return getCurrentContentInsetStart();
    }

    public int getCurrentContentInsetRight() {
        if (AnonymousClass028.A05(this) == 1) {
            return getCurrentContentInsetStart();
        }
        return getCurrentContentInsetEnd();
    }

    public int getCurrentContentInsetStart() {
        Drawable navigationIcon = getNavigationIcon();
        int contentInsetStart = getContentInsetStart();
        if (navigationIcon != null) {
            return Math.max(contentInsetStart, Math.max(this.A02, 0));
        }
        return contentInsetStart;
    }

    public Drawable getLogo() {
        ImageView imageView = this.A0J;
        if (imageView != null) {
            return imageView.getDrawable();
        }
        return null;
    }

    public CharSequence getLogoDescription() {
        ImageView imageView = this.A0J;
        if (imageView != null) {
            return imageView.getContentDescription();
        }
        return null;
    }

    public Menu getMenu() {
        A08();
        return this.A0O.getMenu();
    }

    private MenuInflater getMenuInflater() {
        return new AnonymousClass0Ax(getContext());
    }

    public CharSequence getNavigationContentDescription() {
        ImageButton imageButton = this.A0I;
        if (imageButton != null) {
            return imageButton.getContentDescription();
        }
        return null;
    }

    public Drawable getNavigationIcon() {
        ImageButton imageButton = this.A0I;
        if (imageButton != null) {
            return imageButton.getDrawable();
        }
        return null;
    }

    public AnonymousClass0XQ getOuterActionMenuPresenter() {
        return this.A0N;
    }

    public Drawable getOverflowIcon() {
        A08();
        return this.A0O.getOverflowIcon();
    }

    public Context getPopupContext() {
        return this.A0C;
    }

    public int getPopupTheme() {
        return this.A05;
    }

    public CharSequence getSubtitle() {
        return this.A0U;
    }

    public final TextView getSubtitleTextView() {
        return this.A0K;
    }

    public CharSequence getTitle() {
        return this.A0V;
    }

    public int getTitleMarginBottom() {
        return this.A07;
    }

    public int getTitleMarginEnd() {
        return this.A08;
    }

    public int getTitleMarginStart() {
        return this.A09;
    }

    public int getTitleMarginTop() {
        return this.A0A;
    }

    public final TextView getTitleTextView() {
        return this.mTitleTextView;
    }

    public AbstractC017508e getWrapper() {
        C017408d r1 = this.A0S;
        if (r1 != null) {
            return r1;
        }
        C017408d r12 = new C017408d(this, true);
        this.A0S = r12;
        return r12;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.A0a);
    }

    @Override // android.view.View
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.A0X = false;
        }
        if (!this.A0X) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9) {
                if (!onHoverEvent) {
                    this.A0X = true;
                }
                return true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.A0X = false;
            return true;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x028c A[LOOP:1: B:104:0x028a->B:105:0x028c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02a9 A[LOOP:2: B:107:0x02a7->B:108:0x02a9, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x02e6  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x02f2 A[LOOP:3: B:115:0x02f0->B:116:0x02f2, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01a4 A[LOOP:0: B:69:0x01a2->B:70:0x01a4, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01b8  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0246  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0256  */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r22, int r23, int r24, int r25, int r26) {
        /*
        // Method dump skipped, instructions count: 771
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int[] iArr = this.A0d;
        char c = 1;
        if (AnonymousClass028.A05(this) != 1) {
            c = 0;
        }
        int i9 = 0;
        int i10 = c ^ 1;
        if (A0H(this.A0I)) {
            A0D(this.A0I, i, 0, i2, this.A04);
            int measuredWidth = this.A0I.getMeasuredWidth();
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.A0I.getLayoutParams();
            i3 = measuredWidth + C05670Qm.A01(marginLayoutParams) + C05670Qm.A00(marginLayoutParams);
            i4 = Math.max(0, this.A0I.getMeasuredHeight() + A00(this.A0I));
            i5 = View.combineMeasuredStates(0, this.A0I.getMeasuredState());
        } else {
            i3 = 0;
            i4 = 0;
            i5 = 0;
        }
        if (A0H(this.A0H)) {
            A0D(this.A0H, i, 0, i2, this.A04);
            int measuredWidth2 = this.A0H.getMeasuredWidth();
            ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) this.A0H.getLayoutParams();
            i3 = measuredWidth2 + C05670Qm.A01(marginLayoutParams2) + C05670Qm.A00(marginLayoutParams2);
            i4 = Math.max(i4, this.A0H.getMeasuredHeight() + A00(this.A0H));
            i5 = View.combineMeasuredStates(i5, this.A0H.getMeasuredState());
        }
        int currentContentInsetStart = getCurrentContentInsetStart();
        int max = 0 + Math.max(currentContentInsetStart, i3);
        iArr[c] = Math.max(0, currentContentInsetStart - i3);
        if (A0H(this.A0O)) {
            A0D(this.A0O, i, max, i2, this.A04);
            int measuredWidth3 = this.A0O.getMeasuredWidth();
            ViewGroup.MarginLayoutParams marginLayoutParams3 = (ViewGroup.MarginLayoutParams) this.A0O.getLayoutParams();
            i6 = measuredWidth3 + C05670Qm.A01(marginLayoutParams3) + C05670Qm.A00(marginLayoutParams3);
            i4 = Math.max(i4, this.A0O.getMeasuredHeight() + A00(this.A0O));
            i5 = View.combineMeasuredStates(i5, this.A0O.getMeasuredState());
        } else {
            i6 = 0;
        }
        int currentContentInsetEnd = getCurrentContentInsetEnd();
        int max2 = max + Math.max(currentContentInsetEnd, i6);
        iArr[i10] = Math.max(0, currentContentInsetEnd - i6);
        if (A0H(this.A0G)) {
            max2 += A05(this.A0G, iArr, i, max2, i2, 0);
            i4 = Math.max(i4, this.A0G.getMeasuredHeight() + A00(this.A0G));
            i5 = View.combineMeasuredStates(i5, this.A0G.getMeasuredState());
        }
        if (A0H(this.A0J)) {
            max2 += A05(this.A0J, iArr, i, max2, i2, 0);
            i4 = Math.max(i4, this.A0J.getMeasuredHeight() + A00(this.A0J));
            i5 = View.combineMeasuredStates(i5, this.A0J.getMeasuredState());
        }
        int childCount = getChildCount();
        for (int i11 = 0; i11 < childCount; i11++) {
            View childAt = getChildAt(i11);
            if (((C017908j) childAt.getLayoutParams()).A00 == 0 && A0H(childAt)) {
                max2 += A05(childAt, iArr, i, max2, i2, 0);
                i4 = Math.max(i4, childAt.getMeasuredHeight() + A00(childAt));
                i5 = View.combineMeasuredStates(i5, childAt.getMeasuredState());
            }
        }
        int i12 = this.A0A + this.A07;
        int i13 = this.A09 + this.A08;
        if (A0H(this.mTitleTextView)) {
            A05(this.mTitleTextView, iArr, i, max2 + i13, i2, i12);
            int measuredWidth4 = this.mTitleTextView.getMeasuredWidth();
            ViewGroup.MarginLayoutParams marginLayoutParams4 = (ViewGroup.MarginLayoutParams) this.mTitleTextView.getLayoutParams();
            i7 = measuredWidth4 + C05670Qm.A01(marginLayoutParams4) + C05670Qm.A00(marginLayoutParams4);
            i8 = this.mTitleTextView.getMeasuredHeight() + A00(this.mTitleTextView);
            i5 = View.combineMeasuredStates(i5, this.mTitleTextView.getMeasuredState());
        } else {
            i7 = 0;
            i8 = 0;
        }
        if (A0H(this.A0K)) {
            i7 = Math.max(i7, A05(this.A0K, iArr, i, max2 + i13, i2, i8 + i12));
            i8 += this.A0K.getMeasuredHeight() + A00(this.A0K);
            i5 = View.combineMeasuredStates(i5, this.A0K.getMeasuredState());
        }
        int max3 = Math.max(i4, i8);
        int paddingLeft = max2 + i7 + getPaddingLeft() + getPaddingRight();
        int paddingTop = max3 + getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = View.resolveSizeAndState(Math.max(paddingLeft, getSuggestedMinimumWidth()), i, -16777216 & i5);
        int resolveSizeAndState2 = View.resolveSizeAndState(Math.max(paddingTop, getSuggestedMinimumHeight()), i2, i5 << 16);
        if (this.A0W) {
            int childCount2 = getChildCount();
            for (int i14 = 0; i14 < childCount2; i14++) {
                View childAt2 = getChildAt(i14);
                if (!A0H(childAt2) || childAt2.getMeasuredWidth() <= 0 || childAt2.getMeasuredHeight() <= 0) {
                }
            }
            setMeasuredDimension(resolveSizeAndState, i9);
        }
        i9 = resolveSizeAndState2;
        setMeasuredDimension(resolveSizeAndState, i9);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        AnonymousClass07H r2;
        MenuItem findItem;
        if (!(parcelable instanceof AnonymousClass0E6)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        AnonymousClass0E6 r4 = (AnonymousClass0E6) parcelable;
        super.onRestoreInstanceState(((AbstractC015707l) r4).A00);
        ActionMenuView actionMenuView = this.A0O;
        if (actionMenuView != null) {
            r2 = actionMenuView.A06;
        } else {
            r2 = null;
        }
        int i = r4.A00;
        if (!(i == 0 || this.A0Q == null || r2 == null || (findItem = r2.findItem(i)) == null)) {
            findItem.expandActionView();
        }
        if (r4.A01) {
            Runnable runnable = this.A0a;
            removeCallbacks(runnable);
            post(runnable);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0030, code lost:
        if (r0 != Integer.MIN_VALUE) goto L_0x0032;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRtlPropertiesChanged(int r5) {
        /*
            r4 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 17
            if (r1 < r0) goto L_0x0009
            super.onRtlPropertiesChanged(r5)
        L_0x0009:
            X.07c r3 = r4.A0P
            if (r3 != 0) goto L_0x0014
            X.07c r3 = new X.07c
            r3.<init>()
            r4.A0P = r3
        L_0x0014:
            r2 = 1
            if (r5 == r2) goto L_0x0018
            r2 = 0
        L_0x0018:
            boolean r0 = r3.A07
            if (r2 == r0) goto L_0x0034
            r3.A07 = r2
            boolean r0 = r3.A06
            if (r0 == 0) goto L_0x0040
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r2 == 0) goto L_0x0035
            int r0 = r3.A00
            if (r0 != r1) goto L_0x002c
            int r0 = r3.A01
        L_0x002c:
            r3.A03 = r0
            int r0 = r3.A05
        L_0x0030:
            if (r0 == r1) goto L_0x0044
        L_0x0032:
            r3.A04 = r0
        L_0x0034:
            return
        L_0x0035:
            int r0 = r3.A05
            if (r0 != r1) goto L_0x003b
            int r0 = r3.A01
        L_0x003b:
            r3.A03 = r0
            int r0 = r3.A00
            goto L_0x0030
        L_0x0040:
            int r0 = r3.A01
            r3.A03 = r0
        L_0x0044:
            int r0 = r3.A02
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.onRtlPropertiesChanged(int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        if (r1 == false) goto L_0x0026;
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.Parcelable onSaveInstanceState() {
        /*
            r3 = this;
            android.os.Parcelable r0 = super.onSaveInstanceState()
            X.0E6 r2 = new X.0E6
            r2.<init>(r0)
            X.0XN r0 = r3.A0Q
            if (r0 == 0) goto L_0x0017
            X.0Xp r0 = r0.A01
            if (r0 == 0) goto L_0x0017
            int r0 = r0.getItemId()
            r2.A00 = r0
        L_0x0017:
            androidx.appcompat.widget.ActionMenuView r0 = r3.A0O
            if (r0 == 0) goto L_0x0026
            X.0XQ r0 = r0.A08
            if (r0 == 0) goto L_0x0026
            boolean r1 = r0.A02()
            r0 = 1
            if (r1 != 0) goto L_0x0027
        L_0x0026:
            r0 = 0
        L_0x0027:
            r2.A01 = r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.Toolbar.onSaveInstanceState():android.os.Parcelable");
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.A0Y = false;
        }
        if (!this.A0Y) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0) {
                if (!onTouchEvent) {
                    this.A0Y = true;
                }
                return true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.A0Y = false;
            return true;
        }
        return true;
    }

    public void setCollapseContentDescription(int i) {
        setCollapseContentDescription(i != 0 ? getContext().getText(i) : null);
    }

    public void setCollapseContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            A06();
        }
        ImageButton imageButton = this.A0H;
        if (imageButton != null) {
            imageButton.setContentDescription(charSequence);
        }
    }

    public void setCollapseIcon(int i) {
        setCollapseIcon(C012005t.A01().A04(getContext(), i));
    }

    public void setCollapseIcon(Drawable drawable) {
        if (drawable != null) {
            A06();
            this.A0H.setImageDrawable(drawable);
            return;
        }
        ImageButton imageButton = this.A0H;
        if (imageButton != null) {
            imageButton.setImageDrawable(this.A0F);
        }
    }

    public void setCollapsible(boolean z) {
        this.A0W = z;
        requestLayout();
    }

    public void setContentInsetEndWithActions(int i) {
        if (i < 0) {
            i = Integer.MIN_VALUE;
        }
        if (i != this.A01) {
            this.A01 = i;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setContentInsetStartWithNavigation(int i) {
        if (i < 0) {
            i = Integer.MIN_VALUE;
        }
        if (i != this.A02) {
            this.A02 = i;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public void setLogo(int i) {
        setLogo(C012005t.A01().A04(getContext(), i));
    }

    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            ImageView imageView = this.A0J;
            if (imageView == null) {
                imageView = new AnonymousClass03X(getContext(), null);
                this.A0J = imageView;
            }
            if (!A0G(imageView)) {
                A0E(this.A0J, true);
            }
        } else {
            View view = this.A0J;
            if (view != null && A0G(view)) {
                removeView(this.A0J);
                this.A0b.remove(this.A0J);
            }
        }
        ImageView imageView2 = this.A0J;
        if (imageView2 != null) {
            imageView2.setImageDrawable(drawable);
        }
    }

    public void setLogoDescription(int i) {
        setLogoDescription(getContext().getText(i));
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence) && this.A0J == null) {
            this.A0J = new AnonymousClass03X(getContext(), null);
        }
        ImageView imageView = this.A0J;
        if (imageView != null) {
            imageView.setContentDescription(charSequence);
        }
    }

    public void setMenu(AnonymousClass07H r5, AnonymousClass0XQ r6) {
        if (r5 != null || this.A0O != null) {
            A09();
            AnonymousClass07H r1 = this.A0O.A06;
            if (r1 != r5) {
                if (r1 != null) {
                    r1.A0D(this.A0N);
                    r1.A0D(this.A0Q);
                }
                if (this.A0Q == null) {
                    this.A0Q = new AnonymousClass0XN(this);
                }
                r6.A0I = true;
                Context context = this.A0C;
                if (r5 != null) {
                    r5.A08(context, r6);
                    r5.A08(this.A0C, this.A0Q);
                } else {
                    r6.AIn(context, null);
                    this.A0Q.AIn(this.A0C, null);
                    r6.AfQ(true);
                    this.A0Q.AfQ(true);
                }
                this.A0O.setPopupTheme(this.A05);
                this.A0O.setPresenter(r6);
                this.A0N = r6;
            }
        }
    }

    public void setMenuCallbacks(AbstractC12280hf r2, AbstractC011605p r3) {
        this.A0M = r2;
        this.A0L = r3;
        ActionMenuView actionMenuView = this.A0O;
        if (actionMenuView != null) {
            actionMenuView.setMenuCallbacks(r2, r3);
        }
    }

    public void setNavigationContentDescription(int i) {
        setNavigationContentDescription(i != 0 ? getContext().getText(i) : null);
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            A0A();
        }
        ImageButton imageButton = this.A0I;
        if (imageButton != null) {
            imageButton.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(int i) {
        setNavigationIcon(C012005t.A01().A04(getContext(), i));
    }

    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            A0A();
            if (!A0G(this.A0I)) {
                A0E(this.A0I, true);
            }
        } else {
            ImageButton imageButton = this.A0I;
            if (imageButton != null && A0G(imageButton)) {
                removeView(this.A0I);
                this.A0b.remove(this.A0I);
            }
        }
        ImageButton imageButton2 = this.A0I;
        if (imageButton2 != null) {
            imageButton2.setImageDrawable(drawable);
        }
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        A0A();
        this.A0I.setOnClickListener(onClickListener);
    }

    public void setOnMenuItemClickListener(AbstractC017308c r1) {
        this.A0R = r1;
    }

    public void setOverflowIcon(Drawable drawable) {
        A08();
        this.A0O.setOverflowIcon(drawable);
    }

    public void setPopupTheme(int i) {
        Context contextThemeWrapper;
        if (this.A05 != i) {
            this.A05 = i;
            if (i == 0) {
                contextThemeWrapper = getContext();
            } else {
                contextThemeWrapper = new ContextThemeWrapper(getContext(), i);
            }
            this.A0C = contextThemeWrapper;
        }
    }

    public void setSubtitle(int i) {
        setSubtitle(getContext().getText(i));
    }

    public void setSubtitle(CharSequence charSequence) {
        boolean isEmpty = TextUtils.isEmpty(charSequence);
        TextView textView = this.A0K;
        if (!isEmpty) {
            if (textView == null) {
                Context context = getContext();
                C004602b r0 = new C004602b(context, null);
                this.A0K = r0;
                r0.setSingleLine();
                this.A0K.setEllipsize(TextUtils.TruncateAt.END);
                int i = this.A06;
                if (i != 0) {
                    this.A0K.setTextAppearance(context, i);
                }
                ColorStateList colorStateList = this.A0D;
                if (colorStateList != null) {
                    this.A0K.setTextColor(colorStateList);
                }
            }
            if (!A0G(this.A0K)) {
                A0E(this.A0K, true);
            }
        } else if (textView != null && A0G(textView)) {
            removeView(this.A0K);
            this.A0b.remove(this.A0K);
        }
        TextView textView2 = this.A0K;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.A0U = charSequence;
    }

    public void setSubtitleTextColor(int i) {
        setSubtitleTextColor(ColorStateList.valueOf(i));
    }

    public void setSubtitleTextColor(ColorStateList colorStateList) {
        this.A0D = colorStateList;
        TextView textView = this.A0K;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }

    public void setTitle(int i) {
        setTitle(getContext().getText(i));
    }

    public void setTitle(CharSequence charSequence) {
        boolean isEmpty = TextUtils.isEmpty(charSequence);
        TextView textView = this.mTitleTextView;
        if (!isEmpty) {
            if (textView == null) {
                Context context = getContext();
                C004602b r0 = new C004602b(context, null);
                this.mTitleTextView = r0;
                r0.setSingleLine();
                this.mTitleTextView.setEllipsize(TextUtils.TruncateAt.END);
                int i = this.A0B;
                if (i != 0) {
                    this.mTitleTextView.setTextAppearance(context, i);
                }
                ColorStateList colorStateList = this.A0E;
                if (colorStateList != null) {
                    this.mTitleTextView.setTextColor(colorStateList);
                }
            }
            if (!A0G(this.mTitleTextView)) {
                A0E(this.mTitleTextView, true);
            }
        } else if (textView != null && A0G(textView)) {
            removeView(this.mTitleTextView);
            this.A0b.remove(this.mTitleTextView);
        }
        TextView textView2 = this.mTitleTextView;
        if (textView2 != null) {
            textView2.setText(charSequence);
        }
        this.A0V = charSequence;
    }

    public void setTitleMarginBottom(int i) {
        this.A07 = i;
        requestLayout();
    }

    public void setTitleMarginEnd(int i) {
        this.A08 = i;
        requestLayout();
    }

    public void setTitleMarginStart(int i) {
        this.A09 = i;
        requestLayout();
    }

    public void setTitleMarginTop(int i) {
        this.A0A = i;
        requestLayout();
    }

    public void setTitleTextColor(int i) {
        setTitleTextColor(ColorStateList.valueOf(i));
    }

    public void setTitleTextColor(ColorStateList colorStateList) {
        this.A0E = colorStateList;
        TextView textView = this.mTitleTextView;
        if (textView != null) {
            textView.setTextColor(colorStateList);
        }
    }
}
