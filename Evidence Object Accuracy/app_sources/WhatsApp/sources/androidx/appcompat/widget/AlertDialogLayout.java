package androidx.appcompat.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class AlertDialogLayout extends LinearLayoutCompat {
    public AlertDialogLayout(Context context) {
        super(context, null);
    }

    public AlertDialogLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public static int A00(View view) {
        int minimumHeight = view.getMinimumHeight();
        if (minimumHeight > 0) {
            return minimumHeight;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (viewGroup.getChildCount() == 1) {
                return A00(viewGroup.getChildAt(0));
            }
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0077  */
    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r14, int r15, int r16, int r17, int r18) {
        /*
            r13 = this;
            int r12 = r13.getPaddingLeft()
            int r17 = r17 - r15
            int r0 = r13.getPaddingRight()
            int r11 = r17 - r0
            int r17 = r17 - r12
            int r0 = r13.getPaddingRight()
            int r17 = r17 - r0
            int r5 = r13.getMeasuredHeight()
            int r4 = r13.getChildCount()
            int r3 = r13.A06
            r1 = r3 & 112(0x70, float:1.57E-43)
            r0 = 8388615(0x800007, float:1.1754953E-38)
            r3 = r3 & r0
            r0 = 16
            if (r1 == r0) goto L_0x009c
            r0 = 80
            int r2 = r13.getPaddingTop()
            if (r1 != r0) goto L_0x0035
            int r2 = r2 + r18
            int r2 = r2 - r16
            int r2 = r2 - r5
        L_0x0035:
            android.graphics.drawable.Drawable r0 = r13.A0A
            if (r0 != 0) goto L_0x0097
            r10 = 0
        L_0x003a:
            r7 = 0
        L_0x003b:
            if (r7 >= r4) goto L_0x00a8
            android.view.View r8 = r13.getChildAt(r7)
            if (r8 == 0) goto L_0x0085
            int r1 = r8.getVisibility()
            r0 = 8
            if (r1 == r0) goto L_0x0085
            int r6 = r8.getMeasuredWidth()
            int r9 = r8.getMeasuredHeight()
            android.view.ViewGroup$LayoutParams r5 = r8.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r5 = (android.widget.LinearLayout.LayoutParams) r5
            int r1 = r5.gravity
            if (r1 >= 0) goto L_0x005e
            r1 = r3
        L_0x005e:
            int r0 = X.AnonymousClass028.A05(r13)
            int r0 = X.C05660Ql.A00(r1, r0)
            r1 = r0 & 7
            r0 = 1
            if (r1 == r0) goto L_0x008b
            r0 = 5
            if (r1 == r0) goto L_0x0088
            int r1 = r5.leftMargin
            int r1 = r1 + r12
        L_0x0071:
            boolean r0 = r13.A05(r7)
            if (r0 == 0) goto L_0x0078
            int r2 = r2 + r10
        L_0x0078:
            int r0 = r5.topMargin
            int r2 = r2 + r0
            int r6 = r6 + r1
            int r0 = r9 + r2
            r8.layout(r1, r2, r6, r0)
            int r0 = r5.bottomMargin
            int r9 = r9 + r0
            int r2 = r2 + r9
        L_0x0085:
            int r7 = r7 + 1
            goto L_0x003b
        L_0x0088:
            int r1 = r11 - r6
            goto L_0x0093
        L_0x008b:
            int r0 = r17 - r6
            int r1 = r0 >> 1
            int r1 = r1 + r12
            int r0 = r5.leftMargin
            int r1 = r1 + r0
        L_0x0093:
            int r0 = r5.rightMargin
            int r1 = r1 - r0
            goto L_0x0071
        L_0x0097:
            int r10 = r0.getIntrinsicHeight()
            goto L_0x003a
        L_0x009c:
            int r2 = r13.getPaddingTop()
            int r18 = r18 - r16
            int r18 = r18 - r5
            int r0 = r18 >> 1
            int r2 = r2 + r0
            goto L_0x0035
        L_0x00a8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.AlertDialogLayout.onLayout(boolean, int, int, int, int):void");
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int childCount = getChildCount();
        View view = null;
        int i7 = 0;
        View view2 = null;
        View view3 = null;
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                int id = childAt.getId();
                if (id == R.id.topPanel) {
                    view = childAt;
                } else if (id == R.id.buttonPanel) {
                    view2 = childAt;
                } else if ((id == R.id.contentPanel || id == R.id.customPanel) && view3 == null) {
                    view3 = childAt;
                } else {
                    super.onMeasure(i, i2);
                    return;
                }
            }
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i);
        int paddingTop = getPaddingTop() + getPaddingBottom();
        if (view != null) {
            view.measure(i, 0);
            paddingTop += view.getMeasuredHeight();
            i3 = View.combineMeasuredStates(0, view.getMeasuredState());
        } else {
            i3 = 0;
        }
        if (view2 != null) {
            view2.measure(i, 0);
            i4 = A00(view2);
            i5 = view2.getMeasuredHeight() - i4;
            paddingTop += i4;
            i3 = View.combineMeasuredStates(i3, view2.getMeasuredState());
        } else {
            i4 = 0;
            i5 = 0;
        }
        if (view3 != null) {
            if (mode != 0) {
                i7 = View.MeasureSpec.makeMeasureSpec(Math.max(0, size - paddingTop), mode);
            }
            view3.measure(i, i7);
            i6 = view3.getMeasuredHeight();
            paddingTop += i6;
            i3 = View.combineMeasuredStates(i3, view3.getMeasuredState());
        } else {
            i6 = 0;
        }
        int i9 = size - paddingTop;
        if (view2 != null) {
            int i10 = paddingTop - i4;
            int min = Math.min(i9, i5);
            if (min > 0) {
                i9 -= min;
                i4 += min;
            }
            view2.measure(i, View.MeasureSpec.makeMeasureSpec(i4, 1073741824));
            paddingTop = i10 + view2.getMeasuredHeight();
            i3 = View.combineMeasuredStates(i3, view2.getMeasuredState());
        }
        if (view3 != null && i9 > 0) {
            view3.measure(i, View.MeasureSpec.makeMeasureSpec(i6 + i9, mode));
            paddingTop = (paddingTop - i6) + view3.getMeasuredHeight();
            i3 = View.combineMeasuredStates(i3, view3.getMeasuredState());
        }
        int i11 = 0;
        for (int i12 = 0; i12 < childCount; i12++) {
            View childAt2 = getChildAt(i12);
            if (childAt2.getVisibility() != 8) {
                i11 = Math.max(i11, childAt2.getMeasuredWidth());
            }
        }
        setMeasuredDimension(View.resolveSizeAndState(i11 + getPaddingLeft() + getPaddingRight(), i, i3), View.resolveSizeAndState(paddingTop, i2, 0));
        if (mode2 != 1073741824) {
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
            for (int i13 = 0; i13 < childCount; i13++) {
                View childAt3 = getChildAt(i13);
                if (childAt3.getVisibility() != 8) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt3.getLayoutParams();
                    if (layoutParams.width == -1) {
                        int i14 = layoutParams.height;
                        layoutParams.height = childAt3.getMeasuredHeight();
                        measureChildWithMargins(childAt3, makeMeasureSpec, 0, i2, 0);
                        layoutParams.height = i14;
                    }
                }
            }
        }
    }
}
