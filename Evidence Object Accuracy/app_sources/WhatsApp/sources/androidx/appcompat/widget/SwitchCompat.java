package androidx.appcompat.widget;

import X.AnonymousClass028;
import X.AnonymousClass04D;
import X.AnonymousClass07O;
import X.AnonymousClass084;
import X.AnonymousClass086;
import X.AnonymousClass0Vs;
import X.C012005t;
import X.C013406h;
import X.C014706y;
import X.C015607k;
import X.C02160Ag;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.Property;
import android.view.ActionMode;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.CompoundButton;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class SwitchCompat extends CompoundButton {
    public static final Property A0d = new C02160Ag();
    public static final int[] A0e = {16842912};
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public ObjectAnimator A0G;
    public ColorStateList A0H;
    public ColorStateList A0I;
    public ColorStateList A0J;
    public PorterDuff.Mode A0K;
    public PorterDuff.Mode A0L;
    public Drawable A0M;
    public Drawable A0N;
    public Layout A0O;
    public Layout A0P;
    public TransformationMethod A0Q;
    public VelocityTracker A0R;
    public CharSequence A0S;
    public CharSequence A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public final Rect A0a;
    public final TextPaint A0b;
    public final AnonymousClass086 A0c;

    public SwitchCompat(Context context) {
        this(context, null);
    }

    public SwitchCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.switchStyle);
    }

    public SwitchCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Typeface typeface;
        AnonymousClass0Vs r0;
        Typeface create;
        int i2;
        this.A0I = null;
        this.A0K = null;
        this.A0U = false;
        this.A0V = false;
        this.A0J = null;
        this.A0L = null;
        this.A0W = false;
        this.A0X = false;
        this.A0R = VelocityTracker.obtain();
        this.A0a = new Rect();
        AnonymousClass084.A03(getContext(), this);
        TextPaint textPaint = new TextPaint(1);
        this.A0b = textPaint;
        textPaint.density = getResources().getDisplayMetrics().density;
        int[] iArr = AnonymousClass07O.A0L;
        C013406h A00 = C013406h.A00(context, attributeSet, iArr, i, 0);
        TypedArray typedArray = A00.A02;
        AnonymousClass028.A0L(context, typedArray, attributeSet, this, iArr, i);
        Drawable A02 = A00.A02(2);
        this.A0M = A02;
        if (A02 != null) {
            A02.setCallback(this);
        }
        Drawable A022 = A00.A02(11);
        this.A0N = A022;
        if (A022 != null) {
            A022.setCallback(this);
        }
        this.A0T = typedArray.getText(0);
        this.A0S = typedArray.getText(1);
        this.A0Y = typedArray.getBoolean(3, true);
        this.A0C = typedArray.getDimensionPixelSize(8, 0);
        this.A07 = typedArray.getDimensionPixelSize(5, 0);
        this.A08 = typedArray.getDimensionPixelSize(6, 0);
        this.A0Z = typedArray.getBoolean(4, false);
        ColorStateList A01 = A00.A01(9);
        if (A01 != null) {
            this.A0I = A01;
            this.A0U = true;
        }
        PorterDuff.Mode A002 = C014706y.A00(null, typedArray.getInt(10, -1));
        if (this.A0K != A002) {
            this.A0K = A002;
            this.A0V = true;
        }
        if (this.A0U || this.A0V) {
            A01();
        }
        ColorStateList A012 = A00.A01(12);
        if (A012 != null) {
            this.A0J = A012;
            this.A0W = true;
        }
        PorterDuff.Mode A003 = C014706y.A00(null, typedArray.getInt(13, -1));
        if (this.A0L != A003) {
            this.A0L = A003;
            this.A0X = true;
        }
        if (this.A0W || this.A0X) {
            A02();
        }
        boolean z = false;
        int resourceId = typedArray.getResourceId(7, 0);
        if (resourceId != 0) {
            C013406h r4 = new C013406h(context, context.obtainStyledAttributes(resourceId, AnonymousClass07O.A0M));
            ColorStateList A013 = r4.A01(3);
            this.A0H = A013 == null ? getTextColors() : A013;
            TypedArray typedArray2 = r4.A02;
            int dimensionPixelSize = typedArray2.getDimensionPixelSize(0, 0);
            if (dimensionPixelSize != 0) {
                float f = (float) dimensionPixelSize;
                TextPaint textPaint2 = this.A0b;
                if (f != textPaint2.getTextSize()) {
                    textPaint2.setTextSize(f);
                    requestLayout();
                }
            }
            int i3 = typedArray2.getInt(1, -1);
            int i4 = typedArray2.getInt(2, -1);
            if (i3 == 1) {
                typeface = Typeface.SANS_SERIF;
            } else if (i3 == 2) {
                typeface = Typeface.SERIF;
            } else if (i3 != 3) {
                typeface = null;
            } else {
                typeface = Typeface.MONOSPACE;
            }
            float f2 = 0.0f;
            if (i4 > 0) {
                if (typeface == null) {
                    create = Typeface.defaultFromStyle(i4);
                } else {
                    create = Typeface.create(typeface, i4);
                }
                setSwitchTypeface(create);
                if (create != null) {
                    i2 = create.getStyle();
                } else {
                    i2 = 0;
                }
                int i5 = (i2 ^ -1) & i4;
                TextPaint textPaint3 = this.A0b;
                textPaint3.setFakeBoldText((i5 & 1) != 0 ? true : z);
                textPaint3.setTextSkewX((i5 & 2) != 0 ? -0.25f : f2);
            } else {
                TextPaint textPaint4 = this.A0b;
                textPaint4.setFakeBoldText(false);
                textPaint4.setTextSkewX(0.0f);
                setSwitchTypeface(typeface);
            }
            if (typedArray2.getBoolean(14, false)) {
                r0 = new AnonymousClass0Vs(getContext());
            } else {
                r0 = null;
            }
            this.A0Q = r0;
            r4.A04();
        }
        AnonymousClass086 r02 = new AnonymousClass086(this);
        this.A0c = r02;
        r02.A0A(attributeSet, i);
        A00.A04();
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.A0F = viewConfiguration.getScaledTouchSlop();
        this.A03 = viewConfiguration.getScaledMinimumFlingVelocity();
        refreshDrawableState();
        setChecked(isChecked());
    }

    public final Layout A00(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        TransformationMethod transformationMethod = this.A0Q;
        if (transformationMethod != null) {
            charSequence2 = transformationMethod.getTransformation(charSequence, this);
        }
        TextPaint textPaint = this.A0b;
        return new StaticLayout(charSequence2, textPaint, charSequence2 != null ? (int) Math.ceil((double) Layout.getDesiredWidth(charSequence2, textPaint)) : 0, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
    }

    public final void A01() {
        Drawable drawable = this.A0M;
        if (drawable == null) {
            return;
        }
        if (this.A0U || this.A0V) {
            Drawable mutate = C015607k.A03(drawable).mutate();
            this.A0M = mutate;
            if (this.A0U) {
                C015607k.A04(this.A0I, mutate);
            }
            if (this.A0V) {
                C015607k.A07(this.A0K, this.A0M);
            }
            if (this.A0M.isStateful()) {
                this.A0M.setState(getDrawableState());
            }
        }
    }

    public final void A02() {
        Drawable drawable = this.A0N;
        if (drawable == null) {
            return;
        }
        if (this.A0W || this.A0X) {
            Drawable mutate = C015607k.A03(drawable).mutate();
            this.A0N = mutate;
            if (this.A0W) {
                C015607k.A04(this.A0J, mutate);
            }
            if (this.A0X) {
                C015607k.A07(this.A0L, this.A0N);
            }
            if (this.A0N.isStateful()) {
                this.A0N.setState(getDrawableState());
            }
        }
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        Rect rect;
        Rect rect2 = this.A0a;
        int i = this.A06;
        int i2 = this.A0A;
        int i3 = this.A09;
        int i4 = this.A04;
        int thumbOffset = getThumbOffset() + i;
        Drawable drawable = this.A0M;
        if (drawable != null) {
            rect = C014706y.A01(drawable);
        } else {
            rect = C014706y.A01;
        }
        Drawable drawable2 = this.A0N;
        if (drawable2 != null) {
            drawable2.getPadding(rect2);
            int i5 = rect2.left;
            thumbOffset += i5;
            int i6 = rect.left;
            if (i6 > i5) {
                i += i6 - i5;
            }
            int i7 = rect.top;
            int i8 = rect2.top;
            int i9 = (i7 - i8) + i2;
            if (i7 <= i8) {
                i9 = i2;
            }
            int i10 = rect.right;
            int i11 = rect2.right;
            if (i10 > i11) {
                i3 -= i10 - i11;
            }
            int i12 = rect.bottom;
            int i13 = rect2.bottom;
            int i14 = i4 - (i12 - i13);
            if (i12 <= i13) {
                i14 = i4;
            }
            this.A0N.setBounds(i, i9, i3, i14);
        }
        Drawable drawable3 = this.A0M;
        if (drawable3 != null) {
            drawable3.getPadding(rect2);
            int i15 = thumbOffset - rect2.left;
            int i16 = thumbOffset + this.A0D + rect2.right;
            this.A0M.setBounds(i15, i2, i16, i4);
            Drawable background = getBackground();
            if (background != null) {
                C015607k.A0B(background, i15, i2, i16, i4);
            }
        }
        super.draw(canvas);
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public void drawableHotspotChanged(float f, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            super.drawableHotspotChanged(f, f2);
        }
        Drawable drawable = this.A0M;
        if (drawable != null) {
            C015607k.A09(drawable, f, f2);
        }
        Drawable drawable2 = this.A0N;
        if (drawable2 != null) {
            C015607k.A09(drawable2, f, f2);
        }
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.A0M;
        boolean z = false;
        if (drawable != null && drawable.isStateful()) {
            z = false | drawable.setState(drawableState);
        }
        Drawable drawable2 = this.A0N;
        if (drawable2 != null && drawable2.isStateful()) {
            z |= drawable2.setState(drawableState);
        }
        if (z) {
            invalidate();
        }
    }

    @Override // android.widget.CompoundButton, android.widget.TextView
    public int getCompoundPaddingLeft() {
        boolean z = true;
        if (AnonymousClass028.A05(this) != 1) {
            z = false;
        }
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        if (!z) {
            return compoundPaddingLeft;
        }
        int i = compoundPaddingLeft + this.A0B;
        if (!TextUtils.isEmpty(getText())) {
            return i + this.A08;
        }
        return i;
    }

    @Override // android.widget.CompoundButton, android.widget.TextView
    public int getCompoundPaddingRight() {
        boolean z = true;
        if (AnonymousClass028.A05(this) != 1) {
            z = false;
        }
        int compoundPaddingRight = super.getCompoundPaddingRight();
        if (z) {
            return compoundPaddingRight;
        }
        int i = compoundPaddingRight + this.A0B;
        if (!TextUtils.isEmpty(getText())) {
            return i + this.A08;
        }
        return i;
    }

    public boolean getShowText() {
        return this.A0Y;
    }

    public boolean getSplitTrack() {
        return this.A0Z;
    }

    public int getSwitchMinWidth() {
        return this.A07;
    }

    public int getSwitchPadding() {
        return this.A08;
    }

    private boolean getTargetCheckedState() {
        return this.A00 > 0.5f;
    }

    public CharSequence getTextOff() {
        return this.A0S;
    }

    public CharSequence getTextOn() {
        return this.A0T;
    }

    public Drawable getThumbDrawable() {
        return this.A0M;
    }

    private int getThumbOffset() {
        float f;
        if (AnonymousClass028.A05(this) == 1) {
            f = 1.0f - this.A00;
        } else {
            f = this.A00;
        }
        return (int) ((f * ((float) getThumbScrollRange())) + 0.5f);
    }

    private int getThumbScrollRange() {
        Rect rect;
        Drawable drawable = this.A0N;
        if (drawable == null) {
            return 0;
        }
        Rect rect2 = this.A0a;
        drawable.getPadding(rect2);
        Drawable drawable2 = this.A0M;
        if (drawable2 != null) {
            rect = C014706y.A01(drawable2);
        } else {
            rect = C014706y.A01;
        }
        return ((((this.A0B - this.A0D) - rect2.left) - rect2.right) - rect.left) - rect.right;
    }

    public int getThumbTextPadding() {
        return this.A0C;
    }

    public ColorStateList getThumbTintList() {
        return this.A0I;
    }

    public PorterDuff.Mode getThumbTintMode() {
        return this.A0K;
    }

    public Drawable getTrackDrawable() {
        return this.A0N;
    }

    public ColorStateList getTrackTintList() {
        return this.A0J;
    }

    public PorterDuff.Mode getTrackTintMode() {
        return this.A0L;
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.A0M;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
        Drawable drawable2 = this.A0N;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
        }
        ObjectAnimator objectAnimator = this.A0G;
        if (objectAnimator != null && objectAnimator.isStarted()) {
            this.A0G.end();
            this.A0G = null;
        }
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (isChecked()) {
            CompoundButton.mergeDrawableStates(onCreateDrawableState, A0e);
        }
        return onCreateDrawableState;
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        Layout layout;
        int width;
        super.onDraw(canvas);
        Rect rect = this.A0a;
        Drawable drawable = this.A0N;
        if (drawable != null) {
            drawable.getPadding(rect);
        } else {
            rect.setEmpty();
        }
        int i = this.A0A;
        int i2 = this.A04;
        int i3 = i + rect.top;
        int i4 = i2 - rect.bottom;
        Drawable drawable2 = this.A0M;
        if (drawable != null) {
            if (!this.A0Z || drawable2 == null) {
                drawable.draw(canvas);
            } else {
                Rect A01 = C014706y.A01(drawable2);
                drawable2.copyBounds(rect);
                rect.left += A01.left;
                rect.right -= A01.right;
                int save = canvas.save();
                canvas.clipRect(rect, Region.Op.DIFFERENCE);
                drawable.draw(canvas);
                canvas.restoreToCount(save);
            }
        }
        int save2 = canvas.save();
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
        if (getTargetCheckedState()) {
            layout = this.A0P;
        } else {
            layout = this.A0O;
        }
        if (layout != null) {
            int[] drawableState = getDrawableState();
            ColorStateList colorStateList = this.A0H;
            if (colorStateList != null) {
                this.A0b.setColor(colorStateList.getColorForState(drawableState, 0));
            }
            this.A0b.drawableState = drawableState;
            if (drawable2 != null) {
                Rect bounds = drawable2.getBounds();
                width = bounds.left + bounds.right;
            } else {
                width = getWidth();
            }
            canvas.translate((float) ((width >> 1) - (layout.getWidth() >> 1)), (float) (((i3 + i4) >> 1) - (layout.getHeight() >> 1)));
            layout.draw(canvas);
        }
        canvas.restoreToCount(save2);
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName("android.widget.Switch");
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        CharSequence charSequence;
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName("android.widget.Switch");
        if (Build.VERSION.SDK_INT < 30) {
            if (isChecked()) {
                charSequence = this.A0T;
            } else {
                charSequence = this.A0S;
            }
            if (!TextUtils.isEmpty(charSequence)) {
                CharSequence text = accessibilityNodeInfo.getText();
                if (TextUtils.isEmpty(text)) {
                    accessibilityNodeInfo.setText(charSequence);
                    return;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(text);
                sb.append(' ');
                sb.append(charSequence);
                accessibilityNodeInfo.setText(sb);
            }
        }
    }

    @Override // android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int width;
        int i6;
        int i7;
        int paddingTop;
        int i8;
        super.onLayout(z, i, i2, i3, i4);
        int i9 = 0;
        if (this.A0M != null) {
            Rect rect = this.A0a;
            Drawable drawable = this.A0N;
            if (drawable != null) {
                drawable.getPadding(rect);
            } else {
                rect.setEmpty();
            }
            Rect A01 = C014706y.A01(this.A0M);
            i5 = Math.max(0, A01.left - rect.left);
            i9 = Math.max(0, A01.right - rect.right);
        } else {
            i5 = 0;
        }
        if (AnonymousClass028.A05(this) == 1) {
            i6 = getPaddingLeft() + i5;
            width = ((this.A0B + i6) - i5) - i9;
        } else {
            width = (getWidth() - getPaddingRight()) - i9;
            i6 = (width - this.A0B) + i5 + i9;
        }
        int gravity = getGravity() & 112;
        if (gravity == 16) {
            i7 = this.A05;
            paddingTop = (((getPaddingTop() + getHeight()) - getPaddingBottom()) >> 1) - (i7 >> 1);
        } else if (gravity != 80) {
            paddingTop = getPaddingTop();
            i7 = this.A05;
        } else {
            i8 = getHeight() - getPaddingBottom();
            paddingTop = i8 - this.A05;
            this.A06 = i6;
            this.A0A = paddingTop;
            this.A04 = i8;
            this.A09 = width;
        }
        i8 = i7 + paddingTop;
        this.A06 = i6;
        this.A0A = paddingTop;
        this.A04 = i8;
        this.A09 = width;
    }

    @Override // android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        if (this.A0Y) {
            if (this.A0P == null) {
                this.A0P = A00(this.A0T);
            }
            if (this.A0O == null) {
                this.A0O = A00(this.A0S);
            }
        }
        Rect rect = this.A0a;
        Drawable drawable = this.A0M;
        int i6 = 0;
        if (drawable != null) {
            drawable.getPadding(rect);
            i3 = (this.A0M.getIntrinsicWidth() - rect.left) - rect.right;
            i4 = this.A0M.getIntrinsicHeight();
        } else {
            i3 = 0;
            i4 = 0;
        }
        if (this.A0Y) {
            i5 = Math.max(this.A0P.getWidth(), this.A0O.getWidth()) + (this.A0C << 1);
        } else {
            i5 = 0;
        }
        this.A0D = Math.max(i5, i3);
        Drawable drawable2 = this.A0N;
        if (drawable2 != null) {
            drawable2.getPadding(rect);
            i6 = this.A0N.getIntrinsicHeight();
        } else {
            rect.setEmpty();
        }
        int i7 = rect.left;
        int i8 = rect.right;
        Drawable drawable3 = this.A0M;
        if (drawable3 != null) {
            Rect A01 = C014706y.A01(drawable3);
            i7 = Math.max(i7, A01.left);
            i8 = Math.max(i8, A01.right);
        }
        int max = Math.max(this.A07, (this.A0D << 1) + i7 + i8);
        int max2 = Math.max(i6, i4);
        this.A0B = max;
        this.A05 = max2;
        super.onMeasure(i, i2);
        if (getMeasuredHeight() < max2) {
            setMeasuredDimension(getMeasuredWidthAndState(), max2);
        }
    }

    @Override // android.view.View
    public void onPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        CharSequence charSequence;
        super.onPopulateAccessibilityEvent(accessibilityEvent);
        if (isChecked()) {
            charSequence = this.A0T;
        } else {
            charSequence = this.A0S;
        }
        if (charSequence != null) {
            accessibilityEvent.getText().add(charSequence);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a0, code lost:
        if (isEnabled() == false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0012, code lost:
        if (r1 != 3) goto L_0x0014;
     */
    @Override // android.widget.TextView, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r10) {
        /*
        // Method dump skipped, instructions count: 331
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.SwitchCompat.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.widget.CompoundButton, android.widget.Checkable
    public void setChecked(boolean z) {
        CharSequence charSequence;
        Resources resources;
        int i;
        super.setChecked(z);
        boolean isChecked = isChecked();
        int i2 = Build.VERSION.SDK_INT;
        if (isChecked) {
            if (i2 >= 30) {
                charSequence = this.A0T;
                if (charSequence == null) {
                    resources = getResources();
                    i = R.string.abc_capital_on;
                    charSequence = resources.getString(i);
                }
                AnonymousClass028.A0j(this, charSequence);
            }
        } else if (i2 >= 30) {
            charSequence = this.A0S;
            if (charSequence == null) {
                resources = getResources();
                i = R.string.abc_capital_off;
                charSequence = resources.getString(i);
            }
            AnonymousClass028.A0j(this, charSequence);
        }
        if (getWindowToken() == null || !AnonymousClass028.A0r(this)) {
            ObjectAnimator objectAnimator = this.A0G;
            if (objectAnimator != null) {
                objectAnimator.cancel();
            }
            float f = 0.0f;
            if (isChecked) {
                f = 1.0f;
            }
            setThumbPosition(f);
            return;
        }
        float f2 = 0.0f;
        if (isChecked) {
            f2 = 1.0f;
        }
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, A0d, f2);
        this.A0G = ofFloat;
        ofFloat.setDuration(250L);
        if (Build.VERSION.SDK_INT >= 18) {
            this.A0G.setAutoCancel(true);
        }
        this.A0G.start();
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(AnonymousClass04D.A02(callback, this));
    }

    public void setShowText(boolean z) {
        if (this.A0Y != z) {
            this.A0Y = z;
            requestLayout();
        }
    }

    public void setSplitTrack(boolean z) {
        this.A0Z = z;
        invalidate();
    }

    public void setSwitchMinWidth(int i) {
        this.A07 = i;
        requestLayout();
    }

    public void setSwitchPadding(int i) {
        this.A08 = i;
        requestLayout();
    }

    public void setSwitchTypeface(Typeface typeface) {
        TextPaint textPaint = this.A0b;
        if ((textPaint.getTypeface() != null && !textPaint.getTypeface().equals(typeface)) || (textPaint.getTypeface() == null && typeface != null)) {
            textPaint.setTypeface(typeface);
            requestLayout();
            invalidate();
        }
    }

    public void setTextOff(CharSequence charSequence) {
        this.A0S = charSequence;
        requestLayout();
        if (!isChecked() && Build.VERSION.SDK_INT >= 30) {
            CharSequence charSequence2 = this.A0S;
            if (charSequence2 == null) {
                charSequence2 = getResources().getString(R.string.abc_capital_off);
            }
            AnonymousClass028.A0j(this, charSequence2);
        }
    }

    public void setTextOn(CharSequence charSequence) {
        this.A0T = charSequence;
        requestLayout();
        if (isChecked() && Build.VERSION.SDK_INT >= 30) {
            CharSequence charSequence2 = this.A0T;
            if (charSequence2 == null) {
                charSequence2 = getResources().getString(R.string.abc_capital_on);
            }
            AnonymousClass028.A0j(this, charSequence2);
        }
    }

    public void setThumbDrawable(Drawable drawable) {
        Drawable drawable2 = this.A0M;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.A0M = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
        requestLayout();
    }

    public void setThumbPosition(float f) {
        this.A00 = f;
        invalidate();
    }

    public void setThumbResource(int i) {
        setThumbDrawable(C012005t.A01().A04(getContext(), i));
    }

    public void setThumbTextPadding(int i) {
        this.A0C = i;
        requestLayout();
    }

    public void setThumbTintList(ColorStateList colorStateList) {
        this.A0I = colorStateList;
        this.A0U = true;
        A01();
    }

    public void setThumbTintMode(PorterDuff.Mode mode) {
        this.A0K = mode;
        this.A0V = true;
        A01();
    }

    public void setTrackDrawable(Drawable drawable) {
        Drawable drawable2 = this.A0N;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.A0N = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
        requestLayout();
    }

    public void setTrackResource(int i) {
        setTrackDrawable(C012005t.A01().A04(getContext(), i));
    }

    public void setTrackTintList(ColorStateList colorStateList) {
        this.A0J = colorStateList;
        this.A0W = true;
        A02();
    }

    public void setTrackTintMode(PorterDuff.Mode mode) {
        this.A0L = mode;
        this.A0X = true;
        A02();
    }

    @Override // android.widget.CompoundButton, android.widget.Checkable
    public void toggle() {
        setChecked(!isChecked());
    }

    @Override // android.widget.CompoundButton, android.widget.TextView, android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.A0M || drawable == this.A0N;
    }
}
