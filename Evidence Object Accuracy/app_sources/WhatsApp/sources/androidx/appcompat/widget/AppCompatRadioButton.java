package androidx.appcompat.widget;

import X.AbstractC12370ho;
import X.AnonymousClass012;
import X.AnonymousClass083;
import X.AnonymousClass084;
import X.AnonymousClass085;
import X.AnonymousClass086;
import X.C012005t;
import X.C016107p;
import X.C05440Po;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class AppCompatRadioButton extends RadioButton implements AnonymousClass012, AbstractC12370ho {
    public final AnonymousClass085 A00;
    public final C05440Po A01;
    public final AnonymousClass086 A02;

    public AppCompatRadioButton(Context context) {
        this(context, null);
    }

    public AppCompatRadioButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.radioButtonStyle);
    }

    public AppCompatRadioButton(Context context, AttributeSet attributeSet, int i) {
        super(AnonymousClass083.A00(context), attributeSet, i);
        AnonymousClass084.A03(getContext(), this);
        C05440Po r0 = new C05440Po(this);
        this.A01 = r0;
        r0.A02(attributeSet, i);
        AnonymousClass085 r02 = new AnonymousClass085(this);
        this.A00 = r02;
        r02.A05(attributeSet, i);
        AnonymousClass086 r03 = new AnonymousClass086(this);
        this.A02 = r03;
        r03.A0A(attributeSet, i);
    }

    @Override // android.widget.TextView, android.widget.CompoundButton, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass086 r02 = this.A02;
        if (r02 != null) {
            r02.A02();
        }
    }

    @Override // android.widget.TextView, android.widget.CompoundButton
    public int getCompoundPaddingLeft() {
        int compoundPaddingLeft = super.getCompoundPaddingLeft();
        C05440Po r0 = this.A01;
        return r0 != null ? r0.A00(compoundPaddingLeft) : compoundPaddingLeft;
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A01;
    }

    public ColorStateList getSupportButtonTintList() {
        C05440Po r0 = this.A01;
        if (r0 != null) {
            return r0.A00;
        }
        return null;
    }

    public PorterDuff.Mode getSupportButtonTintMode() {
        C05440Po r0 = this.A01;
        if (r0 != null) {
            return r0.A01;
        }
        return null;
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A02(i);
        }
    }

    @Override // android.widget.CompoundButton
    public void setButtonDrawable(int i) {
        setButtonDrawable(C012005t.A01().A04(getContext(), i));
    }

    @Override // android.widget.CompoundButton
    public void setButtonDrawable(Drawable drawable) {
        super.setButtonDrawable(drawable);
        C05440Po r1 = this.A01;
        if (r1 == null) {
            return;
        }
        if (r1.A04) {
            r1.A04 = false;
            return;
        }
        r1.A04 = true;
        r1.A01();
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A03(colorStateList);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A04(mode);
        }
    }

    @Override // X.AbstractC12370ho
    public void setSupportButtonTintList(ColorStateList colorStateList) {
        C05440Po r1 = this.A01;
        if (r1 != null) {
            r1.A00 = colorStateList;
            r1.A02 = true;
            r1.A01();
        }
    }

    @Override // X.AbstractC12370ho
    public void setSupportButtonTintMode(PorterDuff.Mode mode) {
        C05440Po r1 = this.A01;
        if (r1 != null) {
            r1.A01 = mode;
            r1.A03 = true;
            r1.A01();
        }
    }
}
