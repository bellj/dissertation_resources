package androidx.appcompat.app;

import X.AnonymousClass04T;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/* loaded from: classes.dex */
public class AppCompatDialogFragment extends DialogFragment {
    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        return new AnonymousClass04T(A0p(), A18());
    }

    @Override // androidx.fragment.app.DialogFragment
    public void A1E(int i, Dialog dialog) {
        if (dialog instanceof AnonymousClass04T) {
            AnonymousClass04T r2 = (AnonymousClass04T) dialog;
            if (!(i == 1 || i == 2)) {
                if (i == 3) {
                    dialog.getWindow().addFlags(24);
                } else {
                    return;
                }
            }
            r2.A01();
            return;
        }
        super.A1E(i, dialog);
    }
}
