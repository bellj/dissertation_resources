package androidx.savedstate;

import X.AbstractC001200n;
import X.AbstractC001400p;
import X.AbstractC001500q;
import X.AbstractC019008y;
import X.AnonymousClass015;
import X.AnonymousClass054;
import X.AnonymousClass058;
import X.AnonymousClass05C;
import X.AnonymousClass074;
import android.os.Bundle;
import androidx.lifecycle.SavedStateHandleController;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* loaded from: classes.dex */
public final class Recreator implements AnonymousClass054 {
    public final AbstractC001500q A00;

    public Recreator(AbstractC001500q r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r8, AbstractC001200n r9) {
        if (r8 == AnonymousClass074.ON_CREATE) {
            r9.ADr().A01(this);
            AbstractC001500q r5 = this.A00;
            Bundle A00 = r5.AGO().A00("androidx.savedstate.Restarter");
            if (A00 != null) {
                ArrayList<String> stringArrayList = A00.getStringArrayList("classes_to_restore");
                if (stringArrayList != null) {
                    Iterator<String> it = stringArrayList.iterator();
                    while (it.hasNext()) {
                        String next = it.next();
                        try {
                            Class<? extends U> asSubclass = Class.forName(next, false, Recreator.class.getClassLoader()).asSubclass(AbstractC019008y.class);
                            try {
                                Constructor declaredConstructor = asSubclass.getDeclaredConstructor(new Class[0]);
                                declaredConstructor.setAccessible(true);
                                try {
                                    declaredConstructor.newInstance(new Object[0]);
                                    if (r5 instanceof AbstractC001400p) {
                                        AnonymousClass05C AHb = ((AbstractC001400p) r5).AHb();
                                        AnonymousClass058 AGO = r5.AGO();
                                        HashMap hashMap = AHb.A00;
                                        Iterator it2 = new HashSet(hashMap.keySet()).iterator();
                                        while (it2.hasNext()) {
                                            SavedStateHandleController.A00(r5.ADr(), (AnonymousClass015) hashMap.get(it2.next()), AGO);
                                        }
                                        if (!new HashSet(hashMap.keySet()).isEmpty()) {
                                            AGO.A01();
                                        }
                                    } else {
                                        throw new IllegalStateException("Internal error: OnRecreation should be registered only on componentsthat implement ViewModelStoreOwner");
                                    }
                                } catch (Exception e) {
                                    StringBuilder sb = new StringBuilder("Failed to instantiate ");
                                    sb.append(next);
                                    throw new RuntimeException(sb.toString(), e);
                                }
                            } catch (NoSuchMethodException e2) {
                                StringBuilder sb2 = new StringBuilder("Class");
                                sb2.append(asSubclass.getSimpleName());
                                sb2.append(" must have default constructor in order to be automatically recreated");
                                throw new IllegalStateException(sb2.toString(), e2);
                            }
                        } catch (ClassNotFoundException e3) {
                            StringBuilder sb3 = new StringBuilder("Class ");
                            sb3.append(next);
                            sb3.append(" wasn't found");
                            throw new RuntimeException(sb3.toString(), e3);
                        }
                    }
                    return;
                }
                throw new IllegalStateException("Bundle with restored state for the component \"androidx.savedstate.Restarter\" must contain list of strings by the key \"classes_to_restore\"");
            }
            return;
        }
        throw new AssertionError("Next event must be ON_CREATE");
    }
}
