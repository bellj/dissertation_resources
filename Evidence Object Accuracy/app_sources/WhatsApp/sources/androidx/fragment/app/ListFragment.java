package androidx.fragment.app;

import X.AnonymousClass01E;
import X.C07120Wt;
import X.RunnableC09240ca;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/* loaded from: classes.dex */
public class ListFragment extends AnonymousClass01E {
    public View A00;
    public View A01;
    public View A02;
    public ListAdapter A03;
    public ListView A04;
    public TextView A05;
    public boolean A06;
    public final Handler A07 = new Handler();
    public final AdapterView.OnItemClickListener A08 = new C07120Wt(this);
    public final Runnable A09 = new RunnableC09240ca(this);

    public static void A00(ListFragment listFragment) {
        if (listFragment.A04 == null) {
            View view = listFragment.A0A;
            if (view != null) {
                if (view instanceof ListView) {
                    listFragment.A04 = (ListView) view;
                } else {
                    TextView textView = (TextView) view.findViewById(16711681);
                    listFragment.A05 = textView;
                    if (textView == null) {
                        listFragment.A00 = view.findViewById(16908292);
                    } else {
                        textView.setVisibility(8);
                    }
                    listFragment.A02 = view.findViewById(16711682);
                    listFragment.A01 = view.findViewById(16711683);
                    View findViewById = view.findViewById(16908298);
                    if (findViewById instanceof ListView) {
                        ListView listView = (ListView) findViewById;
                        listFragment.A04 = listView;
                        View view2 = listFragment.A00;
                        if (view2 != null) {
                            listView.setEmptyView(view2);
                        }
                    } else if (findViewById == null) {
                        throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
                    } else {
                        throw new RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
                    }
                }
                listFragment.A06 = true;
                listFragment.A04.setOnItemClickListener(listFragment.A08);
                ListAdapter listAdapter = listFragment.A03;
                if (listAdapter != null) {
                    listFragment.A03 = null;
                    listFragment.A18(listAdapter);
                } else if (listFragment.A02 != null) {
                    A00(listFragment);
                    View view3 = listFragment.A02;
                    if (view3 == null) {
                        throw new IllegalStateException("Can't be used with a custom content view");
                    } else if (listFragment.A06) {
                        listFragment.A06 = false;
                        view3.clearAnimation();
                        listFragment.A01.clearAnimation();
                        listFragment.A02.setVisibility(0);
                        listFragment.A01.setVisibility(8);
                    }
                }
                listFragment.A07.post(listFragment.A09);
                return;
            }
            throw new IllegalStateException("Content view not yet created");
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Context A01 = A01();
        FrameLayout frameLayout = new FrameLayout(A01);
        LinearLayout linearLayout = new LinearLayout(A01);
        linearLayout.setId(16711682);
        linearLayout.setOrientation(1);
        linearLayout.setVisibility(8);
        linearLayout.setGravity(17);
        linearLayout.addView(new ProgressBar(A01, null, 16842874), new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout, new FrameLayout.LayoutParams(-1, -1));
        FrameLayout frameLayout2 = new FrameLayout(A01);
        frameLayout2.setId(16711683);
        TextView textView = new TextView(A01);
        textView.setId(16711681);
        textView.setGravity(17);
        frameLayout2.addView(textView, new FrameLayout.LayoutParams(-1, -1));
        ListView listView = new ListView(A01);
        listView.setId(16908298);
        listView.setDrawSelectorOnTop(false);
        frameLayout2.addView(listView, new FrameLayout.LayoutParams(-1, -1));
        frameLayout.addView(frameLayout2, new FrameLayout.LayoutParams(-1, -1));
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        return frameLayout;
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        this.A07.removeCallbacks(this.A09);
        this.A04 = null;
        this.A06 = false;
        this.A01 = null;
        this.A02 = null;
        this.A00 = null;
        this.A05 = null;
        this.A0V = true;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        A00(this);
    }

    public void A18(ListAdapter listAdapter) {
        boolean z = false;
        boolean z2 = false;
        if (this.A03 != null) {
            z2 = true;
        }
        this.A03 = listAdapter;
        ListView listView = this.A04;
        if (listView != null) {
            listView.setAdapter(listAdapter);
            if (!this.A06 && !z2) {
                if (A05().getWindowToken() != null) {
                    z = true;
                }
                A00(this);
                View view = this.A02;
                if (view == null) {
                    throw new IllegalStateException("Can't be used with a custom content view");
                } else if (!this.A06) {
                    this.A06 = true;
                    if (z) {
                        view.startAnimation(AnimationUtils.loadAnimation(A0p(), 17432577));
                        this.A01.startAnimation(AnimationUtils.loadAnimation(A0p(), 17432576));
                    } else {
                        view.clearAnimation();
                        this.A01.clearAnimation();
                    }
                    this.A02.setVisibility(8);
                    this.A01.setVisibility(0);
                }
            }
        }
    }
}
