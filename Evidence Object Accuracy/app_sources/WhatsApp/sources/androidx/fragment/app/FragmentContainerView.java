package androidx.fragment.app;

import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass028;
import X.AnonymousClass0MA;
import X.C004902f;
import X.C010605f;
import X.C018408o;
import X.C06460Ts;
import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.FrameLayout;
import com.whatsapp.R;
import java.util.ArrayList;

/* loaded from: classes.dex */
public final class FragmentContainerView extends FrameLayout {
    public View.OnApplyWindowInsetsListener A00;
    public ArrayList A01;
    public ArrayList A02;
    public boolean A03;

    @Override // android.view.View
    public WindowInsets onApplyWindowInsets(WindowInsets windowInsets) {
        return windowInsets;
    }

    public FragmentContainerView(Context context) {
        super(context);
        this.A03 = true;
    }

    public FragmentContainerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FragmentContainerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        String str;
        this.A03 = true;
        if (attributeSet != null) {
            String classAttribute = attributeSet.getClassAttribute();
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MA.A01);
            if (classAttribute == null) {
                classAttribute = obtainStyledAttributes.getString(0);
                str = "android:name";
            } else {
                str = "class";
            }
            obtainStyledAttributes.recycle();
            if (classAttribute != null && !isInEditMode()) {
                StringBuilder sb = new StringBuilder("FragmentContainerView must be within a FragmentActivity to use ");
                sb.append(str);
                sb.append("=\"");
                sb.append(classAttribute);
                sb.append("\"");
                throw new UnsupportedOperationException(sb.toString());
            }
        }
    }

    public FragmentContainerView(Context context, AttributeSet attributeSet, AnonymousClass01F r9) {
        super(context, attributeSet);
        View view;
        String str;
        this.A03 = true;
        String classAttribute = attributeSet.getClassAttribute();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MA.A01);
        classAttribute = classAttribute == null ? obtainStyledAttributes.getString(0) : classAttribute;
        String string = obtainStyledAttributes.getString(1);
        obtainStyledAttributes.recycle();
        int id = getId();
        AnonymousClass01E A07 = r9.A07(id);
        if (classAttribute != null && A07 == null) {
            if (id <= 0) {
                if (string != null) {
                    StringBuilder sb = new StringBuilder(" with tag ");
                    sb.append(string);
                    str = sb.toString();
                } else {
                    str = "";
                }
                StringBuilder sb2 = new StringBuilder("FragmentContainerView must have an android:id to add Fragment ");
                sb2.append(classAttribute);
                sb2.append(str);
                throw new IllegalStateException(sb2.toString());
            }
            C010605f A0B = r9.A0B();
            context.getClassLoader();
            AnonymousClass01E A00 = A0B.A00(classAttribute);
            A00.A0K();
            C004902f r1 = new C004902f(r9);
            r1.A0H = true;
            A00.A0B = this;
            r1.A0A(A00, string, getId());
            r1.A03();
        }
        for (C06460Ts r3 : r9.A0U.A01()) {
            AnonymousClass01E r2 = r3.A02;
            if (r2.A01 == getId() && (view = r2.A0A) != null && view.getParent() == null) {
                r2.A0B = this;
                r3.A02();
            }
        }
    }

    public final void A00(View view) {
        ArrayList arrayList = this.A02;
        if (arrayList != null && arrayList.contains(view)) {
            ArrayList arrayList2 = this.A01;
            if (arrayList2 == null) {
                arrayList2 = new ArrayList();
                this.A01 = arrayList2;
            }
            arrayList2.add(view);
        }
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        Object tag = view.getTag(R.id.fragment_container_view_tag);
        if (!(tag instanceof AnonymousClass01E) || tag == null) {
            StringBuilder sb = new StringBuilder("Views added to a FragmentContainerView must be associated with a Fragment. View ");
            sb.append(view);
            sb.append(" is not associated with a Fragment.");
            throw new IllegalStateException(sb.toString());
        }
        super.addView(view, i, layoutParams);
    }

    @Override // android.view.ViewGroup
    public boolean addViewInLayout(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        Object tag = view.getTag(R.id.fragment_container_view_tag);
        if ((tag instanceof AnonymousClass01E) && tag != null) {
            return super.addViewInLayout(view, i, layoutParams, z);
        }
        StringBuilder sb = new StringBuilder("Views added to a FragmentContainerView must be associated with a Fragment. View ");
        sb.append(view);
        sb.append(" is not associated with a Fragment.");
        throw new IllegalStateException(sb.toString());
    }

    @Override // android.view.View, android.view.ViewGroup
    public WindowInsets dispatchApplyWindowInsets(WindowInsets windowInsets) {
        C018408o A0I;
        C018408o A02 = C018408o.A02(windowInsets);
        View.OnApplyWindowInsetsListener onApplyWindowInsetsListener = this.A00;
        if (onApplyWindowInsetsListener != null) {
            A0I = C018408o.A02(onApplyWindowInsetsListener.onApplyWindowInsets(this, windowInsets));
        } else {
            A0I = AnonymousClass028.A0I(this, A02);
        }
        if (!A0I.A00.A0E()) {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                AnonymousClass028.A0H(getChildAt(i), A0I);
            }
        }
        return windowInsets;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchDraw(Canvas canvas) {
        if (this.A03 && this.A01 != null) {
            int i = 0;
            while (true) {
                ArrayList arrayList = this.A01;
                if (i >= arrayList.size()) {
                    break;
                }
                super.drawChild(canvas, (View) arrayList.get(i), getDrawingTime());
                i++;
            }
        }
        super.dispatchDraw(canvas);
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        ArrayList arrayList;
        if (!this.A03 || (arrayList = this.A01) == null || arrayList.size() <= 0 || !arrayList.contains(view)) {
            return super.drawChild(canvas, view, j);
        }
        return false;
    }

    @Override // android.view.ViewGroup
    public void endViewTransition(View view) {
        ArrayList arrayList = this.A02;
        if (arrayList != null) {
            arrayList.remove(view);
            ArrayList arrayList2 = this.A01;
            if (arrayList2 != null && arrayList2.remove(view)) {
                this.A03 = true;
            }
        }
        super.endViewTransition(view);
    }

    @Override // android.view.ViewGroup
    public void removeAllViewsInLayout() {
        int childCount = getChildCount();
        while (true) {
            childCount--;
            if (childCount >= 0) {
                A00(getChildAt(childCount));
            } else {
                super.removeAllViewsInLayout();
                return;
            }
        }
    }

    @Override // android.view.ViewGroup
    public void removeDetachedView(View view, boolean z) {
        if (z) {
            A00(view);
        }
        super.removeDetachedView(view, z);
    }

    @Override // android.view.ViewGroup, android.view.ViewManager
    public void removeView(View view) {
        A00(view);
        super.removeView(view);
    }

    @Override // android.view.ViewGroup
    public void removeViewAt(int i) {
        A00(getChildAt(i));
        super.removeViewAt(i);
    }

    @Override // android.view.ViewGroup
    public void removeViewInLayout(View view) {
        A00(view);
        super.removeViewInLayout(view);
    }

    @Override // android.view.ViewGroup
    public void removeViews(int i, int i2) {
        for (int i3 = i; i3 < i + i2; i3++) {
            A00(getChildAt(i3));
        }
        super.removeViews(i, i2);
    }

    @Override // android.view.ViewGroup
    public void removeViewsInLayout(int i, int i2) {
        for (int i3 = i; i3 < i + i2; i3++) {
            A00(getChildAt(i3));
        }
        super.removeViewsInLayout(i, i2);
    }

    public void setDrawDisappearingViewsLast(boolean z) {
        this.A03 = z;
    }

    @Override // android.view.ViewGroup
    public void setLayoutTransition(LayoutTransition layoutTransition) {
        if (Build.VERSION.SDK_INT < 18) {
            super.setLayoutTransition(layoutTransition);
            return;
        }
        throw new UnsupportedOperationException("FragmentContainerView does not support Layout Transitions or animateLayoutChanges=\"true\".");
    }

    @Override // android.view.View
    public void setOnApplyWindowInsetsListener(View.OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
        this.A00 = onApplyWindowInsetsListener;
    }

    @Override // android.view.ViewGroup
    public void startViewTransition(View view) {
        if (view.getParent() == this) {
            ArrayList arrayList = this.A02;
            if (arrayList == null) {
                arrayList = new ArrayList();
                this.A02 = arrayList;
            }
            arrayList.add(view);
        }
        super.startViewTransition(view);
    }
}
