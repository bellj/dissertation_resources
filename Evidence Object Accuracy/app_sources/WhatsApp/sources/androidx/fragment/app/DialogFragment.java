package androidx.fragment.app;

import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass02B;
import X.AnonymousClass05W;
import X.AnonymousClass0EF;
import X.AnonymousClass0EG;
import X.AnonymousClass0V3;
import X.AnonymousClass0V7;
import X.C004902f;
import X.C07500Yj;
import X.RunnableC09220cY;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class DialogFragment extends AnonymousClass01E implements DialogInterface.OnDismissListener, DialogInterface.OnCancelListener {
    public int A00 = -1;
    public int A01 = 0;
    public int A02 = 0;
    public Dialog A03;
    public DialogInterface.OnCancelListener A04 = new AnonymousClass0V3(this);
    public DialogInterface.OnDismissListener A05 = new AnonymousClass0V7(this);
    public Handler A06;
    public AnonymousClass02B A07 = new C07500Yj(this);
    public Runnable A08 = new RunnableC09220cY(this);
    public boolean A09 = true;
    public boolean A0A;
    public boolean A0B = false;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E = true;
    public boolean A0F;

    @Override // android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
    }

    @Override // X.AnonymousClass01E
    public AnonymousClass05W A0D() {
        return new AnonymousClass0EG(this, new AnonymousClass0EF(this));
    }

    @Override // X.AnonymousClass01E
    public void A0V(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        Bundle bundle2;
        super.A0V(bundle, layoutInflater, viewGroup);
        if (super.A0A == null && this.A03 != null && bundle != null && (bundle2 = bundle.getBundle("android:savedDialogState")) != null) {
            this.A03.onRestoreInstanceState(bundle2);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0k(Bundle bundle) {
        Bundle bundle2;
        this.A0V = true;
        if (this.A03 != null && bundle != null && (bundle2 = bundle.getBundle("android:savedDialogState")) != null) {
            this.A03.onRestoreInstanceState(bundle2);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0l() {
        this.A0V = true;
        if (!this.A0D && !this.A0C) {
            this.A0C = true;
        }
        this.A0L.A09(this.A07);
    }

    @Override // X.AnonymousClass01E
    public LayoutInflater A0q(Bundle bundle) {
        StringBuilder sb;
        String str;
        LayoutInflater A0q = super.A0q(bundle);
        if (this.A0E && !this.A0A) {
            if (!this.A0B) {
                try {
                    this.A0A = true;
                    Dialog A1A = A1A(bundle);
                    this.A03 = A1A;
                    if (this.A0E) {
                        A1E(this.A01, A1A);
                        Context A0p = A0p();
                        if (A0p instanceof Activity) {
                            this.A03.setOwnerActivity((Activity) A0p);
                        }
                        this.A03.setCancelable(this.A09);
                        this.A03.setOnCancelListener(this.A04);
                        this.A03.setOnDismissListener(this.A05);
                        this.A0B = true;
                    } else {
                        this.A03 = null;
                    }
                } finally {
                    this.A0A = false;
                }
            }
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb2 = new StringBuilder("get layout inflater for DialogFragment ");
                sb2.append(this);
                sb2.append(" from dialog context");
                Log.d("FragmentManager", sb2.toString());
            }
            Dialog dialog = this.A03;
            if (dialog != null) {
                return A0q.cloneInContext(dialog.getContext());
            }
        } else if (AnonymousClass01F.A01(2)) {
            StringBuilder sb3 = new StringBuilder("getting layout inflater for DialogFragment ");
            sb3.append(this);
            String obj = sb3.toString();
            if (!this.A0E) {
                sb = new StringBuilder();
                str = "mShowsDialog = false: ";
            } else {
                sb = new StringBuilder();
                str = "mCreatingDialog = true: ";
            }
            sb.append(str);
            sb.append(obj);
            Log.d("FragmentManager", sb.toString());
        }
        return A0q;
    }

    @Override // X.AnonymousClass01E
    public void A0s() {
        this.A0V = true;
        Dialog dialog = this.A03;
        if (dialog != null) {
            this.A0F = false;
            dialog.show();
            View decorView = this.A03.getWindow().getDecorView();
            decorView.setTag(R.id.view_tree_lifecycle_owner, this);
            decorView.setTag(R.id.view_tree_view_model_store_owner, this);
            decorView.setTag(R.id.view_tree_saved_state_registry_owner, this);
        }
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        Dialog dialog = this.A03;
        if (dialog != null) {
            Bundle onSaveInstanceState = dialog.onSaveInstanceState();
            onSaveInstanceState.putBoolean("android:dialogShowing", false);
            bundle.putBundle("android:savedDialogState", onSaveInstanceState);
        }
        int i = this.A01;
        if (i != 0) {
            bundle.putInt("android:style", i);
        }
        int i2 = this.A02;
        if (i2 != 0) {
            bundle.putInt("android:theme", i2);
        }
        boolean z = this.A09;
        if (!z) {
            bundle.putBoolean("android:cancelable", z);
        }
        boolean z2 = this.A0E;
        if (!z2) {
            bundle.putBoolean("android:showsDialog", z2);
        }
        int i3 = this.A00;
        if (i3 != -1) {
            bundle.putInt("android:backStackId", i3);
        }
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        this.A0V = true;
        Dialog dialog = this.A03;
        if (dialog != null) {
            this.A0F = true;
            dialog.setOnDismissListener(null);
            this.A03.dismiss();
            if (!this.A0C) {
                onDismiss(this.A03);
            }
            this.A03 = null;
            this.A0B = false;
        }
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        this.A0V = true;
        Dialog dialog = this.A03;
        if (dialog != null) {
            dialog.hide();
        }
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        this.A0L.A08(this.A07);
        if (!this.A0D) {
            this.A0C = false;
        }
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        super.A16(bundle);
        this.A06 = new Handler();
        boolean z = false;
        if (super.A01 == 0) {
            z = true;
        }
        this.A0E = z;
        if (bundle != null) {
            this.A01 = bundle.getInt("android:style", 0);
            this.A02 = bundle.getInt("android:theme", 0);
            this.A09 = bundle.getBoolean("android:cancelable", true);
            this.A0E = bundle.getBoolean("android:showsDialog", this.A0E);
            this.A00 = bundle.getInt("android:backStackId", -1);
        }
    }

    public int A18() {
        return this.A02;
    }

    public final Dialog A19() {
        Dialog dialog = this.A03;
        if (dialog != null) {
            return dialog;
        }
        StringBuilder sb = new StringBuilder("DialogFragment ");
        sb.append(this);
        sb.append(" does not have a Dialog.");
        throw new IllegalStateException(sb.toString());
    }

    public Dialog A1A(Bundle bundle) {
        if (AnonymousClass01F.A01(3)) {
            StringBuilder sb = new StringBuilder("onCreateDialog called for DialogFragment ");
            sb.append(this);
            Log.d("FragmentManager", sb.toString());
        }
        return new Dialog(A01(), A18());
    }

    public void A1B() {
        A1H(false, false);
    }

    public void A1C() {
        A1H(true, false);
    }

    public void A1D(int i, int i2) {
        if (AnonymousClass01F.A01(2)) {
            StringBuilder sb = new StringBuilder("Setting style and theme for DialogFragment ");
            sb.append(this);
            sb.append(" to ");
            sb.append(i);
            sb.append(", ");
            sb.append(i2);
            Log.d("FragmentManager", sb.toString());
        }
        this.A01 = i;
        if (i == 2 || i == 3) {
            this.A02 = 16973913;
        }
        if (i2 != 0) {
            this.A02 = i2;
        }
    }

    public void A1E(int i, Dialog dialog) {
        if (!(i == 1 || i == 2)) {
            if (i == 3) {
                Window window = dialog.getWindow();
                if (window != null) {
                    window.addFlags(24);
                }
            } else {
                return;
            }
        }
        dialog.requestWindowFeature(1);
    }

    public void A1F(AnonymousClass01F r3, String str) {
        this.A0C = false;
        this.A0D = true;
        C004902f r1 = new C004902f(r3);
        r1.A09(this, str);
        r1.A00(false);
    }

    public void A1G(boolean z) {
        this.A09 = z;
        Dialog dialog = this.A03;
        if (dialog != null) {
            dialog.setCancelable(z);
        }
    }

    public final void A1H(boolean z, boolean z2) {
        if (!this.A0C) {
            this.A0C = true;
            this.A0D = false;
            Dialog dialog = this.A03;
            if (dialog != null) {
                dialog.setOnDismissListener(null);
                this.A03.dismiss();
                if (!z2) {
                    if (Looper.myLooper() == this.A06.getLooper()) {
                        onDismiss(this.A03);
                    } else {
                        this.A06.post(this.A08);
                    }
                }
            }
            this.A0F = true;
            if (this.A00 >= 0) {
                A0F().A0M(this.A00);
                this.A00 = -1;
                return;
            }
            C004902f r1 = new C004902f(A0F());
            r1.A05(this);
            if (z) {
                r1.A00(true);
            } else {
                r1.A00(false);
            }
        }
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        if (!this.A0F) {
            if (AnonymousClass01F.A01(3)) {
                StringBuilder sb = new StringBuilder("onDismiss called for DialogFragment ");
                sb.append(this);
                Log.d("FragmentManager", sb.toString());
            }
            A1H(true, true);
        }
    }
}
