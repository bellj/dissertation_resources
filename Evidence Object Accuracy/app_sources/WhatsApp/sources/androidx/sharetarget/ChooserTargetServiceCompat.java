package androidx.sharetarget;

import X.AnonymousClass0MY;
import X.AnonymousClass0NK;
import X.AnonymousClass0NL;
import X.AnonymousClass0TS;
import X.C007603x;
import X.C08920c4;
import X.CallableC10460ef;
import X.CallableC10470eg;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.service.chooser.ChooserTarget;
import android.service.chooser.ChooserTargetService;
import android.text.TextUtils;
import android.util.Log;
import androidx.core.graphics.drawable.IconCompat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class ChooserTargetServiceCompat extends ChooserTargetService {
    @Override // android.service.chooser.ChooserTargetService
    public List onGetChooserTargets(ComponentName componentName, IntentFilter intentFilter) {
        IconCompat iconCompat;
        Bitmap bitmap;
        int identifier;
        Context applicationContext = getApplicationContext();
        ArrayList A01 = AnonymousClass0TS.A01(applicationContext);
        ArrayList arrayList = new ArrayList();
        Iterator it = A01.iterator();
        while (it.hasNext()) {
            AnonymousClass0NK r6 = (AnonymousClass0NK) it.next();
            if (r6.A00.equals(componentName.getClassName())) {
                AnonymousClass0MY[] r5 = r6.A01;
                int length = r5.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (intentFilter.hasDataType(r5[i].A00)) {
                        arrayList.add(r6);
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            if (ShortcutInfoCompatSaverImpl.A08 == null) {
                synchronized (ShortcutInfoCompatSaverImpl.A07) {
                    if (ShortcutInfoCompatSaverImpl.A08 == null) {
                        TimeUnit timeUnit = TimeUnit.SECONDS;
                        ShortcutInfoCompatSaverImpl.A08 = new ShortcutInfoCompatSaverImpl(applicationContext, new ThreadPoolExecutor(0, 1, 20, timeUnit, new LinkedBlockingQueue()), new ThreadPoolExecutor(0, 1, 20, timeUnit, new LinkedBlockingQueue()));
                    }
                }
            }
            ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl = ShortcutInfoCompatSaverImpl.A08;
            try {
                List<C007603x> A02 = shortcutInfoCompatSaverImpl.A02();
                if (A02 != null && !A02.isEmpty()) {
                    ArrayList arrayList2 = new ArrayList();
                    for (C007603x r8 : A02) {
                        Iterator it2 = arrayList.iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                AnonymousClass0NK r7 = (AnonymousClass0NK) it2.next();
                                if (r8.A0F.containsAll(Arrays.asList(r7.A02))) {
                                    arrayList2.add(new C08920c4(new ComponentName(applicationContext.getPackageName(), r7.A00), r8));
                                    break;
                                }
                            }
                        }
                    }
                    if (arrayList2.isEmpty()) {
                        return new ArrayList();
                    }
                    Collections.sort(arrayList2);
                    ArrayList arrayList3 = new ArrayList();
                    float f = 1.0f;
                    int i2 = ((C08920c4) arrayList2.get(0)).A01.A02;
                    Iterator it3 = arrayList2.iterator();
                    while (it3.hasNext()) {
                        C08920c4 r62 = (C08920c4) it3.next();
                        C007603x r72 = r62.A01;
                        Icon icon = null;
                        try {
                            AnonymousClass0NL r9 = (AnonymousClass0NL) shortcutInfoCompatSaverImpl.A05.submit(new CallableC10460ef(shortcutInfoCompatSaverImpl, r72.A0D)).get();
                            iconCompat = null;
                            if (r9 != null) {
                                String str = r9.A02;
                                if (!TextUtils.isEmpty(str)) {
                                    try {
                                        identifier = shortcutInfoCompatSaverImpl.A00.getResources().getIdentifier(str, null, null);
                                    } catch (Exception unused) {
                                    }
                                    if (identifier != 0) {
                                        Context context = shortcutInfoCompatSaverImpl.A00;
                                        iconCompat = IconCompat.A02(context.getResources(), context.getPackageName(), identifier);
                                    }
                                }
                                if (!TextUtils.isEmpty(r9.A01) && (bitmap = (Bitmap) shortcutInfoCompatSaverImpl.A06.submit(new CallableC10470eg(shortcutInfoCompatSaverImpl, r9)).get()) != null) {
                                    iconCompat = new IconCompat(1);
                                    iconCompat.A06 = bitmap;
                                }
                            }
                        } catch (Exception e) {
                            Log.e("ChooserServiceCompat", "Failed to retrieve shortcut icon: ", e);
                            iconCompat = null;
                        }
                        Bundle bundle = new Bundle();
                        bundle.putString("android.intent.extra.shortcut.ID", r72.A0D);
                        int i3 = r72.A02;
                        if (i2 != i3) {
                            f -= 0.01f;
                            i2 = i3;
                        }
                        CharSequence charSequence = r72.A0B;
                        if (iconCompat != null) {
                            icon = iconCompat.A07();
                        }
                        arrayList3.add(new ChooserTarget(charSequence, icon, f, r62.A00, bundle));
                    }
                    return arrayList3;
                }
            } catch (Exception e2) {
                Log.e("ChooserServiceCompat", "Failed to retrieve shortcuts: ", e2);
                return Collections.emptyList();
            }
        }
        return Collections.emptyList();
    }
}
