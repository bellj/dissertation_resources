package androidx.sharetarget;

import X.AbstractFutureC44231yX;
import X.AnonymousClass00N;
import X.AnonymousClass03w;
import X.AnonymousClass0NL;
import X.AnonymousClass0PX;
import X.C007603x;
import X.C02510Co;
import X.CallableC10430ec;
import X.RunnableC09610dE;
import X.RunnableC09870de;
import X.RunnableC09890dg;
import X.RunnableC09900dh;
import X.RunnableC09910di;
import X.RunnableC10180eA;
import X.RunnableC10190eB;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/* loaded from: classes.dex */
public class ShortcutInfoCompatSaverImpl extends AnonymousClass0PX {
    public static final Object A07 = new Object();
    public static volatile ShortcutInfoCompatSaverImpl A08;
    public final Context A00;
    public final File A01;
    public final File A02;
    public final Map A03 = new AnonymousClass00N();
    public final Map A04 = new AnonymousClass00N();
    public final ExecutorService A05;
    public final ExecutorService A06;

    public ShortcutInfoCompatSaverImpl(Context context, ExecutorService executorService, ExecutorService executorService2) {
        this.A00 = context.getApplicationContext();
        this.A05 = executorService;
        this.A06 = executorService2;
        File file = new File(context.getFilesDir(), "ShortcutInfoCompatSaver_share_targets");
        this.A01 = new File(file, "ShortcutInfoCompatSaver_share_targets_bitmaps");
        this.A02 = new File(file, "targets.xml");
        executorService.submit(new RunnableC10190eB(this, file));
    }

    @Override // X.AnonymousClass0PX
    public /* bridge */ /* synthetic */ Object A00() {
        C02510Co A00 = C02510Co.A00();
        this.A05.submit(new RunnableC09610dE(A00, this));
        return A00;
    }

    @Override // X.AnonymousClass0PX
    public /* bridge */ /* synthetic */ Object A01(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new AnonymousClass03w((C007603x) it.next()).A00());
        }
        C02510Co A00 = C02510Co.A00();
        this.A05.submit(new RunnableC09890dg(A00, this, arrayList));
        return A00;
    }

    @Override // X.AnonymousClass0PX
    public List A02() {
        return (List) this.A05.submit(new CallableC10430ec(this)).get();
    }

    public AbstractFutureC44231yX A03(Bitmap bitmap, String str) {
        RunnableC09900dh r3 = new RunnableC09900dh(bitmap, this, str);
        C02510Co A00 = C02510Co.A00();
        this.A06.submit(new RunnableC09910di(A00, this, r3));
        return A00;
    }

    public void A04(C02510Co r5) {
        RunnableC10180eA r3 = new RunnableC10180eA(this, new ArrayList(this.A04.values()));
        C02510Co A00 = C02510Co.A00();
        this.A06.submit(new RunnableC09910di(A00, this, r3));
        A00.A5i(new RunnableC09870de(r5, this, A00), this.A05);
    }

    public void A05(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            String str = ((AnonymousClass0NL) it.next()).A01;
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(str);
            }
        }
        File[] listFiles = this.A01.listFiles();
        for (File file : listFiles) {
            if (!arrayList.contains(file.getAbsolutePath())) {
                file.delete();
            }
        }
    }
}
