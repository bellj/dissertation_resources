package androidx.lifecycle;

import X.AbstractC001200n;
import X.AnonymousClass017;
import X.AnonymousClass02B;
import X.AnonymousClass054;
import X.AnonymousClass05I;
import X.AnonymousClass06P;
import X.AnonymousClass074;
import X.C009804x;

/* loaded from: classes.dex */
public class LiveData$LifecycleBoundObserver extends AnonymousClass06P implements AnonymousClass054 {
    public final AbstractC001200n A00;
    public final /* synthetic */ AnonymousClass017 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public LiveData$LifecycleBoundObserver(AbstractC001200n r1, AnonymousClass017 r2, AnonymousClass02B r3) {
        super(r2, r3);
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass06P
    public void A00() {
        this.A00.ADr().A01(this);
    }

    @Override // X.AnonymousClass06P
    public boolean A02() {
        return ((C009804x) this.A00.ADr()).A02.compareTo(AnonymousClass05I.STARTED) >= 0;
    }

    @Override // X.AnonymousClass06P
    public boolean A03(AbstractC001200n r3) {
        return this.A00 == r3;
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r5, AbstractC001200n r6) {
        AbstractC001200n r2 = this.A00;
        AnonymousClass05I r1 = ((C009804x) r2.ADr()).A02;
        AnonymousClass05I r3 = r1;
        if (r1 == AnonymousClass05I.DESTROYED) {
            this.A01.A09(this.A02);
            return;
        }
        AnonymousClass05I r0 = null;
        while (r0 != r1) {
            A01(A02());
            r1 = ((C009804x) r2.ADr()).A02;
            r0 = r3;
            r3 = r1;
        }
    }
}
