package androidx.lifecycle;

import X.AbstractC001200n;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.AnonymousClass5VF;
import X.AnonymousClass5X4;

/* loaded from: classes.dex */
public final class LifecycleCoroutineScopeImpl implements AnonymousClass054, AnonymousClass5VF {
    @Override // X.AnonymousClass5VF
    public AnonymousClass5X4 ABk() {
        return null;
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r3, AbstractC001200n r4) {
        throw new NullPointerException("getCurrentState");
    }
}
