package androidx.lifecycle;

import X.AbstractC001200n;
import X.AbstractC11300g4;
import X.AnonymousClass054;
import X.AnonymousClass074;

/* loaded from: classes.dex */
public class CompositeGeneratedAdaptersObserver implements AnonymousClass054 {
    public final AbstractC11300g4[] A00;

    public CompositeGeneratedAdaptersObserver(AbstractC11300g4[] r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r3, AbstractC001200n r4) {
        if (0 < this.A00.length) {
            throw new NullPointerException("callMethods");
        }
    }
}
