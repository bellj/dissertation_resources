package androidx.lifecycle;

import X.AbstractC001200n;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.C05840Re;
import X.C06330Te;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public class ReflectiveGenericLifecycleObserver implements AnonymousClass054 {
    public final C05840Re A00;
    public final Object A01;

    public ReflectiveGenericLifecycleObserver(Object obj) {
        this.A01 = obj;
        C06330Te r2 = C06330Te.A02;
        Class<?> cls = obj.getClass();
        C05840Re r0 = (C05840Re) r2.A00.get(cls);
        this.A00 = r0 == null ? r2.A01(cls, null) : r0;
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r4, AbstractC001200n r5) {
        C05840Re r0 = this.A00;
        Object obj = this.A01;
        Map map = r0.A00;
        C05840Re.A00(r4, r5, obj, (List) map.get(r4));
        C05840Re.A00(r4, r5, obj, (List) map.get(AnonymousClass074.ON_ANY));
    }
}
