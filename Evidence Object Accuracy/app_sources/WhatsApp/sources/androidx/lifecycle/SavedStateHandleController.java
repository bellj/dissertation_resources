package androidx.lifecycle;

import X.AbstractC001200n;
import X.AbstractC009904y;
import X.AnonymousClass015;
import X.AnonymousClass054;
import X.AnonymousClass058;
import X.AnonymousClass05I;
import X.AnonymousClass074;
import X.AnonymousClass07E;
import X.C009804x;
import java.util.Map;

/* loaded from: classes.dex */
public final class SavedStateHandleController implements AnonymousClass054 {
    public boolean A00 = false;
    public final AnonymousClass07E A01;
    public final String A02;

    public SavedStateHandleController(AnonymousClass07E r2, String str) {
        this.A02 = str;
        this.A01 = r2;
    }

    public static void A00(AbstractC009904y r3, AnonymousClass015 r4, AnonymousClass058 r5) {
        Object obj;
        Map map = r4.A00;
        synchronized (map) {
            obj = map.get("androidx.lifecycle.savedstate.vm.tag");
        }
        SavedStateHandleController savedStateHandleController = (SavedStateHandleController) obj;
        if (savedStateHandleController != null && !savedStateHandleController.A00) {
            savedStateHandleController.A02(r3, r5);
            A01(r3, r5);
        }
    }

    public static void A01(final AbstractC009904y r2, final AnonymousClass058 r3) {
        AnonymousClass05I r1 = ((C009804x) r2).A02;
        if (r1 == AnonymousClass05I.INITIALIZED || r1.compareTo(AnonymousClass05I.STARTED) >= 0) {
            r3.A01();
        } else {
            r2.A00(new AnonymousClass054() { // from class: androidx.lifecycle.SavedStateHandleController.1
                @Override // X.AnonymousClass054
                public void AWQ(AnonymousClass074 r22, AbstractC001200n r32) {
                    if (r22 == AnonymousClass074.ON_START) {
                        AbstractC009904y.this.A01(this);
                        r3.A01();
                    }
                }
            });
        }
    }

    public void A02(AbstractC009904y r3, AnonymousClass058 r4) {
        if (!this.A00) {
            this.A00 = true;
            r3.A00(this);
            r4.A02(this.A01.A00, this.A02);
            return;
        }
        throw new IllegalStateException("Already attached to lifecycleOwner");
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r2, AbstractC001200n r3) {
        if (r2 == AnonymousClass074.ON_DESTROY) {
            this.A00 = false;
            r3.ADr().A01(this);
        }
    }
}
