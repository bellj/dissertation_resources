package androidx.media;

import X.AbstractC007203r;
import X.AnonymousClass0QP;

/* loaded from: classes.dex */
public class AudioAttributesCompatParcelizer {
    public static AudioAttributesCompat read(AnonymousClass0QP r3) {
        AudioAttributesCompat audioAttributesCompat = new AudioAttributesCompat();
        AbstractC007203r r1 = audioAttributesCompat.A00;
        if (r3.A09(1)) {
            r1 = r3.A03();
        }
        audioAttributesCompat.A00 = (AudioAttributesImpl) r1;
        return audioAttributesCompat;
    }

    public static void write(AudioAttributesCompat audioAttributesCompat, AnonymousClass0QP r2) {
        AudioAttributesImpl audioAttributesImpl = audioAttributesCompat.A00;
        r2.A05(1);
        r2.A08(audioAttributesImpl);
    }
}
