package androidx.room;

import X.AnonymousClass0AL;
import X.AnonymousClass0AU;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import java.util.HashMap;

/* loaded from: classes.dex */
public class MultiInstanceInvalidationService extends Service {
    public int A00 = 0;
    public final RemoteCallbackList A01 = new AnonymousClass0AU(this);
    public final AnonymousClass0AL A02 = new AnonymousClass0AL(this);
    public final HashMap A03 = new HashMap();

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.A02;
    }
}
