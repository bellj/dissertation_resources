package androidx.preference;

import X.AbstractC005302m;
import X.AbstractC11330g7;
import X.AbstractC11820gv;
import X.AbstractC11830gw;
import X.AbstractC11840gx;
import X.AnonymousClass02M;
import X.AnonymousClass047;
import X.AnonymousClass04Z;
import X.AnonymousClass06r;
import X.AnonymousClass0F1;
import X.AnonymousClass0W3;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.AbsSavedState;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class Preference implements Comparable {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public Context A05;
    public Intent A06;
    public Drawable A07;
    public Bundle A08;
    public AbstractC11330g7 A09;
    public AbstractC11820gv A0A;
    public AbstractC11830gw A0B;
    public AnonymousClass0W3 A0C;
    public AbstractC11840gx A0D;
    public PreferenceGroup A0E;
    public AnonymousClass047 A0F;
    public CharSequence A0G;
    public CharSequence A0H;
    public Object A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public List A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public final View.OnClickListener A0d;

    public Object A03(TypedArray typedArray, int i) {
        return null;
    }

    public void A07() {
    }

    @Deprecated
    public void A0G(AnonymousClass04Z r1) {
    }

    public void A0J(Object obj) {
    }

    public Preference(Context context) {
        this(context, null);
    }

    public Preference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, AnonymousClass06r.A00(context, R.attr.preferenceStyle, 16842894));
    }

    public Preference(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0105, code lost:
        if (r3.hasValue(11) != false) goto L_0x0107;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Preference(android.content.Context r7, android.util.AttributeSet r8, int r9, int r10) {
        /*
        // Method dump skipped, instructions count: 349
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.Preference.<init>(android.content.Context, android.util.AttributeSet, int, int):void");
    }

    public long A00() {
        return this.A04;
    }

    public Parcelable A01() {
        this.A0P = true;
        return AbsSavedState.EMPTY_STATE;
    }

    public CharSequence A02() {
        AbstractC11840gx r0 = this.A0D;
        if (r0 != null) {
            return r0.AZc(this);
        }
        return this.A0G;
    }

    public void A04() {
        AbstractC11330g7 r1 = this.A09;
        if (r1 != null) {
            AnonymousClass0F1 r12 = (AnonymousClass0F1) r1;
            int indexOf = r12.A05.indexOf(this);
            if (indexOf != -1) {
                ((AnonymousClass02M) r12).A01.A04(this, indexOf, 1);
            }
        }
    }

    public void A05() {
        AbstractC11330g7 r0 = this.A09;
        if (r0 != null) {
            AnonymousClass0F1 r02 = (AnonymousClass0F1) r0;
            Handler handler = r02.A00;
            Runnable runnable = r02.A02;
            handler.removeCallbacks(runnable);
            handler.post(runnable);
        }
    }

    public void A06() {
        PreferenceScreen preferenceScreen;
        Preference A0S;
        String str = this.A0J;
        if (!TextUtils.isEmpty(str)) {
            AnonymousClass047 r0 = this.A0F;
            if (r0 == null || (preferenceScreen = r0.A07) == null || (A0S = preferenceScreen.A0S(str)) == null) {
                StringBuilder sb = new StringBuilder("Dependency \"");
                sb.append(str);
                sb.append("\" not found for preference \"");
                sb.append(this.A0L);
                sb.append("\" (title: \"");
                sb.append((Object) this.A0H);
                sb.append("\"");
                throw new IllegalStateException(sb.toString());
            }
            List list = A0S.A0M;
            if (list == null) {
                list = new ArrayList();
                A0S.A0M = list;
            }
            list.add(this);
            boolean A0O = A0S.A0O();
            if (this.A0R == A0O) {
                this.A0R = !A0O;
                A0L(A0O());
                A04();
            }
        }
    }

    public void A08() {
        A09();
    }

    public final void A09() {
        AnonymousClass047 r0;
        PreferenceScreen preferenceScreen;
        Preference A0S;
        List list;
        String str = this.A0J;
        if (str != null && (r0 = this.A0F) != null && (preferenceScreen = r0.A07) != null && (A0S = preferenceScreen.A0S(str)) != null && (list = A0S.A0M) != null) {
            list.remove(this);
        }
    }

    public void A0A(int i) {
        String string = this.A05.getString(i);
        CharSequence charSequence = this.A0H;
        if (string == null) {
            if (charSequence == null) {
                return;
            }
        } else if (string.equals(charSequence)) {
            return;
        }
        this.A0H = string;
        A04();
    }

    public void A0B(Bundle bundle) {
        Parcelable parcelable;
        if ((!TextUtils.isEmpty(this.A0L)) && (parcelable = bundle.getParcelable(this.A0L)) != null) {
            this.A0P = false;
            A0D(parcelable);
            if (!this.A0P) {
                throw new IllegalStateException("Derived class did not call super.onRestoreInstanceState()");
            }
        }
    }

    public void A0C(Bundle bundle) {
        if (!TextUtils.isEmpty(this.A0L)) {
            this.A0P = false;
            Parcelable A01 = A01();
            if (!this.A0P) {
                throw new IllegalStateException("Derived class did not call super.onSaveInstanceState()");
            } else if (A01 != null) {
                bundle.putParcelable(this.A0L, A01);
            }
        }
    }

    public void A0D(Parcelable parcelable) {
        this.A0P = true;
        if (parcelable != AbsSavedState.EMPTY_STATE && parcelable != null) {
            throw new IllegalArgumentException("Wrong state class -- expecting Preference State");
        }
    }

    public void A0E(View view) {
        Intent intent;
        AbstractC005302m r0;
        if (A0N() && this.A0Z) {
            A07();
            AbstractC11830gw r02 = this.A0B;
            if (r02 != null) {
                r02.AU5(this);
                return;
            }
            AnonymousClass047 r03 = this.A0F;
            if ((r03 == null || (r0 = r03.A06) == null || !r0.AU6(this)) && (intent = this.A06) != null) {
                this.A05.startActivity(intent);
            }
        }
    }

    public final void A0F(View view, boolean z) {
        view.setEnabled(z);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            while (true) {
                childCount--;
                if (childCount >= 0) {
                    A0F(viewGroup.getChildAt(childCount), z);
                } else {
                    return;
                }
            }
        }
    }

    public void A0H(AnonymousClass047 r5) {
        Object obj;
        SharedPreferences sharedPreferences;
        long j;
        this.A0F = r5;
        if (!this.A0T) {
            synchronized (r5) {
                j = r5.A00;
                r5.A00 = 1 + j;
            }
            this.A04 = j;
        }
        if (A0P()) {
            AnonymousClass047 r0 = this.A0F;
            if (r0 != null) {
                sharedPreferences = r0.A02();
            } else {
                sharedPreferences = null;
            }
            if (sharedPreferences.contains(this.A0L)) {
                obj = null;
                A0J(obj);
            }
        }
        obj = this.A0I;
        if (obj == null) {
            return;
        }
        A0J(obj);
    }

    public void A0I(CharSequence charSequence) {
        if (this.A0D != null) {
            throw new IllegalStateException("Preference already has a SummaryProvider set.");
        } else if (!TextUtils.equals(this.A0G, charSequence)) {
            this.A0G = charSequence;
            A04();
        }
    }

    public void A0K(String str) {
        if (A0P()) {
            String str2 = null;
            if (A0P()) {
                str2 = this.A0F.A02().getString(this.A0L, null);
            }
            if (!TextUtils.equals(str, str2)) {
                SharedPreferences.Editor A01 = this.A0F.A01();
                A01.putString(this.A0L, str);
                if (!this.A0F.A09) {
                    A01.apply();
                }
            }
        }
    }

    public void A0L(boolean z) {
        List list = this.A0M;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Preference preference = (Preference) list.get(i);
                if (preference.A0R == z) {
                    preference.A0R = !z;
                    preference.A0L(preference.A0O());
                    preference.A04();
                }
            }
        }
    }

    public void A0M(boolean z) {
        if (this.A0S != z) {
            this.A0S = z;
            A0L(A0O());
            A04();
        }
    }

    public boolean A0N() {
        return this.A0S && this.A0R && this.A0W;
    }

    public boolean A0O() {
        return !A0N();
    }

    public boolean A0P() {
        return this.A0F != null && this.A0X && (TextUtils.isEmpty(this.A0L) ^ true);
    }

    public boolean A0Q(Object obj) {
        AbstractC11820gv r0 = this.A0A;
        return r0 == null || r0.AU4(this, obj);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008a, code lost:
        if (r0 != null) goto L_0x008c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0R(X.AnonymousClass0FF r9) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.Preference.A0R(X.0FF):void");
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        Preference preference = (Preference) obj;
        int i = this.A02;
        int i2 = preference.A02;
        if (i != i2) {
            return i - i2;
        }
        CharSequence charSequence = this.A0H;
        CharSequence charSequence2 = preference.A0H;
        if (charSequence == charSequence2) {
            return 0;
        }
        if (charSequence == null) {
            return 1;
        }
        if (charSequence2 == null) {
            return -1;
        }
        return charSequence.toString().compareToIgnoreCase(preference.A0H.toString());
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        CharSequence charSequence = this.A0H;
        if (!TextUtils.isEmpty(charSequence)) {
            sb.append(charSequence);
            sb.append(' ');
        }
        CharSequence A02 = A02();
        if (!TextUtils.isEmpty(A02)) {
            sb.append(A02);
            sb.append(' ');
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }
}
