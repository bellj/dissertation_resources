package androidx.preference;

import X.AnonymousClass0FF;
import X.AnonymousClass0X1;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Checkable;
import android.widget.Switch;

/* loaded from: classes.dex */
public class SwitchPreference extends TwoStatePreference {
    public CharSequence A00;
    public CharSequence A01;
    public final AnonymousClass0X1 A02 = new AnonymousClass0X1(this);

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SwitchPreference(android.content.Context r5, android.util.AttributeSet r6) {
        /*
            r4 = this;
            r1 = 2130969558(0x7f0403d6, float:1.7547801E38)
            r0 = 16843629(0x101036d, float:2.3696016E-38)
            int r2 = X.AnonymousClass06r.A00(r5, r1, r0)
            r1 = 0
            r4.<init>(r5, r6, r2, r1)
            X.0X1 r0 = new X.0X1
            r0.<init>(r4)
            r4.A02 = r0
            int[] r0 = X.AnonymousClass0MO.A0B
            android.content.res.TypedArray r3 = r5.obtainStyledAttributes(r6, r0, r2, r1)
            r0 = 7
            java.lang.String r0 = r3.getString(r0)
            if (r0 != 0) goto L_0x0026
            java.lang.String r0 = r3.getString(r1)
        L_0x0026:
            r4.A01 = r0
            boolean r0 = r4.A02
            if (r0 == 0) goto L_0x002f
            r4.A04()
        L_0x002f:
            r0 = 6
            r1 = 1
            java.lang.String r0 = r3.getString(r0)
            if (r0 != 0) goto L_0x003b
            java.lang.String r0 = r3.getString(r1)
        L_0x003b:
            r4.A00 = r0
            boolean r0 = r4.A02
            if (r0 != 0) goto L_0x0044
            r4.A04()
        L_0x0044:
            r0 = 9
            r1 = 3
            java.lang.String r0 = r3.getString(r0)
            if (r0 != 0) goto L_0x0051
            java.lang.String r0 = r3.getString(r1)
        L_0x0051:
            r4.A01 = r0
            r4.A04()
            r0 = 8
            r1 = 4
            java.lang.String r0 = r3.getString(r0)
            if (r0 != 0) goto L_0x0063
            java.lang.String r0 = r3.getString(r1)
        L_0x0063:
            r4.A00 = r0
            r4.A04()
            r2 = 5
            r1 = 2
            r0 = 0
            boolean r0 = r3.getBoolean(r1, r0)
            boolean r0 = r3.getBoolean(r2, r0)
            r4.A04 = r0
            r3.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.SwitchPreference.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    @Override // androidx.preference.Preference
    public void A0E(View view) {
        super.A0E(view);
        if (((AccessibilityManager) this.A05.getSystemService("accessibility")).isEnabled()) {
            A0U(view.findViewById(16908352));
            A0S(view.findViewById(16908304));
        }
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r2) {
        super.A0R(r2);
        A0U(r2.A08(16908352));
        A0S(r2.A08(16908304));
    }

    public final void A0U(View view) {
        boolean z = view instanceof Switch;
        if (z) {
            ((Switch) view).setOnCheckedChangeListener(null);
        }
        if (view instanceof Checkable) {
            ((Checkable) view).setChecked(((TwoStatePreference) this).A02);
        }
        if (z) {
            Switch r4 = (Switch) view;
            r4.setTextOn(this.A01);
            r4.setTextOff(this.A00);
            r4.setOnCheckedChangeListener(this.A02);
        }
    }
}
