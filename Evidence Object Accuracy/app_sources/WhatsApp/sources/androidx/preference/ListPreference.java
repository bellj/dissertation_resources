package androidx.preference;

import X.AbstractC11840gx;
import X.AnonymousClass06r;
import X.AnonymousClass0MO;
import X.AnonymousClass0Yy;
import X.C02810Eb;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.AbsSavedState;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class ListPreference extends DialogPreference {
    public String A00;
    public String A01;
    public boolean A02;
    public CharSequence[] A03;
    public CharSequence[] A04;

    public ListPreference(Context context) {
        this(context, null);
    }

    public ListPreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, AnonymousClass06r.A00(context, R.attr.dialogPreferenceStyle, 16842897));
    }

    public ListPreference(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public ListPreference(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MO.A04, i, i2);
        CharSequence[] textArray = obtainStyledAttributes.getTextArray(2);
        this.A03 = textArray == null ? obtainStyledAttributes.getTextArray(0) : textArray;
        CharSequence[] textArray2 = obtainStyledAttributes.getTextArray(3);
        this.A04 = textArray2 == null ? obtainStyledAttributes.getTextArray(1) : textArray2;
        if (obtainStyledAttributes.getBoolean(4, obtainStyledAttributes.getBoolean(4, false))) {
            AnonymousClass0Yy r0 = AnonymousClass0Yy.A00;
            if (r0 == null) {
                r0 = new AnonymousClass0Yy();
                AnonymousClass0Yy.A00 = r0;
            }
            this.A0D = r0;
            A04();
        }
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, AnonymousClass0MO.A06, i, i2);
        String string = obtainStyledAttributes2.getString(33);
        this.A00 = string == null ? obtainStyledAttributes2.getString(7) : string;
        obtainStyledAttributes2.recycle();
    }

    @Override // androidx.preference.Preference
    public Parcelable A01() {
        this.A0P = true;
        AbsSavedState absSavedState = AbsSavedState.EMPTY_STATE;
        if (this.A0X) {
            return absSavedState;
        }
        C02810Eb r1 = new C02810Eb(absSavedState);
        r1.A00 = this.A01;
        return r1;
    }

    @Override // androidx.preference.Preference
    public CharSequence A02() {
        AbstractC11840gx r0 = this.A0D;
        if (r0 != null) {
            return r0.AZc(this);
        }
        CharSequence A0T = A0T();
        CharSequence A02 = super.A02();
        String str = this.A00;
        if (str != null) {
            Object[] objArr = new Object[1];
            if (A0T == null) {
                A0T = "";
            }
            objArr[0] = A0T;
            String format = String.format(str, objArr);
            if (!TextUtils.equals(format, A02)) {
                Log.w("ListPreference", "Setting a summary with a String formatting marker is no longer supported. You should use a SummaryProvider instead.");
                return format;
            }
        }
        return A02;
    }

    @Override // androidx.preference.Preference
    public Object A03(TypedArray typedArray, int i) {
        return typedArray.getString(i);
    }

    @Override // androidx.preference.Preference
    public void A0D(Parcelable parcelable) {
        if (parcelable == null || !parcelable.getClass().equals(C02810Eb.class)) {
            super.A0D(parcelable);
            return;
        }
        C02810Eb r3 = (C02810Eb) parcelable;
        super.A0D(r3.getSuperState());
        A0U(r3.A00);
    }

    @Override // androidx.preference.Preference
    public void A0I(CharSequence charSequence) {
        String charSequence2;
        super.A0I(charSequence);
        String str = this.A00;
        if (charSequence == null) {
            if (str != null) {
                charSequence2 = null;
            } else {
                return;
            }
        } else if (!charSequence.equals(str)) {
            charSequence2 = charSequence.toString();
        } else {
            return;
        }
        this.A00 = charSequence2;
    }

    @Override // androidx.preference.Preference
    public void A0J(Object obj) {
        String str = (String) obj;
        if (A0P()) {
            str = this.A0F.A02().getString(this.A0L, str);
        }
        A0U(str);
    }

    public int A0S(String str) {
        CharSequence[] charSequenceArr;
        if (str == null || (charSequenceArr = this.A04) == null) {
            return -1;
        }
        int length = charSequenceArr.length;
        do {
            length--;
            if (length < 0) {
                return -1;
            }
        } while (!this.A04[length].equals(str));
        return length;
    }

    public CharSequence A0T() {
        CharSequence[] charSequenceArr;
        int A0S = A0S(this.A01);
        if (A0S < 0 || (charSequenceArr = this.A03) == null) {
            return null;
        }
        return charSequenceArr[A0S];
    }

    public void A0U(String str) {
        boolean z = !TextUtils.equals(this.A01, str);
        if (z || !this.A02) {
            this.A01 = str;
            this.A02 = true;
            A0K(str);
            if (z) {
                A04();
            }
        }
    }

    public void A0V(CharSequence[] charSequenceArr) {
        this.A03 = charSequenceArr;
    }
}
