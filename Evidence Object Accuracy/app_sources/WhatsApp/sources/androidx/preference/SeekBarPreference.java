package androidx.preference;

import X.AnonymousClass0FF;
import X.AnonymousClass0MO;
import X.AnonymousClass0WP;
import X.AnonymousClass0X8;
import X.C02820Ef;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.AbsSavedState;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class SeekBarPreference extends Preference {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public View.OnKeyListener A04 = new AnonymousClass0WP(this);
    public SeekBar.OnSeekBarChangeListener A05 = new AnonymousClass0X8(this);
    public SeekBar A06;
    public TextView A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;

    public SeekBarPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, R.attr.seekBarPreferenceStyle, 0);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MO.A0A, R.attr.seekBarPreferenceStyle, 0);
        this.A01 = obtainStyledAttributes.getInt(3, 0);
        int i = obtainStyledAttributes.getInt(1, 100);
        int i2 = this.A01;
        i = i < i2 ? i2 : i;
        if (i != this.A00) {
            this.A00 = i;
            A04();
        }
        int i3 = obtainStyledAttributes.getInt(4, 0);
        if (i3 != this.A02) {
            this.A02 = Math.min(this.A00 - this.A01, Math.abs(i3));
            A04();
        }
        this.A08 = obtainStyledAttributes.getBoolean(2, true);
        this.A09 = obtainStyledAttributes.getBoolean(5, false);
        this.A0B = obtainStyledAttributes.getBoolean(6, false);
        obtainStyledAttributes.recycle();
    }

    @Override // androidx.preference.Preference
    public Parcelable A01() {
        this.A0P = true;
        AbsSavedState absSavedState = AbsSavedState.EMPTY_STATE;
        if (this.A0X) {
            return absSavedState;
        }
        C02820Ef r1 = new C02820Ef(absSavedState);
        r1.A02 = this.A03;
        r1.A01 = this.A01;
        r1.A00 = this.A00;
        return r1;
    }

    @Override // androidx.preference.Preference
    public Object A03(TypedArray typedArray, int i) {
        return Integer.valueOf(typedArray.getInt(i, 0));
    }

    @Override // androidx.preference.Preference
    public void A0D(Parcelable parcelable) {
        if (!parcelable.getClass().equals(C02820Ef.class)) {
            super.A0D(parcelable);
            return;
        }
        C02820Ef r3 = (C02820Ef) parcelable;
        super.A0D(r3.getSuperState());
        this.A03 = r3.A02;
        this.A01 = r3.A01;
        this.A00 = r3.A00;
        A04();
    }

    @Override // androidx.preference.Preference
    public void A0J(Object obj) {
        if (obj == null) {
            obj = 0;
        }
        int intValue = ((Number) obj).intValue();
        if (A0P()) {
            intValue = this.A0F.A02().getInt(this.A0L, intValue);
        }
        A0S(intValue, true);
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r4) {
        super.A0R(r4);
        r4.A0H.setOnKeyListener(this.A04);
        this.A06 = (SeekBar) r4.A08(R.id.seekbar);
        TextView textView = (TextView) r4.A08(R.id.seekbar_value);
        this.A07 = textView;
        if (this.A09) {
            textView.setVisibility(0);
        } else {
            textView.setVisibility(8);
            this.A07 = null;
        }
        SeekBar seekBar = this.A06;
        if (seekBar == null) {
            Log.e("SeekBarPreference", "SeekBar view is null in onBindViewHolder.");
            return;
        }
        seekBar.setOnSeekBarChangeListener(this.A05);
        this.A06.setMax(this.A00 - this.A01);
        int i = this.A02;
        SeekBar seekBar2 = this.A06;
        if (i != 0) {
            seekBar2.setKeyProgressIncrement(i);
        } else {
            this.A02 = seekBar2.getKeyProgressIncrement();
        }
        this.A06.setProgress(this.A03 - this.A01);
        int i2 = this.A03;
        TextView textView2 = this.A07;
        if (textView2 != null) {
            textView2.setText(String.valueOf(i2));
        }
        this.A06.setEnabled(A0N());
    }

    public final void A0S(int i, boolean z) {
        int i2 = this.A01;
        if (i < i2) {
            i = i2;
        }
        int i3 = this.A00;
        if (i > i3) {
            i = i3;
        }
        if (i != this.A03) {
            this.A03 = i;
            TextView textView = this.A07;
            if (textView != null) {
                textView.setText(String.valueOf(i));
            }
            if (A0P()) {
                int i4 = i ^ -1;
                if (A0P()) {
                    i4 = this.A0F.A02().getInt(this.A0L, i4);
                }
                if (i != i4) {
                    SharedPreferences.Editor A01 = this.A0F.A01();
                    A01.putInt(this.A0L, i);
                    if (!this.A0F.A09) {
                        A01.apply();
                    }
                }
            }
            if (z) {
                A04();
            }
        }
    }

    public void A0T(SeekBar seekBar) {
        int progress = this.A01 + seekBar.getProgress();
        if (progress == this.A03) {
            return;
        }
        if (A0Q(Integer.valueOf(progress))) {
            A0S(progress, false);
            return;
        }
        seekBar.setProgress(this.A03 - this.A01);
        int i = this.A03;
        TextView textView = this.A07;
        if (textView != null) {
            textView.setText(String.valueOf(i));
        }
    }
}
