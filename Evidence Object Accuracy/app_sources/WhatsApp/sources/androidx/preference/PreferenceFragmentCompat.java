package androidx.preference;

import X.AbstractC005202l;
import X.AbstractC005302m;
import X.AbstractC005402n;
import X.AbstractC005502o;
import X.AnonymousClass01E;
import X.AnonymousClass01F;
import X.AnonymousClass047;
import X.AnonymousClass0AS;
import X.AnonymousClass0F1;
import X.AnonymousClass0F3;
import X.AnonymousClass0FG;
import X.AnonymousClass0MO;
import X.C004902f;
import X.C010605f;
import X.RunnableC09250cb;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* loaded from: classes.dex */
public abstract class PreferenceFragmentCompat extends AnonymousClass01E implements AbstractC005202l, AbstractC005302m, AbstractC005402n, AbstractC005502o {
    public int A00 = R.layout.preference_list_fragment;
    public Handler A01 = new AnonymousClass0AS(this);
    public AnonymousClass047 A02;
    public RecyclerView A03;
    public boolean A04;
    public boolean A05;
    public final AnonymousClass0F3 A06 = new AnonymousClass0F3(this);
    public final Runnable A07 = new RunnableC09250cb(this);

    public abstract void A18(String str, Bundle bundle);

    @Override // X.AnonymousClass01E
    public void A0s() {
        this.A0V = true;
        AnonymousClass047 r0 = this.A02;
        r0.A06 = this;
        r0.A04 = this;
    }

    @Override // X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        PreferenceScreen preferenceScreen = this.A02.A07;
        if (preferenceScreen != null) {
            Bundle bundle2 = new Bundle();
            preferenceScreen.A0C(bundle2);
            bundle.putBundle("android:preferences", bundle2);
        }
    }

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        RecyclerView recyclerView;
        int i;
        TypedArray obtainStyledAttributes = A0p().obtainStyledAttributes(null, AnonymousClass0MO.A07, R.attr.preferenceFragmentCompatStyle, 0);
        this.A00 = obtainStyledAttributes.getResourceId(0, this.A00);
        Drawable drawable = obtainStyledAttributes.getDrawable(1);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(2, -1);
        boolean z = obtainStyledAttributes.getBoolean(3, true);
        obtainStyledAttributes.recycle();
        LayoutInflater cloneInContext = layoutInflater.cloneInContext(A0p());
        View inflate = cloneInContext.inflate(this.A00, viewGroup, false);
        View findViewById = inflate.findViewById(16908351);
        if (findViewById instanceof ViewGroup) {
            ViewGroup viewGroup2 = (ViewGroup) findViewById;
            if (!A0p().getPackageManager().hasSystemFeature("android.hardware.type.automotive") || (recyclerView = (RecyclerView) viewGroup2.findViewById(R.id.recycler_view)) == null) {
                recyclerView = (RecyclerView) cloneInContext.inflate(R.layout.preference_recyclerview, viewGroup2, false);
                A0p();
                recyclerView.setLayoutManager(new LinearLayoutManager(1));
                recyclerView.setAccessibilityDelegateCompat(new AnonymousClass0FG(recyclerView));
            }
            this.A03 = recyclerView;
            AnonymousClass0F3 r7 = this.A06;
            recyclerView.A0l(r7);
            if (drawable != null) {
                i = drawable.getIntrinsicHeight();
            } else {
                i = 0;
            }
            r7.A00 = i;
            r7.A01 = drawable;
            PreferenceFragmentCompat preferenceFragmentCompat = r7.A03;
            preferenceFragmentCompat.A03.A0M();
            if (dimensionPixelSize != -1) {
                r7.A00 = dimensionPixelSize;
                preferenceFragmentCompat.A03.A0M();
            }
            r7.A02 = z;
            if (this.A03.getParent() == null) {
                viewGroup2.addView(this.A03);
            }
            this.A01.post(this.A07);
            return inflate;
        }
        throw new IllegalStateException("Content has view with id attribute 'android.R.id.list_container' that is not a ViewGroup class");
    }

    @Override // X.AnonymousClass01E
    public void A12() {
        Handler handler = this.A01;
        handler.removeCallbacks(this.A07);
        handler.removeMessages(1);
        if (this.A04) {
            this.A03.setAdapter(null);
            PreferenceScreen preferenceScreen = this.A02.A07;
            if (preferenceScreen != null) {
                preferenceScreen.A08();
            }
        }
        this.A03 = null;
        this.A0V = true;
    }

    @Override // X.AnonymousClass01E
    public void A14() {
        this.A0V = true;
        AnonymousClass047 r1 = this.A02;
        r1.A06 = null;
        r1.A04 = null;
    }

    @Override // X.AnonymousClass01E
    public void A16(Bundle bundle) {
        String str;
        super.A16(bundle);
        TypedValue typedValue = new TypedValue();
        A0B().getTheme().resolveAttribute(R.attr.preferenceTheme, typedValue, true);
        int i = typedValue.resourceId;
        if (i == 0) {
            i = R.style.PreferenceThemeOverlay;
        }
        A0B().getTheme().applyStyle(i, false);
        AnonymousClass047 r0 = new AnonymousClass047(A0p());
        this.A02 = r0;
        r0.A05 = this;
        Bundle bundle2 = super.A05;
        if (bundle2 != null) {
            str = bundle2.getString("androidx.preference.PreferenceFragmentCompat.PREFERENCE_ROOT");
        } else {
            str = null;
        }
        A18(str, bundle);
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        PreferenceScreen preferenceScreen;
        Bundle bundle2;
        PreferenceScreen preferenceScreen2;
        if (!(bundle == null || (bundle2 = bundle.getBundle("android:preferences")) == null || (preferenceScreen2 = this.A02.A07) == null)) {
            preferenceScreen2.A0B(bundle2);
        }
        if (this.A04 && (preferenceScreen = this.A02.A07) != null) {
            this.A03.setAdapter(new AnonymousClass0F1(preferenceScreen));
            preferenceScreen.A06();
        }
        this.A05 = true;
    }

    @Override // X.AbstractC005202l
    public Preference A9x(CharSequence charSequence) {
        PreferenceScreen preferenceScreen;
        AnonymousClass047 r0 = this.A02;
        if (r0 == null || (preferenceScreen = r0.A07) == null) {
            return null;
        }
        return preferenceScreen.A0S(charSequence);
    }

    @Override // X.AbstractC005302m
    public boolean AU6(Preference preference) {
        String str = preference.A0K;
        if (str == null) {
            return false;
        }
        Log.w("PreferenceFragment", "onPreferenceStartFragment is not implemented in the parent activity - attempting to use a fallback implementation. You should implement this method so that you can configure the new fragment that will be displayed, and set a transition between the fragments.");
        AnonymousClass01F r4 = A0C().A03.A00.A03;
        Bundle bundle = preference.A08;
        if (bundle == null) {
            bundle = new Bundle();
            preference.A08 = bundle;
        }
        C010605f A0B = r4.A0B();
        A0C().getClassLoader();
        AnonymousClass01E A00 = A0B.A00(str);
        A00.A0U(bundle);
        A00.A0X(this, 0);
        C004902f r1 = new C004902f(r4);
        r1.A07(A00, ((View) this.A0A.getParent()).getId());
        r1.A0F(null);
        r1.A00(false);
        return true;
    }
}
