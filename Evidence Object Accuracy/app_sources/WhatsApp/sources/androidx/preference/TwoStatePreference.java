package androidx.preference;

import X.AnonymousClass0Ee;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.AbsSavedState;

/* loaded from: classes.dex */
public abstract class TwoStatePreference extends Preference {
    public CharSequence A00;
    public CharSequence A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;

    public TwoStatePreference(Context context) {
        this(context, null);
    }

    public TwoStatePreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TwoStatePreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, 0);
    }

    public TwoStatePreference(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @Override // androidx.preference.Preference
    public Parcelable A01() {
        this.A0P = true;
        AbsSavedState absSavedState = AbsSavedState.EMPTY_STATE;
        if (this.A0X) {
            return absSavedState;
        }
        AnonymousClass0Ee r1 = new AnonymousClass0Ee(absSavedState);
        r1.A00 = this.A02;
        return r1;
    }

    @Override // androidx.preference.Preference
    public Object A03(TypedArray typedArray, int i) {
        return Boolean.valueOf(typedArray.getBoolean(i, false));
    }

    @Override // androidx.preference.Preference
    public void A07() {
        boolean z = !this.A02;
        if (A0Q(Boolean.valueOf(z))) {
            A0T(z);
        }
    }

    @Override // androidx.preference.Preference
    public void A0D(Parcelable parcelable) {
        if (parcelable == null || !parcelable.getClass().equals(AnonymousClass0Ee.class)) {
            super.A0D(parcelable);
            return;
        }
        AnonymousClass0Ee r3 = (AnonymousClass0Ee) parcelable;
        super.A0D(r3.getSuperState());
        A0T(r3.A00);
    }

    @Override // androidx.preference.Preference
    public void A0J(Object obj) {
        if (obj == null) {
            obj = Boolean.FALSE;
        }
        boolean booleanValue = ((Boolean) obj).booleanValue();
        if (A0P()) {
            booleanValue = this.A0F.A02().getBoolean(this.A0L, booleanValue);
        }
        A0T(booleanValue);
    }

    @Override // androidx.preference.Preference
    public boolean A0O() {
        boolean z = this.A04;
        boolean z2 = this.A02;
        if (z) {
            if (z2) {
                return true;
            }
        } else if (!z2) {
            return true;
        }
        if (!(!A0N())) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0S(android.view.View r4) {
        /*
            r3 = this;
            boolean r0 = r4 instanceof android.widget.TextView
            if (r0 == 0) goto L_0x0021
            android.widget.TextView r4 = (android.widget.TextView) r4
            boolean r0 = r3.A02
            r2 = 0
            if (r0 == 0) goto L_0x0022
            java.lang.CharSequence r0 = r3.A01
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0022
            java.lang.CharSequence r0 = r3.A01
        L_0x0015:
            r4.setText(r0)
        L_0x0018:
            int r0 = r4.getVisibility()
            if (r2 == r0) goto L_0x0021
            r4.setVisibility(r2)
        L_0x0021:
            return
        L_0x0022:
            boolean r0 = r3.A02
            if (r0 != 0) goto L_0x0031
            java.lang.CharSequence r0 = r3.A00
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0031
            java.lang.CharSequence r0 = r3.A00
            goto L_0x0015
        L_0x0031:
            java.lang.CharSequence r1 = r3.A02()
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x003f
            r4.setText(r1)
            goto L_0x0018
        L_0x003f:
            r2 = 8
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.TwoStatePreference.A0S(android.view.View):void");
    }

    public void A0T(boolean z) {
        boolean z2 = true;
        if (this.A02 == z) {
            z2 = false;
            if (this.A03) {
                return;
            }
        }
        this.A02 = z;
        this.A03 = true;
        if (A0P()) {
            boolean z3 = !z;
            if (A0P()) {
                z3 = this.A0F.A02().getBoolean(this.A0L, z3);
            }
            if (z != z3) {
                SharedPreferences.Editor A01 = this.A0F.A01();
                A01.putBoolean(this.A0L, z);
                if (!this.A0F.A09) {
                    A01.apply();
                }
            }
        }
        if (z2) {
            A0L(A0O());
            A04();
        }
    }
}
