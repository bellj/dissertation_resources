package androidx.preference;

import X.AnonymousClass0FF;
import X.AnonymousClass0MO;
import X.AnonymousClass0X2;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Checkable;
import android.widget.CompoundButton;
import androidx.appcompat.widget.SwitchCompat;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class SwitchPreferenceCompat extends TwoStatePreference {
    public CharSequence A00;
    public CharSequence A01;
    public final AnonymousClass0X2 A02 = new AnonymousClass0X2(this);

    public SwitchPreferenceCompat(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, R.attr.switchPreferenceCompatStyle, 0);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MO.A0C, R.attr.switchPreferenceCompatStyle, 0);
        String string = obtainStyledAttributes.getString(7);
        ((TwoStatePreference) this).A01 = string == null ? obtainStyledAttributes.getString(0) : string;
        if (((TwoStatePreference) this).A02) {
            A04();
        }
        String string2 = obtainStyledAttributes.getString(6);
        ((TwoStatePreference) this).A00 = string2 == null ? obtainStyledAttributes.getString(1) : string2;
        if (!((TwoStatePreference) this).A02) {
            A04();
        }
        String string3 = obtainStyledAttributes.getString(9);
        this.A01 = string3 == null ? obtainStyledAttributes.getString(3) : string3;
        A04();
        String string4 = obtainStyledAttributes.getString(8);
        this.A00 = string4 == null ? obtainStyledAttributes.getString(4) : string4;
        A04();
        ((TwoStatePreference) this).A04 = obtainStyledAttributes.getBoolean(5, obtainStyledAttributes.getBoolean(2, false));
        obtainStyledAttributes.recycle();
    }

    @Override // androidx.preference.Preference
    public void A0E(View view) {
        super.A0E(view);
        if (((AccessibilityManager) this.A05.getSystemService("accessibility")).isEnabled()) {
            A0U(view.findViewById(R.id.switchWidget));
            A0S(view.findViewById(16908304));
        }
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r2) {
        super.A0R(r2);
        A0U(r2.A08(R.id.switchWidget));
        A0S(r2.A08(16908304));
    }

    public final void A0U(View view) {
        boolean z = view instanceof SwitchCompat;
        if (z) {
            ((CompoundButton) view).setOnCheckedChangeListener(null);
        }
        if (view instanceof Checkable) {
            ((Checkable) view).setChecked(((TwoStatePreference) this).A02);
        }
        if (z) {
            SwitchCompat switchCompat = (SwitchCompat) view;
            switchCompat.setTextOn(this.A01);
            switchCompat.setTextOff(this.A00);
            switchCompat.setOnCheckedChangeListener(this.A02);
        }
    }
}
