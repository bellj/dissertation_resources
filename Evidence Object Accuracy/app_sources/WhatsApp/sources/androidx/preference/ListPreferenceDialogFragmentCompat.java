package androidx.preference;

import X.AnonymousClass0V6;
import X.C004802e;
import android.os.Bundle;

/* loaded from: classes.dex */
public class ListPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {
    public int A00;
    public CharSequence[] A01;
    public CharSequence[] A02;

    public static ListPreferenceDialogFragmentCompat A00(String str) {
        ListPreferenceDialogFragmentCompat listPreferenceDialogFragmentCompat = new ListPreferenceDialogFragmentCompat();
        Bundle bundle = new Bundle(1);
        bundle.putString("key", str);
        listPreferenceDialogFragmentCompat.A0U(bundle);
        return listPreferenceDialogFragmentCompat;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        bundle.putInt("ListPreferenceDialogFragment.index", this.A00);
        bundle.putCharSequenceArray("ListPreferenceDialogFragment.entries", this.A01);
        bundle.putCharSequenceArray("ListPreferenceDialogFragment.entryValues", this.A02);
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        CharSequence[] charSequenceArray;
        super.A16(bundle);
        if (bundle == null) {
            ListPreference listPreference = (ListPreference) A1I();
            if (listPreference.A03 == null || listPreference.A04 == null) {
                throw new IllegalStateException("ListPreference requires an entries array and an entryValues array.");
            }
            this.A00 = listPreference.A0S(listPreference.A01);
            this.A01 = listPreference.A03;
            charSequenceArray = listPreference.A04;
        } else {
            this.A00 = bundle.getInt("ListPreferenceDialogFragment.index", 0);
            this.A01 = bundle.getCharSequenceArray("ListPreferenceDialogFragment.entries");
            charSequenceArray = bundle.getCharSequenceArray("ListPreferenceDialogFragment.entryValues");
        }
        this.A02 = charSequenceArray;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public void A1K(C004802e r4) {
        r4.A09(new AnonymousClass0V6(this), this.A01, this.A00);
        r4.A03(null, null);
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public void A1L(boolean z) {
        int i;
        if (z && (i = this.A00) >= 0) {
            String charSequence = this.A02[i].toString();
            ListPreference listPreference = (ListPreference) A1I();
            if (listPreference.A0Q(charSequence)) {
                listPreference.A0U(charSequence);
            }
        }
    }
}
