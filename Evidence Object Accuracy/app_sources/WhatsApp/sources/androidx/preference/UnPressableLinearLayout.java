package androidx.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/* loaded from: classes.dex */
public class UnPressableLinearLayout extends LinearLayout {
    @Override // android.view.View, android.view.ViewGroup
    public void dispatchSetPressed(boolean z) {
    }

    public UnPressableLinearLayout(Context context) {
        super(context, null);
    }

    public UnPressableLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
