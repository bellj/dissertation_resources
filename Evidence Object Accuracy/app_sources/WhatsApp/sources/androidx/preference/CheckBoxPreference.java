package androidx.preference;

import X.AnonymousClass06r;
import X.AnonymousClass0FF;
import X.AnonymousClass0MO;
import X.AnonymousClass0X0;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Checkable;
import android.widget.CompoundButton;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class CheckBoxPreference extends TwoStatePreference {
    public final AnonymousClass0X0 A00;

    public CheckBoxPreference(Context context) {
        this(context, null);
    }

    public CheckBoxPreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, AnonymousClass06r.A00(context, R.attr.checkBoxPreferenceStyle, 16842895));
    }

    public CheckBoxPreference(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public CheckBoxPreference(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.A00 = new AnonymousClass0X0(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MO.A01, i, i2);
        String string = obtainStyledAttributes.getString(5);
        ((TwoStatePreference) this).A01 = string == null ? obtainStyledAttributes.getString(0) : string;
        if (((TwoStatePreference) this).A02) {
            A04();
        }
        String string2 = obtainStyledAttributes.getString(4);
        ((TwoStatePreference) this).A00 = string2 == null ? obtainStyledAttributes.getString(1) : string2;
        if (!((TwoStatePreference) this).A02) {
            A04();
        }
        ((TwoStatePreference) this).A04 = obtainStyledAttributes.getBoolean(3, obtainStyledAttributes.getBoolean(2, false));
        obtainStyledAttributes.recycle();
    }

    @Override // androidx.preference.Preference
    public void A0E(View view) {
        super.A0E(view);
        if (((AccessibilityManager) this.A05.getSystemService("accessibility")).isEnabled()) {
            A0U(view.findViewById(16908289));
            A0S(view.findViewById(16908304));
        }
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r2) {
        super.A0R(r2);
        A0U(r2.A08(16908289));
        A0S(r2.A08(16908304));
    }

    public final void A0U(View view) {
        boolean z = view instanceof CompoundButton;
        if (z) {
            ((CompoundButton) view).setOnCheckedChangeListener(null);
        }
        if (view instanceof Checkable) {
            ((Checkable) view).setChecked(((TwoStatePreference) this).A02);
        }
        if (z) {
            ((CompoundButton) view).setOnCheckedChangeListener(this.A00);
        }
    }
}
