package androidx.preference;

import X.AnonymousClass06r;
import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.R;

/* loaded from: classes.dex */
public final class PreferenceScreen extends PreferenceGroup {
    @Override // androidx.preference.PreferenceGroup
    public boolean A0W() {
        return false;
    }

    public PreferenceScreen(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, AnonymousClass06r.A00(context, R.attr.preferenceScreenStyle, 16842891));
    }

    @Override // androidx.preference.Preference
    public void A07() {
        if (((Preference) this).A06 == null && this.A0K == null) {
            ((PreferenceGroup) this).A02.size();
        }
    }
}
