package androidx.preference;

import X.AbstractC005402n;
import X.AnonymousClass01E;
import X.AnonymousClass06r;
import X.AnonymousClass0MO;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.R;

/* loaded from: classes.dex */
public abstract class DialogPreference extends Preference {
    public int A00;
    public Drawable A01;
    public CharSequence A02;
    public CharSequence A03;
    public CharSequence A04;
    public CharSequence A05;

    public DialogPreference(Context context) {
        this(context, null);
    }

    public DialogPreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, AnonymousClass06r.A00(context, R.attr.dialogPreferenceStyle, 16842897));
    }

    public DialogPreference(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public DialogPreference(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MO.A02, i, i2);
        String string = obtainStyledAttributes.getString(9);
        string = string == null ? obtainStyledAttributes.getString(0) : string;
        this.A03 = string;
        if (string == null) {
            this.A03 = this.A0H;
        }
        String string2 = obtainStyledAttributes.getString(8);
        this.A02 = string2 == null ? obtainStyledAttributes.getString(1) : string2;
        Drawable drawable = obtainStyledAttributes.getDrawable(6);
        this.A01 = drawable == null ? obtainStyledAttributes.getDrawable(2) : drawable;
        String string3 = obtainStyledAttributes.getString(11);
        this.A05 = string3 == null ? obtainStyledAttributes.getString(3) : string3;
        String string4 = obtainStyledAttributes.getString(10);
        this.A04 = string4 == null ? obtainStyledAttributes.getString(4) : string4;
        this.A00 = obtainStyledAttributes.getResourceId(7, obtainStyledAttributes.getResourceId(5, 0));
        obtainStyledAttributes.recycle();
    }

    @Override // androidx.preference.Preference
    public void A07() {
        DialogFragment A00;
        AbstractC005402n r4 = this.A0F.A04;
        if (r4 != null) {
            AnonymousClass01E r42 = (AnonymousClass01E) r4;
            if (r42.A0F().A0A("androidx.preference.PreferenceFragment.DIALOG") == null) {
                if (this instanceof EditTextPreference) {
                    A00 = EditTextPreferenceDialogFragmentCompat.A00(this.A0L);
                } else if (this instanceof ListPreference) {
                    A00 = ListPreferenceDialogFragmentCompat.A00(this.A0L);
                } else if (this instanceof MultiSelectListPreference) {
                    A00 = MultiSelectListPreferenceDialogFragmentCompat.A00(this.A0L);
                } else {
                    StringBuilder sb = new StringBuilder("Cannot display dialog for an unknown Preference type: ");
                    sb.append(getClass().getSimpleName());
                    sb.append(". Make sure to implement onPreferenceDisplayDialog() to handle displaying a custom dialog for this Preference.");
                    throw new IllegalArgumentException(sb.toString());
                }
                A00.A0X(r42, 0);
                A00.A1F(r42.A0F(), "androidx.preference.PreferenceFragment.DIALOG");
            }
        }
    }
}
