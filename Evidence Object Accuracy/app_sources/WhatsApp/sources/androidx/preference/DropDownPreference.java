package androidx.preference;

import X.AnonymousClass0FF;
import X.C07180Wz;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class DropDownPreference extends ListPreference {
    public Spinner A00;
    public final Context A01;
    public final AdapterView.OnItemSelectedListener A02 = new C07180Wz(this);
    public final ArrayAdapter A03;

    public DropDownPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, R.attr.dropdownPreferenceStyle, 0);
        this.A01 = context;
        ArrayAdapter arrayAdapter = new ArrayAdapter(context, 17367049);
        this.A03 = arrayAdapter;
        arrayAdapter.clear();
        CharSequence[] charSequenceArr = ((ListPreference) this).A03;
        if (charSequenceArr != null) {
            for (CharSequence charSequence : charSequenceArr) {
                arrayAdapter.add(charSequence.toString());
            }
        }
    }

    @Override // androidx.preference.Preference
    public void A04() {
        super.A04();
        ArrayAdapter arrayAdapter = this.A03;
        if (arrayAdapter != null) {
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override // androidx.preference.DialogPreference, androidx.preference.Preference
    public void A07() {
        this.A00.performClick();
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r6) {
        int i;
        Spinner spinner = (Spinner) r6.A0H.findViewById(R.id.spinner);
        this.A00 = spinner;
        spinner.setAdapter((SpinnerAdapter) this.A03);
        this.A00.setOnItemSelectedListener(this.A02);
        Spinner spinner2 = this.A00;
        String str = ((ListPreference) this).A01;
        CharSequence[] charSequenceArr = ((ListPreference) this).A04;
        if (str != null && charSequenceArr != null) {
            i = charSequenceArr.length;
            do {
                i--;
                if (i >= 0) {
                }
            } while (!charSequenceArr[i].equals(str));
            spinner2.setSelection(i);
            super.A0R(r6);
        }
        i = -1;
        spinner2.setSelection(i);
        super.A0R(r6);
    }

    @Override // androidx.preference.ListPreference
    public void A0V(CharSequence[] charSequenceArr) {
        ((ListPreference) this).A03 = charSequenceArr;
        ArrayAdapter arrayAdapter = this.A03;
        arrayAdapter.clear();
        CharSequence[] charSequenceArr2 = ((ListPreference) this).A03;
        if (charSequenceArr2 != null) {
            for (CharSequence charSequence : charSequenceArr2) {
                arrayAdapter.add(charSequence.toString());
            }
        }
    }
}
