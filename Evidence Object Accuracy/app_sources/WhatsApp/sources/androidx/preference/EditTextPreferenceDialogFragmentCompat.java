package androidx.preference;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/* loaded from: classes.dex */
public class EditTextPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {
    public EditText A00;
    public CharSequence A01;

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public boolean A1M() {
        return true;
    }

    public static EditTextPreferenceDialogFragmentCompat A00(String str) {
        EditTextPreferenceDialogFragmentCompat editTextPreferenceDialogFragmentCompat = new EditTextPreferenceDialogFragmentCompat();
        Bundle bundle = new Bundle(1);
        bundle.putString("key", str);
        editTextPreferenceDialogFragmentCompat.A0U(bundle);
        return editTextPreferenceDialogFragmentCompat;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        bundle.putCharSequence("EditTextPreferenceDialogFragment.text", this.A01);
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        CharSequence charSequence;
        super.A16(bundle);
        if (bundle == null) {
            charSequence = ((EditTextPreference) A1I()).A00;
        } else {
            charSequence = bundle.getCharSequence("EditTextPreferenceDialogFragment.text");
        }
        this.A01 = charSequence;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public void A1J(View view) {
        super.A1J(view);
        EditText editText = (EditText) view.findViewById(16908291);
        this.A00 = editText;
        if (editText != null) {
            editText.requestFocus();
            this.A00.setText(this.A01);
            EditText editText2 = this.A00;
            editText2.setSelection(editText2.getText().length());
            A1I();
            return;
        }
        throw new IllegalStateException("Dialog view must contain an EditText with id @android:id/edit");
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public void A1L(boolean z) {
        if (z) {
            String obj = this.A00.getText().toString();
            EditTextPreference editTextPreference = (EditTextPreference) A1I();
            if (editTextPreference.A0Q(obj)) {
                editTextPreference.A0S(obj);
            }
        }
    }
}
