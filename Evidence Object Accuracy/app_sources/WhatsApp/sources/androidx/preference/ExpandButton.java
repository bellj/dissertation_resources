package androidx.preference;

import X.AnonymousClass0FF;
import X.C012005t;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes.dex */
public final class ExpandButton extends Preference {
    public long A00;

    public ExpandButton(Context context, List list, long j) {
        super(context, null);
        this.A01 = R.layout.expand_button;
        Context context2 = this.A05;
        Drawable A04 = C012005t.A01().A04(context2, R.drawable.ic_arrow_down_24dp);
        if (this.A07 != A04) {
            this.A07 = A04;
            super.A00 = 0;
            A04();
        }
        super.A00 = R.drawable.ic_arrow_down_24dp;
        A0A(R.string.expand_button_title);
        if (999 != this.A02) {
            this.A02 = 999;
            A05();
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        String str = null;
        while (it.hasNext()) {
            Preference preference = (Preference) it.next();
            CharSequence charSequence = preference.A0H;
            boolean z = preference instanceof PreferenceGroup;
            if (z && !TextUtils.isEmpty(charSequence)) {
                arrayList.add(preference);
            }
            if (arrayList.contains(preference.A0E)) {
                if (z) {
                    arrayList.add(preference);
                }
            } else if (!TextUtils.isEmpty(charSequence)) {
                if (str == null) {
                    str = charSequence;
                } else {
                    str = context2.getString(R.string.summary_collapsed_preference_list, str, charSequence);
                }
            }
        }
        A0I(str);
        this.A00 = j + SearchActionVerificationClientService.MS_TO_NS;
    }

    @Override // androidx.preference.Preference
    public long A00() {
        return this.A00;
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r2) {
        super.A0R(r2);
        r2.A00 = false;
    }
}
