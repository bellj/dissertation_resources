package androidx.preference;

import X.AnonymousClass0OC;
import X.AnonymousClass0V8;
import X.C004802e;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/* loaded from: classes.dex */
public class MultiSelectListPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {
    public Set A00 = new HashSet();
    public boolean A01;
    public CharSequence[] A02;
    public CharSequence[] A03;

    public static MultiSelectListPreferenceDialogFragmentCompat A00(String str) {
        MultiSelectListPreferenceDialogFragmentCompat multiSelectListPreferenceDialogFragmentCompat = new MultiSelectListPreferenceDialogFragmentCompat();
        Bundle bundle = new Bundle(1);
        bundle.putString("key", str);
        multiSelectListPreferenceDialogFragmentCompat.A0U(bundle);
        return multiSelectListPreferenceDialogFragmentCompat;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A0w(Bundle bundle) {
        super.A0w(bundle);
        bundle.putStringArrayList("MultiSelectListPreferenceDialogFragmentCompat.values", new ArrayList<>(this.A00));
        bundle.putBoolean("MultiSelectListPreferenceDialogFragmentCompat.changed", this.A01);
        bundle.putCharSequenceArray("MultiSelectListPreferenceDialogFragmentCompat.entries", this.A02);
        bundle.putCharSequenceArray("MultiSelectListPreferenceDialogFragmentCompat.entryValues", this.A03);
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment, X.AnonymousClass01E
    public void A16(Bundle bundle) {
        CharSequence[] charSequenceArr;
        super.A16(bundle);
        if (bundle == null) {
            MultiSelectListPreference multiSelectListPreference = (MultiSelectListPreference) A1I();
            CharSequence[] charSequenceArr2 = multiSelectListPreference.A01;
            if (charSequenceArr2 == null || (charSequenceArr = multiSelectListPreference.A02) == null) {
                throw new IllegalStateException("MultiSelectListPreference requires an entries array and an entryValues array.");
            }
            Set set = this.A00;
            set.clear();
            set.addAll(multiSelectListPreference.A00);
            this.A01 = false;
            this.A02 = charSequenceArr2;
            this.A03 = charSequenceArr;
            return;
        }
        Set set2 = this.A00;
        set2.clear();
        set2.addAll(bundle.getStringArrayList("MultiSelectListPreferenceDialogFragmentCompat.values"));
        this.A01 = bundle.getBoolean("MultiSelectListPreferenceDialogFragmentCompat.changed", false);
        this.A02 = bundle.getCharSequenceArray("MultiSelectListPreferenceDialogFragmentCompat.entries");
        this.A03 = bundle.getCharSequenceArray("MultiSelectListPreferenceDialogFragmentCompat.entryValues");
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public void A1K(C004802e r6) {
        int length = this.A03.length;
        boolean[] zArr = new boolean[length];
        for (int i = 0; i < length; i++) {
            zArr[i] = this.A00.contains(this.A03[i].toString());
        }
        CharSequence[] charSequenceArr = this.A02;
        AnonymousClass0V8 r0 = new AnonymousClass0V8(this);
        AnonymousClass0OC r1 = r6.A01;
        r1.A0M = charSequenceArr;
        r1.A09 = r0;
        r1.A0N = zArr;
        r1.A0K = true;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public void A1L(boolean z) {
        if (z && this.A01) {
            MultiSelectListPreference multiSelectListPreference = (MultiSelectListPreference) A1I();
            Set set = this.A00;
            if (multiSelectListPreference.A0Q(set)) {
                multiSelectListPreference.A0S(set);
            }
        }
        this.A01 = false;
    }
}
