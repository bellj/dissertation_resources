package androidx.preference;

import X.AnonymousClass00T;
import X.AnonymousClass04Z;
import X.AnonymousClass06r;
import X.AnonymousClass0FF;
import X.C06410Tm;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class PreferenceCategory extends PreferenceGroup {
    @Override // androidx.preference.Preference
    public boolean A0N() {
        return false;
    }

    public PreferenceCategory(Context context) {
        this(context, null);
    }

    public PreferenceCategory(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, AnonymousClass06r.A00(context, R.attr.preferenceCategoryStyle, 16842892));
    }

    public PreferenceCategory(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i, 0);
    }

    public PreferenceCategory(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @Override // androidx.preference.Preference
    @Deprecated
    public void A0G(AnonymousClass04Z r8) {
        C06410Tm A01;
        if (Build.VERSION.SDK_INT < 28 && (A01 = r8.A01()) != null) {
            r8.A0J(C06410Tm.A01(A01.A04(), A01.A05(), A01.A02(), A01.A03(), true, A01.A06()));
        }
    }

    @Override // androidx.preference.Preference
    public boolean A0O() {
        return !super.A0N();
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r6) {
        TextView textView;
        super.A0R(r6);
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            r6.A0H.setAccessibilityHeading(true);
        } else if (i < 21) {
            TypedValue typedValue = new TypedValue();
            Context context = ((Preference) this).A05;
            if (context.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true) && (textView = (TextView) r6.A08(16908310)) != null && textView.getCurrentTextColor() == AnonymousClass00T.A00(context, R.color.preference_fallback_accent_color)) {
                textView.setTextColor(typedValue.data);
            }
        }
    }
}
