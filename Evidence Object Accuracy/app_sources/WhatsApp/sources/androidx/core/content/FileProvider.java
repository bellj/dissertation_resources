package androidx.core.content;

import X.AnonymousClass00T;
import X.AnonymousClass01Q;
import X.AnonymousClass01R;
import X.AnonymousClass0KW;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.xmlpull.v1.XmlPullParserException;

/* loaded from: classes.dex */
public class FileProvider extends ContentProvider {
    public static final File A02 = new File("/");
    public static final HashMap A03 = new HashMap();
    public static final String[] A04 = {"_display_name", "_size"};
    public int A00;
    public AnonymousClass01R A01;

    @Override // android.content.ContentProvider
    public boolean onCreate() {
        return true;
    }

    public FileProvider() {
        this.A00 = 0;
    }

    public FileProvider(int i) {
        this.A00 = i;
    }

    public static Uri A00(Context context, File file, String str) {
        AnonymousClass01Q r4 = (AnonymousClass01Q) A01(context, str, 0);
        try {
            String canonicalPath = file.getCanonicalPath();
            Map.Entry entry = null;
            for (Map.Entry entry2 : r4.A01.entrySet()) {
                String path = ((File) entry2.getValue()).getPath();
                if (canonicalPath.startsWith(path) && (entry == null || path.length() > ((File) entry.getValue()).getPath().length())) {
                    entry = entry2;
                }
            }
            if (entry != null) {
                String path2 = ((File) entry.getValue()).getPath();
                boolean endsWith = path2.endsWith("/");
                int length = path2.length();
                if (!endsWith) {
                    length++;
                }
                String substring = canonicalPath.substring(length);
                StringBuilder sb = new StringBuilder();
                sb.append(Uri.encode((String) entry.getKey()));
                sb.append('/');
                sb.append(Uri.encode(substring, "/"));
                return new Uri.Builder().scheme("content").authority(r4.A00).encodedPath(sb.toString()).build();
            }
            StringBuilder sb2 = new StringBuilder("Failed to find configured root that contains ");
            sb2.append(canonicalPath);
            throw new IllegalArgumentException(sb2.toString());
        } catch (IOException unused) {
            StringBuilder sb3 = new StringBuilder("Failed to resolve canonical path for ");
            sb3.append(file);
            throw new IllegalArgumentException(sb3.toString());
        }
    }

    public static AnonymousClass01R A01(Context context, String str, int i) {
        File file;
        File[] A0J;
        AnonymousClass01Q r4;
        HashMap hashMap = A03;
        synchronized (hashMap) {
            AnonymousClass01R r42 = (AnonymousClass01R) hashMap.get(str);
            r4 = r42;
            if (r42 == null) {
                try {
                    try {
                        AnonymousClass01Q r43 = new AnonymousClass01Q(str);
                        ProviderInfo resolveContentProvider = context.getPackageManager().resolveContentProvider(str, 128);
                        if (resolveContentProvider != null) {
                            if (resolveContentProvider.metaData == null && i != 0) {
                                Bundle bundle = new Bundle(1);
                                resolveContentProvider.metaData = bundle;
                                bundle.putInt("android.support.FILE_PROVIDER_PATHS", i);
                            }
                            XmlResourceParser loadXmlMetaData = resolveContentProvider.loadXmlMetaData(context.getPackageManager(), "android.support.FILE_PROVIDER_PATHS");
                            if (loadXmlMetaData != null) {
                                while (true) {
                                    int next = loadXmlMetaData.next();
                                    if (next == 1) {
                                        hashMap.put(str, r43);
                                        r4 = r43;
                                        break;
                                    } else if (next == 2) {
                                        String name = loadXmlMetaData.getName();
                                        String attributeValue = loadXmlMetaData.getAttributeValue(null, "name");
                                        String attributeValue2 = loadXmlMetaData.getAttributeValue(null, "path");
                                        if ("root-path".equals(name)) {
                                            file = A02;
                                        } else if ("files-path".equals(name)) {
                                            file = context.getFilesDir();
                                        } else if ("cache-path".equals(name)) {
                                            file = context.getCacheDir();
                                        } else if ("external-path".equals(name)) {
                                            file = Environment.getExternalStorageDirectory();
                                        } else {
                                            if ("external-files-path".equals(name)) {
                                                A0J = AnonymousClass00T.A0J(context);
                                            } else if ("external-cache-path".equals(name)) {
                                                A0J = AnonymousClass00T.A0I(context);
                                            } else if (Build.VERSION.SDK_INT >= 21 && "external-media-path".equals(name)) {
                                                A0J = AnonymousClass0KW.A00(context);
                                            }
                                            if (A0J.length > 0) {
                                                file = A0J[0];
                                            } else {
                                                continue;
                                            }
                                        }
                                        if (file == null) {
                                            continue;
                                        } else {
                                            String str2 = new String[]{attributeValue2}[0];
                                            if (str2 != null) {
                                                file = new File(file, str2);
                                            }
                                            if (!TextUtils.isEmpty(attributeValue)) {
                                                try {
                                                    r43.A01.put(attributeValue, file.getCanonicalFile());
                                                } catch (IOException e) {
                                                    StringBuilder sb = new StringBuilder("Failed to resolve canonical path for ");
                                                    sb.append(file);
                                                    throw new IllegalArgumentException(sb.toString(), e);
                                                }
                                            } else {
                                                throw new IllegalArgumentException("Name must not be empty");
                                            }
                                        }
                                    }
                                }
                            } else {
                                throw new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
                            }
                        } else {
                            StringBuilder sb2 = new StringBuilder("Couldn't find meta-data for provider with authority ");
                            sb2.append(str);
                            throw new IllegalArgumentException(sb2.toString());
                        }
                    } catch (XmlPullParserException e2) {
                        throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e2);
                    }
                } catch (IOException e3) {
                    throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", e3);
                }
            }
        }
        return r4;
    }

    @Override // android.content.ContentProvider
    public void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
        if (providerInfo.exported) {
            throw new SecurityException("Provider must not be exported");
        } else if (providerInfo.grantUriPermissions) {
            String str = providerInfo.authority.split(";")[0];
            HashMap hashMap = A03;
            synchronized (hashMap) {
                hashMap.remove(str);
            }
            this.A01 = A01(context, str, this.A00);
        } else {
            throw new SecurityException("Provider must grant uri permissions");
        }
    }

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        return this.A01.ACz(uri).delete() ? 1 : 0;
    }

    @Override // android.content.ContentProvider
    public String getType(Uri uri) {
        String mimeTypeFromExtension;
        File ACz = this.A01.ACz(uri);
        int lastIndexOf = ACz.getName().lastIndexOf(46);
        return (lastIndexOf < 0 || (mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ACz.getName().substring(lastIndexOf + 1))) == null) ? "application/octet-stream" : mimeTypeFromExtension;
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException("No external inserts");
    }

    @Override // android.content.ContentProvider
    public ParcelFileDescriptor openFile(Uri uri, String str) {
        int i;
        File ACz = this.A01.ACz(uri);
        if ("r".equals(str)) {
            i = 268435456;
        } else if ("w".equals(str) || "wt".equals(str)) {
            i = 738197504;
        } else if ("wa".equals(str)) {
            i = 704643072;
        } else if ("rw".equals(str)) {
            i = 939524096;
        } else if ("rwt".equals(str)) {
            i = 1006632960;
        } else {
            StringBuilder sb = new StringBuilder("Invalid mode: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
        return ParcelFileDescriptor.open(ACz, i);
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        int i;
        String str3;
        File ACz = this.A01.ACz(uri);
        String queryParameter = uri.getQueryParameter("displayName");
        if (strArr == null) {
            strArr = A04;
        }
        int length = strArr.length;
        String[] strArr3 = new String[length];
        Object[] objArr = new Object[length];
        int i2 = 0;
        for (String str4 : strArr) {
            if ("_display_name".equals(str4)) {
                strArr3[i2] = "_display_name";
                i = i2 + 1;
                if (queryParameter == null) {
                    str3 = ACz.getName();
                } else {
                    str3 = queryParameter;
                }
                objArr[i2] = str3;
            } else if ("_size".equals(str4)) {
                strArr3[i2] = "_size";
                i = i2 + 1;
                objArr[i2] = Long.valueOf(ACz.length());
            }
            i2 = i;
        }
        String[] strArr4 = new String[i2];
        System.arraycopy(strArr3, 0, strArr4, 0, i2);
        Object[] objArr2 = new Object[i2];
        System.arraycopy(objArr, 0, objArr2, 0, i2);
        MatrixCursor matrixCursor = new MatrixCursor(strArr4, 1);
        matrixCursor.addRow(objArr2);
        return matrixCursor;
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        throw new UnsupportedOperationException("No external updates");
    }
}
