package androidx.core.app;

import X.AbstractC007203r;
import android.app.PendingIntent;
import androidx.core.graphics.drawable.IconCompat;

/* loaded from: classes.dex */
public final class RemoteActionCompat implements AbstractC007203r {
    public PendingIntent A00;
    public IconCompat A01;
    public CharSequence A02;
    public CharSequence A03;
    public boolean A04;
    public boolean A05;
}
