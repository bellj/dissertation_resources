package androidx.core.app;

import X.AbstractC006703e;
import X.AnonymousClass02S;
import X.AnonymousClass02T;
import X.AnonymousClass069;
import X.AnonymousClass0SK;
import X.C007303s;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* loaded from: classes.dex */
public class NotificationCompat$MessagingStyle extends AbstractC006703e {
    public C007303s A00;
    public Boolean A01;
    public CharSequence A02;
    public final List A03 = new ArrayList();
    public final List A04 = new ArrayList();

    @Override // X.AbstractC006703e
    public String A05() {
        return "androidx.core.app.NotificationCompat$MessagingStyle";
    }

    public NotificationCompat$MessagingStyle() {
    }

    public NotificationCompat$MessagingStyle(C007303s r3) {
        if (!TextUtils.isEmpty(r3.A01)) {
            this.A00 = r3;
            return;
        }
        throw new IllegalArgumentException("User's name must not be empty.");
    }

    @Override // X.AbstractC006703e
    public void A06(Bundle bundle) {
        super.A06(bundle);
        C007303s r2 = this.A00;
        bundle.putCharSequence("android.selfDisplayName", r2.A01);
        bundle.putBundle("android.messagingStyleUser", r2.A02());
        bundle.putCharSequence("android.hiddenConversationTitle", this.A02);
        CharSequence charSequence = this.A02;
        if (charSequence != null && this.A01.booleanValue()) {
            bundle.putCharSequence("android.conversationTitle", charSequence);
        }
        List list = this.A04;
        if (!list.isEmpty()) {
            bundle.putParcelableArray("android.messages", AnonymousClass0SK.A00(list));
        }
        List list2 = this.A03;
        if (!list2.isEmpty()) {
            bundle.putParcelableArray("android.messages.historic", AnonymousClass0SK.A00(list2));
        }
        Boolean bool = this.A01;
        if (bool != null) {
            bundle.putBoolean("android.isGroupConversation", bool.booleanValue());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d2, code lost:
        if (r3 != null) goto L_0x00d4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0109  */
    @Override // X.AbstractC006703e
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.AbstractC11240fy r9) {
        /*
        // Method dump skipped, instructions count: 369
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.core.app.NotificationCompat$MessagingStyle.A08(X.0fy):void");
    }

    public final CharSequence A09(AnonymousClass0SK r14) {
        AnonymousClass02S r5;
        CharSequence charSequence;
        int i;
        boolean z = true;
        if (AnonymousClass069.A00(Locale.getDefault()) != 1) {
            z = false;
        }
        AnonymousClass02T r1 = AnonymousClass02S.A05;
        if (r1 != r1) {
            r5 = new AnonymousClass02S(r1, 2, z);
        } else if (z) {
            r5 = AnonymousClass02S.A04;
        } else {
            r5 = AnonymousClass02S.A03;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        boolean z2 = false;
        int i2 = -1;
        if (Build.VERSION.SDK_INT >= 21) {
            z2 = true;
            i2 = -16777216;
        }
        C007303s r0 = r14.A04;
        CharSequence charSequence2 = "";
        if (r0 == null) {
            charSequence = charSequence2;
        } else {
            charSequence = r0.A01;
        }
        if (TextUtils.isEmpty(charSequence)) {
            charSequence = this.A00.A01;
            if (z2 && (i = super.A00.A00) != 0) {
                i2 = i;
            }
        }
        AnonymousClass02T r2 = r5.A01;
        CharSequence A02 = r5.A02(r2, charSequence);
        spannableStringBuilder.append(A02);
        spannableStringBuilder.setSpan(new TextAppearanceSpan(null, 0, 0, ColorStateList.valueOf(i2), null), spannableStringBuilder.length() - A02.length(), spannableStringBuilder.length(), 33);
        CharSequence charSequence3 = r14.A05;
        if (charSequence3 != null) {
            charSequence2 = charSequence3;
        }
        spannableStringBuilder.append((CharSequence) "  ").append(r5.A02(r2, charSequence2));
        return spannableStringBuilder;
    }
}
