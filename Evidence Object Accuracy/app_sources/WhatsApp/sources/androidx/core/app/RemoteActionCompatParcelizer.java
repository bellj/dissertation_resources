package androidx.core.app;

import X.AbstractC007203r;
import X.AnonymousClass0GE;
import X.AnonymousClass0QP;
import android.app.PendingIntent;
import android.os.Parcel;
import android.text.TextUtils;
import androidx.core.graphics.drawable.IconCompat;

/* loaded from: classes.dex */
public class RemoteActionCompatParcelizer {
    public static RemoteActionCompat read(AnonymousClass0QP r3) {
        RemoteActionCompat remoteActionCompat = new RemoteActionCompat();
        AbstractC007203r r1 = remoteActionCompat.A01;
        if (r3.A09(1)) {
            r1 = r3.A03();
        }
        remoteActionCompat.A01 = (IconCompat) r1;
        CharSequence charSequence = remoteActionCompat.A03;
        if (r3.A09(2)) {
            charSequence = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(((AnonymousClass0GE) r3).A05);
        }
        remoteActionCompat.A03 = charSequence;
        CharSequence charSequence2 = remoteActionCompat.A02;
        if (r3.A09(3)) {
            charSequence2 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(((AnonymousClass0GE) r3).A05);
        }
        remoteActionCompat.A02 = charSequence2;
        remoteActionCompat.A00 = (PendingIntent) r3.A01(remoteActionCompat.A00, 4);
        boolean z = remoteActionCompat.A04;
        if (r3.A09(5)) {
            z = false;
            if (((AnonymousClass0GE) r3).A05.readInt() != 0) {
                z = true;
            }
        }
        remoteActionCompat.A04 = z;
        boolean z2 = remoteActionCompat.A05;
        if (r3.A09(6)) {
            z2 = false;
            if (((AnonymousClass0GE) r3).A05.readInt() != 0) {
                z2 = true;
            }
        }
        remoteActionCompat.A05 = z2;
        return remoteActionCompat;
    }

    public static void write(RemoteActionCompat remoteActionCompat, AnonymousClass0QP r5) {
        IconCompat iconCompat = remoteActionCompat.A01;
        r5.A05(1);
        r5.A08(iconCompat);
        CharSequence charSequence = remoteActionCompat.A03;
        r5.A05(2);
        Parcel parcel = ((AnonymousClass0GE) r5).A05;
        TextUtils.writeToParcel(charSequence, parcel, 0);
        CharSequence charSequence2 = remoteActionCompat.A02;
        r5.A05(3);
        TextUtils.writeToParcel(charSequence2, parcel, 0);
        r5.A07(remoteActionCompat.A00, 4);
        boolean z = remoteActionCompat.A04;
        r5.A05(5);
        parcel.writeInt(z ? 1 : 0);
        boolean z2 = remoteActionCompat.A05;
        r5.A05(6);
        parcel.writeInt(z2 ? 1 : 0);
    }
}
