package androidx.core.app;

import X.AbstractC006703e;
import X.AbstractC11240fy;
import X.C05600Qe;
import X.C07310Xm;
import android.app.Notification;
import android.graphics.Bitmap;
import android.os.Build;

/* loaded from: classes.dex */
public class NotificationCompat$BigPictureStyle extends AbstractC006703e {
    public Bitmap A00;
    public boolean A01;

    @Override // X.AbstractC006703e
    public String A05() {
        return "androidx.core.app.NotificationCompat$BigPictureStyle";
    }

    @Override // X.AbstractC006703e
    public void A08(AbstractC11240fy r4) {
        int i = Build.VERSION.SDK_INT;
        Notification.BigPictureStyle bigPicture = new Notification.BigPictureStyle(((C07310Xm) r4).A02).setBigContentTitle(null).bigPicture(this.A00);
        if (this.A01) {
            bigPicture.bigLargeIcon((Bitmap) null);
        }
        if (this.A02) {
            bigPicture.setSummaryText(super.A01);
        }
        if (i >= 31) {
            C05600Qe.A01(bigPicture);
            C05600Qe.A00(bigPicture);
        }
    }
}
