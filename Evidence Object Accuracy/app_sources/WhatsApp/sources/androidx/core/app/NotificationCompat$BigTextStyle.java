package androidx.core.app;

import X.AbstractC006703e;
import X.AbstractC11240fy;
import X.C005602s;
import X.C07310Xm;
import android.app.Notification;
import android.os.Build;
import android.os.Bundle;

/* loaded from: classes.dex */
public class NotificationCompat$BigTextStyle extends AbstractC006703e {
    public CharSequence A00;

    @Override // X.AbstractC006703e
    public String A05() {
        return "androidx.core.app.NotificationCompat$BigTextStyle";
    }

    @Override // X.AbstractC006703e
    public void A06(Bundle bundle) {
        super.A06(bundle);
        if (Build.VERSION.SDK_INT < 21) {
            bundle.putCharSequence("android.bigText", this.A00);
        }
    }

    @Override // X.AbstractC006703e
    public void A08(AbstractC11240fy r3) {
        Notification.BigTextStyle bigText = new Notification.BigTextStyle(((C07310Xm) r3).A02).setBigContentTitle(null).bigText(this.A00);
        if (this.A02) {
            bigText.setSummaryText(this.A01);
        }
    }

    public void A09(CharSequence charSequence) {
        this.A00 = C005602s.A00(charSequence);
    }
}
