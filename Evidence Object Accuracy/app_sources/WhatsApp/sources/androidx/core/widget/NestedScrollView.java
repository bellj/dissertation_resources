package androidx.core.widget;

import X.AbstractC11770gq;
import X.AbstractC12820ib;
import X.AnonymousClass028;
import X.AnonymousClass02D;
import X.AnonymousClass02F;
import X.AnonymousClass0B1;
import X.AnonymousClass0DU;
import X.AnonymousClass0L4;
import X.AnonymousClass0L5;
import X.AnonymousClass0T7;
import X.AnonymousClass0UY;
import X.C04740Mw;
import X.C25981Bo;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AnimationUtils;
import android.widget.EdgeEffect;
import android.widget.FrameLayout;
import android.widget.OverScroller;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class NestedScrollView extends FrameLayout implements AnonymousClass02D, AbstractC12820ib, AnonymousClass02F {
    public static final AnonymousClass0DU A0Q = new AnonymousClass0DU();
    public static final int[] A0R = {16843130};
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public long A08;
    public VelocityTracker A09;
    public View A0A;
    public EdgeEffect A0B;
    public EdgeEffect A0C;
    public OverScroller A0D;
    public AbstractC11770gq A0E;
    public AnonymousClass0B1 A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public final Rect A0L;
    public final AnonymousClass0UY A0M;
    public final C04740Mw A0N;
    public final int[] A0O;
    public final int[] A0P;

    @Override // X.AbstractC016407t
    public boolean AWL(View view, View view2, int i, int i2) {
        return (i & 2) != 0;
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return true;
    }

    public NestedScrollView(Context context) {
        this(context, null);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.nestedScrollViewStyle);
    }

    public NestedScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        EdgeEffect edgeEffect;
        EdgeEffect edgeEffect2;
        this.A0L = new Rect();
        this.A0J = true;
        this.A0I = false;
        this.A0A = null;
        this.A0H = false;
        this.A0K = true;
        this.A01 = -1;
        this.A0P = new int[2];
        this.A0O = new int[2];
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 31) {
            edgeEffect = AnonymousClass0T7.A02(context, attributeSet);
        } else {
            edgeEffect = new EdgeEffect(context);
        }
        this.A0C = edgeEffect;
        if (i2 >= 31) {
            edgeEffect2 = AnonymousClass0T7.A02(context, attributeSet);
        } else {
            edgeEffect2 = new EdgeEffect(context);
        }
        this.A0B = edgeEffect2;
        this.A0D = new OverScroller(getContext());
        setFocusable(true);
        setDescendantFocusability(262144);
        setWillNotDraw(false);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.A07 = viewConfiguration.getScaledTouchSlop();
        this.A05 = viewConfiguration.getScaledMinimumFlingVelocity();
        this.A04 = viewConfiguration.getScaledMaximumFlingVelocity();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, A0R, i, 0);
        setFillViewport(obtainStyledAttributes.getBoolean(0, false));
        obtainStyledAttributes.recycle();
        this.A0N = new C04740Mw();
        this.A0M = new AnonymousClass0UY(this);
        setNestedScrollingEnabled(true);
        AnonymousClass028.A0g(this, A0Q);
    }

    public static float A00(EdgeEffect edgeEffect) {
        if (Build.VERSION.SDK_INT >= 31) {
            return AnonymousClass0T7.A00(edgeEffect);
        }
        return 0.0f;
    }

    public static float A01(EdgeEffect edgeEffect, float f, float f2) {
        if (Build.VERSION.SDK_INT >= 31) {
            return AnonymousClass0T7.A01(edgeEffect, f, f2);
        }
        AnonymousClass0L4.A00(edgeEffect, f, f2);
        return f;
    }

    public static boolean A02(View view, View view2) {
        if (view != view2) {
            ViewParent parent = view.getParent();
            if (!(parent instanceof ViewGroup) || !A02((View) parent, view2)) {
                return false;
            }
        }
        return true;
    }

    public int A03(Rect rect) {
        int i;
        int i2;
        if (getChildCount() == 0) {
            return 0;
        }
        int height = getHeight();
        int scrollY = getScrollY();
        int i3 = scrollY + height;
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        if (rect.top > 0) {
            scrollY += verticalFadingEdgeLength;
        }
        View childAt = getChildAt(0);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) childAt.getLayoutParams();
        int i4 = i3 - verticalFadingEdgeLength;
        if (rect.bottom >= childAt.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin) {
            i4 = i3;
        }
        int i5 = rect.bottom;
        if (i5 > i4 && rect.top > scrollY) {
            if (rect.height() > height) {
                i2 = rect.top - scrollY;
            } else {
                i2 = rect.bottom - i4;
            }
            return Math.min(i2 + 0, (childAt.getBottom() + layoutParams.bottomMargin) - i3);
        } else if (rect.top >= scrollY || i5 >= i4) {
            return 0;
        } else {
            if (rect.height() > height) {
                i = 0 - (i4 - rect.bottom);
            } else {
                i = 0 - (scrollY - rect.top);
            }
            return Math.max(i, -getScrollY());
        }
    }

    public void A04(int i) {
        if (getChildCount() > 0) {
            this.A0D.fling(getScrollX(), getScrollY(), 0, i, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 0);
            this.A0M.A03(2, 1);
            this.A03 = getScrollY();
            postInvalidateOnAnimation();
        }
    }

    public final void A05(int i) {
        if (i == 0) {
            return;
        }
        if (this.A0K) {
            A06(0, i, false);
        } else {
            scrollBy(0, i);
        }
    }

    public final void A06(int i, int i2, boolean z) {
        if (getChildCount() != 0) {
            if (AnimationUtils.currentAnimationTimeMillis() - this.A08 > 250) {
                View childAt = getChildAt(0);
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) childAt.getLayoutParams();
                int scrollY = getScrollY();
                OverScroller overScroller = this.A0D;
                int scrollX = getScrollX();
                overScroller.startScroll(scrollX, scrollY, 0, Math.max(0, Math.min(i2 + scrollY, Math.max(0, ((childAt.getHeight() + layoutParams.topMargin) + layoutParams.bottomMargin) - ((getHeight() - getPaddingTop()) - getPaddingBottom())))) - scrollY, 250);
                if (z) {
                    this.A0M.A03(2, 1);
                } else {
                    AeR(1);
                }
                this.A03 = getScrollY();
                postInvalidateOnAnimation();
            } else {
                OverScroller overScroller2 = this.A0D;
                if (!overScroller2.isFinished()) {
                    overScroller2.abortAnimation();
                    AeR(1);
                }
                scrollBy(i, i2);
            }
            this.A08 = AnimationUtils.currentAnimationTimeMillis();
        }
    }

    public final void A07(int i, int[] iArr, int i2) {
        int scrollY = getScrollY();
        scrollBy(0, i);
        int scrollY2 = getScrollY() - scrollY;
        if (iArr != null) {
            iArr[1] = iArr[1] + scrollY2;
        }
        this.A0M.A05(null, iArr, 0, scrollY2, 0, i - scrollY2, i2);
    }

    public final void A08(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.A01) {
            int i = 0;
            if (actionIndex == 0) {
                i = 1;
            }
            this.A02 = (int) motionEvent.getY(i);
            this.A01 = motionEvent.getPointerId(i);
            VelocityTracker velocityTracker = this.A09;
            if (velocityTracker != null) {
                velocityTracker.clear();
            }
        }
    }

    public boolean A09(int i) {
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i);
        int maxScrollAmount = getMaxScrollAmount();
        if (findNextFocus == null || !A0E(findNextFocus, maxScrollAmount, getHeight())) {
            if (i == 33) {
                if (getScrollY() < maxScrollAmount) {
                    maxScrollAmount = getScrollY();
                }
            } else if (i == 130 && getChildCount() > 0) {
                View childAt = getChildAt(0);
                maxScrollAmount = Math.min((childAt.getBottom() + ((FrameLayout.LayoutParams) childAt.getLayoutParams()).bottomMargin) - ((getScrollY() + getHeight()) - getPaddingBottom()), maxScrollAmount);
            }
            if (maxScrollAmount == 0) {
                return false;
            }
            if (i != 130) {
                maxScrollAmount = -maxScrollAmount;
            }
            A05(maxScrollAmount);
        } else {
            Rect rect = this.A0L;
            findNextFocus.getDrawingRect(rect);
            offsetDescendantRectToMyCoords(findNextFocus, rect);
            A05(A03(rect));
            findNextFocus.requestFocus(i);
        }
        if (findFocus == null || !findFocus.isFocused() || !(!A0E(findFocus, 0, getHeight()))) {
            return true;
        }
        int descendantFocusability = getDescendantFocusability();
        setDescendantFocusability(C25981Bo.A0F);
        requestFocus();
        setDescendantFocusability(descendantFocusability);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003c, code lost:
        if (r2 >= r20) goto L_0x003e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0043 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0A(int r18, int r19, int r20) {
        /*
            r17 = this;
            r13 = r19
            r11 = r17
            int r10 = r11.getHeight()
            int r9 = r11.getScrollY()
            int r10 = r10 + r9
            r16 = 0
            r0 = 33
            r15 = 0
            r12 = r18
            if (r12 != r0) goto L_0x0017
            r15 = 1
        L_0x0017:
            r0 = 2
            java.util.ArrayList r8 = r11.getFocusables(r0)
            int r7 = r8.size()
            r6 = 0
            r5 = 0
            r14 = 0
        L_0x0023:
            r0 = r20
            if (r5 >= r7) goto L_0x0064
            java.lang.Object r4 = r8.get(r5)
            android.view.View r4 = (android.view.View) r4
            int r3 = r4.getTop()
            int r2 = r4.getBottom()
            if (r13 >= r2) goto L_0x0043
            if (r3 >= r0) goto L_0x0043
            if (r13 >= r3) goto L_0x003e
            r1 = 1
            if (r2 < r0) goto L_0x003f
        L_0x003e:
            r1 = 0
        L_0x003f:
            if (r6 != 0) goto L_0x0046
            r6 = r4
            r14 = r1
        L_0x0043:
            int r5 = r5 + 1
            goto L_0x0023
        L_0x0046:
            if (r15 == 0) goto L_0x005c
            int r0 = r6.getTop()
            if (r3 < r0) goto L_0x0062
        L_0x004e:
            r0 = 0
        L_0x004f:
            if (r14 == 0) goto L_0x0057
            if (r1 == 0) goto L_0x0043
        L_0x0053:
            if (r0 == 0) goto L_0x0043
            r6 = r4
            goto L_0x0043
        L_0x0057:
            if (r1 == 0) goto L_0x0053
            r6 = r4
            r14 = 1
            goto L_0x0043
        L_0x005c:
            int r0 = r6.getBottom()
            if (r2 <= r0) goto L_0x004e
        L_0x0062:
            r0 = 1
            goto L_0x004f
        L_0x0064:
            if (r6 != 0) goto L_0x0067
            r6 = r11
        L_0x0067:
            if (r13 < r9) goto L_0x0075
            if (r0 > r10) goto L_0x0075
        L_0x006b:
            android.view.View r0 = r11.findFocus()
            if (r6 == r0) goto L_0x0074
            r6.requestFocus(r12)
        L_0x0074:
            return r16
        L_0x0075:
            int r13 = r19 - r9
            if (r15 != 0) goto L_0x007b
            int r13 = r20 - r10
        L_0x007b:
            r11.A05(r13)
            r16 = 1
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.core.widget.NestedScrollView.A0A(int, int, int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0037 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0B(int r12, int r13, int r14, int r15) {
        /*
            r11 = this;
            r7 = 0
            r11.getOverScrollMode()
            r11.computeHorizontalScrollRange()
            r11.computeHorizontalScrollExtent()
            r3 = 0
            r11.computeVerticalScrollRange()
            r11.computeVerticalScrollExtent()
            int r5 = r13 + r7
            r2 = 0
            int r6 = r14 + r12
            int r1 = -r7
            int r0 = r7 + r15
            if (r5 <= r7) goto L_0x0041
            r5 = 0
        L_0x001c:
            r2 = 1
        L_0x001d:
            if (r6 <= r0) goto L_0x003b
            r6 = r0
        L_0x0020:
            r1 = 1
            X.0UY r0 = r11.A0M
            android.view.ViewParent r0 = r0.A00
            if (r0 != 0) goto L_0x0032
            android.widget.OverScroller r4 = r11.A0D
            int r10 = r11.getScrollRange()
            r8 = 0
            r9 = 0
            r4.springBack(r5, r6, r7, r8, r9, r10)
        L_0x0032:
            r11.onOverScrolled(r5, r6, r2, r1)
            if (r2 != 0) goto L_0x0039
            if (r1 == 0) goto L_0x003a
        L_0x0039:
            r3 = 1
        L_0x003a:
            return r3
        L_0x003b:
            if (r6 >= r1) goto L_0x003f
            r6 = r1
            goto L_0x0020
        L_0x003f:
            r1 = 0
            goto L_0x0032
        L_0x0041:
            if (r5 >= r1) goto L_0x001d
            r5 = r1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.core.widget.NestedScrollView.A0B(int, int, int, int):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c4, code lost:
        if (r0 < 0) goto L_0x00b6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0C(android.view.KeyEvent r9) {
        /*
        // Method dump skipped, instructions count: 286
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.core.widget.NestedScrollView.A0C(android.view.KeyEvent):boolean");
    }

    public final boolean A0D(MotionEvent motionEvent) {
        boolean z;
        EdgeEffect edgeEffect = this.A0C;
        if (A00(edgeEffect) != 0.0f) {
            A01(edgeEffect, 0.0f, motionEvent.getX() / ((float) getWidth()));
            z = true;
        } else {
            z = false;
        }
        EdgeEffect edgeEffect2 = this.A0B;
        if (A00(edgeEffect2) == 0.0f) {
            return z;
        }
        A01(edgeEffect2, 0.0f, 1.0f - (motionEvent.getX() / ((float) getWidth())));
        return true;
    }

    public final boolean A0E(View view, int i, int i2) {
        Rect rect = this.A0L;
        view.getDrawingRect(rect);
        offsetDescendantRectToMyCoords(view, rect);
        return rect.bottom + i >= getScrollY() && rect.top - i <= getScrollY() + i2;
    }

    @Override // X.AbstractC016407t
    public void ASw(View view, int[] iArr, int i, int i2, int i3) {
        this.A0M.A04(iArr, null, i, i2, i3);
    }

    @Override // X.AbstractC016407t
    public void ASx(View view, int i, int i2, int i3, int i4, int i5) {
        A07(i4, null, i5);
    }

    @Override // X.AbstractC12820ib
    public void ASy(View view, int[] iArr, int i, int i2, int i3, int i4, int i5) {
        A07(i4, iArr, i5);
    }

    @Override // X.AbstractC016407t
    public void ASz(View view, View view2, int i, int i2) {
        C04740Mw r1 = this.A0N;
        if (i2 == 1) {
            r1.A00 = i;
        } else {
            r1.A01 = i;
        }
        this.A0M.A03(2, i2);
    }

    @Override // X.AbstractC016407t
    public void AWp(View view, int i) {
        C04740Mw r2 = this.A0N;
        if (i == 1) {
            r2.A00 = 0;
        } else {
            r2.A01 = 0;
        }
        AeR(i);
    }

    @Override // X.AnonymousClass02D
    public void AeR(int i) {
        this.A0M.A00(i);
    }

    @Override // android.view.ViewGroup
    public void addView(View view) {
        if (getChildCount() <= 0) {
            super.addView(view);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i) {
        if (getChildCount() <= 0) {
            super.addView(view, i);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() <= 0) {
            super.addView(view, i, layoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    @Override // android.view.ViewGroup, android.view.ViewManager
    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (getChildCount() <= 0) {
            super.addView(view, layoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }

    @Override // android.view.View
    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }

    @Override // android.view.View
    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }

    @Override // android.view.View
    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }

    @Override // android.view.View
    public void computeScroll() {
        EdgeEffect edgeEffect;
        OverScroller overScroller = this.A0D;
        if (!overScroller.isFinished()) {
            overScroller.computeScrollOffset();
            int currY = overScroller.getCurrY();
            int i = currY - this.A03;
            this.A03 = currY;
            int[] iArr = this.A0O;
            iArr[1] = 0;
            AnonymousClass0UY r5 = this.A0M;
            r5.A04(iArr, null, 0, i, 1);
            int i2 = i - iArr[1];
            int scrollRange = getScrollRange();
            if (i2 != 0) {
                int scrollY = getScrollY();
                A0B(i2, getScrollX(), scrollY, scrollRange);
                int scrollY2 = getScrollY() - scrollY;
                int i3 = i2 - scrollY2;
                iArr[1] = 0;
                r5.A05(this.A0P, iArr, 0, scrollY2, 0, i3, 1);
                int i4 = i3 - iArr[1];
                if (i4 != 0) {
                    int overScrollMode = getOverScrollMode();
                    if (overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0)) {
                        if (i4 < 0) {
                            edgeEffect = this.A0C;
                        } else {
                            edgeEffect = this.A0B;
                        }
                        if (edgeEffect.isFinished()) {
                            edgeEffect.onAbsorb((int) overScroller.getCurrVelocity());
                        }
                    }
                    overScroller.abortAnimation();
                    AeR(1);
                }
            }
            if (!overScroller.isFinished()) {
                postInvalidateOnAnimation();
            } else {
                AeR(1);
            }
        }
    }

    @Override // android.view.View
    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }

    @Override // android.view.View
    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    @Override // android.view.View
    public int computeVerticalScrollRange() {
        int childCount = getChildCount();
        int height = (getHeight() - getPaddingBottom()) - getPaddingTop();
        if (childCount == 0) {
            return height;
        }
        View childAt = getChildAt(0);
        int bottom = childAt.getBottom() + ((FrameLayout.LayoutParams) childAt.getLayoutParams()).bottomMargin;
        int scrollY = getScrollY();
        int max = Math.max(0, bottom - height);
        if (scrollY < 0) {
            return bottom - scrollY;
        }
        return scrollY > max ? bottom + (scrollY - max) : bottom;
    }

    @Override // android.view.View, android.view.ViewGroup
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || A0C(keyEvent);
    }

    @Override // android.view.View
    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        return this.A0M.A02(f, f2, z);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreFling(float f, float f2) {
        return this.A0M.A01(f, f2);
    }

    @Override // android.view.View
    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        return this.A0M.A04(iArr, iArr2, i, i2, 0);
    }

    @Override // android.view.View
    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        return this.A0M.A05(iArr, null, i, i2, i3, i4, 0);
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        int paddingLeft;
        super.draw(canvas);
        int scrollY = getScrollY();
        EdgeEffect edgeEffect = this.A0C;
        int i = 0;
        if (!edgeEffect.isFinished()) {
            int save = canvas.save();
            int width = getWidth();
            int height = getHeight();
            int min = Math.min(0, scrollY);
            int i2 = Build.VERSION.SDK_INT;
            if (i2 < 21 || AnonymousClass0L5.A00(this)) {
                width -= getPaddingLeft() + getPaddingRight();
                paddingLeft = getPaddingLeft() + 0;
            } else {
                paddingLeft = 0;
            }
            if (i2 >= 21 && AnonymousClass0L5.A00(this)) {
                height -= getPaddingTop() + getPaddingBottom();
                min += getPaddingTop();
            }
            canvas.translate((float) paddingLeft, (float) min);
            edgeEffect.setSize(width, height);
            if (edgeEffect.draw(canvas)) {
                postInvalidateOnAnimation();
            }
            canvas.restoreToCount(save);
        }
        EdgeEffect edgeEffect2 = this.A0B;
        if (!edgeEffect2.isFinished()) {
            int save2 = canvas.save();
            int width2 = getWidth();
            int height2 = getHeight();
            int max = Math.max(getScrollRange(), scrollY) + height2;
            int i3 = Build.VERSION.SDK_INT;
            if (i3 < 21 || AnonymousClass0L5.A00(this)) {
                width2 -= getPaddingLeft() + getPaddingRight();
                i = 0 + getPaddingLeft();
            }
            if (i3 >= 21 && AnonymousClass0L5.A00(this)) {
                height2 -= getPaddingTop() + getPaddingBottom();
                max -= getPaddingBottom();
            }
            canvas.translate((float) (i - width2), (float) max);
            canvas.rotate(180.0f, (float) width2, 0.0f);
            edgeEffect2.setSize(width2, height2);
            if (edgeEffect2.draw(canvas)) {
                postInvalidateOnAnimation();
            }
            canvas.restoreToCount(save2);
        }
    }

    @Override // android.view.View
    public float getBottomFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        View childAt = getChildAt(0);
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int bottom = ((childAt.getBottom() + ((FrameLayout.LayoutParams) childAt.getLayoutParams()).bottomMargin) - getScrollY()) - (getHeight() - getPaddingBottom());
        if (bottom < verticalFadingEdgeLength) {
            return ((float) bottom) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    public int getMaxScrollAmount() {
        return (int) (((float) getHeight()) * 0.5f);
    }

    @Override // android.view.ViewGroup
    public int getNestedScrollAxes() {
        C04740Mw r0 = this.A0N;
        return r0.A01 | r0.A00;
    }

    public int getScrollRange() {
        if (getChildCount() <= 0) {
            return 0;
        }
        View childAt = getChildAt(0);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) childAt.getLayoutParams();
        return Math.max(0, ((childAt.getHeight() + layoutParams.topMargin) + layoutParams.bottomMargin) - ((getHeight() - getPaddingTop()) - getPaddingBottom()));
    }

    @Override // android.view.View
    public float getTopFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }
        int verticalFadingEdgeLength = getVerticalFadingEdgeLength();
        int scrollY = getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return ((float) scrollY) / ((float) verticalFadingEdgeLength);
        }
        return 1.0f;
    }

    private float getVerticalScrollFactorCompat() {
        float f = this.A00;
        if (f != 0.0f) {
            return f;
        }
        TypedValue typedValue = new TypedValue();
        Context context = getContext();
        if (context.getTheme().resolveAttribute(16842829, typedValue, true)) {
            float dimension = typedValue.getDimension(context.getResources().getDisplayMetrics());
            this.A00 = dimension;
            return dimension;
        }
        throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
    }

    @Override // android.view.View
    public boolean hasNestedScrollingParent() {
        return this.A0M.A01 != null;
    }

    @Override // android.view.View, X.AnonymousClass02E
    public boolean isNestedScrollingEnabled() {
        return this.A0M.A02;
    }

    @Override // android.view.ViewGroup
    public void measureChild(View view, int i, int i2) {
        view.measure(FrameLayout.getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), view.getLayoutParams().width), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    @Override // android.view.ViewGroup
    public void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        view.measure(FrameLayout.getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i2, marginLayoutParams.width), View.MeasureSpec.makeMeasureSpec(marginLayoutParams.topMargin + marginLayoutParams.bottomMargin, 0));
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A0I = false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v11, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r0v13, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r0v19, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r0v27, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // android.view.View
    public boolean onGenericMotionEvent(MotionEvent motionEvent) {
        int i;
        boolean z;
        int i2 = 0;
        if (motionEvent.getAction() == 8 && !this.A0H) {
            boolean z2 = false;
            if ((motionEvent.getSource() & 2) == 2) {
                z2 = true;
            }
            if (z2) {
                i = 9;
            } else if ((motionEvent.getSource() & 4194304) == 4194304) {
                i = 26;
            }
            float axisValue = motionEvent.getAxisValue(i);
            if (axisValue != 0.0f) {
                int scrollRange = getScrollRange();
                int scrollY = getScrollY();
                int verticalScrollFactorCompat = scrollY - ((int) (axisValue * getVerticalScrollFactorCompat()));
                if (verticalScrollFactorCompat < 0) {
                    int overScrollMode = getOverScrollMode();
                    if ((overScrollMode == 0 || (overScrollMode == 1 && getScrollRange() > 0)) && (motionEvent.getSource() & 8194) != 8194) {
                        EdgeEffect edgeEffect = this.A0C;
                        A01(edgeEffect, (-((float) verticalScrollFactorCompat)) / ((float) getHeight()), 0.5f);
                        edgeEffect.onRelease();
                        invalidate();
                        z = 1;
                    }
                    z = 0;
                } else if (verticalScrollFactorCompat > scrollRange) {
                    int overScrollMode2 = getOverScrollMode();
                    if ((overScrollMode2 == 0 || (overScrollMode2 == 1 && getScrollRange() > 0)) && (motionEvent.getSource() & 8194) != 8194) {
                        EdgeEffect edgeEffect2 = this.A0B;
                        A01(edgeEffect2, ((float) (verticalScrollFactorCompat - scrollRange)) / ((float) getHeight()), 0.5f);
                        edgeEffect2.onRelease();
                        invalidate();
                        i2 = 1;
                    }
                    z = i2;
                    i2 = scrollRange;
                } else {
                    i2 = verticalScrollFactorCompat;
                    z = 0;
                }
                if (i2 == scrollY) {
                    return z;
                }
                super.scrollTo(getScrollX(), i2);
                return true;
            }
        }
        return false;
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        boolean z = true;
        if (action == 2 && this.A0H) {
            return true;
        }
        int i = action & 255;
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    int i2 = this.A01;
                    if (i2 != -1) {
                        int findPointerIndex = motionEvent.findPointerIndex(i2);
                        if (findPointerIndex == -1) {
                            StringBuilder sb = new StringBuilder("Invalid pointerId=");
                            sb.append(i2);
                            sb.append(" in onInterceptTouchEvent");
                            Log.e("NestedScrollView", sb.toString());
                        } else {
                            int y = (int) motionEvent.getY(findPointerIndex);
                            if (Math.abs(y - this.A02) > this.A07 && (2 & getNestedScrollAxes()) == 0) {
                                this.A0H = true;
                                this.A02 = y;
                                VelocityTracker velocityTracker = this.A09;
                                if (velocityTracker == null) {
                                    velocityTracker = VelocityTracker.obtain();
                                    this.A09 = velocityTracker;
                                }
                                velocityTracker.addMovement(motionEvent);
                                this.A06 = 0;
                                ViewParent parent = getParent();
                                if (parent != null) {
                                    parent.requestDisallowInterceptTouchEvent(true);
                                }
                            }
                        }
                    }
                } else if (i != 3) {
                    if (i == 6) {
                        A08(motionEvent);
                    }
                }
            }
            this.A0H = false;
            this.A01 = -1;
            VelocityTracker velocityTracker2 = this.A09;
            if (velocityTracker2 != null) {
                velocityTracker2.recycle();
                this.A09 = null;
            }
            if (this.A0D.springBack(getScrollX(), getScrollY(), 0, 0, 0, getScrollRange())) {
                postInvalidateOnAnimation();
            }
            AeR(0);
        } else {
            int y2 = (int) motionEvent.getY();
            int x = (int) motionEvent.getX();
            if (getChildCount() > 0) {
                int scrollY = getScrollY();
                View childAt = getChildAt(0);
                if (y2 >= childAt.getTop() - scrollY && y2 < childAt.getBottom() - scrollY && x >= childAt.getLeft() && x < childAt.getRight()) {
                    this.A02 = y2;
                    this.A01 = motionEvent.getPointerId(0);
                    VelocityTracker velocityTracker3 = this.A09;
                    if (velocityTracker3 == null) {
                        this.A09 = VelocityTracker.obtain();
                    } else {
                        velocityTracker3.clear();
                    }
                    this.A09.addMovement(motionEvent);
                    OverScroller overScroller = this.A0D;
                    overScroller.computeScrollOffset();
                    if (!A0D(motionEvent) && overScroller.isFinished()) {
                        z = false;
                    }
                    this.A0H = z;
                    this.A0M.A03(2, 0);
                }
            }
            if (!A0D(motionEvent) && this.A0D.isFinished()) {
                z = false;
            }
            this.A0H = z;
            VelocityTracker velocityTracker4 = this.A09;
            if (velocityTracker4 != null) {
                velocityTracker4.recycle();
                this.A09 = null;
            }
        }
        return this.A0H;
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        int i5 = 0;
        this.A0J = false;
        View view = this.A0A;
        if (view != null && A02(view, this)) {
            View view2 = this.A0A;
            Rect rect = this.A0L;
            view2.getDrawingRect(rect);
            offsetDescendantRectToMyCoords(view2, rect);
            int A03 = A03(rect);
            if (A03 != 0) {
                scrollBy(0, A03);
            }
        }
        this.A0A = null;
        if (!this.A0I) {
            if (this.A0F != null) {
                scrollTo(getScrollX(), this.A0F.A00);
                this.A0F = null;
            }
            if (getChildCount() > 0) {
                View childAt = getChildAt(0);
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) childAt.getLayoutParams();
                i5 = childAt.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
            }
            int paddingTop = ((i4 - i2) - getPaddingTop()) - getPaddingBottom();
            int scrollY = getScrollY();
            int i6 = scrollY;
            if (paddingTop >= i5 || scrollY < 0) {
                i6 = 0;
            } else if (paddingTop + scrollY > i5) {
                i6 = i5 - paddingTop;
            }
            if (i6 != scrollY) {
                scrollTo(getScrollX(), i6);
            }
        }
        scrollTo(getScrollX(), getScrollY());
        this.A0I = true;
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.A0G && View.MeasureSpec.getMode(i2) != 0 && getChildCount() > 0) {
            View childAt = getChildAt(0);
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int measuredHeight2 = (((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom()) - layoutParams.topMargin) - layoutParams.bottomMargin;
            if (measuredHeight < measuredHeight2) {
                childAt.measure(FrameLayout.getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width), View.MeasureSpec.makeMeasureSpec(measuredHeight2, 1073741824));
            }
        }
    }

    @Override // android.view.ViewParent, android.view.ViewGroup, X.AbstractC016507u
    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        if (z) {
            return false;
        }
        dispatchNestedFling(0.0f, f2, true);
        A04((int) f2);
        return true;
    }

    @Override // android.view.ViewParent, android.view.ViewGroup, X.AbstractC016507u
    public boolean onNestedPreFling(View view, float f, float f2) {
        return this.A0M.A01(f, f2);
    }

    @Override // android.view.ViewParent, android.view.ViewGroup, X.AbstractC016507u
    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        ASw(view, iArr, i, i2, 0);
    }

    @Override // android.view.ViewParent, android.view.ViewGroup, X.AbstractC016507u
    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        A07(i4, null, 0);
    }

    @Override // android.view.ViewParent, android.view.ViewGroup, X.AbstractC016507u
    public void onNestedScrollAccepted(View view, View view2, int i) {
        ASz(view, view2, i, 0);
    }

    @Override // android.view.View
    public void onOverScrolled(int i, int i2, boolean z, boolean z2) {
        super.scrollTo(i, i2);
    }

    @Override // android.view.ViewGroup
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        View findNextFocusFromRect;
        if (i == 2) {
            i = 130;
        } else if (i == 1) {
            i = 33;
        }
        FocusFinder instance = FocusFinder.getInstance();
        if (rect == null) {
            findNextFocusFromRect = instance.findNextFocus(this, null, i);
        } else {
            findNextFocusFromRect = instance.findNextFocusFromRect(this, rect, i);
        }
        if (findNextFocusFromRect == null || (!A0E(findNextFocusFromRect, 0, getHeight()))) {
            return false;
        }
        return findNextFocusFromRect.requestFocus(i, rect);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof AnonymousClass0B1)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        AnonymousClass0B1 r2 = (AnonymousClass0B1) parcelable;
        super.onRestoreInstanceState(r2.getSuperState());
        this.A0F = r2;
        requestLayout();
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        AnonymousClass0B1 r1 = new AnonymousClass0B1(super.onSaveInstanceState());
        r1.A00 = getScrollY();
        return r1;
    }

    @Override // android.view.View
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        AbstractC11770gq r0 = this.A0E;
        if (r0 != null) {
            r0.AVa(this, i, i2, i3, i4);
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        View findFocus = findFocus();
        if (findFocus != null && this != findFocus && A0E(findFocus, 0, i4)) {
            Rect rect = this.A0L;
            findFocus.getDrawingRect(rect);
            offsetDescendantRectToMyCoords(findFocus, rect);
            A05(A03(rect));
        }
    }

    @Override // android.view.ViewParent, android.view.ViewGroup, X.AbstractC016507u
    public boolean onStartNestedScroll(View view, View view2, int i) {
        return (i & 2) != 0;
    }

    @Override // android.view.ViewParent, android.view.ViewGroup, X.AbstractC016507u
    public void onStopNestedScroll(View view) {
        AWp(view, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0232, code lost:
        if (getChildCount() > 0) goto L_0x0234;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01f5  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r23) {
        /*
        // Method dump skipped, instructions count: 645
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.core.widget.NestedScrollView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.ViewParent, android.view.ViewGroup
    public void requestChildFocus(View view, View view2) {
        if (!this.A0J) {
            Rect rect = this.A0L;
            view2.getDrawingRect(rect);
            offsetDescendantRectToMyCoords(view2, rect);
            int A03 = A03(rect);
            if (A03 != 0) {
                scrollBy(0, A03);
            }
        } else {
            this.A0A = view2;
        }
        super.requestChildFocus(view, view2);
    }

    @Override // android.view.ViewParent, android.view.ViewGroup
    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        int A03 = A03(rect);
        boolean z2 = false;
        if (A03 != 0) {
            z2 = true;
            if (z) {
                scrollBy(0, A03);
            } else {
                A06(0, A03, false);
                return true;
            }
        }
        return z2;
    }

    @Override // android.view.ViewParent, android.view.ViewGroup
    public void requestDisallowInterceptTouchEvent(boolean z) {
        VelocityTracker velocityTracker;
        if (z && (velocityTracker = this.A09) != null) {
            velocityTracker.recycle();
            this.A09 = null;
        }
        super.requestDisallowInterceptTouchEvent(z);
    }

    @Override // android.view.ViewParent, android.view.View
    public void requestLayout() {
        this.A0J = true;
        super.requestLayout();
    }

    @Override // android.view.View
    public void scrollTo(int i, int i2) {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) childAt.getLayoutParams();
            int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
            int width2 = childAt.getWidth() + layoutParams.leftMargin + layoutParams.rightMargin;
            int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
            int height2 = childAt.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
            if (width >= width2 || i < 0) {
                i = 0;
            } else if (width + i > width2) {
                i = width2 - width;
            }
            if (height >= height2 || i2 < 0) {
                i2 = 0;
            } else if (height + i2 > height2) {
                i2 = height2 - height;
            }
            if (i != getScrollX() || i2 != getScrollY()) {
                super.scrollTo(i, i2);
            }
        }
    }

    public void setFillViewport(boolean z) {
        if (z != this.A0G) {
            this.A0G = z;
            requestLayout();
        }
    }

    @Override // android.view.View, X.AnonymousClass02E
    public void setNestedScrollingEnabled(boolean z) {
        AnonymousClass0UY r1 = this.A0M;
        if (r1.A02) {
            AnonymousClass028.A0T(r1.A04);
        }
        r1.A02 = z;
    }

    public void setOnScrollChangeListener(AbstractC11770gq r1) {
        this.A0E = r1;
    }

    public void setSmoothScrollingEnabled(boolean z) {
        this.A0K = z;
    }

    @Override // android.view.View
    public boolean startNestedScroll(int i) {
        return this.A0M.A03(i, 0);
    }

    @Override // android.view.View, X.AnonymousClass02E
    public void stopNestedScroll() {
        AeR(0);
    }
}
