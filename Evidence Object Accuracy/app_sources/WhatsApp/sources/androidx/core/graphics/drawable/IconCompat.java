package androidx.core.graphics.drawable;

import X.AnonymousClass00X;
import X.AnonymousClass0UI;
import X.C015607k;
import X.C05650Qk;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.versionedparcelable.CustomVersionedParcelable;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/* loaded from: classes.dex */
public class IconCompat extends CustomVersionedParcelable {
    public static final PorterDuff.Mode A0A = PorterDuff.Mode.SRC_IN;
    public int A00 = 0;
    public int A01 = 0;
    public int A02 = -1;
    public ColorStateList A03 = null;
    public PorterDuff.Mode A04 = A0A;
    public Parcelable A05 = null;
    public Object A06;
    public String A07;
    public String A08 = null;
    public byte[] A09 = null;

    public IconCompat() {
    }

    public IconCompat(int i) {
        this.A02 = i;
    }

    public static Resources A00(Context context, String str) {
        if ("android".equals(str)) {
            return Resources.getSystem();
        }
        PackageManager packageManager = context.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, DefaultCrypto.BUFFER_SIZE);
            if (applicationInfo != null) {
                return packageManager.getResourcesForApplication(applicationInfo);
            }
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("IconCompat", String.format("Unable to find pkg=%s for icon", str), e);
            return null;
        }
    }

    public static Bitmap A01(Bitmap bitmap, boolean z) {
        int min = (int) (((float) Math.min(bitmap.getWidth(), bitmap.getHeight())) * 0.6666667f);
        Bitmap createBitmap = Bitmap.createBitmap(min, min, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint(3);
        float f = (float) min;
        float f2 = 0.5f * f;
        float f3 = 0.9166667f * f2;
        if (z) {
            float f4 = 0.010416667f * f;
            paint.setColor(0);
            paint.setShadowLayer(f4, 0.0f, f * 0.020833334f, 1023410176);
            canvas.drawCircle(f2, f2, f3, paint);
            paint.setShadowLayer(f4, 0.0f, 0.0f, 503316480);
            canvas.drawCircle(f2, f2, f3, paint);
            paint.clearShadowLayer();
        }
        paint.setColor(-16777216);
        Shader.TileMode tileMode = Shader.TileMode.CLAMP;
        BitmapShader bitmapShader = new BitmapShader(bitmap, tileMode, tileMode);
        Matrix matrix = new Matrix();
        matrix.setTranslate(((float) (-(bitmap.getWidth() - min))) / 2.0f, ((float) (-(bitmap.getHeight() - min))) / 2.0f);
        bitmapShader.setLocalMatrix(matrix);
        paint.setShader(bitmapShader);
        canvas.drawCircle(f2, f2, f3, paint);
        canvas.setBitmap(null);
        return createBitmap;
    }

    public static IconCompat A02(Resources resources, String str, int i) {
        if (i != 0) {
            IconCompat iconCompat = new IconCompat(2);
            iconCompat.A00 = i;
            if (resources != null) {
                try {
                    iconCompat.A06 = resources.getResourceName(i);
                } catch (Resources.NotFoundException unused) {
                    throw new IllegalArgumentException("Icon resource cannot be found");
                }
            } else {
                iconCompat.A06 = str;
            }
            iconCompat.A07 = str;
            return iconCompat;
        }
        throw new IllegalArgumentException("Drawable resource ID must not be 0");
    }

    public int A03() {
        int i = this.A02;
        if (i == -1) {
            if (Build.VERSION.SDK_INT >= 23) {
                return AnonymousClass0UI.A00(this.A06);
            }
        } else if (i == 2) {
            return this.A00;
        }
        StringBuilder sb = new StringBuilder("called getResId() on ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }

    public int A04() {
        int i = this.A02;
        return (i != -1 || Build.VERSION.SDK_INT < 23) ? i : AnonymousClass0UI.A01(this.A06);
    }

    public Bitmap A05() {
        Object obj;
        int i = this.A02;
        if (i == -1) {
            if (Build.VERSION.SDK_INT >= 23) {
                obj = this.A06;
                if (!(obj instanceof Bitmap)) {
                    return null;
                }
                return (Bitmap) obj;
            }
            StringBuilder sb = new StringBuilder("called getBitmap() on ");
            sb.append(this);
            throw new IllegalStateException(sb.toString());
        } else if (i == 1) {
            obj = this.A06;
            return (Bitmap) obj;
        } else {
            if (i == 5) {
                return A01((Bitmap) this.A06, true);
            }
            StringBuilder sb = new StringBuilder("called getBitmap() on ");
            sb.append(this);
            throw new IllegalStateException(sb.toString());
        }
    }

    public Drawable A06(Context context) {
        Resources resources;
        Bitmap bitmap;
        Resources resources2;
        Bitmap bitmap2;
        A0D(context);
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            return AnonymousClass0UI.A02(context, A08(context));
        }
        Drawable drawable = null;
        switch (this.A02) {
            case 1:
                resources2 = context.getResources();
                bitmap2 = (Bitmap) this.A06;
                drawable = new BitmapDrawable(resources2, bitmap2);
                break;
            case 2:
                String A0C = A0C();
                if (TextUtils.isEmpty(A0C)) {
                    A0C = context.getPackageName();
                }
                try {
                    drawable = AnonymousClass00X.A04(context.getTheme(), A00(context, A0C), this.A00);
                    break;
                } catch (RuntimeException e) {
                    Log.e("IconCompat", String.format("Unable to load resource 0x%08x from pkg=%s", Integer.valueOf(this.A00), this.A06), e);
                    break;
                }
            case 3:
                resources2 = context.getResources();
                bitmap2 = BitmapFactory.decodeByteArray((byte[]) this.A06, this.A00, this.A01);
                drawable = new BitmapDrawable(resources2, bitmap2);
                break;
            case 4:
                InputStream A0B = A0B(context);
                if (A0B != null) {
                    resources = context.getResources();
                    bitmap = BitmapFactory.decodeStream(A0B);
                    drawable = new BitmapDrawable(resources, bitmap);
                    break;
                }
                break;
            case 5:
                resources2 = context.getResources();
                bitmap2 = A01((Bitmap) this.A06, false);
                drawable = new BitmapDrawable(resources2, bitmap2);
                break;
            case 6:
                InputStream A0B2 = A0B(context);
                if (A0B2 != null) {
                    if (i < 26) {
                        resources = context.getResources();
                        bitmap = A01(BitmapFactory.decodeStream(A0B2), false);
                        drawable = new BitmapDrawable(resources, bitmap);
                        break;
                    } else {
                        drawable = C05650Qk.A00(null, new BitmapDrawable(context.getResources(), BitmapFactory.decodeStream(A0B2)));
                        break;
                    }
                }
                break;
        }
        if (drawable == null) {
            return drawable;
        }
        if (this.A03 == null && this.A04 == A0A) {
            return drawable;
        }
        drawable.mutate();
        C015607k.A04(this.A03, drawable);
        C015607k.A07(this.A04, drawable);
        return drawable;
    }

    @Deprecated
    public Icon A07() {
        return A08(null);
    }

    public Icon A08(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            return AnonymousClass0UI.A03(context, this);
        }
        throw new UnsupportedOperationException("This method is only supported on API level 23+");
    }

    public Uri A09() {
        int i = this.A02;
        if (i == -1) {
            if (Build.VERSION.SDK_INT >= 23) {
                return AnonymousClass0UI.A04(this.A06);
            }
        } else if (i == 4 || i == 6) {
            return Uri.parse((String) this.A06);
        }
        StringBuilder sb = new StringBuilder("called getUri() on ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }

    public Bundle A0A() {
        Parcelable parcelable;
        Bundle bundle = new Bundle();
        switch (this.A02) {
            case -1:
                parcelable = (Parcelable) this.A06;
                bundle.putParcelable("obj", parcelable);
                break;
            case 0:
            default:
                throw new IllegalArgumentException("Invalid icon");
            case 1:
            case 5:
                parcelable = (Bitmap) this.A06;
                bundle.putParcelable("obj", parcelable);
                break;
            case 2:
            case 4:
            case 6:
                bundle.putString("obj", (String) this.A06);
                break;
            case 3:
                bundle.putByteArray("obj", (byte[]) this.A06);
                break;
        }
        bundle.putInt("type", this.A02);
        bundle.putInt("int1", this.A00);
        bundle.putInt("int2", this.A01);
        bundle.putString("string1", this.A07);
        ColorStateList colorStateList = this.A03;
        if (colorStateList != null) {
            bundle.putParcelable("tint_list", colorStateList);
        }
        PorterDuff.Mode mode = this.A04;
        if (mode != A0A) {
            bundle.putString("tint_mode", mode.name());
        }
        return bundle;
    }

    public InputStream A0B(Context context) {
        StringBuilder sb;
        String str;
        Uri A09 = A09();
        String scheme = A09.getScheme();
        if ("content".equals(scheme) || "file".equals(scheme)) {
            try {
                return context.getContentResolver().openInputStream(A09);
            } catch (Exception e) {
                e = e;
                sb = new StringBuilder();
                str = "Unable to load image from URI: ";
            }
        } else {
            try {
                return new FileInputStream(new File((String) this.A06));
            } catch (FileNotFoundException e2) {
                e = e2;
                sb = new StringBuilder();
                str = "Unable to load image from path: ";
            }
        }
        sb.append(str);
        sb.append(A09);
        Log.w("IconCompat", sb.toString(), e);
        return null;
    }

    public String A0C() {
        int i = this.A02;
        if (i == -1) {
            if (Build.VERSION.SDK_INT >= 23) {
                return AnonymousClass0UI.A05(this.A06);
            }
        } else if (i == 2) {
            String str = this.A07;
            if (str == null || TextUtils.isEmpty(str)) {
                return ((String) this.A06).split(":", -1)[0];
            }
            return this.A07;
        }
        StringBuilder sb = new StringBuilder("called getResPackage() on ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }

    public void A0D(Context context) {
        Object obj;
        if (this.A02 == 2 && (obj = this.A06) != null) {
            String str = (String) obj;
            if (str.contains(":")) {
                String str2 = str.split(":", -1)[1];
                String str3 = str2.split("/", -1)[0];
                String str4 = str2.split("/", -1)[1];
                String str5 = str.split(":", -1)[0];
                if ("0_resource_name_obfuscated".equals(str4)) {
                    Log.i("IconCompat", "Found obfuscated resource, not trying to update resource id for it");
                    return;
                }
                String A0C = A0C();
                int identifier = A00(context, A0C).getIdentifier(str4, str3, str5);
                if (this.A00 != identifier) {
                    StringBuilder sb = new StringBuilder("Id has changed for ");
                    sb.append(A0C);
                    sb.append(" ");
                    sb.append(str);
                    Log.i("IconCompat", sb.toString());
                    this.A00 = identifier;
                }
            }
        }
    }

    public String toString() {
        String str;
        int i = this.A02;
        if (i == -1) {
            return String.valueOf(this.A06);
        }
        StringBuilder sb = new StringBuilder("Icon(typ=");
        switch (i) {
            case 1:
                str = "BITMAP";
                break;
            case 2:
                str = "RESOURCE";
                break;
            case 3:
                str = "DATA";
                break;
            case 4:
                str = "URI";
                break;
            case 5:
                str = "BITMAP_MASKABLE";
                break;
            case 6:
                str = "URI_MASKABLE";
                break;
            default:
                str = "UNKNOWN";
                break;
        }
        sb.append(str);
        switch (i) {
            case 1:
            case 5:
                sb.append(" size=");
                sb.append(((Bitmap) this.A06).getWidth());
                sb.append("x");
                sb.append(((Bitmap) this.A06).getHeight());
                break;
            case 2:
                sb.append(" pkg=");
                sb.append(this.A07);
                sb.append(" id=");
                sb.append(String.format("0x%08x", Integer.valueOf(A03())));
                break;
            case 3:
                sb.append(" len=");
                sb.append(this.A00);
                int i2 = this.A01;
                if (i2 != 0) {
                    sb.append(" off=");
                    sb.append(i2);
                    break;
                }
                break;
            case 4:
            case 6:
                sb.append(" uri=");
                sb.append(this.A06);
                break;
        }
        ColorStateList colorStateList = this.A03;
        if (colorStateList != null) {
            sb.append(" tint=");
            sb.append(colorStateList);
        }
        PorterDuff.Mode mode = this.A04;
        if (mode != A0A) {
            sb.append(" mode=");
            sb.append(mode);
        }
        sb.append(")");
        return sb.toString();
    }
}
