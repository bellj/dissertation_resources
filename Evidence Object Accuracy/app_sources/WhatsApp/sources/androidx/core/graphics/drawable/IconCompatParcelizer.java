package androidx.core.graphics.drawable;

import X.AnonymousClass0GE;
import X.AnonymousClass0QP;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Parcel;
import android.os.Parcelable;
import java.nio.charset.Charset;

/* loaded from: classes.dex */
public class IconCompatParcelizer {
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static IconCompat read(AnonymousClass0QP r4) {
        IconCompat iconCompat = new IconCompat();
        iconCompat.A02 = r4.A00(iconCompat.A02, 1);
        byte[] bArr = iconCompat.A09;
        if (r4.A09(2)) {
            Parcel parcel = ((AnonymousClass0GE) r4).A05;
            int readInt = parcel.readInt();
            if (readInt < 0) {
                bArr = null;
            } else {
                bArr = new byte[readInt];
                parcel.readByteArray(bArr);
            }
        }
        iconCompat.A09 = bArr;
        iconCompat.A05 = r4.A01(iconCompat.A05, 3);
        iconCompat.A00 = r4.A00(iconCompat.A00, 4);
        iconCompat.A01 = r4.A00(iconCompat.A01, 5);
        iconCompat.A03 = (ColorStateList) r4.A01(iconCompat.A03, 6);
        String str = iconCompat.A08;
        if (r4.A09(7)) {
            str = ((AnonymousClass0GE) r4).A05.readString();
        }
        iconCompat.A08 = str;
        String str2 = iconCompat.A07;
        if (r4.A09(8)) {
            str2 = ((AnonymousClass0GE) r4).A05.readString();
        }
        iconCompat.A07 = str2;
        iconCompat.A04 = PorterDuff.Mode.valueOf(iconCompat.A08);
        switch (iconCompat.A02) {
            case -1:
                Parcelable parcelable = iconCompat.A05;
                if (parcelable != null) {
                    iconCompat.A06 = parcelable;
                    return iconCompat;
                }
                throw new IllegalArgumentException("Invalid icon");
            case 1:
            case 5:
                Parcelable parcelable2 = iconCompat.A05;
                if (parcelable2 != null) {
                    iconCompat.A06 = parcelable2;
                    return iconCompat;
                }
                byte[] bArr2 = iconCompat.A09;
                iconCompat.A06 = bArr2;
                iconCompat.A02 = 3;
                iconCompat.A00 = 0;
                iconCompat.A01 = bArr2.length;
                return iconCompat;
            case 2:
            case 4:
            case 6:
                String str3 = new String(iconCompat.A09, Charset.forName("UTF-16"));
                iconCompat.A06 = str3;
                if (iconCompat.A02 == 2 && iconCompat.A07 == null) {
                    iconCompat.A07 = str3.split(":", -1)[0];
                    return iconCompat;
                }
                break;
            case 3:
                iconCompat.A06 = iconCompat.A09;
                return iconCompat;
        }
        return iconCompat;
    }

    public static void write(IconCompat iconCompat, AnonymousClass0QP r5) {
        byte[] bArr;
        String str;
        iconCompat.A08 = iconCompat.A04.name();
        switch (iconCompat.A02) {
            case -1:
            case 1:
            case 5:
                iconCompat.A05 = (Parcelable) iconCompat.A06;
                break;
            case 2:
                str = (String) iconCompat.A06;
                bArr = str.getBytes(Charset.forName("UTF-16"));
                iconCompat.A09 = bArr;
                break;
            case 3:
                bArr = (byte[]) iconCompat.A06;
                iconCompat.A09 = bArr;
                break;
            case 4:
            case 6:
                str = iconCompat.A06.toString();
                bArr = str.getBytes(Charset.forName("UTF-16"));
                iconCompat.A09 = bArr;
                break;
        }
        int i = iconCompat.A02;
        if (-1 != i) {
            r5.A06(i, 1);
        }
        byte[] bArr2 = iconCompat.A09;
        if (bArr2 != null) {
            r5.A05(2);
            Parcel parcel = ((AnonymousClass0GE) r5).A05;
            parcel.writeInt(bArr2.length);
            parcel.writeByteArray(bArr2);
        }
        Parcelable parcelable = iconCompat.A05;
        if (parcelable != null) {
            r5.A07(parcelable, 3);
        }
        int i2 = iconCompat.A00;
        if (i2 != 0) {
            r5.A06(i2, 4);
        }
        int i3 = iconCompat.A01;
        if (i3 != 0) {
            r5.A06(i3, 5);
        }
        ColorStateList colorStateList = iconCompat.A03;
        if (colorStateList != null) {
            r5.A07(colorStateList, 6);
        }
        String str2 = iconCompat.A08;
        if (str2 != null) {
            r5.A05(7);
            ((AnonymousClass0GE) r5).A05.writeString(str2);
        }
        String str3 = iconCompat.A07;
        if (str3 != null) {
            r5.A05(8);
            ((AnonymousClass0GE) r5).A05.writeString(str3);
        }
    }
}
