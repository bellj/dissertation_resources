package androidx.versionedparcelable;

import X.AbstractC007203r;
import X.AnonymousClass0GE;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* loaded from: classes.dex */
public class ParcelImpl implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(28);
    public final AbstractC007203r A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public ParcelImpl(Parcel parcel) {
        this.A00 = new AnonymousClass0GE(parcel).A03();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        new AnonymousClass0GE(parcel).A08(this.A00);
    }
}
