package androidx.cardview.widget;

import X.AbstractC11670gf;
import X.AbstractC12750iR;
import X.AnonymousClass0XY;
import X.AnonymousClass0XZ;
import X.C02460Cj;
import X.C04260La;
import X.C07190Xa;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;

/* loaded from: classes.dex */
public class CardView extends FrameLayout {
    public static final AbstractC12750iR A07;
    public static final int[] A08 = {16842801};
    public int A00;
    public int A01;
    public boolean A02;
    public boolean A03;
    public final Rect A04;
    public final Rect A05;
    public final AbstractC11670gf A06;

    @Override // android.view.View
    public void setPadding(int i, int i2, int i3, int i4) {
    }

    @Override // android.view.View
    public void setPaddingRelative(int i, int i2, int i3, int i4) {
    }

    static {
        AbstractC12750iR r0;
        A08 = new int[]{16842801};
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            r0 = new AnonymousClass0XZ();
            A07 = r0;
        } else if (i >= 17) {
            r0 = new C02460Cj();
            A07 = r0;
        } else {
            r0 = new C07190Xa();
            A07 = r0;
        }
        r0.AIq();
    }

    public CardView(Context context) {
        this(context, null);
    }

    public CardView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.cardViewStyle);
    }

    public CardView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ColorStateList valueOf;
        Rect rect = new Rect();
        this.A04 = rect;
        this.A05 = new Rect();
        AnonymousClass0XY r8 = new AnonymousClass0XY(this);
        this.A06 = r8;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C04260La.A00, i, R.style.CardView);
        if (obtainStyledAttributes.hasValue(2)) {
            valueOf = obtainStyledAttributes.getColorStateList(2);
        } else {
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(A08);
            int color = obtainStyledAttributes2.getColor(0, 0);
            obtainStyledAttributes2.recycle();
            float[] fArr = new float[3];
            Color.colorToHSV(color, fArr);
            valueOf = ColorStateList.valueOf(getResources().getColor(fArr[2] > 0.5f ? R.color.cardview_light_background : R.color.cardview_dark_background));
        }
        float dimension = obtainStyledAttributes.getDimension(3, 0.0f);
        float dimension2 = obtainStyledAttributes.getDimension(4, 0.0f);
        float dimension3 = obtainStyledAttributes.getDimension(5, 0.0f);
        this.A02 = obtainStyledAttributes.getBoolean(7, false);
        this.A03 = obtainStyledAttributes.getBoolean(6, true);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(8, 0);
        rect.left = obtainStyledAttributes.getDimensionPixelSize(10, dimensionPixelSize);
        rect.top = obtainStyledAttributes.getDimensionPixelSize(12, dimensionPixelSize);
        rect.right = obtainStyledAttributes.getDimensionPixelSize(11, dimensionPixelSize);
        rect.bottom = obtainStyledAttributes.getDimensionPixelSize(9, dimensionPixelSize);
        dimension3 = dimension2 > dimension3 ? dimension2 : dimension3;
        this.A01 = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.A00 = obtainStyledAttributes.getDimensionPixelSize(1, 0);
        obtainStyledAttributes.recycle();
        A07.AIw(context, valueOf, r8, dimension, dimension2, dimension3);
    }

    public ColorStateList getCardBackgroundColor() {
        return A07.AAn(this.A06);
    }

    public float getCardElevation() {
        return A07.ACg(this.A06);
    }

    public int getContentPaddingBottom() {
        return this.A04.bottom;
    }

    public int getContentPaddingLeft() {
        return this.A04.left;
    }

    public int getContentPaddingRight() {
        return this.A04.right;
    }

    public int getContentPaddingTop() {
        return this.A04.top;
    }

    public float getMaxCardElevation() {
        return A07.AE8(this.A06);
    }

    public boolean getPreventCornerOverlap() {
        return this.A03;
    }

    public float getRadius() {
        return A07.AG3(this.A06);
    }

    public boolean getUseCompatPadding() {
        return this.A02;
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        AbstractC12750iR r6 = A07;
        if (!(r6 instanceof AnonymousClass0XZ)) {
            int mode = View.MeasureSpec.getMode(i);
            if (mode == Integer.MIN_VALUE || mode == 1073741824) {
                i = View.MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil((double) r6.AEW(this.A06)), View.MeasureSpec.getSize(i)), mode);
            }
            int mode2 = View.MeasureSpec.getMode(i2);
            if (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) {
                i2 = View.MeasureSpec.makeMeasureSpec(Math.max((int) Math.ceil((double) r6.AEU(this.A06)), View.MeasureSpec.getSize(i2)), mode2);
            }
        }
        super.onMeasure(i, i2);
    }

    public void setCardBackgroundColor(int i) {
        A07.Abm(ColorStateList.valueOf(i), this.A06);
    }

    public void setCardBackgroundColor(ColorStateList colorStateList) {
        A07.Abm(colorStateList, this.A06);
    }

    public void setCardElevation(float f) {
        A07.Ac6(this.A06, f);
    }

    public void setMaxCardElevation(float f) {
        A07.AcJ(this.A06, f);
    }

    @Override // android.view.View
    public void setMinimumHeight(int i) {
        this.A00 = i;
        super.setMinimumHeight(i);
    }

    @Override // android.view.View
    public void setMinimumWidth(int i) {
        this.A01 = i;
        super.setMinimumWidth(i);
    }

    public void setPreventCornerOverlap(boolean z) {
        if (z != this.A03) {
            this.A03 = z;
            A07.AUD(this.A06);
        }
    }

    public void setRadius(float f) {
        A07.Aci(this.A06, f);
    }

    public void setUseCompatPadding(boolean z) {
        if (this.A02 != z) {
            this.A02 = z;
            A07.AOK(this.A06);
        }
    }
}
