package androidx.coordinatorlayout.widget;

import X.AbstractC009304r;
import X.AbstractC015707l;
import X.AbstractC016407t;
import X.AbstractC12350hm;
import X.AnonymousClass00O;
import X.AnonymousClass00T;
import X.AnonymousClass028;
import X.AnonymousClass07F;
import X.AnonymousClass0B5;
import X.AnonymousClass0DQ;
import X.AnonymousClass0E4;
import X.AnonymousClass0M9;
import X.AnonymousClass0OY;
import X.AnonymousClass0RK;
import X.AnonymousClass0WX;
import X.AnonymousClass0Y7;
import X.C015607k;
import X.C018408o;
import X.C04740Mw;
import X.C05660Ql;
import X.C10290eN;
import X.ViewTreeObserver$OnPreDrawListenerC06980Wf;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.whatsapp.R;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/* loaded from: classes.dex */
public class CoordinatorLayout extends ViewGroup implements AbstractC016407t {
    public static final AbstractC12350hm A0I = new AnonymousClass0DQ(12);
    public static final String A0J;
    public static final ThreadLocal A0K = new ThreadLocal();
    public static final Comparator A0L = new C10290eN();
    public static final Class[] A0M = {Context.class, AttributeSet.class};
    public Drawable A00;
    public View A01;
    public View A02;
    public ViewGroup.OnHierarchyChangeListener A03;
    public ViewTreeObserver$OnPreDrawListenerC06980Wf A04;
    public AnonymousClass07F A05;
    public C018408o A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public int[] A0B;
    public final AnonymousClass0OY A0C;
    public final C04740Mw A0D;
    public final List A0E;
    public final List A0F;
    public final List A0G;
    public final int[] A0H;

    @Deprecated
    /* loaded from: classes.dex */
    public @interface DefaultBehavior {
        Class value();
    }

    static {
        Package r1 = CoordinatorLayout.class.getPackage();
        String str = null;
        if (r1 != null) {
            str = r1.getName();
        }
        A0J = str;
        if (Build.VERSION.SDK_INT >= 21) {
        }
    }

    public CoordinatorLayout(Context context) {
        this(context, null);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.coordinatorLayoutStyle);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes;
        this.A0E = new ArrayList();
        this.A0C = new AnonymousClass0OY();
        this.A0G = new ArrayList();
        this.A0F = new ArrayList();
        this.A0H = new int[2];
        this.A0D = new C04740Mw();
        if (i == 0) {
            obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0M9.A00, 0, 2131952679);
        } else {
            obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0M9.A00, i, 0);
        }
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        if (resourceId != 0) {
            Resources resources = context.getResources();
            this.A0B = resources.getIntArray(resourceId);
            float f = resources.getDisplayMetrics().density;
            int[] iArr = this.A0B;
            int length = iArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = (int) (((float) iArr[i2]) * f);
            }
        }
        this.A00 = obtainStyledAttributes.getDrawable(1);
        obtainStyledAttributes.recycle();
        A09();
        super.setOnHierarchyChangeListener(new AnonymousClass0WX(this));
    }

    public static Rect A00() {
        Rect rect = (Rect) A0I.A5a();
        return rect == null ? new Rect() : rect;
    }

    public static AnonymousClass0B5 A01(View view) {
        AnonymousClass0B5 r5 = (AnonymousClass0B5) view.getLayoutParams();
        if (!r5.A0B) {
            Class<?> cls = view.getClass();
            while (true) {
                if (cls == null) {
                    break;
                }
                DefaultBehavior defaultBehavior = (DefaultBehavior) cls.getAnnotation(DefaultBehavior.class);
                if (defaultBehavior == null) {
                    cls = cls.getSuperclass();
                } else {
                    try {
                        r5.A00((AbstractC009304r) defaultBehavior.value().getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                        break;
                    } catch (Exception e) {
                        StringBuilder sb = new StringBuilder("Default behavior class ");
                        sb.append(defaultBehavior.value().getName());
                        sb.append(" could not be instantiated. Did you forget");
                        sb.append(" a default constructor?");
                        Log.e("CoordinatorLayout", sb.toString(), e);
                    }
                }
            }
            r5.A0B = true;
        }
        return r5;
    }

    public static void A02(Rect rect) {
        rect.setEmpty();
        A0I.Aa6(rect);
    }

    public static final void A03(Rect rect, Rect rect2, AnonymousClass0B5 r11, int i, int i2, int i3) {
        int width;
        int height;
        int i4 = r11.A02;
        if (i4 == 0) {
            i4 = 17;
        }
        int A00 = C05660Ql.A00(i4, i);
        int i5 = r11.A00;
        if ((i5 & 7) == 0) {
            i5 |= 8388611;
        }
        if ((i5 & 112) == 0) {
            i5 |= 48;
        }
        int A002 = C05660Ql.A00(i5, i);
        int i6 = A00 & 7;
        int i7 = A00 & 112;
        int i8 = A002 & 7;
        int i9 = A002 & 112;
        if (i8 == 1) {
            width = rect.left + (rect.width() >> 1);
        } else if (i8 != 5) {
            width = rect.left;
        } else {
            width = rect.right;
        }
        if (i9 == 16) {
            height = rect.top + (rect.height() >> 1);
        } else if (i9 != 80) {
            height = rect.top;
        } else {
            height = rect.bottom;
        }
        if (i6 == 1) {
            width -= i2 >> 1;
        } else if (i6 != 5) {
            width -= i2;
        }
        if (i7 == 16) {
            height -= i3 >> 1;
        } else if (i7 != 80) {
            height -= i3;
        }
        rect2.set(width, height, i2 + width, i3 + height);
    }

    public static final void A04(View view, int i) {
        AnonymousClass0B5 r1 = (AnonymousClass0B5) view.getLayoutParams();
        int i2 = r1.A06;
        if (i2 != i) {
            AnonymousClass028.A0X(view, i - i2);
            r1.A06 = i;
        }
    }

    public static final void A05(View view, int i) {
        AnonymousClass0B5 r1 = (AnonymousClass0B5) view.getLayoutParams();
        int i2 = r1.A07;
        if (i2 != i) {
            AnonymousClass028.A0Y(view, i - i2);
            r1.A07 = i;
        }
    }

    public final int A06(int i) {
        StringBuilder sb;
        int[] iArr = this.A0B;
        if (iArr == null) {
            sb = new StringBuilder("No keylines defined for ");
            sb.append(this);
            sb.append(" - attempted index lookup ");
            sb.append(i);
        } else if (i >= 0 && i < iArr.length) {
            return iArr[i];
        } else {
            sb = new StringBuilder("Keyline index ");
            sb.append(i);
            sb.append(" out of range for ");
            sb.append(this);
        }
        Log.e("CoordinatorLayout", sb.toString());
        return 0;
    }

    public List A07(View view) {
        AnonymousClass00O r5 = this.A0C.A00;
        int size = r5.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            AbstractCollection abstractCollection = (AbstractCollection) r5.A02[(i << 1) + 1];
            if (abstractCollection != null && abstractCollection.contains(view)) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(r5.A02[i << 1]);
            }
        }
        List list = this.A0F;
        list.clear();
        if (arrayList != null) {
            list.addAll(arrayList);
        }
        return list;
    }

    public final void A08() {
        View childAt;
        int A05;
        int A00;
        AbstractC009304r r0;
        List list = this.A0E;
        list.clear();
        AnonymousClass0OY r7 = this.A0C;
        AnonymousClass00O r6 = r7.A00;
        int size = r6.size();
        for (int i = 0; i < size; i++) {
            AbstractCollection abstractCollection = (AbstractCollection) r6.A02[(i << 1) + 1];
            if (abstractCollection != null) {
                abstractCollection.clear();
                r7.A01.Aa6(abstractCollection);
            }
        }
        r6.clear();
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt2 = getChildAt(i2);
            AnonymousClass0B5 A01 = A01(childAt2);
            if (A01.A05 == -1) {
                A01.A08 = null;
                A01.A09 = null;
            } else {
                View view = A01.A09;
                if (view != null && view.getId() == A01.A05) {
                    View view2 = A01.A09;
                    for (ViewParent parent = view2.getParent(); parent != this; parent = parent.getParent()) {
                        if (parent == null || parent == childAt2) {
                            A01.A08 = null;
                            A01.A09 = null;
                        } else {
                            if (parent instanceof View) {
                                view2 = (View) parent;
                            }
                        }
                    }
                    A01.A08 = view2;
                }
                int i3 = A01.A05;
                View findViewById = findViewById(i3);
                A01.A09 = findViewById;
                if (findViewById != null) {
                    if (findViewById != this) {
                        ViewParent parent2 = findViewById.getParent();
                        while (parent2 != this && parent2 != null) {
                            if (parent2 != childAt2) {
                                if (parent2 instanceof View) {
                                    findViewById = (View) parent2;
                                }
                                parent2 = parent2.getParent();
                            } else if (!isInEditMode()) {
                                throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                            }
                        }
                        A01.A08 = findViewById;
                    } else if (!isInEditMode()) {
                        throw new IllegalStateException("View can not be anchored to the the parent CoordinatorLayout");
                    }
                } else if (!isInEditMode()) {
                    StringBuilder sb = new StringBuilder("Could not find CoordinatorLayout descendant view with id ");
                    sb.append(getResources().getResourceName(i3));
                    sb.append(" to anchor view ");
                    sb.append(childAt2);
                    throw new IllegalStateException(sb.toString());
                }
                A01.A08 = null;
                A01.A09 = null;
            }
            if (!r6.containsKey(childAt2)) {
                r6.put(childAt2, null);
            }
            for (int i4 = 0; i4 < childCount; i4++) {
                if (i4 != i2 && ((childAt = getChildAt(i4)) == A01.A08 || (((A00 = C05660Ql.A00(((AnonymousClass0B5) childAt.getLayoutParams()).A03, (A05 = AnonymousClass028.A05(this)))) != 0 && (C05660Ql.A00(A01.A01, A05) & A00) == A00) || ((r0 = A01.A0A) != null && r0.A05(childAt2, childAt, this))))) {
                    if (!r6.containsKey(childAt) && !r6.containsKey(childAt)) {
                        r6.put(childAt, null);
                    }
                    if (!r6.containsKey(childAt) || !r6.containsKey(childAt2)) {
                        throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
                    }
                    AbstractCollection abstractCollection2 = (AbstractCollection) r6.get(childAt);
                    if (abstractCollection2 == null) {
                        abstractCollection2 = (AbstractCollection) r7.A01.A5a();
                        if (abstractCollection2 == null) {
                            abstractCollection2 = new ArrayList();
                        }
                        r6.put(childAt, abstractCollection2);
                    }
                    abstractCollection2.add(childAt2);
                }
            }
        }
        ArrayList arrayList = r7.A02;
        arrayList.clear();
        HashSet hashSet = r7.A03;
        hashSet.clear();
        int size2 = r6.size();
        for (int i5 = 0; i5 < size2; i5++) {
            r7.A00(r6.A02[i5 << 1], arrayList, hashSet);
        }
        list.addAll(arrayList);
        Collections.reverse(list);
    }

    public final void A09() {
        if (Build.VERSION.SDK_INT < 21) {
            return;
        }
        if (getFitsSystemWindows()) {
            AnonymousClass07F r0 = this.A05;
            if (r0 == null) {
                r0 = new AnonymousClass0Y7(this);
                this.A05 = r0;
            }
            AnonymousClass028.A0h(this, r0);
            setSystemUiVisibility(1280);
            return;
        }
        AnonymousClass028.A0h(this, null);
    }

    public final void A0A(int i) {
        boolean A06;
        boolean z;
        boolean z2;
        int width;
        int i2;
        int i3;
        int i4;
        int height;
        int i5;
        int i6;
        int i7;
        AbstractC009304r r8;
        int A05 = AnonymousClass028.A05(this);
        List list = this.A0E;
        int size = list.size();
        Rect A00 = A00();
        Rect A002 = A00();
        Rect A003 = A00();
        for (int i8 = 0; i8 < size; i8++) {
            View view = (View) list.get(i8);
            AnonymousClass0B5 r10 = (AnonymousClass0B5) view.getLayoutParams();
            if (i != 0 || view.getVisibility() != 8) {
                for (int i9 = 0; i9 < i8; i9++) {
                    if (r10.A08 == list.get(i9)) {
                        AnonymousClass0B5 r13 = (AnonymousClass0B5) view.getLayoutParams();
                        if (r13.A09 != null) {
                            Rect A004 = A00();
                            Rect A005 = A00();
                            Rect A006 = A00();
                            A0F(r13.A09, A004);
                            boolean z3 = false;
                            if (view.isLayoutRequested() || view.getVisibility() == 8) {
                                A005.setEmpty();
                            } else {
                                A005.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
                            }
                            int measuredWidth = view.getMeasuredWidth();
                            int measuredHeight = view.getMeasuredHeight();
                            A03(A004, A006, r13, A05, measuredWidth, measuredHeight);
                            if (!(A006.left == A005.left && A006.top == A005.top)) {
                                z3 = true;
                            }
                            A0B(A006, r13, measuredWidth, measuredHeight);
                            int i10 = A006.left - A005.left;
                            int i11 = A006.top - A005.top;
                            if (i10 != 0) {
                                AnonymousClass028.A0X(view, i10);
                            }
                            if (i11 != 0) {
                                AnonymousClass028.A0Y(view, i11);
                            }
                            if (z3 && (r8 = r13.A0A) != null) {
                                r8.A06(view, r13.A09, this);
                            }
                            A02(A004);
                            A02(A005);
                            A02(A006);
                        }
                    }
                }
                if (view.isLayoutRequested() || view.getVisibility() == 8) {
                    A002.setEmpty();
                } else {
                    A0F(view, A002);
                }
                if (r10.A03 != 0 && !A002.isEmpty()) {
                    int A007 = C05660Ql.A00(r10.A03, A05);
                    int i12 = A007 & 112;
                    if (i12 == 48) {
                        A00.top = Math.max(A00.top, A002.bottom);
                    } else if (i12 == 80) {
                        A00.bottom = Math.max(A00.bottom, getHeight() - A002.top);
                    }
                    int i13 = A007 & 7;
                    if (i13 == 3) {
                        A00.left = Math.max(A00.left, A002.right);
                    } else if (i13 == 5) {
                        A00.right = Math.max(A00.right, getWidth() - A002.left);
                    }
                }
                if (r10.A01 != 0 && view.getVisibility() == 0 && AnonymousClass028.A0r(view) && view.getWidth() > 0 && view.getHeight() > 0) {
                    AnonymousClass0B5 r12 = (AnonymousClass0B5) view.getLayoutParams();
                    AbstractC009304r r15 = r12.A0A;
                    Rect A008 = A00();
                    Rect A009 = A00();
                    A009.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
                    if (r15 == null || !r15.A02(A008, view, this)) {
                        A008.set(A009);
                    } else if (!A009.contains(A008)) {
                        StringBuilder sb = new StringBuilder("Rect should be within the child's bounds. Rect:");
                        sb.append(A008.toShortString());
                        sb.append(" | Bounds:");
                        sb.append(A009.toShortString());
                        throw new IllegalArgumentException(sb.toString());
                    }
                    A02(A009);
                    if (!A008.isEmpty()) {
                        int A0010 = C05660Ql.A00(r12.A01, A05);
                        if ((A0010 & 48) != 48 || (i6 = (A008.top - ((ViewGroup.MarginLayoutParams) r12).topMargin) - r12.A07) >= (i7 = A00.top)) {
                            z = false;
                        } else {
                            A05(view, i7 - i6);
                            z = true;
                        }
                        if ((A0010 & 80) == 80 && (height = ((getHeight() - A008.bottom) - ((ViewGroup.MarginLayoutParams) r12).bottomMargin) + r12.A07) < (i5 = A00.bottom)) {
                            A05(view, height - i5);
                        } else if (!z) {
                            A05(view, 0);
                        }
                        if ((A0010 & 3) != 3 || (i3 = (A008.left - ((ViewGroup.MarginLayoutParams) r12).leftMargin) - r12.A06) >= (i4 = A00.left)) {
                            z2 = false;
                        } else {
                            A04(view, i4 - i3);
                            z2 = true;
                        }
                        if ((A0010 & 5) == 5 && (width = ((getWidth() - A008.right) - ((ViewGroup.MarginLayoutParams) r12).rightMargin) + r12.A06) < (i2 = A00.right)) {
                            A04(view, width - i2);
                        } else if (!z2) {
                            A04(view, 0);
                        }
                    }
                    A02(A008);
                }
                if (i != 2) {
                    A003.set(((AnonymousClass0B5) view.getLayoutParams()).A0F);
                    if (!A003.equals(A002)) {
                        ((AnonymousClass0B5) view.getLayoutParams()).A0F.set(A002);
                    }
                }
                for (int i14 = i8 + 1; i14 < size; i14++) {
                    View view2 = (View) list.get(i14);
                    AnonymousClass0B5 r11 = (AnonymousClass0B5) view2.getLayoutParams();
                    AbstractC009304r r9 = r11.A0A;
                    if (r9 != null && r9.A05(view2, view, this)) {
                        if (i == 0) {
                            if (r11.A0E) {
                                A06 = false;
                                r11.A0E = A06;
                            }
                        } else if (i == 2) {
                        }
                        A06 = r9.A06(view2, view, this);
                        if (i != 1) {
                        }
                        r11.A0E = A06;
                    }
                }
            }
        }
        A02(A00);
        A02(A002);
        A02(A003);
    }

    public final void A0B(Rect rect, AnonymousClass0B5 r7, int i, int i2) {
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(getPaddingLeft() + ((ViewGroup.MarginLayoutParams) r7).leftMargin, Math.min(rect.left, ((width - getPaddingRight()) - i) - ((ViewGroup.MarginLayoutParams) r7).rightMargin));
        int max2 = Math.max(getPaddingTop() + ((ViewGroup.MarginLayoutParams) r7).topMargin, Math.min(rect.top, ((height - getPaddingBottom()) - i2) - ((ViewGroup.MarginLayoutParams) r7).bottomMargin));
        rect.set(max, max2, i + max, i2 + max2);
    }

    public void A0C(View view) {
        List list = (List) this.A0C.A00.get(view);
        if (!(list == null || list.isEmpty())) {
            for (int i = 0; i < list.size(); i++) {
                View view2 = (View) list.get(i);
                AbstractC009304r r0 = ((AnonymousClass0B5) view2.getLayoutParams()).A0A;
                if (r0 != null) {
                    r0.A06(view2, view, this);
                }
            }
        }
    }

    public void A0D(View view, int i) {
        Rect A00;
        Rect A002;
        AnonymousClass0B5 r2 = (AnonymousClass0B5) view.getLayoutParams();
        View view2 = r2.A09;
        if (view2 != null) {
            A00 = A00();
            A002 = A00();
            try {
                A0F(view2, A00);
                AnonymousClass0B5 r7 = (AnonymousClass0B5) view.getLayoutParams();
                int measuredWidth = view.getMeasuredWidth();
                int measuredHeight = view.getMeasuredHeight();
                A03(A00, A002, r7, i, measuredWidth, measuredHeight);
                A0B(A002, r7, measuredWidth, measuredHeight);
                view.layout(A002.left, A002.top, A002.right, A002.bottom);
            } finally {
                A02(A00);
                A02(A002);
            }
        } else if (r2.A05 != -1) {
            throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
        } else {
            int i2 = r2.A04;
            if (i2 >= 0) {
                AnonymousClass0B5 r72 = (AnonymousClass0B5) view.getLayoutParams();
                int i3 = r72.A02;
                if (i3 == 0) {
                    i3 = 8388661;
                }
                int A003 = C05660Ql.A00(i3, i);
                int i4 = A003 & 7;
                int i5 = A003 & 112;
                int width = getWidth();
                int height = getHeight();
                int measuredWidth2 = view.getMeasuredWidth();
                int measuredHeight2 = view.getMeasuredHeight();
                if (i == 1) {
                    i2 = width - i2;
                }
                int A06 = A06(i2) - measuredWidth2;
                int i6 = 0;
                if (i4 == 1) {
                    A06 += measuredWidth2 >> 1;
                } else if (i4 == 5) {
                    A06 += measuredWidth2;
                }
                if (i5 == 16) {
                    i6 = 0 + (measuredHeight2 >> 1);
                } else if (i5 == 80) {
                    i6 = measuredHeight2;
                }
                int max = Math.max(getPaddingLeft() + ((ViewGroup.MarginLayoutParams) r72).leftMargin, Math.min(A06, ((width - getPaddingRight()) - measuredWidth2) - ((ViewGroup.MarginLayoutParams) r72).rightMargin));
                int max2 = Math.max(getPaddingTop() + ((ViewGroup.MarginLayoutParams) r72).topMargin, Math.min(i6, ((height - getPaddingBottom()) - measuredHeight2) - ((ViewGroup.MarginLayoutParams) r72).bottomMargin));
                view.layout(max, max2, measuredWidth2 + max, measuredHeight2 + max2);
                return;
            }
            AnonymousClass0B5 r22 = (AnonymousClass0B5) view.getLayoutParams();
            A00 = A00();
            A00.set(getPaddingLeft() + ((ViewGroup.MarginLayoutParams) r22).leftMargin, getPaddingTop() + ((ViewGroup.MarginLayoutParams) r22).topMargin, (getWidth() - getPaddingRight()) - ((ViewGroup.MarginLayoutParams) r22).rightMargin, (getHeight() - getPaddingBottom()) - ((ViewGroup.MarginLayoutParams) r22).bottomMargin);
            if (this.A06 != null && getFitsSystemWindows() && !view.getFitsSystemWindows()) {
                A00.left += this.A06.A04();
                A00.top += this.A06.A06();
                A00.right -= this.A06.A05();
                A00.bottom -= this.A06.A03();
            }
            A002 = A00();
            int i7 = r22.A02;
            if ((i7 & 7) == 0) {
                i7 |= 8388611;
            }
            if ((i7 & 112) == 0) {
                i7 |= 48;
            }
            C05660Ql.A01(i7, view.getMeasuredWidth(), view.getMeasuredHeight(), A00, A002, i);
            view.layout(A002.left, A002.top, A002.right, A002.bottom);
        }
    }

    public void A0E(View view, int i, int i2, int i3) {
        measureChildWithMargins(view, i, i2, i3, 0);
    }

    public void A0F(View view, Rect rect) {
        rect.set(0, 0, view.getWidth(), view.getHeight());
        ThreadLocal threadLocal = AnonymousClass0RK.A00;
        Matrix matrix = (Matrix) threadLocal.get();
        if (matrix == null) {
            matrix = new Matrix();
            threadLocal.set(matrix);
        } else {
            matrix.reset();
        }
        AnonymousClass0RK.A00(matrix, view, this);
        ThreadLocal threadLocal2 = AnonymousClass0RK.A01;
        RectF rectF = (RectF) threadLocal2.get();
        if (rectF == null) {
            rectF = new RectF();
            threadLocal2.set(rectF);
        }
        rectF.set(rect);
        matrix.mapRect(rectF);
        rect.set((int) (rectF.left + 0.5f), (int) (rectF.top + 0.5f), (int) (rectF.right + 0.5f), (int) (rectF.bottom + 0.5f));
    }

    public final void A0G(boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            AbstractC009304r r1 = ((AnonymousClass0B5) childAt.getLayoutParams()).A0A;
            if (r1 != null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                if (z) {
                    r1.A0C(obtain, childAt, this);
                } else {
                    r1.A0D(obtain, childAt, this);
                }
                obtain.recycle();
            }
        }
        for (int i2 = 0; i2 < childCount; i2++) {
            getChildAt(i2).getLayoutParams();
        }
        this.A01 = null;
        this.A07 = false;
    }

    public final boolean A0H(MotionEvent motionEvent, int i) {
        int i2;
        int actionMasked = motionEvent.getActionMasked();
        List list = this.A0G;
        list.clear();
        boolean isChildrenDrawingOrderEnabled = isChildrenDrawingOrderEnabled();
        int childCount = getChildCount();
        for (int i3 = childCount - 1; i3 >= 0; i3--) {
            if (isChildrenDrawingOrderEnabled) {
                i2 = getChildDrawingOrder(childCount, i3);
            } else {
                i2 = i3;
            }
            list.add(getChildAt(i2));
        }
        Comparator comparator = A0L;
        if (comparator != null) {
            Collections.sort(list, comparator);
        }
        int size = list.size();
        MotionEvent motionEvent2 = null;
        boolean z = false;
        for (int i4 = 0; i4 < size; i4++) {
            View view = (View) list.get(i4);
            AbstractC009304r r1 = ((AnonymousClass0B5) view.getLayoutParams()).A0A;
            if (!z) {
                if (r1 != null) {
                    if (i == 0) {
                        z = r1.A0C(motionEvent, view, this);
                    } else if (i == 1) {
                        z = r1.A0D(motionEvent, view, this);
                    }
                    if (z) {
                        this.A01 = view;
                    }
                }
            } else if (!(actionMasked == 0 || r1 == null)) {
                if (motionEvent2 == null) {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    motionEvent2 = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                }
                if (i == 0) {
                    r1.A0C(motionEvent2, view, this);
                } else if (i == 1) {
                    r1.A0D(motionEvent2, view, this);
                }
            }
        }
        list.clear();
        return z;
    }

    public boolean A0I(View view, int i, int i2) {
        Rect A00 = A00();
        A0F(view, A00);
        try {
            return A00.contains(i, i2);
        } finally {
            A02(A00);
        }
    }

    @Override // X.AbstractC016407t
    public void ASw(View view, int[] iArr, int i, int i2, int i3) {
        boolean z;
        AbstractC009304r r10;
        int childCount = getChildCount();
        boolean z2 = false;
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                AnonymousClass0B5 r1 = (AnonymousClass0B5) childAt.getLayoutParams();
                if (i3 == 0) {
                    z = r1.A0D;
                } else if (i3 == 1) {
                    z = r1.A0C;
                }
                if (z && (r10 = r1.A0A) != null) {
                    int[] iArr2 = this.A0H;
                    iArr2[1] = 0;
                    iArr2[0] = 0;
                    r10.A0B(childAt, view, this, iArr2, i, i2, i3);
                    int i7 = iArr2[0];
                    if (i > 0) {
                        i4 = Math.max(i4, i7);
                    } else {
                        i4 = Math.min(i4, i7);
                    }
                    int i8 = iArr2[1];
                    if (i2 > 0) {
                        i5 = Math.max(i5, i8);
                    } else {
                        i5 = Math.min(i5, i8);
                    }
                    z2 = true;
                }
            }
        }
        iArr[0] = i4;
        iArr[1] = i5;
        if (z2) {
            A0A(1);
        }
    }

    @Override // X.AbstractC016407t
    public void ASx(View view, int i, int i2, int i3, int i4, int i5) {
        boolean z;
        AbstractC009304r r6;
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                AnonymousClass0B5 r1 = (AnonymousClass0B5) childAt.getLayoutParams();
                if (i5 == 0) {
                    z = r1.A0D;
                } else if (i5 == 1) {
                    z = r1.A0C;
                }
                if (z && (r6 = r1.A0A) != null) {
                    r6.A01(childAt, view, this, i, i2, i3, i4, i5);
                    z2 = true;
                }
            }
        }
        if (z2) {
            A0A(1);
        }
    }

    @Override // X.AbstractC016407t
    public void ASz(View view, View view2, int i, int i2) {
        C04740Mw r1 = this.A0D;
        if (i2 == 1) {
            r1.A00 = i;
        } else {
            r1.A01 = i;
        }
        this.A02 = view2;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).getLayoutParams();
        }
    }

    @Override // X.AbstractC016407t
    public boolean AWL(View view, View view2, int i, int i2) {
        boolean z;
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() != 8) {
                AnonymousClass0B5 r2 = (AnonymousClass0B5) childAt.getLayoutParams();
                AbstractC009304r r6 = r2.A0A;
                if (r6 != null) {
                    z = r6.A0E(childAt, view, view2, this, i, i2);
                    z2 |= z;
                } else {
                    z = false;
                }
                if (i2 == 0) {
                    r2.A0D = z;
                } else if (i2 == 1) {
                    r2.A0C = z;
                }
            }
        }
        return z2;
    }

    @Override // X.AbstractC016407t
    public void AWp(View view, int i) {
        boolean z;
        C04740Mw r2 = this.A0D;
        if (i == 1) {
            r2.A00 = 0;
        } else {
            r2.A01 = 0;
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            AnonymousClass0B5 r22 = (AnonymousClass0B5) childAt.getLayoutParams();
            if (i == 0) {
                z = r22.A0D;
            } else if (i == 1) {
                z = r22.A0C;
            }
            if (z) {
                AbstractC009304r r0 = r22.A0A;
                if (r0 != null) {
                    r0.A0A(childAt, view, this, i);
                }
                if (i == 0) {
                    r22.A0D = false;
                } else if (i == 1) {
                    r22.A0C = false;
                }
                r22.A0E = false;
            }
        }
        this.A02 = null;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof AnonymousClass0B5) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.view.ViewGroup
    public boolean drawChild(Canvas canvas, View view, long j) {
        view.getLayoutParams();
        return super.drawChild(canvas, view, j);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.A00;
        if (drawable != null && drawable.isStateful() && (false || drawable.setState(drawableState))) {
            invalidate();
        }
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new AnonymousClass0B5();
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new AnonymousClass0B5(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof AnonymousClass0B5) {
            return new AnonymousClass0B5((AnonymousClass0B5) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new AnonymousClass0B5((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new AnonymousClass0B5(layoutParams);
    }

    public final List getDependencySortedChildren() {
        A08();
        return Collections.unmodifiableList(this.A0E);
    }

    public final C018408o getLastWindowInsets() {
        return this.A06;
    }

    @Override // android.view.ViewGroup
    public int getNestedScrollAxes() {
        C04740Mw r0 = this.A0D;
        return r0.A01 | r0.A00;
    }

    public Drawable getStatusBarBackground() {
        return this.A00;
    }

    @Override // android.view.View
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
    }

    @Override // android.view.View
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        A0G(false);
        if (this.A0A) {
            if (this.A04 == null) {
                this.A04 = new ViewTreeObserver$OnPreDrawListenerC06980Wf(this);
            }
            getViewTreeObserver().addOnPreDrawListener(this.A04);
        }
        if (this.A06 == null && getFitsSystemWindows()) {
            AnonymousClass028.A0R(this);
        }
        this.A09 = true;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        A0G(false);
        if (this.A0A && this.A04 != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.A04);
        }
        View view = this.A02;
        if (view != null) {
            onStopNestedScroll(view);
        }
        this.A09 = false;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        C018408o r0;
        int A06;
        super.onDraw(canvas);
        if (this.A08 && this.A00 != null && (r0 = this.A06) != null && (A06 = r0.A06()) > 0) {
            this.A00.setBounds(0, 0, getWidth(), A06);
            this.A00.draw(canvas);
        }
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            A0G(true);
        }
        boolean A0H = A0H(motionEvent, 0);
        if (actionMasked == 1 || actionMasked == 3) {
            A0G(true);
        }
        return A0H;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        AbstractC009304r r0;
        int A05 = AnonymousClass028.A05(this);
        List list = this.A0E;
        int size = list.size();
        for (int i5 = 0; i5 < size; i5++) {
            View view = (View) list.get(i5);
            if (view.getVisibility() != 8 && ((r0 = ((AnonymousClass0B5) view.getLayoutParams()).A0A) == null || !r0.A0G(view, this, A05))) {
                A0D(view, A05);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0095, code lost:
        if (getFitsSystemWindows() == false) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d1, code lost:
        if (r20 != false) goto L_0x00d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0157, code lost:
        if (r20 != false) goto L_0x0159;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0159, code lost:
        r3 = java.lang.Math.max(0, (r19 - r21) - r2);
     */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r34, int r35) {
        /*
        // Method dump skipped, instructions count: 416
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.coordinatorlayout.widget.CoordinatorLayout.onMeasure(int, int):void");
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() != 8) {
                childAt.getLayoutParams();
            }
        }
        return false;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onNestedPreFling(View view, float f, float f2) {
        AbstractC009304r r5;
        int childCount = getChildCount();
        boolean z = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() != 8) {
                AnonymousClass0B5 r1 = (AnonymousClass0B5) childAt.getLayoutParams();
                if (r1.A0D && (r5 = r1.A0A) != null) {
                    z |= r5.A0F(childAt, view, this, f, f2);
                }
            }
        }
        return z;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        ASw(view, iArr, i, i2, 0);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        ASx(view, i, i2, i3, i4, 0);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onNestedScrollAccepted(View view, View view2, int i) {
        ASz(view, view2, i, 0);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        if (!(parcelable instanceof AnonymousClass0E4)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        AnonymousClass0E4 r8 = (AnonymousClass0E4) parcelable;
        super.onRestoreInstanceState(((AbstractC015707l) r8).A00);
        SparseArray sparseArray = r8.A00;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int id = childAt.getId();
            AbstractC009304r r1 = A01(childAt).A0A;
            if (!(id == -1 || r1 == null || (parcelable2 = (Parcelable) sparseArray.get(id)) == null)) {
                r1.A09(parcelable2, childAt, this);
            }
        }
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        Parcelable A08;
        AnonymousClass0E4 r7 = new AnonymousClass0E4(super.onSaveInstanceState());
        SparseArray sparseArray = new SparseArray();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int id = childAt.getId();
            AbstractC009304r r1 = ((AnonymousClass0B5) childAt.getLayoutParams()).A0A;
            if (!(id == -1 || r1 == null || (A08 = r1.A08(childAt, this)) == null)) {
                sparseArray.append(id, A08);
            }
        }
        r7.A00 = sparseArray;
        return r7;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public boolean onStartNestedScroll(View view, View view2, int i) {
        return AWL(view, view2, i, 0);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent, X.AbstractC016507u
    public void onStopNestedScroll(View view) {
        AWp(view, 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r4 != false) goto L_0x0010;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0034  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r13) {
        /*
            r12 = this;
            int r2 = r13.getActionMasked()
            android.view.View r0 = r12.A01
            r3 = 1
            r11 = 0
            if (r0 != 0) goto L_0x004d
            boolean r4 = r12.A0H(r13, r3)
            if (r4 == 0) goto L_0x004b
        L_0x0010:
            android.view.View r0 = r12.A01
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            X.0B5 r0 = (X.AnonymousClass0B5) r0
            X.04r r1 = r0.A0A
            if (r1 == 0) goto L_0x004b
            android.view.View r0 = r12.A01
            boolean r1 = r1.A0D(r13, r0, r12)
        L_0x0022:
            android.view.View r0 = r12.A01
            if (r0 != 0) goto L_0x0034
            boolean r0 = super.onTouchEvent(r13)
            r1 = r1 | r0
        L_0x002b:
            if (r2 == r3) goto L_0x0030
            r0 = 3
            if (r2 != r0) goto L_0x0033
        L_0x0030:
            r12.A0G(r11)
        L_0x0033:
            return r1
        L_0x0034:
            if (r4 == 0) goto L_0x002b
            long r4 = android.os.SystemClock.uptimeMillis()
            r8 = 3
            r9 = 0
            r10 = 0
            r6 = r4
            android.view.MotionEvent r0 = android.view.MotionEvent.obtain(r4, r6, r8, r9, r10, r11)
            super.onTouchEvent(r0)
            if (r0 == 0) goto L_0x002b
            r0.recycle()
            goto L_0x002b
        L_0x004b:
            r1 = 0
            goto L_0x0022
        L_0x004d:
            r4 = 0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.coordinatorlayout.widget.CoordinatorLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        AbstractC009304r r0 = ((AnonymousClass0B5) view.getLayoutParams()).A0A;
        if (r0 == null || !r0.A03(rect, view, this, z)) {
            return super.requestChildRectangleOnScreen(view, rect, z);
        }
        return true;
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        if (z && !this.A07) {
            A0G(false);
            this.A07 = true;
        }
    }

    @Override // android.view.View
    public void setFitsSystemWindows(boolean z) {
        super.setFitsSystemWindows(z);
        A09();
    }

    @Override // android.view.ViewGroup
    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener) {
        this.A03 = onHierarchyChangeListener;
    }

    public void setStatusBarBackground(Drawable drawable) {
        Drawable drawable2 = this.A00;
        if (drawable2 != drawable) {
            Drawable drawable3 = null;
            if (drawable2 != null) {
                drawable2.setCallback(null);
            }
            if (drawable != null) {
                drawable3 = drawable.mutate();
            }
            this.A00 = drawable3;
            if (drawable3 != null) {
                if (drawable3.isStateful()) {
                    this.A00.setState(getDrawableState());
                }
                C015607k.A0D(AnonymousClass028.A05(this), this.A00);
                Drawable drawable4 = this.A00;
                boolean z = false;
                if (getVisibility() == 0) {
                    z = true;
                }
                drawable4.setVisible(z, false);
                this.A00.setCallback(this);
            }
            postInvalidateOnAnimation();
        }
    }

    public void setStatusBarBackgroundColor(int i) {
        setStatusBarBackground(new ColorDrawable(i));
    }

    public void setStatusBarBackgroundResource(int i) {
        setStatusBarBackground(i != 0 ? AnonymousClass00T.A04(getContext(), i) : null);
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        boolean z = false;
        if (i == 0) {
            z = true;
        }
        Drawable drawable = this.A00;
        if (drawable != null && drawable.isVisible() != z) {
            this.A00.setVisible(z, false);
        }
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.A00;
    }
}
