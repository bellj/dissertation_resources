package androidx.constraintlayout.widget;

import X.AbstractC02530Cr;
import X.AbstractC03980Jx;
import X.AnonymousClass064;
import X.AnonymousClass067;
import X.AnonymousClass0MN;
import X.AnonymousClass0NZ;
import X.AnonymousClass0QV;
import X.AnonymousClass0SY;
import X.AnonymousClass0U6;
import X.C02540Cs;
import X.C02560Cw;
import X.C06240Ss;
import X.C07260Xh;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.Xml;
import android.view.View;
import android.view.ViewGroup;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParserException;

/* loaded from: classes.dex */
public class ConstraintLayout extends ViewGroup {
    public int A00 = -1;
    public int A01 = Integer.MAX_VALUE;
    public int A02 = Integer.MAX_VALUE;
    public int A03 = 0;
    public int A04 = 0;
    public int A05 = 263;
    public SparseArray A06 = new SparseArray();
    public SparseArray A07 = new SparseArray();
    public C02560Cw A08 = new C02560Cw();
    public C07260Xh A09 = new C07260Xh(this, this);
    public AnonymousClass0NZ A0A = null;
    public AnonymousClass0U6 A0B = null;
    public AbstractC03980Jx A0C;
    public ArrayList A0D = new ArrayList(4);
    public HashMap A0E = new HashMap();
    public boolean A0F = true;

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ConstraintLayout(Context context) {
        super(context);
        A01(null, 0, 0);
    }

    public ConstraintLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01(attributeSet, 0, 0);
    }

    public ConstraintLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01(attributeSet, i, 0);
    }

    public ConstraintLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01(attributeSet, i, i2);
    }

    public final AnonymousClass0QV A00(View view) {
        if (view == this) {
            return this.A08;
        }
        if (view == null) {
            return null;
        }
        return ((AnonymousClass064) view.getLayoutParams()).A0r;
    }

    public final void A01(AttributeSet attributeSet, int i, int i2) {
        C02560Cw r8 = this.A08;
        r8.A0e = this;
        C07260Xh r1 = this.A09;
        r8.A06 = r1;
        r8.A08.A03 = r1;
        this.A06.put(getId(), this);
        this.A0B = null;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, AnonymousClass0MN.A01, i, i2);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i3 = 0; i3 < indexCount; i3++) {
                int index = obtainStyledAttributes.getIndex(i3);
                if (index == 9) {
                    this.A04 = obtainStyledAttributes.getDimensionPixelOffset(index, this.A04);
                } else if (index == 10) {
                    this.A03 = obtainStyledAttributes.getDimensionPixelOffset(index, this.A03);
                } else if (index == 7) {
                    this.A02 = obtainStyledAttributes.getDimensionPixelOffset(index, this.A02);
                } else if (index == 8) {
                    this.A01 = obtainStyledAttributes.getDimensionPixelOffset(index, this.A01);
                } else if (index == 89) {
                    this.A05 = obtainStyledAttributes.getInt(index, this.A05);
                } else if (index == 38) {
                    int resourceId = obtainStyledAttributes.getResourceId(index, 0);
                    if (resourceId != 0) {
                        try {
                            this.A0A = new AnonymousClass0NZ(getContext(), this, resourceId);
                        } catch (Resources.NotFoundException unused) {
                            this.A0A = null;
                        }
                    }
                } else if (index == 18) {
                    int resourceId2 = obtainStyledAttributes.getResourceId(index, 0);
                    try {
                        AnonymousClass0U6 r11 = new AnonymousClass0U6();
                        this.A0B = r11;
                        Context context = getContext();
                        XmlResourceParser xml = context.getResources().getXml(resourceId2);
                        try {
                            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                                if (eventType == 0) {
                                    xml.getName();
                                } else if (eventType == 2) {
                                    String name = xml.getName();
                                    AnonymousClass0SY A03 = r11.A03(context, Xml.asAttributeSet(xml));
                                    if (name.equalsIgnoreCase("Guideline")) {
                                        A03.A02.A0y = true;
                                    }
                                    r11.A00.put(Integer.valueOf(A03.A00), A03);
                                }
                            }
                        } catch (IOException | XmlPullParserException e) {
                            e.printStackTrace();
                        }
                    } catch (Resources.NotFoundException unused2) {
                        this.A0B = null;
                    }
                    this.A00 = resourceId2;
                }
            }
            obtainStyledAttributes.recycle();
        }
        int i4 = this.A05;
        r8.A01 = i4;
        boolean z = false;
        if ((i4 & 256) == 256) {
            z = true;
        }
        C06240Ss.A0F = z;
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof AnonymousClass064;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d2 A[ORIG_RETURN, RETURN] */
    @Override // android.view.ViewGroup, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dispatchDraw(android.graphics.Canvas r24) {
        /*
        // Method dump skipped, instructions count: 211
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.ConstraintLayout.dispatchDraw(android.graphics.Canvas):void");
    }

    @Override // android.view.View
    public void forceLayout() {
        this.A0F = true;
        super.forceLayout();
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new AnonymousClass064();
    }

    @Override // android.view.ViewGroup
    public /* bridge */ /* synthetic */ ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new AnonymousClass064(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new AnonymousClass064(layoutParams);
    }

    public int getMaxHeight() {
        return this.A01;
    }

    public int getMaxWidth() {
        return this.A02;
    }

    public int getMinHeight() {
        return this.A03;
    }

    public int getMinWidth() {
        return this.A04;
    }

    public int getOptimizationLevel() {
        return this.A08.A01;
    }

    private int getPaddingWidth() {
        int max;
        return (Build.VERSION.SDK_INT < 17 || (max = Math.max(0, getPaddingEnd()) + Math.max(0, getPaddingStart())) <= 0) ? Math.max(0, getPaddingLeft()) + Math.max(0, getPaddingRight()) : max;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        boolean isInEditMode = isInEditMode();
        int i5 = 0;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            AnonymousClass064 r2 = (AnonymousClass064) childAt.getLayoutParams();
            AnonymousClass0QV r4 = r2.A0r;
            if (childAt.getVisibility() != 8 || r2.A0x || r2.A0y || isInEditMode) {
                int A02 = r4.A02();
                int A03 = r4.A03();
                childAt.layout(A02, A03, r4.A01() + A02, r4.A00() + A03);
            }
        }
        ArrayList arrayList = this.A0D;
        int size = arrayList.size();
        if (size > 0) {
            do {
                ((AnonymousClass067) arrayList.get(i5)).A07(this);
                i5++;
            } while (i5 < size);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:346:0x06d9, code lost:
        if ((r9 & 64) == 64) goto L_0x06db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:0x0706, code lost:
        if (r13.A01 <= 0.0f) goto L_0x0708;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:0x071a, code lost:
        if (r12 == false) goto L_0x071d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:403:0x079f, code lost:
        if (r16 == 0) goto L_0x07a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:405:0x07a4, code lost:
        if (r16 == 0) goto L_0x07a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:406:0x07a6, code lost:
        r14 = java.lang.Math.max(0, r29.A03);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:410:0x07be, code lost:
        if (r16 == 0) goto L_0x07c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:412:0x07c3, code lost:
        if (r16 == 0) goto L_0x07c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:413:0x07c5, code lost:
        r15 = java.lang.Math.max(0, r29.A04);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:484:0x0977, code lost:
        if (r6 != 2) goto L_0x0979;
     */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x03c6  */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x03e3  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x0402  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x0421  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x0471  */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x0479  */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x0492  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x04a8  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x0596  */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x0651  */
    /* JADX WARNING: Removed duplicated region for block: B:329:0x065a  */
    /* JADX WARNING: Removed duplicated region for block: B:339:0x06a9  */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x06b1  */
    /* JADX WARNING: Removed duplicated region for block: B:345:0x06d5  */
    /* JADX WARNING: Removed duplicated region for block: B:350:0x06e2  */
    /* JADX WARNING: Removed duplicated region for block: B:402:0x079d  */
    /* JADX WARNING: Removed duplicated region for block: B:409:0x07bc  */
    /* JADX WARNING: Removed duplicated region for block: B:599:0x0bbb  */
    /* JADX WARNING: Removed duplicated region for block: B:603:0x0bf4  */
    /* JADX WARNING: Removed duplicated region for block: B:605:0x0bf7  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01a1 A[LOOP:6: B:93:0x019f->B:94:0x01a1, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01bb A[LOOP:7: B:96:0x01b9->B:97:0x01bb, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01cf  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r30, int r31) {
        /*
        // Method dump skipped, instructions count: 3068
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.ConstraintLayout.onMeasure(int, int):void");
    }

    @Override // android.view.ViewGroup
    public void onViewAdded(View view) {
        super.onViewAdded(view);
        AnonymousClass0QV A00 = A00(view);
        if ((view instanceof Guideline) && !(A00 instanceof C02540Cs)) {
            AnonymousClass064 r0 = (AnonymousClass064) view.getLayoutParams();
            C02540Cs r1 = new C02540Cs();
            r0.A0r = r1;
            r0.A0x = true;
            r1.A0I(r0.A0b);
        }
        if (view instanceof AnonymousClass067) {
            AnonymousClass067 r2 = (AnonymousClass067) view;
            r2.A02();
            ((AnonymousClass064) view.getLayoutParams()).A0y = true;
            ArrayList arrayList = this.A0D;
            if (!arrayList.contains(r2)) {
                arrayList.add(r2);
            }
        }
        this.A06.put(view.getId(), view);
        this.A0F = true;
    }

    @Override // android.view.ViewGroup
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        this.A06.remove(view.getId());
        AnonymousClass0QV A00 = A00(view);
        ((AbstractC02530Cr) this.A08).A00.remove(A00);
        A00.A0Z = null;
        this.A0D.remove(view);
        this.A0F = true;
    }

    @Override // android.view.ViewParent, android.view.View
    public void requestLayout() {
        this.A0F = true;
        super.requestLayout();
    }

    public void setConstraintSet(AnonymousClass0U6 r1) {
        this.A0B = r1;
    }

    @Override // android.view.View
    public void setId(int i) {
        SparseArray sparseArray = this.A06;
        sparseArray.remove(getId());
        super.setId(i);
        sparseArray.put(getId(), this);
    }

    public void setMaxHeight(int i) {
        if (i != this.A01) {
            this.A01 = i;
            requestLayout();
        }
    }

    public void setMaxWidth(int i) {
        if (i != this.A02) {
            this.A02 = i;
            requestLayout();
        }
    }

    public void setMinHeight(int i) {
        if (i != this.A03) {
            this.A03 = i;
            requestLayout();
        }
    }

    public void setMinWidth(int i) {
        if (i != this.A04) {
            this.A04 = i;
            requestLayout();
        }
    }

    public void setOnConstraintsChanged(AbstractC03980Jx r2) {
        this.A0C = r2;
        AnonymousClass0NZ r0 = this.A0A;
        if (r0 != null) {
            r0.A02 = r2;
        }
    }

    public void setOptimizationLevel(int i) {
        this.A05 = i;
        this.A08.A01 = i;
        boolean z = false;
        if ((i & 256) == 256) {
            z = true;
        }
        C06240Ss.A0F = z;
    }
}
