package androidx.constraintlayout.widget;

import X.AnonymousClass064;
import X.AnonymousClass067;
import X.AnonymousClass0QV;
import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes.dex */
public class Group extends AnonymousClass067 {
    public Group(Context context) {
        super(context);
    }

    public Group(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public Group(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // X.AnonymousClass067
    public void A04(AttributeSet attributeSet) {
        super.A04(attributeSet);
    }

    @Override // X.AnonymousClass067
    public void A07(ConstraintLayout constraintLayout) {
        AnonymousClass0QV r1 = ((AnonymousClass064) getLayoutParams()).A0r;
        r1.A06(0);
        r1.A05(0);
    }

    @Override // X.AnonymousClass067, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        A01();
    }

    @Override // android.view.View
    public void setElevation(float f) {
        super.setElevation(f);
        A01();
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        A01();
    }
}
