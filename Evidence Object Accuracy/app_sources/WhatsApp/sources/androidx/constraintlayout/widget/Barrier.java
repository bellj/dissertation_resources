package androidx.constraintlayout.widget;

import X.AnonymousClass067;
import X.AnonymousClass0Cu;
import X.AnonymousClass0MN;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

/* loaded from: classes.dex */
public class Barrier extends AnonymousClass067 {
    public int A00;
    public AnonymousClass0Cu A01;

    public Barrier(Context context) {
        super(context);
        super.setVisibility(8);
    }

    public Barrier(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        super.setVisibility(8);
    }

    public Barrier(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        super.setVisibility(8);
    }

    @Override // X.AnonymousClass067
    public void A04(AttributeSet attributeSet) {
        super.A04(attributeSet);
        this.A01 = new AnonymousClass0Cu();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, AnonymousClass0MN.A01);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i = 0; i < indexCount; i++) {
                int index = obtainStyledAttributes.getIndex(i);
                if (index == 15) {
                    this.A00 = obtainStyledAttributes.getInt(index, 0);
                } else if (index == 14) {
                    this.A01.A02 = obtainStyledAttributes.getBoolean(index, true);
                } else if (index == 16) {
                    this.A01.A01 = obtainStyledAttributes.getDimensionPixelSize(index, 0);
                }
            }
        }
        this.A02 = this.A01;
        A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (r4 == 6) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (r4 == 6) goto L_0x0010;
     */
    @Override // X.AnonymousClass067
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(X.AnonymousClass0QV r6, boolean r7) {
        /*
            r5 = this;
            int r4 = r5.A00
            int r3 = android.os.Build.VERSION.SDK_INT
            r2 = 6
            r1 = 5
            r0 = 17
            if (r3 < r0) goto L_0x001a
            if (r7 == 0) goto L_0x001a
            if (r4 == r1) goto L_0x001e
            if (r4 != r2) goto L_0x0011
        L_0x0010:
            r4 = 0
        L_0x0011:
            boolean r0 = r6 instanceof X.AnonymousClass0Cu
            if (r0 == 0) goto L_0x0019
            X.0Cu r6 = (X.AnonymousClass0Cu) r6
            r6.A00 = r4
        L_0x0019:
            return
        L_0x001a:
            if (r4 == r1) goto L_0x0010
            if (r4 != r2) goto L_0x0011
        L_0x001e:
            r4 = 1
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.widget.Barrier.A05(X.0QV, boolean):void");
    }

    public int getMargin() {
        return this.A01.A01;
    }

    public int getType() {
        return this.A00;
    }

    public void setAllowsGoneWidget(boolean z) {
        this.A01.A02 = z;
    }

    public void setDpMargin(int i) {
        this.A01.A01 = (int) ((((float) i) * getResources().getDisplayMetrics().density) + 0.5f);
    }

    public void setMargin(int i) {
        this.A01.A01 = i;
    }

    public void setType(int i) {
        this.A00 = i;
    }
}
