package android.support.v4.graphics.drawable;

import X.AnonymousClass0QP;
import androidx.core.graphics.drawable.IconCompat;

/* loaded from: classes.dex */
public final class IconCompatParcelizer extends androidx.core.graphics.drawable.IconCompatParcelizer {
    public static IconCompat read(AnonymousClass0QP r0) {
        return androidx.core.graphics.drawable.IconCompatParcelizer.read(r0);
    }

    public static void write(IconCompat iconCompat, AnonymousClass0QP r1) {
        androidx.core.graphics.drawable.IconCompatParcelizer.write(iconCompat, r1);
    }
}
