package android.support.v4.media;

import X.AnonymousClass0QP;
import androidx.media.AudioAttributesImplApi26;

/* loaded from: classes.dex */
public final class AudioAttributesImplApi26Parcelizer extends androidx.media.AudioAttributesImplApi26Parcelizer {
    public static AudioAttributesImplApi26 read(AnonymousClass0QP r0) {
        return androidx.media.AudioAttributesImplApi26Parcelizer.read(r0);
    }

    public static void write(AudioAttributesImplApi26 audioAttributesImplApi26, AnonymousClass0QP r1) {
        androidx.media.AudioAttributesImplApi26Parcelizer.write(audioAttributesImplApi26, r1);
    }
}
