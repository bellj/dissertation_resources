package android.support.v4.media.session;

import android.os.Build;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* loaded from: classes.dex */
public final class MediaSessionCompat$Token implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(5);
    public final Object A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public MediaSessionCompat$Token(Object obj) {
        this.A00 = obj;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MediaSessionCompat$Token) {
            Object obj2 = this.A00;
            Object obj3 = ((MediaSessionCompat$Token) obj).A00;
            if (obj2 == null) {
                if (obj3 != null) {
                    return false;
                }
                return true;
            } else if (obj3 != null) {
                return obj2.equals(obj3);
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object obj = this.A00;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            parcel.writeParcelable((Parcelable) this.A00, i);
        } else {
            parcel.writeStrongBinder((IBinder) this.A00);
        }
    }
}
