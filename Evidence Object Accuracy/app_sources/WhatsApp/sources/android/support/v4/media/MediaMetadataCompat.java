package android.support.v4.media;

import X.AnonymousClass00N;
import X.C03970Jw;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* loaded from: classes.dex */
public final class MediaMetadataCompat implements Parcelable {
    public static final AnonymousClass00N A01;
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(1);
    public final Bundle A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    static {
        AnonymousClass00N r3 = new AnonymousClass00N();
        A01 = r3;
        r3.put("android.media.metadata.TITLE", 1);
        r3.put("android.media.metadata.ARTIST", 1);
        r3.put("android.media.metadata.DURATION", 0);
        r3.put("android.media.metadata.ALBUM", 1);
        r3.put("android.media.metadata.AUTHOR", 1);
        r3.put("android.media.metadata.WRITER", 1);
        r3.put("android.media.metadata.COMPOSER", 1);
        r3.put("android.media.metadata.COMPILATION", 1);
        r3.put("android.media.metadata.DATE", 1);
        r3.put("android.media.metadata.YEAR", 0);
        r3.put("android.media.metadata.GENRE", 1);
        r3.put("android.media.metadata.TRACK_NUMBER", 0);
        r3.put("android.media.metadata.NUM_TRACKS", 0);
        r3.put("android.media.metadata.DISC_NUMBER", 0);
        r3.put("android.media.metadata.ALBUM_ARTIST", 1);
        r3.put("android.media.metadata.ART", 2);
        r3.put("android.media.metadata.ART_URI", 1);
        r3.put("android.media.metadata.ALBUM_ART", 2);
        r3.put("android.media.metadata.ALBUM_ART_URI", 1);
        r3.put("android.media.metadata.USER_RATING", 3);
        r3.put("android.media.metadata.RATING", 3);
        r3.put("android.media.metadata.DISPLAY_TITLE", 1);
        r3.put("android.media.metadata.DISPLAY_SUBTITLE", 1);
        r3.put("android.media.metadata.DISPLAY_DESCRIPTION", 1);
        r3.put("android.media.metadata.DISPLAY_ICON", 2);
        r3.put("android.media.metadata.DISPLAY_ICON_URI", 1);
        r3.put("android.media.metadata.MEDIA_ID", 1);
        r3.put("android.media.metadata.BT_FOLDER_TYPE", 0);
        r3.put("android.media.metadata.MEDIA_URI", 1);
        r3.put("android.media.metadata.ADVERTISEMENT", 0);
        r3.put("android.media.metadata.DOWNLOAD_STATUS", 0);
    }

    public MediaMetadataCompat(Parcel parcel) {
        this.A00 = parcel.readBundle(C03970Jw.class.getClassLoader());
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.A00);
    }
}
