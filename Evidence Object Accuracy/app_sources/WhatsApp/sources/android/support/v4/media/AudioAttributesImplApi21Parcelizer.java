package android.support.v4.media;

import X.AnonymousClass0QP;
import androidx.media.AudioAttributesImplApi21;

/* loaded from: classes.dex */
public final class AudioAttributesImplApi21Parcelizer extends androidx.media.AudioAttributesImplApi21Parcelizer {
    public static AudioAttributesImplApi21 read(AnonymousClass0QP r0) {
        return androidx.media.AudioAttributesImplApi21Parcelizer.read(r0);
    }

    public static void write(AudioAttributesImplApi21 audioAttributesImplApi21, AnonymousClass0QP r1) {
        androidx.media.AudioAttributesImplApi21Parcelizer.write(audioAttributesImplApi21, r1);
    }
}
