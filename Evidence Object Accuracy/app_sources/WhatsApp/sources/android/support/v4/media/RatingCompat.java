package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* loaded from: classes.dex */
public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(2);
    public final float A00;
    public final int A01;

    public RatingCompat(int i, float f) {
        this.A01 = i;
        this.A00 = f;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return this.A01;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("Rating:style=");
        sb.append(this.A01);
        sb.append(" rating=");
        float f = this.A00;
        sb.append(f < 0.0f ? "unrated" : String.valueOf(f));
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A01);
        parcel.writeFloat(this.A00);
    }
}
