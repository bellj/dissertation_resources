package android.support.v4.media;

import X.AnonymousClass0QP;
import androidx.media.AudioAttributesCompat;

/* loaded from: classes.dex */
public final class AudioAttributesCompatParcelizer extends androidx.media.AudioAttributesCompatParcelizer {
    public static AudioAttributesCompat read(AnonymousClass0QP r0) {
        return androidx.media.AudioAttributesCompatParcelizer.read(r0);
    }

    public static void write(AudioAttributesCompat audioAttributesCompat, AnonymousClass0QP r1) {
        androidx.media.AudioAttributesCompatParcelizer.write(audioAttributesCompat, r1);
    }
}
