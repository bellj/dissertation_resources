package android.support.v4.media;

import X.AnonymousClass0QP;
import androidx.media.AudioAttributesImplBase;

/* loaded from: classes.dex */
public final class AudioAttributesImplBaseParcelizer extends androidx.media.AudioAttributesImplBaseParcelizer {
    public static AudioAttributesImplBase read(AnonymousClass0QP r0) {
        return androidx.media.AudioAttributesImplBaseParcelizer.read(r0);
    }

    public static void write(AudioAttributesImplBase audioAttributesImplBase, AnonymousClass0QP r1) {
        androidx.media.AudioAttributesImplBaseParcelizer.write(audioAttributesImplBase, r1);
    }
}
