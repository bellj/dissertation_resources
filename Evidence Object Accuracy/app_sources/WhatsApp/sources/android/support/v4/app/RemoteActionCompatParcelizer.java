package android.support.v4.app;

import X.AnonymousClass0QP;
import androidx.core.app.RemoteActionCompat;

/* loaded from: classes.dex */
public final class RemoteActionCompatParcelizer extends androidx.core.app.RemoteActionCompatParcelizer {
    public static RemoteActionCompat read(AnonymousClass0QP r0) {
        return androidx.core.app.RemoteActionCompatParcelizer.read(r0);
    }

    public static void write(RemoteActionCompat remoteActionCompat, AnonymousClass0QP r1) {
        androidx.core.app.RemoteActionCompatParcelizer.write(remoteActionCompat, r1);
    }
}
