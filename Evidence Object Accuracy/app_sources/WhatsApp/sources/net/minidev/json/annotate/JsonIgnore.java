package net.minidev.json.annotate;

/* loaded from: classes.dex */
public @interface JsonIgnore {
    boolean value() default true;
}
