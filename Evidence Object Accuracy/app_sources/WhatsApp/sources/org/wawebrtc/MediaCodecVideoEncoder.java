package org.wawebrtc;

import X.AnonymousClass1MS;
import X.AnonymousClass4F8;
import X.AnonymousClass5Pf;
import X.AnonymousClass5S5;
import X.C17140qK;
import X.C39011p7;
import X.C90594Om;
import X.C93054Yu;
import X.C93514aI;
import X.C94204bQ;
import X.C94794cY;
import X.EnumC87054Aa;
import android.media.MediaCodec;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Surface;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class MediaCodecVideoEncoder {
    public static final int DEQUEUE_TIMEOUT = 0;
    public static final String[] H264_HW_EXCEPTION_MODELS = {"SAMSUNG-SGH-I337", "Nexus 7"};
    public static final String H264_MIME_TYPE = "video/avc";
    public static final String[] H265_HW_EXCEPTION_MODELS = new String[0];
    public static final String H265_MIME_TYPE = "video/hevc";
    public static final int MEDIA_CODEC_RELEASE_TIMEOUT_MS = 5000;
    public static final int MIN_ENCODER_HEIGHT = 144;
    public static final int MIN_ENCODER_WIDTH = 176;
    public static final String TAG = "MediaCodecVideoEncoder";
    public static final String VP8_MIME_TYPE = "video/x-vnd.on2.vp8";
    public static final String VP9_MIME_TYPE = "video/x-vnd.on2.vp9";
    public static final String[] blacklistedHwCodecPrefixes = new String[0];
    public static final Map cachedCodecCapabilites = new HashMap();
    public static int codecErrors;
    public static AnonymousClass5S5 errorCallback;
    public static final String[] h264BlacklistedBuildHardware = {"sc8830", "sc8830a", "samsungexynos7580"};
    public static final String[] h265BlacklistedBuildHardware = new String[0];
    public static Set hwEncoderDisabledTypes = new HashSet();
    public static long lastReleaseTimestamp;
    public static MediaCodecVideoEncoder runningInstance;
    public static final String[] supportedH264HwCodecPrefixes = null;
    public static final String[] supportedH265HwCodecPrefixes = null;
    public static final int[] supportedSurfaceColorList = {2130708361};
    public static final String[] supportedVp8HwCodecPrefixes = {"OMX.qcom.", "OMX.Intel."};
    public static final String[] supportedVp9HwCodecPrefixes = {"OMX.qcom."};
    public static final String[] trustedCodecPrefixes = {"OMX.qcom.", "OMX.Exynos.", "OMX.google", "OMX.IMG."};
    public MediaCodec.BufferInfo cachedBufferInfo = new MediaCodec.BufferInfo();
    public BufferInfo cachedInputBuffer = new BufferInfo();
    public BufferInfo cachedOutputBuffer = new BufferInfo();
    public final Queue carryAlongInfos = new LinkedList();
    public String codecName;
    public int colorFormat;
    public int colorId;
    public C94794cY drawer;
    public AnonymousClass5Pf eglBase;
    public final List freeInfos = new LinkedList();
    public int height;
    public ByteBuffer[] inputBuffers;
    public Surface inputSurface;
    public Bundle keyFrameRequestBundle = new Bundle();
    public MediaCodec mediaCodec;
    public Thread mediaCodecThread;
    public ByteBuffer[] outputBuffers;
    public EnumC87054Aa type;
    public final C17140qK voipSharedPreferences;
    public int width;

    private void checkOnMediaCodecThread() {
    }

    public MediaCodecVideoEncoder(C17140qK r5) {
        this.voipSharedPreferences = r5;
        int i = 0;
        do {
            this.freeInfos.add(new C93514aI());
            i++;
        } while (i < 10);
        this.keyFrameRequestBundle.putInt("request-sync", 0);
    }

    public static MediaCodec createByCodecName(String str) {
        try {
            return MediaCodec.createByCodecName(str);
        } catch (Exception e) {
            Log.e(e);
            return null;
        }
    }

    public BufferInfo dequeueInputBuffer() {
        try {
            int dequeueInputBuffer = this.mediaCodec.dequeueInputBuffer(0);
            if (dequeueInputBuffer < 0) {
                return null;
            }
            this.cachedInputBuffer.set(dequeueInputBuffer, getInputBuffer(dequeueInputBuffer), false, 0, 0, 0, false);
            return this.cachedInputBuffer;
        } catch (Throwable th) {
            Log.e(th);
            throw th;
        }
    }

    public BufferInfo dequeueOutputBuffer(int i) {
        try {
            MediaCodec.BufferInfo bufferInfo = this.cachedBufferInfo;
            int dequeueOutputBuffer = this.mediaCodec.dequeueOutputBuffer(bufferInfo, (long) i);
            if (dequeueOutputBuffer >= 0) {
                ByteBuffer outputBuffer = getOutputBuffer(dequeueOutputBuffer);
                outputBuffer.position(bufferInfo.offset);
                outputBuffer.limit(bufferInfo.offset + bufferInfo.size);
                int i2 = bufferInfo.flags;
                if ((i2 & 2) != 0) {
                    this.cachedOutputBuffer.set(dequeueOutputBuffer, outputBuffer.slice(), false, 0, 0, 0, true);
                    return this.cachedOutputBuffer;
                }
                boolean z = false;
                if ((i2 & 1) != 0) {
                    z = true;
                }
                C93514aI r2 = (C93514aI) this.carryAlongInfos.remove();
                int i3 = r2.A00;
                this.cachedOutputBuffer.set(dequeueOutputBuffer, outputBuffer.slice(), z, r2.A02, SystemClock.elapsedRealtime() - r2.A01, i3, false);
                this.freeInfos.add(r2);
                return this.cachedOutputBuffer;
            } else if (dequeueOutputBuffer == -3) {
                if (Build.VERSION.SDK_INT < 21) {
                    this.outputBuffers = this.mediaCodec.getOutputBuffers();
                }
                return dequeueOutputBuffer(i);
            } else if (dequeueOutputBuffer == -2) {
                return dequeueOutputBuffer(i);
            } else {
                if (dequeueOutputBuffer == -1) {
                    return null;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("dequeueOutputBuffer: ");
                sb.append(dequeueOutputBuffer);
                throw new RuntimeException(sb.toString());
            }
        } catch (IllegalStateException e) {
            Log.e("MediaCodecVideoEncoder dequeueOutputBuffer failed", e);
            BufferInfo bufferInfo2 = this.cachedOutputBuffer;
            bufferInfo2.set(-1, null, false, -1, -1, 0, false);
            return bufferInfo2;
        }
    }

    public static void disableH264HwCodec() {
        Log.w("MediaCodecVideoEncoder H.264 encoding is disabled by application.");
        hwEncoderDisabledTypes.add("video/avc");
    }

    public static void disableH265HwCodec() {
        Log.w("MediaCodecVideoEncoder H.265 encoding is disabled by application.");
        hwEncoderDisabledTypes.add("video/hevc");
    }

    public static void disableVp8HwCodec() {
        Log.w("MediaCodecVideoEncoder VP8 encoding is disabled by application.");
        hwEncoderDisabledTypes.add("video/x-vnd.on2.vp8");
    }

    public static void disableVp9HwCodec() {
        Log.w("MediaCodecVideoEncoder VP9 encoding is disabled by application.");
        hwEncoderDisabledTypes.add("video/x-vnd.on2.vp9");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0036 A[Catch: IllegalStateException -> 0x0066, TryCatch #0 {IllegalStateException -> 0x0066, blocks: (B:4:0x0006, B:6:0x000c, B:8:0x0012, B:9:0x0019, B:11:0x0026, B:13:0x0036, B:14:0x003b, B:15:0x0056), top: B:20:0x0006 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056 A[Catch: IllegalStateException -> 0x0066, TryCatch #0 {IllegalStateException -> 0x0066, blocks: (B:4:0x0006, B:6:0x000c, B:8:0x0012, B:9:0x0019, B:11:0x0026, B:13:0x0036, B:14:0x003b, B:15:0x0056), top: B:20:0x0006 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean encodeBuffer(boolean r18, int r19, int r20, long r21, long r23, int r25) {
        /*
            r17 = this;
            r3 = 1
            r6 = 0
            r1 = r17
            if (r18 == 0) goto L_0x0025
            boolean r0 = r1.supportForceKeyFrame()     // Catch: IllegalStateException -> 0x0066
            if (r0 == 0) goto L_0x0025
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch: IllegalStateException -> 0x0066
            r0 = 21
            if (r2 < r0) goto L_0x0019
            java.lang.String r0 = "MediaCodecVideoEncoder force Keyframe"
            com.whatsapp.util.Log.i(r0)     // Catch: IllegalStateException -> 0x0066
            r10 = 1
            goto L_0x0026
        L_0x0019:
            java.lang.String r0 = "MediaCodecVideoEncoder Sync frame request"
            com.whatsapp.util.Log.i(r0)     // Catch: IllegalStateException -> 0x0066
            android.media.MediaCodec r2 = r1.mediaCodec     // Catch: IllegalStateException -> 0x0066
            android.os.Bundle r0 = r1.keyFrameRequestBundle     // Catch: IllegalStateException -> 0x0066
            r2.setParameters(r0)     // Catch: IllegalStateException -> 0x0066
        L_0x0025:
            r10 = 0
        L_0x0026:
            long r13 = android.os.SystemClock.elapsedRealtime()     // Catch: IllegalStateException -> 0x0066
            java.util.List r0 = r1.freeInfos     // Catch: IllegalStateException -> 0x0066
            boolean r0 = r0.isEmpty()     // Catch: IllegalStateException -> 0x0066
            r12 = r25
            r15 = r21
            if (r0 == 0) goto L_0x0056
            X.4aI r11 = new X.4aI     // Catch: IllegalStateException -> 0x0066
            r11.<init>(r12, r13, r15)     // Catch: IllegalStateException -> 0x0066
        L_0x003b:
            java.util.Queue r0 = r1.carryAlongInfos     // Catch: IllegalStateException -> 0x0066
            r0.add(r11)     // Catch: IllegalStateException -> 0x0066
            r5 = r19
            java.nio.ByteBuffer r0 = r1.getInputBuffer(r5)     // Catch: IllegalStateException -> 0x0066
            r0.position(r6)     // Catch: IllegalStateException -> 0x0066
            r7 = r20
            r0.limit(r7)     // Catch: IllegalStateException -> 0x0066
            android.media.MediaCodec r4 = r1.mediaCodec     // Catch: IllegalStateException -> 0x0066
            r8 = r23
            r4.queueInputBuffer(r5, r6, r7, r8, r10)     // Catch: IllegalStateException -> 0x0066
            goto L_0x0065
        L_0x0056:
            java.util.List r0 = r1.freeInfos     // Catch: IllegalStateException -> 0x0066
            java.lang.Object r11 = r0.remove(r6)     // Catch: IllegalStateException -> 0x0066
            X.4aI r11 = (X.C93514aI) r11     // Catch: IllegalStateException -> 0x0066
            r11.A01 = r13     // Catch: IllegalStateException -> 0x0066
            r11.A02 = r15     // Catch: IllegalStateException -> 0x0066
            r11.A00 = r12     // Catch: IllegalStateException -> 0x0066
            goto L_0x003b
        L_0x0065:
            return r3
        L_0x0066:
            r1 = move-exception
            java.lang.String r0 = "MediaCodecVideoEncoder encodeBuffer failed"
            com.whatsapp.util.Log.e(r0, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.wawebrtc.MediaCodecVideoEncoder.encodeBuffer(boolean, int, int, long, long, int):boolean");
    }

    public boolean encodeTexture(boolean z, int i, float[] fArr, long j) {
        C90594Om r3;
        if (z) {
            try {
                if (Build.VERSION.SDK_INT >= 19) {
                    Log.i("MediaCodecVideoEncoder Sync frame request");
                    this.mediaCodec.setParameters(this.keyFrameRequestBundle);
                }
            } catch (RuntimeException e) {
                Log.e("MediaCodecVideoEncoder encodeTexture failed", e);
                return false;
            }
        }
        this.eglBase.A04();
        GLES20.glClear(16384);
        C94794cY r0 = this.drawer;
        int i2 = this.width;
        int i3 = this.height;
        Map map = r0.A00;
        if (map.containsKey("#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES oes_tex;\n\nvoid main() {\n  gl_FragColor = texture2D(oes_tex, interp_tc);\n}\n")) {
            r3 = (C90594Om) map.get("#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES oes_tex;\n\nvoid main() {\n  gl_FragColor = texture2D(oes_tex, interp_tc);\n}\n");
        } else {
            r3 = new C90594Om();
            map.put("#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES oes_tex;\n\nvoid main() {\n  gl_FragColor = texture2D(oes_tex, interp_tc);\n}\n", r3);
            C94204bQ r2 = r3.A01;
            int i4 = r2.A00;
            if (i4 != -1) {
                GLES20.glUseProgram(i4);
                C93054Yu.A01("glUseProgram");
                GLES20.glUniform1i(r2.A01("oes_tex"), 0);
                C93054Yu.A01("Initialize fragment shader uniform values.");
                r2.A02("in_pos", C94794cY.A01);
                r2.A02("in_tc", C94794cY.A02);
            } else {
                throw new RuntimeException("The program has been released");
            }
        }
        int i5 = r3.A01.A00;
        if (i5 != -1) {
            GLES20.glUseProgram(i5);
            C93054Yu.A01("glUseProgram");
            GLES20.glUniformMatrix4fv(r3.A00, 1, false, fArr, 0);
            GLES20.glActiveTexture(33984);
            GLES20.glBindTexture(36197, i);
            GLES20.glViewport(0, 0, i2, i3);
            GLES20.glDrawArrays(5, 0, 4);
            GLES20.glBindTexture(36197, 0);
            this.eglBase.A0C(TimeUnit.MICROSECONDS.toNanos(j));
            return true;
        }
        throw new RuntimeException("The program has been released");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0141, code lost:
        if (r7.startsWith("OMX.google") != false) goto L_0x0143;
     */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x031d  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0332  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0345 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List findHwEncoder(java.lang.String r22, java.lang.String[] r23, int[] r24, int r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 838
        */
        throw new UnsupportedOperationException("Method not decompiled: org.wawebrtc.MediaCodecVideoEncoder.findHwEncoder(java.lang.String, java.lang.String[], int[], int, boolean):java.util.List");
    }

    private int getCodecKeyFrameInterval() {
        return C39011p7.A0B(this.codecName, trustedCodecPrefixes) ? 4 : 2;
    }

    private int getDequeueOutputTimeout() {
        String str = this.codecName;
        boolean A0B = C39011p7.A0B(str, trustedCodecPrefixes);
        if ("OMX.google.h264.encoder".equalsIgnoreCase(str)) {
            return 100;
        }
        return A0B ? 30 : 1000;
    }

    private ByteBuffer getInputBuffer(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.mediaCodec.getInputBuffer(i);
        }
        return this.inputBuffers[i];
    }

    private ByteBuffer getOutputBuffer(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.mediaCodec.getOutputBuffer(i);
        }
        return this.outputBuffers[i];
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean initEncode(X.EnumC87054Aa r32, int r33, int r34, int r35, int r36, int r37, X.AnonymousClass5Pd r38) {
        /*
        // Method dump skipped, instructions count: 936
        */
        throw new UnsupportedOperationException("Method not decompiled: org.wawebrtc.MediaCodecVideoEncoder.initEncode(X.4Aa, int, int, int, int, int, X.5Pd):boolean");
    }

    public boolean initH264Encoder(int i, int i2, int i3, int i4, int i5) {
        return initEncode(EnumC87054Aa.VIDEO_CODEC_H264, i, i2, i3, i4, i5, null);
    }

    public boolean initH265Encoder(int i, int i2, int i3, int i4, int i5) {
        return initEncode(EnumC87054Aa.VIDEO_CODEC_H265, i, i2, i3, i4, i5, null);
    }

    public static boolean isH264HwSupported() {
        if (hwEncoderDisabledTypes.contains("video/avc") || findHwEncoder("video/avc", supportedH264HwCodecPrefixes, null, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isH264HwSupportedUsingTextures() {
        if (hwEncoderDisabledTypes.contains("video/avc") || findHwEncoder("video/avc", supportedH264HwCodecPrefixes, supportedSurfaceColorList, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isH265HwSupported() {
        if (hwEncoderDisabledTypes.contains("video/hevc") || findHwEncoder("video/hevc", supportedH265HwCodecPrefixes, null, -1, true) == null) {
            return false;
        }
        return true;
    }

    public static boolean isH265HwSupportedUsingTextures() {
        if (hwEncoderDisabledTypes.contains("video/hevc") || findHwEncoder("video/hevc", supportedH265HwCodecPrefixes, supportedSurfaceColorList, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isVp8HwSupported() {
        if (hwEncoderDisabledTypes.contains("video/x-vnd.on2.vp8") || findHwEncoder("video/x-vnd.on2.vp8", supportedVp8HwCodecPrefixes, null, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isVp8HwSupportedUsingTextures() {
        if (hwEncoderDisabledTypes.contains("video/x-vnd.on2.vp8") || findHwEncoder("video/x-vnd.on2.vp8", supportedVp8HwCodecPrefixes, supportedSurfaceColorList, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isVp9HwSupported() {
        if (hwEncoderDisabledTypes.contains("video/x-vnd.on2.vp9") || findHwEncoder("video/x-vnd.on2.vp9", supportedVp9HwCodecPrefixes, null, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isVp9HwSupportedUsingTextures() {
        if (hwEncoderDisabledTypes.contains("video/x-vnd.on2.vp9") || findHwEncoder("video/x-vnd.on2.vp9", supportedVp9HwCodecPrefixes, supportedSurfaceColorList, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static void printStackTrace() {
        Thread thread;
        StackTraceElement[] stackTrace;
        int length;
        MediaCodecVideoEncoder mediaCodecVideoEncoder = runningInstance;
        if (mediaCodecVideoEncoder != null && (thread = mediaCodecVideoEncoder.mediaCodecThread) != null && (length = (stackTrace = thread.getStackTrace()).length) > 0) {
            Log.i("MediaCodecVideoEncoder  stacks trace:");
            int i = 0;
            do {
                Log.i(stackTrace[i].toString());
                i++;
            } while (i < length);
        }
    }

    public void release() {
        StringBuilder sb = new StringBuilder("MediaCodecVideoEncoder Java releaseEncoder ");
        sb.append(this.codecName);
        Log.i(sb.toString());
        CountDownLatch countDownLatch = new CountDownLatch(1);
        new AnonymousClass1MS(new RunnableBRunnable0Shape8S0200000_I0_8(this, 48, countDownLatch), TAG).start();
        if (!AnonymousClass4F8.A00(countDownLatch)) {
            Log.e("MediaCodecVideoEncoder Media encoder release timeout");
            codecErrors++;
        }
        this.codecName = null;
        this.mediaCodec = null;
        this.mediaCodecThread = null;
        C94794cY r0 = this.drawer;
        if (r0 != null) {
            Map map = r0.A00;
            for (C90594Om r02 : map.values()) {
                C94204bQ r2 = r02.A01;
                Log.i("GlShader Deleting shader.");
                int i = r2.A00;
                if (i != -1) {
                    GLES20.glDeleteProgram(i);
                    r2.A00 = -1;
                }
            }
            map.clear();
            this.drawer = null;
        }
        AnonymousClass5Pf r03 = this.eglBase;
        if (r03 != null) {
            r03.A05();
            this.eglBase = null;
        }
        Surface surface = this.inputSurface;
        if (surface != null) {
            surface.release();
            this.inputSurface = null;
        }
        runningInstance = null;
        lastReleaseTimestamp = SystemClock.uptimeMillis();
        Log.i("MediaCodecVideoEncoder Java releaseEncoder done");
    }

    public boolean releaseOutputBuffer(int i) {
        try {
            this.mediaCodec.releaseOutputBuffer(i, false);
            return true;
        } catch (IllegalStateException e) {
            Log.e("MediaCodecVideoEncoder releaseOutputBuffer failed", e);
            return false;
        }
    }

    public boolean resetEncoderOnFPSChanges() {
        return "OMX.Exynos.AVC.Encoder".equalsIgnoreCase(this.codecName);
    }

    public static void setErrorCallback(AnonymousClass5S5 r1) {
        Log.i("MediaCodecVideoEncoder Set error callback");
        errorCallback = r1;
    }

    private boolean setRates(int i, int i2) {
        if (!supportUpdateBitrate()) {
            return false;
        }
        try {
            Bundle bundle = new Bundle();
            bundle.putInt("video-bitrate", i * 1000);
            this.mediaCodec.setParameters(bundle);
            return true;
        } catch (IllegalStateException e) {
            Log.e("MediaCodecVideoEncoder setRates failed", e);
            return false;
        }
    }

    public boolean supportForceKeyFrame() {
        return Build.VERSION.SDK_INT >= 19 && !"OMX.google.h264.encoder".equalsIgnoreCase(this.codecName);
    }

    public boolean supportUpdateBitrate() {
        return Build.VERSION.SDK_INT >= 19 && !"OMX.google.h264.encoder".equalsIgnoreCase(this.codecName);
    }

    /* loaded from: classes3.dex */
    public class BufferInfo {
        public int bitInfo;
        public ByteBuffer buffer;
        public long encodeTimeMs;
        public int index;
        public boolean isConfigData;
        public boolean isKeyFrame;
        public long timestamp;

        public void set(int i, ByteBuffer byteBuffer, boolean z, long j, long j2, int i2, boolean z2) {
            this.index = i;
            this.buffer = byteBuffer;
            this.isKeyFrame = z;
            this.timestamp = j;
            this.encodeTimeMs = j2;
            this.bitInfo = i2;
            this.isConfigData = z2;
        }
    }
}
