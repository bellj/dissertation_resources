package org.wawebrtc;

import X.AbstractC87664Ck;
import X.AnonymousClass1MS;
import X.AnonymousClass4AZ;
import X.AnonymousClass4F8;
import X.AnonymousClass4SD;
import X.AnonymousClass5S4;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C39011p7;
import X.C91154Qq;
import X.C93654aW;
import X.C93804al;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Build;
import android.os.SystemClock;
import android.view.Surface;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* loaded from: classes2.dex */
public class MediaCodecVideoDecoder {
    public static final int DEQUEUE_INPUT_TIMEOUT = 500000;
    public static final String H264_MIME_TYPE = "video/avc";
    public static final String H265_MIME_TYPE = "video/hevc";
    public static final long MAX_DECODE_TIME_MS = 500;
    public static final int MAX_QUEUED_OUTPUTBUFFERS = 3;
    public static final int MEDIA_CODEC_RELEASE_TIMEOUT_MS = 5000;
    public static final String TAG = "MediaCodecVideoDecoder";
    public static final String VP8_MIME_TYPE = "video/x-vnd.on2.vp8";
    public static final String VP9_MIME_TYPE = "video/x-vnd.on2.vp9";
    public static final String[] blacklistedDeviceBoard = new String[0];
    public static final String[] blacklistedHwCodecPrefixes = {"OMX.TI.DUCATI1.VIDEO.DECODER"};
    public static final Map cachedCodecCapabilites = C12970iu.A11();
    public static int codecErrors;
    public static AnonymousClass5S4 errorCallback;
    public static Set hwDecoderDisabledTypes = C12970iu.A12();
    public static long lastReleaseTimestamp;
    public static final String[] restartDecoderOnNewSpsPpsPrefixes = {"omx.mtk.", "OMX.SEC.avc.dec"};
    public static MediaCodecVideoDecoder runningInstance;
    public static final String[] spsBaselineProfileHackPrefixes = new String[0];
    public static final String[] spsBitstreamRestrictionsPrefixes = {"omx.qcom.", "omx.nvidia.", "omx.brcm.", "OMX.Exynos."};
    public static final String[] spsConstrainedHighProfilePrefixes = new String[0];
    public static final String[] supportedH264HwCodecPrefixes = null;
    public static final String[] supportedH265HwCodecPrefixes = null;
    public static final String[] supportedVp8HwCodecPrefixes = {"OMX.qcom.", "OMX.Nvidia.", "OMX.Exynos.", "OMX.Intel."};
    public static final String[] supportedVp9HwCodecPrefixes = C12990iw.A1b("OMX.qcom.", "OMX.Exynos.", 2);
    public MediaCodec.BufferInfo cachedBufferInfo = new MediaCodec.BufferInfo();
    public BufferInfo cachedInputBuffer = new BufferInfo();
    public BufferInfo cachedOutputBuffer = new BufferInfo();
    public final Queue carryAlongInfos = new LinkedList();
    public String codecName;
    public int colorFormat;
    public int colorId;
    public int cropBottom;
    public int cropLeft;
    public int cropRight;
    public int cropTop;
    public final Queue dequeuedSurfaceOutputBuffers = new LinkedList();
    public int droppedFrames;
    public final List freeInfos = new LinkedList();
    public boolean hasDecodedFirstFrame;
    public int height;
    public ByteBuffer[] inputBuffers;
    public MediaCodec mediaCodec;
    public Thread mediaCodecThread;
    public boolean needsRestartDecoderOnNewSpsPps;
    public boolean needsSpsBaselineProfileHack;
    public boolean needsSpsBitstreamRestrictions;
    public boolean needsSpsConstrainedHighProfile;
    public boolean needsSpsPpsInCsd;
    public ByteBuffer[] outputBuffers;
    public int sliceHeight;
    public int stride;
    public Surface surface = null;
    public C91154Qq textureListener;
    public boolean useSurface;
    public int width;

    private void checkOnMediaCodecThread() {
    }

    private int getDequeueOutputTimeout() {
        return 20;
    }

    public MediaCodecVideoDecoder() {
        int i = 0;
        do {
            this.freeInfos.add(new C93654aW());
            i++;
        } while (i < 10);
    }

    private void MaybeRenderDecodedTextureBuffer() {
        boolean A1W;
        if (!this.dequeuedSurfaceOutputBuffers.isEmpty()) {
            C91154Qq r0 = this.textureListener;
            synchronized (r0.A01) {
                A1W = C12960it.A1W(r0.A00);
            }
            if (!A1W) {
                BufferInfo bufferInfo = (BufferInfo) this.dequeuedSurfaceOutputBuffers.remove();
                C91154Qq r1 = this.textureListener;
                if (r1.A00 == null) {
                    r1.A00 = bufferInfo;
                    this.mediaCodec.releaseOutputBuffer(bufferInfo.index, true);
                    return;
                }
                Log.e("Unexpected addBufferToRender() called while waiting for a texture.");
                throw C12960it.A0U("Waiting for a texture.");
            }
        }
    }

    private BufferInfo dequeueInputBuffer() {
        MediaCodec mediaCodec = this.mediaCodec;
        if (mediaCodec != null) {
            try {
                int dequeueInputBuffer = mediaCodec.dequeueInputBuffer(500000);
                if (dequeueInputBuffer >= 0) {
                    this.cachedInputBuffer.set(dequeueInputBuffer, getInputBuffer(dequeueInputBuffer), 0, 0, 0, 0, 0, 0);
                    return this.cachedInputBuffer;
                }
            } catch (Throwable th) {
                Log.e(th);
                throw th;
            }
        }
        return null;
    }

    private BufferInfo dequeueOutputBuffer(int i) {
        int dequeueOutputBuffer;
        int i2;
        int i3;
        int i4;
        int i5;
        if (!this.carryAlongInfos.isEmpty()) {
            MediaCodec.BufferInfo bufferInfo = this.cachedBufferInfo;
            while (true) {
                dequeueOutputBuffer = this.mediaCodec.dequeueOutputBuffer(bufferInfo, TimeUnit.MILLISECONDS.toMicros((long) i));
                if (dequeueOutputBuffer != -3) {
                    if (dequeueOutputBuffer != -2) {
                        break;
                    }
                    MediaFormat outputFormat = this.mediaCodec.getOutputFormat();
                    Log.i(C12960it.A0d(outputFormat.toString(), C12960it.A0k("MediaCodecVideoDecoder Decoder format changed: ")));
                    C93804al A04 = C39011p7.A04(outputFormat, null, this.codecName);
                    int i6 = A04.A09;
                    this.width = i6;
                    int i7 = A04.A06;
                    this.height = i7;
                    this.stride = Math.max(i6, A04.A08);
                    this.sliceHeight = Math.max(i7, A04.A07);
                    int i8 = A04.A00;
                    this.colorFormat = i8;
                    this.colorId = getFrameConverterColorId(i8, A04.A05);
                    this.cropLeft = 0;
                    int i9 = this.width;
                    this.cropRight = i9 - 1;
                    this.cropTop = 0;
                    this.cropBottom = this.height - 1;
                    int i10 = A04.A02;
                    if (i10 >= 0 && i10 < (i4 = A04.A08) && (i5 = A04.A03) >= 0 && i5 < i4 && i10 < i5) {
                        this.cropLeft = i10;
                        this.cropRight = i5;
                        int i11 = (i5 - i10) + 1;
                        if (i11 < i9) {
                            this.width = i11;
                            Log.w(C12960it.A0W(i11, "MediaCodecVideoDecoder Decoder format changed, use cropRight and cropLeft to calculate width "));
                        }
                    }
                    int i12 = A04.A01;
                    if (i12 >= 0 && i12 < (i2 = A04.A07) && (i3 = A04.A04) >= 0 && i3 < i2 && i3 < i12) {
                        this.cropTop = i3;
                        this.cropBottom = i12;
                        int i13 = (i12 - i3) + 1;
                        if (i13 < this.height) {
                            this.height = i13;
                            Log.w(C12960it.A0W(i13, "MediaCodecVideoDecoder Decoder format changed, use cropBottom and cropTop to calculate height "));
                        }
                    }
                } else if (Build.VERSION.SDK_INT < 21) {
                    ByteBuffer[] outputBuffers = this.mediaCodec.getOutputBuffers();
                    this.outputBuffers = outputBuffers;
                    StringBuilder A0k = C12960it.A0k("MediaCodecVideoDecoder Decoder output buffers changed: ");
                    A0k.append(outputBuffers.length);
                    C12960it.A1F(A0k);
                }
            }
            if (dequeueOutputBuffer != -1) {
                this.hasDecodedFirstFrame = true;
                C93654aW r0 = (C93654aW) this.carryAlongInfos.remove();
                long elapsedRealtime = SystemClock.elapsedRealtime() - r0.A01;
                ByteBuffer outputBuffer = getOutputBuffer(dequeueOutputBuffer);
                outputBuffer.position(bufferInfo.offset);
                outputBuffer.limit(bufferInfo.offset + bufferInfo.size);
                this.cachedOutputBuffer.set(dequeueOutputBuffer, outputBuffer.slice(), TimeUnit.MICROSECONDS.toMillis(bufferInfo.presentationTimeUs), r0.A03, r0.A02, r0.A00, elapsedRealtime, SystemClock.elapsedRealtime());
                this.freeInfos.add(r0);
                return this.cachedOutputBuffer;
            }
        }
        return null;
    }

    private DecodedTextureBuffer dequeueTextureBuffer(int i) {
        StringBuilder A0k;
        String str;
        if (this.useSurface) {
            BufferInfo dequeueOutputBuffer = dequeueOutputBuffer(i);
            if (dequeueOutputBuffer != null) {
                this.dequeuedSurfaceOutputBuffers.add(dequeueOutputBuffer);
            }
            MaybeRenderDecodedTextureBuffer();
            C91154Qq r0 = this.textureListener;
            Object obj = r0.A01;
            synchronized (obj) {
                if (i > 0) {
                    if (r0.A00 != null) {
                        try {
                            obj.wait((long) i);
                        } catch (InterruptedException unused) {
                            Thread.currentThread().interrupt();
                        }
                    }
                }
            }
            if (this.dequeuedSurfaceOutputBuffers.size() < Math.min(3, this.outputBuffers.length) && (i <= 0 || this.dequeuedSurfaceOutputBuffers.isEmpty())) {
                return null;
            }
            this.droppedFrames++;
            BufferInfo bufferInfo = (BufferInfo) this.dequeuedSurfaceOutputBuffers.remove();
            if (i > 0) {
                A0k = C12960it.A0h();
                str = "MediaCodecVideoDecoder Draining decoder. Dropping frame with TS: ";
            } else {
                A0k = C12960it.A0k("MediaCodecVideoDecoder Too many output buffers ");
                A0k.append(this.dequeuedSurfaceOutputBuffers.size());
                str = ". Dropping frame with TS: ";
            }
            A0k.append(str);
            A0k.append(bufferInfo.presentationTimeStampMs);
            A0k.append(". Total number of dropped frames: ");
            Log.w(C12960it.A0f(A0k, this.droppedFrames));
            this.mediaCodec.releaseOutputBuffer(bufferInfo.index, false);
            return new DecodedTextureBuffer(0, null, bufferInfo.presentationTimeStampMs, bufferInfo.timeStampMs, bufferInfo.ntpTimeStampMs, bufferInfo.decodeTimeMs, SystemClock.elapsedRealtime() - bufferInfo.endDecodeTimeMs);
        }
        throw C12960it.A0U("dequeueTexture() called for byte buffer decoding.");
    }

    public static void disableH264HwCodec() {
        Log.w("MediaCodecVideoDecoder H.264 decoding is disabled by application.");
        hwDecoderDisabledTypes.add("video/avc");
    }

    public static void disableH265HwCodec() {
        Log.w("MediaCodecVideoDecoder H.265 decoding is disabled by application.");
        hwDecoderDisabledTypes.add("video/hevc");
    }

    public static void disableVp8HwCodec() {
        Log.w("MediaCodecVideoDecoder VP8 decoding is disabled by application.");
        hwDecoderDisabledTypes.add("video/x-vnd.on2.vp8");
    }

    public static void disableVp9HwCodec() {
        Log.w("MediaCodecVideoDecoder VP9 decoding is disabled by application.");
        hwDecoderDisabledTypes.add("video/x-vnd.on2.vp9");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0170, code lost:
        if (r8.isFeatureSupported("adaptive-playback") == false) goto L_0x0172;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List findDecoder(java.lang.String r20, java.lang.String[] r21, int r22, boolean r23) {
        /*
        // Method dump skipped, instructions count: 581
        */
        throw new UnsupportedOperationException("Method not decompiled: org.wawebrtc.MediaCodecVideoDecoder.findDecoder(java.lang.String, java.lang.String[], int, boolean):java.util.List");
    }

    public static int getDecoderImplFromString(String str) {
        if ("SW".equalsIgnoreCase(str)) {
            return 1;
        }
        return "HW".equalsIgnoreCase(str) ? 2 : 0;
    }

    private int getFrameConverterColorId(int i, int i2) {
        Integer A01 = Voip.A01("vid_driver.decoder_frame_converter_color_format");
        String A07 = Voip.A07("vid_driver.decoder_name");
        Integer A012 = Voip.A01("vid_driver.decoder_color_format");
        return (A01 == null || A07 == null || A012 == null || !A07.equalsIgnoreCase(this.codecName) || i != A012.intValue()) ? i2 : A01.intValue();
    }

    private ByteBuffer getInputBuffer(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.mediaCodec.getInputBuffer(i);
        }
        return this.inputBuffers[i];
    }

    private ByteBuffer getOutputBuffer(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.mediaCodec.getOutputBuffer(i);
        }
        return this.outputBuffers[i];
    }

    private boolean initDecode(AnonymousClass4AZ r11, int i, int i2, AbstractC87664Ck r14, byte[] bArr, byte[] bArr2, boolean z) {
        String[] strArr;
        String str;
        if (this.mediaCodecThread == null) {
            this.useSurface = C12960it.A1W(r14);
            if (r11 == AnonymousClass4AZ.VIDEO_CODEC_VP8) {
                strArr = supportedVp8HwCodecPrefixes;
                str = "video/x-vnd.on2.vp8";
            } else if (r11 == AnonymousClass4AZ.VIDEO_CODEC_VP9) {
                strArr = supportedVp9HwCodecPrefixes;
                str = "video/x-vnd.on2.vp9";
            } else if (r11 == AnonymousClass4AZ.VIDEO_CODEC_H264) {
                strArr = supportedH264HwCodecPrefixes;
                str = "video/avc";
            } else if (r11 == AnonymousClass4AZ.VIDEO_CODEC_H265) {
                strArr = supportedH265HwCodecPrefixes;
                str = "video/hevc";
            } else {
                throw C12990iw.A0m(C12960it.A0b("initDecode: Non-supported codec ", r11));
            }
            try {
                List<AnonymousClass4SD> findDecoder = findDecoder(str, strArr, -1, C12960it.A1U(((SystemClock.uptimeMillis() - lastReleaseTimestamp) > 3000 ? 1 : ((SystemClock.uptimeMillis() - lastReleaseTimestamp) == 3000 ? 0 : -1))));
                if (findDecoder == null || findDecoder.isEmpty()) {
                    Log.e(C12960it.A0d(str, C12960it.A0k("MediaCodecVideoDecoder Can not find HW decoder for ")));
                } else {
                    runningInstance = this;
                    this.mediaCodecThread = Thread.currentThread();
                    this.freeInfos.addAll(this.carryAlongInfos);
                    this.carryAlongInfos.clear();
                    for (AnonymousClass4SD r1 : findDecoder) {
                        StringBuilder A0k = C12960it.A0k("MediaCodecVideoDecoder Java initDecode: ");
                        A0k.append(r11);
                        A0k.append(" : ");
                        A0k.append(i);
                        A0k.append(" x ");
                        A0k.append(i2);
                        A0k.append(". Color: 0x");
                        int i3 = r1.A00;
                        A0k.append(Integer.toHexString(i3));
                        A0k.append(". Use Surface: ");
                        A0k.append(this.useSurface);
                        A0k.append(". Decoder: ");
                        String str2 = r1.A02;
                        Log.i(C12960it.A0d(str2, A0k));
                        try {
                            this.codecName = str2;
                            this.width = i;
                            this.height = i2;
                            this.stride = i;
                            this.sliceHeight = i2;
                            this.cropLeft = 0;
                            this.cropRight = i - 1;
                            this.cropTop = 0;
                            this.cropBottom = i2 - 1;
                        } catch (Throwable th) {
                            Log.e("MediaCodecVideoDecoder initDecode failed with Exception", th);
                        }
                        if (this.useSurface) {
                            new C91154Qq(r14);
                            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                        }
                        MediaFormat createVideoFormat = MediaFormat.createVideoFormat(str, i, i2);
                        if (bArr != null) {
                            StringBuilder A0h = C12960it.A0h();
                            A0h.append("MediaCodecVideoDecoder Java initDecode: csd-0 ");
                            Log.i(C12960it.A0d(Arrays.toString(bArr), A0h));
                            createVideoFormat.setByteBuffer("csd-0", ByteBuffer.wrap(bArr));
                        }
                        if (bArr2 != null) {
                            StringBuilder A0h2 = C12960it.A0h();
                            A0h2.append("MediaCodecVideoDecoder Java initDecode: csd-1 ");
                            Log.i(C12960it.A0d(Arrays.toString(bArr2), A0h2));
                            createVideoFormat.setByteBuffer("csd-1", ByteBuffer.wrap(bArr2));
                        }
                        if (!this.useSurface) {
                            createVideoFormat.setInteger("color-format", i3);
                        }
                        StringBuilder A0h3 = C12960it.A0h();
                        A0h3.append("MediaCodecVideoDecoder   Format: ");
                        A0h3.append(createVideoFormat);
                        C12960it.A1F(A0h3);
                        MediaCodec createByCodecName = MediaCodecVideoEncoder.createByCodecName(str2);
                        this.mediaCodec = createByCodecName;
                        if (createByCodecName == null) {
                            Log.e("MediaCodecVideoDecoder Can not create media decoder");
                            if (!z) {
                                break;
                            }
                        } else {
                            createByCodecName.configure(createVideoFormat, this.surface, (MediaCrypto) null, 0);
                            this.mediaCodec.start();
                            C93804al A04 = C39011p7.A04(createVideoFormat, null, this.codecName);
                            int i4 = A04.A00;
                            this.colorFormat = i4;
                            this.colorId = getFrameConverterColorId(i4, A04.A05);
                            if (Build.VERSION.SDK_INT < 21) {
                                this.outputBuffers = this.mediaCodec.getOutputBuffers();
                                ByteBuffer[] inputBuffers = this.mediaCodec.getInputBuffers();
                                this.inputBuffers = inputBuffers;
                                StringBuilder A0h4 = C12960it.A0h();
                                A0h4.append("MediaCodecVideoDecoder Input buffers: ");
                                A0h4.append(inputBuffers.length);
                                A0h4.append(". Output buffers: ");
                                A0h4.append(this.outputBuffers.length);
                                C12960it.A1F(A0h4);
                            }
                            this.hasDecodedFirstFrame = false;
                            this.dequeuedSurfaceOutputBuffers.clear();
                            this.droppedFrames = 0;
                            setDecoderFlags(r1);
                            return true;
                        }
                    }
                }
                return false;
            } catch (Throwable th2) {
                Log.e("MediaCodecVideoDecoder Exception in findDecoder", th2);
                return false;
            }
        } else {
            throw C12990iw.A0m("initDecode: Forgot to release()?");
        }
    }

    private boolean initH264Decoder(int i, int i2, byte[] bArr, byte[] bArr2) {
        return initH264Decoder(i, i2, bArr, bArr2, false);
    }

    private boolean initH264Decoder(int i, int i2, byte[] bArr, byte[] bArr2, boolean z) {
        AnonymousClass4SD r4 = null;
        try {
            List findDecoder = findDecoder("video/avc", supportedH264HwCodecPrefixes, -1, false);
            if (findDecoder != null && !findDecoder.isEmpty()) {
                r4 = (AnonymousClass4SD) findDecoder.get(0);
            }
        } catch (Throwable th) {
            Log.e("MediaCodecVideoDecoder Exception in findDecoder", th);
        }
        setDecoderFlags(r4);
        return initDecode(AnonymousClass4AZ.VIDEO_CODEC_H264, i, i2, null, bArr, bArr2, z);
    }

    private boolean initH265Decoder(int i, int i2, byte[] bArr, byte[] bArr2) {
        return initH265Decoder(i, i2, bArr, bArr2, false);
    }

    private boolean initH265Decoder(int i, int i2, byte[] bArr, byte[] bArr2, boolean z) {
        AnonymousClass4SD r4 = null;
        try {
            List findDecoder = findDecoder("video/hevc", supportedH265HwCodecPrefixes, -1, true);
            if (findDecoder != null && !findDecoder.isEmpty()) {
                r4 = (AnonymousClass4SD) C12980iv.A0o(findDecoder);
            }
        } catch (Throwable th) {
            Log.e("MediaCodecVideoDecoder Exception in findDecoder", th);
        }
        setDecoderFlags(r4);
        return initDecode(AnonymousClass4AZ.VIDEO_CODEC_H265, i, i2, null, bArr, bArr2, z);
    }

    public static boolean isH264HwSupported() {
        if (hwDecoderDisabledTypes.contains("video/avc") || findDecoder("video/avc", supportedH264HwCodecPrefixes, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isH265HwSupported() {
        if (hwDecoderDisabledTypes.contains("video/hevc") || findDecoder("video/hevc", supportedH265HwCodecPrefixes, -1, true) == null) {
            return false;
        }
        return true;
    }

    public static boolean isSoftwareCodec(MediaCodecInfo mediaCodecInfo) {
        if (Build.VERSION.SDK_INT >= 29) {
            return Api29Impl.mediaCodecInfoIsSoftwareOnly(mediaCodecInfo);
        }
        String name = mediaCodecInfo.getName();
        return name.equalsIgnoreCase("OMX.google.h264.decoder") || name.equalsIgnoreCase("c2.android.avc.decoder") || name.toLowerCase(Locale.US).contains(".sw.");
    }

    public static boolean isVp8HwSupported() {
        if (hwDecoderDisabledTypes.contains("video/x-vnd.on2.vp8") || findDecoder("video/x-vnd.on2.vp8", supportedVp8HwCodecPrefixes, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static boolean isVp9HwSupported() {
        if (hwDecoderDisabledTypes.contains("video/x-vnd.on2.vp9") || findDecoder("video/x-vnd.on2.vp9", supportedVp9HwCodecPrefixes, -1, false) == null) {
            return false;
        }
        return true;
    }

    public static void printStackTrace() {
        Thread thread;
        StackTraceElement[] stackTrace;
        int length;
        MediaCodecVideoDecoder mediaCodecVideoDecoder = runningInstance;
        if (mediaCodecVideoDecoder != null && (thread = mediaCodecVideoDecoder.mediaCodecThread) != null && (length = (stackTrace = thread.getStackTrace()).length) > 0) {
            Log.i("MediaCodecVideoDecoder MediaCodecVideoDecoder stacks trace:");
            int i = 0;
            do {
                C12960it.A1F(stackTrace[i]);
                i++;
            } while (i < length);
        }
    }

    private boolean queueInputBuffer(int i, int i2, long j, long j2, int i3) {
        C93654aW r4;
        try {
            ByteBuffer inputBuffer = getInputBuffer(i);
            inputBuffer.position(0);
            inputBuffer.limit(i2);
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (this.freeInfos.isEmpty()) {
                r4 = new C93654aW(i3, elapsedRealtime, j, j2);
            } else {
                r4 = (C93654aW) this.freeInfos.remove(0);
                r4.A01 = elapsedRealtime;
                r4.A03 = j;
                r4.A02 = j2;
                r4.A00 = i3;
            }
            this.carryAlongInfos.add(r4);
            this.mediaCodec.queueInputBuffer(i, 0, i2, 1000 * elapsedRealtime, 0);
            return true;
        } catch (IllegalStateException e) {
            Log.e("MediaCodecVideoDecoder decode failed", e);
            return false;
        }
    }

    private void release() {
        StringBuilder A0k = C12960it.A0k("MediaCodecVideoDecoder Java releaseDecoder ");
        A0k.append(this.codecName);
        A0k.append(". Total number of dropped frames: ");
        A0k.append(this.droppedFrames);
        C12960it.A1F(A0k);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        new AnonymousClass1MS(new RunnableBRunnable0Shape11S0200000_I1_1(this, 46, countDownLatch), TAG).start();
        if (!AnonymousClass4F8.A00(countDownLatch)) {
            Log.e("MediaCodecVideoDecoder Media decoder release timeout");
            codecErrors++;
        }
        this.mediaCodec = null;
        this.mediaCodecThread = null;
        runningInstance = null;
        lastReleaseTimestamp = SystemClock.uptimeMillis();
        if (this.useSurface) {
            this.surface.release();
            this.surface = null;
            throw C12980iv.A0n("dispose");
        }
        Log.i("MediaCodecVideoDecoder Java releaseDecoder done");
    }

    private void reset(int i, int i2) {
        if (this.mediaCodecThread == null || this.mediaCodec == null) {
            throw C12990iw.A0m("Incorrect reset call for non-initialized decoder.");
        }
        StringBuilder A0k = C12960it.A0k("MediaCodecVideoDecoder Java reset: ");
        A0k.append(i);
        A0k.append(" x ");
        A0k.append(i2);
        C12960it.A1F(A0k);
        this.mediaCodec.flush();
        this.codecName = null;
        this.width = i;
        this.height = i2;
        this.freeInfos.addAll(this.carryAlongInfos);
        this.carryAlongInfos.clear();
        this.dequeuedSurfaceOutputBuffers.clear();
        this.hasDecodedFirstFrame = false;
        this.droppedFrames = 0;
    }

    private void returnDecodedOutputBuffer(int i) {
        if (!this.useSurface) {
            this.mediaCodec.releaseOutputBuffer(i, false);
            return;
        }
        throw C12960it.A0U("returnDecodedOutputBuffer() called for surface decoding.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005b, code lost:
        if (r7.intValue() > 0) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00af, code lost:
        if (r9.A01.isFeatureSupported("adaptive-playback") != false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x002c, code lost:
        if (r1 == 0) goto L_0x002e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00b6 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setDecoderFlags(X.AnonymousClass4SD r9) {
        /*
        // Method dump skipped, instructions count: 339
        */
        throw new UnsupportedOperationException("Method not decompiled: org.wawebrtc.MediaCodecVideoDecoder.setDecoderFlags(X.4SD):void");
    }

    public static void setErrorCallback(AnonymousClass5S4 r1) {
        Log.i("MediaCodecVideoDecoder Set error callback");
        errorCallback = r1;
    }

    /* loaded from: classes3.dex */
    public class DecodedTextureBuffer {
        public final long decodeTimeMs;
        public final long frameDelayMs;
        public final long ntpTimeStampMs;
        public final long presentationTimeStampMs;
        public final int textureID;
        public final long timeStampMs;
        public final float[] transformMatrix;

        public DecodedTextureBuffer(int i, float[] fArr, long j, long j2, long j3, long j4, long j5) {
            this.textureID = i;
            this.transformMatrix = fArr;
            this.presentationTimeStampMs = j;
            this.timeStampMs = j2;
            this.ntpTimeStampMs = j3;
            this.decodeTimeMs = j4;
            this.frameDelayMs = j5;
        }
    }

    /* loaded from: classes3.dex */
    public class Api29Impl {
        public static boolean mediaCodecInfoIsSoftwareOnly(MediaCodecInfo mediaCodecInfo) {
            return mediaCodecInfo.isSoftwareOnly();
        }
    }

    /* loaded from: classes3.dex */
    public class BufferInfo {
        public int bitInfo;
        public ByteBuffer buffer;
        public long decodeTimeMs;
        public long endDecodeTimeMs;
        public int index;
        public long ntpTimeStampMs;
        public long presentationTimeStampMs;
        public long timeStampMs;

        public void set(int i, ByteBuffer byteBuffer, long j, long j2, long j3, int i2, long j4, long j5) {
            this.index = i;
            this.buffer = byteBuffer;
            this.presentationTimeStampMs = j;
            this.timeStampMs = j2;
            this.ntpTimeStampMs = j3;
            this.bitInfo = i2;
            this.decodeTimeMs = j4;
            this.endDecodeTimeMs = j5;
        }
    }
}
