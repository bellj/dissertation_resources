package org.pjsip;

import X.AnonymousClass048;
import X.AnonymousClass1JO;
import X.AnonymousClass1YJ;
import X.AnonymousClass2NR;
import android.os.Build;
import android.util.Pair;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.pjsip.PjCameraInfo;

/* loaded from: classes2.dex */
public final class PjCameraInfo {
    public static final Comparator CAMERA_SIZE_COMPARATOR = new Comparator() { // from class: X.5CW
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            return PjCameraInfo.lambda$static$0((AnonymousClass2NR) obj, (AnonymousClass2NR) obj2);
        }
    };
    public static final Pair PAIR_1280_720 = new Pair(1280, 720);
    public static final AnonymousClass1JO ZOOMING_OPPO_MODELS;
    public static final AnonymousClass1JO ZOOMING_VIVO_MODELS;
    public final int facing;
    public final int orient;
    public final int[] supportedFormat;
    public final int[] supportedSize;

    static {
        AnonymousClass1YJ r1 = new AnonymousClass1YJ();
        r1.A02("CPH2023");
        r1.A02("CPH2025");
        r1.A02("CPH2363");
        ZOOMING_OPPO_MODELS = r1.A00();
        AnonymousClass1YJ r12 = new AnonymousClass1YJ();
        r12.A02("V2027");
        r12.A02("V2029");
        r12.A02("V2130");
        ZOOMING_VIVO_MODELS = r12.A00();
    }

    public PjCameraInfo(int i, int i2, int[] iArr, int[] iArr2) {
        this.facing = i;
        this.orient = i2;
        this.supportedSize = iArr;
        this.supportedFormat = iArr2;
    }

    public static int[] SizeListToIntArray(List list) {
        int[] iArr = new int[list.size() << 1];
        Iterator it = list.iterator();
        int i = 0;
        while (it.hasNext()) {
            AnonymousClass2NR r2 = (AnonymousClass2NR) it.next();
            int i2 = i + 1;
            iArr[i] = r2.A01;
            i = i2 + 1;
            iArr[i2] = r2.A00;
        }
        return iArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x015f A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x018c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.pjsip.PjCameraInfo createFromRawInfo(X.AnonymousClass2NQ r13, X.C17140qK r14) {
        /*
        // Method dump skipped, instructions count: 436
        */
        throw new UnsupportedOperationException("Method not decompiled: org.pjsip.PjCameraInfo.createFromRawInfo(X.2NQ, X.0qK):org.pjsip.PjCameraInfo");
    }

    public static Pair deviceSpecificSize(boolean z) {
        String str = Build.MANUFACTURER;
        if (!"asus".equalsIgnoreCase(str)) {
            if (!"samsung".equalsIgnoreCase(str)) {
                if (!z) {
                    return null;
                }
                String str2 = Build.MODEL;
                if (!"Pixel 6".equals(str2) && !"Pixel 6 Pro".equals(str2) && ((!"OPPO".equals(str) || !ZOOMING_OPPO_MODELS.A00.contains(str2)) && (!"vivo".equals(str) || !ZOOMING_VIVO_MODELS.A00.contains(str2)))) {
                    return null;
                }
            }
            return PAIR_1280_720;
        } else if (!z || Build.VERSION.SDK_INT != 19) {
            return null;
        } else {
            return new Pair(Integer.valueOf((int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), 768);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003a, code lost:
        if (r1.equalsIgnoreCase("ks01lte") == false) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0061, code lost:
        if (r1.startsWith("hwG") != false) goto L_0x003c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int[] getEncoderSupportedColorFormats(X.C17140qK r3) {
        /*
            android.content.SharedPreferences r2 = r3.A01()
            java.lang.String r1 = "video_encoder_frame_convertor_color_id"
            r0 = -1
            int r3 = r2.getInt(r1, r0)
            java.lang.String r1 = android.os.Build.MANUFACTURER
            java.lang.String r0 = "samsung"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0046
            java.lang.String r1 = android.os.Build.BOARD
            java.lang.String r0 = "MSM8960"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 != 0) goto L_0x003c
            java.lang.String r0 = "universal7580"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 != 0) goto L_0x003c
            java.lang.String r1 = android.os.Build.DEVICE
            java.lang.String r0 = "xcover3lte"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 != 0) goto L_0x003c
            java.lang.String r0 = "ks01lte"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0046
        L_0x003c:
            r2 = 1
        L_0x003d:
            r1 = 3
            if (r2 == 0) goto L_0x0064
            int[] r0 = new int[r1]
            r0 = {x0088: FILL_ARRAY_DATA  , data: [17, 35, 842094169} // fill-array
            return r0
        L_0x0046:
            java.lang.String r1 = android.os.Build.BOARD
            java.lang.String r0 = "7x27"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 != 0) goto L_0x003c
            java.lang.String r1 = android.os.Build.DEVICE
            java.lang.String r0 = "hwY"
            boolean r0 = r1.startsWith(r0)
            if (r0 != 0) goto L_0x003c
            java.lang.String r0 = "hwG"
            boolean r0 = r1.startsWith(r0)
            r2 = 0
            if (r0 == 0) goto L_0x003d
            goto L_0x003c
        L_0x0064:
            r0 = 1
            if (r3 != r0) goto L_0x006d
            int[] r0 = new int[r1]
            r0 = {x0092: FILL_ARRAY_DATA  , data: [35, 842094169, 17} // fill-array
            return r0
        L_0x006d:
            r0 = 2
            if (r3 != r0) goto L_0x0076
            int[] r0 = new int[r1]
            r0 = {x009c: FILL_ARRAY_DATA  , data: [842094169, 35, 17} // fill-array
            return r0
        L_0x0076:
            if (r3 == r1) goto L_0x0081
            r0 = 4
            if (r3 == r0) goto L_0x0081
            int[] r0 = new int[r1]
            r0 = {x00a6: FILL_ARRAY_DATA  , data: [35, 842094169, 17} // fill-array
            return r0
        L_0x0081:
            int[] r0 = new int[r1]
            r0 = {x00b0: FILL_ARRAY_DATA  , data: [17, 35, 842094169} // fill-array
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.pjsip.PjCameraInfo.getEncoderSupportedColorFormats(X.0qK):int[]");
    }

    public static /* synthetic */ int lambda$static$0(AnonymousClass2NR r2, AnonymousClass2NR r3) {
        int i = r2.A01;
        int i2 = r3.A01;
        if (i > i2) {
            return -1;
        }
        if (i == i2) {
            return AnonymousClass048.A00(r3.A00, r2.A00);
        }
        return 1;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder("facing ");
        if (this.facing == 0) {
            str = "back";
        } else {
            str = "front";
        }
        sb.append(str);
        sb.append(", orientation: ");
        sb.append(this.orient);
        sb.append(", returned preview formats: ");
        sb.append(Arrays.toString(this.supportedFormat));
        sb.append(", returned preview size: ");
        sb.append(Arrays.toString(this.supportedSize));
        return sb.toString();
    }
}
