package org.pjsip;

import X.AbstractC1311461l;
import X.AnonymousClass009;
import X.AnonymousClass01d;
import X.AnonymousClass2NP;
import X.AnonymousClass2NT;
import X.AnonymousClass2NY;
import X.C14850m9;
import X.C92444Vx;
import X.C92704Xc;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.camera.VoipCamera;
import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import org.pjsip.PjCamera;

/* loaded from: classes2.dex */
public class PjCamera extends VoipPhysicalCamera implements Camera.PreviewCallback {
    public volatile Point adjustedPreviewSize;
    public final int camIdx;
    public Camera camera;
    public final AnonymousClass2NT cameraInfo;
    public final AtomicInteger cameraStartMode = new AtomicInteger(0);
    public final Object frameLock = new Object();
    public boolean isRunning;
    public volatile byte[] lastCachedFrameData;
    public volatile boolean newFrameAvailable;
    public boolean receivedCameraError = false;
    public final AnonymousClass01d systemServices;

    public PjCamera(int i, int i2, int i3, int i4, int i5, AnonymousClass01d r16, C14850m9 r17) {
        super(r17);
        StringBuilder sb = new StringBuilder("voip/video/VoipCamera/create idx: ");
        sb.append(i);
        sb.append(", size:");
        sb.append(i2);
        sb.append("x");
        sb.append(i3);
        sb.append(", format: 0x");
        sb.append(Integer.toHexString(i4));
        sb.append(", fps * 1000: ");
        sb.append(i5);
        sb.append(", this ");
        sb.append(this);
        sb.append(", class ");
        Class<?> cls = getClass();
        sb.append(cls);
        sb.append("@");
        sb.append(cls.hashCode());
        sb.append(", class loader ");
        sb.append(PjCamera.class.getClassLoader());
        sb.append(", hash: ");
        sb.append(System.identityHashCode(PjCamera.class.getClassLoader()));
        Log.i(sb.toString());
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(i, cameraInfo);
        this.systemServices = r16;
        this.camIdx = i;
        this.cameraInfo = new AnonymousClass2NT(i2, i3, i4, i5, cameraInfo.orientation, i, cameraInfo.facing == 1);
        if (this.cameraProcessor != null) {
            Log.i("voip/video/VoipCamera/ Camera Processor: CPU-frame channel Processor->VoIP setup");
            this.cameraProcessor.Acc(this, this.cameraThreadHandler);
        }
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public void closeOnCameraThread() {
        Log.i("voip/video/VoipCamera/closeOnCameraThread");
        AnonymousClass009.A0A("close should only be called after stop.", !this.isRunning);
        this.cameraEventsDispatcher.A00();
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public Point getAdjustedPreviewSize() {
        return this.adjustedPreviewSize;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public AnonymousClass2NT getCameraInfo() {
        return this.cameraInfo;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int getCameraStartMode() {
        return this.cameraStartMode.get();
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public AnonymousClass2NY getLastCachedFrame() {
        byte[] bArr = this.lastCachedFrameData;
        if (bArr == null) {
            return null;
        }
        AnonymousClass2NY r2 = new AnonymousClass2NY();
        r2.A05 = bArr;
        AnonymousClass2NT r1 = this.cameraInfo;
        r2.A03 = r1.A05;
        r2.A01 = r1.A02;
        r2.A00 = r1.A00;
        r2.A02 = r1.A04;
        r2.A04 = r1.A06;
        return r2;
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int getLatestFrame(ByteBuffer byteBuffer) {
        AnonymousClass009.A0A("Should be here only in passive mode", this.passiveMode);
        if (!this.passiveMode) {
            return -1;
        }
        synchronized (this.frameLock) {
            if (!this.newFrameAvailable) {
                return 0;
            }
            int min = Math.min(byteBuffer.capacity(), this.lastCachedFrameData.length);
            byteBuffer.rewind();
            byteBuffer.order(ByteOrder.nativeOrder());
            byteBuffer.put(this.lastCachedFrameData, 0, min);
            this.newFrameAvailable = false;
            return min;
        }
    }

    public static int getPreviewSizeForFormat(int i, int i2, int i3) {
        if (i3 != 842094169) {
            return ((i * i2) * ImageFormat.getBitsPerPixel(i3)) >> 3;
        }
        int ceil = ((int) Math.ceil(((double) i) / 16.0d)) << 4;
        return Math.max(((i * i2) * 3) >> 1, (ceil * i2) + ((((((int) Math.ceil(((double) (ceil >> 1)) / 16.0d)) << 4) * i2) >> 1) << 1));
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void lambda$startOnCameraThread$0(int i, Camera camera) {
        StringBuilder sb = new StringBuilder("camera error occurred: ");
        sb.append(i);
        Log.e(sb.toString());
        this.receivedCameraError = true;
        if (i != 2) {
            C92704Xc r3 = this.cameraEventsDispatcher;
            if (i != 100) {
                r3.A02();
                return;
            }
            Iterator it = r3.A00.iterator();
            while (it.hasNext()) {
                ((AnonymousClass2NP) it.next()).AVt(r3.A01);
            }
            return;
        }
        this.cameraEventsDispatcher.A01();
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public void onFrameAvailableOnCameraThread() {
        VideoPort videoPort = this.videoPort;
        if (videoPort == null) {
            Log.e("voip/video/VoipCamera/videoport null while receiving frames");
            return;
        }
        C92444Vx r2 = this.textureHolder;
        if (r2 != null) {
            AnonymousClass2NT r0 = this.cameraInfo;
            videoPort.renderTexture(r2, r0.A05, r0.A02);
        }
    }

    @Override // android.hardware.Camera.PreviewCallback
    public void onPreviewFrame(byte[] bArr, Camera camera) {
        byte[] bArr2;
        if (!(camera == null || bArr == null)) {
            Camera camera2 = this.camera;
            if (camera != camera2) {
                StringBuilder sb = new StringBuilder("Unexpected camera in callback! current camera = ");
                sb.append(camera2);
                sb.append(", callback camera is ");
                sb.append(camera);
                Log.w(sb.toString());
                return;
            }
            updateCameraCallbackCheck();
            if (this.isRunning) {
                if (this.passiveMode) {
                    synchronized (this.frameLock) {
                        bArr2 = this.lastCachedFrameData;
                        this.lastCachedFrameData = bArr;
                        this.newFrameAvailable = true;
                    }
                    bArr = bArr2;
                } else {
                    for (Map.Entry entry : this.virtualCameras.entrySet()) {
                        if (((VoipCamera) entry.getValue()).started) {
                            ((VoipCamera) entry.getValue()).frameCallback(bArr, bArr.length);
                        }
                    }
                    this.lastCachedFrameData = bArr;
                }
            }
            if (this.cameraStartMode.get() == 0) {
                camera.addCallbackBuffer(bArr);
            }
        }
    }

    private int preparePreviewOnCameraThread() {
        AnonymousClass009.A05(this.videoPort);
        AnonymousClass2NT r0 = this.cameraInfo;
        createTexture(r0.A05, r0.A02);
        try {
            AbstractC1311461l r1 = this.cameraProcessor;
            if (r1 == null || this.textureHolder == null) {
                C92444Vx r02 = this.textureHolder;
                if (r02 != null) {
                    Camera camera = this.camera;
                    AnonymousClass009.A05(camera);
                    camera.setPreviewTexture(r02.A01);
                } else {
                    this.cameraProcessor = null;
                    Camera camera2 = this.camera;
                    AnonymousClass009.A05(camera2);
                    SurfaceHolder surfaceHolder = this.videoPort.getSurfaceHolder();
                    AnonymousClass009.A05(surfaceHolder);
                    camera2.setPreviewDisplay(surfaceHolder);
                }
            } else {
                SurfaceTexture ABC = r1.ABC();
                boolean z = false;
                if (ABC != null) {
                    z = true;
                }
                AnonymousClass009.A0A("camera processor should output non null texture", z);
                AnonymousClass2NT r03 = this.cameraInfo;
                ABC.setDefaultBufferSize(r03.A05, r03.A02);
                Camera camera3 = this.camera;
                AnonymousClass009.A05(camera3);
                camera3.setPreviewTexture(ABC);
            }
            this.videoPort.setScaleType(0);
            return 0;
        } catch (IOException e) {
            Log.e(e);
            return -2;
        }
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int setVideoPortOnCameraThread(VideoPort videoPort) {
        int i;
        int i2;
        VideoPort videoPort2 = this.videoPort;
        int i3 = 0;
        if (videoPort2 != videoPort) {
            StringBuilder sb = new StringBuilder("voip/video/VoipCamera/setVideoPortOnCameraThread to ");
            if (videoPort != null) {
                i = videoPort.hashCode();
            } else {
                i = 0;
            }
            sb.append(i);
            sb.append(" from ");
            if (videoPort2 != null) {
                i2 = videoPort2.hashCode();
            } else {
                i2 = 0;
            }
            sb.append(i2);
            sb.append(", running: ");
            sb.append(this.isRunning);
            Log.i(sb.toString());
            if (!this.isRunning) {
                this.videoPort = videoPort;
                if (!(videoPort == null || (i3 = startOnCameraThread()) == 0)) {
                    stopOnCameraThread();
                    this.videoPort = videoPort2;
                }
            } else if (videoPort != null) {
                stopPreviewOnCameraThread(true);
                this.videoPort = videoPort;
                if (preparePreviewOnCameraThread() != 0) {
                    stopOnCameraThread();
                    this.videoPort = videoPort2;
                    return -7;
                }
                AbstractC1311461l r1 = this.cameraProcessor;
                Camera camera = this.camera;
                AnonymousClass009.A05(camera);
                if (r1 != null) {
                    camera.setPreviewCallback(r1);
                } else {
                    camera.setPreviewCallback(this);
                }
                int updatePreviewOrientationOnCameraThread = updatePreviewOrientationOnCameraThread();
                this.camera.startPreview();
                return updatePreviewOrientationOnCameraThread;
            } else {
                int stopOnCameraThread = stopOnCameraThread();
                this.videoPort = null;
                return stopOnCameraThread;
            }
        }
        return i3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x015d, code lost:
        if (r8.contains(r6) != false) goto L_0x015f;
     */
    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int startOnCameraThread() {
        /*
        // Method dump skipped, instructions count: 871
        */
        throw new UnsupportedOperationException("Method not decompiled: org.pjsip.PjCamera.startOnCameraThread():int");
    }

    public void startPreview(Camera camera) {
        camera.startPreview();
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public int stopOnCameraThread() {
        boolean z = this.isRunning;
        this.isRunning = false;
        stopPeriodicCameraCallbackCheck();
        if (this.camera == null) {
            return -6;
        }
        Log.i("voip/video/VoipCamera/stopOnCameraThread");
        stopPreviewOnCameraThread(z);
        this.camera.release();
        this.camera = null;
        return 0;
    }

    private void stopPreviewOnCameraThread(boolean z) {
        if (z && !this.receivedCameraError) {
            try {
                Camera camera = this.camera;
                AnonymousClass009.A05(camera);
                camera.setPreviewCallbackWithBuffer(null);
                this.camera.stopPreview();
            } catch (RuntimeException e) {
                Log.e("voip/video/VoipCamera/stopPreviewOnCameraThread exception while calling stopPreview", e);
            }
        }
        releaseTexture();
    }

    private int tryNextStartModeOnCameraThread() {
        this.isRunning = false;
        this.cameraStartMode.incrementAndGet();
        stopOnCameraThread();
        if (this.passiveMode || this.cameraStartMode.get() > 2) {
            return -8;
        }
        return startOnCameraThread();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0021, code lost:
        if (r1.getRotation() == 2) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updateAdjustedPreviewSizeOnCameraThread() {
        /*
            r5 = this;
            X.01d r0 = r5.systemServices
            android.view.WindowManager r0 = r0.A0O()
            android.view.Display r1 = r0.getDefaultDisplay()
            X.2NT r0 = r5.cameraInfo
            int r0 = r0.A04
            int r0 = r0 % 180
            r4 = 1
            r3 = 0
            if (r0 != 0) goto L_0x0015
            r3 = 1
        L_0x0015:
            int r0 = r1.getRotation()
            if (r0 == 0) goto L_0x0023
            int r2 = r1.getRotation()
            r1 = 2
            r0 = 0
            if (r2 != r1) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            if (r3 == r0) goto L_0x0027
            r4 = 0
        L_0x0027:
            X.2NT r0 = r5.cameraInfo
            if (r4 == 0) goto L_0x0037
            int r2 = r0.A05
            int r1 = r0.A02
        L_0x002f:
            android.graphics.Point r0 = new android.graphics.Point
            r0.<init>(r2, r1)
            r5.adjustedPreviewSize = r0
            return
        L_0x0037:
            int r2 = r0.A02
            int r1 = r0.A05
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.pjsip.PjCamera.updateAdjustedPreviewSizeOnCameraThread():void");
    }

    @Override // com.whatsapp.voipcalling.camera.VoipPhysicalCamera
    public void updatePreviewOrientation() {
        Log.i("voip/video/VoipCamera/updateCameraPreviewOrientation Enter");
        int intValue = ((Number) syncRunOnCameraThread(new Callable() { // from class: X.5Di
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return Integer.valueOf(PjCamera.this.updatePreviewOrientationOnCameraThread());
            }
        }, -100)).intValue();
        StringBuilder sb = new StringBuilder("voip/video/VoipCamera/updateCameraPreviewOrientation Exit with ");
        sb.append(intValue);
        Log.i(sb.toString());
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(10:6|(9:8|(2:10|(1:12)(1:19))(1:20)|15|(1:17)|18|27|21|24|25)|14|15|(0)|18|27|21|24|25) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        if (r1 != 3) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007e, code lost:
        com.whatsapp.util.Log.e(r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int updatePreviewOrientationOnCameraThread() {
        /*
            r7 = this;
            com.whatsapp.voipcalling.VideoPort r0 = r7.videoPort
            if (r0 == 0) goto L_0x0085
            boolean r0 = r7.isRunning
            if (r0 == 0) goto L_0x0085
            X.01d r0 = r7.systemServices
            android.view.WindowManager r0 = r0.A0O()
            android.view.Display r0 = r0.getDefaultDisplay()
            int r1 = r0.getRotation()
            r6 = 0
            if (r1 == 0) goto L_0x0024
            r0 = 1
            if (r1 == r0) goto L_0x0071
            r0 = 2
            if (r1 == r0) goto L_0x006e
            r0 = 3
            r5 = 270(0x10e, float:3.78E-43)
            if (r1 == r0) goto L_0x0025
        L_0x0024:
            r5 = 0
        L_0x0025:
            X.2NT r0 = r7.cameraInfo
            boolean r4 = r0.A06
            int r3 = r0.A04
            int r0 = r3 - r5
            int r0 = r0 + 360
            if (r4 == 0) goto L_0x0037
            int r0 = r3 + r5
            int r0 = r0 % 360
            int r0 = 360 - r0
        L_0x0037:
            int r2 = r0 % 360
            java.lang.String r0 = "voip/video/VoipCamera/updatePreviewOrientationOnCameraThread to "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r2)
            java.lang.String r0 = " degree. Camera #"
            r1.append(r0)
            int r0 = r7.camIdx
            r1.append(r0)
            java.lang.String r0 = ", facing front: "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = ", camera orientation: "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = ", activity rotation: "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.i(r0)
            goto L_0x0074
        L_0x006e:
            r5 = 180(0xb4, float:2.52E-43)
            goto L_0x0025
        L_0x0071:
            r5 = 90
            goto L_0x0025
        L_0x0074:
            android.hardware.Camera r0 = r7.camera     // Catch: Exception -> 0x007d
            X.AnonymousClass009.A05(r0)     // Catch: Exception -> 0x007d
            r0.setDisplayOrientation(r2)     // Catch: Exception -> 0x007d
            goto L_0x0081
        L_0x007d:
            r0 = move-exception
            com.whatsapp.util.Log.e(r0)
        L_0x0081:
            r7.updateAdjustedPreviewSizeOnCameraThread()
            return r6
        L_0x0085:
            r0 = -1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.pjsip.PjCamera.updatePreviewOrientationOnCameraThread():int");
    }
}
