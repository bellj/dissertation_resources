package org.spongycastle.jcajce.provider.symmetric;

import X.AbstractC114775Na;
import X.AbstractC116985Xr;
import X.AbstractC117005Xt;
import X.AbstractC94944cn;
import X.AnonymousClass1TD;
import X.AnonymousClass1TJ;
import X.AnonymousClass1TL;
import X.AnonymousClass1TY;
import X.AnonymousClass20L;
import X.AnonymousClass5EC;
import X.AnonymousClass5ED;
import X.AnonymousClass5HV;
import X.AnonymousClass5IY;
import X.C114645Mn;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C95114dA;
import X.EnumC87374Bg;
import java.io.IOException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.SecretKey;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/* loaded from: classes2.dex */
public class PBEPBKDF2 {
    public static final Map A00;

    /* loaded from: classes2.dex */
    public class Mappings extends AnonymousClass1TD {
        public static final String A00 = PBEPBKDF2.class.getName();
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withGOST3411 extends BasePBKDF2 {
        public PBKDF2withGOST3411() {
            super(5, "PBKDF2", 6);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA224 extends BasePBKDF2 {
        public PBKDF2withSHA224() {
            super(5, "PBKDF2", 7);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA256 extends BasePBKDF2 {
        public PBKDF2withSHA256() {
            super(5, "PBKDF2", 4);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA384 extends BasePBKDF2 {
        public PBKDF2withSHA384() {
            super(5, "PBKDF2", 8);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA3_224 extends BasePBKDF2 {
        public PBKDF2withSHA3_224() {
            super(5, "PBKDF2", 10);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA3_256 extends BasePBKDF2 {
        public PBKDF2withSHA3_256() {
            super(5, "PBKDF2", 11);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA3_384 extends BasePBKDF2 {
        public PBKDF2withSHA3_384() {
            super(5, "PBKDF2", 12);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA3_512 extends BasePBKDF2 {
        public PBKDF2withSHA3_512() {
            super(5, "PBKDF2", 13);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSHA512 extends BasePBKDF2 {
        public PBKDF2withSHA512() {
            super(5, "PBKDF2", 9);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withSM3 extends BasePBKDF2 {
        public PBKDF2withSM3() {
            super(5, "PBKDF2", 14);
        }
    }

    static {
        HashMap hashMap = new HashMap();
        A00 = hashMap;
        hashMap.put(AbstractC116985Xr.A0J, 6);
        hashMap.put(AnonymousClass1TJ.A1E, 1);
        hashMap.put(AnonymousClass1TJ.A1G, 4);
        hashMap.put(AnonymousClass1TJ.A1F, 7);
        hashMap.put(AnonymousClass1TJ.A1H, 8);
        hashMap.put(AnonymousClass1TJ.A1I, 9);
        hashMap.put(AnonymousClass1TY.A0g, 11);
        hashMap.put(AnonymousClass1TY.A0f, 10);
        hashMap.put(AnonymousClass1TY.A0h, 12);
        hashMap.put(AnonymousClass1TY.A0i, 13);
        hashMap.put(AbstractC117005Xt.A00, 14);
    }

    /* loaded from: classes3.dex */
    public class PBKDF2with8BIT extends BasePBKDF2 {
        public PBKDF2with8BIT() {
            super(1, "PBKDF2", 1);
        }
    }

    /* loaded from: classes3.dex */
    public class PBKDF2withUTF8 extends BasePBKDF2 {
        public PBKDF2withUTF8() {
            super(5, "PBKDF2", 1);
        }
    }

    /* loaded from: classes3.dex */
    public class AlgParams extends AnonymousClass5HV {
        public C114645Mn A00;

        @Override // java.security.AlgorithmParametersSpi
        public byte[] engineGetEncoded(String str) {
            if (AnonymousClass5HV.A00(str)) {
                return engineGetEncoded();
            }
            return null;
        }

        @Override // java.security.AlgorithmParametersSpi
        public void engineInit(AlgorithmParameterSpec algorithmParameterSpec) {
            if (algorithmParameterSpec instanceof PBEParameterSpec) {
                PBEParameterSpec pBEParameterSpec = (PBEParameterSpec) algorithmParameterSpec;
                this.A00 = new C114645Mn(pBEParameterSpec.getSalt(), pBEParameterSpec.getIterationCount());
                return;
            }
            throw new InvalidParameterSpecException("PBEParameterSpec required to initialise a PBKDF2 PBE parameters algorithm parameters object");
        }

        @Override // java.security.AlgorithmParametersSpi
        public String engineToString() {
            return "PBKDF2 Parameters";
        }

        @Override // java.security.AlgorithmParametersSpi
        public byte[] engineGetEncoded() {
            try {
                return this.A00.A02("DER");
            } catch (IOException e) {
                throw C12990iw.A0m(C12960it.A0d(e.toString(), C12960it.A0k("Oooops! ")));
            }
        }

        @Override // java.security.AlgorithmParametersSpi
        public void engineInit(byte[] bArr) {
            AnonymousClass1TL A03 = AnonymousClass1TL.A03(bArr);
            this.A00 = A03 != null ? new C114645Mn(AbstractC114775Na.A04((Object) A03)) : null;
        }

        @Override // java.security.AlgorithmParametersSpi
        public void engineInit(byte[] bArr, String str) {
            if (AnonymousClass5HV.A00(str)) {
                engineInit(bArr);
                return;
            }
            throw C12990iw.A0i("Unknown parameters format in PBKDF2 parameters object");
        }
    }

    /* loaded from: classes3.dex */
    public class BasePBKDF2 extends AnonymousClass5IY {
        public int A00;
        public int A01;

        public BasePBKDF2(int i, String str, int i2) {
            super(str, AnonymousClass1TJ.A0G);
            this.A01 = i;
            this.A00 = i2;
        }

        @Override // X.AnonymousClass5IY, javax.crypto.SecretKeyFactorySpi
        public SecretKey engineGenerateSecret(KeySpec keySpec) {
            if (keySpec instanceof PBEKeySpec) {
                PBEKeySpec pBEKeySpec = (PBEKeySpec) keySpec;
                if (pBEKeySpec.getSalt() == null) {
                    return new AnonymousClass5EC(this.A01 == 1 ? EnumC87374Bg.A01 : EnumC87374Bg.A02, pBEKeySpec.getPassword());
                } else if (pBEKeySpec.getIterationCount() <= 0) {
                    throw new InvalidKeySpecException(C12960it.A0f(C12960it.A0k("positive iteration count required: "), pBEKeySpec.getIterationCount()));
                } else if (pBEKeySpec.getKeyLength() <= 0) {
                    throw new InvalidKeySpecException(C12960it.A0f(C12960it.A0k("positive key length required: "), pBEKeySpec.getKeyLength()));
                } else if (pBEKeySpec.getPassword().length != 0) {
                    int i = this.A00;
                    int keyLength = pBEKeySpec.getKeyLength();
                    int i2 = this.A01;
                    AbstractC94944cn A01 = C95114dA.A01(i2, i);
                    byte[] A02 = C95114dA.A02(pBEKeySpec, i2);
                    byte[] salt = pBEKeySpec.getSalt();
                    int iterationCount = pBEKeySpec.getIterationCount();
                    A01.A01 = A02;
                    A01.A02 = salt;
                    A01.A00 = iterationCount;
                    AnonymousClass20L A022 = A01.A02(keyLength);
                    for (int i3 = 0; i3 != A02.length; i3++) {
                        A02[i3] = 0;
                    }
                    return new AnonymousClass5ED(super.A00, pBEKeySpec, super.A01, A022, i2, i, keyLength, -1);
                } else {
                    throw C12970iu.A0f("password empty");
                }
            } else {
                throw new InvalidKeySpecException("Invalid KeySpec");
            }
        }
    }
}
