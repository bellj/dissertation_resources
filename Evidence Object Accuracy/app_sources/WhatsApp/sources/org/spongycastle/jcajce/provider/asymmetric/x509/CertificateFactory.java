package org.spongycastle.jcajce.provider.asymmetric.x509;

import X.AbstractC114775Na;
import X.AnonymousClass1TJ;
import X.AnonymousClass1TK;
import X.AnonymousClass1TN;
import X.AnonymousClass1TO;
import X.AnonymousClass4F6;
import X.AnonymousClass5GT;
import X.AnonymousClass5MU;
import X.AnonymousClass5N4;
import X.AnonymousClass5NU;
import X.AnonymousClass5NV;
import X.AnonymousClass5ON;
import X.AnonymousClass5OP;
import X.AnonymousClass5S2;
import X.C113335Hc;
import X.C113405Hk;
import X.C114565Mf;
import X.C12960it;
import X.C93874at;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactorySpi;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes3.dex */
public class CertificateFactory extends CertificateFactorySpi {
    public static final C93874at A07 = new C93874at("CERTIFICATE");
    public static final C93874at A08 = new C93874at("CRL");
    public static final C93874at A09 = new C93874at("PKCS7");
    public int A00 = 0;
    public int A01 = 0;
    public InputStream A02 = null;
    public InputStream A03 = null;
    public AnonymousClass5NV A04 = null;
    public AnonymousClass5NV A05 = null;
    public final AnonymousClass5S2 A06 = new AnonymousClass5GT();

    @Override // java.security.cert.CertificateFactorySpi
    public CertPath engineGenerateCertPath(InputStream inputStream, String str) {
        return new C113335Hc(inputStream, str);
    }

    @Override // java.security.cert.CertificateFactorySpi
    public Iterator engineGetCertPathEncodings() {
        return C113335Hc.A00.iterator();
    }

    public final CRL A00() {
        AnonymousClass5NV r0 = this.A04;
        if (r0 == null) {
            return null;
        }
        int i = this.A00;
        AnonymousClass1TN[] r1 = r0.A01;
        if (i >= r1.length) {
            return null;
        }
        this.A00 = i + 1;
        AnonymousClass1TN r12 = r1[i];
        return new AnonymousClass5ON(r12 instanceof AnonymousClass5MU ? (AnonymousClass5MU) r12 : r12 != null ? new AnonymousClass5MU(AbstractC114775Na.A04(r12)) : null, this.A06);
    }

    public final CRL A01(AbstractC114775Na r4) {
        if (r4 == null) {
            return null;
        }
        if (r4.A0B() <= 1 || !(r4.A0D(0) instanceof AnonymousClass1TK) || !r4.A0D(0).equals(AnonymousClass1TJ.A2K)) {
            return new AnonymousClass5ON(new AnonymousClass5MU(AbstractC114775Na.A04((Object) r4)), this.A06);
        }
        AbstractC114775Na A05 = AbstractC114775Na.A05((AnonymousClass5NU) r4.A0D(1), true);
        this.A04 = (A05 != null ? new AnonymousClass5N4(AbstractC114775Na.A04((Object) A05)) : null).A02;
        return A00();
    }

    public final Certificate A02() {
        AnonymousClass1TN r1;
        AnonymousClass5NV r3 = this.A05;
        if (r3 == null) {
            return null;
        }
        do {
            int i = this.A01;
            AnonymousClass1TN[] r12 = r3.A01;
            if (i >= r12.length) {
                return null;
            }
            this.A01 = i + 1;
            r1 = r12[i];
        } while (!(r1 instanceof AbstractC114775Na));
        return new AnonymousClass5OP(C114565Mf.A00(r1), this.A06);
    }

    public final Certificate A03(AbstractC114775Na r4) {
        if (r4 == null) {
            return null;
        }
        if (r4.A0B() <= 1 || !(r4.A0D(0) instanceof AnonymousClass1TK) || !r4.A0D(0).equals(AnonymousClass1TJ.A2K)) {
            return new AnonymousClass5OP(C114565Mf.A00(r4), this.A06);
        }
        AbstractC114775Na A05 = AbstractC114775Na.A05((AnonymousClass5NU) r4.A0D(1), true);
        this.A05 = (A05 != null ? new AnonymousClass5N4(AbstractC114775Na.A04((Object) A05)) : null).A01;
        return A02();
    }

    @Override // java.security.cert.CertificateFactorySpi
    public CRL engineGenerateCRL(InputStream inputStream) {
        InputStream inputStream2 = this.A02;
        if (inputStream2 == null || inputStream2 != inputStream) {
            this.A02 = inputStream;
            this.A04 = null;
            this.A00 = 0;
        }
        try {
            AnonymousClass5NV r0 = this.A04;
            if (r0 == null) {
                if (!inputStream.markSupported()) {
                    inputStream = new ByteArrayInputStream(AnonymousClass4F6.A00(inputStream));
                }
                inputStream.mark(1);
                int read = inputStream.read();
                if (read == -1) {
                    return null;
                }
                inputStream.reset();
                return read != 48 ? A01(A08.A01(inputStream)) : A01(AbstractC114775Na.A04((Object) new AnonymousClass1TO(inputStream, true).A05()));
            } else if (this.A00 != r0.A01.length) {
                return A00();
            } else {
                this.A04 = null;
                this.A00 = 0;
                return null;
            }
        } catch (CRLException e) {
            throw e;
        } catch (Exception e2) {
            throw new CRLException(e2.toString());
        }
    }

    @Override // java.security.cert.CertificateFactorySpi
    public Collection engineGenerateCRLs(InputStream inputStream) {
        ArrayList A0l = C12960it.A0l();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        while (true) {
            CRL engineGenerateCRL = engineGenerateCRL(bufferedInputStream);
            if (engineGenerateCRL == null) {
                return A0l;
            }
            A0l.add(engineGenerateCRL);
        }
    }

    @Override // java.security.cert.CertificateFactorySpi
    public CertPath engineGenerateCertPath(InputStream inputStream) {
        return new C113335Hc(inputStream, "PkiPath");
    }

    @Override // java.security.cert.CertificateFactorySpi
    public CertPath engineGenerateCertPath(List list) {
        for (Object obj : list) {
            if (!(obj == null || (obj instanceof X509Certificate))) {
                throw new CertificateException(C12960it.A0d(obj.toString(), C12960it.A0k("list contains non X509Certificate object while creating CertPath\n")));
            }
        }
        return new C113335Hc(list);
    }

    @Override // java.security.cert.CertificateFactorySpi
    public Certificate engineGenerateCertificate(InputStream inputStream) {
        InputStream inputStream2 = this.A03;
        if (inputStream2 == null || inputStream2 != inputStream) {
            this.A03 = inputStream;
            this.A05 = null;
            this.A01 = 0;
        }
        try {
            AnonymousClass5NV r0 = this.A05;
            if (r0 == null) {
                if (!inputStream.markSupported()) {
                    inputStream = new ByteArrayInputStream(AnonymousClass4F6.A00(inputStream));
                }
                inputStream.mark(1);
                int read = inputStream.read();
                if (read == -1) {
                    return null;
                }
                inputStream.reset();
                return read != 48 ? A03(A07.A01(inputStream)) : A03(AbstractC114775Na.A04((Object) new AnonymousClass1TO(inputStream).A05()));
            } else if (this.A01 != r0.A01.length) {
                return A02();
            } else {
                this.A05 = null;
                this.A01 = 0;
                return null;
            }
        } catch (Exception e) {
            throw new C113405Hk(C12960it.A0d(e.getMessage(), C12960it.A0k("parsing issue: ")), e, this);
        }
    }

    @Override // java.security.cert.CertificateFactorySpi
    public Collection engineGenerateCertificates(InputStream inputStream) {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        ArrayList A0l = C12960it.A0l();
        while (true) {
            Certificate engineGenerateCertificate = engineGenerateCertificate(bufferedInputStream);
            if (engineGenerateCertificate == null) {
                return A0l;
            }
            A0l.add(engineGenerateCertificate);
        }
    }
}
