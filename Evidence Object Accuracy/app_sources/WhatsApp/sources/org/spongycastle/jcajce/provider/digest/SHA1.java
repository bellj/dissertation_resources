package org.spongycastle.jcajce.provider.digest;

import X.AnonymousClass1TC;
import X.AnonymousClass4X1;
import X.AnonymousClass5G4;
import X.AnonymousClass5HY;
import X.AnonymousClass5IW;
import X.AnonymousClass5IX;
import X.AnonymousClass5OB;
import X.AnonymousClass5PL;

/* loaded from: classes2.dex */
public class SHA1 {

    /* loaded from: classes3.dex */
    public class Digest extends AnonymousClass5HY implements Cloneable {
        public Digest() {
            super(new AnonymousClass5OB());
        }

        @Override // java.security.MessageDigest, java.security.MessageDigestSpi, java.lang.Object
        public Object clone() {
            AnonymousClass5HY r2 = (AnonymousClass5HY) super.clone();
            r2.A01 = new AnonymousClass5OB((AnonymousClass5OB) this.A01);
            return r2;
        }
    }

    /* loaded from: classes3.dex */
    public class HashMac extends AnonymousClass5IX {
        public HashMac() {
            super(new AnonymousClass5G4(new AnonymousClass5OB()));
        }
    }

    /* loaded from: classes3.dex */
    public class KeyGenerator extends AnonymousClass5IW {
        public KeyGenerator() {
            super("HMACSHA1", new AnonymousClass4X1(), 160);
        }
    }

    /* loaded from: classes2.dex */
    public class Mappings extends AnonymousClass1TC {
        public static final String A00 = SHA1.class.getName();
    }

    /* loaded from: classes3.dex */
    public class PBEWithMacKeyFactory extends AnonymousClass5PL {
        public PBEWithMacKeyFactory() {
            super("PBEwithHmacSHA", null, 2, 1, 160, 0, false);
        }
    }

    /* loaded from: classes3.dex */
    public class SHA1Mac extends AnonymousClass5IX {
        public SHA1Mac() {
            super(new AnonymousClass5G4(new AnonymousClass5OB()));
        }
    }
}
