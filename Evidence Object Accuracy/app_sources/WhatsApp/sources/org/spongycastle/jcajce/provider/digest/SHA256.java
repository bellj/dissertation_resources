package org.spongycastle.jcajce.provider.digest;

import X.AnonymousClass1TC;
import X.AnonymousClass4X1;
import X.AnonymousClass5G4;
import X.AnonymousClass5HY;
import X.AnonymousClass5IW;
import X.AnonymousClass5IX;
import X.AnonymousClass5OE;
import X.AnonymousClass5PL;

/* loaded from: classes2.dex */
public class SHA256 {

    /* loaded from: classes3.dex */
    public class Digest extends AnonymousClass5HY implements Cloneable {
        public Digest() {
            super(new AnonymousClass5OE());
        }

        @Override // java.security.MessageDigest, java.security.MessageDigestSpi, java.lang.Object
        public Object clone() {
            AnonymousClass5HY r2 = (AnonymousClass5HY) super.clone();
            r2.A01 = new AnonymousClass5OE((AnonymousClass5OE) this.A01);
            return r2;
        }
    }

    /* loaded from: classes3.dex */
    public class HashMac extends AnonymousClass5IX {
        public HashMac() {
            super(new AnonymousClass5G4(new AnonymousClass5OE()));
        }
    }

    /* loaded from: classes3.dex */
    public class KeyGenerator extends AnonymousClass5IW {
        public KeyGenerator() {
            super("HMACSHA256", new AnonymousClass4X1(), 256);
        }
    }

    /* loaded from: classes2.dex */
    public class Mappings extends AnonymousClass1TC {
        public static final String A00 = SHA256.class.getName();
    }

    /* loaded from: classes3.dex */
    public class PBEWithMacKeyFactory extends AnonymousClass5PL {
        public PBEWithMacKeyFactory() {
            super("PBEwithHmacSHA256", null, 2, 4, 256, 0, false);
        }
    }
}
