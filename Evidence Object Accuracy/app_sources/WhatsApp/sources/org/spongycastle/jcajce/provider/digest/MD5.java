package org.spongycastle.jcajce.provider.digest;

import X.AnonymousClass1TC;
import X.AnonymousClass4X1;
import X.AnonymousClass5G4;
import X.AnonymousClass5HY;
import X.AnonymousClass5IW;
import X.AnonymousClass5IX;
import X.AnonymousClass5OC;

/* loaded from: classes2.dex */
public class MD5 {

    /* loaded from: classes3.dex */
    public class Digest extends AnonymousClass5HY implements Cloneable {
        public Digest() {
            super(new AnonymousClass5OC());
        }

        @Override // java.security.MessageDigest, java.security.MessageDigestSpi, java.lang.Object
        public Object clone() {
            AnonymousClass5HY r2 = (AnonymousClass5HY) super.clone();
            r2.A01 = new AnonymousClass5OC((AnonymousClass5OC) this.A01);
            return r2;
        }
    }

    /* loaded from: classes3.dex */
    public class HashMac extends AnonymousClass5IX {
        public HashMac() {
            super(new AnonymousClass5G4(new AnonymousClass5OC()));
        }
    }

    /* loaded from: classes3.dex */
    public class KeyGenerator extends AnonymousClass5IW {
        public KeyGenerator() {
            super("HMACMD5", new AnonymousClass4X1(), 128);
        }
    }

    /* loaded from: classes2.dex */
    public class Mappings extends AnonymousClass1TC {
        public static final String A00 = MD5.class.getName();
    }
}
