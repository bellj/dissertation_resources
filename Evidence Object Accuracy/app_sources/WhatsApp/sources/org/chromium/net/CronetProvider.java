package org.chromium.net;

import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import android.content.Context;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.chromium.net.CronetEngine;

/* loaded from: classes2.dex */
public abstract class CronetProvider {
    public static final String GMS_CORE_CRONET_PROVIDER_CLASS = "com.google.android.gms.net.GmsCoreCronetProvider";
    public static final String JAVA_CRONET_PROVIDER_CLASS = "org.chromium.net.impl.JavaCronetProvider";
    public static final String NATIVE_CRONET_PROVIDER_CLASS = "org.chromium.net.impl.NativeCronetProvider";
    public static final String PLAY_SERVICES_CRONET_PROVIDER_CLASS = "com.google.android.gms.net.PlayServicesCronetProvider";
    public static final String PROVIDER_NAME_APP_PACKAGED = "App-Packaged-Cronet-Provider";
    public static final String PROVIDER_NAME_FALLBACK = "Fallback-Cronet-Provider";
    public static final String RES_KEY_CRONET_IMPL_CLASS = "CronetProviderClassName";
    public static final String TAG = "CronetProvider";
    public final Context mContext;

    public abstract CronetEngine.Builder createBuilder();

    public abstract String getName();

    public abstract String getVersion();

    public abstract boolean isEnabled();

    public CronetProvider(Context context) {
        if (context != null) {
            this.mContext = context;
            return;
        }
        throw C12970iu.A0f("Context must not be null");
    }

    public static boolean addCronetProviderFromResourceFile(Context context, Set set) {
        String string;
        int identifier = context.getResources().getIdentifier(RES_KEY_CRONET_IMPL_CLASS, "string", context.getPackageName());
        boolean z = false;
        if (identifier != 0 && (string = context.getResources().getString(identifier)) != null && !string.equals(PLAY_SERVICES_CRONET_PROVIDER_CLASS) && !string.equals(GMS_CORE_CRONET_PROVIDER_CLASS) && !string.equals(JAVA_CRONET_PROVIDER_CLASS) && !string.equals(NATIVE_CRONET_PROVIDER_CLASS)) {
            z = true;
            if (!addCronetProviderImplByClassName(context, string, set, true)) {
                String str = TAG;
                StringBuilder A0k = C12960it.A0k("Unable to instantiate Cronet implementation class ");
                A0k.append(string);
                A0k.append(" that is listed as in the app string resource file under ");
                A0k.append(RES_KEY_CRONET_IMPL_CLASS);
                Log.e(str, C12960it.A0d(" key", A0k));
            }
        }
        return z;
    }

    public static boolean addCronetProviderImplByClassName(Context context, String str, Set set, boolean z) {
        try {
            set.add(context.getClassLoader().loadClass(str).asSubclass(CronetProvider.class).getConstructor(Context.class).newInstance(context));
            return true;
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            logReflectiveOperationException(str, z, e);
            return false;
        }
    }

    public static List getAllProviders(Context context) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        addCronetProviderFromResourceFile(context, linkedHashSet);
        addCronetProviderImplByClassName(context, PLAY_SERVICES_CRONET_PROVIDER_CLASS, linkedHashSet, false);
        addCronetProviderImplByClassName(context, GMS_CORE_CRONET_PROVIDER_CLASS, linkedHashSet, false);
        addCronetProviderImplByClassName(context, NATIVE_CRONET_PROVIDER_CLASS, linkedHashSet, false);
        addCronetProviderImplByClassName(context, JAVA_CRONET_PROVIDER_CLASS, linkedHashSet, false);
        return Collections.unmodifiableList(C12980iv.A0x(linkedHashSet));
    }

    public static void logReflectiveOperationException(String str, boolean z, Exception exc) {
        String str2 = TAG;
        if (z) {
            Log.e(str2, C12960it.A0d(str, C12960it.A0k("Unable to load provider class: ")), exc);
        } else if (Log.isLoggable(str2, 3)) {
            StringBuilder A0k = C12960it.A0k("Tried to load ");
            A0k.append(str);
            A0k.append(" provider class but it wasn't");
            Log.d(str2, C12960it.A0d(" included in the app classpath", A0k));
        }
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[class=");
        A0k.append(C12980iv.A0s(this));
        A0k.append(", ");
        A0k.append("name=");
        A0k.append("Google-Play-Services-Cronet-Provider");
        A0k.append(", ");
        A0k.append("version=");
        A0k.append(getVersion());
        A0k.append(", ");
        A0k.append("enabled=");
        A0k.append(isEnabled());
        return C12960it.A0d("]", A0k);
    }
}
