package org.chromium.net;

/* loaded from: classes3.dex */
public abstract class CallbackException extends CronetException {
    public CallbackException(String str, Throwable th) {
        super(str, th);
    }
}
