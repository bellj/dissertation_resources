package org.chromium.net;

import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import android.content.Context;
import android.util.Log;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandlerFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public abstract class CronetEngine {
    public static final String TAG = "CronetEngine";

    public abstract URLStreamHandlerFactory createURLStreamHandlerFactory();

    public abstract byte[] getGlobalMetricsDeltas();

    public abstract String getVersionString();

    public abstract UrlRequest.Builder newUrlRequestBuilder(String str, UrlRequest.Callback callback, Executor executor);

    public abstract URLConnection openConnection(URL url);

    public abstract void shutdown();

    public abstract void startNetLogToFile(String str, boolean z);

    public abstract void stopNetLog();

    /* loaded from: classes2.dex */
    public class Builder {
        public static final int HTTP_CACHE_DISABLED = 0;
        public static final int HTTP_CACHE_DISK = 3;
        public static final int HTTP_CACHE_DISK_NO_HTTP = 2;
        public static final int HTTP_CACHE_IN_MEMORY = 1;
        public final ICronetEngineBuilder mBuilderDelegate;

        /* loaded from: classes3.dex */
        public abstract class LibraryLoader {
            public abstract void loadLibrary(String str);
        }

        @Deprecated
        public Builder enableSdch(boolean z) {
            return this;
        }

        public Builder(Context context) {
            this(createBuilderDelegate(context));
        }

        public Builder(ICronetEngineBuilder iCronetEngineBuilder) {
            this.mBuilderDelegate = iCronetEngineBuilder;
        }

        public Builder addPublicKeyPins(String str, Set set, boolean z, Date date) {
            throw C12980iv.A0n("addPublicKeyPins");
        }

        public Builder addQuicHint(String str, int i, int i2) {
            throw C12980iv.A0n("addQuicHint");
        }

        public CronetEngine build() {
            throw C12980iv.A0n("build");
        }

        public static int compareVersions(String str, String str2) {
            int length;
            if (str == null || str2 == null) {
                throw C12970iu.A0f("The input values cannot be null");
            }
            String[] split = str.split("\\.");
            String[] split2 = str2.split("\\.");
            int i = 0;
            while (true) {
                length = split.length;
                if (i >= length || i >= split2.length) {
                    break;
                }
                try {
                    int parseInt = Integer.parseInt(split[i]);
                    int parseInt2 = Integer.parseInt(split2[i]);
                    if (parseInt != parseInt2) {
                        return Integer.signum(parseInt - parseInt2);
                    }
                    i++;
                } catch (NumberFormatException e) {
                    StringBuilder A0k = C12960it.A0k("Unable to convert version segments into integers: ");
                    A0k.append(split[i]);
                    A0k.append(" & ");
                    throw new IllegalArgumentException(C12960it.A0d(split2[i], A0k), e);
                }
            }
            return Integer.signum(length - split2.length);
        }

        public static ICronetEngineBuilder createBuilderDelegate(Context context) {
            ArrayList A0x = C12980iv.A0x(CronetProvider.getAllProviders(context));
            getEnabledCronetProviders(context, A0x);
            CronetProvider cronetProvider = (CronetProvider) A0x.get(0);
            if (Log.isLoggable(CronetEngine.TAG, 3)) {
                String str = CronetEngine.TAG;
                Object[] A1b = C12970iu.A1b();
                A1b[0] = cronetProvider;
                Log.d(str, String.format("Using '%s' provider for creating CronetEngine.Builder.", A1b));
            }
            return cronetProvider.createBuilder().mBuilderDelegate;
        }

        public Builder enableBrotli(boolean z) {
            return this;
        }

        public Builder enableHttp2(boolean z) {
            throw C12980iv.A0n("enableHttp2");
        }

        public Builder enableHttpCache(int i, long j) {
            throw C12980iv.A0n("enableHttpCache");
        }

        public Builder enablePublicKeyPinningBypassForLocalTrustAnchors(boolean z) {
            throw C12980iv.A0n("enablePublicKeyPinningBypassForLocalTrustAnchors");
        }

        public Builder enableQuic(boolean z) {
            throw C12980iv.A0n("enableQuic");
        }

        public String getDefaultUserAgent() {
            throw C12980iv.A0n("getDefaultUserAgent");
        }

        public static List getEnabledCronetProviders(Context context, List list) {
            if (list.size() != 0) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    if (!((CronetProvider) it.next()).isEnabled()) {
                        it.remove();
                    }
                }
                if (list.size() != 0) {
                    Collections.sort(list, new Comparator() { // from class: org.chromium.net.CronetEngine.Builder.1
                        public int compare(CronetProvider cronetProvider, CronetProvider cronetProvider2) {
                            if (CronetProvider.PROVIDER_NAME_FALLBACK.equals("Google-Play-Services-Cronet-Provider")) {
                                return 1;
                            }
                            return -Builder.compareVersions(cronetProvider.getVersion(), cronetProvider2.getVersion());
                        }
                    });
                    return list;
                }
                throw C12990iw.A0m("All available Cronet providers are disabled. A provider should be enabled before it can be used.");
            }
            throw C12990iw.A0m("Unable to find any Cronet provider. Have you included all necessary jars?");
        }

        public Builder setLibraryLoader(LibraryLoader libraryLoader) {
            throw C12980iv.A0n("setLibraryLoader");
        }

        public Builder setStoragePath(String str) {
            throw C12980iv.A0n("setStoragePath");
        }

        public Builder setUserAgent(String str) {
            throw C12980iv.A0n("setUserAgent");
        }
    }
}
