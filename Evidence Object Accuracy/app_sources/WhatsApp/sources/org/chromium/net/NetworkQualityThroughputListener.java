package org.chromium.net;

import X.C12960it;
import java.util.concurrent.Executor;

/* loaded from: classes3.dex */
public abstract class NetworkQualityThroughputListener {
    public final Executor mExecutor;

    public abstract void onThroughputObservation(int i, long j, int i2);

    public NetworkQualityThroughputListener(Executor executor) {
        if (executor != null) {
            this.mExecutor = executor;
            return;
        }
        throw C12960it.A0U("Executor must not be null");
    }

    public Executor getExecutor() {
        return this.mExecutor;
    }
}
