package org.npci.commonlibrary;

import X.AbstractC136396Mj;
import X.AbstractC136546My;
import X.AnonymousClass00T;
import X.C117305Zk;
import X.C117315Zl;
import X.C117325Zm;
import X.C117665aN;
import X.C117795ag;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C130165yu;
import X.View$OnClickListenerC1318164f;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewSwitcher;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class ATMPinFragment extends Hilt_ATMPinFragment implements AbstractC136396Mj {
    public int A00 = 0;
    public ViewSwitcher A01 = null;
    public C130165yu A02;
    public boolean A03 = false;
    public final HashMap A04 = C12970iu.A11();

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.npci_fragment_atmpin);
    }

    @Override // org.npci.commonlibrary.NPCIFragment, X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        int i;
        String A0I;
        super.A17(bundle, view);
        A19();
        ViewGroup A0P = C12980iv.A0P(view, R.id.switcherLayout1);
        ViewGroup A0P2 = C12980iv.A0P(view, R.id.switcherLayout2);
        this.A01 = (ViewSwitcher) view.findViewById(R.id.view_switcher);
        if (((NPCIFragment) this).A07 != null) {
            boolean z = false;
            for (int i2 = 0; i2 < ((NPCIFragment) this).A07.length(); i2++) {
                try {
                    JSONObject jSONObject = ((NPCIFragment) this).A07.getJSONObject(i2);
                    String string = jSONObject.getString("subtype");
                    if (jSONObject.optInt("dLength") == 0) {
                        i = 6;
                    } else {
                        i = jSONObject.optInt("dLength");
                    }
                    if (string.equals("MPIN")) {
                        C117795ag A18 = A18(A0I(R.string.npci_set_mpin_title), i2, i);
                        C117795ag A182 = A18(A0I(R.string.npci_confirm_mpin_title), i2, i);
                        if (!z) {
                            A18.AA5();
                            z = true;
                        }
                        ArrayList A0l = C12960it.A0l();
                        A0l.add(A18);
                        A0l.add(A182);
                        C117665aN r1 = new C117665aN(A0B());
                        r1.A00(A0l, this);
                        r1.A02 = jSONObject;
                        ((NPCIFragment) this).A0B.add(r1);
                        A0P2.addView(r1);
                    } else {
                        if (string.equals("ATMPIN")) {
                            A0I = A0I(R.string.npci_atm_title);
                        } else if ("OTP".equals(string) || "SMS".equals(string) || "EMAIL".equals(string) || "HOTP".equals(string) || "TOTP".equals(string)) {
                            A0I = A0I(R.string.npci_otp_title);
                            ((NPCIFragment) this).A00 = i2;
                        } else {
                            A0I = "";
                        }
                        C117795ag A183 = A18(A0I, i2, i);
                        if (!z) {
                            A183.AA5();
                            z = true;
                        }
                        A183.A07 = jSONObject;
                        ((NPCIFragment) this).A0B.add(A183);
                        A0P.addView(A183);
                    }
                } catch (JSONException e) {
                    throw C117315Zl.A0J(e);
                }
            }
        }
        int i3 = ((NPCIFragment) this).A00;
        if (i3 != -1) {
            ArrayList arrayList = ((NPCIFragment) this).A0B;
            if (arrayList.get(i3) instanceof C117795ag) {
                C117795ag A0s = C117305Zk.A0s(arrayList, i3);
                A1B(A0s);
                A0s.A0C = true;
            }
        }
        ArrayList arrayList2 = ((NPCIFragment) this).A0B;
        int size = arrayList2.size();
        for (int i4 = 0; i4 < size; i4++) {
            if (i4 != ((NPCIFragment) this).A00) {
                AbstractC136546My A02 = C117325Zm.A02(arrayList2, i4);
                Drawable A04 = AnonymousClass00T.A04(A0B(), R.drawable.ic_visibility_on);
                Drawable A042 = AnonymousClass00T.A04(A0B(), R.drawable.ic_visibility_off);
                String A0I2 = A0I(R.string.npci_action_hide);
                String A0I3 = A0I(R.string.npci_action_show);
                A02.Aez(A04, new View$OnClickListenerC1318164f(A042, A04, A0I2, A0I3, this, A02), A0I3, 0, true, true);
            }
        }
    }

    @Override // X.AbstractC136396Mj
    public void AQl(int i) {
        if (!(((NPCIFragment) this).A0B.get(i) instanceof C117665aN)) {
            this.A00 = i;
        }
    }

    @Override // X.AbstractC136396Mj
    public void AQm(int i, String str) {
        int i2 = ((NPCIFragment) this).A00;
        if (i2 != -1 && i2 == i) {
            ArrayList arrayList = ((NPCIFragment) this).A0B;
            if (arrayList.get(i2) instanceof C117795ag) {
                Timer timer = ((NPCIFragment) this).A06;
                if (timer != null) {
                    timer.cancel();
                }
                C117795ag A00 = C117795ag.A00(arrayList, this);
                Drawable A04 = AnonymousClass00T.A04(A0B(), R.drawable.ic_tick_ok);
                if (A04 != null) {
                    A00.A03.setImageDrawable(A04);
                }
                A00.A01(A00.A03, true);
            }
        }
    }
}
