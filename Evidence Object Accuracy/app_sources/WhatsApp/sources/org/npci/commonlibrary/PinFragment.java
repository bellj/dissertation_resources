package org.npci.commonlibrary;

import X.AbstractC136396Mj;
import X.AnonymousClass00T;
import X.C117665aN;
import X.C117795ag;
import X.C12960it;
import X.C12970iu;
import X.C130165yu;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PinFragment extends Hilt_PinFragment implements AbstractC136396Mj {
    public int A00 = 0;
    public Boolean A01 = null;
    public C130165yu A02;
    public boolean A03 = false;
    public final HashMap A04 = C12970iu.A11();

    @Override // X.AnonymousClass01E
    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return C12960it.A0F(layoutInflater, viewGroup, R.layout.npci_fragment_pin);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008c, code lost:
        if (r0.booleanValue() != false) goto L_0x008e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0163  */
    @Override // org.npci.commonlibrary.NPCIFragment, X.AnonymousClass01E
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A17(android.os.Bundle r20, android.view.View r21) {
        /*
        // Method dump skipped, instructions count: 520
        */
        throw new UnsupportedOperationException("Method not decompiled: org.npci.commonlibrary.PinFragment.A17(android.os.Bundle, android.view.View):void");
    }

    @Override // X.AbstractC136396Mj
    public void AQl(int i) {
        if (!(((NPCIFragment) this).A0B.get(i) instanceof C117665aN)) {
            this.A00 = i;
        }
    }

    @Override // X.AbstractC136396Mj
    public void AQm(int i, String str) {
        int i2 = ((NPCIFragment) this).A00;
        if (i2 != -1 && i2 == i) {
            ArrayList arrayList = ((NPCIFragment) this).A0B;
            if (arrayList.get(i2) instanceof C117795ag) {
                C117795ag A00 = C117795ag.A00(arrayList, this);
                Drawable A04 = AnonymousClass00T.A04(A0B(), R.drawable.ic_tick_ok);
                if (A04 != null) {
                    A00.A03.setImageDrawable(A04);
                }
                A00.A01(A00.A03, true);
            }
        }
    }
}
