package org.npci.commonlibrary;

import X.AbstractActivityC117815ak;
import X.AnonymousClass018;
import X.C117315Zl;
import X.C117345Zo;
import X.C117375Zr;
import X.C128045vT;
import X.C12970iu;
import X.C130165yu;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.commonlibrary.GetCredential;

/* loaded from: classes4.dex */
public class GetCredential extends AbstractActivityC117815ak {
    public int A00;
    public TransitionDrawable A01;
    public View A02;
    public View A03;
    public ImageView A04;
    public AnonymousClass018 A05;
    public JSONArray A06 = null;
    public JSONArray A07 = C117315Zl.A0L();
    public JSONObject A08 = null;
    public JSONObject A09 = null;
    public C128045vT A0A;
    public C130165yu A0B;
    public C117375Zr A0C;
    public NPCIFragment A0D = null;
    public boolean A0E = false;
    public boolean A0F;
    public final Context A0G = this;

    public static final void A0A(View view, float f, float f2) {
        RotateAnimation rotateAnimation = new RotateAnimation(f, f2, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration((long) 300);
        rotateAnimation.setFillEnabled(true);
        rotateAnimation.setFillAfter(true);
        view.startAnimation(rotateAnimation);
    }

    public final void A1i(boolean z) {
        float f = 0.0f;
        ImageView imageView = this.A04;
        if (z) {
            A0A(imageView, 0.0f, 180.0f);
        } else {
            A0A(imageView, 180.0f, 0.0f);
        }
        int height = this.A02.getHeight();
        if (height == 0) {
            height = this.A00;
        }
        this.A02.clearAnimation();
        ViewPropertyAnimator animate = this.A02.animate();
        float f2 = 0.0f;
        if (!z) {
            f2 = ((float) height) * -1.0f;
        }
        ViewPropertyAnimator y = animate.y(f2);
        if (z) {
            f = 1.0f;
        }
        y.alpha(f).setDuration(300).setInterpolator(new AccelerateInterpolator()).setListener(new C117345Zo(this, height, z));
    }

    @Override // X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        if (this.A0E) {
            Intent A0A = C12970iu.A0A();
            int i = 252;
            if (this.A0F) {
                i = 251;
            }
            setResult(i, A0A);
            super.onBackPressed();
            return;
        }
        this.A0E = true;
        Toast.makeText(this, getString(R.string.npci_back_button_exit_message), 0).show();
        new Handler().postDelayed(new Runnable() { // from class: X.6Hb
            @Override // java.lang.Runnable
            public final void run() {
                GetCredential.this.A0E = false;
            }
        }, 2000);
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x0301  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x031d  */
    /* JADX WARNING: Removed duplicated region for block: B:128:? A[RETURN, SYNTHETIC] */
    @Override // X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r13) {
        /*
        // Method dump skipped, instructions count: 809
        */
        throw new UnsupportedOperationException("Method not decompiled: org.npci.commonlibrary.GetCredential.onCreate(android.os.Bundle):void");
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        try {
            C117375Zr r0 = this.A0C;
            if (r0 != null) {
                unregisterReceiver(r0);
                this.A0C = null;
            }
        } catch (Throwable unused) {
            Log.e("PAY: Failed to unregister SMS receiver (Ignoring)");
        }
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (checkCallingOrSelfPermission("android.permission.RECEIVE_SMS") == 0) {
            this.A0C = new C117375Zr(this);
            IntentFilter intentFilter = new IntentFilter();
            try {
                intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
                intentFilter.setPriority(Integer.MAX_VALUE);
                registerReceiver(this.A0C, intentFilter);
            } catch (Throwable unused) {
                Log.e("PAY: Failed to register SMS broadcast receiver (Ignoring)");
            }
        } else {
            Log.e("PAY: RECEIVE_SMS permission not provided by the App. This will affect Auto OTP detection feature of Common Library");
        }
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("has_error", this.A0F);
    }
}
