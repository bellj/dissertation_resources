package org.npci.commonlibrary.widget;

import X.AnonymousClass00T;
import X.AnonymousClass64E;
import X.AnonymousClass6MH;
import X.C117295Zj;
import X.C125355r7;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes4.dex */
public class Keypad extends TableLayout {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public AnonymousClass6MH A04;

    public Keypad(Context context) {
        this(context, null);
    }

    public Keypad(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A02 = 61;
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, C125355r7.A02, 0, 0);
        this.A03 = obtainStyledAttributes.getColor(3, AnonymousClass00T.A00(getContext(), R.color.npci_keypad_bg_color));
        this.A01 = obtainStyledAttributes.getColor(0, AnonymousClass00T.A00(getContext(), R.color.npci_key_digit_color));
        this.A00 = (float) obtainStyledAttributes.getDimensionPixelSize(2, 36);
        this.A02 = obtainStyledAttributes.getDimensionPixelSize(1, this.A02);
        obtainStyledAttributes.recycle();
        setBackgroundColor(this.A03);
        TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(-1, 0, 1.0f);
        int i = 0;
        int i2 = 1;
        while (true) {
            Context context2 = getContext();
            if (i < 3) {
                TableRow tableRow = new TableRow(context2);
                tableRow.setLayoutParams(layoutParams);
                tableRow.setWeightSum(3.0f);
                int i3 = 0;
                do {
                    TextView textView = new TextView(getContext());
                    textView.setGravity(17);
                    textView.setLayoutParams(getItemParams());
                    textView.setTextColor(this.A01);
                    textView.setTextSize(2, this.A00);
                    textView.setText(String.valueOf(i2));
                    textView.setClickable(true);
                    setClickFeedback(textView);
                    textView.setOnClickListener(new AnonymousClass64E(this, i2));
                    tableRow.addView(textView);
                    i2++;
                    i3++;
                } while (i3 < 3);
                addView(tableRow);
                i++;
            } else {
                ImageView imageView = new ImageView(context2);
                imageView.setImageResource(R.drawable.ic_action_backspace);
                imageView.setLayoutParams(getItemParams());
                imageView.setClickable(true);
                setClickFeedback(imageView);
                C117295Zj.A0n(imageView, this, 205);
                TextView textView2 = new TextView(getContext());
                textView2.setLayoutParams(getItemParams());
                textView2.setGravity(17);
                textView2.setText(String.valueOf(0));
                textView2.setTextColor(this.A01);
                textView2.setTextSize(2, this.A00);
                textView2.setClickable(true);
                setClickFeedback(textView2);
                C117295Zj.A0n(textView2, this, 206);
                ImageView imageView2 = new ImageView(getContext());
                imageView2.setImageResource(R.drawable.ic_action_submit);
                imageView2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                imageView2.setAdjustViewBounds(true);
                TableRow.LayoutParams itemParams = getItemParams();
                itemParams.height = (int) (C117295Zj.A01(this, (float) this.A02) * 1.25f);
                imageView2.setLayoutParams(itemParams);
                imageView2.setClickable(true);
                imageView2.setContentDescription(getContext().getString(R.string.cl_done));
                setClickFeedback(imageView2);
                C117295Zj.A0n(imageView2, this, 207);
                TableRow tableRow2 = new TableRow(getContext());
                tableRow2.setLayoutParams(layoutParams);
                tableRow2.setWeightSum(3.0f);
                tableRow2.addView(imageView);
                tableRow2.addView(textView2);
                tableRow2.addView(imageView2);
                addView(tableRow2);
                return;
            }
        }
    }

    private TableRow.LayoutParams getItemParams() {
        return new TableRow.LayoutParams(0, (int) (((float) this.A02) * ((float) (getResources().getDisplayMetrics().densityDpi / 160))), 1.0f);
    }

    private void setClickFeedback(View view) {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
        view.setBackgroundResource(typedValue.resourceId);
    }

    public void setOnKeyPressCallback(AnonymousClass6MH r1) {
        this.A04 = r1;
    }
}
