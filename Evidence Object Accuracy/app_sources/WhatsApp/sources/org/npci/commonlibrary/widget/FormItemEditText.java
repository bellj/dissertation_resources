package org.npci.commonlibrary.widget;

import X.AnonymousClass61u;
import X.C117295Zj;
import X.C117305Zk;
import X.C125355r7;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1312061r;
import X.C1312161s;
import X.View$OnLongClickListenerC1319064o;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import com.whatsapp.R;
import com.whatsapp.WaEditText;

/* loaded from: classes4.dex */
public class FormItemEditText extends WaEditText {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public int A06;
    public ColorStateList A07;
    public Paint A08;
    public Paint A09;
    public Paint A0A;
    public Paint A0B;
    public Drawable A0C;
    public View.OnClickListener A0D;
    public String A0E;
    public String A0F;
    public StringBuilder A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public float[] A0L;
    public float[] A0M;
    public float[] A0N;
    public RectF[] A0O;
    public final Rect A0P = new Rect();
    public final int[] A0Q;
    public final int[][] A0R;

    public FormItemEditText(Context context) {
        super(context);
        C117295Zj.A1Z(this);
        int[][] A1b = C117295Zj.A1b(this);
        this.A0R = A1b;
        int[] iArr = {-16711936, -65536, -16777216, -7829368};
        this.A0Q = iArr;
        C117305Zk.A1S(this, iArr, A1b);
    }

    public FormItemEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        C117295Zj.A1Z(this);
        int[][] A1b = C117295Zj.A1b(this);
        this.A0R = A1b;
        int[] iArr = {-16711936, -65536, -16777216, -7829368};
        this.A0Q = iArr;
        C117305Zk.A1S(this, iArr, A1b);
        A05(context, attributeSet);
    }

    public FormItemEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        C117295Zj.A1Z(this);
        int[][] A1b = C117295Zj.A1b(this);
        this.A0R = A1b;
        int[] iArr = {-16711936, -65536, -16777216, -7829368};
        this.A0Q = iArr;
        C117305Zk.A1S(this, iArr, A1b);
        A05(context, attributeSet);
    }

    /* JADX INFO: finally extract failed */
    public final void A05(Context context, AttributeSet attributeSet) {
        this.A01 = C117295Zj.A01(this, this.A01);
        this.A02 = C117295Zj.A01(this, this.A02);
        this.A04 = C117295Zj.A01(this, this.A04);
        this.A05 = C117295Zj.A01(this, this.A05);
        boolean z = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C125355r7.A00, 0, 0);
        try {
            TypedValue typedValue = new TypedValue();
            obtainStyledAttributes.getValue(0, typedValue);
            this.A06 = typedValue.data;
            this.A0E = obtainStyledAttributes.getString(3);
            this.A0F = obtainStyledAttributes.getString(11);
            this.A01 = obtainStyledAttributes.getDimension(8, this.A01);
            this.A02 = obtainStyledAttributes.getDimension(10, this.A02);
            this.A0K = obtainStyledAttributes.getBoolean(9, false);
            this.A00 = obtainStyledAttributes.getDimension(4, 0.0f);
            this.A04 = obtainStyledAttributes.getDimension(5, this.A04);
            this.A05 = obtainStyledAttributes.getDimension(12, this.A05);
            this.A0J = obtainStyledAttributes.getBoolean(2, this.A0J);
            this.A0C = obtainStyledAttributes.getDrawable(1);
            ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(7);
            if (colorStateList != null) {
                this.A07 = colorStateList;
            }
            obtainStyledAttributes.recycle();
            this.A08 = new Paint(getPaint());
            this.A09 = new Paint(getPaint());
            this.A0B = new Paint(getPaint());
            Paint paint = new Paint(getPaint());
            this.A0A = paint;
            paint.setStrokeWidth(this.A01);
            setFontSize(this.A00);
            TypedValue typedValue2 = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.colorControlActivated, typedValue2, true);
            int i = typedValue2.data;
            int[] iArr = this.A0Q;
            iArr[0] = i;
            iArr[1] = -7829368;
            iArr[2] = -7829368;
            setBackgroundResource(0);
            this.A03 = (float) attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "maxLength", 4);
            super.setOnClickListener(C117305Zk.A0A(this, 204));
            super.setOnLongClickListener(new View$OnLongClickListenerC1319064o(this));
            if (((getInputType() & 128) == 128 && TextUtils.isEmpty(this.A0E)) || ((getInputType() & 16) == 16 && TextUtils.isEmpty(this.A0E))) {
                this.A0E = "●";
            }
            if (!TextUtils.isEmpty(this.A0E)) {
                this.A0G = getMaskChars();
            }
            getPaint().getTextBounds("|", 0, 1, this.A0P);
            if (this.A06 > -1) {
                z = true;
            }
            this.A0H = z;
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    private CharSequence getFullText() {
        if (this.A0E == null) {
            return getText();
        }
        return getMaskChars();
    }

    private StringBuilder getMaskChars() {
        if (this.A0G == null) {
            this.A0G = C12960it.A0h();
        }
        int length = getText().length();
        while (true) {
            StringBuilder sb = this.A0G;
            if (sb.length() == length) {
                return sb;
            }
            if (sb.length() < length) {
                sb.append(this.A0E);
            } else {
                sb.deleteCharAt(sb.length() - 1);
            }
        }
    }

    @Override // com.whatsapp.WaEditText, android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        float f;
        int i;
        Paint paint;
        int[] iArr;
        int i2;
        int i3;
        float f2;
        float f3;
        Paint paint2;
        int[] iArr2;
        int i4;
        CharSequence fullText = getFullText();
        int length = fullText.length();
        float[] fArr = this.A0N;
        int length2 = fArr.length;
        if (length2 < length) {
            float[] fArr2 = new float[length + 0];
            System.arraycopy(fArr, 0, fArr2, 0, length2);
            fArr = fArr2;
        }
        this.A0N = fArr;
        getPaint().getTextWidths(fullText, 0, length, this.A0N);
        String str = this.A0F;
        float f4 = 0.0f;
        if (str != null) {
            float[] fArr3 = this.A0L;
            int length3 = str.length();
            int length4 = fArr3.length;
            if (length4 < length3) {
                float[] fArr4 = new float[length3 + 0];
                System.arraycopy(fArr3, 0, fArr4, 0, length4);
                fArr3 = fArr4;
            }
            this.A0L = fArr3;
            getPaint().getTextWidths(this.A0F, this.A0L);
            for (int i5 = 0; i5 < this.A0F.length(); i5++) {
                f4 += this.A0L[i5];
            }
        }
        for (int i6 = 0; ((float) i6) < this.A03; i6++) {
            Drawable drawable = this.A0C;
            boolean z = true;
            if (drawable != null) {
                boolean z2 = false;
                if (i6 < length) {
                    z2 = true;
                }
                boolean A1V = C12960it.A1V(i6, length);
                if (this.A0I) {
                    iArr2 = new int[1];
                    i4 = 16842914;
                } else {
                    boolean isFocused = isFocused();
                    drawable = this.A0C;
                    iArr2 = new int[1];
                    if (isFocused) {
                        iArr2[0] = 16842908;
                        drawable.setState(iArr2);
                        if (A1V) {
                            drawable = this.A0C;
                            iArr2 = new int[]{16842908, 16842913};
                        } else {
                            if (z2) {
                                drawable = this.A0C;
                                iArr2 = new int[]{16842908, 16842912};
                            }
                            Drawable drawable2 = this.A0C;
                            RectF rectF = this.A0O[i6];
                            drawable2.setBounds((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
                            this.A0C.draw(canvas);
                        }
                        drawable.setState(iArr2);
                        Drawable drawable2 = this.A0C;
                        RectF rectF = this.A0O[i6];
                        drawable2.setBounds((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
                        this.A0C.draw(canvas);
                    } else {
                        i4 = -16842908;
                    }
                }
                iArr2[0] = i4;
                drawable.setState(iArr2);
                Drawable drawable2 = this.A0C;
                RectF rectF = this.A0O[i6];
                drawable2.setBounds((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
                this.A0C.draw(canvas);
            }
            float f5 = this.A0O[i6].left + (this.A00 / 2.0f);
            if (length > i6) {
                if (!this.A0H || i6 != length - 1) {
                    i3 = i6 + 1;
                    f2 = f5 - (this.A0N[i6] / 2.0f);
                    f3 = this.A0M[i6];
                    paint2 = this.A08;
                } else {
                    i3 = i6 + 1;
                    f2 = f5 - (this.A0N[i6] / 2.0f);
                    f3 = this.A0M[i6];
                    paint2 = this.A09;
                }
                canvas.drawText(fullText, i6, i3, f2, f3, paint2);
            } else {
                String str2 = this.A0F;
                if (str2 != null) {
                    canvas.drawText(str2, f5 - (f4 / 2.0f), this.A0M[i6], this.A0B);
                }
            }
            if (this.A0C == null) {
                boolean z3 = false;
                if (i6 < length) {
                    z3 = true;
                }
                if (i6 != length) {
                    z = false;
                }
                if (this.A0I) {
                    paint = this.A0A;
                    iArr = new int[1];
                    i2 = 16842914;
                } else {
                    Paint paint3 = this.A0A;
                    if (isFocused()) {
                        f = this.A02;
                    } else {
                        f = this.A01;
                    }
                    paint3.setStrokeWidth(f);
                    if (z3) {
                        paint = this.A0A;
                        iArr = new int[1];
                        i2 = 16842913;
                    } else {
                        boolean isFocused2 = isFocused();
                        int[] iArr3 = new int[1];
                        if (z) {
                            i = -16842918;
                            if (isFocused2) {
                                i = 16842918;
                            }
                        } else {
                            i = -16842908;
                            if (isFocused2) {
                                i = 16842908;
                            }
                        }
                        iArr3[0] = i;
                        this.A0A.setColor(this.A07.getColorForState(iArr3, -7829368));
                        RectF rectF2 = this.A0O[i6];
                        canvas.drawLine(rectF2.left, rectF2.top, rectF2.right, rectF2.bottom, this.A0A);
                    }
                }
                iArr[0] = i2;
                paint.setColor(this.A07.getColorForState(iArr, -7829368));
                RectF rectF2 = this.A0O[i6];
                canvas.drawLine(rectF2.left, rectF2.top, rectF2.right, rectF2.bottom, this.A0A);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ed  */
    @Override // android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSizeChanged(int r11, int r12, int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 259
        */
        throw new UnsupportedOperationException("Method not decompiled: org.npci.commonlibrary.widget.FormItemEditText.onSizeChanged(int, int, int, int):void");
    }

    @Override // android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.A0I = false;
        RectF[] rectFArr = this.A0O;
        if (rectFArr != null && this.A0H) {
            int i4 = this.A06;
            if (i4 == -1) {
                invalidate();
            } else if (i3 <= i2) {
            } else {
                if (i4 == 0) {
                    this.A09.setAlpha(125);
                    ValueAnimator ofInt = ValueAnimator.ofInt(125, 255);
                    ofInt.setDuration(150L);
                    ofInt.addUpdateListener(new C1312061r(this));
                    AnimatorSet animatorSet = new AnimatorSet();
                    charSequence.length();
                    animatorSet.playTogether(ofInt);
                    animatorSet.start();
                    return;
                }
                float[] fArr = this.A0M;
                float f = rectFArr[i].bottom - this.A05;
                fArr[i] = f;
                ValueAnimator ofFloat = ValueAnimator.ofFloat(f + getPaint().getTextSize(), this.A0M[i]);
                ofFloat.setDuration(300L);
                ofFloat.setInterpolator(new OvershootInterpolator());
                ofFloat.addUpdateListener(new AnonymousClass61u(this, i));
                this.A09.setAlpha(255);
                ValueAnimator ofInt2 = ValueAnimator.ofInt(0, 255);
                ofInt2.setDuration(300L);
                ofInt2.addUpdateListener(new C1312161s(this));
                AnimatorSet animatorSet2 = new AnimatorSet();
                charSequence.length();
                animatorSet2.playTogether(ofFloat, ofInt2);
                animatorSet2.start();
            }
        }
    }

    @Override // android.view.View
    public boolean performClick() {
        return super.performClick();
    }

    public void setCharSize(float f) {
        this.A00 = f;
        invalidate();
    }

    public void setColorStates(ColorStateList colorStateList) {
        this.A07 = colorStateList;
        invalidate();
    }

    @Override // X.AnonymousClass011, android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        throw C12990iw.A0m("setCustomSelectionActionModeCallback() not supported.");
    }

    private void setError(boolean z) {
        this.A0I = z;
    }

    public void setFontSize(float f) {
        this.A08.setTextSize(f);
        this.A09.setTextSize(f);
        this.A0B.setTextSize(f);
    }

    public void setLineStroke(float f) {
        this.A01 = f;
        invalidate();
    }

    public void setLineStrokeCentered(boolean z) {
        this.A0K = z;
        invalidate();
    }

    public void setLineStrokeSelected(float f) {
        this.A02 = f;
        invalidate();
    }

    public void setMargin(int[] iArr) {
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(this);
        A0H.setMargins(iArr[0], iArr[1], iArr[2], iArr[3]);
        setLayoutParams(A0H);
    }

    public void setMaxLength(int i) {
        this.A03 = (float) i;
        setFilters(new InputFilter[]{new InputFilter.LengthFilter(i)});
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.A0D = onClickListener;
    }

    public void setSpace(float f) {
        this.A04 = f;
        invalidate();
    }
}
