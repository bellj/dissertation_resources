package org.npci.commonlibrary;

import X.AbstractC136396Mj;
import X.ActivityC000900k;
import X.AnonymousClass00T;
import X.AnonymousClass01E;
import X.AnonymousClass0QQ;
import X.AnonymousClass6L9;
import X.C117295Zj;
import X.C117795ag;
import X.C12960it;
import X.C12970iu;
import X.C13000ix;
import X.RunnableC135086Hc;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.commonlibrary.widget.FormItemEditText;

/* loaded from: classes4.dex */
public abstract class NPCIFragment extends AnonymousClass01E implements AbstractC136396Mj {
    public int A00 = -1;
    public Context A01;
    public Handler A02;
    public PopupWindow A03;
    public Runnable A04;
    public Timer A05;
    public Timer A06 = null;
    public JSONArray A07 = null;
    public JSONObject A08 = null;
    public JSONObject A09 = null;
    public boolean A0A = false;
    public final ArrayList A0B = C12960it.A0l();

    private int A00(float f) {
        return (int) (f * ((float) (A02().getDisplayMetrics().densityDpi / 160)));
    }

    @Override // X.AnonymousClass01E
    public void A11() {
        Runnable runnable;
        super.A11();
        Timer timer = this.A06;
        if (timer != null) {
            timer.cancel();
        }
        Timer timer2 = this.A05;
        if (timer2 != null) {
            timer2.cancel();
        }
        Handler handler = this.A02;
        if (!(handler == null || (runnable = this.A04) == null)) {
            handler.removeCallbacks(runnable);
        }
        PopupWindow popupWindow = this.A03;
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    @Override // X.AnonymousClass01E
    public void A15(Context context) {
        super.A15(context);
        this.A01 = context;
    }

    @Override // X.AnonymousClass01E
    public void A17(Bundle bundle, View view) {
        ActivityC000900k A0B = A0B();
        if (A0B instanceof GetCredential) {
            ((GetCredential) A0B).A0D = this;
        }
    }

    public C117795ag A18(String str, int i, int i2) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        C117795ag r3 = new C117795ag(A0B());
        if (this.A07.length() == 1) {
            r3.setActionBarPositionTop(true);
            layoutParams.width = A00(300.0f);
            layoutParams.topMargin = A00(40.0f);
            FormItemEditText formItemEditText = r3.A0A;
            formItemEditText.setCharSize(0.0f);
            formItemEditText.setSpace((float) A00(15.0f));
            formItemEditText.setFontSize((float) A00(22.0f));
            formItemEditText.setPadding(0, A00(32.0f), 0, 0);
            formItemEditText.setMargin(new int[]{0, A00(32.0f), 0, 0});
            formItemEditText.setLineStrokeCentered(true);
            formItemEditText.setLineStrokeSelected((float) A00(2.0f));
            formItemEditText.setColorStates(AnonymousClass00T.A03(A0B(), R.color.form_item_input_colors_transparent));
        }
        r3.setLayoutParams(layoutParams);
        r3.setInputLength(i2);
        r3.A0B = this;
        r3.setTitle(str);
        r3.A01 = i;
        return r3;
    }

    public void A19() {
        String string;
        Bundle bundle = super.A05;
        if (bundle != null) {
            try {
                String string2 = bundle.getString("configuration");
                if (string2 != null) {
                    this.A08 = C13000ix.A05(string2);
                }
                String string3 = bundle.getString("controls");
                if (!(string3 == null || (string = C13000ix.A05(string3).getString("CredAllowed")) == null)) {
                    this.A07 = new JSONArray(string);
                    ArrayList A0l = C12960it.A0l();
                    JSONObject jSONObject = null;
                    JSONObject jSONObject2 = null;
                    JSONObject jSONObject3 = null;
                    JSONObject jSONObject4 = null;
                    for (int i = 0; i < this.A07.length(); i++) {
                        try {
                            String optString = ((JSONObject) this.A07.get(i)).optString("subtype", "");
                            if (optString.equals("ATMPIN")) {
                                jSONObject = this.A07.getJSONObject(i);
                            }
                            if (optString.matches("OTP|SMS|HOTP|TOTP")) {
                                jSONObject2 = this.A07.getJSONObject(i);
                            }
                            if (optString.equals("MPIN")) {
                                jSONObject3 = this.A07.getJSONObject(i);
                            }
                            if (optString.equals("NMPIN")) {
                                jSONObject4 = this.A07.getJSONObject(i);
                            }
                        } catch (JSONException unused) {
                            Log.e("PAY: sortCredAllowedString failed");
                        }
                    }
                    if (!(this.A07.length() != 3 || jSONObject == null || jSONObject2 == null || jSONObject3 == null)) {
                        A0l.add(jSONObject2);
                        A0l.add(jSONObject);
                        A0l.add(jSONObject3);
                    }
                    if (!(this.A07.length() != 2 || jSONObject2 == null || jSONObject3 == null)) {
                        A0l.add(jSONObject2);
                        A0l.add(jSONObject3);
                    }
                    if (!(this.A07.length() != 2 || jSONObject3 == null || jSONObject4 == null)) {
                        A0l.add(jSONObject3);
                        A0l.add(jSONObject4);
                    }
                    if (A0l.size() > 0) {
                        this.A07 = new JSONArray((Collection) A0l);
                    }
                }
                String string4 = bundle.getString("salt");
                if (string4 != null) {
                    this.A09 = C13000ix.A05(string4);
                }
                String string5 = bundle.getString("payInfo");
                if (string5 != null) {
                    new JSONArray(string5);
                }
            } catch (JSONException e) {
                Log.e("PAY: Error while reading Arguments", e);
            }
        }
    }

    public void A1A(View view, String str) {
        PopupWindow popupWindow = this.A03;
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
        View inflate = A0B().getLayoutInflater().inflate(R.layout.npci_layout_popup_view, (ViewGroup) null);
        C12960it.A0J(inflate, R.id.popup_text).setText(str);
        PopupWindow popupWindow2 = new PopupWindow(inflate, -2, A00(60.0f));
        this.A03 = popupWindow2;
        popupWindow2.setAnimationStyle(R.style.PopupAnimation);
        this.A03.showAtLocation(view, 17, 0, 0);
        C117295Zj.A0n(inflate.findViewById(R.id.popup_button), this, 203);
        this.A05 = new Timer();
        Handler A0E = C12970iu.A0E();
        this.A02 = A0E;
        RunnableC135086Hc r2 = new RunnableC135086Hc(this);
        this.A04 = r2;
        A0E.postDelayed(r2, 3000);
    }

    public void A1B(C117795ag r11) {
        Timer timer = new Timer();
        this.A06 = timer;
        timer.schedule(new AnonymousClass6L9(this), 45000);
        r11.Aez(null, null, "", 0, false, false);
        r11.A01(r11.A03, false);
        String A0I = A0I(R.string.npci_detecting_otp);
        if (!TextUtils.isEmpty(A0I)) {
            r11.A02.setText(A0I);
        }
        Button button = r11.A02;
        r11.A01(button, true);
        button.setEnabled(false);
        button.setOnClickListener(null);
        AnonymousClass0QQ A01 = r11.A01(r11.A05, true);
        A01.A08(new AccelerateDecelerateInterpolator());
        A01.A01();
    }
}
