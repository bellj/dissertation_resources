package org.whispersystems.jobqueue;

import java.io.Serializable;
import java.util.List;

/* loaded from: classes2.dex */
public class JobParameters implements Serializable {
    public final String groupId;
    public final boolean isPersistent;
    public final List requirements;
    public final int retryCount = 100;
    public final boolean wakeLock = false;
    public final long wakeLockTimeout = 0;

    public /* synthetic */ JobParameters(String str, List list, boolean z) {
        this.requirements = list;
        this.isPersistent = z;
        this.groupId = str;
    }
}
