package org.whispersystems.jobqueue;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* loaded from: classes2.dex */
public final class JobConsumer$JobResult extends Enum {
    public static final JobConsumer$JobResult A00 = new JobConsumer$JobResult("DEFERRED", 2);
    public static final JobConsumer$JobResult A01 = new JobConsumer$JobResult("FAILURE", 1);
    public static final JobConsumer$JobResult A02 = new JobConsumer$JobResult("SUCCESS", 0);

    public JobConsumer$JobResult(String str, int i) {
    }
}
