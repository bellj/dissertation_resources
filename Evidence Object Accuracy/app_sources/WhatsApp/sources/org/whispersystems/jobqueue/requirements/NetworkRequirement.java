package org.whispersystems.jobqueue.requirements;

import X.AnonymousClass1LJ;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/* loaded from: classes3.dex */
public class NetworkRequirement implements Requirement, AnonymousClass1LJ {
    public transient Context A00;

    @Override // org.whispersystems.jobqueue.requirements.Requirement
    public boolean AJu() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.A00.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        this.A00 = context;
    }
}
