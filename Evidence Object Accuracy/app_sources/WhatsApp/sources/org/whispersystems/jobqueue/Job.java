package org.whispersystems.jobqueue;

import X.AbstractC14640lm;
import X.AnonymousClass009;
import X.AnonymousClass1E7;
import X.AnonymousClass1G8;
import X.AnonymousClass1IS;
import X.AnonymousClass1LI;
import X.C15380n4;
import X.C27081Fy;
import X.C47792Cr;
import X.C47802Cs;
import android.os.PowerManager;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.BulkGetPreKeyJob;
import com.whatsapp.jobqueue.job.DeleteAccountFromHsmServerJob;
import com.whatsapp.jobqueue.job.GenerateTcTokenJob;
import com.whatsapp.jobqueue.job.GetHsmMessagePackJob;
import com.whatsapp.jobqueue.job.GetStatusPrivacyJob;
import com.whatsapp.jobqueue.job.GetVNameCertificateJob;
import com.whatsapp.jobqueue.job.ReceiptMultiTargetProcessingJob;
import com.whatsapp.jobqueue.job.ReceiptProcessingJob;
import com.whatsapp.jobqueue.job.RehydrateHsmJob;
import com.whatsapp.jobqueue.job.RehydrateTemplateJob;
import com.whatsapp.jobqueue.job.RotateSignedPreKeyJob;
import com.whatsapp.jobqueue.job.SendDeleteHistorySyncMmsJob;
import com.whatsapp.jobqueue.job.SendDisableLiveLocationJob;
import com.whatsapp.jobqueue.job.SendE2EMessageJob;
import com.whatsapp.jobqueue.job.SendFinalLiveLocationNotificationJob;
import com.whatsapp.jobqueue.job.SendFinalLiveLocationRetryJob;
import com.whatsapp.jobqueue.job.SendLiveLocationKeyJob;
import com.whatsapp.jobqueue.job.SendMediaErrorReceiptJob;
import com.whatsapp.jobqueue.job.SendOrderStatusUpdateFailureReceiptJob;
import com.whatsapp.jobqueue.job.SendPaymentInviteSetupJob;
import com.whatsapp.jobqueue.job.SendPeerMessageJob;
import com.whatsapp.jobqueue.job.SendPermanentFailureReceiptJob;
import com.whatsapp.jobqueue.job.SendPlayedReceiptJob;
import com.whatsapp.jobqueue.job.SendPlayedReceiptJobV2;
import com.whatsapp.jobqueue.job.SendReadReceiptJob;
import com.whatsapp.jobqueue.job.SendRetryReceiptJob;
import com.whatsapp.jobqueue.job.SendStatusPrivacyListJob;
import com.whatsapp.jobqueue.job.SendWebForwardJob;
import com.whatsapp.jobqueue.job.SyncDeviceAndResendMessageJob;
import com.whatsapp.jobqueue.job.SyncDeviceForAdvValidationJob;
import com.whatsapp.jobqueue.job.SyncDevicesAndSendInvisibleMessageJob;
import com.whatsapp.jobqueue.job.SyncDevicesJob;
import com.whatsapp.jobqueue.job.SyncProfilePictureJob;
import com.whatsapp.jobqueue.job.SyncdDeleteAllDataForNonMdUserJob;
import com.whatsapp.jobqueue.job.SyncdTableEmptyKeyCheckJob;
import com.whatsapp.jobqueue.job.messagejob.AsyncMessageJob;
import com.whatsapp.jobqueue.job.messagejob.ProcessVCardMessageJob;
import com.whatsapp.util.Log;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.whispersystems.jobqueue.requirements.Requirement;

/* loaded from: classes2.dex */
public abstract class Job implements Serializable {
    public transient int A00;
    public transient long A01;
    public transient PowerManager.WakeLock A02;
    public final JobParameters parameters;

    public Job(JobParameters jobParameters) {
        this.parameters = jobParameters;
    }

    public void A00() {
        String str;
        StringBuilder sb;
        String str2;
        StringBuilder sb2;
        String str3;
        StringBuilder sb3;
        String A04;
        StringBuilder sb4;
        String str4;
        long j;
        String str5;
        if (!(this instanceof AnonymousClass1LI)) {
            if (this instanceof AsyncMessageJob) {
                AsyncMessageJob asyncMessageJob = (AsyncMessageJob) this;
                sb = new StringBuilder("asyncMessageJob/canceled async message job");
                sb2 = new StringBuilder("; rowId=");
                sb2.append(asyncMessageJob.rowId);
                sb2.append("; job=");
                if (!(asyncMessageJob instanceof ProcessVCardMessageJob)) {
                    str3 = "asyncTokenize";
                } else {
                    str3 = "processVCard";
                }
                sb2.append(str3);
            } else if (this instanceof SyncdTableEmptyKeyCheckJob) {
                Log.i("SyncdTableEmptyKeyCheckJob/onCanceled");
                ((SyncdTableEmptyKeyCheckJob) this).A00.A03(7);
                return;
            } else if (this instanceof SyncdDeleteAllDataForNonMdUserJob) {
                Log.i("SyncdDeleteAllDataForNonMdUserJob/onCanceled");
                ((SyncdDeleteAllDataForNonMdUserJob) this).A01.A08(true);
                return;
            } else if (this instanceof SyncProfilePictureJob) {
                SyncProfilePictureJob syncProfilePictureJob = (SyncProfilePictureJob) this;
                StringBuilder sb5 = new StringBuilder("SyncProfilePictureJob/onCanceled/cancel sync picture job param=");
                sb5.append(syncProfilePictureJob.A04());
                Log.w(sb5.toString());
                AnonymousClass009.A09("jid list is empty", C15380n4.A08(syncProfilePictureJob.jids));
                return;
            } else if (this instanceof SyncDevicesJob) {
                SyncDevicesJob syncDevicesJob = (SyncDevicesJob) this;
                StringBuilder sb6 = new StringBuilder("SyncDevicesJob/onCanceled/cancel sync devices job param=");
                sb6.append(syncDevicesJob.A04());
                Log.w(sb6.toString());
                syncDevicesJob.A01.A00(syncDevicesJob.jids);
                return;
            } else if (this instanceof SyncDevicesAndSendInvisibleMessageJob) {
                SyncDevicesAndSendInvisibleMessageJob syncDevicesAndSendInvisibleMessageJob = (SyncDevicesAndSendInvisibleMessageJob) this;
                StringBuilder sb7 = new StringBuilder("SyncDeviceAndResendMessageJob/onCanceled/param=");
                sb7.append(syncDevicesAndSendInvisibleMessageJob.A04());
                Log.w(sb7.toString());
                AnonymousClass1E7 r1 = syncDevicesAndSendInvisibleMessageJob.A01;
                AnonymousClass1IS r0 = syncDevicesAndSendInvisibleMessageJob.A04;
                Set set = r1.A02;
                synchronized (set) {
                    set.remove(r0);
                }
                return;
            } else if (this instanceof SyncDeviceForAdvValidationJob) {
                return;
            } else {
                if (this instanceof SyncDeviceAndResendMessageJob) {
                    SyncDeviceAndResendMessageJob syncDeviceAndResendMessageJob = (SyncDeviceAndResendMessageJob) this;
                    StringBuilder sb8 = new StringBuilder("SyncDeviceAndResendMessageJob/onCanceled/param=");
                    sb8.append(syncDeviceAndResendMessageJob.A04());
                    Log.w(sb8.toString());
                    AnonymousClass1E7 r12 = syncDeviceAndResendMessageJob.A04;
                    AnonymousClass1IS r02 = syncDeviceAndResendMessageJob.A0C;
                    Set set2 = r12.A02;
                    synchronized (set2) {
                        set2.remove(r02);
                    }
                    return;
                } else if (this instanceof SendWebForwardJob) {
                    return;
                } else {
                    if (!(this instanceof SendWebForwardJob.AckWebForwardJob)) {
                        if (this instanceof SendStatusPrivacyListJob) {
                            sb3 = new StringBuilder("canceled send status privacy job");
                            A04 = ((SendStatusPrivacyListJob) this).A04();
                        } else if (this instanceof SendRetryReceiptJob) {
                            sb3 = new StringBuilder("canceled sent read receipts job");
                            A04 = ((SendRetryReceiptJob) this).A04();
                        } else if (this instanceof SendReadReceiptJob) {
                            sb3 = new StringBuilder("canceled sent read receipts job");
                            A04 = ((SendReadReceiptJob) this).A04();
                        } else if (this instanceof SendPlayedReceiptJobV2) {
                            sb3 = new StringBuilder("SendPlayedReceiptJobV2/onCanceled; ");
                            A04 = ((SendPlayedReceiptJobV2) this).A04();
                        } else if (!(this instanceof SendPlayedReceiptJob)) {
                            if (this instanceof SendPermanentFailureReceiptJob) {
                                SendPermanentFailureReceiptJob sendPermanentFailureReceiptJob = (SendPermanentFailureReceiptJob) this;
                                sb = new StringBuilder("canceled send permananent-failure receipt job");
                                sb4 = new StringBuilder("; jid=");
                                sb4.append(sendPermanentFailureReceiptJob.jid);
                                sb4.append("; participant=");
                                sb4.append(sendPermanentFailureReceiptJob.participant);
                                sb4.append("; id=");
                                str4 = sendPermanentFailureReceiptJob.messageKeyId;
                            } else if (this instanceof SendPeerMessageJob) {
                                sb3 = new StringBuilder("SendPeerMessageJob/onCanceled/cancel send job");
                                A04 = ((SendPeerMessageJob) this).A04();
                            } else if (this instanceof SendPaymentInviteSetupJob) {
                                sb3 = new StringBuilder("canceled SendPaymentInviteSetupJob job");
                                A04 = ((SendPaymentInviteSetupJob) this).A04();
                            } else if (this instanceof SendOrderStatusUpdateFailureReceiptJob) {
                                SendOrderStatusUpdateFailureReceiptJob sendOrderStatusUpdateFailureReceiptJob = (SendOrderStatusUpdateFailureReceiptJob) this;
                                sb = new StringBuilder("canceled send order-status-update-failure receipt job");
                                sb4 = new StringBuilder("; jid=");
                                sb4.append(sendOrderStatusUpdateFailureReceiptJob.jid);
                                sb4.append("; id=");
                                str4 = sendOrderStatusUpdateFailureReceiptJob.messageKeyId;
                            } else if (this instanceof SendMediaErrorReceiptJob) {
                                sb3 = new StringBuilder("SendMediaErrorReceiptJob/canceled send played receipts job id=");
                                A04 = ((SendMediaErrorReceiptJob) this).messageId;
                            } else if (this instanceof SendLiveLocationKeyJob) {
                                sb3 = new StringBuilder("canceled send live location key job");
                                A04 = ((SendLiveLocationKeyJob) this).A04();
                            } else if (this instanceof SendFinalLiveLocationRetryJob) {
                                sb3 = new StringBuilder("canceled send final live location retry job");
                                A04 = ((SendFinalLiveLocationRetryJob) this).A04();
                            } else if (this instanceof SendFinalLiveLocationNotificationJob) {
                                sb3 = new StringBuilder("canceled send final live location job");
                                A04 = ((SendFinalLiveLocationNotificationJob) this).A04();
                            } else if (this instanceof SendE2EMessageJob) {
                                SendE2EMessageJob sendE2EMessageJob = (SendE2EMessageJob) this;
                                StringBuilder sb9 = new StringBuilder("sende2emessagejob/e2e send job canceled");
                                sb9.append(sendE2EMessageJob.A04());
                                Log.w(sb9.toString());
                                SendE2EMessageJob.A0j.remove(new C47792Cr(sendE2EMessageJob.jid, sendE2EMessageJob.id, sendE2EMessageJob.participant, sendE2EMessageJob.editVersion));
                                C27081Fy r2 = sendE2EMessageJob.A0X;
                                if ((r2.A01 & 256) == 256) {
                                    C47802Cs r03 = r2.A0M;
                                    if (r03 == null) {
                                        r03 = C47802Cs.A04;
                                    }
                                    AnonymousClass1G8 r04 = r03.A03;
                                    if (r04 == null) {
                                        r04 = AnonymousClass1G8.A05;
                                    }
                                    AbstractC14640lm A01 = AbstractC14640lm.A01(r04.A03);
                                    if (A01 != null) {
                                        sendE2EMessageJob.A0U.A00(sendE2EMessageJob.A0P, new AnonymousClass1IS(A01, sendE2EMessageJob.id, true));
                                        sendE2EMessageJob.A06.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(sendE2EMessageJob, 27, A01));
                                        return;
                                    }
                                    return;
                                }
                                return;
                            } else if (this instanceof SendDisableLiveLocationJob) {
                                sb3 = new StringBuilder("canceled disable live location job");
                                A04 = ((SendDisableLiveLocationJob) this).A04();
                            } else if (this instanceof SendDeleteHistorySyncMmsJob) {
                                sb3 = new StringBuilder("SendDeleteHistorySyncMmsJob/ cancelled chunkId=");
                                A04 = ((SendDeleteHistorySyncMmsJob) this).chunkId;
                            } else if (this instanceof RotateSignedPreKeyJob) {
                                sb3 = new StringBuilder("canceled rotate signed pre key job");
                                A04 = ((RotateSignedPreKeyJob) this).A04();
                            } else if (this instanceof RehydrateTemplateJob) {
                                sb3 = new StringBuilder("RehydrateTemplateJob/onCanceled/error canceled rehydrate hsm job, loggableParam=");
                                A04 = ((RehydrateTemplateJob) this).A04();
                            } else if (this instanceof RehydrateHsmJob) {
                                sb3 = new StringBuilder("RehydrateHsmJob/onCanceled/w: canceled rehydrate hsm job");
                                A04 = ((RehydrateHsmJob) this).A04();
                            } else if (this instanceof ReceiptProcessingJob) {
                                sb3 = new StringBuilder("ReceiptProcessingJob/onCanceled/cancel job param=");
                                A04 = ((ReceiptProcessingJob) this).A04();
                            } else if (this instanceof ReceiptMultiTargetProcessingJob) {
                                sb3 = new StringBuilder("ReceiptMultiTargetProcessingJob/onCanceled/cancel job param=");
                                A04 = ((ReceiptMultiTargetProcessingJob) this).A04();
                            } else if (!(this instanceof GetVNameCertificateJob)) {
                                if (this instanceof GetStatusPrivacyJob) {
                                    str5 = "canceled get status privacy job";
                                } else if (this instanceof GetHsmMessagePackJob) {
                                    GetHsmMessagePackJob getHsmMessagePackJob = (GetHsmMessagePackJob) this;
                                    StringBuilder sb10 = new StringBuilder("canceled get hsm message pack job");
                                    sb10.append(getHsmMessagePackJob.A04());
                                    Log.w(sb10.toString());
                                    HashSet hashSet = GetHsmMessagePackJob.A02;
                                    synchronized (hashSet) {
                                        hashSet.remove(getHsmMessagePackJob);
                                    }
                                    return;
                                } else if (this instanceof GenerateTcTokenJob) {
                                    GenerateTcTokenJob generateTcTokenJob = (GenerateTcTokenJob) this;
                                    UserJid userJid = generateTcTokenJob.A01;
                                    if (userJid != null) {
                                        generateTcTokenJob.A03.A01(userJid);
                                    }
                                    sb = new StringBuilder("canceled generate trusted contact token job");
                                    sb2 = new StringBuilder("; persistentId=");
                                    j = ((Job) generateTcTokenJob).A01;
                                    sb2.append(j);
                                } else if (!(this instanceof DeleteAccountFromHsmServerJob)) {
                                    sb3 = new StringBuilder("canceled bulk get pre key job");
                                    A04 = ((BulkGetPreKeyJob) this).A04();
                                } else {
                                    str5 = "DeleteAccountFromHsmServerJob/canceled delete account from hsm server job";
                                }
                                sb = new StringBuilder(str5);
                                sb2 = new StringBuilder("; persistentId=");
                                j = this.A01;
                                sb2.append(j);
                            } else {
                                GetVNameCertificateJob getVNameCertificateJob = (GetVNameCertificateJob) this;
                                StringBuilder sb11 = new StringBuilder("canceled get vname certificate job");
                                sb11.append(getVNameCertificateJob.A04());
                                Log.w(sb11.toString());
                                GetVNameCertificateJob.A02.remove(getVNameCertificateJob.jid);
                                return;
                            }
                            sb4.append(str4);
                            str2 = sb4.toString();
                            sb.append(str2);
                            str = sb.toString();
                        } else {
                            sb3 = new StringBuilder("SendPlayedReceiptJob/canceled send played receipts job; id=");
                            A04 = ((SendPlayedReceiptJob) this).messageId;
                        }
                        sb3.append(A04);
                        str = sb3.toString();
                    } else {
                        SendWebForwardJob.AckWebForwardJob ackWebForwardJob = (SendWebForwardJob.AckWebForwardJob) this;
                        String str6 = ackWebForwardJob.A04;
                        if (str6 == null || str6.equals(ackWebForwardJob.A01.A00().A03)) {
                            ackWebForwardJob.A00.A00(new SendWebForwardJob(ackWebForwardJob.A02, ackWebForwardJob.A03, str6));
                            return;
                        }
                        ackWebForwardJob.A01.A00();
                        return;
                    }
                }
            }
            str2 = sb2.toString();
            sb.append(str2);
            str = sb.toString();
        } else {
            str = "Fetch2FAEmailStatusJob/canceled";
        }
        Log.w(str);
    }

    public void A01(long j) {
        if (!(this instanceof SendStatusPrivacyListJob)) {
            this.A01 = j;
            return;
        }
        SendStatusPrivacyListJob sendStatusPrivacyListJob = (SendStatusPrivacyListJob) this;
        ((Job) sendStatusPrivacyListJob).A01 = j;
        SendStatusPrivacyListJob.A02 = j;
        StringBuilder sb = new StringBuilder("set persistent id for send status privacy job");
        sb.append(sendStatusPrivacyListJob.A04());
        Log.i(sb.toString());
    }

    public boolean A02() {
        for (Requirement requirement : this.parameters.requirements) {
            if (!requirement.AJu()) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:714:0x10e3 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.whatsapp.jobqueue.job.SyncDevicesJob */
    /* JADX DEBUG: Multi-variable search result rejected for r0v53, resolved type: com.whatsapp.jobqueue.job.SyncDevicesJob */
    /* JADX DEBUG: Multi-variable search result rejected for r0v55, resolved type: com.whatsapp.jobqueue.job.SyncDevicesJob */
    /* JADX WARN: Multi-variable type inference failed */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A03() {
        /*
        // Method dump skipped, instructions count: 5371
        */
        throw new UnsupportedOperationException("Method not decompiled: org.whispersystems.jobqueue.Job.A03():void");
    }
}
