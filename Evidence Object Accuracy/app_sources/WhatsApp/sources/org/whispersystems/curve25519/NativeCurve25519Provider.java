package org.whispersystems.curve25519;

import X.AnonymousClass23U;
import X.AnonymousClass5H6;
import X.AnonymousClass5XJ;

/* loaded from: classes3.dex */
public class NativeCurve25519Provider implements AnonymousClass5XJ {
    public AnonymousClass23U A00 = new AnonymousClass23U();

    private native boolean smokeCheck(int i);

    @Override // X.AnonymousClass5XJ
    public native byte[] calculateAgreement(byte[] bArr, byte[] bArr2);

    @Override // X.AnonymousClass5XJ
    public native byte[] calculateSignature(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public native byte[] calculateVrfSignature(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public native byte[] generatePrivateKey(byte[] bArr);

    @Override // X.AnonymousClass5XJ
    public native byte[] generatePublicKey(byte[] bArr);

    @Override // X.AnonymousClass5XJ
    public native boolean verifySignature(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public native byte[] verifyVrfSignature(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public NativeCurve25519Provider() {
        try {
            smokeCheck(31337);
        } catch (UnsatisfiedLinkError e) {
            throw new AnonymousClass5H6(e);
        }
    }

    @Override // X.AnonymousClass5XJ
    public byte[] AAH() {
        byte[] bArr = new byte[32];
        this.A00.A00(bArr);
        return generatePrivateKey(bArr);
    }

    @Override // X.AnonymousClass5XJ
    public byte[] AG4(int i) {
        byte[] bArr = new byte[64];
        this.A00.A00(bArr);
        return bArr;
    }
}
