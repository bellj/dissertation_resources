package org.whispersystems.curve25519;

import X.AnonymousClass5H6;
import X.AnonymousClass5XJ;

/* loaded from: classes3.dex */
public class OpportunisticCurve25519Provider implements AnonymousClass5XJ {
    public AnonymousClass5XJ A00;

    public OpportunisticCurve25519Provider() {
        try {
            this.A00 = new NativeCurve25519Provider();
        } catch (AnonymousClass5H6 unused) {
            this.A00 = new JavaCurve25519Provider();
        }
    }

    @Override // X.AnonymousClass5XJ
    public byte[] AAH() {
        return this.A00.AAH();
    }

    @Override // X.AnonymousClass5XJ
    public byte[] AG4(int i) {
        return this.A00.AG4(64);
    }

    @Override // X.AnonymousClass5XJ
    public byte[] calculateAgreement(byte[] bArr, byte[] bArr2) {
        return this.A00.calculateAgreement(bArr, bArr2);
    }

    @Override // X.AnonymousClass5XJ
    public byte[] calculateSignature(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        return this.A00.calculateSignature(bArr, bArr2, bArr3);
    }

    @Override // X.AnonymousClass5XJ
    public byte[] generatePublicKey(byte[] bArr) {
        return this.A00.generatePublicKey(bArr);
    }

    @Override // X.AnonymousClass5XJ
    public boolean verifySignature(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        return this.A00.verifySignature(bArr, bArr2, bArr3);
    }
}
