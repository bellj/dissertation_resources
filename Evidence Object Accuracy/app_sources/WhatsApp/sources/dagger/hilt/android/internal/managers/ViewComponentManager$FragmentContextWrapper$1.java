package dagger.hilt.android.internal.managers;

import X.AbstractC001200n;
import X.AnonymousClass054;
import X.AnonymousClass074;
import X.C51062Sr;

/* loaded from: classes3.dex */
public class ViewComponentManager$FragmentContextWrapper$1 implements AnonymousClass054 {
    public final /* synthetic */ C51062Sr A00;

    public ViewComponentManager$FragmentContextWrapper$1(C51062Sr r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass054
    public void AWQ(AnonymousClass074 r3, AbstractC001200n r4) {
        if (r3 == AnonymousClass074.ON_DESTROY) {
            C51062Sr r1 = this.A00;
            r1.A02 = null;
            r1.A00 = null;
            r1.A01 = null;
        }
    }
}
