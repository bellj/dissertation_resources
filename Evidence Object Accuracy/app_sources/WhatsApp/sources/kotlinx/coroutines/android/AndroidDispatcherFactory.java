package kotlinx.coroutines.android;

import X.AbstractC114155Kk;
import X.AnonymousClass0TV;
import X.AnonymousClass5L5;
import X.C12960it;
import android.os.Looper;
import java.util.List;

/* loaded from: classes3.dex */
public final class AndroidDispatcherFactory {
    public int getLoadPriority() {
        return 1073741823;
    }

    public String hintOnError() {
        return "For tests Dispatchers.setMain from kotlinx-coroutines-test module can be used";
    }

    public AbstractC114155Kk createDispatcher(List list) {
        Looper mainLooper = Looper.getMainLooper();
        if (mainLooper != null) {
            return new AnonymousClass5L5(AnonymousClass0TV.A00(mainLooper), false);
        }
        throw C12960it.A0U("The main looper is not available");
    }
}
