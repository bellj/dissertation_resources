package kotlinx.coroutines.android;

import X.AbstractC112675Eh;
import X.AnonymousClass5X4;
import X.AnonymousClass5ZT;
import android.os.Build;
import java.lang.Thread;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* loaded from: classes3.dex */
public final class AndroidExceptionPreHandler extends AbstractC112675Eh implements AnonymousClass5ZT {
    public volatile Object _preHandler = this;

    public AndroidExceptionPreHandler() {
        super(AnonymousClass5ZT.A00);
    }

    @Override // X.AnonymousClass5ZT
    public void handleException(AnonymousClass5X4 r5, Throwable th) {
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler;
        int i = Build.VERSION.SDK_INT;
        if (26 <= i && i < 28) {
            Method preHandler = preHandler();
            Object obj = null;
            if (preHandler != null) {
                obj = preHandler.invoke(null, new Object[0]);
            }
            if ((obj instanceof Thread.UncaughtExceptionHandler) && (uncaughtExceptionHandler = (Thread.UncaughtExceptionHandler) obj) != null) {
                uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), th);
            }
        }
    }

    private final Method preHandler() {
        Object obj = this._preHandler;
        if (obj != this) {
            return (Method) obj;
        }
        Method method = null;
        try {
            boolean z = false;
            Method declaredMethod = Thread.class.getDeclaredMethod("getUncaughtExceptionPreHandler", new Class[0]);
            if (Modifier.isPublic(declaredMethod.getModifiers())) {
                if (Modifier.isStatic(declaredMethod.getModifiers())) {
                    z = true;
                }
            }
            if (z) {
                method = declaredMethod;
            }
        } catch (Throwable unused) {
        }
        this._preHandler = method;
        return method;
    }
}
