package kotlinx.coroutines;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.Metadata;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.text.StringsKt__StringNumberConversionsKt;

/* compiled from: CommonPool.kt */
@Metadata(bv = {}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\bÀ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\"\u0010#J\b\u0010\u0003\u001a\u00020\u0002H\u0002J\b\u0010\u0004\u001a\u00020\u0002H\u0002J\b\u0010\u0006\u001a\u00020\u0005H\u0002J#\u0010\r\u001a\u00020\n2\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\u00072\u0006\u0010\t\u001a\u00020\u0002H\u0000¢\u0006\u0004\b\u000b\u0010\fJ\u001c\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u000f\u001a\u00020\u000e2\n\u0010\u0012\u001a\u00060\u0010j\u0002`\u0011H\u0016J\b\u0010\u0016\u001a\u00020\u0015H\u0016J\b\u0010\u0017\u001a\u00020\u0013H\u0016R\u0014\u0010\u0019\u001a\u00020\u00188\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\n8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0014\u0010!\u001a\u00020\u00188BX\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lkotlinx/coroutines/CommonPool;", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "Ljava/util/concurrent/ExecutorService;", "createPool", "createPlainPool", "Ljava/util/concurrent/Executor;", "getOrCreatePoolSync", "Ljava/lang/Class;", "fjpClass", "executor", "", "isGoodCommonPool$kotlinx_coroutines_core", "(Ljava/lang/Class;Ljava/util/concurrent/ExecutorService;)Z", "isGoodCommonPool", "Lkotlin/coroutines/CoroutineContext;", "context", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "block", "", "dispatch", "", "toString", "close", "", "requestedParallelism", "I", "usePrivatePool", "Z", "pool", "Ljava/util/concurrent/Executor;", "getParallelism", "()I", "parallelism", "<init>", "()V", "kotlinx-coroutines-core"}, k = 1, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public final class CommonPool extends ExecutorCoroutineDispatcher {
    public static final CommonPool INSTANCE = new CommonPool();
    private static volatile Executor pool;
    private static final int requestedParallelism;
    private static boolean usePrivatePool;

    /* renamed from: isGoodCommonPool$lambda-9 */
    public static final void m175isGoodCommonPool$lambda9() {
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher, java.lang.Object
    public String toString() {
        return "CommonPool";
    }

    private CommonPool() {
    }

    static {
        String str;
        int i;
        INSTANCE = new CommonPool();
        try {
            str = System.getProperty("kotlinx.coroutines.default.parallelism");
        } catch (Throwable unused) {
            str = null;
        }
        if (str == null) {
            i = -1;
        } else {
            Integer num = StringsKt__StringNumberConversionsKt.toIntOrNull(str);
            if (num == null || num.intValue() < 1) {
                throw new IllegalStateException(Intrinsics.stringPlus("Expected positive number in kotlinx.coroutines.default.parallelism, but has ", str).toString());
            }
            i = num.intValue();
        }
        requestedParallelism = i;
    }

    private final int getParallelism() {
        Integer valueOf = Integer.valueOf(requestedParallelism);
        if (!(valueOf.intValue() > 0)) {
            valueOf = null;
        }
        if (valueOf == null) {
            return RangesKt___RangesKt.coerceAtLeast(Runtime.getRuntime().availableProcessors() - 1, 1);
        }
        return valueOf.intValue();
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.concurrent.ExecutorService createPool() {
        /*
            r6 = this;
            java.lang.SecurityManager r0 = java.lang.System.getSecurityManager()
            if (r0 == 0) goto L_0x000b
            java.util.concurrent.ExecutorService r0 = r6.createPlainPool()
            return r0
        L_0x000b:
            r0 = 0
            java.lang.String r1 = "java.util.concurrent.ForkJoinPool"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch: all -> 0x0013
            goto L_0x0015
        L_0x0013:
            r1 = r0
        L_0x0015:
            if (r1 != 0) goto L_0x001c
            java.util.concurrent.ExecutorService r0 = r6.createPlainPool()
            return r0
        L_0x001c:
            boolean r2 = kotlinx.coroutines.CommonPool.usePrivatePool
            r3 = 0
            if (r2 != 0) goto L_0x004d
            int r2 = kotlinx.coroutines.CommonPool.requestedParallelism
            if (r2 >= 0) goto L_0x004d
            java.lang.String r2 = "commonPool"
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch: all -> 0x003a
            java.lang.reflect.Method r2 = r1.getMethod(r2, r4)     // Catch: all -> 0x003a
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch: all -> 0x003a
            java.lang.Object r2 = r2.invoke(r0, r4)     // Catch: all -> 0x003a
            boolean r4 = r2 instanceof java.util.concurrent.ExecutorService     // Catch: all -> 0x003a
            if (r4 == 0) goto L_0x003b
            java.util.concurrent.ExecutorService r2 = (java.util.concurrent.ExecutorService) r2     // Catch: all -> 0x003a
            goto L_0x003c
        L_0x003a:
        L_0x003b:
            r2 = r0
        L_0x003c:
            if (r2 != 0) goto L_0x003f
            goto L_0x004d
        L_0x003f:
            kotlinx.coroutines.CommonPool r4 = kotlinx.coroutines.CommonPool.INSTANCE
            boolean r4 = r4.isGoodCommonPool$kotlinx_coroutines_core(r1, r2)
            if (r4 == 0) goto L_0x0048
            goto L_0x0049
        L_0x0048:
            r2 = r0
        L_0x0049:
            if (r2 != 0) goto L_0x004c
            goto L_0x004d
        L_0x004c:
            return r2
        L_0x004d:
            r2 = 1
            java.lang.Class[] r4 = new java.lang.Class[r2]     // Catch: all -> 0x0072
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch: all -> 0x0072
            r4[r3] = r5     // Catch: all -> 0x0072
            java.lang.reflect.Constructor r1 = r1.getConstructor(r4)     // Catch: all -> 0x0072
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch: all -> 0x0072
            kotlinx.coroutines.CommonPool r4 = kotlinx.coroutines.CommonPool.INSTANCE     // Catch: all -> 0x0072
            int r4 = r4.getParallelism()     // Catch: all -> 0x0072
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch: all -> 0x0072
            r2[r3] = r4     // Catch: all -> 0x0072
            java.lang.Object r1 = r1.newInstance(r2)     // Catch: all -> 0x0072
            boolean r2 = r1 instanceof java.util.concurrent.ExecutorService     // Catch: all -> 0x0072
            if (r2 == 0) goto L_0x0073
            java.util.concurrent.ExecutorService r1 = (java.util.concurrent.ExecutorService) r1     // Catch: all -> 0x0072
            r0 = r1
            goto L_0x0073
        L_0x0072:
        L_0x0073:
            if (r0 != 0) goto L_0x0079
            java.util.concurrent.ExecutorService r0 = r6.createPlainPool()
        L_0x0079:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlinx.coroutines.CommonPool.createPool():java.util.concurrent.ExecutorService");
    }

    public final boolean isGoodCommonPool$kotlinx_coroutines_core(Class<?> cls, ExecutorService executorService) {
        executorService.submit(new Runnable() { // from class: kotlinx.coroutines.CommonPool$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                CommonPool.m173$r8$lambda$36bgNy4lLwRHCWOZfm6LcwyUbo();
            }
        });
        Integer num = null;
        try {
            Object invoke = cls.getMethod("getPoolSize", new Class[0]).invoke(executorService, new Object[0]);
            if (invoke instanceof Integer) {
                num = (Integer) invoke;
            }
        } catch (Throwable unused) {
        }
        if (num != null && num.intValue() >= 1) {
            return true;
        }
        return false;
    }

    private final ExecutorService createPlainPool() {
        return Executors.newFixedThreadPool(getParallelism(), new ThreadFactory(new AtomicInteger()) { // from class: kotlinx.coroutines.CommonPool$$ExternalSyntheticLambda0
            public final /* synthetic */ AtomicInteger f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.ThreadFactory
            public final Thread newThread(Runnable runnable) {
                return CommonPool.$r8$lambda$ERvPtt6BNpepqyLHHf5J6mHx7SQ(this.f$0, runnable);
            }
        });
    }

    /* renamed from: createPlainPool$lambda-12 */
    public static final Thread m174createPlainPool$lambda12(AtomicInteger atomicInteger, Runnable runnable) {
        Thread thread = new Thread(runnable, Intrinsics.stringPlus("CommonPool-worker-", Integer.valueOf(atomicInteger.incrementAndGet())));
        thread.setDaemon(true);
        return thread;
    }

    private final synchronized Executor getOrCreatePoolSync() {
        Executor executor;
        executor = pool;
        if (executor == null) {
            executor = createPool();
            pool = executor;
        }
        return executor;
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void dispatch(CoroutineContext coroutineContext, Runnable runnable) {
        try {
            Executor executor = pool;
            if (executor == null) {
                executor = getOrCreatePoolSync();
            }
            AbstractTimeSourceKt.getTimeSource();
            executor.execute(runnable);
        } catch (RejectedExecutionException unused) {
            AbstractTimeSourceKt.getTimeSource();
            DefaultExecutor.INSTANCE.enqueue(runnable);
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on CommonPool".toString());
    }
}
