package kotlinx.coroutines;

import kotlin.Metadata;
import kotlinx.coroutines.internal.Symbol;

/* compiled from: JobSupport.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0016\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a\u0010\u0010\u0001\u001a\u0004\u0018\u00010\u0000*\u0004\u0018\u00010\u0000H\u0000\"\u001a\u0010\u0003\u001a\u00020\u00028\u0002X\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u0012\u0004\b\u0005\u0010\u0006\"\u001a\u0010\u0007\u001a\u00020\u00028\u0000X\u0004¢\u0006\f\n\u0004\b\u0007\u0010\u0004\u0012\u0004\b\b\u0010\u0006\"\u001a\u0010\t\u001a\u00020\u00028\u0002X\u0004¢\u0006\f\n\u0004\b\t\u0010\u0004\u0012\u0004\b\n\u0010\u0006\"\u001a\u0010\u000b\u001a\u00020\u00028\u0002X\u0004¢\u0006\f\n\u0004\b\u000b\u0010\u0004\u0012\u0004\b\f\u0010\u0006\"\u001a\u0010\r\u001a\u00020\u00028\u0002X\u0004¢\u0006\f\n\u0004\b\r\u0010\u0004\u0012\u0004\b\u000e\u0010\u0006\"\u001a\u0010\u0010\u001a\u00020\u000f8\u0002X\u0004¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u0012\u0004\b\u0012\u0010\u0006\"\u001a\u0010\u0013\u001a\u00020\u000f8\u0002X\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0011\u0012\u0004\b\u0014\u0010\u0006¨\u0006\u0015"}, d2 = {"", "boxIncomplete", "Lkotlinx/coroutines/internal/Symbol;", "COMPLETING_ALREADY", "Lkotlinx/coroutines/internal/Symbol;", "getCOMPLETING_ALREADY$annotations", "()V", "COMPLETING_WAITING_CHILDREN", "getCOMPLETING_WAITING_CHILDREN$annotations", "COMPLETING_RETRY", "getCOMPLETING_RETRY$annotations", "TOO_LATE_TO_CANCEL", "getTOO_LATE_TO_CANCEL$annotations", "SEALED", "getSEALED$annotations", "Lkotlinx/coroutines/Empty;", "EMPTY_NEW", "Lkotlinx/coroutines/Empty;", "getEMPTY_NEW$annotations", "EMPTY_ACTIVE", "getEMPTY_ACTIVE$annotations", "kotlinx-coroutines-core"}, k = 2, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public final class JobSupportKt {
    private static final Symbol COMPLETING_ALREADY = new Symbol("COMPLETING_ALREADY");
    private static final Symbol COMPLETING_RETRY = new Symbol("COMPLETING_RETRY");
    public static final Symbol COMPLETING_WAITING_CHILDREN = new Symbol("COMPLETING_WAITING_CHILDREN");
    private static final Empty EMPTY_ACTIVE = new Empty(true);
    private static final Empty EMPTY_NEW = new Empty(false);
    private static final Symbol SEALED = new Symbol("SEALED");
    private static final Symbol TOO_LATE_TO_CANCEL = new Symbol("TOO_LATE_TO_CANCEL");

    public static final Object boxIncomplete(Object obj) {
        return obj instanceof Incomplete ? new IncompleteStateBox((Incomplete) obj) : obj;
    }
}
