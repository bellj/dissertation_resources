package kotlinx.coroutines;

import java.util.concurrent.CancellationException;
import kotlin.Metadata;
import kotlin.coroutines.CoroutineContext;

@Metadata(bv = {}, d1 = {"kotlinx/coroutines/JobKt__JobKt"}, d2 = {}, k = 4, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public final class JobKt {
    public static final void cancel(CoroutineContext coroutineContext, CancellationException cancellationException) {
        JobKt__JobKt.cancel(coroutineContext, cancellationException);
    }
}
