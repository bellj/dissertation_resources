package kotlinx.coroutines;

import kotlin.Metadata;
import kotlinx.coroutines.internal.Symbol;

/* compiled from: EventLoop.common.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\"\u001a\u0010\u0001\u001a\u00020\u00008\u0002X\u0004¢\u0006\f\n\u0004\b\u0001\u0010\u0002\u0012\u0004\b\u0003\u0010\u0004\"\u001a\u0010\u0005\u001a\u00020\u00008\u0002X\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0002\u0012\u0004\b\u0006\u0010\u0004*\u001e\b\u0002\u0010\t\u001a\u0004\b\u0000\u0010\u0007\"\b\u0012\u0004\u0012\u00028\u00000\b2\b\u0012\u0004\u0012\u00028\u00000\b¨\u0006\n"}, d2 = {"Lkotlinx/coroutines/internal/Symbol;", "DISPOSED_TASK", "Lkotlinx/coroutines/internal/Symbol;", "getDISPOSED_TASK$annotations", "()V", "CLOSED_EMPTY", "getCLOSED_EMPTY$annotations", "T", "Lkotlinx/coroutines/internal/LockFreeTaskQueueCore;", "Queue", "kotlinx-coroutines-core"}, k = 2, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public final class EventLoop_commonKt {
    private static final Symbol CLOSED_EMPTY = new Symbol("CLOSED_EMPTY");
    private static final Symbol DISPOSED_TASK = new Symbol("REMOVED_TASK");
}
