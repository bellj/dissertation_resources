package kotlinx.coroutines;

import kotlin.Metadata;

/* compiled from: AbstractTimeSource.kt */
@Metadata(bv = {}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0007\"$\u0010\u0001\u001a\u0004\u0018\u00010\u00008\u0000@\u0000X\u000e¢\u0006\u0012\n\u0004\b\u0001\u0010\u0002\u001a\u0004\b\u0003\u0010\u0004\"\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lkotlinx/coroutines/AbstractTimeSource;", "timeSource", "Lkotlinx/coroutines/AbstractTimeSource;", "getTimeSource", "()Lkotlinx/coroutines/AbstractTimeSource;", "setTimeSource", "(Lkotlinx/coroutines/AbstractTimeSource;)V", "kotlinx-coroutines-core"}, k = 2, mv = {1, 5, 1})
/* loaded from: classes3.dex */
public final class AbstractTimeSourceKt {
    public static final AbstractTimeSource getTimeSource() {
        return null;
    }
}
