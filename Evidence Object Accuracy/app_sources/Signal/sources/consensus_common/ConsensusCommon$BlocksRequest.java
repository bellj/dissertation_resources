package consensus_common;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ConsensusCommon$BlocksRequest extends GeneratedMessageLite<ConsensusCommon$BlocksRequest, Builder> implements MessageLiteOrBuilder {
    private static final ConsensusCommon$BlocksRequest DEFAULT_INSTANCE;
    public static final int LIMIT_FIELD_NUMBER;
    public static final int OFFSET_FIELD_NUMBER;
    private static volatile Parser<ConsensusCommon$BlocksRequest> PARSER;
    private int limit_;
    private long offset_;

    private ConsensusCommon$BlocksRequest() {
    }

    public long getOffset() {
        return this.offset_;
    }

    public void setOffset(long j) {
        this.offset_ = j;
    }

    public void clearOffset() {
        this.offset_ = 0;
    }

    public int getLimit() {
        return this.limit_;
    }

    public void setLimit(int i) {
        this.limit_ = i;
    }

    public void clearLimit() {
        this.limit_ = 0;
    }

    public static ConsensusCommon$BlocksRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$BlocksRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusCommon$BlocksRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$BlocksRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusCommon$BlocksRequest consensusCommon$BlocksRequest) {
        return DEFAULT_INSTANCE.createBuilder(consensusCommon$BlocksRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusCommon$BlocksRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusCommon$1 consensusCommon$1) {
            this();
        }

        private Builder() {
            super(ConsensusCommon$BlocksRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusCommon$BlocksRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0003\u0002\u000b", new Object[]{"offset_", "limit_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusCommon$BlocksRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusCommon$BlocksRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusCommon$BlocksRequest consensusCommon$BlocksRequest = new ConsensusCommon$BlocksRequest();
        DEFAULT_INSTANCE = consensusCommon$BlocksRequest;
        GeneratedMessageLite.registerDefaultInstance(ConsensusCommon$BlocksRequest.class, consensusCommon$BlocksRequest);
    }

    public static ConsensusCommon$BlocksRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusCommon$BlocksRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
