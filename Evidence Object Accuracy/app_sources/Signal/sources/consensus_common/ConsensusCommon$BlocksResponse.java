package consensus_common;

import blockchain.Blockchain$Block;
import blockchain.Blockchain$BlockOrBuilder;
import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class ConsensusCommon$BlocksResponse extends GeneratedMessageLite<ConsensusCommon$BlocksResponse, Builder> implements MessageLiteOrBuilder {
    public static final int BLOCKS_FIELD_NUMBER;
    private static final ConsensusCommon$BlocksResponse DEFAULT_INSTANCE;
    private static volatile Parser<ConsensusCommon$BlocksResponse> PARSER;
    private Internal.ProtobufList<Blockchain$Block> blocks_ = GeneratedMessageLite.emptyProtobufList();

    private ConsensusCommon$BlocksResponse() {
    }

    public List<Blockchain$Block> getBlocksList() {
        return this.blocks_;
    }

    public List<? extends Blockchain$BlockOrBuilder> getBlocksOrBuilderList() {
        return this.blocks_;
    }

    public int getBlocksCount() {
        return this.blocks_.size();
    }

    public Blockchain$Block getBlocks(int i) {
        return this.blocks_.get(i);
    }

    public Blockchain$BlockOrBuilder getBlocksOrBuilder(int i) {
        return this.blocks_.get(i);
    }

    private void ensureBlocksIsMutable() {
        if (!this.blocks_.isModifiable()) {
            this.blocks_ = GeneratedMessageLite.mutableCopy(this.blocks_);
        }
    }

    public void setBlocks(int i, Blockchain$Block blockchain$Block) {
        blockchain$Block.getClass();
        ensureBlocksIsMutable();
        this.blocks_.set(i, blockchain$Block);
    }

    public void setBlocks(int i, Blockchain$Block.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.set(i, builder.build());
    }

    public void addBlocks(Blockchain$Block blockchain$Block) {
        blockchain$Block.getClass();
        ensureBlocksIsMutable();
        this.blocks_.add(blockchain$Block);
    }

    public void addBlocks(int i, Blockchain$Block blockchain$Block) {
        blockchain$Block.getClass();
        ensureBlocksIsMutable();
        this.blocks_.add(i, blockchain$Block);
    }

    public void addBlocks(Blockchain$Block.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.add(builder.build());
    }

    public void addBlocks(int i, Blockchain$Block.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.add(i, builder.build());
    }

    public void addAllBlocks(Iterable<? extends Blockchain$Block> iterable) {
        ensureBlocksIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.blocks_);
    }

    public void clearBlocks() {
        this.blocks_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeBlocks(int i) {
        ensureBlocksIsMutable();
        this.blocks_.remove(i);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$BlocksResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusCommon$BlocksResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$BlocksResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusCommon$BlocksResponse consensusCommon$BlocksResponse) {
        return DEFAULT_INSTANCE.createBuilder(consensusCommon$BlocksResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusCommon$BlocksResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusCommon$1 consensusCommon$1) {
            this();
        }

        private Builder() {
            super(ConsensusCommon$BlocksResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusCommon$BlocksResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"blocks_", Blockchain$Block.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusCommon$BlocksResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusCommon$BlocksResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusCommon$BlocksResponse consensusCommon$BlocksResponse = new ConsensusCommon$BlocksResponse();
        DEFAULT_INSTANCE = consensusCommon$BlocksResponse;
        GeneratedMessageLite.registerDefaultInstance(ConsensusCommon$BlocksResponse.class, consensusCommon$BlocksResponse);
    }

    public static ConsensusCommon$BlocksResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusCommon$BlocksResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
