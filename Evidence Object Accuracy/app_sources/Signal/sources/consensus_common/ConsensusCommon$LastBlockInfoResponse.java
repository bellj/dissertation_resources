package consensus_common;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ConsensusCommon$LastBlockInfoResponse extends GeneratedMessageLite<ConsensusCommon$LastBlockInfoResponse, Builder> implements MessageLiteOrBuilder {
    private static final ConsensusCommon$LastBlockInfoResponse DEFAULT_INSTANCE;
    public static final int INDEX_FIELD_NUMBER;
    public static final int MINIMUM_FEE_FIELD_NUMBER;
    private static volatile Parser<ConsensusCommon$LastBlockInfoResponse> PARSER;
    private long index_;
    private long minimumFee_;

    private ConsensusCommon$LastBlockInfoResponse() {
    }

    public long getIndex() {
        return this.index_;
    }

    public void setIndex(long j) {
        this.index_ = j;
    }

    public void clearIndex() {
        this.index_ = 0;
    }

    public long getMinimumFee() {
        return this.minimumFee_;
    }

    public void setMinimumFee(long j) {
        this.minimumFee_ = j;
    }

    public void clearMinimumFee() {
        this.minimumFee_ = 0;
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusCommon$LastBlockInfoResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$LastBlockInfoResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusCommon$LastBlockInfoResponse consensusCommon$LastBlockInfoResponse) {
        return DEFAULT_INSTANCE.createBuilder(consensusCommon$LastBlockInfoResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusCommon$LastBlockInfoResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusCommon$1 consensusCommon$1) {
            this();
        }

        private Builder() {
            super(ConsensusCommon$LastBlockInfoResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusCommon$LastBlockInfoResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0003\u0002\u0003", new Object[]{"index_", "minimumFee_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusCommon$LastBlockInfoResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusCommon$LastBlockInfoResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusCommon$LastBlockInfoResponse consensusCommon$LastBlockInfoResponse = new ConsensusCommon$LastBlockInfoResponse();
        DEFAULT_INSTANCE = consensusCommon$LastBlockInfoResponse;
        GeneratedMessageLite.registerDefaultInstance(ConsensusCommon$LastBlockInfoResponse.class, consensusCommon$LastBlockInfoResponse);
    }

    public static ConsensusCommon$LastBlockInfoResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusCommon$LastBlockInfoResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
