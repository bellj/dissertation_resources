package consensus_common;

import com.google.protobuf.Empty;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes3.dex */
public final class BlockchainAPIGrpc {
    private static volatile MethodDescriptor<Empty, ConsensusCommon$LastBlockInfoResponse> getGetLastBlockInfoMethod;

    /* renamed from: consensus_common.BlockchainAPIGrpc$1 */
    /* loaded from: classes3.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private BlockchainAPIGrpc() {
    }

    public static MethodDescriptor<Empty, ConsensusCommon$LastBlockInfoResponse> getGetLastBlockInfoMethod() {
        MethodDescriptor<Empty, ConsensusCommon$LastBlockInfoResponse> methodDescriptor = getGetLastBlockInfoMethod;
        if (methodDescriptor == null) {
            synchronized (BlockchainAPIGrpc.class) {
                methodDescriptor = getGetLastBlockInfoMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("consensus_common.BlockchainAPI", "GetLastBlockInfo")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Empty.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(ConsensusCommon$LastBlockInfoResponse.getDefaultInstance())).build();
                    getGetLastBlockInfoMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static BlockchainAPIBlockingStub newBlockingStub(Channel channel) {
        return (BlockchainAPIBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<BlockchainAPIBlockingStub>() { // from class: consensus_common.BlockchainAPIGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public BlockchainAPIBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new BlockchainAPIBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes3.dex */
    public static final class BlockchainAPIBlockingStub extends AbstractBlockingStub<BlockchainAPIBlockingStub> {
        /* synthetic */ BlockchainAPIBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private BlockchainAPIBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public BlockchainAPIBlockingStub build(Channel channel, CallOptions callOptions) {
            return new BlockchainAPIBlockingStub(channel, callOptions);
        }

        public ConsensusCommon$LastBlockInfoResponse getLastBlockInfo(Empty empty) {
            return (ConsensusCommon$LastBlockInfoResponse) ClientCalls.blockingUnaryCall(getChannel(), BlockchainAPIGrpc.getGetLastBlockInfoMethod(), getCallOptions(), empty);
        }
    }
}
