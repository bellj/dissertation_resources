package consensus_common;

import com.google.protobuf.Internal;

/* loaded from: classes3.dex */
public enum ConsensusCommon$ProposeTxResult implements Internal.EnumLite {
    Ok(0),
    InputsProofsLengthMismatch(10),
    NoInputs(11),
    TooManyInputs(12),
    InsufficientInputSignatures(13),
    InvalidInputSignature(14),
    InvalidTransactionSignature(15),
    InvalidRangeProof(16),
    InsufficientRingSize(17),
    TombstoneBlockExceeded(18),
    TombstoneBlockTooFar(19),
    NoOutputs(20),
    TooManyOutputs(21),
    ExcessiveRingSize(22),
    DuplicateRingElements(23),
    UnsortedRingElements(24),
    UnequalRingSizes(25),
    UnsortedKeyImages(26),
    ContainsSpentKeyImage(27),
    DuplicateKeyImages(28),
    DuplicateOutputPublicKey(29),
    ContainsExistingOutputPublicKey(30),
    MissingTxOutMembershipProof(31),
    InvalidTxOutMembershipProof(32),
    InvalidRistrettoPublicKey(33),
    InvalidLedgerContext(34),
    Ledger(35),
    MembershipProofValidationError(36),
    TxFeeError(37),
    KeyError(38),
    UnsortedInputs(39),
    UNRECOGNIZED(-1);
    
    private static final Internal.EnumLiteMap<ConsensusCommon$ProposeTxResult> internalValueMap = new Internal.EnumLiteMap<ConsensusCommon$ProposeTxResult>() { // from class: consensus_common.ConsensusCommon$ProposeTxResult.1
        @Override // com.google.protobuf.Internal.EnumLiteMap
        public ConsensusCommon$ProposeTxResult findValueByNumber(int i) {
            return ConsensusCommon$ProposeTxResult.forNumber(i);
        }
    };
    private final int value;

    @Override // com.google.protobuf.Internal.EnumLite
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    public static ConsensusCommon$ProposeTxResult forNumber(int i) {
        if (i == 0) {
            return Ok;
        }
        switch (i) {
            case 10:
                return InputsProofsLengthMismatch;
            case 11:
                return NoInputs;
            case 12:
                return TooManyInputs;
            case 13:
                return InsufficientInputSignatures;
            case 14:
                return InvalidInputSignature;
            case 15:
                return InvalidTransactionSignature;
            case 16:
                return InvalidRangeProof;
            case 17:
                return InsufficientRingSize;
            case 18:
                return TombstoneBlockExceeded;
            case 19:
                return TombstoneBlockTooFar;
            case 20:
                return NoOutputs;
            case 21:
                return TooManyOutputs;
            case 22:
                return ExcessiveRingSize;
            case 23:
                return DuplicateRingElements;
            case 24:
                return UnsortedRingElements;
            case 25:
                return UnequalRingSizes;
            case 26:
                return UnsortedKeyImages;
            case 27:
                return ContainsSpentKeyImage;
            case 28:
                return DuplicateKeyImages;
            case 29:
                return DuplicateOutputPublicKey;
            case 30:
                return ContainsExistingOutputPublicKey;
            case 31:
                return MissingTxOutMembershipProof;
            case 32:
                return InvalidTxOutMembershipProof;
            case 33:
                return InvalidRistrettoPublicKey;
            case 34:
                return InvalidLedgerContext;
            case 35:
                return Ledger;
            case 36:
                return MembershipProofValidationError;
            case 37:
                return TxFeeError;
            case 38:
                return KeyError;
            case 39:
                return UnsortedInputs;
            default:
                return null;
        }
    }

    ConsensusCommon$ProposeTxResult(int i) {
        this.value = i;
    }
}
