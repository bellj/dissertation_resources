package consensus_common;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ConsensusCommon$ProposeTxResponse extends GeneratedMessageLite<ConsensusCommon$ProposeTxResponse, Builder> implements MessageLiteOrBuilder {
    public static final int BLOCK_COUNT_FIELD_NUMBER;
    private static final ConsensusCommon$ProposeTxResponse DEFAULT_INSTANCE;
    private static volatile Parser<ConsensusCommon$ProposeTxResponse> PARSER;
    public static final int RESULT_FIELD_NUMBER;
    private long blockCount_;
    private int result_;

    private ConsensusCommon$ProposeTxResponse() {
    }

    public int getResultValue() {
        return this.result_;
    }

    public ConsensusCommon$ProposeTxResult getResult() {
        ConsensusCommon$ProposeTxResult forNumber = ConsensusCommon$ProposeTxResult.forNumber(this.result_);
        return forNumber == null ? ConsensusCommon$ProposeTxResult.UNRECOGNIZED : forNumber;
    }

    public void setResultValue(int i) {
        this.result_ = i;
    }

    public void setResult(ConsensusCommon$ProposeTxResult consensusCommon$ProposeTxResult) {
        consensusCommon$ProposeTxResult.getClass();
        this.result_ = consensusCommon$ProposeTxResult.getNumber();
    }

    public void clearResult() {
        this.result_ = 0;
    }

    public long getBlockCount() {
        return this.blockCount_;
    }

    public void setBlockCount(long j) {
        this.blockCount_ = j;
    }

    public void clearBlockCount() {
        this.blockCount_ = 0;
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$ProposeTxResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusCommon$ProposeTxResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusCommon$ProposeTxResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusCommon$ProposeTxResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusCommon$ProposeTxResponse consensusCommon$ProposeTxResponse) {
        return DEFAULT_INSTANCE.createBuilder(consensusCommon$ProposeTxResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusCommon$ProposeTxResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusCommon$1 consensusCommon$1) {
            this();
        }

        private Builder() {
            super(ConsensusCommon$ProposeTxResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusCommon$ProposeTxResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0002\u0003", new Object[]{"result_", "blockCount_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusCommon$ProposeTxResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusCommon$ProposeTxResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusCommon$ProposeTxResponse consensusCommon$ProposeTxResponse = new ConsensusCommon$ProposeTxResponse();
        DEFAULT_INSTANCE = consensusCommon$ProposeTxResponse;
        GeneratedMessageLite.registerDefaultInstance(ConsensusCommon$ProposeTxResponse.class, consensusCommon$ProposeTxResponse);
    }

    public static ConsensusCommon$ProposeTxResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusCommon$ProposeTxResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
