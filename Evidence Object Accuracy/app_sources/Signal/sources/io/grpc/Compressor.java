package io.grpc;

import java.io.IOException;
import java.io.OutputStream;

/* loaded from: classes3.dex */
public interface Compressor {
    OutputStream compress(OutputStream outputStream) throws IOException;

    @Override // io.grpc.Decompressor
    String getMessageEncoding();
}
