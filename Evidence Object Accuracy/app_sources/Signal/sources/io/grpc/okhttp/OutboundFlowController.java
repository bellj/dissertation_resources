package io.grpc.okhttp;

import com.google.common.base.Preconditions;
import io.grpc.okhttp.internal.framed.FrameWriter;
import java.io.IOException;
import okio.Buffer;
import org.thoughtcrime.securesms.database.PushDatabase;

/* loaded from: classes3.dex */
public class OutboundFlowController {
    private final OutboundFlowState connectionState = new OutboundFlowState(0, 65535);
    private final FrameWriter frameWriter;
    private int initialWindowSize = 65535;
    private final OkHttpClientTransport transport;

    public OutboundFlowController(OkHttpClientTransport okHttpClientTransport, FrameWriter frameWriter) {
        this.transport = (OkHttpClientTransport) Preconditions.checkNotNull(okHttpClientTransport, "transport");
        this.frameWriter = (FrameWriter) Preconditions.checkNotNull(frameWriter, "frameWriter");
    }

    public boolean initialOutboundWindowSize(int i) {
        if (i >= 0) {
            int i2 = i - this.initialWindowSize;
            this.initialWindowSize = i;
            OkHttpClientStream[] activeStreams = this.transport.getActiveStreams();
            for (OkHttpClientStream okHttpClientStream : activeStreams) {
                OutboundFlowState outboundFlowState = (OutboundFlowState) okHttpClientStream.getOutboundFlowState();
                if (outboundFlowState == null) {
                    okHttpClientStream.setOutboundFlowState(new OutboundFlowState(this, okHttpClientStream, this.initialWindowSize));
                } else {
                    outboundFlowState.incrementStreamWindow(i2);
                }
            }
            if (i2 > 0) {
                return true;
            }
            return false;
        }
        throw new IllegalArgumentException("Invalid initial window size: " + i);
    }

    public int windowUpdate(OkHttpClientStream okHttpClientStream, int i) {
        if (okHttpClientStream == null) {
            int incrementStreamWindow = this.connectionState.incrementStreamWindow(i);
            writeStreams();
            return incrementStreamWindow;
        }
        OutboundFlowState state = state(okHttpClientStream);
        int incrementStreamWindow2 = state.incrementStreamWindow(i);
        WriteStatus writeStatus = new WriteStatus();
        state.writeBytes(state.writableWindow(), writeStatus);
        if (writeStatus.hasWritten()) {
            flush();
        }
        return incrementStreamWindow2;
    }

    public void data(boolean z, int i, Buffer buffer, boolean z2) {
        Preconditions.checkNotNull(buffer, PushDatabase.SOURCE_E164);
        OkHttpClientStream stream = this.transport.getStream(i);
        if (stream != null) {
            OutboundFlowState state = state(stream);
            int writableWindow = state.writableWindow();
            boolean hasPendingData = state.hasPendingData();
            int size = (int) buffer.size();
            if (hasPendingData || writableWindow < size) {
                if (!hasPendingData && writableWindow > 0) {
                    state.write(buffer, writableWindow, false);
                }
                state.enqueue(buffer, (int) buffer.size(), z);
            } else {
                state.write(buffer, size, z);
            }
            if (z2) {
                flush();
            }
        }
    }

    void flush() {
        try {
            this.frameWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private OutboundFlowState state(OkHttpClientStream okHttpClientStream) {
        OutboundFlowState outboundFlowState = (OutboundFlowState) okHttpClientStream.getOutboundFlowState();
        if (outboundFlowState != null) {
            return outboundFlowState;
        }
        OutboundFlowState outboundFlowState2 = new OutboundFlowState(this, okHttpClientStream, this.initialWindowSize);
        okHttpClientStream.setOutboundFlowState(outboundFlowState2);
        return outboundFlowState2;
    }

    public void writeStreams() {
        int i;
        OkHttpClientStream[] activeStreams = this.transport.getActiveStreams();
        int window = this.connectionState.window();
        int length = activeStreams.length;
        while (true) {
            i = 0;
            if (length <= 0 || window <= 0) {
                break;
            }
            int ceil = (int) Math.ceil((double) (((float) window) / ((float) length)));
            for (int i2 = 0; i2 < length && window > 0; i2++) {
                OkHttpClientStream okHttpClientStream = activeStreams[i2];
                OutboundFlowState state = state(okHttpClientStream);
                int min = Math.min(window, Math.min(state.unallocatedBytes(), ceil));
                if (min > 0) {
                    state.allocateBytes(min);
                    window -= min;
                }
                if (state.unallocatedBytes() > 0) {
                    activeStreams[i] = okHttpClientStream;
                    i++;
                }
            }
            length = i;
        }
        WriteStatus writeStatus = new WriteStatus();
        OkHttpClientStream[] activeStreams2 = this.transport.getActiveStreams();
        int length2 = activeStreams2.length;
        while (i < length2) {
            OutboundFlowState state2 = state(activeStreams2[i]);
            state2.writeBytes(state2.allocatedBytes(), writeStatus);
            state2.clearAllocatedBytes();
            i++;
        }
        if (writeStatus.hasWritten()) {
            flush();
        }
    }

    /* loaded from: classes3.dex */
    public static final class WriteStatus {
        int numWrites;

        private WriteStatus() {
        }

        void incrementNumWrites() {
            this.numWrites++;
        }

        boolean hasWritten() {
            return this.numWrites > 0;
        }
    }

    /* loaded from: classes3.dex */
    public final class OutboundFlowState {
        int allocatedBytes;
        boolean pendingBufferHasEndOfStream;
        final Buffer pendingWriteBuffer;
        OkHttpClientStream stream;
        final int streamId;
        int window;

        OutboundFlowState(int i, int i2) {
            OutboundFlowController.this = r1;
            this.pendingBufferHasEndOfStream = false;
            this.streamId = i;
            this.window = i2;
            this.pendingWriteBuffer = new Buffer();
        }

        OutboundFlowState(OutboundFlowController outboundFlowController, OkHttpClientStream okHttpClientStream, int i) {
            this(okHttpClientStream.id(), i);
            this.stream = okHttpClientStream;
        }

        int window() {
            return this.window;
        }

        void allocateBytes(int i) {
            this.allocatedBytes += i;
        }

        int allocatedBytes() {
            return this.allocatedBytes;
        }

        int unallocatedBytes() {
            return streamableBytes() - this.allocatedBytes;
        }

        void clearAllocatedBytes() {
            this.allocatedBytes = 0;
        }

        int incrementStreamWindow(int i) {
            if (i <= 0 || Integer.MAX_VALUE - i >= this.window) {
                int i2 = this.window + i;
                this.window = i2;
                return i2;
            }
            throw new IllegalArgumentException("Window size overflow for stream: " + this.streamId);
        }

        int writableWindow() {
            return Math.min(this.window, OutboundFlowController.this.connectionState.window());
        }

        int streamableBytes() {
            return Math.max(0, Math.min(this.window, (int) this.pendingWriteBuffer.size()));
        }

        boolean hasPendingData() {
            return this.pendingWriteBuffer.size() > 0;
        }

        int writeBytes(int i, WriteStatus writeStatus) {
            int min = Math.min(i, writableWindow());
            int i2 = 0;
            while (hasPendingData() && min > 0) {
                if (((long) min) >= this.pendingWriteBuffer.size()) {
                    i2 += (int) this.pendingWriteBuffer.size();
                    Buffer buffer = this.pendingWriteBuffer;
                    write(buffer, (int) buffer.size(), this.pendingBufferHasEndOfStream);
                } else {
                    i2 += min;
                    write(this.pendingWriteBuffer, min, false);
                }
                writeStatus.incrementNumWrites();
                min = Math.min(i - i2, writableWindow());
            }
            return i2;
        }

        void write(Buffer buffer, int i, boolean z) {
            do {
                int min = Math.min(i, OutboundFlowController.this.frameWriter.maxDataLength());
                int i2 = -min;
                OutboundFlowController.this.connectionState.incrementStreamWindow(i2);
                incrementStreamWindow(i2);
                try {
                    OutboundFlowController.this.frameWriter.data(buffer.size() == ((long) min) && z, this.streamId, buffer, min);
                    this.stream.transportState().onSentBytes(min);
                    i -= min;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            } while (i > 0);
        }

        void enqueue(Buffer buffer, int i, boolean z) {
            this.pendingWriteBuffer.write(buffer, (long) i);
            this.pendingBufferHasEndOfStream |= z;
        }
    }
}
