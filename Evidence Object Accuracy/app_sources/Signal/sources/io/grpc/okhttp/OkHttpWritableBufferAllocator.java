package io.grpc.okhttp;

import androidx.recyclerview.widget.RecyclerView;
import io.grpc.internal.WritableBuffer;
import io.grpc.internal.WritableBufferAllocator;
import okio.Buffer;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class OkHttpWritableBufferAllocator implements WritableBufferAllocator {
    @Override // io.grpc.internal.WritableBufferAllocator
    public WritableBuffer allocate(int i) {
        return new OkHttpWritableBuffer(new Buffer(), Math.min(1048576, Math.max((int) RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT, i)));
    }
}
