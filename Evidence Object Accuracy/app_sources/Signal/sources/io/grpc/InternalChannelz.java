package io.grpc;

import com.google.common.base.Preconditions;
import j$.util.concurrent.ConcurrentHashMap;
import java.security.cert.Certificate;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

/* loaded from: classes3.dex */
public final class InternalChannelz {
    private static final InternalChannelz INSTANCE = new InternalChannelz();
    private static final Logger log = Logger.getLogger(InternalChannelz.class.getName());
    private final ConcurrentMap<Long, InternalInstrumented<Object>> otherSockets = new ConcurrentHashMap();
    private final ConcurrentMap<Long, Object> perServerSockets = new ConcurrentHashMap();
    private final ConcurrentNavigableMap<Long, InternalInstrumented<Object>> rootChannels = new ConcurrentSkipListMap();
    private final ConcurrentNavigableMap<Long, InternalInstrumented<Object>> servers = new ConcurrentSkipListMap();
    private final ConcurrentMap<Long, InternalInstrumented<Object>> subchannels = new ConcurrentHashMap();

    public static InternalChannelz instance() {
        return INSTANCE;
    }

    public void addSubchannel(InternalInstrumented<Object> internalInstrumented) {
        add(this.subchannels, internalInstrumented);
    }

    public void addRootChannel(InternalInstrumented<Object> internalInstrumented) {
        add(this.rootChannels, internalInstrumented);
    }

    public void addClientSocket(InternalInstrumented<Object> internalInstrumented) {
        add(this.otherSockets, internalInstrumented);
    }

    public void removeSubchannel(InternalInstrumented<Object> internalInstrumented) {
        remove(this.subchannels, internalInstrumented);
    }

    public void removeRootChannel(InternalInstrumented<Object> internalInstrumented) {
        remove(this.rootChannels, internalInstrumented);
    }

    public void removeClientSocket(InternalInstrumented<Object> internalInstrumented) {
        remove(this.otherSockets, internalInstrumented);
    }

    private static <T extends InternalInstrumented<?>> void add(Map<Long, T> map, T t) {
        map.put(Long.valueOf(t.getLogId().getId()), t);
    }

    private static <T extends InternalInstrumented<?>> void remove(Map<Long, T> map, T t) {
        map.remove(Long.valueOf(id(t)));
    }

    /* loaded from: classes3.dex */
    public static final class Security {
        public final Tls tls;

        public Security(Tls tls) {
            this.tls = (Tls) Preconditions.checkNotNull(tls);
        }
    }

    /* loaded from: classes3.dex */
    public static final class Tls {
        public final String cipherSuiteStandardName;
        public final Certificate localCert;
        public final Certificate remoteCert;

        public Tls(SSLSession sSLSession) {
            String cipherSuite = sSLSession.getCipherSuite();
            Certificate[] localCertificates = sSLSession.getLocalCertificates();
            Certificate certificate = null;
            Certificate certificate2 = localCertificates != null ? localCertificates[0] : null;
            try {
                Certificate[] peerCertificates = sSLSession.getPeerCertificates();
                if (peerCertificates != null) {
                    certificate = peerCertificates[0];
                }
            } catch (SSLPeerUnverifiedException e) {
                InternalChannelz.log.log(Level.FINE, String.format("Peer cert not available for peerHost=%s", sSLSession.getPeerHost()), (Throwable) e);
            }
            this.cipherSuiteStandardName = cipherSuite;
            this.localCert = certificate2;
            this.remoteCert = certificate;
        }
    }

    public static long id(InternalWithLogId internalWithLogId) {
        return internalWithLogId.getLogId().getId();
    }
}
