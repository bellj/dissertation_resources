package io.grpc;

/* loaded from: classes3.dex */
public final class InternalDecompressorRegistry {
    public static byte[] getRawAdvertisedMessageEncodings(DecompressorRegistry decompressorRegistry) {
        return decompressorRegistry.getRawAdvertisedMessageEncodings();
    }
}
