package io.grpc;

import io.grpc.Attributes;
import io.grpc.NameResolver;

/* loaded from: classes3.dex */
public abstract class NameResolverProvider extends NameResolver.Factory {
    @Deprecated
    public static final Attributes.Key<Integer> PARAMS_DEFAULT_PORT = NameResolver.Factory.PARAMS_DEFAULT_PORT;

    /* access modifiers changed from: protected */
    public abstract boolean isAvailable();

    /* access modifiers changed from: protected */
    public abstract int priority();
}
