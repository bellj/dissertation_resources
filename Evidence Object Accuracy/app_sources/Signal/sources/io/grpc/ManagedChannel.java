package io.grpc;

import java.util.concurrent.TimeUnit;

/* loaded from: classes3.dex */
public abstract class ManagedChannel extends Channel {
    public abstract boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException;

    public abstract ManagedChannel shutdown();
}
