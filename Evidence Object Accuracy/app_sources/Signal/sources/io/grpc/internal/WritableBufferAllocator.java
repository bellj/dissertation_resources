package io.grpc.internal;

/* loaded from: classes3.dex */
public interface WritableBufferAllocator {
    WritableBuffer allocate(int i);
}
