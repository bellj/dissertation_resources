package io.grpc.internal;

/* loaded from: classes3.dex */
public interface LongCounter {
    void add(long j);
}
