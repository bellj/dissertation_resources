package io.grpc.internal;

import com.google.common.base.Preconditions;
import io.grpc.CallCredentials;
import io.grpc.ChannelCredentials;
import io.grpc.ClientInterceptor;
import io.grpc.CompressorRegistry;
import io.grpc.DecompressorRegistry;
import io.grpc.InternalChannelz;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.NameResolver;
import io.grpc.NameResolverRegistry;
import io.grpc.ProxyDetector;
import io.grpc.internal.ExponentialBackoffPolicy;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.thoughtcrime.securesms.BuildConfig;

/* loaded from: classes3.dex */
public final class ManagedChannelImplBuilder extends ManagedChannelBuilder<ManagedChannelImplBuilder> {
    private static final CompressorRegistry DEFAULT_COMPRESSOR_REGISTRY = CompressorRegistry.getDefaultInstance();
    private static final DecompressorRegistry DEFAULT_DECOMPRESSOR_REGISTRY = DecompressorRegistry.getDefaultInstance();
    private static final ObjectPool<? extends Executor> DEFAULT_EXECUTOR_POOL = SharedResourcePool.forResource(GrpcUtil.SHARED_CHANNEL_EXECUTOR);
    static final long IDLE_MODE_DEFAULT_TIMEOUT_MILLIS = TimeUnit.MINUTES.toMillis(30);
    static final long IDLE_MODE_MIN_TIMEOUT_MILLIS = TimeUnit.SECONDS.toMillis(1);
    private static final Logger log = Logger.getLogger(ManagedChannelImplBuilder.class.getName());
    String authorityOverride;
    final CallCredentials callCredentials;
    private final ChannelBuilderDefaultPortProvider channelBuilderDefaultPortProvider;
    InternalChannelz channelz;
    private final ClientTransportFactoryBuilder clientTransportFactoryBuilder;
    CompressorRegistry compressorRegistry;
    DecompressorRegistry decompressorRegistry;
    String defaultLbPolicy;
    Map<String, ?> defaultServiceConfig;
    private final SocketAddress directServerAddress;
    ObjectPool<? extends Executor> executorPool;
    boolean fullStreamDecompression;
    long idleTimeoutMillis;
    private final List<ClientInterceptor> interceptors;
    boolean lookUpServiceConfig;
    int maxHedgedAttempts;
    int maxRetryAttempts;
    int maxTraceEvents;
    NameResolver.Factory nameResolverFactory;
    final NameResolverRegistry nameResolverRegistry;
    ObjectPool<? extends Executor> offloadExecutorPool;
    long perRpcBufferLimit;
    ProxyDetector proxyDetector;
    private boolean recordFinishedRpcs;
    private boolean recordRealTimeMetrics;
    private boolean recordStartedRpcs;
    long retryBufferSize;
    boolean retryEnabled;
    private boolean statsEnabled;
    final String target;
    boolean temporarilyDisableRetry;
    private boolean tracingEnabled;
    String userAgent;

    /* loaded from: classes3.dex */
    public interface ChannelBuilderDefaultPortProvider {
        int getDefaultPort();
    }

    /* loaded from: classes3.dex */
    public interface ClientTransportFactoryBuilder {
        ClientTransportFactory buildClientTransportFactory();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static final class ManagedChannelDefaultPortProvider implements ChannelBuilderDefaultPortProvider {
        @Override // io.grpc.internal.ManagedChannelImplBuilder.ChannelBuilderDefaultPortProvider
        public int getDefaultPort() {
            return BuildConfig.CONTENT_PROXY_PORT;
        }

        private ManagedChannelDefaultPortProvider() {
        }
    }

    public ManagedChannelImplBuilder(String str, ClientTransportFactoryBuilder clientTransportFactoryBuilder, ChannelBuilderDefaultPortProvider channelBuilderDefaultPortProvider) {
        this(str, null, null, clientTransportFactoryBuilder, channelBuilderDefaultPortProvider);
    }

    public ManagedChannelImplBuilder(String str, ChannelCredentials channelCredentials, CallCredentials callCredentials, ClientTransportFactoryBuilder clientTransportFactoryBuilder, ChannelBuilderDefaultPortProvider channelBuilderDefaultPortProvider) {
        ObjectPool<? extends Executor> objectPool = DEFAULT_EXECUTOR_POOL;
        this.executorPool = objectPool;
        this.offloadExecutorPool = objectPool;
        this.interceptors = new ArrayList();
        NameResolverRegistry defaultRegistry = NameResolverRegistry.getDefaultRegistry();
        this.nameResolverRegistry = defaultRegistry;
        this.nameResolverFactory = defaultRegistry.asFactory();
        this.defaultLbPolicy = "pick_first";
        this.decompressorRegistry = DEFAULT_DECOMPRESSOR_REGISTRY;
        this.compressorRegistry = DEFAULT_COMPRESSOR_REGISTRY;
        this.idleTimeoutMillis = IDLE_MODE_DEFAULT_TIMEOUT_MILLIS;
        this.maxRetryAttempts = 5;
        this.maxHedgedAttempts = 5;
        this.retryBufferSize = 16777216;
        this.perRpcBufferLimit = 1048576;
        this.retryEnabled = false;
        this.channelz = InternalChannelz.instance();
        this.lookUpServiceConfig = true;
        this.statsEnabled = true;
        this.recordStartedRpcs = true;
        this.recordFinishedRpcs = true;
        this.recordRealTimeMetrics = false;
        this.tracingEnabled = true;
        this.target = (String) Preconditions.checkNotNull(str, "target");
        this.callCredentials = callCredentials;
        this.clientTransportFactoryBuilder = (ClientTransportFactoryBuilder) Preconditions.checkNotNull(clientTransportFactoryBuilder, "clientTransportFactoryBuilder");
        this.directServerAddress = null;
        if (channelBuilderDefaultPortProvider != null) {
            this.channelBuilderDefaultPortProvider = channelBuilderDefaultPortProvider;
        } else {
            this.channelBuilderDefaultPortProvider = new ManagedChannelDefaultPortProvider();
        }
    }

    @Override // io.grpc.ManagedChannelBuilder
    public ManagedChannel build() {
        return new ManagedChannelOrphanWrapper(new ManagedChannelImpl(this, this.clientTransportFactoryBuilder.buildClientTransportFactory(), new ExponentialBackoffPolicy.Provider(), SharedResourcePool.forResource(GrpcUtil.SHARED_CHANNEL_EXECUTOR), GrpcUtil.STOPWATCH_SUPPLIER, getEffectiveInterceptors(), TimeProvider.SYSTEM_TIME_PROVIDER));
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    java.util.List<io.grpc.ClientInterceptor> getEffectiveInterceptors() {
        /*
            r11 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            java.util.List<io.grpc.ClientInterceptor> r1 = r11.interceptors
            r0.<init>(r1)
            r1 = 0
            r11.temporarilyDisableRetry = r1
            boolean r2 = r11.statsEnabled
            java.lang.String r3 = "getClientInterceptor"
            r4 = 0
            r5 = 1
            java.lang.String r6 = "Unable to apply census stats"
            if (r2 == 0) goto L_0x0076
            r11.temporarilyDisableRetry = r5
            java.lang.String r2 = "io.grpc.census.InternalCensusStatsAccessor"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            r7 = 3
            java.lang.Class[] r8 = new java.lang.Class[r7]     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            java.lang.Class r9 = java.lang.Boolean.TYPE     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            r8[r1] = r9     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            r8[r5] = r9     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            r10 = 2
            r8[r10] = r9     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            java.lang.reflect.Method r2 = r2.getDeclaredMethod(r3, r8)     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            boolean r8 = r11.recordStartedRpcs     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            r7[r1] = r8     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            boolean r8 = r11.recordFinishedRpcs     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            r7[r5] = r8     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            boolean r8 = r11.recordRealTimeMetrics     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            r7[r10] = r8     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            java.lang.Object r2 = r2.invoke(r4, r7)     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            io.grpc.ClientInterceptor r2 = (io.grpc.ClientInterceptor) r2     // Catch: ClassNotFoundException -> 0x0068, NoSuchMethodException -> 0x005f, IllegalAccessException -> 0x0056, InvocationTargetException -> 0x004d
            goto L_0x0071
        L_0x004d:
            r2 = move-exception
            java.util.logging.Logger r7 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r8 = java.util.logging.Level.FINE
            r7.log(r8, r6, r2)
            goto L_0x0070
        L_0x0056:
            r2 = move-exception
            java.util.logging.Logger r7 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r8 = java.util.logging.Level.FINE
            r7.log(r8, r6, r2)
            goto L_0x0070
        L_0x005f:
            r2 = move-exception
            java.util.logging.Logger r7 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r8 = java.util.logging.Level.FINE
            r7.log(r8, r6, r2)
            goto L_0x0070
        L_0x0068:
            r2 = move-exception
            java.util.logging.Logger r7 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r8 = java.util.logging.Level.FINE
            r7.log(r8, r6, r2)
        L_0x0070:
            r2 = r4
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r0.add(r1, r2)
        L_0x0076:
            boolean r2 = r11.tracingEnabled
            if (r2 == 0) goto L_0x00ba
            r11.temporarilyDisableRetry = r5
            java.lang.String r2 = "io.grpc.census.InternalCensusTracingAccessor"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch: ClassNotFoundException -> 0x00ad, NoSuchMethodException -> 0x00a4, IllegalAccessException -> 0x009b, InvocationTargetException -> 0x0092
            java.lang.Class[] r5 = new java.lang.Class[r1]     // Catch: ClassNotFoundException -> 0x00ad, NoSuchMethodException -> 0x00a4, IllegalAccessException -> 0x009b, InvocationTargetException -> 0x0092
            java.lang.reflect.Method r2 = r2.getDeclaredMethod(r3, r5)     // Catch: ClassNotFoundException -> 0x00ad, NoSuchMethodException -> 0x00a4, IllegalAccessException -> 0x009b, InvocationTargetException -> 0x0092
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch: ClassNotFoundException -> 0x00ad, NoSuchMethodException -> 0x00a4, IllegalAccessException -> 0x009b, InvocationTargetException -> 0x0092
            java.lang.Object r2 = r2.invoke(r4, r3)     // Catch: ClassNotFoundException -> 0x00ad, NoSuchMethodException -> 0x00a4, IllegalAccessException -> 0x009b, InvocationTargetException -> 0x0092
            io.grpc.ClientInterceptor r2 = (io.grpc.ClientInterceptor) r2     // Catch: ClassNotFoundException -> 0x00ad, NoSuchMethodException -> 0x00a4, IllegalAccessException -> 0x009b, InvocationTargetException -> 0x0092
            r4 = r2
            goto L_0x00b5
        L_0x0092:
            r2 = move-exception
            java.util.logging.Logger r3 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r5 = java.util.logging.Level.FINE
            r3.log(r5, r6, r2)
            goto L_0x00b5
        L_0x009b:
            r2 = move-exception
            java.util.logging.Logger r3 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r5 = java.util.logging.Level.FINE
            r3.log(r5, r6, r2)
            goto L_0x00b5
        L_0x00a4:
            r2 = move-exception
            java.util.logging.Logger r3 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r5 = java.util.logging.Level.FINE
            r3.log(r5, r6, r2)
            goto L_0x00b5
        L_0x00ad:
            r2 = move-exception
            java.util.logging.Logger r3 = io.grpc.internal.ManagedChannelImplBuilder.log
            java.util.logging.Level r5 = java.util.logging.Level.FINE
            r3.log(r5, r6, r2)
        L_0x00b5:
            if (r4 == 0) goto L_0x00ba
            r0.add(r1, r4)
        L_0x00ba:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: io.grpc.internal.ManagedChannelImplBuilder.getEffectiveInterceptors():java.util.List");
    }

    public int getDefaultPort() {
        return this.channelBuilderDefaultPortProvider.getDefaultPort();
    }
}
