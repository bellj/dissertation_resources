package io.grpc.internal;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.base.Supplier;
import io.grpc.Attributes;
import io.grpc.CallOptions;
import io.grpc.ChannelLogger;
import io.grpc.ConnectivityState;
import io.grpc.ConnectivityStateInfo;
import io.grpc.EquivalentAddressGroup;
import io.grpc.HttpConnectProxiedSocketAddress;
import io.grpc.InternalChannelz;
import io.grpc.InternalInstrumented;
import io.grpc.InternalLogId;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import io.grpc.Status;
import io.grpc.SynchronizationContext;
import io.grpc.internal.BackoffPolicy;
import io.grpc.internal.ClientStreamListener;
import io.grpc.internal.ClientTransportFactory;
import io.grpc.internal.ManagedClientTransport;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* loaded from: classes3.dex */
public final class InternalSubchannel implements InternalInstrumented<Object>, TransportProvider {
    private volatile ManagedClientTransport activeTransport;
    private volatile List<EquivalentAddressGroup> addressGroups;
    private final Index addressIndex;
    private final String authority;
    private final BackoffPolicy.Provider backoffPolicyProvider;
    private final Callback callback;
    private final CallTracer callsTracer;
    private final ChannelLogger channelLogger;
    private final ChannelTracer channelTracer;
    private final InternalChannelz channelz;
    private final Stopwatch connectingTimer;
    private final InUseStateAggregator<ConnectionClientTransport> inUseStateAggregator = new InUseStateAggregator<ConnectionClientTransport>() { // from class: io.grpc.internal.InternalSubchannel.1
        @Override // io.grpc.internal.InUseStateAggregator
        protected void handleInUse() {
            InternalSubchannel.this.callback.onInUse(InternalSubchannel.this);
        }

        @Override // io.grpc.internal.InUseStateAggregator
        protected void handleNotInUse() {
            InternalSubchannel.this.callback.onNotInUse(InternalSubchannel.this);
        }
    };
    private final InternalLogId logId;
    private ConnectionClientTransport pendingTransport;
    private BackoffPolicy reconnectPolicy;
    private SynchronizationContext.ScheduledHandle reconnectTask;
    private final ScheduledExecutorService scheduledExecutor;
    private SynchronizationContext.ScheduledHandle shutdownDueToUpdateTask;
    private ManagedClientTransport shutdownDueToUpdateTransport;
    private Status shutdownReason;
    private volatile ConnectivityStateInfo state = ConnectivityStateInfo.forNonError(ConnectivityState.IDLE);
    private final SynchronizationContext syncContext;
    private final ClientTransportFactory transportFactory;
    private final Collection<ConnectionClientTransport> transports = new ArrayList();
    private final String userAgent;

    /* loaded from: classes3.dex */
    public static abstract class Callback {
        abstract void onInUse(InternalSubchannel internalSubchannel);

        abstract void onNotInUse(InternalSubchannel internalSubchannel);

        abstract void onStateChange(InternalSubchannel internalSubchannel, ConnectivityStateInfo connectivityStateInfo);

        abstract void onTerminated(InternalSubchannel internalSubchannel);
    }

    public InternalSubchannel(List<EquivalentAddressGroup> list, String str, String str2, BackoffPolicy.Provider provider, ClientTransportFactory clientTransportFactory, ScheduledExecutorService scheduledExecutorService, Supplier<Stopwatch> supplier, SynchronizationContext synchronizationContext, Callback callback, InternalChannelz internalChannelz, CallTracer callTracer, ChannelTracer channelTracer, InternalLogId internalLogId, ChannelLogger channelLogger) {
        Preconditions.checkNotNull(list, "addressGroups");
        Preconditions.checkArgument(!list.isEmpty(), "addressGroups is empty");
        checkListHasNoNulls(list, "addressGroups contains null entry");
        List<EquivalentAddressGroup> unmodifiableList = Collections.unmodifiableList(new ArrayList(list));
        this.addressGroups = unmodifiableList;
        this.addressIndex = new Index(unmodifiableList);
        this.authority = str;
        this.userAgent = str2;
        this.backoffPolicyProvider = provider;
        this.transportFactory = clientTransportFactory;
        this.scheduledExecutor = scheduledExecutorService;
        this.connectingTimer = supplier.get();
        this.syncContext = synchronizationContext;
        this.callback = callback;
        this.channelz = internalChannelz;
        this.callsTracer = callTracer;
        this.channelTracer = (ChannelTracer) Preconditions.checkNotNull(channelTracer, "channelTracer");
        this.logId = (InternalLogId) Preconditions.checkNotNull(internalLogId, "logId");
        this.channelLogger = (ChannelLogger) Preconditions.checkNotNull(channelLogger, "channelLogger");
    }

    @Override // io.grpc.internal.TransportProvider
    public ClientTransport obtainActiveTransport() {
        ManagedClientTransport managedClientTransport = this.activeTransport;
        if (managedClientTransport != null) {
            return managedClientTransport;
        }
        this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.2
            @Override // java.lang.Runnable
            public void run() {
                if (InternalSubchannel.this.state.getState() == ConnectivityState.IDLE) {
                    InternalSubchannel.this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "CONNECTING as requested");
                    InternalSubchannel.this.gotoNonErrorState(ConnectivityState.CONNECTING);
                    InternalSubchannel.this.startNewTransport();
                }
            }
        });
        return null;
    }

    public void startNewTransport() {
        InetSocketAddress inetSocketAddress;
        HttpConnectProxiedSocketAddress httpConnectProxiedSocketAddress;
        this.syncContext.throwIfNotInThisSynchronizationContext();
        Preconditions.checkState(this.reconnectTask == null, "Should have no reconnectTask scheduled");
        if (this.addressIndex.isAtBeginning()) {
            this.connectingTimer.reset().start();
        }
        SocketAddress currentAddress = this.addressIndex.getCurrentAddress();
        if (currentAddress instanceof HttpConnectProxiedSocketAddress) {
            httpConnectProxiedSocketAddress = (HttpConnectProxiedSocketAddress) currentAddress;
            inetSocketAddress = httpConnectProxiedSocketAddress.getTargetAddress();
        } else {
            inetSocketAddress = currentAddress;
            httpConnectProxiedSocketAddress = null;
        }
        Attributes currentEagAttributes = this.addressIndex.getCurrentEagAttributes();
        String str = (String) currentEagAttributes.get(EquivalentAddressGroup.ATTR_AUTHORITY_OVERRIDE);
        ClientTransportFactory.ClientTransportOptions clientTransportOptions = new ClientTransportFactory.ClientTransportOptions();
        if (str == null) {
            str = this.authority;
        }
        ClientTransportFactory.ClientTransportOptions httpConnectProxiedSocketAddress2 = clientTransportOptions.setAuthority(str).setEagAttributes(currentEagAttributes).setUserAgent(this.userAgent).setHttpConnectProxiedSocketAddress(httpConnectProxiedSocketAddress);
        TransportLogger transportLogger = new TransportLogger();
        transportLogger.logId = getLogId();
        CallTracingTransport callTracingTransport = new CallTracingTransport(this.transportFactory.newClientTransport(inetSocketAddress, httpConnectProxiedSocketAddress2, transportLogger), this.callsTracer);
        transportLogger.logId = callTracingTransport.getLogId();
        this.channelz.addClientSocket(callTracingTransport);
        this.pendingTransport = callTracingTransport;
        this.transports.add(callTracingTransport);
        Runnable start = callTracingTransport.start(new TransportListener(callTracingTransport, inetSocketAddress));
        if (start != null) {
            this.syncContext.executeLater(start);
        }
        this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "Started transport {0}", transportLogger.logId);
    }

    public void scheduleBackoff(Status status) {
        this.syncContext.throwIfNotInThisSynchronizationContext();
        gotoState(ConnectivityStateInfo.forTransientFailure(status));
        if (this.reconnectPolicy == null) {
            this.reconnectPolicy = this.backoffPolicyProvider.get();
        }
        long nextBackoffNanos = this.reconnectPolicy.nextBackoffNanos();
        Stopwatch stopwatch = this.connectingTimer;
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        long elapsed = nextBackoffNanos - stopwatch.elapsed(timeUnit);
        boolean z = false;
        this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "TRANSIENT_FAILURE ({0}). Will reconnect after {1} ns", printShortStatus(status), Long.valueOf(elapsed));
        if (this.reconnectTask == null) {
            z = true;
        }
        Preconditions.checkState(z, "previous reconnectTask is not done");
        this.reconnectTask = this.syncContext.schedule(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.1EndOfCurrentBackoff
            @Override // java.lang.Runnable
            public void run() {
                InternalSubchannel.this.reconnectTask = null;
                InternalSubchannel.this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "CONNECTING after backoff");
                InternalSubchannel.this.gotoNonErrorState(ConnectivityState.CONNECTING);
                InternalSubchannel.this.startNewTransport();
            }
        }, elapsed, timeUnit, this.scheduledExecutor);
    }

    public void gotoNonErrorState(ConnectivityState connectivityState) {
        this.syncContext.throwIfNotInThisSynchronizationContext();
        gotoState(ConnectivityStateInfo.forNonError(connectivityState));
    }

    private void gotoState(ConnectivityStateInfo connectivityStateInfo) {
        this.syncContext.throwIfNotInThisSynchronizationContext();
        if (this.state.getState() != connectivityStateInfo.getState()) {
            boolean z = this.state.getState() != ConnectivityState.SHUTDOWN;
            Preconditions.checkState(z, "Cannot transition out of SHUTDOWN to " + connectivityStateInfo);
            this.state = connectivityStateInfo;
            this.callback.onStateChange(this, connectivityStateInfo);
        }
    }

    public void updateAddresses(final List<EquivalentAddressGroup> list) {
        Preconditions.checkNotNull(list, "newAddressGroups");
        checkListHasNoNulls(list, "newAddressGroups contains null entry");
        Preconditions.checkArgument(!list.isEmpty(), "newAddressGroups is empty");
        this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.4
            /* JADX WARNING: Removed duplicated region for block: B:14:0x009b  */
            /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
            @Override // java.lang.Runnable
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r7 = this;
                    java.util.ArrayList r0 = new java.util.ArrayList
                    java.util.List r1 = r3
                    r0.<init>(r1)
                    java.util.List r0 = java.util.Collections.unmodifiableList(r0)
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel$Index r1 = io.grpc.internal.InternalSubchannel.access$800(r1)
                    java.net.SocketAddress r1 = r1.getCurrentAddress()
                    io.grpc.internal.InternalSubchannel r2 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel$Index r2 = io.grpc.internal.InternalSubchannel.access$800(r2)
                    r2.updateGroups(r0)
                    io.grpc.internal.InternalSubchannel r2 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel.access$902(r2, r0)
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.ConnectivityStateInfo r0 = io.grpc.internal.InternalSubchannel.access$100(r0)
                    io.grpc.ConnectivityState r0 = r0.getState()
                    io.grpc.ConnectivityState r2 = io.grpc.ConnectivityState.READY
                    r3 = 0
                    if (r0 == r2) goto L_0x0040
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.ConnectivityStateInfo r0 = io.grpc.internal.InternalSubchannel.access$100(r0)
                    io.grpc.ConnectivityState r0 = r0.getState()
                    io.grpc.ConnectivityState r4 = io.grpc.ConnectivityState.CONNECTING
                    if (r0 != r4) goto L_0x0098
                L_0x0040:
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel$Index r0 = io.grpc.internal.InternalSubchannel.access$800(r0)
                    boolean r0 = r0.seekTo(r1)
                    if (r0 != 0) goto L_0x0098
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.ConnectivityStateInfo r0 = io.grpc.internal.InternalSubchannel.access$100(r0)
                    io.grpc.ConnectivityState r0 = r0.getState()
                    if (r0 != r2) goto L_0x0074
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.ManagedClientTransport r0 = io.grpc.internal.InternalSubchannel.access$1000(r0)
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel.access$1002(r1, r3)
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel$Index r1 = io.grpc.internal.InternalSubchannel.access$800(r1)
                    r1.reset()
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.ConnectivityState r2 = io.grpc.ConnectivityState.IDLE
                    io.grpc.internal.InternalSubchannel.access$300(r1, r2)
                    goto L_0x0099
                L_0x0074:
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.ConnectionClientTransport r0 = io.grpc.internal.InternalSubchannel.access$1100(r0)
                    io.grpc.Status r1 = io.grpc.Status.UNAVAILABLE
                    java.lang.String r2 = "InternalSubchannel closed pending transport due to address change"
                    io.grpc.Status r1 = r1.withDescription(r2)
                    r0.shutdown(r1)
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel.access$1102(r0, r3)
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel$Index r0 = io.grpc.internal.InternalSubchannel.access$800(r0)
                    r0.reset()
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel.access$400(r0)
                L_0x0098:
                    r0 = r3
                L_0x0099:
                    if (r0 == 0) goto L_0x00e8
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.SynchronizationContext$ScheduledHandle r1 = io.grpc.internal.InternalSubchannel.access$1200(r1)
                    if (r1 == 0) goto L_0x00c7
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.ManagedClientTransport r1 = io.grpc.internal.InternalSubchannel.access$1300(r1)
                    io.grpc.Status r2 = io.grpc.Status.UNAVAILABLE
                    java.lang.String r4 = "InternalSubchannel closed transport early due to address change"
                    io.grpc.Status r2 = r2.withDescription(r4)
                    r1.shutdown(r2)
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.SynchronizationContext$ScheduledHandle r1 = io.grpc.internal.InternalSubchannel.access$1200(r1)
                    r1.cancel()
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel.access$1202(r1, r3)
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel.access$1302(r1, r3)
                L_0x00c7:
                    io.grpc.internal.InternalSubchannel r1 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.internal.InternalSubchannel.access$1302(r1, r0)
                    io.grpc.internal.InternalSubchannel r0 = io.grpc.internal.InternalSubchannel.this
                    io.grpc.SynchronizationContext r1 = io.grpc.internal.InternalSubchannel.access$1500(r0)
                    io.grpc.internal.InternalSubchannel$4$1 r2 = new io.grpc.internal.InternalSubchannel$4$1
                    r2.<init>()
                    r3 = 5
                    java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS
                    io.grpc.internal.InternalSubchannel r6 = io.grpc.internal.InternalSubchannel.this
                    java.util.concurrent.ScheduledExecutorService r6 = io.grpc.internal.InternalSubchannel.access$1400(r6)
                    io.grpc.SynchronizationContext$ScheduledHandle r1 = r1.schedule(r2, r3, r5, r6)
                    io.grpc.internal.InternalSubchannel.access$1202(r0, r1)
                L_0x00e8:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: io.grpc.internal.InternalSubchannel.AnonymousClass4.run():void");
            }
        });
    }

    public void shutdown(final Status status) {
        this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.5
            @Override // java.lang.Runnable
            public void run() {
                ConnectivityState state = InternalSubchannel.this.state.getState();
                ConnectivityState connectivityState = ConnectivityState.SHUTDOWN;
                if (state != connectivityState) {
                    InternalSubchannel.this.shutdownReason = status;
                    ManagedClientTransport managedClientTransport = InternalSubchannel.this.activeTransport;
                    ConnectionClientTransport connectionClientTransport = InternalSubchannel.this.pendingTransport;
                    InternalSubchannel.this.activeTransport = null;
                    InternalSubchannel.this.pendingTransport = null;
                    InternalSubchannel.this.gotoNonErrorState(connectivityState);
                    InternalSubchannel.this.addressIndex.reset();
                    if (InternalSubchannel.this.transports.isEmpty()) {
                        InternalSubchannel.this.handleTermination();
                    }
                    InternalSubchannel.this.cancelReconnectTask();
                    if (InternalSubchannel.this.shutdownDueToUpdateTask != null) {
                        InternalSubchannel.this.shutdownDueToUpdateTask.cancel();
                        InternalSubchannel.this.shutdownDueToUpdateTransport.shutdown(status);
                        InternalSubchannel.this.shutdownDueToUpdateTask = null;
                        InternalSubchannel.this.shutdownDueToUpdateTransport = null;
                    }
                    if (managedClientTransport != null) {
                        managedClientTransport.shutdown(status);
                    }
                    if (connectionClientTransport != null) {
                        connectionClientTransport.shutdown(status);
                    }
                }
            }
        });
    }

    public String toString() {
        return MoreObjects.toStringHelper(this).add("logId", this.logId.getId()).add("addressGroups", this.addressGroups).toString();
    }

    public void handleTermination() {
        this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.6
            @Override // java.lang.Runnable
            public void run() {
                InternalSubchannel.this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "Terminated");
                InternalSubchannel.this.callback.onTerminated(InternalSubchannel.this);
            }
        });
    }

    public void handleTransportInUseState(final ConnectionClientTransport connectionClientTransport, final boolean z) {
        this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.7
            @Override // java.lang.Runnable
            public void run() {
                InternalSubchannel.this.inUseStateAggregator.updateObjectInUse(connectionClientTransport, z);
            }
        });
    }

    public void shutdownNow(final Status status) {
        shutdown(status);
        this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.8
            @Override // java.lang.Runnable
            public void run() {
                for (ManagedClientTransport managedClientTransport : new ArrayList(InternalSubchannel.this.transports)) {
                    managedClientTransport.shutdownNow(status);
                }
            }
        });
    }

    public void cancelReconnectTask() {
        this.syncContext.throwIfNotInThisSynchronizationContext();
        SynchronizationContext.ScheduledHandle scheduledHandle = this.reconnectTask;
        if (scheduledHandle != null) {
            scheduledHandle.cancel();
            this.reconnectTask = null;
            this.reconnectPolicy = null;
        }
    }

    @Override // io.grpc.InternalWithLogId
    public InternalLogId getLogId() {
        return this.logId;
    }

    private static void checkListHasNoNulls(List<?> list, String str) {
        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Preconditions.checkNotNull(it.next(), str);
        }
    }

    /* loaded from: classes3.dex */
    public class TransportListener implements ManagedClientTransport.Listener {
        final SocketAddress address;
        boolean shutdownInitiated = false;
        final ConnectionClientTransport transport;

        TransportListener(ConnectionClientTransport connectionClientTransport, SocketAddress socketAddress) {
            InternalSubchannel.this = r1;
            this.transport = connectionClientTransport;
            this.address = socketAddress;
        }

        @Override // io.grpc.internal.ManagedClientTransport.Listener
        public void transportReady() {
            InternalSubchannel.this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "READY");
            InternalSubchannel.this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.TransportListener.1
                @Override // java.lang.Runnable
                public void run() {
                    InternalSubchannel.this.reconnectPolicy = null;
                    if (InternalSubchannel.this.shutdownReason != null) {
                        Preconditions.checkState(InternalSubchannel.this.activeTransport == null, "Unexpected non-null activeTransport");
                        TransportListener transportListener = TransportListener.this;
                        transportListener.transport.shutdown(InternalSubchannel.this.shutdownReason);
                        return;
                    }
                    ConnectionClientTransport connectionClientTransport = InternalSubchannel.this.pendingTransport;
                    TransportListener transportListener2 = TransportListener.this;
                    ConnectionClientTransport connectionClientTransport2 = transportListener2.transport;
                    if (connectionClientTransport == connectionClientTransport2) {
                        InternalSubchannel.this.activeTransport = connectionClientTransport2;
                        InternalSubchannel.this.pendingTransport = null;
                        InternalSubchannel.this.gotoNonErrorState(ConnectivityState.READY);
                    }
                }
            });
        }

        @Override // io.grpc.internal.ManagedClientTransport.Listener
        public void transportInUse(boolean z) {
            InternalSubchannel.this.handleTransportInUseState(this.transport, z);
        }

        @Override // io.grpc.internal.ManagedClientTransport.Listener
        public void transportShutdown(final Status status) {
            InternalSubchannel.this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "{0} SHUTDOWN with {1}", this.transport.getLogId(), InternalSubchannel.this.printShortStatus(status));
            this.shutdownInitiated = true;
            InternalSubchannel.this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.TransportListener.2
                @Override // java.lang.Runnable
                public void run() {
                    if (InternalSubchannel.this.state.getState() != ConnectivityState.SHUTDOWN) {
                        ManagedClientTransport managedClientTransport = InternalSubchannel.this.activeTransport;
                        TransportListener transportListener = TransportListener.this;
                        if (managedClientTransport == transportListener.transport) {
                            InternalSubchannel.this.activeTransport = null;
                            InternalSubchannel.this.addressIndex.reset();
                            InternalSubchannel.this.gotoNonErrorState(ConnectivityState.IDLE);
                            return;
                        }
                        ConnectionClientTransport connectionClientTransport = InternalSubchannel.this.pendingTransport;
                        TransportListener transportListener2 = TransportListener.this;
                        if (connectionClientTransport == transportListener2.transport) {
                            Preconditions.checkState(InternalSubchannel.this.state.getState() == ConnectivityState.CONNECTING, "Expected state is CONNECTING, actual state is %s", InternalSubchannel.this.state.getState());
                            InternalSubchannel.this.addressIndex.increment();
                            if (!InternalSubchannel.this.addressIndex.isValid()) {
                                InternalSubchannel.this.pendingTransport = null;
                                InternalSubchannel.this.addressIndex.reset();
                                InternalSubchannel.this.scheduleBackoff(status);
                                return;
                            }
                            InternalSubchannel.this.startNewTransport();
                        }
                    }
                }
            });
        }

        @Override // io.grpc.internal.ManagedClientTransport.Listener
        public void transportTerminated() {
            Preconditions.checkState(this.shutdownInitiated, "transportShutdown() must be called before transportTerminated().");
            InternalSubchannel.this.channelLogger.log(ChannelLogger.ChannelLogLevel.INFO, "{0} Terminated", this.transport.getLogId());
            InternalSubchannel.this.channelz.removeClientSocket(this.transport);
            InternalSubchannel.this.handleTransportInUseState(this.transport, false);
            InternalSubchannel.this.syncContext.execute(new Runnable() { // from class: io.grpc.internal.InternalSubchannel.TransportListener.3
                @Override // java.lang.Runnable
                public void run() {
                    InternalSubchannel.this.transports.remove(TransportListener.this.transport);
                    if (InternalSubchannel.this.state.getState() == ConnectivityState.SHUTDOWN && InternalSubchannel.this.transports.isEmpty()) {
                        InternalSubchannel.this.handleTermination();
                    }
                }
            });
        }
    }

    /* loaded from: classes3.dex */
    public static final class CallTracingTransport extends ForwardingConnectionClientTransport {
        private final CallTracer callTracer;
        private final ConnectionClientTransport delegate;

        private CallTracingTransport(ConnectionClientTransport connectionClientTransport, CallTracer callTracer) {
            this.delegate = connectionClientTransport;
            this.callTracer = callTracer;
        }

        @Override // io.grpc.internal.ForwardingConnectionClientTransport
        protected ConnectionClientTransport delegate() {
            return this.delegate;
        }

        @Override // io.grpc.internal.ForwardingConnectionClientTransport, io.grpc.internal.ClientTransport
        public ClientStream newStream(MethodDescriptor<?, ?> methodDescriptor, Metadata metadata, CallOptions callOptions) {
            final ClientStream newStream = super.newStream(methodDescriptor, metadata, callOptions);
            return new ForwardingClientStream() { // from class: io.grpc.internal.InternalSubchannel.CallTracingTransport.1
                @Override // io.grpc.internal.ForwardingClientStream
                protected ClientStream delegate() {
                    return newStream;
                }

                @Override // io.grpc.internal.ForwardingClientStream, io.grpc.internal.ClientStream
                public void start(final ClientStreamListener clientStreamListener) {
                    CallTracingTransport.this.callTracer.reportCallStarted();
                    super.start(new ForwardingClientStreamListener() { // from class: io.grpc.internal.InternalSubchannel.CallTracingTransport.1.1
                        @Override // io.grpc.internal.ForwardingClientStreamListener
                        protected ClientStreamListener delegate() {
                            return clientStreamListener;
                        }

                        @Override // io.grpc.internal.ForwardingClientStreamListener, io.grpc.internal.ClientStreamListener
                        public void closed(Status status, Metadata metadata2) {
                            CallTracingTransport.this.callTracer.reportCallEnded(status.isOk());
                            super.closed(status, metadata2);
                        }

                        @Override // io.grpc.internal.ForwardingClientStreamListener, io.grpc.internal.ClientStreamListener
                        public void closed(Status status, ClientStreamListener.RpcProgress rpcProgress, Metadata metadata2) {
                            CallTracingTransport.this.callTracer.reportCallEnded(status.isOk());
                            super.closed(status, rpcProgress, metadata2);
                        }
                    });
                }
            };
        }
    }

    /* loaded from: classes3.dex */
    public static final class Index {
        private List<EquivalentAddressGroup> addressGroups;
        private int addressIndex;
        private int groupIndex;

        public Index(List<EquivalentAddressGroup> list) {
            this.addressGroups = list;
        }

        public boolean isValid() {
            return this.groupIndex < this.addressGroups.size();
        }

        public boolean isAtBeginning() {
            return this.groupIndex == 0 && this.addressIndex == 0;
        }

        public void increment() {
            int i = this.addressIndex + 1;
            this.addressIndex = i;
            if (i >= this.addressGroups.get(this.groupIndex).getAddresses().size()) {
                this.groupIndex++;
                this.addressIndex = 0;
            }
        }

        public void reset() {
            this.groupIndex = 0;
            this.addressIndex = 0;
        }

        public SocketAddress getCurrentAddress() {
            return this.addressGroups.get(this.groupIndex).getAddresses().get(this.addressIndex);
        }

        public Attributes getCurrentEagAttributes() {
            return this.addressGroups.get(this.groupIndex).getAttributes();
        }

        public void updateGroups(List<EquivalentAddressGroup> list) {
            this.addressGroups = list;
            reset();
        }

        public boolean seekTo(SocketAddress socketAddress) {
            for (int i = 0; i < this.addressGroups.size(); i++) {
                int indexOf = this.addressGroups.get(i).getAddresses().indexOf(socketAddress);
                if (indexOf != -1) {
                    this.groupIndex = i;
                    this.addressIndex = indexOf;
                    return true;
                }
            }
            return false;
        }
    }

    public String printShortStatus(Status status) {
        StringBuilder sb = new StringBuilder();
        sb.append(status.getCode());
        if (status.getDescription() != null) {
            sb.append("(");
            sb.append(status.getDescription());
            sb.append(")");
        }
        return sb.toString();
    }

    /* loaded from: classes3.dex */
    public static final class TransportLogger extends ChannelLogger {
        InternalLogId logId;

        TransportLogger() {
        }

        @Override // io.grpc.ChannelLogger
        public void log(ChannelLogger.ChannelLogLevel channelLogLevel, String str) {
            ChannelLoggerImpl.logOnly(this.logId, channelLogLevel, str);
        }

        @Override // io.grpc.ChannelLogger
        public void log(ChannelLogger.ChannelLogLevel channelLogLevel, String str, Object... objArr) {
            ChannelLoggerImpl.logOnly(this.logId, channelLogLevel, str, objArr);
        }
    }
}
