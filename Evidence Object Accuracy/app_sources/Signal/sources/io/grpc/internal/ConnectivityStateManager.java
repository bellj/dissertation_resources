package io.grpc.internal;

import com.google.common.base.Preconditions;
import io.grpc.ConnectivityState;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executor;

/* loaded from: classes3.dex */
public final class ConnectivityStateManager {
    private ArrayList<Listener> listeners = new ArrayList<>();
    private volatile ConnectivityState state = ConnectivityState.IDLE;

    public void gotoState(ConnectivityState connectivityState) {
        Preconditions.checkNotNull(connectivityState, "newState");
        if (this.state != connectivityState && this.state != ConnectivityState.SHUTDOWN) {
            this.state = connectivityState;
            if (!this.listeners.isEmpty()) {
                ArrayList<Listener> arrayList = this.listeners;
                this.listeners = new ArrayList<>();
                Iterator<Listener> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().runInExecutor();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static final class Listener {
        final Runnable callback;
        final Executor executor;

        void runInExecutor() {
            this.executor.execute(this.callback);
        }
    }
}
