package io.perfmark;

/* loaded from: classes3.dex */
public final class Tag {
    final long tagId;
    final String tagName;

    public Tag(String str, long j) {
        this.tagName = str;
        this.tagId = j;
    }
}
