package io.perfmark;

/* loaded from: classes3.dex */
public class Impl {
    static final Link NO_LINK = new Link(Long.MIN_VALUE);
    static final Tag NO_TAG = new Tag("", Long.MIN_VALUE);

    public void event(String str, Tag tag) {
    }

    public void linkIn(Link link) {
    }

    public void startTask(String str) {
    }

    public void startTask(String str, Tag tag) {
    }

    public void stopTask(String str) {
    }

    public void stopTask(String str, Tag tag) {
    }

    public Impl(Tag tag) {
        if (tag != NO_TAG) {
            throw new AssertionError("nope");
        }
    }

    public Link linkOut() {
        return NO_LINK;
    }

    public Tag createTag(String str, long j) {
        return NO_TAG;
    }
}
