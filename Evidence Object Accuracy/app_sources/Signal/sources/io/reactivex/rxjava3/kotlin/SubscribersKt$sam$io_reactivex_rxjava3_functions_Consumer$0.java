package io.reactivex.rxjava3.kotlin;

import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: subscribers.kt */
/* access modifiers changed from: package-private */
@Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class SubscribersKt$sam$io_reactivex_rxjava3_functions_Consumer$0 implements Consumer {
    private final /* synthetic */ Function1 function;

    public SubscribersKt$sam$io_reactivex_rxjava3_functions_Consumer$0(Function1 function1) {
        this.function = function1;
    }

    @Override // io.reactivex.rxjava3.functions.Consumer
    public final /* synthetic */ void accept(Object obj) {
        Intrinsics.checkNotNullExpressionValue(this.function.invoke(obj), "invoke(...)");
    }
}
