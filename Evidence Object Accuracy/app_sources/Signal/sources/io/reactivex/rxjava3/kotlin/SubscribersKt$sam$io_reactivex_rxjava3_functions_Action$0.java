package io.reactivex.rxjava3.kotlin;

import io.reactivex.rxjava3.functions.Action;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: subscribers.kt */
/* access modifiers changed from: package-private */
@Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class SubscribersKt$sam$io_reactivex_rxjava3_functions_Action$0 implements Action {
    private final /* synthetic */ Function0 function;

    public SubscribersKt$sam$io_reactivex_rxjava3_functions_Action$0(Function0 function0) {
        this.function = function0;
    }

    @Override // io.reactivex.rxjava3.functions.Action
    public final /* synthetic */ void run() {
        Intrinsics.checkNotNullExpressionValue(this.function.invoke(), "invoke(...)");
    }
}
