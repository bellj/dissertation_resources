package io.reactivex.rxjava3.kotlin;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.internal.functions.Functions;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: subscribers.kt */
@Metadata(bv = {}, d1 = {"\u0000D\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a(\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\"\b\b\u0000\u0010\u0001*\u00020\u0000*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00030\u0002H\u0002\u001a\u001e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0004*\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u0002H\u0002\u001a\u0012\u0010\n\u001a\u00020\t*\b\u0012\u0004\u0012\u00020\u00030\bH\u0002\u001aX\u0010\u0010\u001a\u00020\u000f\"\b\b\u0000\u0010\u0001*\u00020\u0000*\b\u0012\u0004\u0012\u00028\u00000\u000b2\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\b2\u0014\b\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00030\u0002H\u0007\u001aX\u0010\u0010\u001a\u00020\u000f\"\b\b\u0000\u0010\u0001*\u00020\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00112\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\b2\u0014\b\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00030\u0002H\u0007\u001aH\u0010\u0010\u001a\u00020\u000f\"\b\b\u0000\u0010\u0001*\u00020\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00122\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u00022\u0014\b\u0002\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00030\u0002H\u0007\u001a2\u0010\u0010\u001a\u00020\u000f*\u00020\u00142\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\bH\u0007\" \u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00030\u00028\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016\" \u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u00028\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0016\"\u001a\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00030\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001a"}, d2 = {"", "T", "Lkotlin/Function1;", "", "Lio/reactivex/rxjava3/functions/Consumer;", "asConsumer", "", "asOnErrorConsumer", "Lkotlin/Function0;", "Lio/reactivex/rxjava3/functions/Action;", "asOnCompleteAction", "Lio/reactivex/rxjava3/core/Observable;", "onError", "onComplete", "onNext", "Lio/reactivex/rxjava3/disposables/Disposable;", "subscribeBy", "Lio/reactivex/rxjava3/core/Flowable;", "Lio/reactivex/rxjava3/core/Single;", "onSuccess", "Lio/reactivex/rxjava3/core/Completable;", "onNextStub", "Lkotlin/jvm/functions/Function1;", "onErrorStub", "onCompleteStub", "Lkotlin/jvm/functions/Function0;", "rxkotlin"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class SubscribersKt {
    private static final Function0<Unit> onCompleteStub = SubscribersKt$onCompleteStub$1.INSTANCE;
    private static final Function1<Throwable, Unit> onErrorStub = SubscribersKt$onErrorStub$1.INSTANCE;
    private static final Function1<Object, Unit> onNextStub = SubscribersKt$onNextStub$1.INSTANCE;

    private static final <T> Consumer<T> asConsumer(Function1<? super T, Unit> function1) {
        if (function1 == onNextStub) {
            Consumer<T> emptyConsumer = Functions.emptyConsumer();
            Intrinsics.checkNotNullExpressionValue(emptyConsumer, "Functions.emptyConsumer()");
            return emptyConsumer;
        }
        if (function1 != null) {
            function1 = (Function1<? super T, Unit>) new SubscribersKt$sam$io_reactivex_rxjava3_functions_Consumer$0(function1);
        }
        return (Consumer) function1;
    }

    private static final Consumer<Throwable> asOnErrorConsumer(Function1<? super Throwable, Unit> function1) {
        Object obj = function1;
        if (function1 == onErrorStub) {
            Consumer<Throwable> consumer = Functions.ON_ERROR_MISSING;
            Intrinsics.checkNotNullExpressionValue(consumer, "Functions.ON_ERROR_MISSING");
            return consumer;
        }
        if (function1 != null) {
            obj = new SubscribersKt$sam$io_reactivex_rxjava3_functions_Consumer$0(function1);
        }
        return (Consumer) obj;
    }

    private static final Action asOnCompleteAction(Function0<Unit> function0) {
        Object obj = function0;
        if (function0 == onCompleteStub) {
            Action action = Functions.EMPTY_ACTION;
            Intrinsics.checkNotNullExpressionValue(action, "Functions.EMPTY_ACTION");
            return action;
        }
        if (function0 != null) {
            obj = new SubscribersKt$sam$io_reactivex_rxjava3_functions_Action$0(function0);
        }
        return (Action) obj;
    }

    public static /* synthetic */ Disposable subscribeBy$default(Observable observable, Function1 function1, Function0 function0, Function1 function12, int i, Object obj) {
        if ((i & 1) != 0) {
            function1 = onErrorStub;
        }
        if ((i & 2) != 0) {
            function0 = onCompleteStub;
        }
        if ((i & 4) != 0) {
            function12 = onNextStub;
        }
        return subscribeBy(observable, function1, function0, function12);
    }

    public static final <T> Disposable subscribeBy(Observable<T> observable, Function1<? super Throwable, Unit> function1, Function0<Unit> function0, Function1<? super T, Unit> function12) {
        Intrinsics.checkNotNullParameter(observable, "$this$subscribeBy");
        Intrinsics.checkNotNullParameter(function1, "onError");
        Intrinsics.checkNotNullParameter(function0, "onComplete");
        Intrinsics.checkNotNullParameter(function12, "onNext");
        Disposable subscribe = observable.subscribe(asConsumer(function12), asOnErrorConsumer(function1), asOnCompleteAction(function0));
        Intrinsics.checkNotNullExpressionValue(subscribe, "subscribe(onNext.asConsu…ete.asOnCompleteAction())");
        return subscribe;
    }

    public static /* synthetic */ Disposable subscribeBy$default(Flowable flowable, Function1 function1, Function0 function0, Function1 function12, int i, Object obj) {
        if ((i & 1) != 0) {
            function1 = onErrorStub;
        }
        if ((i & 2) != 0) {
            function0 = onCompleteStub;
        }
        if ((i & 4) != 0) {
            function12 = onNextStub;
        }
        return subscribeBy(flowable, function1, function0, function12);
    }

    public static final <T> Disposable subscribeBy(Flowable<T> flowable, Function1<? super Throwable, Unit> function1, Function0<Unit> function0, Function1<? super T, Unit> function12) {
        Intrinsics.checkNotNullParameter(flowable, "$this$subscribeBy");
        Intrinsics.checkNotNullParameter(function1, "onError");
        Intrinsics.checkNotNullParameter(function0, "onComplete");
        Intrinsics.checkNotNullParameter(function12, "onNext");
        Disposable subscribe = flowable.subscribe(asConsumer(function12), asOnErrorConsumer(function1), asOnCompleteAction(function0));
        Intrinsics.checkNotNullExpressionValue(subscribe, "subscribe(onNext.asConsu…ete.asOnCompleteAction())");
        return subscribe;
    }

    public static /* synthetic */ Disposable subscribeBy$default(Single single, Function1 function1, Function1 function12, int i, Object obj) {
        if ((i & 1) != 0) {
            function1 = onErrorStub;
        }
        if ((i & 2) != 0) {
            function12 = onNextStub;
        }
        return subscribeBy(single, function1, function12);
    }

    public static final <T> Disposable subscribeBy(Single<T> single, Function1<? super Throwable, Unit> function1, Function1<? super T, Unit> function12) {
        Intrinsics.checkNotNullParameter(single, "$this$subscribeBy");
        Intrinsics.checkNotNullParameter(function1, "onError");
        Intrinsics.checkNotNullParameter(function12, "onSuccess");
        Disposable subscribe = single.subscribe(asConsumer(function12), asOnErrorConsumer(function1));
        Intrinsics.checkNotNullExpressionValue(subscribe, "subscribe(onSuccess.asCo…rror.asOnErrorConsumer())");
        return subscribe;
    }

    public static /* synthetic */ Disposable subscribeBy$default(Completable completable, Function1 function1, Function0 function0, int i, Object obj) {
        if ((i & 1) != 0) {
            function1 = onErrorStub;
        }
        if ((i & 2) != 0) {
            function0 = onCompleteStub;
        }
        return subscribeBy(completable, function1, function0);
    }

    public static final Disposable subscribeBy(Completable completable, Function1<? super Throwable, Unit> function1, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(completable, "$this$subscribeBy");
        Intrinsics.checkNotNullParameter(function1, "onError");
        Intrinsics.checkNotNullParameter(function0, "onComplete");
        Function1<Throwable, Unit> function12 = onErrorStub;
        if (function1 == function12 && function0 == onCompleteStub) {
            Disposable subscribe = completable.subscribe();
            Intrinsics.checkNotNullExpressionValue(subscribe, "subscribe()");
            return subscribe;
        } else if (function1 == function12) {
            Disposable subscribe2 = completable.subscribe(new SubscribersKt$sam$io_reactivex_rxjava3_functions_Action$0(function0));
            Intrinsics.checkNotNullExpressionValue(subscribe2, "subscribe(onComplete)");
            return subscribe2;
        } else {
            Disposable subscribe3 = completable.subscribe(asOnCompleteAction(function0), new SubscribersKt$sam$io_reactivex_rxjava3_functions_Consumer$0(function1));
            Intrinsics.checkNotNullExpressionValue(subscribe3, "subscribe(onComplete.asO…ion(), Consumer(onError))");
            return subscribe3;
        }
    }
}
