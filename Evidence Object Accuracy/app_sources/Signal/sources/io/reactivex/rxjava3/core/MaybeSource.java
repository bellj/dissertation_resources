package io.reactivex.rxjava3.core;

@FunctionalInterface
/* loaded from: classes3.dex */
public interface MaybeSource<T> {
    void subscribe(MaybeObserver<? super T> maybeObserver);
}
