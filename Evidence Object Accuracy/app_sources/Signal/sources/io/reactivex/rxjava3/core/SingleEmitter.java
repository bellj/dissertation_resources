package io.reactivex.rxjava3.core;

import io.reactivex.rxjava3.functions.Cancellable;

/* loaded from: classes3.dex */
public interface SingleEmitter<T> {
    void onError(Throwable th);

    void onSuccess(T t);

    void setCancellable(Cancellable cancellable);
}
