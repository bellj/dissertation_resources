package io.reactivex.rxjava3.core;

/* loaded from: classes3.dex */
public interface CompletableEmitter {
    void onComplete();

    void onError(Throwable th);
}
