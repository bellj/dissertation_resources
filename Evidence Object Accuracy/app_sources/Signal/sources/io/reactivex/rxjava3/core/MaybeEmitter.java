package io.reactivex.rxjava3.core;

/* loaded from: classes3.dex */
public interface MaybeEmitter<T> {
    void onComplete();

    void onSuccess(T t);
}
