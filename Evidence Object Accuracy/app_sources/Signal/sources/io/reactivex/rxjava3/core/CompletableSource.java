package io.reactivex.rxjava3.core;

@FunctionalInterface
/* loaded from: classes3.dex */
public interface CompletableSource {
    void subscribe(CompletableObserver completableObserver);
}
