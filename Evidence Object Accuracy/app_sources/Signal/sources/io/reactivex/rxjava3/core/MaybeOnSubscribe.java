package io.reactivex.rxjava3.core;

@FunctionalInterface
/* loaded from: classes3.dex */
public interface MaybeOnSubscribe<T> {
    void subscribe(MaybeEmitter<T> maybeEmitter) throws Throwable;
}
