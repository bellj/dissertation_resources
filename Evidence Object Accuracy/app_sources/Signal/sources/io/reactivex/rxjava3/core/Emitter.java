package io.reactivex.rxjava3.core;

/* loaded from: classes3.dex */
public interface Emitter<T> {
    void onComplete();

    void onError(Throwable th);

    void onNext(T t);
}
