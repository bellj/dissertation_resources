package io.reactivex.rxjava3.core;

import io.reactivex.rxjava3.disposables.Disposable;

/* loaded from: classes3.dex */
public interface CompletableObserver {
    @Override // io.reactivex.rxjava3.core.MaybeObserver
    void onComplete();

    void onError(Throwable th);

    void onSubscribe(Disposable disposable);
}
