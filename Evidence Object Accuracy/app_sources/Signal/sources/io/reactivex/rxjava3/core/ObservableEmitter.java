package io.reactivex.rxjava3.core;

import io.reactivex.rxjava3.functions.Cancellable;

/* loaded from: classes3.dex */
public interface ObservableEmitter<T> extends Emitter<T> {
    @Override // io.reactivex.rxjava3.disposables.Disposable
    boolean isDisposed();

    void setCancellable(Cancellable cancellable);
}
