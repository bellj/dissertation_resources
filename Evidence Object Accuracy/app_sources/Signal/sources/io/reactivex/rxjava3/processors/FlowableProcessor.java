package io.reactivex.rxjava3.processors;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.FlowableSubscriber;
import org.reactivestreams.Subscriber;

/* loaded from: classes3.dex */
public abstract class FlowableProcessor<T> extends Flowable<T> implements Subscriber, FlowableSubscriber<T> {
}
