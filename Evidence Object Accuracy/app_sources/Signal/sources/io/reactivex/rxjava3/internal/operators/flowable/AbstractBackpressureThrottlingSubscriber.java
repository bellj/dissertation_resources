package io.reactivex.rxjava3.internal.operators.flowable;

import io.reactivex.rxjava3.core.FlowableSubscriber;
import io.reactivex.rxjava3.internal.subscriptions.SubscriptionHelper;
import io.reactivex.rxjava3.internal.util.BackpressureHelper;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

/* loaded from: classes3.dex */
public abstract class AbstractBackpressureThrottlingSubscriber<T, R> extends AtomicInteger implements FlowableSubscriber<T>, Subscription {
    volatile boolean cancelled;
    final AtomicReference<R> current = new AtomicReference<>();
    volatile boolean done;
    final Subscriber<? super R> downstream;
    Throwable error;
    final AtomicLong requested = new AtomicLong();
    Subscription upstream;

    public AbstractBackpressureThrottlingSubscriber(Subscriber<? super R> subscriber) {
        this.downstream = subscriber;
    }

    @Override // io.reactivex.rxjava3.core.FlowableSubscriber, org.reactivestreams.Subscriber
    public void onSubscribe(Subscription subscription) {
        if (SubscriptionHelper.validate(this.upstream, subscription)) {
            this.upstream = subscription;
            this.downstream.onSubscribe(this);
            subscription.request(Long.MAX_VALUE);
        }
    }

    @Override // org.reactivestreams.Subscriber
    public void onError(Throwable th) {
        this.error = th;
        this.done = true;
        drain();
    }

    @Override // org.reactivestreams.Subscriber
    public void onComplete() {
        this.done = true;
        drain();
    }

    @Override // org.reactivestreams.Subscription
    public void request(long j) {
        if (SubscriptionHelper.validate(j)) {
            BackpressureHelper.add(this.requested, j);
            drain();
        }
    }

    @Override // org.reactivestreams.Subscription
    public void cancel() {
        if (!this.cancelled) {
            this.cancelled = true;
            this.upstream.cancel();
            if (getAndIncrement() == 0) {
                this.current.lazySet(null);
            }
        }
    }

    public void drain() {
        boolean z;
        if (getAndIncrement() == 0) {
            Subscriber<? super R> subscriber = this.downstream;
            AtomicLong atomicLong = this.requested;
            AtomicReference<R> atomicReference = this.current;
            int i = 1;
            do {
                long j = 0;
                while (true) {
                    z = false;
                    if (j == atomicLong.get()) {
                        break;
                    }
                    boolean z2 = this.done;
                    Object obj = (R) atomicReference.getAndSet(null);
                    boolean z3 = obj == null;
                    if (!checkTerminated(z2, z3, subscriber, atomicReference)) {
                        if (z3) {
                            break;
                        }
                        subscriber.onNext(obj);
                        j++;
                    } else {
                        return;
                    }
                }
                if (j == atomicLong.get()) {
                    boolean z4 = this.done;
                    if (atomicReference.get() == null) {
                        z = true;
                    }
                    if (checkTerminated(z4, z, subscriber, atomicReference)) {
                        return;
                    }
                }
                if (j != 0) {
                    BackpressureHelper.produced(atomicLong, j);
                }
                i = addAndGet(-i);
            } while (i != 0);
        }
    }

    boolean checkTerminated(boolean z, boolean z2, Subscriber<?> subscriber, AtomicReference<R> atomicReference) {
        if (this.cancelled) {
            atomicReference.lazySet(null);
            return true;
        } else if (!z) {
            return false;
        } else {
            Throwable th = this.error;
            if (th != null) {
                atomicReference.lazySet(null);
                subscriber.onError(th);
                return true;
            } else if (!z2) {
                return false;
            } else {
                subscriber.onComplete();
                return true;
            }
        }
    }
}
