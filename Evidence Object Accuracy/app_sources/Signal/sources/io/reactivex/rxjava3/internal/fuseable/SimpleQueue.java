package io.reactivex.rxjava3.internal.fuseable;

/* loaded from: classes3.dex */
public interface SimpleQueue<T> {
    void clear();

    boolean isEmpty();

    boolean offer(T t);

    T poll() throws Throwable;
}
