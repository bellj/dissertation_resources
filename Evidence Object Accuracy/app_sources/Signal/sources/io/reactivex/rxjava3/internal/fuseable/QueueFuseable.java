package io.reactivex.rxjava3.internal.fuseable;

/* loaded from: classes3.dex */
public interface QueueFuseable<T> extends SimpleQueue<T> {
    int requestFusion(int i);
}
