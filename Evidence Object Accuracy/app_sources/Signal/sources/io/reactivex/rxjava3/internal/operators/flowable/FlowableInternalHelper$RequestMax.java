package io.reactivex.rxjava3.internal.operators.flowable;

import io.reactivex.rxjava3.functions.Consumer;
import org.reactivestreams.Subscription;

/* loaded from: classes3.dex */
public enum FlowableInternalHelper$RequestMax implements Consumer<Subscription> {
    INSTANCE;

    public void accept(Subscription subscription) {
        subscription.request(Long.MAX_VALUE);
    }
}
