package io.reactivex.rxjava3.internal.util;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;

/* loaded from: classes3.dex */
public final class AtomicThrowable extends AtomicReference<Throwable> {
    public boolean tryAddThrowable(Throwable th) {
        return ExceptionHelper.addThrowable(this, th);
    }

    public boolean tryAddThrowableOrReport(Throwable th) {
        if (tryAddThrowable(th)) {
            return true;
        }
        RxJavaPlugins.onError(th);
        return false;
    }

    public Throwable terminate() {
        return ExceptionHelper.terminate(this);
    }

    public void tryTerminateAndReport() {
        Throwable terminate = terminate();
        if (terminate != null && terminate != ExceptionHelper.TERMINATED) {
            RxJavaPlugins.onError(terminate);
        }
    }

    public void tryTerminateConsumer(Subscriber<?> subscriber) {
        Throwable terminate = terminate();
        if (terminate == null) {
            subscriber.onComplete();
        } else if (terminate != ExceptionHelper.TERMINATED) {
            subscriber.onError(terminate);
        }
    }

    public void tryTerminateConsumer(Observer<?> observer) {
        Throwable terminate = terminate();
        if (terminate == null) {
            observer.onComplete();
        } else if (terminate != ExceptionHelper.TERMINATED) {
            observer.onError(terminate);
        }
    }
}
