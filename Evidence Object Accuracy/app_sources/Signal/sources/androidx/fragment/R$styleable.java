package androidx.fragment;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int[] Fragment = {16842755, 16842960, 16842961};
    public static final int[] FragmentContainerView = {16842755, 16842961};
    public static final int FragmentContainerView_android_name;
    public static final int FragmentContainerView_android_tag;
    public static final int Fragment_android_id;
    public static final int Fragment_android_name;
    public static final int Fragment_android_tag;
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
}
