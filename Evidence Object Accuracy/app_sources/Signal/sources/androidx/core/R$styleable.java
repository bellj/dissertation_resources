package androidx.core;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int ColorStateListItem_alpha;
    public static final int ColorStateListItem_android_alpha;
    public static final int ColorStateListItem_android_color;
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int FontFamilyFont_android_font;
    public static final int FontFamilyFont_android_fontStyle;
    public static final int FontFamilyFont_android_fontVariationSettings;
    public static final int FontFamilyFont_android_fontWeight;
    public static final int FontFamilyFont_android_ttcIndex;
    public static final int FontFamilyFont_font;
    public static final int FontFamilyFont_fontStyle;
    public static final int FontFamilyFont_fontVariationSettings;
    public static final int FontFamilyFont_fontWeight;
    public static final int FontFamilyFont_ttcIndex;
    public static final int FontFamily_fontProviderAuthority;
    public static final int FontFamily_fontProviderCerts;
    public static final int FontFamily_fontProviderFetchStrategy;
    public static final int FontFamily_fontProviderFetchTimeout;
    public static final int FontFamily_fontProviderPackage;
    public static final int FontFamily_fontProviderQuery;
    public static final int FontFamily_fontProviderSystemFontFamily;
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
    public static final int GradientColorItem_android_color;
    public static final int GradientColorItem_android_offset;
    public static final int GradientColor_android_centerColor;
    public static final int GradientColor_android_centerX;
    public static final int GradientColor_android_centerY;
    public static final int GradientColor_android_endColor;
    public static final int GradientColor_android_endX;
    public static final int GradientColor_android_endY;
    public static final int GradientColor_android_gradientRadius;
    public static final int GradientColor_android_startColor;
    public static final int GradientColor_android_startX;
    public static final int GradientColor_android_startY;
    public static final int GradientColor_android_tileMode;
    public static final int GradientColor_android_type;
}
