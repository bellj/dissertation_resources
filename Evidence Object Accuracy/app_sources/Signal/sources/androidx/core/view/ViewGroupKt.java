package androidx.core.view;

import android.view.View;
import android.view.ViewGroup;
import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.Sequence;

/* compiled from: ViewGroup.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010)\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\u0013\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0000H\u0002\"\u001b\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00020\u0004*\u00020\u00008F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\b"}, d2 = {"Landroid/view/ViewGroup;", "", "Landroid/view/View;", "iterator", "Lkotlin/sequences/Sequence;", "getChildren", "(Landroid/view/ViewGroup;)Lkotlin/sequences/Sequence;", "children", "core-ktx_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ViewGroupKt {
    public static final Iterator<View> iterator(ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter(viewGroup, "$this$iterator");
        return new Object(viewGroup) { // from class: androidx.core.view.ViewGroupKt$iterator$1
            final /* synthetic */ ViewGroup $this_iterator;
            private int index;

            @Override // j$.util.Iterator
            public /* synthetic */ void forEachRemaining(Consumer consumer) {
                Iterator.CC.$default$forEachRemaining(this, consumer);
            }

            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.util.function.Consumer] */
            @Override // java.util.Iterator
            public /* synthetic */ void forEachRemaining(java.util.function.Consumer<? super View> consumer) {
                forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
            }

            /* access modifiers changed from: package-private */
            {
                this.$this_iterator = r1;
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public boolean hasNext() {
                return this.index < this.$this_iterator.getChildCount();
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public View next() {
                ViewGroup viewGroup2 = this.$this_iterator;
                int i = this.index;
                this.index = i + 1;
                View childAt = viewGroup2.getChildAt(i);
                if (childAt != null) {
                    return childAt;
                }
                throw new IndexOutOfBoundsException();
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public void remove() {
                ViewGroup viewGroup2 = this.$this_iterator;
                int i = this.index - 1;
                this.index = i;
                viewGroup2.removeViewAt(i);
            }
        };
    }

    public static final Sequence<View> getChildren(ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter(viewGroup, "$this$children");
        return new Sequence<View>(viewGroup) { // from class: androidx.core.view.ViewGroupKt$children$1
            final /* synthetic */ ViewGroup $this_children;

            /* access modifiers changed from: package-private */
            {
                this.$this_children = r1;
            }

            @Override // kotlin.sequences.Sequence
            public java.util.Iterator<View> iterator() {
                return ViewGroupKt.iterator(this.$this_children);
            }
        };
    }
}
