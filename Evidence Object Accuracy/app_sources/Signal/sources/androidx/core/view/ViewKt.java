package androidx.core.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: View.kt */
@Metadata(bv = {}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u001a&\u0010\u0004\u001a\u00020\u0002*\u00020\u00002\u0014\b\u0004\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001H\bø\u0001\u0000\u001a&\u0010\u0006\u001a\u00020\u0005*\u00020\u00002\u0014\b\u0004\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001H\bø\u0001\u0000\u001a\u0014\u0010\n\u001a\u00020\t*\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u0007\"\u0016\u0010\u000e\u001a\u00020\u000b*\u00020\u00008Æ\u0002¢\u0006\u0006\u001a\u0004\b\f\u0010\r\u0002\u0007\n\u0005\b20\u0001¨\u0006\u000f"}, d2 = {"Landroid/view/View;", "Lkotlin/Function1;", "", "action", "doOnLayout", "Landroidx/core/view/OneShotPreDrawListener;", "doOnPreDraw", "Landroid/graphics/Bitmap$Config;", "config", "Landroid/graphics/Bitmap;", "drawToBitmap", "", "getMarginBottom", "(Landroid/view/View;)I", "marginBottom", "core-ktx_release"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ViewKt {
    public static final void doOnLayout(View view, Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter(view, "$this$doOnLayout");
        Intrinsics.checkNotNullParameter(function1, "action");
        if (!ViewCompat.isLaidOut(view) || view.isLayoutRequested()) {
            view.addOnLayoutChangeListener(new View.OnLayoutChangeListener(function1) { // from class: androidx.core.view.ViewKt$doOnLayout$$inlined$doOnNextLayout$1
                final /* synthetic */ Function1 $action$inlined;

                {
                    this.$action$inlined = r1;
                }

                @Override // android.view.View.OnLayoutChangeListener
                public void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    Intrinsics.checkNotNullParameter(view2, "view");
                    view2.removeOnLayoutChangeListener(this);
                    this.$action$inlined.invoke(view2);
                }
            });
        } else {
            function1.invoke(view);
        }
    }

    public static final OneShotPreDrawListener doOnPreDraw(View view, Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter(view, "$this$doOnPreDraw");
        Intrinsics.checkNotNullParameter(function1, "action");
        OneShotPreDrawListener add = OneShotPreDrawListener.add(view, new Runnable(view, function1) { // from class: androidx.core.view.ViewKt$doOnPreDraw$1
            final /* synthetic */ Function1 $action;
            final /* synthetic */ View $this_doOnPreDraw;

            {
                this.$this_doOnPreDraw = r1;
                this.$action = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.$action.invoke(this.$this_doOnPreDraw);
            }
        });
        Intrinsics.checkNotNullExpressionValue(add, "OneShotPreDrawListener.add(this) { action(this) }");
        return add;
    }

    public static /* synthetic */ Bitmap drawToBitmap$default(View view, Bitmap.Config config, int i, Object obj) {
        if ((i & 1) != 0) {
            config = Bitmap.Config.ARGB_8888;
        }
        return drawToBitmap(view, config);
    }

    public static final Bitmap drawToBitmap(View view, Bitmap.Config config) {
        Intrinsics.checkNotNullParameter(view, "$this$drawToBitmap");
        Intrinsics.checkNotNullParameter(config, "config");
        if (ViewCompat.isLaidOut(view)) {
            Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), config);
            Intrinsics.checkNotNullExpressionValue(createBitmap, "Bitmap.createBitmap(width, height, config)");
            Canvas canvas = new Canvas(createBitmap);
            canvas.translate(-((float) view.getScrollX()), -((float) view.getScrollY()));
            view.draw(canvas);
            return createBitmap;
        }
        throw new IllegalStateException("View needs to be laid out before calling drawToBitmap()");
    }

    public static final int getMarginBottom(View view) {
        Intrinsics.checkNotNullParameter(view, "$this$marginBottom");
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof ViewGroup.MarginLayoutParams)) {
            layoutParams = null;
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        if (marginLayoutParams != null) {
            return marginLayoutParams.bottomMargin;
        }
        return 0;
    }
}
