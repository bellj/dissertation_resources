package androidx.core;

/* loaded from: classes.dex */
public final class R$dimen {
    public static final int compat_notification_large_icon_max_height;
    public static final int compat_notification_large_icon_max_width;
    public static final int notification_big_circle_margin;
    public static final int notification_large_icon_width;
    public static final int notification_right_icon_size;
    public static final int notification_small_icon_background_padding;
    public static final int notification_small_icon_size_as_large;
    public static final int notification_subtext_size;
}
