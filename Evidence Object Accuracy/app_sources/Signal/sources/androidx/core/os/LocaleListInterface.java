package androidx.core.os;

import java.util.Locale;

/* loaded from: classes.dex */
public interface LocaleListInterface {
    Locale get(int i);

    Locale getFirstMatch(String[] strArr);

    Object getLocaleList();

    int size();
}
