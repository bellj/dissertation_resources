package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.LinearSystem;
import java.util.ArrayList;

/* loaded from: classes.dex */
public class Chain {
    public static void applyChainConstraints(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, ArrayList<ConstraintWidget> arrayList, int i) {
        ChainHead[] chainHeadArr;
        int i2;
        int i3;
        if (i == 0) {
            i3 = constraintWidgetContainer.mHorizontalChainsSize;
            chainHeadArr = constraintWidgetContainer.mHorizontalChainsArray;
            i2 = 0;
        } else {
            int i4 = constraintWidgetContainer.mVerticalChainsSize;
            chainHeadArr = constraintWidgetContainer.mVerticalChainsArray;
            i3 = i4;
            i2 = 2;
        }
        for (int i5 = 0; i5 < i3; i5++) {
            ChainHead chainHead = chainHeadArr[i5];
            chainHead.define();
            if (arrayList == null || arrayList.contains(chainHead.mFirst)) {
                applyChainConstraints(constraintWidgetContainer, linearSystem, i, i2, chainHead);
            }
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:301:0x0118 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v38 */
    /* JADX WARN: Type inference failed for: r2v56, types: [androidx.constraintlayout.solver.widgets.ConstraintWidget] */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        if (r7 == 2) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        if (r7 == 2) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003e, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0040, code lost:
        r5 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x024e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x02a7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x038c  */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x0395 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x03a8  */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x0477  */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x04ac  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x04bf A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x04d4  */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x04d7  */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x04dd  */
    /* JADX WARNING: Removed duplicated region for block: B:288:0x04e0  */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x04e4  */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x04f4  */
    /* JADX WARNING: Removed duplicated region for block: B:297:0x04f8 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x038d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:319:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void applyChainConstraints(androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r37, androidx.constraintlayout.solver.LinearSystem r38, int r39, int r40, androidx.constraintlayout.solver.widgets.ChainHead r41) {
        /*
        // Method dump skipped, instructions count: 1305
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.Chain.applyChainConstraints(androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer, androidx.constraintlayout.solver.LinearSystem, int, int, androidx.constraintlayout.solver.widgets.ChainHead):void");
    }
}
