package androidx.transition;

import android.view.View;

/* loaded from: classes.dex */
public interface ViewGroupOverlayImpl extends ViewOverlayImpl {
    void add(View view);

    void remove(View view);
}
