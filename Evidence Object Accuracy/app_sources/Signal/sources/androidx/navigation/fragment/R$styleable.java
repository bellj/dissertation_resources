package androidx.navigation.fragment;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] ActivityNavigator = {16842755, R.attr.action, R.attr.data, R.attr.dataPattern, R.attr.targetPackage};
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int[] DialogFragmentNavigator = {16842755};
    public static final int DialogFragmentNavigator_android_name;
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int[] Fragment = {16842755, 16842960, 16842961};
    public static final int[] FragmentContainerView = {16842755, 16842961};
    public static final int[] FragmentNavigator = {16842755};
    public static final int FragmentNavigator_android_name;
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
    public static final int[] NavAction = {16842960, R.attr.destination, R.attr.enterAnim, R.attr.exitAnim, R.attr.launchSingleTop, R.attr.popEnterAnim, R.attr.popExitAnim, R.attr.popUpTo, R.attr.popUpToInclusive};
    public static final int[] NavArgument = {16842755, 16843245, R.attr.argType, R.attr.nullable};
    public static final int[] NavDeepLink = {16844014, R.attr.action, R.attr.mimeType, R.attr.uri};
    public static final int[] NavGraphNavigator = {R.attr.startDestination};
    public static final int[] NavHost = {R.attr.navGraph};
    public static final int[] NavHostFragment = {R.attr.defaultNavHost};
    public static final int NavHostFragment_defaultNavHost;
    public static final int[] NavInclude = {R.attr.graph};
    public static final int[] Navigator = {16842753, 16842960};
}
