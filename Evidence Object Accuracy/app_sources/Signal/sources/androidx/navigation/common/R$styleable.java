package androidx.navigation.common;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
    public static final int[] NavAction = {16842960, R.attr.destination, R.attr.enterAnim, R.attr.exitAnim, R.attr.launchSingleTop, R.attr.popEnterAnim, R.attr.popExitAnim, R.attr.popUpTo, R.attr.popUpToInclusive};
    public static final int NavAction_android_id;
    public static final int NavAction_destination;
    public static final int NavAction_enterAnim;
    public static final int NavAction_exitAnim;
    public static final int NavAction_launchSingleTop;
    public static final int NavAction_popEnterAnim;
    public static final int NavAction_popExitAnim;
    public static final int NavAction_popUpTo;
    public static final int NavAction_popUpToInclusive;
    public static final int[] NavArgument = {16842755, 16843245, R.attr.argType, R.attr.nullable};
    public static final int NavArgument_android_defaultValue;
    public static final int NavArgument_android_name;
    public static final int NavArgument_argType;
    public static final int NavArgument_nullable;
    public static final int[] NavDeepLink = {16844014, R.attr.action, R.attr.mimeType, R.attr.uri};
    public static final int NavDeepLink_action;
    public static final int NavDeepLink_mimeType;
    public static final int NavDeepLink_uri;
    public static final int[] NavGraphNavigator = {R.attr.startDestination};
    public static final int NavGraphNavigator_startDestination;
    public static final int[] Navigator = {16842753, 16842960};
    public static final int Navigator_android_id;
    public static final int Navigator_android_label;
}
