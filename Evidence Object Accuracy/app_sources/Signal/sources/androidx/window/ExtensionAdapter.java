package androidx.window;

import android.app.Activity;
import android.graphics.Rect;
import androidx.window.FoldingFeature;
import androidx.window.extensions.ExtensionDisplayFeature;
import androidx.window.extensions.ExtensionFoldingFeature;
import androidx.window.extensions.ExtensionWindowLayoutInfo;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ExtensionAdapter.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u0005\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Landroidx/window/ExtensionAdapter;", "", "windowMetricsCalculator", "Landroidx/window/WindowMetricsCalculator;", "(Landroidx/window/WindowMetricsCalculator;)V", "translate", "Landroidx/window/DisplayFeature;", "activity", "Landroid/app/Activity;", "displayFeature", "Landroidx/window/extensions/ExtensionDisplayFeature;", "Landroidx/window/WindowLayoutInfo;", "layoutInfo", "Landroidx/window/extensions/ExtensionWindowLayoutInfo;", "Companion", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes.dex */
public final class ExtensionAdapter {
    public static final Companion Companion = new Companion(null);
    private static final String TAG;
    private final WindowMetricsCalculator windowMetricsCalculator;

    public ExtensionAdapter(WindowMetricsCalculator windowMetricsCalculator) {
        Intrinsics.checkNotNullParameter(windowMetricsCalculator, "windowMetricsCalculator");
        this.windowMetricsCalculator = windowMetricsCalculator;
    }

    public final WindowLayoutInfo translate(Activity activity, ExtensionWindowLayoutInfo extensionWindowLayoutInfo) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        Intrinsics.checkNotNullParameter(extensionWindowLayoutInfo, "layoutInfo");
        List<ExtensionDisplayFeature> displayFeatures = extensionWindowLayoutInfo.getDisplayFeatures();
        Intrinsics.checkNotNullExpressionValue(displayFeatures, "layoutInfo.displayFeatures");
        ArrayList arrayList = new ArrayList();
        for (ExtensionDisplayFeature extensionDisplayFeature : displayFeatures) {
            Intrinsics.checkNotNullExpressionValue(extensionDisplayFeature, "it");
            DisplayFeature translate = translate(activity, extensionDisplayFeature);
            if (translate != null) {
                arrayList.add(translate);
            }
        }
        return new WindowLayoutInfo(arrayList);
    }

    public final DisplayFeature translate(Activity activity, ExtensionDisplayFeature extensionDisplayFeature) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        Intrinsics.checkNotNullParameter(extensionDisplayFeature, "displayFeature");
        if (!(extensionDisplayFeature instanceof ExtensionFoldingFeature)) {
            return null;
        }
        return Companion.translateFoldFeature$window_release(this.windowMetricsCalculator.computeCurrentWindowMetrics(activity).getBounds(), (ExtensionFoldingFeature) extensionDisplayFeature);
    }

    /* compiled from: ExtensionAdapter.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0002J\u0018\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\rH\u0002J\u001f\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\rH\u0000¢\u0006\u0002\b\u0010R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Landroidx/window/ExtensionAdapter$Companion;", "", "()V", "TAG", "", "hasMatchingDimension", "", "lhs", "Landroid/graphics/Rect;", "rhs", "isValid", "windowBounds", "feature", "Landroidx/window/extensions/ExtensionFoldingFeature;", "translateFoldFeature", "Landroidx/window/DisplayFeature;", "translateFoldFeature$window_release", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final DisplayFeature translateFoldFeature$window_release(Rect rect, ExtensionFoldingFeature extensionFoldingFeature) {
            FoldingFeature.Type type;
            FoldingFeature.State state;
            Intrinsics.checkNotNullParameter(rect, "windowBounds");
            Intrinsics.checkNotNullParameter(extensionFoldingFeature, "feature");
            if (!isValid(rect, extensionFoldingFeature)) {
                return null;
            }
            int type2 = extensionFoldingFeature.getType();
            if (type2 == 1) {
                type = FoldingFeature.Type.FOLD;
            } else if (type2 != 2) {
                return null;
            } else {
                type = FoldingFeature.Type.HINGE;
            }
            int state2 = extensionFoldingFeature.getState();
            if (state2 == 1) {
                state = FoldingFeature.State.FLAT;
            } else if (state2 != 2) {
                return null;
            } else {
                state = FoldingFeature.State.HALF_OPENED;
            }
            Rect bounds = extensionFoldingFeature.getBounds();
            Intrinsics.checkNotNullExpressionValue(bounds, "feature.bounds");
            return new FoldingFeature(new Bounds(bounds), type, state);
        }

        private final boolean isValid(Rect rect, ExtensionFoldingFeature extensionFoldingFeature) {
            if (extensionFoldingFeature.getBounds().width() == 0 && extensionFoldingFeature.getBounds().height() == 0) {
                return false;
            }
            if (extensionFoldingFeature.getType() == 1 && !extensionFoldingFeature.getBounds().isEmpty()) {
                return false;
            }
            if (extensionFoldingFeature.getType() != 1 && extensionFoldingFeature.getType() != 2) {
                return false;
            }
            Rect bounds = extensionFoldingFeature.getBounds();
            Intrinsics.checkNotNullExpressionValue(bounds, "feature.bounds");
            return hasMatchingDimension(bounds, rect);
        }

        private final boolean hasMatchingDimension(Rect rect, Rect rect2) {
            boolean z = rect.left == rect2.left && rect.right == rect2.right;
            boolean z2 = rect.top == rect2.top && rect.bottom == rect2.bottom;
            if (z || z2) {
                return true;
            }
            return false;
        }
    }
}
