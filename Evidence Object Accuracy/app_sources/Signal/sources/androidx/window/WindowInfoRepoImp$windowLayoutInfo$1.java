package androidx.window;

import androidx.core.util.Consumer;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsKt;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.channels.ProduceKt;
import kotlinx.coroutines.channels.ProducerScope;

/* compiled from: WindowInfoRepoImp.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0004\u001a\u00020\u0003*\u0010\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u00010\u0000H@"}, d2 = {"Lkotlinx/coroutines/channels/ProducerScope;", "Landroidx/window/WindowLayoutInfo;", "kotlin.jvm.PlatformType", "", "<anonymous>"}, k = 3, mv = {1, 5, 1})
@DebugMetadata(c = "androidx.window.WindowInfoRepoImp$windowLayoutInfo$1", f = "WindowInfoRepoImp.kt", l = {105}, m = "invokeSuspend")
/* loaded from: classes.dex */
final class WindowInfoRepoImp$windowLayoutInfo$1 extends SuspendLambda implements Function2<ProducerScope<? super WindowLayoutInfo>, Continuation<? super Unit>, Object> {
    private /* synthetic */ Object L$0;
    int label;
    final /* synthetic */ WindowInfoRepoImp this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WindowInfoRepoImp$windowLayoutInfo$1(WindowInfoRepoImp windowInfoRepoImp, Continuation<? super WindowInfoRepoImp$windowLayoutInfo$1> continuation) {
        super(2, continuation);
        this.this$0 = windowInfoRepoImp;
    }

    @Override // kotlin.coroutines.jvm.internal.BaseContinuationImpl
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        WindowInfoRepoImp$windowLayoutInfo$1 windowInfoRepoImp$windowLayoutInfo$1 = new WindowInfoRepoImp$windowLayoutInfo$1(this.this$0, continuation);
        windowInfoRepoImp$windowLayoutInfo$1.L$0 = obj;
        return windowInfoRepoImp$windowLayoutInfo$1;
    }

    public final Object invoke(ProducerScope<? super WindowLayoutInfo> producerScope, Continuation<? super Unit> continuation) {
        return ((WindowInfoRepoImp$windowLayoutInfo$1) create(producerScope, continuation)).invokeSuspend(Unit.INSTANCE);
    }

    public final Object invokeSuspend(Object obj) {
        Object obj2 = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            ResultKt.throwOnFailure(obj);
            ProducerScope producerScope = (ProducerScope) this.L$0;
            final WindowInfoRepoImp$windowLayoutInfo$1$callback$1 windowInfoRepoImp$windowLayoutInfo$1$callback$1 = new Consumer(producerScope) { // from class: androidx.window.WindowInfoRepoImp$windowLayoutInfo$1$callback$1
                final /* synthetic */ ProducerScope<WindowLayoutInfo> $$this$callbackFlow;

                /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: kotlinx.coroutines.channels.ProducerScope<? super androidx.window.WindowLayoutInfo> */
                /* JADX WARN: Multi-variable type inference failed */
                /* access modifiers changed from: package-private */
                {
                    this.$$this$callbackFlow = r1;
                }

                public final void accept(WindowLayoutInfo windowLayoutInfo) {
                    this.$$this$callbackFlow.m176trySendJP2dKIU(windowLayoutInfo);
                }
            };
            this.this$0.windowBackend.registerLayoutChangeCallback(this.this$0.activity, AnonymousClass1.INSTANCE, windowInfoRepoImp$windowLayoutInfo$1$callback$1);
            final WindowInfoRepoImp windowInfoRepoImp = this.this$0;
            AnonymousClass2 r3 = new Function0<Unit>() { // from class: androidx.window.WindowInfoRepoImp$windowLayoutInfo$1.2
                public final void invoke() {
                    windowInfoRepoImp.windowBackend.unregisterLayoutChangeCallback(windowInfoRepoImp$windowLayoutInfo$1$callback$1);
                }
            };
            this.label = 1;
            if (ProduceKt.awaitClose(producerScope, r3, this) == obj2) {
                return obj2;
            }
        } else if (i == 1) {
            ResultKt.throwOnFailure(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Unit.INSTANCE;
    }
}
