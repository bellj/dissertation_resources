package androidx.window;

import androidx.window.ExtensionWindowBackend;
import kotlin.Metadata;

/* compiled from: ExtensionWindowBackend.kt */
/* access modifiers changed from: package-private */
@Metadata(bv = {}, d1 = {"\u0000\u0006\n\u0002\u0010\u0002\n\u0000\u0010\u0001\u001a\u00020\u0000H\n"}, d2 = {"", "<anonymous>"}, k = 3, mv = {1, 5, 1})
/* loaded from: classes.dex */
public final class ExtensionWindowBackend$WindowLayoutChangeCallbackWrapper$accept$1 implements Runnable {
    final /* synthetic */ WindowLayoutInfo $newLayoutInfo;
    final /* synthetic */ ExtensionWindowBackend.WindowLayoutChangeCallbackWrapper this$0;

    public ExtensionWindowBackend$WindowLayoutChangeCallbackWrapper$accept$1(ExtensionWindowBackend.WindowLayoutChangeCallbackWrapper windowLayoutChangeCallbackWrapper, WindowLayoutInfo windowLayoutInfo) {
        this.this$0 = windowLayoutChangeCallbackWrapper;
        this.$newLayoutInfo = windowLayoutInfo;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.this$0.getCallback().accept(this.$newLayoutInfo);
    }
}
