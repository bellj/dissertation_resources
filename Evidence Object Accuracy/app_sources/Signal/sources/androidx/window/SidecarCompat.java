package androidx.window;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import androidx.window.ExtensionInterfaceCompat;
import androidx.window.sidecar.SidecarDeviceState;
import androidx.window.sidecar.SidecarInterface;
import androidx.window.sidecar.SidecarProvider;
import androidx.window.sidecar.SidecarWindowLayoutInfo;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SidecarCompat.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0000\u0018\u0000 \u001d2\u00020\u0001:\u0005\u001d\u001e\u001f !B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u001b\b\u0007\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0011H\u0007J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0014\u001a\u00020\u0011H\u0016J\u0010\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0014\u001a\u00020\u0011H\u0016J\u0016\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0011J\u0010\u0010\u001a\u001a\u00020\u00162\u0006\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0017R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\""}, d2 = {"Landroidx/window/SidecarCompat;", "Landroidx/window/ExtensionInterfaceCompat;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "sidecar", "Landroidx/window/sidecar/SidecarInterface;", "sidecarAdapter", "Landroidx/window/SidecarAdapter;", "(Landroidx/window/sidecar/SidecarInterface;Landroidx/window/SidecarAdapter;)V", "extensionCallback", "Landroidx/window/ExtensionInterfaceCompat$ExtensionCallbackInterface;", "getSidecar", "()Landroidx/window/sidecar/SidecarInterface;", "windowListenerRegisteredContexts", "", "Landroid/os/IBinder;", "Landroid/app/Activity;", "getWindowLayoutInfo", "Landroidx/window/WindowLayoutInfo;", "activity", "onWindowLayoutChangeListenerAdded", "", "onWindowLayoutChangeListenerRemoved", "register", "windowToken", "setExtensionCallback", "validateExtensionInterface", "", "Companion", "DistinctElementCallback", "DistinctSidecarElementCallback", "FirstAttachAdapter", "TranslatingCallback", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes.dex */
public final class SidecarCompat implements ExtensionInterfaceCompat {
    public static final Companion Companion = new Companion(null);
    private static final String TAG;
    private ExtensionInterfaceCompat.ExtensionCallbackInterface extensionCallback;
    private final SidecarInterface sidecar;
    private final SidecarAdapter sidecarAdapter;
    private final Map<IBinder, Activity> windowListenerRegisteredContexts;

    public SidecarCompat(SidecarInterface sidecarInterface, SidecarAdapter sidecarAdapter) {
        Intrinsics.checkNotNullParameter(sidecarAdapter, "sidecarAdapter");
        this.sidecar = sidecarInterface;
        this.sidecarAdapter = sidecarAdapter;
        this.windowListenerRegisteredContexts = new LinkedHashMap();
    }

    public final SidecarInterface getSidecar() {
        return this.sidecar;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public SidecarCompat(Context context) {
        this(SidecarProvider.getSidecarImpl(context), new SidecarAdapter());
        Intrinsics.checkNotNullParameter(context, "context");
    }

    @Override // androidx.window.ExtensionInterfaceCompat
    public void setExtensionCallback(ExtensionInterfaceCompat.ExtensionCallbackInterface extensionCallbackInterface) {
        Intrinsics.checkNotNullParameter(extensionCallbackInterface, "extensionCallback");
        this.extensionCallback = new DistinctElementCallback(extensionCallbackInterface);
        SidecarInterface sidecarInterface = this.sidecar;
        if (sidecarInterface != null) {
            sidecarInterface.setSidecarCallback(new DistinctSidecarElementCallback(this.sidecarAdapter, new TranslatingCallback(this)));
        }
    }

    public final WindowLayoutInfo getWindowLayoutInfo(Activity activity) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        IBinder activityWindowToken = ActivityExtKt.getActivityWindowToken(activity);
        if (activityWindowToken == null) {
            return new WindowLayoutInfo(CollectionsKt__CollectionsKt.emptyList());
        }
        SidecarInterface sidecarInterface = this.sidecar;
        SidecarDeviceState sidecarDeviceState = null;
        SidecarWindowLayoutInfo windowLayoutInfo = sidecarInterface == null ? null : sidecarInterface.getWindowLayoutInfo(activityWindowToken);
        SidecarAdapter sidecarAdapter = this.sidecarAdapter;
        SidecarInterface sidecarInterface2 = this.sidecar;
        if (sidecarInterface2 != null) {
            sidecarDeviceState = sidecarInterface2.getDeviceState();
        }
        if (sidecarDeviceState == null) {
            sidecarDeviceState = new SidecarDeviceState();
        }
        return sidecarAdapter.translate(windowLayoutInfo, sidecarDeviceState);
    }

    @Override // androidx.window.ExtensionInterfaceCompat
    public void onWindowLayoutChangeListenerAdded(Activity activity) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        IBinder activityWindowToken = ActivityExtKt.getActivityWindowToken(activity);
        if (activityWindowToken != null) {
            register(activityWindowToken, activity);
            return;
        }
        activity.getWindow().getDecorView().addOnAttachStateChangeListener(new FirstAttachAdapter(this, activity));
    }

    public final void register(IBinder iBinder, Activity activity) {
        SidecarInterface sidecarInterface;
        Intrinsics.checkNotNullParameter(iBinder, "windowToken");
        Intrinsics.checkNotNullParameter(activity, "activity");
        this.windowListenerRegisteredContexts.put(iBinder, activity);
        SidecarInterface sidecarInterface2 = this.sidecar;
        if (sidecarInterface2 != null) {
            sidecarInterface2.onWindowLayoutChangeListenerAdded(iBinder);
        }
        if (this.windowListenerRegisteredContexts.size() == 1 && (sidecarInterface = this.sidecar) != null) {
            sidecarInterface.onDeviceStateListenersChanged(false);
        }
        ExtensionInterfaceCompat.ExtensionCallbackInterface extensionCallbackInterface = this.extensionCallback;
        if (extensionCallbackInterface != null) {
            extensionCallbackInterface.onWindowLayoutChanged(activity, getWindowLayoutInfo(activity));
        }
    }

    @Override // androidx.window.ExtensionInterfaceCompat
    public void onWindowLayoutChangeListenerRemoved(Activity activity) {
        SidecarInterface sidecarInterface;
        Intrinsics.checkNotNullParameter(activity, "activity");
        IBinder activityWindowToken = ActivityExtKt.getActivityWindowToken(activity);
        if (activityWindowToken != null) {
            SidecarInterface sidecarInterface2 = this.sidecar;
            if (sidecarInterface2 != null) {
                sidecarInterface2.onWindowLayoutChangeListenerRemoved(activityWindowToken);
            }
            boolean z = this.windowListenerRegisteredContexts.size() == 1;
            this.windowListenerRegisteredContexts.remove(activityWindowToken);
            if (z && (sidecarInterface = this.sidecar) != null) {
                sidecarInterface.onDeviceStateListenersChanged(true);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0021 A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002d A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005a A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0066 A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0084 A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0090 A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00ad A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00b9 A[Catch: all -> 0x01a1, TRY_LEAVE, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0171 A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x017d A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0189 A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0195 A[Catch: all -> 0x01a1, TryCatch #0 {all -> 0x01a1, blocks: (B:3:0x0002, B:6:0x0009, B:9:0x0010, B:12:0x0021, B:13:0x0025, B:15:0x002d, B:18:0x0032, B:19:0x0035, B:22:0x003a, B:23:0x003d, B:26:0x0043, B:29:0x004a, B:32:0x005a, B:33:0x005e, B:35:0x0066, B:38:0x006c, B:41:0x0073, B:44:0x0084, B:45:0x0088, B:47:0x0090, B:50:0x0096, B:53:0x009d, B:56:0x00ad, B:57:0x00b1, B:59:0x00b9, B:61:0x00bf, B:62:0x00c2, B:64:0x00ee, B:66:0x00f6, B:67:0x0112, B:68:0x0116, B:70:0x0146, B:73:0x014f, B:74:0x0156, B:75:0x0157, B:76:0x015f, B:77:0x0160, B:78:0x0167, B:79:0x0168, B:80:0x0170, B:81:0x0171, B:82:0x017c, B:83:0x017d, B:84:0x0188, B:85:0x0189, B:86:0x0194, B:87:0x0195, B:88:0x01a0), top: B:90:0x0002, inners: #1, #2 }] */
    @Override // androidx.window.ExtensionInterfaceCompat
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean validateExtensionInterface() {
        /*
        // Method dump skipped, instructions count: 419
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.window.SidecarCompat.validateExtensionInterface():boolean");
    }

    /* compiled from: SidecarCompat.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016R\u001c\u0010\u0007\u001a\u0010\u0012\f\u0012\n \t*\u0004\u0018\u00010\u00050\u00050\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Landroidx/window/SidecarCompat$FirstAttachAdapter;", "Landroid/view/View$OnAttachStateChangeListener;", "sidecarCompat", "Landroidx/window/SidecarCompat;", "activity", "Landroid/app/Activity;", "(Landroidx/window/SidecarCompat;Landroid/app/Activity;)V", "activityWeakReference", "Ljava/lang/ref/WeakReference;", "kotlin.jvm.PlatformType", "onViewAttachedToWindow", "", "view", "Landroid/view/View;", "onViewDetachedFromWindow", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes.dex */
    private static final class FirstAttachAdapter implements View.OnAttachStateChangeListener {
        private final WeakReference<Activity> activityWeakReference;
        private final SidecarCompat sidecarCompat;

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewDetachedFromWindow(View view) {
            Intrinsics.checkNotNullParameter(view, "view");
        }

        public FirstAttachAdapter(SidecarCompat sidecarCompat, Activity activity) {
            Intrinsics.checkNotNullParameter(sidecarCompat, "sidecarCompat");
            Intrinsics.checkNotNullParameter(activity, "activity");
            this.sidecarCompat = sidecarCompat;
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override // android.view.View.OnAttachStateChangeListener
        public void onViewAttachedToWindow(View view) {
            Intrinsics.checkNotNullParameter(view, "view");
            view.removeOnAttachStateChangeListener(this);
            Activity activity = this.activityWeakReference.get();
            IBinder activityWindowToken = ActivityExtKt.getActivityWindowToken(activity);
            if (activity != null && activityWindowToken != null) {
                this.sidecarCompat.register(activityWindowToken, activity);
            }
        }
    }

    /* compiled from: SidecarCompat.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0017J\u0018\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0017¨\u0006\f"}, d2 = {"Landroidx/window/SidecarCompat$TranslatingCallback;", "Landroidx/window/sidecar/SidecarInterface$SidecarCallback;", "(Landroidx/window/SidecarCompat;)V", "onDeviceStateChanged", "", "newDeviceState", "Landroidx/window/sidecar/SidecarDeviceState;", "onWindowLayoutChanged", "windowToken", "Landroid/os/IBinder;", "newLayout", "Landroidx/window/sidecar/SidecarWindowLayoutInfo;", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes.dex */
    public final class TranslatingCallback implements SidecarInterface.SidecarCallback {
        final /* synthetic */ SidecarCompat this$0;

        public TranslatingCallback(SidecarCompat sidecarCompat) {
            Intrinsics.checkNotNullParameter(sidecarCompat, "this$0");
            this.this$0 = sidecarCompat;
        }

        public void onDeviceStateChanged(SidecarDeviceState sidecarDeviceState) {
            SidecarInterface sidecar;
            Intrinsics.checkNotNullParameter(sidecarDeviceState, "newDeviceState");
            Collection<Activity> values = this.this$0.windowListenerRegisteredContexts.values();
            SidecarCompat sidecarCompat = this.this$0;
            for (Activity activity : values) {
                IBinder activityWindowToken = ActivityExtKt.getActivityWindowToken(activity);
                SidecarWindowLayoutInfo sidecarWindowLayoutInfo = null;
                if (!(activityWindowToken == null || (sidecar = sidecarCompat.getSidecar()) == null)) {
                    sidecarWindowLayoutInfo = sidecar.getWindowLayoutInfo(activityWindowToken);
                }
                ExtensionInterfaceCompat.ExtensionCallbackInterface extensionCallbackInterface = sidecarCompat.extensionCallback;
                if (extensionCallbackInterface != null) {
                    extensionCallbackInterface.onWindowLayoutChanged(activity, sidecarCompat.sidecarAdapter.translate(sidecarWindowLayoutInfo, sidecarDeviceState));
                }
            }
        }

        public void onWindowLayoutChanged(IBinder iBinder, SidecarWindowLayoutInfo sidecarWindowLayoutInfo) {
            Intrinsics.checkNotNullParameter(iBinder, "windowToken");
            Intrinsics.checkNotNullParameter(sidecarWindowLayoutInfo, "newLayout");
            Activity activity = (Activity) this.this$0.windowListenerRegisteredContexts.get(iBinder);
            if (activity == null) {
                Log.w(SidecarCompat.TAG, "Unable to resolve activity from window token. Missing a call to #onWindowLayoutChangeListenerAdded()?");
                return;
            }
            SidecarAdapter sidecarAdapter = this.this$0.sidecarAdapter;
            SidecarInterface sidecar = this.this$0.getSidecar();
            SidecarDeviceState deviceState = sidecar == null ? null : sidecar.getDeviceState();
            if (deviceState == null) {
                deviceState = new SidecarDeviceState();
            }
            WindowLayoutInfo translate = sidecarAdapter.translate(sidecarWindowLayoutInfo, deviceState);
            ExtensionInterfaceCompat.ExtensionCallbackInterface extensionCallbackInterface = this.this$0.extensionCallback;
            if (extensionCallbackInterface != null) {
                extensionCallbackInterface.onWindowLayoutChanged(activity, translate);
            }
        }
    }

    /* compiled from: SidecarCompat.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0002\u0010\u0003J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u0007H\u0016R\u001c\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00058\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Landroidx/window/SidecarCompat$DistinctElementCallback;", "Landroidx/window/ExtensionInterfaceCompat$ExtensionCallbackInterface;", "callbackInterface", "(Landroidx/window/ExtensionInterfaceCompat$ExtensionCallbackInterface;)V", "activityWindowLayoutInfo", "Ljava/util/WeakHashMap;", "Landroid/app/Activity;", "Landroidx/window/WindowLayoutInfo;", "lock", "Ljava/util/concurrent/locks/ReentrantLock;", "onWindowLayoutChanged", "", "activity", "newLayout", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes.dex */
    private static final class DistinctElementCallback implements ExtensionInterfaceCompat.ExtensionCallbackInterface {
        private final WeakHashMap<Activity, WindowLayoutInfo> activityWindowLayoutInfo = new WeakHashMap<>();
        private final ExtensionInterfaceCompat.ExtensionCallbackInterface callbackInterface;
        private final ReentrantLock lock = new ReentrantLock();

        public DistinctElementCallback(ExtensionInterfaceCompat.ExtensionCallbackInterface extensionCallbackInterface) {
            Intrinsics.checkNotNullParameter(extensionCallbackInterface, "callbackInterface");
            this.callbackInterface = extensionCallbackInterface;
        }

        @Override // androidx.window.ExtensionInterfaceCompat.ExtensionCallbackInterface
        public void onWindowLayoutChanged(Activity activity, WindowLayoutInfo windowLayoutInfo) {
            Intrinsics.checkNotNullParameter(activity, "activity");
            Intrinsics.checkNotNullParameter(windowLayoutInfo, "newLayout");
            ReentrantLock reentrantLock = this.lock;
            reentrantLock.lock();
            try {
                if (!Intrinsics.areEqual(windowLayoutInfo, this.activityWindowLayoutInfo.get(activity))) {
                    this.activityWindowLayoutInfo.put(activity, windowLayoutInfo);
                    reentrantLock.unlock();
                    this.callbackInterface.onWindowLayoutChanged(activity, windowLayoutInfo);
                }
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    /* compiled from: SidecarCompat.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0001¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0007H\u0016J\u0018\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b8\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Landroidx/window/SidecarCompat$DistinctSidecarElementCallback;", "Landroidx/window/sidecar/SidecarInterface$SidecarCallback;", "sidecarAdapter", "Landroidx/window/SidecarAdapter;", "callbackInterface", "(Landroidx/window/SidecarAdapter;Landroidx/window/sidecar/SidecarInterface$SidecarCallback;)V", "lastDeviceState", "Landroidx/window/sidecar/SidecarDeviceState;", "lock", "Ljava/util/concurrent/locks/ReentrantLock;", "mActivityWindowLayoutInfo", "Ljava/util/WeakHashMap;", "Landroid/os/IBinder;", "Landroidx/window/sidecar/SidecarWindowLayoutInfo;", "onDeviceStateChanged", "", "newDeviceState", "onWindowLayoutChanged", "token", "newLayout", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes.dex */
    private static final class DistinctSidecarElementCallback implements SidecarInterface.SidecarCallback {
        private final SidecarInterface.SidecarCallback callbackInterface;
        private SidecarDeviceState lastDeviceState;
        private final ReentrantLock lock = new ReentrantLock();
        private final WeakHashMap<IBinder, SidecarWindowLayoutInfo> mActivityWindowLayoutInfo = new WeakHashMap<>();
        private final SidecarAdapter sidecarAdapter;

        public DistinctSidecarElementCallback(SidecarAdapter sidecarAdapter, SidecarInterface.SidecarCallback sidecarCallback) {
            Intrinsics.checkNotNullParameter(sidecarAdapter, "sidecarAdapter");
            Intrinsics.checkNotNullParameter(sidecarCallback, "callbackInterface");
            this.sidecarAdapter = sidecarAdapter;
            this.callbackInterface = sidecarCallback;
        }

        public void onDeviceStateChanged(SidecarDeviceState sidecarDeviceState) {
            Intrinsics.checkNotNullParameter(sidecarDeviceState, "newDeviceState");
            ReentrantLock reentrantLock = this.lock;
            reentrantLock.lock();
            try {
                if (!this.sidecarAdapter.isEqualSidecarDeviceState(this.lastDeviceState, sidecarDeviceState)) {
                    this.lastDeviceState = sidecarDeviceState;
                    this.callbackInterface.onDeviceStateChanged(sidecarDeviceState);
                    Unit unit = Unit.INSTANCE;
                }
            } finally {
                reentrantLock.unlock();
            }
        }

        public void onWindowLayoutChanged(IBinder iBinder, SidecarWindowLayoutInfo sidecarWindowLayoutInfo) {
            Intrinsics.checkNotNullParameter(iBinder, "token");
            Intrinsics.checkNotNullParameter(sidecarWindowLayoutInfo, "newLayout");
            synchronized (this.lock) {
                if (!this.sidecarAdapter.isEqualSidecarWindowLayoutInfo(this.mActivityWindowLayoutInfo.get(iBinder), sidecarWindowLayoutInfo)) {
                    this.mActivityWindowLayoutInfo.put(iBinder, sidecarWindowLayoutInfo);
                    this.callbackInterface.onWindowLayoutChanged(iBinder, sidecarWindowLayoutInfo);
                }
            }
        }
    }

    /* compiled from: SidecarCompat.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Landroidx/window/SidecarCompat$Companion;", "", "()V", "TAG", "", "sidecarVersion", "Landroidx/window/Version;", "getSidecarVersion", "()Landroidx/window/Version;", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Version getSidecarVersion() {
            try {
                String apiVersion = SidecarProvider.getApiVersion();
                if (!TextUtils.isEmpty(apiVersion)) {
                    return Version.Companion.parse(apiVersion);
                }
                return null;
            } catch (NoClassDefFoundError | UnsupportedOperationException unused) {
                return null;
            }
        }
    }
}
