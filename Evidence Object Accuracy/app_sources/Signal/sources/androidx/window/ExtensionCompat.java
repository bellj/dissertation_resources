package androidx.window;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import androidx.window.ExtensionInterfaceCompat;
import androidx.window.extensions.ExtensionInterface;
import androidx.window.extensions.ExtensionProvider;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ExtensionCompat.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0019\b\u0007\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0015H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0017"}, d2 = {"Landroidx/window/ExtensionCompat;", "Landroidx/window/ExtensionInterfaceCompat;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "windowExtension", "Landroidx/window/extensions/ExtensionInterface;", "adapter", "Landroidx/window/ExtensionAdapter;", "(Landroidx/window/extensions/ExtensionInterface;Landroidx/window/ExtensionAdapter;)V", "getWindowExtension", "()Landroidx/window/extensions/ExtensionInterface;", "onWindowLayoutChangeListenerAdded", "", "activity", "Landroid/app/Activity;", "onWindowLayoutChangeListenerRemoved", "setExtensionCallback", "extensionCallback", "Landroidx/window/ExtensionInterfaceCompat$ExtensionCallbackInterface;", "validateExtensionInterface", "", "Companion", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes.dex */
public final class ExtensionCompat implements ExtensionInterfaceCompat {
    public static final Companion Companion = new Companion(null);
    public static final boolean DEBUG;
    private static final String TAG;
    private final ExtensionAdapter adapter;
    private final ExtensionInterface windowExtension;

    public ExtensionCompat(ExtensionInterface extensionInterface, ExtensionAdapter extensionAdapter) {
        Intrinsics.checkNotNullParameter(extensionAdapter, "adapter");
        this.windowExtension = extensionInterface;
        this.adapter = extensionAdapter;
    }

    public final ExtensionInterface getWindowExtension() {
        return this.windowExtension;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ExtensionCompat(Context context) {
        this(ExtensionProvider.getExtensionImpl(context), new ExtensionAdapter(WindowMetricsCalculatorCompat.INSTANCE));
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.windowExtension == null) {
            throw new IllegalArgumentException("Extension provider returned null".toString());
        }
    }

    @Override // androidx.window.ExtensionInterfaceCompat
    public void setExtensionCallback(ExtensionInterfaceCompat.ExtensionCallbackInterface extensionCallbackInterface) {
        Intrinsics.checkNotNullParameter(extensionCallbackInterface, "extensionCallback");
        ExtensionInterface.ExtensionCallback extensionTranslatingCallback = new ExtensionTranslatingCallback(extensionCallbackInterface, this.adapter);
        ExtensionInterface extensionInterface = this.windowExtension;
        if (extensionInterface != null) {
            extensionInterface.setExtensionCallback(extensionTranslatingCallback);
        }
    }

    @Override // androidx.window.ExtensionInterfaceCompat
    public void onWindowLayoutChangeListenerAdded(Activity activity) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        ExtensionInterface extensionInterface = this.windowExtension;
        if (extensionInterface != null) {
            extensionInterface.onWindowLayoutChangeListenerAdded(activity);
        }
    }

    @Override // androidx.window.ExtensionInterfaceCompat
    public void onWindowLayoutChangeListenerRemoved(Activity activity) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        ExtensionInterface extensionInterface = this.windowExtension;
        if (extensionInterface != null) {
            extensionInterface.onWindowLayoutChangeListenerRemoved(activity);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0023 A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002f A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004b A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0057 A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0072 A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007e A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ae A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ba A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c6 A[Catch: all -> 0x00d2, TryCatch #0 {all -> 0x00d2, blocks: (B:3:0x0004, B:6:0x000b, B:9:0x0012, B:12:0x0023, B:13:0x0027, B:15:0x002f, B:18:0x0035, B:21:0x003c, B:24:0x004b, B:25:0x004f, B:27:0x0057, B:30:0x005d, B:33:0x0064, B:36:0x0072, B:37:0x0076, B:39:0x007e, B:40:0x00ae, B:41:0x00b9, B:42:0x00ba, B:43:0x00c5, B:44:0x00c6, B:45:0x00d1), top: B:47:0x0004 }] */
    @Override // androidx.window.ExtensionInterfaceCompat
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean validateExtensionInterface() {
        /*
            r8 = this;
            java.lang.Class<android.app.Activity> r0 = android.app.Activity.class
            r1 = 1
            r2 = 0
            androidx.window.extensions.ExtensionInterface r3 = r8.windowExtension     // Catch: all -> 0x00d2
            r4 = 0
            if (r3 != 0) goto L_0x000b
        L_0x0009:
            r3 = r4
            goto L_0x001f
        L_0x000b:
            java.lang.Class r3 = r3.getClass()     // Catch: all -> 0x00d2
            if (r3 != 0) goto L_0x0012
            goto L_0x0009
        L_0x0012:
            java.lang.String r5 = "setExtensionCallback"
            java.lang.Class[] r6 = new java.lang.Class[r1]     // Catch: all -> 0x00d2
            java.lang.Class<androidx.window.extensions.ExtensionInterface$ExtensionCallback> r7 = androidx.window.extensions.ExtensionInterface.ExtensionCallback.class
            r6[r2] = r7     // Catch: all -> 0x00d2
            java.lang.reflect.Method r3 = r3.getMethod(r5, r6)     // Catch: all -> 0x00d2
        L_0x001f:
            if (r3 != 0) goto L_0x0023
            r3 = r4
            goto L_0x0027
        L_0x0023:
            java.lang.Class r3 = r3.getReturnType()     // Catch: all -> 0x00d2
        L_0x0027:
            java.lang.Class r5 = java.lang.Void.TYPE     // Catch: all -> 0x00d2
            boolean r5 = kotlin.jvm.internal.Intrinsics.areEqual(r3, r5)     // Catch: all -> 0x00d2
            if (r5 == 0) goto L_0x00c6
            androidx.window.extensions.ExtensionInterface r3 = r8.windowExtension     // Catch: all -> 0x00d2
            if (r3 != 0) goto L_0x0035
        L_0x0033:
            r3 = r4
            goto L_0x0047
        L_0x0035:
            java.lang.Class r3 = r3.getClass()     // Catch: all -> 0x00d2
            if (r3 != 0) goto L_0x003c
            goto L_0x0033
        L_0x003c:
            java.lang.String r5 = "onWindowLayoutChangeListenerAdded"
            java.lang.Class[] r6 = new java.lang.Class[r1]     // Catch: all -> 0x00d2
            r6[r2] = r0     // Catch: all -> 0x00d2
            java.lang.reflect.Method r3 = r3.getMethod(r5, r6)     // Catch: all -> 0x00d2
        L_0x0047:
            if (r3 != 0) goto L_0x004b
            r3 = r4
            goto L_0x004f
        L_0x004b:
            java.lang.Class r3 = r3.getReturnType()     // Catch: all -> 0x00d2
        L_0x004f:
            java.lang.Class r5 = java.lang.Void.TYPE     // Catch: all -> 0x00d2
            boolean r5 = kotlin.jvm.internal.Intrinsics.areEqual(r3, r5)     // Catch: all -> 0x00d2
            if (r5 == 0) goto L_0x00ba
            androidx.window.extensions.ExtensionInterface r3 = r8.windowExtension     // Catch: all -> 0x00d2
            if (r3 != 0) goto L_0x005d
        L_0x005b:
            r0 = r4
            goto L_0x006f
        L_0x005d:
            java.lang.Class r3 = r3.getClass()     // Catch: all -> 0x00d2
            if (r3 != 0) goto L_0x0064
            goto L_0x005b
        L_0x0064:
            java.lang.String r5 = "onWindowLayoutChangeListenerRemoved"
            java.lang.Class[] r6 = new java.lang.Class[r1]     // Catch: all -> 0x00d2
            r6[r2] = r0     // Catch: all -> 0x00d2
            java.lang.reflect.Method r0 = r3.getMethod(r5, r6)     // Catch: all -> 0x00d2
        L_0x006f:
            if (r0 != 0) goto L_0x0072
            goto L_0x0076
        L_0x0072:
            java.lang.Class r4 = r0.getReturnType()     // Catch: all -> 0x00d2
        L_0x0076:
            java.lang.Class r0 = java.lang.Void.TYPE     // Catch: all -> 0x00d2
            boolean r0 = kotlin.jvm.internal.Intrinsics.areEqual(r4, r0)     // Catch: all -> 0x00d2
            if (r0 == 0) goto L_0x00ae
            androidx.window.extensions.ExtensionFoldingFeature r0 = new androidx.window.extensions.ExtensionFoldingFeature     // Catch: all -> 0x00d2
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch: all -> 0x00d2
            r4 = 100
            r3.<init>(r2, r2, r4, r2)     // Catch: all -> 0x00d2
            r0.<init>(r3, r1, r1)     // Catch: all -> 0x00d2
            android.graphics.Rect r3 = r0.getBounds()     // Catch: all -> 0x00d2
            java.lang.String r4 = "displayFoldingFeature.bounds"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r3, r4)     // Catch: all -> 0x00d2
            r0.getState()     // Catch: all -> 0x00d2
            r0.getType()     // Catch: all -> 0x00d2
            androidx.window.extensions.ExtensionWindowLayoutInfo r0 = new androidx.window.extensions.ExtensionWindowLayoutInfo     // Catch: all -> 0x00d2
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch: all -> 0x00d2
            r3.<init>()     // Catch: all -> 0x00d2
            r0.<init>(r3)     // Catch: all -> 0x00d2
            java.util.List r0 = r0.getDisplayFeatures()     // Catch: all -> 0x00d2
            java.lang.String r3 = "windowLayoutInfo.displayFeatures"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r3)     // Catch: all -> 0x00d2
            goto L_0x00d3
        L_0x00ae:
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException     // Catch: all -> 0x00d2
            java.lang.String r1 = "Illegal return type for 'onWindowLayoutChangeListenerRemoved': "
            java.lang.String r1 = kotlin.jvm.internal.Intrinsics.stringPlus(r1, r4)     // Catch: all -> 0x00d2
            r0.<init>(r1)     // Catch: all -> 0x00d2
            throw r0     // Catch: all -> 0x00d2
        L_0x00ba:
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException     // Catch: all -> 0x00d2
            java.lang.String r1 = "Illegal return type for 'onWindowLayoutChangeListenerAdded': "
            java.lang.String r1 = kotlin.jvm.internal.Intrinsics.stringPlus(r1, r3)     // Catch: all -> 0x00d2
            r0.<init>(r1)     // Catch: all -> 0x00d2
            throw r0     // Catch: all -> 0x00d2
        L_0x00c6:
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException     // Catch: all -> 0x00d2
            java.lang.String r1 = "Illegal return type for 'setExtensionCallback': "
            java.lang.String r1 = kotlin.jvm.internal.Intrinsics.stringPlus(r1, r3)     // Catch: all -> 0x00d2
            r0.<init>(r1)     // Catch: all -> 0x00d2
            throw r0     // Catch: all -> 0x00d2
        L_0x00d2:
            r1 = 0
        L_0x00d3:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.window.ExtensionCompat.validateExtensionInterface():boolean");
    }

    /* compiled from: ExtensionCompat.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b8F¢\u0006\u0006\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Landroidx/window/ExtensionCompat$Companion;", "", "()V", "DEBUG", "", "TAG", "", "extensionVersion", "Landroidx/window/Version;", "getExtensionVersion", "()Landroidx/window/Version;", "window_release"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Version getExtensionVersion() {
            try {
                String apiVersion = ExtensionProvider.getApiVersion();
                if (!TextUtils.isEmpty(apiVersion)) {
                    return Version.Companion.parse(apiVersion);
                }
                return null;
            } catch (NoClassDefFoundError | UnsupportedOperationException unused) {
                return null;
            }
        }
    }
}
