package androidx.biometric;

/* loaded from: classes.dex */
public final class R$array {
    public static final int assume_strong_biometrics_models;
    public static final int crypto_fingerprint_fallback_prefixes;
    public static final int crypto_fingerprint_fallback_vendors;
    public static final int delay_showing_prompt_models;
    public static final int hide_fingerprint_instantly_prefixes;
}
