package androidx.biometric;

/* loaded from: classes.dex */
public final class R$string {
    public static final int confirm_device_credential_password;
    public static final int default_error_msg;
    public static final int fingerprint_dialog_touch_sensor;
    public static final int fingerprint_error_hw_not_available;
    public static final int fingerprint_error_hw_not_present;
    public static final int fingerprint_error_lockout;
    public static final int fingerprint_error_no_fingerprints;
    public static final int fingerprint_error_user_canceled;
    public static final int fingerprint_not_recognized;
    public static final int generic_error_no_device_credential;
    public static final int generic_error_no_keyguard;
    public static final int generic_error_user_canceled;
}
