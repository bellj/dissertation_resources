package androidx.activity;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface Cancellable {
    void cancel();
}
