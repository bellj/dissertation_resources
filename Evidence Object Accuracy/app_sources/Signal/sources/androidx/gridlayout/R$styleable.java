package androidx.gridlayout;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int[] CoordinatorLayout = {R.attr.keylines, R.attr.statusBarBackground};
    public static final int[] CoordinatorLayout_Layout = {16842931, R.attr.layout_anchor, R.attr.layout_anchorGravity, R.attr.layout_behavior, R.attr.layout_dodgeInsetEdges, R.attr.layout_insetEdge, R.attr.layout_keyline};
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
    public static final int[] GridLayout = {R.attr.alignmentMode, R.attr.columnCount, R.attr.columnOrderPreserved, R.attr.orientation, R.attr.rowCount, R.attr.rowOrderPreserved, R.attr.useDefaultMargins};
    public static final int[] GridLayout_Layout = {16842996, 16842997, 16842998, 16842999, 16843000, 16843001, 16843002, R.attr.layout_column, R.attr.layout_columnSpan, R.attr.layout_columnWeight, R.attr.layout_gravity, R.attr.layout_row, R.attr.layout_rowSpan, R.attr.layout_rowWeight};
    public static final int GridLayout_Layout_android_layout_margin;
    public static final int GridLayout_Layout_android_layout_marginBottom;
    public static final int GridLayout_Layout_android_layout_marginLeft;
    public static final int GridLayout_Layout_android_layout_marginRight;
    public static final int GridLayout_Layout_android_layout_marginTop;
    public static final int GridLayout_Layout_layout_column;
    public static final int GridLayout_Layout_layout_columnSpan;
    public static final int GridLayout_Layout_layout_columnWeight;
    public static final int GridLayout_Layout_layout_gravity;
    public static final int GridLayout_Layout_layout_row;
    public static final int GridLayout_Layout_layout_rowSpan;
    public static final int GridLayout_Layout_layout_rowWeight;
    public static final int GridLayout_alignmentMode;
    public static final int GridLayout_columnCount;
    public static final int GridLayout_columnOrderPreserved;
    public static final int GridLayout_orientation;
    public static final int GridLayout_rowCount;
    public static final int GridLayout_rowOrderPreserved;
    public static final int GridLayout_useDefaultMargins;
}
