package androidx.sqlite.db;

import android.content.ContentValues;
import android.database.Cursor;
import java.io.Closeable;

/* loaded from: classes.dex */
public interface SupportSQLiteDatabase extends Closeable {
    void beginTransaction();

    int delete(String str, String str2, Object[] objArr);

    void endTransaction();

    Cursor query(SupportSQLiteQuery supportSQLiteQuery);

    Cursor query(String str);

    Cursor query(String str, Object[] objArr);

    void setTransactionSuccessful();

    int update(String str, int i, ContentValues contentValues, String str2, Object[] objArr);
}
