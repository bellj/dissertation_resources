package androidx.coordinatorlayout;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int[] CoordinatorLayout = {R.attr.keylines, R.attr.statusBarBackground};
    public static final int[] CoordinatorLayout_Layout = {16842931, R.attr.layout_anchor, R.attr.layout_anchorGravity, R.attr.layout_behavior, R.attr.layout_dodgeInsetEdges, R.attr.layout_insetEdge, R.attr.layout_keyline};
    public static final int CoordinatorLayout_Layout_android_layout_gravity;
    public static final int CoordinatorLayout_Layout_layout_anchor;
    public static final int CoordinatorLayout_Layout_layout_anchorGravity;
    public static final int CoordinatorLayout_Layout_layout_behavior;
    public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges;
    public static final int CoordinatorLayout_Layout_layout_insetEdge;
    public static final int CoordinatorLayout_Layout_layout_keyline;
    public static final int CoordinatorLayout_keylines;
    public static final int CoordinatorLayout_statusBarBackground;
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
}
