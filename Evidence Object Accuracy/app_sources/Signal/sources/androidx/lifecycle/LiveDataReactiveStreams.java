package androidx.lifecycle;

import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.camera.view.PreviewView$1$$ExternalSyntheticBackportWithForwarding0;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

/* loaded from: classes.dex */
public final class LiveDataReactiveStreams {
    public static <T> LiveData<T> fromPublisher(Publisher<T> publisher) {
        return new PublisherLiveData(publisher);
    }

    /* access modifiers changed from: private */
    /* loaded from: classes.dex */
    public static class PublisherLiveData<T> extends LiveData<T> {
        private final Publisher<T> mPublisher;
        final AtomicReference<PublisherLiveData<T>.LiveDataSubscriber> mSubscriber = new AtomicReference<>();

        PublisherLiveData(Publisher<T> publisher) {
            this.mPublisher = publisher;
        }

        @Override // androidx.lifecycle.LiveData
        public void onActive() {
            super.onActive();
            PublisherLiveData<T>.LiveDataSubscriber liveDataSubscriber = new LiveDataSubscriber();
            this.mSubscriber.set(liveDataSubscriber);
            this.mPublisher.subscribe(liveDataSubscriber);
        }

        @Override // androidx.lifecycle.LiveData
        public void onInactive() {
            super.onInactive();
            PublisherLiveData<T>.LiveDataSubscriber andSet = this.mSubscriber.getAndSet(null);
            if (andSet != null) {
                andSet.cancelSubscription();
            }
        }

        /* access modifiers changed from: package-private */
        /* loaded from: classes.dex */
        public final class LiveDataSubscriber extends AtomicReference<Subscription> implements Subscriber<T> {
            LiveDataSubscriber() {
                PublisherLiveData.this = r1;
            }

            @Override // org.reactivestreams.Subscriber
            public void onSubscribe(Subscription subscription) {
                if (compareAndSet(null, subscription)) {
                    subscription.request(Long.MAX_VALUE);
                } else {
                    subscription.cancel();
                }
            }

            @Override // org.reactivestreams.Subscriber
            public void onNext(T t) {
                PublisherLiveData.this.postValue(t);
            }

            @Override // org.reactivestreams.Subscriber
            public void onError(final Throwable th) {
                PreviewView$1$$ExternalSyntheticBackportWithForwarding0.m(PublisherLiveData.this.mSubscriber, this, null);
                ArchTaskExecutor.getInstance().executeOnMainThread(new Runnable() { // from class: androidx.lifecycle.LiveDataReactiveStreams.PublisherLiveData.LiveDataSubscriber.1
                    @Override // java.lang.Runnable
                    public void run() {
                        throw new RuntimeException("LiveData does not handle errors. Errors from publishers should be handled upstream and propagated as state", th);
                    }
                });
            }

            @Override // org.reactivestreams.Subscriber
            public void onComplete() {
                PreviewView$1$$ExternalSyntheticBackportWithForwarding0.m(PublisherLiveData.this.mSubscriber, this, null);
            }

            public void cancelSubscription() {
                Subscription subscription = get();
                if (subscription != null) {
                    subscription.cancel();
                }
            }
        }
    }
}
