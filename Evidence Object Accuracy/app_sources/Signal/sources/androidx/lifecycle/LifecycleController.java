package androidx.lifecycle;

import androidx.lifecycle.Lifecycle;
import kotlin.Metadata;

/* compiled from: LifecycleController.kt */
@Metadata(bv = {}, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\b\u0001\u0018\u00002\u00020\u0001¨\u0006\u0002"}, d2 = {"Landroidx/lifecycle/LifecycleController;", "", "lifecycle-runtime-ktx_release"}, k = 1, mv = {1, 4, 1})
/* loaded from: classes.dex */
public final class LifecycleController {
    public static final /* synthetic */ DispatchQueue access$getDispatchQueue$p(LifecycleController lifecycleController) {
        throw null;
    }

    public static final /* synthetic */ Lifecycle.State access$getMinState$p(LifecycleController lifecycleController) {
        throw null;
    }
}
