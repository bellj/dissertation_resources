package androidx.recyclerview;

/* loaded from: classes.dex */
public final class R$dimen {
    public static final int fastscroll_default_thickness;
    public static final int fastscroll_margin;
    public static final int fastscroll_minimum_range;
    public static final int item_touch_helper_max_drag_scroll_per_frame;
    public static final int item_touch_helper_swipe_escape_max_velocity;
    public static final int item_touch_helper_swipe_escape_velocity;
}
