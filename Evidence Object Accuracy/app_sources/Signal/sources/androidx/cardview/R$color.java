package androidx.cardview;

/* loaded from: classes.dex */
public final class R$color {
    public static final int cardview_dark_background;
    public static final int cardview_light_background;
    public static final int cardview_shadow_end_color;
    public static final int cardview_shadow_start_color;
}
