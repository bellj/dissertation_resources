package androidx.cardview;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] CardView = {16843071, 16843072, R.attr.cardBackgroundColor, R.attr.cardCornerRadius, R.attr.cardElevation, R.attr.cardMaxElevation, R.attr.cardPreventCornerOverlap, R.attr.cardUseCompatPadding, R.attr.contentPadding, R.attr.contentPaddingBottom, R.attr.contentPaddingLeft, R.attr.contentPaddingRight, R.attr.contentPaddingTop};
    public static final int CardView_android_minHeight;
    public static final int CardView_android_minWidth;
    public static final int CardView_cardBackgroundColor;
    public static final int CardView_cardCornerRadius;
    public static final int CardView_cardElevation;
    public static final int CardView_cardMaxElevation;
    public static final int CardView_cardPreventCornerOverlap;
    public static final int CardView_cardUseCompatPadding;
    public static final int CardView_contentPadding;
    public static final int CardView_contentPaddingBottom;
    public static final int CardView_contentPaddingLeft;
    public static final int CardView_contentPaddingRight;
    public static final int CardView_contentPaddingTop;
}
