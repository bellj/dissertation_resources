package androidx.appcompat;

/* loaded from: classes.dex */
public final class R$id {
    public static final int action_bar;
    public static final int action_bar_activity_content;
    public static final int action_bar_container;
    public static final int action_bar_subtitle;
    public static final int action_bar_title;
    public static final int action_context_bar;
    public static final int action_menu_presenter;
    public static final int action_mode_bar_stub;
    public static final int action_mode_close_button;
    public static final int alertTitle;
    public static final int buttonPanel;
    public static final int content;
    public static final int contentPanel;
    public static final int custom;
    public static final int customPanel;
    public static final int decor_content_parent;
    public static final int edit_query;
    public static final int group_divider;
    public static final int message;
    public static final int parentPanel;
    public static final int scrollIndicatorDown;
    public static final int scrollIndicatorUp;
    public static final int scrollView;
    public static final int search_button;
    public static final int search_close_btn;
    public static final int search_edit_frame;
    public static final int search_go_btn;
    public static final int search_mag_icon;
    public static final int search_plate;
    public static final int search_src_text;
    public static final int search_voice_btn;
    public static final int shortcut;
    public static final int spacer;
    public static final int split_action_bar;
    public static final int submenuarrow;
    public static final int submit_area;
    public static final int textSpacerNoButtons;
    public static final int textSpacerNoTitle;
    public static final int title;
    public static final int titleDividerNoCustom;
    public static final int title_template;
    public static final int topPanel;
}
