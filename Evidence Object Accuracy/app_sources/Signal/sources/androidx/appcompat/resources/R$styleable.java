package androidx.appcompat.resources;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] AnimatedStateListDrawableCompat = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    public static final int AnimatedStateListDrawableCompat_android_constantSize;
    public static final int AnimatedStateListDrawableCompat_android_dither;
    public static final int AnimatedStateListDrawableCompat_android_enterFadeDuration;
    public static final int AnimatedStateListDrawableCompat_android_exitFadeDuration;
    public static final int AnimatedStateListDrawableCompat_android_variablePadding;
    public static final int AnimatedStateListDrawableCompat_android_visible;
    public static final int[] AnimatedStateListDrawableItem = {16842960, 16843161};
    public static final int AnimatedStateListDrawableItem_android_drawable;
    public static final int AnimatedStateListDrawableItem_android_id;
    public static final int[] AnimatedStateListDrawableTransition = {16843161, 16843849, 16843850, 16843851};
    public static final int AnimatedStateListDrawableTransition_android_drawable;
    public static final int AnimatedStateListDrawableTransition_android_fromId;
    public static final int AnimatedStateListDrawableTransition_android_reversible;
    public static final int AnimatedStateListDrawableTransition_android_toId;
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
    public static final int[] StateListDrawable = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    public static final int[] StateListDrawableItem = {16843161};
}
