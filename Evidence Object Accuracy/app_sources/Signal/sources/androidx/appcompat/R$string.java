package androidx.appcompat;

/* loaded from: classes.dex */
public final class R$string {
    public static final int abc_action_bar_up_description;
    public static final int abc_menu_alt_shortcut_label;
    public static final int abc_menu_ctrl_shortcut_label;
    public static final int abc_menu_delete_shortcut_label;
    public static final int abc_menu_enter_shortcut_label;
    public static final int abc_menu_function_shortcut_label;
    public static final int abc_menu_meta_shortcut_label;
    public static final int abc_menu_shift_shortcut_label;
    public static final int abc_menu_space_shortcut_label;
    public static final int abc_menu_sym_shortcut_label;
    public static final int abc_prepend_shortcut_label;
    public static final int abc_searchview_description_search;
}
