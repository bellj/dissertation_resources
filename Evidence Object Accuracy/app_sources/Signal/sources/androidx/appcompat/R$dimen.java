package androidx.appcompat;

/* loaded from: classes.dex */
public final class R$dimen {
    public static final int abc_action_bar_stacked_max_height;
    public static final int abc_action_bar_stacked_tab_max_width;
    public static final int abc_cascading_menus_min_smallest_width;
    public static final int abc_config_prefDialogWidth;
    public static final int abc_dropdownitem_icon_width;
    public static final int abc_dropdownitem_text_padding_left;
    public static final int abc_search_view_preferred_height;
    public static final int abc_search_view_preferred_width;
    public static final int tooltip_precise_anchor_extra_offset;
    public static final int tooltip_precise_anchor_threshold;
    public static final int tooltip_y_offset_non_touch;
    public static final int tooltip_y_offset_touch;
}
