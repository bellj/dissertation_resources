package androidx.appcompat;

/* loaded from: classes.dex */
public final class R$color {
    public static final int abc_decor_view_status_guard;
    public static final int abc_decor_view_status_guard_light;
    public static final int abc_tint_btn_checkable;
    public static final int abc_tint_default;
    public static final int abc_tint_edittext;
    public static final int abc_tint_seek_thumb;
    public static final int abc_tint_spinner;
    public static final int abc_tint_switch_track;
}
