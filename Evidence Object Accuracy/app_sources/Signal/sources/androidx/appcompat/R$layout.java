package androidx.appcompat;

/* loaded from: classes.dex */
public final class R$layout {
    public static final int abc_action_bar_title_item;
    public static final int abc_action_menu_item_layout;
    public static final int abc_action_menu_layout;
    public static final int abc_action_mode_close_item_material;
    public static final int abc_cascading_menu_item_layout;
    public static final int abc_dialog_title_material;
    public static final int abc_expanded_menu_layout;
    public static final int abc_list_menu_item_checkbox;
    public static final int abc_list_menu_item_icon;
    public static final int abc_list_menu_item_layout;
    public static final int abc_list_menu_item_radio;
    public static final int abc_popup_menu_header_item_layout;
    public static final int abc_popup_menu_item_layout;
    public static final int abc_screen_simple;
    public static final int abc_screen_simple_overlay_action_mode;
    public static final int abc_screen_toolbar;
    public static final int abc_search_dropdown_item_icons_2line;
    public static final int abc_search_view;
    public static final int abc_tooltip;
    public static final int support_simple_spinner_dropdown_item;
}
