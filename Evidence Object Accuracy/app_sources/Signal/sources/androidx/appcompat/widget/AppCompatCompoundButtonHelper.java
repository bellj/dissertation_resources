package androidx.appcompat.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.widget.CompoundButton;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.widget.CompoundButtonCompat;

/* loaded from: classes.dex */
public class AppCompatCompoundButtonHelper {
    private ColorStateList mButtonTintList = null;
    private PorterDuff.Mode mButtonTintMode = null;
    private boolean mHasButtonTint = false;
    private boolean mHasButtonTintMode = false;
    private boolean mSkipNextApply;
    private final CompoundButton mView;

    public int getCompoundPaddingLeft(int i) {
        return i;
    }

    public AppCompatCompoundButtonHelper(CompoundButton compoundButton) {
        this.mView = compoundButton;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e A[Catch: all -> 0x0085, TRY_ENTER, TryCatch #1 {all -> 0x0085, blocks: (B:3:0x001d, B:5:0x0025, B:7:0x002b, B:12:0x003e, B:14:0x0046, B:16:0x004c, B:17:0x0059, B:19:0x0061, B:20:0x006a, B:22:0x0072), top: B:30:0x001d }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0061 A[Catch: all -> 0x0085, TryCatch #1 {all -> 0x0085, blocks: (B:3:0x001d, B:5:0x0025, B:7:0x002b, B:12:0x003e, B:14:0x0046, B:16:0x004c, B:17:0x0059, B:19:0x0061, B:20:0x006a, B:22:0x0072), top: B:30:0x001d }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0072 A[Catch: all -> 0x0085, TRY_LEAVE, TryCatch #1 {all -> 0x0085, blocks: (B:3:0x001d, B:5:0x0025, B:7:0x002b, B:12:0x003e, B:14:0x0046, B:16:0x004c, B:17:0x0059, B:19:0x0061, B:20:0x006a, B:22:0x0072), top: B:30:0x001d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadFromAttributes(android.util.AttributeSet r10, int r11) {
        /*
            r9 = this;
            android.widget.CompoundButton r0 = r9.mView
            android.content.Context r0 = r0.getContext()
            int[] r3 = androidx.appcompat.R$styleable.CompoundButton
            r8 = 0
            androidx.appcompat.widget.TintTypedArray r0 = androidx.appcompat.widget.TintTypedArray.obtainStyledAttributes(r0, r10, r3, r11, r8)
            android.widget.CompoundButton r1 = r9.mView
            android.content.Context r2 = r1.getContext()
            android.content.res.TypedArray r5 = r0.getWrappedTypeArray()
            r7 = 0
            r4 = r10
            r6 = r11
            androidx.core.view.ViewCompat.saveAttributeDataForStyleable(r1, r2, r3, r4, r5, r6, r7)
            int r10 = androidx.appcompat.R$styleable.CompoundButton_buttonCompat     // Catch: all -> 0x0085
            boolean r11 = r0.hasValue(r10)     // Catch: all -> 0x0085
            if (r11 == 0) goto L_0x003b
            int r10 = r0.getResourceId(r10, r8)     // Catch: all -> 0x0085
            if (r10 == 0) goto L_0x003b
            android.widget.CompoundButton r11 = r9.mView     // Catch: NotFoundException -> 0x003a, all -> 0x0085
            android.content.Context r1 = r11.getContext()     // Catch: NotFoundException -> 0x003a, all -> 0x0085
            android.graphics.drawable.Drawable r10 = androidx.appcompat.content.res.AppCompatResources.getDrawable(r1, r10)     // Catch: NotFoundException -> 0x003a, all -> 0x0085
            r11.setButtonDrawable(r10)     // Catch: NotFoundException -> 0x003a, all -> 0x0085
            r10 = 1
            goto L_0x003c
        L_0x003a:
        L_0x003b:
            r10 = 0
        L_0x003c:
            if (r10 != 0) goto L_0x0059
            int r10 = androidx.appcompat.R$styleable.CompoundButton_android_button     // Catch: all -> 0x0085
            boolean r11 = r0.hasValue(r10)     // Catch: all -> 0x0085
            if (r11 == 0) goto L_0x0059
            int r10 = r0.getResourceId(r10, r8)     // Catch: all -> 0x0085
            if (r10 == 0) goto L_0x0059
            android.widget.CompoundButton r11 = r9.mView     // Catch: all -> 0x0085
            android.content.Context r1 = r11.getContext()     // Catch: all -> 0x0085
            android.graphics.drawable.Drawable r10 = androidx.appcompat.content.res.AppCompatResources.getDrawable(r1, r10)     // Catch: all -> 0x0085
            r11.setButtonDrawable(r10)     // Catch: all -> 0x0085
        L_0x0059:
            int r10 = androidx.appcompat.R$styleable.CompoundButton_buttonTint     // Catch: all -> 0x0085
            boolean r11 = r0.hasValue(r10)     // Catch: all -> 0x0085
            if (r11 == 0) goto L_0x006a
            android.widget.CompoundButton r11 = r9.mView     // Catch: all -> 0x0085
            android.content.res.ColorStateList r10 = r0.getColorStateList(r10)     // Catch: all -> 0x0085
            androidx.core.widget.CompoundButtonCompat.setButtonTintList(r11, r10)     // Catch: all -> 0x0085
        L_0x006a:
            int r10 = androidx.appcompat.R$styleable.CompoundButton_buttonTintMode     // Catch: all -> 0x0085
            boolean r11 = r0.hasValue(r10)     // Catch: all -> 0x0085
            if (r11 == 0) goto L_0x0081
            android.widget.CompoundButton r11 = r9.mView     // Catch: all -> 0x0085
            r1 = -1
            int r10 = r0.getInt(r10, r1)     // Catch: all -> 0x0085
            r1 = 0
            android.graphics.PorterDuff$Mode r10 = androidx.appcompat.widget.DrawableUtils.parseTintMode(r10, r1)     // Catch: all -> 0x0085
            androidx.core.widget.CompoundButtonCompat.setButtonTintMode(r11, r10)     // Catch: all -> 0x0085
        L_0x0081:
            r0.recycle()
            return
        L_0x0085:
            r10 = move-exception
            r0.recycle()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.AppCompatCompoundButtonHelper.loadFromAttributes(android.util.AttributeSet, int):void");
    }

    public void setSupportButtonTintList(ColorStateList colorStateList) {
        this.mButtonTintList = colorStateList;
        this.mHasButtonTint = true;
        applyButtonTint();
    }

    public ColorStateList getSupportButtonTintList() {
        return this.mButtonTintList;
    }

    public void setSupportButtonTintMode(PorterDuff.Mode mode) {
        this.mButtonTintMode = mode;
        this.mHasButtonTintMode = true;
        applyButtonTint();
    }

    public PorterDuff.Mode getSupportButtonTintMode() {
        return this.mButtonTintMode;
    }

    public void onSetButtonDrawable() {
        if (this.mSkipNextApply) {
            this.mSkipNextApply = false;
            return;
        }
        this.mSkipNextApply = true;
        applyButtonTint();
    }

    void applyButtonTint() {
        Drawable buttonDrawable = CompoundButtonCompat.getButtonDrawable(this.mView);
        if (buttonDrawable == null) {
            return;
        }
        if (this.mHasButtonTint || this.mHasButtonTintMode) {
            Drawable mutate = DrawableCompat.wrap(buttonDrawable).mutate();
            if (this.mHasButtonTint) {
                DrawableCompat.setTintList(mutate, this.mButtonTintList);
            }
            if (this.mHasButtonTintMode) {
                DrawableCompat.setTintMode(mutate, this.mButtonTintMode);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.mView.getDrawableState());
            }
            this.mView.setButtonDrawable(mutate);
        }
    }
}
