package androidx.camera.core;

/* loaded from: classes.dex */
public final class CameraClosedException extends RuntimeException {
    public CameraClosedException(String str) {
        super(str);
    }
}
