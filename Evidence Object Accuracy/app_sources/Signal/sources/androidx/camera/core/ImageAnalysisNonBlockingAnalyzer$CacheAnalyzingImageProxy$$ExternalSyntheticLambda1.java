package androidx.camera.core;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class ImageAnalysisNonBlockingAnalyzer$CacheAnalyzingImageProxy$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ ImageAnalysisNonBlockingAnalyzer f$0;

    public /* synthetic */ ImageAnalysisNonBlockingAnalyzer$CacheAnalyzingImageProxy$$ExternalSyntheticLambda1(ImageAnalysisNonBlockingAnalyzer imageAnalysisNonBlockingAnalyzer) {
        this.f$0 = imageAnalysisNonBlockingAnalyzer;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.analyzeCachedImage();
    }
}
