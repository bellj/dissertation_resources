package androidx.camera.core;

import androidx.lifecycle.LiveData;

/* loaded from: classes.dex */
public interface CameraInfo {
    String getImplementationType();

    int getSensorRotationDegrees(int i);

    LiveData<ZoomState> getZoomState();

    boolean hasFlashUnit();
}
