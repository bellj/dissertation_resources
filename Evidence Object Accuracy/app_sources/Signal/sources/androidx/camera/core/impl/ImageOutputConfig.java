package androidx.camera.core.impl;

import android.util.Pair;
import android.util.Size;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.impl.Config;
import java.util.List;

/* loaded from: classes.dex */
public interface ImageOutputConfig extends ReadableConfig {
    public static final Config.Option<Size> OPTION_DEFAULT_RESOLUTION = Config.Option.create("camerax.core.imageOutput.defaultResolution", Size.class);
    public static final Config.Option<Size> OPTION_MAX_RESOLUTION = Config.Option.create("camerax.core.imageOutput.maxResolution", Size.class);
    public static final Config.Option<List<Pair<Integer, Size[]>>> OPTION_SUPPORTED_RESOLUTIONS = Config.Option.create("camerax.core.imageOutput.supportedResolutions", List.class);
    public static final Config.Option<Integer> OPTION_TARGET_ASPECT_RATIO = Config.Option.create("camerax.core.imageOutput.targetAspectRatio", AspectRatio.class);
    public static final Config.Option<Size> OPTION_TARGET_RESOLUTION = Config.Option.create("camerax.core.imageOutput.targetResolution", Size.class);
    public static final Config.Option<Integer> OPTION_TARGET_ROTATION = Config.Option.create("camerax.core.imageOutput.targetRotation", Integer.TYPE);

    /* loaded from: classes.dex */
    public interface Builder<B> {
        B setTargetResolution(Size size);

        B setTargetRotation(int i);
    }

    Size getDefaultResolution(Size size);

    Size getMaxResolution(Size size);

    List<Pair<Integer, Size[]>> getSupportedResolutions(List<Pair<Integer, Size[]>> list);

    int getTargetAspectRatio();

    Size getTargetResolution(Size size);

    int getTargetRotation(int i);

    boolean hasTargetAspectRatio();

    /* renamed from: androidx.camera.core.impl.ImageOutputConfig$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static boolean $default$hasTargetAspectRatio(ImageOutputConfig imageOutputConfig) {
            return imageOutputConfig.containsOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO);
        }

        public static int $default$getTargetAspectRatio(ImageOutputConfig imageOutputConfig) {
            return ((Integer) imageOutputConfig.retrieveOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO)).intValue();
        }

        public static int $default$getTargetRotation(ImageOutputConfig imageOutputConfig, int i) {
            return ((Integer) imageOutputConfig.retrieveOption(ImageOutputConfig.OPTION_TARGET_ROTATION, Integer.valueOf(i))).intValue();
        }

        public static Size $default$getTargetResolution(ImageOutputConfig imageOutputConfig, Size size) {
            return (Size) imageOutputConfig.retrieveOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, size);
        }

        public static Size $default$getDefaultResolution(ImageOutputConfig imageOutputConfig, Size size) {
            return (Size) imageOutputConfig.retrieveOption(ImageOutputConfig.OPTION_DEFAULT_RESOLUTION, size);
        }

        public static Size $default$getMaxResolution(ImageOutputConfig imageOutputConfig, Size size) {
            return (Size) imageOutputConfig.retrieveOption(ImageOutputConfig.OPTION_MAX_RESOLUTION, size);
        }

        public static List $default$getSupportedResolutions(ImageOutputConfig imageOutputConfig, List list) {
            return (List) imageOutputConfig.retrieveOption(ImageOutputConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
        }
    }
}
