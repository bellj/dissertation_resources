package androidx.camera.core.impl;

import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public interface Observable<T> {

    /* loaded from: classes.dex */
    public interface Observer<T> {
        void onError(Throwable th);

        void onNewData(T t);
    }

    void addObserver(Executor executor, Observer<T> observer);

    void removeObserver(Observer<T> observer);
}
