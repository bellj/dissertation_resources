package androidx.camera.core.impl;

import java.util.Set;

/* loaded from: classes.dex */
public interface Config {

    /* loaded from: classes.dex */
    public interface OptionMatcher {
        boolean onOptionMatched(Option<?> option);
    }

    /* loaded from: classes.dex */
    public enum OptionPriority {
        ALWAYS_OVERRIDE,
        REQUIRED,
        OPTIONAL
    }

    boolean containsOption(Option<?> option);

    void findOptions(String str, OptionMatcher optionMatcher);

    OptionPriority getOptionPriority(Option<?> option);

    Set<OptionPriority> getPriorities(Option<?> option);

    Set<Option<?>> listOptions();

    <ValueT> ValueT retrieveOption(Option<ValueT> option);

    <ValueT> ValueT retrieveOption(Option<ValueT> option, ValueT valuet);

    <ValueT> ValueT retrieveOptionWithPriority(Option<ValueT> option, OptionPriority optionPriority);

    /* loaded from: classes.dex */
    public static abstract class Option<T> {
        public abstract String getId();

        public abstract Object getToken();

        public abstract Class<T> getValueClass();

        public static <T> Option<T> create(String str, Class<?> cls) {
            return create(str, cls, null);
        }

        public static <T> Option<T> create(String str, Class<?> cls, Object obj) {
            return new AutoValue_Config_Option(str, cls, obj);
        }
    }

    /* renamed from: androidx.camera.core.impl.Config$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static boolean hasConflict(OptionPriority optionPriority, OptionPriority optionPriority2) {
            OptionPriority optionPriority3 = OptionPriority.ALWAYS_OVERRIDE;
            if (optionPriority == optionPriority3 && optionPriority2 == optionPriority3) {
                return true;
            }
            OptionPriority optionPriority4 = OptionPriority.REQUIRED;
            if (optionPriority == optionPriority4 && optionPriority2 == optionPriority4) {
                return true;
            }
            return false;
        }
    }
}
