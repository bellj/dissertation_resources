package androidx.camera.core.impl;

import androidx.camera.core.impl.Config;
import java.util.Set;

/* loaded from: classes.dex */
public interface ReadableConfig extends Config {
    @Override // androidx.camera.core.impl.Config
    boolean containsOption(Config.Option<?> option);

    Config getConfig();

    @Override // androidx.camera.core.impl.Config
    Config.OptionPriority getOptionPriority(Config.Option<?> option);

    @Override // androidx.camera.core.impl.Config
    Set<Config.Option<?>> listOptions();

    @Override // androidx.camera.core.impl.Config
    <ValueT> ValueT retrieveOption(Config.Option<ValueT> option);

    @Override // androidx.camera.core.impl.Config
    <ValueT> ValueT retrieveOption(Config.Option<ValueT> option, ValueT valuet);

    /* renamed from: androidx.camera.core.impl.ReadableConfig$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static boolean $default$containsOption(ReadableConfig readableConfig, Config.Option option) {
            return readableConfig.getConfig().containsOption(option);
        }

        public static Object $default$retrieveOption(ReadableConfig readableConfig, Config.Option option) {
            return readableConfig.getConfig().retrieveOption(option);
        }

        public static Object $default$retrieveOption(ReadableConfig readableConfig, Config.Option option, Object obj) {
            return readableConfig.getConfig().retrieveOption(option, obj);
        }

        public static void $default$findOptions(ReadableConfig readableConfig, String str, Config.OptionMatcher optionMatcher) {
            readableConfig.getConfig().findOptions(str, optionMatcher);
        }

        public static Set $default$listOptions(ReadableConfig readableConfig) {
            return readableConfig.getConfig().listOptions();
        }

        public static Object $default$retrieveOptionWithPriority(ReadableConfig readableConfig, Config.Option option, Config.OptionPriority optionPriority) {
            return readableConfig.getConfig().retrieveOptionWithPriority(option, optionPriority);
        }

        public static Config.OptionPriority $default$getOptionPriority(ReadableConfig readableConfig, Config.Option option) {
            return readableConfig.getConfig().getOptionPriority(option);
        }

        public static Set $default$getPriorities(ReadableConfig readableConfig, Config.Option option) {
            return readableConfig.getConfig().getPriorities(option);
        }
    }
}
