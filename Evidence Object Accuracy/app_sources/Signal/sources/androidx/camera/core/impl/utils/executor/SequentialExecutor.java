package androidx.camera.core.impl.utils.executor;

import androidx.core.util.Preconditions;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

/* loaded from: classes.dex */
public final class SequentialExecutor implements Executor {
    private final Executor mExecutor;
    final Deque<Runnable> mQueue = new ArrayDeque();
    private final QueueWorker mWorker = new QueueWorker();
    long mWorkerRunCount = 0;
    WorkerRunningState mWorkerRunningState = WorkerRunningState.IDLE;

    /* loaded from: classes.dex */
    public enum WorkerRunningState {
        IDLE,
        QUEUING,
        QUEUED,
        RUNNING
    }

    public SequentialExecutor(Executor executor) {
        this.mExecutor = (Executor) Preconditions.checkNotNull(executor);
    }

    @Override // java.util.concurrent.Executor
    public void execute(final Runnable runnable) {
        WorkerRunningState workerRunningState;
        Preconditions.checkNotNull(runnable);
        synchronized (this.mQueue) {
            WorkerRunningState workerRunningState2 = this.mWorkerRunningState;
            if (!(workerRunningState2 == WorkerRunningState.RUNNING || workerRunningState2 == (workerRunningState = WorkerRunningState.QUEUED))) {
                long j = this.mWorkerRunCount;
                AnonymousClass1 r1 = new Runnable() { // from class: androidx.camera.core.impl.utils.executor.SequentialExecutor.1
                    @Override // java.lang.Runnable
                    public void run() {
                        runnable.run();
                    }
                };
                this.mQueue.add(r1);
                WorkerRunningState workerRunningState3 = WorkerRunningState.QUEUING;
                this.mWorkerRunningState = workerRunningState3;
                boolean z = true;
                try {
                    this.mExecutor.execute(this.mWorker);
                    if (this.mWorkerRunningState == workerRunningState3) {
                        z = false;
                    }
                    if (!z) {
                        synchronized (this.mQueue) {
                            if (this.mWorkerRunCount == j && this.mWorkerRunningState == workerRunningState3) {
                                this.mWorkerRunningState = workerRunningState;
                            }
                        }
                        return;
                    }
                    return;
                } catch (Error | RuntimeException e) {
                    synchronized (this.mQueue) {
                        WorkerRunningState workerRunningState4 = this.mWorkerRunningState;
                        if ((workerRunningState4 != WorkerRunningState.IDLE && workerRunningState4 != WorkerRunningState.QUEUING) || !this.mQueue.removeLastOccurrence(r1)) {
                            z = false;
                        }
                        if (!(e instanceof RejectedExecutionException) || z) {
                            throw e;
                        }
                    }
                    return;
                }
            }
            this.mQueue.add(runnable);
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes.dex */
    public final class QueueWorker implements Runnable {
        QueueWorker() {
            SequentialExecutor.this = r1;
        }

        @Override // java.lang.Runnable
        public void run() {
            try {
                workOnQueue();
            } catch (Error e) {
                synchronized (SequentialExecutor.this.mQueue) {
                    SequentialExecutor.this.mWorkerRunningState = WorkerRunningState.IDLE;
                    throw e;
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0039, code lost:
            if (r1 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x003b, code lost:
            java.lang.Thread.currentThread().interrupt();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0042, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0048, code lost:
            r1 = r1 | java.lang.Thread.interrupted();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0049, code lost:
            r3.run();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x004d, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x004e, code lost:
            androidx.camera.core.Logger.e("SequentialExecutor", "Exception while executing runnable " + r3, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void workOnQueue() {
            /*
                r9 = this;
                r0 = 0
                r1 = 0
            L_0x0002:
                androidx.camera.core.impl.utils.executor.SequentialExecutor r2 = androidx.camera.core.impl.utils.executor.SequentialExecutor.this     // Catch: all -> 0x0068
                java.util.Deque<java.lang.Runnable> r2 = r2.mQueue     // Catch: all -> 0x0068
                monitor-enter(r2)     // Catch: all -> 0x0068
                if (r0 != 0) goto L_0x0026
                androidx.camera.core.impl.utils.executor.SequentialExecutor r0 = androidx.camera.core.impl.utils.executor.SequentialExecutor.this     // Catch: all -> 0x0065
                androidx.camera.core.impl.utils.executor.SequentialExecutor$WorkerRunningState r3 = r0.mWorkerRunningState     // Catch: all -> 0x0065
                androidx.camera.core.impl.utils.executor.SequentialExecutor$WorkerRunningState r4 = androidx.camera.core.impl.utils.executor.SequentialExecutor.WorkerRunningState.RUNNING     // Catch: all -> 0x0065
                if (r3 != r4) goto L_0x001c
                monitor-exit(r2)     // Catch: all -> 0x0065
                if (r1 == 0) goto L_0x001b
                java.lang.Thread r0 = java.lang.Thread.currentThread()
                r0.interrupt()
            L_0x001b:
                return
            L_0x001c:
                long r5 = r0.mWorkerRunCount     // Catch: all -> 0x0065
                r7 = 1
                long r5 = r5 + r7
                r0.mWorkerRunCount = r5     // Catch: all -> 0x0065
                r0.mWorkerRunningState = r4     // Catch: all -> 0x0065
                r0 = 1
            L_0x0026:
                androidx.camera.core.impl.utils.executor.SequentialExecutor r3 = androidx.camera.core.impl.utils.executor.SequentialExecutor.this     // Catch: all -> 0x0065
                java.util.Deque<java.lang.Runnable> r3 = r3.mQueue     // Catch: all -> 0x0065
                java.lang.Object r3 = r3.poll()     // Catch: all -> 0x0065
                java.lang.Runnable r3 = (java.lang.Runnable) r3     // Catch: all -> 0x0065
                if (r3 != 0) goto L_0x0043
                androidx.camera.core.impl.utils.executor.SequentialExecutor r0 = androidx.camera.core.impl.utils.executor.SequentialExecutor.this     // Catch: all -> 0x0065
                androidx.camera.core.impl.utils.executor.SequentialExecutor$WorkerRunningState r3 = androidx.camera.core.impl.utils.executor.SequentialExecutor.WorkerRunningState.IDLE     // Catch: all -> 0x0065
                r0.mWorkerRunningState = r3     // Catch: all -> 0x0065
                monitor-exit(r2)     // Catch: all -> 0x0065
                if (r1 == 0) goto L_0x0042
                java.lang.Thread r0 = java.lang.Thread.currentThread()
                r0.interrupt()
            L_0x0042:
                return
            L_0x0043:
                monitor-exit(r2)     // Catch: all -> 0x0065
                boolean r2 = java.lang.Thread.interrupted()     // Catch: all -> 0x0068
                r1 = r1 | r2
                r3.run()     // Catch: RuntimeException -> 0x004d, all -> 0x0068
                goto L_0x0002
            L_0x004d:
                r2 = move-exception
                java.lang.String r4 = "SequentialExecutor"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch: all -> 0x0068
                r5.<init>()     // Catch: all -> 0x0068
                java.lang.String r6 = "Exception while executing runnable "
                r5.append(r6)     // Catch: all -> 0x0068
                r5.append(r3)     // Catch: all -> 0x0068
                java.lang.String r3 = r5.toString()     // Catch: all -> 0x0068
                androidx.camera.core.Logger.e(r4, r3, r2)     // Catch: all -> 0x0068
                goto L_0x0002
            L_0x0065:
                r0 = move-exception
                monitor-exit(r2)     // Catch: all -> 0x0065
                throw r0     // Catch: all -> 0x0068
            L_0x0068:
                r0 = move-exception
                if (r1 == 0) goto L_0x0072
                java.lang.Thread r1 = java.lang.Thread.currentThread()
                r1.interrupt()
            L_0x0072:
                goto L_0x0074
            L_0x0073:
                throw r0
            L_0x0074:
                goto L_0x0073
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.camera.core.impl.utils.executor.SequentialExecutor.QueueWorker.workOnQueue():void");
        }
    }
}
