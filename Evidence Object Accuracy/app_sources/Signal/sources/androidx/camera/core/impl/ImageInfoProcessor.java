package androidx.camera.core.impl;

import androidx.camera.core.ImageInfo;

/* loaded from: classes.dex */
public interface ImageInfoProcessor {
    boolean process(ImageInfo imageInfo);
}
