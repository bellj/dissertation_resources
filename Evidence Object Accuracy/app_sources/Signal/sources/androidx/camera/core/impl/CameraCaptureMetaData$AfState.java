package androidx.camera.core.impl;

/* loaded from: classes.dex */
public enum CameraCaptureMetaData$AfState {
    UNKNOWN,
    INACTIVE,
    SCANNING,
    FOCUSED,
    LOCKED_FOCUSED,
    LOCKED_NOT_FOCUSED
}
