package androidx.camera.core.impl;

/* loaded from: classes.dex */
public interface CameraConfig extends ReadableConfig {
    UseCaseConfigFactory getUseCaseConfigFactory();
}
