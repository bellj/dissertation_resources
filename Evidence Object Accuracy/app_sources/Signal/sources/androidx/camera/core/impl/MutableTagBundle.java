package androidx.camera.core.impl;

import android.util.ArrayMap;
import java.util.Map;

/* loaded from: classes.dex */
public class MutableTagBundle extends TagBundle {
    private MutableTagBundle(Map<String, Integer> map) {
        super(map);
    }

    public static MutableTagBundle create() {
        return new MutableTagBundle(new ArrayMap());
    }

    public static MutableTagBundle from(TagBundle tagBundle) {
        ArrayMap arrayMap = new ArrayMap();
        for (String str : tagBundle.listKeys()) {
            arrayMap.put(str, tagBundle.getTag(str));
        }
        return new MutableTagBundle(arrayMap);
    }

    public void putTag(String str, Integer num) {
        this.mTagMap.put(str, num);
    }

    public void addTagBundle(TagBundle tagBundle) {
        Map<String, Integer> map;
        Map<String, Integer> map2 = this.mTagMap;
        if (map2 != null && (map = tagBundle.mTagMap) != null) {
            map2.putAll(map);
        }
    }
}
