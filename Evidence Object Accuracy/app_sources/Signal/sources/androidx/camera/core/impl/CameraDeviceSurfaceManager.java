package androidx.camera.core.impl;

import android.content.Context;
import android.util.Size;
import androidx.camera.core.InitializationException;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public interface CameraDeviceSurfaceManager {

    /* loaded from: classes.dex */
    public interface Provider {
        CameraDeviceSurfaceManager newInstance(Context context, Object obj) throws InitializationException;
    }

    Map<UseCaseConfig<?>, Size> getSuggestedResolutions(String str, List<SurfaceConfig> list, List<UseCaseConfig<?>> list2);

    SurfaceConfig transformSurfaceConfig(String str, int i, Size size);
}
