package androidx.camera.core.impl;

import androidx.camera.core.Camera;
import androidx.camera.core.CameraControl;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.UseCase;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Collection;

/* loaded from: classes.dex */
public interface CameraInternal extends Camera, UseCase.StateChangeCallback {
    void attachUseCases(Collection<UseCase> collection);

    void detachUseCases(Collection<UseCase> collection);

    CameraControlInternal getCameraControlInternal();

    @Override // androidx.camera.core.Camera
    CameraInfo getCameraInfo();

    CameraInfoInternal getCameraInfoInternal();

    Observable<State> getCameraState();

    ListenableFuture<Void> release();

    /* loaded from: classes.dex */
    public enum State {
        PENDING_OPEN(false),
        OPENING(true),
        OPEN(true),
        CLOSING(true),
        CLOSED(false),
        RELEASING(true),
        RELEASED(false);
        
        private final boolean mHoldsCameraSlot;

        State(boolean z) {
            this.mHoldsCameraSlot = z;
        }

        public boolean holdsCameraSlot() {
            return this.mHoldsCameraSlot;
        }
    }

    /* renamed from: androidx.camera.core.impl.CameraInternal$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static CameraControl $default$getCameraControl(CameraInternal cameraInternal) {
            return cameraInternal.getCameraControlInternal();
        }

        public static CameraInfo $default$getCameraInfo(CameraInternal cameraInternal) {
            return cameraInternal.getCameraInfoInternal();
        }
    }
}
