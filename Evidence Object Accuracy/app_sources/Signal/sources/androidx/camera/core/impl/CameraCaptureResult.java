package androidx.camera.core.impl;

/* loaded from: classes.dex */
public interface CameraCaptureResult {
    CameraCaptureMetaData$AeState getAeState();

    CameraCaptureMetaData$AfMode getAfMode();

    CameraCaptureMetaData$AfState getAfState();

    CameraCaptureMetaData$AwbState getAwbState();

    TagBundle getTagBundle();

    long getTimestamp();

    /* loaded from: classes.dex */
    public static final class EmptyCameraCaptureResult implements CameraCaptureResult {
        @Override // androidx.camera.core.impl.CameraCaptureResult
        public TagBundle getTagBundle() {
            return null;
        }

        @Override // androidx.camera.core.impl.CameraCaptureResult
        public long getTimestamp() {
            return -1;
        }

        public static CameraCaptureResult create() {
            return new EmptyCameraCaptureResult();
        }

        @Override // androidx.camera.core.impl.CameraCaptureResult
        public CameraCaptureMetaData$AfMode getAfMode() {
            return CameraCaptureMetaData$AfMode.UNKNOWN;
        }

        @Override // androidx.camera.core.impl.CameraCaptureResult
        public CameraCaptureMetaData$AfState getAfState() {
            return CameraCaptureMetaData$AfState.UNKNOWN;
        }

        @Override // androidx.camera.core.impl.CameraCaptureResult
        public CameraCaptureMetaData$AeState getAeState() {
            return CameraCaptureMetaData$AeState.UNKNOWN;
        }

        @Override // androidx.camera.core.impl.CameraCaptureResult
        public CameraCaptureMetaData$AwbState getAwbState() {
            return CameraCaptureMetaData$AwbState.UNKNOWN;
        }
    }
}
