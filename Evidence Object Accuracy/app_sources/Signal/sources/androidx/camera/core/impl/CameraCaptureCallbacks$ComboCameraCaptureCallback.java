package androidx.camera.core.impl;

import java.util.List;

/* loaded from: classes.dex */
public final class CameraCaptureCallbacks$ComboCameraCaptureCallback extends CameraCaptureCallback {
    private final List<CameraCaptureCallback> mCallbacks;

    @Override // androidx.camera.core.impl.CameraCaptureCallback
    public void onCaptureCompleted(CameraCaptureResult cameraCaptureResult) {
        for (CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
            cameraCaptureCallback.onCaptureCompleted(cameraCaptureResult);
        }
    }

    @Override // androidx.camera.core.impl.CameraCaptureCallback
    public void onCaptureFailed(CameraCaptureFailure cameraCaptureFailure) {
        for (CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
            cameraCaptureCallback.onCaptureFailed(cameraCaptureFailure);
        }
    }

    @Override // androidx.camera.core.impl.CameraCaptureCallback
    public void onCaptureCancelled() {
        for (CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
            cameraCaptureCallback.onCaptureCancelled();
        }
    }

    public List<CameraCaptureCallback> getCallbacks() {
        return this.mCallbacks;
    }
}
