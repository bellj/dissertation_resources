package androidx.camera.core.impl;

import android.util.Log;
import android.view.Surface;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/* loaded from: classes.dex */
public abstract class DeferrableSurface {
    private static final boolean DEBUG = Logger.isDebugEnabled("DeferrableSurface");
    private static AtomicInteger sTotalCount = new AtomicInteger(0);
    private static AtomicInteger sUsedCount = new AtomicInteger(0);
    private boolean mClosed = false;
    private final Object mLock = new Object();
    private CallbackToFutureAdapter.Completer<Void> mTerminationCompleter;
    private final ListenableFuture<Void> mTerminationFuture;
    private int mUseCount = 0;

    protected abstract ListenableFuture<Surface> provideSurface();

    /* loaded from: classes.dex */
    public static final class SurfaceUnavailableException extends Exception {
        public SurfaceUnavailableException(String str) {
            super(str);
        }
    }

    /* loaded from: classes.dex */
    public static final class SurfaceClosedException extends Exception {
        DeferrableSurface mDeferrableSurface;

        public SurfaceClosedException(String str, DeferrableSurface deferrableSurface) {
            super(str);
            this.mDeferrableSurface = deferrableSurface;
        }

        public DeferrableSurface getDeferrableSurface() {
            return this.mDeferrableSurface;
        }
    }

    public DeferrableSurface() {
        ListenableFuture<Void> future = CallbackToFutureAdapter.getFuture(new CallbackToFutureAdapter.Resolver() { // from class: androidx.camera.core.impl.DeferrableSurface$$ExternalSyntheticLambda0
            @Override // androidx.concurrent.futures.CallbackToFutureAdapter.Resolver
            public final Object attachCompleter(CallbackToFutureAdapter.Completer completer) {
                return DeferrableSurface.this.lambda$new$0(completer);
            }
        });
        this.mTerminationFuture = future;
        if (Logger.isDebugEnabled("DeferrableSurface")) {
            printGlobalDebugCounts("Surface created", sTotalCount.incrementAndGet(), sUsedCount.get());
            future.addListener(new Runnable(Log.getStackTraceString(new Exception())) { // from class: androidx.camera.core.impl.DeferrableSurface$$ExternalSyntheticLambda1
                public final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DeferrableSurface.this.lambda$new$1(this.f$1);
                }
            }, CameraXExecutors.directExecutor());
        }
    }

    public /* synthetic */ Object lambda$new$0(CallbackToFutureAdapter.Completer completer) throws Exception {
        synchronized (this.mLock) {
            this.mTerminationCompleter = completer;
        }
        return "DeferrableSurface-termination(" + this + ")";
    }

    public /* synthetic */ void lambda$new$1(String str) {
        try {
            this.mTerminationFuture.get();
            printGlobalDebugCounts("Surface terminated", sTotalCount.decrementAndGet(), sUsedCount.get());
        } catch (Exception e) {
            Logger.e("DeferrableSurface", "Unexpected surface termination for " + this + "\nStack Trace:\n" + str);
            throw new IllegalArgumentException("DeferrableSurface terminated with unexpected exception.", e);
        }
    }

    private void printGlobalDebugCounts(String str, int i, int i2) {
        if (!DEBUG && Logger.isDebugEnabled("DeferrableSurface")) {
            Logger.d("DeferrableSurface", "DeferrableSurface usage statistics may be inaccurate since debug logging was not enabled at static initialization time. App restart may be required to enable accurate usage statistics.");
        }
        Logger.d("DeferrableSurface", str + "[total_surfaces=" + i + ", used_surfaces=" + i2 + "](" + this + "}");
    }

    public final ListenableFuture<Surface> getSurface() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return Futures.immediateFailedFuture(new SurfaceClosedException("DeferrableSurface already closed.", this));
            }
            return provideSurface();
        }
    }

    public ListenableFuture<Void> getTerminationFuture() {
        return Futures.nonCancellationPropagating(this.mTerminationFuture);
    }

    public void incrementUseCount() throws SurfaceClosedException {
        synchronized (this.mLock) {
            int i = this.mUseCount;
            if (i == 0 && this.mClosed) {
                throw new SurfaceClosedException("Cannot begin use on a closed surface.", this);
            }
            this.mUseCount = i + 1;
            if (Logger.isDebugEnabled("DeferrableSurface")) {
                if (this.mUseCount == 1) {
                    printGlobalDebugCounts("New surface in use", sTotalCount.get(), sUsedCount.incrementAndGet());
                }
                Logger.d("DeferrableSurface", "use count+1, useCount=" + this.mUseCount + " " + this);
            }
        }
    }

    public final void close() {
        CallbackToFutureAdapter.Completer<Void> completer;
        synchronized (this.mLock) {
            if (!this.mClosed) {
                this.mClosed = true;
                if (this.mUseCount == 0) {
                    completer = this.mTerminationCompleter;
                    this.mTerminationCompleter = null;
                } else {
                    completer = null;
                }
                if (Logger.isDebugEnabled("DeferrableSurface")) {
                    Logger.d("DeferrableSurface", "surface closed,  useCount=" + this.mUseCount + " closed=true " + this);
                }
            } else {
                completer = null;
            }
        }
        if (completer != null) {
            completer.set(null);
        }
    }

    public void decrementUseCount() {
        CallbackToFutureAdapter.Completer<Void> completer;
        synchronized (this.mLock) {
            int i = this.mUseCount;
            if (i != 0) {
                int i2 = i - 1;
                this.mUseCount = i2;
                if (i2 != 0 || !this.mClosed) {
                    completer = null;
                } else {
                    completer = this.mTerminationCompleter;
                    this.mTerminationCompleter = null;
                }
                if (Logger.isDebugEnabled("DeferrableSurface")) {
                    Logger.d("DeferrableSurface", "use count-1,  useCount=" + this.mUseCount + " closed=" + this.mClosed + " " + this);
                    if (this.mUseCount == 0) {
                        printGlobalDebugCounts("Surface no longer in use", sTotalCount.get(), sUsedCount.decrementAndGet());
                    }
                }
            } else {
                throw new IllegalStateException("Decrementing use count occurs more times than incrementing");
            }
        }
        if (completer != null) {
            completer.set(null);
        }
    }
}
