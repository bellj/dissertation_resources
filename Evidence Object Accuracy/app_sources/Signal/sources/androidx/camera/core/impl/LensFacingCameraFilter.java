package androidx.camera.core.impl;

import androidx.camera.core.Camera;
import androidx.camera.core.CameraFilter;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* loaded from: classes.dex */
public class LensFacingCameraFilter implements CameraFilter {
    private int mLensFacing;

    public LensFacingCameraFilter(int i) {
        this.mLensFacing = i;
    }

    @Override // androidx.camera.core.CameraFilter
    public LinkedHashSet<Camera> filter(LinkedHashSet<Camera> linkedHashSet) {
        LinkedHashSet<Camera> linkedHashSet2 = new LinkedHashSet<>();
        Iterator<Camera> it = linkedHashSet.iterator();
        while (it.hasNext()) {
            Camera next = it.next();
            Preconditions.checkState(next instanceof CameraInternal, "The camera doesn't contain internal implementation.");
            Integer lensFacing = ((CameraInternal) next).getCameraInfoInternal().getLensFacing();
            if (lensFacing != null && lensFacing.intValue() == this.mLensFacing) {
                linkedHashSet2.add(next);
            }
        }
        return linkedHashSet2;
    }
}
