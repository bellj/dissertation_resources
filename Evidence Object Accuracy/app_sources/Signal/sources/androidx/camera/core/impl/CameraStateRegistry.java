package androidx.camera.core.impl;

import androidx.camera.core.Camera;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.CameraInternal;
import androidx.core.util.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

/* loaded from: classes.dex */
public final class CameraStateRegistry {
    private int mAvailableCameras;
    private final Map<Camera, CameraRegistration> mCameraStates = new HashMap();
    private final StringBuilder mDebugString = new StringBuilder();
    private final Object mLock = new Object();
    private final int mMaxAllowedOpenedCameras;

    /* loaded from: classes.dex */
    public interface OnOpenAvailableListener {
        void onOpenAvailable();
    }

    public CameraStateRegistry(int i) {
        this.mMaxAllowedOpenedCameras = i;
        synchronized ("mLock") {
            this.mAvailableCameras = i;
        }
    }

    public void registerCamera(Camera camera, Executor executor, OnOpenAvailableListener onOpenAvailableListener) {
        synchronized (this.mLock) {
            boolean z = !this.mCameraStates.containsKey(camera);
            Preconditions.checkState(z, "Camera is already registered: " + camera);
            this.mCameraStates.put(camera, new CameraRegistration(null, executor, onOpenAvailableListener));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0072 A[Catch: all -> 0x009c, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x001d, B:7:0x0053, B:9:0x0057, B:13:0x0064, B:14:0x006a, B:16:0x0072, B:20:0x0081, B:22:0x0097, B:23:0x009a), top: B:28:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0097 A[Catch: all -> 0x009c, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x001d, B:7:0x0053, B:9:0x0057, B:13:0x0064, B:14:0x006a, B:16:0x0072, B:20:0x0081, B:22:0x0097, B:23:0x009a), top: B:28:0x0003 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean tryOpenCamera(androidx.camera.core.Camera r10) {
        /*
            r9 = this;
            java.lang.Object r0 = r9.mLock
            monitor-enter(r0)
            java.util.Map<androidx.camera.core.Camera, androidx.camera.core.impl.CameraStateRegistry$CameraRegistration> r1 = r9.mCameraStates     // Catch: all -> 0x009c
            java.lang.Object r1 = r1.get(r10)     // Catch: all -> 0x009c
            androidx.camera.core.impl.CameraStateRegistry$CameraRegistration r1 = (androidx.camera.core.impl.CameraStateRegistry.CameraRegistration) r1     // Catch: all -> 0x009c
            java.lang.String r2 = "Camera must first be registered with registerCamera()"
            java.lang.Object r1 = androidx.core.util.Preconditions.checkNotNull(r1, r2)     // Catch: all -> 0x009c
            androidx.camera.core.impl.CameraStateRegistry$CameraRegistration r1 = (androidx.camera.core.impl.CameraStateRegistry.CameraRegistration) r1     // Catch: all -> 0x009c
            java.lang.String r2 = "CameraStateRegistry"
            boolean r2 = androidx.camera.core.Logger.isDebugEnabled(r2)     // Catch: all -> 0x009c
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0053
            java.lang.StringBuilder r2 = r9.mDebugString     // Catch: all -> 0x009c
            r2.setLength(r4)     // Catch: all -> 0x009c
            java.lang.StringBuilder r2 = r9.mDebugString     // Catch: all -> 0x009c
            java.util.Locale r5 = java.util.Locale.US     // Catch: all -> 0x009c
            java.lang.String r6 = "tryOpenCamera(%s) [Available Cameras: %d, Already Open: %b (Previous state: %s)]"
            r7 = 4
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch: all -> 0x009c
            r7[r4] = r10     // Catch: all -> 0x009c
            int r10 = r9.mAvailableCameras     // Catch: all -> 0x009c
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch: all -> 0x009c
            r7[r3] = r10     // Catch: all -> 0x009c
            r10 = 2
            androidx.camera.core.impl.CameraInternal$State r8 = r1.getState()     // Catch: all -> 0x009c
            boolean r8 = isOpen(r8)     // Catch: all -> 0x009c
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch: all -> 0x009c
            r7[r10] = r8     // Catch: all -> 0x009c
            r10 = 3
            androidx.camera.core.impl.CameraInternal$State r8 = r1.getState()     // Catch: all -> 0x009c
            r7[r10] = r8     // Catch: all -> 0x009c
            java.lang.String r10 = java.lang.String.format(r5, r6, r7)     // Catch: all -> 0x009c
            r2.append(r10)     // Catch: all -> 0x009c
        L_0x0053:
            int r10 = r9.mAvailableCameras     // Catch: all -> 0x009c
            if (r10 > 0) goto L_0x0064
            androidx.camera.core.impl.CameraInternal$State r10 = r1.getState()     // Catch: all -> 0x009c
            boolean r10 = isOpen(r10)     // Catch: all -> 0x009c
            if (r10 == 0) goto L_0x0062
            goto L_0x0064
        L_0x0062:
            r10 = 0
            goto L_0x006a
        L_0x0064:
            androidx.camera.core.impl.CameraInternal$State r10 = androidx.camera.core.impl.CameraInternal.State.OPENING     // Catch: all -> 0x009c
            r1.setState(r10)     // Catch: all -> 0x009c
            r10 = 1
        L_0x006a:
            java.lang.String r1 = "CameraStateRegistry"
            boolean r1 = androidx.camera.core.Logger.isDebugEnabled(r1)     // Catch: all -> 0x009c
            if (r1 == 0) goto L_0x0095
            java.lang.StringBuilder r1 = r9.mDebugString     // Catch: all -> 0x009c
            java.util.Locale r2 = java.util.Locale.US     // Catch: all -> 0x009c
            java.lang.String r5 = " --> %s"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch: all -> 0x009c
            if (r10 == 0) goto L_0x007f
            java.lang.String r6 = "SUCCESS"
            goto L_0x0081
        L_0x007f:
            java.lang.String r6 = "FAIL"
        L_0x0081:
            r3[r4] = r6     // Catch: all -> 0x009c
            java.lang.String r2 = java.lang.String.format(r2, r5, r3)     // Catch: all -> 0x009c
            r1.append(r2)     // Catch: all -> 0x009c
            java.lang.String r1 = "CameraStateRegistry"
            java.lang.StringBuilder r2 = r9.mDebugString     // Catch: all -> 0x009c
            java.lang.String r2 = r2.toString()     // Catch: all -> 0x009c
            androidx.camera.core.Logger.d(r1, r2)     // Catch: all -> 0x009c
        L_0x0095:
            if (r10 == 0) goto L_0x009a
            r9.recalculateAvailableCameras()     // Catch: all -> 0x009c
        L_0x009a:
            monitor-exit(r0)     // Catch: all -> 0x009c
            return r10
        L_0x009c:
            r10 = move-exception
            monitor-exit(r0)     // Catch: all -> 0x009c
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.camera.core.impl.CameraStateRegistry.tryOpenCamera(androidx.camera.core.Camera):boolean");
    }

    public void markCameraState(Camera camera, CameraInternal.State state) {
        CameraInternal.State state2;
        List<CameraRegistration> list;
        synchronized (this.mLock) {
            int i = this.mAvailableCameras;
            if (state == CameraInternal.State.RELEASED) {
                state2 = unregisterCamera(camera);
            } else {
                state2 = updateAndVerifyState(camera, state);
            }
            if (state2 != state) {
                if (i >= 1 || this.mAvailableCameras <= 0) {
                    list = (state != CameraInternal.State.PENDING_OPEN || this.mAvailableCameras <= 0) ? null : Collections.singletonList(this.mCameraStates.get(camera));
                } else {
                    list = new ArrayList();
                    for (Map.Entry<Camera, CameraRegistration> entry : this.mCameraStates.entrySet()) {
                        if (entry.getValue().getState() == CameraInternal.State.PENDING_OPEN) {
                            list.add(entry.getValue());
                        }
                    }
                }
                if (list != null) {
                    for (CameraRegistration cameraRegistration : list) {
                        cameraRegistration.notifyListener();
                    }
                }
            }
        }
    }

    private CameraInternal.State unregisterCamera(Camera camera) {
        CameraRegistration remove = this.mCameraStates.remove(camera);
        if (remove == null) {
            return null;
        }
        recalculateAvailableCameras();
        return remove.getState();
    }

    private CameraInternal.State updateAndVerifyState(Camera camera, CameraInternal.State state) {
        CameraInternal.State state2 = ((CameraRegistration) Preconditions.checkNotNull(this.mCameraStates.get(camera), "Cannot update state of camera which has not yet been registered. Register with CameraAvailabilityRegistry.registerCamera()")).setState(state);
        CameraInternal.State state3 = CameraInternal.State.OPENING;
        if (state == state3) {
            Preconditions.checkState(isOpen(state) || state2 == state3, "Cannot mark camera as opening until camera was successful at calling CameraAvailabilityRegistry.tryOpen()");
        }
        if (state2 != state) {
            recalculateAvailableCameras();
        }
        return state2;
    }

    private static boolean isOpen(CameraInternal.State state) {
        return state != null && state.holdsCameraSlot();
    }

    private void recalculateAvailableCameras() {
        if (Logger.isDebugEnabled("CameraStateRegistry")) {
            this.mDebugString.setLength(0);
            this.mDebugString.append("Recalculating open cameras:\n");
            this.mDebugString.append(String.format(Locale.US, "%-45s%-22s\n", "Camera", "State"));
            this.mDebugString.append("-------------------------------------------------------------------\n");
        }
        int i = 0;
        for (Map.Entry<Camera, CameraRegistration> entry : this.mCameraStates.entrySet()) {
            if (Logger.isDebugEnabled("CameraStateRegistry")) {
                this.mDebugString.append(String.format(Locale.US, "%-45s%-22s\n", entry.getKey().toString(), entry.getValue().getState() != null ? entry.getValue().getState().toString() : "UNKNOWN"));
            }
            if (isOpen(entry.getValue().getState())) {
                i++;
            }
        }
        if (Logger.isDebugEnabled("CameraStateRegistry")) {
            this.mDebugString.append("-------------------------------------------------------------------\n");
            this.mDebugString.append(String.format(Locale.US, "Open count: %d (Max allowed: %d)", Integer.valueOf(i), Integer.valueOf(this.mMaxAllowedOpenedCameras)));
            Logger.d("CameraStateRegistry", this.mDebugString.toString());
        }
        this.mAvailableCameras = Math.max(this.mMaxAllowedOpenedCameras - i, 0);
    }

    /* loaded from: classes.dex */
    public static class CameraRegistration {
        private final OnOpenAvailableListener mCameraAvailableListener;
        private final Executor mNotifyExecutor;
        private CameraInternal.State mState;

        CameraRegistration(CameraInternal.State state, Executor executor, OnOpenAvailableListener onOpenAvailableListener) {
            this.mState = state;
            this.mNotifyExecutor = executor;
            this.mCameraAvailableListener = onOpenAvailableListener;
        }

        CameraInternal.State setState(CameraInternal.State state) {
            CameraInternal.State state2 = this.mState;
            this.mState = state;
            return state2;
        }

        CameraInternal.State getState() {
            return this.mState;
        }

        void notifyListener() {
            try {
                Executor executor = this.mNotifyExecutor;
                OnOpenAvailableListener onOpenAvailableListener = this.mCameraAvailableListener;
                Objects.requireNonNull(onOpenAvailableListener);
                executor.execute(new CameraStateRegistry$CameraRegistration$$ExternalSyntheticLambda0(onOpenAvailableListener));
            } catch (RejectedExecutionException e) {
                Logger.e("CameraStateRegistry", "Unable to notify camera.", e);
            }
        }
    }
}
