package androidx.camera.core;

import androidx.camera.core.impl.ImageReaderProxy;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.utils.futures.Futures;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes.dex */
public final class ImageAnalysisNonBlockingAnalyzer extends ImageAnalysisAbstractAnalyzer {
    final Executor mBackgroundExecutor;
    private ImageProxy mCachedImage;
    private final AtomicReference<CacheAnalyzingImageProxy> mPostedImage = new AtomicReference<>();
    private final AtomicLong mPostedImageTimestamp = new AtomicLong();

    public ImageAnalysisNonBlockingAnalyzer(Executor executor) {
        this.mBackgroundExecutor = executor;
        open();
    }

    @Override // androidx.camera.core.impl.ImageReaderProxy.OnImageAvailableListener
    public void onImageAvailable(ImageReaderProxy imageReaderProxy) {
        ImageProxy acquireLatestImage = imageReaderProxy.acquireLatestImage();
        if (acquireLatestImage != null) {
            analyze(acquireLatestImage);
        }
    }

    @Override // androidx.camera.core.ImageAnalysisAbstractAnalyzer
    public synchronized void open() {
        super.open();
        ImageProxy imageProxy = this.mCachedImage;
        if (imageProxy != null) {
            imageProxy.close();
            this.mCachedImage = null;
        }
    }

    @Override // androidx.camera.core.ImageAnalysisAbstractAnalyzer
    public synchronized void close() {
        super.close();
        ImageProxy imageProxy = this.mCachedImage;
        if (imageProxy != null) {
            imageProxy.close();
            this.mCachedImage = null;
        }
    }

    public synchronized void analyzeCachedImage() {
        ImageProxy imageProxy = this.mCachedImage;
        if (imageProxy != null) {
            this.mCachedImage = null;
            analyze(imageProxy);
        }
    }

    private synchronized void analyze(ImageProxy imageProxy) {
        if (isClosed()) {
            imageProxy.close();
            return;
        }
        CacheAnalyzingImageProxy cacheAnalyzingImageProxy = this.mPostedImage.get();
        if (cacheAnalyzingImageProxy != null && imageProxy.getImageInfo().getTimestamp() <= this.mPostedImageTimestamp.get()) {
            imageProxy.close();
        } else if (cacheAnalyzingImageProxy == null || cacheAnalyzingImageProxy.isClosed()) {
            final CacheAnalyzingImageProxy cacheAnalyzingImageProxy2 = new CacheAnalyzingImageProxy(imageProxy, this);
            this.mPostedImage.set(cacheAnalyzingImageProxy2);
            this.mPostedImageTimestamp.set(cacheAnalyzingImageProxy2.getImageInfo().getTimestamp());
            Futures.addCallback(analyzeImage(cacheAnalyzingImageProxy2), new FutureCallback<Void>() { // from class: androidx.camera.core.ImageAnalysisNonBlockingAnalyzer.1
                public void onSuccess(Void r1) {
                }

                @Override // androidx.camera.core.impl.utils.futures.FutureCallback
                public void onFailure(Throwable th) {
                    cacheAnalyzingImageProxy2.close();
                }
            }, CameraXExecutors.directExecutor());
        } else {
            ImageProxy imageProxy2 = this.mCachedImage;
            if (imageProxy2 != null) {
                imageProxy2.close();
            }
            this.mCachedImage = imageProxy;
        }
    }

    /* loaded from: classes.dex */
    public static class CacheAnalyzingImageProxy extends ForwardingImageProxy {
        private boolean mClosed = false;
        WeakReference<ImageAnalysisNonBlockingAnalyzer> mNonBlockingAnalyzerWeakReference;

        CacheAnalyzingImageProxy(ImageProxy imageProxy, ImageAnalysisNonBlockingAnalyzer imageAnalysisNonBlockingAnalyzer) {
            super(imageProxy);
            this.mNonBlockingAnalyzerWeakReference = new WeakReference<>(imageAnalysisNonBlockingAnalyzer);
            addOnImageCloseListener(new ImageAnalysisNonBlockingAnalyzer$CacheAnalyzingImageProxy$$ExternalSyntheticLambda0(this));
        }

        public /* synthetic */ void lambda$new$0(ImageProxy imageProxy) {
            this.mClosed = true;
            ImageAnalysisNonBlockingAnalyzer imageAnalysisNonBlockingAnalyzer = this.mNonBlockingAnalyzerWeakReference.get();
            if (imageAnalysisNonBlockingAnalyzer != null) {
                imageAnalysisNonBlockingAnalyzer.mBackgroundExecutor.execute(new ImageAnalysisNonBlockingAnalyzer$CacheAnalyzingImageProxy$$ExternalSyntheticLambda1(imageAnalysisNonBlockingAnalyzer));
            }
        }

        boolean isClosed() {
            return this.mClosed;
        }
    }
}
