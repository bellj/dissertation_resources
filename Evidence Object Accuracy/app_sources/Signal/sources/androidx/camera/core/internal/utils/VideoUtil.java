package androidx.camera.core.internal.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import androidx.camera.core.Logger;
import androidx.core.util.Preconditions;
import org.thoughtcrime.securesms.database.AttachmentDatabase;

/* loaded from: classes.dex */
public final class VideoUtil {
    public static String getAbsolutePathFromUri(ContentResolver contentResolver, Uri uri) {
        Throwable th;
        Cursor cursor;
        RuntimeException e;
        Cursor cursor2;
        try {
            cursor = null;
            try {
                cursor = contentResolver.query(uri, new String[]{AttachmentDatabase.DATA}, null, null, null);
                cursor2 = (Cursor) Preconditions.checkNotNull(cursor);
            } catch (RuntimeException e2) {
                e = e2;
            }
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            int columnIndexOrThrow = cursor2.getColumnIndexOrThrow(AttachmentDatabase.DATA);
            cursor2.moveToFirst();
            String string = cursor2.getString(columnIndexOrThrow);
            cursor2.close();
            return string;
        } catch (RuntimeException e3) {
            e = e3;
            cursor = cursor2;
            Logger.e("VideoUtil", String.format("Failed in getting absolute path for Uri %s with Exception %s", uri.toString(), e.toString()));
            if (cursor != null) {
                cursor.close();
            }
            return "";
        } catch (Throwable th3) {
            th = th3;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
    }
}
