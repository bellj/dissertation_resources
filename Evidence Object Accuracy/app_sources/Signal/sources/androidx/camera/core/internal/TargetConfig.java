package androidx.camera.core.internal;

import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;

/* loaded from: classes.dex */
public interface TargetConfig<T> extends ReadableConfig {
    public static final Config.Option<Class<?>> OPTION_TARGET_CLASS = Config.Option.create("camerax.core.target.class", Class.class);
    public static final Config.Option<String> OPTION_TARGET_NAME = Config.Option.create("camerax.core.target.name", String.class);

    String getTargetName(String str);

    /* renamed from: androidx.camera.core.internal.TargetConfig$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC<T> {
        public static String $default$getTargetName(TargetConfig targetConfig, String str) {
            return (String) targetConfig.retrieveOption(TargetConfig.OPTION_TARGET_NAME, str);
        }
    }
}
