package androidx.camera.core.internal.utils;

import android.graphics.Rect;
import android.util.Rational;
import android.util.Size;
import androidx.camera.core.Logger;

/* loaded from: classes.dex */
public final class ImageUtil {
    public static float[] sizeToVertexes(Size size) {
        return new float[]{0.0f, 0.0f, (float) size.getWidth(), 0.0f, (float) size.getWidth(), (float) size.getHeight(), 0.0f, (float) size.getHeight()};
    }

    public static float min(float f, float f2, float f3, float f4) {
        return Math.min(Math.min(f, f2), Math.min(f3, f4));
    }

    public static Rational getRotatedAspectRatio(int i, Rational rational) {
        if (i == 90 || i == 270) {
            return inverseRational(rational);
        }
        return new Rational(rational.getNumerator(), rational.getDenominator());
    }

    public static boolean isAspectRatioValid(Rational rational) {
        return rational != null && rational.floatValue() > 0.0f && !rational.isNaN();
    }

    public static boolean isAspectRatioValid(Size size, Rational rational) {
        return rational != null && rational.floatValue() > 0.0f && isCropAspectRatioHasEffect(size, rational) && !rational.isNaN();
    }

    public static Rect computeCropRectFromAspectRatio(Size size, Rational rational) {
        int i;
        if (!isAspectRatioValid(rational)) {
            Logger.w("ImageUtil", "Invalid view ratio.");
            return null;
        }
        int width = size.getWidth();
        int height = size.getHeight();
        float f = (float) width;
        float f2 = (float) height;
        int numerator = rational.getNumerator();
        int denominator = rational.getDenominator();
        int i2 = 0;
        if (rational.floatValue() > f / f2) {
            int round = Math.round((f / ((float) numerator)) * ((float) denominator));
            i = (height - round) / 2;
            height = round;
        } else {
            int round2 = Math.round((f2 / ((float) denominator)) * ((float) numerator));
            i2 = (width - round2) / 2;
            width = round2;
            i = 0;
        }
        return new Rect(i2, i, width + i2, height + i);
    }

    private static boolean isCropAspectRatioHasEffect(Size size, Rational rational) {
        int width = size.getWidth();
        int height = size.getHeight();
        float numerator = (float) rational.getNumerator();
        float denominator = (float) rational.getDenominator();
        return (height == Math.round((((float) width) / numerator) * denominator) && width == Math.round((((float) height) / denominator) * numerator)) ? false : true;
    }

    private static Rational inverseRational(Rational rational) {
        if (rational == null) {
            return rational;
        }
        return new Rational(rational.getDenominator(), rational.getNumerator());
    }
}
