package androidx.camera.core.internal;

import androidx.camera.core.UseCase;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;

/* loaded from: classes.dex */
public interface UseCaseEventConfig extends ReadableConfig {
    public static final Config.Option<UseCase.EventCallback> OPTION_USE_CASE_EVENT_CALLBACK = Config.Option.create("camerax.core.useCaseEventCallback", UseCase.EventCallback.class);

    UseCase.EventCallback getUseCaseEventCallback(UseCase.EventCallback eventCallback);

    /* renamed from: androidx.camera.core.internal.UseCaseEventConfig$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static UseCase.EventCallback $default$getUseCaseEventCallback(UseCaseEventConfig useCaseEventConfig, UseCase.EventCallback eventCallback) {
            return (UseCase.EventCallback) useCaseEventConfig.retrieveOption(UseCaseEventConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
        }
    }
}
