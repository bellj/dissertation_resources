package androidx.camera.core.internal;

import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public interface ThreadConfig extends ReadableConfig {
    public static final Config.Option<Executor> OPTION_BACKGROUND_EXECUTOR = Config.Option.create("camerax.core.thread.backgroundExecutor", Executor.class);

    /* renamed from: androidx.camera.core.internal.ThreadConfig$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static Executor $default$getBackgroundExecutor(ThreadConfig threadConfig, Executor executor) {
            return (Executor) threadConfig.retrieveOption(ThreadConfig.OPTION_BACKGROUND_EXECUTOR, executor);
        }
    }
}
