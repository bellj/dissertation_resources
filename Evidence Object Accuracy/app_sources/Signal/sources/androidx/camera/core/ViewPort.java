package androidx.camera.core;

import android.util.Rational;
import androidx.core.util.Preconditions;

/* loaded from: classes.dex */
public final class ViewPort {
    private Rational mAspectRatio;
    private int mLayoutDirection;
    private int mRotation;
    private int mScaleType;

    ViewPort(int i, Rational rational, int i2, int i3) {
        this.mScaleType = i;
        this.mAspectRatio = rational;
        this.mRotation = i2;
        this.mLayoutDirection = i3;
    }

    public Rational getAspectRatio() {
        return this.mAspectRatio;
    }

    public int getRotation() {
        return this.mRotation;
    }

    public int getScaleType() {
        return this.mScaleType;
    }

    public int getLayoutDirection() {
        return this.mLayoutDirection;
    }

    /* loaded from: classes.dex */
    public static final class Builder {
        private final Rational mAspectRatio;
        private int mLayoutDirection = 0;
        private final int mRotation;
        private int mScaleType = 1;

        public Builder(Rational rational, int i) {
            this.mAspectRatio = rational;
            this.mRotation = i;
        }

        public Builder setScaleType(int i) {
            this.mScaleType = i;
            return this;
        }

        public Builder setLayoutDirection(int i) {
            this.mLayoutDirection = i;
            return this;
        }

        public ViewPort build() {
            Preconditions.checkNotNull(this.mAspectRatio, "The crop aspect ratio must be set.");
            return new ViewPort(this.mScaleType, this.mAspectRatio, this.mRotation, this.mLayoutDirection);
        }
    }
}
