package androidx.camera.core;

/* loaded from: classes.dex */
public final class SingleCloseImageProxy extends ForwardingImageProxy {
    private boolean mClosed = false;

    public SingleCloseImageProxy(ImageProxy imageProxy) {
        super(imageProxy);
    }

    @Override // androidx.camera.core.ForwardingImageProxy, androidx.camera.core.ImageProxy, java.lang.AutoCloseable
    public synchronized void close() {
        if (!this.mClosed) {
            this.mClosed = true;
            super.close();
        }
    }
}
