package androidx.camera.core;

import android.graphics.Rect;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public interface ImageProxy extends AutoCloseable {

    /* loaded from: classes.dex */
    public interface PlaneProxy {
        ByteBuffer getBuffer();

        int getRowStride();
    }

    @Override // java.lang.AutoCloseable
    void close();

    Rect getCropRect();

    int getFormat();

    int getHeight();

    ImageInfo getImageInfo();

    PlaneProxy[] getPlanes();

    int getWidth();

    void setCropRect(Rect rect);
}
