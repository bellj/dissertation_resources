package androidx.camera.core;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class ImageAnalysis$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SafeCloseImageReaderProxy f$0;

    public /* synthetic */ ImageAnalysis$$ExternalSyntheticLambda0(SafeCloseImageReaderProxy safeCloseImageReaderProxy) {
        this.f$0 = safeCloseImageReaderProxy;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.safeClose();
    }
}
