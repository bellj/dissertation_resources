package androidx.camera.core;

import androidx.camera.core.impl.TagBundle;

/* loaded from: classes.dex */
public abstract class ImmutableImageInfo implements ImageInfo {
    @Override // androidx.camera.core.ImageInfo
    public abstract int getRotationDegrees();

    @Override // androidx.camera.core.ImageInfo
    public abstract TagBundle getTagBundle();

    @Override // androidx.camera.core.ImageInfo
    public abstract long getTimestamp();

    public static ImageInfo create(TagBundle tagBundle, long j, int i) {
        return new AutoValue_ImmutableImageInfo(tagBundle, j, i);
    }
}
