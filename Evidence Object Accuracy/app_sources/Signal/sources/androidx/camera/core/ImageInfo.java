package androidx.camera.core;

import androidx.camera.core.impl.TagBundle;

/* loaded from: classes.dex */
public interface ImageInfo {
    int getRotationDegrees();

    TagBundle getTagBundle();

    long getTimestamp();
}
