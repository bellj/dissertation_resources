package androidx.camera.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Size;
import android.util.SizeF;
import android.view.Display;
import android.view.TextureView;
import android.view.View;
import androidx.camera.core.Logger;
import androidx.camera.core.SurfaceRequest;
import androidx.camera.view.PreviewView;
import androidx.core.util.Preconditions;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

/* loaded from: classes.dex */
public final class PreviewTransformation {
    private static final PreviewView.ScaleType DEFAULT_SCALE_TYPE = PreviewView.ScaleType.FILL_CENTER;
    private boolean mIsFrontCamera;
    private int mPreviewRotationDegrees;
    private Size mResolution;
    private PreviewView.ScaleType mScaleType = DEFAULT_SCALE_TYPE;
    private Rect mSurfaceCropRect;
    private int mTargetRotation;

    public void setTransformationInfo(SurfaceRequest.TransformationInfo transformationInfo, Size size, boolean z) {
        Logger.d("PreviewTransform", "Transformation info set: " + transformationInfo + " " + size + " " + z);
        this.mSurfaceCropRect = transformationInfo.getCropRect();
        this.mPreviewRotationDegrees = transformationInfo.getRotationDegrees();
        this.mTargetRotation = transformationInfo.getTargetRotation();
        this.mResolution = size;
        this.mIsFrontCamera = z;
    }

    Matrix getTextureViewCorrectionMatrix() {
        Preconditions.checkState(isTransformationInfoReady());
        Matrix matrix = new Matrix();
        float[] sizeToVertices = sizeToVertices(this.mResolution);
        matrix.setPolyToPoly(sizeToVertices, 0, createRotatedVertices(sizeToVertices, -rotationValueToRotationDegrees(this.mTargetRotation)), 0, 4);
        return matrix;
    }

    public void transformView(Size size, int i, View view) {
        if (isTransformationInfoReady()) {
            if (view instanceof TextureView) {
                ((TextureView) view).setTransform(getTextureViewCorrectionMatrix());
            } else {
                Display display = view.getDisplay();
                if (!(display == null || display.getRotation() == this.mTargetRotation)) {
                    Logger.e("PreviewTransform", "Non-display rotation not supported with SurfaceView / PERFORMANCE mode.");
                }
            }
            RectF transformedSurfaceRect = getTransformedSurfaceRect(size, i);
            view.setPivotX(0.0f);
            view.setPivotY(0.0f);
            view.setScaleX(transformedSurfaceRect.width() / ((float) this.mResolution.getWidth()));
            view.setScaleY(transformedSurfaceRect.height() / ((float) this.mResolution.getHeight()));
            view.setTranslationX(transformedSurfaceRect.left - ((float) view.getLeft()));
            view.setTranslationY(transformedSurfaceRect.top - ((float) view.getTop()));
        }
    }

    public void setScaleType(PreviewView.ScaleType scaleType) {
        this.mScaleType = scaleType;
    }

    public PreviewView.ScaleType getScaleType() {
        return this.mScaleType;
    }

    private RectF getTransformedSurfaceRect(Size size, int i) {
        Preconditions.checkState(isTransformationInfoReady());
        Matrix surfaceToPreviewViewMatrix = getSurfaceToPreviewViewMatrix(size, i);
        float[] sizeToVertices = sizeToVertices(this.mResolution);
        surfaceToPreviewViewMatrix.mapPoints(sizeToVertices);
        return verticesToRect(sizeToVertices);
    }

    private Matrix getSurfaceToPreviewViewMatrix(Size size, int i) {
        float[] fArr;
        Preconditions.checkState(isTransformationInfoReady());
        Matrix matrix = new Matrix();
        if (isCropRectAspectRatioMatchPreviewView(size)) {
            fArr = sizeToVertices(size);
        } else {
            fArr = rectToVertices(getPreviewViewCropRectForMismatchedAspectRatios(size, i));
        }
        matrix.setPolyToPoly(rectToVertices(new RectF(this.mSurfaceCropRect)), 0, createRotatedVertices(fArr, this.mPreviewRotationDegrees), 0, 4);
        if (this.mIsFrontCamera) {
            if (is90or270(this.mPreviewRotationDegrees)) {
                matrix.preScale(1.0f, -1.0f, (float) this.mSurfaceCropRect.centerX(), (float) this.mSurfaceCropRect.centerY());
            } else {
                matrix.preScale(-1.0f, 1.0f, (float) this.mSurfaceCropRect.centerX(), (float) this.mSurfaceCropRect.centerY());
            }
        }
        return matrix;
    }

    RectF getPreviewViewCropRectForMismatchedAspectRatios(Size size, int i) {
        RectF rectF = new RectF(0.0f, 0.0f, (float) size.getWidth(), (float) size.getHeight());
        SizeF rotatedCropRectSize = getRotatedCropRectSize();
        RectF rectF2 = new RectF(0.0f, 0.0f, rotatedCropRectSize.getWidth(), rotatedCropRectSize.getHeight());
        Matrix matrix = new Matrix();
        setMatrixRectToRect(matrix, rectF2, rectF, this.mScaleType);
        matrix.mapRect(rectF2);
        return i == 1 ? flipHorizontally(rectF2, ((float) size.getWidth()) / 2.0f) : rectF2;
    }

    /* renamed from: androidx.camera.view.PreviewTransformation$1 */
    /* loaded from: classes.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$androidx$camera$view$PreviewView$ScaleType;

        static {
            int[] iArr = new int[PreviewView.ScaleType.values().length];
            $SwitchMap$androidx$camera$view$PreviewView$ScaleType = iArr;
            try {
                iArr[PreviewView.ScaleType.FIT_CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$androidx$camera$view$PreviewView$ScaleType[PreviewView.ScaleType.FILL_CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$androidx$camera$view$PreviewView$ScaleType[PreviewView.ScaleType.FIT_END.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$androidx$camera$view$PreviewView$ScaleType[PreviewView.ScaleType.FILL_END.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$androidx$camera$view$PreviewView$ScaleType[PreviewView.ScaleType.FIT_START.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$androidx$camera$view$PreviewView$ScaleType[PreviewView.ScaleType.FILL_START.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    private static void setMatrixRectToRect(Matrix matrix, RectF rectF, RectF rectF2, PreviewView.ScaleType scaleType) {
        Matrix.ScaleToFit scaleToFit;
        switch (AnonymousClass1.$SwitchMap$androidx$camera$view$PreviewView$ScaleType[scaleType.ordinal()]) {
            case 1:
            case 2:
                scaleToFit = Matrix.ScaleToFit.CENTER;
                break;
            case 3:
            case 4:
                scaleToFit = Matrix.ScaleToFit.END;
                break;
            case 5:
            case 6:
                scaleToFit = Matrix.ScaleToFit.START;
                break;
            default:
                Logger.e("PreviewTransform", "Unexpected crop rect: " + scaleType);
                scaleToFit = Matrix.ScaleToFit.FILL;
                break;
        }
        if (scaleType == PreviewView.ScaleType.FIT_CENTER || scaleType == PreviewView.ScaleType.FIT_START || scaleType == PreviewView.ScaleType.FIT_END) {
            matrix.setRectToRect(rectF, rectF2, scaleToFit);
            return;
        }
        matrix.setRectToRect(rectF2, rectF, scaleToFit);
        matrix.invert(matrix);
    }

    private static RectF flipHorizontally(RectF rectF, float f) {
        float f2 = f + f;
        return new RectF(f2 - rectF.right, rectF.top, f2 - rectF.left, rectF.bottom);
    }

    private SizeF getRotatedCropRectSize() {
        Preconditions.checkNotNull(this.mSurfaceCropRect);
        if (is90or270(this.mPreviewRotationDegrees)) {
            return new SizeF((float) this.mSurfaceCropRect.height(), (float) this.mSurfaceCropRect.width());
        }
        return new SizeF((float) this.mSurfaceCropRect.width(), (float) this.mSurfaceCropRect.height());
    }

    boolean isCropRectAspectRatioMatchPreviewView(Size size) {
        float width = ((float) size.getWidth()) / ((float) size.getHeight());
        SizeF rotatedCropRectSize = getRotatedCropRectSize();
        return width >= (rotatedCropRectSize.getWidth() - 0.5f) / (rotatedCropRectSize.getHeight() + 0.5f) && width <= (rotatedCropRectSize.getWidth() + 0.5f) / (rotatedCropRectSize.getHeight() - 0.5f);
    }

    public Bitmap createTransformedBitmap(Bitmap bitmap, Size size, int i) {
        if (!isTransformationInfoReady()) {
            return bitmap;
        }
        Matrix textureViewCorrectionMatrix = getTextureViewCorrectionMatrix();
        RectF transformedSurfaceRect = getTransformedSurfaceRect(size, i);
        Bitmap createBitmap = Bitmap.createBitmap(size.getWidth(), size.getHeight(), bitmap.getConfig());
        Canvas canvas = new Canvas(createBitmap);
        Matrix matrix = new Matrix();
        matrix.postConcat(textureViewCorrectionMatrix);
        matrix.postScale(transformedSurfaceRect.width() / ((float) this.mResolution.getWidth()), transformedSurfaceRect.height() / ((float) this.mResolution.getHeight()));
        matrix.postTranslate(transformedSurfaceRect.left, transformedSurfaceRect.top);
        canvas.drawBitmap(bitmap, matrix, new Paint(7));
        return createBitmap;
    }

    public Matrix getPreviewViewToNormalizedSurfaceMatrix(Size size, int i) {
        if (!isTransformationInfoReady()) {
            return null;
        }
        Matrix matrix = new Matrix();
        getSurfaceToPreviewViewMatrix(size, i).invert(matrix);
        Matrix matrix2 = new Matrix();
        matrix2.setRectToRect(new RectF(0.0f, 0.0f, (float) this.mResolution.getWidth(), (float) this.mResolution.getHeight()), new RectF(0.0f, 0.0f, 1.0f, 1.0f), Matrix.ScaleToFit.FILL);
        matrix.postConcat(matrix2);
        return matrix;
    }

    static int rotationValueToRotationDegrees(int i) {
        if (i == 0) {
            return 0;
        }
        if (i == 1) {
            return 90;
        }
        if (i == 2) {
            return SubsamplingScaleImageView.ORIENTATION_180;
        }
        if (i == 3) {
            return SubsamplingScaleImageView.ORIENTATION_270;
        }
        throw new IllegalStateException("Unexpected rotation value " + i);
    }

    private static boolean is90or270(int i) {
        if (i == 90 || i == 270) {
            return true;
        }
        if (i == 0 || i == 180) {
            return false;
        }
        throw new IllegalArgumentException("Invalid rotation degrees: " + i);
    }

    static float[] sizeToVertices(Size size) {
        return new float[]{0.0f, 0.0f, (float) size.getWidth(), 0.0f, (float) size.getWidth(), (float) size.getHeight(), 0.0f, (float) size.getHeight()};
    }

    private static float[] rectToVertices(RectF rectF) {
        float f = rectF.left;
        float f2 = rectF.top;
        float f3 = rectF.right;
        float f4 = rectF.bottom;
        return new float[]{f, f2, f3, f2, f3, f4, f, f4};
    }

    private static RectF verticesToRect(float[] fArr) {
        return new RectF(min(fArr[0], fArr[2], fArr[4], fArr[6]), min(fArr[1], fArr[3], fArr[5], fArr[7]), max(fArr[0], fArr[2], fArr[4], fArr[6]), max(fArr[1], fArr[3], fArr[5], fArr[7]));
    }

    private static float max(float f, float f2, float f3, float f4) {
        return Math.max(Math.max(f, f2), Math.max(f3, f4));
    }

    private static float min(float f, float f2, float f3, float f4) {
        return Math.min(Math.min(f, f2), Math.min(f3, f4));
    }

    private boolean isTransformationInfoReady() {
        return (this.mSurfaceCropRect == null || this.mResolution == null) ? false : true;
    }

    private static float[] createRotatedVertices(float[] fArr, int i) {
        float[] fArr2 = new float[fArr.length];
        int i2 = ((-i) / 90) * 2;
        for (int i3 = 0; i3 < fArr.length; i3++) {
            int length = (i3 + i2) % fArr.length;
            if (length < 0) {
                length += fArr.length;
            }
            fArr2[length] = fArr[i3];
        }
        return fArr2;
    }
}
