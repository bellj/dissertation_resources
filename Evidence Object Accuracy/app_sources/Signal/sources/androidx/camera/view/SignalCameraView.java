package androidx.camera.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import androidx.camera.core.Camera;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.FocusMeteringResult;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Logger;
import androidx.camera.core.MeteringPointFactory;
import androidx.camera.core.VideoCapture;
import androidx.camera.core.impl.LensFacingConverter;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.view.PreviewView;
import androidx.core.util.Consumer;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import io.reactivex.rxjava3.disposables.Disposable;
import java.util.concurrent.Executor;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mediasend.RotationListener;

/* loaded from: classes.dex */
public final class SignalCameraView extends FrameLayout {
    static final String TAG = Log.tag(SignalCameraView.class);
    private Consumer<Throwable> errorConsumer;
    SignalCameraXModule mCameraModule;
    private final DisplayManager.DisplayListener mDisplayListener;
    private long mDownEventTimestamp;
    private boolean mIsPinchToZoomEnabled;
    private PinchToZoomGestureDetector mPinchToZoomGestureDetector;
    private PreviewView mPreviewView;
    private MotionEvent mUpEvent;
    private Throwable pendingError;
    private RotationListener.Rotation rotation;
    private Disposable rotationDisposable;
    private RotationListener rotationListener;

    public SignalCameraView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SignalCameraView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mIsPinchToZoomEnabled = true;
        this.mDisplayListener = new DisplayManager.DisplayListener() { // from class: androidx.camera.view.SignalCameraView.1
            @Override // android.hardware.display.DisplayManager.DisplayListener
            public void onDisplayAdded(int i2) {
            }

            @Override // android.hardware.display.DisplayManager.DisplayListener
            public void onDisplayRemoved(int i2) {
            }

            @Override // android.hardware.display.DisplayManager.DisplayListener
            public void onDisplayChanged(int i2) {
                SignalCameraView.this.mCameraModule.invalidateView();
            }
        };
        init(context, attributeSet);
    }

    public void bindToLifecycle(LifecycleOwner lifecycleOwner, Consumer<Throwable> consumer) {
        this.mCameraModule.bindToLifecycle(lifecycleOwner);
        this.errorConsumer = consumer;
        Throwable th = this.pendingError;
        if (th != null) {
            consumer.accept(th);
        }
    }

    private void init(Context context, AttributeSet attributeSet) {
        PreviewView previewView = new PreviewView(getContext());
        this.mPreviewView = previewView;
        addView(previewView, 0);
        this.rotationListener = new RotationListener(context);
        this.mPreviewView.setImplementationMode(PreviewView.ImplementationMode.COMPATIBLE);
        this.mCameraModule = new SignalCameraXModule(this, new Consumer() { // from class: androidx.camera.view.SignalCameraView$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                SignalCameraView.$r8$lambda$CQvHIXbtrrZTfb41kAceJnqYSA8(SignalCameraView.this, (Throwable) obj);
            }
        });
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.CameraView);
            setScaleType(PreviewView.ScaleType.fromId(obtainStyledAttributes.getInteger(5, getScaleType().getId())));
            setPinchToZoomEnabled(obtainStyledAttributes.getBoolean(4, isPinchToZoomEnabled()));
            setCaptureMode(CaptureMode.fromId(obtainStyledAttributes.getInteger(1, getCaptureMode().getId())));
            int i = obtainStyledAttributes.getInt(3, 2);
            if (i == 0) {
                setCameraLensFacing(null);
            } else if (i == 1) {
                setCameraLensFacing(0);
            } else if (i == 2) {
                setCameraLensFacing(1);
            }
            int i2 = obtainStyledAttributes.getInt(2, 0);
            if (i2 == 1) {
                setFlash(0);
            } else if (i2 == 2) {
                setFlash(1);
            } else if (i2 == 4) {
                setFlash(2);
            }
            obtainStyledAttributes.recycle();
        }
        if (getBackground() == null) {
            setBackgroundColor(-15658735);
        }
        this.mPinchToZoomGestureDetector = new PinchToZoomGestureDetector(this, context);
    }

    public /* synthetic */ void lambda$init$0(Throwable th) {
        Consumer<Throwable> consumer = this.errorConsumer;
        if (consumer != null) {
            consumer.accept(th);
        } else {
            this.pendingError = th;
        }
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public FrameLayout.LayoutParams generateDefaultLayoutParams() {
        return new FrameLayout.LayoutParams(-1, -1);
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("super", super.onSaveInstanceState());
        bundle.putInt("scale_type", getScaleType().getId());
        bundle.putFloat("zoom_ratio", getZoomRatio());
        bundle.putBoolean("pinch_to_zoom_enabled", isPinchToZoomEnabled());
        bundle.putString("flash", FlashModeConverter.nameOf(getFlash()));
        bundle.putLong("max_video_duration", getMaxVideoDuration());
        bundle.putLong("max_video_size", getMaxVideoSize());
        if (getCameraLensFacing() != null) {
            bundle.putString("camera_direction", LensFacingConverter.nameOf(getCameraLensFacing().intValue()));
        }
        bundle.putInt("captureMode", getCaptureMode().getId());
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        Integer num;
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            super.onRestoreInstanceState(bundle.getParcelable("super"));
            setScaleType(PreviewView.ScaleType.fromId(bundle.getInt("scale_type")));
            setZoomRatio(bundle.getFloat("zoom_ratio"));
            setPinchToZoomEnabled(bundle.getBoolean("pinch_to_zoom_enabled"));
            setFlash(FlashModeConverter.valueOf(bundle.getString("flash")));
            setMaxVideoDuration(bundle.getLong("max_video_duration"));
            setMaxVideoSize(bundle.getLong("max_video_size"));
            String string = bundle.getString("camera_direction");
            if (TextUtils.isEmpty(string)) {
                num = null;
            } else {
                num = Integer.valueOf(LensFacingConverter.valueOf(string));
            }
            setCameraLensFacing(num);
            setCaptureMode(CaptureMode.fromId(bundle.getInt("captureMode")));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ((DisplayManager) getContext().getSystemService("display")).registerDisplayListener(this.mDisplayListener, new Handler(Looper.getMainLooper()));
        this.rotationDisposable = this.rotationListener.getObservable().distinctUntilChanged().subscribe(new io.reactivex.rxjava3.functions.Consumer() { // from class: androidx.camera.view.SignalCameraView$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SignalCameraView.m40$r8$lambda$_0yvX8Cd6ScQI3PAb9g2wZjBG4(SignalCameraView.this, (RotationListener.Rotation) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onAttachedToWindow$1(RotationListener.Rotation rotation) throws Throwable {
        this.rotation = rotation;
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ((DisplayManager) getContext().getSystemService("display")).unregisterDisplayListener(this.mDisplayListener);
        this.rotationDisposable.dispose();
    }

    public LiveData<PreviewView.StreamState> getPreviewStreamState() {
        return this.mPreviewView.getPreviewStreamState();
    }

    public PreviewView getPreviewView() {
        return this.mPreviewView;
    }

    @Override // android.widget.FrameLayout, android.view.View
    protected void onMeasure(int i, int i2) {
        if (getMeasuredWidth() > 0 && getMeasuredHeight() > 0) {
            this.mCameraModule.bindToLifecycleAfterViewMeasured();
        }
        super.onMeasure(i, i2);
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.mCameraModule.bindToLifecycleAfterViewMeasured();
        this.mCameraModule.invalidateView();
        super.onLayout(z, i, i2, i3, i4);
    }

    public int getDisplaySurfaceRotation() {
        RotationListener.Rotation rotation = this.rotation;
        if (rotation != null) {
            return rotation.getSurfaceRotation();
        }
        Display display = getDisplay();
        if (display == null) {
            return 0;
        }
        return display.getRotation();
    }

    public PreviewView.ScaleType getScaleType() {
        return this.mPreviewView.getScaleType();
    }

    public void setScaleType(PreviewView.ScaleType scaleType) {
        this.mPreviewView.setScaleType(scaleType);
    }

    public CaptureMode getCaptureMode() {
        return this.mCameraModule.getCaptureMode();
    }

    public void setCaptureMode(CaptureMode captureMode) {
        this.mCameraModule.setCaptureMode(captureMode);
    }

    public long getMaxVideoDuration() {
        return this.mCameraModule.getMaxVideoDuration();
    }

    private void setMaxVideoDuration(long j) {
        this.mCameraModule.setMaxVideoDuration(j);
    }

    private long getMaxVideoSize() {
        return this.mCameraModule.getMaxVideoSize();
    }

    private void setMaxVideoSize(long j) {
        this.mCameraModule.setMaxVideoSize(j);
    }

    public void takePicture(Executor executor, ImageCapture.OnImageCapturedCallback onImageCapturedCallback) {
        this.mCameraModule.takePicture(executor, onImageCapturedCallback);
    }

    public void startRecording(VideoCapture.OutputFileOptions outputFileOptions, Executor executor, VideoCapture.OnVideoSavedCallback onVideoSavedCallback) {
        this.mCameraModule.startRecording(outputFileOptions, executor, onVideoSavedCallback);
    }

    public void stopRecording() {
        this.mCameraModule.stopRecording();
    }

    public boolean hasCameraWithLensFacing(int i) {
        return this.mCameraModule.hasCameraWithLensFacing(i);
    }

    public void toggleCamera() {
        this.mCameraModule.toggleCamera();
    }

    public void setCameraLensFacing(Integer num) {
        this.mCameraModule.setCameraLensFacing(num);
    }

    public Integer getCameraLensFacing() {
        return this.mCameraModule.getLensFacing();
    }

    public int getFlash() {
        return this.mCameraModule.getFlash();
    }

    public boolean hasFlash() {
        return this.mCameraModule.hasFlash();
    }

    public void setFlash(int i) {
        this.mCameraModule.setFlash(i);
    }

    private long delta() {
        return System.currentTimeMillis() - this.mDownEventTimestamp;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.mCameraModule.isPaused()) {
            return false;
        }
        if (isPinchToZoomEnabled()) {
            this.mPinchToZoomGestureDetector.onTouchEvent(motionEvent);
        }
        if (motionEvent.getPointerCount() == 2 && isPinchToZoomEnabled() && isZoomSupported()) {
            return true;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            this.mDownEventTimestamp = System.currentTimeMillis();
        } else if (action != 1) {
            return false;
        } else {
            if (delta() < ((long) ViewConfiguration.getLongPressTimeout()) && this.mCameraModule.isBoundToLifecycle()) {
                this.mUpEvent = motionEvent;
                performClick();
            }
        }
        return true;
    }

    @Override // android.view.View
    public boolean performClick() {
        super.performClick();
        MotionEvent motionEvent = this.mUpEvent;
        float x = motionEvent != null ? motionEvent.getX() : getX() + (((float) getWidth()) / 2.0f);
        MotionEvent motionEvent2 = this.mUpEvent;
        float y = motionEvent2 != null ? motionEvent2.getY() : getY() + (((float) getHeight()) / 2.0f);
        this.mUpEvent = null;
        Camera camera = this.mCameraModule.getCamera();
        if (camera != null) {
            MeteringPointFactory meteringPointFactory = this.mPreviewView.getMeteringPointFactory();
            Futures.addCallback(camera.getCameraControl().startFocusAndMetering(new FocusMeteringAction.Builder(meteringPointFactory.createPoint(x, y, 0.16666667f), 1).addPoint(meteringPointFactory.createPoint(x, y, 0.25f), 2).build()), new FutureCallback<FocusMeteringResult>() { // from class: androidx.camera.view.SignalCameraView.2
                public void onSuccess(FocusMeteringResult focusMeteringResult) {
                }

                @Override // androidx.camera.core.impl.utils.futures.FutureCallback
                public void onFailure(Throwable th) {
                    throw new RuntimeException(th);
                }
            }, CameraXExecutors.directExecutor());
        } else {
            Logger.d(TAG, "cannot access camera");
        }
        return true;
    }

    float rangeLimit(float f, float f2, float f3) {
        return Math.min(Math.max(f, f3), f2);
    }

    public boolean isPinchToZoomEnabled() {
        return this.mIsPinchToZoomEnabled;
    }

    public void setPinchToZoomEnabled(boolean z) {
        this.mIsPinchToZoomEnabled = z;
    }

    public float getZoomRatio() {
        return this.mCameraModule.getZoomRatio();
    }

    public void setZoomRatio(float f) {
        this.mCameraModule.setZoomRatio(f);
    }

    public float getMinZoomRatio() {
        return this.mCameraModule.getMinZoomRatio();
    }

    public float getMaxZoomRatio() {
        return this.mCameraModule.getMaxZoomRatio();
    }

    public boolean isZoomSupported() {
        return this.mCameraModule.isZoomSupported();
    }

    /* loaded from: classes.dex */
    public enum CaptureMode {
        IMAGE(0),
        VIDEO(1),
        MIXED(2);
        
        private final int mId;

        int getId() {
            return this.mId;
        }

        CaptureMode(int i) {
            this.mId = i;
        }

        static CaptureMode fromId(int i) {
            CaptureMode[] values = values();
            for (CaptureMode captureMode : values) {
                if (captureMode.mId == i) {
                    return captureMode;
                }
            }
            throw new IllegalArgumentException();
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes.dex */
    public static class S extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private ScaleGestureDetector.OnScaleGestureListener mListener;

        S() {
        }

        void setRealGestureDetector(ScaleGestureDetector.OnScaleGestureListener onScaleGestureListener) {
            this.mListener = onScaleGestureListener;
        }

        @Override // android.view.ScaleGestureDetector.SimpleOnScaleGestureListener, android.view.ScaleGestureDetector.OnScaleGestureListener
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            return this.mListener.onScale(scaleGestureDetector);
        }
    }

    /* loaded from: classes.dex */
    public class PinchToZoomGestureDetector extends ScaleGestureDetector implements ScaleGestureDetector.OnScaleGestureListener {
        @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            return true;
        }

        @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        }

        PinchToZoomGestureDetector(SignalCameraView signalCameraView, Context context) {
            this(context, new S());
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        PinchToZoomGestureDetector(Context context, S s) {
            super(context, s);
            SignalCameraView.this = r1;
            s.setRealGestureDetector(this);
        }

        @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            float zoomRatio = SignalCameraView.this.getZoomRatio() * (scaleFactor > 1.0f ? ((scaleFactor - 1.0f) * 2.0f) + 1.0f : 1.0f - ((1.0f - scaleFactor) * 2.0f));
            SignalCameraView signalCameraView = SignalCameraView.this;
            SignalCameraView.this.setZoomRatio(signalCameraView.rangeLimit(zoomRatio, signalCameraView.getMaxZoomRatio(), SignalCameraView.this.getMinZoomRatio()));
            return true;
        }
    }
}
