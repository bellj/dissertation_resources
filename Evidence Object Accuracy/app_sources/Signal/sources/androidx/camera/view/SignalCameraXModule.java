package androidx.camera.view;

import android.content.res.Resources;
import android.util.Rational;
import android.util.Size;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Logger;
import androidx.camera.core.Preview;
import androidx.camera.core.UseCase;
import androidx.camera.core.VideoCapture;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.LensFacingConverter;
import androidx.camera.core.impl.utils.CameraOrientationUtil;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.SignalCameraView;
import androidx.core.util.Consumer;
import androidx.core.util.Preconditions;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import org.thoughtcrime.securesms.mediasend.camerax.CameraXUtil;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.video.VideoUtil;

/* loaded from: classes.dex */
public final class SignalCameraXModule {
    private static final Rational ASPECT_RATIO_16_9 = new Rational(16, 9);
    private static final Rational ASPECT_RATIO_3_4 = new Rational(3, 4);
    private static final Rational ASPECT_RATIO_4_3 = new Rational(4, 3);
    private static final Rational ASPECT_RATIO_9_16 = new Rational(9, 16);
    Camera mCamera;
    Integer mCameraLensFacing = 1;
    ProcessCameraProvider mCameraProvider;
    private final SignalCameraView mCameraView;
    private SignalCameraView.CaptureMode mCaptureMode = SignalCameraView.CaptureMode.IMAGE;
    LifecycleOwner mCurrentLifecycle;
    private final LifecycleObserver mCurrentLifecycleObserver = new LifecycleObserver() { // from class: androidx.camera.view.SignalCameraXModule.1
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void onDestroy(LifecycleOwner lifecycleOwner) {
            SignalCameraXModule signalCameraXModule = SignalCameraXModule.this;
            if (lifecycleOwner == signalCameraXModule.mCurrentLifecycle) {
                signalCameraXModule.clearCurrentLifecycle();
            }
        }
    };
    private int mFlash = 2;
    private ImageCapture mImageCapture;
    private final ImageCapture.Builder mImageCaptureBuilder;
    private long mMaxVideoDuration = -1;
    private long mMaxVideoSize = -1;
    private LifecycleOwner mNewLifecycle;
    Preview mPreview;
    private final Preview.Builder mPreviewBuilder;
    private VideoCapture mVideoCapture;
    private final VideoCapture.Builder mVideoCaptureBuilder;
    final AtomicBoolean mVideoIsRecording = new AtomicBoolean(false);

    public boolean isPaused() {
        return false;
    }

    public SignalCameraXModule(SignalCameraView signalCameraView, final Consumer<Throwable> consumer) {
        this.mCameraView = signalCameraView;
        Futures.addCallback(ProcessCameraProvider.getInstance(signalCameraView.getContext()), new FutureCallback<ProcessCameraProvider>() { // from class: androidx.camera.view.SignalCameraXModule.2
            public void onSuccess(ProcessCameraProvider processCameraProvider) {
                Preconditions.checkNotNull(processCameraProvider);
                SignalCameraXModule signalCameraXModule = SignalCameraXModule.this;
                signalCameraXModule.mCameraProvider = processCameraProvider;
                LifecycleOwner lifecycleOwner = signalCameraXModule.mCurrentLifecycle;
                if (lifecycleOwner != null) {
                    signalCameraXModule.bindToLifecycle(lifecycleOwner);
                }
            }

            @Override // androidx.camera.core.impl.utils.futures.FutureCallback
            public void onFailure(Throwable th) {
                consumer.accept(th);
            }
        }, CameraXExecutors.mainThreadExecutor());
        this.mPreviewBuilder = new Preview.Builder().setTargetName("Preview");
        this.mImageCaptureBuilder = new ImageCapture.Builder().setTargetName("ImageCapture");
        this.mVideoCaptureBuilder = new VideoCapture.Builder().setTargetName("VideoCapture").setAudioBitRate(VideoUtil.AUDIO_BIT_RATE).setVideoFrameRate(30).setBitRate(VideoUtil.VIDEO_BIT_RATE);
    }

    public void bindToLifecycle(LifecycleOwner lifecycleOwner) {
        this.mNewLifecycle = lifecycleOwner;
        if (getMeasuredWidth() > 0 && getMeasuredHeight() > 0) {
            bindToLifecycleAfterViewMeasured();
        }
    }

    public void bindToLifecycleAfterViewMeasured() {
        if (this.mNewLifecycle != null) {
            clearCurrentLifecycle();
            if (this.mNewLifecycle.getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED) {
                this.mNewLifecycle = null;
                return;
            }
            this.mCurrentLifecycle = this.mNewLifecycle;
            this.mNewLifecycle = null;
            if (this.mCameraProvider != null) {
                Set<Integer> availableCameraLensFacing = getAvailableCameraLensFacing();
                if (availableCameraLensFacing.isEmpty()) {
                    Logger.w("CameraXModule", "Unable to bindToLifeCycle since no cameras available");
                    this.mCameraLensFacing = null;
                }
                Integer num = this.mCameraLensFacing;
                if (num != null && !availableCameraLensFacing.contains(num)) {
                    Logger.w("CameraXModule", "Camera does not exist with direction " + this.mCameraLensFacing);
                    this.mCameraLensFacing = availableCameraLensFacing.iterator().next();
                    Logger.w("CameraXModule", "Defaulting to primary camera with direction " + this.mCameraLensFacing);
                }
                if (this.mCameraLensFacing != null) {
                    boolean z = getDisplayRotationDegrees() == 0 || getDisplayRotationDegrees() == 180;
                    int idealResolution = CameraXUtil.getIdealResolution(Resources.getSystem().getDisplayMetrics().widthPixels, Resources.getSystem().getDisplayMetrics().heightPixels);
                    ImageCapture.Builder builder = this.mImageCaptureBuilder;
                    Rational rational = ASPECT_RATIO_16_9;
                    builder.setTargetResolution(CameraXUtil.buildResolutionForRatio(idealResolution, rational, z));
                    if (z) {
                        rational = ASPECT_RATIO_9_16;
                    }
                    this.mImageCaptureBuilder.setCaptureMode(CameraXUtil.getOptimalCaptureMode());
                    this.mImageCaptureBuilder.setTargetRotation(getDisplaySurfaceRotation());
                    this.mImageCapture = this.mImageCaptureBuilder.build();
                    Size videoRecordingSize = VideoUtil.getVideoRecordingSize();
                    this.mVideoCaptureBuilder.setTargetResolution(videoRecordingSize);
                    this.mVideoCaptureBuilder.setMaxResolution(videoRecordingSize);
                    this.mVideoCaptureBuilder.setTargetRotation(getDisplaySurfaceRotation());
                    if (MediaConstraints.isVideoTranscodeAvailable()) {
                        this.mVideoCapture = this.mVideoCaptureBuilder.build();
                    }
                    this.mPreviewBuilder.setTargetResolution(new Size(getMeasuredWidth(), (int) (((float) getMeasuredWidth()) / rational.floatValue())));
                    Preview build = this.mPreviewBuilder.build();
                    this.mPreview = build;
                    build.setSurfaceProvider(this.mCameraView.getPreviewView().getSurfaceProvider());
                    CameraSelector build2 = new CameraSelector.Builder().requireLensFacing(this.mCameraLensFacing.intValue()).build();
                    if (getCaptureMode() == SignalCameraView.CaptureMode.IMAGE) {
                        this.mCamera = this.mCameraProvider.bindToLifecycle(this.mCurrentLifecycle, build2, this.mImageCapture, this.mPreview);
                    } else if (getCaptureMode() == SignalCameraView.CaptureMode.VIDEO) {
                        this.mCamera = this.mCameraProvider.bindToLifecycle(this.mCurrentLifecycle, build2, this.mVideoCapture, this.mPreview);
                    } else {
                        this.mCamera = this.mCameraProvider.bindToLifecycle(this.mCurrentLifecycle, build2, this.mImageCapture, this.mVideoCapture, this.mPreview);
                    }
                    setZoomRatio(1.0f);
                    this.mCurrentLifecycle.getLifecycle().addObserver(this.mCurrentLifecycleObserver);
                    setFlash(getFlash());
                }
            }
        }
    }

    public void takePicture(Executor executor, ImageCapture.OnImageCapturedCallback onImageCapturedCallback) {
        if (this.mImageCapture != null) {
            if (getCaptureMode() == SignalCameraView.CaptureMode.VIDEO) {
                throw new IllegalStateException("Can not take picture under VIDEO capture mode.");
            } else if (onImageCapturedCallback != null) {
                this.mImageCapture.lambda$takePicture$3(executor, onImageCapturedCallback);
            } else {
                throw new IllegalArgumentException("OnImageCapturedCallback should not be empty");
            }
        }
    }

    public void startRecording(VideoCapture.OutputFileOptions outputFileOptions, Executor executor, final VideoCapture.OnVideoSavedCallback onVideoSavedCallback) {
        if (this.mVideoCapture != null) {
            if (getCaptureMode() == SignalCameraView.CaptureMode.IMAGE) {
                throw new IllegalStateException("Can not record video under IMAGE capture mode.");
            } else if (onVideoSavedCallback != null) {
                this.mVideoIsRecording.set(true);
                this.mVideoCapture.lambda$startRecording$0(outputFileOptions, executor, new VideoCapture.OnVideoSavedCallback() { // from class: androidx.camera.view.SignalCameraXModule.3
                    @Override // androidx.camera.core.VideoCapture.OnVideoSavedCallback
                    public void onVideoSaved(VideoCapture.OutputFileResults outputFileResults) {
                        SignalCameraXModule.this.mVideoIsRecording.set(false);
                        onVideoSavedCallback.onVideoSaved(outputFileResults);
                    }

                    @Override // androidx.camera.core.VideoCapture.OnVideoSavedCallback
                    public void onError(int i, String str, Throwable th) {
                        SignalCameraXModule.this.mVideoIsRecording.set(false);
                        Logger.e("CameraXModule", str, th);
                        onVideoSavedCallback.onError(i, str, th);
                    }
                });
            } else {
                throw new IllegalArgumentException("OnVideoSavedCallback should not be empty");
            }
        }
    }

    public void stopRecording() {
        VideoCapture videoCapture = this.mVideoCapture;
        if (videoCapture != null) {
            videoCapture.lambda$stopRecording$5();
        }
    }

    public void setCameraLensFacing(Integer num) {
        if (!Objects.equals(this.mCameraLensFacing, num)) {
            this.mCameraLensFacing = num;
            LifecycleOwner lifecycleOwner = this.mCurrentLifecycle;
            if (lifecycleOwner != null) {
                bindToLifecycle(lifecycleOwner);
            }
        }
    }

    public boolean hasCameraWithLensFacing(int i) {
        ProcessCameraProvider processCameraProvider = this.mCameraProvider;
        if (processCameraProvider == null) {
            return false;
        }
        return processCameraProvider.hasCamera(new CameraSelector.Builder().requireLensFacing(i).build());
    }

    public Integer getLensFacing() {
        return this.mCameraLensFacing;
    }

    public void toggleCamera() {
        Set<Integer> availableCameraLensFacing = getAvailableCameraLensFacing();
        if (!availableCameraLensFacing.isEmpty()) {
            Integer num = this.mCameraLensFacing;
            if (num == null) {
                setCameraLensFacing(availableCameraLensFacing.iterator().next());
            } else if (num.intValue() == 1 && availableCameraLensFacing.contains(0)) {
                setCameraLensFacing(0);
            } else if (this.mCameraLensFacing.intValue() == 0 && availableCameraLensFacing.contains(1)) {
                setCameraLensFacing(1);
            }
        }
    }

    public float getZoomRatio() {
        Camera camera = this.mCamera;
        if (camera != null) {
            return camera.getCameraInfo().getZoomState().getValue().getZoomRatio();
        }
        return 1.0f;
    }

    public void setZoomRatio(float f) {
        Camera camera = this.mCamera;
        if (camera != null) {
            Futures.addCallback(camera.getCameraControl().setZoomRatio(f), new FutureCallback<Void>() { // from class: androidx.camera.view.SignalCameraXModule.4
                public void onSuccess(Void r1) {
                }

                @Override // androidx.camera.core.impl.utils.futures.FutureCallback
                public void onFailure(Throwable th) {
                    throw new RuntimeException(th);
                }
            }, CameraXExecutors.directExecutor());
        } else {
            Logger.e("CameraXModule", "Failed to set zoom ratio");
        }
    }

    public float getMinZoomRatio() {
        Camera camera = this.mCamera;
        if (camera != null) {
            return camera.getCameraInfo().getZoomState().getValue().getMinZoomRatio();
        }
        return 1.0f;
    }

    public float getMaxZoomRatio() {
        Camera camera = this.mCamera;
        if (camera != null) {
            return camera.getCameraInfo().getZoomState().getValue().getMaxZoomRatio();
        }
        return 1.0f;
    }

    public boolean isZoomSupported() {
        return getMaxZoomRatio() != 1.0f;
    }

    private void rebindToLifecycle() {
        LifecycleOwner lifecycleOwner = this.mCurrentLifecycle;
        if (lifecycleOwner != null) {
            bindToLifecycle(lifecycleOwner);
        }
    }

    public boolean isBoundToLifecycle() {
        return this.mCamera != null;
    }

    public void invalidateView() {
        Preview preview = this.mPreview;
        if (preview != null) {
            preview.setTargetRotation(getDisplaySurfaceRotation());
        }
        updateViewInfo();
    }

    void clearCurrentLifecycle() {
        if (!(this.mCurrentLifecycle == null || this.mCameraProvider == null)) {
            ArrayList arrayList = new ArrayList();
            ImageCapture imageCapture = this.mImageCapture;
            if (imageCapture != null && this.mCameraProvider.isBound(imageCapture)) {
                arrayList.add(this.mImageCapture);
            }
            VideoCapture videoCapture = this.mVideoCapture;
            if (videoCapture != null && this.mCameraProvider.isBound(videoCapture)) {
                arrayList.add(this.mVideoCapture);
            }
            Preview preview = this.mPreview;
            if (preview != null && this.mCameraProvider.isBound(preview)) {
                arrayList.add(this.mPreview);
            }
            if (!arrayList.isEmpty()) {
                this.mCameraProvider.unbind((UseCase[]) arrayList.toArray(new UseCase[0]));
            }
            Preview preview2 = this.mPreview;
            if (preview2 != null) {
                preview2.setSurfaceProvider(null);
            }
        }
        this.mCamera = null;
        this.mCurrentLifecycle = null;
    }

    private void updateViewInfo() {
        ImageCapture imageCapture = this.mImageCapture;
        if (imageCapture != null) {
            imageCapture.setCropAspectRatio(new Rational(getWidth(), getHeight()));
            this.mImageCapture.setTargetRotation(getDisplaySurfaceRotation());
        }
        VideoCapture videoCapture = this.mVideoCapture;
        if (videoCapture != null) {
            videoCapture.setTargetRotation(getDisplaySurfaceRotation());
        }
    }

    private Set<Integer> getAvailableCameraLensFacing() {
        LinkedHashSet linkedHashSet = new LinkedHashSet(Arrays.asList(LensFacingConverter.values()));
        if (this.mCurrentLifecycle != null) {
            if (!hasCameraWithLensFacing(1)) {
                linkedHashSet.remove(1);
            }
            if (!hasCameraWithLensFacing(0)) {
                linkedHashSet.remove(0);
            }
        }
        return linkedHashSet;
    }

    public int getFlash() {
        return this.mFlash;
    }

    public boolean hasFlash() {
        CameraInternal camera;
        ImageCapture imageCapture = this.mImageCapture;
        if (imageCapture == null || (camera = imageCapture.getCamera()) == null) {
            return false;
        }
        return camera.getCameraInfoInternal().hasFlashUnit();
    }

    public void setFlash(int i) {
        this.mFlash = i;
        ImageCapture imageCapture = this.mImageCapture;
        if (imageCapture != null) {
            imageCapture.setFlashMode(i);
        }
    }

    public int getWidth() {
        return this.mCameraView.getWidth();
    }

    public int getHeight() {
        return this.mCameraView.getHeight();
    }

    public int getDisplayRotationDegrees() {
        return CameraOrientationUtil.surfaceRotationToDegrees(getDisplaySurfaceRotation());
    }

    protected int getDisplaySurfaceRotation() {
        return this.mCameraView.getDisplaySurfaceRotation();
    }

    private int getMeasuredWidth() {
        return this.mCameraView.getMeasuredWidth();
    }

    private int getMeasuredHeight() {
        return this.mCameraView.getMeasuredHeight();
    }

    public Camera getCamera() {
        return this.mCamera;
    }

    public SignalCameraView.CaptureMode getCaptureMode() {
        return this.mCaptureMode;
    }

    public void setCaptureMode(SignalCameraView.CaptureMode captureMode) {
        this.mCaptureMode = captureMode;
        rebindToLifecycle();
    }

    public long getMaxVideoDuration() {
        return this.mMaxVideoDuration;
    }

    public void setMaxVideoDuration(long j) {
        this.mMaxVideoDuration = j;
    }

    public long getMaxVideoSize() {
        return this.mMaxVideoSize;
    }

    public void setMaxVideoSize(long j) {
        this.mMaxVideoSize = j;
    }
}
