package androidx.camera.view;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.Size;
import androidx.camera.core.MeteringPointFactory;

/* loaded from: classes.dex */
public class PreviewViewMeteringPointFactory extends MeteringPointFactory {
    static final PointF INVALID_POINT = new PointF(2.0f, 2.0f);
    private Matrix mMatrix;
    private final PreviewTransformation mPreviewTransformation;

    public PreviewViewMeteringPointFactory(PreviewTransformation previewTransformation) {
        this.mPreviewTransformation = previewTransformation;
    }

    @Override // androidx.camera.core.MeteringPointFactory
    protected PointF convertPoint(float f, float f2) {
        Matrix matrix = this.mMatrix;
        if (matrix == null) {
            return INVALID_POINT;
        }
        float[] fArr = {f, f2};
        matrix.mapPoints(fArr);
        return new PointF(fArr[0], fArr[1]);
    }

    public void recalculate(Size size, int i) {
        if (size.getWidth() == 0 || size.getHeight() == 0) {
            this.mMatrix = null;
        } else {
            this.mMatrix = this.mPreviewTransformation.getPreviewViewToNormalizedSurfaceMatrix(size, i);
        }
    }
}
