package androidx.camera.view;

import androidx.camera.core.SurfaceRequest;
import androidx.camera.view.PreviewView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class PreviewView$1$$ExternalSyntheticLambda1 implements SurfaceRequest.TransformationInfoListener {
    public final /* synthetic */ PreviewView.AnonymousClass1 f$0;
    public final /* synthetic */ SurfaceRequest f$1;

    public /* synthetic */ PreviewView$1$$ExternalSyntheticLambda1(PreviewView.AnonymousClass1 r1, SurfaceRequest surfaceRequest) {
        this.f$0 = r1;
        this.f$1 = surfaceRequest;
    }

    @Override // androidx.camera.core.SurfaceRequest.TransformationInfoListener
    public final void onTransformationInfoUpdate(SurfaceRequest.TransformationInfo transformationInfo) {
        this.f$0.lambda$onSurfaceRequested$0(this.f$1, transformationInfo);
    }
}
