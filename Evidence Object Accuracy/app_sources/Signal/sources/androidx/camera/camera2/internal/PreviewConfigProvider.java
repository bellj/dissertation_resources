package androidx.camera.camera2.internal;

import android.content.Context;
import android.view.WindowManager;
import androidx.camera.core.Preview;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.ConfigProvider;
import androidx.camera.core.impl.PreviewConfig;
import androidx.camera.core.impl.SessionConfig;

/* loaded from: classes.dex */
public final class PreviewConfigProvider implements ConfigProvider<PreviewConfig> {
    private final WindowManager mWindowManager;

    public PreviewConfigProvider(Context context) {
        this.mWindowManager = (WindowManager) context.getSystemService("window");
    }

    @Override // androidx.camera.core.impl.ConfigProvider
    public PreviewConfig getConfig() {
        Preview.Builder fromConfig = Preview.Builder.fromConfig(Preview.DEFAULT_CONFIG.getConfig());
        SessionConfig.Builder builder = new SessionConfig.Builder();
        builder.setTemplateType(1);
        fromConfig.setDefaultSessionConfig(builder.build());
        fromConfig.setSessionOptionUnpacker(Camera2SessionOptionUnpacker.INSTANCE);
        CaptureConfig.Builder builder2 = new CaptureConfig.Builder();
        builder2.setTemplateType(1);
        fromConfig.setDefaultCaptureConfig(builder2.build());
        fromConfig.setCaptureOptionUnpacker(Camera2CaptureOptionUnpacker.INSTANCE);
        fromConfig.setMaxResolution(SupportedSurfaceCombination.getPreviewSize(this.mWindowManager));
        fromConfig.setTargetAspectRatio(0);
        fromConfig.setTargetRotation(this.mWindowManager.getDefaultDisplay().getRotation());
        return fromConfig.getUseCaseConfig();
    }
}
