package androidx.camera.camera2.internal;

import android.content.Context;
import android.view.WindowManager;
import androidx.camera.core.VideoCapture;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.ConfigProvider;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.VideoCaptureConfig;

/* loaded from: classes.dex */
public final class VideoCaptureConfigProvider implements ConfigProvider<VideoCaptureConfig> {
    private final WindowManager mWindowManager;

    public VideoCaptureConfigProvider(Context context) {
        this.mWindowManager = (WindowManager) context.getSystemService("window");
    }

    @Override // androidx.camera.core.impl.ConfigProvider
    public VideoCaptureConfig getConfig() {
        VideoCapture.Builder fromConfig = VideoCapture.Builder.fromConfig(VideoCapture.DEFAULT_CONFIG.getConfig());
        SessionConfig.Builder builder = new SessionConfig.Builder();
        builder.setTemplateType(1);
        fromConfig.setDefaultSessionConfig(builder.build());
        fromConfig.setSessionOptionUnpacker(Camera2SessionOptionUnpacker.INSTANCE);
        CaptureConfig.Builder builder2 = new CaptureConfig.Builder();
        builder2.setTemplateType(1);
        fromConfig.setDefaultCaptureConfig(builder2.build());
        fromConfig.setCaptureOptionUnpacker(Camera2CaptureOptionUnpacker.INSTANCE);
        fromConfig.setTargetAspectRatio(1);
        fromConfig.setTargetRotation(this.mWindowManager.getDefaultDisplay().getRotation());
        return fromConfig.getUseCaseConfig();
    }
}
