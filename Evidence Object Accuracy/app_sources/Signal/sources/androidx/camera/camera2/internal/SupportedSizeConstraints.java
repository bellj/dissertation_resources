package androidx.camera.camera2.internal;

import android.os.Build;
import android.util.Range;
import android.util.Size;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.thoughtcrime.securesms.badges.gifts.Gifts;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class SupportedSizeConstraints {
    private static final Range<Integer> ALL_API_LEVELS;
    private static final Map<CameraDeviceId, List<ExcludedSizeConstraint>> EXCLUDED_SIZES_MAP;

    static {
        Range<Integer> range = new Range<>(0, Integer.MAX_VALUE);
        ALL_API_LEVELS = range;
        TreeMap treeMap = new TreeMap(new Comparator<CameraDeviceId>() { // from class: androidx.camera.camera2.internal.SupportedSizeConstraints.1
            public int compare(CameraDeviceId cameraDeviceId, CameraDeviceId cameraDeviceId2) {
                int compareTo;
                if (cameraDeviceId.equals(cameraDeviceId2)) {
                    return 0;
                }
                int compareTo2 = cameraDeviceId.getBrand().compareTo(cameraDeviceId2.getBrand());
                if (compareTo2 != 0) {
                    return compareTo2;
                }
                int compareTo3 = cameraDeviceId.getDevice().compareTo(cameraDeviceId2.getDevice());
                if (compareTo3 != 0) {
                    return compareTo3;
                }
                if (!"allmodels".equals(cameraDeviceId.getModel()) && !"allmodels".equals(cameraDeviceId2.getModel()) && (compareTo = cameraDeviceId.getModel().compareTo(cameraDeviceId2.getModel())) != 0) {
                    return compareTo;
                }
                int compareTo4 = cameraDeviceId.getCameraId().compareTo(cameraDeviceId2.getCameraId());
                if (compareTo4 != 0) {
                    return compareTo4;
                }
                return 0;
            }
        });
        EXCLUDED_SIZES_MAP = treeMap;
        treeMap.put(CameraDeviceId.create("OnePlus", "OnePlus6T", "allmodels", "0"), Collections.singletonList(ExcludedSizeConstraint.create(Collections.singleton(256), range, Arrays.asList(new Size(4160, 3120), new Size(4000, Gifts.GOOGLE_PAY_REQUEST_CODE)))));
        treeMap.put(CameraDeviceId.create("OnePlus", "OnePlus6", "allmodels", "0"), Collections.singletonList(ExcludedSizeConstraint.create(Collections.singleton(256), range, Arrays.asList(new Size(4160, 3120), new Size(4000, Gifts.GOOGLE_PAY_REQUEST_CODE)))));
    }

    public static List<Size> getExcludedSizes(String str, int i) {
        CameraDeviceId create = CameraDeviceId.create(Build.BRAND, Build.DEVICE, Build.MODEL, str);
        Map<CameraDeviceId, List<ExcludedSizeConstraint>> map = EXCLUDED_SIZES_MAP;
        if (!map.containsKey(create)) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (ExcludedSizeConstraint excludedSizeConstraint : map.get(create)) {
            if (excludedSizeConstraint.getAffectedFormats().contains(Integer.valueOf(i)) && excludedSizeConstraint.getAffectedApiLevels().contains((Range<Integer>) Integer.valueOf(Build.VERSION.SDK_INT))) {
                arrayList.addAll(excludedSizeConstraint.getExcludedSizes());
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes.dex */
    public static abstract class ExcludedSizeConstraint {
        public abstract Range<Integer> getAffectedApiLevels();

        public abstract Set<Integer> getAffectedFormats();

        public abstract List<Size> getExcludedSizes();

        public static ExcludedSizeConstraint create(Set<Integer> set, Range<Integer> range, List<Size> list) {
            return new AutoValue_SupportedSizeConstraints_ExcludedSizeConstraint(set, range, list);
        }
    }
}
