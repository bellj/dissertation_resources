package androidx.camera.camera2.internal;

import android.content.Context;
import android.view.WindowManager;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.ConfigProvider;
import androidx.camera.core.impl.ImageAnalysisConfig;
import androidx.camera.core.impl.SessionConfig;

/* loaded from: classes.dex */
public final class ImageAnalysisConfigProvider implements ConfigProvider<ImageAnalysisConfig> {
    private final WindowManager mWindowManager;

    public ImageAnalysisConfigProvider(Context context) {
        this.mWindowManager = (WindowManager) context.getSystemService("window");
    }

    @Override // androidx.camera.core.impl.ConfigProvider
    public ImageAnalysisConfig getConfig() {
        ImageAnalysis.Builder fromConfig = ImageAnalysis.Builder.fromConfig(ImageAnalysis.DEFAULT_CONFIG.getConfig());
        SessionConfig.Builder builder = new SessionConfig.Builder();
        builder.setTemplateType(1);
        fromConfig.setDefaultSessionConfig(builder.build());
        fromConfig.setSessionOptionUnpacker(Camera2SessionOptionUnpacker.INSTANCE);
        CaptureConfig.Builder builder2 = new CaptureConfig.Builder();
        builder2.setTemplateType(1);
        fromConfig.setDefaultCaptureConfig(builder2.build());
        fromConfig.setCaptureOptionUnpacker(Camera2CaptureOptionUnpacker.INSTANCE);
        fromConfig.setTargetAspectRatio(0);
        fromConfig.setTargetRotation(this.mWindowManager.getDefaultDisplay().getRotation());
        return fromConfig.getUseCaseConfig();
    }
}
