package androidx.camera.camera2.internal;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class Camera2CameraControlImpl$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Camera2CameraControlImpl f$0;

    public /* synthetic */ Camera2CameraControlImpl$$ExternalSyntheticLambda1(Camera2CameraControlImpl camera2CameraControlImpl) {
        this.f$0 = camera2CameraControlImpl;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.updateSessionConfig();
    }
}
