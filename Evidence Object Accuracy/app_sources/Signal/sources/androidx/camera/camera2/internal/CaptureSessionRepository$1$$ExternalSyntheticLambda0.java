package androidx.camera.camera2.internal;

import androidx.camera.camera2.internal.CaptureSessionRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class CaptureSessionRepository$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ CaptureSessionRepository.AnonymousClass1 f$0;

    public /* synthetic */ CaptureSessionRepository$1$$ExternalSyntheticLambda0(CaptureSessionRepository.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$forceOnClosedCaptureSessions$0();
    }
}
