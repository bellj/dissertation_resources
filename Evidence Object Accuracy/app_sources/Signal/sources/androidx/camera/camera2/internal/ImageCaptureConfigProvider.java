package androidx.camera.camera2.internal;

import android.content.Context;
import android.view.WindowManager;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.ConfigProvider;
import androidx.camera.core.impl.ImageCaptureConfig;
import androidx.camera.core.impl.SessionConfig;

/* loaded from: classes.dex */
public final class ImageCaptureConfigProvider implements ConfigProvider<ImageCaptureConfig> {
    private final WindowManager mWindowManager;

    public ImageCaptureConfigProvider(Context context) {
        this.mWindowManager = (WindowManager) context.getSystemService("window");
    }

    @Override // androidx.camera.core.impl.ConfigProvider
    public ImageCaptureConfig getConfig() {
        ImageCapture.Builder fromConfig = ImageCapture.Builder.fromConfig(ImageCapture.DEFAULT_CONFIG.getConfig());
        SessionConfig.Builder builder = new SessionConfig.Builder();
        builder.setTemplateType(1);
        fromConfig.setDefaultSessionConfig(builder.build());
        fromConfig.setSessionOptionUnpacker(Camera2SessionOptionUnpacker.INSTANCE);
        CaptureConfig.Builder builder2 = new CaptureConfig.Builder();
        builder2.setTemplateType(2);
        fromConfig.setDefaultCaptureConfig(builder2.build());
        fromConfig.setCaptureOptionUnpacker(ImageCaptureOptionUnpacker.INSTANCE);
        fromConfig.setTargetAspectRatio(0);
        fromConfig.setTargetRotation(this.mWindowManager.getDefaultDisplay().getRotation());
        return fromConfig.getUseCaseConfig();
    }
}
