package androidx.camera.camera2.internal;

import androidx.camera.core.impl.CameraCaptureCallback;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class Camera2CameraControlImpl$CameraCaptureCallbackSet$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ CameraCaptureCallback f$0;

    public /* synthetic */ Camera2CameraControlImpl$CameraCaptureCallbackSet$$ExternalSyntheticLambda1(CameraCaptureCallback cameraCaptureCallback) {
        this.f$0 = cameraCaptureCallback;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.onCaptureCancelled();
    }
}
