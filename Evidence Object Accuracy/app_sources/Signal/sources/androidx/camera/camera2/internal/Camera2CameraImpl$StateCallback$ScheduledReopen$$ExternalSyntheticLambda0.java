package androidx.camera.camera2.internal;

import androidx.camera.camera2.internal.Camera2CameraImpl;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class Camera2CameraImpl$StateCallback$ScheduledReopen$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Camera2CameraImpl.StateCallback.ScheduledReopen f$0;

    public /* synthetic */ Camera2CameraImpl$StateCallback$ScheduledReopen$$ExternalSyntheticLambda0(Camera2CameraImpl.StateCallback.ScheduledReopen scheduledReopen) {
        this.f$0 = scheduledReopen;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$0();
    }
}
