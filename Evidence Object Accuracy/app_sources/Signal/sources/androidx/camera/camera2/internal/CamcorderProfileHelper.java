package androidx.camera.camera2.internal;

/* loaded from: classes.dex */
public interface CamcorderProfileHelper {
    boolean hasProfile(int i, int i2);
}
