package androidx.camera.camera2.internal;

import android.hardware.camera2.CameraDevice;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

/* loaded from: classes.dex */
public class CaptureSessionRepository {
    private final CameraDevice.StateCallback mCameraStateCallback = new CameraDevice.StateCallback() { // from class: androidx.camera.camera2.internal.CaptureSessionRepository.1
        @Override // android.hardware.camera2.CameraDevice.StateCallback
        public void onOpened(CameraDevice cameraDevice) {
        }

        @Override // android.hardware.camera2.CameraDevice.StateCallback
        public void onError(CameraDevice cameraDevice, int i) {
            forceOnClosedCaptureSessions();
        }

        @Override // android.hardware.camera2.CameraDevice.StateCallback
        public void onDisconnected(CameraDevice cameraDevice) {
            forceOnClosedCaptureSessions();
        }

        private void forceOnClosedCaptureSessions() {
            CaptureSessionRepository.this.mExecutor.execute(new CaptureSessionRepository$1$$ExternalSyntheticLambda0(this));
        }

        public /* synthetic */ void lambda$forceOnClosedCaptureSessions$0() {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            synchronized (CaptureSessionRepository.this.mLock) {
                linkedHashSet.addAll(new LinkedHashSet(CaptureSessionRepository.this.mCreatingCaptureSessions));
                linkedHashSet.addAll(new LinkedHashSet(CaptureSessionRepository.this.mCaptureSessions));
            }
            CaptureSessionRepository.forceOnClosed(linkedHashSet);
        }
    };
    final Set<SynchronizedCaptureSession> mCaptureSessions = new LinkedHashSet();
    final Set<SynchronizedCaptureSession> mClosingCaptureSession = new LinkedHashSet();
    final Set<SynchronizedCaptureSession> mCreatingCaptureSessions = new LinkedHashSet();
    final Map<SynchronizedCaptureSession, List<DeferrableSurface>> mDeferrableSurfaceMap = new HashMap();
    final Executor mExecutor;
    final Object mLock = new Object();

    public CaptureSessionRepository(Executor executor) {
        this.mExecutor = executor;
    }

    public CameraDevice.StateCallback getCameraStateCallback() {
        return this.mCameraStateCallback;
    }

    static void forceOnClosed(Set<SynchronizedCaptureSession> set) {
        for (SynchronizedCaptureSession synchronizedCaptureSession : set) {
            synchronizedCaptureSession.getStateCallback().onClosed(synchronizedCaptureSession);
        }
    }

    public List<SynchronizedCaptureSession> getCaptureSessions() {
        ArrayList arrayList;
        synchronized (this.mLock) {
            arrayList = new ArrayList(this.mCaptureSessions);
        }
        return arrayList;
    }

    public List<SynchronizedCaptureSession> getClosingCaptureSession() {
        ArrayList arrayList;
        synchronized (this.mLock) {
            arrayList = new ArrayList(this.mClosingCaptureSession);
        }
        return arrayList;
    }

    public List<SynchronizedCaptureSession> getCreatingCaptureSessions() {
        ArrayList arrayList;
        synchronized (this.mLock) {
            arrayList = new ArrayList(this.mCreatingCaptureSessions);
        }
        return arrayList;
    }

    public Map<SynchronizedCaptureSession, List<DeferrableSurface>> registerDeferrableSurface(SynchronizedCaptureSession synchronizedCaptureSession, List<DeferrableSurface> list) {
        HashMap hashMap;
        synchronized (this.mLock) {
            this.mDeferrableSurfaceMap.put(synchronizedCaptureSession, list);
            hashMap = new HashMap(this.mDeferrableSurfaceMap);
        }
        return hashMap;
    }

    public void unregisterDeferrableSurface(SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mDeferrableSurfaceMap.remove(synchronizedCaptureSession);
        }
    }

    public void onCreateCaptureSession(SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mCreatingCaptureSessions.add(synchronizedCaptureSession);
        }
    }

    public void onCaptureSessionConfigureFail(SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mCreatingCaptureSessions.remove(synchronizedCaptureSession);
        }
    }

    public void onCaptureSessionCreated(SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mCaptureSessions.add(synchronizedCaptureSession);
            this.mCreatingCaptureSessions.remove(synchronizedCaptureSession);
        }
    }

    public void onCaptureSessionClosed(SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mCaptureSessions.remove(synchronizedCaptureSession);
            this.mClosingCaptureSession.remove(synchronizedCaptureSession);
        }
    }

    public void onCaptureSessionClosing(SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mClosingCaptureSession.add(synchronizedCaptureSession);
        }
    }
}
