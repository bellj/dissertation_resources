package androidx.camera.camera2.interop;

import androidx.camera.camera2.internal.Camera2CameraInfoImpl;

/* loaded from: classes.dex */
public final class Camera2CameraInfo {
    private final Camera2CameraInfoImpl mCamera2CameraInfoImpl;

    public Camera2CameraInfo(Camera2CameraInfoImpl camera2CameraInfoImpl) {
        this.mCamera2CameraInfoImpl = camera2CameraInfoImpl;
    }
}
