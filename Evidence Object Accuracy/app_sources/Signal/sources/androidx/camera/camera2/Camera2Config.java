package androidx.camera.camera2;

import android.content.Context;
import androidx.camera.camera2.internal.Camera2CameraFactory;
import androidx.camera.camera2.internal.Camera2DeviceSurfaceManager;
import androidx.camera.camera2.internal.ImageAnalysisConfigProvider;
import androidx.camera.camera2.internal.ImageCaptureConfigProvider;
import androidx.camera.camera2.internal.PreviewConfigProvider;
import androidx.camera.camera2.internal.VideoCaptureConfigProvider;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.core.CameraXConfig;
import androidx.camera.core.InitializationException;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import androidx.camera.core.impl.CameraFactory;
import androidx.camera.core.impl.CameraThreadConfig;
import androidx.camera.core.impl.ExtendableUseCaseConfigFactory;
import androidx.camera.core.impl.ImageAnalysisConfig;
import androidx.camera.core.impl.ImageCaptureConfig;
import androidx.camera.core.impl.PreviewConfig;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.impl.VideoCaptureConfig;

/* loaded from: classes.dex */
public final class Camera2Config {
    public static CameraXConfig defaultConfig() {
        Camera2Config$$ExternalSyntheticLambda0 camera2Config$$ExternalSyntheticLambda0 = new CameraFactory.Provider() { // from class: androidx.camera.camera2.Camera2Config$$ExternalSyntheticLambda0
            @Override // androidx.camera.core.impl.CameraFactory.Provider
            public final CameraFactory newInstance(Context context, CameraThreadConfig cameraThreadConfig) {
                return new Camera2CameraFactory(context, cameraThreadConfig);
            }
        };
        Camera2Config$$ExternalSyntheticLambda1 camera2Config$$ExternalSyntheticLambda1 = new CameraDeviceSurfaceManager.Provider() { // from class: androidx.camera.camera2.Camera2Config$$ExternalSyntheticLambda1
            @Override // androidx.camera.core.impl.CameraDeviceSurfaceManager.Provider
            public final CameraDeviceSurfaceManager newInstance(Context context, Object obj) {
                return Camera2Config.$r8$lambda$AkzM73pME1TMUu5ZXiUXAqrvISk(context, obj);
            }
        };
        return new CameraXConfig.Builder().setCameraFactoryProvider(camera2Config$$ExternalSyntheticLambda0).setDeviceSurfaceManagerProvider(camera2Config$$ExternalSyntheticLambda1).setUseCaseConfigFactoryProvider(new UseCaseConfigFactory.Provider() { // from class: androidx.camera.camera2.Camera2Config$$ExternalSyntheticLambda2
            @Override // androidx.camera.core.impl.UseCaseConfigFactory.Provider
            public final UseCaseConfigFactory newInstance(Context context) {
                return Camera2Config.$r8$lambda$g_hY10kZhqC56um0PalOLTzuFlU(context);
            }
        }).build();
    }

    public static /* synthetic */ CameraDeviceSurfaceManager lambda$defaultConfig$0(Context context, Object obj) throws InitializationException {
        try {
            return new Camera2DeviceSurfaceManager(context, obj);
        } catch (CameraUnavailableException e) {
            throw new InitializationException(e);
        }
    }

    public static /* synthetic */ UseCaseConfigFactory lambda$defaultConfig$1(Context context) throws InitializationException {
        ExtendableUseCaseConfigFactory extendableUseCaseConfigFactory = new ExtendableUseCaseConfigFactory();
        extendableUseCaseConfigFactory.installDefaultProvider(ImageAnalysisConfig.class, new ImageAnalysisConfigProvider(context));
        extendableUseCaseConfigFactory.installDefaultProvider(ImageCaptureConfig.class, new ImageCaptureConfigProvider(context));
        extendableUseCaseConfigFactory.installDefaultProvider(VideoCaptureConfig.class, new VideoCaptureConfigProvider(context));
        extendableUseCaseConfigFactory.installDefaultProvider(PreviewConfig.class, new PreviewConfigProvider(context));
        return extendableUseCaseConfigFactory;
    }

    /* loaded from: classes.dex */
    public static final class DefaultProvider implements CameraXConfig.Provider {
        @Override // androidx.camera.core.CameraXConfig.Provider
        public CameraXConfig getCameraXConfig() {
            return Camera2Config.defaultConfig();
        }
    }
}
