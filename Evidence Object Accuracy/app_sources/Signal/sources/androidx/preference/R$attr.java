package androidx.preference;

/* loaded from: classes.dex */
public final class R$attr {
    public static final int checkBoxPreferenceStyle;
    public static final int dialogPreferenceStyle;
    public static final int dropdownPreferenceStyle;
    public static final int editTextPreferenceStyle;
    public static final int preferenceCategoryStyle;
    public static final int preferenceFragmentCompatStyle;
    public static final int preferenceScreenStyle;
    public static final int preferenceStyle;
    public static final int preferenceTheme;
    public static final int seekBarPreferenceStyle;
    public static final int switchPreferenceCompatStyle;
    public static final int switchPreferenceStyle;
}
