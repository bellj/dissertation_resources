package rxdogtag2;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import org.reactivestreams.Subscriber;

/* loaded from: classes5.dex */
public interface ObserverHandler {

    /* renamed from: rxdogtag2.ObserverHandler$-CC */
    /* loaded from: classes5.dex */
    public final /* synthetic */ class CC {
        public static CompletableObserver $default$handle(ObserverHandler observerHandler, Completable completable, CompletableObserver completableObserver) {
            return completableObserver;
        }

        public static MaybeObserver $default$handle(ObserverHandler observerHandler, Maybe maybe, MaybeObserver maybeObserver) {
            return maybeObserver;
        }

        public static Observer $default$handle(ObserverHandler observerHandler, Observable observable, Observer observer) {
            return observer;
        }

        public static SingleObserver $default$handle(ObserverHandler observerHandler, Single single, SingleObserver singleObserver) {
            return singleObserver;
        }

        public static Subscriber $default$handle(ObserverHandler observerHandler, Flowable flowable, Subscriber subscriber) {
            return subscriber;
        }
    }

    CompletableObserver handle(Completable completable, CompletableObserver completableObserver);

    MaybeObserver handle(Maybe maybe, MaybeObserver maybeObserver);

    Observer handle(Observable observable, Observer observer);

    SingleObserver handle(Single single, SingleObserver singleObserver);

    Subscriber handle(Flowable flowable, Subscriber subscriber);
}
