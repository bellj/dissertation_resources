package rxdogtag2;

import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.LambdaConsumerIntrospection;
import java.util.Objects;
import rxdogtag2.RxDogTag;

/* loaded from: classes5.dex */
public final class DogTagCompletableObserver implements CompletableObserver, LambdaConsumerIntrospection {
    private final RxDogTag.Configuration config;
    private final CompletableObserver delegate;
    private final Throwable t = new Throwable();

    public DogTagCompletableObserver(RxDogTag.Configuration configuration, CompletableObserver completableObserver) {
        this.config = configuration;
        this.delegate = completableObserver;
    }

    @Override // io.reactivex.rxjava3.core.CompletableObserver
    public void onSubscribe(Disposable disposable) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagCompletableObserver$$ExternalSyntheticLambda4
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagCompletableObserver.$r8$lambda$nk6sA2LLucULfKsgFJWSpCnNY2o(DogTagCompletableObserver.this, (Throwable) obj);
                }
            }, new Runnable(disposable) { // from class: rxdogtag2.DogTagCompletableObserver$$ExternalSyntheticLambda5
                public final /* synthetic */ Disposable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagCompletableObserver.m3379$r8$lambda$CvEDC2c8m6Xc4pMDfCVSNTUFfI(DogTagCompletableObserver.this, this.f$1);
                }
            });
        } else {
            this.delegate.onSubscribe(disposable);
        }
    }

    public /* synthetic */ void lambda$onSubscribe$0(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onSubscribe");
    }

    public /* synthetic */ void lambda$onSubscribe$1(Disposable disposable) {
        this.delegate.onSubscribe(disposable);
    }

    @Override // io.reactivex.rxjava3.core.CompletableObserver
    public void onError(Throwable th) {
        CompletableObserver completableObserver = this.delegate;
        if (!(completableObserver instanceof RxDogTagErrorReceiver)) {
            RxDogTag.reportError(this.config, this.t, th, null);
        } else if (completableObserver instanceof RxDogTagTaggedExceptionReceiver) {
            completableObserver.onError(RxDogTag.createException(this.config, this.t, th, null));
        } else if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagCompletableObserver$$ExternalSyntheticLambda0
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagCompletableObserver.m3378$r8$lambda$3X8XWc5cVUndEKGTCT2UPzDEC4(DogTagCompletableObserver.this, (Throwable) obj);
                }
            }, new Runnable(th) { // from class: rxdogtag2.DogTagCompletableObserver$$ExternalSyntheticLambda1
                public final /* synthetic */ Throwable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagCompletableObserver.$r8$lambda$yhvm0H_2dg4cWOhUH1agvlQClRs(DogTagCompletableObserver.this, this.f$1);
                }
            });
        } else {
            completableObserver.onError(th);
        }
    }

    public /* synthetic */ void lambda$onError$2(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onError");
    }

    public /* synthetic */ void lambda$onError$3(Throwable th) {
        this.delegate.onError(th);
    }

    @Override // io.reactivex.rxjava3.core.CompletableObserver, io.reactivex.rxjava3.core.MaybeObserver
    public void onComplete() {
        if (this.config.guardObserverCallbacks) {
            DogTagCompletableObserver$$ExternalSyntheticLambda2 dogTagCompletableObserver$$ExternalSyntheticLambda2 = new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagCompletableObserver$$ExternalSyntheticLambda2
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagCompletableObserver.$r8$lambda$ogrqiN5b9Eg2yBE_5eh6cA3lGAM(DogTagCompletableObserver.this, (Throwable) obj);
                }
            };
            CompletableObserver completableObserver = this.delegate;
            Objects.requireNonNull(completableObserver);
            RxDogTag.guardedDelegateCall(dogTagCompletableObserver$$ExternalSyntheticLambda2, new Runnable() { // from class: rxdogtag2.DogTagCompletableObserver$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    CompletableObserver.this.onComplete();
                }
            });
            return;
        }
        this.delegate.onComplete();
    }

    public /* synthetic */ void lambda$onComplete$4(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onComplete");
    }

    @Override // io.reactivex.rxjava3.observers.LambdaConsumerIntrospection
    public boolean hasCustomOnError() {
        CompletableObserver completableObserver = this.delegate;
        return (completableObserver instanceof LambdaConsumerIntrospection) && ((LambdaConsumerIntrospection) completableObserver).hasCustomOnError();
    }
}
