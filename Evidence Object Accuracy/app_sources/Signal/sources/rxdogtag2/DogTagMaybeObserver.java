package rxdogtag2;

import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.LambdaConsumerIntrospection;
import java.util.Objects;
import rxdogtag2.RxDogTag;

/* loaded from: classes5.dex */
public final class DogTagMaybeObserver<T> implements MaybeObserver<T>, LambdaConsumerIntrospection {
    private final RxDogTag.Configuration config;
    private final MaybeObserver<T> delegate;
    private final Throwable t = new Throwable();

    public DogTagMaybeObserver(RxDogTag.Configuration configuration, MaybeObserver<T> maybeObserver) {
        this.config = configuration;
        this.delegate = maybeObserver;
    }

    @Override // io.reactivex.rxjava3.core.MaybeObserver
    public void onSubscribe(Disposable disposable) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda0
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagMaybeObserver.m3383$r8$lambda$hP903yBortBexsAdsC7difM9lo(DogTagMaybeObserver.this, (Throwable) obj);
                }
            }, new Runnable(disposable) { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda1
                public final /* synthetic */ Disposable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagMaybeObserver.$r8$lambda$oZbZwTXdH3PbkXhPwGJJZ5hucwc(DogTagMaybeObserver.this, this.f$1);
                }
            });
        } else {
            this.delegate.onSubscribe(disposable);
        }
    }

    public /* synthetic */ void lambda$onSubscribe$0(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onSubscribe");
    }

    public /* synthetic */ void lambda$onSubscribe$1(Disposable disposable) {
        this.delegate.onSubscribe(disposable);
    }

    @Override // io.reactivex.rxjava3.core.MaybeObserver
    public void onSuccess(T t) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda6
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagMaybeObserver.m3381$r8$lambda$F4FzMNX5yZsIdNSNIEHgVQY6D4(DogTagMaybeObserver.this, (Throwable) obj);
                }
            }, new Runnable(t) { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda7
                public final /* synthetic */ Object f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagMaybeObserver.m3382$r8$lambda$IHM4QGi59jxNgbolKLinAg8Hf4(DogTagMaybeObserver.this, this.f$1);
                }
            });
        } else {
            this.delegate.onSuccess(t);
        }
    }

    public /* synthetic */ void lambda$onSuccess$2(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onSuccess");
    }

    public /* synthetic */ void lambda$onSuccess$3(Object obj) {
        this.delegate.onSuccess(obj);
    }

    @Override // io.reactivex.rxjava3.core.MaybeObserver
    public void onError(Throwable th) {
        MaybeObserver<T> maybeObserver = this.delegate;
        if (!(maybeObserver instanceof RxDogTagErrorReceiver)) {
            RxDogTag.reportError(this.config, this.t, th, null);
        } else if (maybeObserver instanceof RxDogTagTaggedExceptionReceiver) {
            maybeObserver.onError(RxDogTag.createException(this.config, this.t, th, null));
        } else if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda2
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagMaybeObserver.$r8$lambda$bzcOrfWgNTRhzVJDYk2v0iQCf8w(DogTagMaybeObserver.this, (Throwable) obj);
                }
            }, new Runnable(th) { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda3
                public final /* synthetic */ Throwable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagMaybeObserver.$r8$lambda$QbBHr6iDvjrRtDMewAZ9O8_JY_c(DogTagMaybeObserver.this, this.f$1);
                }
            });
        } else {
            maybeObserver.onError(th);
        }
    }

    public /* synthetic */ void lambda$onError$4(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onError");
    }

    public /* synthetic */ void lambda$onError$5(Throwable th) {
        this.delegate.onError(th);
    }

    @Override // io.reactivex.rxjava3.core.MaybeObserver
    public void onComplete() {
        if (this.config.guardObserverCallbacks) {
            DogTagMaybeObserver$$ExternalSyntheticLambda4 dogTagMaybeObserver$$ExternalSyntheticLambda4 = new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda4
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagMaybeObserver.m3380$r8$lambda$3FfZZlJiD9_HSB8NJ6RSElcutU(DogTagMaybeObserver.this, (Throwable) obj);
                }
            };
            MaybeObserver<T> maybeObserver = this.delegate;
            Objects.requireNonNull(maybeObserver);
            RxDogTag.guardedDelegateCall(dogTagMaybeObserver$$ExternalSyntheticLambda4, new Runnable() { // from class: rxdogtag2.DogTagMaybeObserver$$ExternalSyntheticLambda5
                @Override // java.lang.Runnable
                public final void run() {
                    MaybeObserver.this.onComplete();
                }
            });
            return;
        }
        this.delegate.onComplete();
    }

    public /* synthetic */ void lambda$onComplete$6(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onComplete");
    }

    @Override // io.reactivex.rxjava3.observers.LambdaConsumerIntrospection
    public boolean hasCustomOnError() {
        MaybeObserver<T> maybeObserver = this.delegate;
        return (maybeObserver instanceof LambdaConsumerIntrospection) && ((LambdaConsumerIntrospection) maybeObserver).hasCustomOnError();
    }
}
