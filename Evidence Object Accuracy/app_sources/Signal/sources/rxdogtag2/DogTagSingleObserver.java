package rxdogtag2;

import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.LambdaConsumerIntrospection;
import rxdogtag2.RxDogTag;

/* loaded from: classes5.dex */
public final class DogTagSingleObserver<T> implements SingleObserver<T>, LambdaConsumerIntrospection {
    private final RxDogTag.Configuration config;
    private final SingleObserver<T> delegate;
    private final Throwable t = new Throwable();

    public DogTagSingleObserver(RxDogTag.Configuration configuration, SingleObserver<T> singleObserver) {
        this.config = configuration;
        this.delegate = singleObserver;
    }

    @Override // io.reactivex.rxjava3.core.SingleObserver
    public void onSubscribe(Disposable disposable) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagSingleObserver$$ExternalSyntheticLambda4
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagSingleObserver.$r8$lambda$JfIXqMYZGtT568YIWe19j6TrV2M(DogTagSingleObserver.this, (Throwable) obj);
                }
            }, new Runnable(disposable) { // from class: rxdogtag2.DogTagSingleObserver$$ExternalSyntheticLambda5
                public final /* synthetic */ Disposable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagSingleObserver.$r8$lambda$bu5rZib8qIaCqFy3X5GHmz5VHo0(DogTagSingleObserver.this, this.f$1);
                }
            });
        } else {
            this.delegate.onSubscribe(disposable);
        }
    }

    public /* synthetic */ void lambda$onSubscribe$0(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onSubscribe");
    }

    public /* synthetic */ void lambda$onSubscribe$1(Disposable disposable) {
        this.delegate.onSubscribe(disposable);
    }

    @Override // io.reactivex.rxjava3.core.SingleObserver
    public void onSuccess(T t) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagSingleObserver$$ExternalSyntheticLambda0
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagSingleObserver.m3388$r8$lambda$vmgZiGj6cOsMnj50SYSTPsm9bo(DogTagSingleObserver.this, (Throwable) obj);
                }
            }, new Runnable(t) { // from class: rxdogtag2.DogTagSingleObserver$$ExternalSyntheticLambda1
                public final /* synthetic */ Object f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagSingleObserver.$r8$lambda$XHPr6lQHRHXcw_CKIYStMAqfnNo(DogTagSingleObserver.this, this.f$1);
                }
            });
        } else {
            this.delegate.onSuccess(t);
        }
    }

    public /* synthetic */ void lambda$onSuccess$2(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onSuccess");
    }

    public /* synthetic */ void lambda$onSuccess$3(Object obj) {
        this.delegate.onSuccess(obj);
    }

    @Override // io.reactivex.rxjava3.core.SingleObserver
    public void onError(Throwable th) {
        SingleObserver<T> singleObserver = this.delegate;
        if (!(singleObserver instanceof RxDogTagErrorReceiver)) {
            RxDogTag.reportError(this.config, this.t, th, null);
        } else if (singleObserver instanceof RxDogTagTaggedExceptionReceiver) {
            singleObserver.onError(RxDogTag.createException(this.config, this.t, th, null));
        } else if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagSingleObserver$$ExternalSyntheticLambda2
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagSingleObserver.$r8$lambda$dbtNvY991lQ9zaL37GwbVoTZmKI(DogTagSingleObserver.this, (Throwable) obj);
                }
            }, new Runnable(th) { // from class: rxdogtag2.DogTagSingleObserver$$ExternalSyntheticLambda3
                public final /* synthetic */ Throwable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagSingleObserver.m3387$r8$lambda$tzVL0vWk0IENnWH7jI7Ysy_t4s(DogTagSingleObserver.this, this.f$1);
                }
            });
        } else {
            singleObserver.onError(th);
        }
    }

    public /* synthetic */ void lambda$onError$4(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onError");
    }

    public /* synthetic */ void lambda$onError$5(Throwable th) {
        this.delegate.onError(th);
    }

    @Override // io.reactivex.rxjava3.observers.LambdaConsumerIntrospection
    public boolean hasCustomOnError() {
        SingleObserver<T> singleObserver = this.delegate;
        return (singleObserver instanceof LambdaConsumerIntrospection) && ((LambdaConsumerIntrospection) singleObserver).hasCustomOnError();
    }
}
