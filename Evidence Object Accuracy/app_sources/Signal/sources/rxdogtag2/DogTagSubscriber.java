package rxdogtag2;

import io.reactivex.rxjava3.core.FlowableSubscriber;
import io.reactivex.rxjava3.observers.LambdaConsumerIntrospection;
import java.util.Objects;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import rxdogtag2.RxDogTag;

/* loaded from: classes5.dex */
public final class DogTagSubscriber<T> implements FlowableSubscriber<T>, LambdaConsumerIntrospection {
    private final RxDogTag.Configuration config;
    private final Subscriber<T> delegate;
    private final Throwable t = new Throwable();

    public DogTagSubscriber(RxDogTag.Configuration configuration, Subscriber<T> subscriber) {
        this.config = configuration;
        this.delegate = subscriber;
    }

    @Override // io.reactivex.rxjava3.core.FlowableSubscriber, org.reactivestreams.Subscriber
    public void onSubscribe(Subscription subscription) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda4
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagSubscriber.$r8$lambda$GqMhbyhtkdnlrS2zaa8Z2z1_P0g(DogTagSubscriber.this, (Throwable) obj);
                }
            }, new Runnable(subscription) { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda5
                public final /* synthetic */ Subscription f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagSubscriber.$r8$lambda$2zfLu5RX2IvP1ii4ybHL0tD6hpo(DogTagSubscriber.this, this.f$1);
                }
            });
        } else {
            this.delegate.onSubscribe(subscription);
        }
    }

    public /* synthetic */ void lambda$onSubscribe$0(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onSubscribe");
    }

    public /* synthetic */ void lambda$onSubscribe$1(Subscription subscription) {
        this.delegate.onSubscribe(subscription);
    }

    @Override // org.reactivestreams.Subscriber
    public void onNext(T t) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda2
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagSubscriber.$r8$lambda$WvJQVgWA4Ob1S5Pegm3Y85IEG58(DogTagSubscriber.this, (Throwable) obj);
                }
            }, new Runnable(t) { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda3
                public final /* synthetic */ Object f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagSubscriber.$r8$lambda$FAKufiO9uOSxkNLq0iovS2ks3ic(DogTagSubscriber.this, this.f$1);
                }
            });
        } else {
            this.delegate.onNext(t);
        }
    }

    public /* synthetic */ void lambda$onNext$2(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onNext");
    }

    public /* synthetic */ void lambda$onNext$3(Object obj) {
        this.delegate.onNext(obj);
    }

    @Override // org.reactivestreams.Subscriber
    public void onError(Throwable th) {
        Subscriber<T> subscriber = this.delegate;
        if (!(subscriber instanceof RxDogTagErrorReceiver)) {
            RxDogTag.reportError(this.config, this.t, th, null);
        } else if (subscriber instanceof RxDogTagTaggedExceptionReceiver) {
            subscriber.onError(RxDogTag.createException(this.config, this.t, th, null));
        } else if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda6
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagSubscriber.m3390$r8$lambda$k4GjgOt87b0IuTlpJYXWSEFxRI(DogTagSubscriber.this, (Throwable) obj);
                }
            }, new Runnable(th) { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda7
                public final /* synthetic */ Throwable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagSubscriber.m3389$r8$lambda$C8IgXE2vvqMw6BP29dDFumwEhY(DogTagSubscriber.this, this.f$1);
                }
            });
        } else {
            subscriber.onError(th);
        }
    }

    public /* synthetic */ void lambda$onError$4(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onError");
    }

    public /* synthetic */ void lambda$onError$5(Throwable th) {
        this.delegate.onError(th);
    }

    @Override // org.reactivestreams.Subscriber
    public void onComplete() {
        if (this.config.guardObserverCallbacks) {
            DogTagSubscriber$$ExternalSyntheticLambda0 dogTagSubscriber$$ExternalSyntheticLambda0 = new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda0
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagSubscriber.$r8$lambda$ts0lXqMxOJgs5GIMah9FuNlFI_Q(DogTagSubscriber.this, (Throwable) obj);
                }
            };
            Subscriber<T> subscriber = this.delegate;
            Objects.requireNonNull(subscriber);
            RxDogTag.guardedDelegateCall(dogTagSubscriber$$ExternalSyntheticLambda0, new Runnable() { // from class: rxdogtag2.DogTagSubscriber$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    Subscriber.this.onComplete();
                }
            });
            return;
        }
        this.delegate.onComplete();
    }

    public /* synthetic */ void lambda$onComplete$6(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onComplete");
    }

    @Override // io.reactivex.rxjava3.observers.LambdaConsumerIntrospection
    public boolean hasCustomOnError() {
        Subscriber<T> subscriber = this.delegate;
        return (subscriber instanceof LambdaConsumerIntrospection) && ((LambdaConsumerIntrospection) subscriber).hasCustomOnError();
    }
}
