package rxdogtag2;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeObserver;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.exceptions.OnErrorNotImplementedException;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.observers.LambdaConsumerIntrospection;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.reactivestreams.Subscriber;
import rxdogtag2.ObserverHandler;
import rxdogtag2.RxDogTag;

/* loaded from: classes.dex */
public final class RxDogTag {
    public static final String STACK_ELEMENT_SOURCE_DELEGATE;
    public static final String STACK_ELEMENT_SOURCE_HEADER;
    public static final String STACK_ELEMENT_TRACE_HEADER;

    /* loaded from: classes5.dex */
    public interface Configurer {
        void apply(Builder builder);
    }

    /* loaded from: classes5.dex */
    public interface NonCheckingConsumer<T> {
        void accept(T t);
    }

    /* loaded from: classes5.dex */
    public interface NonCheckingPredicate<T> {
        boolean test(T t);
    }

    private RxDogTag() {
        throw new InstantiationError();
    }

    public static synchronized void reset() {
        synchronized (RxDogTag.class) {
            RxJavaPlugins.setOnFlowableSubscribe(null);
            RxJavaPlugins.setOnObservableSubscribe(null);
            RxJavaPlugins.setOnMaybeSubscribe(null);
            RxJavaPlugins.setOnSingleSubscribe(null);
            RxJavaPlugins.setOnCompletableSubscribe(null);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static void install() {
        new Builder().install();
    }

    private static boolean shouldDecorate(Object obj) {
        if (obj instanceof RxDogTagErrorReceiver) {
            return true;
        }
        if (obj instanceof LambdaConsumerIntrospection) {
            return !((LambdaConsumerIntrospection) obj).hasCustomOnError();
        }
        return false;
    }

    public static synchronized void installWithBuilder(Configuration configuration) {
        synchronized (RxDogTag.class) {
            RxJavaPlugins.setOnObservableSubscribe(new BiFunction() { // from class: rxdogtag2.RxDogTag$$ExternalSyntheticLambda0
                @Override // io.reactivex.rxjava3.functions.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return RxDogTag.lambda$installWithBuilder$0(RxDogTag.Configuration.this, (Observable) obj, (Observer) obj2);
                }
            });
            RxJavaPlugins.setOnFlowableSubscribe(new BiFunction() { // from class: rxdogtag2.RxDogTag$$ExternalSyntheticLambda1
                @Override // io.reactivex.rxjava3.functions.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return RxDogTag.lambda$installWithBuilder$1(RxDogTag.Configuration.this, (Flowable) obj, (Subscriber) obj2);
                }
            });
            RxJavaPlugins.setOnSingleSubscribe(new BiFunction() { // from class: rxdogtag2.RxDogTag$$ExternalSyntheticLambda2
                @Override // io.reactivex.rxjava3.functions.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return RxDogTag.lambda$installWithBuilder$2(RxDogTag.Configuration.this, (Single) obj, (SingleObserver) obj2);
                }
            });
            RxJavaPlugins.setOnMaybeSubscribe(new BiFunction() { // from class: rxdogtag2.RxDogTag$$ExternalSyntheticLambda3
                @Override // io.reactivex.rxjava3.functions.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return RxDogTag.lambda$installWithBuilder$3(RxDogTag.Configuration.this, (Maybe) obj, (MaybeObserver) obj2);
                }
            });
            RxJavaPlugins.setOnCompletableSubscribe(new BiFunction() { // from class: rxdogtag2.RxDogTag$$ExternalSyntheticLambda4
                @Override // io.reactivex.rxjava3.functions.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return RxDogTag.lambda$installWithBuilder$4(RxDogTag.Configuration.this, (Completable) obj, (CompletableObserver) obj2);
                }
            });
        }
    }

    public static /* synthetic */ Observer lambda$installWithBuilder$0(Configuration configuration, Observable observable, Observer observer) throws Throwable {
        for (ObserverHandler observerHandler : configuration.observerHandlers) {
            if (shouldDecorate(observerHandler.handle(observable, observer))) {
                return new DogTagObserver(configuration, observer);
            }
        }
        return observer;
    }

    public static /* synthetic */ Subscriber lambda$installWithBuilder$1(Configuration configuration, Flowable flowable, Subscriber subscriber) throws Throwable {
        for (ObserverHandler observerHandler : configuration.observerHandlers) {
            if (shouldDecorate(observerHandler.handle(flowable, subscriber))) {
                return new DogTagSubscriber(configuration, subscriber);
            }
        }
        return subscriber;
    }

    public static /* synthetic */ SingleObserver lambda$installWithBuilder$2(Configuration configuration, Single single, SingleObserver singleObserver) throws Throwable {
        for (ObserverHandler observerHandler : configuration.observerHandlers) {
            if (shouldDecorate(observerHandler.handle(single, singleObserver))) {
                return new DogTagSingleObserver(configuration, singleObserver);
            }
        }
        return singleObserver;
    }

    public static /* synthetic */ MaybeObserver lambda$installWithBuilder$3(Configuration configuration, Maybe maybe, MaybeObserver maybeObserver) throws Throwable {
        for (ObserverHandler observerHandler : configuration.observerHandlers) {
            if (shouldDecorate(observerHandler.handle(maybe, maybeObserver))) {
                return new DogTagMaybeObserver(configuration, maybeObserver);
            }
        }
        return maybeObserver;
    }

    public static /* synthetic */ CompletableObserver lambda$installWithBuilder$4(Configuration configuration, Completable completable, CompletableObserver completableObserver) throws Throwable {
        for (ObserverHandler observerHandler : configuration.observerHandlers) {
            if (shouldDecorate(observerHandler.handle(completable, completableObserver))) {
                return new DogTagCompletableObserver(configuration, completableObserver);
            }
        }
        return completableObserver;
    }

    private static StackTraceElement extractStackElementTag(Throwable th, Set<String> set) {
        StackTraceElement[] stackTrace = th.getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (!containsAnyPackages(stackTraceElement.getClassName(), set)) {
                return stackTraceElement;
            }
        }
        return new StackTraceElement("Unknown", "unknown", "unknown", 0);
    }

    public static void guardedDelegateCall(NonCheckingConsumer<Throwable> nonCheckingConsumer, Runnable runnable) {
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler;
        try {
            uncaughtExceptionHandler = Thread.currentThread().getUncaughtExceptionHandler();
            try {
                Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(uncaughtExceptionHandler, nonCheckingConsumer) { // from class: rxdogtag2.RxDogTag$$ExternalSyntheticLambda5
                    public final /* synthetic */ Thread.UncaughtExceptionHandler f$0;
                    public final /* synthetic */ RxDogTag.NonCheckingConsumer f$1;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Thread.UncaughtExceptionHandler
                    public final void uncaughtException(Thread thread, Throwable th) {
                        RxDogTag.lambda$guardedDelegateCall$5(this.f$0, this.f$1, thread, th);
                    }
                });
                runnable.run();
            } catch (OnErrorNotImplementedException e) {
                th = e.getCause();
            } finally {
            }
        } finally {
            Thread.currentThread().setUncaughtExceptionHandler(uncaughtExceptionHandler);
        }
    }

    public static /* synthetic */ void lambda$guardedDelegateCall$5(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, NonCheckingConsumer nonCheckingConsumer, Thread thread, Throwable th) {
        Thread.currentThread().setUncaughtExceptionHandler(uncaughtExceptionHandler);
        if (th instanceof OnErrorNotImplementedException) {
            nonCheckingConsumer.accept(th);
        } else if (!(th instanceof NullPointerException) || !"subscribeActual failed".equals(th.getMessage())) {
            uncaughtExceptionHandler.uncaughtException(thread, th);
        } else {
            nonCheckingConsumer.accept(th.getCause());
        }
    }

    public static OnErrorNotImplementedException createException(Configuration configuration, Throwable th, Throwable th2, String str) {
        OnErrorNotImplementedException onErrorNotImplementedException;
        StackTraceElement[] stackTraceElementArr;
        StackTraceElement extractStackElementTag = extractStackElementTag(th, configuration.ignoredPackages);
        if (configuration.repackageOnErrorNotImplementedExceptions && (th2 instanceof OnErrorNotImplementedException)) {
            th2 = th2.getCause();
        }
        if (th2 instanceof OnErrorNotImplementedException) {
            OnErrorNotImplementedException onErrorNotImplementedException2 = (OnErrorNotImplementedException) th2;
            th2 = onErrorNotImplementedException2.getCause();
            onErrorNotImplementedException = onErrorNotImplementedException2;
        } else {
            String message = th2.getMessage();
            if (message == null) {
                message = "";
            }
            onErrorNotImplementedException = new OnErrorNotImplementedException(message, th2);
            onErrorNotImplementedException.setStackTrace(new StackTraceElement[0]);
        }
        StackTraceElement[] stackTrace = th2.getStackTrace();
        char c = 3;
        int i = str != null ? 4 : 3;
        if (configuration.disableAnnotations) {
            stackTraceElementArr = new StackTraceElement[stackTrace.length + 1];
            stackTraceElementArr[0] = extractStackElementTag;
            if (stackTrace.length != 0) {
                System.arraycopy(stackTrace, 0, stackTraceElementArr, 1, stackTrace.length);
            }
        } else {
            int indexOfLast = indexOfLast(stackTrace, new NonCheckingPredicate() { // from class: rxdogtag2.RxDogTag$$ExternalSyntheticLambda6
                @Override // rxdogtag2.RxDogTag.NonCheckingPredicate
                public final boolean test(Object obj) {
                    return RxDogTag.lambda$createException$6((StackTraceElement) obj);
                }
            });
            int i2 = indexOfLast != -1 ? indexOfLast + 1 : 0;
            StackTraceElement[] stackTraceElementArr2 = new StackTraceElement[(stackTrace.length + i) - i2];
            stackTraceElementArr2[0] = extractStackElementTag;
            stackTraceElementArr2[1] = new StackTraceElement(STACK_ELEMENT_SOURCE_HEADER, "", "", 0);
            if (str != null) {
                stackTraceElementArr2[2] = new StackTraceElement(String.format(Locale.US, STACK_ELEMENT_SOURCE_DELEGATE, str), "", "", 0);
            } else {
                c = 2;
            }
            stackTraceElementArr2[c] = new StackTraceElement(STACK_ELEMENT_TRACE_HEADER, "", "", 0);
            if (stackTrace.length != 0) {
                System.arraycopy(stackTrace, i2, stackTraceElementArr2, i, stackTrace.length - i2);
            }
            stackTraceElementArr = stackTraceElementArr2;
        }
        th2.setStackTrace(stackTraceElementArr);
        return onErrorNotImplementedException;
    }

    public static /* synthetic */ boolean lambda$createException$6(StackTraceElement stackTraceElement) {
        return STACK_ELEMENT_TRACE_HEADER.equals(stackTraceElement.getClassName());
    }

    public static void reportError(Configuration configuration, Throwable th, Throwable th2, String str) {
        RxJavaPlugins.onError(createException(configuration, th, th2, str));
    }

    private static boolean containsAnyPackages(String str, Set<String> set) {
        for (String str2 : set) {
            if (str.startsWith(str2)) {
                return true;
            }
        }
        return false;
    }

    private static <T> int indexOfLast(T[] tArr, NonCheckingPredicate<T> nonCheckingPredicate) {
        for (int length = tArr.length - 1; length >= 0; length--) {
            if (nonCheckingPredicate.test(tArr[length])) {
                return length;
            }
        }
        return -1;
    }

    /* loaded from: classes5.dex */
    public static final class Builder {
        boolean disableAnnotations = false;
        boolean guardObserverCallbacks = true;
        Set<String> ignoredPackages = new LinkedHashSet();
        List<ObserverHandler> observerHandlers = new ArrayList();
        boolean repackageOnErrorNotImplementedExceptions = true;

        Builder() {
        }

        public Builder disableAnnotations() {
            this.disableAnnotations = true;
            return this;
        }

        public Builder addObserverHandlers(ObserverHandler... observerHandlerArr) {
            return addObserverHandlers(Arrays.asList(observerHandlerArr));
        }

        public Builder addObserverHandlers(Collection<ObserverHandler> collection) {
            this.observerHandlers.addAll(collection);
            return this;
        }

        public Builder addIgnoredPackages(String... strArr) {
            return addIgnoredPackages(Arrays.asList(strArr));
        }

        public Builder addIgnoredPackages(Collection<String> collection) {
            this.ignoredPackages.addAll(collection);
            return this;
        }

        public Builder guardObserverCallbacks(boolean z) {
            this.guardObserverCallbacks = z;
            return this;
        }

        public Builder configureWith(Configurer configurer) {
            configurer.apply(this);
            return this;
        }

        public Builder disableRepackagingOnErrorNotImplementedExceptions() {
            this.repackageOnErrorNotImplementedExceptions = false;
            return this;
        }

        public void install() {
            RxDogTag.installWithBuilder(new Configuration(this));
        }
    }

    /* loaded from: classes5.dex */
    public static class Configuration {
        private static final ObserverHandler DEFAULT_HANDLER = new ObserverHandler() { // from class: rxdogtag2.RxDogTag.Configuration.1
            @Override // rxdogtag2.ObserverHandler
            public /* synthetic */ CompletableObserver handle(Completable completable, CompletableObserver completableObserver) {
                return ObserverHandler.CC.$default$handle(this, completable, completableObserver);
            }

            @Override // rxdogtag2.ObserverHandler
            public /* synthetic */ MaybeObserver handle(Maybe maybe, MaybeObserver maybeObserver) {
                return ObserverHandler.CC.$default$handle(this, maybe, maybeObserver);
            }

            @Override // rxdogtag2.ObserverHandler
            public /* synthetic */ Observer handle(Observable observable, Observer observer) {
                return ObserverHandler.CC.$default$handle(this, observable, observer);
            }

            @Override // rxdogtag2.ObserverHandler
            public /* synthetic */ SingleObserver handle(Single single, SingleObserver singleObserver) {
                return ObserverHandler.CC.$default$handle(this, single, singleObserver);
            }

            @Override // rxdogtag2.ObserverHandler
            public /* synthetic */ Subscriber handle(Flowable flowable, Subscriber subscriber) {
                return ObserverHandler.CC.$default$handle(this, flowable, subscriber);
            }
        };
        private static final Collection<String> DEFAULT_IGNORED_PACKAGES = Arrays.asList("io.reactivex.rxjava3", DogTagObserver.class.getPackage().getName());
        final boolean disableAnnotations;
        final boolean guardObserverCallbacks;
        final Set<String> ignoredPackages;
        final List<ObserverHandler> observerHandlers;
        final boolean repackageOnErrorNotImplementedExceptions;

        Configuration(Builder builder) {
            this.disableAnnotations = builder.disableAnnotations;
            ArrayList arrayList = new ArrayList(builder.observerHandlers);
            arrayList.add(DEFAULT_HANDLER);
            LinkedHashSet linkedHashSet = new LinkedHashSet(builder.ignoredPackages);
            linkedHashSet.addAll(DEFAULT_IGNORED_PACKAGES);
            this.observerHandlers = Collections.unmodifiableList(arrayList);
            this.ignoredPackages = Collections.unmodifiableSet(linkedHashSet);
            this.repackageOnErrorNotImplementedExceptions = builder.repackageOnErrorNotImplementedExceptions;
            this.guardObserverCallbacks = builder.guardObserverCallbacks;
        }
    }
}
