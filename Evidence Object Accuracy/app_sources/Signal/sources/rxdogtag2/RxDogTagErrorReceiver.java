package rxdogtag2;

/* loaded from: classes5.dex */
public interface RxDogTagErrorReceiver {
    void onError(Throwable th);
}
