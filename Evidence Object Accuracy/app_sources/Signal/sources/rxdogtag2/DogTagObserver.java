package rxdogtag2;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.LambdaConsumerIntrospection;
import java.util.Objects;
import rxdogtag2.RxDogTag;

/* loaded from: classes5.dex */
public final class DogTagObserver<T> implements Observer<T>, LambdaConsumerIntrospection {
    private final RxDogTag.Configuration config;
    private final Observer<T> delegate;
    private final Throwable t = new Throwable();

    public DogTagObserver(RxDogTag.Configuration configuration, Observer<T> observer) {
        this.config = configuration;
        this.delegate = observer;
    }

    @Override // io.reactivex.rxjava3.core.Observer
    public void onSubscribe(Disposable disposable) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda4
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagObserver.m3386$r8$lambda$NasJj3oRBUZpMcNpkxFuqQXaP0(DogTagObserver.this, (Throwable) obj);
                }
            }, new Runnable(disposable) { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda5
                public final /* synthetic */ Disposable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagObserver.$r8$lambda$tfuuFW4uJ1e1bE4RxcLKQ9tobZo(DogTagObserver.this, this.f$1);
                }
            });
        } else {
            this.delegate.onSubscribe(disposable);
        }
    }

    public /* synthetic */ void lambda$onSubscribe$0(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onSubscribe");
    }

    public /* synthetic */ void lambda$onSubscribe$1(Disposable disposable) {
        this.delegate.onSubscribe(disposable);
    }

    @Override // io.reactivex.rxjava3.core.Observer
    public void onNext(T t) {
        if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda6
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagObserver.m3385$r8$lambda$DP6QkMeYCOb9nq7X8Wm87FriyQ(DogTagObserver.this, (Throwable) obj);
                }
            }, new Runnable(t) { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda7
                public final /* synthetic */ Object f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagObserver.$r8$lambda$H_QNeov4x3pXt2znCyqAzFaM5fE(DogTagObserver.this, this.f$1);
                }
            });
        } else {
            this.delegate.onNext(t);
        }
    }

    public /* synthetic */ void lambda$onNext$2(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onNext");
    }

    public /* synthetic */ void lambda$onNext$3(Object obj) {
        this.delegate.onNext(obj);
    }

    @Override // io.reactivex.rxjava3.core.Observer
    public void onError(Throwable th) {
        Observer<T> observer = this.delegate;
        if (!(observer instanceof RxDogTagErrorReceiver)) {
            RxDogTag.reportError(this.config, this.t, th, null);
        } else if (observer instanceof RxDogTagTaggedExceptionReceiver) {
            observer.onError(RxDogTag.createException(this.config, this.t, th, null));
        } else if (this.config.guardObserverCallbacks) {
            RxDogTag.guardedDelegateCall(new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda0
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagObserver.m3384$r8$lambda$8NsoS8bvNwcIULKuQadPa7kFoA(DogTagObserver.this, (Throwable) obj);
                }
            }, new Runnable(th) { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda1
                public final /* synthetic */ Throwable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DogTagObserver.$r8$lambda$6BOjxfmh7dtAyc32S7DcgfddQnk(DogTagObserver.this, this.f$1);
                }
            });
        } else {
            observer.onError(th);
        }
    }

    public /* synthetic */ void lambda$onError$4(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onError");
    }

    public /* synthetic */ void lambda$onError$5(Throwable th) {
        this.delegate.onError(th);
    }

    @Override // io.reactivex.rxjava3.core.Observer
    public void onComplete() {
        if (this.config.guardObserverCallbacks) {
            DogTagObserver$$ExternalSyntheticLambda2 dogTagObserver$$ExternalSyntheticLambda2 = new RxDogTag.NonCheckingConsumer() { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda2
                @Override // rxdogtag2.RxDogTag.NonCheckingConsumer
                public final void accept(Object obj) {
                    DogTagObserver.$r8$lambda$lM03tKnGMZu3T65MY3E9c5f_5ls(DogTagObserver.this, (Throwable) obj);
                }
            };
            Observer<T> observer = this.delegate;
            Objects.requireNonNull(observer);
            RxDogTag.guardedDelegateCall(dogTagObserver$$ExternalSyntheticLambda2, new Runnable() { // from class: rxdogtag2.DogTagObserver$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    Observer.this.onComplete();
                }
            });
            return;
        }
        this.delegate.onComplete();
    }

    public /* synthetic */ void lambda$onComplete$6(Throwable th) {
        RxDogTag.reportError(this.config, this.t, th, "onComplete");
    }

    @Override // io.reactivex.rxjava3.observers.LambdaConsumerIntrospection
    public boolean hasCustomOnError() {
        Observer<T> observer = this.delegate;
        return (observer instanceof LambdaConsumerIntrospection) && ((LambdaConsumerIntrospection) observer).hasCustomOnError();
    }
}
