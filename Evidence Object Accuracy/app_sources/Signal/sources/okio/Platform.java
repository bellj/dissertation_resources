package okio;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.Charsets;

/* compiled from: -Platform.kt */
@Metadata(bv = {}, d1 = {"\u00002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a0\u0010\b\u001a\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0002H\u0000\u001a\f\u0010\n\u001a\u00020\t*\u00020\u0000H\u0000\u001a\f\u0010\u000b\u001a\u00020\u0000*\u00020\tH\u0000*\n\u0010\r\"\u00020\f2\u00020\f*\n\u0010\u000f\"\u00020\u000e2\u00020\u000e*\n\u0010\u0010\"\u00020\u000e2\u00020\u000e*\n\u0010\u0011\"\u00020\u000e2\u00020\u000e*\n\u0010\u0013\"\u00020\u00122\u00020\u0012¨\u0006\u0014"}, d2 = {"", "src", "", "srcPos", "dest", "destPos", "length", "", "arraycopy", "", "toUtf8String", "asUtf8ToByteArray", "Ljava/lang/ArrayIndexOutOfBoundsException;", "ArrayIndexOutOfBoundsException", "", "JvmField", "JvmName", "JvmOverloads", "Lkotlin/jvm/JvmStatic;", "JvmStatic", "jvm"}, k = 2, mv = {1, 4, 0})
/* renamed from: okio.-Platform */
/* loaded from: classes3.dex */
public final class Platform {
    public static final void arraycopy(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        Intrinsics.checkParameterIsNotNull(bArr, "src");
        Intrinsics.checkParameterIsNotNull(bArr2, "dest");
        System.arraycopy(bArr, i, bArr2, i2, i3);
    }

    public static final String toUtf8String(byte[] bArr) {
        Intrinsics.checkParameterIsNotNull(bArr, "$receiver");
        return new String(bArr, Charsets.UTF_8);
    }

    public static final byte[] asUtf8ToByteArray(String str) {
        Intrinsics.checkParameterIsNotNull(str, "$receiver");
        byte[] bytes = str.getBytes(Charsets.UTF_8);
        Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }
}
