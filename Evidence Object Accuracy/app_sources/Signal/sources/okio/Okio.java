package okio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.database.PushDatabase;

/* compiled from: Okio.kt */
@Metadata(bv = {}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0000\u001a\n\u0010\u0002\u001a\u00020\u0004*\u00020\u0003\u001a\n\u0010\u0006\u001a\u00020\u0003*\u00020\u0005\u001a\n\u0010\b\u001a\u00020\u0000*\u00020\u0007\u001a\u000f\u0010\u000b\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\t\u0010\n\u001a\n\u0010\u0006\u001a\u00020\u0003*\u00020\f\u001a\n\u0010\b\u001a\u00020\u0000*\u00020\f\u001a\u0016\u0010\u0006\u001a\u00020\u0003*\u00020\r2\b\b\u0002\u0010\u000f\u001a\u00020\u000eH\u0007\u001a\n\u0010\u0010\u001a\u00020\u0003*\u00020\r\u001a\n\u0010\b\u001a\u00020\u0000*\u00020\r\"\u001c\u0010\u0013\u001a\u00020\u000e*\u00060\u0011j\u0002`\u00128@X\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0015"}, d2 = {"Lokio/Source;", "Lokio/BufferedSource;", "buffer", "Lokio/Sink;", "Lokio/BufferedSink;", "Ljava/io/OutputStream;", "sink", "Ljava/io/InputStream;", PushDatabase.SOURCE_E164, "blackhole", "()Lokio/Sink;", "blackholeSink", "Ljava/net/Socket;", "Ljava/io/File;", "", "append", "appendingSink", "Ljava/lang/AssertionError;", "Lkotlin/AssertionError;", "isAndroidGetsocknameError", "(Ljava/lang/AssertionError;)Z", "jvm"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class Okio {
    public static final Sink sink(File file) throws FileNotFoundException {
        return sink$default(file, false, 1, null);
    }

    public static final BufferedSource buffer(Source source) {
        Intrinsics.checkParameterIsNotNull(source, "$receiver");
        return new RealBufferedSource(source);
    }

    public static final BufferedSink buffer(Sink sink) {
        Intrinsics.checkParameterIsNotNull(sink, "$receiver");
        return new RealBufferedSink(sink);
    }

    public static final Sink sink(OutputStream outputStream) {
        Intrinsics.checkParameterIsNotNull(outputStream, "$receiver");
        return new OutputStreamSink(outputStream, new Timeout());
    }

    public static final Source source(InputStream inputStream) {
        Intrinsics.checkParameterIsNotNull(inputStream, "$receiver");
        return new InputStreamSource(inputStream, new Timeout());
    }

    public static final Sink blackhole() {
        return new BlackholeSink();
    }

    public static final Sink sink(Socket socket) throws IOException {
        Intrinsics.checkParameterIsNotNull(socket, "$receiver");
        SocketAsyncTimeout socketAsyncTimeout = new SocketAsyncTimeout(socket);
        OutputStream outputStream = socket.getOutputStream();
        Intrinsics.checkExpressionValueIsNotNull(outputStream, "getOutputStream()");
        return socketAsyncTimeout.sink(new OutputStreamSink(outputStream, socketAsyncTimeout));
    }

    public static final Source source(Socket socket) throws IOException {
        Intrinsics.checkParameterIsNotNull(socket, "$receiver");
        SocketAsyncTimeout socketAsyncTimeout = new SocketAsyncTimeout(socket);
        InputStream inputStream = socket.getInputStream();
        Intrinsics.checkExpressionValueIsNotNull(inputStream, "getInputStream()");
        return socketAsyncTimeout.source(new InputStreamSource(inputStream, socketAsyncTimeout));
    }

    public static final Sink sink(File file, boolean z) throws FileNotFoundException {
        Intrinsics.checkParameterIsNotNull(file, "$receiver");
        return sink(new FileOutputStream(file, z));
    }

    public static /* bridge */ /* synthetic */ Sink sink$default(File file, boolean z, int i, Object obj) throws FileNotFoundException {
        if ((i & 1) != 0) {
            z = false;
        }
        return sink(file, z);
    }

    public static final Sink appendingSink(File file) throws FileNotFoundException {
        Intrinsics.checkParameterIsNotNull(file, "$receiver");
        return sink(new FileOutputStream(file, true));
    }

    public static final Source source(File file) throws FileNotFoundException {
        Intrinsics.checkParameterIsNotNull(file, "$receiver");
        return source(new FileInputStream(file));
    }

    public static final boolean isAndroidGetsocknameError(AssertionError assertionError) {
        Intrinsics.checkParameterIsNotNull(assertionError, "$receiver");
        if (assertionError.getCause() == null) {
            return false;
        }
        String message = assertionError.getMessage();
        return message != null ? StringsKt__StringsKt.contains$default((CharSequence) message, (CharSequence) "getsockname failed", false, 2, (Object) null) : false;
    }
}
