package okio;

import java.security.MessageDigest;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SegmentedByteString.kt */
@Metadata(bv = {}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u0005\n\u0002\b\u0006\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\u0015\n\u0002\b\b\b\u0000\u0018\u0000 62\u00020\u0001:\u00016B\u001f\b\u0002\u0012\f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00150*\u0012\u0006\u00100\u001a\u00020/¢\u0006\u0004\b4\u00105J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0002J\b\u0010\u0005\u001a\u00020\u0001H\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0016J\b\u0010\b\u001a\u00020\u0006H\u0016J\b\u0010\t\u001a\u00020\u0001H\u0016J\u0017\u0010\r\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0006H\u0010¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u0002H\u0010¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0014\u001a\u00020\u0002H\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\b\u0010\u0016\u001a\u00020\u0015H\u0016J\u0017\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u0018\u001a\u00020\u0017H\u0010¢\u0006\u0004\b\u001a\u0010\u001bJ(\u0010\"\u001a\u00020!2\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u00012\u0006\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020\u0002H\u0016J(\u0010\"\u001a\u00020!2\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020\u0002H\u0016J\u000f\u0010%\u001a\u00020\u0015H\u0010¢\u0006\u0004\b#\u0010$J\u0013\u0010'\u001a\u00020!2\b\u0010\u001e\u001a\u0004\u0018\u00010&H\u0002J\b\u0010(\u001a\u00020\u0002H\u0016J\b\u0010)\u001a\u00020\u0006H\u0016R \u0010+\u001a\b\u0012\u0004\u0012\u00020\u00150*8\u0006X\u0004¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u001a\u00100\u001a\u00020/8\u0006X\u0004¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103¨\u00067"}, d2 = {"Lokio/SegmentedByteString;", "Lokio/ByteString;", "", "pos", "segment", "toByteString", "", "base64", "hex", "toAsciiLowercase", "algorithm", "digest$jvm", "(Ljava/lang/String;)Lokio/ByteString;", "digest", "", "internalGet$jvm", "(I)B", "internalGet", "getSize$jvm", "()I", "getSize", "", "toByteArray", "Lokio/Buffer;", "buffer", "", "write$jvm", "(Lokio/Buffer;)V", "write", "offset", "other", "otherOffset", "byteCount", "", "rangeEquals", "internalArray$jvm", "()[B", "internalArray", "", "equals", "hashCode", "toString", "", "segments", "[[B", "getSegments", "()[[B", "", "directory", "[I", "getDirectory", "()[I", "<init>", "([[B[I)V", "Companion", "jvm"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class SegmentedByteString extends ByteString {
    public static final Companion Companion = new Companion(null);
    private final transient int[] directory;
    private final transient byte[][] segments;

    public /* synthetic */ SegmentedByteString(byte[][] bArr, int[] iArr, DefaultConstructorMarker defaultConstructorMarker) {
        this(bArr, iArr);
    }

    public final byte[][] getSegments() {
        return this.segments;
    }

    public final int[] getDirectory() {
        return this.directory;
    }

    private SegmentedByteString(byte[][] bArr, int[] iArr) {
        super(ByteString.EMPTY.getData$jvm());
        this.segments = bArr;
        this.directory = iArr;
    }

    /* compiled from: SegmentedByteString.kt */
    @Metadata(bv = {1, 0, 2}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"}, d2 = {"Lokio/SegmentedByteString$Companion;", "", "()V", "of", "Lokio/ByteString;", "buffer", "Lokio/Buffer;", "byteCount", "", "jvm"}, k = 1, mv = {1, 1, 11})
    /* loaded from: classes3.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final ByteString of(Buffer buffer, int i) {
            Intrinsics.checkParameterIsNotNull(buffer, "buffer");
            Util.checkOffsetAndCount(buffer.size(), 0, (long) i);
            Segment segment = buffer.head;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            while (i3 < i) {
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                int i5 = segment.limit;
                int i6 = segment.pos;
                if (i5 != i6) {
                    i3 += i5 - i6;
                    i4++;
                    segment = segment.next;
                } else {
                    throw new AssertionError("s.limit == s.pos");
                }
            }
            byte[][] bArr = new byte[i4];
            int[] iArr = new int[i4 * 2];
            Segment segment2 = buffer.head;
            int i7 = 0;
            while (i2 < i) {
                if (segment2 == null) {
                    Intrinsics.throwNpe();
                }
                bArr[i7] = segment2.data;
                i2 += segment2.limit - segment2.pos;
                iArr[i7] = Math.min(i2, i);
                iArr[i7 + i4] = segment2.pos;
                segment2.shared = true;
                i7++;
                segment2 = segment2.next;
            }
            return new SegmentedByteString(bArr, iArr, null);
        }
    }

    @Override // okio.ByteString
    public String base64() {
        return toByteString().base64();
    }

    @Override // okio.ByteString
    public String hex() {
        return toByteString().hex();
    }

    @Override // okio.ByteString
    public ByteString toAsciiLowercase() {
        return toByteString().toAsciiLowercase();
    }

    @Override // okio.ByteString
    public ByteString digest$jvm(String str) {
        Intrinsics.checkParameterIsNotNull(str, "algorithm");
        MessageDigest instance = MessageDigest.getInstance(str);
        int length = getSegments().length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = getDirectory()[length + i];
            int i4 = getDirectory()[i];
            instance.update(getSegments()[i], i3, i4 - i2);
            i++;
            i2 = i4;
        }
        byte[] digest = instance.digest();
        Intrinsics.checkExpressionValueIsNotNull(digest, "digest.digest()");
        return new ByteString(digest);
    }

    @Override // okio.ByteString
    public byte internalGet$jvm(int i) {
        int i2;
        Util.checkOffsetAndCount((long) this.directory[this.segments.length - 1], (long) i, 1);
        int segment = segment(i);
        if (segment == 0) {
            i2 = 0;
        } else {
            i2 = this.directory[segment - 1];
        }
        int[] iArr = this.directory;
        byte[][] bArr = this.segments;
        return bArr[segment][(i - i2) + iArr[bArr.length + segment]];
    }

    public final int segment(int i) {
        int binarySearch = Arrays.binarySearch(this.directory, 0, this.segments.length, i + 1);
        return binarySearch >= 0 ? binarySearch : binarySearch ^ -1;
    }

    @Override // okio.ByteString
    public int getSize$jvm() {
        return this.directory[this.segments.length - 1];
    }

    @Override // okio.ByteString
    public byte[] toByteArray() {
        byte[] bArr = new byte[size()];
        int length = getSegments().length;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i < length) {
            int i4 = getDirectory()[length + i];
            int i5 = getDirectory()[i];
            int i6 = i5 - i2;
            Platform.arraycopy(getSegments()[i], i4, bArr, i3, i6);
            i3 += i6;
            i++;
            i2 = i5;
        }
        return bArr;
    }

    @Override // okio.ByteString
    public boolean rangeEquals(int i, ByteString byteString, int i2, int i3) {
        int i4;
        Intrinsics.checkParameterIsNotNull(byteString, "other");
        if (i < 0 || i > size() - i3) {
            return false;
        }
        int i5 = i3 + i;
        int segment = segment(i);
        while (i < i5) {
            if (segment == 0) {
                i4 = 0;
            } else {
                i4 = getDirectory()[segment - 1];
            }
            int i6 = getDirectory()[getSegments().length + segment];
            int min = Math.min(i5, (getDirectory()[segment] - i4) + i4) - i;
            if (!byteString.rangeEquals(i2, getSegments()[segment], i6 + (i - i4), min)) {
                return false;
            }
            i2 += min;
            i += min;
            segment++;
        }
        return true;
    }

    @Override // okio.ByteString
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        int i4;
        Intrinsics.checkParameterIsNotNull(bArr, "other");
        if (i < 0 || i > size() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int i5 = i3 + i;
        int segment = segment(i);
        while (i < i5) {
            if (segment == 0) {
                i4 = 0;
            } else {
                i4 = getDirectory()[segment - 1];
            }
            int i6 = getDirectory()[getSegments().length + segment];
            int min = Math.min(i5, (getDirectory()[segment] - i4) + i4) - i;
            if (!Util.arrayRangeEquals(getSegments()[segment], i6 + (i - i4), bArr, i2, min)) {
                return false;
            }
            i2 += min;
            i += min;
            segment++;
        }
        return true;
    }

    private final ByteString toByteString() {
        return new ByteString(toByteArray());
    }

    @Override // okio.ByteString
    public byte[] internalArray$jvm() {
        return toByteArray();
    }

    @Override // okio.ByteString
    public void write$jvm(Buffer buffer) {
        Intrinsics.checkParameterIsNotNull(buffer, "buffer");
        int length = getSegments().length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = getDirectory()[length + i];
            int i4 = getDirectory()[i];
            Segment segment = new Segment(getSegments()[i], i3, i3 + (i4 - i2), true, false);
            Segment segment2 = buffer.head;
            if (segment2 == null) {
                segment.prev = segment;
                segment.next = segment;
                buffer.head = segment;
            } else {
                if (segment2 == null) {
                    Intrinsics.throwNpe();
                }
                Segment segment3 = segment2.prev;
                if (segment3 == null) {
                    Intrinsics.throwNpe();
                }
                segment3.push(segment);
            }
            i++;
            i2 = i4;
        }
        buffer.setSize$jvm(buffer.size() + ((long) size()));
    }

    @Override // okio.ByteString, java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ByteString) {
            ByteString byteString = (ByteString) obj;
            if (byteString.size() == size() && rangeEquals(0, byteString, 0, size())) {
                return true;
            }
        }
        return false;
    }

    @Override // okio.ByteString, java.lang.Object
    public int hashCode() {
        int hashCode$jvm = getHashCode$jvm();
        if (hashCode$jvm != 0) {
            return hashCode$jvm;
        }
        int length = getSegments().length;
        int i = 0;
        int i2 = 0;
        int i3 = 1;
        while (i < length) {
            int i4 = getDirectory()[length + i];
            int i5 = getDirectory()[i];
            byte[] bArr = getSegments()[i];
            int i6 = (i5 - i2) + i4;
            while (i4 < i6) {
                i3 = (i3 * 31) + bArr[i4];
                i4++;
            }
            i++;
            i2 = i5;
        }
        setHashCode$jvm(i3);
        return i3;
    }

    @Override // okio.ByteString, java.lang.Object
    public String toString() {
        return toByteString().toString();
    }
}
