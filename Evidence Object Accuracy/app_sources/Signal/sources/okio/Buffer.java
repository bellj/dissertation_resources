package okio;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.text.Charsets;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.PushDatabase;

/* compiled from: Buffer.kt */
@Metadata(bv = {}, d1 = {"\u0000¦\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0005\n\u0002\b\u0005\n\u0002\u0010\n\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0011\u0018\u0000 \u00012\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0004\u0001\u0001B\t¢\u0006\u0006\b\u0001\u0010\u0001J0\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u0007H\u0002J\b\u0010\u000f\u001a\u00020\u0000H\u0016J\b\u0010\u0011\u001a\u00020\u0010H\u0016J\b\u0010\u0012\u001a\u00020\u0000H\u0016J\b\u0010\u0013\u001a\u00020\u0000H\u0016J\b\u0010\u0014\u001a\u00020\rH\u0016J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\u0010\u0010\u0019\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\b\u0010\u001a\u001a\u00020\u0001H\u0016J\b\u0010\u001c\u001a\u00020\u001bH\u0016J\"\u0010\u001f\u001a\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u001e\u001a\u00020\u00152\b\b\u0002\u0010\u0016\u001a\u00020\u0015J\u001a\u0010 \u001a\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u00102\b\b\u0002\u0010\u0016\u001a\u00020\u0015H\u0007J\u0006\u0010!\u001a\u00020\u0015J\b\u0010#\u001a\u00020\"H\u0016J\u0018\u0010'\u001a\u00020\"2\u0006\u0010$\u001a\u00020\u0015H\u0002¢\u0006\u0004\b%\u0010&J\b\u0010)\u001a\u00020(H\u0016J\b\u0010*\u001a\u00020\u0007H\u0016J\b\u0010+\u001a\u00020\u0015H\u0016J\b\u0010,\u001a\u00020(H\u0016J\b\u0010-\u001a\u00020\u0007H\u0016J\b\u0010.\u001a\u00020\u0015H\u0016J\b\u0010/\u001a\u00020\u0015H\u0016J\b\u00101\u001a\u000200H\u0016J\u0010\u00101\u001a\u0002002\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\u0010\u00104\u001a\u00020\u00072\u0006\u00103\u001a\u000202H\u0016J!\u00108\u001a\u00020\u00072\u0006\u00103\u001a\u0002022\b\b\u0002\u00105\u001a\u00020\rH\u0000¢\u0006\u0004\b6\u00107J\u0018\u0010:\u001a\u00020\u00172\u0006\u00109\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\u0010\u0010<\u001a\u00020\u00152\u0006\u00109\u001a\u00020;H\u0016J\b\u0010>\u001a\u00020=H\u0016J\u0010\u0010>\u001a\u00020=2\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\u0010\u0010A\u001a\u00020=2\u0006\u0010@\u001a\u00020?H\u0016J\u0018\u0010A\u001a\u00020=2\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010@\u001a\u00020?H\u0016J\b\u0010B\u001a\u00020=H\u0016J\u0010\u0010B\u001a\u00020=2\u0006\u0010C\u001a\u00020\u0015H\u0016J\u0017\u0010G\u001a\u00020=2\u0006\u0010D\u001a\u00020\u0015H\u0000¢\u0006\u0004\bE\u0010FJ\b\u0010H\u001a\u00020\tH\u0016J\u0010\u0010H\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\u0010\u0010:\u001a\u00020\u00172\u0006\u00109\u001a\u00020\tH\u0016J \u0010I\u001a\u00020\u00072\u0006\u00109\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00020\u0007H\u0016J\u0010\u0010I\u001a\u00020\u00072\u0006\u00109\u001a\u00020JH\u0016J\u0006\u0010K\u001a\u00020\u0017J\u0010\u0010L\u001a\u00020\u00172\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\u0010\u0010N\u001a\u00020\u00002\u0006\u0010M\u001a\u000200H\u0016J\u0010\u0010P\u001a\u00020\u00002\u0006\u0010O\u001a\u00020=H\u0016J \u0010P\u001a\u00020\u00002\u0006\u0010O\u001a\u00020=2\u0006\u0010Q\u001a\u00020\u00072\u0006\u0010R\u001a\u00020\u0007H\u0016J\u0010\u0010T\u001a\u00020\u00002\u0006\u0010S\u001a\u00020\u0007H\u0016J\u0018\u0010U\u001a\u00020\u00002\u0006\u0010O\u001a\u00020=2\u0006\u0010@\u001a\u00020?H\u0016J(\u0010U\u001a\u00020\u00002\u0006\u0010O\u001a\u00020=2\u0006\u0010Q\u001a\u00020\u00072\u0006\u0010R\u001a\u00020\u00072\u0006\u0010@\u001a\u00020?H\u0016J\u0010\u0010N\u001a\u00020\u00002\u0006\u0010V\u001a\u00020\tH\u0016J \u0010N\u001a\u00020\u00002\u0006\u0010V\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00020\u0007H\u0016J\u0010\u0010N\u001a\u00020\u00072\u0006\u0010V\u001a\u00020JH\u0016J\u0010\u0010X\u001a\u00020\u00152\u0006\u0010V\u001a\u00020WH\u0016J\u0010\u0010Z\u001a\u00020\u00002\u0006\u0010Y\u001a\u00020\u0007H\u0016J\u0010\u0010\\\u001a\u00020\u00002\u0006\u0010[\u001a\u00020\u0007H\u0016J\u0010\u0010^\u001a\u00020\u00002\u0006\u0010]\u001a\u00020\u0007H\u0016J\u0010\u0010`\u001a\u00020\u00002\u0006\u0010_\u001a\u00020\u0015H\u0016J\u0010\u0010a\u001a\u00020\u00002\u0006\u0010_\u001a\u00020\u0015H\u0016J\u0010\u0010b\u001a\u00020\u00002\u0006\u0010_\u001a\u00020\u0015H\u0016J\u0017\u0010f\u001a\u00020\u00052\u0006\u0010c\u001a\u00020\u0007H\u0000¢\u0006\u0004\bd\u0010eJ\u0018\u0010N\u001a\u00020\u00172\u0006\u0010V\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J\u0018\u0010I\u001a\u00020\u00152\u0006\u00109\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0015H\u0016J \u0010i\u001a\u00020\u00152\u0006\u0010Y\u001a\u00020\"2\u0006\u0010g\u001a\u00020\u00152\u0006\u0010h\u001a\u00020\u0015H\u0016J\u0010\u0010i\u001a\u00020\u00152\u0006\u0010\n\u001a\u000200H\u0016J\u0018\u0010i\u001a\u00020\u00152\u0006\u0010\n\u001a\u0002002\u0006\u0010g\u001a\u00020\u0015H\u0016J\u0010\u0010k\u001a\u00020\u00152\u0006\u0010j\u001a\u000200H\u0016J\u0018\u0010k\u001a\u00020\u00152\u0006\u0010j\u001a\u0002002\u0006\u0010g\u001a\u00020\u0015H\u0016J\u0018\u0010\u000e\u001a\u00020\r2\u0006\u0010\u001e\u001a\u00020\u00152\u0006\u0010\n\u001a\u000200H\u0016J(\u0010\u000e\u001a\u00020\r2\u0006\u0010\u001e\u001a\u00020\u00152\u0006\u0010\n\u001a\u0002002\u0006\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00020\u0007H\u0016J\b\u0010l\u001a\u00020\u0017H\u0016J\b\u0010m\u001a\u00020\rH\u0016J\b\u0010n\u001a\u00020\u0017H\u0016J\b\u0010p\u001a\u00020oH\u0016J\u0013\u0010s\u001a\u00020\r2\b\u0010r\u001a\u0004\u0018\u00010qH\u0002J\b\u0010t\u001a\u00020\u0007H\u0016J\b\u0010u\u001a\u00020=H\u0016J\b\u0010v\u001a\u00020\u0000H\u0016J\u0006\u0010w\u001a\u000200J\u000e\u0010w\u001a\u0002002\u0006\u0010\u0016\u001a\u00020\u0007J\u0012\u0010z\u001a\u00020x2\b\b\u0002\u0010y\u001a\u00020xH\u0007R\u0018\u0010{\u001a\u0004\u0018\u00010\u00058\u0000@\u0000X\u000e¢\u0006\u0006\n\u0004\b{\u0010|R-\u0010~\u001a\u00020\u00152\u0006\u0010}\u001a\u00020\u00158\u0007@@X\u000e¢\u0006\u0015\n\u0004\b~\u0010\u001a\u0005\b~\u0010\u0001\"\u0006\b\u0001\u0010\u0001R\u0016\u0010\u000f\u001a\u00020\u00008VX\u0004¢\u0006\b\u001a\u0006\b\u0001\u0010\u0001¨\u0006\u0001"}, d2 = {"Lokio/Buffer;", "Lokio/BufferedSource;", "Lokio/BufferedSink;", "", "Ljava/nio/channels/ByteChannel;", "Lokio/Segment;", "segment", "", "segmentPos", "", "bytes", "bytesOffset", "bytesLimit", "", "rangeEquals", "buffer", "Ljava/io/OutputStream;", "outputStream", "emitCompleteSegments", "emit", "exhausted", "", "byteCount", "", "require", "request", "peek", "Ljava/io/InputStream;", "inputStream", "out", "offset", "copyTo", "writeTo", "completeSegmentByteCount", "", "readByte", "pos", "getByte", "(J)B", "get", "", "readShort", "readInt", "readLong", "readShortLe", "readIntLe", "readDecimalLong", "readHexadecimalUnsignedLong", "Lokio/ByteString;", "readByteString", "Lokio/Options;", "options", "select", "selectTruncated", "selectPrefix$jvm", "(Lokio/Options;Z)I", "selectPrefix", "sink", "readFully", "Lokio/Sink;", "readAll", "", "readUtf8", "Ljava/nio/charset/Charset;", "charset", "readString", "readUtf8LineStrict", "limit", "newline", "readUtf8Line$jvm", "(J)Ljava/lang/String;", "readUtf8Line", "readByteArray", "read", "Ljava/nio/ByteBuffer;", "clear", "skip", "byteString", "write", "string", "writeUtf8", "beginIndex", "endIndex", "codePoint", "writeUtf8CodePoint", "writeString", PushDatabase.SOURCE_E164, "Lokio/Source;", "writeAll", "b", "writeByte", "s", "writeShort", "i", "writeInt", "v", "writeLong", "writeDecimalLong", "writeHexadecimalUnsignedLong", "minimumCapacity", "writableSegment$jvm", "(I)Lokio/Segment;", "writableSegment", "fromIndex", "toIndex", "indexOf", "targetBytes", "indexOfElement", "flush", "isOpen", "close", "Lokio/Timeout;", "timeout", "", "other", "equals", "hashCode", "toString", "clone", "snapshot", "Lokio/Buffer$UnsafeCursor;", "unsafeCursor", "readAndWriteUnsafe", "head", "Lokio/Segment;", "<set-?>", MediaPreviewActivity.SIZE_EXTRA, "J", "()J", "setSize$jvm", "(J)V", "getBuffer", "()Lokio/Buffer;", "<init>", "()V", "Companion", "UnsafeCursor", "jvm"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class Buffer implements BufferedSource, BufferedSink, Cloneable, ByteChannel {
    public static final Companion Companion = new Companion(null);
    private static final byte[] DIGITS;
    public Segment head;
    private long size;

    @Override // okio.BufferedSource, okio.BufferedSink
    public Buffer buffer() {
        return this;
    }

    @Override // okio.Source, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // okio.BufferedSink
    public Buffer emit() {
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer emitCompleteSegments() {
        return this;
    }

    @Override // okio.BufferedSink, okio.Sink, java.io.Flushable
    public void flush() {
    }

    @Override // okio.BufferedSource, okio.BufferedSink
    public Buffer getBuffer() {
        return this;
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return true;
    }

    public final void setSize$jvm(long j) {
        this.size = j;
    }

    public final long size() {
        return this.size;
    }

    @Override // okio.BufferedSink
    public OutputStream outputStream() {
        return new OutputStream(this) { // from class: okio.Buffer$outputStream$1
            final /* synthetic */ Buffer this$0;

            @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
            public void close() {
            }

            @Override // java.io.OutputStream, java.io.Flushable
            public void flush() {
            }

            /* JADX WARN: Incorrect args count in method signature: ()V */
            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // java.io.OutputStream
            public void write(int i) {
                this.this$0.writeByte(i);
            }

            @Override // java.io.OutputStream
            public void write(byte[] bArr, int i, int i2) {
                Intrinsics.checkParameterIsNotNull(bArr, "data");
                this.this$0.write(bArr, i, i2);
            }

            @Override // java.lang.Object
            public String toString() {
                return this.this$0 + ".outputStream()";
            }
        };
    }

    @Override // okio.BufferedSource
    public boolean exhausted() {
        return this.size == 0;
    }

    @Override // okio.BufferedSource
    public void require(long j) throws EOFException {
        if (this.size < j) {
            throw new EOFException();
        }
    }

    @Override // okio.BufferedSource
    public boolean request(long j) {
        return this.size >= j;
    }

    @Override // okio.BufferedSource
    public BufferedSource peek() {
        return Okio.buffer(new PeekSource(this));
    }

    @Override // okio.BufferedSource
    public InputStream inputStream() {
        return new InputStream(this) { // from class: okio.Buffer$inputStream$1
            final /* synthetic */ Buffer this$0;

            @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
            public void close() {
            }

            /* JADX WARN: Incorrect args count in method signature: ()V */
            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // java.io.InputStream
            public int read() {
                if (this.this$0.size() > 0) {
                    return this.this$0.readByte() & 255;
                }
                return -1;
            }

            @Override // java.io.InputStream
            public int read(byte[] bArr, int i, int i2) {
                Intrinsics.checkParameterIsNotNull(bArr, "sink");
                return this.this$0.read(bArr, i, i2);
            }

            @Override // java.io.InputStream
            public int available() {
                return (int) Math.min(this.this$0.size(), (long) Integer.MAX_VALUE);
            }

            @Override // java.lang.Object
            public String toString() {
                return this.this$0 + ".inputStream()";
            }
        };
    }

    public final Buffer copyTo(Buffer buffer, long j, long j2) {
        Intrinsics.checkParameterIsNotNull(buffer, "out");
        Util.checkOffsetAndCount(this.size, j, j2);
        if (j2 == 0) {
            return this;
        }
        buffer.size += j2;
        Segment segment = this.head;
        while (true) {
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            int i = segment.limit;
            int i2 = segment.pos;
            if (j >= ((long) (i - i2))) {
                j -= (long) (i - i2);
                segment = segment.next;
            }
        }
        while (j2 > 0) {
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            Segment sharedCopy = segment.sharedCopy();
            int i3 = sharedCopy.pos + ((int) j);
            sharedCopy.pos = i3;
            sharedCopy.limit = Math.min(i3 + ((int) j2), sharedCopy.limit);
            Segment segment2 = buffer.head;
            if (segment2 == null) {
                sharedCopy.prev = sharedCopy;
                sharedCopy.next = sharedCopy;
                buffer.head = sharedCopy;
            } else {
                if (segment2 == null) {
                    Intrinsics.throwNpe();
                }
                Segment segment3 = segment2.prev;
                if (segment3 == null) {
                    Intrinsics.throwNpe();
                }
                segment3.push(sharedCopy);
            }
            j2 -= (long) (sharedCopy.limit - sharedCopy.pos);
            segment = segment.next;
            j = 0;
        }
        return this;
    }

    public final Buffer writeTo(OutputStream outputStream, long j) throws IOException {
        Intrinsics.checkParameterIsNotNull(outputStream, "out");
        Util.checkOffsetAndCount(this.size, 0, j);
        Segment segment = this.head;
        while (j > 0) {
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            int min = (int) Math.min(j, (long) (segment.limit - segment.pos));
            outputStream.write(segment.data, segment.pos, min);
            int i = segment.pos + min;
            segment.pos = i;
            long j2 = (long) min;
            this.size -= j2;
            j -= j2;
            if (i == segment.limit) {
                Segment pop = segment.pop();
                this.head = pop;
                SegmentPool.recycle(segment);
                segment = pop;
            }
        }
        return this;
    }

    public final long completeSegmentByteCount() {
        long j = this.size;
        if (j == 0) {
            return 0;
        }
        Segment segment = this.head;
        if (segment == null) {
            Intrinsics.throwNpe();
        }
        Segment segment2 = segment.prev;
        if (segment2 == null) {
            Intrinsics.throwNpe();
        }
        int i = segment2.limit;
        return (i >= 8192 || !segment2.owner) ? j : j - ((long) (i - segment2.pos));
    }

    @Override // okio.BufferedSource
    public byte readByte() throws EOFException {
        if (this.size != 0) {
            Segment segment = this.head;
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            int i = segment.pos;
            int i2 = segment.limit;
            int i3 = i + 1;
            byte b = segment.data[i];
            this.size--;
            if (i3 == i2) {
                this.head = segment.pop();
                SegmentPool.recycle(segment);
            } else {
                segment.pos = i3;
            }
            return b;
        }
        throw new EOFException();
    }

    public final byte getByte(long j) {
        Util.checkOffsetAndCount(this.size, j, 1);
        Segment segment = this.head;
        if (segment == null) {
            Intrinsics.throwNpe();
            throw null;
        } else if (size() - j < j) {
            long size = size();
            while (size > j) {
                segment = segment.prev;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                size -= (long) (segment.limit - segment.pos);
            }
            return segment.data[(int) ((((long) segment.pos) + j) - size)];
        } else {
            long j2 = 0;
            while (true) {
                int i = segment.limit;
                int i2 = segment.pos;
                long j3 = ((long) (i - i2)) + j2;
                if (j3 > j) {
                    return segment.data[(int) ((((long) i2) + j) - j2)];
                }
                segment = segment.next;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                j2 = j3;
            }
        }
    }

    @Override // okio.BufferedSource
    public short readShort() throws EOFException {
        if (this.size >= 2) {
            Segment segment = this.head;
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            int i = segment.pos;
            int i2 = segment.limit;
            if (i2 - i < 2) {
                return (short) (((readByte() & 255) << 8) | (readByte() & 255));
            }
            byte[] bArr = segment.data;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
            this.size -= 2;
            if (i4 == i2) {
                this.head = segment.pop();
                SegmentPool.recycle(segment);
            } else {
                segment.pos = i4;
            }
            return (short) i5;
        }
        throw new EOFException();
    }

    @Override // okio.BufferedSource
    public int readInt() throws EOFException {
        if (this.size >= 4) {
            Segment segment = this.head;
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            int i = segment.pos;
            int i2 = segment.limit;
            if (((long) (i2 - i)) < 4) {
                return ((readByte() & 255) << 24) | ((readByte() & 255) << 16) | ((readByte() & 255) << 8) | (readByte() & 255);
            }
            byte[] bArr = segment.data;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
            int i6 = i4 + 1;
            int i7 = i5 | ((bArr[i4] & 255) << 8);
            int i8 = i6 + 1;
            int i9 = i7 | (bArr[i6] & 255);
            this.size -= 4;
            if (i8 == i2) {
                this.head = segment.pop();
                SegmentPool.recycle(segment);
            } else {
                segment.pos = i8;
            }
            return i9;
        }
        throw new EOFException();
    }

    @Override // okio.BufferedSource
    public long readLong() throws EOFException {
        if (this.size >= 8) {
            Segment segment = this.head;
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            int i = segment.pos;
            int i2 = segment.limit;
            if (((long) (i2 - i)) < 8) {
                return ((((long) readInt()) & 4294967295L) << 32) | (4294967295L & ((long) readInt()));
            }
            byte[] bArr = segment.data;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = i4 + 1;
            long j = ((((long) bArr[i]) & 255) << 56) | ((((long) bArr[i3]) & 255) << 48) | ((((long) bArr[i4]) & 255) << 40);
            int i6 = i5 + 1;
            long j2 = ((((long) bArr[i5]) & 255) << 32) | j;
            int i7 = i6 + 1;
            int i8 = i7 + 1;
            long j3 = j2 | ((((long) bArr[i6]) & 255) << 24) | ((((long) bArr[i7]) & 255) << 16);
            int i9 = i8 + 1;
            int i10 = i9 + 1;
            long j4 = j3 | ((((long) bArr[i8]) & 255) << 8) | (((long) bArr[i9]) & 255);
            this.size -= 8;
            if (i10 == i2) {
                this.head = segment.pop();
                SegmentPool.recycle(segment);
            } else {
                segment.pos = i10;
            }
            return j4;
        }
        throw new EOFException();
    }

    public short readShortLe() throws EOFException {
        return Util.reverseBytes(readShort());
    }

    public int readIntLe() throws EOFException {
        return Util.reverseBytes(readInt());
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b5 A[EDGE_INSN: B:48:0x00b5->B:41:0x00b5 ?: BREAK  , SYNTHETIC] */
    @Override // okio.BufferedSource
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long readDecimalLong() throws java.io.EOFException {
        /*
            r17 = this;
            r0 = r17
            long r1 = r0.size
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00c0
            r5 = -7
            r7 = 0
            r8 = 0
            r9 = 0
        L_0x000f:
            okio.Segment r10 = r0.head
            if (r10 != 0) goto L_0x0016
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0016:
            byte[] r11 = r10.data
            int r12 = r10.pos
            int r13 = r10.limit
        L_0x001c:
            if (r12 >= r13) goto L_0x00a1
            byte r15 = r11[r12]
            r14 = 48
            byte r14 = (byte) r14
            if (r15 < r14) goto L_0x0072
            r1 = 57
            byte r1 = (byte) r1
            if (r15 > r1) goto L_0x0072
            int r14 = r14 - r15
            r1 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r16 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r16 < 0) goto L_0x0045
            int r16 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r16 != 0) goto L_0x003e
            long r1 = (long) r14
            int r16 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r16 >= 0) goto L_0x003e
            goto L_0x0045
        L_0x003e:
            r1 = 10
            long r3 = r3 * r1
            long r1 = (long) r14
            long r3 = r3 + r1
            goto L_0x007d
        L_0x0045:
            okio.Buffer r1 = new okio.Buffer
            r1.<init>()
            okio.Buffer r1 = r1.writeDecimalLong(r3)
            okio.Buffer r1 = r1.writeByte(r15)
            if (r8 != 0) goto L_0x0057
            r1.readByte()
        L_0x0057:
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Number too large: "
            r3.append(r4)
            java.lang.String r1 = r1.readUtf8()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x0072:
            r1 = 45
            byte r1 = (byte) r1
            if (r15 != r1) goto L_0x0082
            if (r7 != 0) goto L_0x0082
            r1 = 1
            long r5 = r5 - r1
            r8 = 1
        L_0x007d:
            int r12 = r12 + 1
            int r7 = r7 + 1
            goto L_0x001c
        L_0x0082:
            if (r7 == 0) goto L_0x0086
            r9 = 1
            goto L_0x00a1
        L_0x0086:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Expected leading [0-9] or '-' character but was 0x"
            r2.append(r3)
            java.lang.String r3 = java.lang.Integer.toHexString(r15)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x00a1:
            if (r12 != r13) goto L_0x00ad
            okio.Segment r1 = r10.pop()
            r0.head = r1
            okio.SegmentPool.recycle(r10)
            goto L_0x00af
        L_0x00ad:
            r10.pos = r12
        L_0x00af:
            if (r9 != 0) goto L_0x00b5
            okio.Segment r1 = r0.head
            if (r1 != 0) goto L_0x000f
        L_0x00b5:
            long r1 = r0.size
            long r5 = (long) r7
            long r1 = r1 - r5
            r0.size = r1
            if (r8 == 0) goto L_0x00be
            goto L_0x00bf
        L_0x00be:
            long r3 = -r3
        L_0x00bf:
            return r3
        L_0x00c0:
            java.io.EOFException r1 = new java.io.EOFException
            r1.<init>()
            goto L_0x00c7
        L_0x00c6:
            throw r1
        L_0x00c7:
            goto L_0x00c6
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.readDecimalLong():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ae A[EDGE_INSN: B:44:0x00ae->B:39:0x00ae ?: BREAK  , SYNTHETIC] */
    @Override // okio.BufferedSource
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long readHexadecimalUnsignedLong() throws java.io.EOFException {
        /*
            r15 = this;
            long r0 = r15.size
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x00b5
            r0 = 0
            r4 = r2
            r1 = 0
        L_0x000b:
            okio.Segment r6 = r15.head
            if (r6 != 0) goto L_0x0012
            kotlin.jvm.internal.Intrinsics.throwNpe()
        L_0x0012:
            byte[] r7 = r6.data
            int r8 = r6.pos
            int r9 = r6.limit
        L_0x0018:
            if (r8 >= r9) goto L_0x009a
            byte r10 = r7[r8]
            r11 = 48
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0029
            r12 = 57
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0029
            int r11 = r10 - r11
            goto L_0x0043
        L_0x0029:
            r11 = 97
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0038
            r12 = 102(0x66, float:1.43E-43)
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0038
        L_0x0033:
            int r11 = r10 - r11
            int r11 = r11 + 10
            goto L_0x0043
        L_0x0038:
            r11 = 65
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x007b
            r12 = 70
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x007b
            goto L_0x0033
        L_0x0043:
            r12 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r12 = r12 & r4
            int r14 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r14 != 0) goto L_0x0053
            r10 = 4
            long r4 = r4 << r10
            long r10 = (long) r11
            long r4 = r4 | r10
            int r8 = r8 + 1
            int r0 = r0 + 1
            goto L_0x0018
        L_0x0053:
            okio.Buffer r0 = new okio.Buffer
            r0.<init>()
            okio.Buffer r0 = r0.writeHexadecimalUnsignedLong(r4)
            okio.Buffer r0 = r0.writeByte(r10)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Number too large: "
            r2.append(r3)
            java.lang.String r0 = r0.readUtf8()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x007b:
            if (r0 == 0) goto L_0x007f
            r1 = 1
            goto L_0x009a
        L_0x007f:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Expected leading [0-9a-fA-F] character but was 0x"
            r1.append(r2)
            java.lang.String r2 = java.lang.Integer.toHexString(r10)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x009a:
            if (r8 != r9) goto L_0x00a6
            okio.Segment r7 = r6.pop()
            r15.head = r7
            okio.SegmentPool.recycle(r6)
            goto L_0x00a8
        L_0x00a6:
            r6.pos = r8
        L_0x00a8:
            if (r1 != 0) goto L_0x00ae
            okio.Segment r6 = r15.head
            if (r6 != 0) goto L_0x000b
        L_0x00ae:
            long r1 = r15.size
            long r6 = (long) r0
            long r1 = r1 - r6
            r15.size = r1
            return r4
        L_0x00b5:
            java.io.EOFException r0 = new java.io.EOFException
            r0.<init>()
            goto L_0x00bc
        L_0x00bb:
            throw r0
        L_0x00bc:
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.readHexadecimalUnsignedLong():long");
    }

    public ByteString readByteString() {
        return new ByteString(readByteArray());
    }

    @Override // okio.BufferedSource
    public ByteString readByteString(long j) throws EOFException {
        return new ByteString(readByteArray(j));
    }

    @Override // okio.BufferedSource
    public int select(Options options) {
        Intrinsics.checkParameterIsNotNull(options, "options");
        int selectPrefix$jvm$default = selectPrefix$jvm$default(this, options, false, 2, null);
        if (selectPrefix$jvm$default == -1) {
            return -1;
        }
        skip((long) options.getByteStrings$jvm()[selectPrefix$jvm$default].size());
        return selectPrefix$jvm$default;
    }

    public static /* bridge */ /* synthetic */ int selectPrefix$jvm$default(Buffer buffer, Options options, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return buffer.selectPrefix$jvm(options, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        if (r19 == false) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005e, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005f, code lost:
        return r11;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int selectPrefix$jvm(okio.Options r18, boolean r19) {
        /*
        // Method dump skipped, instructions count: 180
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.selectPrefix$jvm(okio.Options, boolean):int");
    }

    @Override // okio.BufferedSource
    public void readFully(Buffer buffer, long j) throws EOFException {
        Intrinsics.checkParameterIsNotNull(buffer, "sink");
        long j2 = this.size;
        if (j2 >= j) {
            buffer.write(this, j);
        } else {
            buffer.write(this, j2);
            throw new EOFException();
        }
    }

    @Override // okio.BufferedSource
    public long readAll(Sink sink) throws IOException {
        Intrinsics.checkParameterIsNotNull(sink, "sink");
        long j = this.size;
        if (j > 0) {
            sink.write(this, j);
        }
        return j;
    }

    public String readUtf8() {
        return readString(this.size, Charsets.UTF_8);
    }

    public String readUtf8(long j) throws EOFException {
        return readString(j, Charsets.UTF_8);
    }

    @Override // okio.BufferedSource
    public String readString(Charset charset) {
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        return readString(this.size, charset);
    }

    public String readString(long j, Charset charset) throws EOFException {
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (this.size < j) {
            throw new EOFException();
        } else if (j == 0) {
            return "";
        } else {
            Segment segment = this.head;
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            int i = segment.pos;
            if (((long) i) + j > ((long) segment.limit)) {
                return new String(readByteArray(j), charset);
            }
            int i2 = (int) j;
            String str = new String(segment.data, i, i2, charset);
            int i3 = segment.pos + i2;
            segment.pos = i3;
            this.size -= j;
            if (i3 == segment.limit) {
                this.head = segment.pop();
                SegmentPool.recycle(segment);
            }
            return str;
        }
    }

    @Override // okio.BufferedSource
    public String readUtf8LineStrict() throws EOFException {
        return readUtf8LineStrict(Long.MAX_VALUE);
    }

    @Override // okio.BufferedSource
    public String readUtf8LineStrict(long j) throws EOFException {
        if (j >= 0) {
            long j2 = Long.MAX_VALUE;
            if (j != Long.MAX_VALUE) {
                j2 = j + 1;
            }
            byte b = (byte) 10;
            long indexOf = indexOf(b, 0, j2);
            if (indexOf != -1) {
                return readUtf8Line$jvm(indexOf);
            }
            if (j2 < this.size && getByte(j2 - 1) == ((byte) 13) && getByte(j2) == b) {
                return readUtf8Line$jvm(j2);
            }
            Buffer buffer = new Buffer();
            copyTo(buffer, 0, Math.min((long) 32, this.size));
            throw new EOFException("\\n not found: limit=" + Math.min(this.size, j) + " content=" + buffer.readByteString().hex() + (char) 8230);
        }
        throw new IllegalArgumentException(("limit < 0: " + j).toString());
    }

    public final String readUtf8Line$jvm(long j) throws EOFException {
        if (j > 0) {
            long j2 = j - 1;
            if (getByte(j2) == ((byte) 13)) {
                String readUtf8 = readUtf8(j2);
                skip(2);
                return readUtf8;
            }
        }
        String readUtf82 = readUtf8(j);
        skip(1);
        return readUtf82;
    }

    @Override // okio.BufferedSource
    public byte[] readByteArray() {
        return readByteArray(this.size);
    }

    @Override // okio.BufferedSource
    public byte[] readByteArray(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j).toString());
        } else if (this.size >= j) {
            byte[] bArr = new byte[(int) j];
            readFully(bArr);
            return bArr;
        } else {
            throw new EOFException();
        }
    }

    @Override // okio.BufferedSource
    public void readFully(byte[] bArr) throws EOFException {
        Intrinsics.checkParameterIsNotNull(bArr, "sink");
        int i = 0;
        while (i < bArr.length) {
            int read = read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                throw new EOFException();
            }
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        Intrinsics.checkParameterIsNotNull(bArr, "sink");
        Util.checkOffsetAndCount((long) bArr.length, (long) i, (long) i2);
        Segment segment = this.head;
        if (segment == null) {
            return -1;
        }
        int min = Math.min(i2, segment.limit - segment.pos);
        System.arraycopy(segment.data, segment.pos, bArr, i, min);
        int i3 = segment.pos + min;
        segment.pos = i3;
        this.size -= (long) min;
        if (i3 == segment.limit) {
            this.head = segment.pop();
            SegmentPool.recycle(segment);
        }
        return min;
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) throws IOException {
        Intrinsics.checkParameterIsNotNull(byteBuffer, "sink");
        Segment segment = this.head;
        if (segment == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), segment.limit - segment.pos);
        byteBuffer.put(segment.data, segment.pos, min);
        int i = segment.pos + min;
        segment.pos = i;
        this.size -= (long) min;
        if (i == segment.limit) {
            this.head = segment.pop();
            SegmentPool.recycle(segment);
        }
        return min;
    }

    public final void clear() {
        skip(this.size);
    }

    @Override // okio.BufferedSource
    public void skip(long j) throws EOFException {
        while (j > 0) {
            Segment segment = this.head;
            if (segment != null) {
                int min = (int) Math.min(j, (long) (segment.limit - segment.pos));
                long j2 = (long) min;
                this.size -= j2;
                j -= j2;
                int i = segment.pos + min;
                segment.pos = i;
                if (i == segment.limit) {
                    this.head = segment.pop();
                    SegmentPool.recycle(segment);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    @Override // okio.BufferedSink
    public Buffer write(ByteString byteString) {
        Intrinsics.checkParameterIsNotNull(byteString, "byteString");
        byteString.write$jvm(this);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeUtf8(String str) {
        Intrinsics.checkParameterIsNotNull(str, "string");
        return writeUtf8(str, 0, str.length());
    }

    @Override // okio.BufferedSink
    public Buffer writeUtf8(String str, int i, int i2) {
        char c;
        Intrinsics.checkParameterIsNotNull(str, "string");
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 <= str.length()) {
                    while (i < i2) {
                        char charAt = str.charAt(i);
                        if (charAt < 128) {
                            Segment writableSegment$jvm = writableSegment$jvm(1);
                            byte[] bArr = writableSegment$jvm.data;
                            int i3 = writableSegment$jvm.limit - i;
                            int min = Math.min(i2, 8192 - i3);
                            int i4 = i + 1;
                            bArr[i + i3] = (byte) charAt;
                            while (i4 < min) {
                                char charAt2 = str.charAt(i4);
                                if (charAt2 >= 128) {
                                    break;
                                }
                                bArr[i4 + i3] = (byte) charAt2;
                                i4++;
                            }
                            int i5 = writableSegment$jvm.limit;
                            int i6 = (i3 + i4) - i5;
                            writableSegment$jvm.limit = i5 + i6;
                            this.size += (long) i6;
                            i = i4;
                        } else {
                            if (charAt < 2048) {
                                Segment writableSegment$jvm2 = writableSegment$jvm(2);
                                byte[] bArr2 = writableSegment$jvm2.data;
                                int i7 = writableSegment$jvm2.limit;
                                bArr2[i7] = (byte) ((charAt >> 6) | 192);
                                bArr2[i7 + 1] = (byte) ((charAt & '?') | 128);
                                writableSegment$jvm2.limit = i7 + 2;
                                this.size += 2;
                            } else if (charAt < 55296 || charAt > 57343) {
                                Segment writableSegment$jvm3 = writableSegment$jvm(3);
                                byte[] bArr3 = writableSegment$jvm3.data;
                                int i8 = writableSegment$jvm3.limit;
                                bArr3[i8] = (byte) ((charAt >> '\f') | 224);
                                bArr3[i8 + 1] = (byte) ((63 & (charAt >> 6)) | 128);
                                bArr3[i8 + 2] = (byte) ((charAt & '?') | 128);
                                writableSegment$jvm3.limit = i8 + 3;
                                this.size += 3;
                            } else {
                                int i9 = i + 1;
                                if (i9 < i2) {
                                    c = str.charAt(i9);
                                } else {
                                    c = 0;
                                }
                                if (charAt > 56319 || 56320 > c || 57343 < c) {
                                    writeByte(63);
                                    i = i9;
                                } else {
                                    int i10 = (((charAt & 1023) << 10) | (c & 1023)) + 65536;
                                    Segment writableSegment$jvm4 = writableSegment$jvm(4);
                                    byte[] bArr4 = writableSegment$jvm4.data;
                                    int i11 = writableSegment$jvm4.limit;
                                    bArr4[i11] = (byte) ((i10 >> 18) | 240);
                                    bArr4[i11 + 1] = (byte) (((i10 >> 12) & 63) | 128);
                                    bArr4[i11 + 2] = (byte) (((i10 >> 6) & 63) | 128);
                                    bArr4[i11 + 3] = (byte) ((i10 & 63) | 128);
                                    writableSegment$jvm4.limit = i11 + 4;
                                    this.size += 4;
                                    i += 2;
                                }
                            }
                            i++;
                        }
                    }
                    return this;
                }
                throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
            }
            throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
        }
        throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
    }

    public Buffer writeUtf8CodePoint(int i) {
        if (i < 128) {
            writeByte(i);
        } else if (i < 2048) {
            Segment writableSegment$jvm = writableSegment$jvm(2);
            byte[] bArr = writableSegment$jvm.data;
            int i2 = writableSegment$jvm.limit;
            bArr[i2] = (byte) ((i >> 6) | 192);
            bArr[i2 + 1] = (byte) ((i & 63) | 128);
            writableSegment$jvm.limit = i2 + 2;
            this.size += 2;
        } else if (55296 <= i && 57343 >= i) {
            writeByte(63);
        } else if (i < 65536) {
            Segment writableSegment$jvm2 = writableSegment$jvm(3);
            byte[] bArr2 = writableSegment$jvm2.data;
            int i3 = writableSegment$jvm2.limit;
            bArr2[i3] = (byte) ((i >> 12) | 224);
            bArr2[i3 + 1] = (byte) (((i >> 6) & 63) | 128);
            bArr2[i3 + 2] = (byte) ((i & 63) | 128);
            writableSegment$jvm2.limit = i3 + 3;
            this.size += 3;
        } else if (i <= 1114111) {
            Segment writableSegment$jvm3 = writableSegment$jvm(4);
            byte[] bArr3 = writableSegment$jvm3.data;
            int i4 = writableSegment$jvm3.limit;
            bArr3[i4] = (byte) ((i >> 18) | 240);
            bArr3[i4 + 1] = (byte) (((i >> 12) & 63) | 128);
            bArr3[i4 + 2] = (byte) (((i >> 6) & 63) | 128);
            bArr3[i4 + 3] = (byte) ((i & 63) | 128);
            writableSegment$jvm3.limit = i4 + 4;
            this.size += 4;
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    public Buffer writeString(String str, Charset charset) {
        Intrinsics.checkParameterIsNotNull(str, "string");
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        return writeString(str, 0, str.length(), charset);
    }

    public Buffer writeString(String str, int i, int i2, Charset charset) {
        Intrinsics.checkParameterIsNotNull(str, "string");
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        boolean z = true;
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 > str.length()) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(("endIndex > string.length: " + i2 + " > " + str.length()).toString());
                } else if (Intrinsics.areEqual(charset, Charsets.UTF_8)) {
                    return writeUtf8(str, i, i2);
                } else {
                    String substring = str.substring(i, i2);
                    Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    if (substring != null) {
                        byte[] bytes = substring.getBytes(charset);
                        Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
                        return write(bytes, 0, bytes.length);
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new IllegalArgumentException(("endIndex < beginIndex: " + i2 + " < " + i).toString());
            }
        } else {
            throw new IllegalArgumentException(("beginIndex < 0: " + i).toString());
        }
    }

    @Override // okio.BufferedSink
    public Buffer write(byte[] bArr) {
        Intrinsics.checkParameterIsNotNull(bArr, PushDatabase.SOURCE_E164);
        return write(bArr, 0, bArr.length);
    }

    @Override // okio.BufferedSink
    public Buffer write(byte[] bArr, int i, int i2) {
        Intrinsics.checkParameterIsNotNull(bArr, PushDatabase.SOURCE_E164);
        long j = (long) i2;
        Util.checkOffsetAndCount((long) bArr.length, (long) i, j);
        int i3 = i2 + i;
        while (i < i3) {
            Segment writableSegment$jvm = writableSegment$jvm(1);
            int min = Math.min(i3 - i, 8192 - writableSegment$jvm.limit);
            System.arraycopy(bArr, i, writableSegment$jvm.data, writableSegment$jvm.limit, min);
            i += min;
            writableSegment$jvm.limit += min;
        }
        this.size += j;
        return this;
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) throws IOException {
        Intrinsics.checkParameterIsNotNull(byteBuffer, PushDatabase.SOURCE_E164);
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            Segment writableSegment$jvm = writableSegment$jvm(1);
            int min = Math.min(i, 8192 - writableSegment$jvm.limit);
            byteBuffer.get(writableSegment$jvm.data, writableSegment$jvm.limit, min);
            i -= min;
            writableSegment$jvm.limit += min;
        }
        this.size += (long) remaining;
        return remaining;
    }

    @Override // okio.BufferedSink
    public long writeAll(Source source) throws IOException {
        Intrinsics.checkParameterIsNotNull(source, PushDatabase.SOURCE_E164);
        long j = 0;
        while (true) {
            long read = source.read(this, (long) 8192);
            if (read == -1) {
                return j;
            }
            j += read;
        }
    }

    @Override // okio.BufferedSink
    public Buffer writeByte(int i) {
        Segment writableSegment$jvm = writableSegment$jvm(1);
        byte[] bArr = writableSegment$jvm.data;
        int i2 = writableSegment$jvm.limit;
        writableSegment$jvm.limit = i2 + 1;
        bArr[i2] = (byte) i;
        this.size++;
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeShort(int i) {
        Segment writableSegment$jvm = writableSegment$jvm(2);
        byte[] bArr = writableSegment$jvm.data;
        int i2 = writableSegment$jvm.limit;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        writableSegment$jvm.limit = i3 + 1;
        this.size += 2;
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeInt(int i) {
        Segment writableSegment$jvm = writableSegment$jvm(4);
        byte[] bArr = writableSegment$jvm.data;
        int i2 = writableSegment$jvm.limit;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        writableSegment$jvm.limit = i5 + 1;
        this.size += 4;
        return this;
    }

    public Buffer writeLong(long j) {
        Segment writableSegment$jvm = writableSegment$jvm(8);
        byte[] bArr = writableSegment$jvm.data;
        int i = writableSegment$jvm.limit;
        int i2 = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 56) & 255));
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) ((j >>> 48) & 255));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((int) ((j >>> 40) & 255));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((int) ((j >>> 32) & 255));
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((int) ((j >>> 24) & 255));
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((int) ((j >>> 16) & 255));
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((int) ((j >>> 8) & 255));
        bArr[i8] = (byte) ((int) (j & 255));
        writableSegment$jvm.limit = i8 + 1;
        this.size += 8;
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeDecimalLong(long j) {
        if (j == 0) {
            return writeByte(48);
        }
        boolean z = false;
        int i = 1;
        if (j < 0) {
            j = -j;
            if (j < 0) {
                return writeUtf8("-9223372036854775808");
            }
            z = true;
        }
        if (j >= 100000000) {
            i = j < 1000000000000L ? j < 10000000000L ? j < 1000000000 ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        } else if (j >= 10000) {
            i = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
        } else if (j >= 100) {
            i = j < 1000 ? 3 : 4;
        } else if (j >= 10) {
            i = 2;
        }
        if (z) {
            i++;
        }
        Segment writableSegment$jvm = writableSegment$jvm(i);
        byte[] bArr = writableSegment$jvm.data;
        int i2 = writableSegment$jvm.limit + i;
        while (j != 0) {
            long j2 = (long) 10;
            i2--;
            bArr[i2] = DIGITS[(int) (j % j2)];
            j /= j2;
        }
        if (z) {
            bArr[i2 - 1] = (byte) 45;
        }
        writableSegment$jvm.limit += i;
        this.size += (long) i;
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeHexadecimalUnsignedLong(long j) {
        if (j == 0) {
            return writeByte(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        Segment writableSegment$jvm = writableSegment$jvm(numberOfTrailingZeros);
        byte[] bArr = writableSegment$jvm.data;
        int i = writableSegment$jvm.limit;
        for (int i2 = (i + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = DIGITS[(int) (15 & j)];
            j >>>= 4;
        }
        writableSegment$jvm.limit += numberOfTrailingZeros;
        this.size += (long) numberOfTrailingZeros;
        return this;
    }

    public final Segment writableSegment$jvm(int i) {
        boolean z = true;
        if (i < 1 || i > 8192) {
            z = false;
        }
        if (z) {
            Segment segment = this.head;
            if (segment == null) {
                Segment take = SegmentPool.take();
                this.head = take;
                take.prev = take;
                take.next = take;
                return take;
            }
            if (segment == null) {
                Intrinsics.throwNpe();
            }
            Segment segment2 = segment.prev;
            if (segment2 == null) {
                Intrinsics.throwNpe();
            }
            return (segment2.limit + i > 8192 || !segment2.owner) ? segment2.push(SegmentPool.take()) : segment2;
        }
        throw new IllegalArgumentException("unexpected capacity".toString());
    }

    @Override // okio.Sink
    public void write(Buffer buffer, long j) {
        Segment segment;
        Intrinsics.checkParameterIsNotNull(buffer, PushDatabase.SOURCE_E164);
        if (buffer != this) {
            Util.checkOffsetAndCount(buffer.size, 0, j);
            while (j > 0) {
                Segment segment2 = buffer.head;
                if (segment2 == null) {
                    Intrinsics.throwNpe();
                }
                int i = segment2.limit;
                Segment segment3 = buffer.head;
                if (segment3 == null) {
                    Intrinsics.throwNpe();
                }
                if (j < ((long) (i - segment3.pos))) {
                    Segment segment4 = this.head;
                    if (segment4 != null) {
                        if (segment4 == null) {
                            Intrinsics.throwNpe();
                        }
                        segment = segment4.prev;
                    } else {
                        segment = null;
                    }
                    if (segment != null && segment.owner) {
                        if ((((long) segment.limit) + j) - ((long) (segment.shared ? 0 : segment.pos)) <= ((long) 8192)) {
                            Segment segment5 = buffer.head;
                            if (segment5 == null) {
                                Intrinsics.throwNpe();
                            }
                            segment5.writeTo(segment, (int) j);
                            buffer.size -= j;
                            this.size += j;
                            return;
                        }
                    }
                    Segment segment6 = buffer.head;
                    if (segment6 == null) {
                        Intrinsics.throwNpe();
                    }
                    buffer.head = segment6.split((int) j);
                }
                Segment segment7 = buffer.head;
                if (segment7 == null) {
                    Intrinsics.throwNpe();
                }
                long j2 = (long) (segment7.limit - segment7.pos);
                buffer.head = segment7.pop();
                Segment segment8 = this.head;
                if (segment8 == null) {
                    this.head = segment7;
                    segment7.prev = segment7;
                    segment7.next = segment7;
                } else {
                    if (segment8 == null) {
                        Intrinsics.throwNpe();
                    }
                    Segment segment9 = segment8.prev;
                    if (segment9 == null) {
                        Intrinsics.throwNpe();
                    }
                    segment9.push(segment7).compact();
                }
                buffer.size -= j2;
                this.size += j2;
                j -= j2;
            }
            return;
        }
        throw new IllegalArgumentException("source == this".toString());
    }

    @Override // okio.Source
    public long read(Buffer buffer, long j) {
        Intrinsics.checkParameterIsNotNull(buffer, "sink");
        if (j >= 0) {
            long j2 = this.size;
            if (j2 == 0) {
                return -1;
            }
            if (j > j2) {
                j = j2;
            }
            buffer.write(this, j);
            return j;
        }
        throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
    }

    public long indexOfElement(ByteString byteString, long j) {
        int i;
        int i2;
        Intrinsics.checkParameterIsNotNull(byteString, "targetBytes");
        long j2 = 0;
        if (j >= 0) {
            Segment segment = this.head;
            if (segment == null) {
                return -1;
            }
            if (size() - j < j) {
                j2 = size();
                while (j2 > j) {
                    segment = segment.prev;
                    if (segment == null) {
                        Intrinsics.throwNpe();
                    }
                    j2 -= (long) (segment.limit - segment.pos);
                }
                if (byteString.size() == 2) {
                    byte b = byteString.getByte(0);
                    byte b2 = byteString.getByte(1);
                    while (j2 < this.size) {
                        byte[] bArr = segment.data;
                        i = (int) ((((long) segment.pos) + j) - j2);
                        int i3 = segment.limit;
                        while (i < i3) {
                            byte b3 = bArr[i];
                            if (!(b3 == b || b3 == b2)) {
                                i++;
                            }
                            i2 = segment.pos;
                        }
                        j2 += (long) (segment.limit - segment.pos);
                        segment = segment.next;
                        if (segment == null) {
                            Intrinsics.throwNpe();
                        }
                        j = j2;
                    }
                } else {
                    byte[] internalArray$jvm = byteString.internalArray$jvm();
                    while (j2 < this.size) {
                        byte[] bArr2 = segment.data;
                        i = (int) ((((long) segment.pos) + j) - j2);
                        int i4 = segment.limit;
                        while (i < i4) {
                            byte b4 = bArr2[i];
                            for (byte b5 : internalArray$jvm) {
                                if (b4 == b5) {
                                    i2 = segment.pos;
                                }
                            }
                            i++;
                        }
                        j2 += (long) (segment.limit - segment.pos);
                        segment = segment.next;
                        if (segment == null) {
                            Intrinsics.throwNpe();
                        }
                        j = j2;
                    }
                }
                return -1;
            }
            while (true) {
                long j3 = ((long) (segment.limit - segment.pos)) + j2;
                if (j3 > j) {
                    break;
                }
                segment = segment.next;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                j2 = j3;
            }
            if (byteString.size() == 2) {
                byte b6 = byteString.getByte(0);
                byte b7 = byteString.getByte(1);
                while (j2 < this.size) {
                    byte[] bArr3 = segment.data;
                    i = (int) ((((long) segment.pos) + j) - j2);
                    int i5 = segment.limit;
                    while (i < i5) {
                        byte b8 = bArr3[i];
                        if (!(b8 == b6 || b8 == b7)) {
                            i++;
                        }
                        i2 = segment.pos;
                    }
                    j2 += (long) (segment.limit - segment.pos);
                    segment = segment.next;
                    if (segment == null) {
                        Intrinsics.throwNpe();
                    }
                    j = j2;
                }
            } else {
                byte[] internalArray$jvm2 = byteString.internalArray$jvm();
                while (j2 < this.size) {
                    byte[] bArr4 = segment.data;
                    i = (int) ((((long) segment.pos) + j) - j2);
                    int i6 = segment.limit;
                    while (i < i6) {
                        byte b9 = bArr4[i];
                        for (byte b10 : internalArray$jvm2) {
                            if (b9 == b10) {
                                i2 = segment.pos;
                            }
                        }
                        i++;
                    }
                    j2 += (long) (segment.limit - segment.pos);
                    segment = segment.next;
                    if (segment == null) {
                        Intrinsics.throwNpe();
                    }
                    j = j2;
                }
            }
            return -1;
            return ((long) (i - i2)) + j2;
        }
        throw new IllegalArgumentException(("fromIndex < 0: " + j).toString());
    }

    public long indexOf(byte b, long j, long j2) {
        Segment segment;
        int i;
        long j3 = 0;
        if (0 <= j && j2 >= j) {
            long j4 = this.size;
            if (j2 > j4) {
                j2 = j4;
            }
            if (j == j2 || (segment = this.head) == null) {
                return -1;
            }
            if (size() - j < j) {
                j3 = size();
                while (j3 > j) {
                    segment = segment.prev;
                    if (segment == null) {
                        Intrinsics.throwNpe();
                    }
                    j3 -= (long) (segment.limit - segment.pos);
                }
                while (j3 < j2) {
                    byte[] bArr = segment.data;
                    int min = (int) Math.min((long) segment.limit, (((long) segment.pos) + j2) - j3);
                    i = (int) ((((long) segment.pos) + j) - j3);
                    while (i < min) {
                        if (bArr[i] != b) {
                            i++;
                        }
                    }
                    j3 += (long) (segment.limit - segment.pos);
                    segment = segment.next;
                    if (segment == null) {
                        Intrinsics.throwNpe();
                    }
                    j = j3;
                }
                return -1;
            }
            while (true) {
                long j5 = ((long) (segment.limit - segment.pos)) + j3;
                if (j5 > j) {
                    break;
                }
                segment = segment.next;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                j3 = j5;
            }
            while (j3 < j2) {
                byte[] bArr2 = segment.data;
                int min2 = (int) Math.min((long) segment.limit, (((long) segment.pos) + j2) - j3);
                i = (int) ((((long) segment.pos) + j) - j3);
                while (i < min2) {
                    if (bArr2[i] != b) {
                        i++;
                    }
                }
                j3 += (long) (segment.limit - segment.pos);
                segment = segment.next;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                j = j3;
            }
            return -1;
            return ((long) (i - segment.pos)) + j3;
        }
        throw new IllegalArgumentException(("size=" + this.size + " fromIndex=" + j + " toIndex=" + j2).toString());
    }

    @Override // okio.BufferedSource
    public long indexOf(ByteString byteString) throws IOException {
        Intrinsics.checkParameterIsNotNull(byteString, "bytes");
        return indexOf(byteString, 0);
    }

    public long indexOf(ByteString byteString, long j) throws IOException {
        Buffer buffer = this;
        long j2 = j;
        Intrinsics.checkParameterIsNotNull(byteString, "bytes");
        boolean z = true;
        if (byteString.size() > 0) {
            long j3 = 0;
            if (j2 < 0) {
                z = false;
            }
            if (z) {
                Segment segment = buffer.head;
                if (segment == null) {
                    return -1;
                }
                if (size() - j2 < j2) {
                    long size = size();
                    while (size > j2) {
                        segment = segment.prev;
                        if (segment == null) {
                            Intrinsics.throwNpe();
                        }
                        size -= (long) (segment.limit - segment.pos);
                    }
                    byte[] internalArray$jvm = byteString.internalArray$jvm();
                    byte b = internalArray$jvm[0];
                    int size2 = byteString.size();
                    long j4 = size;
                    Segment segment2 = segment;
                    for (long j5 = (buffer.size - ((long) size2)) + 1; j4 < j5; j5 = j5) {
                        byte[] bArr = segment2.data;
                        int min = (int) Math.min((long) segment2.limit, (((long) segment2.pos) + j5) - j4);
                        for (int i = (int) ((((long) segment2.pos) + j2) - j4); i < min; i++) {
                            if (bArr[i] == b && rangeEquals(segment2, i + 1, internalArray$jvm, 1, size2)) {
                                return ((long) (i - segment2.pos)) + j4;
                            }
                        }
                        j4 += (long) (segment2.limit - segment2.pos);
                        segment2 = segment2.next;
                        if (segment2 == null) {
                            Intrinsics.throwNpe();
                        }
                        j2 = j4;
                    }
                    return -1;
                }
                while (true) {
                    long j6 = ((long) (segment.limit - segment.pos)) + j3;
                    if (j6 > j2) {
                        break;
                    }
                    segment = segment.next;
                    if (segment == null) {
                        Intrinsics.throwNpe();
                    }
                    buffer = this;
                    j3 = j6;
                }
                byte[] internalArray$jvm2 = byteString.internalArray$jvm();
                byte b2 = internalArray$jvm2[0];
                int size3 = byteString.size();
                Segment segment3 = segment;
                for (long j7 = 1 + (buffer.size - ((long) size3)); j3 < j7; j7 = j7) {
                    byte[] bArr2 = segment3.data;
                    int min2 = (int) Math.min((long) segment3.limit, (((long) segment3.pos) + j7) - j3);
                    for (int i2 = (int) ((((long) segment3.pos) + j2) - j3); i2 < min2; i2++) {
                        if (bArr2[i2] == b2 && rangeEquals(segment3, i2 + 1, internalArray$jvm2, 1, size3)) {
                            return ((long) (i2 - segment3.pos)) + j3;
                        }
                    }
                    j3 += (long) (segment3.limit - segment3.pos);
                    segment3 = segment3.next;
                    if (segment3 == null) {
                        Intrinsics.throwNpe();
                    }
                    j2 = j3;
                }
                return -1;
            }
            throw new IllegalArgumentException(("fromIndex < 0: " + j2).toString());
        }
        throw new IllegalArgumentException("bytes is empty".toString());
    }

    @Override // okio.BufferedSource
    public long indexOfElement(ByteString byteString) {
        Intrinsics.checkParameterIsNotNull(byteString, "targetBytes");
        return indexOfElement(byteString, 0);
    }

    @Override // okio.BufferedSource
    public boolean rangeEquals(long j, ByteString byteString) {
        Intrinsics.checkParameterIsNotNull(byteString, "bytes");
        return rangeEquals(j, byteString, 0, byteString.size());
    }

    public boolean rangeEquals(long j, ByteString byteString, int i, int i2) {
        Intrinsics.checkParameterIsNotNull(byteString, "bytes");
        if (j < 0 || i < 0 || i2 < 0 || this.size - j < ((long) i2) || byteString.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (getByte(((long) i3) + j) != byteString.getByte(i + i3)) {
                return false;
            }
        }
        return true;
    }

    private final boolean rangeEquals(Segment segment, int i, byte[] bArr, int i2, int i3) {
        int i4 = segment.limit;
        byte[] bArr2 = segment.data;
        while (i2 < i3) {
            if (i == i4) {
                segment = segment.next;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                byte[] bArr3 = segment.data;
                int i5 = segment.pos;
                i4 = segment.limit;
                bArr2 = bArr3;
                i = i5;
            }
            if (bArr2[i] != bArr[i2]) {
                return false;
            }
            i++;
            i2++;
        }
        return true;
    }

    @Override // okio.Source
    public Timeout timeout() {
        return Timeout.NONE;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Buffer)) {
            return false;
        }
        long j = this.size;
        Buffer buffer = (Buffer) obj;
        if (j != buffer.size) {
            return false;
        }
        if (j == 0) {
            return true;
        }
        Segment segment = this.head;
        if (segment == null) {
            Intrinsics.throwNpe();
        }
        Segment segment2 = buffer.head;
        if (segment2 == null) {
            Intrinsics.throwNpe();
        }
        int i = segment.pos;
        int i2 = segment2.pos;
        long j2 = 0;
        while (j2 < this.size) {
            long min = (long) Math.min(segment.limit - i, segment2.limit - i2);
            for (long j3 = 0; j3 < min; j3++) {
                i++;
                i2++;
                if (segment.data[i] != segment2.data[i2]) {
                    return false;
                }
            }
            if (i == segment.limit) {
                segment = segment.next;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                i = segment.pos;
            }
            if (i2 == segment2.limit) {
                segment2 = segment2.next;
                if (segment2 == null) {
                    Intrinsics.throwNpe();
                }
                i2 = segment2.pos;
            }
            j2 += min;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Segment segment = this.head;
        if (segment == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = segment.limit;
            for (int i3 = segment.pos; i3 < i2; i3++) {
                i = (i * 31) + segment.data[i3];
            }
            segment = segment.next;
            if (segment == null) {
                Intrinsics.throwNpe();
            }
        } while (segment != this.head);
        return i;
    }

    @Override // java.lang.Object
    public String toString() {
        return snapshot().toString();
    }

    @Override // java.lang.Object
    public Buffer clone() {
        Buffer buffer = new Buffer();
        if (this.size == 0) {
            return buffer;
        }
        Segment segment = this.head;
        if (segment == null) {
            Intrinsics.throwNpe();
        }
        Segment sharedCopy = segment.sharedCopy();
        buffer.head = sharedCopy;
        if (sharedCopy == null) {
            Intrinsics.throwNpe();
        }
        Segment segment2 = buffer.head;
        sharedCopy.prev = segment2;
        if (segment2 == null) {
            Intrinsics.throwNpe();
        }
        Segment segment3 = buffer.head;
        if (segment3 == null) {
            Intrinsics.throwNpe();
        }
        segment2.next = segment3.prev;
        Segment segment4 = this.head;
        if (segment4 == null) {
            Intrinsics.throwNpe();
        }
        for (Segment segment5 = segment4.next; segment5 != this.head; segment5 = segment5.next) {
            Segment segment6 = buffer.head;
            if (segment6 == null) {
                Intrinsics.throwNpe();
            }
            Segment segment7 = segment6.prev;
            if (segment7 == null) {
                Intrinsics.throwNpe();
            }
            if (segment5 == null) {
                Intrinsics.throwNpe();
            }
            segment7.push(segment5.sharedCopy());
        }
        buffer.size = this.size;
        return buffer;
    }

    public final ByteString snapshot() {
        long j = this.size;
        if (j <= ((long) Integer.MAX_VALUE)) {
            return snapshot((int) j);
        }
        throw new IllegalStateException(("size > Integer.MAX_VALUE: " + this.size).toString());
    }

    public final ByteString snapshot(int i) {
        return i == 0 ? ByteString.EMPTY : SegmentedByteString.Companion.of(this, i);
    }

    public final UnsafeCursor readAndWriteUnsafe(UnsafeCursor unsafeCursor) {
        Intrinsics.checkParameterIsNotNull(unsafeCursor, "unsafeCursor");
        if (unsafeCursor.buffer == null) {
            unsafeCursor.buffer = this;
            unsafeCursor.readWrite = true;
            return unsafeCursor;
        }
        throw new IllegalStateException("already attached to a buffer".toString());
    }

    /* compiled from: Buffer.kt */
    @Metadata(bv = {}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0012\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0019\u0010\u001aJ\u0006\u0010\u0003\u001a\u00020\u0002J\u000e\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004J\b\u0010\b\u001a\u00020\u0007H\u0016R\u0018\u0010\n\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u000e¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0006@\u0006X\u000e¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0005\u001a\u00020\u00048\u0006@\u0006X\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010\u0012R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0006@\u0006X\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00028\u0006@\u0006X\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00028\u0006@\u0006X\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0017¨\u0006\u001b"}, d2 = {"Lokio/Buffer$UnsafeCursor;", "Ljava/io/Closeable;", "", "next", "", "offset", "seek", "", "close", "Lokio/Buffer;", "buffer", "Lokio/Buffer;", "", "readWrite", "Z", "Lokio/Segment;", "segment", "Lokio/Segment;", "J", "", "data", "[B", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "I", NotificationProfileDatabase.NotificationProfileScheduleTable.END, "<init>", "()V", "jvm"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes3.dex */
    public static final class UnsafeCursor implements Closeable {
        public Buffer buffer;
        public byte[] data;
        public int end = -1;
        public long offset = -1;
        public boolean readWrite;
        private Segment segment;
        public int start = -1;

        public final int next() {
            long j = this.offset;
            Buffer buffer = this.buffer;
            if (buffer == null) {
                Intrinsics.throwNpe();
            }
            if (j != buffer.size()) {
                long j2 = this.offset;
                return seek(j2 == -1 ? 0 : j2 + ((long) (this.end - this.start)));
            }
            throw new IllegalStateException("no more bytes".toString());
        }

        public final int seek(long j) {
            Segment segment;
            Buffer buffer = this.buffer;
            if (buffer == null) {
                throw new IllegalStateException("not attached to a buffer".toString());
            } else if (j < ((long) -1) || j > buffer.size()) {
                StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
                String format = String.format("offset=%s > size=%s", Arrays.copyOf(new Object[]{Long.valueOf(j), Long.valueOf(buffer.size())}, 2));
                Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
                throw new ArrayIndexOutOfBoundsException(format);
            } else if (j == -1 || j == buffer.size()) {
                this.segment = null;
                this.offset = j;
                this.data = null;
                this.start = -1;
                this.end = -1;
                return -1;
            } else {
                long j2 = 0;
                long size = buffer.size();
                Segment segment2 = buffer.head;
                Segment segment3 = this.segment;
                if (segment3 != null) {
                    long j3 = this.offset;
                    int i = this.start;
                    if (segment3 == null) {
                        Intrinsics.throwNpe();
                    }
                    long j4 = j3 - ((long) (i - segment3.pos));
                    if (j4 > j) {
                        segment2 = this.segment;
                        segment = segment2;
                        size = j4;
                    } else {
                        segment = this.segment;
                        j2 = j4;
                    }
                } else {
                    segment = segment2;
                }
                if (size - j > j - j2) {
                    while (true) {
                        if (segment == null) {
                            Intrinsics.throwNpe();
                        }
                        int i2 = segment.limit;
                        int i3 = segment.pos;
                        if (j < ((long) (i2 - i3)) + j2) {
                            break;
                        }
                        j2 += (long) (i2 - i3);
                        segment = segment.next;
                    }
                } else {
                    while (size > j) {
                        if (segment2 == null) {
                            Intrinsics.throwNpe();
                        }
                        segment2 = segment2.prev;
                        if (segment2 == null) {
                            Intrinsics.throwNpe();
                        }
                        size -= (long) (segment2.limit - segment2.pos);
                    }
                    j2 = size;
                    segment = segment2;
                }
                if (this.readWrite) {
                    if (segment == null) {
                        Intrinsics.throwNpe();
                    }
                    if (segment.shared) {
                        Segment unsharedCopy = segment.unsharedCopy();
                        if (buffer.head == segment) {
                            buffer.head = unsharedCopy;
                        }
                        segment = segment.push(unsharedCopy);
                        Segment segment4 = segment.prev;
                        if (segment4 == null) {
                            Intrinsics.throwNpe();
                        }
                        segment4.pop();
                    }
                }
                this.segment = segment;
                this.offset = j;
                if (segment == null) {
                    Intrinsics.throwNpe();
                }
                this.data = segment.data;
                int i4 = segment.pos + ((int) (j - j2));
                this.start = i4;
                int i5 = segment.limit;
                this.end = i5;
                return i5 - i4;
            }
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            if (this.buffer != null) {
                this.buffer = null;
                this.segment = null;
                this.offset = -1;
                this.data = null;
                this.start = -1;
                this.end = -1;
                return;
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }
    }

    /* compiled from: Buffer.kt */
    @Metadata(bv = {1, 0, 2}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lokio/Buffer$Companion;", "", "()V", "DIGITS", "", "jvm"}, k = 1, mv = {1, 1, 11})
    /* loaded from: classes3.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    static {
        Companion = new Companion(null);
        byte[] bytes = "0123456789abcdef".getBytes(Charsets.UTF_8);
        Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        DIGITS = bytes;
    }
}
