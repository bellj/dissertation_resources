package okio;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import okio.internal.ByteStringKt;
import org.thoughtcrime.securesms.MediaPreviewActivity;

/* compiled from: ByteString.kt */
@Metadata(bv = {}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0005\n\u0002\b\t\n\u0002\u0010\u0012\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0015\b\u0016\u0018\u0000 A2\u00020\u00012\b\u0012\u0004\u0012\u00020\u00000\u0002:\u0001AB\u0011\b\u0000\u0012\u0006\u0010;\u001a\u00020\u001a¢\u0006\u0004\b?\u0010@J\b\u0010\u0004\u001a\u00020\u0003H\u0016J\b\u0010\u0005\u001a\u00020\u0003H\u0016J\b\u0010\u0006\u001a\u00020\u0000H\u0016J\b\u0010\u0007\u001a\u00020\u0000H\u0016J\u0017\u0010\u000b\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\u0003H\u0010¢\u0006\u0004\b\t\u0010\nJ\b\u0010\f\u001a\u00020\u0003H\u0016J\b\u0010\r\u001a\u00020\u0000H\u0016J\u0017\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u000eH\u0010¢\u0006\u0004\b\u0011\u0010\u0012J\u0018\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0015\u0010\u0012J\u000f\u0010\u0019\u001a\u00020\u000eH\u0010¢\u0006\u0004\b\u0017\u0010\u0018J\b\u0010\u001b\u001a\u00020\u001aH\u0016J\u000f\u0010\u001e\u001a\u00020\u001aH\u0010¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010$\u001a\u00020!2\u0006\u0010 \u001a\u00020\u001fH\u0010¢\u0006\u0004\b\"\u0010#J(\u0010*\u001a\u00020)2\u0006\u0010%\u001a\u00020\u000e2\u0006\u0010&\u001a\u00020\u00002\u0006\u0010'\u001a\u00020\u000e2\u0006\u0010(\u001a\u00020\u000eH\u0016J(\u0010*\u001a\u00020)2\u0006\u0010%\u001a\u00020\u000e2\u0006\u0010&\u001a\u00020\u001a2\u0006\u0010'\u001a\u00020\u000e2\u0006\u0010(\u001a\u00020\u000eH\u0016J\u000e\u0010,\u001a\u00020)2\u0006\u0010+\u001a\u00020\u0000J\u0013\u0010.\u001a\u00020)2\b\u0010&\u001a\u0004\u0018\u00010-H\u0002J\b\u0010/\u001a\u00020\u000eH\u0016J\u0011\u00100\u001a\u00020\u000e2\u0006\u0010&\u001a\u00020\u0000H\u0002J\b\u00101\u001a\u00020\u0003H\u0016R\"\u0010/\u001a\u00020\u000e8\u0000@\u0000X\u000e¢\u0006\u0012\n\u0004\b/\u00102\u001a\u0004\b3\u0010\u0018\"\u0004\b4\u00105R$\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0000@\u0000X\u000e¢\u0006\u0012\n\u0004\b\u0004\u00106\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R\u001a\u0010;\u001a\u00020\u001a8\u0000X\u0004¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010\u001dR\u0011\u0010>\u001a\u00020\u000e8G¢\u0006\u0006\u001a\u0004\b>\u0010\u0018¨\u0006B"}, d2 = {"Lokio/ByteString;", "Ljava/io/Serializable;", "", "", "utf8", "base64", "sha1", "sha256", "algorithm", "digest$jvm", "(Ljava/lang/String;)Lokio/ByteString;", "digest", "hex", "toAsciiLowercase", "", "pos", "", "internalGet$jvm", "(I)B", "internalGet", "index", "getByte", "get", "getSize$jvm", "()I", "getSize", "", "toByteArray", "internalArray$jvm", "()[B", "internalArray", "Lokio/Buffer;", "buffer", "", "write$jvm", "(Lokio/Buffer;)V", "write", "offset", "other", "otherOffset", "byteCount", "", "rangeEquals", "prefix", "startsWith", "", "equals", "hashCode", "compareTo", "toString", "I", "getHashCode$jvm", "setHashCode$jvm", "(I)V", "Ljava/lang/String;", "getUtf8$jvm", "()Ljava/lang/String;", "setUtf8$jvm", "(Ljava/lang/String;)V", "data", "[B", "getData$jvm", MediaPreviewActivity.SIZE_EXTRA, "<init>", "([B)V", "Companion", "jvm"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public class ByteString implements Serializable, Comparable<ByteString> {
    public static final Companion Companion = new Companion(null);
    public static final ByteString EMPTY = ByteStringKt.getCOMMON_EMPTY();
    private final byte[] data;
    private transient int hashCode;
    private transient String utf8;

    @JvmStatic
    public static final ByteString decodeBase64(String str) {
        return Companion.decodeBase64(str);
    }

    @JvmStatic
    public static final ByteString decodeHex(String str) {
        return Companion.decodeHex(str);
    }

    @JvmStatic
    public static final ByteString encodeString(String str, Charset charset) {
        return Companion.encodeString(str, charset);
    }

    @JvmStatic
    public static final ByteString encodeUtf8(String str) {
        return Companion.encodeUtf8(str);
    }

    @JvmStatic
    public static final ByteString of(byte... bArr) {
        return Companion.of(bArr);
    }

    public ByteString(byte[] bArr) {
        Intrinsics.checkParameterIsNotNull(bArr, "data");
        this.data = bArr;
    }

    public final byte[] getData$jvm() {
        return this.data;
    }

    public final int getHashCode$jvm() {
        return this.hashCode;
    }

    public final void setHashCode$jvm(int i) {
        this.hashCode = i;
    }

    public final String getUtf8$jvm() {
        return this.utf8;
    }

    public final void setUtf8$jvm(String str) {
        this.utf8 = str;
    }

    public String utf8() {
        return ByteStringKt.commonUtf8(this);
    }

    public String base64() {
        return ByteStringKt.commonBase64(this);
    }

    public ByteString sha1() {
        return digest$jvm("SHA-1");
    }

    public ByteString sha256() {
        return digest$jvm("SHA-256");
    }

    public ByteString digest$jvm(String str) {
        Intrinsics.checkParameterIsNotNull(str, "algorithm");
        byte[] digest = MessageDigest.getInstance(str).digest(this.data);
        Intrinsics.checkExpressionValueIsNotNull(digest, "MessageDigest.getInstance(algorithm).digest(data)");
        return new ByteString(digest);
    }

    public String hex() {
        return ByteStringKt.commonHex(this);
    }

    public ByteString toAsciiLowercase() {
        return ByteStringKt.commonToAsciiLowercase(this);
    }

    public byte internalGet$jvm(int i) {
        return ByteStringKt.commonGetByte(this, i);
    }

    public final byte getByte(int i) {
        return internalGet$jvm(i);
    }

    public final int size() {
        return getSize$jvm();
    }

    public int getSize$jvm() {
        return ByteStringKt.commonGetSize(this);
    }

    public byte[] toByteArray() {
        return ByteStringKt.commonToByteArray(this);
    }

    public byte[] internalArray$jvm() {
        return ByteStringKt.commonInternalArray(this);
    }

    public void write$jvm(Buffer buffer) {
        Intrinsics.checkParameterIsNotNull(buffer, "buffer");
        byte[] bArr = this.data;
        buffer.write(bArr, 0, bArr.length);
    }

    public boolean rangeEquals(int i, ByteString byteString, int i2, int i3) {
        Intrinsics.checkParameterIsNotNull(byteString, "other");
        return ByteStringKt.commonRangeEquals(this, i, byteString, i2, i3);
    }

    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        Intrinsics.checkParameterIsNotNull(bArr, "other");
        return ByteStringKt.commonRangeEquals(this, i, bArr, i2, i3);
    }

    public final boolean startsWith(ByteString byteString) {
        Intrinsics.checkParameterIsNotNull(byteString, "prefix");
        return ByteStringKt.commonStartsWith(this, byteString);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return ByteStringKt.commonEquals(this, obj);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ByteStringKt.commonHashCode(this);
    }

    public int compareTo(ByteString byteString) {
        Intrinsics.checkParameterIsNotNull(byteString, "other");
        return ByteStringKt.commonCompareTo(this, byteString);
    }

    @Override // java.lang.Object
    public String toString() {
        return ByteStringKt.commonToString(this);
    }

    /* compiled from: ByteString.kt */
    @Metadata(bv = {}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0012\n\u0002\u0010\u0005\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0014\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00020\u0002\"\u00020\u0003H\u0007J\f\u0010\b\u001a\u00020\u0005*\u00020\u0007H\u0007J\u001d\u0010\r\u001a\u00020\u0005*\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\tH\u0007¢\u0006\u0004\b\u000b\u0010\fJ\u000e\u0010\u000e\u001a\u0004\u0018\u00010\u0005*\u00020\u0007H\u0007J\f\u0010\u000f\u001a\u00020\u0005*\u00020\u0007H\u0007R\u0014\u0010\u0010\u001a\u00020\u00058\u0006X\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0013\u001a\u00020\u00128\u0002XT¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lokio/ByteString$Companion;", "", "", "", "data", "Lokio/ByteString;", "of", "", "encodeUtf8", "Ljava/nio/charset/Charset;", "charset", "encodeString", "(Ljava/lang/String;Ljava/nio/charset/Charset;)Lokio/ByteString;", "encode", "decodeBase64", "decodeHex", "EMPTY", "Lokio/ByteString;", "", "serialVersionUID", "J", "<init>", "()V", "jvm"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes3.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final ByteString of(byte... bArr) {
            Intrinsics.checkParameterIsNotNull(bArr, "data");
            return ByteStringKt.commonOf(bArr);
        }

        @JvmStatic
        public final ByteString encodeUtf8(String str) {
            Intrinsics.checkParameterIsNotNull(str, "$receiver");
            return ByteStringKt.commonEncodeUtf8(str);
        }

        @JvmStatic
        public final ByteString encodeString(String str, Charset charset) {
            Intrinsics.checkParameterIsNotNull(str, "$receiver");
            Intrinsics.checkParameterIsNotNull(charset, "charset");
            byte[] bytes = str.getBytes(charset);
            Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            return new ByteString(bytes);
        }

        @JvmStatic
        public final ByteString decodeBase64(String str) {
            Intrinsics.checkParameterIsNotNull(str, "$receiver");
            return ByteStringKt.commonDecodeBase64(str);
        }

        @JvmStatic
        public final ByteString decodeHex(String str) {
            Intrinsics.checkParameterIsNotNull(str, "$receiver");
            return ByteStringKt.commonDecodeHex(str);
        }
    }
}
