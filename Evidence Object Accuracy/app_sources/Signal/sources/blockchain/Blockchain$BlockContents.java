package blockchain;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$KeyImage;
import com.mobilecoin.api.MobileCoinAPI$KeyImageOrBuilder;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.api.MobileCoinAPI$TxOutOrBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes.dex */
public final class Blockchain$BlockContents extends GeneratedMessageLite<Blockchain$BlockContents, Builder> implements MessageLiteOrBuilder {
    private static final Blockchain$BlockContents DEFAULT_INSTANCE;
    public static final int KEY_IMAGES_FIELD_NUMBER;
    public static final int OUTPUTS_FIELD_NUMBER;
    private static volatile Parser<Blockchain$BlockContents> PARSER;
    private Internal.ProtobufList<MobileCoinAPI$KeyImage> keyImages_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<MobileCoinAPI$TxOut> outputs_ = GeneratedMessageLite.emptyProtobufList();

    private Blockchain$BlockContents() {
    }

    public List<MobileCoinAPI$KeyImage> getKeyImagesList() {
        return this.keyImages_;
    }

    public List<? extends MobileCoinAPI$KeyImageOrBuilder> getKeyImagesOrBuilderList() {
        return this.keyImages_;
    }

    public int getKeyImagesCount() {
        return this.keyImages_.size();
    }

    public MobileCoinAPI$KeyImage getKeyImages(int i) {
        return this.keyImages_.get(i);
    }

    public MobileCoinAPI$KeyImageOrBuilder getKeyImagesOrBuilder(int i) {
        return this.keyImages_.get(i);
    }

    private void ensureKeyImagesIsMutable() {
        if (!this.keyImages_.isModifiable()) {
            this.keyImages_ = GeneratedMessageLite.mutableCopy(this.keyImages_);
        }
    }

    public void setKeyImages(int i, MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        ensureKeyImagesIsMutable();
        this.keyImages_.set(i, mobileCoinAPI$KeyImage);
    }

    public void setKeyImages(int i, MobileCoinAPI$KeyImage.Builder builder) {
        ensureKeyImagesIsMutable();
        this.keyImages_.set(i, builder.build());
    }

    public void addKeyImages(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        ensureKeyImagesIsMutable();
        this.keyImages_.add(mobileCoinAPI$KeyImage);
    }

    public void addKeyImages(int i, MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        ensureKeyImagesIsMutable();
        this.keyImages_.add(i, mobileCoinAPI$KeyImage);
    }

    public void addKeyImages(MobileCoinAPI$KeyImage.Builder builder) {
        ensureKeyImagesIsMutable();
        this.keyImages_.add(builder.build());
    }

    public void addKeyImages(int i, MobileCoinAPI$KeyImage.Builder builder) {
        ensureKeyImagesIsMutable();
        this.keyImages_.add(i, builder.build());
    }

    public void addAllKeyImages(Iterable<? extends MobileCoinAPI$KeyImage> iterable) {
        ensureKeyImagesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.keyImages_);
    }

    public void clearKeyImages() {
        this.keyImages_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeKeyImages(int i) {
        ensureKeyImagesIsMutable();
        this.keyImages_.remove(i);
    }

    public List<MobileCoinAPI$TxOut> getOutputsList() {
        return this.outputs_;
    }

    public List<? extends MobileCoinAPI$TxOutOrBuilder> getOutputsOrBuilderList() {
        return this.outputs_;
    }

    public int getOutputsCount() {
        return this.outputs_.size();
    }

    public MobileCoinAPI$TxOut getOutputs(int i) {
        return this.outputs_.get(i);
    }

    public MobileCoinAPI$TxOutOrBuilder getOutputsOrBuilder(int i) {
        return this.outputs_.get(i);
    }

    private void ensureOutputsIsMutable() {
        if (!this.outputs_.isModifiable()) {
            this.outputs_ = GeneratedMessageLite.mutableCopy(this.outputs_);
        }
    }

    public void setOutputs(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.set(i, mobileCoinAPI$TxOut);
    }

    public void setOutputs(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.set(i, builder.build());
    }

    public void addOutputs(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.add(mobileCoinAPI$TxOut);
    }

    public void addOutputs(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.add(i, mobileCoinAPI$TxOut);
    }

    public void addOutputs(MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.add(builder.build());
    }

    public void addOutputs(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.add(i, builder.build());
    }

    public void addAllOutputs(Iterable<? extends MobileCoinAPI$TxOut> iterable) {
        ensureOutputsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.outputs_);
    }

    public void clearOutputs() {
        this.outputs_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeOutputs(int i) {
        ensureOutputsIsMutable();
        this.outputs_.remove(i);
    }

    public static Blockchain$BlockContents parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Blockchain$BlockContents parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Blockchain$BlockContents parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Blockchain$BlockContents parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Blockchain$BlockContents parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Blockchain$BlockContents parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Blockchain$BlockContents parseFrom(InputStream inputStream) throws IOException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$BlockContents parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$BlockContents parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$BlockContents parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$BlockContents parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Blockchain$BlockContents parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockContents) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Blockchain$BlockContents blockchain$BlockContents) {
        return DEFAULT_INSTANCE.createBuilder(blockchain$BlockContents);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Blockchain$BlockContents, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Blockchain$1 blockchain$1) {
            this();
        }

        private Builder() {
            super(Blockchain$BlockContents.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Blockchain$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Blockchain$BlockContents();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0002\u0000\u0001\u001b\u0002\u001b", new Object[]{"keyImages_", MobileCoinAPI$KeyImage.class, "outputs_", MobileCoinAPI$TxOut.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Blockchain$BlockContents> parser = PARSER;
                if (parser == null) {
                    synchronized (Blockchain$BlockContents.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Blockchain$BlockContents blockchain$BlockContents = new Blockchain$BlockContents();
        DEFAULT_INSTANCE = blockchain$BlockContents;
        GeneratedMessageLite.registerDefaultInstance(Blockchain$BlockContents.class, blockchain$BlockContents);
    }

    public static Blockchain$BlockContents getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Blockchain$BlockContents> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
