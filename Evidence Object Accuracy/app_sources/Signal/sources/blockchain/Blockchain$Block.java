package blockchain;

import blockchain.Blockchain$BlockContentsHash;
import blockchain.Blockchain$BlockID;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$TxOutMembershipElement;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Blockchain$Block extends GeneratedMessageLite<Blockchain$Block, Builder> implements Blockchain$BlockOrBuilder {
    public static final int CONTENTS_HASH_FIELD_NUMBER;
    public static final int CUMULATIVE_TXO_COUNT_FIELD_NUMBER;
    private static final Blockchain$Block DEFAULT_INSTANCE;
    public static final int ID_FIELD_NUMBER;
    public static final int INDEX_FIELD_NUMBER;
    public static final int PARENT_ID_FIELD_NUMBER;
    private static volatile Parser<Blockchain$Block> PARSER;
    public static final int ROOT_ELEMENT_FIELD_NUMBER;
    public static final int VERSION_FIELD_NUMBER;
    private Blockchain$BlockContentsHash contentsHash_;
    private long cumulativeTxoCount_;
    private Blockchain$BlockID id_;
    private long index_;
    private Blockchain$BlockID parentId_;
    private MobileCoinAPI$TxOutMembershipElement rootElement_;
    private int version_;

    private Blockchain$Block() {
    }

    public boolean hasId() {
        return this.id_ != null;
    }

    public Blockchain$BlockID getId() {
        Blockchain$BlockID blockchain$BlockID = this.id_;
        return blockchain$BlockID == null ? Blockchain$BlockID.getDefaultInstance() : blockchain$BlockID;
    }

    public void setId(Blockchain$BlockID blockchain$BlockID) {
        blockchain$BlockID.getClass();
        this.id_ = blockchain$BlockID;
    }

    public void setId(Blockchain$BlockID.Builder builder) {
        this.id_ = builder.build();
    }

    public void mergeId(Blockchain$BlockID blockchain$BlockID) {
        blockchain$BlockID.getClass();
        Blockchain$BlockID blockchain$BlockID2 = this.id_;
        if (blockchain$BlockID2 == null || blockchain$BlockID2 == Blockchain$BlockID.getDefaultInstance()) {
            this.id_ = blockchain$BlockID;
        } else {
            this.id_ = Blockchain$BlockID.newBuilder(this.id_).mergeFrom((Blockchain$BlockID.Builder) blockchain$BlockID).buildPartial();
        }
    }

    public void clearId() {
        this.id_ = null;
    }

    public int getVersion() {
        return this.version_;
    }

    public void setVersion(int i) {
        this.version_ = i;
    }

    public void clearVersion() {
        this.version_ = 0;
    }

    public boolean hasParentId() {
        return this.parentId_ != null;
    }

    public Blockchain$BlockID getParentId() {
        Blockchain$BlockID blockchain$BlockID = this.parentId_;
        return blockchain$BlockID == null ? Blockchain$BlockID.getDefaultInstance() : blockchain$BlockID;
    }

    public void setParentId(Blockchain$BlockID blockchain$BlockID) {
        blockchain$BlockID.getClass();
        this.parentId_ = blockchain$BlockID;
    }

    public void setParentId(Blockchain$BlockID.Builder builder) {
        this.parentId_ = builder.build();
    }

    public void mergeParentId(Blockchain$BlockID blockchain$BlockID) {
        blockchain$BlockID.getClass();
        Blockchain$BlockID blockchain$BlockID2 = this.parentId_;
        if (blockchain$BlockID2 == null || blockchain$BlockID2 == Blockchain$BlockID.getDefaultInstance()) {
            this.parentId_ = blockchain$BlockID;
        } else {
            this.parentId_ = Blockchain$BlockID.newBuilder(this.parentId_).mergeFrom((Blockchain$BlockID.Builder) blockchain$BlockID).buildPartial();
        }
    }

    public void clearParentId() {
        this.parentId_ = null;
    }

    public long getIndex() {
        return this.index_;
    }

    public void setIndex(long j) {
        this.index_ = j;
    }

    public void clearIndex() {
        this.index_ = 0;
    }

    public long getCumulativeTxoCount() {
        return this.cumulativeTxoCount_;
    }

    public void setCumulativeTxoCount(long j) {
        this.cumulativeTxoCount_ = j;
    }

    public void clearCumulativeTxoCount() {
        this.cumulativeTxoCount_ = 0;
    }

    public boolean hasRootElement() {
        return this.rootElement_ != null;
    }

    public MobileCoinAPI$TxOutMembershipElement getRootElement() {
        MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement = this.rootElement_;
        return mobileCoinAPI$TxOutMembershipElement == null ? MobileCoinAPI$TxOutMembershipElement.getDefaultInstance() : mobileCoinAPI$TxOutMembershipElement;
    }

    public void setRootElement(MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement) {
        mobileCoinAPI$TxOutMembershipElement.getClass();
        this.rootElement_ = mobileCoinAPI$TxOutMembershipElement;
    }

    public void setRootElement(MobileCoinAPI$TxOutMembershipElement.Builder builder) {
        this.rootElement_ = builder.build();
    }

    public void mergeRootElement(MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement) {
        mobileCoinAPI$TxOutMembershipElement.getClass();
        MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement2 = this.rootElement_;
        if (mobileCoinAPI$TxOutMembershipElement2 == null || mobileCoinAPI$TxOutMembershipElement2 == MobileCoinAPI$TxOutMembershipElement.getDefaultInstance()) {
            this.rootElement_ = mobileCoinAPI$TxOutMembershipElement;
        } else {
            this.rootElement_ = MobileCoinAPI$TxOutMembershipElement.newBuilder(this.rootElement_).mergeFrom((MobileCoinAPI$TxOutMembershipElement.Builder) mobileCoinAPI$TxOutMembershipElement).buildPartial();
        }
    }

    public void clearRootElement() {
        this.rootElement_ = null;
    }

    public boolean hasContentsHash() {
        return this.contentsHash_ != null;
    }

    public Blockchain$BlockContentsHash getContentsHash() {
        Blockchain$BlockContentsHash blockchain$BlockContentsHash = this.contentsHash_;
        return blockchain$BlockContentsHash == null ? Blockchain$BlockContentsHash.getDefaultInstance() : blockchain$BlockContentsHash;
    }

    public void setContentsHash(Blockchain$BlockContentsHash blockchain$BlockContentsHash) {
        blockchain$BlockContentsHash.getClass();
        this.contentsHash_ = blockchain$BlockContentsHash;
    }

    public void setContentsHash(Blockchain$BlockContentsHash.Builder builder) {
        this.contentsHash_ = builder.build();
    }

    public void mergeContentsHash(Blockchain$BlockContentsHash blockchain$BlockContentsHash) {
        blockchain$BlockContentsHash.getClass();
        Blockchain$BlockContentsHash blockchain$BlockContentsHash2 = this.contentsHash_;
        if (blockchain$BlockContentsHash2 == null || blockchain$BlockContentsHash2 == Blockchain$BlockContentsHash.getDefaultInstance()) {
            this.contentsHash_ = blockchain$BlockContentsHash;
        } else {
            this.contentsHash_ = Blockchain$BlockContentsHash.newBuilder(this.contentsHash_).mergeFrom((Blockchain$BlockContentsHash.Builder) blockchain$BlockContentsHash).buildPartial();
        }
    }

    public void clearContentsHash() {
        this.contentsHash_ = null;
    }

    public static Blockchain$Block parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Blockchain$Block parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Blockchain$Block parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Blockchain$Block parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Blockchain$Block parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Blockchain$Block parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Blockchain$Block parseFrom(InputStream inputStream) throws IOException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$Block parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$Block parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Blockchain$Block) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$Block parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$Block) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$Block parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Blockchain$Block parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Blockchain$Block blockchain$Block) {
        return DEFAULT_INSTANCE.createBuilder(blockchain$Block);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Blockchain$Block, Builder> implements Blockchain$BlockOrBuilder {
        /* synthetic */ Builder(Blockchain$1 blockchain$1) {
            this();
        }

        private Builder() {
            super(Blockchain$Block.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Blockchain$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Blockchain$Block();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0007\u0000\u0000\u0001\u0007\u0007\u0000\u0000\u0000\u0001\t\u0002\u000b\u0003\t\u0004\u0003\u0005\u0003\u0006\t\u0007\t", new Object[]{"id_", "version_", "parentId_", "index_", "cumulativeTxoCount_", "rootElement_", "contentsHash_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Blockchain$Block> parser = PARSER;
                if (parser == null) {
                    synchronized (Blockchain$Block.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Blockchain$Block blockchain$Block = new Blockchain$Block();
        DEFAULT_INSTANCE = blockchain$Block;
        GeneratedMessageLite.registerDefaultInstance(Blockchain$Block.class, blockchain$Block);
    }

    public static Blockchain$Block getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Blockchain$Block> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
