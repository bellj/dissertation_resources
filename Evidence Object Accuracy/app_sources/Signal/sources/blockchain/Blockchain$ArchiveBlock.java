package blockchain;

import blockchain.Blockchain$ArchiveBlockV1;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Blockchain$ArchiveBlock extends GeneratedMessageLite<Blockchain$ArchiveBlock, Builder> implements Blockchain$ArchiveBlockOrBuilder {
    private static final Blockchain$ArchiveBlock DEFAULT_INSTANCE;
    private static volatile Parser<Blockchain$ArchiveBlock> PARSER;
    public static final int V1_FIELD_NUMBER;
    private int blockCase_ = 0;
    private Object block_;

    private Blockchain$ArchiveBlock() {
    }

    /* loaded from: classes.dex */
    public enum BlockCase {
        V1(1),
        BLOCK_NOT_SET(0);
        
        private final int value;

        BlockCase(int i) {
            this.value = i;
        }

        public static BlockCase forNumber(int i) {
            if (i == 0) {
                return BLOCK_NOT_SET;
            }
            if (i != 1) {
                return null;
            }
            return V1;
        }
    }

    public BlockCase getBlockCase() {
        return BlockCase.forNumber(this.blockCase_);
    }

    public void clearBlock() {
        this.blockCase_ = 0;
        this.block_ = null;
    }

    public boolean hasV1() {
        return this.blockCase_ == 1;
    }

    public Blockchain$ArchiveBlockV1 getV1() {
        if (this.blockCase_ == 1) {
            return (Blockchain$ArchiveBlockV1) this.block_;
        }
        return Blockchain$ArchiveBlockV1.getDefaultInstance();
    }

    public void setV1(Blockchain$ArchiveBlockV1 blockchain$ArchiveBlockV1) {
        blockchain$ArchiveBlockV1.getClass();
        this.block_ = blockchain$ArchiveBlockV1;
        this.blockCase_ = 1;
    }

    public void setV1(Blockchain$ArchiveBlockV1.Builder builder) {
        this.block_ = builder.build();
        this.blockCase_ = 1;
    }

    public void mergeV1(Blockchain$ArchiveBlockV1 blockchain$ArchiveBlockV1) {
        blockchain$ArchiveBlockV1.getClass();
        if (this.blockCase_ != 1 || this.block_ == Blockchain$ArchiveBlockV1.getDefaultInstance()) {
            this.block_ = blockchain$ArchiveBlockV1;
        } else {
            this.block_ = Blockchain$ArchiveBlockV1.newBuilder((Blockchain$ArchiveBlockV1) this.block_).mergeFrom((Blockchain$ArchiveBlockV1.Builder) blockchain$ArchiveBlockV1).buildPartial();
        }
        this.blockCase_ = 1;
    }

    public void clearV1() {
        if (this.blockCase_ == 1) {
            this.blockCase_ = 0;
            this.block_ = null;
        }
    }

    public static Blockchain$ArchiveBlock parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Blockchain$ArchiveBlock parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlock parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Blockchain$ArchiveBlock parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlock parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Blockchain$ArchiveBlock parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlock parseFrom(InputStream inputStream) throws IOException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$ArchiveBlock parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlock parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$ArchiveBlock parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlock parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Blockchain$ArchiveBlock parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlock) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Blockchain$ArchiveBlock blockchain$ArchiveBlock) {
        return DEFAULT_INSTANCE.createBuilder(blockchain$ArchiveBlock);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Blockchain$ArchiveBlock, Builder> implements Blockchain$ArchiveBlockOrBuilder {
        /* synthetic */ Builder(Blockchain$1 blockchain$1) {
            this();
        }

        private Builder() {
            super(Blockchain$ArchiveBlock.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Blockchain$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Blockchain$ArchiveBlock();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001<\u0000", new Object[]{"block_", "blockCase_", Blockchain$ArchiveBlockV1.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Blockchain$ArchiveBlock> parser = PARSER;
                if (parser == null) {
                    synchronized (Blockchain$ArchiveBlock.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Blockchain$ArchiveBlock blockchain$ArchiveBlock = new Blockchain$ArchiveBlock();
        DEFAULT_INSTANCE = blockchain$ArchiveBlock;
        GeneratedMessageLite.registerDefaultInstance(Blockchain$ArchiveBlock.class, blockchain$ArchiveBlock);
    }

    public static Blockchain$ArchiveBlock getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Blockchain$ArchiveBlock> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
