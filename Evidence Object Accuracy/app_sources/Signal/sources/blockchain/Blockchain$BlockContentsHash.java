package blockchain;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Blockchain$BlockContentsHash extends GeneratedMessageLite<Blockchain$BlockContentsHash, Builder> implements MessageLiteOrBuilder {
    public static final int DATA_FIELD_NUMBER;
    private static final Blockchain$BlockContentsHash DEFAULT_INSTANCE;
    private static volatile Parser<Blockchain$BlockContentsHash> PARSER;
    private ByteString data_ = ByteString.EMPTY;

    private Blockchain$BlockContentsHash() {
    }

    public ByteString getData() {
        return this.data_;
    }

    public void setData(ByteString byteString) {
        byteString.getClass();
        this.data_ = byteString;
    }

    public void clearData() {
        this.data_ = getDefaultInstance().getData();
    }

    public static Blockchain$BlockContentsHash parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Blockchain$BlockContentsHash parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Blockchain$BlockContentsHash parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Blockchain$BlockContentsHash parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Blockchain$BlockContentsHash parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Blockchain$BlockContentsHash parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Blockchain$BlockContentsHash parseFrom(InputStream inputStream) throws IOException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$BlockContentsHash parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$BlockContentsHash parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$BlockContentsHash parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$BlockContentsHash parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Blockchain$BlockContentsHash parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockContentsHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Blockchain$BlockContentsHash blockchain$BlockContentsHash) {
        return DEFAULT_INSTANCE.createBuilder(blockchain$BlockContentsHash);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Blockchain$BlockContentsHash, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Blockchain$1 blockchain$1) {
            this();
        }

        private Builder() {
            super(Blockchain$BlockContentsHash.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Blockchain$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Blockchain$BlockContentsHash();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"data_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Blockchain$BlockContentsHash> parser = PARSER;
                if (parser == null) {
                    synchronized (Blockchain$BlockContentsHash.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Blockchain$BlockContentsHash blockchain$BlockContentsHash = new Blockchain$BlockContentsHash();
        DEFAULT_INSTANCE = blockchain$BlockContentsHash;
        GeneratedMessageLite.registerDefaultInstance(Blockchain$BlockContentsHash.class, blockchain$BlockContentsHash);
    }

    public static Blockchain$BlockContentsHash getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Blockchain$BlockContentsHash> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
